<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@	taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/configuracionDepuracion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/historicoDepuracion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<style type="text/css">

label[for="r_tipoProgramacionPD"]{
    font-size: 11px;
    font-family:arial;
}
label[for="r_tipoProgramacionPF"]{
    font-size: 11px;
    font-family:arial;
}
label[for="r_tipoProgramacionDD"]{
    font-size: 11px;
    font-family:arial;
}
label[for="txt_fechaRegistro"]{
    font-size: 11px;
    font-family:arial;
}
label[for="s_estatus"]{
    font-size: 11px;
    font-family:arial;
}

.fuente {
    font-family: arial;
    font-size: 11px;
    color: #666666;
    font-weight: bold;
}

.label {
    font-family: arial;
    font-size: 11px;
} 

</style>
<script type="text/javascript">
	var coberturasAsociadasPath = '<s:url action="obtenerCoberturasAsociadas" namespace="/siniestros/depuracion/reserva"/>';
	var coberturasDisponiblesPath = '<s:url action="obtenerCoberturasDisponibles" namespace="/siniestros/depuracion/reserva"/>';
	var terminosAjusteAsociadosPath = '<s:url action="obtenerTerminosAsociados" namespace="/siniestros/depuracion/reserva"/>';
	var terminosAjusteDisponiblesPath = '<s:url action="obtenerTerminosDisponibles" namespace="/siniestros/depuracion/reserva"/>';
	var guardarConfiguracionPath = '<s:url action="guardarDepuracion" namespace="/siniestros/depuracion/reserva"/>';
	var asociarCoberturaPath = '<s:url action="asociarCobertura" namespace="/siniestros/depuracion/reserva"/>';
	var eliminarCoberturaPath = '<s:url action="eliminarCobertura" namespace="/siniestros/depuracion/reserva"/>';
	var asociarTodosTerminosPath = '<s:url action="asociarTodosTerminos" namespace="/siniestros/depuracion/reserva"/>';
	var eliminarTodosTerminosPath = '<s:url action="eliminarTodosTermino" namespace="/siniestros/depuracion/reserva"/>';
	var asociarTodasCoberturasPath = '<s:url action="asociarTodasCoberturas" namespace="/siniestros/depuracion/reserva"/>';
	var eliminarTodasCoberturasPath = '<s:url action="eliminarTodasCoberturas" namespace="/siniestros/depuracion/reserva"/>';
	var relacionarTerminosPath = '<s:url action="relacionarTerminos" namespace="/siniestros/depuracion/reserva"/>';
	var mostrarListConfigDepuracionReservaPath = '<s:url action="mostrarBusquedaConfiguracion" namespace="/siniestros/depuracion/reserva"/>';
	var mostrarGenerarReservasADepurarPath = '<s:url action="mostrarGenerarReservasADepurar" namespace="/siniestros/depuracion/reserva/historico"/>';
</script>
<s:form id="confDepuracionForm">
	<s:hidden name="configuracion.id" id="idConfiguracion"></s:hidden>
	<s:hidden name="soloConsulta" id="soloConsulta"></s:hidden>
	<s:hidden name="noEditable" id="noEditable"></s:hidden>
	<s:hidden name="vista" id="vista"></s:hidden>
	<s:if test="%{soloConsulta == 1}">	
		<s:set id="consulta" value="true" />
	</s:if>
	<s:else>
		<s:set id="consulta" value="false" />
	</s:else>
	<table width="95%">
		<tr>
			<td>
				<s:textfield cssClass="txtfield setNew"
					name="configuracion.fechaActivo"
					key="midas.servicio.siniestros.fecha.activo"
					labelposition="left" 
					size="12"
					readonly="true"			
					id="txt_fechaRegistro"/>	
			</td>
			<td>
				<s:textfield cssClass="txtfield setNew"
					name="configuracion.fechaInactivo"
					key="midas.servicio.siniestros.fecha.inactivo"
					labelposition="left" 
					size="12"
					readonly="true"			
					id="txt_fechaRegistro"/>	
			</td>
			<td>
				<s:select id="s_estatus"
					name="estatusProceso"
					key="midas.siniestros.depuracion.Estatus"
					list="estatusList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="key" listValue="value"
					disabled="consulta" 
					cssClass="cajaTextoM2 w200"
					labelposition="left"/>
			</td>
		</tr>
	</table>
	<br></br>
	<div class="titulo" style="width:95%">
		<s:text name="midas.siniestros.depuracion.tipoDepuracion"/>	
	</div>
	<table id="filtrosM2" width="95%">
		<tr>
			<td width="150px">
				<s:text name="midas.siniestros.depuracion.nombreConfiguracion"/>	
			</td>
			<td colspan="2">
				<s:textfield cssClass="cajaTextoM2"
					name="configuracion.nombre"
					labelposition="left" 
					size="100"
					maxlength="100"
					readonly="%{soloConsulta == 1 || noEditable}"
					onkeypress="return soloAlfanumericos(this ,event ,true);"			
					id="txt_nombre"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.siniestros.depuracion.porcentajeDepurar"/>	
			</td>
			<td width="100px">
				<s:textfield cssClass="cajaTextoM2"
					name="configuracion.porcentaje"
					onkeypress="return soloNumeros(this, event, false)"	
					onkeyup="validaPorcentaje(this)"	
					labelposition="left" 
					size="12"
					readonly="%{soloConsulta == 1 || noEditable}"			
					id="txt_porcADepurar"/>
			</td>
			<td style="align:left">
				<s:checkbox id ="ch_aplicaReservaSinDep" 
					name="configuracion.aplicaSinDepurar"
					key="midas.siniestros.depuracion.aplicaReservaSinDep" labelposition="right"
					disabled="%{soloConsulta == 1 || noEditable}"
					cssClass="cajaTextoM2"/>	
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.siniestros.depuracion.programacionReservasDep"/>
			</td>
			<td colspan="2">
				<table>
					<s:hidden name="configuracion.tipoProgramacion" id="h_tipo_programacion"></s:hidden>
						<table border="0">
							<tr>
								<td class="fuente">
									<input id="ch_pd" type="checkbox" value="<s:property value="programacionPorDia"/>" onclick="validaTipoProgramacion(this)">Por Dia</input>
								</td>
								<td>
									<s:textfield cssClass="cajaTextoM2"
										name="configuracion.diaProgramacion"
										size="15"		
										maxlength="2"
										onchange="validateDia('txt_progPorDia');"
										readonly="true"
										onkeypress="return soloNumeros(this, event, false)"			
										id="txt_progPorDia"/>
								</td>
							</tr>
							<tr>
								<td class="fuente">
									<input id="ch_pf" type="checkbox" value="<s:property value="programacionPorFecha"/>" onclick="validaTipoProgramacion(this)">Por Fecha</input>
								</td>
								<td>
									<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
										name="configuracion.fechaProgramacion"
										id="dp_fechaProgramacion"
								   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
								   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				 				   		onblur="esFechaValida(this);">
				 					</sj:datepicker>	
								</td>
							</tr>
							<tr>
								<td class="fuente">
									<input id="ch_dd" type="checkbox" value="<s:property value="programacionDosDiasAntes"/>" onclick="validaTipoProgramacion(this)">Dos dias antes de cada mes</input>
								</td>
							</tr>
						</table>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<s:textarea name="configuracion.observaciones" 
							id="ta_observaciones" labelposition="top"
							key="midas.siniestros.depuracion.observaciones" 
							cols="180"
							rows="5"
							cssClass="textarea setNew"
							readonly="%{soloConsulta == 1 || noEditable}"
							onkeypress="return soloAlfanumericos(this ,event ,true);"/>
			</td>
		</tr>
	</table>
	<s:if test="%{configuracion.id != null}">	
		<br></br>
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.criterioDepuracionReservas"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:hidden name="configuracion.tipoServicio" id="h_tipo_servicio"></s:hidden>
					<table border="0">
						<tr>
							<td class="label">
								<input id="ch_sparticular" type="checkbox" value="1" onclick="seleccionaCheckButton()">
									<s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioParticular"/></input>
							</td>
						</tr>
						<tr>
							<td class="label">
								<input id="ch_spublico" type="checkbox" value="2" onclick="seleccionaCheckButton()">
									<s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioPublico"/></input>
							</td>
						</tr>
					</table>
					
				</td>
				<td>
					<s:select id="oficina"
						name="configuracion.oficina.id" 
						list="oficinaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						key="midas.siniestros.depuracion.oficina"
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="top"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="cajaTextoM2 w200"/>
				</td>
				<td>
					<s:select id="negocio"
						name="configuracion.negocio.idToNegocio" 
						list="negocioList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						key="midas.siniestros.depuracion.negocio"
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						labelposition="top"
						cssClass="cajaTextoM2 w200"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2"
						name="configuracion.numeroPoliza" 
						key="midas.siniestros.depuracion.numeroPoliza"
						labelposition="top" 
						size="12"
						onkeypress="return soloAlfanumericos(this ,event ,true);"
						maxlength="20"
						readonly="%{soloConsulta == 1 || noEditable}"				
						id="txt_numeroPoliza"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2"
						name="configuracion.numeroInciso" 
						key="midas.siniestros.depuracion.inciso"
						labelposition="top" 
						size="12"
						readonly="%{soloConsulta == 1 || noEditable}"
						onkeypress="return soloNumeros(this, event, false)"		
						maxlength="8"				
						id="txt_inciso"/>
				</td> 
				<td>
					<s:textfield cssClass="cajaTextoM2"
						name="configuracion.numeroSiniestro" 
						key="midas.siniestros.depuracion.numeroSiniestro"
						labelposition="top" 
						size="12"
						readonly="%{soloConsulta == 1 || noEditable}"
						onkeypress="return soloAlfanumericos(this ,event ,true);"
						maxlength="20"			
						id="txt_numeroSiniestro"/>
				</td>
			</tr>
		</table>
		<br></br>
		<div class="titulo" style="width:95%">
				<s:text name="midas.siniestros.depuracion.listaCoberturas"/>	
			</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_cobertura"
						name="configuracion.configCoberturas"
						key="midas.siniestros.depuracion.cobertura" labelposition="right"
						cssClass="setNew condReq"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="desahilitarBotones('ch_cobertura', 'btnElimTodasCob', 'btnAsocTodasCob');"/>	
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.siniestros.depuracion.asociar"/>
				</td>
				<td colspan="2"></td>
				<td>
					<s:text name="midas.siniestros.depuracion.asociadas"/>
				</td>
			</tr>
			<tr>
				<td width="30%">					
				<div id="indicadorCoberturasDisponibles"></div>				
					<div id="coberturasDisponiblesTree" 
					style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
								
				<td style="width: 10%;">
					<s:if test="%{soloConsulta != 1 && !noEditable}">
						<div class="btn_back w40" style="display: inline; float: right;" id="btnElimTodasCob">
							<a href="javascript: void(0);" onclick="javascript: eliminarTodasCoberturas();"> 
								<s:text name="&lt;&lt;" /> 
							</a>
						</div>
					</s:if>
				</td>
				<td style="width: 10%;">
					<s:if test="%{soloConsulta != 1 && !noEditable}">
						<div class="btn_back w40" style="display: inline; float: left;" id="btnAsocTodasCob">
							<a href="javascript: void(0);" onclick="javascript: asociarTodasCoberturas();"> 
								<s:text name="&gt;&gt;" /> 
							</a>
						</div>
					</s:if>	
				</td>
				<td>
					<div id="indicadorCoberturasAsociadas"></div>		
					<div id="coberturasAsociadasTree" 
					style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
			</tr>
		</table>
		<br></br>
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.montosReservaActual"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_condicion"
						name="configuracion.configMonto"
						key="midas.siniestros.depuracion.condicion" labelposition="right"
						cssClass="setNew condReq" 
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClassWithMonto('ch_condicion', 'monto', 'ch_rango', 'txt_montoInicial', 'txt_montoFinal')"/>	
				</td>
				<td>
					<s:select id="s_condicion"
						name="configuracion.condicionMontoActual"
						list="condicionRangoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="top"
						onchange="onChangeTipoCondicion('ch_rango', 's_condicion', 'txt_montoInicial', 'txt_montoFinal')"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="cajaTextoM2 w200 monto"/>
				</td>
				<td>
					<s:checkbox id ="ch_rango"
						name="configuracion.criterioRangoMonto"
						key="midas.siniestros.depuracion.rango" labelposition="right"
						onchange="onChangeRango('ch_rango', 's_condicion', 'txt_montoInicial', 'txt_montoFinal')"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="setNew monto"/>	
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 monto formatCurrency"
						name="configuracion.montoInicial"
						key="midas.siniestros.depuracion.monto"
						labelposition="left" 
						size="12"
						onkeypress="return soloNumeros(this, event, true)"	
						readonly="%{soloConsulta == 1 || noEditable}"	
						maxlength="16"
						onkeyup="mascaraDecimal(this.value,'#txt_montoInicial');"
						id="txt_montoInicial"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 monto formatCurrency"
						name="configuracion.montoFinal"
						key="midas.siniestros.depuracion.a"
						labelposition="left" 
						size="12"
						onkeypress="return soloNumeros(this, event, true)"		
						readonly="%{soloConsulta == 1 || noEditable}"
						maxlength="16"	
						onkeyup="mascaraDecimal(this.value,'#txt_montoFinal');"			
						id="txt_montoFinal"/>
				</td>
			</tr>
		</table>
		<br></br>
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.antiguedadSiniestro"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_periodoTiempo"
						name="configuracion.configAntiguedadSin"
						key="midas.siniestros.depuracion.periodoTiempo" labelposition="right"
						cssClass="setNew condReq"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClass('ch_periodoTiempo', 'antigSin')"/>	
				</td>
				<td>
					<s:select id="s_condicionTiempo"
						name="configuracion.condicionAntiguedadSin"
						key="midas.siniestros.depuracion.condicion"
						list="condicionRangoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						labelposition="left"
						cssClass="cajaTextoM2 w200 antigSin"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 antigSin"
						name="configuracion.valorAntiguedadSin"
						key="midas.siniestros.depuracion.antiguedad"
						labelposition="left" 
						size="12"
						readonly="%{soloConsulta == 1 || noEditable}"
						onkeypress="return soloNumeros(this, event, false)"		
						maxlength="5"				
						id="txt_antiguedad"/>
				</td>
				<td>
					<s:select id="s_antiguedad"
						name="configuracion.criterioAntiguedadSin"
						list="criterioAntiguedadList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						labelposition="left"
						cssClass="cajaTextoM2 w200 antigSin"
						onchange="habilitarAntigCompleta()" />
				</td>
				<td>
					<s:checkbox id ="ch_antiguedadCompleta"
						name="configuracion.antiguedadCompleta"
						key="midas.siniestros.depuracion.antiguedadCompleta" labelposition="right"
						cssClass="setNew condReq antigSin"
						disabled="%{soloConsulta == 1 || noEditable}"
						onchange="habilitaCondicionIgual()"/>	
				</td>
			</tr>
		</table>
		<br></br>
			
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.pagos"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_cuentaPagos"
					    title   ="Pagos"
						name    ="configuracion.configPagos"
						key     ="midas.siniestros.depuracion.cuentaPagos" labelposition="right"
						cssClass="setNew condReq combos"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick ="inhabilitaByClass('ch_cuentaPagos','pagos')"/>	
				</td>
				<td>
					<s:select id="s_cuentaPagos"
						name="aplicaPago"
						list="respuestaSimple" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="left"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="cajaTextoM2 w200 pagos combos"/>
				</td>
			</tr>
		</table>
		<br></br>
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.antiguedadSinPagos"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_periodoTiempoSinP"
						name="configuracion.configAntiguedadPago"
						key="midas.siniestros.depuracion.periodoTiempo" labelposition="right"
						cssClass="setNew condReq"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClass('ch_periodoTiempoSinP','antigSinPag')"/>	
				</td>
				<td>
					<s:select id="s_condicionTiempoSinP"
						key="midas.siniestros.depuracion.condicion"
						name="configuracion.condicionAntiguedadSinPago"
						list="condicionRangoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="left"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="cajaTextoM2 w200 antigSinPag"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 antigSinPag"
						name="configuracion.antiguedadSinPago"
						key="midas.siniestros.depuracion.antiguedad"
						labelposition="left" 
						size="12"
						onkeypress="return soloNumeros(this, event, false)"	
						readonly="%{soloConsulta == 1 || noEditable}"	
						maxlength="4"				
						id="txt_antiguedadSinP"/>
				</td>
				<td>
					<s:select id="s_antiguedadSinP"
						name="configuracion.criterioAntiguedadSinPago"
						list="criterioAntiguedadList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						labelposition="left"
						cssClass="cajaTextoM2 w200 antigSinPag"/>
				</td>
			</tr>
		</table>
		<br></br>
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.porcPagosVsEstimacionFinal"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_condicionPorcPagos"
						name="configuracion.configPorcentajePago"
						key="midas.siniestros.depuracion.condicion" labelposition="right"
						cssClass="setNew condReq"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClassWithMonto('ch_condicionPorcPagos', 'porcPag', 'ch_rangoPorcPagos', 'txt_condicionPorcPagoIni', 'txt_condicionPorcPagoFin')"/>	
				</td>
				<td>
					<s:select id="s_condicionPorcPagos"
						name="configuracion.condicionPorcentajePago"
						list="condicionRangoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						onchange="onChangeTipoCondicion('ch_rangoPorcPagos', 's_condicionPorcPagos', 'txt_condicionPorcPagoIni', 'txt_condicionPorcPagoFin')"
						labelposition="top"
						cssClass="cajaTextoM2 w200 porcPag"/>
				</td>
				<td>
					<s:text name="midas.configuracionParametrosAutorizacion.porcentaje" ></s:text>				
				</td>
				
				<td>
					<s:checkbox id ="ch_rangoPorcPagos"
						name="configuracion.criterioRangoPorcentaje"
						key="midas.siniestros.depuracion.rango" labelposition="right"
						onclick="onChangeRango('ch_rangoPorcPagos', 's_condicionPorcPagos', 'txt_condicionPorcPagoIni', 'txt_condicionPorcPagoFin')"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="setNew porcPag"/>	
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 porcPag "
						name="configuracion.porcentajePagoInicial"
						key="midas.siniestros.depuracion.monto"
						labelposition="left" 
						size="12"
						onkeypress="return soloNumeros(this, event, false)"		
						readonly="%{soloConsulta == 1 || noEditable}"
						maxlength="3"				
						id="txt_condicionPorcPagoIni"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 porcPag "
						name="configuracion.porcentajePagoFinal"
						key="midas.siniestros.depuracion.a"
						labelposition="left" 
						size="12"
						onkeypress="return soloNumeros(this, event, false)"
						readonly="%{soloConsulta == 1 || noEditable}"		
						maxlength="3"				
						id="txt_condicionPorcPagoFin"/>
				</td>
			</tr>
		</table>
		<br></br>
			
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.presupuestos"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_cuentenPres"
					    title = "Presupuestos"
						name="configuracion.configPresupuestos"
						key="midas.siniestros.depuracion.cuentenPrseupuestos" labelposition="right"
						cssClass="setNew condReq combos"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClass('ch_cuentenPres','presup')"/>	
				</td>
				<td>
					<s:select id="s_Presupuesto"
						name="aplicaPresupuesto"
						list="respuestaSimple" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="left"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="cajaTextoM2 w200 presup combos"/>
				</td>
			</tr>
		</table>
		<br></br>
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.antiguedadPresupuesto"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_periodoTiempoPres"
						name="configuracion.configAntiguedadPres"
						key="midas.siniestros.depuracion.periodoTiempo" labelposition="right"
						cssClass="setNew condReq"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClass('ch_periodoTiempoPres','antigPresup')"/>	
				</td>
				<td>
					<s:select id="s_condicionTiempoPres"
						name="configuracion.condicionAntiguedadPresupuesto"
						key="midas.siniestros.depuracion.condicion"
						list="condicionRangoList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="left"
						disabled="%{soloConsulta == 1 || noEditable}"
						cssClass="cajaTextoM2 w200 antigPresup"/>
				</td>
				<td>
					<s:textfield cssClass="cajaTextoM2 antigPresup"
						name="configuracion.antiguedadPresupuesto"
						key="midas.siniestros.depuracion.antiguedad"
						readonly="%{soloConsulta == 1 || noEditable}"
						labelposition="left" 
						size="12"			
						id="txt_antiguedadPres"/>
				</td>
				<td>
					<s:select id="s_antiguedadPres"
						name="configuracion.criterioAntiguedadPresupuesto"
						list="criterioAntiguedadList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						labelposition="left"
						onkeypress="return soloNumeros(this, event, false)"	
						disabled="%{soloConsulta == 1 || noEditable}"
						maxlength="4"	
						cssClass="cajaTextoM2 w200 antigPresup"/>
				</td>
			</tr>
		</table>
		<br></br>
			
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.criteriosDmYRcv"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_tipoPerdida"
						title = "Criterios Daños Materiales y RC Vehículos"
						name="configuracion.configDMRCV"
						key="midas.siniestros.depuracion.tipoPerdida" labelposition="right"
						cssClass="setNew condReq combos"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClass('ch_tipoPerdida','tipPerdida')"/>	
				</td>
				<td>
					<s:select id="s_tipoPerdida"
						name="configuracion.tipoPerdida"
						id   = "configuracion.configDMRCV"
						list="tipoPerdidaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						labelposition="left"
						cssClass="cajaTextoM2 w200 tipPerdida combos"/>
				</td>
			</tr>
		</table>
		<br></br>
		
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.criteriosGmYRcp"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_tipoAtencion"
					    title = "Criterios Gastos Médicos y RC Personas"
						name="configuracion.configGMRCP"
						key="midas.siniestros.depuracion.tipoAtencion" labelposition="right"
						cssClass="setNew condReq combos"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="inhabilitaByClass('ch_tipoAtencion','tipAtencion')"/>	
				</td>
				<td>
					<s:select id="s_tipoAtencion"
						name="configuracion.tipoAtencion"
						list="tipoAtencionList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}" 
						listKey="key" listValue="value" 
						disabled="%{soloConsulta == 1 || noEditable}"
						labelposition="left"
						cssClass="cajaTextoM2 w200 tipAtencion combos"/>
				</td>
			</tr>
		</table>
		<br></br>
		
		<div class="titulo" style="width:95%">
			<s:text name="midas.siniestros.depuracion.listaTerminosAjuste"/>	
		</div>
		<table id="filtrosM2" width="95%">
			<tr>
				<td>
					<s:checkbox id ="ch_terminosAjuste"
						name="configuracion.configTerminos"
						key="midas.siniestros.depuracion.terminosAjuste" labelposition="right"
						cssClass="setNew condReq"
						disabled="%{soloConsulta == 1 || noEditable}"
						onclick="desahilitarBotones('ch_terminosAjuste', 'btnElimTodosTerm', 'btnAsocTodosTerm');"/>	
				</td>
			</tr>
			<tr>
				<td>
					<s:text name="midas.siniestros.depuracion.asociar"/>
				</td>
				<td colspan="2"></td>
				<td>
					<s:text name="midas.siniestros.depuracion.asociadas"/>
				</td>
			</tr>
			<tr>
				<td width="30%">
					<div id="indicadorTerminosDisponibles"></div>
					<div id="terminosDisponiblesGrid" 
					style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
				
				<td style="width: 10%;">
					<s:if test="%{soloConsulta != 1 && !noEditable}">
						<div class="btn_back w40" style="display: inline; float: right;" id="btnElimTodosTerm">
							<a href="javascript: void(0);" onclick="javascript: eliminarTodosTerminos();"> 
								<s:text name="&lt;&lt;" /> 
							</a>
						</div>
					</s:if>
				</td>
				<td style="width: 10%;">
					<s:if test="%{soloConsulta != 1 && !noEditable}">
						<div class="btn_back w40" style="display: inline; float: left;" id="btnAsocTodosTerm">
							<a href="javascript: void(0);" onclick="javascript: asociarTodosTerminos();"> 
								<s:text name="&gt;&gt;" /> 
							</a>
						</div>
					</s:if>	
				</td>
				
				<td>
					<div id="indicadorTerminosAsociados"></div>
					<div id="terminosAsociadosGrid" 
					style="height: 300px; width: 350px;background-color: white;overflow: hidden;" ></div>
				</td>
			</tr>
		</table>
		
	</s:if>
	
	<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:regresarConfiguracionesDepuracionReserva();"> 
						<s:text name="midas.boton.cerrar" /> 
						<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
					</a>
				</div>
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar" >
					<a href="javascript: void(0);" onclick="javascript: guardarConfigDepuracion();"> 
						<s:text name="midas.boton.guardar" /> 
						<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/btn_guardar.jpg'/>
					</a>
				</div>				
				<s:if test="configuracion.estatus==1">
					<div class="btn_back w140" style="display: inline; float: right;" id="btn_nuevo" >
						<a href="javascript: void(0);" onclick="javascript: verVista();"> 
							<s:text name="midas.siniestros.depuracion.vista" />
						</a>
					</div>
				</s:if>	
			</td>							
		</tr>
	</table>		
</s:form>
<script>
	jQuery(document).ready(
	
		function(){
			initDepuracion();
			initGrids();	
			initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA	
			<s:if test="noEditable != null">
				var noEditable = <s:property value="noEditable "/>;
			</s:if>
			 <s:else>
			 	var noEditable = null;
			 </s:else>
			configuraTipoProgramacion(<s:property value="soloConsulta "/>,noEditable);
			validaTipoDeServicio(<s:property value="soloConsulta "/>,noEditable);
		}
	);

</script>