<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>			
		</beforeInit>		
		<column id="terminoAjuste" type="ro" width="*" sort="str" hidden="true">idTerminoAjuste</column>
		<column id="terminoAjusteDesc" type="ro" width="*" sort="str"><s:text name="midas.general.nombre"/></column>

	</head>

	<% int a=0;%>
	<s:iterator value="terminoAjusteAsociados">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="key" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="value" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>