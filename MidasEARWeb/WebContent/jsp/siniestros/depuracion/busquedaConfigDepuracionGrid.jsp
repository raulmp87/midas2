<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="servicio" type="ro" width="80" sort="str"><s:text name="midas.siniestros.depuracion.servicio"/></column>
		<column id="oficina" type="ro" width="200" sort="str"><s:text name="midas.siniestros.depuracion.oficina"/></column>
		<column id="nombre" type="ro" width="300" sort="str"><s:text name="midas.siniestros.depuracion.nombreConfig"/></column>
		<column id="fechaActivo" type="ro" width="120" sort="date_custom"  align="center"><s:text name="midas.siniestros.depuracion.fechaActivo"/></column>
		<column id="fechaInactivo" type="ro" width="120" sort="date_custom"  align="center"><s:text name="midas.siniestros.depuracion.fechaInactivo"/></column>
		<column id="estatus" type="ro" width="80" sort="str"><s:text name="midas.siniestros.depuracion.Estatus"/></column>
		<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="accionEditar" type="img" width="70" sort="na" align="center"/>
		<column id="accionHistorico" type="img" width="70" sort="na" align="center"/>
	</head>
	<s:iterator value="configuracionesMap" var="configuracion">
		<row>
			<cell><s:property value="['servicio']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['oficina']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['nombre']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['fechaActivo']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['fechaInactivo']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['estatus']" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript:mostrarConfiguracionReserva(<s:property value="['id']" />, 1);^_self</cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript:mostrarConfiguracionReserva(<s:property value="['id']" />, 0);^_self</cell>
			<s:if test="['estatus']==\"Activo\"||['estatus']==\"Inactivo\"">
				<cell>/MidasWeb/img/dhtmlxgrid/icons_books/books_cat.gif^Ver Historico De Depuraciones^javascript:verHistoricoDepuracionReserva(<s:property value="['id']" />);^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/blank.gif</cell>
			</s:else>
		</row>
	</s:iterator>
</rows>