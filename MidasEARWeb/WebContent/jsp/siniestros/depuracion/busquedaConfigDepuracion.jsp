<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@	taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/configuracionDepuracion.js'/>"></script>
<style type="text/css">

.fuente {
    font-family: arial;
    font-size: 11px;
    color: #666666;
    font-weight: bold;
}

.label {
    font-family: arial;
    font-size: 11px;
} 

</style>
<script type="text/javascript">
	var mostrarConfiguracionPath = '<s:url action="mostrarConfiguracion" namespace="/siniestros/depuracion/reserva"/>';
	var generarDepuracionPath = '<s:url action="generaDepuracion" namespace="/siniestros/depuracion/reserva"/>';
	var buscarDepuracionesPath = '<s:url action="buscarConfiguraciones" namespace="/siniestros/depuracion/reserva"/>';
	var mostrarHistoricoPath = '<s:url action="mostrarHistorico" namespace="/siniestros/depuracion/reserva/historico"/>';
</script>
<s:form id="busqConfigForm">
	<s:hidden name="vista" id="vista"></s:hidden>
	<div class="titulo">
		<s:text name="midas.siniestros.depuracion.datosConfiguracion"/>	
	</div>	
	<table id="filtrosM2" style="width: 95%;">
		<tr>
			<td>
				<s:hidden name="filtroDepuracion.tipoServicio" id="h_tipo_servicio"></s:hidden>
				<table border="0">
					<tr>
						<td class="label">
							<input id="ch_sparticular" type="checkbox" value="1" onclick="seleccionaCheckButton()">
								<s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioParticular"/></input>
						</td>
					</tr>
					<tr>
						<td class="label">
							<input id="ch_spublico" type="checkbox" value="2" onclick="seleccionaCheckButton()">
								<s:text name="midas.siniestros.cabina.reportecabina.busquedaincisopoliza.servicioPublico"/></input>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<s:select id="oficina"
					name="filtroDepuracion.oficinaId" 
					list="oficinaList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					key="midas.siniestros.depuracion.oficina"
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="key" listValue="value" 
					labelposition="top"
					cssClass="cajaTextoM2 w200 setNew"/>
			</td>
			<td>
				<s:select id="negocio"
					name="filtroDepuracion.negocioId" 
					list="negocioList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					key="midas.siniestros.depuracion.negocio"
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="key" listValue="value" 
					labelposition="top"
					cssClass="cajaTextoM2 w200 setNew"/>
			</td>
			<td>
				<s:select id="s_estatus"
					name="filtroDepuracion.estatus"
					key="midas.siniestros.depuracion.Estatus"
					list="estatusList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="key" listValue="value"
					cssClass="cajaTextoM2 w200 setNew"
					labelposition="top"/>
			</td>
		</tr>
		<tr>
			<td>
				<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
					name="filtroDepuracion.fechaIniActivo"
					key="midas.siniestros.depuracion.fechaActivo"
					labelposition="top" 
					id="fechaIniActivo"
			   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
			   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   	onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td>
				<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
					name="filtroDepuracion.fechaFinActivo"
					key="midas.siniestros.depuracion.a"
					labelposition="top" 
					id="fechaFinActivo"
			   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
			   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   	onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td>
				<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
					name="filtroDepuracion.fechaIniInactivo"
					key="midas.siniestros.depuracion.fechaInactivo"
					labelposition="top" 
					id="fechaIniInactivo"
			   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
			   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   	onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
			<td>
				<sj:datepicker buttonImage="/MidasWeb/img/b_calendario.gif"
					name="filtroDepuracion.fechaFinInactivo"
					key="midas.siniestros.depuracion.a"
					labelposition="top" 
					id="fechaFinInactivo"
			   		changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 setNew" 								   								  
			   		onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				   	onblur="esFechaValida(this);">
				</sj:datepicker>
			</td>
		</tr>
		<tr>
			<td>
				<s:select id="s_cobertura"
					name="cobertura" 
					list="coberturasList" headerKey="%{getText('midas.general.defaultHeaderKey')}" 
					key="midas.siniestros.depuracion.cobertura"
					headerValue="%{getText('midas.general.seleccione')}" 
					listKey="claveSubCalculo" listValue="nombreCobertura" 
					onchange="onChangeCobertura();"
					labelposition="top"
					cssClass="cajaTextoM2 w200 setNew"/>
					<s:hidden name="filtroDepuracion.claveSubCalculo" id="h_claveSubCalculo"></s:hidden>
				 	<s:hidden name="filtroDepuracion.coberturaId" id="h_cobertura"></s:hidden>
				  	<s:hidden name="filtroDepuracion.seccionId" id="h_seccion"></s:hidden>
			</td>
			<td colspan="3">
				<s:textfield cssClass="cajaTextoM2 setNew"
					key="midas.siniestros.depuracion.nombreConfig"
					name="filtroDepuracion.nombreConfiguracion"
					labelposition="top" 
					size="100"
					maxlength="100"
					onkeypress="return soloAlfanumericos(this ,event ,true);"			
					id="txt_nombre"/>
			</td>
		</tr>
	</table>
	<br></br>
	
	<div class="titulo">
		<s:text name="midas.siniestros.depuracion.datosReserva"/>	
	</div>
	<table id="filtrosM2" style="width: 95%;">
		<tr>
			<td>
				<s:textfield cssClass="cajaTextoM2 setNew"
					key="midas.siniestros.depuracion.nombreContratante"
					name="filtroDepuracion.contratante"
					labelposition="top" 
					size="100"
					maxlength="100"
					onkeypress="return soloAlfanumericos(this ,event ,true);"			
					id="txt_contratante"/>
			</td>
			<td>
				<s:textfield cssClass="cajaTextoM2 setNew"
					name="filtroDepuracion.numeroPoliza" 
					key="midas.siniestros.depuracion.numeroPoliza"
					labelposition="top" 
					size="12"
					onkeypress="return soloAlfanumericos(this ,event ,true);"
					maxlength="20"			
					id="txt_numeroPoliza"/>
			</td>
			<td>
				<s:textfield cssClass="cajaTextoM2 setNew"
					name="filtroDepuracion.numeroInciso" 
					key="midas.siniestros.depuracion.inciso"
					labelposition="top" 
					size="12"
					onkeypress="return soloNumeros(this, event, false)"		
					maxlength="8"				
					id="txt_inciso"/>
			</td> 
			<td>
				<s:textfield cssClass="cajaTextoM2 setNew"
					name="filtroDepuracion.numeroSiniestro" 
					key="midas.siniestros.depuracion.numeroSiniestro"
					labelposition="top" 
					size="12"
					onkeypress="return soloAlfanumericos(this ,event ,true);"
					maxlength="20"			
					id="txt_numeroSiniestro"/>
			</td>
		</tr>
	</table>		
	<br></br>
	
	<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_buscar">
					<a href="javascript: void(0);" onclick="javascript: initGridConfiguraciones(true);"> 
						<s:text name="midas.boton.buscar" />
					</a>
				</div>	
				<div class="btn_back w140" style="display: inline; float: right;" id="b_limpiar">
					<a href="javascript: void(0);" onclick="limpiarCampos();"> 
						<s:text name="midas.boton.limpiar" /> 
					</a>
				</div>	
			</td>							
		</tr>
	</table>
	
	<br></br>
	<div id="indicador"></div>
	<div id="depuracionesGrid" class="dataGridConfigurationClass" style="width: 95%;height:320px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="javascript: mostrarConfiguracionReserva(0, 0);"> 
						<s:text name="midas.boton.nuevo" />
					</a>
				</div>	
			</td>							
		</tr>
	</table>		
</s:form>
<script>
	initGridConfiguraciones(false);
</script>