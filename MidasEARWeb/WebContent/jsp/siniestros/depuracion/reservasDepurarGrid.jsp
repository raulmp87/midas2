<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="id" type="ro" width="1" sort="int" align="center" hidden="true" ><s:text name="midas.general.codigo"/></column>
		<column id="asignado" type="ch" width="30" sort="int" align="center" >#master_checkbox</column>
		<column id="numero" type="ro" width="70" sort="int"><s:text name="midas.siniestros.depuracion.numero"/></column>
		<column id="siniestro" type="ro" width="120" sort="str"><s:text name="midas.siniestros.depuracion.numeroSiniestro"/></column>
		<column id="poliza" type="ro" width="120" sort="str"><s:text name="midas.siniestros.depuracion.numeroPoliza"/></column>
		<column id="inciso" type="ro" width="50" sort="int"><s:text name="midas.siniestros.depuracion.inciso"/></column>
		<column id="folioPase" type="ro" width="120" sort="str"><s:text name="midas.siniestros.depuracion.folioPase"/></column>
		<column id="cobertura" type="ro" width="250" sort="str"><s:text name="midas.siniestros.depuracion.cobertura"/></column>
		<column id="contratante" type="ro" width="250" sort="str"><s:text name="midas.siniestros.depuracion.nombreContratante"/></column>
		<column id="terminoAjuste" type="ro" width="200" sort="str"><s:text name="midas.siniestros.depuracion.terminoAjuste"/></column>
		<column id="nombreConfig" type="ro" width="200" sort="str"><s:text name="midas.siniestros.depuracion.nombreConfig"/></column>
		<column id="reservaActual" format="$0,000.00" type="ron" width="120" sort="int"><s:text name="midas.siniestros.depuracion.reservaActual"/></column>
		<column id="reservaDepurar" format="$0,000.00" type="ron" width="120" sort="int"><s:text name="midas.siniestros.depuracion.reservaDepurar"/></column>
		<column id="porcentajeDepurar" type="ro" width="70" sort="int"><s:text name="midas.siniestros.depuracion.porcentajeDepurar"/></column>
	</head>
	<s:iterator value="reservasDepurar" var="reserva" status="index">
		<row id="${index.count}">
			<cell><s:property value="['id']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['estatus']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['numero']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['numeroSiniestro']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['poliza']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['numeroInciso']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['folioPase']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['nomCobertura']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['nombreAsegurado']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['terminoAjuste']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['nombreConfiguracion']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['montoReserva']" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="['montoReservaDepurar']" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="['porcentaje']" escapeHtml="false" escapeXml="true"/></cell>	
		</row>
	</s:iterator>
</rows>