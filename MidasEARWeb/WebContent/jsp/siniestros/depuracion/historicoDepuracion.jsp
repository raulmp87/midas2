<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/depuracion/historicoDepuracion.js'/>"></script>

<style type="text/css">
.dhxcont_main_content {
	background: white;
}
</style>



<script type="text/javascript">
	var mostrarListadoHistoricoPath = '<s:url action="mostrarListadoHistorico" namespace="/siniestros/depuracion/reserva/historico"/>';
	var mostrarReservasParaDepurarPath = '<s:url action="mostrarReservasParaDepurar" namespace="/siniestros/depuracion/reserva/historico"/>';
	var mostrarReservasDepuradasPath = '<s:url action="mostrarReservasDepuradas" namespace="/siniestros/depuracion/reserva/historico"/>';
	var mostrarListConfigDepuracionReservaPath = '<s:url action="mostrarBusquedaConfiguracion" namespace="/siniestros/depuracion/reserva"/>';
	var exportarResultadosPath = '<s:url action="exportarResultados" namespace="/siniestros/depuracion/reserva/historico"/>';
</script>

	<s:form id="historicoForm" >	
		<s:hidden id="h_idConfigDepuracionReserva" name="idConfigDepuracionReserva"></s:hidden>
		<div class="titulo" style="width: 95%;">
			<s:text name="midas.depuracion.reserva.historico.title"/>	
		</div>	
		<br/>
		<div id="indicadorHistoricoDepuracion"></div>
		<div id="historicoDepuracionGrid" class="dataGridConfigurationClass" style="width:95%;height:340px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br/>
		<br/>
		<br/>
</s:form>
	<br/>
	<table style="padding: 0px; width: 95%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td>
					<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:regresarConfiguracionesDepuracionReserva();"> 
							<s:text name="midas.boton.cerrar" /> 
							<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
						</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>		
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script>
	jQuery(document).ready(function(){
		inicializarHistorico();
	});
</script>

