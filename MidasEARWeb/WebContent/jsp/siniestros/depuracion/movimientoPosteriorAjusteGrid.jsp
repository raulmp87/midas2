<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column  type="ro"  width="200"  align="center" sort="str"> <s:text name="Oficina" />  </column>
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="No de Poliza" />  </column>
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="No de Siniestro" />  </column>
		<column  type="ro"  width="150"  align="center" sort="str"> <s:text name="No de Reporte" />  </column>
	    <column  type="ro"  width="300"  align="center" sort="str"> <s:text name="Cobertura" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="Termino de Siniestro" />  </column>
	    <column  type="ro"  width="200"  align="center" sort="str"> <s:text name="Concepto de Depuracion" />  </column>
	    <column  type="ro"  width="150"  align="center" sort="str"> <s:text name="Usuario" />  </column>
	    <column  type="ro"  width="120"  align="center" > <s:text name="Monto de Ajuste" />  </column>
	    <column  type="ro"  width="90"  align="center" > <s:text name="Fecha de Ajuste" />  </column>
	    <column id="consultar" type="img" width="40" sort="na" align="center"/>	
	    
	</head>

	<s:iterator value="lstMovimientos" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="terminoSiniestroDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreConcepto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuario" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="%{getText('struts.money.format',{montoAjuste})}" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaAjuste" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: mostrarDetalle(<s:property value="movimientoId" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>
	
</rows>