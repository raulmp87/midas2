<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">
 .labelBlack{
 	color:black !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 9px;
	font-family: verdana,arial;
	}
</style>

<script type="text/javascript">
	var guardarConfiguracionAusenciaPath= '<s:url action="guardarConfiguracionAusenciaIndemnizacion" namespace="/siniestros/indemnizacion/perdidatotal"/>';
</script>

<s:form id="configuracionAusenciaIndemnizacionForm" >
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.indemnizacion.configausencia.titulo"/>	
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>	
				<tr>
					<td style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.configausencia.instruccion" /> </td>
				</tr>
				<tr>
					<td colspan="3">
						<s:select id="rolesAutorizacionList" 
										name="nombreRolAutorizador"
										headerKey="NO_APLICA" headerValue="%{getText('midas.siniestros.indemnizacion.configausencia.noaplica')}"
								  		list="listaRolesSinAutorizacionFinal" 
								  		cssClass="txtfield cajaTextoM2 w160 deshabilitar"   />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		
</s:form>

<br/>
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>
			<td>
				<div id="btn_guardar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:guardarConfiguracionAutorizacionIndemnizacion();"> 
						<s:text name="midas.boton.guardar" /> 
						<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/common/btn_guardar.jpg'/>
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>
	
<script type="text/javascript">
		
</script>

<script src="<s:url value='/js/siniestros/indemnizacion/configuracionAusenciaIndemnizacion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>