<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>

<script type="text/javascript">
	var cancelarIndemnizacionPath = '<s:url action="cancelarIndemnizacion" namespace="/siniestros/indemnizacion/perdidatotal"/>';
</script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: left;	
	font-size: 11px;
	font-family: arial;
	}
</style>

<s:form id="cancelarIndemnizacionForm" action="cancelarIndemnizacion" namespace="/siniestros/indemnizacion/perdidatotal" name="cancelarIndemnizacionForm">
	
	<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
	<s:hidden id="oficinaReporte" name="indemnizacion.siniestro.reporteCabina.claveOficina"/>
	<s:hidden id="consecutivoReporte" name="indemnizacion.siniestro.reporteCabina.consecutivoReporte"/>
	<s:hidden id="anioReporte" name="indemnizacion.siniestro.reporteCabina.anioReporte"/>
	<s:hidden id="oficinaSiniestro" name="indemnizacion.siniestro.claveOficina"/>
	<s:hidden id="consecutivoSiniestro" name="indemnizacion.siniestro.consecutivoReporte"/>
	<s:hidden id="anioSiniestro" name="indemnizacion.siniestro.anioReporte"/>
	<s:hidden id="estatusAutPerdidaTotal" name="indemnizacion.estatusPerdidaTotal"/>
	<s:hidden id="idEstimacion" name="indemnizacion.ordenCompra.idTercero"/>
	<s:hidden id="estatusPTIndemnizacion" name="indemnizacion.estatusPerdidaTotal"/>
	<s:hidden id="estatusAutIndemnizacion" name="indemnizacion.estatusIndemnizacion"/>
	
	<s:hidden id="idOficina" name="filtroPT.idOficina"/>
	<s:hidden id="filtroTipoSiniestro" name="filtroPT.tipoSiniestro"/>
	<s:hidden id="filtroSiniestroOficina" name="filtroPT.siniestroClaveOficina"/>
	<s:hidden id="filtroSiniestroConsecutivo" name="filtroPT.siniestroConsecutivoReporte"/>
	<s:hidden id="filtroSiniestroAnio" name="filtroPT.siniestroAnioReporte"/>
	<s:hidden id="filtroReporteOficina" name="filtroPT.reporteClaveOficina"/>
	<s:hidden id="filtroReporteConsecutivo" name="filtroPT.reporteConsecutivoReporte"/>
	<s:hidden id="filtroReporteAnio" name="filtroPT.reporteAnioReporte"/>
	<s:hidden id="filtroNumeroPoliza" name="filtroPT.numeroPoliza"/>
	<s:hidden id="filtroNumeroSerie" name="filtroPT.numeroSerie"/>
	<s:hidden id="filtroNumeroValuacion" name="filtroPT.numeroValuacion"/>
	<s:hidden id="filtroFechaSiniestro" name="filtroPT.fechaSiniestro"/>
	<s:hidden id="filtroEtapa" name="filtroPT.etapa"/>
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
								<s:text name="midas.siniestros.indemnizacion.cancelacion.titulo" />
	                    </td>
	                  </tr>
	                  
	                  <tr>
						<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.cancelacion.numreporte" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.siniestro.reporteCabina.numeroReporte"  cssClass="cajaTexto w150 alphaextra" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
						
						<th style="padding-left: 1%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
					  
					  <tr>
						<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.cancelacion.numsiniestro" /> </th>
						<td style="width: 15%;"><s:textfield name="numeroSiniestro"  cssClass="cajaTexto w150 alphaextra" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.cancelacion.numserie" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.siniestro.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
					  
					  <tr>
						<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.cancelacion.numpoliza" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.siniestro.reporteCabina.poliza.folioPoliza"  cssClass="cajaTexto w150 alphaextra" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.cancelacion.numvaluacion" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.ordenCompra.numeroValuacion" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
					  
					  <tr>
					  	
						<th style="width: 12%;"></th>
						<td style="width: 15%;"></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
	                  
	                  <tr>
						<td colspan="6" >
							<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
								<tbody>
									<tr>
										<td><label class="labelBlack" ><s:text name="midas.siniestros.indemnizacion.cancelacion.motivo" /></label></td>
									</tr>
									<tr>
										<td>
											<div id="contenedorTextArea">
												<s:textarea name="indemnizacion.motivoAutPerdidaTotal" id="motivoIndemnizacion" disabled="true"
												cssStyle="font-size: 10pt;"	cols="80" rows="3" cssClass="textarea activar"
												onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onchange="truncarTexto(this,3000);"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					  </tr>
	                  
	                  <tr>
	                  	<td colspan="6">
							
							<div id="btn_cerrar" class="btn_back w80 habilitar" style="display: inline; float: right;position: relative;">
								<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:cerrarVentanaCancelarIndemnizacion();}"> 
								<s:text name="midas.boton.cerrar" /> </a>
							</div>
							
							<div  id="cancelarIndemnizacionBtn" class="btn_back w110 habilitar" style="display:none; float:right; position:relative;">
		                        <a href="javascript: void(0);" onclick="if(confirm('Haga clic en Aceptar para Cancelar la indemnizaci\u00F3n')){javascript:cancelarIndemnizacion();}"> 
		                        <s:text name="midas.siniestros.indemnizacion.cancelacion.cancelar.btn" /> </a>
							</div>

						</td>
	                  </tr>
	            </tbody>
	</table>
	
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
	 	configurarCancelarIndemnizacion();
 	}
 );
</script>

<script type="text/javascript" src="<s:url value='/js/siniestros/indemnizacion/cancelarIndemnizacion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>