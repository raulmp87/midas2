<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:form id="determinacionPerdidaTotalForm" >
	<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
	<s:hidden id="token" name="token"/>
	<s:hidden id="userName" name="userName"/>
	<s:hidden id="tipoSiniestroDesc" name="tipoSiniestroDesc"/>
	<s:hidden id="tipoSiniestro" name="tipoSiniestro"/>
	<s:hidden id="tipoBajaPlacas" name="informacionSiniestro.tipoBajaPlacas"/>
	<s:hidden id="fechaDeterminacion" name="fechaDeterminacion"/>
	<s:hidden id="idSiniestro" name="informacionSiniestro.idSiniestro"/>
	<s:hidden id="idReporteCabina" name="informacionSiniestro.idReporteCabina"/>
	<s:hidden id="idValuador" name="informacionSiniestro.idValuador"/>
	<s:hidden id="esFiniquitoAsegurado" name="esFiniquitoAsegurado"/>
	<s:hidden id="esCoberturaAsegurado" name="esCoberturaAsegurado"/>
	<s:hidden id="guardadoCorrecto" name="guardadoCorrecto"/>
	<s:hidden id="esConsulta" name="esConsulta"/>
	<s:hidden id="esRecepcionPagoDanios" name="esRecepcionPagoDanios"/>
	<s:hidden id="numPoliza" name="informacionSiniestro.numPoliza"/>
	<s:hidden id="inciso" name="informacionSiniestro.inciso"/>
	<s:hidden id="numReporte" name="informacionSiniestro.numReporte"/>
	<s:hidden id="agente" name="informacionSiniestro.agente"/>
	<s:hidden id="numSiniestro" name="informacionSiniestro.numSiniestro"/>
	<s:hidden id="fechaSiniestro" name="informacionSiniestro.fechaSiniestro"/>
	<s:hidden id="tipoSiniestro" name="informacionSiniestro.tipoSiniestro"/>
	<s:hidden id="lugarSiniestro" name="informacionSiniestro.lugarSiniestro"/>
	<s:hidden id="asegurado" name="informacionSiniestro.asegurado"/>
	<s:hidden id="primaTotal" name="informacionSiniestro.primaTotal"/>
	<s:hidden id="beneficiario" name="informacionSiniestro.beneficiario"/>
	<s:hidden id="terminoAjuste" name="informacionSiniestro.terminoAjuste"/>
	<s:hidden id="terminoSiniestro" name="informacionSiniestro.terminoSiniestro"/>
	<s:hidden id="marcaTipo" name="informacionSiniestro.marcaTipo"/>
	
	
	<s:hidden id="color" name="informacionSiniestro.color"/>
	<s:hidden id="numSerie" name="informacionSiniestro.numSerie"/>
	<s:hidden id="motor" name="informacionSiniestro.motor"/>
	<s:hidden id="modelo" name="informacionSiniestro.modelo"/>
	<s:hidden id="puertas" name="informacionSiniestro.puertas"/>
	<s:hidden id="tipo" name="informacionSiniestro.tipo"/>
	<s:hidden id="marca" name="informacionSiniestro.marca"/>
	<s:hidden id="placa" name="informacionSiniestro.placa"/>
	





	<s:hidden id="esTercero" name="informacionSiniestro.esTercero"/>

	<s:hidden id="idOrdenCompra" name="idOrdenCompra"/>
	<s:hidden id="etapa" name="indemnizacion.etapa"/>
	<s:hidden id="estatusPT" name="indemnizacion.estatusPerdidaTotal"/>

<!-- 	style="display:none" -->
	<div  style="display:none">
	
		<div style="width: 98%; text-align: right;">
			<label style="font-weight: bold; margin-right: 5px"><s:text name="midas.siniestros.indemnizacion.determinacion.fecha" />:</label>
			<s:text name="fechaDeterminacion" />
		</div>


	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.indemnizacion.determinacion.infosiniestro"/>	
	</div>
	
	<div id="contenedorFiltros" style="width: 96%;">
		<table id="agregar" border="0">
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numpoliza" /></th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.numPoliza"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.inciso" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.inciso"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numreporte" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.numReporte"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.agente" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.agente"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numsiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.numSiniestro"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.fechasiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.fechaSiniestro"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.tiposiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.tipoSiniestro"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.lugarsiniestro" /> </th>
				<td style="width: 40%;">
					<s:property value="informacionSiniestro.lugarSiniestro"/>
				</td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.asegurado" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.asegurado"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.terminoajuste" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.terminoAjuste"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.beneficiario" /></th>
				<s:if test='%{indemnizacion.estatusPerdidaTotal == "A" }'>
					<td style="width: 40%;"><s:property value="informacionSiniestro.beneficiario"/></td>	
					<s:hidden id="bnfEdited" name="informacionSiniestro.beneficiario"/>
				</s:if>
				<s:else>
					<td style="width: 40%;"><s:textfield id ="bnfEdited" name="informacionSiniestro.beneficiario" cssClass="cajaTexto w200 alphaextra"  /></td>
				</s:else>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.terminosiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.terminoSiniestro"/></td>
			</tr>
		</table>
	</div>
	
	<div style="width: 97%;">
		<div class="floatLeft" style="width:63%; margin-right: 1%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.descripcionunidad"/>	
			</div>
			<table id="agregar" border="0" height="120px">
				<s:if test='%{informacionSiniestro.esTercero == "N" }'>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.marcatipo" /></th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.marcaTipo"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.color" /></th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.color"/>  </td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numserie" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.numSerie"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.motor" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.motor" /></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.modelo" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.modelo"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.puertas" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.puertas" /></td>
				</tr>
				</s:if>
				<s:else>
					<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.marca" /></th>
				   <td style="width: 40%;"><s:textfield name="informacionSiniestro.marca"  cssClass="cajaTextoM2"/></td>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.tipo"/></th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.tipo"  cssClass="cajaTextoM2"/></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numserie"  /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.numSerie"  cssClass="cajaTextoM2"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.motor" /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.motor"  cssClass="cajaTextoM2" /></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.modelo" /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.modelo"  cssClass="cajaTextoM2"/></td>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.placas"/></th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.placa"  cssClass="cajaTextoM2"/></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.color"  /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.color"  cssClass="cajaTextoM2"/></td>
					<th style="width: 10%;"></th>
					<td style="width: 40%;"></td>
				</tr>
				</s:else>
				</table>
		</div>
			<div class="floatLeft" style="width:48%; margin-bottom:2%">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.determinacionmontoindem"/>
			</div>
			<table id="agregar" border="0">
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.sumaguias" /></th>
					<td style="width: 50%;"><s:textfield id="sumaGuias" name="indemnizacion.sumaGuias"  cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"
						 onblur="formato2Decimales(this);" readonly="true" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numeroguiasaplicadas" /></th>
					<td style="width: 50%;"><s:textfield id="numeroGuias" name="indemnizacion.numeroGuias" readonly="true" cssClass="cajaTexto w150 alphaextra esConsulta" onkeypress="return soloNumeros(this, event, false)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.promedioguias" /></th>
					<td style="width: 50%;"><s:textfield id="promedioGuias" name="indemnizacion.promedioGuias" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" readonly="true" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.valorunidad" /></th>
					<td style="width: 50%;"><s:textfield id="valorUnidad" name="indemnizacion.valorUnidad" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="onChangeValorUnidad();calcularPorcDaniosVsValorUnidad();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.deduciblemenos" /></th>
					<td style="width: 50%;"><s:textfield id="deducible" name="informacionSiniestro.montoTotalDeducible" cssClass="cajaTexto w150 formatCurrency" readonly="true"/></td>
				</tr>
				<s:iterator value="informacionSiniestro.deducibles" status="rowstatus">
					<tr>
						<td style="text-color:gray;text-indent: 5px;" >
							<s:if test="%{ingresoGenerado}">
								<img src="<s:url value="/img/b_comentariog.gif"/>" alt="Existe un ingreso pendiente por lo que no se puede modificar el deducible" title="Existe un ingreso pendiente por lo que no se puede modificar el deducible" />
							</s:if>
							<s:property  value="descripcion" escapeHtml="false" escapeXml="true" />							
						</td>
						<td >
							<s:hidden name="informacionSiniestro.deducibles[%{#rowstatus.index}].idRecuperacion"/>
							<s:hidden name="informacionSiniestro.deducibles[%{#rowstatus.index}].ingresoGenerado"/>
							<s:property value="%{#informacionSiniestro.deducibles[%{#rowstatus.index}].ingresoGenerado}"/>
							<s:if test="%{ingresoGenerado}">								
								<s:textfield name="informacionSiniestro.deducibles[%{#rowstatus.index}].monto" id="montoDeducible"
								cssStyle="background-color:#FAFAF0;" readonly="true" 
								cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"/>								
							</s:if>
							<s:else>								
								<s:textfield name="informacionSiniestro.deducibles[%{#rowstatus.index}].monto" id="montoDeducible" onblur="onChangeDeducible(this.id);" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"/>
							</s:else>
						</td>
					</tr>				
				</s:iterator>				
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.subtotal1" /></th>
					<td style="width: 50%;"><s:textfield id="subTotal" name="indemnizacion.subTotal" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"  readonly="true"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.descuento" /></th>
					<td style="width: 50%;"><s:textfield id="descuento" name="indemnizacion.descuento" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"
						onblur="validarMaximoMontoDescuento();calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.notadescuento" /></th>
					<td style="width: 50%;"><s:textfield id="notaDescuento" name="indemnizacion.notaDescuento" cssClass="cajaTexto w150 alphaextra esConsulta" maxlength="100"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><div style="display:none;"><s:text name="midas.siniestros.indemnizacion.determinacion.valroequipoespecial" /></div></th>
					<td style="width: 50%;"><div style="display:none;"><s:textfield id="valorEqAdaptaciones" name="indemnizacion.valorEqAdaptaciones" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></div></td>
				</tr>
				<tr>
					<th style="width: 50%;"><div style="display:none;"><s:text name="midas.siniestros.indemnizacion.determinacion.deduciblemenos" /></div></th>
					<td style="width: 50%;"><div style="display:none;"><s:textfield id="deducibleEqAdaptaciones" name="indemnizacion.deducibleEqAdaptaciones" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"  
						onblur="calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></div></td>
				</tr>
				
				<s:if test="tipoSiniestro == 'RB'  || esFiniquitoAsegurado==true  ">
				
					<tr>
						<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.primaspendientespoliza" /></th>
						<td style="width: 50%;"><s:textfield id="primasPendientes" name="indemnizacion.primasPendientes" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
							 onblur="calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
					</tr>
				</s:if>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.totalindemnizar" /></th>
					<td style="width: 50%;"><s:textfield id="totalIndemnizar" name="indemnizacion.totalIndemnizar" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"  readonly="true"></s:textfield></td>
				</tr>
				<s:if test="esRecepcionPagoDanios == true">
					<tr>
						<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.pagodanios" /></th>
						<td style="width: 50%;"><s:checkbox id="esPagoDanios" name="indemnizacion.esPagoDanios" cssClass="esConsulta" onclick="habilitarCapturaTotalPD();"></s:checkbox></td>
					</tr>
					<tr>
						<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.totaldanios" /></th>
						<td style="width: 50%;"><s:textfield id="totalPagoDanios" name="indemnizacion.totalPagoDanios" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" readonly="true"
						 onblur="validarTotalPagoDanios();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
					</tr>
				</s:if>
			</table>
		</div>
		
		
		
		
	</div>
	
	</div>

	
</s:form>

<iframe  id="myIframe"  src="http://cap6.afirme.com.mx/perdidaTotal" style="width : 100%; height:95%; border: none">

</iframe>
<!--  ramo A es igual a autos -->
<script type="text/javascript">



var iframe = document.getElementById('myIframe');


iframe.src = iframe.src + '?idIndemnizacion='+ document.getElementById('idIndemnizacion').value;
iframe.src = iframe.src + '&idSiniestro='+ document.getElementById('idSiniestro').value;
iframe.src = iframe.src + '&idReporteCabina='+ document.getElementById('idReporteCabina').value;
iframe.src = iframe.src + '&idValuador=0';
iframe.src = iframe.src + '&idOrdenCompra='+ document.getElementById('idOrdenCompra').value;
iframe.src = iframe.src + '&numPoliza='+ document.getElementById('numPoliza').value;
iframe.src = iframe.src + '&numReporte='+ document.getElementById('numReporte').value;
iframe.src = iframe.src + '&asegurado='+ document.getElementById('asegurado').value;
iframe.src = iframe.src + '&primaTotal='+ document.getElementById('primaTotal').value;
iframe.src = iframe.src + '&fechaSiniestro='+ document.getElementById('fechaSiniestro').value;
iframe.src = iframe.src + '&lugarSiniestro='+ document.getElementById('lugarSiniestro').value;
iframe.src = iframe.src + '&totalIndemnizar='+ document.getElementById('totalIndemnizar').value;


iframe.src = iframe.src + '&tipoSiniestroDesc='+ document.getElementById('tipoSiniestroDesc').value;
iframe.src = iframe.src + '&tipoSiniestro='+ document.getElementById('tipoSiniestro').value;


iframe.src = iframe.src + '&color='+ document.getElementById('color').value;
iframe.src = iframe.src + '&numSerie='+ document.getElementById('numSerie').value;
iframe.src = iframe.src + '&motor='+ document.getElementById('motor').value;
iframe.src = iframe.src + '&modelo='+ document.getElementById('modelo').value;
iframe.src = iframe.src + '&puertas='+ document.getElementById('puertas').value;
iframe.src = iframe.src + '&tipo='+ document.getElementById('tipo').value;
iframe.src = iframe.src + '&marca='+ document.getElementById('marca').value;
iframe.src = iframe.src + '&placa='+ document.getElementById('placa').value;
iframe.src = iframe.src + '&userName='+ document.getElementById('userName').value;
iframe.src = iframe.src + '&token='+ document.getElementById('token').value;
iframe.src = iframe.src + '&numSiniestro='+ document.getElementById('numSiniestro').value;




</script>








<br>
<!-- <div id="b_agregar" style="width:70px;" > -->
<!-- 	  <a href="javascript: void(0);" onclick="javascript: alert();">Aceptar</a> -->
<!-- </div> -->