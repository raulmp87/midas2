<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>

<sj:head/>


<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
	
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<script type="text/javascript">
var imprimirInfoContratoPath = '<s:url action="imprimirInfoContrato" namespace="/siniestros/indemnizacion/cartas"/>';
var imprimirInfoContratoLiquidacionPath = '<s:url action="imprimirInfoContrato" namespace="/siniestros/liquidacion/liquidacionIndemnizacion"/>';
</script>

<s:form id="infoContratoForm" action="guardarInfoContrato" namespace="/siniestros/indemnizacion/cartas" name="infoContratoForm">
	<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
	<s:hidden id="idLiquidacion" name="liquidacionSiniestro.id" />
	<s:hidden id="idInfoContrato" name="infoContrato.Id" />
	<s:hidden id="esConsulta" name="esConsulta"/>
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
							<div id="tituloInfoContrato" style="display:inline;">
								<s:text name="midas.siniestros.indemnizacion.contrato.titulo" />
							</div> 
	                    </td>
	                  </tr>
					  
						<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.apoderadoafirme" /> </th>
							<td style="width: 15%;"><s:textfield name="infoContrato.apoderadoAfirme"  readonly="true" cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
							
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.segundoapoderadoafirme" /> </th>
							<td style="width: 15%;"><s:textfield name="infoContrato.segundoApoderadoAfirme" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
							
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.apoderadoafectado" /> </th>
							<td style="width: 15%;"><s:textfield name="infoContrato.apoderadoAfectado" cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
						</tr>
					
						<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.calle" /></th>
							<td style="width: 15%;"><s:textfield name="infoContrato.calleVendedor"  cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
						
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.numero" /></th>
							<td style="width: 15%;"><s:textfield name="infoContrato.numeroVendedor"  cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
					
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.colonia" /></th>
							<td style="width: 15%;"><s:textfield name="infoContrato.coloniaVendedor"  cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
						</tr>
					
						<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.cp" /></th>
							<td style="width: 15%;"><s:textfield name="infoContrato.cpVendedor"  cssClass="cajaTexto w150 jQnumeric jQrequired jQrestrict" ></s:textfield></td>
						
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.ciudad" /></th>
							<td style="width: 15%;"><s:textfield name="infoContrato.ciudadVendedor"  cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
							
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.estado" /></th>
							<td style="width: 15%;"><s:textfield name="infoContrato.estadoVendedor"  cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
					  	</tr>
					  	
					  	<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.contrato.documentacion" /></th>
							<td style="width: 15%;" colspan="5"><s:textfield name="infoContrato.documentacion"  cssClass="cajaTexto w150 alphaextra jQrequired" ></s:textfield></td>
					  	</tr>
					  
	            </tbody>
	</table>
	<br/>
	<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="javascript:cerrarVentanaInfoContrato();"> 
		<s:text name="midas.boton.cerrar" /> </a>
	</div>
	<div id="btn_imprimir" class="btn_back w80 validarTipoFiniquitoNotNull" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="imprimirInfoContrato();"> 
		<s:text name="midas.boton.imprimir" /> </a>
	</div>
	<div id="btn_guardar" class="btn_back w80 validarTipoFiniquitoNotNull" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="guardarInfoContrato();"> 
		<s:text name="midas.boton.guardar" /> </a>
	</div>
	
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
 	}
 );
</script>

<script type="text/javascript" src="<s:url value='/js/siniestros/indemnizacion/infoContrato.js'/>"></script>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>