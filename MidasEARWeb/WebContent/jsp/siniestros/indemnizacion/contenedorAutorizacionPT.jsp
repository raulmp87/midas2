<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
</style>

<s:form id="autorizacionForm" action="autorizar" namespace="/siniestros/indemnizacion/perdidatotal" name="autorizacionForm">
	<s:hidden id="idOrdenCompra" name="idOrdenCompra"/>
	<s:hidden id="tipoAutorizacion" name="tipoAutorizacion"/>
	<s:hidden id="idIndemnizacion" name="indemnizacion.id"/>
	<s:hidden id="oficinaReporte" name="indemnizacion.siniestro.reporteCabina.claveOficina"/>
	<s:hidden id="consecutivoReporte" name="indemnizacion.siniestro.reporteCabina.consecutivoReporte"/>
	<s:hidden id="anioReporte" name="indemnizacion.siniestro.reporteCabina.anioReporte"/>
	<s:hidden id="oficinaSiniestro" name="indemnizacion.siniestro.claveOficina"/>
	<s:hidden id="consecutivoSiniestro" name="indemnizacion.siniestro.consecutivoReporte"/>
	<s:hidden id="anioSiniestro" name="indemnizacion.siniestro.anioReporte"/>
	<s:hidden id="estatusAutPerdidaTotal" name="indemnizacion.estatusPerdidaTotal"/>
	<s:hidden id="estatusAutIndemnizacion" name="indemnizacion.estatusIndemnizacion"/>
	<s:hidden id="metodoInvocar" name="metodoInvocar"/>
	<s:hidden id="pantallaOrigen" name="pantallaOrigen"/>
	<s:hidden id="esPagoDanios" name="indemnizacion.esPagoDanios"/>
	
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
							<div id="tituloPerdidaTotal" style="display:none;">
								<s:text name="midas.siniestros.indemnizacion.autorizacion.tituloperdidatotal" />
							</div> 
							<div id="tituloIndemnizacion" style="display:none;">
								<s:text name="midas.siniestros.indemnizacion.autorizacion.tituloindemnizacion" />
							</div>
	                    </td>
	                  </tr>
	                 <s:if test=" pantallaOrigen!='DT' "> 
	                  <tr>
						<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.autorizacion.numreporte" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.siniestro.reporteCabina.numeroReporte"  cssClass="cajaTexto w150 alphaextra" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
						
						<th style="padding-left: 1%;"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.fecha" /> </th>
						<td style="width: 15%;"><s:textfield name="fechaAutorizacion" id="fechaAutorizacion" cssStyle="display:none;" cssClass="cajaTexto w150 alphaextra indemnizacion" readonly="true" ></s:textfield>
												<s:textfield name="fechaAutorizacion" id="fechaAutorizacion" cssStyle="display:none;" cssClass="cajaTexto w150 alphaextra perdidatotal" readonly="true" ></s:textfield></td>
					  </tr>
					  
					  <tr>
						<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.autorizacion.numsiniestro" /> </th>
						<td style="width: 15%;"><s:textfield name="numeroSiniestro"  cssClass="cajaTexto w150 alphaextra" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.autorizacion.numserie" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.siniestro.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
					  
					  <tr>
						<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.autorizacion.numpoliza" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.siniestro.reporteCabina.poliza.folioPoliza"  cssClass="cajaTexto w150 alphaextra" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.autorizacion.numvaluacion" /> </th>
						<td style="width: 15%;"><s:textfield name="indemnizacion.ordenCompra.numeroValuacion" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
					  
					  <tr>
					  	
						<th style="width: 12%;"><div style="display:none;" class="indemnizacion">
							<s:text name="midas.siniestros.indemnizacion.autorizacion.totalindemnizar" /></div>
							<div style="display:none;" class="perdidatotal">
							<s:text name="midas.siniestros.indemnizacion.autorizacion.danios" /></div></th>
						<td style="width: 15%;">
						<s:if test="indemnizacion.esPagoDanios == true">
							<s:textfield id="totalIndemnizacion" name="indemnizacion.totalPagoDanios"  cssStyle="display:none;" cssClass="cajaTexto w150 alphaextra indemnizacion formatCurrency" readonly="true"></s:textfield>
						</s:if>
						<s:else>
							<s:textfield id="totalIndemnizacion" name="indemnizacion.totalIndemnizar"  cssStyle="display:none;" cssClass="cajaTexto w150 alphaextra indemnizacion formatCurrency" readonly="true"></s:textfield>
						</s:else>
							<s:textfield name=""  cssStyle="display:none;" cssClass="cajaTexto w150 alphaextra perdidatotal" readonly="true"></s:textfield></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
						
						<th style="padding-left: 5%;"></th>
						<td style="width: 15%;"></td>
					  </tr>
	                </s:if> 
	                  <tr>
						<td colspan="6" >
							<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
								<tbody>
									<tr>
										<td><label class="labelBlack" style="text-align:left;"><s:text name="midas.siniestros.indemnizacion.autorizacion.motivo" /></label></td>
									</tr>
									<tr>
										<td>
											<div id="contenedorTextArea">
												<s:textarea name="indemnizacion.motivoAutPerdidaTotal" id="motivoPerdidaTotal_a" disabled="true"
												cssClass="textarea perdidatotal activar" cssStyle="font-size: 10pt;display:none;"	cols="80" rows="3"
												onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onchange="truncarTexto(this,3000);"/>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div id="contenedorTextArea">
												<s:textarea name="indemnizacion.motivoAutIndemnizacion" id="motivoIndemnizacion_a" disabled="true"
												cssClass="textarea indemnizacion activar" cssStyle="font-size: 10pt;display:none;"	cols="80" rows="3"
												onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onchange="truncarTexto(this,3000);"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					  </tr>
	                  
	                  <tr>
	                  	<td colspan="6">
	                  		
	                  		<div  id="rechazarBtn" class="btn_back w80 habilitar" style="display: none; float: right;position: relative;">
		                        <a href="javascript: void(0);" onclick="if(confirm('Haga clic en Aceptar para Rechazar')){javascript:rechazar();}"> 
		                        <s:text name="midas.boton.rechazar" /> </a>
							</div>
							
							<div id="autorizarBtn" class="btn_back w80 habilitar" style="display: none; float: right;position: relative;">
		                        <a href="javascript: void(0);" onclick="if(confirm('Haga clic en Aceptar para Autorizar')){javascript:autorizar();}"> 
		                        <s:text name="midas.boton.autorizar" /> </a>
							</div>
							
							<div id="avisoimprimirpd" style="display: none; float: right;position: relative; color: red; font-size: 7.5pt;">
								<s:text name="midas.siniestros.indemnizacion.autorizacion.msgdanios"/>
							</div>
							
							<div id="avisoimprimirind" style="display: none; float: right;position: relative; color: red; font-size: 7.5pt;">
								<s:text name="midas.siniestros.indemnizacion.autorizacion.msgind"/>
							</div>
							
						</td>
	                  </tr>
	            </tbody>
	</table>
	<br/>
	
	<s:if test=" pantallaOrigen!='DT' ">
		<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
				<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:cerrarVentanaAutorizacion();}"> 
				<s:text name="midas.boton.cerrar" /> </a>
		</div>
	</s:if>
	<s:if test=" pantallaOrigen!='DT' "> 
		<div id="btn_hgs" class="btn_back w80 perdidatotal" style="display: none; float: right;position: relative;">
			<a href="javascript: void(0);" onclick="mostrarPaginaHGS();"> 
			<s:text name="midas.siniestros.indemnizacion.autorizacion.hgs" /> </a>
		</div>
	</s:if>
	
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
	 	configurarAutorizacion();
	 	initCurrencyFormatOnTxtInput();
 	}
 );
</script>

<script type="text/javascript" src="<s:url value='/js/siniestros/indemnizacion/autorizacionPT.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>