<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
</style>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>

<script type="text/javascript"	src="<s:url value='/js/siniestros/indemnizacion/bajaPlacas.js'/>"></script>

<s:form id="bajaPlacasForm" namespace="/siniestros/indemnizacion/cartas" name="bajaPlacasForm">
	<s:hidden id="idSiniestro" name="cartaSiniestro.indemnizacion.siniestro.id"/>
	<s:hidden id="idIndemnizacion" name="cartaSiniestro.indemnizacion.id"/>
	<s:hidden id="tipoCarta" name="cartaSiniestro.tipo" />
	<s:hidden id="entidadDescSol" name="entidad.descripcionSol"/>
	<s:hidden id="entidadDescSolComp" name="entidad.descSolComplementaria" />
	<s:hidden id="informacionCompleta" name="informacionCompleta" />
	
	
	<div id="contenedorFiltros" style="width: 96%;">
		<table id="agregar" border="0">
			
			<tr><td>
			<div class="titulo" style="width: 98%;">
				<s:text name="midas.siniestros.indemnizacion.cartasiniestro.bajaplacas"/>	
			</div>
			</td></tr>
			
			<tr><td>
			<div id="personaDirige" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="cartaSiniestro.personaAQuienSeDirige" id="personaDirige_t" cssClass="cajaTextoM2 w370" maxlength="100"
		            		label="%{getText('midas.siniestros.indemnizacion.cartasiniestro.personadirige')}" labelposition="left" disabled="false" />
			</div>
			</td></tr>
			
			<tr><td>
			<div id="descripcionSolicitud" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="cartaSiniestro.descripcionSol" id="descripcionSol_t" cssClass="cajaTextoM2 w370" maxlength="100"
		            		label="%{getText('midas.siniestros.indemnizacion.cartasiniestro.descsol')}" labelposition="left" disabled="false" />
			</div>
			</td></tr>
			
			<tr><td>
			<div id="descripcionSolicitudComp" style="margin-left: 10%;margin-top: 2%;">
				<s:textfield name="cartaSiniestro.descSolComplementaria" id="descripcionSolComp_t" cssClass="cajaTextoM2 w320" maxlength="100"
		            		label="%{getText('midas.siniestros.indemnizacion.cartasiniestro.descsolcomp')}" labelposition="left" disabled="false" />
			</div>
			</td></tr>
			
			<tr><td>
			<div id="estadoPlacas" style="margin-left: 10%;margin-top: 2%;">
		        <s:select list="listaEstados" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="cartaSiniestro.estadoTenencia" id="estadoPlacas_s" cssClass="cajaTextoM2 w400 esConsulta" 
					label="%{getText('midas.siniestros.indemnizacion.cartasiniestro.edoplacas')}" labelposition="left" disabled="false" />
			</div>
			</td></tr>
		
	</table>
	</div>
	
</s:form>

	<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
					<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:parent.cerrarVentanaModal('vm_bajaPlaca');}"> 
					<s:text name="midas.boton.cerrar" /> </a>
				</div>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_regresar">
					<a href="javascript: void(0);" onclick="imprimirCartaBajaPlacas();"> 
					<s:text name="midas.boton.imprimir" /> </a>
				</div>			
				<div class="btn_back w140" style="display: inline; float: right;" id="b_guardar">
					<a href="javascript: void(0);" onclick="guardarCartaBajaPlacas();"> 
					<s:text name="midas.boton.guardar" /> </a>
				</div>		
			</td>							
		</tr>
	</table>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>