<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
	.floatLeft {
		float: left;
		position: relative;
	}
	
	.divFormulario {
	height: 35px;
	position: relative;
}
	.cajaTextoM2{
		border: 1px solid #a0e0a0;
		text-transform: uppercase;
		font-size: 9px;
		padding-left: 3px;
	}
</style>

<script type="text/javascript">
var guardarDeterminacionPath = '<s:url action="guardarDeterminacionPT" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var imprimirDeterminacionPath = '<s:url action="imprimirDeterminacionPT" namespace="/siniestros/indemnizacion/cartas"/>';
var mostrarBusquedaPath = '<s:url action="mostrarCatalogo" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var mostrarAutorizarPath = '<s:url action="mostrarAutorizar" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var mostrarGridAutorizarPath = '<s:url action="listarAutorizaciones" namespace="/siniestros/indemnizacion/perdidatotal"/>';
</script>

<s:form id="determinacionPerdidaTotalForm" >
	<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
	<s:hidden id="tipoSiniestroDesc" name="tipoSiniestroDesc"/>
	<s:hidden id="tipoSiniestro" name="tipoSiniestro"/>
	<s:hidden id="tipoBajaPlacas" name="informacionSiniestro.tipoBajaPlacas"/>
	<s:hidden id="fechaDeterminacion" name="fechaDeterminacion"/>
	<s:hidden id="idSiniestro" name="informacionSiniestro.idSiniestro"/>
	<s:hidden id="idReporteCabina" name="informacionSiniestro.idReporteCabina"/>
	<s:hidden id="idValuador" name="informacionSiniestro.idValuador"/>
	<s:hidden id="esFiniquitoAsegurado" name="esFiniquitoAsegurado"/>
	<s:hidden id="esCoberturaAsegurado" name="esCoberturaAsegurado"/>
	
	
	
	<s:hidden id="guardadoCorrecto" name="guardadoCorrecto"/>
	<s:hidden id="esConsulta" name="esConsulta"/>
	<s:hidden id="esRecepcionPagoDanios" name="esRecepcionPagoDanios"/>
	
	<s:hidden id="numPoliza" name="informacionSiniestro.numPoliza"/>
	<s:hidden id="inciso" name="informacionSiniestro.inciso"/>
	<s:hidden id="numReporte" name="informacionSiniestro.numReporte"/>
	<s:hidden id="agente" name="informacionSiniestro.agente"/>
	<s:hidden id="numSiniestro" name="informacionSiniestro.numSiniestro"/>
	<s:hidden id="fechaSiniestro" name="informacionSiniestro.fechaSiniestro"/>
	<s:hidden id="tipoSiniestro" name="informacionSiniestro.tipoSiniestro"/>
	<s:hidden id="lugarSiniestro" name="informacionSiniestro.lugarSiniestro"/>
	<s:hidden id="asegurado" name="informacionSiniestro.asegurado"/>
	<s:hidden id="primaTotal" name="informacionSiniestro.primaTotal"/>
	<s:hidden id="beneficiario" name="informacionSiniestro.beneficiario"/>
	<s:hidden id="terminoAjuste" name="informacionSiniestro.terminoAjuste"/>
	<s:hidden id="terminoSiniestro" name="informacionSiniestro.terminoSiniestro"/>
	
	<s:hidden id="marcaTipo" name="informacionSiniestro.marcaTipo"/>
<%-- 	<s:hidden id="color" name="informacionSiniestro.color"/> --%>
<%-- 	<s:hidden id="numSerie" name="informacionSiniestro.numSerie"/> --%>
<%-- 	<s:hidden id="motor" name="informacionSiniestro.motor"/> --%>
<%-- 	<s:hidden id="modelo" name="informacionSiniestro.modelo"/> --%>
<%-- 	<s:hidden id="puertas" name="informacionSiniestro.puertas"/> --%>
	<s:hidden id="esTercero" name="informacionSiniestro.esTercero"/>

	<s:hidden id="idOrdenCompra" name="idOrdenCompra"/>
	<s:hidden id="etapa" name="indemnizacion.etapa"/>
	<s:hidden id="estatusPT" name="indemnizacion.estatusPerdidaTotal"/>
	


		<div style="width: 98%; text-align: right;">
			<label style="font-weight: bold; margin-right: 5px"><s:text name="midas.siniestros.indemnizacion.determinacion.fecha" />:</label>
			<s:text name="fechaDeterminacion" />
		</div>


	<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.indemnizacion.determinacion.infosiniestro"/>	
	</div>
	
	<div id="contenedorFiltros" style="width: 96%;">
		<table id="agregar" border="0">
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numpoliza" /></th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.numPoliza"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.inciso" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.inciso"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numreporte" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.numReporte"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.agente" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.agente"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numsiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.numSiniestro"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.fechasiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.fechaSiniestro"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.tiposiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.tipoSiniestro"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.lugarsiniestro" /> </th>
				<td style="width: 40%;">
					<s:property value="informacionSiniestro.lugarSiniestro"/>
				</td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.asegurado" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.asegurado"/></td>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.terminoajuste" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.terminoAjuste"/></td>
			</tr>
			<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.beneficiario" /></th>
				<s:if test='%{indemnizacion.estatusPerdidaTotal == "A" }'>
					<td style="width: 40%;"><s:property value="informacionSiniestro.beneficiario"/></td>	
					<s:hidden id="bnfEdited" name="informacionSiniestro.beneficiario"/>
				</s:if>
				<s:else>
					<td style="width: 40%;"><s:textfield id ="bnfEdited" name="informacionSiniestro.beneficiario" cssClass="cajaTexto w200 alphaextra"  /></td>
				</s:else>
				
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.terminosiniestro" /> </th>
				<td style="width: 40%;"><s:property value="informacionSiniestro.terminoSiniestro"/></td>
			</tr>
		</table>
	</div>
	
	<div style="width: 97%;">
		<div class="floatLeft" style="width:63%; margin-right: 1%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.descripcionunidad"/>	
			</div>
			<table id="agregar" border="0" height="120px">
				<s:if test='%{informacionSiniestro.esTercero == "N" }'>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.marcatipo" /></th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.marcaTipo"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.color" /></th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.color"/>  </td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numserie" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.numSerie"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.motor" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.motor" /></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.modelo" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.modelo"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.puertas" /> </th>
					<td style="width: 40%;"><s:property value="informacionSiniestro.puertas" /></td>
				</tr>
				</s:if>
				<s:else>
					<tr>
				<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.marca" /></th>
				   <td style="width: 40%;"><s:textfield name="informacionSiniestro.marca"  cssClass="cajaTextoM2"/></td>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.tipo"/></th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.tipo"  cssClass="cajaTextoM2"/></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numserie"  /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.numSerie"  cssClass="cajaTextoM2"/></td>
					
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.motor" /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.motor"  cssClass="cajaTextoM2" /></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.modelo" /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.modelo"  cssClass="cajaTextoM2"/></td>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.placas"/></th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.placa"  cssClass="cajaTextoM2"/></td>
				</tr>
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.determinacion.color"  /> </th>
					<td style="width: 40%;"><s:textfield name="informacionSiniestro.color"  cssClass="cajaTextoM2"/></td>
					<th style="width: 10%;"></th>
					<td style="width: 40%;"></td>
				</tr>
				</s:else>
				</table>
		</div>
		<div class="floatLeft" style="width:33%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.guiasutilizadasprecio"/>	
			</div>
			<table id="agregar" border="0">
				<tr style="height: 5px;">
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.guianada" /></th>
					<td style="width: 50%;"><s:textfield id="guiaNada" name="indemnizacion.guiaNada"  cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						onblur="formato2Decimales(this); calculaGuias();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.kellybluebook" /></th>
					<td style="width: 50%;"><s:textfield id="guiaKbb" name="indemnizacion.guiaKbb" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this); calculaGuias();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.guiaebc" /></th>
					<td style="width: 50%;"><s:textfield id="guiaEbc" name="indemnizacion.guiaEbc" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this); calculaGuias();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.guiaautometrica" /></th>
					<td style="width: 50%;"><s:textfield id="guiaAutometrica" name="indemnizacion.guiaAutometrica" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this); calculaGuias();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.valorperitopesado" /></th>
					<td style="width: 50%;"><s:textfield id="valorPeritoEqPesado" name="indemnizacion.valorPeritoEqPesado" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.valormenos20pt" /></th>
					<td style="width: 50%;"><s:textfield id="valorPorcPerdidaTotal" name="indemnizacion.valorPorcPerdidaTotal" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.valorfactura" /></th>
					<td style="width: 50%;"><s:textfield id="valorFactura" name="indemnizacion.valorFactura" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
			</table>
		</div>
	</div>
	
	<div style="width: 97%;">
		<div class="floatLeft" style="width:48%; margin-right: 1%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.determinacionptvaluaciondanios"/>	
			</div>
			<table id="agregar" border="0" height="320px">
				<tr style="height: 5px;">
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.manoobra" /></th>
					<td style="width: 50%;"><s:textfield id="manoObraHP" name="indemnizacion.manoObraHP"  cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this); calcularTotalDanosPiezas();calcularPorcDaniosVsValorUnidad();"    onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.refacciones" /></th>
					<td style="width: 50%;"><s:textfield id="refaccionesCarroceria" name="indemnizacion.refaccionesCarroceria" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this); calcularTotalDanosPiezas();calcularPorcDaniosVsValorUnidad();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.faltantesdesarmar" /></th>
					<td style="width: 50%;"><s:textfield id="faltanteDesarme" name="indemnizacion.faltanteDesarme" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.totaldaniospiezas" /></th>
					<td style="width: 50%;"><s:textfield id="totalMoPiezas" name="indemnizacion.totalMoPiezas" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)" readonly="true"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.porcdaniosvsvalorunidad" /></th>
					<td style="width: 50%;"><s:textfield id="porcentajeDanioValorUnidad" name="indemnizacion.porcentajeDanioValorUnidad" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)" readonly="true"></s:textfield></td>
				</tr>
			</table>
		</div>
		<div class="floatLeft" style="width:48%; margin-bottom:2%">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.determinacionmontoindem"/>
			</div>
			<table id="agregar" border="0">
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.sumaguias" /></th>
					<td style="width: 50%;"><s:textfield id="sumaGuias" name="indemnizacion.sumaGuias"  cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"
						 onblur="formato2Decimales(this);" readonly="true" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.numeroguiasaplicadas" /></th>
					<td style="width: 50%;"><s:textfield id="numeroGuias" name="indemnizacion.numeroGuias" readonly="true" cssClass="cajaTexto w150 alphaextra esConsulta" onkeypress="return soloNumeros(this, event, false)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.promedioguias" /></th>
					<td style="width: 50%;"><s:textfield id="promedioGuias" name="indemnizacion.promedioGuias" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" readonly="true" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.valorunidad" /></th>
					<td style="width: 50%;"><s:textfield id="valorUnidad" name="indemnizacion.valorUnidad" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="onChangeValorUnidad();calcularPorcDaniosVsValorUnidad();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.deduciblemenos" /></th>
					<td style="width: 50%;"><s:textfield id="deducible" name="informacionSiniestro.montoTotalDeducible" cssClass="cajaTexto w150 formatCurrency" readonly="true"/></td>
				</tr>
				<s:iterator value="informacionSiniestro.deducibles" status="rowstatus">
					<tr>
						<td style="text-color:gray;text-indent: 5px;" >
							<s:if test="%{ingresoGenerado}">
								<img src="<s:url value="/img/b_comentariog.gif"/>" alt="Existe un ingreso pendiente por lo que no se puede modificar el deducible" title="Existe un ingreso pendiente por lo que no se puede modificar el deducible" />
							</s:if>
							<s:property  value="descripcion" escapeHtml="false" escapeXml="true" />							
						</td>
						<td >
							<s:hidden name="informacionSiniestro.deducibles[%{#rowstatus.index}].idRecuperacion"/>
							<s:hidden name="informacionSiniestro.deducibles[%{#rowstatus.index}].ingresoGenerado"/>
							<s:property value="%{#informacionSiniestro.deducibles[%{#rowstatus.index}].ingresoGenerado}"/>
							<s:if test="%{ingresoGenerado}">								
								<s:textfield name="informacionSiniestro.deducibles[%{#rowstatus.index}].monto" id="montoDeducible"
								cssStyle="background-color:#FAFAF0;" readonly="true" 
								cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"/>								
							</s:if>
							<s:else>								
								<s:textfield name="informacionSiniestro.deducibles[%{#rowstatus.index}].monto" id="montoDeducible" onblur="onChangeDeducible(this.id);" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"/>
							</s:else>
						</td>
					</tr>				
				</s:iterator>				
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.subtotal1" /></th>
					<td style="width: 50%;"><s:textfield id="subTotal" name="indemnizacion.subTotal" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"  readonly="true"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.descuento" /></th>
					<td style="width: 50%;"><s:textfield id="descuento" name="indemnizacion.descuento" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"
						onblur="validarMaximoMontoDescuento();calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.notadescuento" /></th>
					<td style="width: 50%;"><s:textfield id="notaDescuento" name="indemnizacion.notaDescuento" cssClass="cajaTexto w150 alphaextra esConsulta" maxlength="100"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><div style="display:none;"><s:text name="midas.siniestros.indemnizacion.determinacion.valroequipoespecial" /></div></th>
					<td style="width: 50%;"><div style="display:none;"><s:textfield id="valorEqAdaptaciones" name="indemnizacion.valorEqAdaptaciones" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></div></td>
				</tr>
				<tr>
					<th style="width: 50%;"><div style="display:none;"><s:text name="midas.siniestros.indemnizacion.determinacion.deduciblemenos" /></div></th>
					<td style="width: 50%;"><div style="display:none;"><s:textfield id="deducibleEqAdaptaciones" name="indemnizacion.deducibleEqAdaptaciones" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"  
						onblur="calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></div></td>
				</tr>
				
				<s:if test="tipoSiniestro == 'RB'  || esFiniquitoAsegurado==true  ">
				
					<tr>
						<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.primaspendientespoliza" /></th>
						<td style="width: 50%;"><s:textfield id="primasPendientes" name="indemnizacion.primasPendientes" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
							 onblur="calcularTotalaIndemnizar();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
					</tr>
				</s:if>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.totalindemnizar" /></th>
					<td style="width: 50%;"><s:textfield id="totalIndemnizar" name="indemnizacion.totalIndemnizar" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency"  readonly="true"></s:textfield></td>
				</tr>
				<s:if test="esRecepcionPagoDanios == true">
					<tr>
						<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.pagodanios" /></th>
						<td style="width: 50%;"><s:checkbox id="esPagoDanios" name="indemnizacion.esPagoDanios" cssClass="esConsulta" onclick="habilitarCapturaTotalPD();"></s:checkbox></td>
					</tr>
					<tr>
						<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.totaldanios" /></th>
						<td style="width: 50%;"><s:textfield id="totalPagoDanios" name="indemnizacion.totalPagoDanios" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" readonly="true"
						 onblur="validarTotalPagoDanios();" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
					</tr>
				</s:if>
			</table>
		</div>
	</div>
	
	<div style="width: 97%;">
		<div class="floatLeft" style="width:48%; margin-right: 1%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.estimacionsalvamento"/>	
			</div>
			<table id="agregar" border="0">
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.guiaautometrica" /></th>
					<td style="width: 50%;"><s:textfield id="guiaAutometricaSalv" name="indemnizacion.guiaAutometricaSalv" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.ultimatenencia" /></th>
					<td style="width: 50%;"><s:textfield id="ultimaTenencia" name="indemnizacion.ultimaTenencia"  maxlength="4" cssClass="cajaTexto w150 alphaextra esConsulta" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.ubicacionsalvamento" /></th>
					<td style="width: 50%;"><s:textfield id="ubicacionSalvamento" name="indemnizacion.ubicacionSalvamento" cssClass="cajaTexto w150 alphaextra esConsulta" ></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.sugeridovaluador" /></th>
					<td style="width: 50%;"><s:textfield id="estimadoValuador" name="indemnizacion.estimadoValuador" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.sugerido" /></th>
					<td style="width: 50%;"><s:textfield id="sugeridoDireccion" name="indemnizacion.sugeridoDireccion" cssClass="cajaTexto w150 alphaextra esConsulta dosDecimales formatCurrency" 
						 onblur="formato2Decimales(this);" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
			</table>
		</div>
		<div id="criteriosInvRobos" class="floatLeft" style="width:48%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="midas.siniestros.indemnizacion.determinacion.criteriosnoinvestigacionrobo"/>	
			</div>
			<table id="agregar" border="0" height="255px">
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.derivadoautoplazo" /></th>
					<td style="width: 50%; text-align: center;"><s:checkbox id="derivadoAutoplazo" name="indemnizacion.derivadoAutoplazo" cssClass="esConsulta" ></s:checkbox></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.personasmoralesrenovaciones" /></th>
					<td style="width: 50%; text-align: center;"><s:checkbox id="personaMoralRenovacion" name="indemnizacion.personaMoralRenovacion"  cssClass="esConsulta" ></s:checkbox></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.personasfisicasrenovacion" /></th>
					<td style="width: 50%; text-align: center;"><s:checkbox id="personaFisicaRenovacion" name="indemnizacion.personaFisicaRenovacion" cssClass="esConsulta" ></s:checkbox></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.unidadinspeccionada" /></th>
					<td style="width: 50%; text-align: center;"><s:checkbox id="unidadInspecContratacion" name="indemnizacion.unidadInspecContratacion" cssClass="esConsulta" ></s:checkbox></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.otrospolizaanterior" /></th>
					<td style="width: 50%; text-align: center;"><s:checkbox id="polizaAnteriorInmediata" name="indemnizacion.polizaAnteriorInmediata" cssClass="esConsulta" ></s:checkbox></td>
				</tr>
				<tr>
					<th style="width: 50%;"><s:text name="midas.siniestros.indemnizacion.determinacion.siniestrosanteriores" /></th>
					<td style="width: 50%; text-align: center;"><s:checkbox id="siniestrosAnteriores" name="indemnizacion.siniestrosAnteriores" cssClass="esConsulta" ></s:checkbox></td>
				</tr>
						
			</table>
			<br>
		</div>
	</div>
	
	<div id="contenedorFiltros" style="width: 96%;">
			<div class="floatLeft" style="width:9%; margin-right: 1%; margin-left: 1%; font-weight: bold;">
				<div class="floatLeft" style="width: 90%;">
					<s:text name="midas.siniestros.indemnizacion.determinacion.observaciones" />
				</div>
			</div>
			<div class="floatLeft" style="width:*90%;">
				<div class="floatLeft" style="width: 90%;">
					<div id="contenedorTextArea">
					<s:textarea name="indemnizacion.observaciones" id="observaciones" 
					cssClass="textarea deshabilitar" cssStyle="font-size: 10pt;"	cols="80" rows="3"
					onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onblur="truncarTexto(this,3000);"/>
				</div>
				</div>
			</div>
	
	</div>
	
	<div style="width: 99%;">
		<div class="floatLeft" style="width:96%; margin-right: 1%;">
			<div class="titulo floatLeft" style="width: 90%;">
				<s:text name="Autorizaciones"/>	
			</div>
		</div>
		<div class="floatLeft" style="width:96%; margin-right: 1%;">
			<div id="autorizacionGrid" style="width: 99%; height: 120px;"></div>
			<div id="pagingArea" style="padding-top: 8px"></div>
			<div id="infoArea"></div>
		</div>
	</div>
	
</s:form>

<br/>
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>
			<td>
				<div id="btn_guardar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:guardarDeterminacionPT();"> 
						<s:text name="midas.boton.guardar" /> 
						<img border='0px' alt='Guardar' title='Guardar' src='/MidasWeb/img/common/btn_guardar.jpg'/>
					</a>
				</div>	
				<div id="btn_Autorizar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:mostrarAutorizacionDT('DT');"> 
						<s:text name="Autorizar" /> 
						<img border='0px' alt='Autorizar' title='Autorizar' src='/MidasWeb/img/common/btn_guardar.jpg'/>
					</a>
				</div>		
				<div id="btn_imprimir" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="javascript:imprimirDeterminacionPT();"> 
						<s:text name="midas.boton.imprimir" />
						<img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/common/b_imprimir.gif'/>
					</a>
				</div>
				<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="regresarACatalogoPerdidaTotal();"> 
						<s:text name="midas.boton.cerrar" /> 
						<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_borrar.gif'/>
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<br/>
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>
			<td>
				<div id="btn_hgs" class="btn_back w140" style="display: inline; float: right;position: relative;">
					<a href="javascript: void(0);" onclick="mostrarPaginaHGS();"> 
					<s:text name="midas.siniestros.indemnizacion.autorizacion.hgs" /> </a>
				</div>
				<div id="btn_bajaplacas" class="btn_back w140 determinacionRegistrada" style="display: none; float: right;">
					<a href="javascript: void(0);" onclick="javascript:mostrarBajaPlacas();"> 
						<s:text name="midas.siniestros.indemnizacion.determinacion.bajaplacas" />
					</a>
				</div>
				<div id="btn_notificacionbp" class="btn_back w140 determinacionRegistrada" style="display: none; float: right;" >
					<a href="javascript: void(0);" onclick="imprimirNotificacionPT();"> 
						<s:text name="midas.siniestros.indemnizacion.determinacion.cartanotificacion" /> 
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">

jQuery(document).ready(
	function(){
	 	configurarDeterminacionPT();
	 	initCurrencyFormatOnTxtInput();
 	}
 );
</script>

<script src="<s:url value='/js/siniestros/indemnizacion/determinarPT.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/bajaPlacas.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>