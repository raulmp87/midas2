<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<sj:head/>


<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
	
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<script type="text/javascript">
var imprimirDeterminacionPath = '<s:url action="imprimirCartaFiniquito" namespace="/siniestros/indemnizacion/cartas"/>';
</script>

<s:form id="cartaFiniquitoForm" action="guardarCartaFiniquito" namespace="/siniestros/indemnizacion/cartas" name="cartaFiniquitoForm">
	<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
	<s:hidden id="tipoSiniestro" name="tipoSiniestro"/>
	<s:hidden id="esFiniquitoAsegurado" name="esFiniquitoAsegurado"/>
	<s:hidden id="idCartaFiniquito" name="cartaSiniestro.id" />
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
							<div id="tituloAsegurado" class="asegurado" style="display:none;">
								<s:text name="midas.siniestros.indemnizacion.finiquito.tituloasegurado" />
							</div> 
							<div id="tituloTercero" class="tercero" style="display:none;">
								<s:text name="midas.siniestros.indemnizacion.finiquito.titulotercero" />
							</div>
	                    </td>
	                  </tr>
					  
					  <s:if test="esFiniquitoAsegurado">
						  <tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.finiquito.fechareclamacion" /> </th>
							<td style="width: 20%;">
									<sj:datepicker name="cartaSiniestro.fechaReclamacion"
								id="fechaReclamacion" buttonImage="/MidasWeb/img/b_calendario.gif"
								changeYear="true" changeMonth="true" maxlength="10"
								cssClass="w100 cajaTextoM2"
								cssStyle="margin-left:0px;" 
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								></sj:datepicker>
							</td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.finiquito.resultadoacc" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.resultadoAcc" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
							
							<th></th>
							<td style="width: 15%;"></td>
						  </tr>
						  
						  <tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.finiquito.fechacarta" /> </th>
							<td style="width: 20%;">
								<sj:datepicker name="cartaSiniestro.fechaCarta"
									id="fechaCarta" buttonImage="/MidasWeb/img/b_calendario.gif"
									changeYear="true" changeMonth="true" maxlength="10"
									cssClass="w100 cajaTextoM2"
									cssStyle="margin-left:0px;" 
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									></sj:datepicker>
							</td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.finiquito.lugar" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.lugar"  cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.finiquito.direccionasegurado" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.domicilioAsegurado" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
														
						  </tr>
					  </s:if>
					  
					  <s:elseif test="esFiniquitoAsegurado != null">
						<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.finiquito.nombreAsegurado" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.nombreAsegurado"  cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
							
							<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.finiquito.conductor" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.nombreConductor" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
							
							<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.finiquito.direccionasegurado" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.domicilioAsegurado" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
						</tr>
					
						<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.finiquito.lugar" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.lugar"  cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
						
							<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.finiquito.fechacarta" /> </th>
							<td style="width: 15%;">
								<sj:datepicker name="cartaSiniestro.fechaCarta"
									id="fechaCarta" buttonImage="/MidasWeb/img/b_calendario.gif"
									changeYear="true" changeMonth="true" maxlength="10"
									cssClass="w110 cajaTextoM2"
									cssStyle="margin-left:0px;" 
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									></sj:datepicker>
							</td>
					
							<th style="padding-left: 5%;"></th>
							<td style="width: 15%;"></td>
						</tr>
					
						<tr>
							<th style="width: 12%;"><s:text name="midas.siniestros.indemnizacion.finiquito.nombreTercero" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.nombreTercero" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
						
							<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.finiquito.domicilioTercero" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.domicilioTercero"  cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
							
							<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.finiquito.telefonotercero" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.telefonoTercero" cssClass="cajaTexto w150 alphaextra" ></s:textfield></td>
					  	</tr>
					  </s:elseif>
					  
	            </tbody>
	</table>
	<br/>
	<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:cerrarVentanaFiniquito();}"> 
		<s:text name="midas.boton.cerrar" /> </a>
	</div>
	<div id="btn_imprimir" class="btn_back w80 validarTipoFiniquitoNotNull" style="display: none; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="imprimirCartaFiniquitoWO();"> 
		<s:text name="midas.boton.imprimir" /> </a>
	</div>
	
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
	 	configurarCartaFiniquito();
 	}
 );
</script>

<script type="text/javascript" src="<s:url value='/js/siniestros/indemnizacion/cartaFiniquito.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>