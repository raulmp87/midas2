<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<afterInit>
			<call command="splitAt"><param>6</param></call>
		</afterInit>

		
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.oficina" />   </column>
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.cobertura" />   </column>
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.tiposiniestro" />  </column>
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.numsiniestro" />  </column>
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.numreporte" />  </column>
		<column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.numpoliza" />  </column>
	    <column  type="ro"  width="150"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.numserie" />  </column>
	    <column  type="ro"  width="80"  align="center" sort="int"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.numvaluacion" />   </column>
	    <column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.numpaseatencion" />   </column>
	    <column  type="ro"  width="80"  align="center" sort="date_custom"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.fechasiniestro" />   </column>
	    <column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.etapa" />   </column>
	    <column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.estatuspt" />   </column>
	    <column  type="ro"  width="80"  align="center" sort="str"> <s:text name="midas.siniestros.indemnizacion.perdidatotal.estatusindemnizacion" />   </column>
	    <m:tienePermiso nombre="FN_M2_SN_Indem_Autorizar_Perdidas_Totales">
	    <column  type="img"   width="30" sort="na" align="center" >Acciones</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Indem_Determinancion_PT">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Indem_Recepcion_Docs">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Indem_Formato_Fecha_PT">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Indem_Autorizar_Perdidas_Totales,FN_M2_SN_SubDirector_Indemnizacion">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Orden_Compra_Indemnizacion">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Cancelar_Indemnizacion">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	    <m:tienePermiso nombre="FN_M2_SN_Cancelar_Indemnizacion">
	    <column  type="img"   width="30" sort="na" align="center" >#cspan</column>
	    </m:tienePermiso>
	</head>

	<s:iterator value="listaPerdidasTotales">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="oficinaNombre" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="cobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoSiniestroDesc" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroReporte" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroValuacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numPaseAtencion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="etapaDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatusPerdidaTotalDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatusIndemnizacionDesc" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="tipoSiniestro == \"PT\"">
				 <!--  <m:tienePermiso nombre="FN_M2_SN_Indem_Autorizar_Perdidas_Totales">
					<cell>../img/icons/ico_aceptar.gif^Autorizacion Perdida Total^javascript:mostrarAutorizar(<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>,"AT","GRID")^_self</cell>
				</m:tienePermiso> -->
		  	  <cell>../img/pixel.gif^^^_self</cell>
			</s:if>
			<s:else>
		    <!--  	<cell>../img/pixel.gif^Autorizacion de PT no aplica para robos.^^_self</cell>-->
		    <cell>../img/pixel.gif^^^_self</cell>
		    </s:else>
		    <s:if test="etapa != \"AT\"">
		    	<s:if test="estatusPerdidaTotal == \"P\" && estatusIndemnizacion != \"C\" ">
			    	<cell>../img/icons/ico_agregar.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detdef"/>^javascript:mostrarDeterminacionPT(<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestroDesc" escapeHtml="false" escapeXml="true"/>","<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>",0,<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detedierror"/>^^_self</cell>
				</s:if>
				<s:else>
					<m:tienePermiso nombre="FN_M2_SN_Indem_Determinancion_PT">
						<cell>../img/icons/ico_verdetalle.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detcon"/>^javascript:mostrarDeterminacionPT(<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestroDesc" escapeHtml="false" escapeXml="true"/>","<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>",1,<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
					</m:tienePermiso>
					<s:if test="existeOrdenCompraFinal == \"false\" && estatusIndemnizacion != \"R\" && estatusIndemnizacion != \"C\"   &&  estatusPerdidaTotal != \"R\"   ">
						<m:tienePermiso nombre="FN_M2_SN_Indem_Determinancion_PT">
							<cell>../img/icons/ico_editar.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detedi"/>^javascript:mostrarDeterminacionPT(<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestroDesc" escapeHtml="false" escapeXml="true"/>","<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>",0,<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
						</m:tienePermiso>
					</s:if>
					<s:else>
						<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detdes"/>^^_self</cell>
					</s:else>
				</s:else>
				<s:if test="etapa != \"DT\"">
					<s:if test="existeRecepcionDocumentos == \"false\" && estatusIndemnizacion != \"C\" ">
				    	<cell>../img/icons/ico_agregar.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrddef"/>^javascript:mostrarRecepcionDoctos(<s:property value="idSiniestro" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoBajaPlacas" escapeHtml="false" escapeXml="true"/>",0, <s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>")^_self</cell>
				    	<cell>../img/menu_icons/new.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdcpt"/>^javascript:imprimirCartaPTSAccion(<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			    	</s:if>
			    	<s:else>
						<m:tienePermiso nombre="FN_M2_SN_Indem_Recepcion_Docs">
							<cell>../img/icons/ico_verdetalle.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdcon"/>^javascript:mostrarRecepcionDoctos(<s:property value="idSiniestro" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoBajaPlacas" escapeHtml="false" escapeXml="true"/>",1, <s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>")^_self</cell>
						</m:tienePermiso>
						<s:if test="existeOrdenCompraFinal == \"false\" && estatusIndemnizacion != \"R\" && estatusIndemnizacion != \"C\"">
							<m:tienePermiso nombre="FN_M2_SN_Indem_Recepcion_Docs">
								<cell>../img/icons/ico_editar.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdedi"/>^javascript:mostrarRecepcionDoctos(<s:property value="idSiniestro" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoBajaPlacas" escapeHtml="false" escapeXml="true"/>",0, <s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>")^_self</cell>
							</m:tienePermiso>
						</s:if>
						<s:else>
							<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrddes"/>^^_self</cell>
						</s:else>
					</s:else>
					<s:if test="etapa != \"RD\" ">
						<m:tienePermiso nombre="FN_M2_SN_Indem_Formato_Fecha_PT">
							<cell>../img/menu_icons/print_dis.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffecon"/>^javascript:mostrarFormatoFecha(<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>,1,"<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>",<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
							<s:if test="existeOrdenCompraFinal == \"false\" && estatusIndemnizacion != \"R\" && estatusIndemnizacion != \"C\" && estatusPerdidaTotal != \"R\"">
							<cell>../img/menu_icons/print.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffeedi"/>^javascript:mostrarFormatoFecha(<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>,0,"<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>",<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
							</s:if>
							<s:else>
								<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffedes"/>^^_self</cell>
							</s:else>
						</m:tienePermiso>
					</s:if>
					<s:else>
						<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffeconrrd"/>^^_self</cell>
						<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffeedirrd"/>^^_self</cell>
					</s:else>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdcondet"/>^^_self</cell>
					<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdedidet"/>^^_self</cell>
					<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffecondet"/>^^_self</cell>
					<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffeedidet"/>^^_self</cell>
				</s:else>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detconaut"/>^^_self</cell>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.detediaut"/>^^_self</cell>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdconaut"/>^^_self</cell>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.rrdediaut"/>^^_self</cell>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffeconaut"/>^^_self</cell>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.ffeediaut"/>^^_self</cell>
		    </s:else>
			<s:if test="etapa == \"AI\" || etapa == \"OC\"">
			<m:tienePermiso nombre="FN_M2_SN_Indem_Autorizar_Perdidas_Totales">
				<cell>../img/icons/ico_aceptar.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.autind"/>^javascript:mostrarAutorizar(<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>,"AI","GRID")^_self</cell>
			</m:tienePermiso>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.autrrd"/>^^_self</cell>
			</s:else>
			<s:if test="estatusIndemnizacion == \"A\" && existeOrdenCompraFinal == \"false\" ">
				<m:tienePermiso nombre="FN_M2_SN_Orden_Compra_Indemnizacion">
					<cell>../img/menu_icons/new.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.genorc"/>^javascript:consultarOrdenCompraListadoIndemnizacion(<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>, "<s:property value="formaPago" escapeHtml="false" escapeXml="true"/>",<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
			<s:else>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.genaut"/>^^_self</cell>
		    </s:else>
			  <cell>../img/icons/ico_timbrar.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.generarCFDI"/>^javascript:mostrarVentanaPerdidaTotalCFDI(<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipoSiniestroDesc" escapeHtml="false" escapeXml="true"/>","<s:property value="tipoSiniestro" escapeHtml="false" escapeXml="true"/>",0,<s:property value="idOrdenCompra" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    <s:if test="existeOrdenCompraFinal == \"false\" && estatusIndemnizacion != \"R\" && estatusPerdidaTotal != \"R\"">
		    <m:tienePermiso nombre="FN_M2_SN_Cancelar_Indemnizacion">
		    	<cell>../img/icons/ico_rechazar2.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.cclind"/>^javascript:mostrarVentanaCancelarIndemnizacion(<s:property value="idIndemnizacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </m:tienePermiso>
		    </s:if>
			<s:else>
		    	<cell>../img/pixel.gif^<s:text name="midas.siniestros.indemnizacion.perdidatotal.cclocg"/>^^_self</cell>
		    </s:else>
		</row>
	</s:iterator>
	
</rows>