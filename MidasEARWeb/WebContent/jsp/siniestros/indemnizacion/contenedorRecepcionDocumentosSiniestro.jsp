<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<style type="text/css">
 .labelBlack{
 	color:black !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 9px;
	font-family: verdana,arial;
	}
</style>

<script type="text/javascript">
	var guardarPath= '<s:url action="guardarRecepcionDoctos" namespace="/siniestros/indemnizacion/cartas/recepciondoctos"/>';
	var validarPath = '<s:url action="validarRecepcionDoctos" namespace="/siniestros/indemnizacion/cartas/recepciondoctos"/>';
	var imprimirPath = '<s:url action="imprimirRecepcionDoctos" namespace="/siniestros/indemnizacion/cartas/recepciondoctos"/>';
	var mostrarEntregaDocumentosPath = '<s:url action="mostrarEntregaDocumentos" namespace="/siniestros/indemnizacion/cartas"/>';
	var mostrarBusquedaPath = '<s:url action="mostrarCatalogo" namespace="/siniestros/indemnizacion/perdidatotal"/>';
	var mostrarVentanaInfoContratoPath = '<s:url action="mostrarInfoContrato" namespace="/siniestros/indemnizacion/cartas"/>';
	var imprimirCartaFiniquitoPath = '<s:url action="imprimirCartaFiniquito" namespace="/siniestros/indemnizacion/cartas"/>';
</script>

<s:form id="recepcionDoctosForm" >
		<s:hidden id="siniestroId" name="siniestroId"/>
		<s:hidden id="tipoCarta" name="tipoCarta"/>
		<s:hidden id="fechaOcurrido" name="recepcionDoctoSin.siniestroCabina.reporteCabina.fechaOcurrido"/>
		<s:hidden id="recepcionSiniestroId" name="recepcionDoctoSin.siniestroCabina.id"/>
		<s:hidden id="esConsulta" name="esConsulta"/>
		<s:hidden id="recepcionDoctosId" name="recepcionDoctoSin.id"/>
		<s:hidden id="guardadoCorrecto" name="guardadoCorrecto"/>
		<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
		<s:hidden id="mostrarCartaPT" name="mostrarCartaPT"/>
		<s:hidden id="tipoSiniestro" name="tipoSiniestro"/>
		
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.indemnizacion.recepciondoctos.titulo"/>	
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>	
				<tr>
					<th style="width: 10%;"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.numsiniestro" /> </th>
					<td><s:textfield name="numeroSiniestro"  cssClass="cajaTexto w250 alphaextra" readonly="true"  readonly="true" ></s:textfield></td>
					
					<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.fecha" /> </th>
					<td style="width: 15%;"><s:textfield name="fechaActual" cssClass="cajaTexto w150 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.vehiculoafectado" /> </th>
					<td colspan="3"><s:textfield name="informacionAuto.tipoCarta" cssClass="cajaTexto w250 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.marca" /> </th>
					<td colspan="3"><s:textfield name="informacionAuto.marcaDesc" cssClass="cajaTexto w250 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.tipo" /> </th>
					<td colspan="3"><s:textfield name="informacionAuto.descTipoUso" cssClass="cajaTexto w250 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.anio" /> </th>
					<td colspan="3"><s:textfield name="informacionAuto.modelo" cssClass="cajaTexto w250 alphaextra" readonly="true" ></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.ubicacion" /> </th>
					<td colspan="3"><s:textfield name="recepcionDoctoSin.ubicacion" cssClass="cajaTexto w250 alphaextra deshabilitar requerido" maxlength="100"></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.direccion" /> </th>
					<td colspan="3"><s:textfield name="recepcionDoctoSin.direccion" cssClass="cajaTexto w250 alphaextra deshabilitar requerido" maxlength="100"></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.disposicion" /> </th>
					<td colspan="3"><s:textfield name="recepcionDoctoSin.disposicion" cssClass="cajaTexto w250 alphaextra deshabilitar requerido" maxlength="100"></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.pension" /> </th>
					<td colspan="3">
						<s:radio list="#{'SI':'SI','NO':'NO'}" value="%{recepcionDoctoSin.pension}" 
		                  				 onclick="" 
		                  				 id="pension_r" 
		                  				 name="recepcionDoctoSin.pension"
		                  				 cssClass="deshabilitar"/>
	                 </td>
				</tr>
				<s:if test="tipoCarta == 'BPPT'">
					<tr>
						<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.pagodanios" /> </th>
						<td colspan="3">
							<s:checkbox id="esPagoDanios" name="recepcionDoctoSin.esPagoDanios" cssClass="deshabilitar" ></s:checkbox>
		                 </td>
					</tr>
				</s:if>
			</tbody>
		</table>
		</div>
		
		<br/>
		
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.siniestros.indemnizacion.recepciondoctos.tituloinventariopapeleria"/>	
		</div>
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				<tr>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.juegollaves" /> </th>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.seguimientofactura" /> </th>
				</tr>
				<tr>
					<td colspan="2">
						<s:radio list="#{'1':'1','2':'2'}" value="%{recepcionDoctoSin.juegoLlaves}" 
		                  				onclick="" 
		                  				id="juegoLlaves_r" 
		                  				name="recepcionDoctoSin.juegoLlaves"
		                  				cssClass="deshabilitar"/>
	                 </td>
	                 <td colspan="2">
						<s:radio list="#{'SI':'SI','NO':'NO','NA':'NA'}" value="%{recepcionDoctoSin.seguimientoFactura}" 
		                  				onclick="" 
		                  				id="seguimientoFactura_r" 
		                  				name="recepcionDoctoSin.seguimientoFactura"
		                  				cssClass="deshabilitar"/>
	                 </td>
				</tr>
				
				
				<tr>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.refrendotenencia" /> </th>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.endosoafirme" /> </th>
				</tr>
				<tr>
					<td colspan="2">
						<s:checkboxlist list="refrendosTenencias" value="%{tenenciasSeleccionadas}"
		                  				onclick="" 
		                  				id="refrendoTenencia_c" 
		                  				name="recepcionDoctoSin.refrendoTenencia"
		                  				cssClass="deshabilitar"/>
	                 </td>
	                 <td colspan="2">
						<s:radio list="#{'SI':'SI','NO':'NO','NA':'NA'}" value="%{recepcionDoctoSin.endosoAfirme}" 
		                  				onclick="" 
		                  				id="endosoAfirme_r" 
		                  				name="recepcionDoctoSin.endosoAfirme"
		                  				cssClass="deshabilitar"/>
	                 </td>
				</tr>
				
				<tr>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.bajaplacas" /> </th>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.denunciarobo" /> </th>
				</tr>
				<tr>
					<td colspan="2">
						<s:radio list="#{'SI':'SI','NO':'NO'}" value="%{recepcionDoctoSin.bajaPlacas}" 
		                  				onclick="" 
		                  				id="bajaPlacas_r" 
		                  				name="recepcionDoctoSin.bajaPlacas"
		                  				cssClass="deshabilitar"/>
	                 </td>
	                 <td colspan="2">
						<s:radio list="#{'SI':'SI','NO':'NO','NA':'NA'}" value="%{recepcionDoctoSin.denunciaRobo}" 
		                  				onclick="" 
		                  				id="denunciaRobo_r" 
		                  				name="recepcionDoctoSin.denunciaRobo"
		                  				cssClass="deshabilitar"/>
	                 </td>
				</tr>
				
				
				<tr>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.tipopersona" /> </th>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.formapago" /> </th>
				</tr>
				<tr>
					<td colspan="2">
						<s:radio list="tiposPersona" value="%{recepcionDoctoSin.tipoPersona}" 
		                  				onclick="" 
		                  				id="tipoPersona_r" 
		                  				name="recepcionDoctoSin.tipoPersona"
		                  				cssClass="deshabilitar"/>
	                 </td>
	                 <td colspan="2">
						<s:radio list="formasPago" value="%{recepcionDoctoSin.formaPago}" 
		                  				onclick=""
		                  				id="formaPago_r" 
		                  				name="recepcionDoctoSin.formaPago"
		                  				cssClass="deshabilitar"/>
	                 </td>
				</tr>				
				
				
				<tr>
					<th colspan="2"><s:text name="midas.siniestros.indemnizacion.recepciondoctos.facturaoriginal" /> </th>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.beneficiario" /> </th>
					<td><s:textfield id="beneficiario_t" name="recepcionDoctoSin.beneficiario" cssClass="cajaTexto w150 alphanumeric deshabilitar restrict" maxlength="100"></s:textfield></td>
				</tr>
				<tr>
					<td colspan="2">
						<s:radio list="#{'SI':'SI','NO':'NO','NA':'NA'}" value="%{recepcionDoctoSin.factruraOriginal}" 
		                  				onclick="" 
		                  				id="facturaoriginal_r" 
		                  				name="recepcionDoctoSin.factruraOriginal"
		                  				cssClass="deshabilitar"/>
					</td>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.banco" /> </th>
					<td><s:select id="bancosList" 
										name="recepcionDoctoSin.bancoId"
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="bancos"
								  		cssClass="txtfield cajaTextoM2 w160 deshabilitar"   
								/></td>
				</tr>
				<tr>
					<td colspan="2">
					</td>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.clabe" /> </th>
					<td><s:textfield id="clabeBanco_t" name="recepcionDoctoSin.clabeBanco" cssClass="cajaTexto w150 alphaextra deshabilitar" maxlength="18" onkeypress="return soloNumeros(this, event, true, true)"></s:textfield></td>
				</tr>
				<tr>
					<td colspan="2">
					</td>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.rfc" /> </th>
					<td><s:textfield id="rfc_t" name="recepcionDoctoSin.rfc" cssClass="cajaTexto w150 jQalphanumeric deshabilitar jQrestrict" maxlength="13" ></s:textfield></td>
				</tr>
				<tr>
					<td colspan="2">
					</td>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.correo" /> </th>
					<td><s:textfield id="correo_t" name="recepcionDoctoSin.correo" cssClass="cajaTexto w150 jQemail deshabilitar jQrestrict" maxlength="100" ></s:textfield></td>
				</tr>
				<tr>
					<td colspan="2">
					</td>
					<th><s:text name="midas.siniestros.indemnizacion.recepciondoctos.telefono" /> </th>
					<td>
						<table>
							<tr>
								<td width="30px">
									<s:textfield id="telefonoLada_t" name="recepcionDoctoSin.telefonoLada" cssClass="floatLeft cajaTexto w30 jQnumeric deshabilitar jQrestrict" maxlength="3" ></s:textfield>
								</td>
								<td width="5px">
									-
								</td>
								<td>
									<s:textfield id="telefonoNumero_t" name="recepcionDoctoSin.telefonoNumero" cssClass="floatLeft cajaTexto w80 jQnumeric deshabilitar jQrestrict" maxlength="8" ></s:textfield>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="4" >
						<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
							<tbody>
								<tr>
									<td><label class="labelBlack" style="text-align: left;" ><s:text name="midas.siniestros.indemnizacion.recepciondoctos.comentarios" /></label></td>
								</tr>
								<tr>
									<td>
										<div id="contenedorTextArea">
											<s:textarea name="recepcionDoctoSin.comentarios" id="comentarios_a" 
											cssClass="textarea deshabilitar limited" cssStyle="font-size: 10pt;"
											cols="80" rows="3" onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onchange="truncarTexto(this,3000);"/>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		</div>
	
		<br/>
</s:form>

<br/>
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tbody>
		<tr>
			<td>
				<s:if test="recepcionDoctoSin.formaPago == \"TB\" &&  recepcionDoctoSin.id != null && recepcionDoctoSin.id != \"\" ">
					<div id="btn_infoContrato" class="btn_back w170" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:mostrarVentanaInfoContrato();"> 
								<s:text name="Contrato de Compra Venta" />
								<img border='0px' alt='Contrato de Compra Venta' title='Contrato de Compra Venta' src='/MidasWeb/img/b_aceptar.gif'/> 
							</a>
					</div>
					<div id="btn_imprimirFiniquito" class="btn_back w170" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:imprimirCartaFiniquito();"> 
								<s:text name="Finiquito" />
								<img border='0px' alt='Finiquito' title='Finiquito' src='/MidasWeb/img/b_aceptar.gif'/> 
							</a>
					</div>
				</s:if>
				<div id="btn_cartaPTS" class="btn_back w170" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:imprimirCartaPTS();"> 
							<s:text name="Carta PT No Documentada" />
							<img border='0px' alt='Carta PT No Documentada' title='Carta PT No Documentada' src='/MidasWeb/img/b_aceptar.gif'/> 
						</a>
				</div>
				<div id="btn_entrega" class="btn_back w160" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:mostrarVentanaEntregaDocumentos();"> 
						<s:text name="Entrega de Documentos" />
						<img border='0px' alt='Entrega de Documentos' title='Entrega de Documentos' src='/MidasWeb/img/b_aceptar.gif'/> 
					</a>
				</div>
				<div id="btn_guardar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript:guardarRecepcionDocumentos();"> 
						<s:text name="midas.boton.guardar" /> 
						<img border='0px' alt='Guardar Reporte' title='Guardar Reporte' src='/MidasWeb/img/common/btn_guardar.jpg'/>
					</a>
				</div>
				<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="regresarACatalogoPerdidaTotal();"> 
						<s:text name="midas.boton.cerrar" /> 
						<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_borrar.gif'/>
					</a>
				</div>
				<div id="btn_imprimir" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="javascript:imprimirRecepcionDocumentos();"> 
						<s:text name="midas.boton.imprimir" />
						<img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/common/b_imprimir.gif'/>
					</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>
	
<script type="text/javascript">
		initConfiguration();
</script>

<script src="<s:url value='/js/siniestros/indemnizacion/recepcionDocumentosSiniestro.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/entregaDocumentos.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/infoContrato.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>