<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin"> 
				<param>bricks</param>
			</call>   
        </beforeInit>    
        <column id="tipo" type="ro" width="150" sort="str"  align="center" >Tipo</column>
		<column id="motivo" type="ro" width="350" sort="str"  align="center" >Motivo</column>
		<column id="usuario" type="ro" width="300" sort="str" align="center">Usuario</column>
		<column id="puesto" type="ro" width="300" sort="str" align="center" >Puesto de Usuario</column>	
		<column id="est" type="img" width="20" align="center" ></column>
	</head>
	
	<s:iterator value="listaAutorizaciones" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="tipo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="comentarios" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigoUsuario" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="rol" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus == \"A\"">
		      	<cell>/MidasWeb/img/ico_green.gif^Aceptada^</cell>		
		    </s:if>
		     <s:else>
		      	<cell>/MidasWeb/img/ico_red.gif^Rechazada^</cell>			      	
		    </s:else>

		</row>
	</s:iterator>
	
</rows>