<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>
		<column  type="ro"  width="680"  align="center"> <s:text name="midas.siniestros.indemnizacion.entregadoctos.nombreendoso" />   </column>
		<column  type="img"	width="30" sort="na" align="center"></column>
	</head>

	<s:iterator value="endosos">
		<row id="<s:property value="id"/>">
			
			<cell><s:property value="nombreEndoso" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^<s:text name="midas.siniestros.indemnizacion.entregadoctos.eliminarendoso"/>^javascript:eliminarEndoso(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			
		</row>
	</s:iterator>
	
</rows>