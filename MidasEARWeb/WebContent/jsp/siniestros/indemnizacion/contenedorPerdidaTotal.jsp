<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<script type="text/javascript">
var exportarExcelPath = '<s:url action="exportarResultados" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var listarPath = '<s:url action="listar" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var mostrarAutorizarPath = '<s:url action="mostrarAutorizar" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var mostrarDeterminacionPath = '<s:url action="mostrarDeterminacionPT" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var mostrarCancelarPath = '<s:url action="mostrarCancelarIndemnizacion" namespace="/siniestros/indemnizacion/perdidatotal"/>';
var mostrarPerdidaTotalCFDI = '<s:url action="perdidaTotalCFDI" namespace="/siniestros/indemnizacion/perdidatotal"/>';
</script>

<style type="text/css">
.floatLeft {
	float: left;
	position: relative;
}
</style>

<s:form id="busquedaPerdidasTotales" >
	<s:hidden id="esConsulta" name="esConsulta"/>
	<s:hidden id="mostrarListadoVacio" name="mostrarListadoVacio"/>
	<s:hidden id="urlCancelarIndemnizacion" name="urlCancelarIndemnizacion"/>
	<s:hidden id="idIndemnizacion" name="filtroPT.idIndemnizacion" />

	<table  id="filtrosM2" width="98%">
            <tr>
                  <td class="titulo" colspan="6"><s:text name="midas.siniestros.indemnizacion.perdidatotal.busqueda" /></td>
            </tr>
            <tr>
            
            	<td>			   
	            	<s:select list="oficinasActivas" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="filtroPT.idOficina" id="oficina_s" cssClass="cajaTextoM2 w230" onchange=""
						label="%{getText('midas.siniestros.indemnizacion.perdidatotal.oficina')}"/>
            	</td>
            	
            	<td>
            		<s:select list="tiposSiniestro" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="filtroPT.tipoSiniestro" id="tipoSiniestro_s" cssClass="cajaTextoM2 w230" onchange=""
						label="%{getText('midas.siniestros.indemnizacion.perdidatotal.tiposiniestro')}"/>
            	</td>
            	
            	<td width="300px">
            		<div style="width: 100%;" class="floatLeft">
            				<div class="floatLeft" style="height: 30px; width: 80%;">
								<label style="margin-left: 5%; margin-top:7%; width: 80%;" class="floatLeft">
								<s:text name="midas.siniestros.indemnizacion.perdidatotal.numsiniestro" 
								/>: </label>
							</div>
						
							<div class="floatLeft" style="height: 30px; width: 10%;">
							</div>
							
							<div class="floatLeft" style="height: 30px; width: 33%;">
								<s:textfield id="nsOficina" maxlength="5"
									name="filtroPT.siniestroClaveOficina" cssStyle="width:100%;"
									cssClass="floatLeft setNew txtfield" ></s:textfield>
							</div>
							<div class="floatLeft" style="height: 30px; width: 5%; margin-top:3%;">	
								<label style="margin-left: 10%; width: 10%;" class="floatLeft">
									<b>-</b> </label>
							</div>	
							<div class="floatLeft" style="height: 30px; width: 34%;">
								<s:textfield id="nsConsecutivoR" maxlength="15"
									name="filtroPT.siniestroConsecutivoReporte" cssStyle="width: 100%;"
									cssClass="floatLeft numeric setNew txtfield"></s:textfield>
							</div>
							<div class="floatLeft" style="height: 30px; width: 5%; margin-top:3%;">
								<label style="margin-left: 10%; width: 10%;" class="floatLeft">
									<b>-</b> </label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 20%;">
								<s:textfield id="nsAnio" name="filtroPT.siniestroAnioReporte"
									cssStyle="width: 100%;" maxlength="4"
									cssClass="floatLeft numeric setNew txtfield" ></s:textfield>
							</div>
						</div>
            	
            	</td>
            	
            	<td width="300px">
            		<div style="width: 100%;" class="floatLeft">
            				<div class="floatLeft" style="height: 30px; width: 80%;">
								<label style="margin-left: 5%; margin-top:7%; width: 80%;" class="floatLeft">
								<s:text name="midas.siniestros.indemnizacion.perdidatotal.numreporte" />: </label>
							</div>
						
							<div class="floatLeft" style="height: 30px; width: 10%;">
							</div>
							
							<div class="floatLeft" style="height: 30px; width: 33%;">
								<s:textfield id="nrOficina" maxlength="5"
									name="filtroPT.reporteClaveOficina" cssStyle="width:100%;"
									cssClass="floatLeft setNew txtfield" ></s:textfield>
							</div>
							<div class="floatLeft" style="height: 30px; width: 5%; margin-top:3%; ">
								<label style="margin-left: 10%; width: 10%; " class="floatLeft">
										<span>-</span> 
								</label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 34%;">
								<s:textfield id="nrConsecutivoR" maxlength="15"
									name="filtroPT.reporteConsecutivoReporte" cssStyle="width: 100%;"
									cssClass="floatLeft numeric setNew txtfield"></s:textfield>
							</div>
							<div class="floatLeft" style="height: 30px; width: 5%; margin-top:3%;">
								<label style="margin-left: 10%; width: 10%;" class="floatLeft">
									<span>-</span> 
								</label>
							</div>
							<div class="floatLeft" style="height: 30px; width: 20%;">
								<s:textfield id="nrAnio" name="filtroPT.reporteAnioReporte"
									cssStyle="width: 100%;" maxlength="4"
									cssClass="floatLeft numeric setNew txtfield" ></s:textfield>
							</div>
						</div>
            	
            	</td>
            	
            	<td>
            		<s:textfield name="filtroPT.numeroPoliza" id="numPoliza_t" cssClass="cajaTextoM2 w150" maxlength="20"
	            		label="%{getText('midas.siniestros.indemnizacion.perdidatotal.numpoliza')}" disabled="false"  />
            	</td>
            	
            	<td>
            		<s:textfield name="filtroPT.numeroSerie" id="numSerie_t" cssClass="cajaTextoM2 w150" maxlength="20"
	            		label="%{getText('midas.siniestros.indemnizacion.perdidatotal.numserie')}" disabled="false" />
            	</td>
            	
            </tr>
            <tr>
                <td>
	            	<s:textfield name="filtroPT.numeroValuacion" id="numValuacion_t" cssClass="cajaTextoM2 w150 jQnumeric jQrestrict" 
	            		label="%{getText('midas.siniestros.indemnizacion.perdidatotal.numvaluacion')}" disabled="false" maxlength="15"/>
            	</td>
           		
           		<td  align="left"> <sj:datepicker name="filtroPT.fechaSiniestro"
					id="txtFechaSiniestro" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.siniestros.indemnizacion.perdidatotal.fechasiniestro')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
				
				<td colspan="2">
					<s:select list="etapasPerdidaTotal" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroPT.etapa" id="etapa_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.indemnizacion.perdidatotal.etapa')}"/>
            	</td>
            	
            	<td colspan="2">
					<s:select list="indemnizacionCoberturas" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroPT.cobertura" id="cobertura_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.indemnizacion.perdidatotal.cobertura')}"/>
            	</td>
            	
            </tr>
            
            <tr>
            	<td>
	            	<s:textfield name="filtroPT.numPaseAtencion" id="numPaseAtencion_t" cssClass="cajaTextoM2 w150" 
	            		label="%{getText('midas.siniestros.indemnizacion.perdidatotal.numpaseatencion')}" disabled="false" maxlength="20"/>
            	</td>
				
				<td>
					<s:select list="estatusIndemnizacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroPT.estatusPerdidaTotal" id="estatusPT_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.indemnizacion.perdidatotal.estatuspt')}"/>
            	</td>
            	
            	<td colspan="4">
					<s:select list="estatusIndemnizacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="filtroPT.estatusIndemnizacion" id="estatusIndemnizacion_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.indemnizacion.perdidatotal.estatusindemnizacion')}"/>
            	</td>
            </tr>
            
            <tr>		
					<td colspan="6">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="buscarPerdidasTotales(false);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      
    <BR/>
	<BR/>
      
	<div id="tituloListado" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.indemnizacion.perdidatotal.listado" /></div>	
	<div id="perdidaTotalGridContainer" style="display:none;">
		<div id="listadoPerdidaTotalGrid" style="width:98%;height:200px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
	
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Perdida Total' title='Perdida Total' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			
		</td>
	</tr>
</table>	

</s:form>


<script src="<s:url value='/js/siniestros/indemnizacion/perdidaTotal.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/perdidaTotalCFDI.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/autorizacionPT.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/bajaPlacas.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/determinarPT.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/recepcionDocumentosSiniestro.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/cancelarIndemnizacion.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/valuacion/ordencompra/ordenesCompra.js'/>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	
});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
