<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>
		<column id="detalle" type="sub_row_grid" width="20" sort="int"></column>
		<column  type="ro"  width="230"  align="center"> <s:text name="midas.siniestros.indemnizacion.entregadoctos.numerofactura" />   </column>
		<column  type="ro"  width="230"  align="center"> <s:text name="midas.siniestros.indemnizacion.entregadoctos.emisorfactura" />   </column>
		<column  type="ro"  width="230"  align="center"> <s:text name="midas.siniestros.indemnizacion.entregadoctos.receptorfactura" />   </column>
		<column  type="img"	width="30" sort="na" align="center"></column>
	</head>
	<s:iterator value="facturas" status="row">
		<row id="<s:property value="id"/>">
			<cell>	    	
				<![CDATA[					    	
				<row>					
				</row>]]>
		    </cell>	
			<cell><s:property value="numeroFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="emisorFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="receptorFactura" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^<s:text name="midas.siniestros.indemnizacion.entregadoctos.eliminarfactura"/>^javascript:eliminarFactura(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			
		</row>
	</s:iterator>
	
</rows>