<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css"> 

<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/siniestros/indemnizacion/formatoFechasPT.js'/>"></script>

<style type="text/css">

.subrayado {
	width:               100px;
	font-family:         Verdana, Arial, Helvetica, sans-serif;
	font-size:           7pt;
	z-index:             1;
    border-bottom-style: solid !important;
    border-bottom-width: 1px !important;
    border-bottom-color: black !important; 
    border-top:          none !important; 
    border-right: :      none !important; 
    border-left: :       none !important; 
}

</style>
<script type="text/javascript">
	var guardarPrimasPendientesPath = '<s:url action="guardarPrimasPendientes" namespace="/siniestros/indemnizacion/cartas"/>';
	var cerrarFormatoFechasPTPath   = '<s:url action="mostrarCatalogo" namespace="/siniestros/indemnizacion/perdidatotal"/>';
	var imprimirFormatoFechasPTPath = '<s:url action="imprimirFormatoFechasPT" namespace="/siniestros/indemnizacion/cartas"/>';
</script>

<s:form id="formatoFechaForm">
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.formatoFechasPT.title"/>	
	</div>	
	<div class="titulo" style="width: 98%; text-align: center !important; color:rgb(128,213,74) !important;">
		<s:text name="midas.formatoFechasPT.title"/>	
	</div>
	<table id="agregar" border="0">
		<s:hidden name="fechasPerdidaTotal.reporteCabina.siniestroCabina.id" id="h_idSiniestroCabina"></s:hidden>
		<s:hidden name="fechasPerdidaTotal.indemnizacion.id" id="h_idIndemnizacion"></s:hidden>
		<s:hidden name="fechasPerdidaTotal.idCartaSiniestro" id="h_idCartaSiniestro"></s:hidden>
		<s:hidden name="fechasPerdidaTotal.tipo" id="h_tipo"></s:hidden>
		<s:hidden name="idOrdenCompra" id="h_idOrdenCompra"></s:hidden>
		<s:hidden name="fechasPerdidaTotal.reporteCabina.siniestroCabina.claveOficina" id="h_claveOficina"></s:hidden>
		<s:hidden name="fechasPerdidaTotal.reporteCabina.siniestroCabina.consecutivoReporte" id="h_consecutivoReporte"></s:hidden>
		<s:hidden name="fechasPerdidaTotal.reporteCabina.siniestroCabina.anioReporte" id="h_anioReporte"></s:hidden>
		<s:hidden name="consulta" id="h_consulta"></s:hidden>
		<s:hidden name="idIndemnizacion" id ="idIndemnizacion"/>
		<!-- AIGG INDEMNIZACION -->
		<s:hidden name="tipoSiniestro" id="tipoSiniestro"></s:hidden>
		<s:hidden name="esFiniquitoAsegurado" id="esFiniquitoAsegurado"></s:hidden>
		
		
		<tbody>
			<tr>
				<td>	
					<s:textfield id="txt_siniestro"
								 cssClass="subrayado" 
								 label="%{getText('midas.formatoFechasPT.siniestro')}"
								 labelposition="top" 
								 readonly="true"
								 value="%{fechasPerdidaTotal.reporteCabina.siniestroCabina.numeroSiniestro}"/> 	
				</td>
				<td>
					<s:textfield id="txt_fechaSiniestro"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.fechaSiniestro"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.reporteCabina.fechaOcurrido"/>  	
				</td>
				<td>
					<s:textfield id="txt_fechaRecepcionDocumentos"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.recepcionDocumentos"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaRecepcionDoc"/>  	
				</td>
				<td>
					<s:textfield id="txt_fechaAutorizacionSubdirectorSiniestrosAutos"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.autorizacionSubdirectorSiniestrosAutos"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaAutorizacionSubSiniestro"/>  	
				</td>
			</tr>
			<tr>
				<td>
					<s:textfield id="txt_fechaRecibioAreaOperaciones"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.recibioAreaOperaciones"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaRecepOperaciones"/>  	
				</td>
				<td>
					<s:textfield id="txt_fechaLiquidacionEgresos"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.liquidacionEgresos"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaLiquidacionEgresos"/>  	
				</td>
				<td>
					<s:textfield id="txt_fechaAutorizacionGerenteOperacionesIndemnizacion"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.autorizacionGerenteOperacionesIndemnizacion"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaAutorizacionOperaciones"/>  	
				</td>
				<td>
					<s:textfield id="txt_fechaRecepcionFinanzas"
								 cssClass="subrayado"
								 key="midas.formatoFechasPT.recepcionFinanzas"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaRecepcionFinanzas"/>  	
				</td>
			</tr>
			<tr>
			    <td>
					<s:textfield id="txt_fechaRecepcionChequeOperaciones"
								 cssClass="subrayado" 
								 key="midas.formatoFechasPT.recepcionChequeOperaciones"
								 labelposition="top"  
								 size="10" readonly="true"
								 name="fechasPerdidaTotal.fechaRecepcionCheque"/>  	
				</td>
				<td>
					<s:textfield id="txt_fechaEntregadoAseguradoTercero"
								 cssClass="subrayado"
								 key="midas.formatoFechasPT.entregadoAseguradoTercero"
								 labelposition="top"  
								 size="10" readonly="true" 
								 name="fechasPerdidaTotal.fechaEntregado"/>  	
				</td>
				<td colspan="2">&ensp;</td>
			</tr>
		</tbody>
	</table>
	<div class="titulo" style="width: 98%; text-align: center; color:rgb(128,213,74) !important;">
		<s:text name="midas.formatoFechasPT.primasPendientes"/>	
	</div>
	<br/>	
	<br>
	<table id="agregar" border="0">
		<tbody>
			<tr>
				<td width="30%">				
					<s:textfield id="txt_importePrimaPendiente"
								 cssClass="txtfield setDisabled formatCurrency" cssStyle="width: 200px;"
								 key="midas.formatoFechasPT.importePrimaPendiente"  
								 labelposition="left" readonly="true"
								 name="fechasPerdidaTotal.importePrimaPend"
								 onkeypress="return soloNumeros(this, event, false)"/> 	
				</td>
				<td width="30%">
					<s:textfield id="txt_revisoAreaFinanzas"
								 cssClass="txtfield" cssStyle="width: 200px;"
								 key="midas.formatoFechasPT.revisoAreaFinanzas"
								 labelposition="left"   readonly="true"
								 name="fechasPerdidaTotal.usuarioFinanzas"
								 onkeypress="return soloLetras(this, event, false)"/>  	
				</td>
				<td width="40%">&ensp;</td>
			</tr>
			<tr>
				<td>
					<s:radio  name="fechasPerdidaTotal.tipoPerdida"  cssClass="setDisabled"
							  list="#{'PT':'P\u00e9rdida Total','RT':'Robo Total'}"  />	
				</td>
				<td>
					<s:radio  name="fechasPerdidaTotal.investigacion"  cssClass="setDisabled"
							  key="midas.formatoFechasPT.investigacion" labelposition="left"
							  list="#{'1':'S\u00ED','0':'No'}"  />	
				</td>
				<td>&ensp;</td>
			</tr>
			<tr>
			    <td colspan="3">
			    	<s:textarea id="ta_comentariosAdicionales" 
				    			cssClass="textarea setDisabled" 
				    			key="midas.formatoFechasPT.comentariosAdicionales" 
				    			labelposition="left"
				    			name="fechasPerdidaTotal.comentarios" 
								cols="160"
								rows="15"
								maxlength="7000"
								onkeypress="return soloAlfanumericos(this ,event ,false);"/>
				</td>
			</tr>
		</tbody>
	</table>
	<table  style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_regresar">
					<a href="javascript: void(0);" onclick="if(confirm('Se perder\u00E1n los cambios que no hayan sido guardados. Desea continuar?')){javascript: cerrarFormatoFechasPT();}"> 
					<s:text name="midas.boton.cerrar" /> </a>
				</div>
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_imprimir">
					<a href="javascript: void(0);" onclick="javascript: imprimirFormatoFechasPT();"> 
					<s:text name="midas.boton.imprimir" /> </a>
				</div>			
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar">
					<a href="javascript: void(0);" onclick="javascript: guardarPrimasPendientes();"> 
					<s:text name="midas.boton.guardar" /> </a>
				</div>		
			</td>							
		</tr>
	</table>
</s:form>
<script>
jQuery(document).ready(function(){	
	jQuery('#txt_importePrimaPendiente').focus();
	initCurrencyFormatOnTxtInput();
});
</script>