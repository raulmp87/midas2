<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<sj:head/>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_sub_row.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
	
div.ui-datepicker {
	font-size: 10px;
}

table tr td div span label {
    color:black;
    font-weight: normal;
    text-align: left;
}
</style>

<script type="text/javascript">
var listarEndososPath = '<s:url action="listarEndosos" namespace="/siniestros/indemnizacion/cartas"/>';
var listarFacturasPath = '<s:url action="listarFacturas" namespace="/siniestros/indemnizacion/cartas"/>';
var imprimirEntregaDocumentosPath = '<s:url action="imprimirEntregaDocumentos" namespace="/siniestros/indemnizacion/cartas"/>';
</script>

<s:form id="entregaDocumentosForm" action="imprimirEntregaDocumentos" namespace="/siniestros/indemnizacion/cartas" name="entregaDocumentosForm">
	<s:hidden id="siniestroId" name="siniestroId"/>
	<s:hidden id="tipoCarta" name="tipoCarta"/>
	<s:hidden id="idCartaEntrega" name="cartaSiniestro.id" />
	<s:hidden id="esEliminarEndoso" name="esEliminarEndoso" />
	<s:hidden id="listarEndososEliminar" name="stringListaEndososEliminar" />
	<s:hidden id="idIndemnizacion" name="idIndemnizacion"/>
	<s:hidden id="informacionCompleta" name="informacionCompleta" />
	<s:hidden id="idFactura" name="factura.id" />
	<s:hidden id="numeroFactura_h" name="factura.numeroFactura" />
	<s:hidden id="emisorFactura_h" name="factura.emisorFactura" />
	<s:hidden id="receptorFactura_h" name="factura.receptorFactura" />
	
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	            <tbody>
	                  <tr>
						<td class="titulo" colspan="6">
							<div id="tituloRobo" class="robototal" style="display:none;">
								<s:text name="midas.siniestros.indemnizacion.entregadoctos.titulorobo" />
							</div> 
							<div id="tituloPerdida" class="perdidatotal" style="display:none;">
								<s:text name="midas.siniestros.indemnizacion.entregadoctos.tituloperdida" />
							</div>
	                    </td>
	                  </tr>
					  
					  <s:if test="tipoCarta == \"BPRT\"">
					  	  
<!-- 						  <tr> -->
<%-- 							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.originalfacturanum" /> </th> --%>
<%-- 							<td style="width: 20%;"><s:textfield name="cartaSiniestro.numeroFactura" cssClass="cajaTexto w170 alphaextra" maxlength="100"></s:textfield></td> --%>
							
<%-- 							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.originalfacturadetalle" /></th> --%>
<%-- 							<td style="width: 15%;"><s:textfield name="cartaSiniestro.detalleFactura"  cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td> --%>
							
<%-- 							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.originalfacturanombre" /> </th> --%>
<%-- 							<td style="width: 15%;"><s:textfield name="cartaSiniestro.nombreFactura" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td> --%>
<!-- 						  </tr> -->
						  
						  <tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.bajaplacasconstancia" /> </th>
							<td style="width: 20%;"><s:textfield name="cartaSiniestro.constanciaBajaPlacas" cssClass="cajaTexto w170 alphaextra" onkeypress="return soloNumeros(this, event, false)" maxlength="100"></s:textfield></td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.bajaplacasnumfolio" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.folioBajaPlacas" onkeypress="return soloNumeros(this, event, false)"  cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
							
							<th align="right"></th>
							<td style="width: 15%;"></td>
						  </tr>
						  
						  <tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.altatenencia" /> </th>
							<td style="width: 20%;"><s:textfield name="cartaSiniestro.constanciaPagoTenencia" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTexto w170 alphaextra" maxlength="100"></s:textfield></td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.altatenencianumfolio" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.folioAltaTenencia" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTexto w170 alphaextra" maxlength="10"></s:textfield></td>
							
							<th align="right"></th>
							<td style="width: 15%;"></td>
						  </tr>
						  
						  <tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.tenenciadel" /> </th>
							<td style="width: 20%;"><s:textfield name="cartaSiniestro.fechaTenenciaInicial" cssClass="cajaTexto w170 alphaextra" onkeypress="return soloNumeros(this, event, false, false)" maxlength="4"></s:textfield></td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.tenenciaal" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.fechaTenenciaFin"  cssClass="cajaTexto w170 alphaextra" onkeypress="return soloNumeros(this, event, false, false)" maxlength="4"></s:textfield></td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.tenenciaestado" /> </th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.estadoTenencia" cssClass="cajaTexto w170 alphaextra" maxlength="100"></s:textfield></td>
						  </tr>
						  
						  <tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.numaveriguacionrobo" /> </th>
							<td style="width: 20%;"><s:textfield name="cartaSiniestro.numeroAveriguacion" onkeypress="return soloNumeros(this, event, false, false)" cssClass="cajaTexto w170 alphaextra" maxlength="100"></s:textfield></td>
							
							<th align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.numerollaves" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.llavesEntregadas"  cssClass="cajaTexto w170 alphaextra" onkeypress="return soloNumeros(this, event, false, false)" maxlength="3"></s:textfield></td>
							
							<th align="right"></th>
							<td style="width: 15%;"></td>
						  </tr>
						  
						  <tr>
							<td colspan="6">
						<table id="agregar" border="0">
							<tbody>
							<tr>
								<td class="titulo" colspan="6">
									<div id="tituloFacturas" >
										<s:text name="midas.siniestros.indemnizacion.entregadoctos.titulofacturas" />
									</div> 
			                    </td>
			                </tr>
							<tr>
								<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.numerofactura" /></th>
								<td style="width: 20%;"><s:textfield id="numeroFactura" value="%{factura.numeroFactura}" cssClass="cajaTexto w170 alphaextra" maxlength="100"></s:textfield></td>
								
								<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.emisorfactura" /> </th>
								<td style="width: 15%;"><s:textfield id="emisorFactura" value="%{factura.emisorFactura}" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
								
								<th></th>
								<td></td>
							</tr>
							<tr>
								<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.receptorfactura" /></th>
								<td style="width: 15%;"><s:textfield id="receptorFactura" value="%{factura.receptorFactura}" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
								<td colspan="4" style="width: 15%;">
								<div id="btn_agregar" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:agregarFactura();"> 
									<s:text name="midas.boton.agregar" /> </a>
								</div>
								<div id="btn_nuevo" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:limpiarFacturaTxt();"> 
									<s:text name="midas.boton.nuevo" /> </a>
								</div>
							</td>
							</tr>
							<tr>
								<td class="titulo" colspan="6">
									<div id="tituloEndosos" >
										<s:text name="midas.siniestros.indemnizacion.entregadoctos.tituloendosos" />
									</div> 
			                    </td>
			                </tr>
								<tr>
							<th style="width: 1%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.endoso" /> </th>
							<td style="width: 5%;"><s:textfield name="nombreEndoso" id="nombreEndoso" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
							
							<td colspan="4" style="width: 15%;">
								<div id="btn_agregar" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:agregarEndoso();"> 
									<s:text name="midas.boton.agregar" /> </a>
								</div>
								<div id="btn_nuevo" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:limpiarEndosoTxt();"> 
									<s:text name="midas.boton.nuevo" /> </a>
								</div>
							</td>
						  </tr>
						  
						  <tr>
						  	<th colspan="6" style="width: 1%;color:red;display:none" align="right" id="avisoguardar"><s:text name="midas.siniestros.indemnizacion.entregadoctos.avisoguardar" /> </th>
						  </tr>
						  
						  <tr>
						  	<td colspan="6">
							  	<table border="0">
									<tbody>
										<tr>
											<td >
												<br/>
												<div id="facturasEntregaDoctosContainer">
													<div id="facturasEntregaDocumentosGrid" style="width:98%;height:150px;"></div>
													<div id="pagingArea"></div><div id="infoArea"></div>
												</div>
												<br/>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
							</tbody>
						</table>
							</td>
						</tr>
					  </s:if>
					  
					  <s:elseif test="tipoCarta == \"BPPT\"">
						<tr>
							<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.bajaplacasnumfolio" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.folioBajaPlacas"  cssClass="cajaTexto w170 alphaextra jQnumeric jQrestrict" maxlength="200"></s:textfield></td>
						
							<th style="padding-left: 5%;"><s:text name="midas.siniestros.indemnizacion.entregadoctos.constanciapagotenencia" /></th>
							<td style="width: 15%;"><s:textfield name="cartaSiniestro.constanciaPagoTenencia"  cssClass="cajaTexto w170 alphaextra jQnumeric jQrestrict" maxlength="100"></s:textfield></td>
					
							<th style="padding-left: 5%;"></th>
							<td style="width: 15%;"></td>
						</tr>
					
						<tr>
							<td colspan="6">
						<table id="agregar" border="0">
							<tbody>
							<tr>
								<td class="titulo" colspan="6">
									<div id="tituloFacturas" >
										<s:text name="midas.siniestros.indemnizacion.entregadoctos.titulofacturas" />
									</div> 
			                    </td>
			                </tr>
							<tr>
								<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.numerofactura" /></th>
								<td style="width: 20%;"><s:textfield id="numeroFactura" value="%{factura.numeroFactura}" cssClass="cajaTexto w170 alphaextra" maxlength="100"></s:textfield></td>
								
								<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.emisorfactura" /> </th>
								<td style="width: 15%;"><s:textfield id="emisorFactura" value="%{factura.emisorFactura}" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
								
								<th></th>
								<td></td>
							</tr>
							<tr>
								<th style="width: 12%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.receptorfactura" /></th>
								<td style="width: 15%;"><s:textfield id="receptorFactura" value="%{factura.receptorFactura}" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
								<td colspan="4" style="width: 15%;">
								<div id="btn_agregar" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:agregarFactura();"> 
									<s:text name="midas.boton.agregar" /> </a>
								</div>
								<div id="btn_nuevo" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:limpiarFacturaTxt();"> 
									<s:text name="midas.boton.nuevo" /> </a>
								</div>
							</td>
							</tr>
							<tr>
								<td class="titulo" colspan="6">
									<div id="tituloEndosos" >
										<s:text name="midas.siniestros.indemnizacion.entregadoctos.tituloendosos" />
									</div> 
			                    </td>
			                </tr>
								<tr>
							<th style="width: 1%;" align="right"><s:text name="midas.siniestros.indemnizacion.entregadoctos.endoso" /> </th>
							<td style="width: 5%;"><s:textfield name="nombreEndoso" id="nombreEndoso" cssClass="cajaTexto w170 alphaextra" maxlength="200"></s:textfield></td>
							
							<td colspan="4" style="width: 15%;">
								<div id="btn_agregar" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:agregarEndoso();"> 
									<s:text name="midas.boton.agregar" /> </a>
								</div>
								<div id="btn_nuevo" class="btn_back w80" style="display:inline; float:left; position:relative;">
									<a href="javascript: void(0);" onclick="javascript:limpiarEndosoTxt();"> 
									<s:text name="midas.boton.nuevo" /> </a>
								</div>
							</td>
						  </tr>
						  
						  <tr>
						  	<th colspan="6" style="width: 1%;color:red;display:none" align="right" id="avisoguardar"><s:text name="midas.siniestros.indemnizacion.entregadoctos.avisoguardar" /> </th>
						  </tr>
						  
						  <tr>
						  	<td colspan="6">
							  	<table border="0">
									<tbody>
										<tr>
											<td >
												<br/>
												<div id="facturasEntregaDoctosContainer">
													<div id="facturasEntregaDocumentosGrid" style="width:98%;height:150px;"></div>
													<div id="pagingArea"></div><div id="infoArea"></div>
												</div>
												<br/>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
							</tbody>
						</table>
							</td>
						</tr>
					
					  </s:elseif>
	            </tbody>
	</table>
	<br/>
	<div id="btn_cerrar" class="btn_back w80" style="display: inline; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="if(confirm('Se perderan los cambios que no hayan sido guardados. \u00BFCerrar?')){javascript:cerrarVentanaEntregaDocumentos();}"> 
		<s:text name="midas.boton.cerrar" /> </a>
	</div>
	<div id="btn_imprimir" class="btn_back w80 validatipocarta" style="display: none; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="imprimirEntregaDocumentos();"> 
		<s:text name="midas.boton.imprimir" /> </a>
	</div>
	<div id="btn_guardar" class="btn_back w80 validatipocarta" style="display: none; float: right;position: relative;">
		<a href="javascript: void(0);" onclick="javascript:guardarEntregaDocumentos();"> 
		<s:text name="midas.boton.guardar" /> </a>
	</div>
	<div id="indicador"></div>
			<div id="central_indicator" class="sh2" style="display: none;">
				<img id="img_indicator" name="img_indicator"
					src="/MidasWeb/img/as2.gif" alt="Afirme" />
	</div>
	<div id="avisoimprimir" style="display: none; float: right;position: relative; color: red; font-size: 7.5pt;">
		<s:text name="Debe guardar para poder imprimir"/>
	</div>
	
	<br/>
</s:form>

<script type="text/javascript">
jQuery(document).ready(
	function(){
	 	configurarEntregaDocumentos();
 	}
 );
</script>

<script type="text/javascript" src="<s:url value='/js/siniestros/indemnizacion/entregaDocumentos.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>