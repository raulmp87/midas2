<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<!-- ordenPago.comentarios -->
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        <column id="numeroOrdenPago" type="ro" width="60" sort="int" >No. Orden Compra</column>	
        <column id="numeroSiniestro" type="ro" width="270" sort="int" >Siniestro</column>
        <column id="comentarios" type="ro" width="190" hidden="true" sort="int" >Observaciones</column>
        <s:if test="proveedorEsCompania == \"s\"" >
        <column id="siniestroTercero" type="ro" width="70" sort="str" >Siniestro Tercero</column>
        </s:if>
        <column id="subtotal" type="ro" width="5" sort="int" hidden="true"></column>
        <column id="deducible" type="ro" width="5" sort="int" hidden="true"></column>
        <column id="descuentos" type="ro" width="5" sort="int" hidden="true"></column>
        <column id="iva" type="ro" width="5" sort="int" hidden="true"></column>
        <column id="ivaRet" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="isr" type="ro" width="10" sort="int" hidden="true"></column>	
        <column id="total" type="ron" format="$0,000.00" width="80" sort="int">total</column>
        <column id="salvamento " type="ro" width="10" sort="int" hidden="true"></column>
 	
  	</head>      
   <s:iterator value="listaOrdenesPago" status="row">
		<row id="<s:property value="ordenPago.id"/>">
		  	<cell><s:property value="ordenPago.ordenCompra.id" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="siniestro.numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>				
			<cell><s:property value="ordenPago.comentarios" escapeHtml="true" escapeXml="true"/></cell>
			<s:if test="%{numeroFactura == 0}" >
				<cell><s:property value="ordenPago.ordenCompra.cartaPago.siniestroTercero" escapeHtml="true" escapeXml="true"/></cell>
			</s:if>
			<cell><s:property value="desgloseConceptos.subTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.deduciblesTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.descuentosTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.ivaTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.ivaRetenidoTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.isrTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.totalesPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="desgloseConceptos.salvamentoTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>	
		</row>
	</s:iterator>	
</rows>
   
