<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="oficina"           type="ro" width="140"  sort="str" ><s:text name="%{'midas.liquidaciones.busqueda.oficina'}"/></column>	
        <column id="liquidacion"       type="ro" width="120" sort="int" ><s:text name="%{'midas.liquidaciones.busqueda.noLiquidacion'}"/></column>
        <column id="estatus" 		   type="ro" width="135" sort="str" ><s:text name="%{'midas.liquidaciones.busqueda.estatus'}"/> </column>
		<column id="tipoLiquidacion"   type="ro" width="140" sort="str"><s:text name="%{'midas.liquidaciones.busqueda.tipoLiquidacion'}"/> </column>
		<column id="tipoOperacion"     type="ro" width="150"  sort="str"><s:text name="%{'midas.liquidaciones.busqueda.tipoOperacion'}"/></column>	
		<column id="noProveedor"       type="ro" width="90"  sort="int"><s:text name="%{'midas.liquidaciones.busqueda.noProveedor'}"/> </column>
		<column id="proveedor"         type="ro" width="180" sort="str"><s:text name="%{'midas.liquidaciones.busqueda.proveedor'}"/> </column>
		<column id="siniestro"         type="ro" width="80"  sort="str"><s:text name="%{'midas.liquidaciones.busqueda.noSiniestro'}"/> </column>
		<column id="factura"           type="ro" width="80"  sort="str"><s:text name="%{'midas.liquidaciones.busqueda.noFactura'}"/> </column>
		<column id="solicitadoPor"     type="ro" width="180" sort="str"><s:text name="%{'midas.liquidaciones.busqueda.solicitadoPor'}"/> </column>
		<column id="totalPagar"        type="ro" width="100" sort="int"><s:text name="%{'midas.liquidaciones.busqueda.neto.pagar'}"/> </column>
		<column id="fechaSolicitud"    type="ro" width="120" sort="date"><s:text name="%{'midas.liquidaciones.busqueda.fechaSolicitud'}"/> </column>
		<column id="fechaAutorizacion" type="ro" width="100" sort="date"><s:text name="%{'midas.liquidaciones.busqueda.fechaAutorizacion'}"/> </column>
		<column id="fechaElab" 		   type="ro" width="155" sort="date"><s:text name="%{'midas.liquidaciones.busqueda.fechaElaboracion'}"/> </column>
		<column id="noSolCheque"       type="ro" width="135" sort="int"><s:text name="%{'midas.liquidaciones.busqueda.noSolicitudCheque'}"/> </column>
		<column id="noCheque" 		   type="ro" width="135" sort="int"><s:text name="%{'midas.liquidaciones.busqueda.noChequeReferencia'}"/> </column>
		<column id="autorizadaPor"     type="ro" width="180" sort="str"><s:text name="%{'midas.liquidaciones.busqueda.autorizadoPor'}"/> </column>
		<column id="consultar"         type="img" width="70" sort="na" align="center">Acciones</column>
	  	</head>
	<s:iterator value="listadoLiquidaciones" status="row">
		<row> 
			<cell><s:property value="oficina" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noLiquidacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoLiquidacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoOperacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noProveedor" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="proveedor" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="siniestro" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="factura" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="solicitadaPor" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="%{getText('struts.money.format',{totalAPagar})}" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaSolicitud" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaDeAutorizacion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="fechaDeElaboracion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noSolCheque" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="noCheque" escapeHtml="false" escapeXml="true" /></cell> 
			<cell><s:property value="autorizadoPor" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript:consultarLiquidacion(<s:property value="idLiquidacion" escapeHtml="false" escapeXml="true" />,true,"AUT")^_self</cell>
		</row>
	</s:iterator>	
</rows>
   
