<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacion.js'/>"></script>

<s:hidden id="idLiquidacion" name="idLiqSiniestro"/>
<s:hidden id="envioCancelacion" name="envioCancelacion"/>
<s:hidden id="origenLiquidacion" name="liquidacionSiniestro.origenLiquidacion"/>

<div class="titulo" style="margin-left: 2%"><s:text name="midas.siniestros.liquidacion.tipoCancelacion"/></div>
	<br/>
<table id="agregar">

<s:if test='%{ liquidacionSiniestro.origenLiquidacion == "PVD" }'>
	<tr><td><s:radio name="tipoCancelacion"
			list="#{'1':' Liquidaci\u00f3n ','3':' Liquidaci\u00f3n - Orden de Pago - Orden de Compra '}"
			id="tipoCancelacion"  value="1"/>
	</td></tr>
</s:if>
<s:else>
	<tr><td><s:radio name="tipoCancelacion"
			list="#{'1':' Liquidaci\u00f3n ','2':' Liquidaci\u00f3n - Orden de Pago ','3':' Liquidaci\u00f3n - Orden de Pago - Orden de Compra '}"
			id="tipoCancelacion"  value="1"/> 
	</td></tr>
</s:else>
</table>	
<!-- <td><s:textfield name="liquidacionSiniestro.origenLiquidacion"  ></s:textfield></td> -->

<div class="btn_back w140" style="display: inline; float: right;">
				 <a href="javascript: void(0);" onclick="cerrarVentanaCancelacionLiq(false);">
				 <s:text name="midas.boton.cerrar" /> </a>
			</div>

<div id="botonCancelar" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
	<a href="javascript: void(0);" onclick="confirmarCancelacion();">
		<s:text name="midas.siniestros.liquidacion.cancelarCancelacion" /> 
	</a>
</div>
	
<script type="text/javascript">
    jQuery(document).ready(
		function(){
			if (jQuery('#envioCancelacion').val() == 1) {
				cerrarVentanaCancelacionLiq(true);
			}
		}
	);
</script>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>