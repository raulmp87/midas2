<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>


<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacion.js'/>"></script> 


<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>


	
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">


<style type="text/css">
.label{
    color: #000000;
    font-weight: normal;
    text-align: left;
    font-size:7pt;
    width: 88px !important;
}
</style>

								


<div class="titulo" style="width: 98%;">Detalle Nota de Credito</div>
		
<div id="indicador"></div>
<div id="detalleNotaCreditoGrid"  class="dataGridConfigurationClass" style="width:98%;height:200px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>

<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>	
			<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_cerrar">
				<a href="javascript: void(0);" onclick="cerrarDetalleNota();"> 
				<s:text name="midas.boton.cerrar" /> </a>
			</div>	
		</td>
	</tr>
</table>

	

<script type="text/javascript">
jQuery(document).ready(
		function(){
			initGridDetalleNotaCredito(<s:property value="idFactura"/>,"<s:property value="tipoElemento"/>");	
		}						 
		
);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
