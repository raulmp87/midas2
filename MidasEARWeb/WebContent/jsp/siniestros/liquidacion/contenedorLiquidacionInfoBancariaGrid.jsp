<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>					
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>

		</beforeInit>
		
		 <column id="id" type="ro"  width="100" align="center"><s:text name="%{'midas.liquidaciones.datos.bancarios.banco.nombre'}"/></column>
         <column id="numeroCuenta" type="ro"  width="200"  sort="str" align="center"><s:text name="%{'midas.liquidaciones.datos.bancarios.numero.cuenta'}"/></column>
         <column id="clabe" type="ro"  width="200"  sort="str" align="center"><s:text name="%{'midas.liquidaciones.datos.bancarios.numero.clabe'}"/></column>
         <column id="fechaCambioEstatus" type="ro"  width="*" sort="str" align="center"><s:text name="%{'midas.liquidaciones.datos.bancarios.fecha.cambio'}"/></column>
		 <column id="estatus"  type="img" width="100" sort="na" align="center" ><s:text name="%{'midas.liquidaciones.datos.bancarios.estatus'}"/></column> 
	</head>			
	<s:iterator value="listInfoBancaria" status="row">
		<row>
			<cell ><s:property value="bankName" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="numeroCuenta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="clabe" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="fechaModEstatus" escapeHtml="false" escapeXml="true"/></cell>	
			<s:if test="estatus == 1" >	
				<cell>/MidasWeb/img/ico_green.gif^Activo^javascript: return null;^_self</cell>
			</s:if>
			<s:else>
				<cell>/MidasWeb/img/ico_red.gif^Activo^javascript: return null;^_self</cell>
			</s:else>
		</row>
	</s:iterator>	
</rows>
