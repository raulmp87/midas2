<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
  <head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>          
        <column id="selected"       type="ch" width="30"  sort="int"  align="center" ></column>
        <column id="tieneNotasCredito" type="ro" hidden="true" width="20" sort="int" ></column>
        <column id="numero" type="ro" width="150" sort="int" ><s:text name="midas.liquidaciones.nueva.notaCredito.numeroGridExt"/></column>
        <column id="idFactura" type="ro" width="120" sort="int" hidden="true"></column>
        <column id="descripcion" type="ro" width="150" sort="int" >Tipo</column>
        <column id="total" type="ron" width="*" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
        <column id="consultar" type="img" width="100" sort="na" align="center">Acciones</column>
    </head>      
   <s:iterator value="listaNotasCredito" status="row">
    <row id="<s:property value="idFactura"/>">
        <cell>false</cell> 
        <cell><s:property value="tieneNotasCredito" escapeHtml="true" escapeXml="true"/></cell>   
        <cell><s:property value="noFactura" escapeHtml="true" escapeXml="true"/></cell>  
        <cell><s:property value="idFactura" escapeHtml="false" escapeXml="true"/></cell>
        <cell><s:property value="tipo" escapeHtml="false" escapeXml="true"/></cell>              
        <cell><s:property value="montoTotal" escapeHtml="true" escapeXml="true"/></cell>
        <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript:consultarDetalleNotasCredito(<s:property value="idFactura" escapeHtml="false" escapeXml="true"/>,"<s:property value="tipo" escapeHtml="false" escapeXml="true"/>")^_self</cell>
    </row>
  </s:iterator> 
</rows>
   
