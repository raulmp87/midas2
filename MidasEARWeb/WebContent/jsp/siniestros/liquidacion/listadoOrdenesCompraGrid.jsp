<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        
        <column id="noOrdenCompra" type="ro" width="120" sort="int" >No. Orden Compra</column>
        <column id="Subtotal" type="ron" width="120" format="$0,000.00" align="center" sort="int" >Subtotal</column>
        <column id="Deducible" type="ron" width="120" format="$0,000.00" align="center" sort="int" >Deducible</column>
        <column id="Descuentos" type="ron" width="120" format="$0,000.00" align="center" sort="int" >Descuentos</column>
        <column id="IVA" type="ron" width="120" format="$0,000.00" align="center" sort="int" >IVA</column>
        <column id="IVA_RET" type="ron" width="120" format="$0,000.00" align="center" sort="int" >IVA RET</column>
        <column id="ISR" type="ron" width="120" format="$0,000.00" align="center" sort="int" >ISR</column>
        <column id="Total" type="ron" width="120" format="$0,000.00" align="center" sort="int" >Total</column>
        <column id="salvamento" type="ron" width="120" format="$0,000.00" align="center" sort="int" >Importe Salvamento</column>
        
  	</head>      
   <s:iterator value="ordenesCompraDesglose" status="row">
		<row id="<s:property value="idOrdenCompra"/>">
          <cell><s:property value="idDetalleOrdenCompra" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="subTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="deduciblesTotal" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="descuentosTotal" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="iva" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="ivaRetenido" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="isr" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="totalesPorPagar" escapeHtml="true" escapeXml="true"/></cell>
          <cell>0</cell>
          

		</row>
	</s:iterator>	
</rows>
   

 