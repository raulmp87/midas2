<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/liquidacion/parametros/paramAutorizaLiquidacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>


<style type="text/css">
.derecha {
    text-align: right;
}
.centro {
    text-align: center !important;
}

#tablaInterna,#tablaInterna2 {
    font-family: arial;
    font-size: 11px;
     color: #666666;
    font-weight: bold;
}

.error {
	background-color: red;
	opacity: 0.4;
}


table#filtrosM2 td {
    color: #666666;
    font-weight: bold;
}



</style>


<s:form  id="busquedaParamAutLiquidacionForm">
	<s:hidden name="" id="h_noLiquidacion"></s:hidden>
	<div class="titulo"><s:text name="midas.liquidaciones.parametros.autorizacion.tituloBusqueda"/></div>
	<table  id="filtrosM2" width="95%" >
		<tr>
			<td>
				<s:textfield  	id="txt_No_Configuracion" 
							key="midas.liquidaciones.parametros.autorizacion.noConfiguracion"
						 	cssClass="txtfield jQnumeric jQrestrict cajaTextoM2 w130 filtroBusqueda"
						 	name="filtroParametros.numero"/>
			</td>
			
			<td >
				<s:textfield cssClass="txtfield jQalphaextra cajaTextoM2 w160 filtroBusqueda" 
								key="midas.liquidaciones.parametros.autorizacion.nombreConfiguracion"
								name="filtroParametros.nombreConfiguracion" 
								size="15"/>
			</td>
			
			<td >
				<s:textfield  	id="txt_nomUsuarioConf" 
							key="midas.liquidaciones.parametros.autorizacion.nombreUsuario"
						 	cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w130 filtroBusqueda"
						 	name="filtroParametros.nombreUsuarioConfigurador"/>
			</td>
			
			
		</tr>
		<tr>
			
			<td align="left">
				<s:select id="s_tipoPago" 
							key="midas.liquidaciones.parametros.autorizacion.tipoDePago"
							name="filtroParametros.tipoPago"
							headerKey=""  headerValue="%{getText('midas.liquidaciones.parametros.autorizacion.todas')}"
								list="tipoPago" listKey="key" listValue="value"  
					  		cssClass="txtfield cajaTextoM2 w155 filtroBusqueda"/> 
			</td>
			
			<td>
					<s:select id="s_estatus" 
							key="midas.liquidaciones.busqueda.estatus"
							name="filtroParametros.estatus"
							headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
							list="estatus" listKey="key" listValue="value"  
							cssClass="txtfield cajaTextoM2 w155 filtroBusqueda"/> 
			</td>
			
			<td>
				<s:select id="s_origenLiquidacion" 
							key="midas.liquidaciones.parametros.autorizacion.liquidacion"
							name="filtroParametros.liquidacion"
							headerKey=""  headerValue="%{getText('midas.liquidaciones.parametros.autorizacion.todos')}"
								list="origenLiquidacion" listKey="key" listValue="value"  
					  		cssClass="txtfield cajaTextoM2 w155 filtroBusqueda"/> 
			</td>
			<td>
				<s:select id="s_tipoLiquidacion" 
							key="midas.liquidaciones.busqueda.tipoLiquidacion"
							name="filtroParametros.tipoLiquidacion"
							headerKey=""  headerValue="%{getText('midas.liquidaciones.parametros.autorizacion.todos')}"
								list="tipoLiquidacion" listKey="key" listValue="value"  
					  		cssClass="txtfield cajaTextoM2 w155 filtroBusqueda"/> 
			</td>

		</tr>
		<tr>
			<td>
				<s:select id="s_condFechaActivo" 
							key="midas.liquidaciones.parametros.autorizacion.condicion"
							name="filtroParametros.criterioFechaActivo" onchange="onChangeCondicionActivo();"
							headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
								list="criterioComparacion" listKey="key" listValue="value"
					  		cssClass="txtfield cajaTextoM2 w155 filtroBusqueda"/> 

			</td>
			<td>
				<s:checkbox key="midas.liquidaciones.parametros.autorizacion.rangos" onchange="onChangeRangoActivo();"
				id="c_condFechaActivo" name="filtroParametros.rangoFechaActivo" labelposition="left"//>
			</td>
			<td>
				<sj:datepicker name="filtroParametros.fechaActivoIni" changeMonth="true" changeYear="true"
					buttonImage="/MidasWeb/img/b_calendario.gif"
					buttonImageOnly="true" id="dp_fechaActivoIni" maxlength="10" size="18"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					cssClass="cleaneable txtfield filtroBusqueda"
					key="midas.liquidaciones.parametros.autorizacion.fechaActivo">
				</sj:datepicker>
			</td>
			<td>
				<div id="contenedor_fechaActivoFin" style="display:none;">
					<sj:datepicker name="filtroParametros.fechaActivoFin" changeMonth="true" changeYear="true"
						buttonImage="/MidasWeb/img/b_calendario.gif"
						buttonImageOnly="true" id="fechaActivoFin" maxlength="10" size="18"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						cssClass="cleaneable txtfield filtroBusqueda"
						key="midas.liquidaciones.parametros.autorizacion.busqueda.hasta">
						
					</sj:datepicker>
				</div>
			</td>
			
		</tr>
		<tr>
			<td>
				<s:select id="s_condFechaInactivo" 
							key="midas.liquidaciones.parametros.autorizacion.condicion"
							name="filtroParametros.criterioFechaInactivo" onchange="onChangeCondicionInactivo();"
							headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
								list="criterioComparacion" listKey="key" listValue="value"
					  		cssClass="txtfield cajaTextoM2 w155 filtroBusqueda"/> 

			</td>
			<td>
				<s:checkbox key="midas.liquidaciones.parametros.autorizacion.rangos" onchange="onChangeRangoInactivo();"
				id="c_condFechaInactivo" name="filtroParametros.rangoFechaInactivo" labelposition="left"/>
			</td>
			<td>
				<sj:datepicker name="filtroParametros.fechaInactivoIni" changeMonth="true" changeYear="true"
					key="midas.liquidaciones.parametros.autorizacion.fechaInactivo"
					buttonImage="/MidasWeb/img/b_calendario.gif"
					buttonImageOnly="true" id="dp_fechaInactivoIni" maxlength="10" size="18"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					cssClass="cleaneable txtfield filtroBusqueda">
				</sj:datepicker>
			</td>
			<td>
				<div id="contenedor_fechaInactivoFin" style="display:none">
					<sj:datepicker name="filtroParametros.fechaInactivoFin" changeMonth="true" changeYear="true"
						key="midas.liquidaciones.parametros.autorizacion.busqueda.hasta"
						buttonImage="/MidasWeb/img/b_calendario.gif"
						buttonImageOnly="true" id="fechaInactivoFin" maxlength="10" size="18"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						cssClass="cleaneable txtfield filtroBusqueda">
					</sj:datepicker>
				</div>
			</td>
			
		</tr>
		
		<tr>
			<td colspan="3"/>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="b_agregar">
					<a href="javascript: void(0);" onclick="mostrarAlta();"> 
					<s:text name="midas.boton.agregar" /> </a>
				</div>	
				<div class="btn_back w140" style="display: inline; float: right;" >
					<a href="javascript: void(0);" onclick="javascript: limpiarFormularioParamAutLiquidacion();"> 
						<s:text name="midas.boton.limpiar" /> 
						<img border='0px' alt='Limpiar' title='Limpiar' src='/MidasWeb/img/b_borrar.gif'/>
					</a>
				</div>	
				<div class="btn_back w140" style="display: inline; float: right;" id="btn_guardar">
					<a href="javascript: void(0);" onclick="javascript: realizarBusquedaParamAutLiq(true);"> 
						<s:text name="midas.boton.buscar" /> 
						<img border='0px' alt='Buscar' title='Buscar' src='/MidasWeb/img/b_ico_busq.gif'/>
					</a>
				</div>
			</td>
		</tr>
	</table>
	
<br/>
<div id="indicadorSolAutParamLiquidacion"></div>
<div id="solicitudAutLiquidacionGrid"  class="dataGridConfigurationClass" style="width:95%;height:340px;"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br/>
<br/>	

</s:form>









<script type="text/javascript">
    jQuery(document).ready(
		function(){
			initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
		}
	);
</script>
