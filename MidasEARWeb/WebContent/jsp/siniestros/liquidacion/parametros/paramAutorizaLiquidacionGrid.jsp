<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
      <head>
          <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
            <column id="" type="ro"  width="40"	sort="int" ><s:text name="midas.liquidaciones.parametros.autorizacion.noConfiguracion"/></column>
            <column id="" type="ro"  width="*"  sort="str" ><s:text name="midas.liquidaciones.parametros.autorizacion.nombreConfiguracion"/></column>
          	<column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.parametros.autorizacion.usuarioConfigurador"/></column>
          	<column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.parametros.autorizacion.liquidacion"/></column>
            <column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.busqueda.tipoLiquidacion"/></column>
            <column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.parametros.autorizacion.tipoDePago"/></column>
            <column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.parametros.autorizacion.fechaActivo"/></column>
            <column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.parametros.autorizacion.fechaInactivo"/></column>
            <column id="" type="ro"  width="*"  sort="str"><s:text name="midas.liquidaciones.busqueda.estatus"/></column>

          	
            <column id="editar" type="img" width="40" sort="na" align="center">Acciones</column>
			      <column id="consultar" type="img" width="40" sort="na" align="center">#cspan</column>	   
      </head>
      <s:iterator value="resultados">
            <row id="<s:property value="id" escapeHtml="false" escapeXml="true" />">
                <cell><s:property value="numero" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="nombreConfiguracion" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="nombreUsuarioConfigurador" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="liquidacionDesc" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="tipoLiquidacionDesc" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="tipoPagoDesc" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="fechaActivo" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="fechaInactivo" escapeHtml="false" escapeXml="true"/></cell>
                <cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>

                <s:if test="estatus == true">
                  <cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editarParametro(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>		
                </s:if>
                <s:else>
                      <cell>../img/pixel.gif</cell>
                </s:else>

                <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultarParametro(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
            </row>
      </s:iterator>
       
</rows>
