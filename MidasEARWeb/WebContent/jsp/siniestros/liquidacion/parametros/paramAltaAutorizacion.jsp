<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/liquidacion/parametros/paramAutorizaLiquidacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>


<style type="text/css">
.derecha {
    text-align: right;
}
.centro {
    text-align: center !important;
}

#tablaInterna,#tablaInterna2 {
    font-family: arial;
    font-size: 11px;
     color: #666666;
    font-weight: bold;
}

.error {
	background-color: red;
	opacity: 0.4;
}


table#filtrosM2 td {
    color: #666666;
    font-weight: bold;
}

.js_checkEnable {
    color: #666666;
    font-weight: bold;
}

ul {
    list-style-type: none;
}





</style>


<s:form  id="altaParamAutLiquidacionForm">
	<s:hidden name="parametro.id" id="h_noParametro"></s:hidden>
	<s:hidden name="esParametroSoloLectura" id="h_modoConsulta"></s:hidden>
	<s:hidden name="parametro.estatus" id="estatusParam"></s:hidden>
	
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloConfiguracionParametros"/> 
		<s:property value="nombreusuarioActual"/></div>
	<table  id="filtrosM2" width="98%" >
		

		<tr>
			<td>
				<s:textfield  	id="txt_No_Configuracion" 
					 	cssClass="txtfield jQrestrict cajaTextoM2 w130"
					 	disabled = "true"
					 	value="%{parametro.numero}"
					 	key="midas.liquidaciones.parametros.autorizacion.noConfiguracion"/>
			</td>

			<td>
					<s:textfield cssClass="cajaTextoM2 setNew"
								value="%{parametro.fechaCreacion}" 
								size="12"
								maxlength="8"				
								disabled="true"
								key="midas.liquidaciones.parametros.autorizacion.fechaActivo"
								id="txt_fechaActivo"/>
			</td>
			<td>
					<s:textfield cssClass="cajaTextoM2 setNew"
								value="%{parametro.fechaInactivo}" 
								size="12"
								maxlength="8"				
								disabled="true"
								key="midas.liquidaciones.parametros.autorizacion.fechaInactivo"
								id="txt_fechaInactivo"/>
			</td>

			<td>
					<s:select id="s_estatus" 
							name="parametroEstatus"
							list="estatus" listKey="key" listValue="value"  
							key="midas.liquidaciones.busqueda.estatus"
							cssClass="txtfield cajaTextoM2 w155 desabilitable"/> 
			</td>
		</tr>
		<tr>
			<td>
				<s:textfield cssClass="txtfield jQalphabeticExt jQrestrict cajaTextoM2 w160 desabilitable jQrequired" 
								name="parametro.nombreConfiguracion" 
								key="midas.liquidaciones.parametros.autorizacion.nombreConfiguracion"
								size="15"/>
			</td>

			<td>
				<s:select id="s_origenLiquidacion" 
							name="parametro.origenLiquidacion"
							headerKey=""  headerValue="%{getText('midas.liquidaciones.parametros.autorizacion.ambas')}"
								list="origenLiquidacion" listKey="key" listValue="value"  
					  		cssClass="txtfield cajaTextoM2 w155 desabilitable"
					  		key="midas.liquidaciones.parametros.autorizacion.liquidacion"/> 
			</td>
			<td colspan="4" align="left">
					<s:hidden name="parametro.tipoServicio" id="h_tipo_servicio"></s:hidden>
					<table border="0" id="agregar" >
						<tr>
							<td class="label">
								<input id="ch_sparticular" type="checkbox" value="1" onclick="seleccionaCheckButton()" class="desabilitable">
									<s:text name="midas.liquidaciones.busqueda.servicioPrivado"/></input>
							</td>
						</tr>
						<tr>
							<td class="label">
								<input id="ch_spublico" type="checkbox" value="2" onclick="seleccionaCheckButton()" class="desabilitable">
									<s:text name="midas.liquidaciones.busqueda.servicioPublico"/></input>
							</td>
						</tr>
					</table>
			</td>

		</tr>
		<tr>
			
			<td>
				<s:select id="s_tipoLiquidacion" 
							name="parametro.tipoLiquidacion"
							headerKey=""  headerValue="%{getText('midas.liquidaciones.parametros.autorizacion.ambas')}"
								list="tipoLiquidacion" listKey="key" listValue="value" 
								key="midas.liquidaciones.busqueda.tipoLiquidacion" 
					  		cssClass="txtfield cajaTextoM2 w155 desabilitable"/> 
			</td>
			<td align="left">
				<s:select id="s_tipoPago" 
							name="parametro.tipoPago"
							headerKey=""  headerValue="%{getText('midas.liquidaciones.parametros.autorizacion.todos')}"
								list="tipoPago" listKey="key" listValue="value" 
								key="midas.liquidaciones.parametros.autorizacion.tipoDePago" 
					  		cssClass="txtfield cajaTextoM2 w155 desabilitable"/> 
			</td>
		</tr>
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroVigenciaDeConfiguracion"/></div>
	<table  id="filtrosM2" width="98%" >
		<tr>
			<td>
				<s:checkbox id="ch_condicionVigencia" name="parametro.configuracionVigencia" cssClass="desabilitable" onchange="configurarParaCondicionVigencia();">
									<s:text name="midas.liquidaciones.parametros.autorizacion.condicion"/></s:checkbox>
			</td>
			<td>
				<s:select id="s_condicionVigencia" 
							name="parametro.condicionVigencia" onchange="onChangeCondicionVigencia();"
							headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
								list="criterioComparacion" listKey="key" listValue="value"
					  		cssClass="txtfield cajaTextoM2 w155 desabilitable"/> 

			</td>
			<td>
				<s:checkbox id="ch_rangoVigencia" name="parametro.criterioRangoVigencia" cssClass="desabilitable" onchange="configurarParaRangoVigencia();">
									<s:text name="midas.liquidaciones.parametros.autorizacion.rangos"/></s:checkbox>
			</td>
			<td >
				<table id="agregar" >
					<tr>
						<td colspan="4">
							<s:text name="midas.liquidaciones.parametros.autorizacion.rangoDeVigenciasParaAutorizacion"/>
						</td>
					</tr>
					<tr>
						<td>
							<s:text name="midas.liquidaciones.parametros.autorizacion.de"/>
						</td>
						<td>
							<sj:datepicker name="parametro.vigenciaInicial" changeMonth="true" changeYear="true"
								buttonImage="/MidasWeb/img/b_calendario.gif"
								buttonImageOnly="true" id="fechaVigenciaDe" maxlength="10" size="18"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield desabilitable">
							</sj:datepicker>
						</td>
						<td>
							<div id="contenedor_TituloVigenciaFinal" style="display:none;" class="VigenciaHasta">
							<s:text name="midas.liquidaciones.parametros.autorizacion.hasta"/>
							</div>
						</td>
						<td>
							<div id="contenedor_VigenciaFinal" style="display:none;" class="VigenciaHasta">
							<sj:datepicker name="parametro.vigenciaFinal" changeMonth="true" changeYear="true"
								buttonImage="/MidasWeb/img/b_calendario.gif"
								buttonImageOnly="true" id="fechaVigenciaHasta" maxlength="10" size="18"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								cssClass="cleaneable txtfield desabilitable">
							</sj:datepicker>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroMontoAPagar"/></div>
	<table  id="filtrosM2" width="98%" >
			
		<tr>
			<td>
				<s:checkbox id="ch_condicionMonto" name="parametro.configuracionMonto" cssClass="desabilitable" onchange="configurarParaCondicionMonto();">
									<s:text name="midas.liquidaciones.parametros.autorizacion.condicion"/></s:checkbox>
			</td>
			<td>
				<s:select id="s_condicionMonto" 
							name="parametro.condicionMonto"
							class="desabilitable" onchange="onChangeCondicionMonto();"
							headerKey=""  headerValue="%{getText('midas.general.seleccione')}"
								list="criterioComparacion" listKey="key" listValue="value"  
					  		cssClass="txtfield cajaTextoM2 w155 desabilitable"/> 

			</td>
			<td>
				<s:checkbox id="ch_rangoMontos" name="parametro.criterioRangoMonto" cssClass="desabilitable" onchange="configurarParaRangoMonto();">
									<s:text name="midas.liquidaciones.parametros.autorizacion.rangos"/></s:checkbox>
			</td>
			<td>
				<table id="agregar" >
					<tr>
						<td >
							<s:text name="midas.liquidaciones.parametros.autorizacion.rangoDeMontosTotalAPagar"/>
						</td>
					</tr>
					<tr>
						<td>
							<s:textfield cssClass="cajaTextoM2 setNew desabilitable jQfloat jQrestrict formatCurrency"
								name="parametro.montoInicial" onfocus="removeCurrencyFormatOnTxtInput();"
								size="12"
								maxlength="8"	
								key="midas.liquidaciones.parametros.autorizacion.de"
								id="txt_montoDe"/>
						</td>
						<td>
							<div id="contenedor_montoHasta" style="display:none;">
								<s:textfield cssClass="cajaTextoM2 setNew desabilitable jQfloat jQrestrict formatCurrency"
									name="parametro.montoFinal" onfocus="removeCurrencyFormatOnTxtInput();"
									size="12"		
									maxlength="8"	
									key="midas.liquidaciones.parametros.autorizacion.hasta"
									id="txt_montoHasta"/>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroOficinaDelSiniestro"/></div>
	<table  id="filtrosM2" width="98%" >

		<tr>
			<td align="center">
				<table id="agregar" >
					<tr>
						<td width="30%">
							<ul id="oficinasList" class="w250"
							style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
								<s:iterator value="oficinas"
									status="stat">
									<li><label for="${key}">
											<input type="checkbox" 
											class="desabilitable"
											name="oficinasConcat"
											id="oficinasSeleccionadas${key}"
											value="${key}" class="js_checkEnable" 
											style=""/>
											<s:property value="value"/></label></li>
								</s:iterator>
							</ul>
						</td>
							<td width="80px;" align="left">	
								<div class="controles">
							    <a target="_self" href="javascript: selectAllChecks('oficinasList');">
				                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
				                </a>		    
							    <a target="_self" href="javascript: deselectAllChecks('oficinasList')">
				                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
				                </a> 
				                </div>               
							</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroEstadoDelSiniestro"/></div>
	<table  id="filtrosM2" width="98%" >

		<tr>
			<td align="center">
				<table id="agregar" >
					<tr>
						<td width="30%">
							<ul id="estadosList" class="w250"
							style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
								<s:iterator value="estados"
									status="stat">
									<li><label for="${key}">
											<input type="checkbox" 
											class="desabilitable"
											name="estadosConcat"
											id="estadosSeleccionados${key}"
											value="${key}" class="js_checkEnable" />
											<s:property value="value"/></label></li>
								</s:iterator>
							</ul>
						</td>
							<td width="80px;">	
								<div class="controles">
							    <a target="_self" href="javascript: selectAllChecks('estadosList');">
				                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
				                </a>		    
							    <a target="_self" href="javascript: deselectAllChecks('estadosList')">
				                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
				                </a>  
				                </div>              
							</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroProveedor"/></div>
	<table  id="filtrosM2" width="98%" >

		<tr>
			<td  align="center">
				<table id="agregar" >
					<tr>
						<td width="30%">
							<ul id="proveedoresList" class="w250"
							style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
								<s:iterator value="listPrestadorServicio"
									var="varPrestador" status="stat">
									<li><label for="varPrestador[%{#stat.index}].id">
											<input type="checkbox" 
											class="desabilitable"
											name="proveedoresConcat"
											id="proveedoresSeleccionados${varPrestador.id}"
											value="${varPrestador.id}" class="js_checkEnable" />
											${varPrestador.personaMidas.nombre} </label></li>
								</s:iterator>
							</ul>
						</td>
							<td width="80px;">	
								<div class="controles">
							    <a target="_self" href="javascript: selectAllChecks('proveedoresList');">
				                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
				                </a>		    
							    <a target="_self" href="javascript: deselectAllChecks('proveedoresList')">
				                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
				                </a>  
				                </div>              
							</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroUsuarioSolicitadorAut"/></div>
	<table  id="filtrosM2" width="98%" >

		<tr>
			<td align="center">
				<table id="agregar" >
					<tr>
						<td width="30%">
							<ul id="usuariosSolicitadoresAutorizacionList" class="w250"
							style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
								<s:iterator value="usuarios"
									var="varUsuario" status="stat">
									<li><label for="varUsuario[%{#stat.index}].id">
											<input type="checkbox" 
											class="desabilitable"
											name="usuariosSolConcat"
											id="usuariosSeleccionados${varUsuario.id}"
											value="${varUsuario.id}" class="js_checkEnable" />
											${varUsuario.nombreCompleto} </label></li>
								</s:iterator>
							</ul>
						</td>
							<td width="80px;">	
								<div class="controles">
							    <a target="_self" href="javascript: selectAllChecks('usuariosSolicitadoresAutorizacionList');">
				                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
				                </a>		    
							    <a target="_self" href="javascript: deselectAllChecks('usuariosSolicitadoresAutorizacionList')">
				                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
				                </a>  
				                </div>              
							</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>		
		
	<div class="titulo" ><s:text name="midas.liquidaciones.parametros.autorizacion.tituloParametroUsuariosAutorizadores"/></div>
	<table  id="filtrosM2" width="98%" >
		<tr>
			<td align="center">
				<table id="agregar" >
					<tr>
						<td width="30%">
							<ul id="parametrosUsuariosAutorizadoresList" class="w250"
							style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
								<s:iterator value="usuarios"
									var="varUsuario" status="stat">
									<li><label for="varUsuario[%{#stat.index}].id">
											<input type="checkbox" 
											class = "desabilitable"
											name="usuariosAutConcat"
											id="usuariosAutorizadoresSeleccionados${varUsuario.id}"
											value="${varUsuario.id}" class="js_checkEnable" />
											${varUsuario.nombreCompleto} </label></li>
								</s:iterator>
							</ul>
						</td>
							<td width="80px;">	
								<div class="controles">
							    <a target="_self" href="javascript: selectAllChecks('parametrosUsuariosAutorizadoresList');">
				                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
				                </a>		    
							    <a target="_self" href="javascript: deselectAllChecks('parametrosUsuariosAutorizadoresList')">
				                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selección">
				                </a>   
				                </div>             
							</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td >		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;" id="agregar" > 
							<tr>
								<td  class= "guardar">
									
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="cerrarAlta();"> 
										<s:text name="midas.boton.cerrar" /> </a>
									</div>	
									<s:if test="esParametroSoloLectura==false">
										<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
											 <a href="javascript: void(0);" onclick="guardarParametro();" >
											 <s:text name="midas.boton.guardar" /> </a>
										</div>
									</s:if>	
									
								</td>							
							</tr>
						</table>				
					</td>	
		</tr>
	</table>
</s:form>




<script type="text/javascript">
    jQuery(document).ready(
		function(){
			initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
			setModoConsulta();
			configurarMostrarParametros();
			<s:if test="oficinasConcat != ''">
				cargaMultiplesSelecciones('<s:property value="oficinasConcat"/>','oficinasSeleccionadas')
			</s:if>
			<s:if test="estadosConcat != ''">
				cargaMultiplesSelecciones('<s:property value="estadosConcat"/>','estadosSeleccionados')
			</s:if>
			<s:if test="proveedoresConcat != ''">
				cargaMultiplesSelecciones('<s:property value="proveedoresConcat"/>','proveedoresSeleccionados')
			</s:if>
			<s:if test="usuariosSolConcat != ''">
				cargaMultiplesSelecciones('<s:property value="usuariosSolConcat"/>','usuariosSeleccionados')
			</s:if>
			<s:if test="usuariosAutConcat != ''">
				cargaMultiplesSelecciones('<s:property value="usuariosAutConcat"/>','usuariosAutorizadoresSeleccionados')
			</s:if>
		}
	);
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
