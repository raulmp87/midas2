<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        <column id="selected"       type="ch" width="30"  sort="int"  align="center" >#master_checkbox</column>
        <column id="tieneNotasCredito" type="ro" hidden="true" width="20" sort="int" ></column>
        <column id="factura" type="ro" width="200" sort="int" >Factura</column>
        <column id="total" type="ron" width="150" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
        <column id="consultar" type="img" width="100" sort="na" align="center">Acciones</column>      
  	</head>      
   <s:iterator value="listaFacturasOrdenesPago" status="row">
		<row id="<s:property value="id"/>">
          <cell>false</cell>
				  <cell><s:property value="tieneNotasCredito" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="facturaConOrdenesCompra" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="montoTotal" escapeHtml="true" escapeXml="true"/></cell>
          <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript:consultarOrdenesDeCompra(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		</row>
	</s:iterator>	
</rows>
   
