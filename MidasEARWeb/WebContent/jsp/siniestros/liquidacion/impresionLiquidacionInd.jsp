<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: left;	
	font-size: 11px;
	font-family: arial;
	}
	
.warningField {
    background-color: #f5a9a9;
}
</style>

<script type="text/javascript">
	var mostrarImpresionLiquidacionBeneficiario = '<s:url action="mostrarImpresionLiquidacionBeneficiario" namespace="/siniestros/liquidacion/liquidacionIndemnizacion"/>';
	var imprimirLiquidacionBeneficiario = '<s:url action="imprimirOrdenExpedicionChequeBeneficiario" namespace="/siniestros/liquidacion/liquidacionIndemnizacion"/>';
	var imprimirCartaFiniquitoPath = '<s:url action="imprimirCartaFiniquito" namespace="/siniestros/liquidacion/liquidacionIndemnizacion"/>';
	var mostrarVentanaInfoContratoPath = '<s:url action="mostrarInfoContrato" namespace="/siniestros/liquidacion/liquidacionIndemnizacion"/>';
</script>

<s:form id="impresionLiquidacionBeneficiarioForm" >
		<s:hidden id="idLiqSiniestro" name="liquidacionSiniestro.id"/>
		<s:hidden id="numeroLiquidacion" name="liquidacionSiniestro.numeroLiquidacion"/>
		<s:hidden id="porConceptoPagoSiniestros" value="%{getText('midas.siniestros.liquidacion.conceptopagosiniestros')}" />
		<s:hidden id="esImprimible" name="esImprimible"/>
		<s:hidden id="pantallaOrigen" name="pantallaOrigen"/>
		<s:hidden id="usuarioAutorizador" name="usuarioAutorizador"/>
		<s:hidden id="estatusLiquidacion" name="liquidacionSiniestro.estatus" />
		<s:hidden id="tipoLiquidacion" name="liquidacionSiniestro.tipoLiquidacion" />
		<s:hidden id="tieneIndemnizaciones" name="tieneIndemnizaciones" />
		
		<div class="titulo" style="width: 98%;">
		<s:text name="midas.siniestros.liquidacion.tituloimpresion"/>	
	</div>
	
	<div id="contenedorPDF" style="width: 96%; display:inline; margin-left: 10px;">
	<s:if test="liquidacionSiniestro.id != null">
		<s:if test="esImprimible == true">
			<iframe height="400" width="94%"
				src="/MidasWeb/siniestros/liquidacion/liquidacionIndemnizacion/imprimirOrdenExpedicionChequeBeneficiario.action?liquidacionSiniestro.id=<s:text name="liquidacionSiniestro.id" />&esPreview=true">
				<p>No se pudo generar la previsualizacion de la impresion</p>
			</iframe>
		</s:if>
		<s:else>
			<div style="color:red;font-weight:bold;"><s:text name="midas.siniestros.liquidacion.error.NoImprimible" /></div>
		</s:else>
	</s:if>
	<s:else>
		<div style="color:red;font-weight:bold;"><s:text name="midas.siniestros.liquidacion.error.idLiquidacion" /></div>
	</s:else>
	</div>
</s:form>

<br/>
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td>
					<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:cerrarImprimirLiquidacionBeneficiario();"> 
							<s:text name="midas.boton.cerrar" /> 
							<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_anterior.gif'/>
						</a>
					</div>
					<div id="btn_imprimir" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="javascript:imprimirOrdenExpedicionChequeBeneficiario();"> 
						<s:text name="midas.boton.imprimir" />
						<img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/common/b_imprimir.gif'/>
					</a>
					</div>
					<s:if test="usuarioAutorizador && liquidacionSiniestro.estatus == \"XAUT\"">
						<div id="btn_rechazar" class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="javascript:rechazarLiquidacion();"> 
							<s:text name="midas.boton.rechazar" />
							<img border='0px' alt='Rechazar' title='Rechazar' src='/MidasWeb/img/common/b_borrar.gif'/>
						</a>
						</div>
						<div id="btn_autorizar" class="btn_back w140" style="display: inline; float: right;">
						<a href="javascript: void(0);" onclick="javascript:autorizarLiquidacion();"> 
							<s:text name="midas.boton.autorizar" />
							<img border='0px' alt='Autorizar' title='Autorizar' src='/MidasWeb/img/b_autorizar.gif'/>
						</a>
						</div>
					</s:if>
					<s:if test="liquidacionSiniestro.tipoLiquidacion == \"CHQ\" && tieneIndemnizaciones">
					<div id="btn_infoContrato" class="btn_back w170" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:mostrarVentanaInfoContrato();"> 
								<s:text name="Contrato de Compra Venta" />
								<img border='0px' alt='Contrato de Compra Venta' title='Contrato de Compra Venta' src='/MidasWeb/img/b_aceptar.gif'/> 
							</a>
					</div>
					<div id="btn_imprimirFiniquito" class="btn_back w170" style="display: inline; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:imprimirCartaFiniquito();"> 
								<s:text name="Finiquito" />
								<img border='0px' alt='Finiquito' title='Finiquito' src='/MidasWeb/img/b_aceptar.gif'/> 
							</a>
					</div>
				</s:if>
				</td>
			</tr>
		</tbody>
	</table>

<script src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacionInd.js'/>"></script>
<script src="<s:url value='/js/siniestros/indemnizacion/infoContrato.js'/>"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>