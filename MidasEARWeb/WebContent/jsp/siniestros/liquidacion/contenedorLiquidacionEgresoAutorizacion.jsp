<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script	src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script	src="<s:url value='/js/midas2/siniestros/siniestrosUtil.js'/>"></script>


<style type="text/css">
	#superior{
		height: 350px;
		width: 98%;
	}

	#superiorI{
		height: 100%;
		width: 100%;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		height: 150px;
		width: 250px;
		position: relative;
		float:left;
	}
	
	#SIS{
		margin-top:1%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SII{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SIN{
		height: 60px;
		margin-top:0.5%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDS{
		margin-top:1%;
		height: 25%;
		width: 100%;
		position: relative;
		float:left;
	}
	
	#SDI{
		height: 60px;
		width: 100%;
		margin-top:9%;
		position: relative;
		float:left;
	}
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}
	
	.ui-datepicker-trigger{
		display:none;
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px; padding-bottom:3%; ">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.liquidaciones.busqueda.busqueda.liquidaciones.autorizaciones"/>
</div>	
	<div id="superior" class="divContenedorO">
		<form id="buscarLiquidacionesAutorizacionForm">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 18%;">
						<s:select list="oficinas" cssStyle="width: 90%;"
							name="filtroLiquidaciones.oficinaId" 
							label="%{getText('midas.liquidaciones.busqueda.oficina')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="ctgEstatusLiquidaciones" cssStyle="width: 90%;"
							name="filtroLiquidaciones.estatusLiquidacion" 
							label="%{getText('midas.liquidaciones.busqueda.estatus')}"
							cssClass="cleaneable txtfield obligatorio"
							id="estatusLiquidaciones"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="ctgLiquidaciones" cssStyle="width: 90%;"
							name="filtroLiquidaciones.tipoLiquidacion" 
							label="%{getText('midas.liquidaciones.busqueda.tipoLiquidacion')}"
							cssClass="cleaneable txtfield obligatorio"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:select list="ctgTipoOperaciones" cssStyle="width: 90%;"
							name="filtroLiquidaciones.tipoOperacion" 
							label="%{getText('midas.liquidaciones.busqueda.tipoOperacion')}"
							cssClass="cleaneable txtfield obligatorio"
							id="estatusTipoOperaciones"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 18%;">
						<s:textfield id="noLiquidacion" label="%{getText('midas.liquidaciones.busqueda.noLiquidacion')}" name="filtroLiquidaciones.noLiquidacion"
								cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
				</div>
				<div id="SIS">
					
					<div class="floatLeft" style="width: 18%;">
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.noProveedor')}" name="filtroLiquidaciones.noProveedor" 
								onkeypress="return soloNumeros(this, event, true)"		
								cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style=" padding-right:2%; " >
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.proveedor')}" name="filtroLiquidaciones.nombreProveedor" 
								cssStyle="float: left;width: 300px;" maxlength="100" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.noSiniestro')}" name="filtroLiquidaciones.noSiniestro" 
								onBlur="validaFormatoSiniestro(this);"		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.noFactura')}" name="filtroLiquidaciones.folioFactura" 	
								cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.solicitud.cheque')}" 
								name="filtroLiquidaciones.noSolicitudCheque"
								onkeypress="return soloNumeros(this, event, true)" 	
								cssStyle="float: left;width: 90%;" maxlength="30" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
				</div>
				
				<div id="SIS">
					
					<div class="floatLeft" style=" padding-right:2%; " >
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.solicitadoPor')}" name="filtroLiquidaciones.solicitadoPor" 
								cssStyle="float: left; width: 300px ;" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 15%;">
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.noChequeReferencia')}" name="filtroLiquidaciones.numeroChequeReferencia" 
								onkeypress="return soloNumeros(this, event, true)"		
								cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style=" padding-right:1%; " >
						<s:textfield label="%{getText('midas.liquidaciones.busqueda.autorizadoPor')}" name="filtroLiquidaciones.autorizadoPor" 
								cssStyle="float: left;width: 300px" cssClass="cleaneable txtfield obligatorio"></s:textfield>
					</div>
					
					<div class="floatLeft" style="width: 10%; margin-top:2%;">
						<s:checkbox onClick="javascript:validaCheck(this);" name="servicioPublico" title="Publico" cssClass="obligatorio tipoServicio" labelposition="right" fieldValue="0" value="0" label="%{getText('midas.liquidaciones.busqueda.servicioPublico')}"/>
					</div>
					
					<div class="floatLeft" style="width: 14%; margin-top:2%;">
						<s:checkbox onClick="javascript:validaCheck(this);" name="servicioPrivado" title="Privado" cssClass="obligatorio tipoServicio" labelposition="right" fieldValue="0" value="0"  label="%{getText('midas.liquidaciones.busqueda.servicioPrivado')}"/>
					</div>
					
				</div>
				
				<div style=" width:100%;" >
					<div style=" width:48%; float:left; margin-top:2%;">
					
						<div style=" width:100%; float:left;">
							<div class="floatLeft" >
								<s:select list="ctgCriterioComparacion" id="sCondicionMonto" name="filtroLiquidaciones.condicionMontoTotal" 
									label="%{getText('midas.liquidaciones.busqueda.condicion')}" cssClass="cleaneable txtfield obligatorio" headerKey=""
									onchange="javascript:validaCondicion('#montoDe','#montoHasta',this,'#r1');"
									headerValue="%{getText('midas.general.seleccione')}">
								</s:select>
							</div>
							<div class="floatLeft" style="width: 15%; margin-top:4%;">
								<s:checkbox onClick="javascript:validaRango('#montoDe','#montoHasta',this,'#sCondicionMonto');" id="r1" name="filtroLiquidaciones.esRangoMontoTotal" fieldValue="true" labelposition="left" label="%{getText('midas.liquidaciones.busqueda.rango')}"/>
							</div>
							<div class="floatLeft" style="width: 28%;">
								<s:textfield id="montoDe" label="%{getText('midas.liquidaciones.busqueda.monto.de')}" name="filtroLiquidaciones.montoTotalDe" 
										onkeyup="mascaraDecimales('#montoDe',this.value);"	
										onblur ="mascaraDecimales('#montoDe',this.value);"
										maxlength="10"
										disabled="true" 
										onkeypress="return soloNumeros(this, event, true)"	
										cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield formatCurrency obligatorio"></s:textfield>
							</div>
							<div class="floatLeft" style="width: 25%;">
								<s:textfield id="montoHasta" label="%{getText('midas.liquidaciones.busqueda.hasta')}" name="filtroLiquidaciones.montoTotalHasta" 
										onkeyup="mascaraDecimales('#montoHasta',this.value);"	
										onblur ="mascaraDecimales('#montoHasta',this.value);"
										maxlength="10"
										disabled="true" 
										onkeypress="return soloNumeros(this, event, true)"		
										cssStyle="float: left;width: 90%;" cssClass="cleaneable txtfield formatCurrency obligatorio"></s:textfield>
							</div>
						</div>
						
						<div style=" width:100%; float:left;">
							<div class="floatLeft" >
								<s:select list="ctgCriterioComparacion" id="sCondicionFechaSol" name="filtroLiquidaciones.condicionFechaSolicitud"
									label="%{getText('midas.liquidaciones.busqueda.condicion')}" cssClass="cleaneable txtfield obligatorio" headerKey=""
									onchange="javascript:validaCondicion('#fechaSolDe','#fechaSolHasta',this,'#r2');"
									headerValue="%{getText('midas.general.seleccione')}">
								</s:select>
							</div>
							<div class="floatLeft" style="width: 15%; margin-top:4%;">
								<s:checkbox onClick="javascript:validaRango('#fechaSolDe','#fechaSolHasta',this,'#sCondicionFechaSol');" name="filtroLiquidaciones.esRangoFechaSolicitud" id="r2" fieldValue="true" labelposition="left" label="%{getText('midas.liquidaciones.busqueda.rango')}"/>
							</div>
							<div class="floatLeft" style="width: 29%; margin-top:1%;">
								<label><s:text name="%{getText('midas.liquidaciones.busqueda.fechaSolicitud')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="filtroLiquidaciones.fechaSolicitudDe" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaSolDe" maxlength="10" size="18"
										disabled="true"
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorio">
									</sj:datepicker>
								</div>
							</div>
							<div class="floatLeft" style="width: 28%; margin-top:1%;">
								<label><s:text name="%{getText('midas.liquidaciones.busqueda.hasta')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="filtroLiquidaciones.fechaSolicitudHasta" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaSolHasta" maxlength="10" size="18"
										disabled="true" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorio">
									</sj:datepicker>
								</div>
							</div>
						</div>					
					</div>
					
				<div style=" width:100%;" >
				
					<div style=" width:48%; float:left; margin-top:2%;">
							<div class="floatLeft" >
								<s:select list="ctgCriterioComparacion" id="sCondicionFechaAut" name="filtroLiquidaciones.condicionFechaAutorizacion"
									label="%{getText('midas.liquidaciones.busqueda.condicion')}" cssClass="cleaneable txtfield obligatorio" headerKey=""
									onchange="javascript:validaCondicion('#fechaAutDe','#fechaAutHasta',this,'#r3');"
									headerValue="%{getText('midas.general.seleccione')}">
								</s:select>
							</div>
							
							<div class="floatLeft" style="width: 15%; margin-top:4%;">
								<s:checkbox onClick="javascript:validaRango('#fechaAutDe','#fechaAutHasta',this,'#sCondicionFechaAut');" name="filtroLiquidaciones.esRangoFechaAutorizacion" id="r3" fieldValue="true" labelposition="left" label="%{getText('midas.liquidaciones.busqueda.rango')}"/>
							</div>
							<div class="floatLeft" style="width: 29%; margin-top:1%;">
								<label><s:text name="%{getText('midas.liquidaciones.busqueda.fechaAutorizacion')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="filtroLiquidaciones.fechaAutorizacionDe" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaAutDe" maxlength="10" size="18"
										disabled="true" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorio">
									</sj:datepicker>
								</div>
							</div>
							<div class="floatLeft" style="width: 28%; margin-top:1%;">
								<label><s:text name="%{getText('midas.liquidaciones.busqueda.hasta')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="filtroLiquidaciones.fechaAutorizacionHasta" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaAutHasta" maxlength="10" size="18"
										disabled="true" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorio">
									</sj:datepicker>
								</div>
							</div>
						</div>
						
						<div style=" width:48%; float:left;">
							<div class="floatLeft" >
								<s:select list="ctgCriterioComparacion" id="sCondicionFechaTrans" name="filtroLiquidaciones.condicionFechaElaboracion"
									label="%{getText('midas.liquidaciones.busqueda.condicion')}" cssClass="cleaneable txtfield obligatorio" headerKey=""
									onchange="javascript:validaCondicion('#fechaDeTransfer','#fechaDeTransHasta',this,'#r4');"
									headerValue="%{getText('midas.general.seleccione')}">
								</s:select>
							</div>
							<div class="floatLeft" style="width: 15%; margin-top:4%;">
								<s:checkbox onClick="javascript:validaRango('#fechaDeTransfer','#fechaDeTransHasta',this,'#sCondicionFechaTrans');" name="filtroLiquidaciones.esRangoFechaElaboracion" id="r4" fieldValue="true" labelposition="left" label="%{getText('midas.liquidaciones.busqueda.rango')}"/>
							</div>
							<div class="floatLeft" style="width: 29%; margin-top:1%;">
								<label><s:text name="%{getText('midas.liquidaciones.busqueda.fechaElaboracion')}" /></label>
								<div style="margin-top:6%">
									<sj:datepicker name="filtroLiquidaciones.fechaElaboracionDe" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaDeTransfer" maxlength="10" size="18"
										disabled="true" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorio">
									</sj:datepicker>
								</div>		
							</div>
							<div class="floatLeft" style="width: 28%; margin-top:3%;">
								<label><s:text name="%{getText('midas.liquidaciones.busqueda.hasta')}" />:</label>
								<div style="margin-top:6%">
									<sj:datepicker name="filtroLiquidaciones.fechaElaboracionHasta" changeMonth="true" changeYear="true"
										buttonImage="/MidasWeb/img/b_calendario.gif"
										buttonImageOnly="true" id="fechaDeTransHasta" maxlength="10" size="18"
										disabled="true" 
										onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield obligatorio">
									</sj:datepicker>
								</div>
							</div>
						</div>					
					
					</div>
				</div>
				
				<div id="SIS">
					<div class="btn_back w80" style="display: inline; margin-right: 7%; float: right;">
						<a href="javascript: void(0);" onclick="buscarLiquidacionesAutorizacion();">
						<s:text name="midas.boton.buscar" /> </a>
					</div>
					<div class="btn_back w80" style="display: inline; margin-right: 4%; float: right;">
						<a href="javascript: void(0);" onclick="limpiarBusquedaLiquidaciones('autorizacion');">
						<s:text name="midas.boton.limpiar" /> </a>
					</div>
				</div>
				
			</div>
			<!-- Hidden para tipo de servicio publico o privida -->
			<s:hidden name="filtroLiquidaciones.tipoServicio" id="hTipoServicio"  />
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
		</form>
	</div>
	
	<br/>
	<div class="titulo" style="width: 98%;">
	<s:text name="midas.liquidaciones.busqueda.listado.liquidaciones.autorizaciones"/>
	</div>	
	</br>

	<div id="indicador"></div>
	<div id="listadoGridLiquidaciones" style="width: 99%; height: 380px;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>

	<div id="SIS">
		<div id="divExcelBtn" class="w150"
			style="float: right; display: none;">
			<div class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);"
					onclick="exportarExcelBusquedaLiquidaciones('autorizaciones');"> <s:text
						name="midas.boton.exportarExcel" /> </a>
			</div>
		</div>
	</div>


</div>

	<!-- TIPO DE BUSQUEDA -->
<s:hidden name="origenBusquedaLiquidaciones" value="liquidacionProveedor" id="origenBusquedaLiquidaciones" />

<script>
jQuery(document).ready(function(){
	initCurrencyFormatOnTxtInput(); // FORMATO DE MONEDA 
	initBusquedaLiquidacionesAutorizacionProveedor();
	//initGridBusquedaLiquidacionAutorizacion();
	
  	jQuery("#noLiquidacion").keypress(function(event){
         var code = event.keyCode || event.which;
		 if(code == 13) { 
		   	buscarLiquidacionesEnter(event);
		 }else{
		 	return soloNumeros(event, event, true);
		 }
    });
});
</script>

