<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 25px;
	position: relative;
}
.divFormulario {
	height: 25px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>
<script type="text/javascript">
	
</script>
<s:form id="bandejaLiqForm" class="floatLeft">
<div id="contenido_bandejaLiqForm" style="width:99%;position: relative;">
		<s:hidden id="idReporteCabina" name="idReporteCabina" />
		<s:hidden id="soloLectura" name="soloLectura" />
		<s:hidden id="tipoOrdenPago" name="filtroOrden.tipoOrdenPago" />
		<s:hidden id="cveTipoOrdenPago" name="filtroOrden.cveTipoOrdenPago" />
		<s:hidden id="idCobertura" name="idCobertura" />
		
		
		<s:hidden id="idOrdenCompra" name="idOrdenCompra" />
		<s:hidden id="modoPantalla" name="modoPantalla" />
		<s:hidden id="idOrdenPago" name="idOrdenPago" />
		<s:hidden id="idDetalleOrdenPago" name="deglosePorConcepto.idDetalleOrdenPago" />
		<s:hidden id="idDetalleOrdenCompra" name="deglosePorConcepto.idDetalleOrdenCompra" />
		<s:hidden id="stringPagoSeleccionado" name="stringPagoSeleccionado" />
		<s:hidden id="stringLiquidacionSeleccionada" name="stringLiquidacionSeleccionada" />
		<s:hidden id="estatusTxt" name="liquidacionSiniestro.estatus" />
		
		<s:hidden id="index"  />
		<div class="titulo" align="left" >
		<s:text name="midas.servicio.siniestros.pagos.tituloDeta" />
		</div>
		<div id="divInferior" style="width: 1050px !important;" class="divContenedorO">
			
						<div class="divFormulario" style="padding-top: 5px">
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
								<s:textfield id="idLiquidacionSiniestro" name="liquidacionDetalleDTO.idLiquidacion"   cssClass="txtfield"  label="Numero Liquidacion" labelposition="left" cssStyle="width:80px;" ></s:textfield>
							</div>	
							<div id="estatus" class="floatLeft divInfDivInterno" style="width: 15%; " >
								<s:select list="estautsMap" id="estautslis"
									name="liquidacionDetalleDTO.estatus" label="Estatus" labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:60%;">
								</s:select>
							</div>
							
							<div class="floatLeft divInfDivInterno" style="width: 25%;" >
									<s:select list="tipoOrdenCompraMap" id="tipoOrdenCompraLis"
									name="liquidacionDetalleDTO.tipo" label="Tipo Liquidacion"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:50%;"	
									disabled="soloLectura.value"							
									>
									</s:select>	
									 					
							</div>
							<div class="floatLeft divInfDivInterno" style="width: 30%;" >
									<s:select list="tipoPagoMap" id="tipoPagoLis"
									name="liquidacionDetalleDTO.tipoPago" label="Tipo de Pago"
									labelposition="left"
									cssClass="txtfield" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}"
									cssStyle="width:70%;"
									
									>
									</s:select>							
							</div>
						</div>
						
						<div class="divFormulario" style="padding-top: 5px">
							
							
							<div id="divNomBenef"  class="floatLeft divInfDivInterno" style="width: 25%;" >
									<s:textfield id="nomBeneficiario" name="liquidacionDetalleDTO.beneficiario"   cssClass="txtfield"  label="Beneficiario." labelposition="left" cssStyle="width:150px;"></s:textfield>
							</div>
						
								<div id="divProvedor1" class="floatLeft divInfDivInterno" style="width: 20%; 	" >
									<s:select id="tipoProveedorLis" 
									labelposition="left" 
									label="Tipo Proveedor"
									name="liquidacionDetalleDTO.tipoProveedor"
									headerKey="" headerValue="%{getText('midas.general.seleccione')}"
							  		list="tipoProveedorMap" listKey="key" listValue="value"  
							  		cssClass="txtfield cajaTextoM2 w110"   
							  		onchange="changeTipoPrestador()"
							  	 />
								</div>	
								
								<div id="divProvedorNom1"   class="floatLeft divInfDivInterno" style="width: 25%; " >
										<s:select id="proveedorLis" 
										labelposition="left" 
										label="Proveedor"	
										name="liquidacionDetalleDTO.idProveedor"									
										headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								  		list="proveedorMap" listKey="key" listValue="value"  
								  		onchange="changeProveedor()"
								  		cssClass="txtfield cajaTextoM2 w160"   
								  		/>
								</div>
							
							
						</div>
						
						
						
						
						<div class="divFormulario" style="padding-top: 5px">
							
								<div id="siniestro"   class="floatLeft divInfDivInterno" style="width: 25%; " >
										<div class="floatLeft" style="width: 45%;">
											<s:textfield id="nsOficinaS" maxlength="4"
												name="liquidacionDetalleDTO.claveOficinaSiniestro" cssStyle="width:30%;"								
												cssClass="txtfield" label="No Siniestro" labelposition="left"></s:textfield>
										</div>
										<div class="floatLeft" style="width: 15%;">
											<s:textfield id="nsConsecutivoRS" maxlength="15"
												name="liquidacionDetalleDTO.consecutivoReporteSiniestro"
												cssStyle="width: 99%;"
												cssClass="txtfield"></s:textfield>
				
										</div>
				
										<div class="floatLeft" style="width: 15%;">
											<s:textfield id="nsAnioS" name="liquidacionDetalleDTO.anioReporteSiniestro"
												cssStyle="width: 99%;" maxlength="4"
												cssClass="txtfield"></s:textfield>
										</div>
								</div>
								
								<div id="fact"   class="floatLeft divInfDivInterno" style="width: 25%; " >
										<s:textfield id="factura" name="liquidacionDetalleDTO.numFactura"
										cssClass="txtfield" label="Factura"
										labelposition="left" cssStyle="width:80px;" ></s:textfield>
								</div>
							
							
							
						</div>
						
						
						
						<div class="divFormulario">
							<div id='btnLimpiar'class="btn_back w80"
								style="display: inline; margin-left: 1%; float: right;">
								<a href="javascript: void(0);"
									onclick="limpiar();"> <s:text
										name="midas.boton.limpiar" /> </a>
							</div>
							<div id='btnBuscar' class="btn_back w80"
								style="display: inline; margin-left: 1%; float: right;">
					<a href="javascript: void(0);" onclick="buscarLiquidaciones();">
									<s:text name="midas.boton.buscar" /> </a>
							</div>
							
						
						</div>
						
						
						
		</div>
		
		
		
			
		 
		<div id="divInferiorPagos" style="width: 1050px !important;  " class="floatLeft">
			<div id="tituloPagos" class="titulo" align="left" style="padding-top: 10px">
				<s:text name="Lista Liquidaciones" />
			</div>
		   <div id="liquidacionesGrid" class="dataGridConfigurationClass"
			style="width: 100%; height: 150px;"></div>
			<div id="pagingArea" style="padding-top: 8px "></div>
			<div id="infoArea"></div>
			
		</div>
		
			<div id="divInferior" style="width: 1050px !important; padding-top: 3px" class="floatLeft">	
				
					
				<div id="btnRechazar" class="btn_back w120" style="display: inline;margin-left: 2%;float: right;">
				<a href="javascript: void(0);"  onclick="nuevaLiquidacion();"> 
					<s:text	name="Nueva Liquidacion"/> 
				</a>
				<a href="javascript: void(0);"  onclick="busquedaTempProveedor();"> 
					<s:text	name="BusquedaProveedor"/> 
				</a>
				<a href="javascript: void(0);"  onclick="busquedaTempIndemnizacion();"> 
					<s:text	name="BusquedaIndemnizacion"/> 
				</a>
				</div>
				
			</div>
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	<script>
		jQuery(document).ready(function() {
			iniciaContenedorLiquidacion();
		});
	</script>
	
</div>
	</s:form>
