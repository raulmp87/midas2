<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>	        
        <column id="numeroNotaCredito" type="ro" width="245" sort="int" ><s:text name="midas.liquidaciones.nueva.notaCredito.numero"/></column> 
         <column id="idFactura" type="ro" width="120" sort="int" hidden="true"></column>
        <column id="subtotal" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="iva" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="ivaRet" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="isr" type="ro" width="10" sort="int" hidden="true"></column>	
        <column id="total" type="ron" format="$0,000.00" width="220" sort="int"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column> 	
  	</head>      
   <s:iterator value="listaNotasCredito" status="row">
		<row id="<s:property value="notaCreditoId"/>">
			<cell><s:property value="noFactura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idFactura" escapeHtml="false" escapeXml="true"/></cell>							
			<cell><s:property value="subTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="iva" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="ivaRetenido" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="isr" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="montoTotal" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
</rows>
   
