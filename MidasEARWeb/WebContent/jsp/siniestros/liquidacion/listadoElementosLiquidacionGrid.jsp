<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        <column id="detalle" type="sub_row_grid" width="20" sort="int" ></column>	
        <column id="tieneNotasCredito" type="ro" hidden="true" width="20" sort="int" ></column>
        <column id="numero" type="ro" width="150" sort="int" >Numero</column>
        <column id="tipo" type="ro" width="20" hidden="true" sort="int" ></column>
        <column id="descripcion" type="ro" width="150" sort="int" >Tipo</column>
        <column id="total" type="ron" width="*" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
  	</head>      
   <s:iterator value="listaElementosDisponibles" status="row">
		<row id="<s:property value="id"/>">
				<cell><![CDATA[					    	
				<row>					
				</row>]]></cell>	
				<cell><s:property value="tieneNotasCredito" escapeHtml="true" escapeXml="true"/></cell>		
			  	<cell><s:property value="numero" escapeHtml="true" escapeXml="true"/></cell>	
			 	<cell><s:property value="tipo" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property value="tipoDescripcion" escapeHtml="true" escapeXml="true"/></cell>
			  	<cell><s:property value="montoTotal" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
</rows>
   
