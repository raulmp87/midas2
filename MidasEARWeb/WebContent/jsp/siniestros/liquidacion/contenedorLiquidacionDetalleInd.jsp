<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacionInd.js'/>"></script> 

<script type="text/javascript">
listaBeneficiarios = new Array();  
 	<s:if test="#{listaBeneficiarios != null && listaBeneficiarios.size == 0}"> 
		<s:iterator value="listaBeneficiarios" var="beneficiarioMap" status="index" >
			listaBeneficiarios[${index.count - 1}] = { "label": "${label}", "id": "${id}" } ;		
		</s:iterator>
 	</s:if>		
	
</script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
	
	table.tablaTotalesLiquidacion {
		font-size: 10px;
		white-space: nowrap;
		width: 100%;
		border: 1px solid #73D54A;
	}
	
	table.tablaTotalesLiquidacion th {
		padding-top: 2px;
		padding-bottom: 2px;
		font-size: 11px;
		background-color: #59A950;
		background-image: url(../img/bg_tabla.jpg);
		background-repeat: repeat-x;
		color: white;
	}
	
	.cajaTextoGridTotales{
		color: #000000;
		font-size: 10px;
		text-align: center;
	}	
	
	table#agregarLiq {
	font-size: 9px;
	margin: auto;
	padding: 10px;
	white-space: nowrap;
	width: 98%;
	margin: 10px;
	border: 1px solid #73D54A;		
	}
	
	table#agregarLiq th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
	
	table#agregarLiq td {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
	
	.leftArrow {
	display: block;
	width: 46px;
	height: 46px;
	background: url('/MidasWeb/img/left-arrow.gif') bottom;
	text-indent: -99999px;
	}
	.leftArrow:hover {
    background: url('/MidasWeb/img/left-arrow_hover.png');
	}
	
	.rightArrow {
	display: block;
	width: 46px;
	height: 46px;
	background: url('/MidasWeb/img/right-arrow.gif') bottom;
	text-indent: -99999px;
	}
	.rightArrow:hover {
    background: url('/MidasWeb/img/right-arrow_hover.png');
	}
	
</style>
<s:form id="definirLiquidacionForm" >
<div id="contenido_DefinirLiq" style="width:98%;">

<s:hidden id="idLiquidacion" name="liquidacionSiniestro.id"/>
<s:hidden id="estatusLiquidacion" name="liquidacionSiniestro.estatus"/>
<s:hidden id="soloLectura" name="soloLectura"/>
<s:hidden id="proveedorTieneInfoBancaria" name="proveedorTieneInfoBancaria"></s:hidden>
<s:hidden id="origenBusquedaLiquidaciones" name="origenBusquedaLiquidaciones"/>

		<div class="titulo" align="left" >
		<s:text name="midas.liquidaciones.nueva.datosLiquidacionEgreso" />
		
		</div>
		<div id="contenedorFiltros">		
			<table id="agregarLiq" style="width:98%;" border="0"> 
				<tr>
					<th>
						<s:text name="midas.liquidaciones.busqueda.noLiquidacion"/>
					</th>
					<td>
						<s:textfield id="numLiquidacionSiniestro" disabled="true" name="liquidacionSiniestro.numeroLiquidacion" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
					<td><div id="indicadorA"></div></td>	
				</tr>
				<tr>
					<th>
						<s:text name="midas.liquidaciones.nueva.solicitudChequeMizar"/>
					</th>
					<td  colspan="3">
						<s:textfield id="solicitudCheque" disabled="true" name="liquidacionSiniestro.solicitudCheque.idSolCheque"   cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.busqueda.estatus"/>
					</th>
					<td>
						<s:textfield id="descripcionEstatus" disabled="true" name="liquidacionSiniestro.descripcionEstatus" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.nueva.fechaEstatus"/>
					</th>
					<td>
						<s:textfield id="estatusLiq" disabled="true" name="liquidacionSiniestro.fechaEstatus" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
<!-- Se remueve debido a clarificacion con analisis	-->
<!-- 					<th colspan="1"> -->
<%-- 						<s:text name="midas.liquidaciones.nueva.centroOperacional"/> --%>
<!-- 					</th> -->
<!-- 					<td colspan="3"> -->
<%-- 						<s:select list="listaCentrosOperacion" name="liquidacionSiniestro.idCentroOperacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}"  --%>
<!-- 						 id="centroOperacional" required="true" listValue="nombre" listKey="id" readOnly="%{soloLectura}" disabled="%{soloLectura}" onchange="guardarLiquidacionAutomatico('A')" -->
<!-- 						cssClass="cajaTexto jQrequired w200 campoForma" />  -->
<!-- 					</td>	 -->
					<th colspan="1">
						<s:text name="midas.liquidaciones.nueva.moneda"/>
					</th>
					<td colspan="3">
						<s:textfield id="moneda" disabled="true" name="liquidacionSiniestro.descripcionMoneda" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.busqueda.tipoOperacion"/>
					</th>
					<td>
						<s:select list="ctgTipoOperaciones" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="liquidacionSiniestro.tipoOperacion" id="listaTipoOperaciones" required="true" readOnly="true" disabled="true"
						cssClass="cajaTexto jQrequired w200 campoForma" onchange="guardarLiquidacionAutomatico('A');" /> 
					</td>			
				</tr>
				<tr>					
					<th colspan="1">
					<s:text name="midas.siniestros.liquidacion.beneficiario"/>
				</th>					
			    <td colspan="3">			    	
			    	<s:hidden id="idBeneficiario" name="liquidacionSiniestro.beneficiario" />
 			    	<s:if test="soloLectura">
			    			<s:textfield id="nombreBeneficiario" disabled="true" name="nombreBeneficiario" cssClass="cajaTextoM2 w300" />			    	  				 
						</s:if>
 					<s:else>
						<input value="${nombreBeneficiario}" class="txtfield" style="width: 300px" type="text" id="nombreBeneficiario" 
								name="nombreBeneficiario"
								title="Comience a teclear el nombre del beneficiario y seleccione una opción de la lista desplegada"/>
			    			<img id="iconoBorrar" src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" 
			    					title="Limpiar descripción" onclick ="limpiarBeneficiarioLiq();"/>			    								             		 							
 					</s:else>
				</td>
				<th colspan="1">
						<s:text name="midas.liquidaciones.busqueda.tipoLiquidacion"/>
					</th>					
					<td colspan="3">
						<s:select list="listaTiposLiquidacion"
						name="liquidacionSiniestro.tipoLiquidacion" id="listaTipoLiquidaciones" required="true" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" onchange="guardarLiquidacionAutomatico('A');" />					 
					</td>
				</tr>
				<tr>
					<th colspan="1">
						<s:text name="midas.liquidaciones.nueva.bancoReceptor"/>
					</th>
					<td colspan="3">
						<s:select list="listaBancos" name="liquidacionSiniestro.bancoBeneficiario" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						 id="bancoReceptor" required="true" readOnly="%{soloLectura}" disabled="true" onchange="guardarLiquidacionAutomatico('A')"
						cssClass="cajaTexto jQrequired w200 campoForma" /> 
					</td>
					<th>
						<s:text name="midas.liquidaciones.nueva.clabe"/>
					</th>
					<td>
						<s:textfield id="clabe" disabled="true" name="liquidacionSiniestro.clabeBeneficiario" cssClass="cajaTextoM2 jQnumeric w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.nueva.rfc"/>
					</th>
					<td>
						<s:textfield id="rfc" disabled="true" name="liquidacionSiniestro.rfcBeneficiario" cssClass="cajaTextoM2 jQalphanumeric w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.liquidaciones.nueva.correo"/>
					</th>
					<td colspan="3">
						<s:textfield id="correo" disabled="true" name="liquidacionSiniestro.correo" cssClass="cajaTextoM2 jQnumeric w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.siniestros.cabina.reportecabina.ordenPago.datosTerceroAfectado.lada"/>
					</th>
					<td>						
						<s:textfield id="telefonoLada" disabled="true" name="liquidacionSiniestro.telefonoLada" cssClass="cajaTextoM2 jQalphanumeric w50"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.nueva.telefono"/>
					</th>
					<td>						
						<s:textfield id="telefonoNumero" disabled="true" name="liquidacionSiniestro.telefonoNumero" cssClass="cajaTextoM2 jQalphanumeric w200"></s:textfield>
					</td>
				</tr>					
 			</table> 
 		</div>		 
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.liquidaciones.nueva.datosCheque"/>	
		</div>	
		<div id="datosCheque"> 
			<table id="agregarLiq" style="width:98%;" border="0"> 				
				<tr>
					<th style="width:140px;">
						<s:text name="midas.liquidaciones.nueva.fechaElaboracion"/>
					</th>
					<td>
						<s:textfield id="fechaElaboracion" disabled="true" name="liquidacionSiniestro.solicitudCheque.fechaPago" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>	
				</tr>
				<tr>
					<th>
						<s:text name="midas.liquidaciones.nueva.banco"/>
					</th>
					<td colspan="3">
						<s:textfield id="banco" disabled="true" name="liquidacionSiniestro.solicitudCheque.banco" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>	
					<th style="width:70px;">
						<s:text name="midas.liquidaciones.nueva.cuenta"/>
					</th>
					<td>
						<s:textfield id="cuenta" disabled="true" name="liquidacionSiniestro.solicitudCheque.cuenta" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th style="width:110px;">
						<s:text name="midas.liquidaciones.nueva.noChequeReferencia"/>
					</th>
					<td>
						<s:textfield id="numChequeRef" disabled="true" name="liquidacionSiniestro.solicitudCheque.cheque" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
			</table>
			<table id="agregar" style="width:98%;height:300px;" border="0">
				<tr>
					<td style="width:47%;">
						<table>
							<tr valign="top" style="height: 28px;">
								<td>
									<div class="titulo" style="width: 98%;">
										<s:text name="midas.liquidaciones.nueva.pagosRelacionados"/>	
									</div>
								</td>								
							</tr>
							<tr>
								<td>
								    <div id="indicadorPagosRelacionados"></div>									
									<div id="pagosRelacionadosGrid" style="height: 240px;">
									</div>
								</td>	
							</tr>
						</table>
					</td>
					<s:if test="%{!soloLectura}">
						<td style="width:6%;">
						<a href="javascript: void(0);" onclick="asociarDesasociarTodas(pagosPorRelacionarGrid, pagosRelacionadosGrid);"  class="leftArrow" 
						   title="Asociar Todas las Ordenes de Pago" >
						</a>	
						<a href="javascript: void(0);" onclick="asociarDesasociarTodas(pagosRelacionadosGrid, pagosPorRelacionarGrid);" class="rightArrow" 
						   title="Desasociar Todas las Ordenes de Pago">
						</a>
						</td>
					</s:if>
					<s:else>
						<td style="width:6%;"></td>
					</s:else>
					<td style="width:47%;">
						<table>
							<tr valign="top" style="height: 28px;">
								<td>
									<div class="titulo" style="width: 98%;">
										<s:text name="midas.liquidaciones.nueva.pagosDisponibles"/>	
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div id="indicadorPagosDisponibles"></div>
									<div id="pagosPorRelacionarGrid" style="height: 240px;">
									</div>
								</td>								
							</tr>
						</table>
					</td>
				</tr>			
			</table>
<!-- 			<table id="agregar" style="width:98%;height:300px;" border="0"> -->
<!-- 				<tr> -->
<!-- 					<td style="width:47%;"> -->
<!-- 						<table> -->
<!-- 							<tr valign="top" style="height: 28px;"> -->
<!-- 								<td> -->
<!-- 									<div class="titulo" style="width: 98%;"> -->
<%-- 										<s:text name="midas.liquidaciones.nueva.notasCreditoRelacionadas"/>	 --%>
<!-- 									</div> -->
<!-- 								</td>								 -->
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td> -->
<!-- 								    <div id="indicadorNotasRelacionadas"></div>									 -->
<!-- 									<div id="notasCreditoRelacionadasGrid" style="height: 240px;"> -->
<!-- 									</div> -->
<!-- 								</td>	 -->
<!-- 							</tr> -->
<!-- 						</table> -->
<!-- 					</td> -->
<!-- 					<td style="width:53%;"> -->
<!-- 						<table> -->
<!-- 							<tr valign="top" style="height: 28px;"> -->
<!-- 								<td> -->
<!-- 									<div class="titulo" style="width: 98%;"> -->
<%-- 										<s:text name="midas.liquidaciones.nueva.pagosDisponibles"/>	 --%>
<!-- 									</div> -->
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td> -->
<!-- 									<div id="indicadorNotasRelacionadas"></div> -->
<!-- 									<div id="notasCreditoDisponiblesGrid" style="height: 240px;"> -->
<!-- 									</div> -->
<!-- 								</td>								 -->
<!-- 							</tr> -->
<!-- 						</table> -->
<!-- 					</td> -->
<!-- 				</tr>			 -->
<!-- 			</table>	 -->
		</div>	
		<div class="titulo" align="left" >
			<s:text name="midas.liquidaciones.nueva.pagosYLiquidaciones" />
		</div>		
		<div id="divCifrasPago" style="width:99%">
			<table id="agregar" style="width:98%;" border="0">
				<tr><td><div class="subtituloLeft"><s:text name="midas.liquidaciones.nueva.cifrasPago"/></div></td></tr>
				<tr>
					<td>
						<div id="indicador"></div>	
							<div id="cifrasPagoGrid" style="width:100%;height:180px">	
							<div id="pagingArea"></div><div id="infoArea"></div>
   						</div>
					</td>
				</tr>
			</table>     			
		</div> 
<!-- 		<div id="spacer1" style="height: 2px"></div> -->
<!-- 		<div id="divCifrasNotaCredito" style="width:80%"> -->
<!-- 			<table id="agregar" style="width:80%;" border="0"> -->
<%-- 				<tr><td><div class="subtituloLeft"><s:text name="midas.liquidaciones.nueva.cifrasNotaCredito"/></div></td></tr> --%>
<!-- 				<tr> -->
<!-- 					<td> -->
<!-- 						<div id="indicador"></div>	 -->
<!-- 							<div id="cifrasNotaCreditoGrid" style="width:100%;height:180px">	 -->
<!-- 							<div id="pagingArea"></div><div id="infoArea"></div> -->
<!--    						</div> -->
<!-- 					</td> -->
<!-- 				</tr> -->
<!-- 			</table>     			 -->
<!-- 		</div> -->
		<div id="spacer2" style="height: 2px"></div>
<!-- 		<div id="divImportesNCR">     -->
<!--    			<div id="indicador"></div>	 -->
<!-- 			<div id="ordenesCompraListadoGrid" style="width:98%;height:180px">	 -->
<!-- 				<div id="pagingArea"></div><div id="infoArea"></div> -->
<!--    			</div> -->
<!-- 		</div>  -->		
		<div id="divImportesLiq" style="width:98%"> 
			<table id="agregar" style="width:100%;" border="0">
				<tr><td><div class="subtituloLeft"><s:text name="midas.liquidaciones.nueva.importesLiquidacion"/></div></td></tr>
				<tr>
					<td>
						<table class="tablaTotalesLiquidacion" width="98%" style="width:100%;">
							<tr>
								<th><s:text name="Subtotal"/></th>								
								<th><s:text name="Deducible"/></th>								
								<th><s:text name="Descuentos"/></th>								
								<th><s:text name="IVA"/></th>								
								<th><s:text name="IVA Ret"/></th>								
								<th><s:text name="ISR"/></th>								
								<th><s:text name="Total"/></th>								
								<th><s:text name="Importe Salvamento"/></th>								
							</tr>
							<tr>
								<td><s:textfield id="subtotal" disabled="true" name="liquidacionSiniestro.subtotal" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.subtotal})}" 
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="deducible" disabled="true" name="liquidacionSiniestro.deducible" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.deducible})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="descuentos" disabled="true" name="liquidacionSiniestro.descuento" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.descuento})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="iva" disabled="true" name="liquidacionSiniestro.iva" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.iva})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="ivaRetenido" disabled="true" name="liquidacionSiniestro.ivaRet" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.ivaRet})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="isr" disabled="true" name="liquidacionSiniestro.isr" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.isr})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="total" disabled="true" name="liquidacionSiniestro.total" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.total})}"
									cssClass="cajaTextoGridTotales formatCurrency">
								</s:textfield></td>
								<td><s:textfield id="salvamento" disabled="true" name="liquidacionSiniestro.importeSalvamento" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.importeSalvamento})}"
									cssClass="cajaTextoGridTotales formatCurrency">
									</s:textfield></td>
							</tr>						
						</table>
<!-- 						<div id="indicador"></div>	 -->
<!-- 							<div id="importesLiqGrid" style="width:100%;height:80px">	 -->
<!-- 							<div id="pagingArea"></div><div id="infoArea"></div> -->
<!--    						</div> -->
					</td>
				</tr>
			</table>      			
		</div>
		<div id="totalesLiquidacion" style="width:98%">
			<table id="agregar" border="0"  style="width:100%;">
				<tr align="right">
					<td colspan="1" style="width: 16%"></td>
					<td colspan="4">
						<s:text name="midas.liquidaciones.nueva.netoPagar"/>
					</td>
					<td colspan="1" style="width: 15%">
						<s:hidden id="netoPagar" name="liquidacionSiniestro.total"/>
						<s:textfield id="netoPagarTxt" name="liquidacionSiniestro.total" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.total})}" 
									disabled="true" cssClass="cajaTextoGridTotales formatCurrency jQalphanumeric w150"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan = "1" style="width: 16%"></td>
					<td colspan="2" style="width:15%">
						<div class="floatLeft" style="width: 10%; margin-top:2%;">
						<s:checkbox id="checkPrimaPendiente" onClick="validaCheck(this);seleccionarDeseleccionarPrimas();" name="" 
						labelposition="right" disabled="%{soloLectura || primaPendientePagoConsulta == 0}" value="%{liquidacionSiniestro.primaPendientePago > 0 ? true:false}" 
						label="%{getText('midas.liquidaciones.nueva.primaPendiente')}"/>
					</div>
					</td>
					<td colspan="1"  align="left">
<%-- 						<s:text name="primaPendientePagoConsulta"/> --%>  
						<s:hidden id="primaPendientePagoConsulta" name="primaPendientePagoConsulta"/>
						<s:textfield id="primaPendientePagoConsultaTxt" name="primaPendientePagoConsulta" 
							value="%{getText('struts.money.format',{primaPendientePagoConsulta})}" 
							disabled="true" cssClass="cajaTextoGridTotales formatCurrency jQalphanumeric w150"></s:textfield>
					</td>
					<td colspan="1"  align="right">
						<s:text name="midas.liquidaciones.nueva.primaPendiente"/>
					</td>
					<td colspan="1"  align="right" style="width: 15%">
						<s:hidden id="primaPendientePago" name="liquidacionSiniestro.primaPendientePago"/>
						<s:textfield id="primaPendientePagoTxt" name="liquidacionSiniestro.primaPendientePago" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.primaPendientePago})}" 
									 disabled="true" cssClass="cajaTextoGridTotales formatCurrency jQalphanumeric w150"></s:textfield>
					</td>
				</tr> 
				<tr>
				    <td colspan="1" style="width: 16%"></td>
					<td colspan="2">
						<div class="floatLeft" style="width: 10%; margin-top:2%;">
						<s:checkbox id="checkPrimaNoDevengada" onClick="javascript:validaCheck(this);seleccionarDeseleccionarPrimas();" name=""  
						labelposition="right" disabled="%{soloLectura || primaNoDevengadaConsulta == 0}" value="%{liquidacionSiniestro.primaNoDevengada > 0 ? true:false}" 
						label="%{getText('midas.liquidaciones.nueva.primaNoDevengada')}"/>
					</div>
					</td>
					<td colspan="1"  align="left">
<%-- 						<s:text name="primaNoDevengadaConsulta" />  --%>
						<s:hidden id="primaNoDevengadaConsulta" name="primaNoDevengadaConsulta"/>
						<s:textfield id="primaNoDevengadaConsultaTxt" name="primaNoDevengadaConsulta" 
							value="%{getText('struts.money.format',{primaNoDevengadaConsulta})}" 
							disabled="true" cssClass="cajaTextoGridTotales formatCurrency jQalphanumeric w150"></s:textfield>
					</td>
					<td colspan="1"  align="right">
						<s:text name="midas.liquidaciones.nueva.primaNoDevengada"/>
					</td>
					<td colspan="1"  align="right" style="width: 15%">
						<s:hidden id="primaNoDevengada" name="liquidacionSiniestro.primaNoDevengada"/>
						<s:textfield id="primaNoDevengadaTxt" name="liquidacionSiniestro.primaNoDevengada" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.primaNoDevengada})}" 
									disabled="true" cssClass="cajaTextoGridTotales formatCurrency jQalphanumeric w150"></s:textfield>
					</td>
				</tr>
				<tr  align="right">
				    <td colspan="1" style="width: 16%"></td>
					<td colspan="4">
						<s:text name="midas.liquidaciones.nueva.totalPagar"/>
					</td>
					<td colspan="1" style="width: 15%">
						<s:hidden id="totalPagar" name="liquidacionSiniestro.netoPorPagar"/>
						<s:textfield id="totalPagarTxt" name="liquidacionSiniestro.netoPorPagar" 
						disabled="true" cssClass="formatCurrency cajaTextoGridTotales jQalphanumeric w150"></s:textfield>
					</td>
				</tr>
			</table>		
		</div>
		<div id="textAreaBotones">
			<table id="agregarLiq" style="width:98%;" border="0">
				<tr>		
					<th>
						<s:text name="midas.liquidaciones.nueva.observaciones"/>
					</th>
					<td colspan="6">
						<s:textarea cols="5" rows="5" id="motivo" name="liquidacionSiniestro.comentarios" 
						cssClass="cajaTextoM2 w990" disabled="%{soloLectura}" onblur="guardarLiquidacionAutomatico('B');"/>					 
					</td>										
				</tr>
				<tr>
					<td>
						<div class="btn_back w100" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="cerrarDetalle();">
								<s:text name="midas.boton.cerrar" /> 
							</a>
						</div>						
					</td>
					<td>
						<div id="botonDatosCtaBancaria" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarDatosBancariosLiquidaciones();">
								<s:text name="midas.liquidaciones.nueva.datosCuentaBancaria" /> 
							</a>
						</div>						
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonReexpedir" class="btn_back w160 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="reexpedirLiquidacion(jQuery('#idLiquidacion').val());">
								<s:text name="midas.liquidaciones.nueva.reexpedirCheque" /> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonCancelar" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="cancelarLiquidacion();">
								<s:text name="midas.boton.cancelar" /> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonEliminar" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarMensajeConfirm('Está seguro que desea eliminar la liquidación ?', '20', 
									'eliminarLiquidacion(jQuery(&quot#idLiquidacion&quot).val())',null,null);">
								<s:text name="midas.boton.borrar" /> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonImprimir" class="btn_back w150" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarImprimirLiquidacionBeneficiario(jQuery('#idLiquidacion').val(), jQuery('#origenBusquedaLiquidaciones').val());">
							<s:if test=" liquidacionSiniestro.estatus == 'XAUT' ">
								<s:text name="midas.liquidaciones.nueva.autorizarrechazar" />
							</s:if>
							<s:else>
								<s:text name="midas.boton.imprimir" />
							</s:else> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonSolAutorizacion" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarMensajeConfirm('Está seguro que desea enviar la liquidación a autorizar ?', '20', 
									'solicitarAutorizacionLiq(jQuery(&quot#idLiquidacion&quot).val())', null, null);">
								<s:text name="midas.liquidaciones.nueva.solicitarAutorizacionLiq" /> 
							</a>
						</div>
					</td>					
				</tr>
			</table>		
		</div>
		 
	
			 
		
			
			
	
	
</div>
	</s:form>
	<script>
		jQuery(window).scrollTop(0);
		jQuery(contenido).scrollTop(0);
		buscarOrdenesPagoDisponibles();	
		buscarOrdenesPagoRelacionadas();
		initGridCifrasPorPago();
		desplegarBotones();
		/*
		initGridCifrasPorNotaCredito();
		initGridNotasCreditoAsociadas();	
		*/		
		deshabilitarCombosPrimasLiqAplic();
				
	</script>
