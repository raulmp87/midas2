<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        
        <column id="tipo" type="ro" width="135"  align="center" sort="int" >Tipo</column>
        <column id="subtotal" type="ron" width="135" format="$0,000.00" align="center" sort="int" >Subtital</column>
        <column id="IVA" type="ron" width="135" format="$0,000.00" align="center" sort="int" >IVA</column>
        <column id="IVA_RET" type="ron" width="135" format="$0,000.00" align="center" sort="int" >IVA RET</column>
        <column id="ISR" type="ron" width="135" format="$0,000.00" align="center" sort="int" >ISR</column>
        <column id="Total" type="ron" width="135" format="$0,000.00" align="center" sort="int" >Total</column>
        
  	</head>      
   <s:iterator value="listaNotasCredito" status="row">
		<row id="<s:property value="idFactura"/>">
          <cell><s:property value="tipo" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="subTotal" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="iva" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="ivaRetenido" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="isr" escapeHtml="true" escapeXml="true"/></cell>
          <cell><s:property value="montoTotal" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
</rows>
   

 