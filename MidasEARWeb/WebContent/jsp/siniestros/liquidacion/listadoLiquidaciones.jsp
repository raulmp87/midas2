<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="numOrdenCompra " type="ro" width="30" sort="int" >No</column>
		<column id="tipo" type="ro" width="140" sort="str">Tipo </column>
		<column id="tipoPago" type="ro" width="120" sort="str">Tipo de Pago </column>	
		<column id="proveedor" type="ro" width="220" sort="str">Proveedor </column>
		<column id="beneficiario " type="ro" width="170" sort="str" >Beneficiario </column>  	
		<column id="facturas " type="ro" width="180" sort="str" >Facturas </column> 
		<column id="total " type="ro" width="60" sort="str" >Total </column> 
		<column id="estatus " type="ro" width="65" sort="str" >Estatus </column>  
		<column id="consultar" type="img" width="100" sort="na" align="center">Acciones</column> 			
		
  	</head>      
   <s:iterator value="listadoLiquidaciones" status="row">
		<row id="<s:property value="idLiquidacion"/>">
				
			  	<cell><s:property value="idLiquidacion" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="tipo" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="tipoPago" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="proveedor" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property value="beneficiario" escapeHtml="true" escapeXml="true"/></cell>
				<cell><s:property value="numFactura" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property value="totales" escapeHtml="true" escapeXml="true"/></cell>	
				
				<cell><s:property value="estatus" escapeHtml="true" escapeXml="true"/></cell>
		      	 <s:if test="estatus == 'Registrada' ">
					<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: consultarLiquidacion(<s:property value="idLiquidacion" escapeHtml="false" escapeXml="true"/>,"")^_self</cell>
		    	</s:if>	
		    	<s:else>
					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: consultarLiquidacion(<s:property value="idLiquidacion" escapeHtml="false" escapeXml="true"/>,"consultar")^_self</cell>
		   		</s:else>
			
			
				
				
		
			
				
							
		</row>
	</s:iterator>
	
</rows>
   
