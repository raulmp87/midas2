<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        <column id="detalle" type="sub_row_grid" width="20" sort="int" ></column>
        <column id="tieneNotasCredito" type="ro" hidden="true" width="20" sort="int" ></column>
        <column id="factura" type="ro" width="300" sort="int" >Factura</column>
        <column id="total" type="ron" width="200" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
  	</head>      
   <s:iterator value="listaFacturasOrdenesPagoAsociadas" status="row">
		<row id="<s:property value="id"/>">
				<cell>	    	
				<![CDATA[					    	
				<row>					
				</row>]]>
		    </cell>	
			  	<cell><s:property value="tieneNotasCredito" escapeHtml="true" escapeXml="true"/></cell>	
			  	<cell><s:property value="numeroFactura" escapeHtml="true" escapeXml="true"/></cell>
			  	<cell><s:property value="montoTotal" escapeHtml="true" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
</rows>
   