<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>     
        </beforeInit>		       
        <column id="siniestro" type="ro" width="100" sort="int" >Siniestro</column>
        <column id="formaPago" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="esPerdidaTotal" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="ordenPago" type="ro" width="200" sort="int" >Orden de Pago</column>
        <column id="subtotal" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="deducible" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="descuentos" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="iva" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="ivaRet" type="ro" width="10" sort="int" hidden="true"></column>
        <column id="isr" type="ro" width="10" sort="int" hidden="true"></column>	
        <column id="total" type="ron" width="200" sort="int" format="$0,000.00" align="center"><s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.total"/></column>
        <column id="salvamento " type="ro" width="10" sort="int" hidden="true"></column>        
  	</head>      
   <s:iterator value="listaOrdenesPagoAsociadas" status="row">
		<row id="<s:property value="id"/>">				
			<cell><s:property value="siniestro.numeroSiniestro" escapeHtml="true" escapeXml="true"/></cell>	
			<cell><s:property value="formaPago" escapeHtml="true" escapeXml="true"/></cell>
		  	<cell><s:property value="esPerdidaTotal" escapeHtml="true" escapeXml="true"/></cell>
		  	<cell><s:property value="id" escapeHtml="true" escapeXml="true"/></cell>	
		  	<cell><s:property value="totales.subTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.deduciblesTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.descuentosTotal" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.ivaTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.ivaRetenidoTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.isrTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.totalesPorPagar" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="totales.salvamentoTotalPorPagar" escapeHtml="true" escapeXml="true"/></cell>	
		</row>
	</s:iterator>	
</rows>
   
