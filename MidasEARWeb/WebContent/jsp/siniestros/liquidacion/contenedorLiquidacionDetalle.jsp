<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/siniestros/liquidacion/liquidacion.js'/>"></script> 

<script type="text/javascript">
listaPrestadoresServicio = new Array();  
 	<s:if test="#{listaPrestadoresServicio != null && listaPrestadoresServicio.size == 0}"> 
		<s:iterator value="listaPrestadoresServicio" var="proveedorMap" status="index" >
			listaPrestadoresServicio[${index.count - 1}] = { "label": "${id} - ${label} - ${oficina}", "id": "${id}" } ;		
		</s:iterator>
 	</s:if>		
	
</script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
	
	table.tablaTotalesLiquidacion {
		font-size: 10px;
		white-space: nowrap;
		width: 100%;
		border: 1px solid #73D54A;
	}
	
	table.tablaTotalesLiquidacion th {
		padding-top: 2px;
		padding-bottom: 2px;
		font-size: 11px;
		background-color: #59A950;
		background-image: url(../img/bg_tabla.jpg);
		background-repeat: repeat-x;
		color: white;
	}
	
	.cajaTextoGridTotales{
		color: #000000;
		font-size: 10px;
		text-align: center;
	}	
	
	table#agregarLiq {
	font-size: 9px;
	margin: auto;
	padding: 10px;
	white-space: nowrap;
	width: 98%;
	margin: 10px;
	border: 1px solid #73D54A;		
	}
	
	table#agregarLiq th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
	
	table#agregarLiq td {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
	
	.leftArrow {
	display: block;
	width: 46px;
	height: 46px;
	background: url('/MidasWeb/img/left-arrow.gif') bottom;
	text-indent: -99999px;
	}
	.leftArrow:hover {
    background: url('/MidasWeb/img/left-arrow_hover.png');
	}
	
	.rightArrow {
	display: block;
	width: 46px;
	height: 46px;
	background: url('/MidasWeb/img/right-arrow.gif') bottom;
	text-indent: -99999px;
	}
	.rightArrow:hover {
    background: url('/MidasWeb/img/right-arrow_hover.png');
	}
	
</style>
<s:form id="definirLiquidacionForm" >
<div id="contenido_DefinirLiq" style="width:98%;">

<s:hidden id="idLiquidacion" name="liquidacionSiniestro.id"/>
<s:hidden id="estatusLiquidacion" name="liquidacionSiniestro.estatus"/>
<s:hidden id="soloLectura" name="soloLectura"/>
<s:hidden id="proveedorTieneInfoBancaria" name="proveedorTieneInfoBancaria"></s:hidden>
<s:hidden id="pantallaOrigen" name="pantallaOrigen"/>
<s:hidden id="pagosDisponiblesConcat" name="pagosDisponiblesConcat" />

<%-- 		<s:hidden id="idReporteCabina" name="idReporteCabina" /> --%>
<%-- 		<s:hidden id="soloLectura" name="soloLectura" /> --%>
<%-- 		<s:hidden id="tipoOrdenPago" name="filtroOrden.tipoOrdenPago" /> --%>
<%-- 		<s:hidden id="cveTipoOrdenPago" name="filtroOrden.cveTipoOrdenPago" /> --%>
<%-- 		<s:hidden id="idCobertura" name="idCobertura" /> --%>
		
		
<%-- 		<s:hidden id="idOrdenCompra" name="idOrdenCompra" /> --%>
<%-- 		<s:hidden id="modoPantalla" name="modoPantalla" /> --%>
<%-- 		<s:hidden id="idOrdenPago" name="idOrdenPago" /> --%>
<%-- 		<s:hidden id="idDetalleOrdenPago" name="deglosePorConcepto.idDetalleOrdenPago" /> --%>
<%-- 		<s:hidden id="idDetalleOrdenCompra" name="deglosePorConcepto.idDetalleOrdenCompra" /> --%>
<%-- 		<s:hidden id="stringPagoSeleccionado" name="stringPagoSeleccionado" /> --%>
<%-- 		<s:hidden id="stringLiquidacionSeleccionada" name="stringLiquidacionSeleccionada" /> --%>
<%-- 		<s:hidden id="estatusTxt" name="liquidacionSiniestro.estatus" /> --%>
		
		
<%-- 		<s:hidden id="index"  /> --%>
		<div class="titulo" align="left" >
		<s:text name="midas.liquidaciones.nueva.datosLiquidacionEgreso" />
		
		</div>
		<div id="contenedorFiltros">		
			<table id="agregarLiq" style="width:98%;" border="0"> 
				<tr>
					<th>
						<s:text name="midas.liquidaciones.busqueda.noLiquidacion"/>
					</th>
					<td>
						<s:textfield id="numLiquidacionSiniestro" disabled="true" name="liquidacionSiniestro.numeroLiquidacion" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>	
				</tr>
				<tr>
					<th>
						<s:text name="midas.liquidaciones.nueva.solicitudChequeMizar"/>
					</th>
					<td  colspan="3">
						<s:textfield id="solicitudCheque" disabled="true" name="liquidacionSiniestro.solicitudCheque.idSolCheque"   cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.busqueda.estatus"/>
					</th>
					<td>
						<s:textfield id="descripcionEstatus" disabled="true" name="liquidacionSiniestro.descripcionEstatus" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.nueva.fechaEstatus"/>
					</th>
					<td>
						<s:textfield id="estatusLiq" disabled="true" name="liquidacionSiniestro.fechaEstatus" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
<!-- 					<th colspan="1"> -->
<%-- 						<s:text name="midas.liquidaciones.nueva.centroOperacional"/> --%>
<!-- 					</th> -->
<!-- 					<td colspan="3"> -->
<%-- 						<s:select list="listaCentrosOperacion" name="liquidacionSiniestro.idCentroOperacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}"  --%>
<!-- 						 id="centroOperacional" required="true" listValue="nombre" listKey="id" readOnly="%{soloLectura}" disabled="%{soloLectura}" onchange="guardarLiquidacionAutomatico('A')" -->
<!-- 						cssClass="cajaTexto jQrequired w200 campoForma" />  -->
<!-- 					</td>	 -->
					<th colspan="1">
						<s:text name="midas.liquidaciones.nueva.moneda"/>
					</th>
					<td colspan="3">
						<s:textfield id="moneda" disabled="true" name="liquidacionSiniestro.descripcionMoneda" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th>
						<s:text name="midas.liquidaciones.busqueda.tipoOperacion"/>
					</th>
					<td>
						<s:select list="ctgTipoOperaciones" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="liquidacionSiniestro.tipoOperacion" id="listaTipoOperaciones" required="true" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" onchange="guardarLiquidacionAutomatico('A');" /> 
					</td>			
				</tr>
				<tr>					
					<th colspan="1">
					<s:text name="midas.liquidaciones.busqueda.proveedor"/>
				</th>					
			    <td colspan="3">			    	
			    	<s:hidden id="idPrestadorServicio" name="liquidacionSiniestro.proveedor.id"/>
 			    	<s:if test="soloLectura">
			    			<s:textfield id="nombrePrestadorServicio" disabled="true" name="nombrePrestadorServicio" cssClass="cajaTextoM2 w300" />			    	  				 
						</s:if>
 					<s:else>
						<input value="${nombrePrestadorServicio}" class="txtfield" style="width: 300px" type="text" id="nombrePrestadorServicio" 
								name="nombrePrestadorServicio"
								title="Comience a teclear el nombre del proveedor y seleccione una opción de la lista desplegada"/>
			    			<img id="iconoBorrar" src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" 
			    					title="Limpiar descripción" onclick ="limpiarProveedorLiq();"/>			    								             		 							
 					</s:else>
				</td>
				<th colspan="1">
						<s:text name="midas.liquidaciones.busqueda.tipoLiquidacion"/>
					</th>					
					<td colspan="3">
						<s:select list="listaTiposLiquidacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="liquidacionSiniestro.tipoLiquidacion" id="listaTipoLiquidaciones" required="true" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" onchange="guardarLiquidacionAutomatico('A');desplegarBotones();" />					 
					</td>
				</tr>				
 			</table> 
 		</div>		 
		<div class="titulo" style="width: 98%;">
			<s:text name="midas.liquidaciones.nueva.datosCheque"/>	
		</div>	
		<div id="datosCheque"> 
			<table id="agregarLiq" style="width:98%;" border="0"> 				
				<tr>
					<th style="width:140px;">
						<s:text name="midas.liquidaciones.nueva.fechaElaboracion"/>
					</th>
					<td>
						<s:textfield id="fechaElaboracion" disabled="true" name="liquidacionSiniestro.solicitudCheque.fechaPago" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>	
				</tr>
				<tr>
					<th>
						<s:text name="midas.liquidaciones.nueva.banco"/>
					</th>
					<td colspan="3">
						<s:textfield id="banco" disabled="true" name="liquidacionSiniestro.solicitudCheque.banco" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>	
					<th style="width:70px;">
						<s:text name="midas.liquidaciones.nueva.cuenta"/>
					</th>
					<td>
						<s:textfield id="cuenta" disabled="true" name="liquidacionSiniestro.solicitudCheque.cuenta" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
					<th style="width:110px;">
						<s:text name="midas.liquidaciones.nueva.noChequeReferencia"/>
					</th>
					<td>
						<s:textfield id="numChequeRef" disabled="true" name="liquidacionSiniestro.solicitudCheque.cheque" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
			</table>
			<table id="agregar" style="width:98%;height:300px;" border="0">
				<tr>
					<td style="width:47%;">
						<table>
							<tr valign="top" style="height: 28px;">
								<td>
									<div class="titulo" style="width: 98%;">
										<s:text name="midas.liquidaciones.nueva.pagosRelacionados"/>	
									</div>
								</td>								
							</tr>
							<tr>
								<td>
								    <div id="indicadorPagosRelacionados"></div>									
									<div id="pagosRelacionadosGrid" style="height: 240px;">
									</div>
								</td>	
							</tr>
						</table>
					</td>
					<s:if test="%{!soloLectura}">
						<td style="width:6%;">
						<a href="javascript: void(0);" onclick="asociarDesasociarSeleccionadas(pagosPorRelacionarGrid, pagosRelacionadosGrid,'1');"  class="leftArrow" 
						   title="Asociar Todas las Facturas" >
						</a>	
						<a href="javascript: void(0);" onclick="asociarDesasociarSeleccionadas(pagosRelacionadosGrid, pagosPorRelacionarGrid,'-1');" class="rightArrow" 
						   title="Desasociar Todas las Facturas">
						</a>
						</td>
					</s:if>
					<s:else>
						<td style="width:6%;"></td>
					</s:else>
					<td style="width:47%;">
						<table>
							<tr valign="top" style="height: 28px;">
								<td>
									<div class="titulo" style="width: 98%;">
										<s:text name="midas.liquidaciones.nueva.pagosDisponibles"/>	
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div id="indicadorPagosDisponibles"></div>
									<div id="pagosDisponibles" style="height: 240px;">
									</div>
								</td>								
							</tr>
						</table>
					</td>
				</tr>			
			</table>
			<table id="agregar" style="width:98%;height:300px;" border="0">
				<tr>
					<td style="width:47%;">
						<table>
							<tr valign="top" style="height: 28px;">
								<td>
									<div class="titulo" style="width: 98%;">
										<s:text name="midas.liquidaciones.nueva.notasCreditoRelacionadas"/>	
									</div>
								</td>								
							</tr>
							<tr>
								<td>
								    <div id="indicadorNotasRelacionadas"></div>									
									<div id="notasCreditoRelacionadasGrid" style="height: 240px;">
									</div>
								</td>	
							</tr>
						</table>
					</td>
					<s:if test="%{!soloLectura}">
						<td style="width:6%;">
						<a href="javascript: void(0);" onclick="asociarDesasociarRecuperacion(notasCreditoAsociadasGrid,recuperacionesPendientesGrid,'1');"  class="leftArrow" 
						   title="Asociar Recuperaciones Seleccionadas" >
						</a>	
						<a href="javascript: void(0);" onclick="asociarDesasociarRecuperacion(recuperacionesPendientesGrid,notasCreditoAsociadasGrid,'-1');" class="rightArrow" 
						   title="Desasociar Recuperaciones Seleccionadas">
						</a>
						</td>
					</s:if>
					<s:else>
						<td style="width:6%;"></td>
					</s:else>
					<td style="width:47%;">
						<table>
							<tr valign="top" style="height: 28px;">
								<td>
									<div class="titulo" style="width: 98%;">
										<s:text name="midas.liquidaciones.nueva.notasCreditoDisponibles"/>	
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div id="indicadorNotasDisponibles"></div>
									<div id="notasCreditoDisponiblesGrid" style="height: 240px;">
									</div>
								</td>								
							</tr>
						</table>
					</td>
				</tr>			
			</table>	
		</div>	
		<div class="titulo" align="left" >
			<s:text name="midas.liquidaciones.nueva.pagosYLiquidaciones" />
		</div>		
		<div id="divCifrasPago" style="width:99%">
			<table id="agregar" style="width:98%;" border="0">
				<tr><td><div class="subtituloLeft"><s:text name="midas.liquidaciones.nueva.cifrasPago"/></div></td></tr>
				<tr>
					<td>
						<div id="indicador"></div>	
							<div id="cifrasPagoGrid" style="width:100%;height:180px">	
							<div id="pagingArea"></div><div id="infoArea"></div>
   						</div>
					</td>
				</tr>
			</table>     			
		</div> 
		<div id="spacer1" style="height: 2px"></div>
		<div id="divCifrasNotaCredito" style="width:85%">
			<table id="agregar" style="width:85%;" border="0">
				<tr><td><div class="subtituloLeft"><s:text name="midas.liquidaciones.nueva.cifrasNotaCredito"/></div></td></tr>
				<tr>
					<td>
						<div id="indicador"></div>	
							<div id="cifrasNotaCreditoGrid" style="width:100%;height:180px">	
							<div id="pagingArea"></div><div id="infoArea"></div>
   						</div>
					</td>
				</tr>
			</table>     			
		</div>
		<div id="spacer2" style="height: 2px"></div>
<!-- 		<div id="divImportesNCR">     -->
<!--    			<div id="indicador"></div>	 -->
<!-- 			<div id="ordenesCompraListadoGrid" style="width:98%;height:180px">	 -->
<!-- 				<div id="pagingArea"></div><div id="infoArea"></div> -->
<!--    			</div> -->
<!-- 		</div>  -->		
		<div id="divImportesLiq" style="width:98%"> 
			<table id="agregar" style="width:100%;" border="0">
				<tr><td><div class="subtituloLeft"><s:text name="midas.liquidaciones.nueva.importesLiquidacion"/></div></td></tr>
				<tr>
					<td>
						<table class="tablaTotalesLiquidacion" width="98%" style="width:100%;">
							<tr>
								<th><s:text name="Subtotal"/></th>								
								<th><s:text name="Deducible"/></th>								
								<th><s:text name="Descuentos"/></th>								
								<th><s:text name="IVA"/></th>								
								<th><s:text name="IVA Ret"/></th>								
								<th><s:text name="ISR"/></th>								
								<th><s:text name="Neto Pagar"/></th>								
								<th><s:text name="Importe Salvamento"/></th>								
							</tr>
							<tr>
								<td><s:textfield id="subtotal" disabled="true" name="liquidacionSiniestro.subtotal" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.subtotal})}" 
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="deducible" disabled="true" name="liquidacionSiniestro.deducible" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.deducible})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="descuentos" disabled="true" name="liquidacionSiniestro.descuento" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.descuento})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="iva" disabled="true" name="liquidacionSiniestro.iva" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.iva})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="ivaRetenido" disabled="true" name="liquidacionSiniestro.ivaRet" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.ivaRet})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="isr" disabled="true" name="liquidacionSiniestro.isr" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.isr})}"
									cssClass="cajaTextoGridTotales formatCurrency" >
								</s:textfield></td>
								<td><s:textfield id="total" disabled="true" name="liquidacionSiniestro.netoPorPagar" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.netoPorPagar})}"
									cssClass="cajaTextoGridTotales formatCurrency">
								</s:textfield></td>
								<td><s:textfield id="salvamento" disabled="true" name="liquidacionSiniestro.importeSalvamento" 
									value="%{getText('struts.money.format',{liquidacionSiniestro.importeSalvamento})}"
									cssClass="cajaTextoGridTotales formatCurrency">
									</s:textfield></td>
							</tr>						
						</table>
<!-- 						<div id="indicador"></div>	 -->
<!-- 							<div id="importesLiqGrid" style="width:100%;height:80px">	 -->
<!-- 							<div id="pagingArea"></div><div id="infoArea"></div> -->
<!--    						</div> -->
					</td>
				</tr>
			</table>      			
		</div>
		<div id="textAreaBotones">
			<table id="agregarLiq" style="width:98%;" border="0">
				<tr>		
					<th>
						<s:text name="midas.liquidaciones.nueva.observaciones"/>
					</th>
					<td colspan="6">
						<s:textarea cols="5" rows="5" id="motivo" name="liquidacionSiniestro.comentarios" 
						cssClass="cajaTextoM2 w990" disabled="%{soloLectura}" onblur="guardarLiquidacionAutomatico('B');"/>					 
					</td>										
				</tr>
				<tr>
					<td>
						<div class="btn_back w100" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="cerrarDetalle();">
								<s:text name="midas.boton.cerrar" /> 
							</a>
						</div>						
					</td>
					<td>
						<div id="botonDatosCtaBancaria" class="btn_back w160" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarDatosBancariosLiquidaciones();">
								<s:text name="midas.liquidaciones.nueva.datosCuentaBancaria" /> 
							</a>
						</div>						
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonReexpedir" class="btn_back w160 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="reexpedirLiquidacion();">
								<s:text name="midas.liquidaciones.nueva.reexpedirCheque" /> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonCancelar" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="cancelarLiquidacion();">
								<s:text name="midas.boton.cancelar" /> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonEliminar" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarMensajeConfirm('Está seguro que desea eliminar la liquidación ?', '20', 
									'eliminarLiquidacion(jQuery(&quot#idLiquidacion&quot).val())',null,null);">
								<s:text name="midas.boton.borrar" /> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonImprimir" class="btn_back w150" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarImprimirLiquidacion(jQuery('#idLiquidacion').val(), PANTALLA_LIQUIDACION_EGRESO);">
							<s:if test=" liquidacionSiniestro.estatus == 'XAUT' ">
								<s:text name="midas.liquidaciones.nueva.autorizarrechazar" />
							</s:if>
							<s:else>
								<s:text name="midas.boton.imprimir" />
							</s:else> 
							</a>
						</div>
<!-- 					</td> -->
<!-- 					<td> -->
						<div id="botonSolAutorizacion" class="btn_back w150 esconder" style="display: inline; margin-left: 1%; float: right; ">
							<a href="javascript: void(0);" onclick="mostrarMensajeConfirm('Está seguro que desea enviar la liquidación a autorizar ?', '20', 
									'solicitarAutorizacionLiq(jQuery(&quot#idLiquidacion&quot).val())', null, null);">
								<s:text name="midas.liquidaciones.nueva.solicitarAutorizacionLiq" /> 
							</a>
						</div>
					</td>					
				</tr>
			</table>		
		</div>
		 
	
			 
		
			
			
	
	
</div>
	</s:form>
	<script>

		jQuery(document).ready(function(){
		
			jQuery(window).scrollTop(0);
			jQuery(contenido).scrollTop(0);
			//initGridCifrasPorPago();
			initGridCifrasPorNotaCredito();
			initGridNotasCreditoAsociadas(); 
			//buscarNotasCreditoRelacionadas();	
			buscarOrdenesPagoDisponibles();		
			buscarOrdenesPagoRelacionadas();
			//buscarNotasCreditoDisponibles();
			initGridRecuperaciones();
			habilitarDeshabilitarProveedor();
			initGridOrdenesDeCompra();
			calculaLiquidacion();
			if(jQuery("#idPrestadorServicio").val() != null 
				&& jQuery("#idPrestadorServicio").val()!= "")
			{
				obtenerTipoLiquidacionProveedor(jQuery("#idPrestadorServicio").val());		
			}
			desplegarBotones();	
		});		

	</script>
