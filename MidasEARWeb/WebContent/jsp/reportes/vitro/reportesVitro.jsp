<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:include value="/jsp/reportes/vitro/contenHeaderReporte.jsp"></s:include>

<div id="contenedorReporteVitro" style="width: 98%;">
	<s:form id="reporteVitroForm" >
	<s:hidden id="nombreGrupo" name="nombreGrupo"/>
		<table  style="width: 100%;" border="0">
			<tr >
				<td style="width: 98%;" class="titulo" colspan="4" ><s:text name="midas.reportes.dpn.title" /></td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="mensajes" style="display: none;">
						<div id="textMessage" >  </div>
					</div>
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"></th>
				<th style="width: 33%;"></th>
				<th style="width: 33%;"></th>
			</tr>
		</table>
		<table id="desplegarDetalle" border="0">
			<tr>
				<th style="width: 25%;"><s:text name="midas.reportes.dpn.negocio.dpn"/></th>
				<th style="width: 25%;"><s:text name="midas.reportes.dpn.cliente.dpn" /></th>
				<th style="width: 25%;"><s:text name="midas.reportes.dpn.grupo" /></th>
				<th style="width: 25%;"><s:text name="midas.reportes.dpn.fecha.reporte" /></th>
			</tr>
			<tr>
				<td style="width: 25%;">
					<s:select cssClass="cajaTexto" id="idNegocio"
						name="idNegocio" list="negocios"
						listKey="idToNegocio" listValue="descripcionNegocio" headerKey="-1"
						headerValue="%{getText('midas.general.seleccione')}" />
				</td>
				<td style="width: 25%;">
					<div id="dClietne"></div>
				</td>
				<td style="width: 25%;">
					<div id="dGrupo"></div>
				</td>
				<td style="width: 25%;">
					<div id="dFechaCorte"></div>
				</td>
			</tr>
		</table>
		<div id="botonesBusqieda" style="width: 98%;">
			<table width="100%">
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" onclick="generarReporteVitro()" href="javascript: void(0);"> 
								<s:text name="midas.reportes.dpn.boton.generar" />
							</a>
						</div>
					</td>
				</tr>
			</table>
			</div>
	</s:form>
</div>
<br/><br/><br/>