<%@ taglib prefix="s" uri="/struts-tags"%>
<!--  imports Js files -->
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/reportes/reportesVitro.js'/>"></script>
<script type="text/javascript">
	var fechasReportes = '<s:url action="buscarListaFechasCobro" namespace="/reportes/vitro"/>';
	var generarReporte = '<s:url action="generarReporte" namespace="/reportes/vitro"/>';
	var buscarClientes = '<s:url action="buscarClientes" namespace="/reportes/vitro"/>';
	var buscarGrupos = '<s:url action="buscarGruposCliente" namespace="/reportes/vitro"/>';
</script>