    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
	<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">
	<sj:head/>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/reportes/autos/reporteSesas.js'/>"></script>




<head>
<meta content="IE=8" http-equiv="X-UA-Compatible">
</head>
<div style="width: 98%;">

<s:form action="listar" id="reporteSesasForm" introButon="submit">
	<div class="titulo" style="width: 98%;">
			<s:text name="Reporte Sesas Vida"/>
	</div>
	
	<!-- Filtros Inputs -->
	
	
	
	<div style=" width: 45%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/">
		<div style="width: 100%;float: left;/*border: 1px solid rgba(0, 128, 0, 0.39);*//*background-color: rgba(115, 213, 74, 0.52);*/ ">
		<b style="font-size: medium;font-family: Verdana, Arial, Helvetica, sans-serif;color: rgba(21, 117, 10, 0.63);background-color: #edfae1; padding-left: 1%;">
			<s:text name="1" />
        </b>
        <b style="padding-left: 1%;">
			<s:text name="Programar de ejecucion de Sesas" />
        </b>
    </div>
		<div style="width: 98%;margin-top: 5%;margin-left: 2%; ">
				<b style="font-weight: normal;">
				<s:text name="Seleccione el periodo del reporte" />
                </b>
		</div>
		<div style="width: 100%;margin-left: 1%;">
			<div style="width: 40%; margin-left: 1%;">
					<sj:datepicker name="fechaInicial" cssStyle="width: 80%; border: 1px solid #a0e0a0;"
						key="midas.reporteSesas.fechaInicial"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaInicial"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</div>
			<div style="width: 40%; margin-left: 1%;">
					<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 80%; border: 1px solid #a0e0a0;"
						key="midas.reporteSesas.fechaFinal"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</div>
			</div>
			<div style="/* width: 95%; */margin: 1%;float: left;padding-left: 1%;     margin-top: 5%;">
					<div class="btn_back w140" style="display: inline; float: right;">                    
					<a id="submit" href="javascript: void(0);" onclick="compareDatesVida('V');" class="icon_buscar" title="El proceso se ejecutara a las 8 PM">
					<s:text name="Programar ejecucion"/> </a>
					</div>
			</div>
	</div>
	
	
	<div style=" width: 45%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/">
		<div style="width: 100%;float: left;/*border: 1px solid rgba(0, 128, 0, 0.39);*//*background-color: rgba(115, 213, 74, 0.52); */">
		<b style="font-size: medium;font-family: Verdana, Arial, Helvetica, sans-serif;color: rgba(21, 117, 10, 0.63);background-color: #edfae1;padding-left: 1%;">
			<s:text name="2" />
        </b>
        <b style="padding-left: 1%;">
			<s:text name="Descarga de archivos Sesas" />
        </b>
    </div>
		<div style="width: 98%;margin-top: 5%;margin-left: 2%;">
				<b style="font-weight: normal;">

				<s:text name="Seleccione el subramo y tipo de archivo a generar" />
                </b>
		</div>
        <div id="subramos"  style="width: 96%; padding-top: 2%; float: left; padding-left: 3%;">
		<div style="width: 53%; padding-left: 2%; border: 1px solid rgba(0, 128, 0, 0.39); float: left;">
		<div style="width: 100%; padding-top: 1%; padding-left: 2%;">
		<b><s:text name="midas.reporteSesas.titulosubramos" /></b>
		</div>
        <div style="width: 48%; float: left; padding: 1%;">        
			<input type="radio" name="subramos" value="1" id="individual">
            <s:text name="midas.reporteSesas.individual" />
		</div>
        <div style="width: 48%; float: left; padding: 1%;">        
            <input type="radio" name="subramos" value="2" id="grupo">
            <s:text name="midas.reporteSesas.grupo" /> 
		</div>
		</div>
		</div>
		
		
		
        <div id="tipoarchi"  style="width: 96%; padding-top: 3%; float: left; padding-left: 3%;">
		 
		<div style="width: 53%; padding-left: 2%; border: 1px solid rgba(0, 128, 0, 0.39); float: left;">
		<div style="width: 100%; padding-top: 1%; padding-left: 2%;">
		<b><s:text name="midas.reporteSesas.tituloArchivos" /></b>
		</div>
        <div style="width: 48%; float: left; padding: 1%;">        
			<input type="radio" name="tipoarchi" id="archemi" value="1" checked="checked">
             <s:text name="midas.reporteSesas.emision" />
		</div>
        <div style="width: 48%; float: left; padding: 1%;">        
            <input type="radio" name="tipoarchi" id="archsin" value="2" checked="checked">
            <s:text name="midas.reporteSesas.siniestros" />
		</div>
		</div>
		
		</div>
		<div style="/* width: 97%; */padding-top: 4%;float: left;padding-bottom: 1%;padding-left: 2%;">
			<div class="btn_back w140" style="display: inline; float: right;">                     
					<a id="submit" href="javascript: void(0);" onclick="generarReporteVida();" class="icon_buscar" title="Los archivos a descargar corresponden a la ultima ejecucion de Sesas">
					<s:text name="Descargar Archivo" /> </a>
			</div>
		</div>
	</div>


</s:form>
	<div id="contenedorFiltros"  style=" width: 98%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/">
			<div id="indicador" style=" width: 98%; margin: 1%; float: left;/* border: 1px solid rgba(0, 128, 0, 0.39);*/"></div>
			<div id="reporteSesasGrid" style="width: 94%; height: 130px; cursor: default; margin-left: 2%; padding-top: 2%; border-style: none;"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
	</div>


<script type="text/javascript">
	iniciaReporteSesas('V');
</script>

