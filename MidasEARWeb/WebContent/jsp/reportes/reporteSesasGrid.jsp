<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			 <call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>		
		<column id="fechaCreacion" type="ro" width="*"><s:text name="midas.reporteSesas.fechaCreacion" /></column>
		<column id="usuario" type="ro" width="*"><s:text name="midas.reporteSesas.usuario" /></column>
		<column id="fechaInicial" type="ro" width="*"><s:text name="midas.reporteSesas.fechaInicial" /></column>
		<column id="fechaFinal" type="ro" width="*"><s:text name="midas.reporteSesas.fechaFinal" /></column>
		<column id="estatus" type="ro" width="*"><s:text name="midas.reporteSesas.estatus" /></column> 
		<column id="resultado" type="link" width="*"><s:text name="midas.reporteSesas.resultado" /></column>
		<column id="matriz" type="link" width="*"><s:text name="midas.reporteSesas.matriz" /></column>
</head>
	
	<s:iterator value="listTareasSesas" status="row">
		<row id="<s:property value="id"          escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaInicial" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFinal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell> 
			<s:if test="resultado==\"EXITOSA\"">
			<cell>ARCHIVOS^javascript: generarReporte(<s:property value="id" escapeHtml="false" escapeXml="true"/>,"<s:property value="claveNeg" escapeHtml="false" escapeXml="true"/>");^_self</cell>
			<cell>MATRIZ^javascript: generarMatriz(<s:property value="id" escapeHtml="false" escapeXml="true"/>);^_self</cell>
			</s:if>
			<s:else>
				<cell><s:property value="resultado" escapeHtml="false" escapeXml="true"/></cell>
				<cell></cell>
			</s:else>
		</row>
	</s:iterator>

</rows>

