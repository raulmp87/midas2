<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<script type="text/javascript" src="<s:url value='/js/reportes/reportePolizaXSerie.js'/>"></script>

<script type="text/javascript">
	
	dhx_init_tabbars();
	
	/**
	 * Definicion de las variables de path de los accion.
	 */
	var uploadFileExcel = '<s:url action="processFileReport" namespace="/reporte/polizas/numeroSerie"/>';
	var generateReport = '<s:url action="generaRepoSerInd" namespace="/reporte/polizas/numeroSerie"/>';
</script>



<div id="contenido">
	<div id="contenedorFiltros">
		<s:form action="/generacion/mostrar" namespace="/vida/cargaArchivos" id="generarReporte">
		
	<s:hidden name="idToControlArchivo" />
			<div class="titulo">
				<s:text name="midas.reporte.poliza.numero.serie.titulo"/>
			</div>
			<div select="<s:property value='tabActiva'/>" hrefmode="ajax-html" style="height: 25%; width: 95%;" id="cotizacionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
				<div width="150px" id="detalle" name="<s:text name="midas.reporte.poliza.numero.serie.tab.reporte.layout" />" href="http://void">
					<table width="100%">
						<tr>
							<td width="35%">
								<div class="btn_back w140" style="display: inline; float: left;">
									<a id="submit" onclick="abrirModalCargaArchivo()" href="javascript: void(0);"> 
										<s:text name="midas.reporte.poliza.numero.serie.label.busca.archivo" />
									</a>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div width="150px" id="complementar" name="<s:text name="midas.reporte.poliza.numero.serie.tab.reporte.individual" />"  href="http://void">
					<table id="desplegarDetalle" width="100%">
						<tr>
							<td>
								<s:text name="midas.reporte.poliza.numero.serie.label.poliza.seycos"/>
							</td>
							<td>
								<s:text name="midas.reporte.poliza.numero.serie.label.poliza.midas"/>
							</td>
							<td>
								<s:text name="midas.reporte.poliza.numero.serie.label.serie.vehiculo"/>
							</td>
						</tr>
						<tr>
							<td>
								<s:textfield name="numeroPolizaSeycos" id="numeroPolizaSeycos" cssClass="cajaTexto"/>
							</td>
							<td>
								<s:textfield name="numeroPolizaMidas" id="numeroPolizaMidas" cssClass="cajaTexto"/>
							</td>
							<td>
								<s:textfield name="numeroSerie" id="numeroSerie" cssClass="cajaTexto"/>
							</td>
						</tr>
						<tr>
							<td>
								<div class="btn_back w200" style="display: inline; float: left;">
									<a onclick="generarReporteSerieInd();">
										<s:text name="midas.reporte.poliza.numero.serie.boton.generar.individaual" />
									</a>
								</div>
							</td>
							<td>
								<div class="btn_back w140" style="display: inline; float: right;">
									<a onclick="cleanFilters();">
										<s:text name="midas.boton.limpiar" />
									</a>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</s:form>
	</div>
</div>