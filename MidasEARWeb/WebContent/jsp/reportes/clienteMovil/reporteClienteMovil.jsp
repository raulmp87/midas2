<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/reportes/clienteMovil.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>	
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/util.js'/>"></script>

<script type="text/javascript">
		generarExcelURL='<s:url action="exportExcel" namespace="/reportes/clienteMovil"/>';
	
	</script>

<div id="contenido">
	<div id="contenedorReporteClienteMovil" style="width: 98%;">
		<s:form id="reporteClienteMovilForm" >
		<s:hidden id="nombreGrupo" name="nombreGrupo"/>
		<table  style="width: 100%;" border="0">
			<tr >
				<td style="width: 98%;" class="titulo" colspan="4" ><s:text name="midas.reporte.movil.cliente.titulo" /></td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="mensajes" style="display: none;">
						<div id="textMessage" >  </div>
					</div>
				</td>
			</tr>
			<tr>
				<th style="width: 34%;"></th>
				<th style="width: 33%;"></th>
				<th style="width: 33%;"></th>
			</tr>
		</table>
		<table id="desplegarDetalle" border="0">
			<tr>
				<th style="width: 25%;"><s:text name="midas.reporte.movil.cliente.tipo.poliza"/></th>
			</tr>
			<tr>
				<td style="width: 25%;">
				       <s:select  
					      name="idTipoPol" id="idTipoPol"
					      labelposition="left"  
					      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					      list="tipoPolizaLista" listKey="clave" listValue="nombre" 					      	   
				          cssClass=" txtfield"/>
				</td>
			</tr>
		</table>
		<div id="botonesBusqueda" style="width: 98%;">
			<table width="100%">
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<div class="btn_back w140" style="display: inline; float: right;">
							<a id="submit" onclick="generarReporte()" href="javascript: void(0);"> 
								<s:text name="midas.reporte.movil.cliente.generar" />
							</a>
						</div>
					</td>
				</tr>
			</table>
			</div>
	</s:form>
	
	</div>
</div>