    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
	<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">
	<sj:head/>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>


	<script type="text/javascript" src="<s:url value='/js/midas2/reportes/autos/reporteSiniestros.js'/>"></script>

<head>
<meta content="IE=8" http-equiv="X-UA-Compatible">
</head>

<s:form action="listar" id="reporteVigorForm" introButon="submit">
	<div class="titulo" style="width: 98%;">
			<s:text name="Reporte Vigor Autos"/>
	</div>

<!-- Filtros Inputs -->
	<div style="width: 98%;">
	<table id="agregar" border="0" class="fixTabla" >
			<tr>
				<td>
					<sj:datepicker name="fechaInicial" cssStyle="width: 170px;"
						key="midas.reporteSesas.fechaInicial"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaInicial"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				</td> 
		    	<td>
					<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 170px;"
						key="midas.reporteSesas.fechaFinal"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			 	</td>
			 	<td>
					<div class="btn_back w140" style="display: inline; float: right;">                    
					<a id="submit" href="javascript: void(0);" onclick="compareDatesV(1);" class="icon_buscar">
					<s:text name="midas.reporteSesas.programarEjecucion" /></a>
					</div>
				</td>
			 	<td>
					<div class="btn_back w140" style="display: inline; float: right;">                    
					<a id="submit" href="javascript: void(0);" onclick="compareDatesV(2);" class="icon_buscar">
					<s:text name="midas.reporteSesas.generarInformacion" /> </a>
					</div>
				</td>
			</tr>
			
			</table>
	</div>
</s:form>

<div id="contenedorFiltros" >
			<div id="indicador"></div>
			<div id="reporteSesasGrid" style="width:98%;height:130px"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
</div> 

<script type="text/javascript">
	iniciaReporteReservas(1);
</script>
