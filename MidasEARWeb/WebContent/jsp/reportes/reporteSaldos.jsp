<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<script type="text/javascript">
	var urlExportarDetalleExcel = '<s:url action="exportarDetalleExcel" namespace="/reportes/ppct/saldos"/>';
	var urlExportarResumenExcel = '<s:url action="exportarResumenExcel" namespace="/reportes/ppct/saldos"/>';
</script>
<script src="<s:url value="/js/midas2/reportes/ppct/reportesaldos.js"/>" type="text/javascript"></script>

<head>
	<meta content="IE=8" http-equiv="X-UA-Compatible">
</head>

<s:form id="reporte">
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.reportes.ppct.modulo.titulo" />
	</div>
	<div class="subtituloIzquierdaDiv" style="width: 98%;">
		<s:text name="midas.reportes.ppct.titulo" />
	</div>
	
	<div class="filtros"style="width: 90%;text-align: left; height: 30px">
				<div id="corte" style="display: inline; float: left; width:30%">
					<sj:datepicker required="true" cssStyle="width: 170px;"
						key="midas.reportes.ppct.periodo"
						labelposition="left" 					
						buttonImage="../img/b_calendario.gif"
					    name="filtroReporte.periodo"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    yearRange="-8y:+1y"
						displayFormat="yymm"
						maxlength="12" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('yyyymm', 'date').attach(this)"
					/>
				</div>
				<s:hidden name="filtroReporte.claveNegocio" />
				
				<div id="b_reporteXLS" style="display: inline; float: right; width:27%; margin: 4px 5px;">
					<a href="javascript: void(0);" onclick="javascript: exportarDetalleExcel();"> <s:text
						name="midas.reportes.ppct.detalle.exportar" /> </a>
				</div>
				<div id="b_reporteXLS" style="display: inline; float: right; width:27%; margin: 4px 5px;">
					<a href="javascript: void(0);" onclick="javascript: exportarResumenExcel();"> <s:text
						name="midas.reportes.ppct.resumen.exportar" /> </a>
				</div>
	
	</div>
	
</s:form>
