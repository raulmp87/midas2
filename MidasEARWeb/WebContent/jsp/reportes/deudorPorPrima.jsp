<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<style type="text/css">
label {
	font-weight: bold;
}
</style>
<script type="text/javascript">
	
	function descargar(){
		var nivel = jQuery("input[name='nivel']:checked").val();
		var rango = jQuery('#rango').val();
		window.open('/MidasWeb/reportes/deudorPorPrima/descargaExcel.action?nivel='+nivel+'&rango=30', 'download');
	}

	function actualizar(){
		sendRequestJQ(null,'/MidasWeb/reportes/deudorPorPrima/mostrar.action','contenido', null);
	}
	
	function generar(){
		var fechaReporte = jQuery("#fechaReporte").val();
		sendRequestJQ(null,'/MidasWeb/reportes/deudorPorPrima/generaDXP.action?fechaReporte='+fechaReporte,'contenido', null);
	}
</script>
<div class="titulo" style="float: both; background-size: 99% auto;">
	<b><s:text name="midas.deudorPorPrima.titulo" /></b>
</div>
<s:form id="deudorPrimaForm">	
<div id="generacionCompleta" class="row titulo" style="float: both; background-size: 99% auto;">
	<s:text name="midas.negocio.fechaCreacion" />_
	<s:if test="fechaUltimaGeneracion != null">
		<s:property value="fechaUltimaGeneracion" />		
	</s:if>
	<s:else>
		N/A
	</s:else>
</div>
<s:if test="ejecutandoDXP > 0">
<div id="generando" class="row titulo" style="float: both;">
	<s:text name="midas.deudorPorPrima.generando" />
</div>
</s:if>
<div class="clear"></div>
	<table id="agregar" style="padding: 0px; margin: 0px; border: none;"  width="98%" class="fixTabla">
		<tr>
			<td valign="top" width="55%" colspan="2">
				<s:if test="ejecutandoDXP == 0">
							<sj:datepicker name="fechaReporte" required="#requiredField" 
					           labelposition="left" 
					           key="midas.negocio.fechaOperacion"
					           size="10"
					           changeMonth="true"					           
					           changeYear="true"					           
					           readonly="readonly"	
					           showOn="%{#showOnConsulta}"
					           disabled="%{#disabledConsulta}"
					           cssClass="txtfield" cssStyle="width: 175px;"
							   buttonImage="../img/b_calendario.gif"
							   id="fechaReporte" maxlength="10" cssClass="txtfield"								   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onchange="ajustaFechaFinal();"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>	
				</s:if>	
				<s:else>
					<s:hidden name="fechaReporte" id="fechaReporte" />
				</s:else>				       					   	  										
			</td>
			<td>
			</td>			
		</tr>
		<tr>
			<td>
				<s:if test="0 != 0 && null != null">
				<s:textfield key="midas.deudorPorPrima.rango" cssClass="txtfield jQnumeric jQrestrict"
							 maxlength="17" 
							 labelposition="left" 
							 value = 30
							 id="rango" name="rango" />
				</s:if>					
			</td>		
			<td>
				<s:if test="ejecutandoDXP == 0 && fechaUltimaGeneracion != null">
				<s:radio id="nivel" name="nivel" list="#{'R':'Resumen','D':'Detalle'}"  />
				</s:if>					
			</td>
			<td>
				<s:if test="ejecutandoDXP == 0 && fechaUltimaGeneracion != null">
				<div id="nuevaCotizacion2" class="btn_back w140" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="descargar();"> <s:text
						name="midas.boton.exportarExcel" /> </a>
				</div>
				</s:if>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div id="actualizar" class="btn_back w140" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="actualizar();"> <s:text
						name="midas.boton.actualizar" /> </a>
				</div>
				<s:if test="ejecutandoDXP == 0">
				<div id="nuevaCotizacion" class="btn_back w140" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="generar();"> <s:text
						name="midas.boton.crearNuevaVersion" /> </a>
				</div>
				</s:if>				
			</td>
		</tr>
	</table>
	
</s:form>