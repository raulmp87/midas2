    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
	<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">
	<sj:head/>
	
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>


	<script type="text/javascript" src="<s:url value='/js/midas2/reportes/autos/reporteSesas.js'/>"></script>




<head>
<meta content="IE=8" http-equiv="X-UA-Compatible">
</head>

<s:form action="listar" id="reporteSesasForm" introButon="submit">
	<div class="titulo" style="width: 98%;">
			<s:text name="Reporte Sesas"/>
	</div>

<!-- Filtros Inputs -->
	<div style="width: 98%;">
	<table id="agregar" border="0" class="fixTabla" >
	<tr>
			<td colspan=3>
				<b>
				<s:text name="midas.reporteSesas.tituloFechas" />
                </b>
			</td>			
			</tr>
			<tr>
				<td width="200">
					<sj:datepicker name="fechaInicial" cssStyle="width: 200px;"
						key="midas.reporteSesas.fechaInicial"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaInicial"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				</td> 
		    	<td  width="200">
					<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 200px;"
						key="midas.reporteSesas.fechaFinal"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			 	</td>
			 	<td align="center"  width="1200">
					<div class="btn_back w140" style="display: inline; float: right;">                    
					<a id="submit" href="javascript: void(0);" onclick="compareDates('A');" class="icon_buscar">
					<s:text name="midas.reporteSesas.generarInformacion" /> </a>
					</div>
				</td>
			</tr>
			
			 <tr>
                  <td >
 <b>                        <s:text name="midas.reporteSesas.titulosubramos" />
                     </b>
                   <div id="subramos"  style="width: 98%;">
                     <table height="75" class="fixTabla"  id="agregar">
                        <tbody>
                            <tr>
                           <td align="center" colspan="6">
                              
                           </td>
                        </tr>
                        <tr>
                           <td align="right"><input type="checkbox" name="subramos" value="1" id="individual"></td>
                           <td align="left">
                              <s:text name="midas.reporteSesas.individual" />
                           </td>
                           <td align="right"><input type="checkbox" name="subramos" value="2" id="flotilla"></td>
                           <td align="left">
                             <s:text name="midas.reporteSesas.flotilla" /> 
                           </td>
                          
                          
                        </tr>
                          
                      
                     </tbody></table>
                     </div>
                  </td>
<td >
 <b>
                       &nbsp;
                     </b>
                   <div id="subramos" style="width: 98%;">
                     <table height="75" class="fixTabla" id="agregar">
                        <tbody><tr>
                           <td align="center" colspan="6">
                              <s:text name="midas.reporteSesas.titulomodo" />
                            </td>
                        </tr>
                        
                        <tr>
                           <td align="right"><input type="radio" name="modoRd" value="" id="modoRd"  disabled ></td>
                           <td align="left">
                              <s:text name="midas.reporteSesas.original" />
                           </td>
                           <td align="right"><input type="radio" name="modoRd" value="1" id="modoRd" checked="checked" disabled ></td>
                           <td align="left">
                              <s:text name="midas.reporteSesas.optimizado" />
                           </td>
                          
                          
                        </tr>
                          
                      
                     </tbody></table>
                     </div>
                  </td>
                  <td >
 <b>
                       &nbsp;
                     </b>
                     <table height="75" style="width:90%; " id="agregar">
                        <tbody>
                        <tr>
                           <td align="center" colspan="6">
                              <s:text name="midas.reporteSesas.tituloArchivos" />
                           </td>
                        </tr>
                        <tr>
                          <td style="width:5%;">&nbsp;</td>
                           <td style="width:30%;">
                              <s:text name="midas.reporteSesas.datosGenerales" />
                           </td>
                           <td style="width:10%;"><input type="checkbox" name="tipoarchi" id="archgen" value="1" checked="checked"></td>
                        <td style="width:15%;">&nbsp;</td>
                           <td style="width:30%;">
                            <s:text name="midas.reporteSesas.emision" />
                           </td>
                           <td style="width:10%;"><input type="checkbox" name="tipoarchi" id="archemi" value="2" checked="checked"></td>
                        
                         </tr>
                        <tr>
                        <td>&nbsp;</td>
                           <td>
                              <s:text name="midas.reporteSesas.siniestros" />
                           </td>
                           <td><input type="checkbox" name="tipoarchi" id="archsin" value="3" checked="checked"></td>
                        <td>&nbsp;</td>
                           <td>
                              <s:text name="midas.reporteSesas.deducibles" />
                           </td>
                           <td><input type="checkbox" name="tipoarchi" id="archsin" value="4" checked="checked"></td>
                        </tr>
                   
                     </tbody></table>
                    
                  </td>
               </tr>
			</table>
	</div>
</s:form>

<div id="contenedorFiltros" >
			<div id="indicador"></div>
			<div id="reporteSesasGrid" style="width:98%;height:130px"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
</div>

<script type="text/javascript">
	iniciaReporteSesas('A');
</script>

