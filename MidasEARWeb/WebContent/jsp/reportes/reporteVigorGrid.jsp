<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		<!-- <column id="tipoArchivo" type="ro" width="*"><s:text name="midas.reporteReservas.tipoArchivo"  /></column> -->
		<column id="fechaIni" type="ro" width="*"><s:text name="midas.reporteReservas.fechaInicio" /></column>
		<column id="fechaFin" type="ro" width="*"><s:text name="midas.reporteReservas.fechaFin" /></column>
		<column id="fechaInicialE" type="ro" width="*"><s:text name="midas.reporteReservas.fechaInicialE" /></column>
		<column id="fechaFinalE" type="ro" width="*"><s:text name="midas.reporteReservas.fechaFinalE" /></column>
		<column id="estatus" type="ro" width="*"><s:text name="midas.reporteReservas.estatus" /></column>
</head>
	<% int a=0;%>	
	<s:iterator value="listTareasVigor">
	<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="fechaIni" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFin" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaInicialE" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaFinalE" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>			
		</row>
	</s:iterator>

	<row id="<%=a+1%>">
			<cell><s:property value="resultado" escapeHtml="false" escapeXml="true"/></cell>
				
		</row>
</rows>

