<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<!--script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script-->
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<s:hidden name="idField"></s:hidden>

<script type="text/javascript">
	var bitacoraGridPath = '<s:url action="listarBitacoraConfiguracion" namespace="/compensacionesAdicionales/banca" />';
	jQuery(function() {
		var idReturn = '<s:property value="idCp"/>';
		var urlFiltro = bitacoraGridPath+ "?idCp="+idReturn;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "bitacoraGridDetalle", null, idField,'ordenPagosModal');
	});	 
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 60px;
	position: relative;
}
.divFormulario {
	height: 35px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:form id="adminArchivosForm" name="adminArchivosForm"  class="floatLeft">
	
			<div class="titulo" align="left" >
			<s:text  name="Bitácora de Acciones" />
			</div>			
<div id="divInferior" style="width: 80% !important; padding-top: 10px" class="floatLeft">
<div id="contenido_admin" style="width:80%;position: relative;">
			<div id="bitacoraGridDetalle" class="w700 h300" style="overflow:hidden"></div>
            <div id="pagingArea"></div>
            <div id="infoArea"></div>
</div>	
</div>

</s:form>


