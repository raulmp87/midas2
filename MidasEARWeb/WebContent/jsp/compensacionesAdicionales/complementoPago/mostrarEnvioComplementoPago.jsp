<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
 <script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/compensacionesAdicionales/recepcionComplementoPago.js'/>"></script>

<script type="text/javascript">
var listarComplementosPago = '<s:url action="listarComplementosPago" namespace="/complementosPago/egresos"/>';
var listarComplementosCargados = '<s:url action="listarComplementosCargados" namespace="/complementosPago/egresos"/>';
var listarFacturasPath = '<s:url action="listarFacturas" namespace="/complementosPago/egresos"/>';
var verDetalleValidacionesPath = '<s:url action="verDetalleValidacionFactura" namespace="/complementosPago/egresos"/>';
var validarRegistrarPath = '<s:url action="validarRegistrarFactura" namespace="/complementosPago/egresos"/>';
	
</script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
</style>


<s:form id="recepcionFacturaForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.complementosPago.egresos.listadoFacturas.titulo"/>	
</div>	
<s:hidden name="idBatch" id="idBatch"/>
<div id="contenedorFiltros" style="width: 98%;">
	<table>
		<tbody>
			<tr>
	        	<td>
					<table class="contenedorConFormato">
					    <tbody>
					    	<tr>
					    		<th><s:text name="midas.prestamosAnticipos.numeroAgente" />: </th>
					    		<td>
					    			<s:property value="idAgente" />
					    			<s:hidden name="idAgente" id="hiddenIdAgenteComp" />
					    		</td>
					    		<th><s:text name="midas.prestamosAnticipos.numeroCedula" />: </th>
					    		<td><s:property value="numeroCedula" /></td>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.prestamosAnticipos.venceCedula" />: </th>
					    		<td><s:property value="vencimientoCedula" /></td>
					    		<th><s:text name="midas.prestamosAnticipos.tipoAgente" />: </th>
					    		<td><s:property value="tipoAgente" /></td>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.prestamosAnticipos.nombreAgente" />: </th>
					    		<td><s:property value="nombreAgente" /></td>
					    		<th><s:text name="midas.prestamosAnticipos.centroOperacion" />: </th>
					    		<td><s:property value="centroOperacion" /></td>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.prestamosAnticipos.gerencia" />: </th>
					    		<td><s:property value="gerencia" /></td>
					    		<th><s:text name="midas.prestamosAnticipos.estatus" />: </th>
					    		<td><s:property value="estatus" /></td>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.prestamosAnticipos.rfc" />: </th>
					    		<td><s:property value="rfc" /></td>
					    		<th><s:text name="midas.prestamosAnticipos.ejecutivo" />: </th>
					    		<td><s:property value="ejecutivo" /></td>
					    	</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="divButtons">
	<table width="800" style="font-family: arial; font-size: 11px;">
		<tr>
		<!-- 
		<s:if test="%{isAgentePromotor ==true}">
				<td>
					<font color="#FF6600">* </font>
					<s:text name="midas.fuerzaventa.configBono.agente" />:
					<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
							onchange="addAgenteProduccion();" style="display:none;" />
				</td>
				<td>
					<s:select name="idAgente"
						style="font-family: arial; font-size: 11px; width:250px;"
						id="agentes" 
						list="agenteList" 
						listKey="idAgente" 
						listValue="nombreCompleto" 
						headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="setAgente(this.value)"/>
				</td>
			</s:if> 
			-->
			<td>
			<!-- 
				<s:if test="%{anioMes!='N/A' && ((isAgente==true && tieneFacturasPendientes==false) || isAdministrador==true || isAgentePromotor==true)}">
					<div class="btn_back w170">
						<a id="loadButton" href="javascript: void(0);" class="icon_buscar" onclick="importarArchivo()">Cargar Factura XML</a>
					</div>
				</s:if>
			-->	
			</td>
			<!-- 
			<s:if test="%{isAgentePromotor !=true}">
				<td></td>
				<td></td>
				<td></td>
			</s:if>
			 -->
			<td colspan="3" align="right">
				<s:if test="%{isAdministrador==true}">
					<div>
						Escribe Clave Agente:
						<input id="inputIdAgenteComp" type="text" class="ui-corner-all">
					</div>
				</s:if>
			</td>
			<td>
				<s:if test="%{isAdministrador==true}">
					<div class="btn_back w110">
						<a id="initButton" href="javascript: void(0);" class="icon_buscar" onclick="cargarInfoAgente()">Buscar</a>
					</div>
				</s:if>
			</td>
		</tr>
		<!-- 
		<tr>
			<td>
			<s:if test="%{isAutorizadorExcepcion==true}">
				<div id="autorizacionExcepcion">
				  
					<table class="contenedorConFormato">
						<tbody>
					    	<tr>
					    		<th>
					    			<div class="titulo w300"><s:text name="midas.fuerzaventa.agente.factura.excepcion.autorizacion.titulo" /></div>
					    		</th>
					    		<td/>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.fuerzaventa.agente.factura.excepcion.autorizacion.autorizador1" /> </th>
					    		<td><s:checkbox id="autorizacion1" name="autorizacion1" disabled="disableAutorizacion1" onchange="autorizaExcepcion()"/></td>
					    	</tr>
					    	<tr>
					    		<th><s:text name="midas.fuerzaventa.agente.factura.excepcion.autorizacion.autorizador2" /> </th>
					    		<td><s:checkbox id="autorizacion2" name="autorizacion2" disabled="disableAutorizacion2" onchange="autorizaExcepcion()"/></td>
					    	</tr>
						</tbody>
			    	</table>
				
				</div>
			</s:if>
			</td>
			<td align="center">
				<s:if test="%{tieneFacturasPendientes==true}">
					<td style="font-size: 11px;font-weight: bold;"><s:property value="msgFacturasPendientes" /></td>
				</s:if>
			</td>
			<s:if test="%{tieneFacturasPendientes==true && isAdministrador==true}">
				<td align="right" colspan="7">
					<midas:boton onclick="javascript: marcarFacturasAntiguas();"  tipo="seleccionar" key="midas.agente.factura.antigua.marcado.boton" style="width: 360px; display: inline; float: right; margin-left: 5px;"/>
				</td>	
			</s:if>
		</tr>
		 -->
	</table>
</div>
<br/>


<div id="divFacturas">    
    <div id="indicadorFactura"></div>
	<div id="gridFacturasPaginado">
		<div id="facturasListadoGrid" style="width:98%;height:180px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer2" style="height: 15px"></div>
<div class="titulo" style="width: 98%;">
	<s:text name="midas.complementosPago.egresos.listadoComplementosPago.titulo"/>	
</div>	
<div id="spacer1" style="height: 10px"></div>
<div align="left">
	<table>
		<tr>
			<td>
				<div class="btn_back w110" id="cargar" >
					<a href="javascript: void(0);" title="Seleccione un archivo comprimido extensión ZIP que contenga las facturas a cargar (XMLs)"
					   onclick="cargarZipFacturas();">
						<s:text name="midas.agentes.cargaMasiva.cargarArchivo"/>
					</a>
				</div>
			</td>
			<td>
			<div style="display: inline; float: left; color: #FF6600; font-size: 10;">
				<font color="#FF6600">
				<s:text name="test">El tamaño máximo del archivo puede ser 40 MB</s:text>			
				</font>
			</div>
			</td>
		</tr>		
	</table>
	
</div>
<div id="spacer1" style="height: 10px"></div>

<!-- Listado de complementos de pago -->
<div id="divFacturas">    
    <div id="indicadorComplemento"></div>
	<div id="gridFacturasPaginado">
		<div id="complementosPagoGrid" style="width:98%;height:180px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer1" style="height: 10px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right" style="width: 100%;">
			<tbody>
				<tr>
				<td> 
					<table>
						<tr>
							<td>
								<div style="display: width:150px; inline; float: left; color: #00a000; font-size: 10;"" id="divExcelBtn" style="float:left;">
	               					<s:text name="test">Total Facturas Cargadas:</s:text>	
	               				</div>
							</td>
							<td>
								<s:textfield id="numComplementosCargados" disabled="true" name="numComplementosCargados" cssClass="cajaTextoM2 w80" />
							</td>
						</tr>
					</table>                   
<!--                		<div id="divExcelBtn"  class="w300" style="float:left;"> -->
<!-- 	               		<div style="display: inline; float: left; color: #00a000; font-size: 10;"> -->
<%-- 		               		<s:text name="test">Total Facturas Cargadas: <div style="display: inline;color:black;font-weight:bold; font-size: 10;"><s:textfield id="numFacturasCargadas" disabled="true" name="numFacturasCargadas" cssClass="cajaTextoM2 w100" /></div></s:text>		 --%>
<!-- 	              		 </div> -->
<!--                		</div>                    -->
            	</td>            	  
				<td>
					<s:if test="idBatch != null">
						<div align="right">
						<div class="btn_back w110" id="validarRegistrarBoton" >
							<a href="javascript: void(0);" title="Enviar facturas asociadas a validar"
								onclick="validarRegistrar();">
								<s:text name="midas.siniestros.pagos.facturas.recepcionFacturas.validarRegistrar.boton"/>
							</a>
						</div>
						</div>
					</s:if>
				</td>
				<!--	<td>
						<div align="right">
						<div class="btn_back w110" id="cancelarBoton" >
							<a href="javascript: void(0);"
								onclick="crearCitaJuridico();">
								<s:text name="midas.boton.cancelar"/>
							</a>
						</div>
						</div>
					</td>  -->
				</tr>
			</tbody>
	</table>		
</div>
<s:hidden name="UUID" id="UUID"/>
<s:hidden name="origenEnvio" id="origenEnvio"/>
</s:form>
<script type="text/javascript">
	inicializar();
	//buscarOrdenesCompra();
	 //buscarFacturas(); 
	 
</script>
