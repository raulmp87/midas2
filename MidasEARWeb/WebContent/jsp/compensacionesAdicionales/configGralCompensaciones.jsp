<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:set name="idRamo" value="compensacionesDTO.ramo.id"/>
<s:if test="%{#idRamo == 2}">
	<s:set id="readOnlyD" value="true" ></s:set>
</s:if>
<s:set name="varVidaInd" value="compensacionesDTO.vidaIndiv"/>
<input id="varVidaIndividual" value="<s:property value='compensacionesDTO.vidaIndiv'/>" type="hidden"/>

<div align="center" style="height: 100%; width: 100%;">
	<div id="divCompensacionPrimaBaja" >	
		<table height="100%" width="100%" class="tableStyle" id="tableCompensConfigCa">				
			<tr>
				<td>						
					<table height="100%" width="100%" class="tableStyleNoBorder" id="tableContenedorPrimaBaja">
						<tr>
							<td>
								<table height="100%" width="100%" class="tableStyle" id="tablePrima">
									<tr>
										<td colspan="3">
											<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.general.encabezado"/></div>
											<div class="titulo"></div>
										</td>
									</tr>
									<tr>
										<td rowspan="5" >
											<s:checkbox name="compensacionesDTO.porPrima" id="checkPrimaGral" label="" cssClass="bloquearConf" />
										</td>
										<td rowspan="5">
											<s:text name="midas.compensaciones.configurador.porprima"/>								
										</td>											
										<td>
											<table height="100%" width="100%" class="tableStyleNoBorder" id="tableContenedorProcentajeMontoPrima">
												<tr>														
													<td>
														<table id="tableContenedorPorcentajePrima" class="tableStyleNoBorder">
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenaje"/>
																</td>
																<td>
																	<s:textfield title="midas.compensaciones.configurador.encabezado.porcdistribuir"  name="compensacionesDTO.porcePrima" id="textPorcePrimaGral" readonly="#readOnlyD" cssClass="bloquearConf cajaTextoM2 w100 classDisabledPrima validarPorcentaje"/>
																</td>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																</td>
															</tr>														
														</table>
													</td>
													<s:if test="%{#idRamo > 3}">													
														<td>
															<table id="tableContenedorMontoPrima" class="tableStyleNoBorder">
																<tr>
																	<td>
																		<s:text name="midas.compensaciones.configurador.monto"/>
																	</td>
																	<td>
																		<s:textfield onkeyup="porcentajeOmonto(this.id,'textPorcePrimaGral')" name="compensacionesDTO.montoPrima" id="textMontoPrimaGral" cssClass="bloquearConf cajaTextoM2 w100 classDisabledPrima validarMonto"></s:textfield>
																	</td>																
																</tr>														
															</table>
														</td>
													</s:if>
													<td>
														<table id="tableContenedorPorcentajePrimaAgente" class="tableStyleNoBorder">
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.general.porcagente"/>
																</td>
																<td>
																	<s:textfield title="midas.compensaciones.configurador.general.proporcionagt" onkeyup="checkPorcen2Elementos(this.id,'textPorcePrimaPromoGral')" name="compensacionesDTO.porceAgentePrima" id="textPorcePrimaAgenGral" cssClass="bloquearConf cajaTextoM2 w100 classDisabledPrima validarPorcentaje"></s:textfield>
																</td>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																</td>
															</tr>
														</table>
													</td>														
													<td>
														<table id="tableContenedorPorcentajePrimaPromotor" class="tableStyleNoBorder">
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.general.porcpromotor"/>
																</td>
																<td>
																	<s:textfield title="midas.compensaciones.configurador.general.proporcionpromo" onkeyup="checkPorcen2Elementos(this.id,'textPorcePrimaAgenGral')" name="compensacionesDTO.porcePromotorPrima" id="textPorcePrimaPromoGral" cssClass="bloquearConf cajaTextoM2 w100 classDisabledPrima validarPorcentaje"></s:textfield>
																</td>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																</td>
															</tr>
														</table>
													</td>
												</tr>	
											</table>
										</td>
									</tr>		
									<tr>
										<td>
												<table id="tableContenedorBaseCalculo" class="tableStyleNoBorder">
													<tr>
														<td>
															<s:text name="midas.compensaciones.configurador.basecalculo"/>
														</td>
														<td>
														<s:select id="selectBaseCalGral" 
														  name="compensacionesDTO.baseCalculocaPrima.id" 
														  cssClass="bloquearConf cajaTextoM2 w100 jQrequired classDisabledPrima"
														  disabled="#readOnly" 
														  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
	       												  list="listBaseCalculoca" listKey="id" listValue="nombre"
	       												  />
														</td>
														<s:if test="%{#idRamo == 2}">																			
															<td align="center">														
																<s:text name="midas.compensaciones.configurador.tipoprovision"/>
															</td>
															<td colspan="3">	
																<table class="tableStyle" style="border: none;">
																	<tr>										
																		<td>														 
																			<s:radio id="radioProvisiones" value="compensacionesDTO.tipoProvisioncaPrima.id" name="compensacionesDTO.tipoProvisionca.id" list="listTipoProvisionca" listKey="id" listValue="nombre"/>
																		</td>
																	</tr>
																</table>											
															</td>						
														</s:if>
													<tr>
																									
												</table>

																					
										</td>																			
									</tr>
										
									<s:if test="%{#idRamo == 1}">
										<tr>
											<td colspan="3">											
													<table height="100%" width="100%" class="tableStyleNoBorder classDisabledPrima" id="tableContenedorCondicionesPrimaGral">
														<tr>
															<td>
																<div id="divContenedorCondicionesPrima">
																	<table align="left" height="100%" width="100%" class="tableStyleNoBorder"  id="tableContenedorCondicionesPrima">
																		<tr>
																			<td>
																				<s:text name="midas.compensaciones.configurador.tipocalculo"/>
																			</td>																
																			<td>
																				<s:radio cssClass="bloquearConf classDisabledPrima radioCondicionesCalcGrupo" id="radioCondicionesCalc" value="compensacionesDTO.condicionesCalcPrima.id" name="compensacionesDTO.condicionesCalcPrima.id" list="listCondicionesCalccas" listKey="id" listValue="nombre"/>
																			</td>
																		</tr>
																	</table>
																</div>
															</td>														
														</tr>
													</table>
												</td>
											</tr>
										</s:if>								
										<s:if test="%{#idRamo == 1}">
											<tr>
												<td colspan="3">
													<div id="subramosAutosGrid" class="w350 h100" style="overflow:hidden; "></div>
												</td>
											</tr>
										</s:if>	
										<s:elseif test="%{#idRamo == 2}">
											<tr>
												<td>
													<table class="tableStyleNoBorder">							
														<tr>
															<td colspan="3">
																<div id="divSubRamosDanios"  class="h150" style="overflow:hidden; width: 711px"></div>
															</td>
														</tr>
														<tr>
															<td>
																<s:text name="midas.compensaciones.configurador.total"/>
															</td>
															<td>
																<s:textfield name="compensacionesDTO.importeTotalSubRamosDanios" id="totalCompensacionId" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
															</td>
															<td>
																<div id="divBtnRecalcularCompensacion" class="btn_back w120">
																	<a type="anchor" atributo="agre" id="btnRecalcularCompensacionId" href="javascript:void(0)"  onclick="CompensacionUtils.parametros.calcularSubRamosDanios()">
																		<s:text name="midas.compensaciones.boton.calcular"/>
																	</a>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</s:elseif>						
									
								
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table height="100%" width="100%" class="tableStyle" id="tableBajaSiniestralidad">
									<tr>
										<td colspan="3">
											<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.encabezado.bajasinies"/></div>
											<div class="titulo"></div>													
										</td>
									</tr>
									<tr>
										<td rowspan="3">
											<s:checkbox id="checkBajaSiniestralGral" name="compensacionesDTO.porSiniestralidad" label="" cssClass="bloquearConf"/>
										</td>
										<td rowspan="3">
											<s:text name="midas.compensaciones.configurador.bajasiniestralidad"/>
										</td>
										<td>
											<table height="100%" width="100%" class="tableStyleNoBorder" id="tableContenedorPorcentajesBajaSini">
												<tr>													
													<td>
														<table id="tableContenedorPorcentajeBajaSini" class="tableStyleNoBorder">
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenaje"/>
																</td>
																<td>
																    <s:textfield title="Porcentaje a distribuir."  name="compensacionesDTO.porceSiniestralidad" id="textPorceBajaSiniestralGral" readonly="#readOnlyD" cssClass="bloquearConf cajaTextoM2 w100 classDisabledPorceSiniestralidad validarPorcentaje"/>
																</td>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																</td>											
														  	</tr>														
														</table>
													</td>												
													<td>
														<table id="tableContenedorPorcentajeBajaSiniAgente" class="tableStyleNoBorder">
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.general.porcagente"/>
																</td>
																<td>
																	<s:textfield onkeyup="checkPorcen2Elementos(this.id,'textPorceBajaSiniPromoGral')" name="compensacionesDTO.porceAgenteSiniestralidad" id="textPorceBajaSiniAgenGral" cssClass="bloquearConf cajaTextoM2 w100 classDisabledBajaSini validarPorcentaje"></s:textfield>
																</td>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																</td>
																</tr>
														</table>
													</td>														
													<td>
														<table id="tableContenedorPorcentajeBajaSiniPrmotor" class="tableStyleNoBorder">
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.general.porcpromotor"/>
																</td>
																<td>
																	<s:textfield onkeyup="checkPorcen2Elementos(this.id,'textPorceBajaSiniAgenGral')" name="compensacionesDTO.porcePromotorSiniestralidad" id="textPorceBajaSiniPromoGral" cssClass="bloquearConf cajaTextoM2 w100 classDisabledBajaSini validarPorcentaje"></s:textfield>
																</td>
																<td>
																	<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																</td>
															</tr>
														</table>
													</td>
													<td>
											  			<table id="tableContenedorBaseCalculoAgreRangos" class="tableStyleNoBorder">
											  				<tr>									  		
												   				<td>
											   						<s:text name="midas.compensaciones.configurador.basecalculo"/>
																</td>
																<td>
																	<s:select id="selectBaseCalcBajaSiniGral" 
													    				name="compensacionesDTO.baseCalculocaSiniestralidad.id" 
												        				cssClass="bloquearConf cajaTextoM2 w100 jQrequired classDisabledBajaSini" 
												        				disabled="#readOnly"
												        				headerKey="-1" headerValue="%{getText('midas.general.seleccione')}" 
													        			list="listBaseCalculoca" listKey="id" listValue="nombre"
												        			/>
											    				</td>																							    				
											    				<s:if test="%{#idRamo == 3}">
											    					<td id ="l_FormulaPrimaRiesgo">
																		<s:text name="midas.compensaciones.configurador.formulasinies"/>
																	</td>
																</s:if>
											    			</tr>								    			
											    		</table>
											  		</td>													
												</tr>
											</table>
										</td>
								  	</tr>		
								  	<tr>
					    				<td colspan="3">
					    					<div atributo="agre" class="classDisabledBajaSini" id="divRangosSiniestralidadGrid">
					    						<div id="rangosSiniestralidadGrid" class="w560 h100 " style="overflow:hidden"></div>
					    					</div>
					    				</td>
					    			</tr>
					    			<tr>
					    			<td>
					    				<table>
					    					<tr>
												<td>
													<div id="divBtnAgregaRangos" class="btn_back w120 classDisabledBajaSini">
														<a type="anchor" atributo="agre" id="btnAgregaRangos" href="javascript:void(0)" class="bloquearConf classDisabledBajaSini icon_guardar ." onclick="agregarRango('rangosSiniestralidadGrid');">
															<s:text name="midas.compensaciones.configurador.rangosinies"/>
														</a>
													</div>
												</td>	
												<s:if test="%{#idRamo == 3}">
													<td>
														<div id="divBtnValidarRangos" class="btn_back w120 classDisabledBajaSini">
															<a type="anchor" atributo="agre" id="btnValidarRangos" href="javascript:void(0)" class="bloquearConf classDisabledBajaSini icon_guardar ." onclick="checkCamposBajaSini(false, 'divRangosSiniestralidadGrid', false);">
																<s:text name="midas.compensaciones.configurador.validarrangos"/>
															</a>
														</div>
													</td>	
												</s:if>		    						
					    					</tr>
					    				</table>
					    			</td>
					    			</tr>							  	
								</table>									
							</td>
						</tr>							
					</table>
				</td>
			</tr>
		</table>
		</div>			
</div>