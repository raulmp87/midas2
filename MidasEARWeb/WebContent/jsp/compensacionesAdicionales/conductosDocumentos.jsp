<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script type="text/javascript">
function fortimaxCom(){
	var compDocu = '<s:property value="compensacionesDTO.compensacionId"/>';
	var agenteDocu = '<s:property value="compensacionesDTO.claveAgenteGral"/>';
	ventanaFortimax('COMPENSACION_AGENTE','' , 'FORTIMAX',compDocu + agenteDocu,compDocu + agenteDocu);
}
</script>
<div id="conductosDocumentos">
			<table width="100%" class="tableStyle" id="tableCondutosAgentesDocumetos">
				<tr>
					<td>
						<table class="tableStyleNoBorder">
							<tr>
								<td>
									<s:text name="Conductos:"/>
								</td>
								<td>
							        <s:select id="selectAgregarAgteGral" 
							            name="compensacionesDTO.caEntidadPersona.id" 
							            cssClass="cajaTextoM2 w100 jQrequired" 
							            disabled="#readOnly"							            
							            list="compensacionesDTO.listEntidadPersonaca" 
							            accesskey="xxx"
							            listKey="id" 
							            listValue="nombres"							            
							        />
							    </td>
							    <td>
							    	<div id="btnAgregarAgente" class="btn_back w120">
							        	<a type="anchor" id="btnAgregarAgenteLink" href="javascript:void(0)" class="icon_guardar ." onclick="javascript: seleccionarAgente(CompensacionAdicional.getAgenteABuscar(),'textClaAgeGral','', 'textNomAgenGral',false,0,idNegocio,'selectAgregarAgteGral' ,false,0,'cargaAgenteEncontrado');">
							            	<s:text name="Agregar Agente"/>
							          	</a>
							        </div>
							    </td>	
								<td>								
								
									<div class="btn_back w120" style="display: inline; float: right; visibility:%{consulta} " id="icon_agregar" >
										<a type="anchor" id="btnDocumentosCompensacionLink" title="Carga de Anexos a Fortimax" href="javascript: void(0);" onclick="fortimaxCom();">
											<s:text name="Documentos" />
										</a>
								   </div>
									
								</td>
							</tr>
						</table>
					</td>
					<td>
					</td>
				</tr>																	
			</table>
		</div>