<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<input id="varContraprestacionId" value="<s:property value='compensacionesDTO.compensacionId' />" type="hidden"/>
<input id="varIsContraprestacion" value="1" type="hidden"/>
<input id="bloquearConfContraId" value="<s:property value="compensacionesDTO.bloquearConfiguracion"/>" type="hidden"/>
<input id="varNombreArchivo" value="<s:property value='compensacionesDTO.presupuestoAnual' />" type="hidden"/>
<input id="varEstatusContraprestacion" value="<s:property value="compensacionesDTO.estatusContraprestacionId"/>" type="hidden"/>
<input id="varEstatusCompensacion" value="<s:property value="compensacionesDTO.estatusCompensacionId"/>" type="hidden"/>
<s:set name="varcomisionAgte" value="compensacionesDTO.aplicaComision"/>


<script type="text/javascript">	
	/**
	*Inicializa la pestaña de Contraprestacion
	*/
	CompensacionAdicional.initContraprestacion();
</script>

<s:set name="idRamo" value="compensacionesDTO.ramo.id"/>
<s:if test="%{#idRamo == 2}">
	<s:set id="readOnlyD" value="true" ></s:set>
</s:if>
<s:set name="idToPoliza" value="compensacionesDTO.polizaId"/>
<s:set name="presupuestoAnual" value="compensacionesDTO.presupuestoAnual"/>
<s:set name="varVidaInd" value="compensacionesDTO.vidaIndiv"/>

<s:form  action="" id="formConfigContraprestacion" name="formConfigContraprestacion">
<div align="center" style="height: 83%; width: 100%;">
	<table class="tableStyle" id="tableAgenteContra" width="100%">
		
		<tr>
			<td>
				<s:text name="midas.compensaciones.configurador.idcompensaciones"/>
			</td>		

			<td>
				<s:textfield name="compensacionesDTO.compensacionId" id="textIdCompensacionesCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
			</td>
		</tr>
	
		<tr>
			<td>
				<s:text name="midas.compensaciones.configurador.claveproveedor"/>
			</td>
			<td>
				<s:textfield name="compensacionesDTO.claveProveedorContra" id="textClaProveContra" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>									
			</td>
			<td>
				<s:text name="midas.compensaciones.nombreProveedor"/>
			</td>
			<td>
				<s:textfield name="compensacionesDTO.nombreProveedorContra" id="textNomProvContra" disabled="true" cssClass="cajaTextoM2 w100"></s:textfield>																		
			</td>
			<td>
				<div class="btn_back w120">
					<a type="anchor" id="btnBuscaProveedor" href="javascript:void(0)" class="bloquearConfContra icon_guardar ." onclick="javascript: seleccionarAgente('PROVEEDOR','textClaProveContra','', 'textNomProvContra',true,'',0,'selectAgregarAgteContra',true,0,'cargaAgenteEncontrado');">
						<s:text name="midas.compensaciones.configurador.buscarproveedor"/>
					</a>
				</div>
			</td>								
		</tr>				
		</table>
		<form></form>
		<table class="tableStyle" id="tableAgenteContra" width="100%">
			<tr>
				<td colspan="5">
					<table class="tableStyleNoBorder"
						id="tableContenedorTipoContratoContra">
						<tr>
							<td><s:text name="midas.compensaciones.configurador.tipocontrato" /></td>
							<td><s:radio
									cssClass="bloquearConfContra radioTipoContratoGrupoContra"
									value="compensacionesDTO.caTipoContrato.id"
									name="compensacionesDTO.caTipoContrato.id"
									id="radioTipoContratoContra"
									list="listTipoContratoca"
									onmouseover="insertTitle(this)"
									listKey="id" listValue="nombre" /></td>
						</tr>
					</table></td>
			</tr>
		</table>

<div select="" style= "height: 82%; width:100%; overflow:auto" hrefmode="ajax-html"  id="contraprestaciones" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div height="100%" width="100%" id="configGralContraprestacion" name="<s:text name="midas.compensaciones.configurador.general"/>" href="http://void">

			<table width="100%" class="tableStyle">				
				<tr>
					<td>						
						<table class="tableStyleNoBorder"  width="100%">
							<tr>
								<td>
									<table width="100%" class="tableStyle" id="tablePrimaContra">
										<tr>
											<td colspan="3">
												<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.encabezado.porprima"/></div>
												<div class="titulo"></div>													
											</td>
										</tr>
										<tr>
											<td rowspan="4">
												<s:checkbox cssClass="bloquearConfContra contraPrest" name="compensacionesDTO.porPrimaContra" id="checkPrimaContra" label=""/>
											</td>
											<td rowspan="4">
												<s:text name="midas.compensaciones.configurador.porprima"/>								
											</td>											
											<td>
												<table width="100%" class="tableStyleNoBorder" id="tableContenedorProcentajesPrimaContra">
													<tr>
														<td>
															<table id="tableContenedorPorcentajePrima" class="tableStyleNoBorder">																
																<tr>
																	<td id ="labelPorcentaje">
																		<s:if test="%{#varcomisionAgte != 1}">
																			<s:text name="midas.compensaciones.configurador.porcenaje"/>
																		</s:if>
																	</td>
																	<td>
																		<s:textfield title="Porcentaje a distribuir." name="compensacionesDTO.porcePrimaContra" id="textPorcePrimaContra" readonly="#readOnlyD" cssClass="bloquearConfContra cajaTextoM2 w100 validarPorcentaje classDisabledPrimaContra"></s:textfield>
																	</td>
																	<td id ="labelSigPorcentaje">
																	<s:if test="%{#varcomisionAgte != 1}">
																		<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																	</s:if>
																</tr>																
															</table>														 	
														</td>
														<s:if test="%{false}">
															<td>
																<table id="tableContenedorMontoPrima" class="tableStyleNoBorder">																
																	<tr>
																		<td>
																			<s:text name="midas.compensaciones.configurador.monto"/>
																		</td>
																		<td>
																			<s:textfield onkeyup="porcentajeOmonto(this.id,'textPorcePrimaContra')" name="compensacionesDTO.montoPrimaContra" id="textMontoPrimaContra" cssClass="bloquearConfContra cajaTextoM2 w100 validarMonto classDisabledPrimaContra"></s:textfield>
																		</td>
																	</tr>
																</table>
															</td>
														</s:if>
														<td>
															<table id="tableBaseCalcPrimaContra" class="tableStyleNoBorder">																
																<tr>
																	<td>
																		<s:text name="midas.compensaciones.configurador.basecalculo"/>												
																	</td>
																	<td>
																		<s:select id="selectBaseCalPrimaContra" 
																		  	name="compensacionesDTO.baseCalculocaPrimaContra.id" 
																		  	cssClass="bloquearConfContra cajaTextoM2 w100 jQrequired classDisabledPrimaContra" 
																		  	disabled="#readOnly"
																		  	headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
																		  	list="listBaseCalculoca" listKey="id" listValue="nombre" 
					       												/>
																	</td>
																</tr>
															</table>
														</td>
														<td>
															<table id="tableTipoMonedaPrimaContra" class="tableStyleNoBorder">																
																<tr>
																	<td>
																		<label><s:text name="midas.compensaciones.configurador.moneda"/></label>
																	</td>
																	<td>
																		<s:select id="selectMonedaPrimaContra"
																				  name="compensacionesDTO.tipoMonedacaPrimaContra.id"
																				  cssClass="bloquearConfContra cajaTextoM2 w100 jQrequired classDisabledPrimaContra"
																				  disabled="#readOnly"
							       												  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
																				  list="listTipoMonedaca" listKey="id" listValue="nombre"
							       												  />
																	</td>																
																</tr>
															</table>
														</td>
													</tr>													
												</table>
											</td>																				
										</tr>
										<tr>
											<s:if test="%{#idRamo == 1}">
												<td>
													<table class="tableStyleNoBorder" id="tableContenedorCondicionesPrimaContra">
														<tr>
															<td align="center">
																<s:text name="midas.compensaciones.configurador.tipocalculo"/>
															</td>																
															<td>
																<s:radio cssClass="bloquearConfContra classDisabledPrimaContra radioCondicionesCalcGrupoContra" id="radioCondicionesCalcContra" value="compensacionesDTO.condicionesCalcPrimaContra.id" name="compensacionesDTO.condicionesCalcPrimaContra.id" list="listCondicionesCalccas" listKey="id" listValue="nombre"/>
															</td>												
														</tr>
													</table>
												</td>
											</s:if>
											<s:elseif test="%{#idRamo == 2}">	
												<td>
													<table class="tableStyle" style="border: none;">	
														<tr>																			
															<td align="center">														
																<s:text name="midas.compensaciones.configurador.tipoprovision"/>
															</td>
															<td colspan="3">	
																<table class="tableStyle" style="border: none;">
																	<tr>										
																		<td>														 
																			<s:radio id="radioProvisiones" value="compensacionesDTO.tipoProvisioncaPrima.id" name="compensacionesDTO.tipoProvisionca.id" list="listTipoProvisionca" listKey="id" listValue="nombre"/>
																		</td>
																	</tr>
																</table>											
															</td>	
														</tr>
													</table>											
												</td>	
											</s:elseif>	
											<s:elseif test="%{#idRamo == 3}">
												<td>
													<table class="tableStyle" style="border: none;">	
														<tr>
															<td align="center">														
																<s:text name="midas.compensaciones.configurador.iva"/>
															</td>
															<td>																					 
																<s:radio id="radioComisionIva" cssClass="classDisabledPrimaContra bloquearConfContra" value="compensacionesDTO.ivaPrima" name="compensacionesDTO.ivaPrima" list="#{'1':'Si','0':'No'}"/>
															</td>
														</tr>
													</table>											
												</td>	
											</s:elseif>		
										</tr>
										<s:if test="%{#idRamo == 3}">
											<tr>
								            	<td>
								                	<div id="productosVidaGrid" class="w300 h100" style="display:none;"></div>
								                </td>
							                </tr>
										</s:if>
											<s:if test="%{#idRamo == 1}">
												<tr>
									            	<td>
									                	<div id="subramosAutosGridContra" class="w350 h100" style="overflow:hidden"></div>
									                </td>
								                </tr>
							                </s:if>
							                <s:elseif test="%{#idRamo == 2}">
							                <tr>
												<td>
													<table class="tableStyleNoBorder">	
											                <tr>
																<td colspan="3">
																	<div id="divSubRamosDaniosContra"  class="h150 " style="overflow:hidden; width: 711px"></div>
																</td>
															</tr>
															<tr>
																<td>
																	<s:text name="midas.compensaciones.configurador.total"/>
																</td>
																<td>
																	<s:textfield name="" id="totalContraprestacionId" cssClass="cajaTextoM2 w100 " disabled="true" ></s:textfield>
																</td>
																<td>
																	<div id="divBtnRecalcularContraprestacion" class="btn_back w120 ">
																		<a type="anchor" atributo="agre" id="btnRecalcularContraprestacionId" href="javascript:void(0)" onclick="CompensacionUtils.parametros.calcularSubRamosDanios()">
																			<s:text name="midas.compensaciones.boton.calcular"/>
																		</a>
																	</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</s:elseif>		
							           </tr>
									</table>
								</td>
							</tr>
														
							<tr>
								<td>									
									<table class="tableStyle" id="tableContenedorBajaSiniContra" width="100%">
										<tr>
											<td colspan="3">
												<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.encabezado.bajasinies"/></div>
												<div class="titulo"></div>													
											</td>
										</tr>
										<tr>
											<td rowspan="3">
												<s:checkbox cssClass="bloquearConfContra" name="compensacionesDTO.porSiniestralidadContra" id="checkBajaSiniestralContra"label=""/>
											</td>
											<td rowspan="3">
												<s:text name="midas.compensaciones.configurador.bajasiniestralidad"/>
											</td>

											<td>											
												<table class="tableStyleNoBorder" id="tableContenedorProcentajesBajaSiniContra">
													<tr>													
														<td>
															<table id="tablePorcentajeBajaSiniContra" class="tableStyleNoBorder">
																<tr>
																	<td>
																		<s:text name="midas.compensaciones.configurador.porcenaje"/>
																	</td>
																	<td>
																		<s:textfield onkeyup="porcentajeOmonto(this.id,'textMontoBajaSiniestralContra')" name="compensacionesDTO.porceSiniestralidadContra" id="textPorceBajaSiniestralContra" cssClass="bloquearConfContra cajaTextoM2 w100 validarPorcentaje classDisabledBajaSiniContra"></s:textfield>
																	</td>
																	<td>
																		<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
																	</td>
																</tr>
															</table>
														</td>

														<td>
															<table id="tableBaseCalcBajaSiniContra" class="tableStyleNoBorder">																
																<tr>
																	<td>
																		<s:text name="midas.compensaciones.configurador.basecalculo"/>												
																	</td>
																	<td>
																		<s:select id="selectBaseCalcBajaSiniContra"
																	  		name="compensacionesDTO.baseCalculocaSiniestralidadContra.id"
																	  		cssClass="bloquearConfContra cajaTextoM2 w100 jQrequired classDisabledBajaSiniContra"
																	  		disabled="#readOnly"
																		  	headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
																	  		list="listBaseCalculoca" listKey="id" listValue="nombre"
																		  />
																	</td>
																	<s:if test="%{#idRamo == 3}">
																		<td id ="l_FormulaPrimaRiesgo_contra">
																			<s:text name="midas.compensaciones.configurador.formulasinies"/>
																		</td>
																	</s:if>
																</tr>
															</table>
														</td>
														<td>
															<table id="tableTipoMonedaBajaSiniContra" class="tableStyleNoBorder">																
																<tr>
																	<td>
																		<label><s:text name="midas.compensaciones.configurador.moneda"/></label>
																    </td>
																    <td>
																    	<s:select id="selectMonedaBajaSiniContra"
																        	name="compensacionesDTO.tipoMonedacaSiniestralidadContra.id"
																            cssClass="bloquearConfContra cajaTextoM2 w100 jQrequired classDisabledBajaSiniContra"
																            disabled="#readOnly"
																            headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
																            list="listTipoMonedaca" listKey="id" listValue="nombre"
																        />
																    </td>
																    <td align="center">														
																		<s:text name="midas.compensaciones.configurador.iva"/>
																	</td>
																	<td>																					 
																		<s:radio id="radioComisionIvaSiniestralidad"  cssClass="classDisabledBajaSiniContra bloquearConfContra" value="compensacionesDTO.ivaSiniestralidad" name="compensacionesDTO.ivaSiniestralidad" list="#{'1':'Si','0':'No'}"/>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									  	<tr>
						    				<td colspan="3">
						    					<div atributo="agre" class="classDisabledBajaSiniContra" id="divRangosSiniestralidadGridContra">
							    					<div id="rangosSiniestralidadGridContra" class="w560 h100 " style="overflow:hidden"></div>
						    					</div>
						    				</td>
						    			</tr>
									  	<tr>
										    <td colspan="3">
									    		<table class="tableStyleNoBorder">
													<tr>
														<td>
														<div id="divBtnAgregaRangosContra" class="btn_back w120 classDisabledBajaSiniContra">
															<a type="anchor" atributo="agre" id="btnAgregaRangosContra" href="javascript:void(0)" class="bloquearConfContra classDisabledBajaSiniContra icon_guardar ." onclick="agregarRango('rangosSiniestralidadGridContra');">
																<s:text name="midas.compensaciones.configurador.rangosinies"/>
															</a>
														</div>
														</td>
														<s:if test="%{#idRamo == 3}">
														<td colspan="2">
															<div id="divBtnValidarRangos" class="btn_back w120 classDisabledBajaSiniContra">
																<a type="anchor" atributo="agre" id="btnValidarRangos" href="javascript:void(0)" class="bloquearConfContra classDisabledBajaSiniContra icon_guardar ." onclick="checkCamposBajaSini(true, 'divRangosSiniestralidadGridContra', false);">
																	<s:text name="midas.compensaciones.configurador.validarrangos"/>
																</a>
															</div>
														</td>	
														</s:if>
													</tr>
												</table>
									    	</td>
									  	</tr>
									</table>									
								</td>
							</tr>	
							
							<s:if test="%{#idRamo == 4}">
								<tr>
									<td>									
										<table class="tableStyle" id="tableContenedorCumplimientoMeta" width="100%">
											<tr>
												<td colspan="3">
													<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.encabezado.meta"/></div>
													<div class="titulo"></div>													
												</td>
											</tr>
											<tr>
												<td rowspan="3">
													<s:checkbox cssClass="bloquearConfContra" name="compensacionesDTO.porCumplimientoDeMeta" id="checkCumplimientoMeta"label=""/>
												</td>
												<td rowspan="3">
													<s:text name="midas.compensaciones.configurador.cumplimiento"/>
												</td>
	
												<td>											
													<table class="tableStyleNoBorder" id="tableContenedorProcentajesCumplimientoMeta">
														<tr>
															<td>
																<table id="tableBaseCalcCumplimientoMeta" class="tableStyleNoBorder">																
																	<tr>
																		<td>
																			<s:text name="midas.compensaciones.configurador.basecalculo"/>												
																		</td>
																		<td>
																			<s:select id="selectBaseCalcCumplimientoMeta"
																		  		name="compensacionesDTO.baseCalculoCumplimientoMeta.id"
																		  		cssClass="bloquearConfContra cajaTextoM2 w100 jQrequired classDisabledCumplimientoMeta"
																		  		disabled="#readOnly"
																			  	headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
																		  		list="listBaseCalculoca" listKey="id" listValue="nombre"
																			  />
																		</td>
																	</tr>
																</table>
															</td>
															<td>
																<table id="tableTipoMonedaCumplimientoMeta" class="tableStyleNoBorder">																
																	<tr>
																		<td>
																			<label><s:text name="midas.compensaciones.configurador.moneda"/></label>
																	    </td>
																	    <td>
																	    	<s:select id="selectMonedaCumplimientoMeta"
																	        	name="compensacionesDTO.caTipoMonedaCumplimientoMeta.id"
																	            cssClass="bloquearConfContra cajaTextoM2 w100 jQrequired classDisabledCumplimientoMeta"
																	            disabled="#readOnly"
																	            headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
																	            list="listTipoMonedaca" listKey="id" listValue="nombre"
																	        />
																	    </td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td>
													<s:form  method="post" action="/compensacionesAdicionales/cargarPresupuestoAnual.action" target="iframeMensajesCompensacionesAdicionales" enctype="multipart/form-data" id="formularioCargaPresupuestoAnualId" name="formularioCargaPresupuestoAnual">
														<table width="100%" id="tableFileUpload">
															<s:if test="%{#presupuestoAnual != null}">
																<tr>															
																	<td style="font-size: 11px;font-family: arial;">Ultimo archivo cargado: ${compensacionesDTO.presupuestoAnual}</td>
																</tr>	
															</s:if>
															<tr>
																<td>	
																	<s:file id="fileUpload" name="fileUpload" cssClass="cajaTextoM2 w300 jQrequired" label="compensacionesDTO.presupuestoAnual"/>
																</td>
																<td>
																	<input id="fileIdConfiguracionBanca" name="idConfiguracionBanca"  value="" type="hidden"/>
																</td>
																<td>
																	<div class="btn_back w120">
																		<a href="javascript:cargarPresupuestoAnual()" class="icon_guardar ." 
																			onclick="">
																			<s:text name="midas.compensaciones.configurador.cargararchivo"/>
																		</a>
																	</div>
																</td>
															</tr>
														</table>
													</s:form>
												</td>
											</tr>
										  	<tr>
							    				<td colspan="3">
							    					<div atributo="agre" class="classDisabledCumplimientoMeta" id="divRangosSiniestralidadCumplimientoMeta">
								    					<div id="rangosSiniestralidadCumplimientoMeta" class="w560 h100 " style="overflow:hidden"></div>
							    					</div>
							    				</td>
							    			</tr>
										  	<tr>
											    <td colspan="3" align="left">
										    		<div id="divBtnAgregaCumplimientoMeta" class="btn_back w120 classDisabledCumplimientoMeta">
														<a type="anchor" atributo="agre" id="btnAgregaCumplimientoMeta" href="javascript:void(0)" class="bloquearConfContra classDisabledCumplimientoMeta icon_guardar ." onclick="agregarRango('rangosSiniestralidadCumplimientoMeta');">
															<s:text name="midas.compensaciones.configurador.rangosinies"/>
														</a>
													</div>
										    	</td>
										  	</tr>
										</table>									
									</td>
								</tr>							
							</s:if>	
													
						</table>
					</td>
				</tr>				
			</table>

</div>

	<s:if test="%{#idRamo == 2}">	
		<div width="100%" id="configUtilidadContra" name="<s:text name="midas.compensaciones.configurador.utilidad"/>" href="http://void" >
			<table class="tableStyle" id="tableContenedorUtilidadContra" width="100%">
				<tr>
					<td colspan="3">
						<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.utilidad"/></div>
						<div class="titulo"></div>													
					</td>
				</tr>
				<s:if test="%{#idToPoliza > 0}">
					<tr>
						<td rowspan="3">
							<s:checkbox cssClass="bloquearConfContra" name="compensacionesDTO.porUtilidad" id="checkUtilidadContra"label=""/>
						</td>
						<td rowspan="3">
							<s:text name="midas.compensaciones.configurador.utilidad"/>
						</td>
						<td>	
							<table class="tableStyleNoBorder" width="100%" style="padding: 20px">							
								<tr>				
									<td align="right">
										<s:text name="midas.compensaciones.configurador.participacionutilidades"/>
									</td>
									
									<td align="right">
										<s:textfield name="compensacionesDTO.porcenUtilidadesUtilidad" id="textPorceParticiUtilidadContra" cssClass="cajaTextoM2 w100 classDisabledPorUtilidad" 
										onkeyup="CompensacionUtils.parametros.porUtilidad.cambioPorcentaje('textPorceParticiUtilidadContra');">
										</s:textfield>
									</td>
									
									<td style="text-align: left; width: 30px" >
										<s:text name="midas.compensaciones.configurador.porcenajesimbolo"/>
									</td>
									
									<td align="right">
										<s:text name="midas.compensaciones.configurador.monto"/>
									</td>
									
									<td align="center">
										<s:textfield name="compensacionesDTO.montoDeLasUtilidades" id="textMontoUtilidadTotal" cssClass="cajaTextoM2 w100 classDisabledPorUtilidad"
										onkeyup="CompensacionUtils.parametros.porUtilidad.cambioProvision();"></s:textfield>
									</td>
									
									<td align="left">			
									</td>
									
								</tr>
								
								
								
								<tr>
									
									<td colspan="3" >
										
									</td>
									
									<td align="right">
										<s:text name="midas.compensaciones.configurador.montoutil"/>
									</td>
									
									<td align="center">
										<s:textfield name="compensacionesDTO.montoUtilidadesUtilidad" id="textMontoParticiUtilidadContra" cssClass="cajaTextoM2 w100" disabled="true"> </s:textfield>
										<input id="montoUtilidadProvision" type="hidden"/>
									</td>
									
									<td align="left">
										<div class="btn_back w120">
											<a href="javascript:void(0)" class="icon_guardar ." id="botonProvisionarUtiliad" >
												<s:text name="midas.compensaciones.configurador.provisionar"/>
											</a>
										</div>
									</td>
								
								</tr>
								
								<tr>
									
									<td colspan="3" >
										
									</td>
									
									<td align="right">
											<s:text name="midas.compensaciones.configurador.pagado"/>
									</td>
									
									<td align="center" >
										<s:textfield name="compensacionesDTO.montoPagoUtilidad" id="textMontoPagado" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
									</td>
									
								</tr>
								
								<tr>
									
									<td colspan="3" >
										
									</td>
									
									<td align="right">
											<s:text name="midas.compensaciones.configurador.devengar"/>
									</td>
									
									<td align="center" >
										<s:textfield name="compensacionesDTO.parcialidadUtilidad" id="textMontoPorDevengar" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
									</td>
									
								</tr>
								
								<tr>														
									<td colspan="3" >
										
									</td>
									
									<td align="right">
										<s:text name="midas.compensaciones.configurador.parcialidad"/> 
									</td>
									
									<td align="center">
										<s:textfield 											
											name="compensacionesDTO.parcialidadUtilidad" 
											id="textMontoParcialidad" 
											cssClass="cajaTextoM2 w100"
											onkeyup="CompensacionUtils.parametros.porUtilidad.cambioMontoParcialidad(this);">
										</s:textfield>
									</td>
									
									<td align="left">
										<div class="btn_back w120 classDisabledPorUtilidad">
											<a href="javascript:void(0)" class="icon_guardar" id="botonSolicitarPagoUtiliad" >
												<s:text name="midas.compensaciones.configurador.solicitarpago"/>
											</a>
										</div>	
									</td>				
								</tr>
							</table>
						</td>	
					</tr>
				</s:if>
				<s:else>
					<tr>
						<td>
							<s:text name="midas.compensaciones.configurador.mensajeutilidad"/>
						</td>
					</tr>
				</s:else>
			</table>	
		</div>
	</s:if>
</div>

<table width="100%" class="tableStyleNoBorder" id="tablePieCompensacion">
	<tr>
		<td>
			<s:include value="conductosDocumentosContraprestacion.jsp"></s:include>
		</td>
	</tr>
	<tr>
		<td>
			<table class="tableStyle" width="100%" id="tableContenedorAutorizaciones">		
				<tr>
					<td>
						<div class="fuenteEncabezado"><s:text name="midas.compensaciones.configurador.encabezado.autoriza"/></div>										
						<div class="titulo"></div>					
			    	</td>
				</tr>
				<tr>
					<td>
						<s:checkboxlist list="listTipoAutorizacionca" value="compensacionesDTO.listTipoAutorizacionId" name="compensacionesDTO.listTipoAutorizacionco" listKey="id" listValue="nombre" ></s:checkboxlist>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table align="left" width="100%" id="tableContenedorHistoricoBotonesGral">		
				<tr>				
					<td>						
						<s:include value="historico.jsp"></s:include>
					</td>
					<td align="right">
						<table id="tableContenedorBotonesGral">
							<tr>
								<td>								
									<div class="btn_back w120">
										<a type="anchor" id="btnAceptarContraGral" href="javascript:void(0)" class="icon_guardar ." onclick="CompensacionUtils.autorizarCompensacion();">
											<s:text name="midas.boton.aceptar"/>
										</a>
										
									</div>									
								</td>
								<td>								
									<div class="btn_back w120">
										
										<a type="anchor" id="btnGuardarConfigGralContra" href="javascript:void(0)" class="bloquearConfContra icon_guardar ." onclick="guardarCompensacionesAdicionales(true, false, null);">
											<s:text name="midas.boton.guardar"/>
										</a>
<!-- 										<a type="anchor" id="btnAceptarContraGral" href="javascript:void(0)" class="icon_guardar ." onclick="CompensacionUtils.autorizarCompensacion();"> -->
<%-- 											<s:text name="Aceptar"/> --%>
<!-- 										</a> -->										
									</div>									
								</td>		
								<td>
									<div class="btn_back w120">
										<a id="btnRegresarConfigGral" href="javascript:void(0)" class="icon_guardar ." onclick="cancelar();">
											<s:text name="midas.boton.regresar"/>
										</a>
									</div>
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="cancelar();">
											<s:text name="midas.boton.cancelar"/>
										</a>
									</div>
								</td>
							</tr>
						</table>
					</td>			
				</tr>
			</table>			
		</td>
	</tr>
</table>
	
</div>
</s:form>

<script type="text/javascript">
	dhx_init_tabbars();
</script>