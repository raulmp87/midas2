<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin"><param>bricks</param></call>
			<call command="enableDragAndDrop"><param>false</param></call> 	 		
        </beforeInit>
        	<column id="idcheck" type="ch" width="35" sort="int"></column>
	        <column id="folio" type="ro" width="70" sort="int" ><s:text name="midas.compensaciones.ordenpago.griddetalle.folio"/></column>
	        <column id="idRecibo" type="ro" width="70" sort="int" ><s:text name="midas.compensaciones.ordenpago.griddetalle.recibo"/></column>
	        <column id="ramo" type="ro" width="70" sort="str"><s:text name="midas.compensaciones.ordenpago.griddetalle.ramo"/></column>
	        <column id="idtopoliza" type="ro" width="135" sort="str"><s:text name="midas.compensaciones.ordenpago.griddetalle.poliza"/></column>  
	        <column id="estatusOrdpagGen" type="ro" width="100" sort="str"><s:text name="midas.compensaciones.ordenpago.griddetalle.estatus"/></column> 
	        <column id="siniestralidad" type="ro" width="120" sort="int"><s:text name="midas.compensaciones.ordenpago.griddetalle.sinies"/></column> 
	        <column id="siniestralidad" type="ro" width="100" sort="int"><s:text name="midas.compensaciones.ordenpago.griddetalle.montoprov"/></column> 
	        <column id="siniestralidad" type="ro" width="120" sort="int"><s:text name="midas.compensaciones.ordenpago.griddetalle.montopag"/></column>	
		<s:if test="tipoAccion == 4 ">
			<column id="excluirIncluir" type="img" width="130" sort="na" align="center"><s:text name="midas.compensaciones.ordenpago.griddetalle.excepcion"/></column>
        </s:if>
        	<column id="idrecibo" type="ro" width="120" sort="int" hidden="true"><s:text name="midas.compensaciones.ordenpago.griddetalle.idRecibo"/></column>
	</head>
	<s:iterator value="listaOrdenPagos_Detalle" var="rowOrdenPagos" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${id}]]></cell>	             
		   	<cell><![CDATA[${id}]]></cell>
		   	<cell><![CDATA[${numFolioRbo}]]></cell>
		   	<s:if test="caRamo.nombre == 1">
			   <cell><s:text name="midas.compensaciones.encabezado.autos"></s:text></cell>
			</s:if>
			<s:elseif test="caRamo.nombre == 2">
			   <cell><s:text name="midas.compensaciones.encabezado.danios"></s:text></cell>
			</s:elseif>
			<s:else >
			   <cell><s:text name="midas.compensaciones.encabezado.vida"></s:text></cell>
			</s:else>
		   	<cell><![CDATA[${numeroPoliza}]]></cell>	   
		<s:if test="estatusOrdpagGen == 1">
		   <cell><s:text name="midas.compensaciones.encabezado.pendiente"></s:text></cell>
		</s:if>
		<s:elseif test="estatusOrdpagGen == 2">
		   <cell><s:text name="midas.compensaciones.encabezado.proceso"></s:text></cell>
	    </s:elseif>
	    <s:else>
			<cell><s:text name="midas.compensaciones.encabezado.generado"></s:text></cell>
		</s:else>                     	  
			<cell><![CDATA[<s:textfield value="%{#rowOrdenPagos.porcentajeSiniestralidad}" name="porcentaje_sin%{#index.count}"  cssClass="cajaTextoM2 w100"/>]]></cell> 
			<cell><![CDATA[${importeCompensacion}]]></cell> 
			<cell><![CDATA[<s:textfield value="%{#rowOrdenPagos.montoBanca}" name="monto_banca%{#index.count}"  cssClass="cajaTextoM2 w100 "/>]]></cell>   	   
		<s:if test="tipoAccion == 4 ">
			<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.compensaciones.boton.excluir"/>^javascript:excluirIncluirOrdenPago("true","${idLiquidacion}","${idRecibo}")^_self</cell>
		</s:if>
			<cell><![CDATA[${idRecibo}]]></cell>
			</row>
	</s:iterator>
	</rows>
