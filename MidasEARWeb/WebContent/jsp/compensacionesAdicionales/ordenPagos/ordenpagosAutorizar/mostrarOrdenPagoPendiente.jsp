<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<s:hidden id="txtTest" name="tipoAccion"></s:hidden>
<input id="tipoAccion" value="0" type="hidden"/>  
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoEstatusPath + "?tipoAccion="+5+"&idParametro="+1;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", null, idField,'ordenPagosModal');
	});
	
function agregarOredenPago(){
	var idchecked = grid.getCheckedRows(1);
	if(idchecked!=""){
		var idRow="";
		var idArray = idchecked.split(",");
		for(i=1;i<=idArray.length;i++){
			if(i!=idArray.length){
				idRow += grid.cellById(idArray[i - 1],0).getValue() + ",";				
			}else{
				idRow += grid.cellById(idArray[i - 1],0).getValue();
			}
		}
		if(idRow!=""){
			if(confirm("\u00BFEsta seguro de Generar la Orden de Pago?")){
				sendRequestJQ(null, "/MidasWeb/compensacionesAdicionales/ordenPago/guardar.action?idsAgregarOp="+idRow,
							targetWorkArea,null);
			}
		}
	}
	else{
		alert("Seleccione el registro a generar");
	}
}

function autorizarPagoCompensaciones(){
	var idchecked = grid.getCheckedRows(1);
	if(idchecked!=""){
		var idRow="";
		var idArray = idchecked.split(",");
		for(i=1;i<=idArray.length;i++){
			if(i!=idArray.length){
				idRow += grid.cellById(idArray[i - 1],0).getValue() + ",";				
			}else{
				idRow += grid.cellById(idArray[i - 1],0).getValue();
			}
		}
		if(idRow!=""){
			if(confirm("\u00BFEsta seguro de Generar la Orden de Pago?")){
				sendRequestJQ(null, "/MidasWeb/compensacionesAdicionales/ordenPago/cotabilizarCheques.action?idsAgregarOp="+idRow,
							targetWorkArea,null);
				//sendRequestJQ(null, "/MidasWeb/compensacionesAdicionales/ordenPago/reporteAutorizarOP.action?idsAgregarOp="+idRow,
					//		targetWorkArea,null);
				window.open("/MidasWeb/compensacionesAdicionales/ordenPago/reporteAutorizarOP.action?idsAgregarOp="+idRow,"asDFG");
			}
		}
	}
	else{
		alert("Seleccione el registro a Autorizar");
	}
}

function checkUncheckAll(){
	var sel = jQuery("#checkAll").attr("checked");
	if(sel){
		reloadGrid();	
		grid.checkAll(true);			
	}else{
		grid.getCheckedRows(1);
		grid.checkAll(false);	
	}
}

function reloadGrid(){	 
        grid.filterBy(1,0);		
}

function allChecksSelected(){	
	var idchecked = grid.getCheckedRows(1);

	if(idchecked!=""){
		grid.filterBy(1,1);
	}else{
		alert("No existe ningun registro marcado");
	}
}
	
</script>
<s:form action="" id="ordenPagosCatalogoForm">
  <s:hidden name="tipoAccion"></s:hidden>
	<table width="1165px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name="Ordenes de Pago Pendientes por Autorizar" />
			</td>
		</tr>
	</table>
</s:form>
	<div id="divCarga" style="position: absolute;"></div>
	<div id="ordenPagosGrid" style="width: 1163.2px; overflow: auto; height: 199px; cursor: default;"></div>
	
	<div class ="w900 inline">
		<div class="w150"><s:checkbox name="checkAll" id="checkAll" value="true"  label="Seleccionar Todos" onclick="checkUncheckAll();" labelposition="left"/></div>
	</div>
<div id="pagingArea"></div>
<div id="infoArea"></div>

<s:if test="tipoAccion!=\"consulta\"">
	<div class="w880" align="right">
		<div class="btn_back w200">
			<a href="javascript: void(0);" 
				onclick="autorizarPagoCompensaciones();">
				<s:text name="Autorizar Pago"/>
			</a>
		</div>
	</div>
</s:if>

