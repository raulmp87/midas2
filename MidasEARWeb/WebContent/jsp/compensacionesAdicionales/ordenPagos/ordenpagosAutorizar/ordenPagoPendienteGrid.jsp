<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <afterInit>
        </afterInit>
        
        <column id="id" type="ro" width="0" sort="int" >Beneficiario</column>
		<column id="idcheck" type="ch" width="35" sort="int"></column>
		<column id="id" type="ro" width="0" sort="int" ><s:text name="IdBeneficiario"/></column>
		<column id="folio" type="ro" width="50" sort="str"><s:text name="Folio"/></column>
		<column id="tipoBeneficiario" type="ro" width="100" sort="str"><s:text name="Tipo de Beneficiarios"/></column>
		<column id="claveNombre" name="" type="ro" width="400" sort="str" ><s:text name="Clave/Nombre"/></column>
		<column id="fecha" type="ro" width="100" sort="date_custom" ><s:text name="Fecha de Corte"/></column>
        <column id="subtotal" type="ro" width="100" sort="int"><s:text name="Subtotal"/></column>
        <column id="iva" type="ro" width="80" sort="int"><s:text name="IVA"/></column>
        <column id="ivaRetenido" type="ro" width="100" sort="int"><s:text name="IVA Retenido"/></column>
        <column id="isr" type="ro" width="80" sort="int"><s:text name="ISR"/></column>
        <column id="importeTotal" type="ro" width="80" sort="int"><s:text name="Total"/></column>
		<column id="estatusFactura" type="ro" width="100" sort="int"><s:text name="Estatus Factura"/></column>
		<column id="estatusOrdenPago" type="ro" width="100" sort="int"><s:text name="Estatus Orden de pago"/></column>
	 <s:if test="tipoAccion!=\"consulta\"">
	 <column id="accionEditar" type="img" width="85" sort="na" align="center"><s:text name="Excepciones"/></column>
	 </s:if>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="80" sort="na"  align="center">
			<s:text name="Acciones"/></column>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
    </head>
	<s:iterator value="listaLiquidacion" var="rowOrdenPagos" status="stat">
		<row id="${index.count}">	 
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${cveTipoEntidad}]]></cell> 
		  <cell><![CDATA[${nombreBeneficiario}]]></cell>
		  <cell><s:date name="fecha" format="dd/MM/yyyy"/></cell>
		  <cell><![CDATA[${subtotal}]]></cell>
		  <cell><![CDATA[${iva}]]></cell>
		  <cell><![CDATA[${ivaRetenido}]]></cell>
		  <cell><![CDATA[${isr}]]></cell>
		  <cell><![CDATA[${importeTotal}]]></cell>
          <cell><![CDATA[${estatusFactura}]]></cell>
          <cell><![CDATA[${estatusOrdenPago}]]></cell>
          	  <s:if test="tipoAccion!=\"consulta\"">
		        <cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="Excepciones"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 4,{"ordenesPagoDTO.id":${rowOrdenPagos.id}})^_self</cell>
			  </s:if>
		  <s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 2,{"ordenesPagoDTO.id":${rowOrdenPagos.id}})^_self</cell>
			 <!--   <cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"   />^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 3,{"ordenesPagoDTO.id":${rowOrdenPagos.id}})^_self</cell> -->
		 </s:if>
	   </row>
	</s:iterator>
	</rows>
