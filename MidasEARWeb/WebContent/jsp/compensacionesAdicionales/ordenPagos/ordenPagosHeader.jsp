<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script
	src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
	var verDetalleOrdenPagosPath = '<s:url action="verDetalle" namespace="/compensacionesAdicionales/ordenPago"/>';
	var listarDetalleOrdenPagosPath = '<s:url action="listarFiltradoDetalle" namespace="/compensacionesAdicionales/ordenPago"/>';
	var guardarOrdenPagosPath = '<s:url action="guardar" namespace="/compensacionesAdicionales/ordenPago"/>';
	var eliminarOrdenPagosPath = '<s:url action="eliminar" namespace="/compensacionesAdicionales/ordenPago"/>';
	var listarOrdenPagosPath = '<s:url action="listar" namespace="/compensacionesAdicionales/ordenPago"/>';
	var listarFiltradoOrdenPagosPath = '<s:url action="listarFiltrado" namespace="/compensacionesAdicionales/ordenPago"/>';
	var listarFiltradoOPagPath = '<s:url action="listarOP" namespace="/compensacionesAdicionales/ordenPago"/>';
	var enviarDocumentoPath = '<s:url action="enviarReportePorCorreo" namespace="/compensacionesAdicionales/ordenPago"/>';
	var listarTodosOrdenPagosPath = '<s:url action="listarTodosOrdenesPago" namespace="/compensacionesAdicionales/ordenPago"/>';
	var listarFiltradoEstatusPath = '<s:url action="listarFiltradoEstatus" namespace="/compensacionesAdicionales/ordenPago"/>';
</script>
<script type="text/javascript">
	jQIsRequired();
	function guardarOrdenPagos() {
		var path = "/MidasWeb/compensacionesAdicionales/ordenPagos/guardar.action?"
				+ jQuery("#ordenPagosForm").serialize()
				+ "&tipoAccion="
				+ parent.dwr.util.getValue("tipoAccion");
		if (validateAll(true)) {
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	function eliminarOrdenPagos() {
		var path = "/MidasWeb/compensacionesAdicionales/ordenPagos/eliminar.action?"
				+ jQuery("#ordenPagosForm").serialize()
				+ "&tipoAccion="
				+ parent.dwr.util.getValue("tipoAccion");
		if (validateAll(true)) {
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	function salirDeOrdenPagos() {
		var url = "/MidasWeb/terceros/catalogos/contenedor/mostrar.action?tabActiva=tipo_servicio";
		if (parent.dwr.util.getValue("tipoAccion") == 2
				|| parent.dwr.util.getValue("tipoAccion") == 3) {
			sendRequestJQAsync(null, url, targetWorkArea, null);
		} else {
			if (confirm("Est\u00E1 a punto de abandonar esta secci\u00F3n, si no ha guardado se perder\u00E1n sus datos. \u00BFDesea continuar?")) {
				sendRequestJQAsync(null, url, targetWorkArea, null);
			}
		}
	}	
</script>