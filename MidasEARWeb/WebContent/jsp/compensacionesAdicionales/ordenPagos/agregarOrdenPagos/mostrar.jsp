<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include
	value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoOPagPath + "?tipoAccion="
				+ tipoAccion;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagGrid", null, idField,'ordenPagosModal');
	});
	
	function enviarReportePorCorreo(){
	var idchecked = grid.getCheckedRows(1);
	if(idchecked!=""){
		var idRow="";
		var idArray = idchecked.split(",");
		for(i=1;i<=idArray.length;i++){
			if(i!=idArray.length){
				idRow += grid.cellById(idArray[i - 1],0).getValue() + ",";				
			}else{
				idRow += grid.cellById(idArray[i - 1],0).getValue();
			}
		}
		if(idRow!=""){	
			if(confirm("Usted enviará el correo para la orden de pago?")){
				sendRequestJQ(null, "/MidasWeb/compensacionesAdicionales/ordenPago/enviarReportePorCorreo.action?idParametro="+idRow,
							null,null);
			}
		}
	}
	else{
		alert("Seleccione el registro a aprobar");
	}
}

function checkUncheckAll(){
	var sel = jQuery("#checkTodos").attr("checked");
	if(sel){
		reloadGrid();	
		grid.checkAll(true);			
	}else{
		grid.getCheckedRows(1);
		grid.checkAll(false);	
	}	
}
function reloadGrid(){	 
        grid.filterBy(1,0);		
}

function allChecksSelected(){	
	var idchecked = grid.getCheckedRows(1);

	if(idchecked!=""){
		grid.filterBy(1,1);
	}else{
		alert("No existe ningun registro marcado");
	}
}

function eliminarLiquidacion(idLiquidacion){
		var path ="/MidasWeb/compensacionesAdicionales/ordenPago/eliminarLiquidacion.action?idLiquidacion="+idLiquidacion+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
</script>
<s:form action="listarOP" id="ordenPagosCatalogoForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="1200px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name="Solicitar Factura" />
			</td>
		</tr>
	</table>
	<div id="divCarga" style="position: absolute;"></div>
	<div id="ordenPagGrid" class="w1150 h150" style="overflow: hidden; width: 1198.2px; height: 250px; cursor: default;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
	
	<div class ="w900 inline">
		<div class="w150"><s:checkbox name="checkTodos" id="checkTodos" value="true"  label="Seleccionar Todos" onclick="checkUncheckAll();" labelposition="left"/></div>
	</div>
	
</s:form>

<s:if test="tipoAccion!=\"consulta\"">
	<div class="w880" align="right">
		<div class="btn_back w130">
			<a href="javascript: void(0);" class="icon_guardar ."
				onclick="enviarReportePorCorreo();"> 
				<s:text name="Solicitar Factura" /> 
			</a>
		</div>
	</div>
</s:if>

