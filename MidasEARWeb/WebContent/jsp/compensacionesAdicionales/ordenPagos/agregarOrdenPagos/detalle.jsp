<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden id="tipoAccion" name="tipoAccion" ></s:hidden>
<s:if test="tipoAccion == 1">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />
	<s:set id="accionJsBoton" value="'if(validateAll(true,'save')){realizarOperacionGenerica(guardarOrdenPagosPath, document.ordenPagosForm , null);}'" />
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="titulo" value="%{getText('midas.catalogos.terceros.tiposervicio.titulo.agregar')}"/>	
	<s:set id="required" value="1"></s:set>
</s:if>
<s:elseif test="tipoAccion == 2 || tipoAccion == 5">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('midas.catalogos.terceros.tiposervicio.titulo')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 3">
	<s:set id="claveTextoBoton" value="%{getText('midas.boton.borrar')}"/>
	<s:set id="accionJsBoton" value="'if(validateAll(true)){realizarOperacionGenerica(eliminarOrdenPagosPath, document.ordenPagosForm , null);}'" />
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="titulo" value="%{getText('mmidas.catalogos.terceros.tiposervicio.titulo.eliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');
	</script>	
</s:elseif>
<s:elseif test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="titulo" value="%{getText('midas.catalogos.terceros.tiposervicio.titulo.editar')}"/>
	<s:set id="required" value="1"></s:set>
</s:elseif>
<s:else>
	<s:set id="titulo" value="%{getText('midas.catalogos.tarifa.descuentoagentemovil.porcentajeDescuento')}"/>
</s:else>
<s:form id="ordenPagosForm">
<s:hidden name="ordenPagos.fechaRegistro"/>
    <s:if test="tipoAccion != 5"> 
	    <div class="titulo w400"><s:text name="#titulo"/></div>
	</s:if>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="midas.catalogos.terceros.tiposervicio.titulo"/>
			</td>
		</tr>
		<tr>
			<th width="130px"><s:text name="midas.catalogos.id" /></th>
			<td colspan="3"><s:textfield name="ordenPagos.id"  readonly="true"  cssClass="cajaTextoM2"/></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.descripcion" /></th>
			<td colspan="3"><s:textfield name="ordenPagos.descripcion"  readonly="#readOnly" id="txtPorcentajeDescuento" cssClass="cajaTextoM2 jQrequired" ></s:textfield></td>
		</tr>
		<tr>
			<th class="jQIsRequired"><s:text name="midas.catalogos.estatus" /></th>
			<td colspan="3">
				<s:select name="ordenPagos.bajaLogica" cssClass="cajaTextoM2 w250 jQrequired" disabled="#readOnly" 
				       list="#{'1':'ACTIVO', '0':'INACTIVO'}"/>
			</td>
		</tr>
	 </table>
	<s:if test="tipoAccion != 5">
	    <span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
	</s:if>
	<table width="98%" class="contenedorFormas no-Border" align="center">	
		<tr>
			<td>
				<div align="right" class="inline" >
				    <s:if test="tipoAccion != 5">				    
						<div class="btn_back w110">
							<a href="javascript: void(0);"
								onclick="javascript:salirDeOrdenPagos();">
								<s:text name="midas.boton.regresar"/>
							</a>
						</div>
					</s:if>
					<s:if test="tipoAccion == 1 || tipoAccion == 4">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="guardarOrdenPagos();">
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					</s:if>	
					<s:if test="tipoAccion == 3">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_borrar"
							onclick="eliminarOrdenPagos();">
							<s:text name="midas.boton.borrar"/>
						</a>
					</div>	
					</s:if>
				</div>
			</td>
		</tr>
	</table>	
</s:form>
<script type="text/javascript">
ocultarIndicadorCarga('indicador');
</script>
