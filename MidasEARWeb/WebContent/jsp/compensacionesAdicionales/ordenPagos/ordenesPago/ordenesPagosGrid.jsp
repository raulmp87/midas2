<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="folio " type="ro" width="50" sort="int" >Folio </column>
        <column id="solicitudCheque" type="ro" width="100" sort="int" >No. Solicitud Cheque</column>
		<column id="ramo" type="ro" width="100" sort="str">Ramo.</column>
		<column id="negocio" type="ro" width="300" sort="str" >Negocio/Contratante</column>
		<column id="tipoBeneficiario" type="ro" width="100" sort="str">Tipo de Beneficiario</column>
		<column id="nombre" type="ro" width="90" sort="str">Nombre</column>
		<column id="estatusFactura" type="ro" width="100" sort="str">Estatus Factura</column>
		<column id="estatusOrdenPago" type="ro" width="80" sort="str">Estatus Orden de Pago</column>
		<column id="fechaCreacion" type="ro" width="80" sort="str">Fecha Creación</column>
		<column id="importeTotal" type="ro" width="80" sort="int">Importe Total</column>				        
	 	<s:if test="tipoAccion!=\"consulta\"">
	 		<column id="acciones" type="img" width="80" sort="na" align="center"><s:text name="Acciones"/></column>
	 	</s:if>
	</head>
	<s:iterator value="listaOrdenPagos" var="rowOrdenPagos" status="index">
		<row id="${index.count}">
		  <cell><![CDATA[${folio}]]></cell>
		  <cell><![CDATA[${solicitudCheque}]]></cell>
          <cell><![CDATA[${ramo}]]></cell>
          <cell><![CDATA[${negocio}]]></cell>  
          <cell><![CDATA[${tipoEntidad}]]></cell>  
          <cell><![CDATA[${contratante}]]></cell> 
          <cell><![CDATA[${estatusFactura}]]></cell>
          <cell><![CDATA[${estatusOrdenpago}]]></cell>
          <cell><![CDATA[${fechaCreacion}]]></cell>
          <cell><![CDATA[${importeTotal}]]></cell>
		  <s:if test="tipoAccion!=\"consulta\"">
		        <cell><s:url value="/img/calcularCotizacion.jpg"/>^<s:text name="midas.boton.consultar"/>^javascript:generarExcelCompensaciones(${folio})^_self</cell>
		 </s:if>
			</row>
	</s:iterator>
	</rows>
