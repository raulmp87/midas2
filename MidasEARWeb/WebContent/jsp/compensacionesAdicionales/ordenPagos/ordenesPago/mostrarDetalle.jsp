<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include
	value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
	
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var identificadorBenId='<s:property value="ordenesPagoDTO.identificador_ben_id"/>';
		var urlFiltro=listarDetalleOrdenPagosPath+"?tipoAccion="+tipoAccion+"&ordenesPagoDTO.identificador_ben_id="+identificadorBenId;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"ordenPagosGridDetalle", null,idField,'ordenPagosModal');
	 });
	 
	function excluirIncluirOrdenPago(esExcluir, folio, idRecibo){
		var path ="/MidasWeb/compensacionesAdicionales/ordenPago/actualizarEstatusDetalle.action?esExcluir=" + esExcluir +"&folio="+folio+"&idRecibo="+idRecibo+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	 
	 
</script>
<div id="ordenPagosGridDetalle" class="w700 h200" style="overflow:hidden"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>

<s:if test="tipoAccion!=\"consulta\"">
	<div class="w500" align="right">
		<div class="btn_back w150">
			<a href="javascript: void(0);" 
				onclick="operacionGenerica(verContenedorPath,1);"> <s:text
					name="Regresar" /> </a>
		</div>
	</div>
</s:if>
