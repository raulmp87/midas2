<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <afterInit>
        </afterInit>
        
		<column id="id" type="ro" width="0" sort="int" ><s:text name="Id Liquidacion"/></column>
		<column id="ramo" type="ro" width="100" sort="str"><s:text name="Ramo"/></column>
		<column id="negocioContratante" name="" type="ro" width="400" sort="str" ><s:text name="Negocio/Contartante"/></column>
        <column id="tipoBeneficiario" type="ro" width="80" sort="int"><s:text name="TipoBeneficiario"/></column>
	    <column id="nombre" type="ro" width="70" sort="int" ><s:text name="Nombre"/></column>
        <column id="estatusFactura" type="ro" width="70" sort="int"><s:text name="Estatus Factura"/></column>
		<column id="estatusOrdenPago" type="ro" width="70" sort="int"><s:text name="Estatus Orden Pagos"/></column>
		<column id="fechaCrecion" type="ro" width="70" sort="int" ><s:text name="Fecha Creacion"/></column>
        <column id="importeTotal" type="ro" width="70" sort="int"><s:text name="Importe Total"/></column>
	 <s:if test="tipoAccion!=\"consulta\"">
	 <column id="accionEditar" type="img" width="85" sort="na" align="center"><s:text name="Excepciones"/></column>
	 </s:if>
		<s:if test="tipoAccion!=\"consulta\"">
		<column id="accionVer" type="img" width="80" sort="na"  align="center"><s:text name="Acciones"/></column>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
    </head>
	<s:iterator value="listaOrdenPagos" var="rowOrdenPagos" status="stat">
		<row id="${index.count}">	 
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${ramo}]]></cell>
		  <cell><![CDATA[${negocioContratante}]]></cell> 
		  <cell><![CDATA[${tipoBeneficiario}]]></cell> 
		  <cell><![CDATA[${nombre}]]></cell> 
		  <cell><![CDATA[${estatusFactura}]]></cell>
		  <cell><![CDATA[${estatusOrdenPago}]]></cell>
		  <cell><![CDATA[${fechaCrecion}]]></cell>
		  <cell><![CDATA[${importeTotal}]]></cell>         
          	  <s:if test="tipoAccion!=\"consulta\"">
		        <cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="Liquidacion"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 4,{"ordenesPagoDTO.id":${rowOrdenPagos.id}})^_self</cell>
			  </s:if>
		  <s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 2,{"ordenesPagoDTO.id":${rowOrdenPagos.id}})^_self</cell>
			   <!--  <cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 3,{"ordenPagos.id":${rowOrdenPagos.id},"idRegistro":${rowOrdenPagos.id},"idTipoOperacion":20})^_self</cell> --> 
		 </s:if>
			</row>
	</s:iterator>
	</rows>
