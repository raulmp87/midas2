<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarTodosOrdenPagosPath;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenesPagosGrid", null, idField,'ordenPagosModal');
	});
	
	function generarExcelCompensaciones(idLiquidacion){
		var reporteUrl = "/MidasWeb/compensacionesAdicionales/ordenPago/generarExcelDetallePagos.action"+"?idLiquidacion="+idLiquidacion;
		window.open(reporteUrl,"generarExcelDetallePagos");
	
	}
	function listarTodos(){
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarTodosOrdenPagosPath;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", document.ordenPagosCatalogoForm, idField,'ordenPagosModal');
		listarFiltradoGenerico(urlFiltro, 'ordenPagosGrid',document.ordenPagosCatalogoForm,'${idField}','ordenPagosModal')
	}
	function limpiarTodos(){
		jQuery("input:text").val("");
		jQuery(".txtHidden").val("");
	}
</script>
<s:form action="listarFiltrado" id="ordenPagosCatalogoForm">
  <s:hidden name="tipoAccion"></s:hidden>	
	<table class="contenedorFormas w1100">
		<tr>
			<td class="titulo" colspan="6"><s:text name="Busqueda de Órdenes de Pago de Compensaciones Adicionales" /></td>
		</tr>		
		<tr>				
		<tr>
			<td>
				<table class="contenedorFormas no-border">
					<tr>
						<th width="">Folio:</th>
						<td width=""><s:textfield name="folio" id="txtFolio" readonly="false" cssClass="cajaTextoM2 w100"></s:textfield></td>
					</tr>
					<tr>
						<th width="85px"><s:text name="Ramo:" /></th>
						<td >
				             <s:select name="ramo" list="filtros.ramos" id="ramo"
				                      listKey = "id" listValue="nombre" 
				                      headerKey="0" headerValue="Seleccione"></s:select>
				        </td>
					</tr>
					<tr>
						<th>
						Negocio:
						             </th>
						<td> 
						<s:textfield name="negocioDescription" id="txtNegocioText" readonly="true" cssClass="cajaTextoM2 w150"></s:textfield> 		
					    <s:hidden name="negocio" id="txtNegocioHidden" readonly="true" cssClass="cajaTextoM2 w150 txtHidden"></s:hidden> 
						</td>		
						<td>
							<div class="btn_back w110">
								<a href="javascript:void(0);" class="icon_buscar" onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','txtNegocioHidden', 'txtNegocioText',true,0,0,this,false,4,'agregaValorSeleccionado',': Negocio');deseleccionarCheck('negociosTodos')">
									<s:text name="Buscar"/>
								</a>
							</div>
						</td>			
					</tr>
					<tr>
						<th>
						Contratante
						            </th>
						<td>
						 <s:textfield name="contratante" id="txtContratante_OrdenPagosText"	cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
					     <s:hidden id="txtContratante_OrdenPagosHidden"	cssClass="cajaTextoM2 w150 txtHidden" disabled="false"></s:hidden>
					  	</td>
						<th>
						<div class="btn_back w110">
							<a href="javascript:void(0);" class="icon_buscar" onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','txtContratante_OrdenPagosHidden', 'txtContratante_OrdenPagosText',true,0,0,this,false,5,'agregaValorSeleccionado',': Contratante');">
								<s:text name="Buscar "/>
							</a>
						</div>
						</th>
					</tr>
				</table>
			</td>
			<td valign="top">
				<table class="contenedorFormas no-border">
					<tr>
						<th>
							Agente
													</th>
						<th>
						  <s:textfield name="agenteDescription" id="agenteText" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield> 
						  <s:hidden name="agente" id="agenteHidden" cssClass="cajaTextoM2 w150 txtHidden" disabled="false"></s:hidden> 		
						</th>
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS', '', 'agenteHidden', 'agenteText',true,0,0,this,false,1,'agregaValorSeleccionado',': Agente');">
										<s:text name="Buscar Agente"/>
								</a>
							</div>
						</th>
				   </tr>
					<tr>
						<th>
						Proveedor
						           </th>
						<th>
						    <s:textfield name="proveedorDescription" id="idTxtProvRepCaText" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						    <s:hidden name="proveedor" id="idTxtProvRepCaHidden" cssClass="cajaTextoM2 w150 txtHidden" disabled="false"></s:hidden>
						</th>									
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','idTxtProvRepCaHidden', 'idTxtProvRepCaText',true,0,0,this,false,3,'agregaValorSeleccionado',': Proveedor');">
									<s:text name="Buscar Proveedor"/>
								</a>
							</div>
						</th>
					</tr>
					<tr>
						<th>
						Promotor
						          </th>
						<th>
						    <s:textfield name="promotorDescription" id="idTxtPromRepCaText" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						    <s:hidden name="promotor" id="idTxtPromRepCaHidden" cssClass="cajaTextoM2 w150 txtHidden" disabled="false"></s:hidden>
						</th>						
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', 'idTxtPromRepCaHidden', 'idTxtPromRepCaText',true,0,0,this,false,2,'agregaValorSeleccionado',': Promotor');">
									<s:text name="Buscar Promotor"/>
								</a>
							</div>
						</th>
					</tr>
					<tr>
						<th>
						Gerencia
						          </th>
						<th>
						    <s:textfield name="gerenciaDescription" id="idTxtGerRepCaText" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						    <s:hidden name="gerencia" id="idTxtGerRepCaHidden" cssClass="cajaTextoM2 w150 txtHidden" disabled="false"></s:hidden>
						</th>									
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','idTxtGerRepCaHidden', 'idTxtGerRepCaText',true,0,0,this,false,3,'agregaValorSeleccionado',': Gerencia');">
									<s:text name="Buscar Gerencia"/>
								</a>
							</div>
						</th>
					</tr>
					
				</table></td>
				
				<td valign="top">
				<table class="contenedorFormas no-border">
					<tr>
						<th width=""><s:text name="Estatus Factura" /></th>
						<td width="">
						 <s:select name="estatusFactura" list="filtros.estatusFacura" id="ramo"
				                      listKey = "id" listValue="description" 
				                      headerKey="0" headerValue="Seleccione"></s:select>
						</td>
					</tr>
					<tr>
						<th width=""><s:text name="Estatus Órden Pago" /></th>
						<td width="">
						 <s:select name="estatusOrdenPago" list="filtros.estatusOrdenpago" id="ramo"
				                      listKey = "id" listValue="description" 
				                      headerKey="0" headerValue="Seleccione"></s:select>
						</td>
					</tr>
				</table></td>
				</tr>
				<tr>
					<td colspan="15" align="right">
						<div class="btn_back w110">
							<a href="javascript:void(0);"  onclick="javascript:limpiarTodos()">
								<s:text name="Limpiar"/>
							</a>
						</div>
					</td>
					<td colspan="15" align="right">
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
						onclick="listarFiltradoGenerico(listarTodosOrdenPagosPath, 'ordenesPagosGrid',document.ordenPagosCatalogoForm,'${idField}','ordenPagosModal');">
						<s:text name="Buscar" /> </a>
						</div>
					</td>
				</tr>
	</table>
	<div class="titulo w800"><s:text name="Lista de Órdenes de Pago"/></div>
	<div id="divCarga" style="position: absolute;"></div>
	<div id="ordenesPagosGrid" class="w1110 h150"  style="overflow: hidden; width: 1100.2px; height: 250px; cursor: default;"></div>

</s:form>
<div id="pagingArea"></div>
<div id="infoArea"></div>

