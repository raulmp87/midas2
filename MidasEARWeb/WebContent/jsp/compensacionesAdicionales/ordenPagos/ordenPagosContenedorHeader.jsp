<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- <s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include> --%>
<script type="text/javascript">
	var verContenedorPath = '<s:url action="mostrarContenedorOrdenPagos" namespace="/compensacionesAdicionales/ordenPago"/>';
	var mostrarOrdenPagoPath = '<s:url action= "mostrarOrdenPagos" namespace= "/compensacionesAdicionales/ordenPago"/>';
	var mostrarPagosOrdenPath = '<s:url action= "mostrarPagosOrden" namespace= "/compensacionesAdicionales/ordenPago"/>';
	var tabInfo_general=null;
	var contenedorTab=null;
	var grid=null;
	var gridEntretenimientosAgente;
	var gridHijosAgente;
	var TIPOACCION_CONSULTAHISTORICO = "5";
	/**
	 * Funcion para inicializar tabs, si es una alta, deshabilita el resto de los tabs hasta que se guarde la primer pestania
	 */	
	function incializarTabs(){
		contenedorTab=window["ordenPagosTabBar"];
		//Nombre de todos los tabs		
		var tabs=["orden_pagos_calculos","orden_pagos"];
		
		for(var i=0;i<tabs.length;i++){
			var tabName=tabs[i];
			var catalogoProveniente = jQuery("#moduloOrigen").val();
			var tab=contenedorTab[tabName];
			jQuery("div[tab_id*='"+tabName+"'] span").bind('click',{value:i},function(event){
				if(dwr.util.getValue("tipoAccion")==1||dwr.util.getValue("tipoAccion")==4){
					event.stopPropagation();
					parent.mostrarMensajeConfirm("Está a punto de abandonar la sección, ¿Desea continuar?","20","obtenerFuncionTab("+event.data.value+")",null,null,null);
				}
				else{
					obtenerFuncionTab(event.data.value);
				}
			});
		}
	}
	function  verOrdenPago() {
		sendRequestJQ(null, mostrarOrdenPagoPath, 'contenido_orden_pagos_calculos',
				'limpiarOrdenPago();');
	}
	function  muestraOrdenPago() {
		sendRequestJQ(null, mostrarPagosOrdenPath+"?idParametro="+2, 'contenido_orden_pagos',
				'limpiarOrdenPago();');
	}
	function limpiarOrdenPago() {
		//limpiarDiv('contenido_orden_pagos_calculos');
	}
	function obtenerFuncionTab(tab) {
		var tipoOperac = 50;
		if (tab == 0) {
			sendRequestJQ(null, mostrarCargaExcelTarifasAutosPath,
					targetWorkArea, 'dhx_init_tabbars();');
		} else if (tab == 1) {
			sendRequestJQ(null, mostrarCargaExcelTarifasAutosPath,
					targetWorkArea, 'dhx_init_tabbars();');
		} else if (tab == 2) {
			sendRequestJQ(null, mostrarCargaExcelTarifasAutosPath,
					targetWorkArea, 'dhx_init_tabbars();');
		} else if (tab == 3) {
			sendRequestJQ(null, mostrarCargaExcelTarifasAutosPath,
					targetWorkArea, 'dhx_init_tabbars();');
		} else {
			sendRequestJQ(null, mostrarCargaExcelTarifasAutosPath,
					targetWorkArea, 'dhx_init_tabbars();');
		}
	}
</script>