<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosContenedorHeader.jsp"></s:include>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<script type="text/javascript">
	jQuery(function() {
		dhx_init_tabbars();
	});
</script>
<div select="<s:property value='tabActiva'/>" hrefmode="ajax-html" id="ordenPagosTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">
	<div width="*" id="orden_pagos_calculos" name="Cálculos" href="http://void" extraAction="javascript: limpiarDiv('contenido_orden_pagos'); verOrdenPago();"></div>
	<div width="*" id="orden_pagos" name="Órden de Pago" href="http://void" extraAction="javascript: limpiarDiv('contenido_orden_pagos_calculos');muestraOrdenPago();"></div>	
 </div>