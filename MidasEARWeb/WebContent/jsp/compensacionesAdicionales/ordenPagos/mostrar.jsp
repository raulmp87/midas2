<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<s:hidden id="txtTest" name="tipoAccion"></s:hidden>
<input id="tipoAccion" value="0" type="hidden"/>  
<script type="text/javascript">
	jQuery(function() {
		limpiarFormulario();
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoOrdenPagosPath + "?tipoAccion="+ 0;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", null, idField,'ordenPagosModal');
		
	});
	
function agregarOredenPago(){
	var idchecked = grid.getCheckedRows(1);
	if(idchecked!=""){
		var idRow="";
		var idArray = idchecked.split(",");
		for(i=1;i<=idArray.length;i++){
			if(i!=idArray.length){
				idRow += grid.cellById(idArray[i - 1],0).getValue() + ",";				
			}else{
				idRow += grid.cellById(idArray[i - 1],0).getValue();
			}
		}
		if(idRow!=""){
			if(confirm("\u00BFEsta seguro de Generar la Orden de Pago?")){
				sendRequestJQ(null, "/MidasWeb/compensacionesAdicionales/ordenPago/guardar.action?idsAgregarOp="+idRow,
							targetWorkArea,null);
			}
		}
	}
	else{
		alert("Seleccione el registro a generar");
	}
}

function checkUncheckAll(){
	var sel = jQuery("#checkAll").attr("checked");
	if(sel){
		reloadGrid();	
		grid.checkAll(true);			
	}else{
		grid.getCheckedRows(1);
		grid.checkAll(false);	
	}
}


function reloadGrid(){	 
        grid.filterBy(1,0);		
}

function allChecksSelected(){	
	var idchecked = grid.getCheckedRows(1);

	if(idchecked!=""){
		grid.filterBy(1,1);
	}else{
		alert("No existe ningun registro marcado");
	}
}

function calcularOrdenPagos(idField){
	var $elementClone = jQuery(document.ordenPagosCatalogoForm).clone();
    $elementClone.find(':input').removeAttr('disabled');
	    listarFiltradoGenerico(listarFiltradoOrdenPagosPath+ "?"+jQuery("#ordenPagosCatalogoForm").serialize()+ "&tipoAccion=5", 'ordenPagosGrid',null,idField,'ordenPagosModal');
    $elementClone.detach();
}

function deseleccionarChecks(){
	  jQuery("#agentesTodos").attr('checked',false);
	  jQuery("#promotoresTodos").attr('checked',false);
	  jQuery("#proveedoresTodos").attr('checked',false);
	  jQuery("#contratanteTodos").attr('checked',false);
	  jQuery("#gerenciaTodos").attr('checked',false);
	  jQuery("#negociosTodos").attr('checked',false);
	  //jQuery("#ramoAuto").attr('checked',false);	  
	}
	function deseleccionarCheck(id){
	  jQuery("#"+id).attr('checked',false);
	}
	function deseleccionarCheckConceptoAll(){
		jQuery("#cons_porPrima").attr('checked',false);
		jQuery("#chek_cons_porBS").attr('checked',false);
		jQuery("#check_cons_porCumpMeta").attr('checked',false);
		jQuery("#check_cons_porDerPoliza").attr('checked',false);
		jQuery("#check_cons_porUtilidad").attr('checked',false);
		//jQuery("#ramoAuto").attr('checked',false);
			
	}
	
	function deseleccionarcheckRamoAll(){
		jQuery("#ramoAuto").attr('checked','');
		jQuery("#chek_ramoDanios").attr('checked','');
		jQuery("#chek_ramoVida").attr('checked','');
		//jQuery("#ramoAuto").attr('checked',false);
		
	}
	
	function limpiarFormulario(){
		
			jQuery("#txtAgentes_OrdenPagos").attr('value',"");
			jQuery("#agentesTodos").attr('checked',true);	
 			jQuery("#txtPromotor_OrdenPagos").attr('value',"");
 			jQuery("#promotoresTodos").attr('checked',true);
 			jQuery("#txtProveedor_OrdenPagos").attr('value',"");
 			jQuery("#proveedoresTodos").attr('checked',true);
 			jQuery("#txtContratante_OrdenPagos").attr('value',"");
 			jQuery("#contratanteTodos").attr('checked',true);
 			jQuery("#txtGerencia_OrdenPagos").attr('value',"");
 			jQuery("#gerenciaTodos").attr('checked',true);
 			jQuery("#txtNegocio_OrdenPagos").attr('value',"");
 			jQuery("#negociosTodos").attr('checked',true);
 			jQuery("#txtFechaInicio").attr('value',"");
 			jQuery("#txtFechaFinal").attr('value',"");
 			jQuery("#txtGrupo_OrdenPagos").attr('value',"");
 			jQuery("#checkGrupo").attr('checked',true);
 			jQuery("#ramoTodos").attr('checked',true);
 			jQuery("#cons_Todos").attr('checked',true);
 			jQuery("#ramoAuto").attr('checked',false);
 			jQuery("#chek_ramoDanios").attr('checked',false);
 			jQuery("#chek_ramoVida").attr('checked',false);
 			jQuery("#cons_porPrima").attr('checked',false);
 			jQuery("#chek_cons_porBS").attr('checked',false);
 			jQuery("#check_cons_porCumpMeta").attr('checked',false);
 			jQuery("#check_cons_porDerPoliza").attr('checked',false);
 			jQuery("#check_cons_porUtilidad").attr('checked',false);			
	}

	function deseleccionarCheckConceptoAll(){
			jQuery("#cons_porPrima").attr('checked',false);
			jQuery("#chek_cons_porBS").attr('checked',false);
			jQuery("#check_cons_porCumpMeta").attr('checked',false);
			jQuery("#check_cons_porDerPoliza").attr('checked',false);
			jQuery("#check_cons_porUtilidad").attr('checked',false);
	        
		}
			
</script>
<s:form action="" id="ordenPagosCatalogoForm">
	<table width="1165px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name="Generar Órden Pagos" />
			</td>
		</tr>
		<tr>
				
	<th width="45px"><s:text name="Compactar Por: " /> 
		</tr>		
		<tr>		
        <th width="100px"><s:text  name="Ramo : " /></th>
        	<td width="100"><s:checkbox name="ramoAuto" id="ramoAuto"
					cssClass="checkbox w85"></s:checkbox></td>					
			<th width="75px"><s:text name="Autos" />
			<td><s:text name="Agentes:" />
			</td>
			  <td>
			      <s:textfield name="agente" id="txtAgentes_OrdenPagos"	cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
			  </td>
			<td width="30px">
			      <s:checkbox name="agentesTodos" id="agentesTodos" fieldValue="true"  cssClass="checkbox w30"></s:checkbox>
			</td>
			<th width="45px"><s:text name="Todos" />
			<td colspan="2" align="right">			
		<td>
			<div class="btn_back w110">
			<a href="javascript:void(0);" class="icon_buscar" onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '','txtAgentes_OrdenPagos',true,0,0,this,false,1,'agregaValorSeleccionadoOrdenesPago',': Agente');deseleccionarCheck('agentesTodos')">
					<s:text name="Buscar "/>
				</a>
			</div>
		</td>
			<td><s:text name="Grupo:" />
			</td>
			<td>
			     <s:textfield name="grupo" id="txtGrupo_OrdenPagos"cssClass="cajaTextoM2 w75" disabled="false"></s:textfield>
			</td>
			     <td width="30px"><s:checkbox name="checkGrupo" id="checkGrupo" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" />
 
		<td>
			<div class="btn_back w110">
				<a href="javascript:void(0);" class="icon_buscar" >
					<s:text name="Buscar "/>
				</a>
			</div>
		</td>
		</tr>

		<!-- fila2 -->
		<tr>
			<th width="100px"><s:text name="" /></th>
			<td width="100px"><s:checkbox name="ramoDanios"
					id="chek_ramoDanios" cssClass="checkbox w85"></s:checkbox></td>

			<th width="75px"><s:text name="Daños" />
			<td><s:text name="Promotor:" /></td>

			<td>
			    <s:textfield name="promotor" id="txtPromotor_OrdenPagos" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
			</td>

			<td width="20px"><s:checkbox name="promotoresTodos" 
					id="promotoresTodos" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" />
			<td colspan="2" align="right">
			
		<td>
			<div class="btn_back w110">
				<a href="javascript:void(0);" class="icon_buscar" onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'txtPromotor_OrdenPagos',true,0,0,this,false,2,'agregaValorSeleccionadoOrdenesPago',': Promotor');deseleccionarCheck('promotoresTodos')">
					<s:text name="Buscar "/>
				</a>
			</div>
		</td>		
			<th width="70px"><s:text name=" Concepto:" />
			<td colspan="1" align="right">
			       <s:checkbox name="cons_porPrima" id="cons_porPrima"
					cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Por Prima" />
		</tr>

		<!-- fila3 -->
		<tr>
			<th width="100px"><s:text name="" /></th>
			<td width="w100"><s:checkbox name="ramoVida"
					id="chek_ramoVida" cssClass="checkbox w85"></s:checkbox></td>
			<th width="75px"><s:text name="Vida" />
			<td><s:text name="Proveedor:" />
			</td>
			<td>
			 <s:textfield name="proveedor" id="txtProveedor_OrdenPagos" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
			</td>
			<td width="20px"><s:checkbox name="proveedoresTodos" 
					id="proveedoresTodos" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" />
			<td colspan="2" align="right">
			
		<td>
			<div class="btn_back w110">
				<a href="javascript:void(0);" class="icon_buscar" onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'txtProveedor_OrdenPagos',true,0,0,this,false,3,'agregaValorSeleccionadoOrdenesPago',': Proveedor');deseleccionarCheck('proveedoresTodos')">
					<s:text name="Buscar "/>
				</a>
			</div>
		</td>
		
			<td colspan="2" align="right">
			 <s:checkbox name="cons_porBS" id="chek_cons_porBS" cssClass="checkbox w30"></s:checkbox></td>
			 <th width="45px"><s:text name="Por Baja Siniestralidad" />
		</tr>

		<!--fila4 -->
		
		<tr>
			<th width="100px"><s:text name="" /></th>
			<td><s:checkbox name="ramoTodos" onclick="deseleccionarcheckRamoAll();"
					id="ramoTodos" cssClass="checkbox w85"></s:checkbox></td>
			<th width="10px"><s:text name="Todos" />
			<td><s:text name="Contratante:" />
			</td>
			<td>
			    <s:textfield name="contratante" id="txtContratante_OrdenPagos"	cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
			</td>
			<td width="20px"><s:checkbox name="contratanteTodos" id="contratanteTodos" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" />
			<td colspan="2" align="right">
			
		<td>
			<div class="btn_back w110">
				<a href="javascript:void(0);" class="icon_buscar" onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'txtContratante_OrdenPagos',true,0,0,this,false,5,'agregaValorSeleccionadoOrdenesPago',': Contratante');deseleccionarCheck('contratanteTodos')">
					<s:text name="Buscar "/>
				</a>
			</div>
		</td>

			<td colspan="2" align="right">
			        <s:checkbox name="cons_porCumpMeta"	id="check_cons_porCumpMeta" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Cumplimiento de Meta" />
		</tr>
		<!-- 		fila 5 -->
		<tr>
			<th width="100px"><s:text name="Fecha Inicio" /></th>
		
				<td><sj:datepicker name="fechaInicio"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="4"
					cssClass="w50 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			   </td>

			<th width="75px"><s:text name="" />
			<td><s:text name="Gerencia:" />
			</td>
			<td><s:textfield name="gerencia" id="txtGerencia_OrdenPagos"
					cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
			</td>
			<td width="20px"><s:checkbox name="gerenciaTodos"
					id="gerenciaTodos" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" />
			<td colspan="2" align="right">

		<td>
			<div class="btn_back w110">
				<a href="javascript:void(0);" class="icon_buscar" onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS','','', 'txtGerencia_OrdenPagos',true,0,0,this,false,6,'agregaValorSeleccionadoOrdenesPago',': Gerencia');deseleccionarCheck('gerenciaTodos')">
					<s:text name="Buscar "/>
				</a>
			</div>
		</td>
		
			<td colspan="2" align="right">
			<s:checkbox name="cons_porDerPoliza"
					id="check_cons_porDerPoliza" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Derecho de Poliza" />
		</tr>
		
		<!-- 		fila 6 -->
		<tr>
			<th width="100px"><s:text name="Fecha Final" /></th>
			<%-- 			<td width="30px"><s:checkbox name="checkRamoTodos" id="checkRamoTodos" --%>
			<%-- 					cssClass="checkbox w30"></s:checkbox></td> --%>

			<td ><sj:datepicker name="fechaFinal"
					id="txtFechaFinal" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="4"
					cssClass="w50 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
			
			<th width="75px"><s:text name="" />
			<td><s:text name="Negocio:" />
			</td>
			<td><s:textfield name="negocio" id="txtNegocio_OrdenPagos"
					cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
			</td>
			<td width="20px"><s:checkbox name="negociosTodos" 
					id="negociosTodos" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" />
			<td colspan="2" align="right">

		<td>
			<div class="btn_back w110">
				<a href="javascript:void(0);" class="icon_buscar" onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'txtNegocio_OrdenPagos',true,0,0,this,false,4,'agregaValorSeleccionadoOrdenesPago',': Negocio');deseleccionarCheck('negociosTodos')">
					<s:text name="Buscar"/>
				</a>
			</div>
		</td>
				
			<td colspan="2" align="right">	
			<s:checkbox name="cons_porUtilidad"
					id="check_cons_porUtilidad" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px">
			<s:text name="Utilidad" />
		</tr>
		
		<tr>
		<td colspan="12" align="right">		
			  <s:checkbox name="cons_Todos" onclick="deseleccionarCheckConceptoAll();"
					id="cons_Todos" cssClass="checkbox w30"></s:checkbox></td>
			<th width="45px"><s:text name="Todos" /> 
		</tr>
		<tr>
			<td colspan="15" align="right">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="calcularOrdenPagos('${idField}')">
						<s:text name="Calcular" /> </a>
			</div>
			</td>
						
			<td colspan="15" align="right">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="limpiarFormulario()">
						<s:text name="Limpiar" /> </a>
			</div>
			</td>
		</tr>
	</table>
</s:form>
	<div id="divCarga" style="position: absolute;"></div>
	<div id="ordenPagosGrid" style="width: 1163.2px; overflow: auto; height: 199px; cursor: default;"></div>
	
	<div class ="w900 inline">
		<div class="w150"><s:checkbox name="checkAll" id="checkAll" value="true"  label="Seleccionar Todos" onclick="checkUncheckAll();" labelposition="left"/></div>
	</div>
<div id="pagingArea"></div>
<div id="infoArea"></div>

<s:if test="tipoAccion!=\"consulta\"">

	<div class="w880" align="right">
		<div class="btn_back w200">
			<a href="javascript: void(0);" 
				onclick="agregarOredenPago('${idField}');">
				<s:text name="Generar Orden de Pago"/>
			</a>
		</div>
	</div>
</s:if>

