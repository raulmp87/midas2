<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <afterInit>
        </afterInit>
        
        <column id="id" type="ro" width="0" sort="int" >Beneficiario</column>
		<column id="idcheck" type="ch" width="35" sort="int"></column>
		<column id="id" type="ro" width="0" sort="int" ><s:text name="IdBeneficiario"/></column>
		<column id="tipoBeneficiario" type="ro" width="100" sort="str"><s:text name="Tipo de Beneficiario"/></column>
		<column id="claveNombre" name="" type="ro" width="400" sort="str" ><s:text name="Clave/Nombre"/></column>
        <column id="porPrima" type="ro" width="80" sort="int"><s:text name="Por Prima"/></column>
	    <column id="porBS" type="ro" width="70" sort="int" ><s:text name="Por B.S"/></column>
        <column id="porDP" type="ro" width="70" sort="int"><s:text name="Por D.P"/></column>
		<column id="consepCumMet" type="ro" width="70" sort="int"><s:text name="Concepto por Meta"/></column>
		<column id="porUtil" type="ro" width="70" sort="int" ><s:text name="Por Utilidad"/></column>
        <column id="total" type="ro" width="70" sort="int"><s:text name="Total"/></column>
	 <s:if test="tipoAccion!=\"consulta\"">
	 <column id="accionEditar" type="img" width="85" sort="na" align="center"><s:text name="Excepciones"/></column>
	 </s:if>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="80" sort="na"  align="center">
			<s:text name="Acciones"/></column>
			  <column id="accionBorrar" type="img" width="30" sort="na"/> 
		</s:if>
    </head>
	<s:iterator value="listaOrdenPagos" var="rowOrdenPagos" status="stat">
		<row id="${index.count}">	 
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${id}]]></cell>
		  <cell><![CDATA[${tipobeneficiario}]]></cell>
		  <cell><![CDATA[${claveNombre}]]></cell> 
		  <cell><![CDATA[${importePrima}]]></cell> 
		  <cell><![CDATA[${importeBs}]]></cell> 
		  <cell><![CDATA[${importeDerpol}]]></cell>
		  <cell><![CDATA[${importeCumMeta}]]></cell>
		  <cell><![CDATA[${importeUtilidad}]]></cell>
		  <cell><![CDATA[${importeTotal}]]></cell>
          	  <s:if test="tipoAccion!=\"consulta\"">
		        <cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="Excepciones"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 4,{"ordenesPagoDTO.id":${rowOrdenPagos.id},"idParametro":"3"})^_self</cell>
			  </s:if>
		  <s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleOrdenPagosPath, 2,{"ordenesPagoDTO.id":${rowOrdenPagos.id},"idParametro":"3"})^_self</cell>
		 </s:if>
	   </row>
	</s:iterator>
	</rows>
