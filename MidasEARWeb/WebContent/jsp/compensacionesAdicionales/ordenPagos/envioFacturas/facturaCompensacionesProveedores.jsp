<%@page	language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<%-- <script  type="text/javascript" src="<s:url value='/js/midas2/catalogos/envioFacturaAgentes/envioFacturaAgentes.js'/>"></script> --%>
<script type="text/javascript">
	jQuery(function(){
		var urlFiltro="/MidasWeb/compensacionesAdicionales/ordenPago/listarPendientesOrdenPago.action";
		listarFiltradoGenerico(urlFiltro,"ordenPagGrid", null,null,null);
 	});
 	
 	function importarArchivoCa(){
			if(dhxWins != null) 
				dhxWins.unload();

			dhxWins = new dhtmlXWindows();
			dhxWins.enableAutoViewport(true);
			dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
			var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
			adjuntarDocumento.setText("Carga de Facturas");
			adjuntarDocumento.button("minmax1").hide();
			adjuntarDocumento.button("park").hide();
			adjuntarDocumento.setModal(true);
			adjuntarDocumento.center();
			adjuntarDocumento.denyResize();
			adjuntarDocumento.attachHTMLString("<div id='vault'></div>");				

			var vault = new dhtmlXVaultObject();
		    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
		    vault.setFilesLimit(1);
		    
		    vault.onAddFile = function(fileName) { 
		        var ext = this.getFileExtension(fileName); 
		        if (ext != "xml") { 
		           alert("Solo puede importar archivos XML (.xml). "); 
		           return false; 
		        }else{
		        	return true; 
		        }
		     }; 
		     
		    vault.create("vault");
		    vault.setFormField("claveTipo", "17");
		        
		    vault.onBeforeUpload = function(files){
		    	jQuery("#file1").attr("name", "facturaXml");
		    }
		    
		    jQuery("#buttonUploadId").unbind("click");
		    jQuery("#buttonUploadId").click(function(){
		        
		    	blockPage();
				jQuery.ajaxFileUpload({									
					url: "/MidasWeb/compensacionesAdicionales/ordenPago/subirArchivoXml.action?&idAgente=92586",
					secureuri:false,
					fileElementId: "file1",
					dataType: 'text',
					success: function(data, estatus){
						unblockPage()
						parent.dhxWins.window("divContentForm").close();
						alert(data);
						var url = "/MidasWeb/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa.action";
						sendRequestJQ(null, url, targetWorkArea, null);
					},
					error: function(data, estatus){
						unblockPage()
						parent.dhxWins.window("divContentForm").close();
						alert(data);
						var url = "/MidasWeb/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa.action";
						sendRequestJQ(null, url, targetWorkArea, null);
					}
				});
			});
		}
</script>
<style type="text/css">
.errors {
	background-color:#FFCCCC;
	border:1px solid #CC0000;
	width:400px;
	margin-bottom:8px;
}
.success {
	background-color:#DDFFDD;
	border:1px solid #009900;
	width:200px;
}
</style>
<s:set id="titulo" value="%{getText('midas.suspensiones.tituloEnvioFactura')}"/>
<div class="titulo w800"><s:text name="#titulo"/></div>
<div id="divContentInfoAge" >
<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>
<s:if test="hasActionMessages()">
   <div class="success">
      <s:actionmessage/>
   </div>
</s:if>
<s:hidden id="isAgentePromotor" value="%{isAgentePromotor}" name="isAgentePromotor"></s:hidden>
<table>
	<tbody>
		<tr>
        	<td>
				<table class="contenedorConFormato">
				    <tbody>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.numeroAgente" />: </th>
				    		<td>
				    			<s:property value="idAgente" />
				    			<s:hidden name="idAgente" id="hiddenIdAgente" />
				    		</td>
				    		<th><s:text name="midas.prestamosAnticipos.numeroCedula" />: </th>
				    		<td><s:property value="numeroCedula" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.venceCedula" />: </th>
				    		<td><s:property value="vencimientoCedula" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.tipoAgente" />: </th>
				    		<td><s:property value="tipoAgente" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.nombreAgente" />: </th>
				    		<td><s:property value="nombreAgente" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.centroOperacion" />: </th>
				    		<td><s:property value="centroOperacion" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.gerencia" />: </th>
				    		<td><s:property value="gerencia" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.estatus" />: </th>
				    		<td><s:property value="estatus" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.rfc" />: </th>
				    		<td><s:property value="rfc" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.ejecutivo" />: </th>
				    		<td><s:property value="ejecutivo" /></td>
				    	</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div id="divContentForm" >
</div>
<br/>
<div id="divButtons">
	<table width="800" style="font-family: arial; font-size: 11px;">
		<tr>
		<s:if test="%{isAgentePromotor ==true}">
				<td>
					<font color="#FF6600">* </font>
					<s:text name="midas.fuerzaventa.configBono.agente" />:
					<s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
							onchange="addAgenteProduccion();" style="display:none;" />
				</td>
				<td>
					<s:select name="idAgente"
						style="font-family: arial; font-size: 11px; width:250px;"
						id="agentes" 
						list="agenteList" 
						listKey="idAgente" 
						listValue="nombreCompleto" 
						headerKey="%{getText('midas.general.defaultHeaderKey')}" 
						headerValue="%{getText('midas.general.seleccione')}"
						onchange="setAgente(this.value)"/>
				</td>
			</s:if>
			<td>
							<div class="btn_back w170">
								<a id="loadButton" href="javascript: void(0);" class="icon_buscar" onclick="importarArchivoCa()">Cargar Factura XML</a>
							</div>
			</td>
			<s:if test="%{isAgentePromotor !=true}">
				<td></td>
				<td></td>
				<td></td>
			</s:if>

			<td>
			</td>
			<td>
		</tr>
		<tr>
			<td>
			</td>
		</tr>
	</table>
</div>
<br/>
<div class="titulo w800"><s:text name="Lista de Órdenes de Pago"/></div>
<div id="ordenPagGrid" align="center" width="920px" height="200px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<div id="divContentModal" style="display: none;">
	<div id="divContentTableDet" align="center" width="560px" height="200px" style="background-color:white;overflow:hidden;">
	</div>
	<div id="pagingAreaDet"></div><div id="infoAreaDet"></div>
</div>