<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>8</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="id" type="ro" hidden="true" width="50" sort="int"><s:text name="midas.suspensiones.tituloEnvioFactura"/></column>
		<column id="" type="ro" width="50" sort="str"><s:text name="Envio"/></column>
		<column id="estatusIco" type="img" width="60" sort="na" align="center">Estatus</column>
		<column id="" type="ro" width="80" sort="str"><s:text name="Estatus Desc"/></column>
		<column id="" type="ro" width="80" sort="str"><s:text name="Fecha Envio"/></column>
		<column id="" type="ro" width="300" sort="str"><s:text name="Nombre Archivo"/></column>
		<column id="" type="ro" width="90" sort="str"><s:text name ="Fecha Modifica"/></column>
		<column id="" type="ro" width="90" sort="str"><s:text name ="Annio Mes"/></column>
		<column id="" type="ro" width="90" sort="str">Usuario</column>
		<column id="accionVer" type="img" width="60" sort="na" align="center">Detalle</column>
	</head>
	<s:iterator value="listaEnvios" var="envioFacturaAgente" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${envioFacturaAgente.idEnvio}]]></cell>
			<cell> 
				<s:if test="%{#envioFacturaAgente.status.valor=='Aceptada'}">
					<s:url value="/img/icons/ico_aceptar.gif"/>^Aceptada
				</s:if>	
				<s:else>
					<s:url value="/img/icons/ico_rechazar2.gif"/>^Rechazada
				</s:else>
			</cell>
			<cell><![CDATA[${envioFacturaAgente.status.valor}]]></cell>
			<cell><![CDATA[${envioFacturaAgente.fechaEnvioString}]]></cell>
			<cell><![CDATA[${envioFacturaAgente.nombreArchivo}]]></cell>
			<cell><![CDATA[${envioFacturaAgente.fechaModificaString}]]></cell>	
			<cell><![CDATA[${envioFacturaAgente.honorarios.anioMes}]]></cell>
			<cell><![CDATA[${envioFacturaAgente.usuario}]]></cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:cargaDetalle(${envioFacturaAgente.idEnvio})^_self</cell>
		</row>
	</s:iterator>
</rows>