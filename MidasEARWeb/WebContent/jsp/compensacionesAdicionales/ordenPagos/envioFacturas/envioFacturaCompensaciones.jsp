<%@page	language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script  type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js'/>"></script>
<script type="text/javascript">
var urlFiltro="/MidasWeb/compensacionesAdicionales/ordenPago/listarPendientesOrdenPago.action";       
	
	jQuery(function(){
		listarPendientes();
 	});
	function init(){
		sendRequestJQ(null,"/MidasWeb/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa.action?idAgente="+jQuery("#inputIdAgente").val()+"&estatusOrdenPago="+jQuery("#estatusOrdenPago").val(),'contenido_compensaciones',null);
	}
	function listarPendientes(){
		listarFiltradoGenerico(urlFiltro+"?idAgente="+jQuery("#hiddenIdAgente").val()+"&estatusOrdenPago="+jQuery("#estatusOrdenPago").val(),"ordenPagGrid", null,null,null);
	}
	
 	
 	function importarArchivoCa(idAgente,id){
			if(dhxWins != null) 
				dhxWins.unload();

			dhxWins = new dhtmlXWindows();
			dhxWins.enableAutoViewport(true);
			dhxWins.setImagePath("/MidasWeb/img/dhxwindow/");
			var adjuntarDocumento = dhxWins.createWindow("divContentForm", 34, 100, 440, 265);
			adjuntarDocumento.setText("Carga de Facturas");
			adjuntarDocumento.button("minmax1").hide();
			adjuntarDocumento.button("park").hide();
			adjuntarDocumento.setModal(true);
			adjuntarDocumento.center();
			adjuntarDocumento.denyResize();
			adjuntarDocumento.attachHTMLString("<div id='vault'></div>");				

			var vault = new dhtmlXVaultObject();
		    vault.setImagePath("/MidasWeb/img/dhtmlxvault/");
		    vault.setFilesLimit(1);
		    
		    vault.onAddFile = function(fileName) { 
		        var ext = this.getFileExtension(fileName); 
		        if (ext != "xml") { 
		           alert("Solo puede importar archivos XML (.xml). "); 
		           return false; 
		        }else{
		        	return true; 
		        }
		     }; 
		    
		    vault.create("vault");
		    vault.setFormField("claveTipo", "17");
		        
		    vault.onBeforeUpload = function(files){
		    	jQuery("#file1").attr("name", "facturaXml");
		    }
		    
		    jQuery("#buttonUploadId").unbind("click");
		    jQuery("#buttonUploadId").click(function(){
		        
		    	blockPage();
				jQuery.ajaxFileUpload({									
					url: "/MidasWeb/compensacionesAdicionales/ordenPago/subirArchivoXml.action?idAgente="+idAgente+"&idLiquidacion="+id,
					secureuri:false,
					fileElementId: "file1",
					dataType: 'text',
					success: function(data, estatus){
						unblockPage()
						parent.dhxWins.window("divContentForm").close();
						alert(data);
						var url = "/MidasWeb/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa.action";
						sendRequestJQ(null, url, 'contenido_compensaciones', 'listarPendientes();');
					},
					error: function(data, estatus){
						unblockPage();
						parent.dhxWins.window("divContentForm").close();
						alert(data);
						var url = "/MidasWeb/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa.action";
						sendRequestJQ(null, url, 'contenido_compensaciones', 'listarPendientes();');
					}
				});
			});
		}
</script>
<style type="text/css">
.errors {
	background-color:#FFCCCC;
	border:1px solid #CC0000;
	width:400px;
	margin-bottom:8px;
}
.success {
	background-color:#DDFFDD;
	border:1px solid #009900;
	width:200px;
}
</style>
<s:set id="titulo" value="%{getText('midas.suspensiones.tituloEnvioFactura')}"/>
<div class="titulo w800"><s:text name="#titulo"/></div>
<div id="divContentInfoAge" >
<s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if>
<s:if test="hasActionMessages()">
   <div class="success">
      <s:actionmessage/>
   </div>
</s:if>
<s:hidden id="isAgentePromotor" value="%{isAgentePromotor}" name="isAgentePromotor"></s:hidden>
<s:if test="isAgente || agente1.id!=null">
<table>
	<tbody>
		<tr>
        	<td>
				<table class="contenedorConFormato">
				    <tbody>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.numeroAgente" />: </th>
				    		<td>
				    			<s:property value="idAgente" />
				    			<s:hidden name="idAgente" id="hiddenIdAgente" />
				    		</td>
				    		<th><s:text name="midas.prestamosAnticipos.numeroCedula" />: </th>
				    		<td><s:property value="numeroCedula" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.venceCedula" />: </th>
				    		<td><s:property value="vencimientoCedula" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.tipoAgente" />: </th>
				    		<td><s:property value="tipoAgente" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.nombreAgente" />: </th>
				    		<td><s:property value="nombreAgente" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.centroOperacion" />: </th>
				    		<td><s:property value="centroOperacion" /></td>
				    	</tr>
				    	<tr>  
				    		<th><s:text name="midas.prestamosAnticipos.gerencia" />: </th>
				    		<td><s:property value="gerenciaAgente" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.estatus" />: </th>
				    		<td><s:property value="estatus" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.rfc" />: </th>
				    		<td><s:property value="rfc" /></td>
				    		<th><s:text name="midas.prestamosAnticipos.ejecutivo" />: </th>
				    		<td><s:property value="ejecutivo" /></td>
				    	</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</s:if>
<s:if test="isProveedor || prestadorServicio.id!= null">
<table>
	<tbody>
		<tr>
        	<td>
				<table class="contenedorConFormato">
				    <tbody>
				    	<tr>
				    		<th><s:text name="midas.compensaciones.numeroProveedor" />: </th>
				    		<td>
				    			<s:property value="idAgente" />
				    			<s:hidden name="idAgente" id="hiddenIdAgente" />
				    		</td>
				    		<th><s:text name="midas.compensaciones.nombreProveedor" />: </th>
				    		<td><s:property value="nombreAgente" /></td>
				    	</tr>
				    	<tr>
				    		<th><s:text name="midas.prestamosAnticipos.rfc" />: </th>
				    		<td><s:property value="rfc" /></td>
				    	</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</s:if>
</div>
<div id="divContentForm" >
</div>
<br/>
<div id="divButtons">
	<table width="800" style="font-family: arial; font-size: 11px;">
		<tr>
			<td>
				<div>Escribe Clave:
				<s:if test="isProveedor || isAgente">
					<input id="inputIdAgente" type="text" class="ui-corner-all" readonly="true" value="${idAgente}" >
				</s:if>
				<s:if test="!isProveedor && !isAgente">
					<input id="inputIdAgente" type="text" class="ui-corner-all" value="${idAgente}" >
				</s:if>
				</div>
			</td>
			<td>
			<s:select id="estatusOrdenPago" name="estatusOrdenPago" label="Estatus:" list="#{'0':'Todos','1':'Pendientes'}"></s:select>
			</td>
			<td>
					<div class="btn_back w110">
						<a id="initButton" href="javascript: void(0);" class="icon_buscar" onclick="init()">Buscar</a>
					</div>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td align="center">

					<td style="font-size: 11px;font-weight: bold;"><s:property value="msgFacturasPendientes" /></td>
			</td>
		</tr>
	</table>
</div>
<br/>
<div class="titulo w800"><s:text name="Órdenes de Pago"/></div>
<div id="ordenPagGrid" align="center" width="920px" height="200px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<div id="divContentModal" style="display: none;">
	<div id="divContentTableDet" align="center" width="560px" height="200px" style="background-color:white;overflow:hidden;">
	</div>
	<div id="pagingAreaDet"></div><div id="infoAreaDet"></div>
</div>