<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>8</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		<column id="" type="ro" width="50" sort="str" align="center">Folio</column>
		<column id="" type="edn" width="60" sort="int" format="0,000.00" align="right">Subtotal</column>
		<column id="" type="edn" width="80" sort="int" format="0,000.00" align="right">IVA</column>
		<column id="" type="edn" width="90" sort="int" format="0,000.00" align="right">IVA Ret</column>
		<column id="" type="ro" width="90" sort="str">ISR</column>
		<column id="" type="edn" width="90" sort="int" format="0,000.00" align="right">Total</column>
		<column id="" type="ro" width="130" sort="str">Nombre del Archivo XML</column>
		<column id="" type="ro" width="110" sort="str">Fecha de Envio</column>
		<column id="" type="ro" width="110" sort="str" align="center">Estatus</column>
		<column id="accionVer" type="img" width="60" sort="na" align="center">Accion</column>
	</head>
	<s:iterator value="listaLiquidacion" var="liquidacion" status="index">
		<row id="${liquidacion.id}">
			<cell><![CDATA[${liquidacion.id}]]></cell>
			<cell><![CDATA[${liquidacion.subtotal}]]></cell>
			<cell><![CDATA[${liquidacion.iva}]]></cell>
			<cell><![CDATA[${liquidacion.ivaRetenido}]]></cell>
			<cell><![CDATA[${liquidacion.isr}]]></cell>	
			<cell><![CDATA[${liquidacion.importeTotal}]]></cell>
			<cell><![CDATA[]]></cell>
			<cell><![CDATA[]]></cell>
			<cell><![CDATA[${liquidacion.estatus}]]></cell>
			<s:if test="%{#liquidacion.estatus=='TRAMITE-FACTURA'}">
					<cell><s:url value="/img/icons/ico_agregar.gif"/>^Subir XML^javascript:importarArchivoCa(${liquidacion.identificadorBenId},${liquidacion.id})^_self</cell>
				</s:if>	
				<s:else>
					<cell><s:url value="/img/b_mas_agregar_disabled.gif"/></cell>
				</s:else>
		</row>
	</s:iterator>
</rows>