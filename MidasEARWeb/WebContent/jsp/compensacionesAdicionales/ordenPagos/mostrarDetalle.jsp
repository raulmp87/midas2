<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include
	value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
	
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
var idParametro = '<s:property value="idParametro"/>';
var ordenPagoId='<s:property value="ordenesPagoDTO.id"/>';
var verContenedorPath;
if(idParametro=='1'){  
	verContenedorPath ='<s:url action="mostrarPagosOrdenPediente" namespace="/compensacionesAdicionales/ordenPago"/>';	
}else if(idParametro=='2'){
	verContenedorPath ='<s:url action="mostrarContenedorOrdenPagos" namespace="/compensacionesAdicionales/ordenPago"/>';
}else{
    verContenedorPath ='<s:url action="mostrarContenedorOrdenPagos" namespace="/compensacionesAdicionales/ordenPago"/>';
}

	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var urlFiltro=listarDetalleOrdenPagosPath+"?tipoAccion="+tipoAccion+"&ordenesPagoDTO.id="+ordenPagoId;
		var idField='<s:property value="idField"/>';
	 	listarFiltradoGenerico(urlFiltro,"ordenPagosGridDetalle", null,idField,'ordenPagosModal');
	 });
	 
	function excluirIncluirOrdenPago(esExcluir, idLiquidacion, idRecibo){
		var path ="/MidasWeb/compensacionesAdicionales/ordenPago/actualizarEstatusDetalle.action?esExcluir=" + esExcluir +"&idLiquidacion="+idLiquidacion+"&idRecibo="+idRecibo+"&tipoAccion="+parent.dwr.util.getValue("tipoAccion");
		if(validateAll(true)){
			sendRequestJQ(null, path, targetWorkArea, null);
		}
	}
	function guardarDatos(){
		var idchecked = grid.getCheckedRows(0);
		if(idchecked!=""){
			var folios="",recibos="",siniestralidad="",montos="",datos="";
			var idArray = idchecked.split(",");
			console.log(idArray);
			for(i=1;i<=idArray.length;i++){
					datos += grid.cellById(idArray[i - 1],10).getValue() + ",";//recibos
					datos += grid.cellById(idArray[i - 1],1).getValue() + ",";//folios
					datos += 0+",";//foliosOrdenPago
					datos += jQuery('input[id=porcentaje_sin'+idArray[i - 1]+']').val()+",";//siniestralidad
					datos += jQuery('input[id=monto_banca'+idArray[i - 1]+']').val()+",";//montos
					if(grid.cellById(idArray[i - 1],3).getValue()=="AUTOS")
						datos += 1+";";//ramos
						else 
							if(grid.cellById(idArray[i - 1],3).getValue()=="DAÑOS")
								datos += 2+";";//ramos
								else
									datos += 3+";";//ramos
			}
			if(datos!=""){
				if(confirm("\u00BFEsta seguro de Guardar?")){
					console.log(datos);
					//la variable ordenPagoId se refiere a idLiquidacion de la orden de pago.
					sendRequestJQ(null, "/MidasWeb/compensacionesAdicionales/ordenPago/guardarSiniestralidad.action?idsAgregarOp="+datos+"&tipoAccion=4"+"&idLiquidacion="+ordenPagoId,
								targetWorkArea,null);
				}
			}
		}
		else{
			alert("Seleccione un registro ");
		}
	}
	function checkUncheckAll(){
		var sel = jQuery("#checkAll").attr("checked");
		if(sel){
			reloadGrid();	
			grid.checkAll(true);			
		}else{
			grid.getCheckedRows(1);
			grid.checkAll(false);	
		}
	}

	function reloadGrid(){	 
	        grid.filterBy(1,0);		
	}

	function allChecksSelected(){	
		var idchecked = grid.getCheckedRows(1);

		if(idchecked!=""){
			grid.filterBy(1,1);
		}else{
			alert("No existe ningun registro marcado");
		}
	}
	 
	 
</script>
<div id="ordenPagosGridDetalle" style="width: 900px; overflow: auto; height: 199px; cursor: default;"></div>	
<div class ="w900 inline">
	<div class="w150"><s:checkbox name="checkAll" id="checkAll" value="true"  key="midas.check.seleccionartodos" onclick="checkUncheckAll();" labelposition="left"/></div>
</div>
<div id="pagingArea"></div>
<div id="infoArea"></div>

<s:if test="tipoAccion!=\"consulta\"">
	<div class="h200" align="left">
		<div class="btn_back w150" style="display: inline-block;" align="left">
			<a href="javascript: void(0);" 
				onclick="guardarDatos();"> <s:text
					name="midas.boton.guardar" /> </a>
		</div>
		<div class="btn_back w150" style="display: inline-block;" align="left">
			<a href="javascript: void(0);" 
				onclick="operacionGenerica(verContenedorPath,1);"> <s:text
					name="midas.boton.regresar" /> </a>
		</div>
	</div>
</s:if>

