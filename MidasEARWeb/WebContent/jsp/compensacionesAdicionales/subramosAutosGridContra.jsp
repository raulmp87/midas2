<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="colLineaNegocioNombre" type="ro" width="200" sort="na"><s:text name="Línea de Negocio"/></column>
		<column id="colLineaNegocioValor" type="ro" width="150" sort="na"><s:text name="% comisión Fija"/></column>       	
	</head>
	<s:iterator value="lineasInicial" var="lineas" status="index">
		<row id="${index.count}">
			<cell><s:property value="valorDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[<input id="${valorId}_" class="lineaDescripcion" type="hidden" value="${valorDescripcion}"> <s:textfield value="%{#lineas.valorPorcentaje}" name="%{#lineas.valorPorcentaje}" onkeyup="CompensacionUtils.parametros.validarPorcentaje(this.id);" id="%{#lineas.valorId}" cssClass="cajaTextoM2 w100 lineaNegocioContra classDisabledPrimaContra"/>]]></cell>
		</row>
	</s:iterator>
</rows>