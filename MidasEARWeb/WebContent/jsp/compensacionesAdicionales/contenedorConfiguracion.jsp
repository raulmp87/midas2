<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<!-- HEADER COMPENSACIONES -->
<s:include value="/jsp/compensacionesAdicionales/compensacionesHeader.jsp"></s:include>

<s:hidden id="varTipoAccion" name="tipoAccion"/>
<s:hidden id="varInvocacionConfigurador" name="invocacionConfigurador"/>
<input id="varIdNegocio" value="<s:property value='idNegocio' />" type="hidden"/>
<input id="varIdCotizacion" value="<s:property value='idCotizacion' />" type="hidden"/>
<input id="varIdCotizacionEndoso" value="<s:property value='idCotizacionEndoso' />" type="hidden"/>
<input id="varIdNegocioVida" value="<s:property value='idNegocioVida' />" type="hidden"/>
<input id="varIdNegocioVidaIsNew" value="<s:property value='idNegocioVida' />" type="hidden"/>
<input id="varIdNegocioAutosIsNew" value="<s:property value='compensacionesDTO.idNegocio' />" type="hidden"/>
<input id="varIdConfiguracionBanca" value="<s:property value='idConfiguracionBanca' />" type="hidden"/>
<input id="varIdPoliza" value="<s:property value='idPoliza' />" type="hidden"/>
<input id="varRamo" value="<s:property value='compensacionesDTO.ramo.identificador' />" type="hidden"/>
<input id="varCompensacionNueva" value="<s:property value='compensacionNueva' />" type="hidden"/>
<input id="jsonCompensacionId" value="" type="hidden" />
<input id="jsonContraprestacionId" value="" type="hidden" />
<input id="varFnTecnico" value="<s:property value='compensacionesDTO.fnTecnico' />" type="hidden"/>
<input id="varFnJuridico" value="<s:property value='compensacionesDTO.fnJuridico' />" type="hidden"/>
<input id="varFnAgente" value="<s:property value='compensacionesDTO.fnAgente' />" type="hidden"/>

<s:set name="idRamo" value="compensacionesDTO.ramo.id"/>
<input id="idNegocio" value="<s:property value='compensacionesDTO.idNegocio' />" type="hidden"/>
<s:set name="varVidaInd" value="compensacionesDTO.vidaIndiv"/>

<input id="varEstatus" value="<s:property value='compensacionesDTO.estatusId' />" type="hidden"/>
<input id="varVidaIndividual" value="<s:property value='compensacionesDTO.vidaIndiv' />" type="hidden"/>
<input id="openConfigurador" value="1" type="hidden"/>
<input id="varComisionAgente" value="<s:property value="compensacionesDTO.aplicaComision"/>" type="hidden"/>


<div id="divPrincipal" align="center" style="height: 85%; width: 98%; margin-left: 10px;"  >

	<table  id="filtrosM2" width="98%" align="center">
	 	<tr>
			<td class="titulo" colspan="6"><s:text name="midas.compensaciones.contenedor.encabezado.compcont"/></td>
		</tr>
		
		<tr>
			
			<td>
				<s:text name="midas.compensaciones.ordenpago.griddetalle.ramo"/>
			</td>		

			<td>
				<s:textfield name="compensacionesDTO.ramo.nombre" id="textRamoCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
			</td>
			
			<td>							
				<s:if test="%{#idRamo == 1}">
					<s:text name="midas.compensaciones.contenedor.idnegocio"/>
				</s:if>
				
				<s:elseif test="%{#idRamo == 2}">
				   <s:text name="midas.compensaciones.contenedor.nocotizacion"/>
				</s:elseif>
				
				<s:elseif test="%{#idRamo == 3}">
				   <s:text name="midas.compensaciones.contenedor.idnegociovida"/>
				</s:elseif>
				
				<s:elseif test="%{#idRamo == 4}">
				   <s:text name="midas.compensaciones.contenedor.idconfbanca"/>
				</s:elseif>
			</td>
				
			<td>
			
				<s:if test="%{#idRamo == 1}">
					<s:textfield name="compensacionesDTO.idNegocio" id="textIdNegocioCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
				</s:if>
				
				<s:elseif test="%{#idRamo == 2}">
				   <s:textfield name="idCotizacion" id="textIdCotizacionCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
				</s:elseif>
				
				<s:elseif test="%{#idRamo == 3}">
				   <s:textfield name="idNegocioVida" id="textIdNegocioVidaCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
				</s:elseif>
				
				<s:elseif test="%{#idRamo == 4}">
				   <s:textfield name="idConfiguracionBanca" id="textIdConfiguracionBancaCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
				</s:elseif>
				
			</td>
		</tr>
		
		<s:if test="%{#idRamo == 3}">
			<tr>
				<td>															
					<s:text name="midas.compensaciones.contenedor.comision"/>
				</td>
				<td>
					<s:radio 
					id="radioComisionAgente" 
					value="compensacionesDTO.aplicaComision" 
					name="compensacionesDTO.aplicaComision" 
					list="#{'1':'Si','0':'No'}"
						onchange="selectComAgente();"
					/>
				</td>
				
				<s:if test="compensacionesDTO.vidaIndiv != 14">	
				<td>
					<span id="textFechaModificacionVida">									
						<s:text name="midas.compensaciones.contenedor.fechamodificacion"/>
					</span>
				</td> 
				<td >
					<sj:datepicker name="compensacionesDTO.fechaModificacionVida"
					   buttonImage="../img/b_calendario.gif"
					   id="fechaModificacionVida" 
					   maxlength="10" 
					   cssClass="txtfield"			
					   minDate="today"							   		   								  
					   onkeypress="javascript:;"
					   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					   onblur="javascript:;"								   
					   >
					 </sj:datepicker>	
				</td>
				</s:if>	
				<td>												
					<s:text name="midas.compensaciones.contenedor.topemaximo"/>
				</td>
				<td >	
					<s:textfield  name="compensacionesDTO.topeMaximo" id="topeMaximo" cssClass="bloquearConf bloquearConfContra cajaTextoM2 w100"></s:textfield>											
				</td>
			</tr>
			<tr id="formaPago" style="display:none;">
			   <td>
			   		<s:text name="midas.cotizacion.formapago"/>
			   </td>		
			   <td>
				    <s:textfield  name="compensacionesDTO.formaPagoBanca" id="formaPagoBanca" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>	
			   </td>
			</tr>					
		</s:if>	
		
		<tr>						
			<td>
			</td>

			<td >
			</td>							
	
			<s:if test="%{#idRamo == 1}">
				<td>
					<s:text name="midas.compensaciones.contenedor.nombreNegocio"/>
				</td>
	
				<td >
					<s:textfield name="compensacionesDTO.nombreNegocio" id="textNombreNegocioCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
				</td>
			</s:if>

		</tr>
	</table>
	<br>
	  <s:if test="%{#idRamo == 2 && listRecibos.size>0}">
	  	<table  id="filtrosM2" width="98%" align="center">
	 	<tr>
			<td class="titulo" colspan="6"><s:text name="midas.compensaciones.contenedor.cambioconf"/></td>
		</tr>
		<tr>
			<td>	
			<s:text name="midas.compensaciones.label.seleccionarrecibo"/>
			</td>
			<td>
				 <s:select  name="compensacionesDTO.idRecibo" list="listRecibos" id="idRecibo" cssClass="cajaTextoM2 w100 jQrequired" 
					listKey="idRecibo" listValue="descripcionRecibo" headerKey="0"
					headerValue="Seleccione">
				</s:select>
			</td>
			<td align="left"  style="visibility:hidden;">
			<s:text name="midas.compensaciones.configurador.poliza"/>
			</td>
			<td style="visibility:hidden;">
			<s:textfield name="idPoliza" id="idPoliza" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
			</td>		
		</tr>
		</table>
	</s:if>
	<input id="flagChangeTabId" value="0" type="hidden"/>
	<input id="varTabActiva" value="<s:property value='tabActiva' />" type="hidden"/>
	
	<!-- 	Div de Carga -->
	<div id="divLoadId" style="position:absolute;left: 39%; bottom: 50%;  z-index: 1000;"></div>	
	
	<!-- 	TABS -->
	<div id="configuracionCompensacionTabBar" style="height: 100%; width: 100%; overflow: auto;margin-top: 10px;"></div>	
	
	<script type="text/javascript">
		var tabsCompensaciones = initTabsCompensacion();
	</script>	
		
	<!-- 	Div de Mensajes		 -->
	<div id="mensajesCompensacionesAdicionalesId" ></div>
	<iframe id="iframeMensajesCompensacionesAdicionalesId"  name="iframeMensajesCompensacionesAdicionales" style="display: none" ></iframe>
</div>
