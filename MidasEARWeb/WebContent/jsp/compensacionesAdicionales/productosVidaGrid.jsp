<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="nombreProducto " type="ro" width="150" sort="str" ><s:text name="midas.compensaciones.productosvida.producto"/></column>
        <column id="nombreLinea" type="ro" width="150" sort="str" ><s:text name="midas.compensaciones.productosvida.lineas"/></column>
		<column id="comision" type="ro" width="100" sort="int"><s:text name="midas.compensaciones.productosvida.comision"/></column>				        
	</head>
	<s:iterator value="productosVida" var="rowProductosVida" status="index">
		<row id="${index.count}">
		  <cell>  <s:property value="%{#rowProductosVida.nombreProducto}" /></cell>
		  <cell>  <s:property value="%{#rowProductosVida.nombreLinea}" /></cell>
          <cell>  <s:property value="%{#rowProductosVida.comision}" /></cell>  
        </row>
	</s:iterator>
</rows>