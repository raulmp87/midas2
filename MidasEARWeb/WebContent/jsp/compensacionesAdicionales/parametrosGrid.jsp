<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
    <head>
         <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
         <column id="id" type="ro" width="50" sort="int" hidden="false" align="right" >
             ID
         </column>
         <column id="descripcionNegocio" type="ro" width="150" sort="str" hidden="false">
             Negocio / Gerencia
         </column>
         <column id="tipo" type="ro" width="*" sort="str" hidden="false">
             Tipo
         </column>
         <column id="ramo" type="ro" width="60" sort="str" hidden="false">
             Ramo
         </column>
         <column id="estatus" type="ro" width="60" sort="str" hidden="false">
             Estatus
         </column>
          <column id="acciones" type="img" width="60" sort="na" align="center" hidden="false">
              Acciones
          </column>
    </head>
    <% int a=0;%>
	<s:iterator value="listaParametros" var="parametroGeneral">
	    <% a+=1; %>
	    <row id="<%=a%>">
			<cell><s:property value="compensAdicioca.id" escapeHtml="false"/></cell>
			<cell><![CDATA[<s:property value="compensAdicioca.nombreNegocio"/>]]></cell>
			<cell><![CDATA[<s:property value="tipoCompensacionca.nombre"/>]]></cell>
			<cell><![CDATA[<s:property value="compensAdicioca.ramoca.nombre"/>]]></cell>
			<cell><![CDATA[<s:property value="compensAdicioca.estatusca.nombre"/>]]></cell>
			<cell>../img/icons/ico_editar.gif^Editar^javascript: irConfigurador(<s:property value="compensAdicioca.id"/>)^_self</cell>
		</row>
	</s:iterator>
</rows>