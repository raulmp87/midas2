<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
    <head>
         <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
         <column id="id" type="ro" width="100" sort="int" hidden="false" align="center" >
             ID
         </column>
         <column id="descripcionNegocio" type="ro" width="*" sort="str" hidden="false">
             Negocio
         </column>
         <column id="ramo" type="ro" width="*" sort="str" hidden="false">
             Negocio
         </column>
    </head>
    <% int a=0;%>
	<s:iterator value="listaCompensaciones" var="compensacion">
	    <% a+=1; %>
	    <row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false"/></cell>
			<cell><![CDATA[<s:property value="descripcionNegocio"/>]]></cell>
			<cell><![CDATA[<s:property value="compensAdicioca.ramoca.nombre"/>]]></cell>
		</row>
	</s:iterator>
</rows>