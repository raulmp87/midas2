<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="colRangoSiniNum" type="ro" width="120" sort="na"><s:text name="Num."/></column>
		<column id="colRangoSiniInicial" type="ro" width="120" sort="na"><s:text name="% Inicial"/></column>       	
		<column id="colRangoSiniFinal" type="ro" width="120" sort="na"><s:text name="% Final"/></column>
		<column id="colRangoSiniCompensacion" type="ro" width="130" sort="na"><s:text name="% Compensación"/></column>
		<column id="colRangoSiniAcciones" type="ro" width="70" sort="na"><s:text name="Acciones"/></column>
	</head>
	<s:iterator value="listRangoscas" var="rangos" status="index">
		<row id="${index.count}">
			<cell><![CDATA[<s:textfield disabled="true" value="%{#index.count}" name="%{#index.count}" id="nombre_%{#rangos.id}" cssClass="cajaTextoM2 w100 nombreNivel "/>]]></cell>
			<cell><![CDATA[<s:textfield value="%{#rangos.valorMinimo}" name="%{#rangos.valorMinimo}"  id="inicial_%{#rangos.id}" cssClass="cajaTextoM2 w100 inicialNivel classDisabledBajaSini"/>]]></cell>
			<cell><![CDATA[<s:textfield value="%{#rangos.valorMaximo}" name="%{#rangos.valorMaximo}"  id="final_%{#rangos.id}" cssClass="cajaTextoM2 w100 finalNivel classDisabledBajaSini"/>]]></cell>
			<cell><![CDATA[<s:textfield value="%{#rangos.valorCompensacion}" name="%{#rangos.valorCompensacion}" id="%{#rangos.id}_compensacion" cssClass="cajaTextoM2 w100 compensacionNivel classDisabledBajaSini"/>]]></cell>
			<cell><![CDATA[<a id="${index.count}" href="javascript:void(0)" onclick="eliminarRango(this.id,'rangosSiniestralidadGrid')"><img src="../img/icons/ico_eliminar.gif"></a>]]></cell>
		</row>
	</s:iterator>
</rows>










