<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="idTarifaMovilVida" type="ro" width="50" sort="int" ><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.id"/></column>
		<column id="edadMinima" type="ro" width="100" sort="str"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.edadMin"/></column>
		<column id="edadMaxima" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.edadMax"/></column>
		<column id="sumaAsegurada" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.sa"/></column>
		<column id="tarifaBasica" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.tarifaBasica"/></column>
		<column id="tarifaPlatino" type="ro" width="*" sort="str"><s:text name="midas.catalogos.tarifa.vida.tarifamovilvida.tarifaPlatino"/></column>
		<column id="claveEstatus" type="ro" width="80" sort="str"><s:text name="midas.catalogos.fuerzaventa.grid.estatus.titulo"/></column>	
		<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
		
	</head>
		<row id="1">
			<cell>1</cell>
			<cell>2</cell>
			<cell>3</cell>
			<cell>4</cell>
			<cell>5</cell>
			<cell>6</cell>			
			<cell></cell>			
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/></cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/></cell>
			<cell><s:url value="/img/icons/ico_eliminar.gif"/></cell>			
		</row>
</rows>