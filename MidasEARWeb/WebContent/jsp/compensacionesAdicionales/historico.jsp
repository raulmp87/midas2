<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/compensacionesHeader.jsp"></s:include>
<script type="text/javascript">
function historico(){
	var configuracion =  CompensacionUtils.config();
	var compensacionId = configuracion.isContraprestacion ? configuracion.contraprestacionId : configuracion.compensacionId;
	verHistoriaModificacion(compensacionId);
}
</script>
         <div>
			<table id="tableHistorico" class="tableStyle">
				<tr>
					<td>
						Ultima Modificacion:												
					</td>
					<td>
						<s:textfield name="compensacionesDTO.ultimaModificacion" id="textUltimaModif" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
					<td>
					  Usuario:												
					</td>
					<td>
						<s:textfield name="compensacionesDTO.usuarioModificacion" id="textUsuario" cssClass="cajaTextoM2 w100"></s:textfield>						
					</td>
					<td rowspan="2">
						<div class="btn_back w120"> <%--Invocando Metodo Independiente--%>
							<a href="javascript: void(0); "class="icon_guardar ." title="Bitácora de Acciones." onclick="javascript:historico();">
								<s:text name="Historico"/>
							</a>
						</div>						
					</td>
				</tr>				
			</table>
		</div>