<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<table width="880px" class="tableStyle" id="tableBusquedaCompens">
	<tr>
		<td>
			Compensaciones Adicionales
		</td>
	</tr>
	<tr>
		<td>
			<table class="tableStyle">
				<tr>
					<td>
						<s:text name="ID compensaciones:"/>
					</td>
					<td >
						<s:textfield name="textIdCompensaciones" id="textIdCompensaciones" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
					<td>
						<s:text name="Producto:"/>
					</td>
					<td >
						<s:select id="selectProducto" name="selectProducto" cssClass="cajaTextoM2 w100 jQrequired" disabled="#readOnly" 
							list="#{'-1':'-Seleccione-','1':'A', '2':'B', '3':'C'}"/>	
					</td>
					<td>
						<s:text name="No de Póliza:"/>
					</td>
					<td >
						<s:textfield name="textNoPoliza" id="textNoPoliza" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="Línea de Venta:"/>
					</td>
					<td >
						<s:select id="selectLineaVenta" name="selectLineaVenta" cssClass="cajaTextoM2 w100 jQrequired" disabled="#readOnly" 
							list="#{'-1':'-Seleccione-','1':'Linea A', '2':'Linea B', '3':'Linea C'}"/>						
					</td>
					<td>
						<s:text name="Ejecutivo:"/>
					</td>
					<td >
						<s:select id="selectEjecutivo" name="selectEjecutivo" cssClass="cajaTextoM2 w100 jQrequired" disabled="#readOnly" 
							list="#{'-1':'-Seleccione-','1':'Ejecutivo 1', '2':'Ejecutivo 2', '3':'Ejecutivo 3'}"/>
					</td>
					<td>
						<s:text name="Nombre de Negocio:"/>
					</td>
					<td >
						<s:textfield name="textNoPoliza" id="textNoPoliza" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="Ramo:"/>
					</td>
					<td >
						<s:select id="selectRamo" name="selectRamo" cssClass="cajaTextoM2 w100 jQrequired" disabled="#readOnly" 
							list="#{'-1':'-Seleccione-','1':'Ramo Autos', '2':'Ramo Daños', '3':'Ramos Vida'}"/>						
					</td>
					<td>
						<s:text name="Promotoría:"/>
					</td>
					<td >
						<s:select id="selectPromotoria" name="selectPromotoria" cssClass="cajaTextoM2 w100 jQrequired" disabled="#readOnly" 
							list="#{'-1':'-Seleccione-','1':'Ejecutivo 1', '2':'Ejecutivo 2', '3':'Ejecutivo 3'}"/>
					</td>					
				</tr>
				<tr>
					<td>
						<s:text name="Gerencia:"/>
					</td>
					<td >
						<s:select id="selectGerencia" name="selectGerencia" cssClass="cajaTextoM2 w100 jQrequired" disabled="#readOnly" 
							list="#{'-1':'-Seleccione-','1':'Gere', '2':'n', '3':'cia'}"/>						
					</td>
					<td>
						<s:text name="ID Negocio:"/>
					</td>
					<td >
						<s:textfield name="textNegocio" id="textNegocio" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
					<td rowspan="2" colspan="2" align="right">
						<div class="btn_back w120">
							<a href="javascript:void(0)" class="icon_guardar ." onclick="">
								<s:text name="Buscar"/>
							</a>
						</div>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>