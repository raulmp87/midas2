<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="colRangoSiniNum" type="ro" width="120" sort="na"><s:text name="Num."/></column>
		<column id="colRangoSiniInicial" type="ro" width="120" sort="na"><s:text name="% Inicial"/></column>       	
		<column id="colRangoSiniFinal" type="ro" width="120" sort="na"><s:text name="% Final"/></column>
		<column id="colRangoSiniCompensacion" type="ro" width="130" sort="na"><s:text name="% Compensación"/></column>
		<column id="colRangoSiniAcciones" type="ro" width="70" sort="na"><s:text name="Acciones"/></column>
	</head>
	<s:iterator value="listRangoscas" var="rangos" status="index">
		<row id="${index.count}">
<%-- 			<cell><s:property value="valordescripcion" escapeHtml="false" escapeXml="true"/></cell> --%>
<%-- 			<cell><![CDATA[<input id="${valorid}_" class="lineaDescripcion" type="hidden" value="${valordescripcion}"> <s:textfield value="%{#lineas.valorporcentaje}" name="%{#lineas.valorporcentaje}" onkeyup="validaPorcentaje(this.id);" id="%{#lineas.valorid}" cssClass="cajaTextoM2 w100 lineaNegocio classDisabledPrima"/>]]></cell> --%>
			<cell><![CDATA[<s:textfield value="%{#index.count}" name="%{#index.count}" id="nombre_%{#rangos.id}" cssClass="cajaTextoM2 w100 nombreNivel classDisabledBajaSiniContra"/>]]></cell>
			<cell><![CDATA[<s:textfield value="%{#rangos.valorMinimo}" name="%{#rangos.valorMinimo}" onkeyup="CompensacionUtils.parametros.validaPorcentajevalidaPorcentaje(this.id);" id="inicial_%{#rangos.id}" cssClass="cajaTextoM2 w100 inicialNivel classDisabledBajaSiniContra"/>]]></cell>
			<cell><![CDATA[<s:textfield value="%{#rangos.valorMaximo}" name="%{#rangos.valorMaximo}" onkeyup="CompensacionUtils.parametros.validaPorcentajevalidaPorcentaje(this.id);" id="final_%{#rangos.id}" cssClass="cajaTextoM2 w100 finalNivel classDisabledBajaSiniContra"/>]]></cell>
			<cell><![CDATA[<s:textfield value="%{#rangos.valorCompensacion}" name="%{#rangos.valorCompensacion}" onkeyup="CompensacionUtils.parametros.validaPorcentaje(this.id);" id="%{#rangos.id}_compensacion" cssClass="cajaTextoM2 w100 compensacionNivel classDisabledBajaSiniContra"/>]]></cell>
<%-- 			<cell>Compensacion?</cell>			 --%>
			<cell><![CDATA[<a id="${index.count}" href="javascript:void(0)" onclick="eliminarRango(this.id,'rangosSiniestralidadGridContra')"><img src="../img/icons/ico_eliminar.gif"></a>]]></cell>
		</row>
	</s:iterator>
</rows>










