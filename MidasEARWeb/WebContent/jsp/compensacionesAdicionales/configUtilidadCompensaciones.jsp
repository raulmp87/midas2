<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:form  method="post" action="" target="" enctype="multipart/form-data" id="formConfigGralCa" name="formConfigGralCa">
			<table width="880px" class="tableStyle">
				<tr>
					<td>
						<table class="tableStyle">
							<tr>
								<td>
									<s:text name="Clave Agente:"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.claveAgenteUtilidad" id="textClaAgeUtilidad" cssClass="cajaTextoM2 w100"></s:textfield>									
								</td>
								<td>
									<s:text name="Nombre Agente:"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.nombreAgenteUtilidad" id="textNomAgenUtilidad" cssClass="cajaTextoM2 w100"></s:textfield>																		
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Buscar Agente"/>
										</a>
									</div>
								</td>								
							</tr>							
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="tableStyle">
							<tr>
								<td>
									<s:text name="Participación de Utilidades:"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porcenUtilidadesUtilidad" id="textPorceParticiUtilidad" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="%"/>
								</td>
								<td>
									<s:text name="$"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.montoUtilidadesUtilidad" id="textMontoParticiUtilidad" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td colspan="2" align="left">
<!-- 									<div class="btn_back w120"> -->
<!-- 										<a href="javascript:void(0)" class="icon_guardar ." onclick=""> -->
<%-- 											<s:text name="Calcular"/> --%>
<!-- 										</a> -->
<!-- 									</div> -->
								</td>
							</tr>
							<tr>
								<td>
									<s:text name="Monto de las Utilidades"/>
								</td>
								<td colspan="4">
									<s:textfield name="compensacionesDTO.montoDeLasUtilidades" id="textMontoUtilidad" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Guardar"/>
										</a>
									</div>
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Cancelar"/>
										</a>
									</div>								
								</td>
							</tr>
							<tr>
								<td colspan="6" align="right">
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Provisionar"/>
										</a>
									</div>
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Solicitar Pago"/>
										</a>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<s:include value="historico.jsp"></s:include>
					</td>
				</tr>
			</table>
		</s:form>