<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<input id="varCompensacionId" value="<s:property value='compensacionesDTO.compensacionId' />" type="hidden"/>
<input id="varIsContraprestacion" value="0" type="hidden"/>
<input id="bloquearConfCompId" value="<s:property value="compensacionesDTO.bloquearConfiguracion"/>" type="hidden"/>
<input id="varEstatusCompensacion" value="<s:property value="compensacionesDTO.estatusCompensacionId"/>" type="hidden"/>
<input id="varEstatusContraprestacion" value="<s:property value="compensacionesDTO.estatusContraprestacionId"/>" type="hidden"/>
<script type="text/javascript">
	/**
	*Inicializa la pestaña de Compensacion
	*/
	CompensacionAdicional.initCompensacion();
</script>


<s:set name="idRamo" value="compensacionesDTO.ramo.id"/>
<s:form action="" id="formCompensacionesConf" name="formCompensacionesConf">
<div id="compensacionesDiv" align="center" style="height: 83%; width: 100%;">
<table class="tableStyle" id="tableAgente" width="100%">

	<tr>
			<td>
				<s:text name="ID compensaciones"/>
			</td>		

			<td>
				<s:textfield name="compensacionesDTO.compensacionId" id="textIdCompensacionesCa" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
			</td>
	</tr>
	
	<tr>
		<td>
			<s:text name="Clave Agente:"/>
		</td>
		<td>
			<s:textfield name="compensacionesDTO.claveAgenteGral" id="textClaAgeGral" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>
			<input id="varFSit" value="<s:property value='compensacionesDTO.datosSeycos.fsit' />" type="hidden"/>
			<input id="varIdEmpresa" value="<s:property value='compensacionesDTO.datosSeycos.idEmpresa' />" type="hidden"/>
		</td>
		<td>
			<s:text name="Nombre Agente:"/>
		</td>
		<td>
			<s:textfield name="compensacionesDTO.nombreAgenteGral" id="textNomAgenGral" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>																		
		</td>
		<td>
			<div class="btn_back w120">
				<a type="anchor" id="btnBuscaAgenteCompensacionLink" href="javascript:void(0);" class="bloquearConf icon_persona " onclick="javascript: seleccionarAgente(CompensacionAdicional.getAgenteABuscar(),'textClaAgeGral','', 'textNomAgenGral',true,0, 0,this,false,0,'cargaAgenteEncontrado');">
					<s:text name="Buscar Agente"/>
				</a>
			</div>
		</td>								
	</tr>
	<tr>
		<td>
			<s:text name="Clave Promotor:"/>
		</td>
		<td >
			<s:textfield name="compensacionesDTO.clavePromotorGral" id="textClavePromotorGral" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>									
		</td>
		<td>
			<s:text name="Nombre Promotor:"/>
		</td>
		<td >
			<s:textfield name="compensacionesDTO.nombrePromotorGral" id="textNombrePromotorGral" cssClass="cajaTextoM2 w100" disabled="true"></s:textfield>																		
		</td>
	</tr>
	<tr>								
		<td colspan="5">
			<table class="tableStyleNoBorder" id="tableChecksIncluir">									
 				<tr>
 					<td colspan="4" rowspan="2">
 						<s:radio id="radioInclusiones" cssClass="bloquearConf" name="compensacionesDTO.inclusionesGral" list="#{'1':'Incluir en el Cuaderno de Concursos','2':'Incluir en Convenio','0':'Ninguno'}" />
 					</td>
 				</tr>
 			</table>
		</td>
		<td colspan="2" align="right">
			<table class="tableStyleNoBorder">
				<tr>
					<td colspan="4" align="center">

					</td>											
 				</tr>
 				<tr>
 					<td colspan="4">

 					</td>
 				</tr>
			</table>
		</td>
	</tr>
</table>

<div select="<s:property value='subTabActiva'/>" style= "height:100%; width:100%; overflow:auto" hrefmode="ajax-html"  id="compensaciones" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >

	<div height="100%" width="100%" id="configGralCa" name="<s:text name='General'/>" href="http://void">
		<s:include value="configGralCompensaciones.jsp"></s:include>	
	</div>
	
	<s:if test="%{#idRamo lt 3}">
		<div height="100%" width="100%" id="configPolizaCa" name="<s:text name='Derecho de Poliza'/>" href="http://void" >
			<s:include value="configPolizaCompensaciones.jsp"></s:include>
		</div>
	</s:if>
	
</div>
<table width="100%" class="tableStyleNoBorder" id="tablePieCompensacion">
	<tr>
		<td>
			<s:include value="conductosDocumentos.jsp"></s:include>
		</td>
	</tr>
	<tr>
		<td>
			<table class="tableStyle" width="100%" id="tableContenedorAutorizaciones">		
				<tr>
					<td>
						<div class="fuenteEncabezado">Autorizaciones</div>										
						<div class="titulo"></div>					
			    	</td>
				</tr>
				<tr>
					<td>					
						<s:checkboxlist id="checkListAutorizacionesCompensacion" cssClass="bloquearConf" list="listTipoAutorizacionca" value="compensacionesDTO.listTipoAutorizacionId" name="compensacionesDTO.listTipoAutorizacionca"  listKey="id" listValue="nombre" disabled="" ></s:checkboxlist>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table align="left" width="100%" id="tableContenedorHistoricoBotonesGral">		
				<tr>				
					<td>						
						<s:include value="historico.jsp"></s:include>
					</td>
					<td align="right">
						<table id="tableContenedorBotonesGral">
							<tr>
								<td>
									<div class="btn_back w120">										
										<a type="anchor" id="btnAceptarConfigGral" href="javascript:void(0)" class="icon_guardar ." onclick="CompensacionUtils.autorizarCompensacion();">
											<s:text name="Aceptar"/>
										</a>
									</div>																
								</td>
								<td>
									<div class="btn_back w120">
										<a type="anchor" id="btnGuardarConfigGral" href="javascript:void(0)" class="bloquearConf icon_guardar ." onclick="guardarCompensacionesAdicionales(false, false, null);">
											<s:text name="Guardar"/>
										</a>										
									</div>																
								</td>		
								<td>
									<div class="btn_back w120">
										<a id="btnRegresarConfigGral" href="javascript:void(0)" class="icon_guardar ." onclick="cancelar();">
											<s:text name="Regresar"/>
										</a>
									</div>
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="cancelar();">
											<s:text name="Cancelar"/>
										</a>
									</div>
								</td>
							</tr>
						</table>
					</td>			
				</tr>
			</table>			
		</td>
	</tr>
</table>	
</div>
</s:form>	

<script type="text/javascript">
	dhx_init_tabbars();
	jQuery(document).ready(function(){
		jQuery('#selectBaseCalGral option:contains("PRIMA DE RIESGO")').remove();
	});
</script>
