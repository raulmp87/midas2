<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:form action="" id="formBonoCa">
			<table width="880px" class="tableStyle">				
				<tr>
					<td>
						<s:text name="Clave Agente"/>
					</td>
					<td >
						<s:textfield name="compensacionesDTO.claveAgenteBono" id="textClaAgenBono" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>
					<td>
						<s:text name="Nombre Agente"/>
					</td>
					<td colspan="3">
						<s:textfield name="compensacionesDTO.nombreAgenteBono" id="textNomAgeBono" cssClass="cajaTextoM2 w100"></s:textfield>
					</td>					
				</tr>
				<tr>					
					<td colspan="6">
					<div class="titulo"></div>
						<table class="tableStyle">
							<tr>
								<td rowspan="2">
									<table class="tableStyleNoBorder">
										<tr>
											<td>
                 								<s:checkbox id="checkBono" name="compensacionesDTO.porBono" fieldValue="false" label=""/>
                 							</td>
											<td>
												<s:text name="Bono"/>
											</td>
										</tr>
									</table>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porceBono" id="textPorceBono" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="%"/>
								</td>
								<td>
									<s:text name="$"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.montoBono" id="textMontoBono" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="Base para calculo:"/>												
								</td>
								<td>
									<s:select id="selectBaseCalcBono" 
											  name="compensacionesDTO.baseCalculocaBono" 
											  cssClass="cajaTextoM2 w100 jQrequired" 
											  disabled="#readOnly" 
											  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
				       						  list="listBaseCalculoca" listKey="id" listValue="nombre"
				       						  />
								</td>
							</tr>
							<tr>								
								<td >
									<s:text name="% al Agente"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porceAgenteBono" id="textPorceAgenBono" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="% al Promotor"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porcePromotorBono" id="textPorcePromoBono" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="Frecuencia de Pago:"/>
								</td>
								<td>
									<s:select id="selectFrecuPagoBono" 
											  name="compensacionesDTO.frecuenciaPagocaBono" 
											  cssClass="cajaTextoM2 w100 jQrequired" 
											  disabled="#readOnly"
											  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
				       						  list="listFrecuenciaPagoca" listKey="id" listValue="nombre" 
	       									  />															
								</td>
							</tr>							
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="6">
					<div class="titulo"></div>
						<table class="tableStyle">
							<tr>
								<td rowspan="2">
									<table class="tableStyleNoBorder">
										<tr>
											<td>
                 								<s:checkbox id="checkSobrComis" name="compensacionesDTO.porSobrecomision" fieldValue="false" label=""/>
                 							</td>
											<td>
												<s:text name="Sobrecomisión"/>
											</td>
										</tr>
									</table>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porceSobrecomision" id="textPorceSobrComis" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="%"/>
								</td>
								<td>
									<s:text name="$"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.montoSobrecomision" id="textMontoSobrComis" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="Base para calculo:"/>												
								</td>
								<td>
									<s:select id="selectBaseCalcSobrComis" 
											  name="compensacionesDTO.baseCalculocaSobrecomision" 
											  cssClass="cajaTextoM2 w100 jQrequired" 
											  disabled="#readOnly"
											  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
				       						  list="listBaseCalculoca" listKey="id" listValue="nombre" 
	       									  />
								</td>
							</tr>
							<tr>								
								<td>
									<s:text name="% al Agente"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porceAgenteSobrecomision" id="textPorceAgenSobrComis" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="% al Promotor"/>
								</td>
								<td>
									<s:textfield name="compensacionesDTO.porcePromotorSobrecomision" id="textPorcePromoSobreComisi" cssClass="cajaTextoM2 w100"></s:textfield>
								</td>
								<td>
									<s:text name="Frecuencia de Pago:"/>
								</td>
								<td>
									<s:select id="compensacionesDTO.frecuenciaPagocaSobrecomision" 
											  name="selectFrecuPagoSobreComisi" 
											  cssClass="cajaTextoM2 w100 jQrequired" 
											  disabled="#readOnly" 
	       									  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"
				       						  list="listFrecuenciaPagoca" listKey="id" listValue="nombre" 
	       									  />															
								</td>
							</tr>							
						</table>
						<div class="titulo"></div>
					</td>					
				</tr>
				<tr>
					<td colspan="6" align="right">
						<table class="tableStyle">
							<tr>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Aceptar"/>
										</a>
									</div>
								</td>
								<td>
									<div class="btn_back w120">
										<a href="javascript:void(0)" class="icon_guardar ." onclick="">
											<s:text name="Cancelar"/>
										</a>
									</div>
								</td>
							</tr>							
						</table>
						<div class="titulo"></div>
					</td>
				</tr>
				<tr>
					<td>
						<s:include value="historico.jsp"></s:include>
					</td>
				</tr>
			</table>			
		</s:form> 