<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div align="center" style="height: 100%; width: 100%;">
<%-- 	<s:form action="" id="formDerechoPolizaCa" name="formDerechoPolizaCa"> --%>
			
			<table height="100%" width="100%" class="tableStyle" id="tablePolizaConfigCa">
				<tr>
					<td colspan="3">
						<div class="fuenteEncabezado">Derecho de Póliza</div>
						<div class="titulo"></div>
					</td>
				</tr>				
				<tr>
					<td rowspan="2">
						<s:checkbox name="compensacionesDTO.porPoliza" id="checkDerechoPoliza" label="" cssClass="bloquearConf"/>
					</td>
					<td rowspan="2">
						<s:text name="Derecho de Póliza"/>								
					</td>
				</tr>		
				<tr>				
					<td>
						<table  height="100%" width="100%" class="tableStyleNoBorder" id="tableContenedorPorcMontPoliza">
							<tr>
								<td>
									<table id="tableContenedorPorcentajeDerecho" class="tableStyleNoBorder">
										<tr>
											<td>
												<s:text name="Porcentaje:"/>
											</td>
											<td>
												<s:textfield onkeyup="porcentajeOmonto(this.id,'textMontoPoliza')" name="compensacionesDTO.porcePoliza" id="textPorcePoliza" cssClass="bloquearConf cajaTextoM2 w100 classDisabledDerechoPoliza validarPorcentaje"></s:textfield>
											</td>
											<td>
												<s:text name="%"/>
											</td>											
										</tr>														
									</table>
								</td>								
								<td>
									<table id="tableContenedorMontoDerecho" class="tableStyleNoBorder">
										<tr>
											<td>
												<s:text name="Monto $:"/>
											</td>
											<td>
												<s:textfield onkeyup="porcentajeOmonto(this.id,'textPorcePoliza')" name="compensacionesDTO.montoPoliza" id="textMontoPoliza" cssClass="bloquearConf cajaTextoM2 w100 classDisabledDerechoPoliza validarMonto"></s:textfield>
											</td>
										</tr>														
									</table>
								</td>								
								<td>
									<table id="tableContenedorPorcentajeDerecho" class="tableStyleNoBorder">
										<tr>
											<td>
												<s:text name="Porcentaje al Agente:"/>
											</td>
											<td>
												<s:textfield onkeyup="checkPorcen2Elementos(this.id,'textPorcePromoPoliza')" name="compensacionesDTO.porceAgentePoliza" id="textPorceAgePoliza" cssClass="bloquearConf cajaTextoM2 w100 classDisabledDerechoPoliza validarPorcentaje"></s:textfield>
											</td>
											<td>
												<s:text name="%"/>
											</td>
										</tr>														
									</table>
								</td>
								<td>
									<table id="tableContenedorMontoDerecho" class="tableStyleNoBorder">
										<tr>
											<td>
												<s:text name="Porcentaje al Promotor:"/>
											</td>
											<td>
												<s:textfield onkeyup="checkPorcen2Elementos(this.id,'textPorceAgePoliza')" name="compensacionesDTO.porcePromotorPoliza" id="textPorcePromoPoliza" cssClass="bloquearConf cajaTextoM2 w100 classDisabledDerechoPoliza validarPorcentaje"></s:textfield>
											</td>
											<td>
												<s:text name="%"/>
											</td>										
										</tr>														
									</table>
								</td>
							</tr>							
						</table>						
					</td>
					<td colspan="2">
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<table id="tableEmisionPoliza" class="tableStyleNoBorder ">
							<tr>
								<td>
									<s:text name="Perfil de Usuario:"/>
								</td>
<!-- 								<td colspan="2"> -->
<%-- 									<s:radio id="radioPerfilUsuario" name="compensacionesDTO.emisorcaPoliza" list="listTipoEmisorca" listKey="id" listValue="nombre"/> --%>
<!-- 								</td> -->
								<td>
									<s:checkbox id="checkEmisInterPolizaCompensacion" name="compensacionesDTO.emisionInterna" label="" cssClass="bloquearConf classDisabledDerechoPoliza checkEmisionGrupo"/>
								</td>
								<td>	 
									<s:text name="EMISION INTERNA"/>
								</td>
								<td>
									<s:checkbox id="checkEmisExterPolizaCompensacion" name="compensacionesDTO.emisionExterna" label="" cssClass="bloquearConf classDisabledDerechoPoliza checkEmisionGrupo"/>
								</td>
								<td> 
									<s:text name="EMISION EXTERNA"/>
								</td>
						 	</tr>
						</table>
					</td>
				</tr>				
			</table>
<%-- 		</s:form> --%>
	</div>