<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css"/>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet"
	type="text/css"/>
<style type="text/css">
#content {
	font-size: 10px;
}

#form {
	text-align: center;
	margin-top: 20px;
}

#footer {
	margin-top: 30px;
	margin-left: 30%;
}

table.tr {
	text-alight: left;
}

table {
	font-size: 10px;
}


.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
</style>
<script type="text/javascript">
    var catalogo = '<s:property value="catalogo"/>';
     var tipoEntidad = '<s:property value="tipoEntidad"/>';
	var urlBusquedaAgentes = '<s:url action="buscarCatalogo" namespace="/compensacionesAdicionales/catalogos"/>';
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>	
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'>;
</script>
<script>
	var bandera = null;
	jQuery(document).ready(function(){
		bandera = getParameterByName('bandera');
	});
	function cargarAgente(){	
parent.cargaAgente(jQuery("#idAgente").val());
	}
	
	function getParameterByName(name) {
		  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		  var regexS = "[\\?&]" + name + "=([^&#]*)";
		  var regex = new RegExp(regexS);
		  var results = regex.exec(window.location.search);
		  if(results == null)
		    return "";
		  else
		    return decodeURIComponent(results[1].replace(/\+/g, " "));
		}

	function getStringParentList(id){
		var list = '';
		var index = 0;
		try{
			if(window.parent!=null){
				window.parent.jQuery('#'+id+ ' li').find('input').each(function(i){
					if(jQuery(this).is(':checked')){
						if(index==0){
							list = this.value;
						}else{
							list = list+','+this.value;
						}
						index++;
					}
				});
			}
		}catch(error) {
			list = '';
		}
		return list;
	}
	
	function getStringOpenertList(id){
		var list = '';
		var index = 0;
		try{
			if(window.parent.opener!=null){
				window.parent.opener.jQuery('#'+id+ ' li').find('input').each(function(i){
					if(jQuery(this).is(':checked')){
						if(index==0){
							list = this.value;
						}else{
							list = list+','+this.value;
						}
						index++;
					}
				});
			}
		}catch(error) {
			list = '';
		}
		return list;
	}		

$(function(){
	

	$( '#descripcionBusquedaAgente' ).autocomplete({
               source: function(request, response){
               		var idGerencias = getStringParentList('ajax_listaGerenciaSeycos');
               		if (idGerencias == null || idGerencias == ''){
               			idGerencias = getStringOpenertList('ajax_listaGerenciaSeycos');
               		}
               		$.ajax({
			            type: "POST",
			            url: urlBusquedaAgentes,
			            data: {'descripcionBusquedaAgente':request.term, 'catalogo': catalogo, 'tipoEntidad' : tipoEntidad, 'idGerencias': idGerencias},              
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( $( "item", xmlResponse ).map( function() {
								return {
									id: $( "id", this ).text(),
									idAgente: $("idAgente",this).text(),
									value: $( "descripcion", this ).text(), 
				                    idEmpresa: $( "idEmpresa", this ).text(),
									fsit: $( "fSit", this ).text(),
									nombreCompleto: $( "nombreCompleto", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 3,
               delay: 1000,
               select: function( event, ui ) {
               var funcionNombre = parent[parent.functionExecutePostSelectAgente];
                   funcionNombre(ui.item);
               }		          
         });
    $('#descripcionBusquedaAgente').focus();
 });	
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="detalle" >
	<center>
		<table id="agregar" width="100%">
			<s:hidden id="idAgente" name="idAgente" />
						<tr><td colspan="3">B&uacute;squeda por</td></tr>
						<tr>			
							<td width="15%" ><span>Nombre:</span></td>
							<td width="80%"><input type="text" name="descripcionBusquedaAgente" id="descripcionBusquedaAgente" class="cajaTexto" 						
							 /></td>
							<td width="5%">
								<div id="b_buscar" style="margin-left: 20px; display:none;">
									<a href="javascript: void(0);"
										onclick="javascript:busqueda(jQuery('#descripcionBusquedaAgente').val());">
										<s:text name="midas.boton.buscar" /> </a>
								</div>							
							</td>
						</tr>    
		</table>
	</center>
</div>