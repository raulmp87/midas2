<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:if test="totalEncontrados>0">
		<s:if test="numeroCatalogo == 1">
			<s:iterator value="agentesList">
			<item>
				<idAgente><s:property value="idAgente" /></idAgente>
				<id><s:property value="id"/></id>
				<descripcion><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true" /> - <s:property value="idAgente" /></descripcion>
			</item>
			</s:iterator>
		</s:if>
		<s:if test="numeroCatalogo == 2">
	    	<s:iterator value="listaProveedores">
				<item>
					<idAgente><s:property value="id" /></idAgente>
					<id><s:property value="id"/></id>
					<descripcion><s:property value="nombre" escapeHtml="false" escapeXml="true" /> - <s:property value="id" /></descripcion>
				</item>
			</s:iterator>
		</s:if>
		<s:if test="numeroCatalogo == 3">
	    	<s:iterator value="listaNegocios">
				<item>
					<idAgente><s:property value="idToNegocio" /></idAgente>
					<id><s:property value="idToNegocio"/></id>
					<descripcion><s:property value="descripcionNegocio" escapeHtml="false" escapeXml="true" /> - <s:property value="idToNegocio" /></descripcion>
				</item>
			</s:iterator>
		</s:if>
		<s:if test="numeroCatalogo == 4">
	    	<s:iterator value="entidadPersonas">
				<item>
					<idAgente><s:property value="id" /></idAgente>
					<id><s:property value="id"/></id>
				<s:if test="soloDescripcion == 0">
					<descripcion> <s:property value="nombres" escapeHtml="false" escapeXml="true" /> - <s:property value="id" /></descripcion>
				</s:if>	
				<s:if test="soloDescripcion == 1">
					<descripcion><s:property value="nombres" escapeHtml="false" escapeXml="true" /> </descripcion>
				</s:if>					
				</item>
			</s:iterator>
		</s:if>
		<s:if test="numeroCatalogo == 5">
			<s:iterator value="listaDatosSeycos" var="datosSeycos" >
			<item>
				<id><s:property value="id"/></id>
				<idAgente><s:property value="idAgente" /></idAgente>
				<idEmpresa><s:property value="idEmpresa" /></idEmpresa>
				<fSit><s:property value="fsit" /></fSit>
				<nombreCompleto><s:property value="nombreCompleto" /></nombreCompleto>
				<descripcion><s:property value="idAgente" />-<s:property value="nombreCompleto" escapeHtml="false" escapeXml="true" /> </descripcion>
			</item>
			</s:iterator>
		</s:if>
		<s:if test="numeroCatalogo == 6">
			<s:iterator value="listaPromotoria" var="promotor" >
			<item>
				<id><s:property value="id"/></id>
				<descripcion><s:property value="descripcion" /></descripcion>			
			</item>
			</s:iterator>
		</s:if>
	</s:if>
	<s:else>
		<item>
			<idAgente>0</idAgente>
			<id>0</id>
			<descripcion>No se encontraron Resultados ...</descripcion>
		</item>
	</s:else>
	
</resultados>
