<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="colId" type="ro" width="0" style="display:none" sort="na">Id</column> 
		<column id="colRamoDanios" type="ro" width="150" sort="na">Ramo</column>
		<column id="colSubRamosDaniosId" type="ro" width="0" style="display:none" sort="na">SubRamoId</column> 
		<column id="colSubRamosDanios" type="ro" width="170" sort="na">SubRamo</column>
		<column id="colPrimaBase" type="ro" width="130" sort="na">$ Prima Base</column>
		<column id="colCompensacionAdcionalDanios" type="ro" width="130" sort="na">% Compensación Adicional</column>		
	    <column id="colCompensacionAdicionalDaniosImp" type="ro" width="130" sort="na">$ Compensación Adicional</column>
	    <column id="existeId" type="ro" width="0" style="display:none" sort="na">existe</column> 
	    
	</head>
	<s:iterator value="compensacionesDTO.listSubRamosDaniosView" var="subRamosDanios" status="index">
		<row id="${index.count}" class="${subRamosDanios.existe}_SUBRAMO">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="descripcionRamo" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="idTcSubRamo" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="subRamo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[<s:textfield value="%{#subRamosDanios.primaBase}" 
				onkeyup="CompensacionUtils.parametros.validarPorcentajeSubRamos(this)" disabled="true"
				cssClass="cajaTextoM2 w100 classDisabledSubRamosDanios"></s:textfield>]]>
			</cell>		
			<cell><![CDATA[<s:textfield value="%{#subRamosDanios.porcentajeCompensacion}" 
				onkeyup="CompensacionUtils.parametros.validarPorcentajeSubRamos(this)" 
				cssClass="cajaTextoM2 w100 classDisabledSubRamosDanios"></s:textfield>]]>
			</cell>		
			<cell><![CDATA[<s:textfield value="%{#subRamosDanios.porcentajeCompensacionImporte}" disabled="true"
				cssClass="cajaTextoM2 w100 classDisabledSubRamosDanios" ></s:textfield>]]>
			</cell>		
			<cell><s:property value="existe" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>

