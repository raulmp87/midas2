<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
    <head>
         <beforeInit>
             <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
             <call command="setSkin"><param>light</param></call>
             <call command="enableDragAndDrop"><param>true</param></call>
             <call command="enablePaging">
				<param>true</param>
				<param>12</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			 </call>     
			 <call command="setPagingSkin">
				<param>bricks</param>
			</call>
            </beforeInit>
         <column id="id" type="ro" width="50" sort="int" hidden="false" align="right" >
             ID
         </column>
         <column id="descripcionNegocio" type="ro" width="*" sort="str" hidden="false">
             Negocio
         </column>        
         <column id="ramo" type="ro" width="60" sort="str" hidden="false">
             Ramo
         </column>
         <column id="ramo" type="ro" width="90" sort="str" hidden="false">
             No de Cotizacion
         </column>
         <column id="ramo" type="ro" width="90" sort="str" hidden="false">
            Poliza
         </column>
         <column id="estatus" type="ro" width="80" sort="str" hidden="false">
             Estatus
         </column>
          <column id="editar" type="img" width="60" sort="na" align="center" hidden="false">
              Editar
          </column>         
    </head>
    <% int a=0;%>
	<s:iterator value="listaEntidadesPersona" var="entidadPersona">
	    <% a+=1; %>
	    <row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false"/></cell>
			<cell><s:property value="negocio" escapeHtml="false"/></cell>						
			<cell><s:property value="ramo" escapeHtml="false"/></cell>
			<cell><s:property value="cotizacion" escapeHtml="false"/></cell>	
			<cell><s:property value="poliza" escapeHtml="false"/></cell>	
            <cell><s:property value="estatus" escapeHtml="false"/></cell>
			<cell> ../img/icons/ico_editar.gif^Editar^javascript: irConfigurador(<s:property value="id"/>,<s:property value="sinCompensacion"/>)^_self</cell>
		</row>
	</s:iterator>
</rows>