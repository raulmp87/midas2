<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script type="text/javascript">	
function fortimaxComContra(){
	var compDocu = '<s:property value="compensacionesDTO.compensacionId"/>';
	var agenteDocu = '<s:property value="compensacionesDTO.claveProveedorContra"/>';
	ventanaFortimax('COMPENSACION_PROVEEDOR','' , 'FORTIMAX',compDocu + agenteDocu,compDocu + agenteDocu);
}
jQuery(function(){
	esContraprestacion = true;
	});
</script>
<div id="conductosDocumentosContrap">
			<table width="100%" class="tableStyle" id="tableCondutosAgentesDocumetos">
				<tr>
					<td>
						<table class="tableStyleNoBorder">
							<tr>
								<td>
									<s:text name="Conductos:"/>
								</td>
								<td>
									<s:select id="selectAgregarAgteContra" 
							            name="compensacionesDTO.caEntidadPersona.id" 
							            cssClass="cajaTextoM2 w100 jQrequired" 
							            disabled="#readOnly"							            
							            list="compensacionesDTO.listEntidadPersonaca" listKey="id" listValue="nombres"
							        />
							    </td>
							    <td>
							    	<div id="btnAgregarAgenteContra" class="btn_back w120">
							        	<a type="anchor" id="btnAgregarAgenteContraLink" href="javascript:void(0)" class="bloquearConfContra icon_guardar ." onclick="javascript: seleccionarAgente('PROVEEDOR','textClaAgeGral','', 'textNomAgenGral',false, 0, idNegocio,'selectAgregarAgteContra' ,true,0,'cargaAgenteEncontrado');">
							            	<s:text name="Agregar Proveedor"/>
							          	</a>
							        </div>
							    </td>	
								<td>
									<div class="btn_back w120" style="display: inline; float: right; visibility:%{consulta} " id="icon_agregar" >
										<a type="anchor" id="btnDocumentosContraprestacion" href="javascript: void(0);"  onclick="fortimaxComContra();">
											<s:text name="Documentos" />
										</a>
								   </div>
								</td>
							</tr>
						</table>
					</td>
					<td>
					</td>
				</tr>																	
			</table>
		</div>