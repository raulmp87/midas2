<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
<link href="<s:url value="/css/dhtmlxcalendar_yahoolike.css"/>" rel="stylesheet" type="text/css">		
<link href="<s:url value="/css/dhtmlxcombo.css"/>" rel="stylesheet" type="text/css">			
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<link href='<s:url value="/css/toastr.css"/>' rel="stylesheet" type="text/css">

<!-- head	 -->
<sj:head/>

<script type="text/javascript" src="<s:url value="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptDataGrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptComponents.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtree.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtree_sb.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_filter.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_srnd.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_sub_row.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_drag.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_group.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_link.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_nxml.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxdataprocessor.js"/>"></script>	
<script type="text/javascript" src="<s:url value="/js/dhtmlxaccordion.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxmenu.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtabbar.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxvault.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtabbar_start.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxUtil.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/jQuery/jquery.blockUI.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/mask.js"/>"></script>

  
<script type="text/javascript">
	jQuery.noConflict();	
	unblockPage();		
</script>

<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/CompensacionView.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/CompensacionUtils.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>

<style type="text/css">
/* OVERRIDE CLASS */
div.dhxcont_main_content {
    position: relative;
    left: 0px;
    top: 0px;
    overflow: auto;
}

#tableFileUpload>tbody>tr>td>div>div.wwlbl{
	display: none
}
#tableFileUpload>tbody>tr>td>div>br{
	display: none
}

.ADD_SUBRAMO{
	background: rgba(193, 192, 224, 0.639216) !important;
}

.REMOVE_SUBRAMO{
	background: rgba(216, 80, 0, 0.490196) !important;
}

.EXISTS_SUBRAMO{
	background:none !important ;
}

</style>
