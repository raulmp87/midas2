<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:form  method="post" action="/compensacionesAdicionales/cargarPresupuestoAnual.action" target="iframeProcessCargaMasivaData" enctype="multipart/form-data" id="formularioCargaPresupuestoAnualId" name="formularioCargaPresupuestoAnual">
	<table width="100%" align="center"  class="contenedorFormas no-border">
		<tr>
			<td>
				<s:file id="fileUpload" name="fileUpload" cssClass="cajaTextoM2 w300 jQrequired" label=""/>
			</td>
		</tr>
		<tr>
			<td>
				<div class="btn_back w120">
					<a href="javascript:cargarPresupuestoAnual()" class="icon_guardar ." 
						onclick="">
						<s:text name="Cargar Archivo"/>
					</a>
				</div>
			</td>
		</tr>
	</table>
</s:form>
