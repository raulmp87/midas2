<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>"></script>
<script src="<s:url value='/js/midas2/adminarchivos/adminarchivos.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<table width="98%">
    <tr>
        <td class="titulo">
            Listar Configuraci&oacute;n de Compensaciones Adicionales
        </td>
    </tr>
</table>
<div id="filtrosM2" style="width: 98%;">
  <s:form name="filtroCompensacionForm" id="filtroCompensacionForm">
  <table width="100%"  id="filtros">
    <tr>
    
        <td style="width: 110px;">
            ID Compensaci&oacute;n
        </td>
        
        <td colspan="3">
            <s:textfield cssClass="txtfield jQnumeric jQrestrict" 
						size="10"						
						id="filtroCompensacionIdCompensacion" 
						name="filtroCompensacion.idCompensacion"/>
        </td>        
       
        <td>
            Ramo
        </td>
        
        <td >
			<s:select name="filtroCompensacion.idRamo" list="listaRamos" id="filtroCompensacionIdRamo"
				 listKey = "id" listValue="nombre" 
				 headerKey="0" headerValue="Seleccione">
			 </s:select>
        </td>      
        
    </tr>
    <tr>
        <td>
            Numero de Cotizacion
        </td>
        <td colspan="3">
             <s:textfield cssClass="txtfield jQnumeric jQrestrict"  id="filtroCompensacionIdCotizacion" size="10" />
        </td>
        
        <td>
            Estatus
        </td>

        <td  >
           <s:select name="filtroCompensacion.idEstatus" 
                     list="listaEstatus" listKey="id" listValue="nombre" 
                     headerKey="0" headerValue="Seleccione">
           </s:select>
        </td>
        
    </tr>
    <tr>
        <td>
            Número de Póliza
        </td>
        
        <td colspan="3">
             <s:textfield cssClass="txtfield jQnumeric jQrestrict" 
						size="10"						
                        id="numeroPoliza"/>
        </td>
        <td>
            Agente
        </td>
        <td>
    		<table>
    		     <tr>
    		         <td>
    		            <s:textfield cssClass="txtfield jQrestrict w300" size="50" id="nombreAgente" disabled="true"/>
						<s:hidden id="idAgente" name="filtroCompensacion.idAgente"></s:hidden>
    		         </td>
    		         <td>
    		             <div id="divBuscarBtn" style="width:125px" style="display:block;">
        					<div class="btn_back" style="width:125px">
            					<a class="icon_persona" onClick="javascript: seleccionarAgente('AGENTE','','idAgente', 'nombreAgente',true,0, 0,this,false,0,'filtrosBusquedaCompensaciones');" href="javascript: void(0);">Buscar Agente</a>
       			 			</div>
    					</div>
    		         </td>
    		     </tr>
    		</table>
        </td>
    </tr>
    <tr>
       
        <td>
            Número de Negocio
        </td>
        
        <td colspan="3">
            <s:textfield cssClass="txtfield jQnumeric jQrestrict" 
                        size="10"               
                        name="filtroCompensacion.idNegocio"        
                        id="filtroCompensacionIdNegocio"/>
        </td>
        
        <td>
            Proveedor
        </td>
        
        <td>
    		<table>
    		     <tr>
    		         <td>
    		             <s:textfield cssClass="txtfield jQrestrict w300" size="50" id="nombreProveedor" disabled="true" />
						<s:hidden id="idProveedor" name="filtroCompensacion.idProveedor"></s:hidden>
    		         </td>
    		         <td>
    		             <div id="divBuscarBtn" style="width:125px" style="display:block;">
        					<div class="btn_back" style="width:125px">
            					<a class="icon_persona" onClick="javascript: seleccionarAgente('PROVEEDOR','','idProveedor', 'nombreProveedor',true,0, 0,this,false,0,'filtrosBusquedaCompensaciones');" href="javascript: void(0);">Buscar Proveedor</a>
       			 			</div>
    					</div>
    		         </td>
    		     </tr>
    		</table>
        </td>
        
     </tr>
     
     <tr>
        <td>Nombre de Negocio</td>
        <td colspan="3">
    		<table>
    		     <tr>
    		         <td>
    		             <s:textfield cssClass="txtfield jQrestrict w300" size="50" id="nombreNegocio" disabled="true"/>
						<s:hidden id="idNegocio" name="filtroCompensacion.idNegocio"></s:hidden>
    		         </td>
    		         <td>
    		             <div id="divBuscarBtn" style="width:125px" style="display:block;">
        					<div class="btn_back" style="width:125px" >
            					<a class="icon_persona" onClick="javascript: seleccionarAgente('NEGOCIO','','idNegocio', 'nombreNegocio',true,0, 0,this,false,0,'filtrosBusquedaCompensaciones');" href="javascript: void(0);" >Buscar Negocio</a>
       			 			</div>
    					</div>
    		         </td>
    		     </tr>
    		</table>
        </td>
        <td>Promotor</td>
        <td >
    		<table>
    		     <tr>
    		         <td>
    		             <s:textfield cssClass="txtfield jQrestrict w300" size="50" id="nombrePromotor" disabled="true"/>
						 <s:hidden id="idPromotor" name="filtroCompensacion.idPromotor"></s:hidden>
    		         </td>
    		         <td>
    		             <div id="divBuscarBtn" style="width:125px" style="display:block;">
        					<div class="btn_back" style="width:125px" >
            					<a class="icon_persona" onClick="javascript: seleccionarAgente('PROMOTOR','','idPromotor', 'nombrePromotor',true,0, 0,this,false,0,'filtrosBusquedaCompensaciones');" href="javascript: void(0);" >Buscar Promotor</a>
       			 			</div>
    					</div>
    		         </td>
    		     </tr>
    		</table>
        </td>
    </tr>
    
    <tr>
        <td colspan="5"></td>
        <td >
            <div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: buscarEntidadesPersona();">
						Buscar
					</a>
				</div>
        </td>
          <td >
			<div class="btn_back w110">
				<a href="javascript: void(0);" 
					onclick="javascript: limpiarFiltrosEntidadPersona();">
						Limpiar
				</a>
			</div>
         </td>
    </tr>
  </table>
  </s:form>
</div>

<div id="indicador"></div>
<div id="compensacionPaginadoGrid">
	<div id ="compensacionGrid"  class="dataGridConfigurationClass" style="width:98%;height:315px"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</div>




