<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>      
			<call command="setPagingSkin"><param>bricks</param>
		</call>	     	 		
        </beforeInit>        
        <column id="usuario" type="ro" width="200" sort="str" ><s:text name=" Usuario "/></column>
        <column id="fecha"  type="ro"  width="200" sort="str"><s:text name=" Fecha "/></column>
        <column id="movimiento" type="ro" width="300" sort="str"><s:text name="Movimiento"/></column>
	</head>	
	<s:iterator value="listcaBitacora" var="rowBitacora" status="index">
		<row id="${index.count}">			            
 		   	<cell><![CDATA[${usuario}]]></cell>
 		   	<cell><![CDATA[${fecha}]]></cell>
 		   	<cell><![CDATA[${movimiento}]]></cell>
		</row>				
	</s:iterator>
	</rows>