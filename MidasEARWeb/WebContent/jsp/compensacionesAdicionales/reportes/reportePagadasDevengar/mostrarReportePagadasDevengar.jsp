<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/reportes/reportesCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/calculos/generarDocumentos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoOrdenPagosPath + "?tipoAccion="+ tipoAccion;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", null, idField,'ordenPagosModal');
	});

</script>
<s:form action="listarFiltrado" id="ordenPagosCatalogoForm">
  <s:hidden name="tipoAccion"></s:hidden>
  <s:hidden name="tipoArchivo" value="1"></s:hidden>
	<table width="1165px" id="filtrosM2" class="contenedorFormas w910">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name=" Reporte de Compensaciones Pagadas y Por Devengar " />
			</td>
		</tr>
		
  <tbody id="PagosTBody">
  <tr>
    <th>Ramo</th>
    <th>
    <s:select name="ramo" list="ramoLista" id="idRamo" listKey = "id" listValue="nombre" headerKey="0" headerValue="Seleccione"></s:select>
    </th>
    <th></th>
    <th></th>
    <th>Poliza</th>
    <th><s:textfield name="poliza" id="poliza" cssClass="txtField w200"></s:textfield></th>
    <th></th>
  </tr>
  <tr>
    <th><s:text name="Agentes:" /></th>
	<td>
		<s:textfield name="agente" id="txtAgentes_OrdenPagos"	cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
	</td>
	<td width="30px">
		<div class="btn_back w120">
	<a href="javascript:void(0);" class="icon_buscar" onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '','txtAgentes_OrdenPagos',true,0,0,this,false,1,'agregaValorSeleccionadoOrdenesPago',': Agente');">
		<s:text name="Buscar "/>
	</a>
		</div>
	</td>		
		<td></td>
    <th>Fecha Inicio</th>
	<td ><sj:datepicker name="fechaInicio"
		id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
		changeYear="true" changeMonth="true" maxlength="4"
		cssClass="w50 cajaTextoM2"
		onchange="ajustarFechaFinal();"
		onkeypress="return soloFecha(this, event, false);"
		onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
	</td>
    <td></td>
    </tr>
    <tr>
    <td> <s:text name="Proveedor:" />
   </td>
	<td>
		 <s:textfield name="proveedor" id="txtProveedor_OrdenPagos" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
	</td>
	<td width="20px">
	<div class="btn_back w120">
			<a href="javascript:void(0);" class="icon_buscar" onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'txtProveedor_OrdenPagos',true,0,0,this,false,3,'agregaValorSeleccionadoOrdenesPago',': Proveedor');deseleccionarCheck('proveedoresTodos')">
					<s:text name="Buscar "/>
			</a>
	</div>
	</td>
    <td></td>
    <th>fecha Final </th>
	<td ><sj:datepicker name="fechaFinal"
			    id="txtFechaFinal" buttonImage="../img/b_calendario.gif"
				changeYear="true" changeMonth="true" maxlength="4"
				cssClass="w50 cajaTextoM2"
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
	</td>
    <td></td>
  </tr>
  <tr>
	<th><s:text name="Gerencia" /></th>
	<th>
			<s:textfield name="gerencia" id="idTxtGerRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
	</th>									
	<th>
		<div class="btn_back w120">
					<a href="javascript:void(0);" class="icon_buscar" 
								onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'idTxtGerRepCa',true,0,0,this,false,6,'agregaValorSeleccionadoOrdenesPago',': Gerencia');">
							   <s:text name="Buscar "/>
					</a>
		</div>
	</th>
					</tr>
  
   <tr>
   <td><s:text name="Promotor:" /></td>

	<td>
		<s:textfield name="promotor" id="txtPromotor_OrdenPagos" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
	</td>

	<td width="20px">
		<div class="btn_back w120">
			<a href="javascript:void(0);" class="icon_buscar" onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'txtPromotor_OrdenPagos',true,0,0,this,false,2,'agregaValorSeleccionadoOrdenesPago',': Promotor');deseleccionarCheck('promotoresTodos')">
					<s:text name="Buscar "/>
			</a>
		</div>
	</td>	
    
  </tr>
  
 
  </tbody>
		<tr>
    <td colspan="3">
    Tipo de Reporte
    <select id="select_tipoReporte" onchange="tipoReporte()" class="txtField w200">
              <option value="5">Reporte de Compensaciones Pagadas y Por Devengar</option>
             
    </select>
    </td>
    <td></td>
    <td colspan="3"><div class="btn_back w120">
          <a href="javascript: void(0);" class="icon_buscar"
            onclick="imprimirReporteEstadoCuenta();">
            <s:text name="Exportar"/> </a>
         </div>
    </td>
    </tr>
		
	</table>
</s:form>
