<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/reportes/reportesCompensaciones.js'/>"></script>
<s:hidden name="idField"></s:hidden>
<s:form action="listarFiltrado" id="reporteDerechoPolizaForm">
  <s:hidden name="tipoAccion"></s:hidden>
  <s:hidden name="tipoReporte" value="2"></s:hidden>
	<table class="contenedorFormas w910">
		<tr>
			<td colspan="6" class="titulo"><s:text
					name="Reporte de Compensaciones por Derecho de Póliza"></s:text></td>
		</tr>
		<tr>
			<td>
				<table class="contenedorFormas no-border">
					<tr>
						<th><s:text name="Ramo:" /></th>
						<th>
							<s:select name="ramo" list="ramoLista" id="idRamo"
                      			listKey = "id" listValue="nombre" 
								headerKey="0" headerValue="Seleccione"></s:select>
						</th>
					</tr>					
					<tr>
						<th><s:text name="Agente" /></th>
						  <th><s:textfield name="agente" id="agente" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield></th>			
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'agente',true,0,0,this,false,1,'agregaValorSeleccionadoOrdenesPago',': Agente');">
										<s:text name="Buscar Agente"/>
								</a>
							</div>
						</th>
				   </tr>
					<tr>
						<th><s:text name="Promotor" /></th>
						<th>
						    <s:textfield name="promotor" id="idTxtPromRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>						
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'idTxtPromRepCa',true,0,0,this,false,2,'agregaValorSeleccionadoOrdenesPago',': Promotor');">
									<s:text name="Buscar Promotor"/>
								</a>
							</div>
						</th>
					</tr>
					
				</table></td>
			<td valign="top">
				<table class="contenedorFormas no-border">
					<tr>
						<th width="85px"><s:text name="ID Negocio" /></th>
						<td width="275px"><s:textfield name="negocio" id="idNegocio" cssClass="cajaTextoM2 w200"></s:textfield></td>
					</tr>
					<tr>
						<th width="70px"><s:text name="Nombre Negocio"/></th>
						<td width="275px"><s:textfield name="nombreNegocio"	id="idNombreNegocio" cssClass="cajaTextoM2 w200"></s:textfield></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 20px"></td>
					</tr>
					<tr>
						<th><s:text name="Fecha Inicial" /></th>	
						<th><sj:datepicker name="fechaInicio"
							id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="4"
							cssClass="w100 cajaTextoM2"
							onchange="ajustarFechaFinal();"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
					   </th>					
					</tr>
					<tr>
						<th><s:text name="Fecha Final" /></th>
						<td ><sj:datepicker name="fechaFinal"
							id="txtFechaFinal" buttonImage="../img/b_calendario.gif"
							changeYear="true" changeMonth="true" maxlength="4"
							cssClass="w100 cajaTextoM2"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
						</td>						
					</tr>						
				</table>
		 	</td>				
		</tr>
	</table>
				<tr>
					<s:if test="tipoAccion!=\"consulta\"">
						<div class="w880" align="right">
							<div class="btn_back w200">
								<a href="javascript: void(0);" onclick="imprimirReporteDerechoPoliza()">
									<s:text name="Generar Reporte"/>
								</a>
							</div>
						</div>
					</s:if>
			   </tr>

<!-- 	<div id="divCarga" style="position: absolute;"></div> -->
<!-- 	<div id="ordenPagosGrid" class="w1110 h150" style="overflow: hidden"></div> -->

</s:form>

