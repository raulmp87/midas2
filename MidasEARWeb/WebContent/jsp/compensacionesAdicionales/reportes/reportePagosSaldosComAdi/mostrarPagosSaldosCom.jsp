<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/reportes/reportesCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/calculos/generarDocumentos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoOrdenPagosPath + "?tipoAccion="+ tipoAccion;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", null, idField,'ordenPagosModal');
	});

</script>
<s:form action="listarFiltrado" id="ordenPagosCatalogoForm">
  <s:hidden name="tipoAccion"></s:hidden>
	<table width="1165px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name=" Reporte de Pagos y Saldos " />
			</td>
		</tr>
  <tbody id="PagosTBody">
  <tr>
		<th width="">ID Compensaciones:</th>
		  <td width=""><s:textfield name="folio" id="txtFolio" readonly="false" cssClass="cajaTextoM2 w100"></s:textfield></td>
			<th><s:text name="Agente" /></th>
				<th><s:textfield name="agente" id="agente" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield></th>			
			<th>
				<div class="btn_back w120">
					<a href="javascript:void(0);" class="icon_buscar" 
							onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'agente',true,0,0,this,false,1,'agregaValorSeleccionadoOrdenesPago',': Agente');">
								<s:text name="Buscar Agente"/>
						</a>
					</div>
				</th>
				</tr>
				<tr>
				<th width="">Cotización:</th>
				<td width=""><s:textfield name="cotizacion:" id="txtCotizacion" readonly="false" cssClass="cajaTextoM2 w100"></s:textfield></td>
						<th><s:text name="Promotor" /></th>
						<th>
						    <s:textfield name="promotor" id="idTxtPromRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>						
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'idTxtPromRepCa',true,0,0,this,false,2,'agregaValorSeleccionadoOrdenesPago',': Promotor');">
									<s:text name="Buscar Promotor"/>
								</a>
							</div>
						</th>
						 	
				   </tr>
				   <tr>
				   <th width="">Poliza:</th>
				   <td width=""><s:textfield name="poliza:" id="txtPoliza" readonly="false" cssClass="cajaTextoM2 w100"></s:textfield></td>
				   <th><s:text name="Proveedor" /></th>
						<th>
						    <s:textfield name="proveedor" id="idTxtProvRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>									
						<th>
							<div class="btn_back w120" align="right">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'idTxtProvRepCa',true,0,0,this,false,3,'agregaValorSeleccionadoOrdenesPago',': Proveedor');">
									<s:text name="Buscar Proveedor"/>
								</a>
							</div>
						</th>
				   </tr>
				   <td></td>
				   <th>Fecha Inicio</th>
			<td ><sj:datepicker name="fechaInicio"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="4"
					cssClass="w50 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
			    <th>fecha Final </th>
			<td ><sj:datepicker name="fechaFinal"
					id="txtFechaFinal" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="4"
					cssClass="w50 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
  </tbody>

  </table>
   <td></td>
   <table>
    <td colspan="3" align="right"><div class="btn_back w110">
          <a href="javascript: void(0);" class="icon_buscar"
            onclick="imprimirReportePagosSaldos();">
            <s:text name="Exportar"/> </a>
         </div></td>
         </table>
</s:form>
