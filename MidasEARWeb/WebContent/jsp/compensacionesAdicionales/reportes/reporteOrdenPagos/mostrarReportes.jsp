<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/reportes/reportesCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/calculos/generarDocumentos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoOrdenPagosPath + "?tipoAccion="+ tipoAccion;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", null, idField,'ordenPagosModal');
	});
	function generarExcelCompensaciones(idLiquidacion){
		var reporteUrl = "/MidasWeb/compensacionesAdicionales/ordenPago/generarExcelDetallePagos.action"+"?idLiquidacion="+idLiquidacion;
		window.open(reporteUrl,"generarExcelDetallePagos");
	
	}
	function listarTodos(){
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarTodosOrdenPagosPath;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", document.ordenPagosCatalogoForm, idField,'ordenPagosModal');
		listarFiltradoGenerico(urlFiltro, 'ordenPagosGrid',document.ordenPagosCatalogoForm,'${idField}','ordenPagosModal')
	}
	function limpiarTodos(){
		jQuery("input:text").val("");
		jQuery(".txtHidden").val("");
</script>
<s:form action="listarFiltrado" id="ordenPagosCatalogoForm">
  <s:hidden name="tipoAccion"></s:hidden>
  <s:hidden name="tipoArchivo" value="1"></s:hidden>
	<table class="contenedorFormas w1100">
		<tr>
			<td class="titulo" colspan="6"><s:text
					name="Generar Reporte " />
			</td>
		</tr>
		
		<tbody id="EstadoCuentaTBody">
		 <tr>
    <th>Año</th>
    <th><select id="anios" name="anio" class="txtField w200" ></select></th>
    <th></th>
    <th></th>
    <th>Mes</th>
    <th><select id="meses" name="mes" class="txtField w200" ></select></th>
    <th></th>
    </tr>
    <tr>
    <th>Clave Agente </th>
       <td><s:textfield name="cveAgente" id="cveAgente" cssClass="txtField w200"></s:textfield></td>
    <td></td>
    <td></td>
      <th>Clave Promotor</th>
      <td><s:textfield name="cvePromotor" id="cvePromotor" cssClass="txtField w200" ></s:textfield></td>
    <td></td>
    </tr>
    <tr>
    <th>Clave Proveedor</th>
    <td><s:textfield name="cveProveedor" id="cveProveedor" cssClass="txtField w200" ></s:textfield></td>
    <td></td>
    <td></td>
    <th>Formato de Salida</th>
    <td><select id="select_tipoSalida" class="txtField w200">
              <option value="">Seleccione...</option>
              <option value="0">PDF</option>
            </select>
    </td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </tbody>
  
  <tbody id="PagosTBody" style="display:none;">
  <tr>
    <th>Ramo</th>
    <th>
    <s:select name="ramo" list="ramoLista" id="idRamo" listKey = "id" listValue="nombre" headerKey="0" headerValue="Seleccione"></s:select>
    </th>
    <th></th>
    <th></th>
    <th>Poliza</th>
    <th><s:textfield name="poliza" id="poliza" cssClass="txtField w200"></s:textfield></th>
    <th></th>
  </tr>
  <tr>
						<th>
							Agente
						</th>
						<th>
  						  <s:textfield name="agente" id="agente" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'agente',true,0,0,this,false,1,'agregaValorSeleccionadoOrdenesPago',': Agente');">
										<s:text name="Buscar Agente"/>
								</a>
							</div>
						</th>				   
    <td></td>
    <th>Fecha Inicio</th>
			<td ><sj:datepicker name="fechaInicio"
					id="txtFechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="4"
					cssClass="w50 cajaTextoM2"
					onchange="ajustarFechaFinal();"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
    <td></td>
  </tr>
  <tr>
                      <th>
						Proveedor
					 </th>
						<th>
						    <s:textfield name="proveedor" id="idTxtProvRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>									
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'idTxtProvRepCa',true,0,0,this,false,3,'agregaValorSeleccionadoOrdenesPago',': Proveedor');">
									<s:text name="Buscar Proveedor"/>
								</a>
							</div>
						</th>
    <td></td>
    <th>fecha Final </th>
			<td ><sj:datepicker name="fechaFinal"
					id="txtFechaFinal" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="4"
					cssClass="w50 cajaTextoM2"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
    <td></td>
  </tr>
  <tr>
    <th>
						Gerencia
						          </th>
						<th>
						    <s:textfield name="gerencia" id="idTxtGerRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>									
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
									onclick="javascript: seleccionarAgenteTipo('ORDEN_PAGOS','','', 'idTxtGerRepCa',true,0,0,this,false,6,'agregaValorSeleccionadoOrdenesPago',': Gerencia');">
									<s:text name="Buscar Gerencia"/>
								</a>
							</div>
						</th>
    <td></td>
    <th>Formato de salida</th>
    <td><select id="select_tipoSalida" class="txtField w200">
              <option value="">Seleccione...</option>
              <option value="0">Excel</option>
            </select></td>
    <td></td>
  </tr>
  
   <tr>
   <th>
						Promotor
						          </th>
						<th>
						    <s:textfield name="promotor" id="idTxtPromRepCa" cssClass="cajaTextoM2 w150" disabled="false"></s:textfield>
						</th>						
						<th>
							<div class="btn_back w120">
								<a href="javascript:void(0);" class="icon_buscar" 
								onclick="javascript:seleccionarAgenteTipo('ORDEN_PAGOS', '', '', 'idTxtPromRepCa',true,0,0,this,false,2,'agregaValorSeleccionadoOrdenesPago',': Promotor');">
									<s:text name="Buscar Prromotor"/>
								</a>
							</div>
						</th>
    <td></td>
    
  </tr>
  
 
  </tbody>

  <tr>
    <td colspan="3">
    Tipo de Reporte
    <select id="select_tipoReporte" onchange="tipoReporte()" class="txtField w200">
              <option value="">Seleccione...</option>
              <option value="1">Estado de Cuenta</option>
              <option value="2">Pago de Compensaciones y Contraprestaciones</option>
              <option value="3">Reporte de Honorarios</option>
              <option value="4">Reporte Detalle Primas</option>
             
            </select></td>
    <td></td>
    <td colspan="3"><div class="btn_back w110">
          <a href="javascript: void(0);" class="icon_buscar"
            onclick="imprimirReporteEstadoCuenta();">
            <s:text name="Exportar"/> </a>
         </div></td>
  </tr>
		
	</table>
</s:form>
