<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/compensacionesAdicionales.js'/>" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/reportes/reportesCompensaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/calculos/generarDocumentos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<s:include value="/jsp/compensacionesAdicionales/ordenPagos/ordenPagosHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	jQuery(function() {
		var tipoAccion = '<s:property value="tipoAccion"/>';
		var urlFiltro = listarFiltradoOrdenPagosPath + "?tipoAccion="+ tipoAccion;
		var idField = '<s:property value="idField"/>';
		listarFiltradoGenerico(urlFiltro, "ordenPagosGrid", null, idField,'ordenPagosModal');
	});
</script>
<s:form action="listarFiltrado" id="ordenPagosCatalogoForm">
  <s:hidden name="tipoAccion"></s:hidden>
  <s:hidden name="anioMes" id="anioMes"></s:hidden>
  <s:hidden name="tipoArchivo" value="1"></s:hidden>
	<table width="1165px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4"><s:text
					name=" Reporte Contraprestaciones Banca Seguros " />
			</td>
		</tr>
  <tbody id="PagosTBody">
  <tr>
    <td></td>
    <th>Año</th>
    <td><select id="anios2" name="anios2" class="txtField w200" ></select></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <th>Mes</th>
    <td><select id="meses2" name="meses2" class="txtField w200" ></select></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">
    Gerencia:
    <select id="gerencia" name="gerencia"  class="txtField w200">
              <option value="">Seleccione...</option>
              <option value="1">Banca Seguros Venta Libre</option>
              <option value="2">Banca Seguros Ligados 1</option>
            </select></td>
    <td></td>
  </tr>
 
  </tbody>

  <tr>
    <td></td>
    <td colspan="3"><div class="btn_back w110">
          <a href="javascript: void(0);" class="icon_buscar"
            onclick="imprimirReporteBancaSeguros();">
            <s:text name="Exportar"/> </a>
         </div></td>
  </tr>
	</table>
</s:form>
