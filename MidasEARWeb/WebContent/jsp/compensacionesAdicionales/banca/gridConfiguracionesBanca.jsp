<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
        </beforeInit>
        <column id="colSelect" type="ro" width="30" sort="na"></column>
      	<column id="colId" type="ro" width="120" sort="na">Id</column>
		<column id="colNombre" type="ro" width="300" sort="na">Nombre</column>
		<column id="colAcciones" type="ro" width="100" sort="na">Acciones</column>
	</head>
	<s:iterator value="listCaConfiguracionBanca" var="caBanca" status="index">
		<row id="${index.count}">
			<cell><![CDATA[<input type="checkbox" value="${caBanca.id}" onclick="validateChecksList(this)">]]></cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[<a href="javascript:void(0)" onclick="editarConfiguracionBanca(${caBanca.id})"><img src="../img/icons/ico_editar.gif"></a>]]></cell>
		</row>
	</s:iterator>
</rows>