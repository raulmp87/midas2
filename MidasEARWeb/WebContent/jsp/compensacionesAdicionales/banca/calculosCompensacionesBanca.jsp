<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!-- HEADER CONFIGURADOR BANCA -->
<script type="text/javascript" src="<s:url value="/js/midas2/compensacionesAdicionales/banca/ConfiguradorBanca.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/compensacionesAdicionales/banca/CalculosCompensacionBanca.js"/>"></script>


<div class="titulo" style="width: 98%">
	Calculos Contraprestaciones Banca Seguros
</div>
<br/>
<s:form action="" id="formConfiguracionContraprestacionBanca" name="formConfiguracionContraprestacionBancaName">
	<div style="width: 98%">
	   <table class="contenedorConFormato" style="width: inherit;" cellpadding="1" cellspacing="1" >
	 	<tr>
	 		<td colspan="2">
	 			Gerencia:	
	 		</td>
	 		<td colspan="2">
	 			<div id="divListGerenciasId" style="height: 115px; width: 260px; overflow: auto">
	  			    <s:iterator value="listGerencias" var="gerenciaSeycos">
	  			    	<input 	type="checkbox" 
	  			    			class="checkboxLabel"
	  			    			onclick="getGridListConfiguracionesBanca()"
	  			    			name="configuracionContraprestacionBanca.listGerenciasSeycosId" 
	  			    			value="${gerenciaSeycos.idGerencia}"
	  			    			idempresa="${gerenciaSeycos.idEmpresa}"
	  			    			fsit="${gerenciaSeycos.FSit.time}">
			    		<label class="checkboxLabel">
			    				${gerenciaSeycos.nomGerencia}
	    			   	</label>			    		
	  			    </s:iterator>
	   			</div>
	 		</td>
	 		<td colspan="2">
	 			Linea de Venta:
	 		</td>
	 		<td colspan="2">
	 			<div id="divListLineasVentaId" style="height: 100px; width: 260px; overflow: auto">
	  			    <s:iterator value="listLineasVenta" var="lineaVenta">
	  			    	<input 	type="checkbox" 
	  			    			class="checkboxLabel"
	  			    			onclick="getGridListConfiguracionesBanca()"
	  			    			name="configuracionContraprestacionBanca.listLineasVenta" 
	  			    			value="${lineaVenta.id}">
			    		<label class="checkboxLabel">
			    				${lineaVenta.nombre}
	    			   	</label>			    		
	  			    </s:iterator>
	   			</div>
	 			
	 		</td>
	 		<td colspan="2">
	 			Fecha:
	 		</td>
	 		<td colspan="2">
	 			<sj:datepicker name="configuracionContraprestacionBanca.fechaCalculoCompensacion"
							   buttonImage="../img/b_calendario.gif"
							   id="fechaId" 
							   maxlength="10" 
							   cssClass="txtfield"			
							   maxDate="today"									   		   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							   onblur="esFechaValida(this);"></sj:datepicker>	
	 		</td>
	 	</tr>
	 	<tr>
	 		<td colspan="12">
	 			<br/>
	 		</td>
	 	</tr>
	 	<tr>
	 		<td colspan="2" rowspan="3">
	 			Ramo:
	 		</td>
	 		<td colspan="4" rowspan="3">
				<div id="divListRamosId" style="height: 150px; width: 260px; overflow: auto">
	       			<s:checkboxlist list="listRamos"  
	       							id="listRamosId"
	       							labelposition="left"
	       							onclick="getGridListConfiguracionesBanca()"
	       							tabindex="true"
	       							name="configuracionContraprestacionBanca.listRamosId"
	       							listKey="idRamoContable"
	       							listValue="nomRamo" > 
	  				</s:checkboxlist>
	   			</div>
	 		</td> 		
	 	</tr>
	 	<tr>
	 		<td colspan="2">
	 			Selección de Parámetros para Cálculo:
	 		</td>
	 	</tr>
	 	<tr>
	 		<td colspan="6">
	 			<div  id="divListConfiguracionesBancaId" style="height: 130px; width: 440px; border: 1px; color: green;">
	 			</div>
	 		</td>
	 	</tr>
	 	<tr>
	 		<td colspan="10">
	 		</td>
	 		<td colspan="2">
	 			<div class="btn_back w120">
					<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:getGridResultCalculosBanca();">
						Ejecutar
					</a>
				</div>	 	
	 		</td>
	 	</tr>
	 	
	 	<tr>
	 		<td colspan="12">
	 			<br/>
	 			<div class="titulo" style="font-size: 12px;" >
	 				Calculos
	 			</div>	 			
	 		</td>
	 	</tr>
 		<tr>
	 		<td colspan="12">
	 			<div  id="divResultsCalculosBanca" style="height: 150px; width auto; border: 1px; color: green;">
	 			</div>
	 		</td>
	 	</tr>
	 	<tr>
	 		<td colspan="9">
	 		</td>
	 		<td>
	 			<div class="btn_back w120">
					<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:provisionarCalculosBanca();">
						Provisionar
					</a>
				</div>	 			
	 		</td>
	 		<td>	 			 
	 			<div class="btn_back w120" >	 				
						<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:sendResultCalculos();"  >
							Exportar a Excel
						</a>
				</div>				 		 			
	 		</td>
	 		<td>
	 			<div class="btn_back w120">
					<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:;">
						Cancelar
					</a>
				</div>	 		 			
	 		</td>
	 	<tr>
	   </table>   
	</div>
</s:form>
<div id="divMensajesConfiguracionContraprestacionBanca"></div>
<iframe id="iframeMensajesCompensacionesAdicionalesId"  name="iframeMensajesCompensacionesAdicionales" style="display: none" ></iframe>
<div style="display: none" id="divformExportExcelCalculosBancaId"></div>
<style type="text/css">
	.selectEstado{
		background: gainsboro;
	}
	.checkboxLabel:after {
    	content:"\A"; white-space:pre;
	}
	.ui-datepicker-trigger{
		vertical-align: bottom;
	}
</style>