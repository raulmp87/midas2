<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!-- HEADER CONFIGURADOR BANCA -->
<s:include value="/jsp/compensacionesAdicionales/banca/configuracionBancaHeader.jsp"></s:include>

<div class="titulo" style="width: 100%">
	Configuraci&oacute;n de Contraprestaciones
</div>
<br/>
<s:form action="" id="formConfiguracionContraprestacionBanca" name="formConfiguracionContraprestacionBancaName">
	<div style="width: 98%">
	   <table class="contenedorConFormato" style="width: inherit;">
	      <tr>
		      <td width="7%">
		      	Id de Configuracion:
		      </td>
		      <td width="22%">
		      	<s:textfield id="bancaConfiguracionId" name="configuracionContraprestacionBanca.caConfiguracionBanca.id" disabled="true"> </s:textfield>
		      </td>
		      <td width="7%">
		      	Nombre de Configuracion:
		      </td>
		      <td width="22%">
		      	<s:textfield id="nombreConfiguracionId" name="configuracionContraprestacionBanca.caConfiguracionBanca.nombre" > </s:textfield>
		      </td>
		      <td colspan="3" width="42%">
		      	
		      </td>
	      </tr>
	      <tr>
	     	<td width="7%">
		      	Linea de Venta:
	      	</td>
		    <td width="23%" colspan="3">
			    <s:select id="listLineasVenta" 
					  name="configuracionContraprestacionBanca.lineaVentaId" 
					  cssClass="cajaTextoM2 w100 jQrequired"
					  headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"								  
					  list="listLineasVenta" 
					  listKey="id" 
					  listValue="nombre">
				</s:select>	      	
		    </td>	
		    <td>
		      	Seleccionar Estado:
	        </td>
	      <tr>
	      <tr>
	      
	      	<td rowspan="3">
	      		Ramo:
	      	</td>
	      	<td rowspan="3">
	 			<div id="divListRamosId" style="height: 250px; width: 260px; overflow: auto">
	       			<s:checkboxlist list="listRamos"  
	       							id="listRamosId"
	       							labelposition="left"    							
	       							name="configuracionContraprestacionBanca.listRamosId"
	       							value="configuracionContraprestacionBanca.listRamosId"	       							
	       							listKey="idRamoContable"
	       							listValue="nomRamo" > 
	  				</s:checkboxlist>
	   			</div>
	      	</td>
	      	<td rowspan="3">
	      		Gerencia:
	      	</td>
	      	<td rowspan="3">
	      		<div id="divListGerenciasId" style="height: 250px; width: 260px; overflow: auto">
	      			<s:set name="gerencia" value="configuracionContraprestacionBanca.gerenciaSeycosId.idGerencia"/>
	  			    <s:iterator value="listGerencias" var="gerenciaSeycos">
	  			    	<s:if test="%{idGerencia eq configuracionContraprestacionBanca.gerenciaSeycosId.idGerencia}">
	  			    		<input 	type="radio" 
	  			    			class="checkboxLabel"
	  			    			name="configuracionContraprestacionBanca.gerenciaSeycosId"
	  			    			checked="checked" 
	  			    			value="${gerenciaSeycos.idGerencia}"
	  			    			idempresa="${gerenciaSeycos.idEmpresa}"	
	  			    			fsit="${gerenciaSeycos.FSit.time}">	  
	  			    	</s:if>
	  			    	<s:else>
	  			    		<input 	type="radio" 
	  			    			class="checkboxLabel"
	  			    			name="configuracionContraprestacionBanca.gerenciaSeycosId" 
	  			    			value="${gerenciaSeycos.idGerencia}"
	  			    			idempresa="${gerenciaSeycos.idEmpresa}"
	  			    			fsit="${gerenciaSeycos.FSit.time}">	  		
			    		</s:else>	    		
			    		<label class="checkboxLabel">
			    				${gerenciaSeycos.nomGerencia}
	    			   	</label>			    		
	  			    </s:iterator>
	   			</div>
	      	</td>
	      	
	      	<td colspan="2">
	      		<div id="divListEstadosId" style="height: 300px; width: 260px; overflow: auto">
	      			<ul id="listEstadosId">
		      			<s:iterator value="listEstados" var="varEstado" >             				
		      				<li value="${varEstado.id}" title="${varEstado.stateName}" onclick="selectEstado(this)" style="cursor: pointer; padding: 5px">
		      					${varEstado.stateName}
		      				</li>             				
		      			</s:iterator>
	      			</ul>
	    		</div>
	      	</td>      	
	      	<td>
	      		<div class="btn_back w120">
					<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="agregarEstado();">
						Agregar
					</a>
				</div>
	      	</td>
	      </tr>
	      <tr>
	      	<td colspan="4">
	      		<br/>
	      		Estados Activos:
	      	</td>
	      </tr>      
	      <tr>
	      	<td colspan="4">
				<div id="divGridEstadosId" style="overflow:hidden; width: 360px; height: 120px">
				</div>
	      	</td>
	      </tr> 
	      <tr>
	      	<td> 
	      		Ultima Modificacion:     		
	      	</td>
	      	<td>      		
	      		<s:textfield id="ultimaModificacionId" 
	      					 name="configuracionContraprestacionBanca.caConfiguracionBanca.fechaModificacion" 
	      					 disabled="true"> 
    			</s:textfield>
	      	</td>
	      	<td>      		
	      		Usuario
	      	</td>
      	 	<td>      		
	      		<s:textfield id="usuarioId"  
	      					 name="configuracionContraprestacionBanca.caConfiguracionBanca.usuario"
	      					 disabled="true"> 
      			</s:textfield>
	      	</td>
	      	<td colspan="3">
		      	<table>
		      		<tr>
			     
			      	<td>   
			      		<div class="btn_back w100">
							<a type="anchor" id="botonHistorico" href="javascript:void(0);" onclick="javascript:historicoConfiguracionBanca();">
								Historico
							</a>
						</div> 		
			      	</td>
		      		<td>   	
			      		<div class="btn_back w100">
							<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:openCompensacionAdicional();">
								Compensaciones
							</a>
						</div>	
			      	</td>
			      	<td>	
			      		<div class="btn_back w100">
							<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:guardarConfiguracionContraprestacionBanca();">
								Aceptar
							</a>
						</div>	
			      	</td>
			      	<td>	
			      		<div class="btn_back w100">
							<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:returnListado();">
								Cancelar
							</a>
						</div>	
			      	</td>
			      	</tr>
		      	</table>
	      	</td>
	      </tr>  
	   </table>   
	</div>
</s:form>
<div id="divMensajesConfiguracionContraprestacionBanca"></div>
<script type="text/javascript">

/**
 * Variable que contiene el Objecto Grid de Estados Activos
 */
gridEstadosActivos = getGridEstadosActivos();
	
</script>