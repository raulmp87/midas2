<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="setColumnId">
            	<param>2</param>
            	<param>id</param>
            </call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="colId" type="ro" width="0" style="display:none" sort="na">idTable</column>
        <column id="colConfiguracionBancaId" type="ro" width="0" style="display:none" sort="na">ConfiguracionBancaId</column>  
		<column id="colEstadoId" type="ro" width="120" sort="na">id</column>
		<column id="colEstadoNombre" type="ro" width="170" sort="na">Estado</column>
		<column id="colAcciones" type="ro" width="70" sort="na">Acciones</column>
	</head>
	<s:iterator value="configuracionContraprestacionBanca.listBancaEstadosActivos" var="bancaEstadosView" status="index">
		<row id="${bancaEstadosView.estadoId}">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="configuracionBancaId" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="estadoId" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="estadoNombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[<a id="${index.count}" href="javascript:void(0)" onclick="eliminarEstado(${bancaEstadosView.estadoId})"> <img src="../img/icons/ico_eliminar.gif"></a>]]></cell>
		</row>
	</s:iterator>
</rows>