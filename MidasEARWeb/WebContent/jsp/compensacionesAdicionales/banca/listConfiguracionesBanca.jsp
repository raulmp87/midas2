<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!-- HEADER CONFIGURADOR BANCA -->
<s:include value="/jsp/compensacionesAdicionales/banca/configuracionBancaHeader.jsp"></s:include>

<div class="titulo" style="width: 98%">
	Listado de Configuraciones Contraprestacion de Banca
</div>
<br/>
<div style="width: 66%">
   <table class="contenedorConFormato" style="width: inherit;">
      <tr>
      	<td align="right">	  	
			<div class="btn_back w120">
				<a type="anchor" id="botonAgregarEstadosId" href="javascript:void(0);" onclick="javascript:agregarNuevaConfiguracion();">
					Agregar
				</a>
			</div>
		</td>
      </tr> 
       <tr>
       	<td>
	  		<div id="divListGridConfiguracionesBanca" class="contenedorConFormato" style="height: 200px; width: auto"></div>
	  	</td>
      </tr>   
   </table>   
</div>

<script type="text/javascript">

function agregarNuevaConfiguracion (){
	var url = '/MidasWeb/compensacionesAdicionales/banca/mostrarConfiguracionInicio.action';
	sendRequest(null, url, 'contenido', 'gridEstadosActivos = getGridEstadosActivos();');
}

function editarConfiguracionBanca(id){
	var url = '/MidasWeb/compensacionesAdicionales/banca/mostrarConfiguracionInicio.action?configuracionContraprestacionBanca.caConfiguracionBanca.id='+id;
	sendRequest(null, url, 'contenido', 'gridEstadosActivos = getGridEstadosActivos();');
}

var getGridConfiguracionesBanca = function(){		
	var url = '/MidasWeb/compensacionesAdicionales/banca/gridConfiguracionesBanca.action';
	var grid = listarFiltradoGenerico(url,"divListGridConfiguracionesBanca", null,null,null,null,null);
	grid.setColumnHidden(0, true);
	grid.setColumnMinWidth('0,120,534,90');
	return grid;
}

var gridConfiguracionesBanca = getGridConfiguracionesBanca();
</script>