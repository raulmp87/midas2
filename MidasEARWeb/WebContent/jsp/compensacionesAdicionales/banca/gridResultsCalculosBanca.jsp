<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="setColumnId">
            	<param>2</param>
            	<param>id</param>
            </call>    
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
		<column id="colRamo" type="ro" width="80" sort="na" title="Ramo" >Ramo</column>
		<column id="colCodigoRamo" type="ro" width="80" sort="na" title="CodigoRamo" >Codigo Ramo</column>
		<column id="colPrimaNetaEmitida" type="ro" width="82" sort="na" >P.N.Emitida</column>
		<column id="colPrimaNetaPagada" type="ro" width="80" sort="na" >P.N.Pagada</column>		
		<column id="colPorcentajeContraprestacionProducto" type="ro" width="90" sort="na" >%ContraProduc</column>
		<column id="colMontoContraprestacionProducto" type="ro" width="90" sort="na" >$ContraProduc</column>			
		<column id="colPorcentajeContraprestacionCumplimientoMeta" type="ro" width="90" sort="na" >%ContraCumplMeta</column>
		<column id="colMontoContraprestacionCumplimientoMeta" type="ro" width="90" sort="na" >$ContraCumplMeta</column>
		<column id="colPorcentajeCalidadCartera" type="ro" width="82" sort="na" >%CalCartera</column>
		<column id="colMontoCalidadCartera" type="ro" width="82" sort="na" >$CalCartera</column>
		<column id="colBonoFijo" type="ro" width="80" sort="na" >BonoFijo</column>
		<column id="colDescuento" type="ro" width="80" sort="na" >Descuento</column>
		<column id="colTotal" type="ro" width="80" sort="na" >Total</column>
		<column id="colTotalBase" type="ro" width="80" sort="na" >TotalBase</column>
		<column id="colComentarios" type="ro" width="90" sort="na" >Comentarios</column>
	</head>
	<s:iterator value="listResultsCalculosBanca" var="calculosBanca" status="index">
		<row id="${index.count}">
			<cell><s:property value="ramo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigoRamo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaNetaEmitida" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaNetaPagada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="porcentajeContraprestacionProducto" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="montoContraprestacionProducto" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="porcentajeContraprestacionCumplimientoMeta" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="montoContraprestacionCumplimientoMeta" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="porcentajeCalidadCartera" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="montoCalidadCartera" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="bonoFijo" escapeHtml="false" escapeXml="true" /></cell> 	
			<cell><![CDATA[<s:textfield value="%{#calculosBanca.descuento}"  id="descuento_%{#index.count}"
			cssClass="cajaTextoM2 w100 inicialNivel" title="%{#calculosBanca.descuento}" onkeyup="aplicarDescuento(this)"></s:textfield>]]>
			</cell>	
			<cell><![CDATA[<s:textfield value="%{#calculosBanca.total}"  id="total_%{#index.count}" disabled="true"
			cssClass="cajaTextoM2 w100 inicialNivel" title="%{#calculosBanca.total}"></s:textfield>]]>
			</cell>	
			<cell><![CDATA[<s:textfield value="%{#calculosBanca.total}"  id="totalBase_%{#index.count}"
			cssClass="cajaTextoM2 w100 inicialNivel"></s:textfield>]]>
			</cell>	
				
			<cell><s:property value="comentarios" escapeHtml="false" escapeXml="true"/></cell>			
		</row>
	</s:iterator>
</rows>