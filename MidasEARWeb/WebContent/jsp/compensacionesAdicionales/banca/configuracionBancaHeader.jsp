<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
<link href="<s:url value="/css/dhtmlxcalendar_yahoolike.css"/>" rel="stylesheet" type="text/css">		
<link href="<s:url value="/css/dhtmlxcombo.css"/>" rel="stylesheet" type="text/css">			
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<link href='<s:url value="/css/toastr.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptDataGrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptComponents.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtree.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtree_sb.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_filter.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_srnd.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_sub_row.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_drag.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_group.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_link.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_nxml.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxdataprocessor.js"/>"></script>	
<script type="text/javascript" src="<s:url value="/js/dhtmlxaccordion.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxtabbar.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxvault.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxUtil.js"/>"></script>


<script type="text/javascript" src="<s:url value="/js/midas2/compensacionesAdicionales/banca/ConfiguradorBanca.js"/>"></script>

<style type="text/css">
	.selectEstado{
		background: gainsboro;
	}
	.checkboxLabel:after {
    	content:"\A"; white-space:pre;
	}
</style>
