<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script src="<s:url value='/js/midas2/compensacionesAdicionales/banca/compadicbanca.js'/>"></script>

<div class="titulo" style="width: 98%'">
C&aacute;lculos de Compensaciones Adicionales para BANCA
</div>

<br>
<div style="min-height: 300px; border-width:1px; border-color: green; border-style:solid;">
<table class="tablaConResultadosPoliza" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<th>Num.</th>
			<th>Linea de Negocio</th>
			<th>Ramo</th>
			<th>Tipo de Compensaci&oacute;n</th>
			<th>Base para el C&aacute;lculo</th>
			<th>% Comp. Meta
			    <br>
			    De Hasta
			</th>
			<th>% Calidad Cartera
			    <br>
			    De  Hasta
			</th>
			<th>Contraprestaci&oacute;n</th>
		</tr>
	</tbody>
</table>

</div>
<div >
   <table width="98%" bgcolor="white" class="contenedorConFormato">
       <tr>
           <td>
               <input type="checkbox" />Cargar Presupuesto 
           </td>
           <td>
               <input type="file" />
               <br>
               <input type="button" value="Cargar Archivo"/>
           </td>
           <td>
               Bono Fijo Vida<input type="text" />
           </td>
           <td>
               <div id="b_agregar">
                   <a href="javascript: void(0);" onclick="sayHello();" >Agregar</a>
               </div>
               
               <div class="btn_back w140" style="display: inline; float: right;">
									<a href="javascript: void(0);" >
										Agregar WM
									</a>
			   </div>
								
           </td>
       </tr>
   </table>
   
</div>
<br>
<div >
     <table width="98%" bgcolor="white" class="contenedorConFormato">
         <tr>
             <td>
                 <label class="labelBlack">&Uacute;ltima Actualizaci&oacute;n</label> <input type="text" value="03/09/2015"/>
             </td>
             <td>
                <label class="labelBlack">Usuario</label> <input type="text" value="BARTOLO OLGUIN "/>
             </td>
             <td></td>
         </tr>
     </table>
</div>