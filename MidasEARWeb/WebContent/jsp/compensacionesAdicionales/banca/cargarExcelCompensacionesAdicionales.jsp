<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/calculos/generarDocumentos.js'/>"></script>
<script src="<s:url value='/js/midas2/compensacionesAdicionales/banca/cargarExcelBanca.js'/>"></script>
 <input id="testValor" value="<s:property value='caBancaConfiguracionActual.mes' />" type="hidden"/>
 <input id="testAnio" value="<s:property value='caBancaConfiguracionActual.anio' />" type="hidden"/>
 <input id="testId" value="<s:property value='caBancaConfiguracionActual.id' />" type="hidden"/>

<s:set name="caBanca" value="caBancaConfiguracionActual"/>
<input id="testValor" value="<s:property value='caBancaConfiguracionActual.valor' />" type="hidden"/>
<div class="titulo" style="width: 98%'">
Carga de Archivos
</div>
<div style="border-width:1px; border-color: green; border-style:solid;">
<s:form id="Exceles" onsubmit="return testValidacion()" style="margin-left: 1em;"  action="/compensacionesAdicionales/banca/guardarCPExcel.action" target="iframeMensajesCompensacionesAdicionales" method="post" enctype="multipart/form-data">
<br>
<table class="tableStyle"  style="width: 735px;" >
	<tbody> 
		<tr>
			<td><s:file id="primasPagadas" name="primasPagadas" label="Primas Pagadas"></s:file></td>
			<td><s:file id="siniMes" name="siniMes" label="Siniestralidad Mensual"></s:file></td>
		</tr>
	</tbody>
</table>
<br>
<s:iterator value="caBancaConfiguracionActual" status="iteratorStatus" var="caBancaConf">
<table class="tableStyle" style="width: 735px;">
	<tbody>
		<tr>
			<td>Costo Neto de Siniestralidad Autos</td>
			<s:if test="caBancaConfiguracionActual.valor == 1">
			<td><input type="text" name="costonetosinaut" id="costonetosinaut"  disabled value="${caBancaConf.costonetosinaut}"  /></td>
			</s:if>
			<s:elseif test="caBancaConfiguracionActual.valor == 2">
			<td><input type="text" name="costonetosinaut" id="costonetosinaut" /></td>
			</s:elseif>
			<td>Primas de Retencion Devengadas Autos</td>
			<s:if test="caBancaConfiguracionActual.valor == 1">
			<td><input type="text"  id="primasretendevenaut" name="primasretendevenaut" disabled value ="${caBancaConf.primasretendevenaut}" /></td>
			</s:if>
			<s:elseif test="caBancaConfiguracionActual.valor ==2">
			<td><input type="text"  id="primasretendevenaut" name="primasretendevenaut"  /></td>
			</s:elseif>
		</tr>
		<tr>
			<td>Costo Neto de Siniestralidad Da&ntilde;os</td>
			<s:if test="caBancaConfiguracionActual.valor == 1">
			<td><input type="text"  id="costonetosindan" name="costonetosindan"  disabled value="${caBancaConf.costonetosindan}" /></td>
			</s:if>
			<s:elseif test="caBancaConfiguracionActual.valor ==2 ">
			<td><input type="text"  id="costonetosindan" name="costonetosindan" /></td>
			</s:elseif>
			<td>Primas de Retencion Devengadas Da&ntilde;os</td>
			<s:if test="caBancaConfiguracionActual.valor == 1">
			<td><input type="text"  id="primasretendevendan" name="primasretendevendan"  disabled value="${caBancaConf.primasretendevendan}"/></td>
			</s:if>
			<s:elseif test="caBancaConfiguracionActual.valor ==2 ">
			<td><input type="text"  id="primasretendevendan" name="primasretendevendan" /></td>
			</s:elseif>
		</tr>
	</tbody>
</table>
</s:iterator>
<br>
<table class="tableStyle"  style="width: 735px;" >
	<tbody>
		<tr>
			<td>Mes :<select id="meses" name="mes" class="txtField w100" ></select></td>
			<td>Año :<select id="anios" name="anio" class="txtField w100"></select></td>		
			<td style="float:right;"><input type="submit" class="btn_back w110" value="Enviar" /></td>
			<s:if test="caBancaConfiguracionActual.valor == 1">
			<td style="float:right;"><input style="display:block;" class="btn_back w110" type="button" onclick="desbloquearInputCostosPrimas()" value="Modificar" /></td>
			</s:if>
			<s:else>
			<td style="float:right;"><input style="display:none;"  class="btn_back w110" type="button" value="Modificar" /></td>
			</s:else>
		</tr>
	</tbody>
</table>
<br>
<table class="tableStyle"  style="width: 735px;" >
	<tbody>
		<tr>
		<s:if test="caBancaConfiguracionActual.valor == 1">
		<td style="float:left;">Ultima Modificacion : <input type="text" style="width: 220px;" disabled id="ultimaModificacion" name="ultimaModificacion" value="${caBancaConf.modificacion}" /> </td>
		<td style="float:left;">Usuario : <input type="text"  disabled id="usuario" name="usuario" value="${caBancaConf.usuario}" /> </td>
		</s:if>
		<s:else>
		<td style="float:left;">Ultima Modificacion : <input type="text"  style="width: 220px;" disabled id="ultimaModificacion" name="ultimaModificacion" /> </td>
		<td style="float:left;">Usuario : <input type="text" disabled id="usuario" name="usuario" /> </td>
		</s:else>
		<td>
		<s:if test="caBancaConfiguracionActual.valor == 1">
		<div class="btn_back w120">				
			<a href="javascript: void(0);" class="icon_guardar ."  style="display:block;" onclick="verHistoriaConfiguracion()" ><s:text name="Historico"/></a>
		</div>
		</s:if>
		<s:else>
		<div class="btn_back w120">				
			<a href="javascript: void(0);" class="icon_guardar ."  style="display:none;" onclick="verHistoriaConfiguracion()" ><s:text name="Historico"/></a>
		</div>
		</s:else>
		</td>
		</tr>
	</tbody>
</table>
</s:form>
</div>
<br>

<iframe id="iframeMensajesCompensacionesAdicionalesId"  name="iframeMensajesCompensacionesAdicionales" style="display: none" ></iframe>

<script>
 setTimeout(function(){ 
 setearMesAnio()
 }, 1000);
</script>