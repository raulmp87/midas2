<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/compensacionesAdicionales/vida/negocioVidaHeader.jsp"></s:include>
<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
</style>
<script type="text/javascript" src="<s:url value='/js/midas2/compensacionesAdicionales/vida/negocioVida.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/compensacionesAdicionales/catalogos/catalogosCompensaciones.js'/>"></script>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />
	<script type="text/javascript">
		jQuery(document).ready(function(){
			enableCheckBox();
			jQuery("#txtIdNegocio").attr("disabled","disabled");
			jQuery("#txtIdCompensacion").attr("disabled","disabled");			
			//onChangeProducto();
		});				
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="titulo"
		value="%{getText('midas.fuerzaventa.configuracionPagosComisiones.tituloEditar')}" />
	<script type="text/javascript">
		
	</script>
</s:if>

<s:form action="agregar" id="configuracionNegocioVidaCaForm"
	name="configuracionNegocioVidaCaForm">
	<table class="contenedorFormas w910">
		<tr>
			<td colspan="6" class="titulo">
				<s:text	name="Configuración de Negocio de Vida"></s:text>
			</td>
		</tr>
		<tr>
			<td>
				<table class="contenedorFormas no-border">
					<tr>
						<th width="85px"><s:text name="ID Negocio" /></th>
						<td width="275px"><s:textfield name="configuracionNegocioVida.id" id="txtIdNegocio" readonly="true" cssClass="cajaTextoM2 w200"></s:textfield></td>
					</tr>
					<tr>
						<th width="70px"><s:text name="ID Compensacion" /></th>
						<td width="275px"><s:textfield name="idCompensacion" id="txtIdCompensacion" readonly="true" cssClass="cajaTextoM2 w200"></s:textfield></td>
					</tr>
					<tr>
						<th><s:text name="Ramo*" />
						</th>
						<td colspan="2">
							<ul class="w300" id="ajax_listaRamosSeycos">
								<s:iterator value="listaRamos" var="varObjRamos" status="stat">
									<li><s:if test="#varObjRamos.checado==1">
											<input type="checkbox" 
												name="ramoLong[${stat.index}]"
												id="listaRamosSeleccionados.id" 
												value="${varObjRamos.id}"
												onclick="onChangeRamo();" 
												checked="checked"
												class="js_checkEnable" />
			 						            ${varObjRamos.valor}
	 						          </s:if> 
	 						          <s:else>
											<input type="checkbox" 
												name="ramoLong[${stat.index}]"
												id="listaRamosSeleccionados.id" 
												value="${varObjRamos.id}"
												onclick="onChangeRamo();" 
												class="js_checkEnable" />
					 						    ${varObjRamos.valor}
	 						         </s:else></li>
								</s:iterator>
							</ul></td>
					</tr>
					<tr>
						<th><s:text name="Producto*" />
						</th>
						<td colspan="2">
						    <input  type="checkbox" 
									name="marcarTodo"
									id="marcarTodo"
									checked="checked" 
									onclick="checkAllProducto();"
									/>
							        Seleccionar Todos
							<ul class="w300" id="ajax_listaProductos">
								<s:iterator value="listaProductosSeycos" var="varProductos" status="stat">
									<li><s:if test="#varProductos.checado==1">
											<input type="checkbox" 
												name="productoLong[${stat.index}]"
												id="listaProductosSeleccionados.id"
												value="${varProductos.id}" 
												onclick=""
												checked="checked" 
												class="js_checkEnable" /> 						                  		 
		 						               ${varProductos.valor}
 						               </s:if> <s:else>
											<input type="checkbox" 
												name="productoLong[${stat.index}]"	
												id="listaProductosSeleccionados.id"
												value="${varProductos.id}" 
												onclick=""
												class="js_checkEnable" />  
	 						                	${varProductos.valor}
 						               </s:else></li>
								</s:iterator>
							</ul></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 20px"></td>
					</tr>					
					<tr>
						<td colspan="2"><s:text name="Seleccionar Gerencia asociada al negocio" /></td>
					</tr>
					<tr>
						<th><s:text name="Gerencia" /></th>
						<td colspan="2">
							<ul class="w300" id="ajax_listaGerenciaSeycos">
								<s:iterator value="listaGerenciaSeycos" var="varObjGerenciaSeycos" status="stat">
									<li>
									<s:if test="#varProductos.checado==1">
									<input type="checkbox"
										name="gerenciaLong[${stat.index}]"
										id="listaGerenciaSeleccionadas${stat.index}"
										value="${varObjGerenciaSeycos.id}"
										onclick="onChangeGerenciaNegocio();"
										checked="checked" 
										class="js_checkEnable" />
										${varObjGerenciaSeycos.valor}
									</s:if> <s:else>
											<input type="checkbox"
										name="gerenciaLong[${stat.index}]"
										id="listaGerenciaSeleccionadas${stat.index}"
										value="${varObjGerenciaSeycos.id}"
										onclick="onChangeGerenciaNegocio();" 
										class="js_checkEnable" />
										${varObjGerenciaSeycos.valor}
 						               </s:else></li>
								</s:iterator>
							</ul></td>
					</tr>
				</table>
				<s:if test="tipoAccion != 2">
					<span style="color:red"><s:text name="Los campos indicados con asterisco son requeridos"/></span>
				</s:if> 
				</td>
			<td valign="top">
				<table class="contenedorFormas no-border">
					<tr>
						<th width="70px"><s:text name="Nombre Negocio*" />
						</th>
						<td width="275px"><s:textfield
								name="configuracionNegocioVida.descripcionnegociovida"
								id="descripcionnegociovida" cssClass="cajaTextoM2 w200"></s:textfield>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="height: 20px"></td>
					</tr>
					<tr>
						<th><s:text name="Subramo*" />
						</th>
						<td colspan="2">
							<ul class="w300" id="ajax_listaSubRamosSeycos">
								<s:iterator value="listaSubRamos" var="varObjSubRamos"
									status="stat">
									<li><s:if test="#varObjSubRamos.checado==1">
											<input type="checkbox" 
												name="subRamoLong[${stat.index}]"
												id="listaSubRamosSeleccionados.id"
												value="${varObjSubRamos.id}" 
												checked="checked"
												onclick="onChangeSubRamo();"
												class="js_checkEnable" />
			 						            ${varObjSubRamos.valor}
	 						               </s:if> <s:else>
											<input type="checkbox" 
												name="subRamoLong[${stat.index}]"
												id="listaSubRamosSeleccionados.id"
												onclick="onChangeSubRamo();"
												value="${varObjSubRamos.id}" 
												class="js_checkEnable" />
			 						            ${varObjSubRamos.id}
	 						         </s:else></li>
								</s:iterator>
							</ul></td>
					</tr>
					<tr>
						<th><s:text name="Linea de Negocio*" />
						</th>
						<td colspan="2">
							<input  type="checkbox" 
									name="marcarTodasLineas"
									id="marcarTodasLineas"
									checked="checked"
									onclick="checkAllLineasNegocio();"
									/>
							        Seleccionar Todos
							<ul class="w300" id="ajax_lineaNegocioSeycos">
								<s:iterator value="listaLineaNegocio" var="varObjLineaNegocio"
									status="stat">
									<li id="lineaNegocio"><s:if test="#varObjLineaNegocio.checado==1">
											<input type="checkbox"
												name="lineaNegocioLong[${stat.index}]"
												id="listaLineaNegocioSeleccionados.id"
												value="${varObjLineaNegocio.id}" 
												checked="checked"
												class="js_checkEnable lineaNegocio" />
			 						            ${varObjLineaNegocio.valor}
	 						               </s:if> <s:else>
											<input type="checkbox"
												name="lineaNegocioLong[${stat.index}]"
												id="listaLineaNegocioSeleccionados.id"
												value="${varObjLineaNegocio.id}"
												class="js_checkEnable lineaNegocio" />
			 						            ${varObjLineaNegocio.valor}
	 						         </s:else></li>
								</s:iterator>
							</ul></td>
					</tr>
					<tr>
						<td colspan="2" style="height: 30px"></td>
					</tr>
					<tr><td colspan="2"><s:text name="Seleccionar Agente(s) asociado(s) al negocio" /></td></tr>
					<tr>
						<th><s:text name="Agente*" /></th>
						<td colspan="2">
						  	<ul class="w300" id="listaBeneficiariosAgentes">						  							  	
						  	<s:iterator value="lista_agentes" var="varAgentes" status="stat">
				                <li id="${varAgentes.id}">
				                <s:if test="tipoAccion != 2">
				                    <img src="../img/icons/ico_eliminar.gif" 
				                    onclick="javascript: delete_agente(${varAgentes.id});" style="cursor:pointer;"/>&nbsp;&nbsp;
				                </s:if>
				                <label for="%${varAgentes.id}%">${varAgentes.id} - ${varAgentes.valor}</label> 
				                  <s:hidden name ="lista_agentes[%{#stat.index}].id" value="%{varAgentes.id}"/>
				                </li>
				            </s:iterator>
							</ul>						  	
						</td>
						<s:if test="tipoAccion != 2">
						<td colspan="2">
							<div class="btn_back w90" id="btnBuscarAgenteSeycos">
								<a href="javascript:void(0);" class="icon_buscar"					 
									onclick="javascript:seleccionarAgente('AGENTE_SEYCOS','textClaAgeGral','', 'txtAgenteSeycos',true,0, 0,this,false,5,'cargarAgenteSeycos');">
									<s:text name="Seleccionar" /></a>
							</div>
					  	</td>
					  </s:if>
					</tr>
				</table>
				</td>
		</tr>
	</table>
	<div align="right" class="w910 inline">
		<s:if test="tipoAccion != 5">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_guardar"
					onclick="guardarConfigBono_AndValidConf()"><s:text name="Aceptar" /></a>
			</div>
		</s:if>
		<s:if test="tipoAccion == 4">
			<div class="btn_back w110">
				<a href="javascript: void(0);" class="icon_guardar"
					onclick="guardarConfiguracionNegocioVida()"><s:text name="Guardar" /></a>
			</div>
		</s:if>
	</div>
</s:form>
<script type="text/javascript">
	findIdsCompensaciones();
</script>