<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/compensacionesAdicionales/vida/negocioVidaHeader.jsp"></s:include>
<s:hidden name="idField"></s:hidden>
<script type="text/javascript">
	var claveNegocio='<s:property value="claveNegocio"/>';
	jQuery(function(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var descripcion='<s:property value="descripcion"/>';
		var id='<s:property value="id"/>';
		var urlFiltro=listarFiltradoGerenciaPath+"?tipoAccion="+tipoAccion+"&descripcion="+descripcion+"&id="+id;
		var idField='<s:property value="id"/>';
	 	listarFiltradoGenerico(urlFiltro,"configuracionNegocioVidaGrid", null,idField,'gerenciaModal');
	 });
	 function buscarNegocioVida(){
		var tipoAccion='<s:property value="tipoAccion"/>';
		var descripcion='<s:property value="descripcion"/>';
		var id='<s:property value="id"/>';
		var urlFiltro=listarFiltradoGerenciaPath+"?tipoAccion="+tipoAccion+"&descripcion="+descripcion+"&id="+id;
		var idField='<s:property value="id"/>';
	 	listarFiltradoGenerico(urlFiltro,"configuracionNegocioVidaGrid", null,idField,'gerenciaModal');
	}
</script>
		<s:form action="listarFiltrado" id="negocioVidaCatalogoForm">
			<!-- Parametro de la forma para que sea reutilizable -->
			<s:hidden name="tipoAccion"></s:hidden>
			<table  width="880px" id="filtrosM2">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="Negocios de Vida"/>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Id" />
					</th>
					<td width="275px">
						<s:textfield name="id" id="id" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<th width="70px">
						<s:text name="Descripcion" />
					</th>
					<td width="275px">
						<s:textfield name="descripcion" id="descripcion" cssClass="cajaTextoM2 w200"></s:textfield>
					</td>
				</tr>
				<tr>
					<td colspan="2"  align="right">				
						<div class="btn_back w110">
							<a href="javascript: void(0);" class="icon_buscar"
								onclick="listarFiltradoGenerico(listarFiltradoGerenciaPath, 'configuracionNegocioVidaGrid',document.negocioVidaCatalogoForm,'${idField}','gerenciaModal');">
								<s:text name="midas.boton.buscar"/>
							</a>
						</div>				
					</td>
				</tr>		
			</table>
			<div id="divCarga" style="position:absolute;"></div>	
			<div id="configuracionNegocioVidaGrid" class="w880 h200" style="overflow:hidden"></div>	
				
		</s:form>
		<div id="pagingArea"></div><div id="infoArea"></div>
		
		
