<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/compensacionesAdicionales/vida/negocioVidaHeader.jsp"></s:include>
<s:hidden name="tabActiva"/>
<s:hidden name="tipoAccion"/>

<script type="text/javascript">
	jQuery(function(){
		dhx_init_tabbars();
		incializarTabs(); 
	});
	function verConfiguracionNegocioVida() {		
		sendRequestJQ(null, verDetalleConfiguracionNegocioVidaPath+'?tipoAccion='+parent.dwr.util.getValue("tipoAccion")+"&tabActiva="+parent.dwr.util.getValue("tabActiva"),'contenido_configuracionNegocioVida','limpiarDivsConfiguracionNegocioVida();');
	}
	function incializarTabs(){
	contenedorTab=window["configuracionNegocioVidaTabBar"];
	//Nombre de todos los tabs
	var tabs=["configuracionNegocioVida","compensaciones"];
	//Si no tiene un idAgente, entonces se deshabilitan todas las demas pestanias
}
function limpiarDivsConfiguracionNegocioVida() {
	limpiarDiv('contenido_compensaciones');
}
</script>

<div select="<s:property value="tabActiva"/>" hrefmode="ajax-html"  id="configuracionNegocioVidaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="*" id="configuracionNegocioVida" name="Config. Negocio" href="http://void" extraAction="javascript: verConfiguracionNegocioVida();"></div>
<!-- 	<div width="*" id="compensaciones" name="Compensaciones" href="http://void" extraAction="javascript: verExcepciones();"></div> -->
</div>
