<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="id" type="ro" width="50" sort="int" ><s:text name="id"/></column>
        <column id="descripcionnegociovida" type="ro" width="150" sort="str"><s:text name="Descripcion"/></column>		
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/>
			</column>
		</s:if>
	</head>
	<s:iterator value="listaNegociosVida" var="rowGerencia" status="index">
		<row id="${index.count}">			
			<cell><![CDATA[${id}]]></cell>
			<cell><![CDATA[${descripcionnegociovida}]]></cell>						
			<s:if test="tipoAccion!=\"consulta\"">
				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetallePath, 2,{"id":${rowGerencia.id},"idRegistro":${rowGerencia.id},"idTipoOperacion":20})^_self</cell>
				
			</s:if>
		</row>
	</s:iterator>
</rows>