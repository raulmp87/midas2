<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<!-- script type="text/javascript" src="<s:url value='/js/midas2/compensacionesAdicionales/vida/negocioVida.js'/>"></script>
 --><script type="text/javascript" src="<s:url value='/js/midas2/compensacionesAdicionales/vida/cargarListado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
 <script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
 
<script type="text/javascript">
	var mostrarContenedorConfiguracionNegocioVidaPath = '<s:url action="mostrarContenedor" namespace="/fuerzaventa/configuracionBono"/>';	
	var listarConfiguracionNegocioVidaPath = '<s:url action="listarFiltrado" namespace="/fuerzaventa/configuracionBono"/>';
	var verDetalleConfiguracionNegocioVidaPath = '<s:url action="verDetalleNegocioVida" namespace="/compensacionesAdicionales/vida"/>';	
	var verDetallePath = '<s:url action="verDetalle" namespace="/compensacionesAdicionales/vida"/>';	
	var listarFiltradoGerenciaPath='<s:url action="listarFiltrado" namespace="/compensacionesAdicionales/vida"/>';
	var mostrarPath='<s:url action="mostrar" namespace="/compensacionesAdicionales/vida"/>';
</script>

