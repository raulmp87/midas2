<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <!--  -->   
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			 <!-- -->         
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="id" type="ro" width="200" sort="str" ><s:text name="midas.fuerzaventa.negocio.usuario"/></column>
        <column id="id" type="ro" width="100" sort="str" ><s:text name="Descargas"/></column>
        <column id="id" type="ro" width="200" sort="str" ><s:text name="midas.reporteSesas.estatus"/></column>
       	<column id="accionBloquear" type="img" width="30" sort="na" align="center"/>
       	<column id="accionDesbloquear" type="img" width="30" sort="na" align="center"/>
        <column id="accionDetalles" type="img" width="30" sort="na" align="center"/>
		
	</head>
	<s:iterator value="lstDetalleDescarga" var="usuario" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${usuario}]]></cell>
			<cell><![CDATA[${descargas}]]></cell>
			<cell><![CDATA[${estatus}]]></cell>
			<cell><s:url value="/img/icons/ico_bloquear.gif"/>^<s:text 
			name="Bloquear"/>^javascript:bloquearUsuario("${usuario}")^_self</cell>
			<cell><s:url value="/img/icons/ico_aceptar.gif"/>^<s:text 
			name="Desbloquear"/>^javascript:deBloquearUsuario("${usuario}")^_self</cell>					
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text 
			name="Detalles"/>^javascript:mostrarDetalle("${usuario}")^_self</cell>		
		</row>
	</s:iterator>
</rows>