<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <!-- / -->   
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			 <!-- -->         
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="id" type="ro" width="50" sort="int" ><s:text name="midas.fuerzaventa.negocio.usuario"/></column>
        <column id="id" type="ro" width="50" sort="int" ><s:text name="midas.negocio.producto.id"/></column>
        <column id="id" type="ro" width="50" sort="int" ><s:text name="midas.reporteSesas.estatus"/></column>
		
	</head>
	<s:iterator value="lstDetalleDescarga" var="detalle" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${usuario}]]></cell>
			<cell><![CDATA[${descargas}]]></cell>
			<cell><![CDATA[${estatus}]]></cell>					
		</row>
	</s:iterator>
</rows>