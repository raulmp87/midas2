<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <!-- // -->   
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			 <!-- -->         
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="id" type="ro" width="200" sort="int" ><s:text name="midas.negocio.producto.id"/></column>
        <column id="nombre" type="ro" width="200" sort="int" >Nombre</column>
        <column id="correo" type="ro" width="200" sort="int" >Correo</column>
        <column id="accionBorrar" type="img" width="100" sort="na" align="center"/>
		
	</head>
	<s:iterator value="lstCorreos" var="correo" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${id}]]></cell>
			<cell><![CDATA[${nombre}]]></cell>
			<cell><![CDATA[${correo}]]></cell>						
			<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text 
			name="midas.boton.borrar"/>^javascript:eliminarCorreo(${id})^_self</cell>		
		</row>
	</s:iterator>
</rows>