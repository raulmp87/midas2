<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idField"></s:hidden>
<s:hidden name="closeModal"></s:hidden>
<s:hidden name="pagingCount"></s:hidden>
<script type="text/javascript">   
	
	jQuery(function(){
	 	// pageGridPaginadoAgente(1,true,true);
	 	var gridCorreos = listarFiltradoGenerico(config.contextPath + '/enlace/listarCorreos.action',"correoGrid", jQuery("#agenteForm"),null,true);
	 	
	 });
	 
</script>
<s:form action="listarCorreos" id="agenteForm">
	<!-- Parametro de la forma para que sea reutilizable -->
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="6">
				<s:text name="midas.negocio.agente.correo"/>
			</td>
		</tr>
		<tr>	
			<th>
				<s:text name="midas.suscripcion.cotizacion.nombre"></s:text>
			</th>	 
			<td>
				<s:textfield  id="txtNombre" cssClass="cajaTextoM2 w200"></s:textfield>
			</td>
			<th>
				<s:text name="midas.fuerzaventa.negocio.correoElectronico"></s:text>
			</th>	 
			<td>
				<s:textfield  id="txtCorreo" cssClass="w250 cajaTextoM2 jQemail jQrequired"></s:textfield>
			</td>
			<th>
				<s:text name="midas.poliza.renovacionmasiva.gerencia"></s:text>
			</th>	 
			<td>
				<s:select id="cbGerencia" headerKey="" headerValue="Seleccione..."  list="lstGerencia" listValue="descripcion" listKey="id" name="gerencia.id" cssClass="cajaTextoM2 w150" ></s:select>
			</td>
			<td>
				<div class="btn_back w140" >
					<a href="javascript: void(0);" onclick="agregarCorreo();" class="icon_enviar">	
						<s:text name="midas.boton.agregar"/>	
					</a>
	    		</div>
	    	</td>			
		</tr>
	</table>
	<br>
	<div id="divCarga" style="display:inline-block;float:left;position:absolute;z-index:100;"></div>
	<div id="indicador"></div>
	<div id="gridCorreoPaginado"  class="w880 h270 dataGridGenericStyle">
		<div id="correoGrid" class="w870 h260" style="overflow:hidden"></div>
	</div>
</s:form>
