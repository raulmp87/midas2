<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/poliza/auto/renovacionmasiva/renovacionMasivaHeader.jsp" />
<s:include value="/jsp/catalogos/mensajesHeader.jsp" />
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<s:form id="ordenRenovacionMasivaForm" >
    <div class="titulo" style="width: 98%;">
        <s:text name="midas.poliza.renovacionmasiva.ordenTitle"/>   
    </div>  
    <div id="contenedorFiltros" style="width: 98%;">
        <table id="agregar" style="border: 0">
            <tr>
                <td>
                    <s:textfield cssClass="txtfield jQnumeric jQrestrict" 
                        key="midas.poliza.renovacionmasiva.numOrden"
                        labelposition="top" 
                        size="10"
                        maxlength="6"                       
                        id="idToOrdenRenovacion" name="ordenRenovacion.idToOrdenRenovacion" />
                </td>
                <td>
                    <sj:datepicker name="ordenRenovacion.fechaCreacion"
                        key="midas.poliza.renovacionmasiva.fechaOrden"
                        labelposition="top"
                        changeMonth="true"
                        changeYear="true"               
                        buttonImage="../img/b_calendario.gif"
                           id="fechaCreacion"
                        maxlength="10" cssClass="txtfield"
                        size="12"
                        onkeypress="return soloFecha(this, event, false);"
                        onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
                </td>
                <td>
                    <sj:datepicker name="ordenRenovacion.fechaCreacionHasta"
                        key="midas.suscripcion.cotizacion.fechaA"
                        labelposition="top"                     
                        buttonImage="../img/b_calendario.gif"
                        id="fechaCreacionHasta"
                        size="12"
                        changeMonth="true"
                        changeYear="true"
                        maxlength="10" cssClass="txtfield"
                        onkeypress="return soloFecha(this, event, false);"
                        onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
                 </td>  
                <td>
                    <s:textfield key="midas.poliza.renovacionmasiva.numPolizas" cssClass="txtfield jQnumeric jQrestrict"
                         maxlength="8"
                         labelposition="top" 
                         id="numeroPolizas" name="ordenRenovacion.numeroPoliza" />                                  
                </td>   
                </tr>
                <tr>
                <td>
                    <s:select id="claveEstatus" key="midas.poliza.renovacionmasiva.estatus"
                        labelposition="top" 
                        name="ordenRenovacion.claveEstatus"
                        list="#{'':'Seleccione...','0':'INICIADA','1':'CONTROL USUARIO','2':'TERMINADA','3':'CANCELADA'}" />
                </td>
                <td>
                    <sj:datepicker name="ordenRenovacion.fechaModificacion"
                        key="midas.poliza.renovacionmasiva.fechaEstatus"
                        labelposition="top"
                        changeMonth="true"
                        changeYear="true"               
                        buttonImage="../img/b_calendario.gif"
                        id="fechaModificacion"
                        maxlength="10" cssClass="txtfield"
                        size="12"
                        onkeypress="return soloFecha(this, event, false);"
                        onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
                </td>
                <td>
                    <sj:datepicker name="ordenRenovacion.fechaModificacionHasta"
                        key="midas.suscripcion.cotizacion.fechaA"
                        labelposition="top"                     
                        buttonImage="../img/b_calendario.gif"
                        id="fechaModificacionHasta"
                        size="12"
                        changeMonth="true"
                        changeYear="true"
                        maxlength="10" cssClass="txtfield"
                        onkeypress="return soloFecha(this, event, false);"
                        onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
                </td>   
                <td>
                    <s:textfield key="midas.poliza.renovacionmasiva.nombreUsuario" cssClass="txtfield jQalphaextra jQrestrict"
                        maxlength="200"
                        labelposition="top" 
                        id="nombreUsuarioCreacion" 
                        name="ordenRenovacion.nombreUsuarioCreacion" 
                        onblur="validaLongitud('nombreUsuarioCreacion', this.value)"/>      
                </td>
            </tr>
        </table>
    </div>
    <div  style="width: 98%;">
        <table id="agregar">
            <tr>        
                <td colspan="4">        
                    <table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
                        <tr>
                            <td>
                                <div class="btn_back w100" style="display: inline; float: right;">
                                    <a id="submit" href="javascript: void(0);" onclick="cargarListaOrdenesReno();">
                                        <s:text name="midas.suscripcion.cotizacion.buscar" />
                                    </a>
                                </div>
                                <div class="btn_back w100" style="display: inline; float: right;">
                                    <a href="javascript: void(0);" onclick="limpiarFiltrosRenovacion(2);">
                                        <s:text name="midas.suscripcion.cotizacion.limpiar" />
                                    </a>
                                </div>
                                <div class="btn_back w140" style="display: inline; float: right;">
                                    <a href="javascript: void(0);" onclick="cargarExcelNumeroGuias()">
                                        <s:text name="midas.suscripcion.cotizacion.cargarNumeroGuias" />
                                    </a>
                                </div>
                                <div class="btn_back w120" style="display: inline; float: right;">
                                    <a href="javascript: void(0);" onclick="reporteEfectividad()">
                                        <s:text name="midas.suscripcion.cotizacion.reporteEfectividad" />
                                    </a>
                                </div>
                                <div class="btn_back w140" style=" float: right;display:none">
                                    <a href="javascript: void(0);" onclick="procesaTareaAction()">
                                        Procesar
                                    </a>
                                </div>
                                <div class="btn_back w140" style=" float: right;display:inline">
                                    <a href="javascript: void(0);" onclick="descargarPlantillaGuias()">
                                        <s:text name="midas.suscripcion.cotizacion.descargarPlatillaGuias" />
                                    </a>
                                </div>
                            </td>                           
                        </tr>
                    </table>                
                </td>       
            </tr>
        </table>
    </div>  
</s:form>
<div id="indicador"></div>
<div id="ordenRenovacionPaginadoGrid">
<div id="ordenRenovacionGrid" style="width: 98%; height: 135px"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<div id="documentoNumeroGuias"></div>
</div>
<br>
<script type="text/javascript">
    initOrdenRenovacion();
</script>