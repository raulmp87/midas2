<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script src="<s:url value='/js/midas2/poliza/auto/renovacionmasiva/renovacionMasiva.js'/>"></script>

<sj:head/>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form id="ordenRenovacionDetalleForm" action="/" >
	<s:hidden name="idToOrdenRenovacion" id="idToOrdenRenovacion" />
	<div class="titulo" style="width: 98%;">
		<s:text name="midas.poliza.renovacionmasiva.numOrden"/>	
		<s:property value="ordenRenovacion.idToOrdenRenovacion"/>
	</div>	
	<div id="contenedorFiltros" >
		<table id="agregar" border="0">
			<tr><td>
				<s:text name="midas.poliza.renovacionmasiva.fechaOrden"/>:
				<s:property value="ordenRenovacion.fechaCreacion"/>				
			</td><td>
				<s:text name="midas.poliza.renovacionmasiva.estatus"/>:	
				<s:property value="ordenRenovacion.descripcionEstatus"/>	
			</td><td>
				<s:text name="midas.poliza.renovacionmasiva.numPolizas"/>:	
				<s:property value="ordenRenovacion.numeroPolizas"/>	 	
			</td></tr> 	
			</table>
	</div>
<div id="indicador"></div>
<div id="ordenRenovacionDetalleGrid" style="width: 98%; height: 450px; margin: 10px;"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
	<div style="width: 98%;">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="parent.cerrarVentanaModal('consultaOrdenRenovacion','listarOrdenesRenovacion(true);');"> <s:text
					name="midas.boton.salir" /> </a>
		</div>
		<s:if test="ordenRenovacion.claveEstatus != 2 && ordenRenovacion.claveEstatus != 3">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="if(confirm('\u00BFEst\u00E1 seguro que desea Cancelar las Polizas?')){cancelarPolizasDetalleOrden();}"> 
				<s:text name="midas.boton.cancelar" /> </a>
		</div>
		<div id="divGuardar" style="display: block; float: right;">
			<div class="btn_back w140" style="display: inline; float: left;">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFEst\u00E1 seguro que desea Emitir las Polizas?')){emitirPolizasDetalleOrden();}">
					<s:text name="midas.suscripcion.cotizacion.complementar.emitir" /> </a>
			</div>
		</div>
		</s:if>
	</div>
</s:form>
<script type="text/javascript">
	initOrdenRenovacionDetalle();
</script>