<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/cargamasivaindividual/cargaMasivaIndividualHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form id="cargamasivaindividualForm">
	<div class="titulo"  style="width: 98%;">
		<s:text name="midas.poliza.auto.renovacionmasiva.autoplazo.seleccioneInformacion" />
	</div>
	<div  style="width: 98%;text-align: center;">
	<table id="agregar" >
		<tr>
			<td>
				<sj:datepicker name="fechaCreacion" cssStyle="width: 170px;"
						key="midas.poliza.auto.renovacionmasiva.autoplazo.fechaInicio"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaCreacion"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>	
			</td>
			<td>
				<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 170px;"
						key="midas.poliza.auto.renovacionmasiva.autoplazo.fechaFin"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
		</tr>
		<tr>
			<td>
				<s:select key="midas.poliza.renovacionmasiva.negocio" 
						  name="idToNegocio" id="idToNegocio"
						  labelposition="top"
						  onchange = "cargaProductosNegocio()"
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  list="negocioList" listKey="idToNegocio" listValue="descripcionNegocio" 
						  cssClass="txtfield" /> 	
			</td>
			<td>
				<s:select key="midas.poliza.renovacionmasiva.producto" 
						  name="idToProducto" id="idToProducto"
						  labelposition="top"
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  list="#{}" 
						  cssClass="txtfield" /> 	
			</td>
		</tr>
		<tr>
			<td>
				<div class="btn_back w220" style="display: inline; float: left;">
					<a href="javascript: void(0);" onclick="descargarLayoutRenovacion();"
						class="icon_guardar2"> <s:text
							name="midas.poliza.renovacionmasiva.autoplazo.descargarPlantilla" /> </a>
				</div>
			</td>
		</tr>
	</table>
</div>
</s:form>

