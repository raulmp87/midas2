<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call> 
        </beforeInit>
        <column id="ordenRenovacion.idToOrdenRenovacion" type="ro" width="100" sort="int" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.numOrden"/></column>
        <column id="ordenRenovacion.nombreUsuarioCreacion" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.nombreUsuario"/></column>
        <column id="ordenRenovacion.numeroPolizas" type="ro" width="100" sort="int" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.numPolizas"/></column>
        <column id="ordenRenovacion.fechaCreacion" type="ro" width="150" sort="date_custom" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.fechaOrden"/></column>          
        <column id="ordenRenovacion.claveEstatus" type="ro" width="150" sort="str" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.estatus"/></column>
        <column id="ordenRenovacion.fechaModificacion" type="ro" width="150" sort="date_custom" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.fechaEstatus"/></column>      
		<column id="consultar" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="cancelar" type="img" width="30" sort="na" align="center"></column>
		<column id="terminar" type="img" width="30" sort="na" align="center"></column>
		<column id="exportar" type="img" width="30" sort="na" align="center"></column>
		<column id="exportarXML" type="img" width="30" sort="na" align="center"></column>
		<column id="exportarExcel" type="img" width="30" sort="na" align="center"></column>
	</head>

	<s:iterator value="ordenRenovacionMasivaList">
		<row id="<s:property value="idToOrdenRenovacion" escapeHtml="false"/>">
			<cell><s:property value="idToOrdenRenovacion" escapeHtml="false"/></cell>
			<cell><s:property value="nombreUsuarioCreacion" escapeXml="true"/></cell>
			<cell><s:property value="numeroPolizas" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaModificacion" escapeHtml="false" escapeXml="true"/></cell>
			<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Consultar_Orden">
			<cell>../img/icons/ico_verdetalle.gif^Consultar Orden^javascript: mostrarVentanaConsultaOrden(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
			</m:tienePermiso>
			<s:if test="claveEstatus != 2 && claveEstatus != 3">
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Cancelar_Orden">
				<cell>/MidasWeb/img/cross16.png^Cancelar Orden^javascript: cancelarOrdenRenovacion(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Terminar_Orden">
				<cell>../img/icons/ico_editar.gif^Terminar Orden^javascript: terminarOrdenRenovacion(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus != 3">
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Exportar_Detalle_de_Cotizaciones">
				<cell>../img/b_printer.gif^Exportar Detalle de Cotizaciones de Renovacion^javascript: exportarDetalle(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus == 2">
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Exportar_XML_Caratulas_Poliza">
				<cell>../img/b_printer.gif^Exportar PDF^javascript: exportarXML(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus != 3">
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Exportar_Detalle_de_Cotizaciones">
				<cell>../img/b_printer.gif^Exportar Excel de Comparacion^javascript: exportarExcelComparacion(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
			<s:if test="claveEstatus == 2">
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Enviar_Orden_Proveedor">
				<cell>../img/b_enviar.jpg^Envio a Proveedor^javascript: enviarProveedor(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
				<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Obtener_Orden_Proveedor">
				<cell>../img/complementar.png^Obtener Orden de Proveedor^javascript: obtenerOrdenProveedor(<s:property value="idToOrdenRenovacion"/>)^_self</cell>
				</m:tienePermiso>
			</s:if>
		</row>
	</s:iterator>
</rows>