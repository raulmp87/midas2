<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script src="<s:url value='/js/midas2/poliza/auto/renovacionmasiva/renovacionMasiva.js'/>"></script>

<sj:head/>
<s:form id="accionRenovacionesForm" action="/poliza/renovacionmasiva/cancelarCotizacionValidacion.action" >
	<div id="contenedorFiltros" >
		<table id="agregar" border="0" style="width:470px;" >
			<tr><td>
				<s:text name="midas.poliza.renovacionmasiva.validacionLabel" />
			</td></tr><tr><td>
				- <s:text name="midas.poliza.renovacionmasiva.validacion01" />
			</td></tr><tr><td>
				- <s:text name="midas.poliza.renovacionmasiva.validacion02" />
			</td></tr><tr><td>
				- <s:text name="midas.poliza.renovacionmasiva.validacion03" />
			</td></tr><tr><td>
				- <s:text name="midas.poliza.renovacionmasiva.validacion04" />
			</td></tr><tr><td>
				- <s:text name="midas.poliza.renovacionmasiva.validacion05" />
			</td></tr>
			<tr>
				<td>
					- <s:text name="midas.poliza.renovacionmasiva.validacion06" />
				</td>
			</tr>
			<tr><td>
					<table id=t_riesgo border="0" style="width: 100%;">
						<tr>
							<th><s:text name="midas.poliza.renovacionmasiva.numPoliza" /></th>
							<th><s:text name="midas.poliza.renovacionmasiva.validacion" /></th>
						</tr>
						<s:iterator value="validaciones" status="stat">
							<tr>
								<td>
									<s:hidden name="validaciones[%{#stat.index}].idToPoliza" value="%{idToPoliza}" /> 
									<s:property value="%{numeroPolizaFormateada}" /> 
								</td>
								<td>
									<s:property value="%{mensajeError}" />
								</td>
							</tr>
						</s:iterator>
					</table></td></tr>
			</table>
	</div>
	<div style="width: 410px;">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="cerrarVentanaValidacionRenovacion();"> <s:text
					name="midas.boton.salir" /> </a>
		</div>
		<div id="divGuardar" style="display: block; float: right;">
			<div class="btn_back w140" style="display: inline; float: left;">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cancelar las Cotizaciones?')){cancelarCotizacionValidacion();}">
					<s:text name="midas.poliza.renovacionmasiva.cancelarCotizaciones" /> </a>
			</div>
		</div>
	</div>
</s:form>