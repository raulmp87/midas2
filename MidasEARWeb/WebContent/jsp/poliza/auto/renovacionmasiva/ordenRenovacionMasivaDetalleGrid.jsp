<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>   
        </beforeInit>
        <column id="ordenRenovacion.idToOrdenRenovacion" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
        <column id="ordenRenovacion.poliza.idToPoliza" type="ro" width="150" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.numPolizaAnterior"/></column>
        <column id="ordenRenovacion.poliza.numeroPolizaAsoc" type="ro" width="150" sort="str" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.numPolizaRenovada"/></column>
        <column id="ordenRenovacion.poliza.cotizacionDTO.nombreContratante" type="ro" width="*" sort="str" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.nombreContratante"/></column>          
        <column id="ordenRenovacion.poliza.cotizacionDTO.descripcionEstatus" type="ro" width="100" sort="str" hidden="false" align="center"><s:text name="midas.poliza.renovacionmasiva.estatus"/></column>      
		<column id="editar" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="consultar" type="img" width="30" sort="na" align="center"></column>
		<column id="imprimir" type="img" width="30" sort="na" align="center"></column>
	</head>

	<s:iterator value="ordenRenovacionMasivaDetList">
		<row id="<s:property value="polizaAnterior.idToPoliza" escapeHtml="false"/>">
			<cell>0</cell>
			<cell><s:property value="polizaAnterior.numeroPolizaFormateada" escapeHtml="false"/></cell>
			<cell><s:property value="polizaRenovada.numeroPolizaFormateada" escapeHtml="false"/></cell>
			<cell><s:property value="cotizacion.nombreContratante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="claveEstatus == 1 || claveEstatus == 3 || claveEstatus == 4" >
			<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Editar_Cotizacion">
			<cell>../../img/icons/ico_verdetalle.gif^Editar Cotizacion^javascript: editarCotizacion(<s:property value="cotizacion.idToCotizacion"/>, <s:property value="cotizacion.claveEstatus"/>)^_self</cell>
			</m:tienePermiso>
			</s:if>
			<s:if test="cotizacion != null" >
			<m:tienePermiso nombre="FN_M2_Emision_Orden_Renovacion_Masiva_Consultar_Cotizacion">
			<cell>../../img/icons/ico_verdetalle.gif^Consultar Cotizacion^javascript: verCotizacion(<s:property value="cotizacion.idToCotizacion"/>)^_self</cell>
			</m:tienePermiso>
			</s:if>
			<s:if test="polizaRenovada.idToPoliza != null" >
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Imprimir_Poliza">
	        <cell>../../img/b_printer.gif^Imprimir Poliza^javascript: verDetalleImpresionPoliza(<s:property value="polizaRenovada.idToPoliza"/>)^_self</cell> 
	        </m:tienePermiso>
			</s:if>
		</row>
	</s:iterator>
</rows>