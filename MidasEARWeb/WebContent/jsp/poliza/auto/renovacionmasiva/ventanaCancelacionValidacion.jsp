<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script src="<s:url value='/js/midas2/poliza/auto/renovacionmasiva/renovacionMasiva.js'/>"></script>

<sj:head/>
<s:form id="accionRenovacionesForm" >
	<div id="contenedorFiltros" >
		<table id="agregar" border="0" style="width:470px;" >
			<tr><td>
				<s:text name="midas.poliza.renovacionmasiva.cotizacionesCancelada" />
			</td></tr>
			<tr><td>
					<table id=t_riesgo border="0" style="width: 100%;">
						<tr>
							<th><s:text name="midas.suscripcion.cotizacion.idtocotizacion" /></th>
						</tr>
						<s:iterator value="cotizacionesCanceladas" status="stat">
							<tr>
								<td> 
									1-<s:property /> 
								</td>
							</tr>
						</s:iterator>
					</table></td></tr>
			</table>
	</div>
	<div style="width: 410px;">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="cerrarVentanaCancelacionValidacion();"> <s:text
					name="midas.boton.salir" /> </a>
		</div>
	</div>
</s:form>