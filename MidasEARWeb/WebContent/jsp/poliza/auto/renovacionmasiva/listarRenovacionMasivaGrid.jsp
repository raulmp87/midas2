<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
        </beforeInit>
        <column id="polizaDTO.idToPoliza" type="ch" width="30" sort="na" hidden="false" align="center">#master_checkbox</column>
        <column id="polizaDTO.numeroPolizaFormateada" type="ro" width="110" sort="str" hidden="false"><s:text name="midas.emision.nopoliza"/></column>
        <column id="polizaDTO.cotizacionDTO.nombreContratante" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.nombreContratante"/></column>
        <column id="polizaDTO.numeroSerie" type="ro" width="150" sort="int" hidden="false"><s:text name="midas.poliza.renovacionmasiva.numSerie"/></column>          
        <column id="polizaDTO.cotizacionDTO.fechaFinVigencia" type="ro" width="150" sort="date_custom" hidden="false"><s:text name="midas.poliza.renovacionmasiva.fechafinpoliza"/></column>
        <column id="polizaDTO.numeroRenovacion" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.numeroRenovaciones"/></column>
        <column id="polizaDTO.claveEstatus" type="ro" width="100" sort="int" hidden="false"><s:text name="midas.poliza.renovacionmasiva.estatus"/></column>
        <column id="polizaDTO.cotizacionDTO.solicitudDTO.negocio.descripcionNegocio" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.negocio"/></column>
        <column id="polizaDTO.cotizacionDTO.solicitudDTO.productoDTO.descripcion" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.producto"/></column>
        <column id="polizaDTO.cotizacionDTO.tipoPolizaDTO.descripcion" type="ro" width="150" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.tipoPoliza"/></column>
        <column id="lineaNegocio" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.lineaNegocio"/></column>
        <column id="centroEmisor" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.negocio.centroOperacion"/></column>
        <column id="gerencia" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.gerencia"/></column>
        <column id="oficina" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.oficina"/></column>
        <column id="promotoria" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.promotoria"/></column>
        <column id="nombreAgente" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.poliza.nombreAgente"/></column>
        <column id="numAgente" type="ro" width="150" sort="str" hidden="false"><s:text name="midas.poliza.renovacionmasiva.numAgente"/></column>      
              
		<column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
	</head>

	<s:iterator value="polizaList">
		<row id="<s:property value="idToPoliza" escapeHtml="false"/>">
			<cell>0</cell>
			<cell><s:property value="numeroPolizaFormateada" escapeHtml="false"/></cell>
			<cell><s:property value="cotizacionDTO.nombreContratante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.fechaFinVigencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroRenovacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="claveEstatus== \"1\"">
					VIGENTE
				</s:if>
				<s:else>
					CANCELADA
				</s:else>
			</cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.negocio.descripcionNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.productoDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.tipoPolizaDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.seccionDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.centroEmisorDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.agente.promotoria.ejecutivo.gerencia.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.nombreEjecutivo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.agente.promotoria.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.solicitudDTO.agente.idAgente" escapeHtml="false" escapeXml="true"/></cell>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Consultar_Poliza">
			<cell>../img/icons/ico_verdetalle.gif^Ver Detalle^javascript: verDetalleEndososPoliza(<s:property value="idToPoliza"/>,1)^_self</cell>
			</m:tienePermiso>
		</row>
	</s:iterator>
</rows>