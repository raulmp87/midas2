<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<s:include value="/jsp/poliza/auto/renovacionmasiva/renovacionMasivaHeader.jsp"></s:include>

<sj:head/>
<s:hidden name="estatusRenovacion" id="estatusRenovacion" />
<s:hidden name="idToOrdenRenovacion" id="idToOrdenRenovacion" />
<s:hidden name="mensajePolizaFallida" id="errores"/>
<s:form id="accionRenovacionesForm" action="/poliza/renovacionmasiva/renovarPolizas.action"  >
	<div id="agregar" style="height: 210px; width: 380px;">
		<s:radio name="accionRenovacion" theme="simple"
			list="tipoAccionesRenovacion"
			onclick="onChangeAccionRenovacion(this.value);" id="accionRenovacion"
			cssStyle="agregar" />
	<div id="contenedorFiltros" style="display: none; " class="filtrosNegocio"  >
		<table id="agregar" border="0">
			<tr><td>
					<s:select id="idToNegocio" cssStyle="width: 260px;" 
								key="midas.poliza.renovacionmasiva.negocio"
								labelposition="left" 
								name="idToNegocio"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'productos', 'getMapNegProductoPorNegocio')" 			
								listKey="idToNegocio" listValue="descripcionNegocio" 
								list="negocioList" cssClass="txtfield"  />
</td></tr><tr><td>
					<s:select id="productos" 
								key="midas.poliza.renovacionmasiva.producto"
								labelposition="left" 
								name="idToNegProducto"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'tipopolizas', 'getMapNegTipoPolizaPorNegProducto')"
								list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion" 
								cssClass="txtfield"  />
</td></tr><tr><td>
					<s:select id="tipopolizas" 
								key="midas.poliza.renovacionmasiva.tipoPoliza"
								labelposition="left" 
								name="idToNegTipoPoliza"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								onchange="cargarComboSimpleDWR(this.value, 'secciones', 'getMapNegocioSeccionPorTipoPoliza')"
						  		list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion" 
						  		cssClass="txtfield" /> 	
</td></tr><tr><td>
					<s:select id="secciones" key="midas.poliza.renovacionmasiva.lineaNegocio"
								labelposition="left" 
								name="idToNegSeccion"
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
						  		list="productoList" listKey="idToNegProducto" listValue="productoDTO.descripcion"  
						  		cssClass="txtfield" />
						  		</td></tr> 	
			</table>
	</div>
	</div>
	<div style="width: 410px;">
		<div id="divSalirBtn" class="btn_back w100"
			style="display: inline, none; float: right;">
			<a href="javascript: void(0);"
				onclick="parent.cerrarVentanaModal('accionRenovacion');"> <s:text
					name="midas.boton.cancelar" /> </a>
		</div>
		<div id="divGuardar" style="display: block; float: right;">
			<div class="btn_back w140" style="display: inline; float: left;">
				<a href="javascript: void(0);"
					onclick="if(confirm('\u00BFEst\u00E1 seguro que desea renovar las P\u00F3lizas?')){renovarPolizas();}">
					<s:text name="midas.boton.continuar" /> </a>
			</div>
		</div>
	</div>
</s:form>
<script type="text/javascript">
	resultadoOrdenRenovacion();
</script>