<%@taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/poliza/auto/poliza.js'/>"></script>
<script src="<s:url value='/js/midas2/poliza/auto/renovacionmasiva/renovacionMasiva.js'/>"></script>
<script type="text/javascript">
	var seleccionarPromotoriaPath = '<s:url action="ventanaPromotorias" namespace="/suscripcion/cotizacion/auto"/>';
	var renovarPath = '<s:url action="ventanaAccionRenovaciones" namespace="/poliza/renovacionmasiva"/>';
	var accionRenovarPath = '<s:url action="renovarPolizas" namespace="/poliza/renovacionmasiva"/>';
	var mostrarOrdenesPath = '<s:url action="mostrarOrdenesRenovacion" namespace="/poliza/renovacionmasiva"/>';
	var consultaOrdenRenovacionPath = '<s:url action="verOrdenRenovacionDetalle" namespace="/poliza/renovacionmasiva"/>';
	var procesarCargarExcel = '<s:url action="saveCargaMasiva" namespace="/poliza/renovacionmasiva"/>';
	var procesarEstatusPendientes = '<s:url action="procesarEstatusPendientes" namespace="/poliza/renovacionmasiva"/>';
	var reporteEfectividadEntrega = '<s:url action="getReporteEfectividad" namespace="/poliza/renovacionmasiva"/>';
	var procesaTarea = '<s:url action="procesaTarea" namespace="/poliza/renovacionmasiva"/>';
	var descargaPlantilla = '<s:url action="downLoadFormatGuias" namespace="/poliza/renovacionmasiva"/>';
</script>

<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
.divInputText{
    width: 180px;
    max-width:180px;
    height:14px;
    max-height:14px;
    background-color:#FFF;
    text-align:left;
    cursor:text;
	-moz-user-select: -moz-none;
   	-khtml-user-select: none;
	-webkit-user-select: none;
   /*
     Introduced in IE 10.
     See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/
   */
   	-ms-user-select: none;
   	user-select: none;
}
</style>