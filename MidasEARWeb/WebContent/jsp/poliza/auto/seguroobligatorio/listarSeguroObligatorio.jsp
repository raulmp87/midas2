<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>	
<script src="<s:url value='/js/midas2/poliza/auto/seguroobligatorio/seguroobligatorio.js'/>"></script>

<sj:head/>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form id="seguroObligatorioForm" >
	<s:hidden name="idToPoliza" id="idToPoliza"/>
	<s:label><s:property value="mensajeSeguroObligatorio"  /></s:label>
</s:form>
<div id="indicador"></div>
<div id ="polizaAnexaGrid"  style="width:97%;height:40%"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>
<div style="float:right">
      <div class="btn_back w200"  style="display: inline; float: right; margin-right: 40px;">
            <a href="javascript: void(0);" onclick="creaPolizaAnexas();">
                  <s:text name="midas.poliza.seguroobligatorio.crear"/>
            </a>
      </div>
</div>
<script type="text/javascript">
	listarPolizaAnexa();
</script>