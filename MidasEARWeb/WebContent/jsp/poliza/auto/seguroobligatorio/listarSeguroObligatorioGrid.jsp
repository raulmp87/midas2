<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
        </beforeInit>
        <column id="numeroPolizaAnexa" type="ro" width="*" sort="str" hidden="false"><s:text name="midas.emision.nopoliza"/></column>
        <column id="imprimir" type="img" width="30" sort="na" align="center"></column>              
	</head>
	<s:iterator value="polizaAnexaList">
		<row id="<s:property value="id.idToPoliza" escapeHtml="false"/>">
			<cell><s:property value="polizaAnexaDTO.numeroPolizaFormateada" escapeHtml="false" escapeXml="true"/></cell>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Imprimir_Poliza">
	        <cell>../../img/b_printer.gif^Imprimir Poliza^javascript: verDetalleImpresionPolizaAnexa(<s:property value="polizaAnexaDTO.idToPoliza"/>)^_self</cell> 
	        </m:tienePermiso>			
		</row>
	</s:iterator>
</rows>