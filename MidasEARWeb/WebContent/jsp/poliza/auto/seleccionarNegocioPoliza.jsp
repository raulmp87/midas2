<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css"/>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet"
	type="text/css"/>
<style type="text/css">
#content {
	font-size: 10px;
}

#form {
	text-align: center;
	margin-top: 20px;
}

#footer {
	margin-top: 30px;
	margin-left: 30%;
}

table.tr {
	text-alight: left;
}

table {
	font-size: 10px;
}


.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
</style>
<script type="text/javascript">
	var urlBusquedaNegocios = '<s:url action="buscarNegocio" namespace="/poliza"/>';
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>	
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'>
</script>
<script>
	var bandera = null;
	
	jQuery(document).ready(function(){
		bandera = getParameterByName('bandera');
	});
	
	function getParameterByName(name) {
		  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		  var regexS = "[\\?&]" + name + "=([^&#]*)";
		  var regex = new RegExp(regexS);
		  var results = regex.exec(window.location.search);
		  if(results == null)
		    return "";
		  else
		    return decodeURIComponent(results[1].replace(/\+/g, " "));
		}

$(function(){
	$( '#descripcionBusquedaNegocio' ).autocomplete({
               source: function(request, response){
               		$.ajax({
			            type: "POST",
			            url: urlBusquedaNegocios,
			            data: {descripcionBusquedaNegocio:request.term},              
			            dataType: "xml",	                
			            success: function( xmlResponse ) {
			           		response( $( "item", xmlResponse ).map( function() {
								return {
									value: $( "descripcion", this ).text(),
				                    id: $( "id", this ).text()
								}
							}));			           
               		}
               	})},
               minLength: 3,
               delay: 1000,
               select: function( event, ui ) {
            	   jQuery('#idNegocio').val(ui.item.id);
            	   	if (bandera != null && bandera == 1) {
            	   		jQuery('#seleccionarNegocio').unbind("click");
            	   		jQuery('#seleccionarNegocio').click(function(){
            	   			parent.cargaNegocioPoliza(ui.item.id,ui.item.value);
            	   		});
            	   }
               }		          
         });
 });	
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="detalle" >
	<center>
		<table id="agregar" width="100%">
			<tr><td colspan="3">B&uacute;squeda por</td></tr>
			<tr>			
				<td width="15%" ><span>Descripci&oacute;n:</span></td>
				<td width="80%"><input type="text" name="descripcionBusquedaNegocio" id="descripcionBusquedaNegocio" class="cajaTexto" 						
				 /></td>
				<td width="5%">
					<div id="b_buscar" style="margin-left: 20px; display:none;">
						<a href="javascript: void(0);"
							onclick="javascript:busqueda(jQuery('#descripcionBusquedaNegocio').val());">
							<s:text name="midas.boton.buscar" /> </a>
					</div>							
				</td>
			</tr>    
		</table>
		<div id="b_regresar" style="margin-left: 20px; width:200px;">
			<a id="seleccionarNegocio" href="javascript: void(0);">
				<s:text name="midas.poliza.seleccionarnegocio" /> </a>
		</div>
	</center>
</div>