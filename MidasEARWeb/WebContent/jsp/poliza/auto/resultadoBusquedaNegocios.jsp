<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<resultados>
	<s:iterator value="negocioList">
	<item>
		<id><s:property value="idToNegocio" /></id>
		<descripcion><s:property value="descripcionNegocio" escapeHtml="false" escapeXml="true" /> - <s:property value="versionNegocio" /></descripcion>
	</item>
	</s:iterator>
</resultados>