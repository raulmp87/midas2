<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<s:form id="incisoForm">
<s:hidden name="incisoId" id="incisoId"/>
<s:hidden name="validoEn" id="validoEn"/>
<s:hidden id="recordFrom" name="recordFrom" />
<s:hidden name="validoEnMillis" id="validoEnMillis"></s:hidden>
<s:hidden name="recordFromMillis" id="recordFromMillis"></s:hidden>
<s:hidden id="claveTipoEndoso" name="claveTipoEndoso" />
<div class="subtituloIzquierdaDiv"><s:text name="midas.poliza.inciso.zonaCirculacion" /></div>
<table id="agregar">
	<tr>
		<th width="10%">
			<s:text name="midas.negocio.cobertura.paquete.seccion.estado"/>
		</th>
		<td width="40%"><s:property value="biAutoInciso.value.nombreEstado"/></td>
		<th width="10%">
			<s:text name="midas.negocio.cobertura.paquete.seccion.municipio"/>
		</th >
		<td width="40%"><s:property value="biAutoInciso.value.nombreMunicipio"/></td>
	</tr>
</table>

<table id="agregar">
	<tr>
		<td class="subtitulo align-left" colspan="2" >
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.infVehiculo"/>
		</td>
	</tr>
	<tr>
		<th width="20%">
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.lineaNegocio"/>
		</th>
		<td align="left" width="80%"><s:property value="biAutoInciso.value.descLineaNegocio"/></td>			
	</tr>
			
	<tr>
		<th>
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.marca"/>			
		</th>
		<td><s:property value="biAutoInciso.value.descMarca"/></td>	
	</tr>
	<tr>
		<th>
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.estilo"/>
		</th>
		<td><s:property value="biAutoInciso.value.descEstilo"/></td>	
	</tr>
	<tr>
		<th>
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.modelo"/>
		</th>
		<td><s:property value="biAutoInciso.value.modeloVehiculo"/></td>			
	</tr>	
	<tr>
		<th>
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.tipoUso"/>
		</th>
		<td><s:property value="biAutoInciso.value.descTipoUso"/></td>	
	</tr>
	<tr>
		<th>
			<s:text name="midas.general.descripcionFinal"/>
		</th>
		<td><s:property value="biAutoInciso.value.descripcionFinal"/></td>	
	</tr>
	<tr>
		<th>
			<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.paquete"/>
		</th>
		<td><s:property value="biAutoInciso.value.paquete.descripcion"/></td>				
	</tr>
	
	<tr>
		<th>
			<s:text name="midas.general.pctDescuentoEstado"/>
		</th>
		<td><s:property value="biAutoInciso.value.pctDescuentoEstado"/></td>	
	</tr>
	
	<tr>
		<td> 
<!-- 			<div id="mostrarCaractVehiculo" class="btn_back w140" style="display: inline; float: right;"> -->
<!-- 				<a href="javascript: void(0);" onclick="mostrarCaractVehiculo();">	 -->
<%-- 						<s:text name="midas.poliza.caracteristicasVehiculo"/>	 --%>
<!-- 				</a>		      -->
<!-- 		    </div>		   -->
		</td>				
	</tr>	

</table>

<%--<div class="subtituloIzquierdaDiv"><s:text name="midas.suscripcion.cotizacion.inciso.title"/></div>
<table id="agregar" style="padding: 0px; margin: 0px; border: none;"  width="98%" class="fixTabla">
		<tr>
			<td><s:include value="/jsp/poliza/auto/incisos/coberturaCotizacionGrid.jsp"></s:include></td> 	
		</tr>
	</table>
	
<br/>	
<div class="subtituloIzquierdaDiv"><s:text name="midas.cotizacion.totalprimas"/></div>
<table style=" width:50%; padding: 5px; margin: 5px;" border="0">
	<tr >
		<td valign="top" align="right">
			<div id="cargaResumenTotales"></div>
			<div id="resumenTotalesGrid"></div>
		</td>
	</tr>
</table>	--%>		
	
 <div class="subtituloIzquierdaDiv"><s:text name="midas.suscripcion.cotizacion.inciso.title"/></div>
<div id="cargando" style="display: none;text-align: center;">
	<img src="/MidasWeb/img/loading-green-circles.gif">
	<font style="font-size: 9px;">Procesando la	información, espere un momento por favor...</font>
</div>
<div id="indicador"></div>
<div id="coberturaCotizacionGrid" align="center"></div>

</s:form>
<script type="text/javascript">
	//obtenerResumenTotalesGrid();
	obtenerCoberturaCotizaciones();
</script>
