<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<table id="agregar" style="padding: 0px; margin: 0px; border: none;"  width="98%" >
<tr>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.claveObligatoriedad" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.descripcion" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.sumaAsegurada" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.porcDeducible" /></th>
	<th><s:text name="midas.suscripcion.cotizacion.inciso.primaNeta" /></th>
</tr>
	<s:iterator value="biCoberturaSeccionList" status="stat">
		<s:if test="value.claveContrato == 1">	
			<tr>
				<td class ="txt_v2">
					<s:if test="value.claveContrato==1 && value.claveObligatoriedad==0" >
						<input type="checkbox" checked="checked" disabled value=""/>
					</s:if>			
					<s:else>
						<s:checkbox  name="biCoberturaSeccionList[%{#stat.index}].value.claveContrato" value="%{value.claveContrato}" disabled="true"/>			
					</s:else>								
				</td>
				<td class ="txt_v2"><s:property value="continuity.coberturaDTO.nombreComercial" escapeHtml="false" escapeXml="true" /></td>
				<td class ="txt_v2">
					<s:if test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 0">
						<s:if test="value.valorSumaAseguradaMax != 0.0 && value.valorSumaAseguradaMin!= 0.0">
							<div style="font-size:8px;">
								<s:text name="midas.poliza.inciso.cobertura.entre"/>
								<s:property value="%{getText('struts.money.format',{value.valorSumaAseguradaMin})}" escapeHtml="false" escapeXml="true" />
								<s:text name="midas.general.y"/> 
								<s:property value="%{getText('struts.money.format',{value.valorSumaAseguradaMax})}" escapeHtml="false" escapeXml="true" />
							</div>					
						</s:if>
						<s:property  value="%{getText('struts.money.format',{value.valorSumaAsegurada})}" escapeHtml="false" escapeXml="true" />	
					</s:if>
		
					<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 1">				
						<s:text name="midas.suscripcion.cotizacion.inciso.valorComercial"/>
					</s:elseif>
					<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 2">
						<s:text name="midas.suscripcion.cotizacion.inciso.valorFactura"/>
						<s:property  value="%{value.valorSumaAsegurada}" escapeHtml="false" escapeXml="true"/>				
					</s:elseif>
					<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 9">
						<s:text name="midas.suscripcion.cotizacion.inciso.valorConvenido" />				
					</s:elseif>
					<s:elseif test="continuity.coberturaDTO.claveFuenteSumaAsegurada == 3">				
						<s:text name="midas.suscripcion.cotizacion.inciso.amparada"/>
					</s:elseif>
				</td>
				<td class ="txt_v2">
					<s:if test='continuity.coberturaDTO.claveTipoDeducible == "0"'>
					</s:if>
					<s:elseif test='continuity.coberturaDTO.claveTipoDeducible == "1"'>
						<s:property  value="%{value.porcentajeDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:elseif>
					<s:else>
						<s:property  value="%{value.valorDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:else>
					<s:if test='continuity.coberturaDTO.claveTipoDeducible != "0"'>
						<s:property  value="%{value.descripcionDeducible}" escapeHtml="false" escapeXml="true"/>
					</s:if>
				</td>
				<td class ="txt_v2">
					<s:property value="%{getText('struts.money.format',{value.valorPrimaNeta})}" escapeHtml="false" escapeXml="true"/>
				</td>
				</tr>
			</s:if>
	</s:iterator>	
</table>
