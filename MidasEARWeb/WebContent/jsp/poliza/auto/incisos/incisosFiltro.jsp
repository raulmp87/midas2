<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>


    <table id="agregar" class="fixTablaInciso" cellspacing="1%" cellpadding="1%" width="98%">        
        <tr>                      
            <td>
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroInciso"></s:text>:
            </td>
            <td style="padding-left:0px;">
                <s:textfield id="filtros.id.numeroInciso" name="filtros.id.numeroInciso"                             
                             cssClass="txtfield  jQnumeric jQrestrict w80" 		                     						
					         labelposition="left"   
							 size="10"
							 maxlength="10"
							 onkeypress="return soloNumerosM2(this, event, false)"
							 onblur="this.value=jQuery.trim(this.value)"/>
            </td> 
            
             <td>
                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.descripcionVehiculo"></s:text>:
            </td>
            <td><s:select list="descripcionesIncisos" headerKey="null"
					headerValue="%{getText('midas.general.seleccione')}"
					name="filtros.descripcionGiroAsegurado" id="descripcionesIncisos"
					cssClass="txtfield">
				</s:select>
			</td>
		</tr>
	     <tr>
	            <td>
	                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie"></s:text>:
	            </td>
	            <td style="padding-left:0px;">
	                <s:textfield id="filtros.incisoAutoCot.numeroSerie" name="filtros.incisoAutoCot.numeroSerie" 	                            
	                             cssClass="txtfield jQalphanumeric jQrestrict w160" 	                            							
								 labelposition="left"  
								 size="10"
								 maxlength="17"
								 onblur="this.value=jQuery.trim(this.value)" />
	            </td>
	            <td>
                    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.numeroMotor"></s:text>:
                </td>
                <td style="padding-left:0px;">
                    <s:textfield id="filtros.incisoAutoCot.numeroMotor" name="filtros.incisoAutoCot.numeroMotor"
                                 cssClass="txtfield jQrestrict w160"                                  						
								 labelposition="left"  
								 size="19"
								 maxlength="19" />
                </td>              
	           
        </tr>

        <tr>                      
           <td>
               <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.conductor"></s:text>:
           </td>
           <td style="padding-left:0px;">
               <s:textfield id="filtros.incisoAutoCot.nombreConductor" name="filtros.incisoAutoCot.nombreConductor"
                            cssClass="txtfield jQrestrict w160"                           							
							labelposition="left"  
							size="10"
					     	maxlength="50"
						    onblur="this.value=jQuery.trim(this.value)"/>
           </td>           
           <td>
                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.asegurado"></s:text>:
           </td>
           <td style="padding-left:0px;">	                                    
                <s:textfield id="filtros.incisoAutoCot.nombreAsegurado" name="filtros.incisoAutoCot.nombreAsegurado"
                             cssClass="txtfield  jQrestrict w160"                           							
		                     labelposition="left"  
		                     size="10"
		                     maxlength="50"
		                     onblur="this.value=jQuery.trim(this.value)" />                                      
           </td>             	                
        </tr>

        <tr>
            <td>
                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.placas"></s:text>:
            </td>
            <td style="padding-left:0px;">
               <s:textfield id="filtros.incisoAutoCot.placa" name="filtros.incisoAutoCot.placa"
                            cssClass="txtfield jQrestrict w160"                            							
			                labelposition="left"  
			                size="10"
			                maxlength="10"
			                onblur="this.value=jQuery.trim(this.value)" />
           </td>
        </tr>
                   
        <tr>
            <td colspan="2"></td>                                               
            <td colspan="2" align="right">
                <div id="divBuscarBtn" class="w150" style="float:right;">
		            <div class="btn_back w140" style="display: inline; float: right;">
					    <a href="javascript: void(0);" onclick="limpiarFiltrosIncisos();">
						    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar" /> </a>
	                </div>
                </div>                
                <div id="divBuscarBtn" class="w150" style="float:right;">
		            <div class="btn_back w140" style="display: inline; float: right;">
		            
			            <s:if test="mostrarCancelados== 1">
						    <a href="javascript: void(0);" onclick="pageGridPaginadoIncisosCancelados(1, true);">
							    <s:text name="midas.boton.buscar" /> </a>
						</s:if>
						<s:else>
							<a href="javascript: void(0);" onclick="pageGridPaginadoIncisos(1, true);">
								    <s:text name="midas.boton.buscar" /> </a>
						</s:else>
	                </div>
                </div>
            </td>
        </tr>
    </table>

