<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<s:form id="incisoForm">
<s:hidden name="incisoId" id="incisoId"/>
<s:hidden name="validoEn" id="validoEn"/>
<table id="agregar">
	<tr>
		<th width="30%">
			<s:text name="midas.endosos.cotizacionEndosoListado.nombreAsegurado"/>
		</th >
		<td width="40%"><s:property value="biAutoInciso.value.nombreAsegurado"/></td>
	</tr>
</table>
<br>
<div id="mostrarCaractVehiculo" class="btn_back w140" style="display: inline; float: right;">
	<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('datosAsegurado');">	
		<s:text name="midas.boton.aceptar"/>	
	</a>		     
</div>
</s:form>
<script type="text/javascript">
	//obtenerResumenTotalesGrid();
</script>
