<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <column id="numeroSecuencia" type="ro" width="80" sort="int" hidden="false">#</column>
      	<column id="descripcionFinal" type="ro" width="500" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.inciso.descripcion.vehiculo"/></column>
		<column id="estatus" type="ro" width="137" sort="str"><s:text name="midas.catalogos.tarifa.agrupador.estatus"/></column>
		<column id="valorPrimaNeta" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.cotizacion.primatotal"/></column>
		<column id="paquete" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.paquete"/></column>		
		<column id="numeroSerie" type="ro" width="137" sort="str">No. Serie</column>
		<column id="numeroMotor" type="ro" width="137" sort="str">No. Motor</column>
		<column id="placa" type="ro" width="82" sort="str">Placas</column>
		<column id="nombreConductor" type="ro" width="200" sort="str">Conductor</column>
		<column id="nombreAsegurado" type="ro" width="350" sort="str">Asegurado</column>
		<s:if test="mostrarCancelados!= 1">
			<column id="accionDefinir" type="img" width="81" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		</s:if>
	</head>
	<s:iterator value="biIncisoList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.descripcionFinal" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="value.estatus== 1">
					VIGENTE
				</s:if>
				<s:elseif test="value.estatus== 2">
					CANCELADO
				</s:elseif>
			</cell>
			<cell><s:property value="%{getText('struts.money.format',{value.primaTotal})}" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.paquete.descripcion" escapeHtml="false" escapeXml="true"/></cell>					
			<cell><s:property value="value.autoInciso.numeroSerie" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.numeroMotor" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.placa" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.nombreCompletoConductor" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.nombreAsegurado" escapeHtml="true" escapeXml="true"/></cell>
			<s:if test="mostrarCancelados!= 1">
				<cell>../img/icons/ico_verdetalle.gif^Ver detalle Inciso^javascript:mostrarVentanaInciso(<s:property value="continuity.id"/>);^_self</cell>	
			</s:if>			
		</row>
	</s:iterator>
</rows>