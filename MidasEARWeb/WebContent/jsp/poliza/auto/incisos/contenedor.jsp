<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="content_form"  style="width: 98%;">
	<s:form action="mostrarContenedor" id="IncisoForm">
		<s:hidden name="cotizacionId" id="cotizacionId"/>
		<s:hidden name="validoEn" id="validoEn"/>
		<s:hidden name="recordFrom" id="recordFrom"/>
		<s:hidden name="idToPoliza"id="idToPoliza"/>
		<s:hidden name="mostrarCancelados"id="mostrarCancelados"/>
		<s:hidden name="aplicaFlotillas"id="aplicaFlotillas"/>
	<div id="form">
			
		</div>
	</s:form>
</div>


<s:if test="aplicaFlotillas== 1">
<s:form id="incisosFiltroForm">
	<div id="filtroIncisos">
	<s:include value="/jsp/poliza/auto/incisos/incisosFiltro.jsp"></s:include>
	</div>
</s:form>
</s:if>

<div class="subtituloIzquierdaDiv"><s:text name="midas.poliza.inciso.listado" /></div>
<div id="indicador" style="left:214px; top: 40px; z-index: 1"></div>
<div id="gridListadoDeIncisos">
	<div id="listadoIncisos" style="width: 98%; height: 135px"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){

	limpiarFiltrosIncisos();
	var mostrarCancelados = '<s:property value="mostrarCancelados" />';

	
	if (mostrarCancelados == 1) {
		pageGridPaginadoIncisosCancelados(1,true);
	} else {
		pageGridPaginadoIncisos(1,true);
	}
});
</script>
