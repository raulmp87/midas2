<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript">
	var generarDatosPath = '<s:url action="generarDatos" namespace="/reportes/recibosProveedores"/>';
	var generarReportePath = '<s:url action="generarReporte" namespace="/reportes/recibosProveedores"/>';	
	var validarExistenciaDatosPath = '<s:url action="validarExistenciaDatos" namespace="/reportes/recibosProveedores"/>';
</script>

<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>	
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/reportes/reporteRecibosProveedores.js'/>"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<style type="text/css">
.b_submit:disabled{
	color: #BDBDBD;
	border: 1px solid #999;
	background-color: #ddd;	
}
</style>

<div class="row">
	<div class="titulo c5">
		<label class="">Reporte Recibos Proveedores</label>
	</div>
</div>
<div id="detalle" style="height: 50%;">
<s:form id="repRecibosProvForm">
<s:hidden name="existenDatos" id ="existenDatos"/>
<s:hidden name="mesSeleccionado" id ="mesSeleccionado"/>
<s:hidden name="anioSeleccionado" id ="anioSeleccionado"/>
	<table id="filtros" width="100%">	   		
		<tr>
		    <th width="50px;"><font color="#FF6600">* </font><s:text name="Mes:"></s:text>
			</th>
			<td >
			    <select id="meses" name="mes" class="txtField w200" ></select>			   
			</td>		
			<td width="120px;"></td>	
			<th width="50px;"><font color="#FF6600">* </font><s:text name="A�o:"></s:text>
			</th>
			<td>
			    <select id="anios" name="anio" class="txtField w200" ></select>
			</td>			
		</tr>
		<tr id="spacer" height="20px;"></tr>	
		<tr>
		    <td colspan="6" align="right" valign="bottom">
		       <!--  <div class="btn_back w150">
			        <a href="javascript: void(0);" onclick="generarDatos();">
				        <s:text name="Generar Datos"/>
			        </a>
		        </div>	  -->
		        <div class="btn_back w140" >
						        <s:submit key="Generar Datos" 
						                  id="botonGenerarDatos"
						                  name="botonGenerarDatos"						                  
						                  onclick="generarDatos(); return false;"
								          cssClass="b_submit w140"/>						    
			    </div>            
	        </td>
	        <td>
	            <div class="btn_back w150">
			        <a href="javascript: void(0);" onclick="validarDatos();">
				        <s:text name="Generar Reporte"/>
			        </a>
		        </div>
	        </td>
		</tr>	
    </table>
</s:form>
</div>