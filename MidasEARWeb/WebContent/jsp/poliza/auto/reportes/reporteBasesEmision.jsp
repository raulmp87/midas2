<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<script
	src="<s:url value='/js/midas2/poliza/auto/reportes/impresionAction.js'/>"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		<s:text name="midas.auto.reportes.basesEmision.reporteBasesEmision"></s:text>
	</div>
		<s:form id="reporteBasesEmisionAutoForm">
			<table id="filtros" width="100%">
				<tr>
					<td width="20%"> 
					  <s:text name="midas.auto.reportes.basesEmision.fechaInicial" ></s:text>
					</td>
					<td  width="30%">
						<sj:datepicker 
								name="fechaInicial" required="true"
								cssStyle="width: 100px;" buttonImage="../img/b_calendario.gif"
								id="fechaInicial" maxlength="10"  labelposition="left"
								readonly="false" cssClass="txtfield"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="esFechaValida(this);" changeMonth="true"
								changeYear="true"   >
							</sj:datepicker>
					</td>
					<td width="20%"> 
					  <s:text name="midas.auto.reportes.basesEmision.fechaFinal" ></s:text>
					</td>
					<td width="29%">
						<sj:datepicker name="fechaFinal" required="true"
								cssStyle="width: 100px;" buttonImage="../img/b_calendario.gif"
								
								id="fechaFinal" maxlength="10" labelposition="left"
								readonly="false" cssClass="txtfield"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="esFechaValida(this);" changeMonth="true"
								changeYear="true"   >
							</sj:datepicker>
					</td>
				</tr>
				
				<tr>
					<td width="20%"> 
					  <s:text name="midas.fuerzaventa.configBono.ramo" ></s:text>
					</td>
					<td width="30%">
						<s:select  
					      name="idRamo" id="idRamo"
					      labelposition="left"  
					      onchange="obtenerSubRamos();"
					      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					      list="ramoLista" listKey="idTcRamo" listValue="descripcion" 					      	   
				          cssClass=" txtfield"/>
					</td>
					<td width="20%"> 
					  <s:text name="midas.fuerzaventa.configBono.subramo" ></s:text>
					</td>
					<td width="29%">
						<s:select 
					      name="subRamoId" id="subRamoId"
					      labelposition="left"  
					      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					      list="subRamoLista" listKey="idTcSubRamo" listValue="descripcionSubRamo" 					      	   
				          cssClass=" txtfield"/>
					</td>
				</tr>
				<tr>
					<td width="20%"> 
					  <s:text name="midas.auto.reportes.basesEmision.nivelAgrupamientoMovimientos" ></s:text>
					</td>
					<td  width="30%"> 
						<s:select 
					      name="nivelAgrupamiento" id="nivelAgrupamiento"
					      labelposition="left"    
					      headerKey="" headerValue="%{getText('midas.general.seleccione')}"
					      list="nivelAgrupamientoLista" listKey="id" listValue="nombre" 					      	   
				          cssClass=" txtfield"/>
				    </td>
				    <td colspan="2" width="49%">
				    </td>
				</tr>
				<tr>
					<td colspan="4">
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: imprimirReporteBasesEmisionAuto();" tipo="agregar" texto="Generar Reporte"/>
						</div>
					</td>
				</tr>
			</table>
		</s:form>
</div>