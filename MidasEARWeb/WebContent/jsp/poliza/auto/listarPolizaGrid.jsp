<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>	 			
        </beforeInit>
        <column id="polizaDTO.numeroPolizaFormateada" type="ro" width="110" sort="int" hidden="false"><s:text name="midas.emision.nopoliza"/></column>
        <column id="polizaDTO.clavePolizaSeycos" type="ro" width="110" sort="int" hidden="false"><s:text name="midas.emision.nopolizaseycos"/></column>
        <column id="polizaDTO.nombreAsegurado" type="ro" width="200" sort="str" hidden="false">Contratante</column>
        <column id="polizaDTO.fechaCreacion" type="ro" width="120" sort="date_custom" hidden="false"><s:text name="midas.emision.fechacreacion"/></column>
        <column id="polizaDTO.claveEstatus" type="ro" width="100" sort="str" hidden="false">Estatus</column>          
        <column id="polizaDTO.cotizacionDTO.fechaInicioVigencia" type="ro" width="100" sort="date_custom" hidden="false">Vigencia Desde</column>
        <column id="polizaDTO.cotizacionDTO.fechaFinVigencia" type="ro" width="80" sort="date_custom" hidden="false">Hasta</column>      
		<column id="polizaDTO.numeroIncisos" type="ro" width="90" sort="int" hidden="false"><s:text name="midas.emision.totalincisos"/></column>
		<column id="polizaDTO.descripcionTipo" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.negocio.seccion.estilos.tipos"/></column>
		<column id="polizaDTO.cotizacionDTO.folio" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.folio.reexpedible.field.numero.folio.poliza"/></column>
		<column id="accion" type="img" width="30" sort="na" align="center"></column>
		<column id="edit" type="img" width="30" sort="na" align="center"></column>
		<column id="accion2" type="img" width="30" sort="na" align="center"></column>
		<column id="accion3" type="img" width="30" sort="na" align="center"></column>
		<column id="accion4" type="img" width="30" sort="na" align="center"></column>
		<column id="accion5" type="img" width="30" sort="na" align="center"></column>
	</head>
	<% int a=0;%>
	<s:iterator value="polizaList" var="rowPolizaDTO" >
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="numeroPolizaFormateada" escapeHtml="false"/></cell>
			<cell><s:property value="clavePolizaSeycos" escapeHtml="false"/></cell>
			<cell><![CDATA[${rowPolizaDTO.nombreAsegurado}]]></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				<s:if test="claveEstatus== \"1\"">
					VIGENTE
				</s:if>
				<s:else>
					CANCELADA
				</s:else>
			</cell>
			<cell><s:property value="cotizacionDTO.fechaInicioVigencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="cotizacionDTO.fechaFinVigencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroIncisos" escapeHtml="false"/></cell>
			<cell><s:property value="descripcionTipo" escapeHtml="false"/></cell>
			<cell><s:property value="cotizacionDTO.folio" escapeHtml="false" escapeXml="true"/></cell>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Imprimir_Poliza">
	        <cell>../img/b_printer.gif^Imprimir Poliza^javascript: verDetalleImpresionPoliza(<s:property value="idToPoliza"/>)^_self</cell> 
	        </m:tienePermiso>
<%-- 	        <cell>../img/b_printer.gif^Imprimir Poliza^javascript: imprimirPoliza(<s:property value="idToPoliza"/>,<s:property value="numeroPolizaFormateada"/>,<s:property value="cotizacionDTO.idToCotizacion"/>)^_self</cell> --%>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Consultar_Poliza">
			<cell>../img/icons/ico_verdetalle.gif^Ver Detalle^javascript: verDetalleEndososPoliza(<s:property value="idToPoliza"/>)^_self</cell>
			</m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Solicitar_Endoso">
		    	<cell><s:if test="!esServicioPublico">../img/icons/ico_agregar.gif^Endoso^javascript: solicitarEndoso(<s:property value="idToPoliza"/>,"<s:property value="numeroPolizaFormateada" escapeHtml="false"/>")^_self</s:if><s:elseif test="!usuarioExterno">../img/icons/ico_agregar.gif^Endoso^javascript: solicitarEndoso(<s:property value="idToPoliza"/>,"<s:property value="numeroPolizaFormateada" escapeHtml="false"/>")^_self</s:elseif><s:else>../img/icons/ico_rechazar2.gif^Endoso</s:else></cell>
		    </m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Solicitar_Endoso">
		    <cell>../img/icons/ico_copiar2.gif^Seguro Obligatorio^javascript: solicitarSeguroObligatorio(<s:property value="idToPoliza"/>,"<s:property value="numeroPolizaFormateada" escapeHtml="false"/>")^_self</cell>
		    </m:tienePermiso>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Consultar_Poliza">
			<cell>../img/icons/ico_verdetalle.gif^Ver Recibos^javascript: verDetalleRecibosPoliza(<s:property value="idToPoliza"/>)^_self</cell>
			</m:tienePermiso>
			<cell>../img/csh_winstyle/tombs.gif^Estatus del Envio^javascript: ventanBitacoraEnvioPoliza(<s:property value="idToPoliza"/>, "<s:property value="numeroPolizaFormateada"/>")^_self</cell>	    
		</row>
	</s:iterator>
</rows>