<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/poliza.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/operacioensSapAmis/sapAmisAccionesAlertas.js'/>"></script>
<style type="text/css">
#contenido {
	width: 940px;
	margin-top: 0px;
	margin-right: auto;
	margin-bottom: 0px;
	margin-left: auto;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 0px;
	padding-left: 20px;
	overflow-y: auto;
	height: 100%;
	text-align: left;
}

.subgridAlertas {
	padding-top: 5px;
	padding-bottom: 5px;
	border-bottom-style: solid;
	border-bottom-width: 1px;
	border-bottom-color: green;
}
</style>

<s:hidden name="id" id="idToPoliza"/>
<s:hidden name="idToCotizacion" id="idToCotizacion"/>
<s:hidden name="validoEn" id="validoEn"/>
<s:hidden name="validoEnMillis" id="validoEnMillis" />
<s:hidden name="recordFrom" id="recordFrom" />
<s:hidden name="recordFromMillis" id="recordFromMillis" />
<s:hidden name="tipoRegreso" id="tipoRegreso" />
<s:hidden name="esSituacionActual" id="esSituacionActual" />
<s:hidden name="claveTipoEndoso" id="claveTipoEndoso" />
<s:hidden name="mostrarCancelados" id="mostrarCancelados" />
<s:hidden name="noSerieVin" id="noSerieVin" />
<div style="display:none" id="conenedorRespuestaAccioneSapAmis"></div>
<div class="titulo" style="width: 100%; height: 20px;">
	<s:text name="midas.emision.consultaPoliza"/>
</div>

<div hrefmode="ajax-html" style="height: 430px; width: 920px" id="polizaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
		<div width="200px" id="detallePoliza" name="<s:text name='midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza'/>" href="http://void" extraAction="javascript: desplegarDetallePoliza();"></div>
		<div width="200px" id="emision" name="Datos Complementarios" href="http://void" extraAction="javascript: desplegarEmision();"></div>
		<div width="200px" id="accionesAlertasSAP" name="Acciones Alertas SAP-AMIS" href="http://void" extraAction="javascript: desplegarAccionesAlertasSAPAMIS();"></div>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>