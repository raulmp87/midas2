<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea_Bitacora</param>
				<param>true</param>
				<param>infoArea_Bitacora</param>
			</call>    
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="bitacora.id" type="ro" width="0" sort="na" hidden="true"></column>
		<column id="bitacora.fecha" type="ro" width="100" sort="server">Fecha</column>
		<column id="bitacora.idGuia" type="ro" width="150" sort="server">Guia</column>
		<column id="bitacora.estatusString" type="ro" width="150" sort="server">Estatus</column>
		<column id="bitacora.fechaEntrega" type="ro" width="150" sort="server">Fecha de Entrega</column>	
		<column id="bitacora.recibe" type="ro" width="150" sort="server" >Recibe</column>
		<column id="bitacora.motivoRechazo" type="ro" width="150" sort="server" >Rechazo</column>
	</head>
    
    <s:iterator value="bitacora.detalleBitacora" status="status">
		<row id="<s:property value="#status.index" escapeHtml="false" escapeXml="true"/>">
			<cell><s:property value="idToBitacora" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="bitacora.fecha" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="bitacora.idGuia" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:if test="estatus=='CONFIRMADO'"><s:property value="bitacora.fechaEntrega" escapeHtml="false" escapeXml="true" /></s:if></cell>
			<cell><s:if test="estatus=='CONFIRMADO'"><s:property value="bitacora.recibe" escapeHtml="false" escapeXml="true" /></s:if></cell>
			<cell><s:if test="estatus=='DEVOLUCION'"><s:property value="bitacora.rechazo" escapeHtml="false" escapeXml="true" /></s:if></cell>
		</row>
	</s:iterator>
	
</rows>