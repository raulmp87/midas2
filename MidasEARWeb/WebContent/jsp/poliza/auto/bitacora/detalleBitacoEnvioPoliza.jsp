<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">	
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value="/js/prototype.js"/>"></script>
<script type="text/javascript">
	delete Array.prototype.toJSON;
	var config = {
		contextPath: '${pageContext.request.contextPath}',
           baseUrl: location.protocol + "//" + location.host + '${pageContext.request.contextPath}'
       };
</script>	
<sj:head/>

<script type="text/javascript" src="<s:url value="/js/midas.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptDataGrid.js"/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
  	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_filter.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_srnd.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_nxml.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxvault.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxdataprocessor.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-util.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/jquery.mask.js"/>"></script>
<script src="<s:url value='/js/midas2/poliza/auto/poliza.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		cargarBitacoraEnvio(jQuery('#idToPoliza').val());		
	});
</script>
<s:form id="formBitacoraPoliza">
	<div class="subtituloIzquierdaDiv">
		<s:text name="midas.poliza.bitacora" /> <s:property value="numeroPolizaFormateada"/>
	</div>
	<s:hidden name="polizaDTO.idToPoliza" id="idToPoliza"/>
	<br/>
	<div id="bitacoraPolizaGrid" style="height: 300px; width: 850"></div>
</s:form>