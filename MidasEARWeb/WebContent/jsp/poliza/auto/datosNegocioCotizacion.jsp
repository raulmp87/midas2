<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="subtituloIzquierdaDiv"><s:text name="midas.cotizacion.informacionnegocio" /></div>
<table id="agregar">
	<tr>
		<th width="10%">
			<s:text name="midas.cotizacion.negocio" /> 
		</th>
		<td width="40%">    
			<s:property value="biCotizacion.value.Solicitud.negocio.descripcionNegocio"/>
		</td>
		<th width="10%">
			<s:text name="midas.cotizacion.tipopoliza" /> 
		</th>
		<td width="40%">
			<s:property value="biCotizacion.value.tipoPoliza.descripcion"/>
		</td>
	</tr>
	<tr>
		<th>
			<s:text name="midas.cotizacion.producto" /> 
		</th>
		<td>
			<s:property value="biCotizacion.value.tipoPoliza.productoDTO.descripcion"/>
		</td>
	</tr>	    
</table>