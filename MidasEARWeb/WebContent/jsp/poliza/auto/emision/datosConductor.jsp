<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<div id="datosConductor" style="width:98%;">
	<table id="desplegarDetalle" style="border: 1px solid #73D54A;">
		<s:hidden id="idCliente" name="cliente.idCliente" />
		<s:if test="copiarDatos == 2">
			<s:hidden name="biAutoInciso.value.nombreConductor" />
			<s:hidden id="biAutoInciso.value.paternoConductor" name="biAutoInciso.value.paternoConductor" />
			<s:hidden name="biAutoInciso.value.maternoConductor" />
		</s:if>
		<tr>
			<td class="titulo"colspan="2" style="font-size:10px;">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.datosConductor"/>
			</td>
		</tr>
		<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			<tr>
				<td colspan="6" class="form-group col-md-4" style="font-size:10px;">
					<s:radio name="copiarDatos"  list="#{'1':'Nuevo Conductor','2':'Copiar Datos del Asegurado'}"   id ="copiarDatos" disabled="true" 
					onclick="copiarDatosUsuario(this.value);" />	
				</td>
			</tr>
		</s:if>
		<tr>
			<td class="form-group col-md-4" style="font-size:10px;">
				<label class="small" for="nombre" ><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.nombre"/></label> 
			</td>
			<td>
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
				 	<s:textfield cssClass="form-control-datos-generales txtfield jQrequired"  					 
					  size="25" id="nombre" name="biAutoInciso.value.nombreConductor"
					   maxlength="100"/>	
				</s:if>
				<s:else>
				  	<s:property value="biAutoInciso.value.nombreConductor"/>
				</s:else>
											
			</td>
			<td class="form-group col-md-4" style="font-size:10px;">
				<label for="paterno" class="small"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.paterno"/></label>
			</td>
			<td>
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
				 	<s:textfield cssClass="form-control-datos-generales txtfield jQrequired"  					 
						 maxlength="100" size="25" id="paterno" name="biAutoInciso.value.paternoConductor"  />
				</s:if>
				<s:else>
				  	<s:property value="biAutoInciso.value.paternoConductor"/>	
				</s:else>
					  
			</td>
			<td class="form-group col-md-4" style="font-size:10px;">
				<label for="materno" class="small"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.materno"/></label>
			</td>
			<td>
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
				 	<s:textfield cssClass="form-control-datos-generales txtfield jQrequired"  					 
					  size="25" id="materno" name="biAutoInciso.value.maternoConductor"
					  maxlength="100" />
				</s:if>
				<s:else>
				  	<s:property value="biAutoInciso.value.maternoConductor"/>
				</s:else>	
					
			</td>
		</tr>
		<tr>
			<td class="form-group col-md-4" style="font-size:10px;">
				<label for="licencia" class="small"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.noLicencia"/></label>
			</td>		
			<td>
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
				 	<s:textfield  cssClass="form-control-datos-generales txtfield numeric restrict jQrequired" 				 
					  size="25" id="licencia" name="biAutoInciso.value.numeroLicencia" 
					  onkeypress="return soloNumerosM2(this, event, false)"  required="true" maxlength="50"/>
				</s:if>
				<s:else>
				  	<s:property value="biAutoInciso.value.numeroLicencia"/>	
				</s:else>
				
			</td>
			<td class="form-group col-md-4" style="font-size:10px;">
				<label class="small" for="fechaNacimiento"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.fechaNacimiento"/></label>
			</td>
			<td class="form-group col-md-4" style="font-size:10px;">
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
				 	<sj:datepicker name="biAutoInciso.value.fechaNacConductor" required="#requiredField" 
				 			   buttonImage="../../../../../../img/b_calendario.gif" 
				               id="fechaNacimiento" maxlength="10" cssClass="form-control-datos-generales txtfield jQrequired"	
				               labelposition="%{getText('label.position')}" 
				               size="12"
				               yearRange="-80:-18" maxDate="-18y"
				               changeYear="true"
				               changeMonth="true"
				               onkeypress="return soloFecha(this, event, false);"
				               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				               onblur="esFechaValida(this);"></sj:datepicker>
				</s:if>
				<s:else>
				  	<s:property value="biAutoInciso.value.fechaNacConductor"/>
				</s:else>
					
			</td>
			<td class="form-group col-md-4" style="font-size:10px;">
				<label class="small" for="ocupacionConductor"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.ocupacion"/></label>
			</td>
			<td >
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
				 	<s:textfield cssClass="form-control-datos-generales txtfield jQrequired"  					 
					 size="25" id="ocupacionConductor" name="biAutoInciso.value.ocupacionConductor" maxlength="100" />
				</s:if>
				<s:else>
				  	<s:property value="biAutoInciso.value.ocupacionConductor"/>
				</s:else>
								
			</td>
		</tr>		
	</table>
</div>
<script type="text/javascript">
	var claveTipoPersona = '<s:property value="cliente.claveTipoPersona"/>';
	var cargaDatosCliente = 1;
</script>
