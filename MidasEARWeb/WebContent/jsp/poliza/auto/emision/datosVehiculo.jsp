<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:hidden id="usuarioExterno" name="usuarioExterno" />
	<s:set var="controlesFronterizosDisabled">false</s:set>
			
	<s:if test="usuarioExterno == 1">
		<s:set var="disabledConsultaDescripcion">false</s:set>	
	</s:if>
	<s:else>
		<s:set var="disabledConsultaDescripcion">true</s:set>
	</s:else>
	<table id="agregar" border="0" width="100%">
		<tr>
			<td class="titulo"colspan="2">
				<s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.title"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="numMotor"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numMotor"/></label>
			</td>
			<td>
			 <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			 	<s:textfield cssClass="txtfield jQrequired"  					 
					 maxlength="19" size="25" id="numMotor" name="biAutoInciso.value.numeroMotor" />	
			 </s:if>
			  <s:else>
			  	<s:property value="biAutoInciso.value.numeroMotor"/>
			  </s:else>
									
			</td>
			<td>
				<label for="numSerie"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numSerie"/></label>
				
			</td>
			<td>
			
			<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar() && !biAutoInciso.value.coincideEstilo">
			 	<s:textfield cssClass="txtfield jQrequired jQalphanumeric jQrestrict w160"   					 
					 maxlength="17" size="25" id="numSerie" name="biAutoInciso.value.numeroSerie" 
					 onblur="remplazarCaracteresNumSerie(this)"/>
			 </s:if>
			 <s:else>
			  	<s:property value="biAutoInciso.value.numeroSerie"/>
			 </s:else>
				
			</td>
			
		</tr>
		<tr>
			<td>
				<label for="numPlaca"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.numPlaca"/></label>
				
			</td>
			<td>
			
			<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			 	<s:textfield cssClass="txtfield jQrequired"  					 
					 maxlength="10" size="25" id="numPlaca" name="biAutoInciso.value.placa" maxlength="12" />
			</s:if>
			<s:else>
			  	<s:property value="biAutoInciso.value.placa"/>
			</s:else>
								
			</td>
			<td><label for=""><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.descripcion"/></label>
				
			</td>
			<td colspan="3">
			
			<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar() && !biAutoInciso.value.coincideEstilo">
			 	<s:textfield cssClass="txtfield jQrequired" disabled="%{#disabledConsultaDescripcion}"					 
					 maxlength="80" size="50" id="descripcion" name="biAutoInciso.value.descripcionFinal" maxlength="500" />
			</s:if>
			<s:else>
			  	<s:property value="biAutoInciso.value.descripcionFinal"/>
			</s:else>
								
			</td>
		</tr>
		<tr>
			<td><label for="repuve"><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.repuve" /></label>
				 
			</td>		
			<td>
			
			<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			 	<s:textfield cssClass="txtfield"  					 
					 maxlength="10" size="25" id="repuve" name="biAutoInciso.value.repuve" maxlength="10"/>
			</s:if>
			<s:else>
				<s:property value="biAutoInciso.value.repuve"/>			
			</s:else>				
			</td>
			<td><label for="emailContanto"><s:text name="midas.cotizacion.emails"/></label>
			</td>
			<td colspan="3">
			
			<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			 	<s:textfield cssClass="txtfield"  					 
					 maxlength="500" size="50" id="emailContanto" name="biInciso.value.emailContacto" />
			</s:if>
			<s:else>
			  	<s:property value="biInciso.value.emailContacto"/>
			</s:else>
								
			</td>						
		</tr>
		<tr>
			<td><s:text name="midas.suscripcion.cotizacion.auto.complementar.inciso.rutacirculacion" />			 
			</td>		
			<td colspan="5">
				<s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
					<s:textfield cssClass="txtfield"  			 
						 maxlength="200" size="100" id="rutaCirculacion" name="biAutoInciso.value.rutaCirculacion" />
				</s:if>
				<s:else>
				  	<s:property value="biInciso.value.rutaCirculacion"/>
				</s:else>					 
			</td>			
		</tr>			
	</table>
	
<div id="contenedorObservaciones" style="width:98%">
		<table id="agregar" style="width:98%">
			<tr>
				<td class="titulo" colspan="2"><s:text
						name="midas.suscripcion.cotizacion.auto.complementar.inciso.observaciones" />
				</td>
			</tr>
			<tr>
			<td>
			    <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			        <s:textarea cssClass="txtfield" name="biAutoInciso.value.observacionesinciso" cols="70" rows="4" disabled="%{#disabledConsultaDescripcion}"></s:textarea>
			    </s:if>
			    <s:else>
			        <s:textarea cssClass="txtfield" name="biAutoInciso.value.observacionesinciso" cols="70" rows="4" readOnly="true" disabled="%{#disabledConsultaDescripcion}"></s:textarea>
			    </s:else>				
			</td>
			</tr>
		</table>
</div>