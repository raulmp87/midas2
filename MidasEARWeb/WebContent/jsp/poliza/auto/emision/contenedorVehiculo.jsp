<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<style type="text/css">
input {
	min-width: 100px;
	max-width: 230px;
}

select {
	min-width: 100px;
	max-width: 230px;
}

textarea {
	min-width: 230px;
	max-width: 330px;
}

div.ui-datepicker {
	font-size: 10px;
}
div{
overflow-x:visible;
overflow-y:visible;
}
</style>
<script type="text/javascript">
	function validaGuardar() {
		if (validateAll(true, 'save')
				&& validarFecha(jQuery('#fechaNacimiento').val())) {
			guardarComplementarDatosInciso();
		}
	}

	function getAge(dateString) {
		var today = new Date();
		var birthDate = new Date(dateString);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age;
	}
	
	function validarFecha(fecha) {
		if (fecha != null && fecha != undefined) {
			var dia = fecha.substring(0, 2);
			var mes = fecha.substring(3, 5);
			var anio = fecha.substring(6, 10);

			if (getAge(mes+"/"+dia+"/"+anio) < 18) {
				parent.mostrarMensajeInformativo(
						"El conductor tiene que ser mayor de edad", "10", null,
						null);
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	 
function guardarComplementarDatosInciso(){	
	var form = jQuery('#complementarIncisoForm')[0];
	var nextFunction = "&nextFunction=cerrarComplementarDatosInciso";
	var url = '/MidasWeb/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarIncisoEndoso/guardar.action' 
	+ '?' + jQuery(form).serialize() + nextFunction;			
	parent.redirectVentanaModal('ventanaVehiculo', url, null);	
}

function cerrarComplementarDatosInciso() {
	cerrarVentanaModal('ventanaVehiculo');	
}

jQuery(document).ready(function() {
		jQuery('#loading').css('display', 'none');		
	});
</script>



<s:form  id="complementarIncisoForm">
<s:hidden id="validoEn" name="validoEn" />
<s:hidden id="recordFrom" name="recordFrom" />
<s:hidden name="validoEnMillis" id="validoEnMillis"></s:hidden>
<s:hidden name="recordFromMillis" id="recordFromMillis"></s:hidden>
<s:hidden id="incisoId" name="incisoId" />
<s:hidden id="cotizacionId" name="cotizacionId" />
<s:hidden id="accionEndoso" name="accionEndoso" />
<s:hidden id="claveTipoEndoso" name="claveTipoEndoso" />
<s:hidden id="mostrarDatoConductorInciso" name="mostrarDatoConductorInciso" />
<s:hidden id="mostrarSoloConductor" name="mostrarSoloConductor" />

<s:if test="mostrarDatoConductorInciso">
	<div id="contenedorConductor" style="width:100%;overflow:auto;">
		<jsp:include page="datosConductor.jsp"/>
	</div>
</s:if>

<br/>

<s:if test="!mostrarSoloConductor">
	<div id="contenedorControles" style="width:98%;">
		<jsp:include page="datosVehiculo.jsp"/>
	</div>
</s:if>
<br/>
<div id="contenedorPaquete" style="width:98%;">
		<s:if test="!mostrarSoloConductor">
			<table id="agregar" border="0" width="98%">
			<tr>
				<td class="titulo"colspan="2">
					<s:label  value="Datos Adicionales del Paquete"/>	
				</td>
			</tr>
			<tr><td>	
				<div>
							<div id="loading" style="text-align: center;">
								<img src="/MidasWeb/img/loading-green-circles.gif"> <font
									style="font-size: 9px;">Procesando la información, espere
									un momento por favor...</font>
							</div>
							
							<div id="controlesGrid"></div>						
				
				</div>
			</td></tr>
			
			</table>
		</s:if>
		<div class="inline align-right">
		 <s:if test="accionEndoso==@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditar()">
			<div class="btn_back w100 guardar">
				<a href="javascript: void(0);" id="btnGuardar" class="icon_guardar"	onclick="javascript: validaGuardar();">
					<s:text name="Guardar"/>
				</a>
			</div>		
			<div class="btn_back w100">
				<a href="javascript: void(0);" id="salirBtn" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cerrar la ventana?')){parent.cerrarVentanaModal('ventanaVehiculo');} ">
					<s:text name="Salir"/>
				</a>
			</div>
		</s:if>			
		</div>	
</div>
</s:form>
<s:if test="mostrarDatoConductorInciso">
	<script type="text/javascript">
		validaCiente();
	</script>
</s:if>
 <script  type="text/javascript">
	mostrarControles();
</script>
