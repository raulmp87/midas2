<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <column id="numeroSecuencia" type="ro" width="80" sort="int" hidden="false">#</column>
      	<column id="descripcionGiroAsegurado" type="ro" width="500" sort="str" hidden="false"><s:text name="midas.suscripcion.cotizacion.inciso.descripcion.vehiculo"/></column>
		<column id="valorPrimaNeta" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.general.primaneta"/></column>
		<column id="paquete" type="ro" width="100" sort="str" hidden="false"><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.paquete"/></column>		
		<column id="numeroSerie" type="ro" width="137" sort="str">No. Serie</column>
		<column id="numeroMotor" type="ro" width="137" sort="str">No. Motor</column>
		<column id="placa" type="ro" width="82" sort="str">Placas</column>
		<column id="nombreConductor" type="ro" width="200" sort="str">Conductor</column>
		<column id="nombreAsegurado" type="ro" width="200" sort="str">Asegurado</column>
		<column id="accionDefinir" type="img" width="81" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="asegurado" type="img" width="30" sort="na" align="center"></column>				
	</head>
	<s:iterator value="biIncisoList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.descripcionFinal" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="%{getText('struts.money.format',{value.valorPrimaNeta})}" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.paquete.descripcion" escapeHtml="false" escapeXml="true"/></cell>					
			<cell><s:property value="value.autoInciso.numeroSerie" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.numeroMotor" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.placa" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.nombreCompletoConductor" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.nombreAsegurado" escapeHtml="true" escapeXml="true"/></cell>			
			<cell>../img/icons/ico_verdetalle.gif^Ver Datos Inciso^javascript:mostrarVentanaVehiculo(<s:property value="continuity.id"/>);^_self</cell>
			<cell>../img/persona.jpg^Ver detalle Asegurado^javascript:mostrarVentanaAseguradoConsulta(<s:property value="continuity.id" />);^_self</cell>			
		</row>
	</s:iterator>
</rows>

