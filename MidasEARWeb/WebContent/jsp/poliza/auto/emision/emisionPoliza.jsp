<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/emision/emisionPoliza.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>

	<s:hidden name="biCotizacion.continuity.id" id="cotizacionId"/>
	<s:hidden name="validoEn" id="validoEn"/>
	<s:hidden name="recordFrom" id="recordFrom"/>
	<s:hidden name="validoEnMillis" id="validoEnMillis"></s:hidden>
	<s:hidden name="recordFromMillis" id="recordFromMillis"></s:hidden>
	<s:hidden name="aplicaFlotillas" id="aplicaFlotillas"></s:hidden>

	<table>
		<tr>
			<td colspan="3">
				<div class="c12 titulo">
					<s:text name="midas.poliza.datosComplementarios" />
					<s:text name="midas.emision.nopoliza" />
					<b><s:property value="polizaDTO.numeroPolizaFormateada"/></b>	
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="c12 titulo">
					<s:text name="midas.cotizacion.datoscontratante" />
					<s:property value="biCotizacion.value.nombreContratante" />
				</div>
			</td>
			<td width="10%"/>
			<td align="right">
				<div id="enviaEmail" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);"
						onclick="ventanaCorreo(<s:property value="polizaId"/>,2);"
						class="icon_email"> <s:text name="midas.cotizacion.enviarmail" />
					</a>
				</div>
			</td>		
		</tr>
		
	</table>
<s:if test="aplicaFlotillas== 1">
	<s:form id="incisosFiltroEmisionForm">	
		<div id="filtroEmision">
			<s:include value="/jsp/poliza/auto/incisos/incisosFiltro.jsp"></s:include>
		</div>
	</s:form>
</s:if>
<div id ="listadoDeIncisos" style="height: 400px; float:both;" class="agregar">
	<div class="subtituloIzquierdaDiv"><s:text name="midas.poliza.inciso.listado" /></div>
	<div id="indicador" style="left:214px; top: 40px; z-index: 1"></div>
	<div id="emision_gridListadoDeIncisos">
		<div id="listadoIncisosEmision" style="width: 98%; height: 135px"></div>
		<div id="pagingArea"></div>
		<div id="infoArea"></div>
	</div>
</div>
<div class="btn_back w110">
	<a href="javascript: regresarListadoPoliza();" class="icon_regresar" onclick="">
		<s:text name="midas.boton.regresar"/>
	</a>
</div>

<script type="text/javascript">
	pageGridPaginadoIncisosEmision(1,true);
</script>