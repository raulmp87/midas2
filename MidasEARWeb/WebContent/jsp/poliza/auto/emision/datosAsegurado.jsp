<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/inciso/contenedorHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<style type="text/css">
input {
	min-width: 100px;
	max-width: 230px;
}

select {
	min-width: 100px;
	max-width: 230px;
}

textarea {
	min-width: 230px;
	max-width: 330px;
}

div.ui-datepicker {
	font-size: 10px;
}
div{
overflow-x:visible;
overflow-y:visible;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
		jQuery('#loading').css('display', 'none');		
	});
</script>

<s:form action="guardar" id="complementarIncisoForm">
<s:hidden id="cotizacionId" name="cotizacionId" />
<s:hidden id="incisoId" name="incisoId" />
<s:hidden id="guardadoExitoso" name="guardadoExitoso" />
<s:if test="guardaDatoConductorInciso">
	<div id="contenedorConductor" style="width:100%;overflow:auto;">
		<jsp:include page="datosConductor.jsp"/>
	</div>
</s:if>
<br/>
<div id="contenedorControles" style="width:98%;">
	<jsp:include page="contenedorVehiculo.jsp"/>
</div>
<br/>
<div id="contenedorPaquete" style="width:98%;">
		<table id="agregar" border="0" width="98%">
		<tr>
			<td class="titulo"colspan="2">
				<s:label  value="Datos Adicionales del Paquete"/>	
			</td>
		</tr>
		<tr><td>	
			<div>
						<div id="loading" style="text-align: center;">
							<img src="/MidasWeb/img/loading-green-circles.gif"> <font
								style="font-size: 9px;">Procesando la información, espere
								un momento por favor...</font>
						</div>
						<s:action name="cargaControlDinamicoRiesgo" executeResult="true" 
		namespace="/suscripcion/cotizacion/auto/complementar/datosRiesgo" 
		ignoreContextParams="true">
			<s:param name="idToCotizacion" value="%{cotizacionId}"/>
			<s:param name="numeroInciso" value="%{inciso.id.numeroInciso}"/>
			<s:param name="name" value="'datosRiesgo'"/>
		</s:action>
		</div>
		</td></tr>
		
		</table>
	
		<div class="inline align-right">
			<div class="btn_back w100 guardar">
				<a href="javascript: void(0);" id="btnGuardar" class="icon_guardar"	onclick="javascript: validaGuardar();">
					<s:text name="Guardar"/>
				</a>
			</div>
			<div class="btn_back w100">
				<a href="javascript: void(0);" id="salirBtn" onclick="if(confirm('\u00BFEst\u00E1 seguro que desea cerrar la ventana?')){parent.cerrarVentanaModal('ventanaVehiculo');} ">
					<s:text name="Salir"/>
				</a>
			</div>	
		</div>
		
</div>
	

</s:form>
<s:if test="guardaDatoConductorInciso">
<script type="text/javascript">
	validaCiente();	
</script>
</s:if>
<script type="text/javascript">
	if(jQuery("#guardadoExitoso").val() == "true"){
		var idCotizacion = jQuery('#cotizacionId').val();		
		parent.sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/complementar/iniciarComplementar.action?cotizacionId='+idCotizacion,'contenido', "parent.cerrarVentanaModal('ventanaVehiculo')");		
	}	
</script>