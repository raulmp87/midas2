<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/poliza/auto/polizaHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript">
	function cleanInput(id) {
		jQuery('#' + id).val('');
	}
	function cleanInputDiv(id) {
		jQuery('#' + id).text('');
	}
	
	var agenteControlDeshabilitado = <s:property value="agenteControlDeshabilitado"/>;

</script>

<s:form action="listar" id="polizaForm" namespace="/poliza">
	<div class="titulo" style="width: 98%;">
		<s:text name="Listado de P&oacute;liza Autos"/>	
	</div>	
	<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" style="border:0;">
			<tr>
				<td><s:textfield cssClass="txtfield jQnumeric jQrestrict" 
						key="midas.poliza.numeroPoliza"
						labelposition="top" 
						size="10"
						maxlength="8"						
						id="polizaDTO.numeroPoliza" name="polizaDTO.numeroPoliza" />
				</td>
			    <td>
						<sj:datepicker name="polizaDTO.fechaCreacion"
							key="midas.poliza.fecha.emision.desde"
							labelposition="top" 	
							maxDate="today"
							changeMonth="true"
							changeYear="true"				
							buttonImage="../img/b_calendario.gif"
	                        id="polizaDTO.fechaCreacion"
							maxlength="10" cssClass="txtfield"
							size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				</td>
			    <td>
						<sj:datepicker name="polizaDTO.fechaCreacionHasta"
							key="midas.poliza.fecha.emision.hasta"
							labelposition="top" 					
							buttonImage="../img/b_calendario.gif"
						    id="fechaFinal"
						    size="12"
						    maxDate="today"
							changeMonth="true"
							changeYear="true"
							maxlength="10" cssClass="txtfield"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				 </td>		 	     			 
		    </tr>
			<tr>
				<td>
					<s:textfield key="midas.poliza.numeroPolizaSeycos" cssClass="txtfield jQnumeric jQrestrict"
								 labelposition="top" 
								 size="12"
								 maxlength="12"
								 id="polizaDTO.numeroPolizaSeycos" name="polizaDTO.numeroPolizaSeycos" />														
				</td>
				<td>
 					<s:textfield key="midas.poliza.nombrecontratante" cssClass="txtfield jQalphaextra jQrestrict"
								 maxlength="60"
								 labelposition="top" 
								 id="polizaDTO.cotizacionDTO.nombreContratante" name="polizaDTO.cotizacionDTO.nombreContratante"
								 onblur="validaLongitud('polizaDTO.cotizacionDTO.nombreContratante', this.value)" />		
				</td>
				<td>
					<s:textfield key="midas.poliza.numeroSerie" cssClass="txtfield jQalphanumeric jQrestrict"
								 maxlength="17" 
								 labelposition="top" 
								 id="polizaDTO.numeroSerie" name="polizaDTO.numeroSerie" />						  			
				</td>																										
			</tr>
			<tr>
				<td>
					<s:hidden id="idAgentePol" name="polizaDTO.cotizacionDTO.solicitudDTO.codigoAgente"/>
						<div><label for="agenteNombre"><s:text name="midas.poliza.nombreAgente"></s:text></label></div>
						<br/>
						<div style="vertical-align: bottom;" >
						<s:if test="!agenteControlDeshabilitado">
	 						<s:set var="agenteControlOnclick" value="'seleccionarAgentePoliza(1);'"/>
				 		</s:if>
						<div id="agenteNombre" class="divInputText txtfield" style="width: 180px; float: left; " onclick="${agenteControlOnclick}">
							<s:property value="polizaDTO.cotizacionDTO.solicitudDTO.agente.persona.nombreCompleto"/>
				 		</div>
				 		<div style="float: left; vertical-align: bottom;">
				 		<s:if test="!agenteControlDeshabilitado">
			 			<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripción" 
				 			style="margin-left: 5px;"
				 			onclick ="cleanInputDiv('agenteNombre');cleanInput('idAgentePol');"
				 		/>
				 		</s:if>
				 		</div>
				 		</div>													
				</td>
				<td>
						<s:hidden id="idNegocioPol" name="polizaDTO.cotizacionDTO.solicitudDTO.negocio.idToNegocio"/>
						<div><label for="negocioNombre"><s:text name="midas.negocio.nombrenegocio"></s:text></label></div>
						<br/>
						<div style="vertical-align: bottom;" >
						<div id="negocioNombre" class="divInputText txtfield" style="width: 180px; float: left;" onclick="seleccionarNegocioPoliza(1);">
				 		</div>
				 		<div style="float: left; vertical-align: bottom;">
			 			<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripción" 
				 			style="margin-left: 5px;"
				 			onclick ="cleanInputDiv('negocioNombre');cleanInput('idNegocioPol');"
				 		/>	
				 		</div>
				 		</div>
				</td>
				<td>
 					 <s:text name="midas.cotizacion.conflictoNumeroSerie" />:
					 <s:checkbox name="polizaDTO.cotizacionDTO.conflictoNumeroSerie" id="conflictoNumeroSerie"  />
 							  						  			
				</td>																										
			</tr>
			<tr>
				<td>
					<s:text name="midas.negocio.seccion.estilos.tipos"/>:
					
					<s:select name="polizaDTO.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas"
						id="polizaDTO.cotizacionDTO.tipoPolizaDTO.claveAplicaFlotillas" headerKey="2"
						headerValue="Seleccione ..." cssClass="txtfield input_text jQrequired"
 						list="#{'0':'INDIVIDUAL','1':'FLOTILLA','2':'AMBOS'}">
 					</s:select> 
				</td>
				<td>
					<s:text name="midas.folio.reexpedible.field.numero.folio.poliza"/>
					<s:textfield cssClass="txtfield" id="numeroFolio" 
						name="polizaDTO.cotizacionDTO.folio"/>
				</td>
				<td>
					<s:text name="midas.emision.totalincisos"/>:
					<s:textfield key="midas.modprima.rangoMinimo" cssClass="txtfield jQnumeric jQrestrict"
								 maxlength="6" 
								 labelposition="left" 
								 id="polizaDTO.limiteInferiorIncisos" name="polizaDTO.limiteInferiorIncisos" />	
					<s:textfield key="midas.modprima.rangoMaximo" cssClass="txtfield jQnumeric jQrestrict"
								 maxlength="6" 
								 labelposition="left" 
								 id="polizaDTO.limiteSuperiorIncisos" name="polizaDTO.limiteSuperiorIncisos" />						  			
				</td>																										
			</tr>
		</table>
	</div>
	<div  style="width: 98%;">
		<table id="agregar">
			<tr>		
				<td colspan="4">		
					<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td>
									<div class="btn_back w140" style="display: inline; float: right;">
										<a href="javascript: void(0);"
											onclick="limpiarFiltrosPoliza();"> <s:text
												name="midas.suscripcion.cotizacion.limpiar" /> </a>
									</div>
	
									<div class="btn_back w140" style="display: inline; float: right;">
										<a id="submit" href="javascript: void(0);" onclick="loadBuscarPolizaPag();">
											<s:text name="midas.suscripcion.cotizacion.buscar" /> </a>
									</div>
	
							</td>							
						</tr>
					</table>				
				</td>		
			</tr>
		</table>
	</div>	
</s:form>
<div id="indicador"></div>
<div id="gridPolizasPaginado" >
<div id="polizaGrid" style="width: 98%; height: 135px"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>
</div>
<s:if test="esRetorno==1">
<script type="text/javascript">
	loadBuscarPolizaPag();
</script>
</s:if>
<s:else>
<script type="text/javascript">
	initPoliza();
</script>
</s:else>