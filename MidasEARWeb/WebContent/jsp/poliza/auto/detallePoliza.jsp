<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<table id="agregar">
	<tr>
		<th width="20%"><s:text name="midas.poliza.estatusCobranza" /></th>
		<td width="80%"><s:property value="biCotizacion.value.estatusCobranza"/></td>						
	</tr>
	<tr>
		<th><s:text name="midas.cotizacion.formapago" /></th>
		<td><s:property value="biCotizacion.value.formaPago.descripcion"/></td>	
	</tr>	
	<tr>
		<th><s:text name="midas.cotizacion.iniciovigencia" /></th>
		<td><s:property value="biCotizacion.value.fechaInicioVigencia"/></td>
	</tr>					
	<tr>
		<th><s:text name="midas.cotizacion.finvigencia" /></th>
		<td><s:property value="biCotizacion.value.fechaFinVigencia"/></td>
	</tr>
	<tr>
		<th><s:text name="midas.cotizacion.descuentoporvolumen"/>:</th> 
		<td><s:property value="biCotizacion.value.descuentoVolumen" default="0.0"/>%</td>
	</tr>
	<tr>
		<th><s:text name="midas.cotizacion.comisioncedida"/>:</th>
		<td><s:property value="biCotizacion.value.porcentajebonifcomision"/>%</td>
	</tr>
	<tr>
		<th><s:text name="midas.cotizacion.derechos"/>:</th>
		<td><s:property value="biCotizacion.value.negocioDerechoPoliza.importeDerecho"/></td>	
	</tr>
</table>          				