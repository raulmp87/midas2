<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>

<sj:head/>
<script type="text/javascript">jQuery.noConflict();</script>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>

<script type="text/javascript">
function validaCorreoPoliza(){
        $(".error").hide();
        var hasError = false;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 
        var emailaddressVal = jQuery('#UserEmail').val().split(";");
        if(emailaddressVal == '') {	
            $("#btn-submit").before('<span class="error">Proporcione la direcci&oacute;n de correo</span>');
            hasError = true;
        }
 
         for(i = 0; i < emailaddressVal.length; i++){  
	         if(!emailReg.test(emailaddressVal[i])) { 	
	           jQuery("#btn-submit").before('<span class="error">Proporcione una direcci&oacute;n v&aacute;lida</span> ');
	           hasError = true;
	          break;
	         }
	      }
        if(hasError == true) {
        	return false; 
        }else{
        	//parent.cerrarVentanaModal("correo");
        	parent.submitVentanaModal("correo", document.imprimirPoliza);
        	//jQuery("#btn-submit").click();
        }
}
</script>
<s:form action="enviarPolizaPorCorreo" namespace="/impresiones/poliza" id="imprimirPoliza" >
<s:hidden name="idToPoliza" id="idToPoliza" />
<s:hidden name="claveTipoEndoso" id="claveTipoEndoso" />
<s:hidden name="validoEn" id="validoEn" />
<s:hidden name="validoEnMillis" id="validoEnMillis" />
<s:hidden name="recordFrom" id="recordFrom" />
<s:hidden name="recordFromMillis" id="recordFromMillis" />
<center>
	<table id="agregar">	
		<tr>
			<td colspan="2">
				<s:textfield labelposition="left" key="midas.correo.destinatario" name="nombreDestinatario" size="50"/>			
			</td>
		</tr>	
		<tr>			
			<td colspan="2">
				<s:textarea key="midas.cotizacion.emails"
							id="UserEmail" name="correo"  cssClass="w250 cajaTextoM2 jQemail jQrequired" cssStyle="font-size:11px;"
							title="Correos electronicos separados por punto y coma"
							alt="Correos electronicos separados por punto y coma"
							rows="2" cols="45"  />				
			</td>
		</tr>
		<tr>
			<td>
				<!--<s:submit id="btn-submit" key="midas.cotizacion.enviar"	cssClass="b_submit" align="left" cssStyle="display:none;"/>-->
				<div id="enviaEmail" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);"
						onclick="validaCorreoPoliza();"> 
						<s:text name="midas.cotizacion.enviar" />
					</a>
				</div>
			</td>
			<td>
				<!-- <s:submit id="btn-submit-cancel" key="midas.cotizacion.cancelar"	cssClass="b_submit" align="left"/>-->
				<div id="cerrarEmail" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);"
						onclick="parent.cerrarVentanaModal('correo');"> 
						<s:text name="midas.cotizacion.cancelar" />
					</a>
				</div>
			</td>		
		</tr>	    
	</table>
</center>
<div id="loading" style="display: none;">
                <img id="img_indicator" name="img_indicator"
                               src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>
</s:form>