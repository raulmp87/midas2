<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" src='<s:url value="/js/validaciones.js"></s:url>'></script>
<script language="JavaScript" src='<s:url value="/js/midas2/poliza/auto/poliza.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/componente/impresiones/edicionImpresionPoliza.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
</script>

<style type="text/css">
.icon_guardar {
	display: none;
}
#infoMessage, #error{
display:none;
border: 1px solid;
margin: 10px 0px;
padding:15px 10px 15px 50px;
background-repeat: no-repeat;
background-position: 10px center;
font-size: 12px;
text-aling: center;
margin-left: 242px;
width: 320px;
}
#error {
	color: #D8000C;
	background-color: #FFBABA;
}

#infoMessage {
	color: #00529B;
	background-color: #BDE5F8;
}
</style>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<s:hidden name="tienePermisosImprimirEdicion" id="tienePermisosImprimirEdicion" />
<s:hidden name="tienePermisosModificarEdicion" id ="tienePermisosModificarEdicion" />

<table width="98%" id=t_riesgo border="0">
	<tr>
		<td class="titulo" colspan="8">
			Endosos de la p&oacute;liza <s:property value="numeroPolizaFormateada"/>
		</td>
	</tr>
	<s:set name="hasEndosos" value="%{endosoPolizaList.size() > 1}" />
	<tr>
		<th>N&uacute;mero de Endoso</th>
		<th>Motivo del endoso</th>
		<th>Tipo de endoso</th>
		<th>Fecha de Emisi&oacute;n</th>
		<s:if test="accion==\"consultar\"">
			<th>Consultar</th>
			<s:set id="iconClass" value="'b_submit icon_buscar w140'"/>
		</s:if>
		<s:else>
		    <s:if test="#hasEndosos == true">
		      <th>Imprimir endoso</th>
		      <th>Imprimir poliza</th>
		    </s:if>
		    <s:else>
		      <th>Imprimir</th>
		    </s:else>
			
			<s:set id="iconClass" value="'b_submit icon_imprimir w140'"/>
		</s:else>
		<s:set id="claveTextoBoton" value="%{getText('midas.poliza.consultar.situacionactual')}" />
	</tr>
	
	<s:iterator value="endosoPolizaList">
	<tr>
		<td class ="txt_v2">
			<s:text name="descripcionNumeroEndoso"></s:text>
		</td>
		<td class ="txt_v2">
			<s:text name="descripcionMotivoEndoso"></s:text>
		</td>
		<td class ="txt_v2">
			<s:text name="descripcionTipoEndoso"></s:text>
		</td>
		<td class ="txt_v2">
			<s:text name="fechaCreacion"></s:text>
		</td>
		<td class ="txt_v2">
			<s:if test="accion==\"consultar\"">
				<center>
					<m:tienePermiso nombre="FN_M2_Emision_Emision_Consultar_Poliza,FN_M2_Emision_Emision_Consultar_Endoso">
				  <a href="javascript: void(0);"
					onclick="javascript: verDetallePoliza(<s:text name="id.idToPoliza"/>,'<s:text name="validFrom"/>', <s:text name="%{validFrom.getTime()}"/>, '<s:text name="recordFrom"/>', <s:text name="%{recordFrom.getTime()}"/>, <s:text name="tipoRegreso"/>, <s:text name="claveTipoEndoso"/>)">
					<img border='0px' alt='Consultar' title='Consultar' src='/MidasWeb/img/icons/ico_verdetalle.gif'/>
				  </a>
					</m:tienePermiso>
				</center>
			</s:if>
			<s:else>
				<s:if test="descripcionNumeroEndoso==\"Poliza Original\"">
					<s:if test="#hasEndosos == true">
					    <!-- crea una celda en blanco, que corresponde a la columna de endosos -->
					    </td>
					     &nbsp;
					    <td class="txt_v2">
				    </s:if>
					<center>
						<s:if test="validFrom == null">
							<a href="javascript: void(0);" 
							   onclick="javascript:mostrarContenedorImpresion(2, <s:text name='id.idToPoliza'/>, -1, -1, '<s:text name="validFrom"/>', <s:text name="%{validFrom.getTime()}"/>, <s:text name="%{recordFrom.getTime()}"/>, '<s:text name="recordFrom" />', <s:text name="claveTipoEndoso"/>)">
							   <img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/ico_impresion.gif'/>
						    </a>
						</s:if>
						<s:else>
							<a href="javascript: void(0);" 
							   onclick="javascript:mostrarContenedorImpresion(2, <s:text name='id.idToPoliza'/>, -1, -1, '<s:text name="validFrom"/>', <s:text name="%{validFrom.getTime()}"/>, <s:text name="%{recordFrom.getTime()}"/>, '<s:text name="recordFrom" />', <s:text name="claveTipoEndoso"/>)">
							   <img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/ico_impresion.gif'/>
						    </a>
						</s:else>
					</center>
				</s:if>
				<s:else>
					<center>
					    <a href="javascript: void(0);" 
						   onclick="javascript:imprimirEndoso( <s:text name="idToCotizacion"/>, '<s:text name="recordFrom"/>','<s:text name="%{recordFrom.getTime()}"/>', <s:text name="claveTipoEndoso" />)">
						   <img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/ico_impresion.gif'/>
					    </a>
					    <s:if test="tienePermisosModificarEdicion">
					    <a href="javascript: void(0);" 
						   onclick="javascript:mostrarEditarImpresionEndoso(2, <s:text name="id.idToPoliza"/>, '<s:text name="validFrom"/>', <s:text name="%{validFrom.getTime()}"/>, '<s:text name="%{recordFrom.getTime()}"/>', '<s:text name="recordFrom"/>', <s:text name="claveTipoEndoso" />)">
						   <img border='0px' alt='Editar Endoso' title='Editar Endoso' src='/MidasWeb/img/icons/ico_editar.gif'/>
					    </a>
					    </s:if>
					    <s:if test="(tienePermisosModificarEdicion || tienePermisosImprimirEdicion)">
					    <div id='<s:text name="%{idToCotizacion.toString() + recordFrom.getTime().toString() + claveTipoEndoso.toString()}"/>' 
					    	<s:if test="!existeEdicionEndoso" >style="display:none;"</s:if><s:else>style="display:inline;"</s:else>
					    ><a href="javascript: void(0);" 
						   onclick="javascript:imprimirEndosoEditado( <s:text name="idToCotizacion"/>, '<s:text name="recordFrom"/>','<s:text name="%{recordFrom.getTime()}"/>', <s:text name="claveTipoEndoso" />)">
						   <img border='0px' alt='Imprimir Endoso Editado' title='Imprimir Endoso Editado' src='/MidasWeb/img/ico_impresion.gif'/>
					    </a></div>
					    </s:if>
					</center>
					<s:if test="#hasEndosos == true">
						</td>
						<td class="txt_v2">
					  		<center>
							    <a href="javascript: void(0);" 
								   onclick="javascript:mostrarContenedorImpresion(2, <s:text name='id.idToPoliza'/>, -1, -1, '<s:text name="validFrom"/>', <s:text name="%{validFrom.getTime()}"/>, <s:text name="%{recordFrom.getTime()}"/>, '<s:text name="recordFrom" />', <s:text name="claveTipoEndoso"/>)">
								   <img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/ico_impresion.gif'/>
								</a>
							</center>
				    </s:if>
				</s:else>	
			</s:else>
		</td>
	</tr>
	</s:iterator>
	<s:set name="lastEndosoIdx" value="endosoPolizaList.size()" />
	<s:set name="lastEndosoValidFrom" value="endosoPolizaList.get(lastEndosoIdx-1).getValidFrom()}" />
	<s:set name="lastEndosoRecordFrom" value="endosoPolizaList.get(lastEndosoIdx-1).getRecordFrom()" />
	<s:set name="lastEndosoValidFromTimeString" value="lastEndosoValidFrom.getTime()" />
	<s:set name="lastEndosoRecordFromTimeString" value="lastEndosoRecordFrom.getTime()" />
	<tr>
	    <s:if test="#hasEndosos == true">
	      <td colspan="6">
	    </s:if>
	    <s:else>
	      <td colspan="5">
	    </s:else>
			<div id="divGuardarBtn" style="display: block; float:right;">
				<div class="btn_back"  > 
				<s:if test="accion==\"consultar\"">
					<m:tienePermiso nombre="FN_M2_Emision_Emision_Consultar_Poliza">
					<s:submit onclick="javascript: verDetallePoliza(%{id}, null, %{ultimoEndosoValidFromDateTimeStr}, null, %{ultimoEndosoRecordFromDateTimeStr}, %{tipoRegreso}, %{ultimoEndosoClaveTipoEndoso}, true, true); return false;" 
						value="%{#claveTextoBoton}"  cssClass="%{#iconClass}"/>
					</m:tienePermiso>
				</s:if>
				<s:else>
				    <!-- Control html de prueba imprimir estado actual-->
				    <!-- 
			    	<a href="javascript: void(0);" 
					   onclick="javascript:mostrarContenedorImpresion(
					   2, <s:text name='id'/>, -1, -1, null, null)" >
					   <img border='0px' alt='Imprimir' title='Imprimir' src='/MidasWeb/img/ico_impresion.gif'/>
				    </a>
				     -->
				    <!-- La acción se debe siparar con este botón -->
				    <!-- Este es el botón original con el evento onclick modificado -->
				    												<!-- tipoImpresion,idToPoliza,numeroPolizaFormateada,idCotizacion, validOn, validOnMillis, recordFromMillis, recordFrom -->
					<s:submit onclick="javascript:mostrarContenedorImpresion(2, %{id}, -1, -1, null, %{ultimoEndosoValidFromDateTimeStr}, %{ultimoEndosoRecordFromDateTimeStr}, null, %{ultimoEndosoClaveTipoEndoso}); return false;"
						value="%{#claveTextoBoton}"  cssClass="%{#iconClass}"/>	
				</s:else> 
				</div>
			</div>
		</td>
	</tr>
</table>
