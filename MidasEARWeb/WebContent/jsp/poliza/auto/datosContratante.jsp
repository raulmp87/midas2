<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="subtituloIzquierdaDiv"><s:text name="midas.poliza.datosContratante" /></div>
<table id="agregar">
	<tr>
		<th width="10%">
			<s:text name="midas.negocio.nombre" /> 
		</th>
		<td width="70%">    
			<s:property value="biCotizacion.value.nombreContratante"/>
		</td>
		<td>
			<div id="datosCliente" class="btn_back w140" style="display: inline; float: right;">
				<a href="javascript: void(0);" onclick="mostrarDatosCliente(<s:property value="biCotizacion.value.personaContratanteId"/>);">	
					<s:text name="midas.poliza.datosCliente"/>	
				</a>
		    </div>
		</td>  
	</tr>   
</table>
