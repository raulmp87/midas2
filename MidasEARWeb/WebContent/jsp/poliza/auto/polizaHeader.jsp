<%@taglib prefix="s" uri="/struts-tags"%>
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
.divInputText{
    width: 180px;
    max-width:180px;
    height:14px;
    max-height:14px;
    background-color:#FFF;
    text-align:left;
    cursor:text;
	-moz-user-select: -moz-none;
   	-khtml-user-select: none;
	-webkit-user-select: none;
   /*
     Introduced in IE 10.
     See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/
   */
   	-ms-user-select: none;
   	user-select: none;
}
</style>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/poliza/auto/poliza.js'/>"></script>
<script type="text/javascript">

function $_imprimirDetallePoliza(idToPoliza,claveTipoEndoso,recordFromMillis,validOnMillis){
	var url = '<s:url action="generarImpresion" namespace="/impresiones/componente"/>';
	var validOn = new Date();
	fechaValidaOn = validOn.getDate() + "/" + validOn.getMonth() + "/" + validOn.getFullYear();
		url += "?idToPoliza="
				+ idToPoliza
				+ "&chkanexos=true&chkcaratula=true&chkcertificado=true&chkinciso=true&chkrecibo=true"
				+ "&claveTipoEndoso=" + claveTipoEndoso + "&tipoImpresion=2"
				+ "&validOn=" + fechaValidaOn +"&recordFromMillis="
				+ validOnMillis+ "&validOnMillis=" + recordFromMillis;
		parent.mostrarVentanaModal("mostrarContenedorImpresion", "Impresi\u00f3n de poliza", 100, 100, 350, 250, url, null);
	}
	
</script>

