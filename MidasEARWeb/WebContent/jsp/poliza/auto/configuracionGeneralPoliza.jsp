<%@page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/poliza.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/validarCambios.js'/>"></script>

<script type="text/javascript">

	function onChangeCancelados(inputSelect) {
		var continuityId = '<s:property value="biCotizacion.continuity.id" />';
		switch (inputSelect) {
		case '1':	
			cargarListadoIncisos(continuityId,0);		
			break;
		case '2':
			cargarListadoIncisos(continuityId,1);
			break;
		}
	}
	
</script>

<s:form id="polizaForm" >
	<s:hidden name="polizaDTO.idToPoliza" id="idToPoliza"/>
	<s:hidden name="polizaDTO.numeroPoliza" id="numeroPoliza"/>
	<s:hidden name="biCotizacion.continuity.numero" id="numeroCotizacion"/>
	<s:hidden name="validoEn" id="validoEn"/>
	<s:hidden name="recordFrom" id="recordFrom"></s:hidden>
	<s:hidden name="validoEnMillis" id="validoEnMillis"></s:hidden>
	<s:hidden name="recordFromMillis" id="recordFromMillis"></s:hidden>
	<s:hidden name="claveTipoEndoso" id="claveTipoEndoso"></s:hidden>
	<s:hidden name="tipoRegreso" id="tipoRegreso"></s:hidden>
	<s:hidden name="esSituacionActual" id="esSituacionActual"></s:hidden>
	<s:hidden name="aplicaFlotillas"id="aplicaFlotillas"/>
	<s:hidden name="polizaDTO.numeroPolizaFormateada" id="numPolizaFormateada"></s:hidden>
	<table width="98%">
		<tr>
			<td align="left">
				<s:action name="getInfoAgente" var="agenteInfo" namespace="/componente/agente" ignoreContextParams="true" executeResult="true" >
					<s:param name="idTcAgenteCompName">biCotizacion.value.solicitud.codigoAgente</s:param>
					<s:param name="nombreAgenteCompName">agente.nombre</s:param>
					<s:param name="permiteBuscar">false</s:param>
				</s:action>	
			</td>
			
		</tr>
		<tr>
			<td width="50%">
				<s:include value="/jsp/poliza/auto/datosNegocioCotizacion.jsp"></s:include>
			</td>
		</tr>

	</table>
	<table id="agregar" style="padding: 0px; margin: 0px; border: none;"  width="98%" class="fixTabla">
		<tr>
			<td valign="top" width="50%">
				<br/>
				<br/>
					<s:include value="/jsp/poliza/auto/detallePoliza.jsp"></s:include>	   	  										
			</td>
			<td valign="top" colspan="3">
				<table style=" width:100%; padding: 5px; margin: 5px;" border="0">
					<tr>
						<td>
							<div class="titulo">
								<s:text name="midas.emision.nopoliza" />
								<b><s:property value="polizaDTO.numeroPolizaFormateada"/></b>	
							</div>
						</td>
					</tr>
					<tr >
						<td valign="top" align="right">
							<div id="cargaResumenTotales"></div>
							<div id="resumenTotalesGrid"></div>
						</td>
					</tr>
				</table>			
			</td>
			
		</tr>
		<tr>
			<m:tienePermiso nombre="FN_M2_Emision_Emision_Solicitar_Endoso">
				<td>
					<s:if test="!polizaDTO.esServicioPublico">
						<div id="realizaEndoso" class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="solicitarEndoso(<s:property value="polizaDTO.idToPoliza"/>,'<s:property value="polizaDTO.numeroPolizaFormateada"/>');">	
								<s:text name="midas.poliza.realizaEndoso"/>	
							</a>
				      	</div>
				    </s:if>
				    <s:elseif test="!usuarioExterno">
				    	<div id="realizaEndoso" class="btn_back w140" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="solicitarEndoso(<s:property value="polizaDTO.idToPoliza"/>,'<s:property value="polizaDTO.numeroPolizaFormateada"/>');">	
								<s:text name="midas.poliza.realizaEndoso"/>	
							</a>
				      	</div>
				    </s:elseif>
				</td>
			</m:tienePermiso>
			<td>
				<div id="imprimePoliza" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);" onclick="imprimePoliza(<s:property value="ultimoEndosoRecordFromDateTimeStr"/>);">	
						<s:text name="midas.poliza.impresion"/>	
					</a>
		      	</div>
			</td>
			<td>
				<div id="enviaEmail" class="btn_back w140" style="display: inline; float: right;">
					<a href="javascript: void(0);"
						onclick="ventanaCorreo(<s:property value="id"/>,2);"
						class="icon_email"> <s:text name="midas.cotizacion.enviarmail" />
					</a>
				</div>
			</td>
			<td>
				<div id="enviaEmailMasivo" class="btn_back w140" style="display: inline; float: right;">
					<s:url namespace='/impresiones/poliza' action='mostrarEnviarEmailPorInciso' var="mostrarEnviarEmailPorIncisoUrl">
						<s:param name="enviarEmailPorIncisoParameters.idToPoliza" value="id"/>
					</s:url>					
					<a href="javascript: void(0);"						
						onclick="mostrarVentanaModal('mostrarEnviarEmailPorInciso', 'Envio de Email Masivo', null, null, 675, 300, '<s:property value="#mostrarEnviarEmailPorIncisoUrl"/>', null);"
						class="icon_email"> Enviar Email Masivo
					</a>
				</div>
			</td>
		</tr>

	</table>

	<table width="98%" style="padding: 0px; margin: 0px; border: none;" class="fixTabla">
		<tr>
			<td><s:include value="/jsp/poliza/auto/datosContratante.jsp"></s:include></td> 	
		</tr>
	</table>
	
	<s:if test="mostrarCancelados == 1">
		<div id="divRadioCancelados" style="height:50px; width:380px;" class="agregar">
			<s:radio name="radioCancelados"
						theme="simple"
						list="#{'1':'Incisos Vigentes <br/>','2':'Incisos Cancelados <br/>'}"
						onclick="onChangeCancelados(this.value);"
						id="radioCancelados" cssStyle="agregar" />
		</div>
	</s:if>	

</s:form>


<div id ="listadoDeIncisos" style="height: 400px; float:both;" class="agregar">				
	<%-- <s:action name="mostrarIncisos" namespace="/poliza/inciso" ignoreContextParams="true" executeResult="true" >
		<s:param name="cotizacionId"><s:property value="biCotizacion.continuity.id"/></s:param>
		<s:param name="validoEn"><s:property value="validoEn"/></s:param>
		<s:param name="recordFrom"><s:property value="recordFrom"/></s:param>
		<s:param name="mostrarCancelados">0</s:param>
	</s:action>	--%>
</div>

<s:if test="polizaDTO.cotizacionDTO.incisoCotizacionDTOs.size() == 0">
	<script type="text/javascript">
	 	jQuery('div [id=terminarCotizacion]').css('display','none');
	</script>
</s:if>

<div class="btn_back w110">
	<a href="javascript: regresarListadoPoliza();" class="icon_regresar" onclick="">
		<s:text name="midas.boton.regresar"/>
	</a>
</div>

<script type="text/javascript">
	var continuityId = '<s:property value="biCotizacion.continuity.id" />';
	jQuery(document).ready(function(){
		obtenerResumenTotalesGrid();
		cargarListadoIncisos(continuityId,0);
	});
</script>