<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="80" sort="int">No.Pag</column>
		<column id="" type="ro" width="20" sort="str" hidden="true">id del Pagare</column>
		<column id="" type="ro" width="90" sort="str"><s:text name="midas.prestamosAnticipos.fechaCargo"/></column>
		<column id="" type="ro" width="70" sort="str"><s:text name="midas.prestamosAnticipos.capitalInicial"/></column>
		<column id="" type="ro" width="70" sort="str">Interes Ordinario</column>
		<column id="" type="ro" width="70" sort="str">Interes Moratorio</column>
		<column id="" type="ro" width="70" sort="str"><s:text name="midas.prestamosAnticipos.abonoCapital"/></column>
		<column id="" type="ro" width="70" sort="str">IVA</column>
		<column id="" type="ro" width="70" sort="str"><s:text name="midas.prestamosAnticipos.importePago"/></column>
		<column id="" type="ro" width="70" sort="str"><s:text name="midas.prestamosAnticipos.capitalFinal"/></column>
		<column id="" type="ro" width="70" sort="str"><s:text name="midas.prestamosAnticipos.abonoXPagare"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name="midas.prestamosAnticipos.estatusPagare"/></column>
		<column id="" type="ch" width="50" sort="str">Actvar</column>
		<column id="accionVer" type="img" width="60" sort="na" align="center">Acciones</column>
		<column id="" type="ro" width="50" sort="str" hidden="true">no quitar..</column>
	</head>
	<s:iterator value="listaPagares" var="gridPagares" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${gridPagares.id}]]></cell>
			<cell><![CDATA[${gridPagares.fechaCargoString}]]></cell>
			<cell><![CDATA[${gridPagares.capitalInicial}]]></cell>
			<cell><![CDATA[${gridPagares.interesOrdinario}]]></cell>	
			<cell><![CDATA[${gridPagares.interesMoratorio}]]></cell>	
			<cell><![CDATA[${gridPagares.abonoCapital}]]></cell>	
			<cell><![CDATA[${gridPagares.iva}]]></cell>	
			<cell><![CDATA[${gridPagares.importePago}]]></cell>
			<cell><![CDATA[${gridPagares.capitalfinal}]]></cell>	
			<cell><![CDATA[${gridPagares.abonoPorPagare}]]></cell>	
			<cell><![CDATA[${gridPagares.estatus.valor}]]></cell>
			<s:if test="estatus.valor == \"PENDIENTE\" || estatus.valor == \"CANCELADO\"">
				<cell>0</cell>					
			</s:if>
			<s:if test="estatus.valor == \"ACTIVO\" || estatus.valor == \"VENCIDO\" || estatus.valor == \"PAGADO\"">
				<cell>1</cell>
			</s:if>
			<cell><s:url value="/img/b_printer.gif"/>^<s:text name="midas.boton.imprimir"/>^javascript:imprimirPagare(${gridPagares.id})^_self</cell>
			<cell></cell>					
		</row>
	</s:iterator>
</rows>