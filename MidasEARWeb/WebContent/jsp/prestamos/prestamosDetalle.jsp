<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
cargaPagaresGrid();
var fechaHoy = fechaActual();
jQuery("#fecAltaMovimiento").val(fechaHoy);
controlDeBotones();

</script>

<s:if test="tipoAccion == 1">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />	
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.prestamosAnticipos.tituloAlta')}"/>
</s:if>
<s:if test="tipoAccion == 2">
	<s:set id="readOnly" value="true" />	
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.prestamosAnticipos.tituloConsultar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');	
		pagaresGrid.attachEvent("onXLE", function(){			
				pagaresDeshabilitados();							
		});	
		muestraDocumentosADigitalizar();
		jQuery(".btn_digitalizarDoc").css("display","none");	
	</script>
</s:if>
<s:if test="tipoAccion == 3">
	<s:set id="readOnly" value="true" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="false" />
	<s:set id="required" value="0"/>
	<s:set id="titulo" value="%{getText('midas.prestamosAnticipos.tituloEliminar')}"/>
	<script type="text/javascript">
		jQRemoveClass('jQrequired');		
	</script>
</s:if>
<s:if test="tipoAccion == 4">
	<s:set id="readOnly" value="false" />
	<s:set id="idReadOnly" value="true" />
	<s:set id="display" value="true" />
	<s:set id="required" value="1"/>
	<s:set id="titulo" value="%{getText('midas.prestamosAnticipos.tituloEditar')}"/>
	<script type="text/javascript">
		
	jQuery(document).ready(function(){
		pagaresGrid.attachEvent("onXLE", function(){
			generaEstructuraFortimax();
			var estatusMovimiento = jQuery("#estatusMovimiento :selected").text();
	 		if(estatusMovimiento == "PENDIENTE"){
				pagaresDeshabilitados();
			}			
		});			
		muestraDocumentosADigitalizar();		
	});		
	</script>
</s:if>

<div class="titulo w400"><s:text name="#titulo"/></div>
<s:form action="agregar" id="prestamosForm" name="prestamosForm">
<s:hidden name="tipoAccion"/>
<s:hidden name="prestamoAnticipo.id"  id="prestamoAnticipo_id"/>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" /></th>	
			<td>
				<s:textfield  name="prestamoAnticipo.agente.idAgente" id="txtIdAgente" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict jQrequired" onchange="buscaAgentePorIdAgente();" readonly="#readOnly"></s:textfield>
				<s:textfield  id="id" name="prestamoAnticipo.agente.id" readonly="true" onchange="buscaAgentePorId();" style="display:none;"></s:textfield>
			</td>
			<td>
				<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="buscaAgentePorIdAgente();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
				</s:if>		
			</td>
		</tr>
		<tr>			
			
			<td><s:text name="midas.prestamosAnticipos.estatus" /></td>
			<td><s:textfield id="estatusAgente" name="prestamoAnticipo.agente.tipoSituacion.valor" cssClass="cajaTextoM2 w160" readonly="true"></s:textfield></td>
			<td><s:text name="midas.prestamosAnticipos.centroOperacion" /></td>
			<td><s:textfield id="descripcionCentroOperacion" name="prestamoAnticipo.agente.promotoria.ejecutivo.gerencia.centroOperacion.descripcion" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>			
			<td><s:text name="midas.prestamosAnticipos.gerencia" /></td>
			<td><s:textfield id="descripcionGerencia" name="prestamoAnticipo.agente.promotoria.ejecutivo.gerencia.descripcion" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.promotoria" /></td>
			<td><s:textfield id="descripcionPromotoria" name="prestamoAnticipo.agente.promotoria.descripcion" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>
			<td><s:text name="midas.prestamosAnticipos.nombreAgente" /></td>
			<td width="180px"><s:textfield id="nombreAgente" name="prestamoAnticipo.agente.persona.nombreCompleto"  cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.rfc" /></td>
			<td width="150px"><s:textfield id="rfcAgente" name="prestamoAnticipo.agente.persona.rfc" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.ejecutivo" /></td>
			<td width="150px"><s:textfield id="ejecutivo" name="prestamoAnticipo.agente.promotoria.ejecutivo.personaResponsable.nombreCompleto" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>
			<td><s:text name="midas.prestamosAnticipos.tipoAgente" /></td>
			<td><s:textfield id="tipoAgente" name="prestamoAnticipo.agente.idTipoAgente"  cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.numeroFianza" /></td>
			<td><s:textfield id="numeroFianza" name="prestamoAnticipo.agente.numeroFianza" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.venceFianza" /></td>
			<td><s:textfield id="venceFianza" name="prestamoAnticipo.agente.fechaVencimientoFianza" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
		<tr>
<%-- 			<td><s:text name="midas.prestamosAnticipos.domicilio" /></td> --%>
<!-- 			<td id="domicilioCompleto"></td> -->
			<td><s:text name="midas.prestamosAnticipos.numeroCedula" /></td>
			<td><s:textfield id="numeroCedula" name="prestamoAnticipo.agente.numeroCedula" cssClass="cajaTextoM2 w160" readonly="true"/></td>
			<td><s:text name="midas.prestamosAnticipos.venceCedula" /></td>
			<td><s:textfield id="venceCedula" name="prestamoAnticipo.agente.fechaVencimientoCedula" cssClass="cajaTextoM2 w160" readonly="true"/></td>
		</tr>	
	</table>
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<th><s:text name="midas.prestamosAnticipos.tipoMovimiento" /></th>	
			<td><s:select  name="prestamoAnticipo.tipoMovimiento.id" id="tipoMovimiento" cssClass="cajaTextoM2 jQrequired" list="listaTipoMovimiento" listKey="id" listValue="valor" headerKey="" headerValue="Seleccione.." disabled="#readOnly"/></td>
			<th colspan="2" align="right"><s:text name="midas.prestamosAnticipos.estatusMovimiento" /></th>	
			<td><s:select  name="prestamoAnticipo.estatus.id" id="estatusMovimiento"  cssClass="cajaTextoM2" list="listaEstatusMovimiento" listKey="id" listValue="valor" disabled="true"/></td>
			<th colspan="2" align="right"><s:text name="midas.prestamosAnticipos.fecAltaMovimiento" /></th>	
			<s:if test="tipoAccion == 1 ">
			<td><s:textfield  name="prestamoAnticipo.fechaAltaMovimiento" id="fecAltaMovimiento" cssClass="cajaTextoM2" readonly="true"/></td>
			</s:if>
			<s:else>
			<td><s:textfield  name="prestamoAnticipo.fechaAltaMovimiento" cssClass="cajaTextoM2" readonly="true"/></td>
			</s:else>
			
		</tr>
		<tr>
			<th><s:text name="midas.prestamosAnticipos.importeOtorgado" /></th>	
			<td><s:textfield name="prestamoAnticipo.importeOtorgado" id="importeOtorgado" cssClass="cajaTextoM2 w70 jQfloat jQrestrict jQrequired" readonly="#readOnly"></s:textfield></td>
			<th><s:text name="midas.prestamosAnticipos.plazo" /></th>	
			<td><s:select  name="prestamoAnticipo.plazo.id" id="plazo" cssClass="cajaTextoM2 jQrequired" list="listaPlazo" listKey="id" listValue="valor" headerValue="Seleccione.." headerKey="" disabled="#readOnly"/></td>
			<th><s:text name="midas.prestamosAnticipos.fecIniCalculo" /></th>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<td><sj:datepicker name="prestamoAnticipo.fechaInicioCalculo" id="fecIniCalculo" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100 jQrequired" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this);"
  				   onblur="esFechaValida(this);" onchange="noPermiteFechasPasadas(this);"></sj:datepicker></td>
  			</s:if>
  			<s:else>	    
  				<td><s:textfield name="prestamoAnticipo.fechaInicioCalculo" cssClass="cajaTextoM2 w70 " readonly="true"></s:textfield></td>
  			</s:else>	   
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroPagos" /></th>	 
			<td><s:textfield name="prestamoAnticipo.numeroPagos" id="numeroPagos" cssClass="cajaTextoM2 w70 jQnumeric jQrestrict jQrequired" readonly="#readOnly"></s:textfield></td>
		</tr>
		<tr>
			<th><s:text name="midas.prestamosAnticipos.pcteInteresOrdinario" /></th>	
			<td><s:textfield name="prestamoAnticipo.pcteInteresOrdinario" id="pcteInteresOrdinario" cssClass="cajaTextoM2 w70 jQfloat" readonly="#readOnly"></s:textfield></td>
			<th><s:text name="midas.prestamosAnticipos.plazoInteres" /></th>	
			<td><s:select  name="prestamoAnticipo.plazoInteres.id" id="plazoInteres" cssClass="cajaTextoM2" list="listaPlazoInteres" listKey="id" listValue="valor" headerValue="Seleccione.." headerKey="" disabled="#readOnly"/></td>
			<th><s:text name="midas.prestamosAnticipos.importeInteresOrdinario" /></th>	
			<td><s:textfield name="prestamoAnticipo.importeInteresOrdinario" id="importeInteresOrdinario" cssClass="cajaTextoM2 w70 " readonly="true"></s:textfield></td>
			<th><s:text name="midas.prestamosAnticipos.pcteInteresMoratorio" /></th>	
			<td><s:textfield name="prestamoAnticipo.pcteInteresMoratorio" id="interesMoratorio" cssClass="cajaTextoM2 w70  jQfloat  " readonly="#readOnly"></s:textfield></td>
		</tr>
		<tr>
			<th><s:text name="midas.prestamosAnticipos.importePago" /></th>	
			<td><s:textfield name="prestamoAnticipo.importePago" id="importePago" cssClass="cajaTextoM2 w70 jQrequired" readonly="true"></s:textfield></td>
		</tr>
		<tr>
			<th><s:text name="midas.prestamosAnticipos.observaciones" /></th>	
			<td colspan="5"><s:textfield name="prestamoAnticipo.observaciones" id="observaciones" cssClass="cajaTextoM2 w500 h80" readonly="#readOnly"></s:textfield></td>
<!-- 			<td> -->
<!-- 			<div id='cargarCheck'></div>			 -->
<!-- 			</td> -->
<!-- 			<td colspan="3"> -->
<!-- 				<table class="contenedorFormas no-border js_habilita" style="display:none;"> -->
<!-- 					<tr> -->
<%-- 						<td colspan="2"><div class="titulo"><s:text name="midas.prestamosAnticipos.anexos"/></div></td> --%>
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td id="muestraDocumentosADigitalizar"></td> -->
<!-- 					</tr> -->
<%-- 					<s:iterator value="listaDocumentosFortimax" status="stat" var="doc" id="doc"> --%>
<!-- 					<tr> -->
<!-- 						<td width="5%"> -->
<%-- 							<s:if test="#doc.existeDocumento==1"> --%>
<!-- 							 	<input type="checkbox" checked="checked" disabled="disabled"/> -->
<%-- 							</s:if>					 --%>
<%-- 							<s:else> --%>
<!-- 								<input type="checkbox" disabled="disabled"/> -->
<%-- 							</s:else> --%>
<%-- 							<s:hidden name="listaDocumentosFortimax[%{#stat.index}].id"/> --%>
<%-- 							<s:hidden name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumento"/>				 --%>
<!-- 						</td>			 -->
<!-- 						<td width="95%"> -->
<%-- 							<s:text name="listaDocumentosFortimax[%{#stat.index}].catalogoDocumentoFortimax.nombreDocumentoFortimax"/> --%>
<!-- 						</td>				 -->
<!-- 					</tr> -->
<%-- 					</s:iterator> --%>
				
<!-- 					<tr> -->
<!-- 						<td colspan="2"> -->
<%-- 						<s:if test="tipoAccion == 1 || tipoAccion == 4 "> --%>
<!-- 							<div class="btn_back w150"> -->
<!-- 								<a href="javascript: void(0);" class="" -->
<!-- 									onclick="generarLigaIfimaxPrestamo();"> -->
<%-- 									<s:text name="midas.prestamosAnticipos.btnDigitalizarDocumentos"/> --%>
<!-- 								</a> -->
<!-- 							</div>	 -->
<!-- 							<div class="btn_back w180"> -->
<!-- 								<a href="javascript: auditarDocumentosPrestamo();" class="icon_confirmAll" -->
<!-- 									onclick=""> -->
<%-- 									<s:text name="Auditar"/> --%>
<!-- 								</a> -->
<!-- 							</div>		 -->
<%-- 						</s:if>	 --%>
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
<!-- 			</td> -->
		<td colspan="2" align="right" valign="bottom">
<!-- 				<div class="btn_back w250" id="btnRecCalcularPagaresll"> -->
<!-- 					<a href="javascript: void(0);" class="" -->
<!-- 						onclick="recalculoAbonoXPagare();"> -->
<%-- 						<s:text name="prueba recalcular bono x pagare"/> --%>
<!-- 					</a> -->
<!-- 				</div>	 -->
				<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110" style="display:none" id="btnCalcularPagares">
					<a href="javascript: void(0);" class=""
						onclick="calcularPagares();">
						<s:text name="midas.prestamosAnticipos.btnCalcularPagares"/>
					</a>
				</div>	
				</s:if>
			</td>
		</tr>		
	</table>
	
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td colspan="6"><div class="titulo"><s:text name="midas.prestamosAnticipos.subtituloDetallePagares"/></div></td>
		</tr>
		<tr>	
			<td colspan="6">
				<div id="pagaresGrid" height="200px" width="880px"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
			</td>
		</tr>
		<tr>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110" style="display:none;"  id="btnSolicitarCheque">
					<a href="javascript: void(0);" class=""
						onclick="solicitarChequeDeMovimiento();">
						<s:text name="Solictar Cheque"/>
					</a>
				</div>	
			</s:if>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110" style="display:none;" id="btnAutorizar">
					<a href="javascript: void(0);" class=""
						onclick="autorizarMovimiento();">
						<s:text name="Autorizar"/>
					</a>
				</div>	
			</s:if>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w170" style="display:none;" id="btnTablaAmortizacion">
					<a href="javascript: void(0);" class=""
						onclick="mostrTablaAmortizacion();">
						<s:text name="midas.prestamosAnticipos.btnTablaAmortizacion"/>
					</a>
				</div>	
			</s:if>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110" style="display:none;" id="btnPagarePrincipal">
					<a href="javascript: void(0);" class=""
						onclick="imprimirPagarePrincipal();">
						<s:text name="midas.prestamosAnticipos.btnPagarePrincipal"/>
					</a>
				</div>	
			</s:if>
			</td>
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110" style="display:none;" id="btnGuardar">
					<a href="javascript: void(0);" class=""
						onclick="guardarConfigPrestamoAnticipo();">
						<s:text name="midas.boton.guardar"/>
					</a>
				</div>
			</s:if>		
			</td>			
			<td>
			<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div class="btn_back w110" style="display:none;" id="btnCancelar">
					<a href="javascript: void(0);" class=""
						onclick="cancelarMovimiento();">
						<s:text name="midas.boton.cancelar"/>
					</a>
				</div>	
			</s:if>
			</td>
		</tr>
		<tr>
			<td>
				<s:if test="tipoAccion == 1 || tipoAccion == 4 ">
				<div align="right" class="w80 inline " >
					<div class="btn_back w80">
						<a href="javascript: void(0);"
							onclick="auditarDocumentosPrestamosDigitalizados();">
							<s:text name="Auditar"/>
						</a>
					</div>		
	 			</div>	
	 			</s:if>
	 		</td>
	 	</tr>		
		<tr>
			<td colspan="8" height="150px"><div style="overflow:auto;height:150px;" id='muestraDocumentosADigitalizar'></div></td>
		</tr>
	</table>
	
	<table width="98%" class="contenedorFormas" align="center">
		<tr>
			<td colspan="4"><div class="titulo"><s:text name="midas.prestamosAnticipos.subtituloDatosAval"/></div></td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.nombres" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.nombreAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.colonia" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.coloniaAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>
		</tr>	
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.calleNumero" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.calleNumeroAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.cp" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.codigoPostalAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>
		</tr>	
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.municipio" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.municipioAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.telefonos" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.telefonoAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.aval.entidadFederativa" /></th>	
			<td colspan="3"><s:textfield name="prestamoAnticipo.estadoAval" id="" cssClass="cajaTextoM2 w210" readonly="#readOnly"></s:textfield></td>			
		</tr>	
	</table>	
	
	<div align="right" class="w910 inline" >
		<div class="btn_back w100">
			<a href="javascript: void(0);" class="icon_regresar"
				onclick="javascript: salirPrestamo();">
				<s:text name="midas.boton.salir"/>
			</a>
		</div>		
	</div>	
	
</s:form>

