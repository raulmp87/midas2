<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<s:hidden name="tipoAccion"/>
<s:hidden name="prestamoAnticipo.id"/>
<script type="text/javascript">
	cargaPagaresGridTablaAmortizacion();
</script>

<s:text name="midas.prestamosAnticipos.subtituloDetallePagares"/>

<div id="pagareTablaAmortizacion" height="200px" width="880px"></div>
