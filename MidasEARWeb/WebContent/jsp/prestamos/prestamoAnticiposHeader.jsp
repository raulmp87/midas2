<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script src="<s:url value='/js/midas2/catalogos/prestamosAnticiposCargosAgentes/prestamosAnticipos.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/docFortimax.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
 
<script type="text/javascript">
	var pathDetalle='/MidasWeb/prestamos/prestamosAnticipos/verDetalle.action';
	var listarFiltradoConfigPrestamosPath = '<s:url action="listarFiltrado" namespace="/prestamos/prestamosAnticipos"/>';
</script>
