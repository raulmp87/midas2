<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/prestamos/prestamoAnticiposHeader.jsp"></s:include>

<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro="/MidasWeb/prestamos/prestamosAnticipos/listarFiltrado.action?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"prestamosGrid", null,idField,'prestamosModal');
 });
	
</script>
<s:form action="listarFiltrado" id="prestamosForm" name="prestamosForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.prestamosAnticipos.tituloCatalogo"/>
			</td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" /></th>	
			<td><s:textfield name="filtrar.agente.id" id="" cssClass="cajaTextoM2 w90 jQnumeric jQrestrict"></s:textfield></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.tipoMovimiento" /></th>	
			<td><s:select  name="filtrar.tipoMovimiento.id" id="" cssClass="cajaTextoM2 w150" 
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaTipoMovimiento" listKey="id" listValue="valor" disabled="#readOnly"/></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.estatusMovimiento" /></th>	
			<td><s:select  name="filtrar.estatus.id" id="" cssClass="cajaTextoM2 w130" 
			headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="listaEstatusMovimiento" listKey="id" listValue="valor" disabled="#readOnly" disabled="#readOnly"/></td>			
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.nombreAgente"/></th>	
			<td><s:textfield name="filtrar.agente.persona.nombreCompleto" cssClass="cajaTextoM2 w170"/></td>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.fecIniCalculo" /></th>
			<td><sj:datepicker name="filtrar.fechaInicioCalculo" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></td>
 			<th>&nbsp;<s:text name="midas.prestamosAnticipos.plazo" /></th>	
			<td><s:select  name="filtrar.plazo.id" id="" cssClass="cajaTextoM2 w130" list="listaPlazo" 
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
					listKey="id" listValue="valor" disabled="#readOnly"/></td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.fecAltaMovimiento" /></th>	
			<td><sj:datepicker name="filtrar.fechaDeAltaMovimiento" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></td>
 			<th>&nbsp;<s:text name="midas.prestamosAnticipos.fecFinMovimiento" /></th>		   
 			<td><sj:datepicker name="filtrar.fechaDeFinMovimiento" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></td>
			
			<td colspan="3" align="right">				
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: listarFiltradoGenerico(listarFiltradoConfigPrestamosPath, 'prestamosGrid', document.prestamosForm,'${idField}','afianzadoraModal');">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="prestamosGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>
<s:if test="tipoAccion!=\"consulta\"">
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_guardar"
			onclick="javascript:operacionGenerica('/MidasWeb/prestamos/prestamosAnticipos/verDetalle.action',1);">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>	
</div>
</s:if>