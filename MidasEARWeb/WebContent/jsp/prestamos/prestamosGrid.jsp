<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" width="120" sort="int"><s:text name="midas.prestamosAnticipos.idMovimiento"/></column>
		<column id="" type="ro" width="150" sort="int"><s:text name="midas.prestamosAnticipos.numeroAgente"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.prestamosAnticipos.nombreAgente"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.prestamosAnticipos.tipoMovimiento"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.prestamosAnticipos.estatusMovimiento"/></column>
		<column id="" type="ro" width="150" sort="str"><s:text name="midas.prestamosAnticipos.fecAltaMovimiento"/></column>
		<column id="" type="ro" width="120" sort="int"><s:text name="midas.prestamosAnticipos.importeOtorgado"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name="midas.prestamosAnticipos.plazo"/></column>
		<column id="" type="ro" width="120" sort="str"><s:text name="midas.prestamosAnticipos.importePago"/></column>
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionBorrar" type="img" width="30" sort="na"/>
	</head>
	
	<s:iterator value="listaconfigPrestamoAnticipoView" var="prestamoAnticipoView" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${prestamoAnticipoView.id}]]></cell>
			<cell><![CDATA[${prestamoAnticipoView.idAgente}]]></cell>
			<cell><![CDATA[${prestamoAnticipoView.nombreCompreto}]]></cell>
			<cell><![CDATA[${prestamoAnticipoView.tipoMovimiento}]]></cell>
			<cell><![CDATA[${prestamoAnticipoView.estatus}]]></cell>	
			<cell><![CDATA[${prestamoAnticipoView.fechaAltaMovimientoString}]]></cell>	
			<cell><![CDATA[${prestamoAnticipoView.importeOtorgado}]]></cell>	
			<cell><![CDATA[${prestamoAnticipoView.plazos}]]></cell>	
			<cell><![CDATA[${prestamoAnticipoView.importePago}]]></cell>		
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(pathDetalle, 2,{"prestamoAnticipo.id":${prestamoAnticipoView.id}})^_self</cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams(pathDetalle, 4,{"prestamoAnticipo.id":${prestamoAnticipoView.id}})^_self</cell>
			
		</row>
	</s:iterator>

</rows>