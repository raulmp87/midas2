<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">

<s:include value="/jsp/agtSaldoMovto/agtSaldoMovtoHeader.jsp"></s:include>

		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">	
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">


<s:form action="listarFiltrado" id="movimientosSaldoForm" name="movimientosSaldoForm"  cssStyle="background:white;">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" /></th>	
			<td><s:textfield name="detalleAgtSaldoView.idAgente" id="idAgente" onchange="" cssClass="cajaTextoM2 w90" disabled="true"  ></s:textfield></td>
			<td><s:textfield name="detalleAgtSaldoView.nombreCompleto" id="nombreCompleto" cssClass="cajaTextoM2 w170" disabled="true" />
<%-- 			<script type="text/javascript"> --%>
<!-- 				onChangeIdAgt_SaldoMov(); -->
<%-- 			</script> --%>
			</td> 
		</tr>
		<tr>
			<th>&nbsp;<s:text name="Status" /></th>	
			<td><s:textfield name="detalleAgtSaldoView.situacionAgente" id="tipoSituacion" cssClass="cajaTextoM2 w90" disabled="true" ></s:textfield></td>
			<td><s:textfield name="detalleAgtSaldoView.motivoEstatus" id="motivoEstatusAgente" cssClass="cajaTextoM2 w170" disabled="true" /></td> 
			<th>&nbsp;<s:text name="Fecha Status" /></th>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="fechaEstatus" name="detalleAgtSaldoView.fechaSituacion" disabled="true" />
			</td>	
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.reporteAgente.topAgente.anio" /></th>	
			<td><s:textfield name="detalleAgtSaldoView.anioMes" id="" cssClass="cajaTextoM2 w90" disabled="true"></s:textfield></td>
			<td>
				<s:text name="midas.agtSaldos.moneda"/>
			</td>
			<td>
				<s:select  name="detalleAgtSaldoView.idMoneda" id="" cssClass="cajaTextoM2 w150"  
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="catalogoMoneda" listKey="clave" listValue="valor" disabled="true"/>				
			</td>
		</tr>
	</table>
	<br>	
	<table width="880px" id="filtrosM2">
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes"></s:text>
			</th>
			<td>
				<s:textfield name="detalleAgtSaldoView.mes" cssClass="cajaTextoM2" disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.provision.reporte.periodoInicial" ></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.fechaInicioPeriodo" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.provision.reporte.periodoFial"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.fechaFinPeriodo" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.agtSaldos.saldoInicial"></s:text>
			</td>
			
			<td>
				<s:textfield name="detalleAgtSaldoView.saldoInicial" value="%{getText('struts.money.format',{detalleAgtSaldoView.saldoInicial})}"  cssClass="cajaTextoM2"  disabled="true"></s:textfield>
				
				 
				
				<!--<s:text name="struts.money.format">
					<s:param value="detalleAgtSaldoView.saldoInicial"/>
				</s:text> -->
				
			</td>
			<td>
				<s:text name="midas.agtSaldos.saldoFinal"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.saldoFinal" value="%{getText('struts.money.format',{detalleAgtSaldoView.saldoFinal})}" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.agtSaldos.estatus"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.situacionSaldo" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>
		</tr>
	</table>
	<br>	
	<table width="880px" id="filtrosM2">
		<tr>
			<td>
				<s:textfield name="detalleMovimiento.fehcaCorteEdocta" label="Fecha Estado Cuenta" labelposition="top" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>
			<td>
				<s:textfield name="detalleMovimiento.idConsecMovto" label="Consecutivo" labelposition="top" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>
			<td>
				<s:textfield name="detalleMovimiento.fechaMovimiento" label="Fecha Movimiento" labelposition="top" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>
			<td>
				<s:textfield name="detalleMovimiento.estatusMovimiento.valor" label="Etatus Pago" labelposition="top" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>
			<td>
				<s:textfield name="detalleMovimiento.fechaCortePagoCom" label="Fecha Pago" labelposition="top" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>	
		</tr>
	</table>
	<br>
	<div hrefmode="ajax-html" style="height: 420px; width: 880px" id="DetalleMovimientosTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
		<div id="movimientos" name="Movimiento" style="width:900px;" href="http://void">
			<table width="900px" id="filtrosM2">
				<tr>
					<td><s:text name="Concepto"></s:text></td>
					<td style="width:100px;"><s:textfield name="detalleMovimiento.descripcionCatConcepto" cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					<td style="width:30px;"><s:textfield name="detalleMovimiento.idConcepto" cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					<td colspan="2" style="width:70px;"><s:textfield name="detalleMovimiento.descripcionConcepto" cssStyle="width:350px;" cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					<s:if test="detalleMovimiento.naturalezaConcepto == \"C\"">
						<td><s:textfield name="naturalezaConcepto" value="CARGO"  cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					</s:if>
					<s:elseif test="detalleMovimiento.naturalezaConcepto == \"A\"">
						<td><s:textfield name="naturalezaConcepto" value="ABONO"  cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					</s:elseif>
					
				</tr>
			 	<tr>
					<td><s:text name="Descripción"></s:text></td>
					<td colspan="2"><s:textfield name="detalleMovimiento.descripcionMovto" cssStyle="width:250px;" cssClass="cajaTextoM2" disabled="true" ></s:textfield></td>
				</tr>
			 	<tr>
					<td><s:text name="Ramo/SubRamo"></s:text></td>
					<td><s:textfield name="detalleMovimiento.idRamoContable"  cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					<td><s:textfield name="detalleMovimiento.descripcionRamo"  cssClass="cajaTextoM2"  disabled="true" ></s:textfield></td>
					<td style="width:30px;"><s:textfield name="detalleMovimiento.idSubramoContable"  cssClass="cajaTextoM2" size="10px;"  disabled="true" ></s:textfield></td>
					<td style="width:50px;"><s:textfield name="detalleMovimiento.descripcionSubramo"  cssClass="cajaTextoM2" size="50px;"  disabled="true" ></s:textfield></td>
				</tr>
			</table>
			<br>
			<table width="900px" id="filtrosM2">
				<tr>
					<td width="700px">
						<table style="border: 0px;" width="700px" id="filtrosM2">
							<tr>
								<td><s:text name="Póliza"></s:text></td>
								<td><s:textfield name="detalleMovimiento.numPoliza"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
								<td><s:text name="Endoso"></s:text></td>
								<td><s:textfield name="detalleMovimiento.numeroEndoso"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
							</tr>
							<tr>
								<td><s:text name="Recibo"></s:text></td>
								<td><s:textfield name="detalleMovimiento.numFolioRbo"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
								<td><s:text name="Fecha Vencimiento"></s:text></td>
								<td><s:textfield
										name="detalleMovimiento.fechavencimientorec"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
							</tr>
							<tr>
								<td><s:text name="Moneda Original"></s:text></td>
								<td><s:textfield
										name="detalleMovimiento.monedaOrigen.valor"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
								<td><s:text name="Tipo Cambio"></s:text></td>
								<td><s:textfield name="detalleMovimiento.tipoCambio" value="%{getText('struts.money.format',{detalleMovimiento.tipoCambio})}"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
							</tr>
							<tr>
								<td><s:text name="Prima Neta"></s:text></td>
								<td><s:textfield name="detalleMovimiento.importePrimaNeta"
										value="%{getText('struts.money.format',{detalleMovimiento.importePrimaNeta})}"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
								<td><s:text name="Recargo Pago Fraccionado"></s:text></td>
								<td><s:textfield name="detalleMovimiento.impRcgossPagoFr"
										value="%{getText('struts.money.format',{detalleMovimiento.impRcgossPagoFr})}"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
							</tr>
							<tr>
								<td><s:text name="Participación"></s:text></td>
								<td><s:textfield
										name="detalleMovimiento.porcentajePartAgente"
										value="%{getText('struts.percent.format',{detalleMovimiento.porcentajePartAgente})}"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
								<td><s:text name="Comisión Agente"></s:text></td>
								<td><s:textfield
										name="detalleMovimiento.importeComisionAgente"
										value="%{getText('struts.money.format',{detalleMovimiento.importeComisionAgente})}"
										cssClass="cajaTextoM2" disabled="true"></s:textfield></td>
							</tr>
						</table>
				</td>
				<td width="200px">
						<s:textfield
						name="detalleMovimiento.importePrimaTotal"
						value="%{getText('struts.money.format',{detalleMovimiento.importePrimaTotal})}"
						cssClass="cajaTextoM2" label="Prima Total" labelposition="top"
						disabled="true"></s:textfield></td>
				</tr>
			</table>
		</div>
		<div id="remesa" name="Remesa" style="width:900px;" href="http://void" >
			<table width="900px" id="filtrosM2">
				<tr>
					<td width="100px"><s:text name="Agente Original"></s:text></td>
					<td>
						<s:textfield name="detalleMovimiento.agenteOrigen.idAgente" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
					</td>
				</tr>
				<tr>
					<td><s:text name="Referencia"></s:text></td>
					<td>
						<s:textfield name="detalleMovimiento.referencia" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
					</td>
				</tr>				
			</table>
			<br>
			<table width="900px" id="filtrosM2">
				<tr>
					<td><s:text name="Usuario"></s:text></td>
					<td><s:textfield name="detalleMovimiento.idUsuarioIntegeg" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td width="100px"><s:text name="Fecha Integración"></s:text></td>
					<td><s:textfield name="detalleMovimiento.fhIntegracion" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
			</table>
			<br>
			<table width="900px" id="filtrosM2">
				<tr>
					<td width="100px"><s:text name="Remesa"></s:text></td>
					<td><s:textfield name="detalleMovimiento.idRemesa" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
				<tr>
					<td><s:text name="Usuario Remesa"></s:text></td>
					<td><s:textfield name="detalleMovimiento.idUsuarioIntegeg" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
				<tr>
					<td><s:text name="Origen Remesa"></s:text></td>
					<td><s:textfield name="detalleMovimiento.descripcionOrigenRemesa" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td width="100px"><s:text name="Origen Movimiento"></s:text></td>
					<td><s:textfield name="detalleMovimiento.claveOrigenMovto" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
				<tr>
					<td><s:text name="Aplicación Origen"></s:text></td>
					<td><s:textfield name="detalleMovimiento.claveOrigenAplic" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
			</table>
		</div>
		<div id="bono" name="Bono" style="width:900px;" href="http://void">
			<table width="900px" id="filtrosM2">
				<tr>
					<td width="200px"><s:text name="Cobertura"></s:text></td>
					<td><s:textfield name="detalleMovimiento.idCobertura" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td><s:text name="Año de vigor de la Póliza"></s:text></td>
					<td><s:textfield name="detalleMovimiento.anioVigenciaPoliza" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>					
				</tr>
				<tr>
					<td><s:text name="Sistema de Administración"></s:text></td>
					<td><s:textfield name="detalleMovimiento.claveSistAdmon" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td><s:textfield name="No Aplica" value="No Aplica" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
				<tr>
					<td><s:text name="Cálculo de Dividendos"></s:text></td>
					<td><s:textfield name="detalleMovimiento.claveExpCalcDiv" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td><s:textfield name="No Aplica" value="No Aplica" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
				<tr>
					<td><s:text name="Importe Prima Dotal a Corto Plazo"></s:text></td>
					<td><s:textfield name="detalleMovimiento.importePrimaDcp" value="%{getText('struts.money.format',{detalleMovimiento.importePrimaDcp})}" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
				<tr>
					<td><s:radio list="#{'F':'Si', '':'No'}" disabled="true" label="Facultativo" name="detalleMovimiento.esFacultativo" onclick="javascript: void(0);" ></s:radio></td>
				</tr>
			</table>
			<br>
			<table width="900px" id="filtrosM2">
			Origen del Movimiento
				<tr>
					<td><s:text name="Concepto"></s:text></td>
					<td width="100px"><s:textfield name="detalleMovimiento.descripcionCatConcepto" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td width="100px"><s:textfield name="detalleMovimiento.idConcepto" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
					<td width="100px" colspan="2"><s:textfield name="detalleMovimiento.descripcionConcepto" cssStyle="width:250px;" cssClass="cajaTextoM2"  disabled="true"></s:textfield></td>
				</tr>
			</table>
		</div>
	</div>
	
</s:form>
