<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/agtSaldoMovto.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_splt.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScript.js'/>"></script>	
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxtabbar_start.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>


<script type="text/javascript">
	var pathDetalle='/MidasWeb/agtSaldoMovto/verDetalle.action';
	var listarFiltradoPath = '<s:url action="listarFiltrado" namespace="/agtSaldoMovto"/>';
	var verDetalleMovtoSaldoPath = '<s:url action="verDetalleSaldoMovimientos" namespace="/agtSaldoMovto"/>';
	var verDetalleMovimientoPath = '<s:url action="verDetalleMovimiento" namespace="/agtSaldoMovto"/>';
</script>