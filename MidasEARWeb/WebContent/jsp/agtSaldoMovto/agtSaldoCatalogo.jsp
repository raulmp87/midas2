<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/agtSaldoMovto/agtSaldoMovtoHeader.jsp"></s:include>

<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 
// 	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro="/MidasWeb/agtSaldoMovto/listarFiltrado.action";
	 var idField='<s:property value="idField"/>';
			listarFiltradoGenerico(urlFiltro,"agtSaldoGrid", null,null,null);
 });
function buscar(){
		var idAgente= jQuery("#idAgente").val();
		if(idAgente==""){
			alert("Debe seleccionar un Agente");
		}else{		
		listarFiltradoGenerico(listarFiltradoPath, 'agtSaldoGrid', document.agtSaldoForm,null,null);
		}
	}
</script>


<s:form action="listarFiltrado" id="agtSaldoForm" name="agtSaldoForm">
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="5">
				<s:text name="Saldos"/>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.agtSaldos.agente"></s:text>
			</th>
			<td>
				<s:textfield  name="agtSaldo.idAgente.idAgente" id="idAgente" cssClass="cajaTextoM2 w100 jQrequired" readonly="readOnly" onchange="onChangeIdAgt_SaldoMov();"></s:textfield>
					<s:textfield id="txtId"  cssStyle="display:none" onchange="onChangeAgente_SaldoMov();"/>
					<s:textfield id="id" cssStyle="display:none"/>
			</td>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="mostrarListadoAgentes_SaldoMov();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>
			</td>
			<td colspan="3">
				<s:textfield cssClass="cajaTextoM2 w200" disabled="true" id="nombreCompleto"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.agtSaldos.estatus"/>
			</td>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="tipoSituacion" disabled="true" />
			</td>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="motivoEstatusAgente" disabled="true" />
			</td>
			<td>
				<s:text name="midas.agtSaldos.fechaEstatus"/>
			</td>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="fechaEstatus" disabled="true" />
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.agtSaldos.anio"/>
			</td>
			<td>
				<s:textfield name="agtSaldo.anio" cssClass="cajaTextoM2"/>
<%-- 				<s:select  name="agtSaldo.anio" id="" cssClass="cajaTextoM2 w150"  --%>
<!-- 				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"  -->
<!-- 				list="anios" /> -->
<!-- 				listKey="" listValue="" disabled="#readOnly" -->
			</td>
			<td>
			<!--	<s:text name="midas.agtSaldos.moneda"/>-->
			</td> 
			<td>
			<!--	<s:select  name="agtSaldo.idMoneda.id" id="" cssClass="cajaTextoM2 w150" 
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="catalogoMoneda" listKey="clave" listValue="valor" disabled="#readOnly"/>-->				
			</td>
		</tr>
		<tr>
			<td>
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript: buscar();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			</td>
		</tr>
	</table>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="agtSaldoGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
		
</s:form>
<div id="pagingArea"></div><div id="infoArea"></div>