<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>
		
		<column id="id" type="ro" hidden="true" width="50" sort="int"><s:text name="midas.cargos.gridDetalle.no_Cargo"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name="Mes"/></column>
		<column id="" type="ro" width="110" sort="str"><s:text name="Inicio Periodo"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name="Fin Periodo"/></column>
		<column id="" type="ro" width="110" sort="str"><s:text name="Saldo Inicial"/></column>
		<column id="" type="ro" width="110" sort="str"><s:text name ="Saldo Final"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name ="Estatus"/></column>
		<column id="" type="ro" width="100" sort="str"><s:text name ="Fecha Estatus"/></column>
		<column id="accionVer" type="img" width="*" sort="na" align="center">Acciones</column>	
	</head>
	<s:iterator value="listAgtSaldoView" var="gridListAgtSaldoView" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${index.count}]]></cell>
			<cell><![CDATA[${gridListAgtSaldoView.mes}]]></cell>
			<cell><![CDATA[${gridListAgtSaldoView.fechaInicioPeriodoString}]]></cell>
			<cell><![CDATA[${gridListAgtSaldoView.fechaFinPeriodoString}]]></cell>
			<cell><![CDATA[${gridListAgtSaldoView.saldoInicial}]]></cell>	
			<cell><![CDATA[${gridListAgtSaldoView.saldoFinal}]]></cell>	
			<cell><![CDATA[${gridListAgtSaldoView.situacionSaldo}]]></cell>
			<cell><![CDATA[${gridListAgtSaldoView.fechaSituacionString}]]></cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(verDetalleMovtoSaldoPath, null,{"filtroMovimientosDelSaldo.agente.idAgente":${gridListAgtSaldoView.idAgente},"filtroMovimientosDelSaldo.anioMes":${gridListAgtSaldoView.anioMes},"filtroMovimientosDelSaldo.moneda.id":${gridListAgtSaldoView.idMoneda}})^_self</cell>
		</row>
	</s:iterator>
</rows>