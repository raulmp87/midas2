<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<s:include value="/jsp/agtSaldoMovto/agtSaldoMovtoHeader.jsp"></s:include>

<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
	 var idAgente='<s:property value="filtroMovimientosDelSaldo.agente.idAgente"/>';
	 var anioMes='<s:property value="filtroMovimientosDelSaldo.anioMes"/>';
	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro="/MidasWeb/agtSaldoMovto/listarFiltradoDetalle.action?tipoAccion="+tipoAccion+"&filtroMovimientosDelSaldo.agente.idAgente="+idAgente+"&filtroMovimientosDelSaldo.anioMes="+anioMes;
	 var idField='<s:property value="idField"/>';
	listarFiltradoGenerico(urlFiltro,"movimientosDelSaldoGrid", null,idField,'movimientosDelSaldoModal');
 });
	
</script>
<s:form action="listarFiltrado" id="movimientosSaldoForm" name="movimientosSaldoForm">
	<s:hidden name="tipoAccion"></s:hidden>
	<table width="880px" id="filtrosM2">
		<tr>
			<td class="titulo" colspan="5">
				<s:text name="Movimientos del Saldo"/>
			</td>
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.prestamosAnticipos.numeroAgente" /></th>	
			<td><s:textfield name="detalleAgtSaldoView.idAgente" id="idAgente" onchange="" cssClass="cajaTextoM2 w90" disabled="true"  ></s:textfield></td>
			<td><s:textfield name="detalleAgtSaldoView.nombreCompleto" id="nombreCompleto" cssClass="cajaTextoM2 w170" disabled="true" />
<%-- 			<script type="text/javascript"> --%>
<!-- 				onChangeIdAgt_SaldoMov(); -->
<%-- 			</script> --%>
			</td> 
		</tr>
		<tr>
			<th>&nbsp;<s:text name="Status" /></th>	
			<td><s:textfield name="detalleAgtSaldoView.situacionAgente" id="tipoSituacion" cssClass="cajaTextoM2 w90" disabled="true" ></s:textfield></td>
			<td><s:textfield name="detalleAgtSaldoView.motivoEstatus" id="motivoEstatusAgente" cssClass="cajaTextoM2 w170" disabled="true" /></td> 
			<th>&nbsp;<s:text name="Fecha Status" /></th>
			<td>
				<s:textfield cssClass="cajaTextoM2" id="fechaEstatus" name="detalleAgtSaldoView.fechaSituacion" disabled="true" />
			</td>	
			<!-- <td><sj:datepicker name="filtroMovimientosDelSaldo.fechaStatus" id="" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
   				   onblur="esFechaValida(this);"></sj:datepicker></td>   -->
		</tr>
		<tr>
			<th>&nbsp;<s:text name="midas.reporteAgente.topAgente.anio" /></th>	
			<td><s:textfield name="detalleAgtSaldoView.anioMes" id="" cssClass="cajaTextoM2 w90" disabled="true"></s:textfield></td>
			<td>
				<s:text name="midas.agtSaldos.moneda"/>
			</td>
			<td>
				<s:select  name="detalleAgtSaldoView.idMoneda" id="" cssClass="cajaTextoM2 w150"  
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				list="catalogoMoneda" listKey="clave" listValue="valor" disabled="true"/>				
			</td>
		</tr>
	</table>
	<br>
	<table width="880px" id="filtrosM2">
		<tr>
			<th>
				<s:text name="midas.reporteAgente.topAgente.mes"></s:text>
			</th>
			<td>
				<s:textfield name="detalleAgtSaldoView.mes" cssClass="cajaTextoM2" disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.provision.reporte.periodoInicial" ></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.fechaInicioPeriodo" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.provision.reporte.periodoFial"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.fechaFinPeriodo" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.agtSaldos.saldoInicial"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.saldoInicial" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.agtSaldos.saldoFinal"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.saldoFinal" cssClass="cajaTextoM2"  disabled="true"></s:textfield>
			</td>
			<td>
				<s:text name="midas.agtSaldos.estatus"></s:text>
			</td>
			<td>
				<s:textfield name="detalleAgtSaldoView.situacionSaldo" cssClass="cajaTextoM2"  disabled="true" ></s:textfield>
			</td>
		</tr>
	</table>	
	<br>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="movimientosDelSaldoGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
</s:form>
<div id="pagingArea"></div>
<div id="infoArea"></div>
<div align="right" class="w880">
	<div class="btn_back w110">
		<a href="javascript: void(0);" class="icon_regresar"
			onclick="javascript:operacionGenerica('/MidasWeb/agtSaldoMovto/mostrarContenedor.action',1);">
			<s:text name="midas.boton.regresar"/>
		</a>
	</div>	
</div>