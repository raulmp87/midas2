<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>        
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>    
			<call command="setPagingSkin">
				<param>bricks</param>
			</call> 		
        </beforeInit>
        <afterInit>
        	  <call command="splitAt"><param>1</param></call>
        </afterInit>
		
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
		<column id="id" type="ro" width="50" hidden="true" sort="int">id</column>
		<column id="fehcaCorteEdocta" type="ro" width="90" sort="str">Fecha Edo Cuenta</column>
		<column id="fechaMov" type="ro" width="100" sort="str">Fecha Movimiento</column>
		<column id="numPoliza" type="ro" width="80" sort="str">Número Póliza</column>
		<column id="numRenovacion" type="ro" width="70" sort="str">Número Renov</column>
		<column id="tipoEndoso" type="ro" width="70" sort="str">Endoso</column>
		<column id="fechaVencRecibo" type="ro" width="100" sort="str">Vencimiento Recibo</column>
		<column id="importePN" type="ro" width="100" sort="str">Importe</column>
		<column id="comisionAgt" type="ro" width="100" sort="str">Comisión</column>
		<column id="descConcepto" type="ro" width="200" sort="str">Concepto</column>	
		<column id="naturalezaConcepto" type="ro" width="90" sort="str">Naturaleza</column>
		<column id="descMovto" type="ro" width="200" sort="str">Descripción</column>
		<column id="idConsecMovto" type="ro" width="90" sort="str">Consecutivo</column>
		<column id="referencia" type="ro" width="85" sort="str">Referencia</column>
	<!--	
		<column id="id" type="ro" width="50" sort="int">idEmpresa</column>
 		<column id="nombreAgente" type="ro" width="100" sort="str">nombreAgente</column>
		
		
		<column id="anioMes" type="ro" width="100" sort="str">anioMes</column>
		<column id="idConcepto" type="ro" width="100" sort="str">idConcepto</column>
		<column id="moneda" type="ro" width="100" sort="str">moneda</column>
		<column id="moneda" type="ro" width="100" sort="str">monedaOrigen</column>
		<column id="moneda" type="ro" width="100" sort="str">tipoCambio</column>
		
		<column id="moneda" type="ro" width="100" sort="str">idRamoContable</column>
		<column id="moneda" type="ro" width="100" sort="str">idSubramoContable</column>
		<column id="moneda" type="ro" width="100" sort="str">idLineaNegocio</column>
		
		<column id="moneda" type="ro" width="100" sort="str">idCentroEmisor</column>
		
		
		<column id="moneda" type="ro" width="100" sort="str">idCotizacion</column>
		<column id="moneda" type="ro" width="100" sort="str">idVersionPol</column>
		<column id="moneda" type="ro" width="100" sort="str">idCentroEmisore</column>
		
		<column id="moneda" type="ro" width="100" sort="str">numeroEndoso</column>
		<column id="moneda" type="ro" width="100" sort="str">idSolicitud</column>
		<column id="moneda" type="ro" width="100" sort="str">idVersionEndoso</column>
		
		<column id="moneda" type="ro" width="100" sort="str">centroOperacion</column>
		<column id="moneda" type="ro" width="100" sort="str">idRemesa</column>
		<column id="moneda" type="ro" width="100" sort="str">idConsecMovtoR</column>
		<column id="moneda" type="ro" width="100" sort="str">claveOrigenRemesa</column>
		<column id="moneda" type="ro" width="100" sort="str">serieFolioRbo</column>
		<column id="moneda" type="ro" width="100" sort="str">numFolioRbo</column>
		<column id="moneda" type="ro" width="100" sort="str">idRecibo</column>
		<column id="moneda" type="ro" width="100" sort="str">idVersionRbo</column>
		
		<column id="moneda" type="ro" width="100" sort="str">porcentajePartAgente</column>
		
		<column id="moneda" type="ro" width="100" sort="str">impRcgossPagoFr</column>
		
		<column id="moneda" type="ro" width="100" sort="str">claveOrigenAplic</column>
		<column id="moneda" type="ro" width="100" sort="str">claveOrigenMovto</column>
		<column id="moneda" type="ro" width="100" sort="str">fechaCortePagoCom</column>
		<column id="moneda" type="ro" width="100" sort="str">estatusMovimiento</column>
		<column id="moneda" type="ro" width="100" sort="str">idUsuarioIntegeg</column>
		<column id="moneda" type="ro" width="100" sort="str">fhIntegracion</column>
		<column id="moneda" type="ro" width="100" sort="str">fhAplicacion</column>
		
		<column id="moneda" type="ro" width="100" sort="str">tipoMovtoCobranza</column>
		<column id="moneda" type="ro" width="100" sort="str">fTransfDePv</column>
		<column id="moneda" type="ro" width="100" sort="str">idTransacDePv</column>		
		<column id="moneda" type="ro" width="100" sort="str">idTreansaccOrig</column>
		<column id="moneda" type="ro" width="100" sort="str">agenteOrigen</column>
		<column id="moneda" type="ro" width="100" sort="str">idCobertura</column>
		<column id="moneda" type="ro" width="100" sort="str">esFacultativo</column>
		<column id="moneda" type="ro" width="100" sort="str">anioVigenciaPoliza</column>
		<column id="moneda" type="ro" width="100" sort="str">claveSistAdmon</column>
		<column id="moneda" type="ro" width="100" sort="str">claveExpCalcDiv</column>
		<column id="moneda" type="ro" width="100" sort="str">importePrimaDcp</column>
		<column id="moneda" type="ro" width="100" sort="str">importePrimaTotal</column>
		<column id="moneda" type="ro" width="100" sort="str">cvetCptoAco</column>
		<column id="moneda" type="ro" width="100" sort="str">idConceptoO</column>
		<column id="moneda" type="ro" width="100" sort="str">esMasivo</column>
		<column id="moneda" type="ro" width="100" sort="str">importeBaseCalculo</column>
		<column id="moneda" type="ro" width="100" sort="str">porcentajeComisionAgente</column>
		<column id="moneda" type="ro" width="100" sort="str">numMovtoManAgente</column>
		<column id="moneda" type="ro" width="100" sort="str">importePagoAntImptos</column>
		<column id="moneda" type="ro" width="100" sort="str">porcentajeIva</column>
		<column id="moneda" type="ro" width="100" sort="str">porcentageIvaRetenido</column>
		<column id="moneda" type="ro" width="100" sort="str">porcentajeIsrRet</column>
		<column id="moneda" type="ro" width="100" sort="str">importeIva</column>
		<column id="moneda" type="ro" width="100" sort="str">importeIvaRet</column>
		<column id="moneda" type="ro" width="100" sort="str">importeIsr</column>
		<column id="moneda" type="ro" width="100" sort="str">cveSdoImpto</column>
		<column id="moneda" type="ro" width="100" sort="str">esImpuesto</column>
		<column id="moneda" type="ro" width="100" sort="str">anioMesPago</column>
	  -->	
		
<%-- 		<s:if test="tipoAccion!=\"consulta\""> --%>
<%-- 			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column> --%>
<%-- 			<column id="accionEditar" type="img" width="30" sort="na"/> --%>
<%-- 		</s:if> --%>
	</head>
	<s:iterator value="listaMovimientosDelSaldo" var="rowMovimientos" status="index">
	<row id="${index.count}">
			<cell title="Consultar"><s:url value="/img/icons/ico_editar.gif"/>^"consultar"^javascript:mostrarDetalleMovimiento(${rowMovimientos.id},${rowMovimientos.anioMes})^_self</cell>
			<cell><![CDATA[${rowMovimientos.id}]]></cell>
			<cell><![CDATA[${rowMovimientos.fehcaCorteEdoctaString}]]></cell>
			<cell><![CDATA[${rowMovimientos.fechaMovimientoString}]]></cell>
			<cell><![CDATA[${rowMovimientos.numPoliza}]]></cell>
			<cell><![CDATA[${rowMovimientos.numRenovacionPoliza}]]></cell>
			<cell><![CDATA[${rowMovimientos.numeroEndoso}]]></cell>
			<cell><![CDATA[${rowMovimientos.fechavencimientorecString}]]></cell>
			<cell><![CDATA[${rowMovimientos.importePrimaNeta}]]></cell>	
			<cell><![CDATA[${rowMovimientos.importeComisionAgente}]]></cell>	
			<cell><![CDATA[${rowMovimientos.descripcionConcepto}]]></cell>
			<cell><![CDATA[${rowMovimientos.naturalezaConcepto}]]></cell>
			<cell><![CDATA[${rowMovimientos.descripcionMovto}]]></cell>
			<cell><![CDATA[${rowMovimientos.idConsecMovto}]]></cell>
			<cell><![CDATA[${rowMovimientos.referencia}]]></cell>
		<!-- 
			<cell><![CDATA[${rowMovimientos.idEmpresa}]]></cell>
			<cell><![CDATA[${rowMovimientos.nombreAgente}]]></cell>			
			<cell><![CDATA[${rowMovimientos.anioMes}]]></cell>
			<cell><![CDATA[${rowMovimientos.idConcepto}]]></cell>
			<cell><![CDATA[${rowMovimientos.moneda}]]></cell>
			<cell><![CDATA[${rowMovimientos.monedaOrigen}]]></cell>
			<cell><![CDATA[${rowMovimientos.tipoCambio}]]></cell>			
			<cell><![CDATA[${rowMovimientos.idRamoContable}]]></cell>
			<cell><![CDATA[${rowMovimientos.idSubramoContable}]]></cell>
			<cell><![CDATA[${rowMovimientos.idLineaNegocio}]]></cell>				
			<cell><![CDATA[${rowMovimientos.idCentroEmisor}]]></cell>			
			<cell><![CDATA[${rowMovimientos.idCotizacion}]]></cell>
			<cell><![CDATA[${rowMovimientos.idVersionPol}]]></cell>
			<cell><![CDATA[${rowMovimientos.idCentroEmisore}]]></cell>			
			<cell><![CDATA[${rowMovimientos.numeroEndoso}]]></cell>
			<cell><![CDATA[${rowMovimientos.idSolicitud}]]></cell>
			<cell><![CDATA[${rowMovimientos.idVersionEndoso}]]></cell>			
			<cell><![CDATA[${rowMovimientos.centroOperacion}]]></cell>
			<cell><![CDATA[${rowMovimientos.idRemesa}]]></cell>
			<cell><![CDATA[${rowMovimientos.idConsecMovtoR}]]></cell>
			<cell><![CDATA[${rowMovimientos.claveOrigenRemesa}]]></cell>
			<cell><![CDATA[${rowMovimientos.serieFolioRbo}]]></cell>
			<cell><![CDATA[${rowMovimientos.numFolioRbo}]]></cell>
			<cell><![CDATA[${rowMovimientos.idRecibo}]]></cell>
			<cell><![CDATA[${rowMovimientos.idVersionRbo}]]></cell>			
			<cell><![CDATA[${rowMovimientos.porcentajePartAgente}]]></cell>				
			<cell><![CDATA[${rowMovimientos.impRcgossPagoFr}]]></cell>		
			<cell><![CDATA[${rowMovimientos.claveOrigenAplic}]]></cell>
			<cell><![CDATA[${rowMovimientos.claveOrigenMovto}]]></cell>
			<cell><![CDATA[${rowMovimientos.fechaCortePagoComString}]]></cell>
			<cell><![CDATA[${rowMovimientos.estatusMovimiento}]]></cell>
			<cell><![CDATA[${rowMovimientos.idUsuarioIntegeg}]]></cell>
			<cell><![CDATA[${rowMovimientos.fhIntegracionString}]]></cell>
			<cell><![CDATA[${rowMovimientos.fhAplicacionString}]]></cell>			
			<cell><![CDATA[${rowMovimientos.tipoMovtoCobranza}]]></cell>
			<cell><![CDATA[${rowMovimientos.fTransfDePvString}]]></cell>
			<cell><![CDATA[${rowMovimientos.idTransacDePv}]]></cell>
			<cell><![CDATA[${rowMovimientos.idTreansaccOrig}]]></cell>
			<cell><![CDATA[${rowMovimientos.agenteOrigen}]]></cell>
			<cell><![CDATA[${rowMovimientos.idCobertura}]]></cell>
			<cell><![CDATA[${rowMovimientos.esFacultativo}]]></cell>
			<cell><![CDATA[${rowMovimientos.anioVigenciaPoliza}]]></cell>
			<cell><![CDATA[${rowMovimientos.claveSistAdmon}]]></cell>
			<cell><![CDATA[${rowMovimientos.claveExpCalcDiv}]]></cell>
			<cell><![CDATA[${rowMovimientos.importePrimaDcp}]]></cell>
			<cell><![CDATA[${rowMovimientos.importePrimaTotal}]]></cell>
			<cell><![CDATA[${rowMovimientos.cvetCptoAco}]]></cell>
			<cell><![CDATA[${rowMovimientos.idConceptoO}]]></cell>
			<cell><![CDATA[${rowMovimientos.esMasivo}]]></cell>
			<cell><![CDATA[${rowMovimientos.importeBaseCalculo}]]></cell>
			<cell><![CDATA[${rowMovimientos.porcentajeComisionAgente}]]></cell>
			<cell><![CDATA[${rowMovimientos.numMovtoManAgente}]]></cell>
			<cell><![CDATA[${rowMovimientos.importePagoAntImptos}]]></cell>
			<cell><![CDATA[${rowMovimientos.porcentajeIva}]]></cell>
			<cell><![CDATA[${rowMovimientos.porcentageIvaRetenido}]]></cell>
			<cell><![CDATA[${rowMovimientos.porcentajeIsrRet}]]></cell>
			<cell><![CDATA[${rowMovimientos.importeIva}]]></cell>
			<cell><![CDATA[${rowMovimientos.importeIvaRet}]]></cell>
			<cell><![CDATA[${rowMovimientos.importeIsr}]]></cell>
			<cell><![CDATA[${rowMovimientos.cveSdoImpto}]]></cell>
			<cell><![CDATA[${rowMovimientos.esImpuesto}]]></cell>
			<cell><![CDATA[${rowMovimientos.anioMesPago}]]></cell>
			
			 -->	
			
			
<%-- 			<s:if test="tipoAccion!=\"consulta\""> --%>
<%-- 				<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams("/MidasWeb/fuerzaventa/suspensiones/verDetalle.action", 2,{"suspension.id":${rowSuspension.id},"idRegistro":${rowSuspension.id},"idTipoOperacion":80})^_self</cell> --%>
<%-- 				<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:operacionGenericaConParams("/MidasWeb/fuerzaventa/suspensiones/verDetalle.action", 4,{"suspension.id":${rowSuspension.id},"idRegistro":${rowSuspension.id},"idTipoOperacion":80})^_self</cell> --%>
<%-- 			</s:if>			 --%>
 		</row>
	</s:iterator>
</rows>