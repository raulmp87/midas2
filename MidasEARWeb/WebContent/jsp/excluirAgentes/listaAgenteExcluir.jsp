<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
       <column id="id" type="ro" width="50" sort="int"><s:text name="IdExcluir"/></column>
        <column id="idAgente" type="ro" width="50" sort="int"><s:text name="IdAgente"/></column>
		<column id="comision" type="ro" width="*" sort="str"><s:text name="Comision"/></column>
		<column id="bono" type="ro" width="*" sort="str"><s:text name="Bono"/></column>
		
		<s:if test="tipoAccion!=\"consulta\"">
			
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>

	</head>
	<s:iterator value="listExcluir" var="rowlistExcluir" status="index">
		<row id="${index.count}">
		<cell><![CDATA[${rowlistExcluir.id}]]></cell>
			<cell><![CDATA[${rowlistExcluir.idAgente}]]></cell>
			
			<s:if test="%{#rowlistExcluir.comision == true}">
				<cell><s:text name="midas.catalogos.centro.operacion.activo" /></cell>
			</s:if>
			<s:else>
					<cell><s:text name="midas.catalogos.centro.operacion.inactivo" /></cell>
			</s:else>
			<s:if test="%{#rowlistExcluir.bono == true}">
					<cell><s:text name="midas.catalogos.centro.operacion.activo" /></cell>
			</s:if>
			<s:else>
					<cell><s:text name="midas.catalogos.centro.operacion.inactivo" /></cell>
			</s:else>
			<s:if test="tipoAccion!=\"consulta\"">
 <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: verDetalleAgente(${index.count})^_self</cell>				
 
 <cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarAgenteExcluir(<s:property value="id"/>)^_self</cell>
			</s:if>
		</row>
	</s:iterator>
</rows>