<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/excluirAgentes/agentesheaderExcluir.jsp"></s:include>

<!-- Atributo para saber que campo es el que se actualizara despues de elegir un elemento del grid por si se ocupa en un modal -->
<s:hidden name="idExcluirAd"></s:hidden>

<script type="text/javascript">
	jQuery(function() {
		jQuery("#divAgregar").hide();
		jQuery("#dvExcluir").hide();
		
		var urlFiltro = listarFiltradoEjecutivoPath;
		var idField = '<s:property value="idField"/>';
		gridEx = listarFiltradoGenerico(urlFiltro, "excluirGrid", null,
				idField, 'ejecutivoModal');

	});
</script>
<s:form action="mostrarExcluirAgentes" id="exluirForm" name="exluirForm">
	<!-- Parametro de la forma para que sea reutilizable -->
<s:hidden name="accion"></s:hidden>

	<div id="divBuscar">
		<table width="880px" id="filtrosM2" border="0">
			<tr>
				<td class="titulo" colspan="4"><s:text name="Buscar Agentes" />
				</td>
			</tr>
			<!--Contenerdor de Agente Y Check open-->
			<tr>
				<th width="50px"><s:text name="midas.excluir.config.idAgente" /></th>
				<td width="208px"><s:textfield name="idAgenteSear"
						id="idAgenteSear" cssClass="cajaTextoM2 w150 jQnumeric jQrestrict" maxlength="9"></s:textfield></td>

				<td class="inline"><s:checkbox name="chkComisionSear"
						id="chkComisionSear" />
					<s:text name="midas.excluir.config.comision" /></td>&nbsp;
				<td></td>

				<td class="inline"><s:checkbox name="chkBonoSear"
						id="chkBonoSear" />
					<s:text name="midas.excluir.config.bono" /></td>&nbsp;
				<td></td>

				<td class="inline"><s:checkbox name="chkTodosSear"
						id="chkTodosSear" />
					<s:text name="midas.excluir.config.todosAgentes" /></td>&nbsp;
				<td></td>

			</tr>
			<!--Contenerdor de Agente Y Check close-->

			<br>
			<br>
			<tr>
				<!--Contenedor de Botones open-->
				<td colspan="4" align="right"></td>
				<td colspan="4" align="right"></td>
				<td colspan="4" align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="agregarAgente();"> <s:text name="Agregar / Nuevo" />
						</a>
					</div>
				</td>

				<td colspan="4" align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class=""
							onclick="limpiarFormBuscar();"> <s:text name="Limpiar" />
						</a>
					</div>
				</td>

				<td colspan="4" align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="btnBuscarAgente();"> <s:text name="Buscar" />
						</a>
					</div>
				</td>


			</tr>
			<!--Contenedor de Botones close-->

		</table>
	</div>

	<!--Modulo Agregar -->
	<div id="divAgregar">
		<table width="880px" id="filtrosM2" border="0">
			<tr>
				<td class="titulo" colspan="4"><s:text
						name="Agregar / Actualizar - Agentes a Excluir" /></td>
			</tr>
			<tr>

				<div id="dvExcluir">
				<!--<th width="50px"><s:text  name="id" /></th>	-->
			<td width="5px">
			<s:textfield name="id" id="id" cssClass="cajaTextoM2 w20" disabled="#readOnly" hidden="true">
			</s:textfield></td>	
				</div>
				

				<th width="50px"><s:text name="Id Agente" /></th>&nbsp;
				<td width="208px"><s:textfield name="idAgenteAdd"
						id="idAgenteAdd" cssClass="cajaTextoM2 w150 jQnumeric jQrestrict" maxlength="9"></s:textfield></td>

				<td class="inline"><s:checkbox name="chkComision" />
					<s:text name="midas.excluir.config.comision" /></td>&nbsp;
				<td></td>

				<td class="inline"><s:checkbox name="chkBono" id="chkBono"
						onclick="" cssClass="cajaTextoM2 w90"></s:checkbox>
					<s:text name="midas.excluir.config.bono" /></td>&nbsp;
				<td></td>

			</tr>
			<tr>

			</tr>
			<br>
			<br>
			<tr>
				<!--Contenedor de Botones open-->
				<td colspan="4" align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_buscar"
							onclick="buscarAgente();"> <s:text name="Buscar" />
						</a>
					</div>
				</td>

				<td colspan="4" align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class=""
							onclick="limpiarFormAgregar();"> <s:text name="Limpiar" />
						</a>
					</div>
				</td>

				<td colspan="4" align="right">
					<div class="btn_back w110">
						<a href="javascript: void(0);" class="icon_guardar"
							onclick="datosAgente();"> <s:text name="Guardar" />
						</a>
					</div>
				</td>
			</tr>
			<!--Contenedor de Botones close-->
		</table>
	</div>
	<br>
	<br>
	<div id="divCarga" style="position: absolute;"></div>
	<div id="excluirGrid" class="w880 h200" style="overflow: hidden"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</s:form>