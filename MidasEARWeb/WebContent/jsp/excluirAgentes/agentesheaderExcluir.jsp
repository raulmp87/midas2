<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script
	src="<s:url value='/js/midas2/excluirAgentes/excluirAgentes.js'/>"></script>

<script type="text/javascript">
	/*Cargar pantalla inicial*/
	var mostrarEjecutivoPath = '<s:url action="mostrarContenedor" namespace="/exclusionAgentes/agentes"/>';

	/*Carga de Grid*/
	var listarFiltradoEjecutivoPath = '<s:url action="listarFiltrado" namespace="/exclusionAgentes/agentes"/>';
		
</script>