<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="numeroPoliza" type="ro" width="*"><s:text name="midas.emision.autos.consulta.poliza" /></column>
		<column id="numeroInciso" type="ro" width="40"><s:text name="midas.emision.autos.consulta.inciso" /></column>
		<column id="numeroSerie" type="ro" width="*"><s:text name="midas.emision.autos.consulta.serie" /></column>
					
		<column id="version" type="ro" width="*"><s:text name="midas.emision.autos.consulta.version" /></column>
		<column id="version" type="img" width="25"></column>
		
		<column id="marca" type="ro" width="*"><s:text name="midas.emision.autos.consulta.marca" /></column>
		<column id="marca" type="img" width="25"></column>
		
		<column id="anos" type="ro" width="*"><s:text name="midas.emision.autos.consulta.modelo" /></column>
		<column id="anos" type="img" width="25"></column>
		
		<column id="puertas" type="ro" width="*"><s:text name="midas.emision.autos.consulta.puertas" /></column>
		<column id="puertas" type="img" width="25"></column>
		
		<column id="cilindros" type="ro" width="*"><s:text name="midas.emision.autos.consulta.cilindros" /></column>
		<column id="cilindros" type="img" width="25"></column>
		
		<column id="combustible" type="ro" width="*"><s:text name="midas.emision.autos.consulta.combustible" /></column>
		<column id="combustible" type="img" width="25"></column>
		
		<column id="interiores" type="ro" width="*"><s:text name="midas.emision.autos.consulta.interiores" /></column>
		<column id="interiores" type="img" width="25"></column>
		
		<column id="pasajeros" type="ro" width="*"><s:text name="midas.emision.autos.consulta.pasajeros" /></column>
		<column id="pasajeros" type="img" width="25"></column>
		
		<column id="estatus" type="ro" width="*"><s:text name="midas.emision.autos.consulta.estatus" /></column>
		<column id="accion" type="img" width="20"></column>
</head>
		
		
		<s:iterator value="diferenciasAmisList" var="c" status="row">		
		<row id="<s:property value="#row.index"/>">
		
		<s:if test="numeroPoliza == (#row.index-1)">
			<cell><s:property value="''" escapeHtml="false" escapeXml="true"/></cell>
		</s:if>
		<s:else>
			<cell><s:property value="top.numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
		</s:else>

			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			
			<s:if test="claveError==2 || claveError==3 || claveError==4 || claveError==5 || claveError==6">
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>				
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>				
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>				
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>				
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>				
				<cell><s:property value="'No hay informacion'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No hay informacion^</cell>				
				
			</s:if>
			<s:if test="claveError==1">
				<s:if test="version!=null">
					<cell><s:property value="version" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="version==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="marca!=null">
					<cell><s:property value="marca" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="marca==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="anos!=null">
					<cell><s:property value="anos" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>					
				</s:if>
				<s:if test="anos==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="puertas!=null">
					<cell><s:property value="puertas" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="puertas==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="cilindros!=null">
					<cell><s:property value="cilindros" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="cilindros==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="combustible!=null">
					<cell><s:property value="combustible" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="combustible==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>					
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="interiores!=null">
					<cell><s:property value="interiores" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="interiores==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>
				
				<s:if test="pasajeros!=null">
					<cell><s:property value="pasajeros" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/pixel.gif^No coincide^</cell>
				</s:if>
				<s:if test="pasajeros==null">
					<cell><s:property value="'Correcto'" escapeHtml="false" escapeXml="true"/></cell>
					<cell>/MidasWeb/img/checked.gif^Valor correcto^</cell>
				</s:if>					
		
			</s:if>
			
			<s:if test="estatus==1">
				<cell><s:property value="'Pendiente revisi\u00F3n'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<s:if test="estatus==2">
				<cell><s:property value="'Cambio solicitado'" escapeHtml="false" escapeXml="true"/></cell>
			</s:if>
			<cell>/MidasWeb/img/common/ico_email.gif^Solicitar cambio^javascript: solicitarCambio("<s:property value="id" escapeHtml="false" escapeXml="true"/>","<s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			
			
		</row>
	</s:iterator>

	
</rows>