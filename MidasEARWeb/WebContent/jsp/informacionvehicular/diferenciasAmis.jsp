<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">
<sj:head/>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/emision/consulta/diferenciasAmis.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	
<head>
<meta content="IE=8" http-equiv="X-UA-Compatible">
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
.divInputText{
    width: 180px;
    max-width:180px;
    height:14px;
    max-height:14px;
    background-color:#FFF;
    text-align:left;
    cursor:text;
	-moz-user-select: -moz-none;
   	-khtml-user-select: none;
	-webkit-user-select: none;
   /*
     Introduced in IE 10.
     See http://ie.microsoft.com/testdrive/HTML5/msUserSelect/
   */
   	-ms-user-select: none;
   	user-select: none;
}
</style>
</head>

<script type="text/javascript">
// var agenteControlDeshabilitado = <s:property value="agenteControlDeshabilitado"/>;
var agenteControlDeshabilitado = false;
</script>

<s:form action="listar" id="diferenciasAmisForm" introButon="submit">
	<div class="titulo" style="width: 98%;">
			<s:text name="P�lizas con Diferencias"/>
	</div>
	<div id="contenedorFiltros" style="width: 98%;">
	
	
	<table id="agregar" border="0">
			<tr>
				<td>
					<sj:datepicker name="fechaInicial" cssStyle="width: 170px;"
						key="midas.reporteSesas.fechaInicial"
						labelposition="top" 					
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaInicial"
                        maxDate="today"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
				</td> 
		    	<td>
					<sj:datepicker name="fechaFinal" required="#requiredField" cssStyle="width: 170px;"
						key="midas.reporteSesas.fechaFinal"
						labelposition="top" 					
						buttonImage="../img/b_calendario.gif"
					    id="fechaFinal"
					    size="12"
					    changeMonth="true"
					    changeYear="true"
					    maxDate="today"
						maxlength="10" cssClass="txtfield"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			 	</td>			 	
			<td>
				<s:textfield  cssClass="txtfield" cssStyle="width: 200px;"
					key="midas.endosos.solicitudEndoso.definirSolicitudEndoso.poliza"
			        labelposition="top"
					id="polizaNum" name="polizaNum"
					/>
			</td>
			</tr>
			<tr>
			<td>
				<s:hidden id="idAgente" name="codigoAgente"/>
					<div><label for="agenteNombre"><s:text name="midas.poliza.nombreAgente"></s:text></label></div>
					<br/>
					<div style="vertical-align: bottom;" >
					<s:if test="!agenteControlDeshabilitado">
 						<s:set var="agenteControlOnclick" value="'seleccionarAgente(1);'"/>
			 		</s:if>
					<div id="agenteNombre" class="divInputText txtfield" style="width: 180px; float: left; " onclick="${agenteControlOnclick}">
						<s:property value=""/>
			 		</div>
			 		<div style="float: left; vertical-align: bottom;">
			 		<s:if test="!agenteControlDeshabilitado">
		 			<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripci�n" 
			 			style="margin-left: 5px;"
			 			onclick ="cleanInputDiv('agenteNombre');cleanInput('idAgente');"
			 		/>
			 		</s:if>
			 		</div>
			 		</div>													
			</td>
			<td>
				<s:select key="midas.general.estatus" 
				  name="estatus" id="estatus"
				  labelposition="top" 
				  headerKey="0" headerValue="Seleccione"
				  list="#{'1':'Pendiente revisi�n', '2':'Cambio solicitado'}"
				  cssClass="txtfield" /> 							
			</td>
			 	<td>
					<div class="btn_back w140" style="display: inline; float: left;">                    
					<a id="submit" href="javascript: void(0);" onclick="validarFiltros();" class="icon_buscar">
					<s:text name="midas.boton.buscar" /> </a>
					</div>														
<!-- 					<div class="btn_back w130" style="display: inline; float: right;"> -->
<!-- 						<a href="javascript: void(0);" id="mostrarFiltros" -->
<!-- 							onclick="displayFiltersDifAmis();">Ocultar Filtros</a> -->
<!-- 					</div> -->
				</td>
			</tr>
			</table>	
	
	
	</div>




	</s:form>

<div>
			<div id="indicador"></div>
			<div id="diferenciasAmisGrid" style="width:98%;height:230px"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
</div>

<script type="text/javascript">
/** 	iniciaDiferenciasAmis();*/
</script>