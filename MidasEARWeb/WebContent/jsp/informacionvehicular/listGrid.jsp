<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
        <beforeInit>        
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>10</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>    
			<call command="setPagingSkin">
				<param>bricks</param>
			</call> 		
        </beforeInit>    
		<afterInit>
        </afterInit>
        <column id="idcheckbox" type="ch" width="30" align="center">#master_checkbox</column>
		<column id="id" type="ro" width="50" hidden="true" sort="int" align="center">id</column>
		<column id="fechaMovimiento" type="ro" width="90" sort="str" align="center">Fecha Movimiento</column>		
 		<column id="nombreAgente" type="ro" width="200" sort="str" align="center">Agente</column>
		<column id="idConcepto" type="ro" width="70" sort="str" align="center">Concepto</column>
		<column id="descMovimiento" type="ro" width="180" sort="str" align="center">Desc. Movimiento</column>
		<column id="idRamo" type="ro" width="50" sort="str" align="center">Ramo</column>
		<column id="tipoMovimiento" type="ro" width="85" sort="str" align="center">Tipo Movimiento</column>
		<column id="importe" type="ro" width="80" sort="int" align="center">Importe</column>
		<column id="estatus" type="ro" width="90" sort="str" align="center">Estatus</column>		

	</head>
	<s:iterator value="listaMovimientos" var="rowMovimientos" status="index">
	<row id="${rowMovimientos.id}">
			<cell>0</cell>
			<cell><![CDATA[${rowMovimientos.id}]]></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[${rowMovimientos.agente.persona.nombreCompleto}]]></cell>
			<cell><![CDATA[${rowMovimientos.idConcepto}]]></cell>
			<cell><![CDATA[${rowMovimientos.descripcionMovto}]]></cell>
			<cell><![CDATA[${rowMovimientos.idRamoContable}]]></cell>
			<cell><![CDATA[${rowMovimientos.naturalezaConcepto}]]></cell>
			<cell><![CDATA[${rowMovimientos.importeMovto}]]></cell>						   
				<s:if test="estatusRegistro == 0">
					<cell></cell>	
				</s:if>				
				<s:if test="estatusRegistro == 2">
					<cell type="img" title="Ha ocurrido un error al intentar aplicar este movimiento. Favor de intentar mas tarde o reportar al area de Sistemas">
					/MidasWeb/img/red_att.gif</cell>	
				</s:if>									
 		</row>
	</s:iterator>
</rows>