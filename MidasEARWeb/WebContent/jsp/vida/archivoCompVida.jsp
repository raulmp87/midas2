<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	var mostrarPath = '<s:url action="mostrar" namespace="/vida/cargaArchivos"/>';
	var obtenerPlantillaPath = '<s:url action="obtenerPlantilla" namespace="/vida/cargaArchivos"/>';
	var procesarInfoPath = '<s:url action="procesarinfo" namespace="/vida/cargaArchivos"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/vida/archivoComp.js'/>"></script>

<s:form action="/generacion/mostrar" namespace="/vida/cargaArchivos" id="generacionForm">

	<s:hidden name="claveNegocio" />
	<s:hidden name="tipoPlantilla" />
	<s:hidden name="idToControlArchivo" />
	<s:hidden name="mensaje" id="mensaje" value ="%{mensaje}" />
	<div class="subtituloIzquierdaDiv">
		Carga Archivos Complementos Vida
	</div>
	<table width="98%" id="filtros">
		
		<tr>
			<td colspan="1">Tipo Archivo</td>	
			<td colspan="" class="contenedor" style="width: 350px">
     			<select id="tipoArchivo" name="tipoArchivo" Class="cajaTexto" style="width: 300px">
     			    <option value="0">Seleccione ...</option>
     				<option value="1">Archivo AMIS</option>
     				<!--  
     					<option value="2">Archivo Rescates</option>
     				-->
     				<option value="3">Actualizacion Cal. REAS</option>
     			</select>
     		</td>     		
		</tr>
 		<!-- 
		<tr>
			<td>
			   	 <sj:datepicker key="reaseguro.datoscontraparte.fechacorte" name="fechaCorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
			</td>
		</tr>
 		 -->
		<tr>
			<td colspan="6">
			<!-- 
				<midas:boton onclick="javascript: procesarInfo();"  tipo="agregar" key="reaseguro.datoscontraparte.procesar" style="width:30%"/>
			</td>
			 -->
		</tr>
		<tr><td></td>
			<td colspan="2">
				<div class="divInterno" style="float: left; position: relative; width: 100%; margin-top: 1%;height: 40px;">
			
					<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
						<a href="javascript: void(0);" onclick="javascript: procesarInfo();"> 
							Cargar Archivo 
						</a>
					</div>
					
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="resultadosDocumentos" class="cajaTextoM2 w100" style="height: 300px"></div>
			</td>
		</tr>
	</table>
	
</s:form>