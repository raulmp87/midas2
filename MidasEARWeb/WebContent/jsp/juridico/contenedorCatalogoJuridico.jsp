<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script src="<s:url value='/js/midas2/juridico/catalogosJuridico.js'/>"></script>

<script type="text/javascript"
	src="<s:url value='/dwr/interface/listadoService.js'/>"></script>


<script type="text/javascript">
	var mostrarGuardarCatalogo 			= '<s:url action="guardarCatalogo" namespace="/juridico/catalogosJuridico"/>';
	var mostrarListarCatalogos			= '<s:url action="listarCatalogos" namespace="/juridico/catalogosJuridico"/>';
	var mostrarContendedor   			= '<s:url action="mostrar" namespace="/juridico/catalogosJuridico"/>';
	
</script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 40px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.floatLeft {
	float: left;
	position: relative;
}

.floatLeftNoLabel {
	float: left;
	position: relative;
	margin-top: 12px;
}

.divInfDivInterno {
	width: 17%;
}

.error {
	background-color: red;
	opacity: 0.4;
}

#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
</style>

<div id="contenido_CatalogoJuridico" style="width: 99%; position: relative;">
	<s:form id="catJuridicoForm" class="floatLeft">
		  <s:hidden name="tipoCatalogo" id="tipoCatalogo"></s:hidden>
		  <s:hidden name="filtroCatalogo.id" id="idCatalogo"></s:hidden>
		
		
		<s:if test="tipoCatalogo == 'DEL'">	
			<div class="titulo" align="left">
				<s:text name="midas.siniestros.juridico.catalogo.delegacion.titulo" />
			</div>	
		</s:if>
		
		<s:if test="tipoCatalogo == 'MOT'">	
			<div class="titulo" align="left">
				<s:text name="midas.siniestros.juridico.catalogo.motivos.titulo" />
			</div>						
		</s:if>
		
		
		<s:if test="tipoCatalogo == 'PROC'">	
			<div class="titulo" align="left">
				<s:text name="midas.siniestros.juridico.catalogo.proced.titulo" />
			</div>						
		</s:if>
		<div id="divInferior" style="width: 1142px !important;"
			class="floatLeft">

			<div id="contenedorFiltros" class="divContenedorO"
				style="width: 99%">
				<div class="divInterno">

					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="numero" name="filtroCatalogo.numero"
							cssClass="txtfield" label="%{getText('midas.siniestros.juridico.catalogo.numero'+tipoCatalogo)}" 
							onkeypress="return soloNumeros(this, event, true)"							
							labelposition="left" cssStyle="width:80px;" readonly="true"  ></s:textfield>
					</div>
					
					
					<div class="floatLeft" style="width: 30%;">
						<s:textfield id="nombre" name="filtroCatalogo.nombre"
							cssClass="txtfield" label="%{getText('midas.siniestros.juridico.catalogo.nombre'+tipoCatalogo)}" 
												
							labelposition="left" cssStyle="width:80px;" ></s:textfield>
					</div>
					
					
					<div class="floatLeft" style="width: 30%;">
						<s:select list="estautsMap" id="estautsId"
							name="filtroCatalogo.estatus" label="%{getText('midas.siniestros.juridico.catalogo.estatus')}"  labelposition="left"
							cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:60%;">
						</s:select>
					</div>

					
				</div>
				<div class="divInterno">
					<div class="floatLeft" style="width: 30%;">
						<s:select list="listadoOficinas" id="oficinaId"
							name="filtroCatalogo.idOficina" label="%{getText('midas.siniestros.juridico.catalogo.oficina')}"  labelposition="left"
							cssClass="txtfield" headerKey=""
							headerValue="%{getText('midas.general.seleccione')}"
							cssStyle="width:60%;">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 30%;">					
						<!--<sj:datepicker name="filtroCatalogo.fechaActivo" id="datepickerFechaActivo"
									label="%{getText('midas.siniestros.juridico.catalogo.fechaA')}"  labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaActivo"
									value="%{filtroCatalogo.fechaActivo}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false);"
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this); ">
								</sj:datepicker>-->
								<s:textfield id="fechaActivo" name="filtroCatalogo.fechaActivo"
								cssClass="txtfield" label="%{getText('midas.siniestros.juridico.catalogo.fechaA')}" 
								onkeypress="return soloFecha(this, event, false) "
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="esFechaValida(this) "							
								labelposition="left" cssStyle="width:80px;" readonly="true"  ></s:textfield>
					</div>
					 
					<div id="divInactivo" class="floatLeft" style="width: 30%;">					
						<!-- <sj:datepicker name="filtroCatalogo.fechaInactivo" id="datepickerFechaInactivo"
									label="%{getText('midas.siniestros.juridico.catalogo.fechaI')}"  labelposition="left" changeMonth="true" changeYear="true"
									buttonImage="../img/b_calendario.gif" id="fechaInactivo"
									value="%{filtroCatalogo.fechaInactivo}" maxlength="10"
									cssClass="txtfield" size="12"
									onkeypress="return soloFecha(this, event, false) "
									onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									onblur="esFechaValida(this) ">
								</sj:datepicker>-->
							<s:textfield id="fechaInactivo" name="filtroCatalogo.fechaInactivo"
								cssClass="txtfield" label="%{getText('midas.siniestros.juridico.catalogo.fechaI')}" 
								onkeypress="return soloFecha(this, event, false) "
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="esFechaValida(this) "							
								labelposition="left" cssStyle="width:80px;" readonly="true"  ></s:textfield>
					</div>
				</div>

				

				
				<div class="divInterno" style="padding-right: 200px">
					 <div class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);"
							onclick="nuevo();"> <s:text
								name="midas.boton.nuevo" /> </a>
					</div> 
					<div class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);" onclick="guardar();">
							<s:text name="midas.boton.guardar" /> </a>
					</div>
				</div>
			</div>

			
			
			
		<s:if test="tipoCatalogo == 'DEL'">	
			<div class="titulo" align="left" style="padding-top: 20px">
				<s:text name="midas.siniestros.juridico.catalogo.delegacion.lista" />
			</div>	
		</s:if>
		
		<s:if test="tipoCatalogo == 'MOT'">	
			<div class="titulo" align="left" style="padding-top: 20px">
				<s:text name="midas.siniestros.juridico.catalogo.motivos.lista" />
			</div>					
		</s:if>
		
		
		<s:if test="tipoCatalogo == 'PROC'">	
			<div class="titulo" align="left" style="padding-top: 20px">
				<s:text name="midas.siniestros.juridico.catalogo.proced.lista" />
			</div>						
		</s:if>
			
			
			
			
			
			
			
			
		   <div id="catalogoGrid" class="dataGridConfigurationClass"
			style="width: 99%; height: 380px;"></div>			
			<div id="pagingArea" style="padding-top: 8px "></div>
			<div id="infoArea"></div>
		</div>
		<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</s:form>
	<script>
	jQuery(document).ready(function() {
		buscarLista();
	});
	</script>
	

</div>
