<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: right;	
	font-size: 11px;
	font-family: arial;
	}
</style>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>

<script type="text/javascript"	src="<s:url value='/js/midas2/juridico/reclamacionesJuridico.js'/>"></script>

<script type="text/javascript">
var mostrarAgregarOficioPath = '<s:url action="agregarOficio" namespace="/siniestros/juridico"/>';
</script>

<s:form id="tipoReclamacionForm" name="tipoReclamacionForm" namespace="/siniestros/juridico" action="agregarOficio" >
	
	<div id="contenedorFiltros" style="width: 96%;">
		<table id="agregar" border="0">
			
			<tr><td>
			<div class="titulo" style="width: 98%;">
				<s:text name="midas.siniestros.juridico.selecciontiporeclamacion"/>	
			</div>
			</td></tr>
			
			<tr><td>
			<div id="tipoReclamacionContainer" style="margin-left: 10%;margin-top: 2%;">
				<s:select list="listaTiposReclamacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.tipoReclamacion" id="tipoReclamacion_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.tiporeclamacion')}"/>
			</div>
			</td></tr>
		
	</table>
	</div>
	
</s:form>

	<table id="agregar" style="padding: 0px; width: 100%; margin: 0px; border: none;">
		<tr>
			<td>
				<div class="btn_back w140" style="display: inline; float: right;" id="crearReclamacion">
					<a href="javascript: void(0);" onclick="crearNuevaReclamacion();"> 
					<s:text name="midas.siniestros.juridico.crear" /> </a>
				</div>
				<div id="validacionSeleccionTipo" style="display: none; float: right;position: relative; color: red; font-size: 7.5pt;">
					<s:text name="midas.siniestros.juridico.mensajeValidacionCrear"/>
				</div>
			</td>							
		</tr>
	</table>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>