<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		
		<column  type="ro"  width="*"  align="center" sort="str"> <s:text name="midas.listadoSiniestrosAction.titulo.numeroSiniestro" />  </column>
		<column  type="ro"  width="*"  align="center" sort="str"> <s:text name="midas.negocio.oficina" />   </column>
		<column  type="ro"  width="*"  align="center" sort="date_custom"> <s:text name="midas.siniestros.cabina.reporte.cita.fecha" />  </column>
 		<column  type="ro"  width="*"  align="center" sort="str"> <s:text name="midas.listadoSiniestrosAction.titulo.numeroPoliza" />  </column>
	    <column  type="ro"  width="*"  align="center" sort="str"> <s:text name="midas.suscripcion.solicitud.autorizacion.inciso" />  </column>
	    <column  type="ro"  width="*"  id="oficinaId" sort="int" hidden="true" >oficinaId</column>
	    <column  type="ro"  width="*"  id="siniestroCabinaId" sort="int" hidden="true" >siniestroCabinaId</column>
	</head>


	<s:iterator value="reportesSiniestro">
		<row>
			<cell><s:property value="numeroSiniestro"   escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreOficina"     escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaSiniestro"    escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroPoliza"      escapeHtml="false" escapeXml="true"/></cell>
 		    <cell><s:property value="numeroInciso"      escapeHtml="false" escapeXml="true"/></cell>
 		    <cell><s:property value="oficinaId"         escapeHtml="false" escapeXml="true"/></cell>
 		    <cell><s:property value="siniestroCabinaId" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>