<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="50"  align="center" sort="int" > <s:text name="midas.siniestros.juridico.numreclamacion" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.tiporeclamacion" />  </column>
          <column  type="ro"  width="40"  align="center" sort="int" > <s:text name="midas.siniestros.juridico.totaloficios" />  </column>
          <column  type="ro"  width="40"  align="center" sort="str"  > <s:text name="midas.siniestros.juridico.oficinajuridica" />  </column>
          <column  type="ro"  width="70"  align="center" sort="str"  > <s:text name="midas.siniestros.juridico.numpoliza" />  </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.asegurado" />  </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.numsiniestro" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.oficinasiniestro" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.expjuridico" />   </column>
          <column  type="ro"  width="70"  align="center" sort="date_custom" > <s:text name="midas.siniestros.juridico.fechaalta" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.demandado" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.reclamante" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.asignado" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.procedimiento" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.ramo" />   </column>
          <column  type="ro"  width="70"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.estado" />   </column>
          <column  type="ro"  width="50"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.estatus" />   </column>
          <column  type="img"   width="40" sort="na" align="center" >Acciones</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>

	</head>

	<s:iterator value="reclamaciones">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="numeroReclamacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoReclamacion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="totalOficios" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="oficinaJuridica" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="asegurado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="oficinaSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="expedienteJuridico" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaNotificacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="demandado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="reclamante" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="asignado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="procedimiento" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ramo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:consultarReclamacion(<s:property value="numeroReclamacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<s:if test="estatusFinal==\"P\"">
				<cell>../img/icons/ico_editar.gif^Editar^javascript:editarReclamacion(<s:property value="numeroReclamacion" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </s:if>
		    <s:else>
		    	<cell>../img/pixel.gif^NA^^_self</cell>
		    </s:else>
		</row>
	</s:iterator>
	
</rows>