<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
		<column  type="ro"  width="*"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.cobertura" /> </column>
		<column  type="ron"  width="*"  align="center" format="$0,000.00" sort="int"> <s:text name="midas.siniestros.juridico.reservasistema" /> </column>
		<s:if test="esConsulta || reclamacionOficio.estatus==\"C\"">
			<column  type="ron"  width="*"  align="center" format="$0,000.00" sort="int"> <s:text name="midas.siniestros.juridico.reservaautoridad" /> </column>
		</s:if>
		<s:else>
			<column  type="edn"  width="*"  align="center" format="$0,000.00" sort="int"> <s:text name="midas.siniestros.juridico.reservaautoridad" /> </column>
		</s:else>
          

	</head>

	<s:iterator value="reservas">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property  value="reservaSiniestro" escapeHtml="false" escapeXml="true"  /></cell>
		    <cell class="jQfloat jQrestrict"><s:property  value="reservaMontoAutoridad" escapeHtml="false" escapeXml="true"  /></cell>
		    
		    <userdata name="coberturaIdCell"><s:property value="coberturaId" escapeHtml="false" escapeXml="true"/></userdata>
		    <userdata name="oficioCell"><s:property value="oficio" escapeHtml="false" escapeXml="true"/></userdata>
		    <userdata name="claveSubCoberturaCell"><s:property value="claveSubCobertura" escapeHtml="false" escapeXml="true"/></userdata>
		    <userdata name="reclamacionIdCell"><s:property value="reclamacionId" escapeHtml="false" escapeXml="true"/></userdata>

		</row>
	</s:iterator>
	
</rows>