<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>

<script src="<s:url value='/js/midas2/juridico/reclamacionesJuridico.js'/>" ></script>


<sj:head/>


<style type="text/css">
	#superior{
		height: 200px;
		width: 99%;
	}

	#superiorI{
		height: 150px;
		width: 850px;
		position: relative;
		float:left;
		margin-left: 1%;
	}
	
	#superiorD{
		width: 210px;
		float: left;
		margin-left: 70%;
	}
	
	
	
	div.ui-datepicker {
		font-size: 10px;
	}
	.divContenedorO {
		border: 1px solid #28b31a;
		background-color: #EDFAE1;
	}
	
	.divContenedorU {
		border-bottom: 1px solid #28b31a;
		border-right: 1px solid #28b31a;
	}
	
	.floatLeft {
		float: left;
		position: relative;
	}
	
	label{
		font-family: Verdana,Arial,Helvetica,sans-serif; font-size:   9.33333px; text-align:  left
	}

</style>
<div id="contenido_listadoReporteSiniestro" style="margin-left: 2% !important;height: 250px;">
<div class="titulo" style="width: 98%;">
	<s:text name="midas.siniestros.cabina.listadoReporteSiniestro.busqueda.title"/>	
</div>	
	<div id="superior" class="divContenedorO">
		<form id="formReporteSiniestroDTO">
			<div id="superiorI">
				<div id="SIS">
					<div class="floatLeft" style="width: 25%;">
						<s:textfield label="No. Siniestro" name="reporteSiniestroDTO.numeroSiniestro" 
								onBlur="validaFormatoSiniestroReclamaciones(this);"
								cssStyle="float: left;width: 80%;" cssClass="cleaneable txtfield" id="numSiniestro"></s:textfield>
					</div>
					<div class="floatLeft" style="width: 30%;">
						<s:select list="oficinasActivas" cssStyle="width: 90%;"
							name="reporteSiniestroDTO.oficinaId" 
							label="Oficinas"
							cssClass="cleaneable txtfield"
							headerKey="" headerValue="%{getText('midas.general.seleccione')}">
						</s:select>
					</div>
					<div class="floatLeft" style="width: 25%;">
						<div class="floatLeft" style="width: 90%;">
							<s:textfield label="No. Póliza" name="reporteSiniestroDTO.numeroPoliza"
									cssClass="cleaneable txtfield" cssStyle="float: left;width: 90%;"></s:textfield>
						</div>
					</div>
					<div class="floatLeft" style="width: 20%;">
						<label>Fecha de Reporte:</label><br/>
					</div>
					<div class="floatLeft" style=" margin-top:2%; ">
						<div >
							<label>Del:</label><br/>
							<sj:datepicker name="reporteSiniestroDTO.fechaIniReporte"
									changeMonth="true"
									 changeYear="true"				
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" 
				                      		id="reporteSiniestroDTO.reporteCabina.fechaHoraReporte"
									  maxlength="10" cssClass="txtfield cleanable"
										   size="18"
									 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield">
							</sj:datepicker>
							<br/>
							<label>Al:</label><br/>
							<sj:datepicker name="reporteSiniestroDTO.fechaFinReporte"
									changeMonth="true"
									 changeYear="true"				
									buttonImage="/MidasWeb/img/b_calendario.gif"
									buttonImageOnly="true" 
				                      		id="reporteSiniestroDTO.reporteCabina.fechaHoraTerminacion"
									  maxlength="10" cssClass="txtfield cleanable"
										   size="18"
									 onkeypress="return soloFecha(this, event, false);"
										onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
										cssClass="cleaneable txtfield">
							</sj:datepicker>
						</div>
					</div>
					<br/>
				</div>

			</div>
			<div id="validacionBusqueda" style="display: none; float: right;position: relative; color: red; font-size: 7.5pt;">
						<s:text name="midas.siniestros.juridico.mensajevalidacionbusqueda"/>
			</div>
			<div id="validacionFormatoNumeroSiniestro" style="display: none; float: right;position: relative; color: red; font-size: 7.5pt;">
						<s:text name="midas.siniestros.juridico.mensajevalidacionnumerosiniestro"/>
			</div>
			<div id="superiorD" >
				<div>
					<div class="btn_back w100" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="limpiarFiltrosSiniestro();"> 
							<s:text	name="midas.boton.limpiar"/> 
						</a>
					</div>
					<div class="btn_back w100" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="realizarBusqueda();"> 
							<s:text	name="midas.boton.buscar"/> 
						</a>
					</div>
				</div>
			</div>
			
		</form>
	</div>
	
</div>

	<div class="titulo" style="margin-left: 2%">Listado de Reportes de Siniestros</div>
	<div id="indicador"></div>
	<div id="inferior" >
		<div id="busquedaJuridicoSiniestrosGrid" style="width:98%;height:300px;margin-left: 2%">
		</div>
		<div id="pagingArea" style="margin-left: 2%"></div><div id="infoArea" style="margin-left: 2%"></div>
	</div>

<script type="text/javascript">
	cargaGridBusquedaPoliza("n");
</script>