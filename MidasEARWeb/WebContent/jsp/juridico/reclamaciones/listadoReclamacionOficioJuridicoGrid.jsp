<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="100"  align="center" sort="int" > <s:text name="midas.siniestros.juridico.numoficio" />   </column>
          <s:if test="reclamacionOficio.tipoReclamacion==\"LI\"">
         	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.entidadfederativa.LI" />  </column></s:if>
          <s:if test="reclamacionOficio.tipoReclamacion!=\"LI\"">
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.oficinajuridica" />  </column>
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.tiporeclamacion" />  </column></s:if>
          <column  type="ro"  width="100"  align="center" sort="date_custom" > <s:text name="midas.siniestros.juridico.fechanotificacion" />  </column>
          <s:if test="reclamacionOficio.tipoReclamacion==\"GE\" || reclamacionOficio.tipoReclamacion==\"CO\"">
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.reclamante.GE" />  </column></s:if>
          <s:elseif test="reclamacionOficio.tipoReclamacion==\"AA\"">
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.reclamante.AA" />  </column></s:elseif>
          <s:if test="reclamacionOficio.tipoReclamacion==\"LI\"">
          <column  type="ro"  width="100"  align="center" sort="str"  > <s:text name="midas.siniestros.juridico.actor" />  </column>
          <column  type="ro"  width="100"  align="center" sort="str"  > <s:text name="midas.siniestros.juridico.demandado" />  </column></s:if>
          <s:if test="reclamacionOficio.tipoReclamacion==\"LI\" || reclamacionOficio.tipoReclamacion==\"AA\"">
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.asegurado" />  </column></s:if>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.ramo" />   </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.numpoliza" />   </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.numsiniestro" />   </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.oficinasiniestro" />   </column>
          <s:if test="reclamacionOficio.tipoReclamacion!=\"LI\"">
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.motivorecalmacion" />   </column></s:if>
          <column  type="ro"  width="100"  align="center" sort="int" > <s:text name="midas.siniestros.juridico.montoreclamado" />   </column>
          <s:if test="reclamacionOficio.tipoReclamacion!=\"GE\"">
         	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.asignado" />   </column></s:if>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.procedimiento" />   </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.expedientejuridico" />   </column>
          <s:if test="reclamacionOficio.tipoReclamacion==\"LI\"">
         	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.etapajuicio" />   </column></s:if>
          <s:if test="reclamacionOficio.tipoReclamacion!=\"LI\"">
        	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.estado" />  </column>
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.municipio" />  </column>
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.delegacion" />  </column>
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.estatus" />  </column></s:if>
          <s:if test="reclamacionOficio.tipoReclamacion==\"GE\"">
         	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.clasificacion" />  </column>
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.tipoqueja" />  </column>
          	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.comentarios.GE" />  </column></s:if>
          <s:elseif test="reclamacionOficio.tipoReclamacion==\"AA\"">
         	 <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.siniestros.juridico.comentarios.AA" />  </column></s:elseif>
          <column  type="img"   width="40" sort="na" align="center" >Acciones</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>

	</head>

	<s:iterator value="oficios">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="numeroOficio" escapeHtml="false" escapeXml="true"/></cell>
		    <s:if test="tipoReclamacion==\"LI\"">
				<cell><s:property value="ubicacion" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<s:if test="tipoReclamacion!=\"LI\"">
          	 	<cell><s:property value="oficinaJuridicaDesc" escapeHtml="false" escapeXml="true"/></cell>
          	 	<cell><s:property value="tipoReclamacionDesc" escapeHtml="false" escapeXml="true"/></cell></s:if>
		    <cell><s:property value="fechaNotificacion" escapeHtml="false" escapeXml="true"/></cell>
		    <s:if test="tipoReclamacion==\"GE\" || tipoReclamacion==\"CO\" || tipoReclamacion==\"AA\"">
		    	<cell><s:property value="reclamante" escapeHtml="false" escapeXml="true"/></cell></s:if>
		    <s:if test="tipoReclamacion==\"LI\"">
			    <cell><s:property value="actor" escapeHtml="false" escapeXml="true"/></cell>
			    <cell><s:property value="demandado" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<s:if test="tipoReclamacion==\"LI\" || tipoReclamacion==\"AA\"">
				<cell><s:property value="asegurado" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<cell><s:property value="ramoDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="oficinaSiniestroDesc" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="tipoReclamacion!=\"LI\"">
				<cell><s:property value="motivoReclamacionDesc" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<cell><s:property value="montoReclamado" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="tipoReclamacion!=\"GE\"">
				<cell><s:property value="asignado" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<cell><s:property value="procedimientoDesc" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="expedienteJuridico" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="tipoReclamacion==\"LI\"">
			<cell><s:property value="etapaJuicioDesc" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<s:if test="tipoReclamacion!=\"LI\"">
				<cell><s:property value="estadoDesc" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="municipioDesc" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="delegacionDesc" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<s:if test="tipoReclamacion==\"GE\"">
				<cell><s:property value="clasificacionReclamacionDesc" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="tipoQuejaDesc" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="justificacion" escapeHtml="false" escapeXml="true"/></cell></s:if>
			<s:elseif test="tipoReclamacion==\"AA\"">
				<cell><s:property value="justificacion" escapeHtml="false" escapeXml="true"/></cell></s:elseif>
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:consultarOficio(<s:property value="numeroReclamacion" escapeHtml="false" escapeXml="true"/>,<s:property value="numeroOficio" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<s:if test="estatus!=\"C\" && esUltimoOficio==true">
				<cell>../img/icons/ico_editar.gif^Editar^javascript:editarOficio(<s:property value="numeroReclamacion" escapeHtml="false" escapeXml="true"/>,<s:property value="numeroOficio" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </s:if>
		    <s:else>
		    	<cell>../img/pixel.gif^NA^^_self</cell>
		    </s:else>
		</row>
	</s:iterator>
	
</rows>