<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript">
var mostrarEditarOficioPath = '<s:url action="editarOficio" namespace="/siniestros/juridico"/>';
var guardarOficioPath = '<s:url action="guardarOficio" namespace="/siniestros/juridico"/>';
var exportarExcelPath = '<s:url action="exportarResultados" namespace="/siniestros/juridico"/>';
var listarReclamacionesPath = '<s:url action="listarReclamaciones" namespace="/siniestros/juridico"/>';
var mostrarVentanaTipoReclamacionPath = '<s:url action="mostrarVentanaTipoReclamacion" namespace="/siniestros/juridico"/>';
</script>

<style type="text/css">
.floatLeft {
	float: left;
	position: relative;
}
</style>

<s:form id="reclamacionOficioForm" >
	<s:hidden id="esConsulta" name="esConsulta"/>
	<s:hidden id="tipoReclamacion" name="reclamacionOficio.tipoReclamacion"/>
	<s:hidden id="numeroReclamacion" name="reclamacionOficio.numeroReclamacion"/>
	<s:hidden id="numeroOficio" name="reclamacionOficio.numeroOficio"/>
	<s:hidden id="urlCrearNuevoOficio" name="urlCrearNuevoOficio"/>
	<s:hidden id="mostrarListadoVacio" name="mostrarListadoVacio"/>

	<table  id="filtrosM2" width="98%">
            <tr>
                  <td class="titulo" colspan="6"><s:text name="midas.siniestros.juridico.busqueda" /></td>
            </tr>
          
          	<tr>
            	<td>
	            	<s:textfield name="reclamacionFiltro.numeroReclamacion" id="numeroReclamacion_t" cssClass="cajaTextoM2 w230 jQnumeric jQrestrict" 
	            		label="%{getText('midas.siniestros.juridico.numreclamacion')}" disabled="false" />
            	</td>
				
				<td>
					<s:select list="listaOficinasJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionFiltro.oficinaJuridicaId" id="oficinaJuridica_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.oficinajuridica')}"/>
            	</td>
            	
            	<td>
	            	<s:textfield name="reclamacionFiltro.numeroPoliza" id="numeroPoliza_t" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.siniestros.juridico.numpoliza')}" disabled="false" />
            	</td>
            	
            	<td colspan="3">
	            	<s:textfield name="reclamacionFiltro.numeroSiniestro" id="numeroSiniestro_t" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.siniestros.juridico.numsiniestro')}" disabled="false" />
            	</td>
            </tr>
            
            <tr>
            	<td  align="left"> <sj:datepicker name="reclamacionFiltro.fechaNotificacionDe"
					id="fechaInicio_t" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.siniestros.juridico.fechaaltade')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
            	
            	<td  align="left"> <sj:datepicker name="reclamacionFiltro.fechaNotificacionHasta"
					id="fechaFin_t" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.siniestros.juridico.fechaaltaa')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
            	
            	<td>
	            	<s:textfield name="reclamacionFiltro.asegurado" id="asegurado_t" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.siniestros.juridico.asegurado')}" disabled="false" />
            	</td>
            	
            	<td colspan="3">
	            	<s:textfield name="reclamacionFiltro.reclamante" id="reclamante_t" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.siniestros.juridico.reclamante')}" disabled="false" />
            	</td>
            </tr>
            
            <tr>
            	<td>
					<s:select list="listaEstatus" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionFiltro.estatusReclamo" id="estatus_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.estatus')}"/>
            	</td>
				
				<td>
					<s:select list="listaProcedimientos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionFiltro.procedimiento" id="procedimiento_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.procedimiento')}"/>
            	</td>
            	
            	<td>
					<s:select list="listaTiposReclamacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionFiltro.tipoReclamacion" id="tipoReclamacion_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.tiporeclamacion')}"/>
            	</td>
            	
            	<td colspan="3">
	            	<s:select list="listaRamos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionFiltro.ramo" id="ramo_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.ramo')}"/>
            	</td>
            </tr>
            
            <tr>
            	<td>
					<s:select list="listaOficinasSiniestro" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionFiltro.oficinaSiniestroId" id="oficinaSiniestro_s" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.siniestros.juridico.oficinasiniestro')}"/>
            	</td>
				
				<td colspan="5">
					<s:textfield name="reclamacionFiltro.expediente" id="expediente_t" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.siniestros.juridico.expedientejuridico')}" disabled="false" />
            	</td>
            </tr>
            
            <tr>		
					<td colspan="6">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="buscarReclamaciones(false);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltros();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      
    <BR/>
	<BR/>
       
	<div id="tituloListado" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.siniestros.juridico.listadoreclamacion" /></div>
	<div id="indicadorReclamaciones"></div>	
	<div id="reclamacionesGridContainer" style="display:none;">
		<div id="listadoReclamacionJuridicoGrid" style="width:98%;height:200px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
 	
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Reclamaciones' title='Reclamaciones' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="mostrarVentanaTipoReclamacion();">
					<s:text name="midas.boton.nuevo" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Nueva Reclamacion' title='Nueva Reclamacion' src='/MidasWeb/img/common/b_mas_agregar.jpg' style="vertical-align: middle;"/> 
				</a>
			</div>
		</td>
	</tr>
</table>	

</s:form>

<script src="<s:url value='/js/midas2/juridico/reclamacionesJuridico.js'/>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>