<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">
 .labelBlack{
 	color:#666666 !important;
    width: 98%;
	line-height: 20px; 
	font-weight: bold;
	text-align: left;	
	font-size: 11px;
	font-family: arial;
	}
	
.warningField {
    background-color: #f5a9a9;
}
</style>

<script type="text/javascript">
	var mostrarListadoReclamacionesPath = '<s:url action="mostrarReclamaciones" namespace="/siniestros/juridico"/>';
	var listarReclamacionReservasPath = '<s:url action="listarReservas" namespace="/siniestros/juridico"/>';
	var guardarReclamacionOficioPath = '<s:url action="guardarOficio" namespace="/siniestros/juridico"/>';
	var listarOficiosPath = '<s:url action="listarOficios" namespace="/siniestros/juridico"/>';
	var notificarBloqueoAseguradoPath = '<s:url action="notificarBloqueoAsegurado" namespace="/siniestros/juridico"/>';
	var notificarModificacionReservaPath = '<s:url action="notificarModificacionReserva" namespace="/siniestros/juridico"/>';
	var exportarListadoOficiosPath = '<s:url action="exportarListadoOficios" namespace="/siniestros/juridico"/>';
</script>

<s:form id="reclamacionOficioForm" >
		<s:hidden id="esConsulta" name="esConsulta"/>
		<s:hidden id="numeroReclamacion" name="reclamacionOficio.numeroReclamacion"/>
		<s:hidden id="numeroOficio" name="reclamacionOficio.numeroOficio"/>
		<s:hidden id="tipoReclamacion" name="reclamacionOficio.tipoReclamacion"/>
		<s:hidden id="siniestroId" name="reclamacionOficio.siniestroId"/>
		<s:hidden id="mostrarListadoVacio" name="mostrarListadoVacio"/>
		<s:hidden id="actorPromovida" value="%{getText('midas.siniestros.juridico.actor.segurosafirme')}" />
		<s:hidden id="existePoliza" name="existePoliza"/>
		<s:hidden id="deshabilitarBusquedaSiniestro" name="deshabilitarBusquedaSiniestro"/>
		<s:hidden id="esPantallaAgregarOficio" name="esPantallaAgregarOficio"/>
		<s:hidden id="oficioEstatus" name="reclamacionOficio.estatus"/>
		<s:hidden id="estatusNotificacionReserva" name="estatusNotificacionReserva"/>
		
		<s:hidden id="ramo" name="reclamacionOficio.ramo"/>
		<s:hidden id="numeroPoliza" name="reclamacionOficio.numeroPoliza"/>
		<s:hidden id="numeroSiniestro" name="reclamacionOficio.numeroSiniestro"/>
		<s:hidden id="oficinaId" name="reclamacionOficio.oficinaSiniestro"/>
		
		<div class="titulo" style="width: 98%;">
			<s:text name="%{'midas.siniestros.juridico.titulooficio.registro.' + reclamacionOficio.tipoReclamacion}"/>	
		</div>	
		
		<div id="contenedorFiltros" style="width: 98%;">
		<table id="agregar" border="0">
			<tbody>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.numreclamacion" /> </td>
					<td><s:textfield value="%{reclamacionOficio.numeroReclamacion}"  cssClass="cajaTexto w150 alphaextra" disabled="true" ></s:textfield></td>
					
					<td><s:text name="midas.siniestros.juridico.numoficio" /> </td>
					<td><s:textfield value="%{reclamacionOficio.numeroOficio}"  cssClass="cajaTexto w150 alphaextra" disabled="true" ></s:textfield></td>
					
					<td><s:text name="midas.siniestros.juridico.tiporeclamacion" /> </td>
					<td>
					<s:select list="listaTiposReclamacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					value="%{reclamacionOficio.tipoReclamacion}" id="tipoReclamacion_s" cssClass="cajaTextoM2 w150" onchange="" disabled="true" />
					</td>
				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.oficinajuridica" /> </td>
					<td>
					<s:select list="listaOficinasJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.oficinaJuridica" id="oficinaJuridica_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="" />
					</td>
					
					<s:if test="reclamacionOficio.tipoReclamacion == 'LI'">
						<td><s:text name="midas.siniestros.juridico.partejucio" /> </td>
						<td>
						<s:select list="listaPartesEnJuicio" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="reclamacionOficio.parteEnJuicio" id="parteEnJuicio_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="onChangeParteEnJuicio();" />
						</td>
					</s:if>
					<s:else>
						<td><s:text name="%{'midas.siniestros.juridico.reclamante.' + reclamacionOficio.tipoReclamacion}" /> </td>
						<td><s:textfield name="reclamacionOficio.reclamante"  cssClass="cajaTexto w150 jQalphaextra esConsulta" maxlength="82" ></s:textfield></td>
					</s:else>
					
					<td>
					<div id="fechaNotificacion"><s:text name="midas.siniestros.juridico.fechanotificacion" /></div>
					<div id="fechaNotificacion_contra" style="display:none;"><s:text name="midas.siniestros.juridico.fechanotificacion.encontra" /></div>
					<div id="fechaNotificacion_promovida" style="display:none;"><s:text name="midas.siniestros.juridico.fechanotificacion.promovida" /></div> 
					</td>
					<td  align="left"> <sj:datepicker name="reclamacionOficio.fechaNotificacion"
					id="fechaNotificacion_t" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					cssClass="cajaTextoM2 w150 esConsulta" 
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
					</td>
				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.asegurado" /> </td>
					<td><s:textfield name="reclamacionOficio.asegurado"  cssClass="cajaTexto w150 jQalphaextra esConsulta " maxlength="82"></s:textfield></td>
				
					<s:if test="reclamacionOficio.tipoReclamacion == 'LI'">
						<td><s:text name="midas.siniestros.juridico.actor" /> </td>
						<td><s:textfield id="actor_t" name="reclamacionOficio.actor"  cssClass="cajaTexto w150 jQalphaextra esConsulta" maxlength="82" ></s:textfield></td>
						
						<td><s:text name="midas.siniestros.juridico.demandado" /> </td>
						<td><s:textfield name="reclamacionOficio.demandado"  cssClass="cajaTexto w150 jQalphabeticExt jQrestrict esConsulta" maxlength="20" ></s:textfield></td>
					</s:if>
					<s:elseif test="reclamacionOficio.tipoReclamacion == 'GE'">
						<td><s:text name="midas.siniestros.juridico.tipoqueja" /> </td>
						<td>
						<s:select list="listaTiposQueja" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="reclamacionOficio.tipoQueja" id="tipoQueja_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="" />
						</td>
						
						<td><s:text name="midas.siniestros.juridico.clasificacion" /> </td>
						<td>
						<s:select list="listaClasificacionesReclamacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="reclamacionOficio.clasificacionReclamacion" id="clasificacionReclamacion_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="" />
						</td>
					</s:elseif>
					<s:else>
						<td></td>
						<td></td>
						
						<td></td>
						<td></td>
					</s:else>
					
				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.ramo" /> </td>
					<td>
					<s:select list="listaRamos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					value="%{reclamacionOficio.ramo}" id="ramo_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="onChangeRamo();" />
					</td>
					
					<td><s:text name="midas.siniestros.juridico.montoreclamado" /> </td>
					<td><s:textfield id="montoReclamado_t" name="reclamacionOficio.montoReclamado"  cssClass="cajaTexto w150 esConsulta formatCurrency" onkeypress="return soloNumeros(this, event, true, true);" onblur="validaMaximoMontoReclamacion();" onfocus="removeCurrencyFormatOnTxtInput();"></s:textfield></td>
				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.numpoliza" /> </td>
					<td><s:textfield id="numeroPoliza_t" value="%{reclamacionOficio.numeroPoliza}"  cssClass="cajaTexto w150 alphaextra ramoSelecionado esConsulta" readonly="true"
					onblur="onBlurNumeroPoliza();" onchange="validarNumeroPoliza();"></s:textfield></td>
					
					<td><s:text name="midas.siniestros.juridico.numsiniestro" /> </td>
					<td>
					<div id="numeroSiniestroContainer" style="display: inline; float: left;">
					<s:textfield id="numeroSiniestro_t" value="%{reclamacionOficio.numeroSiniestro}"  cssClass="cajaTexto w150 alphaextra ramoSelecionado esConsulta" readonly="true"
					onblur="onBlurNumeroSiniestro();"></s:textfield>
					</div>
					<div id="btn_busquedaSiniestro" class="btn_back w20" style="display: inline; float: left;">
						<a href="javascript: void(0);" onclick="javascript:mostrarBuscarSiniestro();" > 
							<img border='0px' alt='Buscar Siniestro' title='Buscar Siniestro' src='/MidasWeb/img/b_ico_busq.gif'/>
						</a>
					</div>
					</td>
					
					<td><s:text name="midas.siniestros.juridico.oficinasiniestro" /> </td>
					<td>
					<s:select list="listaOficinasSiniestro" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					value="%{reclamacionOficio.oficinaSiniestro}" id="oficinaSiniestro_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="onChangeOficinaSiniestro();" disabled="true"/>
					</td>
				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.asignado" /> </td>
					<td><s:textfield name="reclamacionOficio.asignado"  cssClass="cajaTexto w150 jQalphaextra esConsulta" maxlength="82" ></s:textfield></td>
					
					<td><s:text name="midas.siniestros.juridico.procedimiento" /> </td>
					<td>
					<s:select list="listaProcedimientos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.procedimiento" id="procedimiento_s" cssClass="cajaTextoM2 w150 esConsulta" onchange=""/>
					</td>
					
					<td><s:text name="midas.siniestros.juridico.expjuridico" /> </td>
					<td><s:textfield name="reclamacionOficio.expedienteJuridico"  cssClass="cajaTexto w150 jQalphaextra esConsulta" maxlength="50" ></s:textfield></td>
				</tr>
				
				<tr>
					<td colspan="2"><s:text	 name="%{'midas.siniestros.juridico.entidadfederativa.' + reclamacionOficio.tipoReclamacion}" /> </td>
					
					<td><s:text name="midas.siniestros.juridico.estado" /></td>
					<td>
					<s:select list="listaEstados" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.estado" id="estado_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="onChangeEstado('municipio_s');"/>
					</td>
					
					<td><s:text name="midas.siniestros.juridico.municipio" /> </td>
					<td>
					<s:select list="listaMunicipios" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.municipio" id="municipio_s" cssClass="cajaTextoM2 w150 esConsulta" onchange=""/>
					</td>

				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.delegacion" /> </td>
					<td>
					<s:select list="listaDelegaciones" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.delegacion" id="delegacion_s" cssClass="cajaTextoM2 w150 esConsulta" onchange=""/>
					</td>
					
					<td><s:text name="midas.siniestros.juridico.motivorecalmacion" /></td>
					<td>
					<s:select list="listaMotivos" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="reclamacionOficio.motivoReclamacion" id="motivo_s" cssClass="cajaTextoM2 w150 esConsulta" onchange=""/>
					</td>
				</tr>
				
				<tr>
					<td><s:text name="midas.siniestros.juridico.estatus" /> </td>
					<td>
					<s:select list="listaEstatus" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					value="%{reclamacionOficio.estatus}" id="estatus_s" cssClass="cajaTextoM2 w150 esConsulta" onchange="onChangeEstatus();"/>
					</td>
					
					<s:if test="reclamacionOficio.tipoReclamacion == 'LI'">
						<td><s:text name="midas.siniestros.juridico.etapajuicio" /></td>
						<td>
						<s:select list="listaEtapasDeJuicio" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="reclamacionOficio.etapaJuicio" id="etapaJuicio_s" cssClass="cajaTextoM2 w150 esConsulta" onchange=""/>
						</td>
					
						<td><s:text name="midas.siniestros.juridico.probabilidadexito" /> </td>
						<td>
						<s:select list="listaProbabilidadesExito" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="reclamacionOficio.probabilidadExito" id="probabilidadExito_s" cssClass="cajaTextoM2 w150 esConsulta" onchange=""/>
						</td>
					</s:if>
					<s:else>
						<td></td>
						<td></td>
						
						<td></td>
						<td></td>
					</s:else>
				</tr>
				
				<tr>
					<td colspan="6" >
						<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
							<tbody>
								<tr>
									<td><label class="labelBlack" style="text-align: left;" ><s:text name="%{'midas.siniestros.juridico.comentarios.' + reclamacionOficio.tipoReclamacion}" /></label></td>
								</tr>
								<tr>
									<td>
										<div id="contenedorTextArea">
											<s:textarea name="reclamacionOficio.justificacion" id="comentarios_a" 
											cssClass="textarea deshabilitar limited esConsulta" cssStyle="font-size: 10pt;"
											cols="80" rows="3" onkeypress="return limiteMaximoCaracteres(this.value, event, 3000)" onchange="truncarTexto(this,3000);"/>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
		
	<br/>
		
	<s:if test="reclamacionOficio.tipoReclamacion == 'LI' || reclamacionOficio.tipoReclamacion == 'CO'">
		<div class="titulo listadoReservas" style="width: 98%;display:none;" >
			<s:text name="%{'midas.siniestros.juridico.titulooficio.reservas.' + reclamacionOficio.tipoReclamacion}"/>	
		</div>
		<div id="indicadorReservas"></div>
		<div id="reclamacionReservasGridContainer" style="display:none;" class="listadoReservas">
			<div id="listadoReclamacionReservasJuridicoGrid" style="width:98%;height:200px;"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
		</div>
		<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
			<tbody>
				<tr>
					<td>
						<div id="btn_notificacion" class="btn_back w140 mostrarEnEdicionDeReserva" style="display: none; float: right;" >
							<a href="javascript: void(0);" onclick="javascript:notificarModificacionReservas();"> 
								<s:text name="midas.siniestros.juridico.notificacion" /> 
							</a>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</s:if>
	
	<br/>
	
	<div class="titulo listadoOficios" style="width: 98%;display:none;">
		<s:text name="%{'midas.siniestros.juridico.titulooficio.listado.' + reclamacionOficio.tipoReclamacion}"/>
	</div>	
	<div id="indicadorOficios"></div>
	<div id="oficiosGridContainer" style="display:none;" class="listadoOficios">
		<div id="listadoReclamacionOficiosJuridicoGrid" style="width:98%;height:200px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
		
</s:form>

<br/>
	<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
		<tbody>
			<tr>
				<td>
					<div id="btn_guardar" class="btn_back w140 noEsConsulta" style="display: none; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:guardarOficio();"> 
							<s:text name="midas.boton.guardar" /> 
							<img border='0px' alt='Guardar Oficio' title='Guardar Oficio' src='/MidasWeb/img/common/btn_guardar.jpg'/>
						</a>
					</div>
					<div id="btn_cerrar" class="btn_back w140" style="display: inline; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:mostrarListadoReclamaciones();"> 
							<s:text name="midas.boton.cerrar" /> 
							<img border='0px' alt='Cerrar' title='Cerrar' src='/MidasWeb/img/b_borrar.gif'/>
						</a>
					</div>
					<div id="btn_bloquearasegurado" class="btn_back w140 noEsConsulta" style="display: none; float: right;" >
						<a href="javascript: void(0);" onclick="javascript:bloquearAsegurado();"> 
							<s:text name="midas.siniestros.juridico.bloquearasegurado" /> 
						</a>
					</div>
					<div class="btn_back w120" style="display: inline; float: right;"  >
						<a href="javascript: void(0);" onclick="exportarListadoOficios();">
							<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Reclamaciones' title='Reclamaciones' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
						</a>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

<script src="<s:url value='/js/midas2/juridico/reclamacionesJuridico.js'/>"></script>

<script type="text/javascript">
	jQuery(document).ready(
	function(){
	 	inicializarReclamacionOficio();
	 	initCurrencyFormatOnTxtInput();
 	}
 );
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>