<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<style type="text/css">
table#agregar th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
</style>
<s:form id="citaJuridicoForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.juridico.citas.mostrarCitas.titulo"/>	
</div>	
<s:hidden name="cita.id" id="citaId"/>
<s:hidden name="soloLectura" id="soloLectura"/>
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar">
			<tbody>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.fechaAlta"/>
					</th>
					<td>
						<s:textfield id="fechaCreacion" disabled="true" name="cita.fechaCreacion" cssClass="cajaTextoM2 w100" />					 
					</td>
				</tr>
				<tr >
					<th>
						<s:text name="midas.juridico.citas.oficina"/>
					</th>					
					<td>
						<s:select list="listaOficinasJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="cita.oficina.id" id="listaOficinas" required="true" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" />					 
					</td>
					<th>
						<s:text name="midas.juridico.citas.numCita"/>
					</th>
					<td>
						<s:textfield id="numCita" disabled="true" name="cita.id" cssClass="cajaTextoM2 w100"/>					 
					</td>					
					<th>
						<s:text name="midas.juridico.citas.fechaCita"/>
					</th>
					<td>
						<table id="desplegarDetalle">
						<tr>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.de"/>	
						</th>						
						<td>
						<s:if test="soloLectura==true">
							<s:textfield id="fechaCitaInicioTF" disabled="true" name="cita.fechaInicio" cssClass="cajaTextoM2 w100" 
											 readOnly="%{soloLectura}" disabled="%{soloLectura}" />					
						</s:if>
						<s:else>						
							<sj:datepicker 
								name="cita.fechaInicio"
								cssStyle="width: 80px;" required="#requiredField" labelposition="top"
								buttonImage="../img/b_calendario.gif" id="fechaCitaInicio"
								changeMonth="true" changeYear="true" maxlength="10" minDate="today" 
								cssClass="txtfield jQrequired" size="12" readOnly="%{soloLectura}" disabled="%{soloLectura}"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</s:else>	
						<td>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.a"/>
						</th>						
						<td>
						<s:if test="soloLectura==true">
							<s:textfield id="fechaCitaFinTF" disabled="true" name="cita.fechaFin" cssClass="cajaTextoM2 w100" 
										 readOnly="%{soloLectura}" disabled="%{soloLectura}" />					
						</s:if>
						<s:else>						
							<sj:datepicker 
								name="cita.fechaFin"
								cssStyle="width: 80px;" required="#requiredField" labelposition="top"
								buttonImage="../img/b_calendario.gif" id="fechaCitaFin" 
								changeMonth="true" changeYear="true" maxlength="10" minDate="today" 
								cssClass="txtfield jQrequired" size="12" readOnly="%{soloLectura}" disabled="%{soloLectura}"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
							</sj:datepicker>
						</s:else>	
						<td>
						</tr>						
						</table>					 
					</td>				
				</tr>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.nombreUsuario"/>
					</th>
					<td>	
						<s:select list="listaUsuariosJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="cita.nombreUsuario" id="nombreUsuario" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" />				 
					</td>
					<th>
						<s:text name="midas.juridico.citas.asignar"/>
					</th>
					<td>
						<s:textfield id="asignar" name="cita.asignado" required="true" cssClass="cajaTextoM2 jQrequired jQalphanumeric w200" disabled="%{soloLectura}"/>					 
					</td>
					<th>
						<s:text name="midas.juridico.citas.estatus"/>
					</th>
					<td>
						<s:select list="listaEstatusCitaJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="cita.estatus" id="estatus" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" />						 
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.tipoReclamacion"/>
					</th>
					<td>
						<s:select list="listaTiposReclamacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="cita.tipoReclamacion" id="tipoReclamacion" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" />						 
					</td>
					<th>
						<s:text name="midas.juridico.citas.lugarCita"/>
					</th>
					<td>
						<s:textfield id="lugarCita" name="cita.lugarCita" required="true" cssClass="cajaTextoM2 w200 jQrequired jQalphanumeric" 
									disabled="%{soloLectura}" />						 
					</td>
					<th>
						<s:text name="midas.juridico.citas.ramo"/>
					</th>
					<td>
						<s:select list="listaRamosJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="cita.ramo" id="ramo" readOnly="%{soloLectura}" disabled="%{soloLectura}"
						cssClass="cajaTexto jQrequired w200 campoForma" />							 
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.motivo"/>
					</th>
					<td colspan="5">
						<s:textarea cols="5" rows="5" id="motivo" name="cita.motivo" cssClass="cajaTextoM2 w850" disabled="%{soloLectura}"/>					 
					</td>
				</tr>
			</tbody>
	</table>		
</div>
<div id="contenedorFiltros" style="width: 97%;">
	<table id="btnGuardar" border="0" align="right">
			<tbody>
				<tr>
					<td>
						<div align="right">
						<div class="btn_back w110" id="cerrarBoton" >
							<a href="javascript: void(0);"
								onclick="regresarListadoCitasJuridico();">
								<s:text name="midas.boton.cerrar"/>
							</a>
						</div>
						</div>
					</td>
					<s:if test="soloLectura==false">
					<td>
						<div align="right">
						<div class="btn_back w110" id="nuevoBoton" >
							<a href="javascript: void(0);"
								onclick="guardarCitaJuridico();">
								<s:text name="midas.boton.guardar"/>
							</a>
						</div>
						</div>
					</td>
					</s:if>
				</tr>
			</tbody>
	</table>		
</div>
</s:form>