<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="oficina" type="ro" width="100" sort="int"><s:text name="midas.juridico.citas.oficina" /></column>		
		<column id="numeroCita" type="ro" width="70" sort="int" align="center"><s:text name="midas.juridico.citas.numCita" /></column>
		<column id="nombreUsuario" type="ro" width="100" sort="str" align="center"><s:text name="midas.juridico.citas.nombreUsuario" /></column>
		<column id="asignadoA" type="ro" width="150" sort="str"><s:text name="midas.juridico.citas.asignar"/></column>
		<column id="fechaAlta"  type="ro" width="80" sort="date_custom" align="center"><s:text name="midas.juridico.citas.fechaAlta" /></column>
		<column id="fechaCita"  type="ro" width="80" sort="date_custom" align="center"><s:text name="midas.juridico.citas.fechaCita" /></column>		
		<column id="tipoReclamacion" type="ro" width="120" sort="str"><s:text name="midas.juridico.citas.tipoReclamacion"/></column>
		<column id="lugarCita" type="ro" width="*" sort="str"><s:text name="midas.juridico.citas.lugarCita"/></column>
		<column id="ramoJuridico" type="ro" width="105" sort="str"><s:text name="midas.juridico.citas.ramo"/></column>
		<column id="estatus" type="ro" width="150" sort="str" align="center"><s:text name="midas.juridico.citas.estatus" /></column>
	    <column id="consultar" type="img" width="30" align="center"></column>		
	    <column id="editar" type="img" width="30" align="center"></column>
		<column id="eliminar" type="img" width="30" align="center"></column>				
	</head>	  		
	<s:iterator value="listadoCitas" status="stats">
		<row id="<s:property value="id"/>">
		    <cell><s:property value="oficina.nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="nombreUsuario" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="asignado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false"/></cell>
			<cell><s:property value="fechaInicio" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="descripcionTipoReclamacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="lugarCita" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="descripcionRamo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionEstatus" escapeHtml="false" escapeXml="true" /></cell>			
				<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar^javascript: verCitaJuridico(<s:property value="id"/>,true)^_self</cell>
				<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: verCitaJuridico(<s:property value="id"/>,false)^_self</cell>
				<s:if test="estatus != \"C\"">			
					<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarCita(<s:property value="id"/>)^_self</cell>
				</s:if>
				<s:else>
					<cell>../img/pixel.gif^Eliminar Deshabilitado (Cita Concluida)^^_self</cell>
				</s:else>		
		</row>
	</s:iterator>	

</rows>
