<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:include value="/jsp/juridico/citas/citaJuridicoHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<style type="text/css">
table#agregar th {
	text-align: left;
	font-weight:normal;
	padding: 3px;
	}
</style>
<s:form id="listadoCitasJuridicoForm" >
<div class="titulo" style="width: 98%;">
	<s:text name="midas.juridico.citas.listadoCitas.titulo"/>	
</div>	
<div id="contenedorFiltros" style="width: 98%;">
	<table id="agregar">
			<tbody>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.oficina"/>
					</th>					
					<td>
						<s:select list="listaOficinasJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="citaJuridicoFiltro.idOficina" id="listaOficinas" 
						cssClass="cajaTexto w200 campoForma" />					 
					</td>
					<th>
						<s:text name="midas.juridico.citas.numCita"/>
					</th>
					<td>
						<s:textfield id="numeroCita" name="citaJuridicoFiltro.numeroCita" cssClass="cajaTextoM2 w100 jQnumeric jQrestrict" />					 
					</td>
					<th>
						<s:text name="midas.juridico.citas.nombreUsuario"/>
					</th>					
					<td>	
						<s:select list="listaUsuariosJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="citaJuridicoFiltro.nombreUsuario" id="nombreUsuario" 
						cssClass="cajaTexto w200 campoForma" />				 
					</td>
				</tr>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.fechaAlta"/>
					</th>
					<td>
						<table id="desplegarDetalle">
						<tr>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.de"/>	
						</th>
						<td>
						<sj:datepicker 
							name="citaJuridicoFiltro.fechaAltaInicio"
							cssStyle="width: 80px;" required="#requiredField"
							buttonImage="../img/b_calendario.gif" id="fechaInicio"
							changeMonth="true" changeYear="true" maxlength="10"
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>	
						<td>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.a"/>	
						</th>
						<td>
						<sj:datepicker 
							name="citaJuridicoFiltro.fechaAltaFin"
							cssStyle="width: 80px;" required="#requiredField"
							buttonImage="../img/b_calendario.gif" id="fechaFin"
							changeMonth="true" changeYear="true" maxlength="10"
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
						<td>
						</tr>						
						</table>					 
					</td>			
					<th>
						<s:text name="midas.juridico.citas.fechaCita"/>
					</th>
					<td colspan=3">
						<table id="desplegarDetalle">
						<tr>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.de"/>	
						</th>
						<td style="width:130px;">
						<sj:datepicker 
							name="citaJuridicoFiltro.fechaCitaInicio"
							cssStyle="width: 80px;" required="#requiredField"
							buttonImage="../img/b_calendario.gif" id="fechaInicio2"
							changeMonth="true" changeYear="true" maxlength="10" 
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>	
						<td>
						<th>
							<s:text name="midas.juridico.citas.fechaCita.a"/>	
						</th>
						<td>
						<sj:datepicker 
							name="citaJuridicoFiltro.fechaCitaFin"
							cssStyle="width: 80px;" required="#requiredField"
							buttonImage="../img/b_calendario.gif" id="fechaFin2"
							changeMonth="true" changeYear="true" maxlength="10" 
							cssClass="txtfield" size="12"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)">
						</sj:datepicker>
						<td>
						</tr>						
						</table>					 
					</td>			
				</tr>
				<tr>
					<th>
						<s:text name="midas.juridico.citas.tipoReclamacion"/>
					</th>					
					<td>
						<s:select list="listaTiposReclamacion" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="citaJuridicoFiltro.tipoReclamacion" id="tipoReclamacion" 
						cssClass="cajaTexto w200 campoForma" />						 
					</td>
					<th>
						<s:text name="midas.juridico.citas.ramo"/>
					</th>
					<td>
						<s:select list="listaRamosJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="citaJuridicoFiltro.claveRamoJuridico" id="ramo" 
						cssClass="cajaTexto w200 campoForma" />							 
					</td>
					<th>
						<s:text name="midas.juridico.citas.estatus"/>
					</th>
					<td>
						<s:select list="listaEstatusCitaJuridico" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
						name="citaJuridicoFiltro.claveEstatusJuridico" id="estatus" 
						cssClass="cajaTexto w200 campoForma" />						 
					</td>				
				</tr>
				<tr>		
				    <td colspan="4">			
					<td colspan="2">
						<table align="right">
							<tr>
								<td align="right">
									<div class="btn_back w110" id="botonLimpiar" >
										<a href="javascript: void(0);"
										   onclick="limpiarFiltrosBusqueda('contenedorFiltros');">
								         <s:text name="midas.boton.limpiar"/>
										</a>
									</div>									
								</td>
								<td align="right" style="width:50px;">
									<div class="btn_back w110" id="botonBuscar" >
										<a href="javascript: void(0);"
										   onclick="buscarCitasPaginado(1, true);">
											<s:text name="midas.boton.buscar"/>
										</a>
									</div>									
								</td>
							</tr>
						</table>						
					</td>					
				</tr>
			</tbody>
	</table>
</div>	
</s:form>			
<div id="spacer1" style="height: 10px"></div>

<div id="tablaCitasJuridicoFiltros">
    <!-- Tabla   -->
    <div id="indicador"></div>
	<div id="gridCitasJuridicoPaginado">
		<div id="gridCitasJuridicoListadoGrid" style="width:96%;height:300px">
	</div>
	<div id="pagingArea"></div><div id="infoArea"></div>
    </div>
</div>
<div id="spacer1" style="height: 15px"></div>
<div id="contenedorBotonesFiltros" style="width: 97%;">
	<table id="btnNuevo" border="0" align="right">
			<tbody>
				<tr>
					<td>
						<div align="right">
						<div class="btn_back w110" id="nuevoBoton" >
							<a href="javascript: void(0);"
								onclick="crearCitaJuridico();">
								<s:text name="midas.boton.nuevo"/>
							</a>
						</div>
						</div>
					</td>
					<td>
						<div align="right">
						<div class="btn_back w110" id="exportarExcelBoton" >
							<a href="javascript: void(0);"
								onclick="exportarExcel();">
								<s:text name="midas.boton.exportarExcel"/>
							</a>
						</div>
						</div>
					</td>
				</tr>
			</tbody>
	</table>		
</div>
<script type="text/javascript">
	buscarCitasPaginado(1, true);
</script>