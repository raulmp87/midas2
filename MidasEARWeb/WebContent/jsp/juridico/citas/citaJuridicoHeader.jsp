<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/juridico/citas/citaJuridico.js'/>"></script>

<script type="text/javascript">
    var pathMostrarListadoCitasJuridico='/MidasWeb/juridico/citas/mostrarListadoCitas.action';
	var pathMostrarCitaJuridico='/MidasWeb/juridico/citas/mostrarCita.action';
	var pathGuardarCitaJuridico='/MidasWeb/juridico/citas/guardarCita.action';
	var pathBuscarCitasJuridico='/MidasWeb/juridico/citas/buscarCitas.action';
	var pathBuscarCitasJuridicoPaginado='/MidasWeb/juridico/citas/buscarCitasPaginacion.action';
	var pathEliminarCitaJuridico='/MidasWeb/juridico/citas/eliminarCita.action';
	var pathExportarCitaExcel='/MidasWeb/juridico/citas/exportarCitas.action';
</script>