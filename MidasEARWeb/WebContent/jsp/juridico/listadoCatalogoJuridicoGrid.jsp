<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>15</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="numero" type="ro" width="*" sort="int" ><s:text name="%{'midas.siniestros.juridico.catalogo.numero'+tipoCatalogo}"/></column>	
        <column id="nombre" type="ro" width="*" sort="str" ><s:text name="%{'midas.siniestros.juridico.catalogo.nombre'+tipoCatalogo}"/></column>
        <column id="estatus " type="ro" width="100" sort="str" ><s:text name="%{'midas.siniestros.juridico.catalogo.estatus'}"/> </column>
		<column id="oficina" type="ro" width="*" sort="str"><s:text name="%{'midas.siniestros.juridico.catalogo.oficina'}"/> </column>
		<column id="fechaA" type="ro" width="*" sort="date_custom"><s:text name="%{'midas.siniestros.juridico.catalogo.fechaA'}"/></column>	
		<column id="fechaI" type="ro" width="*" sort="date_custom"><s:text name="%{'midas.siniestros.juridico.catalogo.fechaI'}"/> </column>	
		<m:tienePermiso nombre="FN_M2_SN_Indem_Determinancion_PT">
		<column id="editar" type="img" width="50" sort="na" align="center">Editar</column>
		</m:tienePermiso>
  	</head>      
   <s:iterator value="listaCatalogo" status="row">
		<row id="<s:property value="#row.index"/>">			
			  	<cell><s:property value="numero" escapeHtml="true" escapeXml="true"/></cell>	
				<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="estatusDesc" escapeHtml="false" escapeXml="true"/></cell>				
				<cell><s:property value="nombreOficina" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="fechaActivo" escapeHtml="false" escapeXml="true"/></cell>	
				<cell><s:property value="fechaInactivo" escapeHtml="false" escapeXml="true"/></cell>
				<m:tienePermiso nombre="FN_M2_SN_Indem_Determinancion_PT">		
				<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: editar("<s:property value="tipoCatalogo" escapeHtml="false" escapeXml="true"/>", <s:property value="id" escapeHtml="false" escapeXml="true"/> )^_self</cell>	
				</m:tienePermiso>
				
		</row>
	</s:iterator>
	
</rows>
   
