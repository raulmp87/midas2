<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<div id="mensajeImg">
	<img src='/MidasWeb/img/b_no.jpg'>
</div>
<div id="mensajeGlobal" class="mensaje_texto">
	<s:text name="midas.componente.error.message"/>
</div>

<script>
jQuery(document).ready(function(){
	
	var win = parent.mainDhxWindow.window("vm_estimacion");
	if(win != null){
		win.button("close").show();
	}
});
</script>