<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<%-- <script src="<s:url value='/js/midas2/componente/componente.js'/>"></script> --%>

<script type="text/javascript">
	var mostrarContenedorRegDinPath = '<s:url action="mostrarContenedor" namespace="/controlDinamico"/>';
	var listarRegistrosDinPath = '<s:url action="listadoRegistros" namespace="/controlDinamico"/>';
 	var verDetalleRegDinPath = '<s:url action="verDetalle" namespace="/controlDinamico"/>';
 	var guardarRegistroDinamicoPath = '<s:url action="actualizarRegistro" namespace="/controlDinamico"/>';
// 	var verDetalleRegDinPath = '<s:url action="verDetalle" namespace="/controlDinamico"/>'; /* VARIABLE REPETIDA.*/
 	var PREFIX=null;
// 	var guardarPaquetePath = '<s:url action="guardar" namespace="/paquete"/>';
// 	var eliminarPaquetePath = '<s:url action="eliminar" namespace="/paquete"/>';
// 	var mostrarCatalogoPaquetePath = '<s:url action="mostrarCatalogo" namespace="/paquete"/>';
</script>

<s:form action="listar" id="paqueteForm">
	<s:hidden name="claveTipoVista" id="claveTipoVista" />
	<s:hidden name="clave" id="clave" />	
	<s:if test="clave == \"VMP_\"">
		<div  class="titulo w900">Listado Variables Primas</div>
		<br></br>
		<div id="indicador"></div>
		<div id ="registrosDinGrid" style="width:97%;height:350px"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br></br>
	</s:if>
	<s:else>
		<br></br>
		<div id="indicador"></div>
		<div id ="registrosDinGrid" style="width:97%;height:200px"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
		<br></br>
	</s:else>
	
	
	<div style="float:right; margin-right: 30px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript:verDetalleRegistroDinamico(1,getClave());return false;">
				<s:text name="midas.boton.agregar"/>
			</a>
		</div>
	</div>
</s:form>