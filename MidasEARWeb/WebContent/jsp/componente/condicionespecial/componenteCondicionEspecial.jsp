<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
 
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/suscripcion/cotizacion/auto/complementar/complementarEmision/complementarEmisionHeader.jsp"/>
<script	src="<s:url value='/js/midas2/componente/condicionespecial/componenteCondicionEspecial.js'/>"></script>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>
<div id="contenido_condicionesEspeciales">
	<s:hidden id="idToCotizacion" name="idToCotizacion" />
	<s:hidden id="idToNegocio" name="idToNegocio"/>
	<s:hidden id="esFiltrado" name="esFiltrado"/>
	<s:hidden id="numeroInciso" name="numeroInciso"/>	
	<s:hidden id="numeroSecuencia" name="numeroSecuencia"/>
	<s:hidden id="tipoLlamada" name="tipoLlamada"/>
	
	<div class="titulo">Condiciones Especiales</div>
	<div  id="filtros" class="c12">
		<table id="agregar" style="max-width: 840px;" class="hdr">
		<tr>
			<td><label class="label">Código o Condición Especial</label></td>
			<td><s:textfield id="codigoCondicionEspecial" name="condicionEspecial.codigo" cssClass="cajaTexto" cssStyle="width:250px"/></td>
			<td><div class="btn_back w100">
					<a id="submit" href="javascript: void(0);" onclick="searchCondicionesEspeciales();" class="icon_buscar">Buscar</a>
				</div>
			</td>
		</tr>
		</table>

	</div>

	<s:form id="condicionEspecialForm">
		<div id="divM">
			<div style="width: 44%; height: 100%; float: left; position: relative;margin-left:2px;" id="divII">
				<div class="subtituloIzquierdaDiv">Disponibles</div>
				<div id="condicionesEspecialesDisponiblesGrid" style="width: 100%; height: 180px;"></div>
			</div>
			<div style="margin-left:2px; width: 8%; height: 180px; float: left; position: relative; top:90px" id="divID">
				<div class="btn_back w55">
					<a id="submit" href="javascript: void(0);" onclick="asociarTodas();">Todas &gt;&gt;</a>
				</div>
				<div class="btn_back w55">
					<a id="submit" href="javascript: void(0);" onclick="borrarAsociadas();">&lt;&lt; Todas</a>
				</div>
			</div>
			<div style="margin-left:5px; width: 44%; height: 100%; float: left; position: relative;" id="divID">
				<div class="subtituloIzquierdaDiv">Asociadas</div>
				<div id="condicionesEspecialesAsociadasGrid" style="width:100%;height:180px;"></div>
			</div>
		</div>
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</s:form>
</div>

<style type="text/css">
	#contenido_condicionesEspeciales{
		margin-left: 5px; 
		margin-top: 3px; 
		width: 99%; 
		height: 95%;
	}
	
	#divS{
		float: left; 
		width: 100%; 
		height: 20%;
	}
	
	#divM{
		float: left; 
		width: 100%; 
		height: 50%;
	}
	
	#divI{
		margin-top:10px;
		float: left; 
		width: 98.7%; 
		height: 15%;
	}
	
</style>

<script type="text/javascript">
	initGridsComponenteCondicionEspecial();
</script>