<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="condicionEspecial.id" type="ro" width="0" sort="int" hidden="true">id</column>
	    <column id="condicionEspecial.nombre" type="ro"  width="*"  sort="str">Nombre</column>
		<column id="condicionEspecial.codigo" type="ro"  width="60" align="center"  sort="str" >Codigo</column>
	</head>
	<% int a=0;%>
	<s:iterator value="condicionesDisponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
		    <cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigo" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>