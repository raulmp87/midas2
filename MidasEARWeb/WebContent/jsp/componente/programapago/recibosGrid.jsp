<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- <%@taglib prefix="sj" uri="/struts-jquery-tags" %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<!-- SE ANULA CARGA DE COMPONENTE PROGRAMA PAGO
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/componenteProgramaPago.js"/>"></script>
 -->
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>


<link href="<s:url value="/css/programaPago.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/paginacionProgramaPago.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxdataprocessor.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_drag.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_link.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptDataGrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/programaPago.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dateUtils.js"/>"></script>
	


<s:form id="mostrarRecibos" name="mostrarRecibos">
<s:hidden name="programaPagoForm.tipoMov"/>
<s:hidden name="programaPagoForm.idToSolicitud"/>
<s:hidden name="programaPagoForm.idToPoliza"/>
	<!-- 
	<bean:define id="idCot" name="programaPagoForm" property="idToCotizacion" />
 	-->
	<div class="titulo">
		Detalle de Recibos
	</div>
	<table width="100%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				Datos del Contratante
			</td>
			<td></td>
		</tr>
		<br>

		<tr class="pf">
			<th width="105px">
				Contratante
			</th>
			<td width="315px">
				<s:property value='programaPagoForm.nombreDelContrante'/>
			</td>
		</tr>
		<tr>
			<th width="105px">
				Dirección Fiscal
			</th>
			<td width="315px" colspan="3">
				<s:property value='programaPagoForm.direccionFiscal'/>
			</td>
		</tr>
		<tr>
			<th width="105px">
				RFC
			</th>
			<td width="315px" colspan="3">
				<s:property value='programaPagoForm.rfc'/>
			</td>
		</tr>
		<tr>
		<td>
						<table style="width: 100%; float: left;">
							<tr>
								<td style="vertical-align: top;">
									<div class="alinearBotonALaDerecha">
										<div class="btn_back w280" style="display:none;">
						 					<a href="javascript: void(0);" class="icon_buscar"
						 						onclick="modificaPersona(<s:property value='programaPagoForm.idProgPago'/>,<s:property value='programaPagoForm.idToCotizacion'/>);"> 
												<s:text name="Modificar Persona del Programa de Pago"/>
											</a>
										</div>	
									</div>
								</td>
							</tr>
						</table>
					</td>
		</tr>
	</table>
	<c:if test="${!programaPagoForm.readOnly}">
		<div id="distYPersona">
			<table width="100%" id="panelDos">
				<tr>
					<td>
						<!-- TABLA DE DISTRIBUCION DE PRIMA -->
						<table style="width: 100%; float: left;">
							<tr>
								<td
									style="vertical-align: top; width: 92%; float: left; padding-left: 0px;">

									<table id="tablaDistribucionAutomatica"
										style="font-size: 9px; border: 1px solid #D3F3B4;">
										<tr>
											<td>
												Distribución de Prima Neta
											</td>
											<td>
												Automatica
											</td>
											<td>
												<input type="radio" id="distribuyeSi" name="dist"
													onclick="eventoRadio(this);" value="1" />
											</td>
											<td>
												Manual
											</td>
											<td>
												<input type="radio" id="distribuyeNo" name="dist"
													onclick="eventoRadio(this);" value="0" checked="checked" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
					<!-- TABLA DE DISTRIBUCION DE RECARGOS -->
						<table style="width: 100%; float: left;">
							<tr>
								<td
									style="vertical-align: top; width: 92%; float: left; padding-left: 0px;">

									<table id="tablaDistribucionAutomatica"
										style="font-size: 9px; border: 1px solid #D3F3B4;">
										<tr>
											<td>
												Distribución de Recargos
											</td>
											<td>
												Automatica
											</td>
											<td>
												<input type="radio" id="distribuyeRecargosSi" name="distRecargos"
													onclick="eventoRadioRecargos(this);" value="1" />
											</td>
											<td>
												Manual
											</td>
											<td>
												<input type="radio" id="distribuyeRecargosNo" name="distRecargos"
													onclick="eventoRadioRecargos(this);" value="0" checked="checked" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					
					<td>
					<!-- TABLA DE DISTRIBUCION DE DERECHOS -->
						<table style="width: 100%; float: left;">
							<tr>
								<td
									style="vertical-align: top; width: 92%; float: left; padding-left: 0px;">

									<table id="tablaDistribucionAutomatica"
										style="font-size: 9px; border: 1px solid #D3F3B4;">
										<tr>
											<td>
												Distribución de Derechos
											</td>
											<td>
												Automatica
											</td>
											<td>
												<input type="radio" id="distribuyeDerechosSi" name="distDerechos"
													onclick="eventoRadioDerechos(this);" value="1" />
											</td>
											<td>
												Manual
											</td>
											<td>
												<input type="radio" id="distribuyeDerechosNo" name="distDerechos"
													onclick="eventoRadioDerechos(this);" value="0" checked="checked" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					
					<td>
					<!-- TABLA DE DISTRIBUCION DE CORTE DE RECIBOS -->
						<table style="width: 100%; float: left;">
							<tr>
								<td
									style="vertical-align: top; width: 92%; float: left; padding-left: 0px;">

									<table id="tablaDistribucionAutomatica"
										style="font-size: 9px; border: 1px solid #D3F3B4;">
										<tr>
											<td>
												Hora de corte
											</td>
											<td>
												11:59 p.m.
											</td>
											<td>
												<input type="radio" id="tipoCorteAM" name="tipoCorte"
													onclick="eventoRadioTipoCorte(this);" value="pm" />
											</td>
											<td>
												11:59 a.m.
											</td>
											<td>
												<input type="radio" id="tipoCortePM" name="tipoCorte"
													onclick="eventoRadioTipoCorte(this);" value="am" checked="checked" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					
					<!-- 
					<td>
						<table style="width: 100%; float: right;">
							<tr>
								<td style="vertical-align: top;">
									<div class="alinearBotonALaDerecha">
										<div class="btn_back w280" style="display:none;">
						 					<a href="javascript: void(0);" class="icon_buscar"
						 						onclick="modificaPersona(<s:property value='programaPagoForm.idProgPago'/>,<s:property value='programaPagoForm.idToCotizacion'/>);"> 
												<s:text name="Modificar Persona del Programa de Pago"/>
											</a>
										</div>	
									</div>
								</td>
							</tr>
						</table>
					</td>
					-->
				</tr>
			</table>
		</div>
	</c:if>
	<div id="tablaYPag">
		<div id="paginador"></div>
		<div id="reciboListadoGrid" style="width: 100%; height: 320px"></div>
	</div>
	<br />




	<table style="width: 60%; float: right;">
		<tr>
			<td style="vertical-align: top;">
				<div class="alinearBotonALaDerecha">
					<c:if test="${!programaPagoForm.readOnly}">
					<div id="b_agregarRecibo">
						<a href="javascript: void(0);"
							onclick="javascript: agregaRecibo();"> Agregar </a>
					</div>

					<div id="b_guardarRecibos">
						<a href="javascript: void(0);"
							onclick="javascript: guardarRecibo(<s:property value='programaPagoForm.idProgPago'/>,'<s:property value='programaPagoForm.tipoMov'/>',<s:property value='programaPagoForm.idToSolicitud'/>,<s:property value='programaPagoForm.idToPoliza'/>,<s:property value='programaPagoForm.idContinuity'/>,<s:property value='programaPagoForm.tipoFormaPago'/>,<s:property value='programaPagoForm.numeroEndoso'/>);">
															   Guardar 
															   </a>
					</div>
					</c:if>
					<div id="b_regresar">
						<a href="javascript: void(0);"
							onclick="javascript: regresarProgramaPago(<s:property value='programaPagoForm.idToCotizacion'/>,'<s:property value='programaPagoForm.tipoMov'/>',<s:property value='programaPagoForm.idToSolicitud'/>,<s:property value='programaPagoForm.idToPoliza'/>,<s:property value='programaPagoForm.idContinuity'/>,<s:property value='programaPagoForm.tipoFormaPago'/>,<s:property value='programaPagoForm.numeroEndoso'/>);">
																	 Salir 
																	 </a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<c:if test="${!programaPagoForm.readOnly}">
		<table style="width: 80%; float: left;">
			<tr>
				<td class="campoRequerido">
					* La columna <strong>Dist. en Prima</strong> permite discriminar e integrar recibos al realizar la distribucion automatica de primas.
				</td>
			</tr>
		</table>
	</c:if>
</s:form>
<script type="text/javascript">
jQuery( document ).ready(function($) {
	setReadOnlyFlag(<s:property value='programaPagoForm.readOnly'/>);readOnly
	
	flagDistribuyePrima='0';
	flagDistribuyeRecargos='0';
	flagDistribuyeDerechos='0';
	configurarReadOnlyRecibos();
	setHasEditPrimaTotalPermitFlag(<s:property value='programaPagoForm.hasEditPrimaTotalPermit'/>);
	setAjustePermitidoPrimaTotal(<s:property value='programaPagoForm.ajustePermitidoPrimaTotal'/>);
	
	//console.log(<s:property value='programaPagoForm.hasEditPrimaTotalPermit'/>);
	getGridRecibos(<s:property value='programaPagoForm.idToCotizacion'/>,<s:property value='programaPagoForm.idProgPago'/>);
	validacionDeBanderas();
	});
</script>