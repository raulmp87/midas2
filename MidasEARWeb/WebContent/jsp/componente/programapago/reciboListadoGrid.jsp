<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		
        <afterInit>
        </afterInit>
	
        <column id="numExhibicion"  type="ro" width="80" sort="str" ><s:text name="No. Exhibicion"/></column>
        <column id="cveRecibo" type="ro" width="100" sort="str"><s:text name="Folio"/></column>
        <column id="fechaVencimiento" type="ed" width="100" sort="int" align="center"><s:text name="Vencimiento"/></column>
        <column id="fechaCubreDesde" type="ed" width="100" sort="int" align="center"><s:text name="Ini Prog"/></column>
        <column id="fechaCubreHasta" type="ed" width="100" sort="int" align="center"><s:text name="Fin Prog"/></column>
        <column id="impPrimaNeta" type="ed" width="98" sort="str" format="$0,000.00"><s:text name="Prima Neta"/></column>
        <column id="impRcgosPagoFR" type="ed" width="98" sort="str" format="$0,000.00"><s:text name="Recargos"/></column>
        <column id="impDerechos" type="ed" width="98" sort="str" format="$0,000.00"><s:text name="Derechos"/></column>
        <column id="impIVA"   type="ro" width="98" sort="str"><s:text name="IVA"/></column>
        <column id="impPrimaTotal" type="ro" width="98" sort="str" format="$0,000.00"><s:text name="Prima Total"/></column> 
 	    <column id="eliminar" type="img" width="30" align="center"></column>
	</head>  		
	
    <s:iterator value="listRecibo" status="stats">
       <row id="<s:property value="id"/>">
               <cell><s:property value="numExhibicion" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="cveRecibo" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="fechaVencimiento" escapeHtml="false" escapeXml="true"/></cell>			
	           <cell><s:property value="fechaCubreDesde" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="fechaCubreHasta" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impPrimaNeta" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impRcgosPagoFR" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impDerechos" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impIVA" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impPrimaTotal" escapeHtml="false" escapeXml="true"/></cell>
               <cell>/MidasWeb/img/delete16.gif^Eliminar^javascript:eliminaRecibo(<s:property value="id"/>)^_self</cell>                         
            </row> 
         </s:iterator>
    </rows>
