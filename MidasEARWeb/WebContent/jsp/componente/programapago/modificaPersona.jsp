<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/componenteProgramaPago.js"/>"></script>
<script src="<s:url value='/js/midas2/generadorCURPRFC.js'/>"></script>
<script src="<s:url value='/js/curpRFC.js'/>"></script>  
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>



<style type="text/css">
input {
	min-width: 100px;
	max-width: 230px;
}

select {
	min-width: 100px;
	max-width: 230px;
}

textarea {
	min-width: 1px;
	max-width: 330px;
}

div.ui-datepicker {
	font-size: 10px;
}
div{
overflow-x:visible;
overflow-y:visible;
}
</style>

<s:set id="display" value="true" />

<!-- Nuevo cliente primer fragmento -->

	<script type="text/javascript">
		
		
	
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
			//Se comenta el jQIsRequired para que no marque los *
			//jQIsRequired();			
			/*
			var idPais=jQuery("#cliente\\.idPaisString").val();
			if(!jQuery.isValid(idPais)){
				jQuery("#cliente\\.idPaisString").val("PAMEXI");
			}
			*/
			var estatusActivo=jQuery("#cliente\\.tipoSituacionString01").attr("checked");
			var estatusInactivo=jQuery("#cliente\\.tipoSituacionString02").attr("checked");
			//Si no tienen un valor seleccionado, entonces lo pone por default
			if(estatusActivo==false && estatusInactivo==false){
				jQuery("#cliente\\.tipoSituacionString01").attr("checked",true);
			}
			var personaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
			var personaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
			if(personaFisica==false && personaMoral==false){
				jQuery("#cliente\\.claveTipoPersonaString1").attr("checked",true);
			}
			
		});
	</script>
<!-- Nuevo cliente primer fragmento -->

<!-- Nuevo cliente segundo fragmento -->

<style>
	label{
		display:inline-block;
	}
</style>

<!-- Nuevo cliente segundo fragmento -->


<body>

<!-- si la bandera esta prendida, es que son distintos -->
<s:if test="banderaClienteCot">
	<s:set id="readOnly" value="false" />
</s:if>
<s:else>
	<s:set id="readOnly" value="true" />
</s:else>


<s:form id="personaForm" name="personaForm">
<!-- Fragmento de seleccion de contratante -->

<table border=0 width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	<tr>
		<td class="titulo" colspan="3">
			Secci�n Personas Relacionadas
		</td>
		
	</tr>
	<tr>
		<td width="33%">
			<s:hidden name="idToProgramaPago" id="idToProgramaPago"/>
			<s:hidden name="idToCotizacion" id="idToCotizacion"/>
			<s:select label="Nuevo Due�o Programa Pago" list="personasAseguradas" headerKey="0" headerValue="Seleccione..." name="idClienteAsegurado" id="comboAsegurado" cssClass="cajaTexto w200 campoForma" onchange="javascript: cargarCliente(jQuery('#comboAsegurado').val(),jQuery('#idToProgramaPago').val(),jQuery('#idToCotizacion').val());"/> 
		</td>
		<s:if test="errorGuardadoCliente">
			<td  colspan="3">
				<s:label name="mensageError"></s:label>
			</td>
		</s:if>
	</tr>
</table>

<!-- Fragmento de seleccion de contratante -->



	
<!-- Nuevo cliente primer fragmento -->	
	<s:hidden name="cliente.idCliente" id="cliente.idCliente"/>
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	<tbody>
		<tr>
			<td class="titulo" colspan="3">
				Secci�n de Datos Generales del Cliente
			</td>
		</tr>
		<tr>
			<td width="33%">
				<s:textfield id="idClienteVista" label="Id Cliente" labelposition="left" disabled="true" cssClass="cajaTextoM2 " value="%{cliente.idCliente}"/>			
			</td>
			<td width="33%">
				<table class="contenedorFormas no-Border">
					<tr>
						<td>
							<label>Tipo de Situaci�n</label>
						</td>
					</tr>
					<tr>
						<td>
							<s:radio id="cliente.tipoSituacionString" disabled="%{#readOnly}" name="cliente.tipoSituacionString" list="#{'A':'Activo','I':'Inactivo'}" cssClass="cajaTextoM2 "></s:radio>	
						</td>
					</tr>					
				</table>
			</td>
			<td width="33%">
				<table class="contenedorFormas no-Border">
					<tr>
						<td>
							<label>Persona Jur�dica</label>
						</td>
					</tr>
					<tr>
						<td>
							<s:radio id="cliente.claveTipoPersonaString" disabled="%{#readOnly}" name="cliente.claveTipoPersona" list="#{'1':'Persona F�sica','2':'Persona Moral'}" onclick="switchPorTipoPersona(this.value,'%{divCarga}');" cssClass="cajaTextoM2 "></s:radio>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="33%">
				<div>
				 <s:textfield name="cliente.nombre"  disabled="%{#readOnly}" id="cliente.nombre" label="Nombre" labelposition="left" maxlength="40" cssClass="" />
				</div>
			</td>
			<td width="33%">
				<s:textfield name="cliente.apellidoPaterno"  disabled="%{#readOnly}" id="cliente.apellidoPaterno" label="Apellido Paterno"  labelposition="left" maxlength="20" cssClass="cajaTextoM2 " />
			</td>
			<td width="33%">
				<s:textfield name="cliente.apellidoMaterno"  disabled="%{#readOnly}" id="cliente.apellidoMaterno" label="Apellido Materno"  labelposition="left" maxlength="20" cssClass="cajaTextoM2 " />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">cliente.claveNacionalidad</s:param>
					<s:param name="idEstadoName">cliente.claveEstadoNacimiento</s:param>	
					<s:param name="idCiudadName">cliente.claveCiudadNacimiento</s:param>		
					<s:param name="labelPais">Nacionalidad</s:param>	
					<s:param name="labelEstado">Estado de Nacimiento</s:param>
					<s:param name="labelCiudad">Ciudad de Nacimiento</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">5</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido" value="0"></s:param>
					<s:param name="enableSearchButton" value="false"></s:param>
				</s:action>
			</td>
		</tr>
		<tr>
			<td width="33%">
				<!-- FALTA CHECAR QUE FUNCIONE -->
				<div class="elementoInline">
					<span class="wwlbl">
						<label class="label"><s:text name="Fecha de Nacimiento"/></label>
					</span>
				</div>
				<div class="elementoInline">
					<sj:datepicker name="cliente.fechaNacimiento" buttonImageOnly="true"
					   label="" cssClass="cajaTextoM2" labelposition="left"   
					   buttonImage="../../img/b_calendario.gif" 
					   changeMonth="true"
					   changeYear="true"
					   yearRange="-80:-18" 
					   maxDate="today"
					   disabled="%{#readOnly}"
					   showOn="%{#showOnConsulta}"
					   id="cliente.fechaNacimiento" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx  " 
					   onkeypress="return soloFecha(this, event, false);"  
					   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" 
					   onblur='esFechaValida(this);midasFormatRFCInOneField(jQuery("#cliente\\.nombre").val(),jQuery("#cliente\\.apellidoPaterno").val(),jQuery("#cliente\\.apellidoMaterno").val(),jQuery("#cliente\\.fechaNacimiento").val(),"cliente.codigoRFC");'>
					   </sj:datepicker>
				</div>
			</td>
			<td width="33%">
				<s:select name="cliente.estadoCivil" disabled="%{#readOnly}" id="cliente.estadoCivil" label="Estado Civil" list="estadosCiviles" labelposition="left" listKey="idEstadoCivil" listValue="nombreEstadoCivil" cssClass="cajaTextoM2 "/>
			</td>
		</tr>
		<tr>
			<td width="33%">
				<s:select name="cliente.sexo" id="cliente.sexo"  disabled="%{#readOnly}" list="#{'M':'Masculino', 'F':'Femenino'}" headerValue="Seleccione..." headerKey="" label="Sexo" labelposition="left" cssClass="cajaTextoM2 "/>
			</td>
			<td width="33%">
				<div class="elementoInline">
					<s:textfield name="cliente.codigoRFC" id="cliente.codigoRFC" disabled="%{#readOnly}" label="RFC" labelposition="left" maxlength="13" cssClass="cajaTextoM2  jQalphanumeric jQrfc-length"/>
				</div>
				<s:if test="display">
				<div class="elementoInline">
	               	<a href="javascript: void(0);" onclick='javascript:midasFormatRFCInOneField(jQuery("#cliente\\.nombre").val(),jQuery("#cliente\\.apellidoPaterno").val(),jQuery("#cliente\\.apellidoMaterno").val(),jQuery("#cliente\\.fechaNacimiento").val(),"cliente.codigoRFC");'>Generar RFC</a>
				</div>
				</s:if>
			</td>
			<td width="33%">
				<div class="elementoInline">
					<s:if test="tipoAccion == 4">
						<s:textfield name="cliente.codigoCURP" id="cliente.codigoCURP" disabled="%{#readOnly}" label="CURP" labelposition="left" maxlength="18" cssClass="cajaTextoM2 w120"/>
					</s:if>
					<s:else>
						<s:textfield name="cliente.codigoCURP" id="cliente.codigoCURP" disabled="%{#readOnly}" label="CURP" labelposition="left" maxlength="18" cssClass="cajaTextoM2  w120"/>
					</s:else>
				</div>
				<s:if test="display">
				<div class="elementoInline" >
					<a href="http://www.renapo.gob.mx/swb/swb/RENAPO/consultacurp" target="_blank">Consulta CURP</a>
				</div>				
				</s:if>	
			</td>
			<td>
				<s:if test="display">
				<div class="btn_back w50 elementoInline">
					<a href="javascript: void(0)" onclick='javascript: validaCURPPersonaClientesJS();'>
						<s:text name="Validar"/>
					</a>
				</div>				
				</s:if>		
			</td>
		</tr>
	</tbody>
	</table>
	
	<table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	<tbody>
		<tr>
			<td class="titulo">
				Domicilio
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
					<s:param name="idPaisName">cliente.idPaisString</s:param>
					<s:param name="idEstadoName">cliente.idEstadoString</s:param>	
					<s:param name="idCiudadName">cliente.idMunicipioString</s:param>		
					<s:param name="idColoniaName">cliente.nombreColonia</s:param>
					<s:param name="calleNumeroName">cliente.nombreCalle</s:param>
					<s:param name="cpName">cliente.codigoPostal</s:param>
					<s:param name="nuevaColoniaName">cliente.nombreColoniaDiferente</s:param>
					<s:param name="idColoniaCheckName">idColoniaCheck</s:param>				
					<s:param name="labelPais">Pa�s</s:param>	
					<s:param name="labelEstado">Estado</s:param>
					<s:param name="labelCiudad">Municipio</s:param>
					<s:param name="labelColonia">Colonia</s:param>
					<s:param name="labelCalleNumero">Calle y N�mero</s:param>
					<s:param name="labelCodigoPostal">C�digo Postal</s:param>
					<s:param name="labelPosicion">left</s:param>
					<s:param name="componente">2</s:param>
					<s:param name="readOnly" value="%{#readOnly}"></s:param>
					<s:param name="requerido" value="0"></s:param>
					<s:param name="enableSearchButton" value="false"></s:param>
				</s:action>
			</td>
		</tr>
	</tbody>
	</table>

<!-- Nuevo cliente primer fragmento -->


<!-- Nuevo cliente segundo fragmento -->

	<table width="98%" bgcolor="white" class="contenedorConFormato" align="center" > <!-- class="contenedorFormas" -->
		<tbody>
			<tr>
				<td class="titulo" colspan="4">
					Secci�n de Datos de Contacto
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefono">Tel�fono Casa</label> 
					<%--<label for="cliente.claveTelefono">Clave Telefono casa</label>
					<label for="cliente.numeroTelefono">Numero Telefono casa</label> --%>
<%-- 					<s:text name="midas.fuerzaventa.negocio.telefonoCasa"/> --%>
				</td>
				<td style="width: 40px;">
					<s:textfield name="cliente.claveTelefono" disabled="%{#readOnly}" id="cliente.claveTelefono" maxlength="2" cssStyle="width: 30px;" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/>
				</td>
				<td style="width: 40px;">
					<s:textfield name="cliente.clave2Telefono" disabled="%{#readOnly}" id="cliente.clave2Telefono" maxlength="2" cssStyle="width: 30px;" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/>
				</td>
				<td>
					<s:textfield name="cliente.numeroTelefono" disabled="%{#readOnly}" id="cliente.numeroTelefono" maxlength="8" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/>
					<%--<s:textfield name="cliente.telefono" disabled="%{#readOnly}" id="cliente.telefono" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2 "/> --%>			
				</td>
				<td>
					<label for="cliente.celularContacto">Celular</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.telefonoCelular"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.celularContacto" disabled="%{#readOnly}" id="cliente.celularContacto" maxlength="10" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefonoOficinaContacto">Tel�fono Oficina</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.telefonoOficina"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.telefonoOficinaContacto" disabled="%{#readOnly}" id="cliente.telefonoOficinaContacto" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
				<td>
					<label for="cliente.extensionContacto">Extensi�n</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.extension"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.extensionContacto" disabled="%{#readOnly}" id="cliente.extensionContacto" maxlength="5" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.faxContacto">Fax</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.fax"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.faxContacto"  disabled="%{#readOnly}" id="cliente.faxContacto" maxlength="12" onkeypress="return soloNumeros(this, event, false)" cssClass="cajaTextoM2"/>
				</td>
				<td>
					<label for="cliente.email">Correo Electr�nico</label>
<%-- 					<s:text name="midas.catalogos.centro.operacion.correoElectronico"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.email" disabled="%{#readOnly}" id="cliente.email" maxlength="50" cssClass="cajaTextoM2 jQemail"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.facebookContacto">Facebook</label>
<%-- 					<s:text name="midas.fuerzaventa.facebook"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.facebookContacto" disabled="%{#readOnly}" id="cliente.facebookContacto" maxlength="50" cssClass="cajaTextoM2"/>
				</td>
				<td>
					<label for="cliente.twitterContacto">Twitter</label>
<%-- 					<s:text name="midas.fuerzaventa.twitter"/> --%>
				</td>
				<td>
					<s:textfield name="cliente.twitterContacto" disabled="%{#readOnly}" id="cliente.twitterContacto" maxlength="50" cssClass="cajaTextoM2"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.paginaWebContacto">P�gina WEB</label>
<%-- 					<s:text name="midas.fuerzaventa.negocio.paginaWeb"/> --%>
				</td>
				<td colspan="3">
					<s:textfield name="cliente.paginaWebContacto" disabled="%{#readOnly}" id="cliente.paginaWebContacto" maxlength="50" cssClass="cajaTextoM2"/>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.telefonosAdicionalesContacto">Tel�fonos Adicionales</label>
<%-- 					<s:text name="midas.fuerzaventa.telefonosAdicionales"/> --%>
				</td>
				<td colspan="3">
					<s:textarea name="cliente.telefonosAdicionalesContacto" disabled="%{#readOnly}" id="cliente.telefonosAdicionalesContacto" cols="28" rows="3" cssClass="cajaTextoM2"></s:textarea>
				</td>				
			</tr>
			<tr>
				<td>
					<label for="cliente.correosAdicionalesContacto">Correos Adicionales</label>
<%-- 					<s:text name="midas.fuerzaventa.correosAdicionales"/> --%>
				</td>
				<td colspan="3">
					<s:textarea name="cliente.correosAdicionalesContacto" disabled="%{#readOnly}" id="cliente.correosAdicionalesContacto" cols="28" rows="3" cssClass="cajaTextoM2"></s:textarea>
				</td>
			</tr>
		</tbody>
	</table>
		
<!-- Nuevo cliente segundo frgmento -->

<!-- Nuevo cliente tercer fragmento -->

	<div class="toolBar">
	
		<s:if test="banderaClienteCot">
			<br/>
			<div class="btn_back w160" style="display:inline-block;">
				<a href="javascript: copiarDatosGenerales();" class="icon_guardar" onclick="">
					Copiar datos generales
				</a>
			</div>
		</s:if>

		</div>
		<table width="98%"  class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td class="titulo" colspan="6">
					Secci�n de Datos Fiscales
				</td>
			</tr>
			<tr>
				<td>
					<label for="cliente.nombreFiscal">
						<s:text name="midas.fuerzaventa.negocio.nombre"/>
					</label>
<!-- 					<label>Nombre</label> -->
				</td>
				<td>
						<s:textfield name="cliente.nombreFiscal" disabled="%{#readOnly}" id="cliente.nombreFiscal" maxlength="20" value="%{cliente.nombre}" cssClass="cajaTextoM2 jQrequired"/>
				</td>
				<td>
<!-- 					<label>Apellido Paterno</label> -->
					<label for="cliente.apellidoPaternoFiscal">
						<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoPaterno"/>
					</label>
				</td>
				<td>
					<s:textfield name="cliente.apellidoPaternoFiscal" disabled="%{#readOnly}" id="cliente.apellidoPaternoFiscal" value="%{cliente.apellidoPaterno}" maxlength="20" cssClass="cajaTextoM2 jQrequired"/>
				</td>
				<td>
<!-- 					<label>Apellido Materno</label> -->
					<label for="cliente.apellidoMaternoFiscal">
						<s:text name="midas.suscripcion.solicitud.solicitudPoliza.apellidoMaterno"/>
					</label>
				</td>
				<td>
					<s:textfield name="cliente.apellidoMaternoFiscal" disabled="%{#readOnly}" id="cliente.apellidoMaternoFiscal" maxlength="20"  value="%{cliente.apellidoMaterno}" cssClass="cajaTextoM2 jQrequired"/>
				</td>
			</tr>
		</tbody>
		</table>
		
		<table width="98%" class="contenedorConFormato" align="center">
		<tbody>
			<tr>
				<td class="titulo">
					Domicilio Fiscal
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idEstadoName">cliente.idEstadoFiscal</s:param>	
						<s:param name="idCiudadName">cliente.idMunicipioFiscal</s:param>		
						<s:param name="idColoniaName">cliente.nombreColoniaFiscal</s:param>
						<s:param name="calleNumeroName">cliente.nombreCalleFiscal</s:param>
						<s:param name="cpName">cliente.codigoPostalFiscal</s:param>
						<s:param name="nuevaColoniaName">cliente.nombreColoniaDiferenteFiscal</s:param>
						<s:param name="idColoniaCheckName">idColoniaCheckFiscal</s:param>				
						<s:param name="labelPais">Pa�s</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle y N�mero</s:param>
						<s:param name="labelCodigoPostal">C�digo Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">2</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
					</s:action>
				</td>
			</tr>
		</tbody>
	</table>
	
</s:form>		
		
<!-- Nuevo cliente tercer fragmento -->

<!-- Si no es el cliente del programa de pago -->
<s:if test="!(banderaClienteProgramaPago)">
	<!-- Botones -->
	<div align="right" class="w910 inline" >

		<!-- Si el cliente a guardar es el de la cotizacion, solo asignamos, sin tocar el cliente por si -->
		<s:if test="!(banderaClienteCot)">
			<div class="btn_back w110">
				<a href="javascript: asignarClienteAProgramaPago(<s:property value='cliente.idCliente'/>,<s:property value='idToProgramaPago'/>,<s:property value='idToCotizacion'/>);" class="icon_guardar" onclick="">
					<s:text name="midas.boton.guardar"/>
				</a>
			</div>
		</s:if>
		<!-- Si no es el cliente de la cotizacion, actualizamos cliente y asignamos -->
		<s:else>
			<div class="btn_back w110">
				<a href="javascript: guardarNuevoCliente();" class="icon_guardar" onclick="">
					<s:text name="midas.boton.guardar"/>
				</a>
			</div>
		</s:else>
		
		<div class="btn_back w110">
			<a href="javascript: cerrarModificarCliente();" class="icon_cerrar" onclick="">
				<s:text name="midas.boton.cerrar"/>
			</a>
		</div>		
									
	</div>
	<!-- Botones -->
</s:if>
<!-- Si es el cliente de programa de pago, no guardamos -->
<s:else>
	<!-- Botones -->
	<div align="right" class="w910 inline" >
		<div class="btn_back w110">
			<a href="javascript: cerrarModificarCliente();" class="icon_cerrar" onclick="">
				<s:text name="midas.boton.cerrar"/>
			</a>
		</div>		
	</div>
	<!-- Botones -->
</s:else>

		

	

</body>
	

