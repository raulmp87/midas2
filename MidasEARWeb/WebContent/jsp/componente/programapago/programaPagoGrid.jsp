<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/poliza/auto/inciso/inciso.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>		
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<!-- 
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/componenteProgramaPago.js"/>"></script>
 -->
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>


<link href="<s:url value="/css/programaPago.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/paginacionProgramaPago.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxdataprocessor.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_drag.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_link.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/ajaxScriptDataGrid.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/programaPago.js"/>"></script>



<s:form id="progPaagoForm" name="progPaagoForm">
<s:hidden name="programaPagoForm.tipoMov"/>
<s:hidden name="programaPagoForm.idToSolicitud"/>
<s:hidden name="programaPagoForm.idToPoliza"/>
	
	<div class="titulo">
		Resumen de Programas de pago 
	</div>
		<div id="tablaYPag">
		<div id="paginador"></div>
		<div id="gridProgramasDePago" style="width:100%;height:400px"></div>
	</div>

	<br />
	<table style="width: 100%;">
		<tr>
			<td style="vertical-align: top; width:50%; float:left; padding-left:40px;">
				<div class="alinearBotonALaIzquierda">

					<c:if test="${!programaPagoForm.readOnly}">
					<div id="b_agregarProgramaPago">
						<a id="linkAgregarProgramaPago" href="javascript: void(0);"
							onclick="javascript: agregaProgramaPago2(<s:property value='programaPagoForm.idToCotizacion'/>,'<s:property value='programaPagoForm.tipoMov'/>',<s:property value='programaPagoForm.idToSolicitud'/>,<s:property value='programaPagoForm.idToPoliza'/>,<s:property value='programaPagoForm.idContinuity'/>,<s:property value='programaPagoForm.tipoFormaPago'/>,<s:property value='programaPagoForm.numeroEndoso'/>);"> 
							                                         Agregar Programa de Pago
							                                         </a>
					</div>
					</c:if>
					<c:if test="${programaPagoForm.readOnly}">
						<c:if test="${programaPagoForm.tieneRolRecuotificacion}">				
							<div id="b_recuotificar">
								<a id="linkRecuotificar" href="javascript: void(0);" onclick="javascript: recuotificar(<s:property value='programaPagoForm.idToCotizacion'/>,'<s:property value='programaPagoForm.tipoMov'/>',<s:property value='programaPagoForm.idToSolicitud'/>,<s:property value='programaPagoForm.idToPoliza'/>,<s:property value='programaPagoForm.idContinuity'/>,<s:property value='programaPagoForm.tipoFormaPago'/>,<s:property value='programaPagoForm.numeroEndoso'/>);">A Recuotificar</a>
							</div>
						</c:if>
					</c:if>
				</div>
			</td>
			<td style="vertical-align: top;">
				<div class="alinearBotonALaDerecha">

					<div id="b_imprimirProgramaPago">
						<a id="linkImprimir" href="javascript: void(0);"
							onclick="javascript: imprimirProgramaPago(<s:property value='programaPagoForm.idToCotizacion'/>);"> Imprimir </a>
					</div>

					<div id="b_regresar">
						<a id="linkregresar" href="javascript: void(0);"
							onclick="javascript: salir();"> Salir </a>
					</div>

					<c:if test="${!programaPagoForm.readOnly}">
					<div id="b_cargarPPoriginal">
						<a id="linkCargarProgramaOriginal" href="javascript: void(0);" onclick="javascript: cargarProgramaDePagoOriginal(<s:property value='programaPagoForm.idToCotizacion'/>,'<s:property value='programaPagoForm.tipoMov'/>',<s:property value='programaPagoForm.idToSolicitud'/>,<s:property value='programaPagoForm.idToPoliza'/>,<s:property value='programaPagoForm.idContinuity'/>,<s:property value='programaPagoForm.tipoFormaPago'/>,<s:property value='programaPagoForm.numeroEndoso'/>);"> 
							Cargar Programa Original
						</a>
					</div>
					</c:if>
				</div>
			</td>
		</tr>
	</table>
	
</s:form>

<script type="text/javascript">
jQuery( document ).ready(function($) {	
	setReadOnlyFlag(<s:property value='programaPagoForm.readOnly'/>);
	setHasEditPrimaTotalPermitFlag(<s:property value='programaPagoForm.hasEditPrimaTotalPermit'/>);	
	configurarReadOnly();
	cargarProgramasDePago(<s:property value='programaPagoForm.idToCotizacion'/>,'<s:property value='programaPagoForm.tipoMov'/>',<s:property value='programaPagoForm.idToSolicitud'/>,<s:property value='programaPagoForm.idToPoliza'/>,<s:property value='programaPagoForm.idContinuity'/>,<s:property value='programaPagoForm.tipoFormaPago'/>,<s:property value='programaPagoForm.numeroEndoso'/>);	
	cargarSaldoOriginal(<s:property value='programaPagoForm.idToCotizacion'/>);
	setAjustePermitidoPrimaTotal(<s:property value='programaPagoForm.ajustePermitidoPrimaTotal'/>);
	});
</script>