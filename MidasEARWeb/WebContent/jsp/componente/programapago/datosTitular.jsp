<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/suscripcion/cotizacion/auto/inciso/contenedorHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/modificaPersona.jsp"></s:include>

<html>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			//dhx_init_tabbars();
			//Se comenta el jQIsRequired para que no marque los *
			//jQIsRequired();			
			/*
			var idPais=jQuery("#cliente\\.idPaisString").val();
			if(!jQuery.isValid(idPais)){
				jQuery("#cliente\\.idPaisString").val("PAMEXI");
			}
			*/
// 			var estatusActivo=jQuery("#cliente\\.tipoSituacionString01").attr("checked");
// 			var estatusInactivo=jQuery("#cliente\\.tipoSituacionString02").attr("checked");
	//		Si no tienen un valor seleccionado, entonces lo pone por default
// 			if(estatusActivo==false && estatusInactivo==false){
// 				jQuery("#cliente\\.tipoSituacionString01").attr("checked",true);
// 			}
			var personaFisica=jQuery("#cliente\\.claveTipoPersonaString1").attr("checked");
			var personaMoral=jQuery("#cliente\\.claveTipoPersonaString2").attr("checked");
			if(personaFisica==false && personaMoral==false){
				jQuery("#cliente\\.claveTipoPersonaString1").attr("checked",true);
				ocultarCamposTipoPersona(1);
			}
			else
			{
			
			if(personaFisica==true)
			{
				ocultarCamposTipoPersona(2);
			}
			else
			{
				ocultarCamposTipoPersona(1);
			}
			}
		
			
			
			
			
		});
	</script>
<!--/head-->


<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">


<body>

<s:form action="listarFiltrado" id="clienteForm" name="clienteForm">
<table border=0 width="100%">

        <table border=0 width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	
		
		    <tr>
				<td class="titulo" colspan="3">
					Sección Personas Relacionadas
				</td>
			</tr>
			
			<tr>
			<td width="33%">
					<s:select name="cliente.sexo" id="cliente.sexo"  disabled="%{#readOnly}" list="#{'M':'Masculino', 'F':'Femenino'}" headerValue="Seleccione..." headerKey="" label="Nuevo Dueño Programa Pago" labelposition="left" cssClass="cajaTextoM2 "/>
				</td>
			
		
			<td width="33%">
			
			<s:select label="Nuevo Dueño Programa Pago" name="cliente.sexo" id="cliente.sexo" list="personasRelacionadas" headerKey="0" headerValue="Seleccione"></s:select>
			</td>
			
		   </tr>
			
		 </table>	

		<table border=0 width="98%" bgcolor="white" align="center" class="contenedorConFormato">
	
		
		    <tr>
				<td class="titulo" colspan="3">
					Sección de Datos Generales del Cliente
				</td>
			</tr>
			
			
			<tr>
			   <td width="33%">
					<table class="contenedorFormas no-Border">
						<tr>
							<td>
								<label>Persona Jurídica</label>
							</td>
						</tr>
						<tr>						
							<td width="315px">
				                 <s:radio id="cliente.claveTipoPersonaString" onchange="ocultarCamposTipoPersona(this.value);" name="cliente.claveTipoPersonaString" list="#{'1':'Persona Física','2':'Persona Moral'}"  cssClass="cajaTextoM2 "></s:radio>
			                </td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr class="pf">
				<td width="33%">
					<s:textfield name="cliente.nombre"  disabled="%{#readOnly}" id="cliente.nombre" label="Nombre" labelposition="left" maxlength="40" cssClass="txtfield input_text jQrequired" />
				</td>
				<td width="33%">
					<s:textfield name="cliente.apellidoPaterno"  disabled="%{#readOnly}" id="cliente.apellidoPaterno" label="Apellido Paterno"  labelposition="left" maxlength="20" cssClass="txtfield input_text jQrequired" />
				</td>
				<td width="33%">
					<s:textfield name="cliente.apellidoMaterno"  disabled="%{#readOnly}" id="cliente.apellidoMaterno" label="Apellido Materno"  labelposition="left" maxlength="20" cssClass="cajaTextoM2 " />
				</td>
			</tr>
			
			<tr class="pf">
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idPaisName">cliente.claveNacionalidad</s:param>
						<s:param name="idEstadoName">cliente.claveEstadoNacimiento</s:param>	
						<s:param name="idCiudadName">cliente.claveCiudadNacimiento</s:param>		
						<s:param name="labelPais">Nacionalidad</s:param>	
						<s:param name="labelEstado">Estado de Nacimiento</s:param>
						<s:param name="labelCiudad">Ciudad de Nacimiento</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">5</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
					</s:action>
				</td>
			</tr>
			
		   <tr class="pf">
				<td width="33%">
					<!-- 
					<div style="display:inline-block;">
						<s:textfield name="cliente.fechaNacimiento" id="cliente.fechaNacimiento" label="Fecha de Nacimiento" readonly="true" labelposition="left" cssClass="cajaTextoM2 "/>
					</div>
					-->
					<!-- FALTA CHECAR QUE FUNCIONE -->
					<div class="elementoInline">
						<span class="wwlbl">
							<label class="label"><s:text name="Fecha de Nacimiento"/></label>
						</span>
					</div>
					<div class="elementoInline">
						
				               	<sj:datepicker name="biAutoInciso.value.fechaNacConductor" 
	               	           required="#requiredField" 
	               	           buttonImageOnly="true"
				 			   buttonImage="/MidasWeb/img/b_calendario.gif" 
				               id="fechaNacimiento" maxlength="10" cssClass="txtfield jQrequired"	
				               labelposition="left"   
				               size="12"
				               showOn="%{#showOnConsulta}"
				               yearRange="-80:-18"
				               maxDate="today"
				               changeYear="true"
				               changeMonth="true"
				               onkeypress="return soloFecha(this, event, false);"
				               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				               onblur='esFechaValida(this);midasFormatRFCInOneField(jQuery("#cliente\\.nombre").val(),jQuery("#cliente\\.apellidoPaterno").val(),jQuery("#cliente\\.apellidoMaterno").val(),jQuery("#cliente\\.fechaNacimiento").val(),"cliente.codigoRFC");'>
                                 </sj:datepicker>
			                
					</div>
				</td>
				
				<td width="33%">
					<s:select name="cliente.sexo" id="cliente.sexo"  disabled="%{#readOnly}" list="#{'M':'Masculino', 'F':'Femenino'}" headerValue="Seleccione..." headerKey="" label="Sexo" labelposition="left" cssClass="cajaTextoM2 "/>
				</td>
			
<!-- 				<td width="33%"> -->
<%-- 					<s:textfield name="cliente.claveOcupacion" id="cliente.claveOcupacion" label="Ocupación" labelposition="left" cssClass="cajaTextoM2 "/> --%>
<!-- 				</td> -->
			</tr>
		
		
		 

			<tr class="pm">
				<th width="105px">				
					Razón Social
				</th>	
				<td width="315px">
					<s:textfield name="cliente.razonSocial" id="nombre" cssClass=" cajaTextoM2 w200"
					maxlength="500"/>
				</td>
				
				<th width="105px">				
					Fecha de Constitución 
				</th>	
				<td width="315px">
					<sj:datepicker name="filtroCliente.fechaConstitucion"  
									   buttonImage="/MidasWeb/img/b_calendario.gif" 
									   buttonImageOnly="true"
									   id="fechaConstitucion" maxlength="10" cssClass="cajaTextoM2"								   								  
									   onkeypress="return soloFecha(this, event, false);" 
									   changeYear="true" changeMonth="true"  
									   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
									   onblur='esFechaValida(this);'></sj:datepicker>
				</td>
			</tr>
		
			
			
			<tr class="pf">
				
				<td width="33%">
					<div class="elementoInline">
						<s:textfield name="cliente.codigoRFC" id="cliente.codigoRFC" disabled="%{#readOnly}" label="RFC" labelposition="left" maxlength="13" cssClass="cajaTextoM2  jQalphanumeric jQrfc-length"/>
					</div>
					
					<div class="elementoInline">
		               	<a href="javascript: void(0);" onclick='javascript:midasFormatRFCInOneField(jQuery("#cliente\\.nombre").val(),jQuery("#cliente\\.apellidoPaterno").val(),jQuery("#cliente\\.apellidoMaterno").val(),jQuery("#cliente\\.fechaNacimiento").val(),"cliente.codigoRFC");'>Generar RFC</a>
					</div>
				
				</td>
				<td width="33%">
					<div class="elementoInline">
							<s:textfield name="cliente.codigoCURP" id="cliente.codigoCURP" disabled="%{#readOnly}" label="CURP" labelposition="left" maxlength="18" cssClass="cajaTextoM2 w120"/>
					</div>
		
					<div class="elementoInline" >
						<a href="http://www.renapo.gob.mx/swb/swb/RENAPO/consultacurp" target="_blank">Consulta CURP</a>
					</div>				

				</td>
				<td>
				
					<div class="btn_back w50 elementoInline">
						<a href="javascript: void(0)" onclick='javascript: validaCURPPersonaClientesJS();'>
							<s:text name="Validar"/>
						</a>
					</div>				
				
				</td>
			</tr>
			
			 
       
        </table>
        
        <table width="98%" bgcolor="white" align="center" class="contenedorConFormato">
		<tbody>
			 <tr>
				<td class="titulo" colspan="3">
					Domicilio
				</td>
			</tr>
				<td colspan="3">
					<s:action name="combosDireccion" var="combosDireccion" namespace="/componente/direccion" ignoreContextParams="true" executeResult="true" >
						<s:param name="idPaisName">cliente.idPaisString</s:param>
						<s:param name="idEstadoName">cliente.idEstadoString</s:param>	
						<s:param name="idCiudadName">cliente.idMunicipioString</s:param>		
						<s:param name="idColoniaName">cliente.nombreColonia</s:param>
						<s:param name="calleNumeroName">cliente.nombreCalle</s:param>
						<s:param name="cpName">cliente.codigoPostal</s:param>
						<s:param name="nuevaColoniaName">cliente.nombreColoniaDiferente</s:param>
						<s:param name="idColoniaCheckName">idColoniaCheck</s:param>				
						<s:param name="labelPais">País</s:param>	
						<s:param name="labelEstado">Estado</s:param>
						<s:param name="labelCiudad">Municipio</s:param>
						<s:param name="labelColonia">Colonia</s:param>
						<s:param name="labelCalleNumero">Calle y Número</s:param>
						<s:param name="labelCodigoPostal">Código Postal</s:param>
						<s:param name="labelPosicion">left</s:param>
						<s:param name="componente">2</s:param>
						<s:param name="readOnly" value="%{#readOnly}"></s:param>
						<s:param name="requerido" value="0"></s:param>
						<s:param name="enableSearchButton" value="false"></s:param>
					</s:action>
				</td>
		</tbody>
		</table>
        

</table>
</s:form>

</body>
</html>
