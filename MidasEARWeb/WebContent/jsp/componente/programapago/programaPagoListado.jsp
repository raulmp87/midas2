<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/componente/programapago/programaPagoListadoHeader.jsp"></s:include>
	


<div id="programaPagoListadoGrid" style="width:100%;height:400px"></div>

<table>



<table align="left">	
				<tr>
					<td align="right">
                     	<div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="limpiar();" class="icon_limpiar" >	
<%-- 									<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar"/>	 --%>
								<s:text name="Agregar"/>	
								</a>
		                    </div>
                      	</div>																					
					</td>
				</tr>
			</table>

<table align="right">	
				<tr>
					<td align="right">
					 	<div id="divBuscarBtn" class="w200" style="float:left;">
								<div class="btn_back w190"  >
									<a href="javascript: void(0);" onclick="cargarProgramaOrigina(<s:property value='idToCotizacion'/>);" class="icon_buscar">	
<%-- 										<s:text name="midas.boton.buscar"/>	 --%>
                                         	<s:text name="Cargar Programa Original"/>	
									</a>
	                      		</div>
	                     	</div>	
                     	<div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="imprimirProgramaPagos(<s:property value='idToCotizacion'/>);" class="icon_imprimir" >	
<%-- 									<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar"/>	 --%>
								<s:text name="Imprimir"/>	
								</a>
		                    </div>
                      	</div>			
	                      	<div id="divBuscarBtn" class="w150" style="float:left;">
								<div class="btn_back w140"  >
									<a href="javascript: void(0);" onclick="salir()" class="icon_exit">	
<%-- 										<s:text name="midas.boton.buscar"/>	 --%>
                                         	<s:text name="Salir"/>	
									</a>
	                      		</div>
	                     	</div>	
	               																		
					</td>
				</tr>
					
			</table>


</table>



<script type="text/javascript">
jQuery( document ).ready(function($) {
   getGridProgramaPagos(<s:property value='idToCotizacion'/>);
});
</script>  