<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/componente/programapago/reciboListadoHeader.jsp"></s:include>

<body>
<s:form action="listarFiltrado" id="clienteForm" name="clienteForm">
	<table width="100%" id="filtrosM2" cellpadding="0" cellspacing="0" >
		<tr>
			<td class="titulo" colspan="4">Datos del Contratante</td>
			<td></td>
			
		</tr>
<!-- 		<tr> -->
<!-- 			<th width="200px">				 -->
<!-- 				Persona Jurídica -->
<!-- 			</th>	 -->
<!-- 			<td width="315px"> -->
<%-- 				<s:radio id="filtroCliente.claveTipoPersonaString" onchange="ocultarCamposTipoPersona(this.value);" name="filtroCliente.claveTipoPersonaString" list="#{'1':'Persona Física','2':'Persona Moral'}"  value="1" cssClass="cajaTextoM2 "></s:radio> --%>
<!-- 			</td> -->
<!-- 			<!--  -->
<!-- 			<th width="105px">				 -->
<!-- 				Nombre -->
<!-- 			</th>	 -->
<!-- 			<td width="315px"> -->
<%-- 				<s:textfield name="filtroCliente.nombre" id="nombre" cssClass="cajaTextoM2 w250"/> --%>
<!-- 			</td> -->
<!-- 			--> 
<!-- 		</tr> -->
        <br>
        
		<tr class="pf">
			<th width="105px">				
				Contratante
			</th>	
			<td width="315px">
				<s:property value="nombreContratante"/>
			</td>
<!-- 			<th width="105px">				 -->
<!-- 				Apellido Paterno -->
<!-- 			</th>	 -->
<!-- 			<td width="315px"> -->
<%-- 				<s:textfield name="filtroCliente.apellidoPaterno" id="nombre" cssClass="jQalphanumeric cajaTextoM2 w200" --%>
<!-- 				maxlength="30"/> -->
<!-- 			</td>			 -->
<!-- 			<th width="105px">				 -->
<!-- 				Apellido Materno -->
<!-- 			</th>	 -->
<!-- 			<td width="315px"> -->
<%-- 				<s:textfield name="filtroCliente.apellidoMaterno" id="nombre" cssClass="jQalphanumeric cajaTextoM2 w200" --%>
<!-- 				maxlength="30"/> -->
<!-- 			</td> -->
		</tr>
<!-- 		<tr class="pf"> -->
<!-- 			<th width="105px">				 -->
<!-- 				CURP -->
<!-- 			</th>	 -->
<!-- 			<td width="315px"> -->
<%-- 				<s:textfield name="filtroCliente.codigoCURP" id="curp" cssClass="jQCURP cajaTextoM2 w200" --%>
<!-- 				maxlength="18"/> -->
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr class="pm" style="display:none;"> -->
<!-- 			<th width="105px">				 -->
<!-- 				Razón Social -->
<!-- 			</th>	 -->
<!-- 			<td width="315px"> -->
<%-- 				<s:textfield name="filtroCliente.razonSocial" id="nombre" cssClass=" cajaTextoM2 w200" --%>
<!-- 				maxlength="500"/> -->
<!-- 			</td> -->
<!-- 			<th width="105px">				 -->
<!-- 				Fecha de Constitución  -->
<!-- 			</th>	 -->

<!-- 		</tr> -->
		<tr>
			<th width="105px">				
				Dirección Fiscal
			</th>	
			<td width="315px" colspan="3">
				<s:property value="direccionFiscal"/>
			</td>
		</tr>
		<tr>
			<th width="105px">				
				RFC
			</th>	
			<td width="315px" colspan="3">
				<s:property value="rfc"/>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right">				
				<div class="btn_back w280" style="display:none;">
 					<a href="javascript: void(0);" class="icon_buscar"
 						onclick="modificaPersona(<s:property value="idToProgramaPago"/>,<s:property value="idToCotizacion" escapeHtml="false" escapeXml="true"/>);"> 
						<s:text name="Modificar Persona del Programa de Pago"/>
					</a>
				</div>				
			</td>
		</tr>	
	</table>
</s:form>

<div id="reciboListadoGrid" style="width:100%;height:350px"></div>
<table>

	  <table align="left"  class="contenedorFormas" align="center">
		<tr>
			<td colspan="4">
				<div align="right">
					<s:text name="Distribución Automática Prima Netaa"></s:text>
					<s:radio theme="simple" name="filtroCargos.tipoReporte" list="#{'1':'Si','2':'No'}" value="1" onclick="check(this.value);"/>	
				</div>
			</td>
		</tr>
	  </table>

      <table align="right">	
				<tr>
					<td align="right">
						<div id="divLimpiarBtn" style="float:left;" class="w150" >
							<div class="btn_back w140" >
								<a href="javascript: void(0);" onclick="agregaRecibo();" class="icon_limpiar" >	
<%-- 									<s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar"/>	 --%>
								<s:text name="Agregar"/>	
								</a>
		                    </div>
                      	</div>	
                      			
					 	<div id="divGuardarBtn" class="w200" style="float:left;">
								<div class="btn_back w190"  >
									<a href="javascript: void(0);" onclick="guardarRecibo(<s:property value='idToCotizacion'/>);" class="icon_guardar">	
<%-- 										<s:text name="midas.boton.buscar"/>	 --%>
                                         	<s:text name="Guardar"/>	
									</a>
	                      		</div>
	                     	</div>	
		
	                      	<div id="divRegresarBtn" class="w150" style="float:left;">
								<div class="btn_back w140"  >
									<a href="javascript: void(0);" onclick="regresaProgramaPago(<s:property value="idToCotizacion"/>);" class="icon_buscar">	
<%-- 										<s:text name="midas.boton.buscar"/>	 --%>
                                         	<s:text name="Regresar"/>	
									</a>
	                      		</div>
	                     	</div>	
	               																		
					</td>
				</tr>
			</table>


</table>
</body>


<script type="text/javascript">
jQuery( document ).ready(function($) {
   getGridRecibos(<s:property value='idToProgramaPago'/>,<s:property value='idToCotizacion'/>);
   flagDistribuyePrima='1';
});
</script>  