<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>


 
<rows>
	<head>
	
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		
        <afterInit>
        </afterInit>
<%-- 		<column id="numeroCotizacion" type="ro" width="130" sort="int" align="center"><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroSolicitud" /></column> --%>
<%-- 		<column id="numeroPoliza"  type="ro" width="110" sort="str" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.numeroPoliza" /></column> --%>
<%-- 		<column id="nombreAsegurado" type="ro" width="185" sort="str" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.nombreAsegurado" /></column>		 --%>
<%-- 		<column id="tipoEndoso" type="ro" width="140" sort="str" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.tipoEndoso" /></column> --%>
<%-- 		<column id="fecha" type="ro" width="90" sort="int" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.fecha" /></column> --%>
<%-- 		<column id="estatus" type="ro" width="130" sort="str" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.estatus" /></column> --%>
<%--		<column id="ver" type="img" width="30" align="center"></column> --%>
<%--		<column id="editar" type="img" width="30" align="center"></column> --%>
<%--		<column id="imprimir" type="img" width="30" align="center"></column>	 --%>
<%--		<column id="cancelar" type="img" width="30" align="center"></column> --%>	
<%--		<column id="emitir" type="img" width="30" align="center"></column>	--%>	
	
	    <column hidden="true" id="cotizacion.idToCotizacion"  type="ro" width="80" sort="str" ><s:text name="Inciso"/></column>
        <column colspan="2"  id="numProgPago"  type="ro" width="80" sort="str" ><s:text name="Numero"/></column>
        <column id="nombreContratante" type="ro" width="150" sort="str"><s:text name="Contratante"/></column>
        <column id="numRecibos" type="ro" width="80" sort="int" align="center"><s:text name="Num Rec"/></column>
        <column id="fechaRegistro" type="ro" width="100" sort="int" align="center"><s:text name="Ini Prog"/></column>
        <column id="fechaRegistro" type="ro" width="100" sort="int" align="center"><s:text name="Fin Prog"/></column>
        <column id="impPrimaNeta" type="ro" width="86" sort="str" format="$0,000.00"><s:text name="Prima Neta"/></column>
        <column id="impRcgosPagoFR" type="ro" width="86" sort="str" format="$0,000.00"><s:text name="Recargos"/></column>
        <column id="impDerechos" type="ro" width="86" sort="str" format="$0,000.00"><s:text name="Derechos"/></column>
        <column id="impIVA"   type="ro" width="86" sort="str"><s:text name="IVA"/></column>
        <column id="impPrimaTotal" type="ro" width="86" sort="str" format="$0,000.00"><s:text name="Prima Total"/></column> 
 	    <column id="eliminar" type="img" width="30"   align="center"></column>
        <column id="detalle"  type="img" width="30"   valign="center"></column>
	</head>  		
	
    <s:iterator value="listProgPago" status="stats">
       <row id="<s:property value="id"/>">
               <cell><s:property value="cotizacion.idToCotizacion" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="numProgPago" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="nombreContratante" escapeHtml="false" escapeXml="true"/></cell>			
	           <cell><s:property value="numRecibos" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impPrimaNeta" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impRcgosPagoFR" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impDerechos" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impIVA" escapeHtml="false" escapeXml="true"/></cell>
               <cell><s:property value="impPrimaTotal" escapeHtml="false" escapeXml="true"/></cell>
               <cell>/MidasWeb/img/delete16.gif^Eliminar^javascript:eliminaProgramaPago(<s:property value="id"/>)^_self</cell>                         
               <cell>/MidasWeb/img/details.gif^Detalle^javascript:openRecibos(<s:property value="id"/>,<s:property value="cotizacion.idToCotizacion"/>)^_self</cell>  
            </row> 
         </s:iterator>
    </rows>
