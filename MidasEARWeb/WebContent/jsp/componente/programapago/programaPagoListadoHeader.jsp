<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script language="JavaScript"
 	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
 </script> 
<script language="JavaScript"
 	src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'> 
 </script> 


<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/complementar/complementar.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/suscripcion/solicitud/comentarios/comentarios.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/poliza/auto/poliza.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/componente/programapago/componenteProgramaPago.js"/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>

<script type="text/javascript">
	var buscarSolicitudPath = '<s:url action="buscarSolicitud"/>';
	var busquedaRapidaPath = '<s:url action="busquedaRapida"/>';
	var opcionCapturaPath = '<s:url action="mostrar" namespace="/suscripcion/solicitud"/>';
	var solicitudesPaginadasPath = '<s:url action="busquedaPaginada"/>';
	var solicitudesPaginadasRapidaPath = '<s:url action="busquedaRapidaPaginada"/>';
    var cambiarAgenteEndosoPath = '<s:url action="mostrarCambioAgente" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var cambiarFormaPagoEndosoPath = '<s:url action="mostrarCambioFormaPago" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var cancelacionPolizaEndosoPath = '<s:url action="mostrarCancelacionPoliza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var rehabilitacionPolizaEndosoPath = '<s:url action="mostrarRehabilitacionPoliza" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso"/>';
    var definirTipoModificacionPath =  '<s:url action="mostrarDefTipoModificacion" namespace="/endoso/cotizacion/auto/solicitudEndoso"/>';
    var cancelarCotizacionPath = '<s:url action="cancelarCotizacion" namespace="/endoso/cotizacion/auto/solicitudEndoso"/>';
	var emitirBajaIncisoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso"/>';
	var emitirMovimientosPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos"/>';
	var emitirCambioDatosPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos"/>';
	var emitirCambioAgentePath =  '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';
	var emitirCambioFormaPagoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago"/>';
	var emitirEndosoCancelacionEndosoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso"/>';
	var emitirEndosoCancelacionPolizaPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"/>';
	var emitirExtensionVigenciaPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"/>';
	var emitirInclusionAnexoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo"/>';
	var emitirInclusionTextoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto"/>';
	var emitirRehabilitacionIncisoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionInciso"/>';
	var emitirRehabilitacionEndosoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso"/>';
	var emitirRehabilitacionPolizaPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza"/>';
	var emitirAltaIncisoPath = '<s:url action="emitir" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso"/>';
</script>