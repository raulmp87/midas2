<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="160"  align="center" sort="int" > <s:text name="midas.imprescionpoliza.edicion.linea" />   </column>
          <column  type="ro"  width="160"  align="center" sort="str" > <s:text name="midas.imprescionpoliza.edicion.lineanegocio" />  </column>
          <column  type="ro"  width="160"  align="center" sort="int" > <s:text name="midas.imprescionpoliza.edicion.unidadriesgo" />  </column>
          <column  type="ron"  width="160"  align="center" sort="int" format="$0,000.00" > <s:text name="midas.imprescionpoliza.edicion.prima" />  </column>

	</head>

	<s:iterator value="datosLineasDeNegocio">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="ROWNUM" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="lineaNegocio" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="unidadesRiesgo" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="impPrimaNeta" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>