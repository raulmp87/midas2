<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/componente/impresiones/edicionImpresionPoliza.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_sub_row.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">

 function imprimirPolizaEditada(){
 	if(confirm("Solo los cambios guardados aparecer\u00E1n en la impresi\u00F3n. \u00BFDesea continuar?")){
	 	removeCurrencyFormatOnTxtInput();
	 	jQuery("#id").val(-1);
		//var path = '/MidasWeb/impresiones/componente/imprimirPolizaEditada.action?'+jQuery(document.contenedorEdicionImpresionForm).serialize();
		jQuery("#contenedorEdicionImpresionForm").attr("action","imprimirPolizaEditada.action");
	 	parent.submitVentanaModal("mostrarEditarImpresionPoliza", contenedorEdicionImpresionForm);
	 	return false;
 	}
 }
	
 function guardarImpresionPolizaEditada(){
 	removeCurrencyFormatOnTxtInput();
 	if(validateAll(true)){
 		if(validarMontosPrimas()){
		 	jQuery("#id").val(-1);
		 	jQuery("#contenedorEdicionImpresionForm").attr("action","guardarImpresionPolizaEditada.action");
		 	codificarInformacion();
		 	console.log("Nombre Contratante:" + jQuery("#nombreContratante").val());
		 	parent.submitVentanaModal("mostrarEditarImpresionPoliza", contenedorEdicionImpresionForm);
		 	return false;
	 	}
 	}
 }
 
 function codificarInformacion(){
 	jQuery("#nombreContratante").val(encodeStr(jQuery("#nombreContratante").val()));
 	jQuery("#domicilio").val(encodeStr(jQuery("#domicilio").val()));
 	jQuery("#rfcContratante").val(encodeStr(jQuery("#rfcContratante").val()));
 	jQuery("#observaciones").val(encodeStr(jQuery("#observaciones").val()));
 	jQuery("#datosAgenteCaratula").val(encodeStr(jQuery("#datosAgenteCaratula").val()));
 }
 
 function validarMontosPrimas(){
 	var esValido = true;
 	var imprimirPrimas = jQuery("#imprimirPrimas").attr('checked');
	if(imprimirPrimas){
		jQuery(".validarPrima").each(
			function(){
			var these = jQuery(this);
                if( isEmpty(these.val()) ){
                      these.addClass("errorField");
                      esValido = false;
                      parent.mostrarMensajeInformativo("Si selecciona Imprimir Primas debe capturar todos los montos.","30");
                } else {
                      these.removeClass("errorField");
                }
		});		
	}
	return esValido;
 }
 
 function restaurarImpresionPolizaEditada(){
 	removeCurrencyFormatOnTxtInput();
 	jQuery("#id").val(-1);
 	jQuery("#contenedorEdicionImpresionForm").attr("action","restaurarImpresionPolizaEditada.action");
 	parent.submitVentanaModal("mostrarEditarImpresionPoliza", contenedorEdicionImpresionForm);
 	return false;
 }
 
 var mostrarLineasNegocioPath = '<s:url action="mostrarLineasNegocio" namespace="/impresiones/componente"/>';
 function mostrarLineasNegocio(){
 		removeCurrencyFormatOnTxtInput();
		jQuery("#listadoLineasNegocioGrid").empty();
	
		var listadoLineasNegocioGrid = new dhtmlXGridObject('listadoLineasNegocioGrid');
		listadoLineasNegocioGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorEdicionPoliza");});
		listadoLineasNegocioGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorEdicionPoliza");});
		
		formParams = jQuery(document.contenedorEdicionImpresionForm).serialize();
		var url = mostrarLineasNegocioPath + '?' +  formParams;
		listadoLineasNegocioGrid.load( url );
		initCurrencyFormatOnTxtInput();
 }
 
 function validarHora(campoTexto){
 	var horaVigencia = jQuery(campoTexto).val();
 	if(horaVigencia < 0 ){
 		jQuery(campoTexto).val(0);
 		parent.mostrarMensajeInformativo("La hora no debe ser menor a 0.","30");
 	}
 	if(horaVigencia > 24){
 		jQuery(campoTexto).val(24);
 		parent.mostrarMensajeInformativo("La hora no debe ser mayor a 24.","30");
 	}
 }


 var validos = ":0123456789";
 function soloNumerosYDosPuntos(campo) {
	var hasError = false;
    for (var i=0; i<campo.value.length; i++) {
     if (validos.indexOf(campo.value.charAt(i)) == -1){
    	 campo.value="";
    	 hasError = true;
    	 jQuery(campo).css("background-color", "#F5A9A9");
    	 jQuery("#errorHora").show();
    }}
    if(!hasError){
       jQuery(campo).css("background-color", "#ffffff");
       jQuery("#errorHora").hide();
    }
 }
  
 jQuery(document).ready(function(){
 	mostrarIncluirEdicionPoliza();
 	initCurrencyFormatOnTxtInput();
 	mostrarLineasNegocio();
 });
</script> 

<div id="detalle" >
 <div id="indicadorEdicionPoliza" style="display:none;"></div>
 	<s:form action="mostrarEditarImpresionPoliza" namespace="/impresiones/componente" id="contenedorEdicionImpresionForm">
		   <s:hidden name="id" id="id"/>
		   <s:hidden name="tipoImpresion"/>
		   <s:hidden name="idToPoliza"/>
		   <s:hidden name="validOn" />
		   <s:hidden name="validOnMillis" />
		   <s:hidden name="recordFrom" />
		   <s:hidden name="recordFromMillis" />
		   <s:hidden name="esSituacionActual" />
		   <s:hidden name="claveTipoEndoso" />
		   <s:hidden name="totalIncisos"/>
		   <s:hidden name="existeEdicionPoliza" id="existeEdicionPoliza"/>
 	<center>
 		<table id="agregar" style="border: 0px;" width="100%" border="0">
 			<tr>
	 			<th colspan="2" width="20%"><img border='0px' alt='Afirme' title='Afirme' src='/MidasWeb/img/logo_afirme1.gif'/></th>
	 			<th colspan="2" style="text-align:center;"><s:text name="POLIZA DE SEGUROS PARA"/><BR/><s:text name="AUTOS"/></th>
	 			<th colspan="2">
	 				<table id="agregar" width="100%" border="1">
	 					<tr><th colspan="2">
				 				<div style="width:15%;display:inline;float:left;"><s:text name="midas.imprescionpoliza.edicion.poliza"/></div>
				 				<div style="width:70%;display:inline;float:left;text-align:center;"><s:property value="datosPoliza.numeroPoliza"/></div>
				 				<div style="width:15%;display:inline;float:right;text-align:right;"><s:property value="datosPoliza.idCentroEmisor"/></div>
						</th></tr>
	 					<tr><th style="text-align:center;" colspan="2"><s:text name="midas.imprescionpoliza.edicion.vigencia"/></th></tr>
	 					<tr>
	 						<th>
	 							<div style="width:100%;text-align:center;"><s:text name="midas.imprescionpoliza.edicion.desde"/></div>
								<div style="width:60%;display:inline;float:left;padding-top:10px;"><s:property value="datosPoliza.fechaInicioVigenciaStr"/></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield name="edicionPoliza.horaInicioVigencia" id="horaInicioVigencia" 
	 									value="%{edicionPoliza.horaInicioVigencia}" maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:10px;"><s:text name=" Hrs" /></div>
	 							</div>
	 						</th>
	 						<th>
	 							<div style="width:100%;text-align:center;"><s:text name="midas.imprescionpoliza.edicion.hasta" /></div>
	 							<div style="width:60%;display:inline;float:left;padding-top:10px;"><s:property value="datosPoliza.fechaFinVigenciaStr"/></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield name="edicionPoliza.horaFinVigencia" id="horaFinVigencia" 
	 									value="%{edicionPoliza.horaFinVigencia}" maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:10px;"><s:text name=" Hrs" /></div>
	 							</div>
	 						</th>
	 					</tr>
	 					<tr id="errorHora" style="display:none;color:red;">
	 						<th colspan="2">
	 							<div >La hora debe tener el formato: 00:00</div>
	 						</th>
	 					</tr>
	 				</table>
	 			</th>
 			</tr>
 		</table>
 	</center>
 	
 	<center>
 		<table id="agregar" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div class="titulo" style="width: 98%;">
					<s:text name="midas.imprescionpoliza.edicion.infogeneral"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:center;">
					<s:text name="midas.imprescionpoliza.edicion.infoasegurado"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textfield name="edicionPoliza.nombreContratante" id="nombreContratante" cssClass="cajaTextoM2" size="112"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textarea name="edicionPoliza.txDomicilio" id="domicilio" 
					cssClass="cajaTextoM2" cssStyle="font-family: Verdana,Arial,Helvetica,sans-serif;" cols="72" rows="3"
					onblur="truncarTexto(this,3000);"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="RFC"/>
 				</th>
 				<th colspan="5"><div style="width: 98%;">
					<s:textfield name="edicionPoliza.rfcContratante" id="rfcContratante" cssClass="cajaTextoM2" size="75"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="ID ASEGURADO"/>
 				</th>
 				<th colspan="5"><div style="width: 98%;">
					<s:textfield name="edicionPoliza.idToPersonaContratante" id="idContratante" cssClass="cajaTextoM2 jQnumeric jQrestrict" size="75"/>	
				</div></th>
 			</tr>
 		</table>
 	</center>
 	
 	<center>
 		<table id="agregar" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div class="titulo" style="width: 98%;">
					<s:text name="midas.imprescionpoliza.edicion.desgloselineas"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:left;">
					<s:text name="midas.imprescionpoliza.edicion.textodesglose"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div id="edicionPolizaGridContainer" >
					<div id="listadoLineasNegocioGrid" style="width:93%;height:200px;"></div>
					<div id="pagingArea"></div><div id="infoArea"></div>
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="4">
 					<div style="width: 98%;text-align:center;vertical-align:top;"><s:text name="midas.imprescionpoliza.edicion.observaciones"/></div>
 					<div style="width: 98%;text-align:center;vertical-align:top;">
 						<s:textarea name="edicionPoliza.observaciones" id="observaciones" 
						cssClass="cajaTextoM2" cssStyle="font-family: Verdana,Arial,Helvetica,sans-serif;"	cols="40" rows="7"
						onblur="truncarTexto(this,3000);"/>
 					</div>
 				</th>
 				<th colspan="2">
 					<table id="agregar" width="100%">
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.imprimirprimas"/></th>
 							<th><div style="width:20%;display:inline;float:left;" ><s:checkbox id="imprimirPrimas" name="edicionPoliza.imprimirPrimas" /></div>
 							<div style="width:20%;display:inline;float:left;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoprimasinciso"/>" 
								title="<s:text name="midas.impresionpoliza.edicion.infoprimaspoliza"/>"  
								src="<s:url value='/img/information.gif'/>" /></div></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.totalprimas"/></th>
 							<th><s:textfield value="%{datosPoliza.totalPrimas}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.descuento"/></th>
 							<th><s:textfield value="%{datosPoliza.descuento}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.primaneta"/></th>
 							<th><s:textfield value="%{datosPoliza.primaNetaCotizacion}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.financiamiento"/></th>
 							<th><s:textfield value="%{datosPoliza.montoRecargoPagoFraccionado}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 							
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.gastosexpedicion"/></th>
 							<th><s:textfield value="%{datosPoliza.derechosPoliza}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.iva"/></th>
 							<th><s:textfield name="edicionPoliza.montoIva" id="montoIva"  cssStyle="width:100px;" cssClass="cajaTextoM2 jQfloat jQrestrict formatCurrency validarPrima" /></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.primatotal"/></th>
 							<th><s:textfield value="%{datosPoliza.primaNetaTotal}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.moneda"/></th>
 							<th><s:property value="datosPoliza.descripcionMoneda"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.formapago"/></th>
 							<th><s:property value="datosPoliza.descripcionFormaPago"/></th>
 						</tr>
 					</table>
 				</th>
 			</tr>
 			
 		</table>
 	</center>
 	
 	<center>
 		<table id="agregar" style="border:0px;" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:left;">
					<s:text name="midas.imprescionpoliza.edicion.articulo25"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textfield name="edicionPoliza.datosAgenteCaratula" id="datosAgenteCaratula" cssClass="cajaTextoM2" size="112"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:left;">
					<s:text name="midas.imprescionpoliza.edicion.articulo20"/>	
				</div></th>
 			</tr>
 		</table>
 	</center>
 	
	<center>
     <table>
		     <tr>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 80px;">
			      <a href="javascript: void(0);"
				  onclick="guardarImpresionPolizaEditada();"> 
				  <s:text
				  name="midas.boton.guardar" /> </a>
			    </div>
			  </th>
			  <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 80px;">
			      <a href="javascript: void(0);"
				  onclick="imprimirPolizaEditada();"> 
				  <s:text
				  name="midas.boton.imprimir" /> </a>
			    </div>
			  </th>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 80px;">
			      <a href="javascript: void(0);"
				  onclick="restaurarImpresionPolizaEditada();"> 
				  <s:text
				  name="midas.imprescionpoliza.edicion.restaurar" /> </a>
			    </div>
			  </th>		
		      <th>
			     <div class="btn_back w140"  style="display:inline; float: left;width: 80px; ">
			      <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){parent.cerrarVentanaModal('mostrarEditarImpresionPoliza');}"> 
				  <s:text
				  name="midas.boton.cerrar" /> </a>
			    </div>	
		       </th>
		      </tr>
	         </table>
			
	</center>
	
	</s:form>
	
</div>