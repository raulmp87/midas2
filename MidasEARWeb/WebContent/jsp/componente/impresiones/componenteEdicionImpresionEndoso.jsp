<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/componente/impresiones/edicionImpresionPoliza.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_sub_row.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">

 function imprimirEndosoEditado(){
 	if(confirm("Solo los cambios guardados aparecer\u00E1n en la impresi\u00F3n. \u00BFDesea continuar?")){
	 	removeCurrencyFormatOnTxtInput();
		jQuery("#contenedorEdicionImpresionForm").attr("action","imprimirEndosoEditado.action");
	 	parent.submitVentanaModal("mostrarEditarImpresionEndoso", contenedorEdicionImpresionForm);
	 	return false;
 	}
 }
	
 function guardarImpresionEndosoEditado(){
 	removeCurrencyFormatOnTxtInput();
 	if(validateAll(true)){
		 	jQuery("#contenedorEdicionImpresionForm").attr("action","guardarImpresionEndosoEditado.action");
		 	parent.submitVentanaModal("mostrarEditarImpresionEndoso", contenedorEdicionImpresionForm);
		 	return false;
 	}
 }
 
 function restaurarImpresionEndosoEditado(){
 	removeCurrencyFormatOnTxtInput();
 	jQuery("#contenedorEdicionImpresionForm").attr("action","restaurarImpresionEndosoEditado.action");
 	parent.submitVentanaModal("mostrarEditarImpresionEndoso", contenedorEdicionImpresionForm);
 	return false;
 }
 
 function validarHora(campoTexto){
 	var horaVigencia = jQuery(campoTexto).val();
 	if(horaVigencia < 0 ){
 		jQuery(campoTexto).val(0);
 		parent.mostrarMensajeInformativo("La hora no debe ser menor a 0.","30");
 	}
 	if(horaVigencia > 24){
 		jQuery(campoTexto).val(24);
 		parent.mostrarMensajeInformativo("La hora no debe ser mayor a 24.","30");
 	}
 }


  var validos = ":0123456789";
 function soloNumerosYDosPuntos(campo,esHoraEndoso) {
	var hasError = false;
    for (var i=0; i<campo.value.length; i++) {
     if (validos.indexOf(campo.value.charAt(i)) == -1){
    	 campo.value="";
    	 hasError = true;
    	 jQuery(campo).css("background-color", "#F5A9A9");
    	 if(esHoraEndoso){jQuery("#errorHoraEndoso").show();}
    	 	else{jQuery("#errorHora").show();}
    }}
    if(!hasError){
		jQuery(campo).css("background-color", "#ffffff");
		if(esHoraEndoso){jQuery("#errorHoraEndoso").hide();}
			else{jQuery("#errorHora").hide();}
    }
 }

 jQuery(document).ready(function(){
 	mostrarIncluirEdicionEndoso();
 	initCurrencyFormatOnTxtInput();
 });
</script> 

<div id="detalle" >
 <div id="indicadorEdicionInciso" style="display:none;"></div>
 	<s:form action="mostrarEditarImpresionInciso" namespace="/impresiones/componente" id="contenedorEdicionImpresionForm">
		   <s:hidden name="id" id="id"/>
		   <s:hidden name="tipoImpresion"/>
		   <s:hidden name="idToPoliza"/>
		   <s:hidden name="validOn" />
		   <s:hidden name="validOnMillis" />
		   <s:hidden name="recordFrom" />
		   <s:hidden name="recordFromMillis"  id="recordFromMillis"/>
		   <s:hidden name="claveTipoEndoso" id="claveTipoEndoso"/>
		   <s:hidden name="existeEdicionEndoso" id="existeEdicionEndoso" />
		   
 	<center>
 		<table id="agregar" style="border: 0px;" width="100%" border="0">
 			<tr>
	 			<th colspan="2" width="170px"><img border='0px' alt='Afirme' title='Afirme' src='/MidasWeb/img/logo_afirme1.gif'/></th>
	 			<th colspan="2" style="text-align:center;"><div style="width:250px;" ><s:text name="edicionEndoso.informacionImpresionEndoso['P_TIPO_ENDOSO_TITULO']"/></div></th>
	 			<th colspan="2">
	 				<table id="agregar" width="100%" border="1">
	 					<tr><th colspan="2">
				 				<div style="width:60px;display:inline;float:left;"><s:text name="midas.impresionpoliza.edicion.polizano"/></div>
				 				<div style="width:100px;display:inline;float:left;text-align:center;"><s:text name="edicionEndoso.informacionImpresionEndoso['P_POLICY_NUMBER']"/></div>
				 				<div style="width:60px;display:inline;float:left;"><s:text name="midas.impresionpoliza.edicion.endosono"/></div>
				 				<div style="width:20px;display:inline;float:right;text-align:right;"><s:text name="edicionEndoso.informacionImpresionEndoso['P_DESC_NUM_ENDOSO']"/></div>
						</th></tr>
	 					<tr><th style="text-align:center;" colspan="2"><s:text name="midas.imprescionpoliza.edicion.vigencia"/></th></tr>
	 					<tr>
	 						<th>
	 							<div style="width:100%;text-align:center;"><s:text name="midas.imprescionpoliza.edicion.desde"/></div>
								<div style="width:60%;display:inline;float:left;padding-top:7px;"><s:text name="edicionEndoso.informacionImpresionEndoso['P_F_INI_VIGENCIA_POLIZA_STR']"/></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield name="edicionEndoso.horaInicioVigenciaPoliza" id="horaInicioVigencia" 
	 									maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this,false);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:7px;"><s:text name=" Hrs" /></div>
	 							</div>
	 						</th>
	 						<th>
	 							<div style="width:100%;text-align:center;"><s:text name="midas.imprescionpoliza.edicion.hasta" /></div>
	 							<div style="width:60%;display:inline;float:left;padding-top:7px;"><s:text name="edicionEndoso.informacionImpresionEndoso['P_F_FIN_VIGENCIA_POLIZA_STR']"/></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield  name="edicionEndoso.horaFinVigenciaPoliza" id="horaFinVigencia" 
	 									maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this,false);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:7px;"><s:text name=" Hrs" /></div>
	 							</div>
	 						</th>
	 					</tr>
	 					<tr id="errorHora" style="display:none;color:red;">
	 						<th colspan="2">
	 							<div >La hora debe tener el formato: 00:00</div>
	 						</th>
	 					</tr>
	 				</table>
	 			</th>
 			</tr>
 		</table>
 	</center>
 
 	<center>
 		<table id="agregar" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div class="titulo" style="width: 98%;">
					<s:text name="midas.impresionpoliza.edicion.infogeneralendoso"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="3"><div style="width: 98%;text-align:center;">
					<s:text name="midas.impresionpoliza.edicion.infocontratante"/>
				</div></th>
				<th colspan="3"><div style="width: 98%;text-align:center;">
					<s:text name="midas.impresionpoliza.edicion.desgloseprimas"/>
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="3">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_NOM_SOLICITANTE']}" id="nombreContratante" cssClass="cajaTextoM2 w300" disabled="true"/>
				</th>
				<th >
					<s:text name="midas.impresionpoliza.edicion.tipoendos"/>
				</th>
				<th colspan="2">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_TIPO_ENDOSO']}" id="tipoEndoso" cssClass="cajaTextoM2 w200" disabled="true"/>
				</th>
 			</tr>
 			<tr>
 				<th colspan="3"><div style="width: 98%;">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_CALLE_NUMERO']}" id="calleNumero" cssClass="cajaTextoM2 w300" disabled="true"/>	
				</div></th>
				<th >
					<s:text name="midas.imprescionpoliza.edicion.primaneta"/>
				</th>
				<th colspan="2">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_V_PRIMA_NETA']}" id="primaNeta" cssClass="cajaTextoM2 w200 formatCurrency" disabled="true"/>
				</th>
 			</tr>
 			<tr>
 				<th colspan="3"><div style="width: 98%;">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_COLONIA']}" id="colonia" cssClass="cajaTextoM2 w300" disabled="true"/>	
				</div></th>
				<th >
					<s:text name="midas.imprescionpoliza.edicion.descuento"/>
				</th>
				<th colspan="2">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_DESCUENTO']}" id="descuento" cssClass="cajaTextoM2 w200 formatCurrency" disabled="true"/>
				</th>
 			</tr>
 			<tr>
 				<th colspan="3"><div style="width: 98%;">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_CODIGO_POSTAL']}" id="cp" cssClass="cajaTextoM2 w300" disabled="true"/>	
				</div></th>
				<th >
					<s:text name="midas.imprescionpoliza.edicion.financiamiento"/>
				</th>
				<th colspan="2">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_IMP_RCGOS_PAGOFR']}" id="financiamiento" cssClass="cajaTextoM2 w200 formatCurrency" disabled="true"/>
				</th>
 			</tr>
 			<tr>
 				<th colspan="3"><div style="width: 98%;">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_CITY']}" id="ciudad" cssClass="cajaTextoM2 w300" disabled="true"/>	
				</div></th>
				<th >
					<s:text name="midas.imprescionpoliza.edicion.gastosexpedicion"/>
				</th>
				<th colspan="2">
					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_IMP_DERECHOS']}" id="gastosExpedicion" cssClass="cajaTextoM2 w200 formatCurrency" disabled="true"/>
				</th>
 			</tr>
 			<tr>
 				<th colspan="3">
					<s:textfield value="%{edicionEndoso.informacionImpresionEndoso['P_STATE']}" id="estado" cssClass="cajaTextoM2 w300" disabled="true"/>	
				</th>
				<th>
					<s:text name="midas.imprescionpoliza.edicion.iva"/>
				</th>
 				<th colspan="2">
 					<s:textfield  value="%{edicionEndoso.informacionImpresionEndoso['P_V_IVA']}" id="iva" cssClass="cajaTextoM2 w200 formatCurrency" disabled="true"/>
 				</th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="RFC"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield value="%{edicionEndoso.informacionImpresionEndoso['P_RFC']}" id="rfcContratante" cssClass="cajaTextoM2 w200" disabled="true"/>	
				</div></th>
				<th bgcolor="GRAY">
 				</th>
 				<th bgcolor="GRAY" colspan="2"><div style="width: 98%;">
					<s:textfield value="%{edicionEndoso.informacionImpresionEndoso['P_IMP_PRIMA_TOTAL']}" id="primaTotal" cssClass="cajaTextoM2 w200 formatCurrency" disabled="true"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="midas.imprescionpoliza.edicion.idcontratante"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield value="%{edicionEndoso.informacionImpresionEndoso['P_CLIENT_ID']}" id="idContratante" cssClass="cajaTextoM2 w200" disabled="true"/>	
				</div></th>
				<th>
 					<s:text name="midas.imprescionpoliza.edicion.moneda"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield value="%{edicionEndoso.informacionImpresionEndoso['P_DESC_MONEDA']}" id="tipoUso" cssClass="cajaTextoM2 w200" disabled="true"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 				</th>
 				<th colspan="2">
				</th>
				<th>
 					<s:text name="midas.imprescionpoliza.edicion.formapago"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield value="%{edicionEndoso.informacionImpresionEndoso['P_PAYMENT_METHOD']}" id="tipoUso" cssClass="cajaTextoM2 w200" disabled="true"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 				</th>
 				<th colspan="2">
				</th>
				<th>
 					<s:text name="midas.impresionpoliza.edicion.vigenciaendoso"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
						<div style="width:50%;text-align:center;display:inline;float:left;">
								<div style="width:60%;display:inline;float:left;padding-top:7px;font-weight:normal;"><s:text name="edicionEndoso.informacionImpresionEndoso['P_F_INI_VIGENCIA_ENDOSO']"/></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield  name="edicionEndoso.horaInicioVigenciaEndoso" id="horaInicioVigencia" 
	 									maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this,true);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:7px;font-weight:normal;"><s:text name=" Hrs" /></div>
	 							</div>
	 					</div>
	 					<div style="width:50%;text-align:center;display:inline;float:left;">
	 							<div style="width:60%;display:inline;float:left;padding-top:7px;font-weight:normal;"><s:text name="edicionEndoso.informacionImpresionEndoso['P_F_FIN_VIGENCIA_ENDOSO']"/></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield  name="edicionEndoso.horaFinVigenciaEndoso" id="horaFinVigencia" 
	 									maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this,true);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:7px;font-weight:normal;"><s:text name=" Hrs" /></div>
	 							</div>
	 					</div>
				</div></th>
 			</tr>
 			<tr id="errorHoraEndoso" style="display:none;color:red;">
 						<th colspan="3">
 						</th>
						<th colspan="3">
							<div >La hora debe tener el formato: 00:00</div>
						</th>
	 		</tr>
 		</table>
 	</center>
 	
	<center>
     <table>
		     <tr>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 100px;">
			      <a href="javascript: void(0);"
				  onclick="guardarImpresionEndosoEditado();"> 
				  <s:text
				  name="midas.boton.guardar" /> </a>
			    </div>
			  </th>
			  <th>
			    <div class="btn_back w180"  style="display:inline; float: left;width: 100px;">
			      <a href="javascript: void(0);"
				  onclick="imprimirEndosoEditado();"> 
				  <s:text
				  name="Imprimir" /> </a>
			    </div>
			  </th>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 100px;">
			      <a href="javascript: void(0);"
				  onclick="restaurarImpresionEndosoEditado();"> 
				  <s:text
				  name="midas.imprescionpoliza.edicion.restaurar" /> </a>
			    </div>
			  </th>		
		      <th>
			     <div class="btn_back w140"  style="display:inline; float: left;width: 100px; ">
			      <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){parent.cerrarVentanaModal('mostrarEditarImpresionEndoso');}"> 
				  <s:text
				  name="midas.boton.cerrar" /> </a>
			    </div>	
		       </th>
		      </tr>
	         </table>
			
	</center>
	
	</s:form>
	
</div>