<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/componente/impresiones/edicionImpresionPoliza.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_filter.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script> 
<script type="text/javascript" src="<s:url value='/js/dataProcessorEventHandlers.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ext/dhtmlxgrid_pgn.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_excell_sub_row.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">

 function imprimirIncisosEditados(){
 	if(confirm("Solo los cambios guardados aparecer\u00E1n en la impresi\u00F3n. \u00BFDesea continuar?")){
	 	removeCurrencyFormatOnTxtInput();
	 	jQuery("#id").val(-1);
		jQuery("#contenedorEdicionImpresionForm").attr("action","imprimirIncisosEditados.action");
	 	parent.submitVentanaModal("mostrarEditarImpresionInciso", contenedorEdicionImpresionForm);
	 	return false;
 	}
 }
	
 function guardarImpresionIncisoEditado(){
 	removeCurrencyFormatOnTxtInput();
 	if(validateAll(true)){
 		if(validarMontosPrimas()){
		 	jQuery("#id").val(-1);
		 	obtenerCoberturasParams();
		 	codificarInformacion();
		 	jQuery("#contenedorEdicionImpresionForm").attr("enctype","application/x-www-form-urlencoded;charset=UTF-8");
		 	jQuery("#contenedorEdicionImpresionForm").attr("action","guardarImpresionIncisoEditado.action");
		 	parent.submitVentanaModal("mostrarEditarImpresionInciso", contenedorEdicionImpresionForm);
		 	return false;
	 	}
 	}
 }
 
 function codificarInformacion(){
 	jQuery("#nombreContratante").val(encodeStr(jQuery("#nombreContratante").val()));
 	jQuery("#nombreContratanteInciso").val(encodeStr(jQuery("#nombreContratanteInciso").val()));
 	jQuery("#calleNumero").val(encodeStr(jQuery("#calleNumero").val()));
 	jQuery("#colonia").val(encodeStr(jQuery("#colonia").val()));
 	jQuery("#estado").val(encodeStr(jQuery("#estado").val()));
 	jQuery("#rfcContratante").val(encodeStr(jQuery("#rfcContratante").val()));
 	jQuery("#tipoUso").val(encodeStr(jQuery("#tipoUso").val()));
 	jQuery("#tipoServicio").val(encodeStr(jQuery("#tipoServicio").val()));
 	jQuery("#observaciones").val(encodeStr(jQuery("#observaciones").val()));
 	jQuery("#datosAgenteCaratula").val(encodeStr(jQuery("#datosAgenteCaratula").val()));
 	jQuery("#telefono1").val(encodeStr(jQuery("#telefono1").val()));
 	jQuery("#descTelefono1").val(encodeStr(jQuery("#descTelefono1").val()));
 	jQuery("#telefono2").val(encodeStr(jQuery("#telefono2").val()));
 	jQuery("#descTelefono2").val(encodeStr(jQuery("#descTelefono2").val()));
 	jQuery("#telefono3").val(encodeStr(jQuery("#telefono3").val()));
 	jQuery("#descTelefono3").val(encodeStr(jQuery("#descTelefono3").val()));
 }
 
 function obtenerCoberturasParams(){
 	var contador = 0;
	listadoCoberturasIncisoGrid.forEachRow(function(id){
		var infoRiesgo = encodeStr(listadoCoberturasIncisoGrid.cells(id,0).getValue());
		var infoSumaAsegurada = encodeStr(listadoCoberturasIncisoGrid.cells(id,2).getValue());
		var infoDeducible = encodeStr(listadoCoberturasIncisoGrid.cells(id,3).getValue());
		
		var riesgoInput = document.createElement("input");
		riesgoInput.type = "hidden";
		riesgoInput.name = "datosCoberturasPoliza[" + contador + "].riesgo";
		riesgoInput.value = infoRiesgo;
		riesgoInput.id = "riesgo" + contador;
		
		var sumaInput = document.createElement("input");
		sumaInput.type = "hidden";
		sumaInput.name = "datosCoberturasPoliza[" + contador + "].sumaAsegurada";
		sumaInput.value = infoSumaAsegurada;
		sumaInput.id = "sumaAsegurada" + contador;
		
		var deducibleInput = document.createElement("input");
		deducibleInput.type = "hidden";
		deducibleInput.id = "riesgo" + contador;
		deducibleInput.name = "datosCoberturasPoliza[" + contador + "].deducible";
		deducibleInput.value = infoDeducible;
		deducibleInput.id = "deducible" + contador;
		
		document.getElementById("datosCoberturasContainer").appendChild(riesgoInput);
		document.getElementById("datosCoberturasContainer").appendChild(sumaInput);
		document.getElementById("datosCoberturasContainer").appendChild(deducibleInput);
		
		console.log("riesgo: " + jQuery("#riesgo"+contador).val());
		console.log("sumaAsegurada: " + jQuery("#sumaAsegurada"+contador).val());
		console.log("deducible: " + jQuery("#deducible"+contador).val());
		contador++;
	});
}
 
 function validarMontosPrimas(){
 	var esValido = true;
 	var imprimirPrimas = jQuery("#imprimirPrimas").attr('checked');
	if(imprimirPrimas){
		jQuery(".validarPrima").each(
			function(){
			var these = jQuery(this);
                if( isEmpty(these.val()) ){
                      these.addClass("errorField");
                      esValido = false;
                      parent.mostrarMensajeInformativo("Si selecciona Imprimir Primas debe capturar todos los montos.","30");
                } else {
                      these.removeClass("errorField");
                }
		});		
	}
	return esValido;
 }
 
 function restaurarImpresionIncisoEditado(){
 	removeCurrencyFormatOnTxtInput();
 	jQuery("#id").val(-1);
 	jQuery("#contenedorEdicionImpresionForm").attr("action","restaurarImpresionIncisoEditado.action");
 	parent.submitVentanaModal("mostrarEditarImpresionInciso", contenedorEdicionImpresionForm);
 	return false;
 }
 
 var mostrarCoberturasPolizaPath = '<s:url action="mostrarCoberturasPoliza" namespace="/impresiones/componente"/>';
 var listadoCoberturasIncisoGrid
 function mostrarCoberturasPoliza(){
 		removeCurrencyFormatOnTxtInput();
		jQuery("#listadoCoberturasIncisoGrid").empty();
	
		listadoCoberturasIncisoGrid = new dhtmlXGridObject('listadoCoberturasIncisoGrid');
		listadoCoberturasIncisoGrid.attachEvent("onXLS", function(grid_obj){blockPage();mostrarIndicadorCarga("indicadorEdicionInciso");});
		listadoCoberturasIncisoGrid.attachEvent("onXLE", function(grid_obj){unblockPage();ocultarIndicadorCarga("indicadorEdicionInciso");});
		listadoCoberturasIncisoGrid.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){
    		if(stage == 2){
    			listadoCoberturasIncisoGrid.cells(rId, cInd).setValue(nValue.toUpperCase());
    		}
    		return true;
		});
		
		formParams = jQuery(document.contenedorEdicionImpresionForm).serialize();
		var url = mostrarCoberturasPolizaPath + '?' +  formParams;
		listadoCoberturasIncisoGrid.load( url );
		initCurrencyFormatOnTxtInput();
 }
 
 function validarHora(campoTexto){
 	var horaVigencia = jQuery(campoTexto).val();
 	if(horaVigencia < 0 ){
 		jQuery(campoTexto).val(0);
 		parent.mostrarMensajeInformativo("La hora no debe ser menor a 0.","30");
 	}
 	if(horaVigencia > 24){
 		jQuery(campoTexto).val(24);
 		parent.mostrarMensajeInformativo("La hora no debe ser mayor a 24.","30");
 	}
 }
 
 function configurarPlaceholders(){
 	jQuery("#nombreContratante").attr("placeholder","Nombre Contratante");
 	jQuery("#nombreContratanteInciso").attr("placeholder","Nombre Contratante Inciso");
 	jQuery("#calleNumero").attr("placeholder","Calle y No.");
 	jQuery("#colonia").attr("placeholder","Colonia");
 	jQuery("#estado").attr("placeholder","Ciudad y Estado");
 	jQuery("#datosAgenteCaratula").attr("placeholder","Datos Agente");
 	jQuery("#telefono1").attr("placeholder","Tel\u00C9fono");
 	jQuery("#descTelefono1").attr("placeholder","Descripci\u00F3n");
 	jQuery("#telefono2").attr("placeholder","Tel\u00C9fono");
 	jQuery("#descTelefono2").attr("placeholder","Descripci\u00F3n");
 	jQuery("#telefono3").attr("placeholder","Tel\u00C9fono");
 	jQuery("#descTelefono3").attr("placeholder","Descripci\u00F3n");
 }


  var validos = ":0123456789";
 function soloNumerosYDosPuntos(campo) {
	var hasError = false;
    for (var i=0; i<campo.value.length; i++) {
     if (validos.indexOf(campo.value.charAt(i)) == -1){
    	 campo.value="";
    	 hasError = true;
    	 jQuery(campo).css("background-color", "#F5A9A9");
    	 jQuery("#errorHora").show();
    }}
    if(!hasError){
       jQuery(campo).css("background-color", "#ffffff");
       jQuery("#errorHora").hide();
    }
 }
  
 function habilitarAgenteCaratula(){
 	var imprimirAgente = jQuery("#imprimirAgenteCaratula").attr('checked');
 	if(imprimirAgente){
 		jQuery("#datosAgenteCaratula").removeAttr("disabled");
 	}else{
 		jQuery("#datosAgenteCaratula").attr("disabled","true");
 		jQuery("#datosAgenteCaratula").val("");
 	}
 }

 jQuery(document).ready(function(){
 	mostrarIncluirEdicionInciso();
 	initCurrencyFormatOnTxtInput();
 	mostrarCoberturasPoliza();
 	configurarPlaceholders();
 	habilitarAgenteCaratula();
 });
</script> 

<div id="detalle" >
 <div id="indicadorEdicionInciso" style="display:none;"></div>
 	<s:form action="mostrarEditarImpresionInciso" namespace="/impresiones/componente" id="contenedorEdicionImpresionForm">
		   <s:hidden name="id" id="id"/>
		   <s:hidden name="tipoImpresion"/>
		   <s:hidden name="idToPoliza"/>
		   <s:hidden name="validOn" />
		   <s:hidden name="validOnMillis" />
		   <s:hidden name="recordFrom" />
		   <s:hidden name="recordFromMillis" />
		   <s:hidden name="esSituacionActual" />
		   <s:hidden name="claveTipoEndoso" />
		   <s:hidden name="totalIncisos" />
		   <s:hidden name="existeEdicionInciso" id="existeEdicionInciso" />
		   <div id="datosCoberturasContainer"></div>
		   
 	<center>
 		<table id="agregar" style="border: 0px;" width="100%" border="0">
 			<tr>
	 			<th colspan="2" width="20%"><img border='0px' alt='Afirme' title='Afirme' src='/MidasWeb/img/logo_afirme1.gif'/></th>
	 			<th colspan="2" style="text-align:center;"><s:text name="POLIZA DE SEGUROS PARA"/><BR/><s:text name="AUTOS"/></th>
	 			<th colspan="2">
	 				<table id="agregar" width="100%" border="1">
	 					<tr><th colspan="2">
				 				<div style="width:15%;display:inline;float:left;"><s:text name="midas.imprescionpoliza.edicion.poliza"/></div>
				 				<div style="width:70%;display:inline;float:left;text-align:center;"><s:property value="datosPoliza.numeroPoliza"/></div>
				 				<div style="width:15%;display:inline;float:right;text-align:right;"><s:property value="datosPoliza.idCentroEmisor"/></div>
						</th></tr>
	 					<tr><th style="text-align:center;" colspan="2"><s:text name="midas.imprescionpoliza.edicion.vigencia"/></th></tr>
	 					<tr>
	 						<th>
	 							<div style="width:100%;text-align:center;"><s:text name="midas.imprescionpoliza.edicion.desde"/></div>
								<div style="width:60%;display:inline;float:left;padding-top:7px;"></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield  name="edicionInciso.horaInicioVigencia" id="horaInicioVigencia" 
	 									value="%{edicionInciso.horaInicioVigencia}" maxlength="5"  cssClass="cajaTextoM2  w30" onblur="soloNumerosYDosPuntos(this);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:7px;"><s:text name=" Hrs" /></div>
	 							</div>
	 						</th>
	 						<th>
	 							<div style="width:100%;text-align:center;"><s:text name="midas.imprescionpoliza.edicion.hasta" /></div>
	 							<div style="width:60%;display:inline;float:left;padding-top:7px;"></div>
	 							<div style="width:40%;display:inline;float:left;">
	 								<div style="width:70%;display:inline;float:left;"><s:textfield name="edicionInciso.horaFinVigencia" id="horaFinVigencia" 
	 									value="%{edicionInciso.horaFinVigencia}" maxlength="5" cssClass="cajaTextoM2 w30" onblur="soloNumerosYDosPuntos(this);"/></div>
	 								<div style="width:20%;display:inline;float:left;padding-top:7px;"><s:text name=" Hrs" /></div>
	 							</div>
	 						</th>
	 					</tr>
	 					<tr id="errorHora" style="display:none;color:red;">
	 						<th colspan="2">
	 							<div >La hora debe tener el formato: 00:00</div>
	 						</th>
	 					</tr>
	 				</table>
	 			</th>
 			</tr>
 		</table>
 	</center>
 	
 	<center>
 		<table id="agregar" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div class="titulo" style="width: 98%;">
					<s:text name="midas.imprescionpoliza.edicion.infogeneral"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:center;">
					<s:text name="midas.imprescionpoliza.edicion.infoasegurado"/>
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<div style="width: 470px;display:inline;float:left;"><s:textfield  name="edicionInciso.nombreContratante" id="nombreContratante" cssClass="cajaTextoM2 w450" /></div>
					<div style="width:30px;display:inline;float:left;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoinciso"/>" 
						title="<s:text name="midas.impresionpoliza.edicion.infoinciso"/>"  
						src="<s:url value='/img/information.gif'/>" /></div>
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textfield  name="edicionInciso.nombreContratanteInciso" id="nombreContratanteInciso" cssClass="cajaTextoM2 w450"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textfield  name="edicionInciso.calleNumero" id="calleNumero" cssClass="cajaTextoM2 w450" />	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textfield  name="edicionInciso.colonia" id="colonia" cssClass="cajaTextoM2 w450" />	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;">
					<s:textfield  name="edicionInciso.estado" id="estado" cssClass="cajaTextoM2 w450" />	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="C.P."/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield name="edicionInciso.codigoPostal" id="codigoPostal" cssClass="cajaTextoM2 jQnumeric jQrestrict w200" maxlength="5" />	
				</div></th>
				<th></th>
 				<th colspan="2"></th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="RFC"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield name="edicionInciso.rfcContratante" id="rfcContratante" cssClass="cajaTextoM2 w200" />	
				</div></th>
				<th>
 					<s:text name="Servicio"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield name="edicionInciso.tipoServicio" id="tipoServicio" cssClass="cajaTextoM2 w200" />	
				</div></th>
 			</tr>
 			<tr>
 				<th>
 					<s:text name="ID ASEGURADO"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield name="edicionInciso.idToPersonaContratante" id="idContratante" cssClass="cajaTextoM2 jQnumeric jQrestrict w200" />	
				</div></th>
				<th>
 					<s:text name="Uso"/>
 				</th>
 				<th colspan="2"><div style="width: 98%;">
					<s:textfield name="edicionInciso.tipoUso" id="tipoUso" cssClass="cajaTextoM2 w200" />	
				</div></th>
 			</tr>
 		</table>
 	</center>
 	
 	<center>
 		<table id="agregar" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div class="titulo" style="width: 98%;">
					<div style="width:230px;display:inline;" ><s:text name="DESGLOSE DE COBERTURAS"/></div>
					<div style="width:30px;display:inline;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoinciso"/>" 
						title="<s:text name="midas.impresionpoliza.edicion.infocoberturas"/>"  
						src="<s:url value='/img/information.gif'/>" /></div>
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:left;">
					<s:text name="midas.imprescionpoliza.edicion.textodesglose"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6"><div id="edicionIncisoGridContainer" >
					<div id="listadoCoberturasIncisoGrid" style="width:93%;height:200px;"></div>
					<div id="pagingArea"></div><div id="infoArea"></div>
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="4">
 					<div style="width: 98%;text-align:center;vertical-align:top;"><s:text name="midas.imprescionpoliza.edicion.observaciones"/></div>
 					<div style="width: 98%;text-align:center;vertical-align:top;">
 						<s:textarea name="edicionInciso.observaciones" id="observaciones" 
						cssClass="cajaTextoM2" cssStyle="font-family: Verdana,Arial,Helvetica,sans-serif;"	cols="40" rows="7"
						onblur="truncarTexto(this,3000);"/>
 					</div>
 				</th>
 				<th colspan="2">
 					<table id="agregar" width="100%">
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.imprimirprimas"/></th>
 							<th><div style="width:20%;display:inline;float:left;" ><s:checkbox id="imprimirPrimas" name="edicionInciso.imprimirPrimas" /></div>
 							<div style="width:20%;display:inline;float:left;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoprimasinciso"/>" 
								title="<s:text name="midas.impresionpoliza.edicion.infoprimasinciso"/>"  
								src="<s:url value='/img/information.gif'/>" /></div>
 							</th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.totalprimas"/></th>
 							<th><s:textfield value="%{datosPoliza.totalPrimas}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.descuento"/></th>
 							<th><s:textfield value="%{datosPoliza.descuento}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.primaneta"/></th>
 							<th><s:textfield value="%{datosPoliza.primaNetaCotizacion}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.financiamiento"/></th>
 							<th><s:textfield value="%{datosPoliza.montoRecargoPagoFraccionado}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 							
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.gastosexpedicion"/></th>
 							<th><s:textfield value="%{datosPoliza.derechosPoliza}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.iva"/></th>
 							<th><s:textfield name="edicionInciso.montoIva" id="montoIva"  cssStyle="width:100px;" cssClass="cajaTextoM2 jQfloat jQrestrict formatCurrency" /></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.primatotal"/></th>
 							<th><s:textfield value="%{datosPoliza.primaNetaTotal}" disabled="true" cssStyle="width:100px;" cssClass="cajaTextoM2 formatCurrency"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.moneda"/></th>
 							<th><s:property value="datosPoliza.descripcionMoneda"/></th>
 						</tr>
 						<tr>
 							<th><s:text name="midas.imprescionpoliza.edicion.formapago"/></th>
 							<th><s:property value="datosPoliza.descripcionFormaPago"/></th>
 						</tr>
 					</table>
 				</th>
 			</tr>
 			
 		</table>
 	</center>
 	
 	<center>
 		<table id="agregar" style="border:0px;" width="100%" border="0">
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:left;">
					<s:text name="midas.imprescionpoliza.edicion.articulo25"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th colspan="6">
	 				<div style="width: 470px;display:inline;float:left;">
						<s:textfield name="edicionInciso.datosAgenteCaratula" id="datosAgenteCaratula" cssClass="cajaTextoM2 w450" />	
					</div>
				     <div style="width:30px;display:inline;float:left;" >
				     	<s:checkbox id="imprimirAgenteCaratula" name="edicionInciso.imprimirAgenteCaratula" onchange="habilitarAgenteCaratula();"/>
				     </div>
				     <div style="width:90px;display:inline;float:left;" >
				     	<s:text name="midas.impresionpoliza.edicion.imprimiragente"/>
				     </div>
				     <div style="width:30px;display:inline;float:left;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoinciso"/>" 
						title="<s:text name="midas.impresionpoliza.edicion.infodatosagente"/>"  
						src="<s:url value='/img/information.gif'/>" /></div>
				</th>
 			</tr>
 			<tr>
 				<th colspan="6"><div style="width: 98%;text-align:left;">
					<s:text name="midas.imprescionpoliza.edicion.articulo20"/>	
				</div></th>
 			</tr>
 			<tr>
 				<th ><div style="width: 98%;">
					<s:textfield name="edicionInciso.descTelefono1" id="descTelefono1" cssClass="cajaTextoM2" maxlength="12" />
				</div></th>
				<td ><div style="width: 98%;">
					<s:textfield name="edicionInciso.telefono1" id="telefono1" cssClass="cajaTextoM2" maxlength="11" />	
				</div></td>
				<th ><div style="width: 98%;">
					<s:textfield name="edicionInciso.descTelefono2" id="descTelefono2" cssClass="cajaTextoM2" maxlength="12" />
				</div></th>
				<td ><div style="width: 98%;">
					<s:textfield name="edicionInciso.telefono2" id="telefono2" cssClass="cajaTextoM2" maxlength="11" />
				</div></td>
				<th ><div style="width: 98%;">
					<s:textfield name="edicionInciso.descTelefono3" id="descTelefono3" cssClass="cajaTextoM2" maxlength="16" />
				</div></th>
				<td ><div style="width: 98%;">
					<s:textfield name="edicionInciso.telefono3" id="telefono3" cssClass="cajaTextoM2" maxlength="16" />	
				</div></td>
 			</tr>
 		</table>
 	</center>
 	
	<center>
     <table>
		     <tr>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 100px;">
			      <a href="javascript: void(0);"
				  onclick="guardarImpresionIncisoEditado();"> 
				  <s:text
				  name="midas.boton.guardar" /> </a>
			    </div>
			  </th>
			  <th>
			    <div class="btn_back w180"  style="display:inline; float: left;width: 100px;">
			      <a href="javascript: void(0);"
				  onclick="imprimirIncisosEditados();"> 
				  <s:text
				  name="Imprimir todos" /> </a>
			    </div>
			  </th>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 100px;">
			      <a href="javascript: void(0);"
				  onclick="restaurarImpresionIncisoEditado();"> 
				  <s:text
				  name="midas.imprescionpoliza.edicion.restaurar" /> </a>
			    </div>
			  </th>		
		      <th>
			     <div class="btn_back w140"  style="display:inline; float: left;width: 100px; ">
			      <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){parent.cerrarVentanaModal('mostrarEditarImpresionInciso');}"> 
				  <s:text
				  name="midas.boton.cerrar" /> </a>
			    </div>	
		       </th>
		      </tr>
	         </table>
			
	</center>
	
	</s:form>
	
</div>