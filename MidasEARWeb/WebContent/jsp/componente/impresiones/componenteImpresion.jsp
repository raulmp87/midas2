<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/componente/impresiones/edicionImpresionPoliza.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>


<script type="text/javascript">

 function generarImpresion(){
 	
	var path = '/MidasWeb/impresiones/componente/generarImpresion.action?"'+ jQuery(document.contenedorImpresionForm).serialize();
	var imprimir = parent.dhxWins.window('mostrarContenedorImpresion');
		
 }
 
function mostrarEditarImpresionPoliza(tipoImpresion,idToPoliza,validOn,validOnMillis,
					recordFromMillis, recordFrom, claveTipoEndoso, esSituacionActual,id){
	console.log(tipoImpresion + ", " + idToPoliza + ", " + validOn + ", " + validOnMillis + ", " + recordFromMillis + ", " + recordFrom);
	var url = '/MidasWeb/impresiones/componente/mostrarEditarImpresionPoliza.action?tipoImpresion='+tipoImpresion+"&idToPoliza="+idToPoliza;
	if(validOn){
		url = url + "&validOn=" + validOn;
	}
	if(validOnMillis){
		url = url + "&validOnMillis=" + validOnMillis;
	}
	if(recordFromMillis){
		url = url + "&recordFromMillis=" + recordFromMillis;
	}
	if(recordFrom){
		url = url + "&recordFrom=" + recordFrom;
	}
	if(claveTipoEndoso){
		url = url + "&claveTipoEndoso=" + claveTipoEndoso;
	}
	if (esSituacionActual) {
		url += "&esSituacionActual=" + esSituacionActual;
	}
	if (id) {
		url += "&id=" + id;
	}
	parent.mostrarVentanaModal("mostrarEditarImpresionPoliza", 'Impresiones', 1, 1, 800, 600, url, null);
}

function mostrarEditarImpresionInciso(tipoImpresion,idToPoliza,validOn,validOnMillis,
					recordFromMillis, recordFrom, claveTipoEndoso, id){
	console.log(tipoImpresion + ", " + idToPoliza + ", " + validOn + ", " + validOnMillis + ", " + recordFromMillis + ", " + recordFrom);
	var url = '/MidasWeb/impresiones/componente/mostrarEditarImpresionInciso.action?tipoImpresion='+tipoImpresion+"&idToPoliza="+idToPoliza;
	if(validOn){
		url = url + "&validOn=" + validOn;
	}
	if(validOnMillis){
		url = url + "&validOnMillis=" + validOnMillis;
	}
	if(recordFromMillis){
		url = url + "&recordFromMillis=" + recordFromMillis;
	}
	if(recordFrom){
		url = url + "&recordFrom=" + recordFrom;
	}
	if(claveTipoEndoso){
		url = url + "&claveTipoEndoso=" + claveTipoEndoso;
	}
	if (id) {
		url += "&id=" + id;
	}
	parent.mostrarVentanaModal("mostrarEditarImpresionInciso", 'Impresiones', 1, 1, 800, 600, url, null);
}
 


 function setIncios(){
	if(jQuery("#chkinciso").is(":checked")){
     jQuery("#txtIncios").val("");		
  }
 }	
 function setInciosTxt(valor){
  if(valor.length > 0){
	 jQuery("input[name$='chkinciso']").removeAttr("checked");		
  }	
 }


 var validos = "-0123456789";
 function soloNumerosYGuion(campo,hasError) {
	 jQuery("#error").hide();
	    hasError = false;
    var letra;
    var bandera = true;
    for (var i=0; i<campo.value.length; i++) {
     if (validos.indexOf(campo.value.charAt(i)) == -1){
    	 campo.value="";
    	 bandera=false;
    	 hasError == true;
     }
    }
 
    if (!bandera) {
        jQuery('#txtIncios').css("background-color", "#FF0000");
        campo.value="";
        hasError = true;
    }
    if(bandera){
       jQuery('#txtIncios').css("background-color", "#ffffff");
    }
    if(hasError == true) {
    	jQuery("#txtIncioshidden").after('<span id="error">Ej: 1-10</span>');
    	campo.value=""; return false; 
    }
 }	

 function submitForm(){
   	  //mostrarIndicadorCarga('indicador');	
      sumbitData();
 }
 function sumbitData(){ 
	document.contenedorImpresionForm.submit();	 			
    
 }
 
 function habilitarEdicionInciso(){
 	var imprimirInciso = jQuery("#chkinciso").attr('checked');
 	var rango = jQuery("#txtIncios").val();
 	if(imprimirInciso || rango){
 		jQuery("#chkedicioninciso").removeAttr("disabled");
 	}else{
 		jQuery("#chkedicioninciso").removeAttr("checked");
 		jQuery("#chkedicioninciso").attr("disabled","true");
 	}
 }

 jQuery(document).ready(function(){
	mostrarIncluirEdicionPoliza();
	mostrarIncluirEdicionInciso();
	if( jQuery('#txtIncios').val() != "" ){
		jQuery('#chkinciso').parent().parent().parent().parent().hide();	
		jQuery('#txtIncios').attr('readonly',true);
	}

 });
</script> 

<div id="detalle" >
<s:include value="/jsp/componente/impresiones/componenteImpresionHeader.jsp"></s:include>
 <div id="indicador" style="display:none;"></div>
	<center>
		<table id="agregar" width="100%" border="0">
		 <s:form action="generarImpresion" namespace="/impresiones/componente" id="contenedorImpresionForm">
		   <s:hidden name="nombreUsuario"/>
		   <s:hidden name="idSesionUsuario"/>
		   <s:hidden name="tipoImpresion"/>
		   <s:hidden name="totalIncisos"/>
		   <s:hidden name="id"/>	
		   <s:hidden name="idToPoliza"/>
		   <s:hidden name="validOnMillis" />
		   <s:hidden name="recordFromMillis" />
		   <s:hidden name="claveTipoEndoso" />
		   <s:hidden name="esSituacionActual" />
		   <s:hidden name="existeEdicionPoliza" id="existeEdicionPoliza"/>
		   <s:hidden name="existeEdicionInciso" id="existeEdicionInciso"/>
		   <s:hidden name="tienePermisosImprimirEdicion" id="tienePermisosImprimirEdicion" />
		   <s:hidden name="tienePermisosModificarEdicion" id ="tienePermisosModificarEdicion" /> 
		   <s:hidden name="tienePermisosNoBrocker" id ="tienePermisosNoBrocker" /> 
		   <s:hidden name="tienePermisosAviso" id ="tienePermisosAviso" /> 
		   <s:hidden name="tienePermisosDerechos" id ="tienePermisosDerechos" /> 
		   <tr>
		     <th>
		       <s:text name="Total de Incisos" /> 
		     </th>
		     <th colspan="3">
		       <s:text name="totalIncisos" /> 
		     </th>		     
		   </tr>
		    
		  <tr>	
			<th width="30%">
				  <s:text name="midas.componente.impresiones.caratula" /> 
			</th>
			<th width="5%">
			  <s:checkbox name="chkcaratula"  id="chkcaratula" onclick=""/> 
			</th>
			<th width="10%">
				<s:if test="tienePermisosModificarEdicion || tienePermisosImprimirEdicion">
					<div id="contenedorChkEdicionPoliza" style="display:none;">
				  		<div style="width:50%;display:inline;float:left;" ><s:checkbox name="incluirEdicionCaratulaPoliza"  id="chkedicioncaratula" title="Incluir Caratula Editada"
				  			onclick=""/></div>
				  		<div style="width:50%;display:inline;float:left;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoincluiredpoliza"/>" 
							title="<s:text name="midas.impresionpoliza.edicion.infoincluiredpoliza"/>"  
							src="<s:url value='/img/information.gif'/>" /></div>
					</div>
			  	</s:if>
			</th>
			<th>
				<s:if test="tienePermisosModificarEdicion">
			    <div class="btn_back w100"  style="display:inline; float: left;width: 80px;">
			      <a href="javascript:void(0);"
				  onclick="mostrarEditarImpresionPoliza(2, <s:text name='idToPoliza'/>, '<s:text name="validOn"/>', <s:text name="validOnMillis"/>, <s:text name="recordFromMillis"/>, '<s:text name="recordFrom" />', <s:text name="claveTipoEndoso"/>, <s:text name="id"/>); return false;"> 
				  <s:text
				  name="Editar" /> </a>
			    </div>
			    </s:if>
			  </th>
		 </tr>
     <s:if test="%{tipoImpresion != 3}">
       <s:if test="%{tipoImpresion != 1}">
		 <tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.recibo" /> 
			</th>
			<th width="5%" colspan="3">
				<s:checkbox name="chkrecibo" id="chkrecibo" alt="Seleccionar Todos" title="Seleccionar Todos" onclick=""/> 
			</th>	
		 </tr>
        </s:if>
		<tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.inciso" /> 
			</th>
			<th width="5%">
			  <s:checkbox name="chkinciso" id="chkinciso" alt="Seleccionar Todos" title="Seleccionar Todos" onclick="setIncios();" onchange="habilitarEdicionInciso();"/> 
			</th>
			<th width="10%">
				<s:if test="tienePermisosModificarEdicion || tienePermisosImprimirEdicion">
				<div id="contenedorChkEdicionInciso" style="display:none;">
			  		<div style="width:50%;display:inline;float:left;" ><s:checkbox name="imprimirEdicionInciso"  id="chkedicioninciso" title="Imprimir Incisos Editados"
			  			disabled="true" onclick=""/></div>
			  		<div style="width:50%;display:inline;float:left;" ><img alt="<s:text name="midas.impresionpoliza.edicion.infoimprimiredinciso"/>" 
						title="<s:text name="midas.impresionpoliza.edicion.infoimprimiredinciso"/>"  
						src="<s:url value='/img/information.gif'/>" /></div>
				</div>
			  	</s:if> 
			</th>
			<th>
				<s:if test="tienePermisosModificarEdicion">
			    <div class="btn_back w100"  style="display:inline; float:left; width:80px;">
			      <a href="javascript: void(0);"
				  onclick="mostrarEditarImpresionInciso(2, <s:text name='idToPoliza'/>, '<s:text name="validOn"/>', <s:text name="validOnMillis"/>, <s:text name="recordFromMillis"/>, '<s:text name="recordFrom" />', <s:text name="claveTipoEndoso"/>, <s:text name="id"/>); return false;"> 
				  <s:text
				  name="Editar" /> </a>
			    </div>
			    </s:if>
			</th>
		</tr>
		<tr>
			<th width="30%">
			</th>
			<th width="5%" colspan="3">
			  <s:hidden name="txtIncioshidden" id="txtIncioshidden"  />
			</th>	
		</tr>
		 <tr>
			<th width="30%">
			  <s:text name="midas.componente.impresiones.incisoTexto" /> 
			</th>
			<th width="5%" colspan="3">
			  <s:textfield name="txtIncios" id="txtIncios" onblur="setInciosTxt(this.value);habilitarEdicionInciso();" onkeyup="return soloNumerosYGuion(this, false)" />
			</th>	
		</tr>
	  <s:if test="%{tipoImpresion ==2}">	
	  
		<tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.inciso.mensaje" /> 
			</th>
				
			<th width="5%" colspan="3">
			  <s:checkbox name="chkcertificado" id="chkinciso" alt="Seleccionar Todos" title="Seleccionar Todos" onclick=""/> 
			</th>	
		</tr>
		
		<tr>
			<th width="30%">
				  <s:text name="midas.componente.impresiones.anexo" /> 
			</th>
				
			<th width="5%" colspan="3">
			  <s:checkbox name="chkanexos" id="chkanexos" alt="Seleccionar Todos" title="Seleccionar Todos" 
			  onclick=""/> 
			</th>	
		</tr>
		
		<tr>
		  <th width="30%">
		    <s:text name="midas.componente.impresiones.condicionesEspeciales" >Referencias Bancarias</s:text>
		  </th>
		  <th width="5%" colspan="3">
		    <s:checkbox name="chkreferencias" id="chkreferencias" alt="Seleccionar Todos" 
		    	title="Imprimir condiciones especiales ligadas." onclick=""/>
		  </th>
		</tr>		
		<tr>
		  <th width="30%">
		    <s:text name="midas.componente.impresiones.condicionesEspeciales" >Condiciones Especiales</s:text>
		  </th>
		  <th width="5%" colspan="3">
		    <s:checkbox name="chkcondiciones" id="chkcondiciones" alt="Seleccionar Todos" 
		    	title="Imprimir condiciones especiales ligadas." onclick=""/>
		  </th>
		</tr>
		
		<tr>
		  <th width="30%">
		    <s:text name="midas.componente.impresiones.fechaDeValidez"></s:text>
		  </th>
		  <th width="5%" colspan="3">
		    <s:textfield name="validOn" id="validOn" value="%{validOn}" readonly="true"></s:textfield>
		  </th>
		</tr>
	  </s:if>									
  </s:if>
  <s:if test="%{tipoImpresion == 2}">
  <s:if test="tienePermisosAviso">
      <tr>
	      <th width="30%">
			  <s:text name="Aviso de privacidad" /> 
		  </th>
		  <th width="5%" colspan="3">
			  <s:checkbox name="chkAviso" id="chkAviso" alt="Seleccionar Todos" title="Aviso de privacidad." onclick=""/> 
		  </th>	
	  </tr>
  </s:if>	  
  <s:if test="tienePermisosDerechos">
	  <tr>
	      <th width="30%">
			  <s:text name="Derechos Basicos del Asegurado" /> 
		  </th>
		  <th width="5%" colspan="3">
			  <s:checkbox name="chkDerechos" id="chkDerechos" alt="Seleccionar Todos" title="Derechos Basicos del Asegurado." onclick=""/> 
		  </th>	
	  </tr>
  </s:if>
	</s:if>
	<s:if test="%{tipoImpresion != 1}">
	<s:if test="tienePermisosNoBrocker">
		 <tr>
			<th width="30%">
			  <s:text name="Orden de Trabajo Brocker" /> 
			</th>
			<th width="5%" colspan="3">
			  <s:textfield name="txtNoOrder" id="txtNoOrder" />
			</th>	
		</tr>
	</s:if>
	</s:if>
     <table>
		     <tr>
		      <th>
			    <div class="btn_back w140"  style="display:inline; float: left;width: 80px;">
			      <a href="javascript: void(0);"
				  onclick="submitForm(); return false;"> 
				  <s:text
				  name="midas.componente.impresiones.botonImprimir" /> </a>
			    </div>
			  </th>		
		      <th>
			     <div class="btn_back w140"  style="display:inline; float: left;width: 80px; ">
			      <a href="javascript: void(0);"
				  onclick="if(confirm('\u00BFEst\u00E1 seguro que desea salir?')){cerrarVentanaComponente(<s:property value="id"/>)}"> 
				  <s:text
				  name="midas.boton.cerrar" /> </a>
			    </div>	
		       </th>
		      </tr>
	         </table>
			</s:form>
		</table>
	</center>
  <s:include value="/jsp/componente/impresiones/componenteImpresionFooter.jsp"></s:include>	
</div>