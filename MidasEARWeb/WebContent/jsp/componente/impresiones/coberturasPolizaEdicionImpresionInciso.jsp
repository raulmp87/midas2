<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="60"  align="center" sort="int" > <s:text name="midas.impresionpoliza.edicion.riesgo" />   </column>
          <column  type="ro"  width="160"  align="center" sort="str" > <s:text name="midas.impresionpoliza.edicion.coberturas" />  </column>
          <column  type="edtxt"  width="160"  align="center" sort="str" > <s:text name="midas.impresionpoliza.edicion.limmaxresp" />  </column>
          <column  type="edtxt"  width="160"  align="center" sort="str" > <s:text name="midas.impresionpoliza.edicion.deducible" />  </column>
          <column  type="ron"  width="90"  align="center" sort="int" format="$0,000.00" > <s:text name="midas.impresionpoliza.edicion.prima" />  </column>

	</head>

	<s:iterator value="datosCoberturasPoliza">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="riesgo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCobertura" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="sumaAsegurada" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="deducible" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="primaCobertura" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>