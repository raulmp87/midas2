<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form action="actualizarRegistro" id="formaDinamica" >
<table id="desplegar">

	<s:hidden name="clave" value="%{clave}"/>
	<s:hidden name="accion" value="%{accion}"/>
	<s:hidden name="claveTipoVista" id="claveTipoVista" />
	<s:hidden name="prefix" id="prefix" />	
	<!-- Rendereo de Controles -->
	<s:iterator value="registroDinamico.controles">
	<tr>
		<th>
		<s:set var="controlName" value="namePrefix + '.' + atributoMapeo" />
		<s:text name="labelPosition" var="label.position"/> 
		<s:if test="tipoControl != @mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN">
			<s:property value="etiqueta"  />
		</s:if>
		</th>
		<td width="250px">
		<s:if test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@TEXTBOX">
			<s:textfield name="%{#controlName}" value="%{valor}"  readonly="%{!editable}"
			  maxlength="%{longitudMaxima}" cssClass="cajaTexto" labelposition="%{labelPosition}" 
			  onchange="%{javaScriptOnChange}" onfocus="%{javaScriptOnFocus}"
			  onblur="%{javaScriptOnBlur}" onkeypress="%{javaScriptOnKeyPress}" />
		</s:if>
		<s:elseif test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@SELECT">
			<s:select name="%{#controlName}" value="%{valor}"  list="elementosCombo" 
				listKey="id" listValue="description" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				cssClass="cajaTexto" labelposition="%{labelPosition}" disabled="%{!editable}" onchange="%{javaScriptOnChange}"  />
				<s:if test="!editable">
					<s:hidden name="%{#controlName}" value="%{valor}"/>
				</s:if>
		</s:elseif>	
		<s:elseif test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@CHECKBOX">
			<s:checkbox name="%{#controlName}" value="%{valor}"  
				cssClass="cajaTexto" labelposition="%{labelPosition}" disabled="%{!editable}" onchange="%{javaScriptOnChange}" />
		</s:elseif>
		<s:elseif test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@RADIOBUTTON">
			<s:radio name="%{#controlName}" value="%{valor}"  list="elementosCombo" 
				listKey="id" listValue="description" cssClass="cajaTexto" labelposition="%{labelPosition}" disabled="%{!editable}" onchange="%{javaScriptOnChange}" />
		</s:elseif>
		<s:elseif test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN">
			<s:hidden name="%{#controlName}" value="%{valor}"/>
		</s:elseif>
		</td>
	</tr>	
	</s:iterator>
	</table>
</s:form>
	