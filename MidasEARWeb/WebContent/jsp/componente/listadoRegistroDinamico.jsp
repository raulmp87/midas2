<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<s:set var="hidden" value="@mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN" />
<s:set var="select" value="@mx.com.afirme.midas2.dto.ControlDinamicoDTO@SELECT" />
<s:set var="txt" value="@mx.com.afirme.midas2.dto.ControlDinamicoDTO@TEXTBOX" />
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<afterInit>
          <call command="attachHeader">
       		<param><s:iterator value="registroDinamico.controles" ><s:if test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN">&amp;nbsp,</s:if><s:elseif test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@SELECT">#select_filter,</s:elseif><s:elseif test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@TEXTBOX">#text_filter,</s:elseif><s:else>&amp;nbsp,</s:else></s:iterator>&amp;nbsp,&amp;nbsp</param>
       	</call>
       </afterInit>
				
		<s:iterator value="registroDinamico.controles" >
			<s:if test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN">
				<column id="${atributoMapeo}" type="ro" width="0" sort="str" hidden="true">
			</s:if>
			<s:else>
				<column id="${atributoMapeo}" type="ro" width="<s:if test="registroDinamico.numControlesVista <= 3 " >*</s:if><s:else>200</s:else>" sort="str" hidden="false">
			</s:else>
				<s:property value="etiqueta" escapeHtml="false"/>
			</column>
		</s:iterator>
		
		<column id="accionEditar" type="img" width="35px" sort="na"/>
		<column id="accionBorrar" type="img" width="35px" sort="na"/>
	</head>
	
	<s:iterator value="listaRegistros" >
		<row id="<s:property value="id" escapeHtml="false" escapeXml="false"/>">
			
			<s:iterator value="controles" >
				<cell><s:property value="valorInterpretado" escapeHtml="false" escapeXml="false"/></cell>
			</s:iterator>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript:TipoAccionDTO.getEditar(verDetalleRegistroDinamico(4))^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Borrar^javascript:TipoAccionDTO.getEliminar(verDetalleRegistroDinamico(3))^_self</cell>

		</row>
	</s:iterator>
</rows>
