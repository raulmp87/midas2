<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componente.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script> <!-- Se agrega para el caso del catalogo de Variables Modificadoras -->
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>

<sj:head/>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
</script>
 
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript">
	var guardarRegistroDinamicoPath = '<s:url action="actualizarRegistro" namespace="/controlDinamico"/>';
	var PREFIX;
</script>



<div id ="ventanaDetalleDinamico">
	<s:include value="/jsp/componente/detalle.jsp"></s:include>
	<table>
		<tr><td colspan="2">
		<div class="alinearBotonALaDerecha">
			<s:if test="mostrarSubmit">
				<div id="b_agregar">
					<a href="javascript: void(0);" onclick="javascript:guardarRegistroDinamico(<s:property value="accion"/>);return false;">
						<s:text name="%{submitKey}"/>
					</a>
				</div>	
			</s:if>
			<br/>
			<div>
			</div>
		</div>
		</td></tr>
	</table>
</div>

<div id="loading" style="display: none;">
			<img id="img_indicator" name="img_indicator"
				src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>