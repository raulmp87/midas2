<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/mask.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/validaciones.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/componente.js'/>"></script>
<script src="<s:url value='/js/midas2/busquedaGenerica.js'/>"></script>


<sj:head/>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
</script>
 
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript">
	var filtrarRegistrosDinPath = '<s:url action="filtrarRegistros" namespace="/controlDinamico"/>';
	PREFIX = '<s:property value="accion" />';
</script>

<div id ="ventanaDetalleDinamico">
	<s:include value="/jsp/componente/detalle.jsp"></s:include>
	<table>
		<tr><td colspan="2">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);" onclick="filtrarRegistroDinamico();return false;">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			<br/>
		</div>
		</td></tr>
	</table>
</div>

<div id ="registrosDinGrid" style="width:97%;height:190px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>

<script language="javascript">filtrarRegistroDinamico()</script>