<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
	var ventana=null;
	function mostrarModalDomicilio(idField){
		var idField=(idField)?idField:"idDomicilio";
		var url="/MidasWeb/catalogos/domicilio/init.action?tipoAccion=consulta&idField="+idField;
		sendRequestWindow(null, url,obtenerVentanaDomicilio);
	}
	
	function obtenerVentanaDomicilio(){
		var wins = obtenerContenedorVentanas();
		ventana= wins.createWindow("domicilioModal", 400, 320, 930, 450);
		ventana.centerOnScreen();
		ventana.setModal(true);
		ventana.setText("Consulta de Domicilios");
		ventana.button("park").hide();
		ventana.button("minmax1").hide();
		return ventana;
	}
	
	function findByIdDomicilio(idDomicilio,fn){
		var url="/MidasWeb/catalogos/domicilio/loadById.action";
		var data={"domicilio.idDomicilio.idDomicilio":idDomicilio};
		jQuery.asyncPostJSON(url,data,window[fn]);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
		
	}
</script>

<s:if test="requerido == 1">
	<s:set id="campoRequerido" value="%{getText('midas.atributo.requerido')}" />
</s:if>
<s:if test="readOnly == \"true\"">
	<s:set id="readOnly" value="true" />
</s:if>
<table class="contenedorDomicilio" width="90%">
 <s:if test="componente == 2"> 	
	<tr>
	  	<td>
	  		<s:textfield id="%{idDomicilioName}" name="%{idDomicilioName}" value="%{idDomicilio}" readonly="true" cssStyle="display:none;" cssClass="cajaTextoM2 w100 jsSearchResultField" onchange="javascript:findByIdDomicilio(this.value,'%{funcionResult}');"/>
	  	</td>
	  	<td colspan="2">
	  		<s:if test="enableSearchButton == \"true\"">
	  		<div class="btn_back w140" id="btnDomicilio">
				<a href="javascript: void(0);" class="icon_buscar" 
					onclick="javascript:mostrarModalDomicilio('${idDomicilioName}');">
					<s:text name="Buscar Domicilio"/>
				</a>
			</div>	
			</s:if>
	  	</td>
	  	</tr>
  </s:if>
  <s:if  test="componente != 1 && componente != 6">  
  <tr>    
  	<td class="jQIsRequired">
  		<label for="${idPaisName}">
  			<s:text name="labelPais"/>
  		</label>
  	</td>
    <td colspan="2"><s:select id="%{idPaisName}" disabled="%{readOnly}" name="%{idPaisName}" value="%{idPais}" 
				list="paises" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangePais('%{idEstadoName}','%{idCiudadName}','%{idColoniaName}','%{cpName}','%{calleNumeroName}','%{idPaisName}')" 
				cssClass="cajaTextoM2 w250 %{#campoRequerido}"/>
	</td>
	
	 <s:if  test="componente == 100">  
	 
	 <td class="jQIsRequired">
  		<label for="${idPaisNacimientoName}">
  			<s:text name="labelPaisNacimiento"/>
  		</label>
  	</td>
    <td colspan="2"><s:select id="%{idPaisNacimientoName}" disabled="%{readOnly}" name="%{idPaisNacimientoName}" value="%{idPaisNacimiento}" 
				list="paises" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
			
				 cssClass="cajaTextoM2 w250 %{#campoRequerido}"/>
				 
				 
	</td>
	 </s:if>
  </tr>	
  </s:if>
  <tr> 
 	<td class="jQIsRequired">
 		<label for="${idEstadoName}">
 			<s:text name="labelEstado"/>
 		</label>
 	</td> 	
    <td>
    	<s:select id="%{idEstadoName}" disabled="%{readOnly}" name="%{idEstadoName}" value="%{idEstado}" 
				list="estados"  headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEstadoGeneral('%{idCiudadName}','%{idColoniaName}','%{cpName}','%{calleNumeroName}','%{idEstadoName}')" cssClass="cajaTextoM2 w250 %{#campoRequerido} jQ_estado"/>
    </td>
    <td class="jQIsRequired">
    	<label for="${idCiudadName}">
 			<s:text name="labelCiudad"/>
 		</label>
 	</td> 
  	<td>
  		<s:select id="%{idCiudadName}" disabled="%{readOnly}" name="%{idCiudadName}" value="%{idCiudad}" 
				list="ciudades"  headerKey="%{getText('midas.general.defaultHeaderKey')}"  headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeCiudad('%{idColoniaName}','%{cpName}','%{calleNumeroName}','%{idCiudadName}')" cssClass="cajaTextoM2 w250 %{#campoRequerido} jQ_ciudad"/>
  	</td>
  </tr>	
 <s:if test="componente == 2 || componente == 1 || componente == 3 || componente == 4 || componente == 6|| componente == 101">
  	<tr> 
  	 <td class="jQIsRequired">
  	 	<label for="${idColoniaName}">
 			<s:text name="labelColonia"/>
 		</label>
 	</td>
  	<td>
  		<s:select id="%{idColoniaName}" disabled="%{readOnly}"  name="%{idColoniaName}" value="%{idColonia}" 
				list="colonias" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeColonia('%{cpName}','%{calleNumeroName}', '%{idColoniaName}','%{idCiudadName}')" cssClass="cajaTextoM2 w250 %{#campoRequerido}"/>
  	</td>
  	<td class="jQIsRequired">
  		<label for="${cpName}">
  			<s:text name="labelCodigoPostal"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{cpName}" name="%{cpName}" value="%{cp}" cssClass="cajaTextoM2 w180  jQnumeric jQrestrict %{#campoRequerido}" disabled="%{readOnly}" maxlength="6" onchange="onChangeCodigoPostal(this.value, '%{idColoniaName}','%{idCiudadName}','%{idEstadoName}','%{calleNumeroName}','%{idPaisName}','%{cpName}');"/>
  	</td>
  	</tr>
  </s:if>
  <s:if test="componente == 2 || componente == 1 || componente == 3|| componente == 101" >  
  <tr>   	
  	<s:if test="componente != 101 ">
  	<td class="jQIsRequired">
  		<label for="${calleNumeroName}">
  			<s:text name="labelCalleNumero"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{calleNumeroName}" name="%{calleNumeroName}" value="%{calleNumero}" cssClass="cajaTextoM2 w300 jQalphaextra %{#campoRequerido}" disabled="%{readOnly}" maxlength="100"/>
  	</td></s:if>
  	
  	<s:if test="componente == 101 ">
  	<td class="jQIsRequired">
  		<label for="${numeroName}">
  			<s:text name="labelNumero"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{numeroName}" name="%{numeroName}" value="%{numero}" cssClass="cajaTextoM2 w100 jQalphaextra jQrequired" disabled="%{readOnly}" maxlength="10"/>
  	</td>
  	<td class="jQIsRequired">
  		<label for="${calleNumeroName}">
  			<s:text name="labelCalleNumero"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{calleNumeroName}" name="%{calleNumeroName}" value="%{calleNumero}" cssClass="cajaTextoM2 w200 jQalphaextra %{#campoRequerido}" disabled="%{readOnly}" maxlength="100"/>
  	</td>
  	
  	</s:if>
	<td>
		<label for="${nuevaColoniaName}">Nueva Colonia</label>
	</td>
	<td>
		<table width="100%">
		<tbody>
			<tr>
				<td><s:checkbox onchange="onChangeNuevaColonia('%{nuevaColoniaName}','%{idColoniaName}','%{idColoniaCheckName}');" id="%{idColoniaCheckName}" name="%{idColoniaCheckName}" cssStyle="display:inline-block;" cssClass="cajaTextoM2 w20" disabled="%{readOnly}"></s:checkbox></td>
				<td><s:textfield id="%{nuevaColoniaName}" name="%{nuevaColoniaName}" value="%{nuevaColonia}" cssClass="cajaTextoM2 w230 jQalphaextra" disabled="%{readOnly}" maxlength="60" cssStyle="display:inline-block;"/></td>
			</tr>
		</tbody>
		</table>
	</td>
  </tr>
  </s:if>
  <s:if test="componente == 4" >  
  <tr>  	
  	<td>
  		<s:hidden id="%{calleNumeroName}" name="%{calleNumeroName}" value="%{calleNumero}" cssClass="cajaTextoM2 w300 jQalphaextra" disabled="%{readOnly}" maxlength="100"/>
  	</td>
  	<s:if test="%{idColoniaName} !=null">
	  	<td>
			<label for="${nuevaColoniaName}">Nueva Colonia</label>
		</td>
		<td>
	 		<table width="100%">
			<tbody>
				<tr>
					<td><s:checkbox onchange="onChangeNuevaColonia('%{nuevaColoniaName}','%{idColoniaName}','%{idColoniaCheckName}');" id="%{idColoniaCheckName}" name="%{idColoniaCheckName}" cssStyle="display:inline-block;" cssClass="cajaTextoM2 w20" disabled="%{readOnly}"></s:checkbox></td>
					<td><s:textfield id="%{nuevaColoniaName}" name="%{nuevaColoniaName}" value="%{nuevaColonia}" cssClass="cajaTextoM2 w230 jQalphaextra" disabled="%{readOnly}" maxlength="60" cssStyle="display:inline-block;"/></td>
				</tr>
			</tbody>
			</table>
		</td>
	</s:if>
  </tr>
  </s:if>
  </table>