<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<s:set var="hidden" value="@mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN" />
<s:set var="select" value="@mx.com.afirme.midas2.dto.ControlDinamicoDTO@SELECT" />
<s:set var="txt" value="@mx.com.afirme.midas2.dto.ControlDinamicoDTO@TEXTBOX" />
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableLightMouseNavigation"><param>true</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>14</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
		</beforeInit>		
		<s:iterator value="registroDinamico.controles" >
			<s:if test="tipoControl == @mx.com.afirme.midas2.dto.ControlDinamicoDTO@HIDDEN">
				<column id="${atributoMapeo}" type="ro" width="0" sort="str" hidden="true">
			</s:if>
			<s:else>
				<column id="${atributoMapeo}" type="ro" width="*" sort="str" hidden="false">
			</s:else>
				<s:property value="etiqueta" escapeHtml="false" escapeXml="true" />
			</column>
		</s:iterator>
		<column id="accionModificar" type="ro" width="*" sort="na" align="center">Acciones</column>
	</head>
	<s:iterator value="listaRegistros" >
		<row id="<s:property value="id" escapeHtml="false" escapeXml="true"/>">
			<s:iterator value="controles" >
				<cell><s:property value="valorInterpretado" escapeHtml="false" escapeXml="true" /></cell>
			</s:iterator>
			<cell>
				&lt;a onclick="javascript:regresarRegistro(<s:property value="id" escapeHtml="false" escapeXml="true"/>)" href="javascript: void(0)"&gt;
					&lt;img border="0" src="<s:url value='/img/dhtmlxgrid/true.gif'/>" &gt;
				&lt;/a&gt;
			</cell>
		</row>
	</s:iterator>
</rows>
