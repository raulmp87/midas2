<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<s:include value="/jsp/suscripcion/cotizacion/cotizacionHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/vehiculo/componenteVehiculo.js'/>"></script>

<sj:head/>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">
	jQuery.noConflict();
		
</script>

<div id="otrasCaracteristicasDiv" >
	<center>	
		<s:form action="definirOtrasCaract" namespace="/componente/vehiculo" name="guardarOtrasCaractForm" id="guardarOtrasCaractForm" >
			<s:hidden name="modificadoresDescripcionName" value="%{modificadoresDescripcionName}"/>
			<s:hidden name="descripcionFinalName" value="%{descripcionFinalName}"/>
			<s:hidden name="cotizacionExpress" value="%{cotizacionExpress}"/>
			<s:hidden name="descripcionBase"/>
			<table id="agregar" border="0">			
			<s:iterator value="caracteristicasVehiculoList" status="stat">
				<tr>								
					<td>	
						  <s:select key="descripcion" 
						  name="caracteristicasVehiculoList[%{#stat.index}].valorSeleccionado" 
						  id="caracteristicasVehiculoList[%{#stat.index}].valorSeleccionado" 
						  value="valorSeleccionado" 
						  list="listadoVariablesModificadoras"									  	 
						  headerKey="" headerValue="%{getText('midas.general.seleccione')}"  
						  required="false" cssClass="cajaTextoM2 w200" labelposition="top"/>
					</td>
				</tr>	
			</s:iterator>					
			</table>
			<div class="inline align-right">
				<div class="btn_back w100 guardar">
					<a href="javascript: void(0);" id="btnGuardar" class="icon_guardar"	onclick="true ? definirOtrasCaract() : false">
						<s:text name="midas.boton.aceptar"/>	
					</a>
				</div>	
				<div class="btn_back w100">
					<a href="javascript: void(0);" onclick="closeCaracteristicas();">
						<s:text name="midas.cotizacion.cancelar"/>
					</a>
				</div>	
			</div>
		</s:form>
	</center>
</div>

<div id="loading" style="display: none;">
			<img id="img_indicator" name="img_indicator"
				src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>