<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<sj:head/>
<script type="text/javascript">
	jQuery.noConflict();

	function guardarVariablesModificadoras(){
		var modificadoresDescripcionName = '<s:property value="modificadoresDescripcionName"/>';
		var descripcionFinalName = '<s:property value="descripcionFinalName"/>';		
		var parentFrame = parent.getWindowContainerFrame('inciso');
		var childFrame = parent.getWindowContainerFrame('ajustarCaracteristicas');
		var modificadoras = childFrame.contentWindow.dwr.util.getValue(modificadoresDescripcionName);
		var descFinal = childFrame.contentWindow.dwr.util.getValue(descripcionFinalName);
		if(parentFrame != null){
			parentFrame.contentWindow.dwr.util.setValue(modificadoresDescripcionName, modificadoras);
			parentFrame.contentWindow.dwr.util.setValue(descripcionFinalName, descFinal);
			parentFrame.contentWindow.document.getElementById('btnGuardar').style.display = 'none';
		}else{
			parent.dwr.util.setValue(modificadoresDescripcionName, modificadoras);
			parent.dwr.util.setValue(descripcionFinalName, descFinal);
			parent.document.getElementById('btnGuardar').style.display = 'none';
		}
		//alert('En caso de ya tener prima, es necesario recalcular el inciso para reflejar lo cambios.');		
		parent.cerrarVentanaModal('ajustarCaracteristicas');
	}
</script>
<div id="otrasCaracteristicasDiv" >
	<center>			
		<table id="agregar" border="0">					
			<tr>								
				<s:include value="/jsp/componente/vehiculo/caracteristicasVehiculoArea.jsp"></s:include>
			</tr>			
		</table>
		<div class="inline align-right">
			<div class="btn_back w100 guardar">
				<a href="javascript: void(0);" id="btnGuardar" class="icon_guardar"	onclick="guardarVariablesModificadoras();">
					<s:text name="midas.boton.aceptar"/>	
				</a>
			</div>	
			<div class="btn_back w100">
				<a href="javascript: void(0);" onclick="parent.cerrarVentanaModal('ajustarCaracteristicas');">
					<s:text name="midas.cotizacion.cancelar"/>
				</a>
			</div>	
		</div>		
	</center>
</div>
