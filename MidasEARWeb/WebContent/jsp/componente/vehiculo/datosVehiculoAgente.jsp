<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<style type="text/css">
#buscador{
		display: none;
}
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }
.ui-autocomplete-category {
		font-weight: bold;
		padding: .2em .4em;
		margin: .8em 0 .2em;
		line-height: 1.5;
	}
.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
#porcentajeDescuentoEstado{
		display: none;
}
</style>

<script type="text/javascript">
	// Variables Globales
	urlBusquedaVehiculos = '<s:url action="buscarVehiculoAgente" namespace="/vehiculo/inciso" />';
	elementoNegocioSeccion = '<s:property value="idNegocioSeccionName"/>';
	elementoMarcaId = '<s:property value="idMarcaVehiculoName" />';
	elementoModeloId = '<s:property value="idModeloVehiculoName"/>';
	elementoEstiloId = '<s:property value="idEstiloVehiculoName"/>';
	elementoMonedaId = '<s:property value="idMoneda"/>';
	elementoNegocioPaquete = '<s:property value="idNegocioPaqueteName"/>';
	targetNegocioSeccion = '<s:property value="idMarcaVehiculoName"/>';
	targetMarcaVehiculo = '<s:property value="idModeloVehiculoName" />';
	targetModelo = '<s:property value="idEstiloVehiculoName"/>';
	targetEstiloVehiculo = '<s:property value="idTipoUsoVehiculoName"/>';	
	targetTipoUso = '<s:property value="idNegocioPaqueteName"/>';
	targetDescripcion = '<s:property value="descripcionFinalName"/>';
	targetModificadoresDescripcion = '<s:property value="modificadoresDescripcionName"/>';
	
	jQuery(document).ready(
			function() {
				if(document.getElementById(elementoNegocioSeccion).value != ''){
					jQuery('#buscador').show();
				}		
				if(document.getElementById(elementoNegocioPaquete).value != ''){
					mostrarPorcentajeDescuentoEstado();
				}
			});
	
	jQuery(function(){				
		jQuery('#descripcionBusqueda' ).autocomplete({
	               source: function(request, response){
	               		jQuery.ajax({
				            type: "POST",
				            url: urlBusquedaVehiculos,
				            data: {descripcionInciso:request.term,lineaNegocioInciso:dwr.util.getValue(elementoNegocioSeccion),idMarca:dwr.util.getValue(elementoMarcaId),modeloVehiculo:dwr.util.getValue(elementoModeloId),idMoneda:elementoMonedaId},              
				            dataType: "xml",	                
				            success: function( xmlResponse ) {
				           		response( jQuery( "item", xmlResponse ).map( function() {
									return {
										value: jQuery("descripcion", this ).text(),
					                    estiloId: jQuery( "estiloId", this ).text(),
					                    marcaId:jQuery("marcaId",this).text(),
					                    category:jQuery("category",this).text()
									}
								}));			           
	               		}
	               	})},
	               minLength: 3,
	               delay: 1000,
	               select: function( event, ui ) {
	            	   try{	         
	            		   dwr.util.setValue(elementoEstiloId,ui.item.estiloId);	            		   					           
				           onChangeEstiloVehiculo(targetEstiloVehiculo, elementoEstiloId, elementoNegocioSeccion);
				           
	            	   }catch(e){
	            	   }
		               
	               }		          
	         });
	});	
	
	jQuery.fn.extend({
 		propAttr: jQuery.fn.prop || jQuery.fn.attr
	});
</script>

<body>
<s:include value="/jsp/componente/vehiculo/datosVehiculoInputsAgente.jsp"></s:include>
</body>