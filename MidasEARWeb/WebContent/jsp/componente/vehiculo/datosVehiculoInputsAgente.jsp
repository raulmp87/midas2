<!-- nueva modificacion -->
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="<s:url value='/js/midas2/operacioensSapAmis/sapAmisAccionesAlertas.js'/>"></script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>

<s:if test="soloConsulta == 1">
	<s:set var="disabledConsulta">true</s:set>	
	<s:set var="disabledLNConsulta">true</s:set>	
	<s:set var="showOnConsulta">focus</s:set>
</s:if>
<s:elseif test="soloConsulta == 2">
	<s:set var="disabledConsulta">false</s:set>
	<s:set var="disabledLNConsulta">true</s:set>		
	<s:set var="showOnConsulta">both</s:set>
</s:elseif>
<s:else>
	<s:set var="disabledConsulta">false</s:set>
	<s:set var="disabledLNConsulta">false</s:set>		
	<s:set var="showOnConsulta">both</s:set>
</s:else>

<s:if test="%{#disabledLNConsulta}">
	<s:set var="disabledCoincideLN">true</s:set>
	<s:set var="muestraBoton">false</s:set>
</s:if>
<s:elseif test="%{coincideEstilo}">
	<s:set var="disabledCoincideLN">true</s:set>
	<s:set var="muestraBoton">false</s:set>
</s:elseif>
<s:else>
	<s:set var="disabledCoincideLN">false</s:set>
	<s:set var="muestraBoton">true</s:set>
</s:else>

<s:if test="%{#disabledConsulta}">
	<s:set var="disabledCoincide">true</s:set>
</s:if>
<s:elseif test="%{coincideEstilo}">
<s:set var="disabledCoincide">true</s:set>
</s:elseif>
<s:else>
<s:set var="disabledCoincide">false</s:set>
</s:else>

<table  class="contenedorMarco">
	<tr>
		<td class="subtitulo align-left" colspan="2" >
			<s:text name="Zona de circulaci&oacute;n sdas"/>
		</td>
	</tr>
	<tr>
		<s:if test="%{mostrarEstado}">
			<td>	
			    <s:if test="%{#disabledCoincide}">
				    <s:hidden id="%{idEstadoName}" name="%{idEstadoName}" value="%{idEstado}"/>
			    </s:if>
				<s:select id="%{idEstadoName}" name="%{idEstadoName}" value="%{idEstado}" 
					list="estadoMap" key="midas.fuerzaventa.negocio.estado" 
					disabled="%{#disabledCoincide}"
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
					onchange="onChangeEstado('%{idMunicipioName}', '%{idEstadoName}');mostrarPorcentajeDescuentoEstadoDefault();cargaDerechosNegocio();"
					required="%{requerido}" labelposition="%{posicionEtiqueta}" cssClass="cajaTextoM2 w200 jQrequired"/>
			</td>
		</s:if>
		<s:if test="%{mostrarEstado}">
			<td>
			     <s:if test="%{#disabledCoincide}">
				    <s:hidden id="%{idMunicipioName}" name="%{idMunicipioName}" value="%{idMunicipio}"/>
			     </s:if>
				 <s:select id="%{idMunicipioName}" name="%{idMunicipioName}" value="%{idMunicipio}" 
					list="municipioMap" key="midas.fuerzaventa.negocio.municipio" 
					disabled="%{#disabledCoincide}"
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
					onchange="cargaDerechosNegocio();"
					required="%{requerido}" labelposition="%{posicionEtiqueta}" cssClass="cajaTextoM2 w200 jQrequired"/>
			</td>
		</s:if>
	    <!-- Codigo postal -->
		<td>
	 		<s:textfield key="midas.catalogos.centro.operacion.codigoPostal" id="idCodigo" 
			name="%{idCodigoPostalName}" value="%{idCodigoPostal}"
	 		labelposition="%{posicionEtiqueta}" cssClass="cajaTextoM2 w150" onchange="validaCP('%{idEstadoName}');ocultaGuardar();" onkeypress="return isNumberKey(event)" maxlength="5"/>
		</td>
		<!-- ---------------------------->
	</tr>
</table>
<div class="row">
	<div class="c7 s3">
		<div id="divMessageInfo"
			style="padding: 0pt 0.7em; width: 310px; margin-top: 0px; display: none; margin-left: 0px;font-size: 9px;"
			class="ui-state-highlight ui-corner-all">
			<p>
				<span style="float: left; margin-right: .3em;"
					class="ui-icon ui-icon-info"></span> <strong>Atenci�n:</strong> <span
					id="message">Este negocio tiene estilos limitados</span>
			</p>
		</div>
	</div>
</div>
<table class="contenedorMarco">
<s:hidden name="cotizacionExpress" value="%{cotizacionExpress}"/>
<s:hidden name="idMonedaName" value="%{idMoneda}"/>
<s:hidden name="idCotizacion" value="%{idCotizacion}"/>
<s:hidden name="onChangePaquete" value="%{onChangePaquete}"/>
<s:hidden name="modificadoresDescripcionName" value="%{modificadoresDescripcionName}"/>
<s:hidden name="descripcionFinalName" value="%{descripcionFinalName}"/>
<s:hidden name="idEstiloVehiculoNameRef" value="%{idEstiloVehiculoName}"/>
<s:hidden name="idNegocioSeccionNameRef" value="%{idNegocioSeccionName}"/>
<s:hidden name="idModeloVehiculoNameRef" value="%{idModeloVehiculoName}"/>
<s:hidden name="pctDescuentoEstadoName" value="%{pctDescuentoEstadoName}"/>
<s:hidden name="idNegocioPaqueteName" value="%{idNegocioPaqueteName}"/>
<s:hidden name="idEstadoName" value="%{idEstadoName}"/>
<s:hidden name="serieName" value="%{serieName}"/>
<s:hidden name="vinValidoName" value="%{vinValidoName}"/>
<s:hidden name="coincideEstiloName" value="%{coincideEstiloName}"/>
<s:hidden name="observacionesSesaName" value="%{observacionesSesaName}"/>
<s:hidden name="claveAmisName" value="%{claveAmisName}"/>
<s:hidden name="claveSesaName" value="%{claveSesaName}"/>
<s:hidden name="idCodigoPostalName" value="%{idCodigoPostalName}"/>

<tr>
	<td class="subtitulo align-left" colspan="3" >
		<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.infVehiculo"/>
	</td>
</tr>
<s:if test="%{mostrarLineaNegocio}">
	<tr>
		<s:if test="%{#disabledCoincideLN}">
			<s:hidden name="%{idNegocioSeccionName}" value="%{idNegocioSeccion}" id="%{idNegocioSeccionName}"/>
		</s:if>		
		<td colspan="1">
			<s:select id="%{idNegocioSeccionName}" name="%{idNegocioSeccionName}" value="%{idNegocioSeccion}" 
					list="negocioSeccionList" listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
					key="midas.suscripcion.cotizacion.datosConfVehiculo.lineaNegocio"
					disabled="%{#disabledCoincideLN}"
					headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
					onchange="onChangeLineaNegocio('%{idMarcaVehiculoName}', '%{idNegocioSeccionName}');limpiarPorLinea('%{serieName}');cargaDerechosNegocio();"
					required="%{requerido}" labelposition="%{posicionEtiqueta}" cssClass="cajaTextoM2 w200 jQrequired"
					/>
		</td>
		<s:if test="%{#disabledCoincideLN}">
			<s:hidden name="%{serieName}" value="%{serie}" id="%{serieName}"/>
		</s:if>
		<td>
	 		<s:textfield key="midas.cotizador.fronterizos.serie" id="%{serieName}" 
			name="%{serieName}" value="%{serie}" disabled="%{#disabledCoincideLN}"
	 		labelposition="%{posicionEtiqueta}" cssClass="cajaTextoM2 w200"/>
		</td>
		<td class="w200">
		<s:if test="%{#muestraBoton}">
			<div id="mostrarInfvehicularBtn" class="btn_back w50">
				<a href="javascript: void(0);"
					onclick="mostrarInfVehicularById('<s:property value="%{serieName}"/>',
					'<s:property value="%{idNegocioSeccionName}"/>',
					'idMonedaName','<s:property value="%{idEstadoName}"/>',
					'idCotizacion','<s:property value="%{idMarcaVehiculoName}"/>',
					'<s:property value="%{idEstiloVehiculoName}"/>','<s:property value="%{idModeloVehiculoName}"/>',
					'<s:property value="%{descripcionFinalName}"/>','<s:property value="%{idTipoUsoVehiculoName}"/>',
					false);">
					<s:text name="midas.cotizador.fronterizos.boton.validar"/>
				</a>
			</div>
		</s:if>				
		</td>
			<s:hidden name="%{vinValidoName}" id="vinValidoId"/>
			<s:hidden name="%{coincideEstiloName}" id="coincideEstiloId"/>		
			<s:hidden name="%{observacionesSesaName}" id="observacionesSesa"/>	
			<s:hidden name="%{claveAmisName}" id="claveAmis"/>
			<s:hidden name="%{claveSesaName}" id="claveSesa"/>	
		</tr>	
</s:if>
<!-- 
<s:if test="%{cotizacionExpress}">
	<s:hidden id="%{idNegocioSeccionName}" name="%{idNegocioSeccionName}"/>
</s:if>
-->
<tr>
<s:if test="%{mostrarMarcaVehiculo}">		
		<td width="400px">
		    <s:if test="%{#disabledCoincide}">
		        <s:hidden id="%{idMarcaVehiculoName}" name="%{idMarcaVehiculoName}" value="%{idMarcaVehiculo}"/>
	        </s:if>
			<s:select id="%{idMarcaVehiculoName}" name="%{idMarcaVehiculoName}" value="%{idMarcaVehiculo}" 
				list="marcaMap" 
				key="midas.suscripcion.cotizacion.datosConfVehiculo.marca" 
				disabled="%{#disabledCoincide}"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeMarcaVehiculo('%{idModeloVehiculoName}', '%{idMarcaVehiculoName}', '%{idNegocioSeccionName}')"
				required="%{requerido}" labelposition="%{posicionEtiqueta}"
				cssClass="cajaTextoM2 w200 jQrequired" cssStyle="margin-left:55px"/>
				<div id="divMessageInfo" style="margin-top: 20px; padding: 0 .7em;width:310px;margin-top: 0px;display: none;" class="ui-state-highlight ui-corner-all">
				<p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
				<strong>Atenci&oacute;n:</strong> <span id="message"></span></p>
			</div>
		</td>	
</s:if>
	<td colspan="2" rowspan="2" valign="top">
		<div id="buscador" style="display:none;">
			<div>
					<s:textfield
						cssClass="cajaTextoM2 w200" label="o Buscar por descripci�n"
					    disabled="%{#disabledCoincide}"
						labelposition="left" id="descripcionBusqueda" />
			</div>
		</div>
	</td>
</tr>
<s:if test="%{mostrarModeloVehiculo}">
	<tr>
		<td>  <s:if test="%{#disabledCoincide}">
				    <s:hidden id="%{idModeloVehiculoName}" name="%{idModeloVehiculoName}" value="%{idModeloVehiculo}"/>
				    <s:hidden id="%{descripcionFinalName}" name="%{descripcionFinalName}"/>
			  </s:if>			
				<s:if test="%{cotizacionExpress}">
					 <s:select id="%{idModeloVehiculoName}" name="%{idModeloVehiculoName}" value="%{idModeloVehiculo}" 
						list="modeloMap" 
						key="midas.suscripcion.cotizacion.datosConfVehiculo.modelo"
					    disabled="%{#disabledCoincide}" 
						headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
						onchange="setDescripcionVehiculo('%{idEstiloVehiculoName}','%{descripcionFinalName}');" 
						required="%{requerido}" labelposition="%{posicionEtiqueta}"
						cssStyle="margin-left:49px" cssClass="cajaTextoM2 w200 jQrequired"/>					
				</s:if>
				<s:else>
					 <s:select id="%{idModeloVehiculoName}" name="%{idModeloVehiculoName}" value="%{idModeloVehiculo}" 
						list="modeloMap" 
						key="midas.suscripcion.cotizacion.datosConfVehiculo.modelo"
					    disabled="%{#disabledCoincide}" 
						headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
						onchange="onChangeModeloVehiculo('%{idEstiloVehiculoName}', '%{idMarcaVehiculoName}', '%{idModeloVehiculoName}', '%{idNegocioSeccionName}'); " 
						required="%{requerido}" labelposition="%{posicionEtiqueta}"
						cssStyle="margin-left:49px" cssClass="cajaTextoM2 w200 jQrequired"/>					
				</s:else>	
		</td>		
	</tr>	
</s:if>
<s:if test="%{mostrarEstiloVehiculo}">
	<tr>
		<td>
		     <s:if test="%{#disabledCoincide}">
				<s:hidden id="%{idEstiloVehiculoName}" name="%{idEstiloVehiculoName}" value="%{idEstiloVehiculo}"/>
			  </s:if>
			 <s:select id="%{idEstiloVehiculoName}" name="%{idEstiloVehiculoName}" value="%{idEstiloVehiculo}" 
				list="estiloMap" 
				key="midas.suscripcion.cotizacion.datosConfVehiculo.estilo" 
				disabled="%{#disabledCoincide}"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEstiloVehiculo('%{idTipoUsoVehiculoName}', '%{idEstiloVehiculoName}', '%{idNegocioSeccionName}')"
				required="%{requerido}" labelposition="%{posicionEtiqueta}"
				cssClass="cajaTextoM2 w200 jQrequired tuttoElchorizo" cssStyle="margin-left:55px"/>
		</td>	
		<s:if test="mostrarAjustarCaracteristicas && soloConsulta == 0 && !cotizacionExpress">
			<td class="w500">
				<div id="ajustarCaracteristicas" class="btn_back w150">
					<a href="javascript: void(0);"
						onclick="ajustarCaracteristicasVehiculo('<s:property value="%{idEstiloVehiculoName}"/>');">
						<s:text name="midas.suscripcion.cotizacion.datosConfVehiculo.ajustarCaract"/>
					</a>
				</div>	
			</td>		
		</s:if>		
	</tr>
</s:if>
<s:if test="mostrarAjustarCaracteristicas && (soloConsulta == 0 || soloConsulta == 2)">
	<tr id="ajustarCaracteristicasDiv">
		<s:include value="/jsp/componente/vehiculo/caracteristicasVehiculoArea.jsp"></s:include>
	</tr>	
</s:if>
<s:if test="%{mostrarCheckEsFronterizo}">
	<tr>
		<td>
			 <s:checkbox id="%{checkEsFronterizoName}" name="%{checkEsFronterizoName}" fieldValue="false" key="midas.general.zonaFronteriza" labelposition="right"
				disabled="%{#disabledConsulta}" />
		</td>
		<s:if test="%{cotizacionExpress}">
			<td>
				<div class="btn_back w170" >
					<a href="javascript: void(0);" onclick="cotizarExpress()" class="icon_buscar">	
						<s:text name="midas.cotizacion.express.cotizar"/>	
					</a>
            	</div>			
			</td>
		</s:if>
	</tr>	
</s:if>
	<tr>
<s:if test="%{mostrarTipoUsoVehiculo}">
		<td>
		      <s:if test="%{#disabledConsulta}">
				<s:hidden id="%{idTipoUsoVehiculoName}" name="%{idTipoUsoVehiculoName}" value="%{idTipoUsoVehiculo}"/>
			  </s:if>
			  <s:select id="%{idTipoUsoVehiculoName}" name="%{idTipoUsoVehiculoName}" value="%{idTipoUsoVehiculo}" 
				list="tipoUsoMap" 
				key="midas.suscripcion.cotizacion.datosConfVehiculo.tipoUso"
				disabled="%{#disabledConsulta}"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeTipoUsoVehiculo('%{idNegocioPaqueteName}', '%{idNegocioSeccionName}');cargaDerechosNegocio();"
				required="%{requerido}" labelposition="%{posicionEtiqueta}"
				cssStyle="margin-left:26px" cssClass="cajaTextoM2 w200 jQrequired"/>
		</td>	
</s:if>
<s:if test="%{mostrarTipoServicio}">
		<td>
			  <s:if test="%{#disabledConsulta}">
				<s:hidden id="%{idTipoServicioVehiculoName}" name="%{idTipoServicioVehiculoName}" value="%{idTipoServicioVehiculo}"/>
			  </s:if>
			  <s:select id="%{idTipoServicioVehiculoName}" name="%{idTipoServicioVehiculoName}" value="%{idTipoServicioVehiculo}" 
				list="tipoServicioMap" 
				key="midas.suscripcion.cotizacion.datosConfVehiculo.tipoServicio"
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
				onchange="onChangeTipoUsoVehiculo('%{idNegocioPaqueteName}', '%{idNegocioSeccionName}')"
				required="%{requerido}" labelposition="%{posicionEtiqueta}"
				cssStyle="margin-left:26px" cssClass="cajaTextoM2 w200 jQrequired"/>
		</td>
</s:if>
</tr>
<tr>
<s:if test="%{mostrarPaquete}">
		<td>
		      <s:if test="%{#disabledConsulta}">
				<s:hidden id="%{idNegocioPaqueteName}" name="%{idNegocioPaqueteName}" value="%{idNegocioPaquete}"/>
			  </s:if>
			  
			   <!-- s:select id="%{idNegocioPaqueteName}" name="%{idNegocioPaqueteName}" value="%{idNegocioPaquete}" 
				list="paqueteMap" 
				key="midas.suscripcion.cotizacion.datosConfVehiculo.paquete" 
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="%{onChangePaquete}; mostrarPorcentajeDescuentoEstado();mostrarPorcentajeDescuentoEstadoDefault();cargaDerechosNegocio();validaRecalcular();"
				disabled="%{#disabledConsulta}"
				required="%{requerido}" labelposition="%{posicionEtiqueta}" 
				cssStyle="margin-left:41px" cssClass="cajaTextoM2 w200 jQrequired"/-->
			  
			  
			  <s:select id="%{idNegocioPaqueteName}" name="%{idNegocioPaqueteName}" value="%{idNegocioPaquete}" 
				list="paqueteMap" 
				key="midas.suscripcion.cotizacion.datosConfVehiculo.paquete" 
				headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="%{onChangePaquete}; mostrarPorcentajeDescuentoEstado();mostrarPorcentajeDescuentoEstadoDefault();cargaDerechosNegocio();"
				disabled="%{#disabledConsulta}"
				required="%{requerido}" labelposition="%{posicionEtiqueta}" 
				cssStyle="margin-left:41px" cssClass="cajaTextoM2 w200 jQrequired"/>
				
		</td>	
</s:if>
		<td>
			<div id="porcentajeDescuentoEstado">
				<div>
					<s:if test="%{#disabledConsulta}">
						<s:hidden id="%{pctDescuentoEstadoName}" name="%{pctDescuentoEstadoName}" value="%{pctDescuentoEstado}"/>
					</s:if>
		 			<s:textfield key="midas.general.pctDescuentoEstado" id="%{pctDescuentoEstadoName}" 
		 			name="%{pctDescuentoEstadoName}" value="%{pctDescuentoEstado}" 
		 			labelposition="%{posicionEtiqueta}" cssClass="cajaTextoM2 w180" readonly="false"
		 			disabled="%{#disabledConsulta}"
		 			onchange="validaPorcentajeDescuentoEstado('%{pctDescuentoEstadoName}'), ocultaGuardar();"
		 			onkeypress="return soloNumeros(this,event,true, true)"
		 			maxlength="5"/>
				</div>
			</div>
		</td>
	</tr>	
</table>
<div style="display:none" id="numSerieVin"><s:property value="serie" /></div>
<script type="text/javascript">
(function() {
	iniciarlizarPestania();
})();
</script>