<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
	var seleccionarAgentePath = '<s:url action="ventanaAgentes" namespace="/suscripcion/cotizacion/auto"/>';
</script>
<div class="subtituloIzquierdaDiv"><s:text name="midas.cotizacion.informacionagente" /></div>
<table id="agregar">
	<tr>
		<s:hidden id="idTcAgenteCompName" name="%{idTcAgenteCompName}"/>
		<th><s:text name="midas.negocio.agente.claveAgente" /></th>
		<td id="idAgenteCot"><s:property value="controlAgenteActionAgente.idAgente" /></td>
		<td>
			<s:if test="%{permiteBuscar}">
				<div id="divBuscarBtn" class="w180" style="display: block; float:right;">
					<div class="btn_back w170" >
						<a href="javascript: void(0);" onclick="seleccionarAgente();" class="icon_persona">	
							<s:text name="midas.cotizacion.seleccionaragente"/>	
						</a>
                    	</div>
                    </div>					
			</s:if>
		</td>
	</tr>
	<tr>
		<th><s:text name="midas.cotizacion.nombreagente" /></th>
		<td id="agenteNombre"><s:property value="controlAgenteActionAgente.nombreCompleto" /></td>
		<td>&nbsp;</td>
	</tr>
</table>

