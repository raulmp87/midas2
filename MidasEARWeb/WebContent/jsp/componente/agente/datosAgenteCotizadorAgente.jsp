<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
	var seleccionarAgentePath = '<s:url action="ventanaAgentes" namespace="/suscripcion/cotizacion/auto"/>';
</script>
<table id="agregar">
	<tr>
		<s:hidden id="idAgente" name="%{idTcAgenteCompName}"/>
		<th><s:text name="midas.negocio.agente.claveAgente" /></th>
		<td id="idAgenteCot"><s:property value="controlAgenteActionAgente.idAgente" /></td>
		<td>
			<s:if test="%{permiteBuscar}">
				<div id="divBuscarBtn" class="w180" style="display: block; float:right;">
					<div class="btn_back w170" >
						<a href="javascript: void(0);" onclick="seleccionarAgenteCotizacion(4);" class="icon_persona">	
							<s:text name="midas.cotizacion.seleccionaragente"/>	
						</a>
                    	</div>
                    </div>					
			</s:if>
		</td>
	</tr>
	<tr>
		<th><s:text name="midas.cotizacion.nombreagente" /></th>
		<td id="agenteNombre" colspan="2"><s:property value="controlAgenteActionAgente.nombreCompleto" /></td>
	</tr>
</table>

