<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value='/css/midas.css'/>" rel="stylesheet" type="text/css">
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script language="JavaScript" src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>

<sj:head/>
<script type="text/javascript">jQuery.noConflict();</script>

<div id="loading" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		 src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
</div>

<script type="text/javascript">
$(document).ready(function() {
	 
    $('#btn-submit').click(function() { 
 
        $(".error").hide();
        var hasError = false;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 
        var emailaddressVal = jQuery('#UserEmail').val().split(";");
        if(emailaddressVal == '') {	
            $("#btn-submit").before('<span class="error">Proporcione la direcci&oacute;n de correo</span>');
            hasError = true;
        }
 
         for(i = 0; i < emailaddressVal.length; i++){  
	         if(!emailReg.test(emailaddressVal[i])) { 	
	           jQuery("#btn-submit").before('<span class="error">Proporcione una direcci&oacute;n v&aacute;lida</span> ' + "\n" );
	           hasError = true;
	          break;
	         }
	        }
	       
	      /* for(i = 0; i < emailaddressVal.length; i++){
	         if(!emailReg.test(emailaddressVal[i])) {
	        	jQuery("#UserEmail").after('<span class="error">'+[emailaddressVal[i]]+',</span> \n' );
	            hasError = true;
	         }
	       }*/
 
        if(hasError == true) { return false; }
 
    });
});

function validateExpress(){
	$(".error").hide();
    var hasError = false;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    var emailaddressVal = jQuery('#UserEmail').val().split(";");
    if(emailaddressVal == '') {	
        $("#b_agregar").before('<span class="error">Proporcione la direcci&oacute;n de correo</span>');
        hasError = true;
    }

     for(i = 0; i < emailaddressVal.length; i++){  
	      if(!emailReg.test(emailaddressVal[i])) { 	
	        jQuery("#b_agregar").before('<span class="error">Proporcione una direcci&oacute;n v&aacute;lida</span> ' + "\n" );
	        hasError = true;
	       break;
	      }
     }  
    if(hasError == true) { return false; }else{return true;}
}

function sendExpress(){
	if(validateExpress()){
		var url = '/MidasWeb/suscripcion/cotizacion-express/auto/enviarCotizacionPorCorreo.action';
		parent.redirectVentanaModal('correo', url, jQuery('#enviarCotizacionPorCorreo')[0]);
	}
}

</script>
<s:form action="enviarCotizacionPorCorreo" namespace="/suscripcion/cotizacion-express/auto" id="enviarCotizacionPorCorreo" >
<center>
	<table id="agregar">	
		<tr>
			<td>
				<s:textfield labelposition="left" key="midas.correo.destinatario" name="nombreDestinatario" size="50"/>			
			</td>
		</tr>	
		<tr>			
			<td>
				<s:textarea key="midas.suscripcion.solicitud.solicitudPoliza.enviarCotizacion"
							id="UserEmail" name="correoDestinatario"  cssClass="cajaTextoM2 jQemail jQrequired" cssStyle="font-size:11px;"
							title="Correos electronicos separados por punto y coma"
							alt="Correos electronicos separados por punto y coma"
							rows="2" cols="45"  />				
			</td>
		</tr>
		<tr>
			<td>
				<div id="b_agregar" style="float:right; margin-right: 5px">
					<a href="javascript: void(0);" 
						onclick="javascript:sendExpress();">
						<s:text name="midas.boton.cotizacion.enviar"/>
					</a>
				</div>				
			</td>			
		</tr>	 		  
	</table>
</center>
<s:hidden name="paquetesMatrizStr"></s:hidden>
<s:hidden name="formaPagoMatrizStr"></s:hidden>
</s:form>