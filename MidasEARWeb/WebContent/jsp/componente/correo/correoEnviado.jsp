<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<div id="detalle" style="overflow:auto;height: 150px" >
	<center>
		<table id="t_riesgo" width="100%">
			<tr>
				<th width="10%">Informaci&oacute;n</th>
			</tr>
			<tr>
				<td>
					
					<s:if test="mensajeCorreoExito != null" >
						<s:property value="mensajeCorreoExito" />
					</s:if>
					<s:else>
						<s:text name="midas.correo.enviadoExitoso"/>
					</s:else>
				</td>			
		</table>
	</center>
</div>