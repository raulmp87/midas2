<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<% 
	String gridColumnsPathVar = (String) request.getParameter("gColPath"); 
   	String gridRowPathVar = (String) request.getParameter("gRowPath");
%>
<rows total_count="<s:property value="total" escapeHtml="false" escapeXml="true"/>" 
	  pos="<s:property value="posStart" escapeHtml="false" escapeXml="true"/>">
	
	<s:if test="posStart == 0">
		<head>
			<beforeInit>
				<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
				<call command="setSkin"><param>light</param></call>
			</beforeInit>
			
			<jsp:include page="<%=gridColumnsPathVar%>" />
		    
		</head>
    </s:if>
    <s:iterator value="scrollableList" status="status">
		<row id="<s:property value="#action.posStart + #status.index" escapeHtml="false" escapeXml="true"/>">
			
			<jsp:include page="<%=gridRowPathVar%>" />
		
		</row>
	</s:iterator>
	
</rows>