<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script id="scrollableJs" type="text/javascript" src="<s:url value="/js/componente/scrollable/scrollableNavigation.js"/>"></script>

<div id ="<s:property value="%{name}"/>_contenedor" style="display:inline;">

	<s:hidden name="gridNameFirst" value="%{name}"/>
	 
	<s:hidden name="gridName" value="%{name}"/> 
	<s:hidden name="gridForm" value="%{form}"/>
	<s:hidden name="loadAction" value="%{loadAction}"/>
	<s:hidden name="selectJs" value="%{selectJs}"/>
	<s:hidden name="checkJs" value="%{checkJs}"/>
	<s:hidden name="cellChangedJs" value="%{cellChangedJs}"/>
	<s:hidden name="gridColumnsPath" value="%{gridColumnsPath}"/>
	<s:hidden name="gridRowPath" value="%{gridRowPath}"/>
	<div id="espacioIndicador" align="left" style="width:97%;height:50px">
		
	</div>
</div>

<div id ="<s:property value="%{name}"/>" style="width:97%;height:247px"></div>
