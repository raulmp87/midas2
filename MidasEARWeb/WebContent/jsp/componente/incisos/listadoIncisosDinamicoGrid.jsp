<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO || 
			idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO">
		   <s:if test="accionEndoso == 3">
		       <column id="selected" type="ch" width="30" sort="int" ></column>   
		   </s:if>
		   <s:else>
		       <column id="selected" type="ch" width="30" sort="int" >#master_checkbox</column>
		   </s:else>
		</s:if>
		<s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL || 
			idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
			<column id="selected" type="ra" width="30" sort="int" ></column>
			<column id="idSiniestro" type="ro" width="30" sort="int" hidden="true"></column>
			<column id="noSiniestro" type="ro" width="30" sort="int" hidden="true"></column> 	
			<column id="fechaOcurrenciaSiniestro" type="ro" width="30" sort="int" hidden="true"></column>
			<column id="tipoIndemnizacion" type="ro" width="30" sort="int" hidden="true"></column>
		</s:elseif>
		<s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_DESAGRUPACION_RECIBOS">
			<column id="selected" type="ra" width="30" sort="int" ></column>
		</s:elseif>	
		<column id="numeroInciso" type="ro" width="100" sort="int" ><s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroInciso" /></column>
		<column id="descripcionVehiculo"  type="ro" width="<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO 
		|| idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS" >*</s:if><s:else>450</s:else>" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.descripcionVehiculo" /></column>
		<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO || 
		idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS">
		    <column id="paquete" type="ro" width="130" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.paquete" /></column>
		</s:if>
		<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CAMBIO_DATOS || 
			idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO ||
	        idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO  || 
	        idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO || 
	        idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_DESAGRUPACION_RECIBOS">
		    <column id="numeroSerieVehiculo" type="ro" width="120" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie" /></column>
		    <column id="numeroMotor" type="ro" width="120" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.numeroMotor" /></column>
		    <column id="placas" type="ro" width="100" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.placas" /></column>
		    <column id="conductor" type="ro" width="200" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.conductor" /></column>
		    <column id="asegurado" type="ro" width="200" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.asegurado" /></column>
		</s:if>
		<s:elseif test = "idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL || 
			idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
	    	<column id="numeroSerieVehiculo" type="ro" width="120" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie" /></column>
		    <column id="numeroMotor" type="ro" width="120" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.numeroMotor" /></column>
		    <column id="placas" type="ro" width="100" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.placas" /></column>
		    <column id="asegurado" type="ro" width="*" sort="int" ><s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.asegurado" /></column>
	    </s:elseif>
		<s:if test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO && estatusCotizacion == 10) || 
		idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS">
		    <column id="valorPrimaNeta" type="ron" width="*" format="$0,000.00" sort="str" hidden="false"><s:text name="midas.cotizacion.totalprimas"/></column>
			<column id="valorPrimaTotal" type="ron" width="*" format="$0,000.00" sort="str" hidden="false"><s:text name="midas.cotizacion.primatotal"/></column>		
		</s:if>
		<s:elseif test="idTipoVista != @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL && 
			idTipoVista != @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
		    <column id="solicitud.primaTotal" type="ron" width="120" sort="int" format="$0,000.00" ><s:text name="midas.cotizacion.totalprimas" /></column>
		</s:elseif>	
		<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO">		  
		    <column id="fechaCancelacion" type="ro" width="200" sort="int" ><s:text name="midas.componente.incisos.columna.fechaCancelacion.titulo" /></column>
		</s:if>
		<column id="solicitud.claveEstatus" type="ro" width="120" sort="int" hidden="true" align="center"><s:text name="midas.endosos.cotizacionEndosoListado.estatus" /></column>		
		
		<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO && estatusCotizacion == 10">
	        <column id="action1" type="img" width="35" align="center"></column>
	        <column id="action2" type="img" width="35" align="center"></column>
	        <column id="action3" type="img" width="35" align="center"></column>		
	        <column id="action7" type="img" width="35" align="center"></column>
		</s:if>
		<s:elseif test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO && (estatusCotizacion == 12 || estatusCotizacion == 14)) ||
		                (idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CAMBIO_DATOS && (estatusCotizacion == 16 || estatusCotizacion == 10))">
		    <column id="action4" type="img" width="30" align="center"></column>
		    <column id="action5" type="img" width="30" align="center"></column>
		    <s:if test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO && (estatusCotizacion == 12 || estatusCotizacion == 14))" >
		    <column id="action8" type="img" width="30" align="center"></column>
		    <column id="action9" type="img" width="30" align="center"></column>
		    </s:if>		
		</s:elseif>
		<s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS && (estatusCotizacion == 16 || estatusCotizacion == 10)">
		    <column id="action3" type="img" width="30" align="center"></column>		    		  
		    <column id="action4" type="img" width="30" align="center"></column>		    
		</s:elseif>		
		<s:elseif test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO || 
			idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO) && (estatusCotizacion == 16 
				|| estatusCotizacion == 10)">
		    <column id="action6" type="img" width="30" align="center"></column>
		</s:elseif>			
	</head> 
	 		
	<s:iterator var="listaIncisosCotizacion" value="listaIncisosCotizacion" status="stats" >
	    <row id="<s:property value="continuity.id" />">
		    <s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL ||
			    idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
		        <cell disabled="true"><s:property value="value.checked" escapeHtml="false" escapeXml="true"/></cell>
		        <cell><s:property value="value.siniestroCabinaId" escapeHtml="false" escapeXml="true"/></cell>
		        <cell><s:property value="value.numeroSiniestro" escapeHtml="false" escapeXml="true"/></cell> 
		        <cell><s:property value="value.fechaOcurrenciaSiniestro" escapeHtml="false" escapeXml="true"/></cell>
		        <cell><s:property value="value.tipoIndemnizacion" escapeHtml="false" escapeXml="true"/></cell>
		     </s:if>
		     <s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO || 
		     	idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO || 
		     	idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_DESAGRUPACION_RECIBOS">
		     	<cell disabled="true"><s:property value="value.checked" escapeHtml="false" escapeXml="true"/></cell>
		    </s:elseif>		     		
			<cell><s:property value="continuity.numero" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="value.autoInciso.descripcionFinal" escapeHtml="false" escapeXml="true"/></cell>		
			<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO || 
			idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS">
		        <cell><s:property value="value.autoInciso.paquete.descripcion" escapeHtml="false" escapeXml="true"/></cell>
		    </s:if>
		    <s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CAMBIO_DATOS || 
			    idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO || 
			    idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO  || 
			    idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO ||
			     idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_DESAGRUPACION_RECIBOS">
		        <cell><s:property value="value.autoInciso.numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			    <cell><s:property value="value.autoInciso.numeroMotor" escapeHtml="false" escapeXml="true"/></cell>			
			    <cell><s:property value="value.autoInciso.placa" escapeHtml="false" escapeXml="true"/></cell>
			    <cell><s:property value="value.autoInciso.nombreCompletoConductor" escapeHtml="false" escapeXml="true" /></cell>
			    <cell><s:property value="value.autoInciso.nombreAseguradoUpper" escapeHtml="false" escapeXml="true"></s:property></cell>
		    
		    </s:if>
		    <s:elseif test = "idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL || 
			    idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
				<cell><s:property value="value.autoInciso.numeroSerie" escapeHtml="false" escapeXml="true"/></cell>
			    <cell><s:property value="value.autoInciso.numeroMotor" escapeHtml="false" escapeXml="true"/></cell>			
			    <cell><s:property value="value.autoInciso.placa" escapeHtml="false" escapeXml="true"/></cell>
			    <cell><s:property value="value.autoInciso.nombreAseguradoUpper" escapeHtml="false" escapeXml="true"></s:property></cell>
			</s:elseif>    
		    <s:if test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO && estatusCotizacion == 10) || 
		    idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS">
		         <cell><s:property value="value.valorPrimaNeta" escapeHtml="false" escapeXml="true"/></cell>
				 <cell><s:property value="value.valorPrimaTotal" escapeHtml="false" escapeXml="true"/></cell>
		    </s:if>
		    <s:elseif test="idTipoVista != @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO_PERDIDA_TOTAL && 
			idTipoVista != @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
		        <cell><s:property value="value.valorPrimaNeta" escapeHtml="false" escapeXml="true"/></cell>
		    </s:elseif>			
			<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO">
		        <cell><s:property value="recordInterval.to" escapeHtml="false" escapeXml="true"/></cell>
		    </s:if>
			<cell>			   
				<s:if test="estatusCotizacion == 10">
					COT EN PROCESO
				</s:if>				
				<s:if test="estatusCotizacion == 12">
					COT TERMINADA
				</s:if>	
				<s:if test="estatusCotizacion == 14">
					COT LISTA EMITIR
				</s:if>				
				<s:if test="estatusCotizacion == 16">
					COT EMITIDA
				</s:if>							
			</cell>				
			<s:if test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_ALTA_INCISO && estatusCotizacion == 10">
			    <s:if test="accionEndoso == 2 || accionEndoso == 4">
			        <cell>/MidasWeb/img/carAdd.png^Multiplicar Inciso^javascript: mostrarMultiplicarInciso(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			    <cell>/MidasWeb/img/carDelet.png^Eliminar Inciso^javascript: eliminarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="accionEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,<s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.cotizacionContinuity.id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			    </s:if>	
			    <s:if test="accionEndoso == 3">
			        <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Inciso^javascript: mostrarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()"/>,<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,<s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			    </s:if> 
			    <s:else>
			        <cell>/MidasWeb/img/carEdit.png^Editar Inciso^javascript: mostrarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getNuevoEndosoCot()" escapeHtml="false" escapeXml="true"/>,<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,<s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
			    </s:else>           
			    <s:if test="accionEndoso == 2 || accionEndoso == 4">
		            <cell>/MidasWeb/img/calcIconAmount.jpg^Igualar Inciso^javascript: mostrarIgualarInciso("<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>","<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>", "<s:property value="idToPoliza"/>",jQuery("#tipoEndoso").val(),"<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()" />","<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>","<s:property value="value.valorPrimaTotal" escapeHtml="false" escapeXml="true"/>")^_self</cell>	
		        </s:if>
		    </s:if>	
		    <s:elseif test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO 
		    && (estatusCotizacion == 12 || estatusCotizacion == 14)) ||
		                (idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CAMBIO_DATOS 
		                && (estatusCotizacion == 16 || estatusCotizacion == 10))">
		        <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
		            <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Complementar Datos Adicionales Vehiculo^javascript: mostrarVentanaVehiculoListadoIncisos(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",null,<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()"/>)^_self</cell>
		        </s:if>
		        <s:else>
		        
		        	<s:if test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_CAMBIO_DATOS)
		        			&& (value.blocked)">
		            	<cell>/MidasWeb/img/icons/ico_bloquear.gif^<s:property value="value.siguienteModificacionProgramada" escapeHtml="false" escapeXml="true"/></cell>
		        	</s:if>
		        	<s:else>
		            	<cell>/MidasWeb/img/icons/ico_agregar.gif^Complementar Datos Adicionales Vehiculo^javascript: mostrarVentanaVehiculoListadoIncisos(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",null,null)^_self</cell>
			        	<cell>/MidasWeb/img/persona.jpg^Complementar Datos Asegurado^javascript: mostrarVentanaAseguradoListadoIncisos(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>")^_self</cell>
			        	<s:if test="(habilitaCobranzaInciso && (value.autoInciso.nombreAseguradoUpper != NULL && value.autoInciso.nombreAseguradoUpper != '') && idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO && (estatusCotizacion == 12 || estatusCotizacion == 14))" >
						<cell>/MidasWeb/img/signopesos.gif^Cobranza^javascript: mostrarVentanaMedioPago(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>")^_self</cell>
						</s:if>	
						<s:if test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_COMPLEMENTAR_INCISO && (estatusCotizacion == 12 || estatusCotizacion == 14))" >
						<cell>/MidasWeb/img/listsicon.gif^Condiciones Especiales^javascript: mostrarVentanaCondicionesEspecialesInciso(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>","<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()" />")^_self</cell>
						</s:if>			        	
		        	</s:else> 
		        
		        </s:else>        		
		    </s:elseif>
		     <s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS && estatusCotizacion == 10 && accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
		        <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Inciso^javascript: mostrarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()" escapeHtml="false" escapeXml="true"/>,<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,<s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
		    </s:elseif>
		    <s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_MOVIMIENTOS && (estatusCotizacion == 16 || estatusCotizacion == 10)">
		    
		    	<s:if test="value.blocked">
		            	<cell>/MidasWeb/img/icons/ico_bloquear.gif^<s:property value="value.siguienteModificacionProgramada" escapeHtml="false" escapeXml="true"/></cell>
		       	</s:if>
		        <s:else>
		        	<cell>/MidasWeb/img/carEdit.png^Editar Inciso^javascript: mostrarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot()" escapeHtml="false" escapeXml="true"/>,<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,<s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/>)^_self</cell>	
		        	<cell>/MidasWeb/img/calcIconAmount.jpg^Igualar Inciso^javascript: mostrarIgualarInciso("<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>","<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>", "<s:property value="idToPoliza"/>",jQuery("#tipoEndoso").val(),"<s:property value="accionEndoso" />","<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>","<s:property value="value.valorPrimaTotal" escapeHtml="false" escapeXml="true"/>")^_self</cell>	
		    	</s:else> 
		    
		    </s:elseif>
		    <s:elseif test="(idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_BAJA_INCISO) && (estatusCotizacion == 16 || estatusCotizacion == 10)">
		        <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Inciso^javascript: mostrarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="@mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()"/>,<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>,<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,<s:property value="value.numeroSecuencia" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </s:elseif>
		     <s:elseif test="idTipoVista == @mx.com.afirme.midas2.action.componente.incisos.ListadoIncisosDinamicoAction@VISTA_REHABILITAR_INCISO && (estatusCotizacion == 16 || estatusCotizacion == 10)">
		        <cell>/MidasWeb/img/icons/ico_verdetalle.gif^Consultar Inciso^javascript: mostrarVentanaConsultaInciso(<s:property value="continuity.id" escapeHtml="false" escapeXml="true"/>,"<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>",<s:property value="validoEn.getTime()" escapeHtml="false" escapeXml="true"/>,<s:property value="getRecordInterval().getInterval().getEndMillis()" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </s:elseif>					 
		</row>
	</s:iterator>
</rows> 