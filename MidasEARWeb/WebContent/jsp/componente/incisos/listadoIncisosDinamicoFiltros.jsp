<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include	value="/jsp/suscripcion/cotizacion/auto/inciso/contenedorHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/incisos/listadoIncisosDinamico.js'/>"></script>

<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso/incisovehiculobitemporal.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarDatosVehiculo.js'/>"></script>

<s:form id="listadoIncisosFiltrosForm">
<s:hidden name="idTipoVista" id="idTipoVista"/>
<s:hidden name="estatusCotizacion" id="estatusCotizacion"/>
<s:hidden name="hasRecordsInProcess" id="hasRecordsInProcess"/>
<s:hidden name="validoEn" id="idValidoEnName"/>
<s:hidden name="idToPoliza" id="idToPolizaName"/>
<s:hidden name="accionEndoso" id="idAccionEndosoName"/>
<s:hidden name="claveTipoEndoso" id="idClaveTipoEndosoName"/>
<s:hidden name="numeroCotizacion" id="numeroCotizacion"/>

<script type="text/javascript">
	var verDetalleIncisoPath = '<s:url action="verDetalleInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	var eliminarIncisoPath = '<s:url action="eliminarInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	var mostrarMultiplicarIncisoPath = '<s:url action="mostrarMultiplicarInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>';
	var mostrarIgualarIncisoPath = '<s:url action="mostrarIgualarInciso" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>'
	var mostrarIgualarMasivoPath = '<s:url action="mostrarIgualarMasivo" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso"/>'
	
	var lineaNegocioId;
	function cargarComboPaquetes(claveTipoEndoso, cotizacionContinuityId, validoEn, idNegocioSeccion) {	    
	    
        var dateParts = validoEn.split("/");  
        var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]); 
  
		parent.removeAllOptionsAndSetHeader(
				document.getElementById('filtros.incisoAutoCot.negocioPaqueteId'), parent.headerValue,
				"Cargando...");
		if (idNegocioSeccion !=null && idNegocioSeccion != '') {
			listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(claveTipoEndoso, cotizacionContinuityId, date, idNegocioSeccion, function(
					data) {
				parent.addOptions(document.getElementById('filtros.incisoAutoCot.negocioPaqueteId'), data);
			});
		}else
		{
		removeAllOptionsAndSetHeader(document.getElementById('filtros.incisoAutoCot.negocioPaqueteId'), 
				'', 
				"SELECCIONE...");
		}
	}
</script>


    <table id="agregar" class="fixTablaInciso" cellspacing="1%" cellpadding="1%" width="98%">
        <s:if test="idTipoVista == @vs@VISTA_ALTA_INCISO || IdTipoVista == @vs@VISTA_COMPLEMENTAR_INCISO || idTipoVista == @vs@VISTA_MOVIMIENTOS || 
                    idTipoVista == @vs@VISTA_BAJA_INCISO || idTipoVista == @vs@VISTA_CAMBIO_DATOS ||
                    idTipoVista == @vs@VISTA_REHABILITAR_INCISO || IdTipoVista == @vs@VISTA_BAJA_INCISO_PERDIDA_TOTAL || IdTipoVista == @vs@VISTA_DESAGRUPACION_RECIBOS">
        <tr>                      
            <td>
                <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.numeroInciso"></s:text>:
            </td>
            <td style="padding-left:0px;">
                <s:textfield id="filtros.id.numeroInciso" name="filtros.id.numeroInciso"                             
                             cssClass="txtfield  jQnumeric jQrestrict w80" 		                     						
					         labelposition="left"   
							 size="10"
							 maxlength="10"
							 onkeypress="return soloNumerosM2(this, event, false)"
							 onblur="this.value=jQuery.trim(this.value)"/>
            </td>           
            <td>
                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.descripcionVehiculo"></s:text>:
            </td>
            <td><s:select list="descripcionesIncisos" headerKey="null"
					headerValue="%{getText('midas.general.seleccione')}"
					name="filtros.descripcionGiroAsegurado" id="descripcionesIncisos"
					cssClass="txtfield">
				</s:select>
			</td> 
		</tr>
		<tr>                       
            <s:if test="idTipoVista == @vs@VISTA_ALTA_INCISO || IdTipoVista == @vs@VISTA_MOVIMIENTOS">
	            <td>
	                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.lineaNegocio"></s:text>:
	            </td>
	             <td width=""><s:select list="negocioSeccionList"
					listValue="seccionDTO.descripcion" listKey="idToNegSeccion"
					headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="filtros.incisoAutoCot.negocioSeccionId" id="filtros.incisoAutoCot.negocioSeccionId"
					onchange="cargarComboPaquetes('%{claveTipoEndoso}', '%{idToPoliza}', '%{validoEn}', this.value);"
					cssClass="txtfield">
				</s:select>
			    </td>
	            <td>
                    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.paquete"></s:text>:
                </td>
                <td>
                <s:select list="paquetes" headerKey=""
					headerValue="%{getText('midas.general.seleccione')}"
					name="filtros.incisoAutoCot.negocioPaqueteId" id="filtros.incisoAutoCot.negocioPaqueteId"
					cssClass="txtfield" />
			</td> 
	        </s:if>
	        <s:elseif test="idTipoVista == @vs@VISTA_BAJA_INCISO || IdTipoVista == @vs@VISTA_BAJA_INCISO_PERDIDA_TOTAL || idTipoVista == @vs@VISTA_CAMBIO_DATOS || 
	                        idTipoVista == @vs@VISTA_REHABILITAR_INCISO || IdTipoVista == @vs@VISTA_COMPLEMENTAR_INCISO || IdTipoVista == @vs@VISTA_DESAGRUPACION_RECIBOS">
	            <td>
	                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.bajaInciso.numeroSerie"></s:text>:
	            </td>
	            <td style="padding-left:0px;">
	                <s:textfield id="filtros.incisoAutoCot.numeroSerie" name="filtros.incisoAutoCot.numeroSerie" 	                            
	                             cssClass="txtfield jQalphanumeric jQrestrict w160" 	                            							
								 labelposition="left"  
								 size="10"
								 maxlength="17"
								 onblur="this.value=jQuery.trim(this.value)" />
	            </td>
	            <td>
                    <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.numeroMotor"></s:text>:
                </td>
                <td style="padding-left:0px;">
                    <s:textfield id="filtros.incisoAutoCot.numeroMotor" name="filtros.incisoAutoCot.numeroMotor"
                                 cssClass="txtfield jQrestrict w160"                                  						
								 labelposition="left"  
								 size="19"
								 maxlength="19" />
                </td>              
	        </s:elseif>	             
        </tr>
        </s:if>        
        <s:if test="idTipoVista == @vs@VISTA_BAJA_INCISO || idTipoVista == @vs@VISTA_CAMBIO_DATOS || 
                    idTipoVista == @vs@VISTA_REHABILITAR_INCISO || IdTipoVista == @vs@VISTA_COMPLEMENTAR_INCISO 
                    || IdTipoVista == @vs@VISTA_DESAGRUPACION_RECIBOS">
        <tr>            
                     
           <td>
               <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.conductor"></s:text>:
           </td>
           <td style="padding-left:0px;">
               <s:textfield id="filtros.incisoAutoCot.nombreConductor" name="filtros.incisoAutoCot.nombreConductor"
                            cssClass="txtfield jQrestrict w160"                           							
							labelposition="left"  
							size="10"
					     	maxlength="50"
						    onblur="validaLongitud('filtros.incisoAutoCot.nombreConductor', this.value=jQuery.trim(this.value))"/>
           </td>                 
	           <td>
	                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.asegurado"></s:text>:
	           </td>
	           <td style="padding-left:0px;">	                                    
	                <s:textfield id="filtros.incisoAutoCot.nombreAsegurado" name="filtros.incisoAutoCot.nombreAsegurado"
	                             cssClass="txtfield  jQrestrict w160"                           							
			                     labelposition="left"  
			                     size="10"
			                     maxlength="50"
			                     onblur="validaLongitud('filtros.incisoAutoCot.nombreAsegurado', this.value=jQuery.trim(this.value))" />                                      
	           </td>                       	                
        </tr>
        </s:if>
        <s:elseif test="IdTipoVista == @vs@VISTA_BAJA_INCISO_PERDIDA_TOTAL">
           <tr>                                               
		   		<td>
		        	<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.asegurado"></s:text>:
		        </td>
		           <td style="padding-left:0px;">	                                    
		                <s:textfield id="filtros.incisoAutoCot.nombreAsegurado" name="filtros.incisoAutoCot.nombreAsegurado"
		                             cssClass="txtfield  jQrestrict w160"                           							
				                     labelposition="left"  
				                     size="10"
				                     maxlength="50"
				                     onblur="this.value=jQuery.trim(this.value)" />                                      
		       </td>                       	                
          </tr>                   
        </s:elseif>            
        <s:if test="idTipoVista == @vs@VISTA_REHABILITAR_INCISO">
            <tr>                    
                <td>
                   <s:text name="fecha Cancelacion de"></s:text>:
                </td>
                <td style="padding-left:0px;">
	                <sj:datepicker name="filtros.cotizacionDTO.fechaInicioVigencia" required="#requiredField" cssStyle="width: 200px;"
								   			   buttonImage="../img/b_calendario.gif"
								               id="fechaCotizacionInicio" maxlength="10" cssClass="txtfield"	
								               labelposition="top" 
								               size="12"
								               maxDate="today"
								               changeMonth="true"
								               changeYear="true"							   								  
								               onkeypress="return soloFecha(this, event, false);"
								               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								               onblur="esFechaValida(this);"></sj:datepicker>
                </td>           
                <td> 
                    <s:text name="hasta"></s:text>:
                </td>
                <td style="padding-left:0px;">	                                    
	                <sj:datepicker name="filtros.cotizacionDTO.fechaFinVigencia" required="#requiredField" cssStyle="width: 200px;"
								    		   buttonImage="../img/b_calendario.gif"
								    		   labelposition="top" 
								    		   size="12"
								    		   maxDate="today"
								               changeMonth="true"
								               changeYear="true"
								    		   id="fechaCotizacionFin" maxlength="10" cssClass="txtfield"								   								  
								    		   onkeypress="return soloFecha(this, event, false);"
								    		   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								    		   onblur="esFechaValida(this);"></sj:datepicker>                                      
                </td>             	                
            </tr>
        </s:if>        
        <s:if test="idTipoVista == @vs@VISTA_BAJA_INCISO || IdTipoVista == @vs@VISTA_BAJA_INCISO_PERDIDA_TOTAL || idTipoVista == @vs@VISTA_CAMBIO_DATOS || 
                    idTipoVista == @vs@VISTA_REHABILITAR_INCISO || IdTipoVista == @vs@VISTA_COMPLEMENTAR_INCISO || IdTipoVista == @vs@VISTA_DESAGRUPACION_RECIBOS">
        <tr>
            <td>
                <s:text name="midas.endosos.solicitudEndoso.tiposEndoso.cambioDatos.placas"></s:text>:
            </td>
            <td style="padding-left:0px;">
               <s:textfield id="filtros.incisoAutoCot.placa" name="filtros.incisoAutoCot.placa"
                            cssClass="txtfield jQrestrict w160"                            							
			                labelposition="left"  
			                size="10"
			                maxlength="10"
			                onblur="this.value=jQuery.trim(this.value)" />
           </td>
        </tr>
        </s:if> 
        <s:if test="idTipoVista != @vs@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">                       
        <tr>
            <td colspan="2"></td>                                               
            <td colspan="2" align="right">
                <div id="divBuscarBtn" class="w150" style="float:right;">
		            <div class="btn_back w140" style="display: inline; float: right;">
					    <a href="javascript: void(0);" onclick="limpiarFiltrosListadoDinamicoIncisos();">
						    <s:text name="midas.endosos.solicitudEndoso.definirSolicitudEndoso.boton.limpiar" /> </a>
	                </div>
                </div>                
                <div id="divBuscarBtn" class="w150" style="float:right;">
		            <div class="btn_back w140" style="display: inline; float: right;">
					    <a href="javascript: void(0);" onclick="buscarIncisos(1, true, true);">
						    <s:text name="midas.boton.buscar" /> </a>
	                </div>
                </div>
               <s:if test="accionEndoso == @mx.com.afirme.midas2.dto.TipoAccionDTO@getEditarEndosoCot() && (idTipoVista == @vs@VISTA_BAJA_INCISO)">	                      
		            <div id="divSoloCotizados" style="display: inline; float: left;" class="w150" >
								<s:checkbox key="midas.componente.incisos.filtro.solocotizados" labelposition="left" name="filtros.soloCotizados" id="soloCotizados"  />
					</div>
		        </s:if>
            
            </td>
        </tr>
        </s:if>
    </table>
</s:form>
<div>
  <center>
    <table width="100%">
        <tr>
            <td style="font-size: 12px;font-weight: bold;">
            	<s:if test="idTipoVista == @vs@VISTA_BAJA_INCISO_PERDIDA_TOTAL || idTipoVista == @vs@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
                	<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.endososPerdidaTotal.listaIncisos"/>
                </s:if>
                <s:else>
                	<s:text name="midas.endosos.solicitudEndoso.tiposEndoso.altaInciso.listaIncisos"/>
                </s:else>
            </td>                   
        </tr>    
    </table>
    <div id="indicador"></div>
			<div id="gridIncisosPaginado" >
				<div id="incisosListadoGrid" style="width:100%;height:130px"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
			</div>
  </center>
</div>    
<script type="text/javascript"> 
	buscarIncisos(1, true, true);
</script>
<div id="spacer1" style="height: 3px"></div>
<div id="botones" align="right">
    <table>
        <tr> 
            <s:if test="idTipoVista == @vs@VISTA_ALTA_INCISO && accionEndoso != @mx.com.afirme.midas2.dto.TipoAccionDTO@getConsultarEndosoCot()">
                <td>
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="agregarInciso(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,'<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>',<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>);">	
										<s:text name="Agregar"/>	
									</a>
			                    </div>
	                </div>
                </td>
                <td>
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="mostrarIgualacionMasiva(<s:property value="idToPoliza" escapeHtml="false" escapeXml="true"/>,'<s:property value="validoEn" escapeHtml="false" escapeXml="true"/>',<s:property value="claveTipoEndoso" escapeHtml="false" escapeXml="true"/>);">	
										<s:text name="midas.suscripcion.cotizacion.igualacionPrimas.igualacionMasiva"/>	
									</a>
			                    </div>
	                </div>
                </td>                
            </s:if>                                  
            <s:if test="idTipoVista != @vs@VISTA_BAJA_INCISO_PERDIDA_TOTAL && idTipoVista != @vs@VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL">
	            <td>
	                <div id="divLimpiarBtn" style="float:left;" class="w150" >
								<div class="btn_back w140" >
									<a href="javascript: void(0);" onclick="buscarIncisos(1, true, true);">	
										<s:text name="Actualizar"/>	
									</a>
			                    </div>
	                </div>
	            </td>
            </s:if>                      
        </tr>    
    </table>
</div>