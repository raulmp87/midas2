<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
	var ventana=null;
	function mostrarModalDomicilioSin(idField){
		var idField=(idField)?idField:"idDomicilio";
		var url="/MidasWeb/catalogos/domicilio/init.action?tipoAccion=consulta&idField="+idField;
		sendRequestWindow(null, url,obtenerVentanaDomicilioSin);
	}
	
	function obtenerVentanaDomicilioSin(){
		var wins = obtenerContenedorVentanas();
		ventana= wins.createWindow("domicilioModal", 400, 320, 930, 450);
		ventana.centerOnScreen();
		ventana.setModal(true);
		ventana.setText("Consulta de Domicilios");
		ventana.button("park").hide();
		ventana.button("minmax1").hide();
		return ventana;
	}
	
	function findByIdDomicilioSin(idDomicilio,fn){
		var url="/MidasWeb/catalogos/domicilio/loadById.action";
		var data={"domicilio.idDomicilio.idDomicilio":idDomicilio};
		jQuery.asyncPostJSON(url,data,window[fn]);//Populate domicilio debe de estar en un js de donde es llamado la ventana de consulta
		
	}
	
</script>

<s:if test="requerido == 1">
	<s:set id="campoRequerido" value="%{getText('midas.atributo.requerido')}" />
</s:if>
<s:if test="readOnly == \"true\"">
	<s:set id="readOnly" value="true" />
</s:if>
<table class="contenedorDomicilio" width="90%">
 <s:if test="componente == 2"> 	
	<tr>
	  	<td>
	  		<s:textfield id="%{idDomicilioName}" name="%{idDomicilioName}" value="%{idDomicilio}" readonly="true" cssStyle="display:none;" cssClass="cajaTextoM2 w100 setNew jsSearchResultField" onchange="javascript:findByIdDomicilioSin(this.value,'%{funcionResult}');"/>
	  	</td>
	  	<td colspan="2">
	  		<s:if test="enableSearchButton == \"true\"">
	  		<div class="btn_back w140" id="btnDomicilio">
				<a href="javascript: void(0);" class="icon_buscar" 
					onclick="javascript:mostrarModalDomicilioSin('${idDomicilioName}');">
					<s:text name="Buscar Domicilio"/>
				</a>
			</div>	
			</s:if>
	  	</td>
	  	</tr>
  </s:if>
  <s:if  test="componente != 1 && componente != 6">  
  <tr>    
  	<td class="jQIsRequired">
  		<label for="${idPaisName}">
  			<s:text name="labelPais"/>
  		</label>
  	</td>
    <td colspan="2"><s:select id="%{idPaisName}" disabled="%{readOnly}" name="%{idPaisName}" value="%{idPais}" 
				list="paises" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangePaisSin('%{idEstadoName}','%{idCiudadName}','%{idColoniaName}','%{cpName}','%{calleName}','%{numeroName}','%{numeroIntName}','%{referenciaName}','%{idPaisName}')" cssClass="cajaTextoM2 w250 setNew %{#campoRequerido}"/>
	</td>
  </tr>	
  </s:if>
  <tr> 
 	<td class="jQIsRequired">
 		<label for="${idEstadoName}">
 			<s:text name="labelEstado"/>
 		</label>
 	</td> 	
    <td>
    	<s:select id="%{idEstadoName}" disabled="%{readOnly}" name="%{idEstadoName}" value="%{idEstado}" 
				list="estados"  headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeEstadoGeneralSin('%{idCiudadName}','%{idColoniaName}','%{cpName}','%{calleName}','%{numeroName}','%{numeroIntName}','%{referenciaName}','%{idEstadoName}')" cssClass="cajaTextoM2 w250 setNew %{#campoRequerido} jQ_estado"/>
    </td>
    <td class="jQIsRequired">
    	<label for="${idCiudadName}">
 			<s:text name="labelCiudad"/>
 		</label>
 	</td> 
  	<td>
  		<s:select id="%{idCiudadName}" disabled="%{readOnly}" name="%{idCiudadName}" value="%{idCiudad}" 
				list="ciudades"  headerKey="%{getText('midas.general.defaultHeaderKey')}"  headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeCiudadSin('%{idColoniaName}','%{cpName}','%{calleName}','%{numeroName}','%{numeroIntName}','%{referenciaName}','%{idCiudadName}')" cssClass="cajaTextoM2 w250 setNew %{#campoRequerido} jQ_ciudad"/>
  	</td>
  </tr>	
 <s:if test="componente == 2 || componente == 1 || componente == 3 || componente == 4 || componente == 6">
  	<tr> 
  	 <td class="jQIsRequired">
  	 	<label for="${idColoniaName}">
 			<s:text name="labelColonia"/>
 		</label>
 	</td>
  	<td>
  		<s:select id="%{idColoniaName}" disabled="%{readOnly}"  name="%{idColoniaName}" value="%{idColonia}" 
				list="colonias" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				onchange="onChangeColoniaSin('%{cpName}','%{calleName}','%{numeroName}','%{numeroIntName}','%{referenciaName}', '%{idColoniaName}','%{idCiudadName}')" cssClass="cajaTextoM2 w250 setNew %{#campoRequerido}" disabled="%{otraColoniaCheck}"/>
  	</td>
  	<td class="jQIsRequired">
  		<label for="${cpName}">
  			<s:text name="labelCodigoPostal"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{cpName}" name="%{cpName}" value="%{cp}" cssClass="cajaTextoM2 w50 setNew  jQnumeric jQrestrict %{#campoRequerido} autocompletar " disabled="%{readOnly}" maxlength="6" onchange="onChangeCodigoPostalSin(this.value, '%{idColoniaName}','%{idCiudadName}','%{idEstadoName}','%{calleName}','%{numeroName}','%{numeroIntName}','%{referenciaName}','%{idPaisName}','%{cpName}', '%{nuevaColoniaName}', '%{idColoniaCheckName}');"/>
  	</td>
  	</tr>
  </s:if>
  <s:if test="componente == 2 || componente == 1 || componente == 3" >  
  <tr>
  	<td>
		<label for="${nuevaColoniaName}">Nueva Colonia</label>
	</td>
	<td>
		<table width="100%">
		<tbody>
			<tr>
				<td><s:checkbox onchange="onChangeNuevaColoniaSin('%{nuevaColoniaName}','%{idColoniaName}','%{idColoniaCheckName}', 'check');" id="%{idColoniaCheckName}" name="%{idColoniaCheckName}" cssStyle="display:inline-block;" cssClass="cajaTextoM2 w8" disabled="%{readOnly}" value="%{otraColoniaCheck}"></s:checkbox></td>
				<td><s:textfield id="%{nuevaColoniaName}" name="%{nuevaColoniaName}" value="%{nuevaColonia}" cssClass="cajaTextoM2 w200 setNew jQalphaextra" disabled="%{readOnly}" maxlength="60" cssStyle="display:inline-block;" onchange="onChangeNuevaColoniaSin('%{nuevaColoniaName}','%{idColoniaName}','%{idColoniaCheckName}', 'text');"/></td>
			</tr>
		</tbody>
		</table>
	</td>
	  	<td class="jQIsRequired">
  		<label for="${calleName}">
  			<s:text name="labelCalle"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{calleName}" name="%{calleName}" value="%{calle}" cssClass="cajaTextoM2 w250 setNew jQalphaextra %{#campoRequerido} autocompletar " disabled="%{readOnly}" maxlength="100"/>
  	</td>
  </tr>
 <s:if test="componente == 1 || componente == 3" >
  <tr>   	
  	<td>
  		<label for="${numeroName}">
  			<s:text name="labelNumero"/>
  		</label>
  	</td>
  	<td>
  		<s:textfield id="%{numeroName}" name="%{numeroName}" value="%{numero}" cssClass="cajaTextoM2 w50 setNew jQalphaextra %{#campoRequerido}" disabled="%{readOnly}" maxlength="6"/>
  	</td>
  	<td>
  		<label for="${numeroIntName}">
  			<s:text name="labelNumeroInt"/>
  		</label>
  	</td>  	
  	<td>
  		<s:textfield id="%{numeroIntName}" name="%{numeroIntName}" value="%{numeroInt}" cssClass="cajaTextoM2 w50 setNew jQalphaextra %{#campoRequerido}" disabled="%{readOnly}" maxlength="6"/>
  	</td>
  </tr>
 </s:if>	
  </s:if>
  <s:if test="componente == 4" >  
  <tr>  	
  	<td>
  		<s:hidden id="%{calleName}" name="%{calleName}" value="%{calle}" cssClass="cajaTextoM2 w300 setNew jQalphaextra" disabled="%{readOnly}" maxlength="100"/>
  	</td>
  	<s:if test="%{idColoniaName} !=null">
	  	<td>
			<label for="${nuevaColoniaName}">Otra Colonia</label>
		</td>
		<td>
	 		<table width="100%">
			<tbody>
				<tr>
					<td><s:checkbox onchange="onChangeNuevaColoniaSin('%{nuevaColoniaName}','%{idColoniaName}','%{idColoniaCheckName}', 'check');" id="%{idColoniaCheckName}" name="%{idColoniaCheckName}" cssStyle="display:inline-block;" cssClass="cajaTextoM2 w8" disabled="%{readOnly}" value="%{otraColoniaCheck}"></s:checkbox></td>
					<td><s:textfield id="%{nuevaColoniaName}" name="%{nuevaColoniaName}" value="%{nuevaColonia}" cssClass="cajaTextoM2 w200 setNew jQalphaextra" disabled="%{readOnly}" maxlength="60" cssStyle="display:inline-block;" onchange="onChangeNuevaColoniaSin('%{nuevaColoniaName}','%{idColoniaName}','%{idColoniaCheckName}', 'text');"/></td>
				</tr>
			</tbody>
			</table>
		</td>
	</s:if>
  </tr>
  </s:if>
  <s:if test="incluirReferencia == 'true'" >  
  <tr>  	
  	<td>
			<label for="${referenciaName}">Referencia</label>
		</td>
		<td colspan="3">
			<table width="100%">
		<tbody>
			<tr>
				<td><s:textarea id="%{referenciaName}" name="%{referenciaName}" value="%{referencia}" cssClass="cajaTextoM2 w780 h30 setNew jQalphaextra" disabled="%{readOnly}" maxlength="250" cssStyle="display:inline-block;"/></td>
			</tr>
		</tbody>
		</table>
	</td>

  </tr>
  </s:if>
  <s:else>
  <s:hidden id="%{referenciaName}" name="%{referenciaName}" value="" cssClass="cajaTextoM2 w300 setNew jQalphaextra"/>
  </s:else>
  </table>