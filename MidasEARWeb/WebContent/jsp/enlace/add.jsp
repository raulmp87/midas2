<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link href="${pageContext.request.contextPath}/css/enlace.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="addPage" class="main">
		<div id="addContent">
			<div>
				<h1>Enlace Afirme</h1>
				<hr class="hline" />
			</div>
			<div class="content">
				<br />
				<div class="center-wrapper">
					<span class="title">Registra tu <span class="boldie">caso</span></span>
				</div>
				<br />
				<form action="" method="post">
					<s:hidden id="createUser" name="usuario.nombreUsuario"></s:hidden>
					<div class="container_12">
						<div class="grid_10 prefix_1">
							<label for="caseType">Tipo de caso</label>
						</div>
						<div class="grid_10 prefix_1">
							<select name="caseType" id="caseType" tabindex="-1">
								<s:iterator value="caseTypes">
									<option value="<s:property value="id"></s:property>"><s:property value="name"></s:property></option>
								</s:iterator>
							</select>
						</div>
						<div class="grid_10 prefix_1">
							<label for="caseType">Severidad</label>
						</div>
						<div class="grid_10 prefix_1">
							<select name="severity" id="severity">
								<option value="1">Alta</option>
								<option value="2" selected="selected">Normal</option>
								<option value="3">Baja</option>
							</select>
						</div>
						<div class="grid_10 prefix_1">
							<label for="caseType">Descripción</label>
						</div>
						<div class="grid_10 prefix_1">
							<textarea placeholder="Explicanos tu caso" name="caseDescription"
								id="caseDescription"></textarea>
						</div>
						<div class="grid_6 prefix_3">
							<button id="addBtn" onclick="addCase();" type="button">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>