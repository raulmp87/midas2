<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link href="${pageContext.request.contextPath}/css/enlace.css" rel="stylesheet" type="text/css">
</head>
<body>
	<s:hidden id="createUser" name="usuario.nombreUsuario"></s:hidden>
	<div id="setupPage" class="main">
		<div id="setupContent">
			<div>
				<h1>Enlace Afirme</h1>
				<hr class="hline" />
			</div>
			<div>
				<br />
				<div class="center-wrapper">
					<span class="title">Desvío de <span class="boldie">casos</span></span>
				</div>
				<div class="container_12">
					<label for="iniDate">Fecha de salida:</label><br />
					<sj:datepicker id="iniDate" value="%{caseDeviation.deviationIniDate}" />
					<br />
					<label for="endDate">Fecha de regreso:</label><br />
					<sj:datepicker id="endDate" value="%{caseDeviation.deviationEndDate}" />
					<br />
					<label for="deviationUser">Desviar casos a:</label><br />
					<s:select id="deviationUser" list="deviationUsers" listKey="nombreUsuario" listValue="nombreCompleto">
					</s:select>
					<button id="btnAddDeviation" onclick="addDeviation()">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>