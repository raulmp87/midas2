<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link href="${pageContext.request.contextPath}/css/enlace.css" rel="stylesheet" type="text/css">
</head>
<body>
	<s:hidden id="createUser" name="usuario.nombreUsuario"></s:hidden>
	<s:hidden id="caseId" name="caso.id"></s:hidden>
	<div id="tracingPage" class="main">
		<div id="tracingContent">
			<div>
				<h1>Enlace Afirme</h1>
				<hr class="hline" />
			</div>
			<div class="content">
				<!-- Titulo -->
				<br />
				<div class="center-wrapper">
					<span class="title">Seguimiento de tu <span class="boldie">caso</span></span>
				</div>
				<br />
				<div class="panel" id="panel-01">
					<a id="closeCase" onclick="closeCase('<s:property value="caso.id"/>')" href="#" title="Close">Cerrar
								caso</a>
					<s:if test="manager">
						<br />
						<a href="#" id="reasignCase" onclick="openReassignCase('<s:property value="caso.id"/>')"
							title="Reasign">Reasignar caso</a>
					</s:if>
				</div>
				<div id="messageForm" class="container_12">
				
					<div class="grid_8 prefix_2"><!-- TODO titulo -->
						<div class="boldie"></div>
					</div>
					<s:iterator value="messages">
						<div class="grid_8 prefix_2 suffix_2">
							<br />
						</div>
						<div class="grid_10 prefix_3">
							<div>
								<s:if test="%{createUser == 'SISTEMA' || ((createUser == usuario.nombreUsuario) && manager) || ((createUser != usuario.nombreUsuario) && !manager) }">
									<img class="userPhoto" src="${pageContext.request.contextPath}/img/enlace/photoLogo.png" /> 
								</s:if>
								<s:else>
									<img class="userPhoto" src="${pageContext.request.contextPath}/img/enlace/photo.png" /> 
								</s:else>
								<span><s:property value="createUserName"/></span><span class="time"><s:date name="createDate" format="dd/MM/yyyy HH:ss" /></span>
							</div>
						</div>
						<div class="grid_9">
							<div class="message"><s:property value="message"/></div>
						</div>
					</s:iterator>
				</div>
			</div>
			<br />
			<div>
				<div class="container_12">
					<div class="grid_9 prefix_1">
						<input id="newMessage" placeholder="Mensaje" style="width:450px;" />
						<button type="button" id="messageBtn" onclick="addMessage();">Enviar</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>