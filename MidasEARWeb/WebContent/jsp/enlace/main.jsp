<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link href="${pageContext.request.contextPath}/css/enlace.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="mainPage" class="main">
		<div id="mainContent">
			<div>
				<h1>Enlace Afirme</h1>
				<hr class="hline" />
			</div>
			<div>
				<!-- Titulo -->
				<br />
				<div class="center-wrapper">
					<s:if test="manager">
						<span class="title">Casos <span class="boldie">asignados</span></span>
						<br />
						<a href="#" id="reasignCase" onclick="openSetupCase()"
							title="Reasign">Configurar devío</a>
					</s:if>
					<s:else>
						<span class="title">Cuentanos tu <span class="boldie">caso</span></span>
					</s:else>
				</div>
				<br />
				<div class="container_12">
					<s:if test="cases.empty">
						<div class="grid_10 prefix_1">
							<s:if test="manager">
								No tienes casos asignados.
							</s:if>
							<s:else>
							 	No cuentas con ningún caso abierto, ¿Quieres agregar uno?
							</s:else>
						</div>
					</s:if>
					<s:else>
						<s:iterator value="cases">
							<div class="grid_10 prefix_1">
									<div id="div<s:property value="id"></s:property>" class="message" onclick="openCase('<s:property value="id"></s:property>')">
										<span class="theTitle">#<s:property value="id"> </s:property><s:property value="caseTypeDTO.name"></s:property></span> <br />
										<s:if test="manager">
											<div class="theMessage">Reportado por:<s:property value="createUserName"></s:property></div>
										</s:if>
										<s:else>
											<div class="theMessage">Asignado a:<s:property value="assignedUserName"></s:property></div>
										</s:else>
										<div class="theMessage"><s:property value="description"></s:property></div>
									</div>
								</div>
								<div class="grid_12">&nbsp;</div>
						</s:iterator>
					</s:else>
					<s:if test="!manager">
						<div class="grid_6 prefix_3">
							<button type="button" href="#" onclick="openAddCase();">Agregar</button>
						</div>
					</s:if>
				</div>
			</div>
		</div>
	</div>
</body>
</html>