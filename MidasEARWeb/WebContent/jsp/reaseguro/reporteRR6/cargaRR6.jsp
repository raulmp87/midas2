<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	var mostrarPath = '<s:url action="mostrar" namespace="/reaseguro/reporteRR6/carga"/>';
	var obtenerPlantillaPath = '<s:url action="obtenerPlantilla" namespace="/reaseguro/reporteRR6/carga"/>';
	var procesarInfoPath = '<s:url action="procesarinfo" namespace="/reaseguro/reporteRR6/carga"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/reaseguro/rr6/reporteRR6.js'/>"></script>

<style type="text/css">

.subtituloLeft {
    font-size: 10px;
    font-weight: bold;
    line-height: 20px;
    margin-bottom: 6px;
    margin-top: 6px;
    text-align: left;
    width: 5%;
}
</style>

<s:form action="/carga/mostrarRR6" namespace="/reaseguro/reporteRR6" id="cargaRR6Form">

	<s:hidden name="claveNegocio" />
	<s:hidden name="tipoPlantilla" />
	<s:hidden name="idToControlArchivo" />
	<s:hidden name="accion" />
	<s:hidden name="mensaje" id="mensaje" value ="%{mensaje}" />
		
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6"><s:text name="reaseguro.rr6.titulo"/></td>
		</tr>	
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="6"><s:text name="reaseguro.rr6.plantilla" /></td>
		</tr>
		<tr>
		    <td class="subtituloLeft" colspan=""><s:text name="reaseguro.rr6.reporte" /></td>	
			<td class="subtituloLeft" colspan="" style="width: 35%">
     			<select id="tipoArchivo" name="tipoArchivo" Class="cajaTexto" style="width: 300px">
     			    <option value="0"><s:text name="Seleccione el reporte..."/></option>
     				<option value="1">Esquemas de Reaseguro - (RTRE)</option>
     				<option value="2">Contratos Automáticos - (RTRC)</option>
     				<option value="3">Operaciones Facultativas - (RTRF)</option>
     				<option value="4">Reporte de Resultados - (RTRR)</option>
     				<option value="5">Reporte de Siniestros - (RTRS)</option>
     				<option value="6">Reaseguradores no Registrados - (RARN)</option>
     				<option value="7">Reporte Intermediación - (RTRI)</option>
     			</select>
     		</td>
     		
     		<td class="subtituloLeft" colspan=""><s:text name="reaseguro.rr6.contrato" /></td>
     		<td class="subtituloLeft" colspan="" style="width: 35%">
     			<select id="tipoContrato" name="tipoContrato" Class="cajaTexto" style="width: 300px">
     			    <option value="0"><s:text name="Seleccione el contrato..."/></option>
     				<option value="1">Contratos Proporcionales  - VIDA</option>
     				<option value="2">Contratos no Proporcionales - XL</option>
     				<option value="3">Contratos Facultativos  -   VIDA</option>     				
     				<option value="4">Siniestros VIDA</option>
     				<option value="5">Contratos Facultativos  -   DAÑOS</option>
     			</select>
     		</td>  
     		<td class="subtituloLeft" colspan="">
				&nbsp;
			</td>
			<td class="subtituloLeft" colspan="">
				&nbsp;
			</td>   		     		
		</tr>
		<tr>
			<td colspan="2"><b>
			   	 <sj:datepicker key="reaseguro.rr6.fechacorte" name="fechaCorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></b>
			</td>
			
			<td colspan="3">
				<midas:boton onclick="javascript: descargarPlantilla();"  tipo="agregar" key="reaseguro.rr6.descarga" style="width:63%"/>
			</td>  
			
		</tr>
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="6"><s:text name="reaseguro.rr6.cargamasiva" /></td>
		</tr>
		<tr>
			<td colspan="6">
				<midas:boton onclick="javascript: procesarInfo('carga');"  tipo="agregar" key="reaseguro.rr6.procesar" style="width:22.5%"/>
			</td>
		</tr>
	</table>
</s:form>