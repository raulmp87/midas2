<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	var mostrarPath = '<s:url action="mostrar" namespace="/reaseguro/reporteRR4/carga"/>';
	var obtenerPlantillaPath = '<s:url action="obtenerPlantilla" namespace="/reaseguro/reporteRR4/carga"/>';
	var procesarInfoPath = '<s:url action="procesarinfo" namespace="/reaseguro/reporteRR4/carga"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/reaseguro/rr4/reportesRR4.js'/>"></script>

<style type="text/css">

.subtituloLeft {
    font-size: 10px;
    font-weight: bold;
    line-height: 20px;
    margin-bottom: 6px;
    margin-top: 6px;
    text-align: left;
    width: 5%;
}
</style>

<s:form action="/carga/mostrarRR6" namespace="/reaseguro/reporteRR6" id="cargaRR4Form">

	<s:hidden name="claveNegocio" />
	<s:hidden name="tipoPlantilla" />
	<s:hidden name="idToControlArchivo" />
	<s:hidden name="accion" />
	<s:hidden name="mensaje" id="mensaje" value ="%{mensaje}" />
		
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6"><s:text name="reaseguro.rr4.titulo"/></td>
		</tr>	
		<tr>
		    <td class="subtituloLeft" colspan="">
		    	<s:text name="reaseguro.rr6.reporte" />	
     			<select id="tipoArchivo"  name="tipoArchivo" Class="cajaTexto" style="padding-left: 5px; width: 300px">
     			    <option value="0"><s:text name="Seleccione el reporte..."/></option>
     				<option value="1">Carga Esquemas XL</option>
     				<option value="2">Carga Esquemas Prop. Vida</option>
     				<option value="4">Carga Esquemas Fac Vida</option>
     				<option value="3">Carga Reaseguradores Autorizados</option>     				    				
     			</select>
     		</td>			   		     		
		</tr>
		<tr>
			<td colspan="1"><b>
			   	 <sj:datepicker key="reaseguro.rr6.fechacorte" name="fechaCorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker></b>
			</td>
		</tr>
		<tr>
			<td>	
				<midas:boton onclick="javascript: descargarPlantilla();"  tipo="agregar" key="reaseguro.rr6.descarga" style="padding-right: 5px; width: 180px"/>
				<midas:boton onclick="javascript: actualizarCalificacionesRR4();" tipo="agregar" texto="Actualizar Calificacion" style="padding-right: 5px;width: 150px"/>
				<midas:boton onclick="javascript: eliminarContratos();" tipo="agregar" texto="Eliminar Contratos" style="padding-right: 5px; width: 120px"/>
				<midas:boton onclick="javascript: procesarInfo('carga');"  tipo="agregar" key="reaseguro.rr6.procesar" style="padding-right: 5px; width: 150px"/>
			</td>
			
		</tr>
		<tr>
			
		</tr>
	</table>
</s:form>