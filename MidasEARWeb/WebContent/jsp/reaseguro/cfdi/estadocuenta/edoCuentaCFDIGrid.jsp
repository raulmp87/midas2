<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>

		<column id="contrato" type="ro" width="180" sort="str"><s:text name="reaseguro.edocuentacfdi.impresion.contrato"/></column>
		<column id="ramo" type="ro" width="100" sort="str"><s:text name="reaseguro.edocuentacfdi.impresion.ramo"/></column>
		<column id="reasegurador" type="ro" width="300" sort="str"><s:text name="reaseguro.edocuentacfdi.impresion.reasegurador"/></column>
		<column id="iniperiodo" type="ro" width="80" sort="date_custom"><s:text name="reaseguro.edocuentacfdi.impresion.iniperiodo"/></column>
		<column id="finperiodo" type="ro" width="80" sort="date_custom"><s:text name="reaseguro.edocuentacfdi.impresion.finperiodo"/></column>	
		<column id="folio" type="ro" width="45" sort="str"><s:text name="reaseguro.edocuentacfdi.impresion.folio"/></column>
		<column id="xml" type="img" width="40" sort="na"><s:text name="reaseguro.edocuentacfdi.impresion.xml"/></column>
		<column id="pdf" type="img" width="70" sort="na"><s:text name="reaseguro.edocuentacfdi.impresion.pdf"/></column>
	</head>
    
    <% int a=0;%>
    <s:iterator var="estadoCuenta" value="estadosCuenta" status="status">
    	<s:iterator var="folio" value="#estadoCuenta.foliosD" status="status">
	    	<% a+=1; %>
			<row id="<%=a%>">
				<cell><s:property value="#estadoCuenta.contratoHistorico.descripcion" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="#estadoCuenta.subRamo.descripcion" escapeHtml="false" escapeXml="true"/></cell>
				<cell><s:property value="#estadoCuenta.nombreReasCorr" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="#estadoCuenta.fechaInicioPeriodo" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="#estadoCuenta.fechaFinPeriodo" escapeHtml="false" escapeXml="true" /></cell>
				<cell><s:property value="#folio.folio" escapeHtml="false" escapeXml="true" /></cell>
				<cell>../img/xml-small.gif^Imprimir CFDI^javascript: imprimirCFDI(<s:property value="#folio.folio"/>, 0)^_self</cell> 
				<cell>../img/logoPDF_small.png^Imprimir Rep. Impresa^javascript: imprimirCFDI(<s:property value="#folio.folio"/>, 1)^_self</cell>
			</row>
		</s:iterator>
	</s:iterator>
	
</rows>