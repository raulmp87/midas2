<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	var mostrarImpPath = '<s:url action="mostrar" namespace="/reaseguro/edocuentacfdi/impresion"/>';
	var buscarImpPath = '<s:url action="buscar" namespace="/reaseguro/edocuentacfdi/impresion"/>';
	var imprimirPath = '<s:url action="imprimir" namespace="/reaseguro/edocuentacfdi/impresion"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/reaseguro/cfdi/estadocuenta/edocuentacfdi.js'/>"></script>

<s:form action="/impresion/mostrar" namespace="/reaseguro/edocuentacfdi" id="impresionForm">
	
	<s:hidden name="claveNegocio" />
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo"><s:text name="reaseguro.edocuentacfdi.impresion.titulo"/></td>
		</tr>	
	</table>
		
	<table width="98%" id="filtros">
		<tr>
			<td>
				<s:select key="reaseguro.edocuentacfdi.impresion.contrato" 
		 							      name="contrato.id" id="contratoId"
		 							      labelposition="left" 
		 							      headerKey="0" headerValue="%{getText('midas.general.seleccione')}"
		 							      list="contratos" listKey="id" listValue="descripcion" 		   
									      cssClass=" txtfield"/>
			</td>
			<td>
				<s:select key="reaseguro.edocuentacfdi.impresion.ramo" 
		 							      name="estadoCuenta.subRamo.id" id="ramoId"
		 							      labelposition="left" 
		 							      headerKey="0" headerValue="%{getText('midas.general.seleccione')}"
		 							      list="subRamos" listKey="id" listValue="descripcion" 		   
									      cssClass=" txtfield"/>
			</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				<s:select key="reaseguro.edocuentacfdi.impresion.reasegurador" 
		 							      name="estadoCuenta.nombreReasCorr" id="nombreReasCorr"
		 							      labelposition="left" 
		 							      headerKey="0" headerValue="%{getText('midas.general.seleccione')}"
		 							      list="reaseguradores" 		   
									      cssClass=" txtfield"/>
			</td>
			<td colspan="2">
				
			</td>
		</tr>	
		<tr>
			<td>
				<sj:datepicker key="reaseguro.edocuentacfdi.impresion.iniperiodo" name="estadoCuenta.fechaInicioPeriodo" id="fIniPer" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				<sj:datepicker key="reaseguro.edocuentacfdi.impresion.finperiodo" name="estadoCuenta.fechaFinPeriodo" id="fFinPer" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td>
				<s:textfield name="folio" id="folio" 
					key="reaseguro.edocuentacfdi.impresion.folio" cssStyle="cajaTexto"
					labelposition="left" maxlength="8"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="right">
				<midas:boton onclick="javascript: buscarCFDI();"  tipo="buscar" key="midas.boton.buscar"/>
			</td>
		</tr>	
	</table>
	
	<div id ="edoCuentaCFDIGrid" style="width:97%;height:247px"></div>
	
	
</s:form>