<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<script type="text/javascript">
	var mostrarPath = '<s:url action="mostrar" namespace="/reaseguro/edocuentacfdi/generacion"/>';
	var obtenerPlantillaPath = '<s:url action="obtenerPlantilla" namespace="/reaseguro/edocuentacfdi/generacion"/>';
	var procesarInfoPath = '<s:url action="procesarinfo" namespace="/reaseguro/edocuentacfdi/generacion"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/reaseguro/cfdi/estadocuenta/edocuentacfdi.js'/>"></script>

<s:form action="/generacion/mostrar" namespace="/reaseguro/edocuentacfdi" id="generacionForm">
	
	<s:hidden name="claveNegocio" />
	<s:hidden name="tipoPlantilla" />
	<s:hidden name="idToControlArchivo" />
			
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6"><s:text name="reaseguro.edocuentacfdi.generacion.titulo"/></td>
		</tr>	
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="6"><s:text name="reaseguro.edocuentacfdi.generacion.plantillas"/></td>
		</tr>
		<tr>
			<td class="subtituloLeft" colspan="3" width="50%"><s:text name="reaseguro.edocuentacfdi.generacion.ec" /></td>
			<td class="subtituloLeft" colspan="3" width="50%"><s:text name="reaseguro.edocuentacfdi.generacion.cancelaciones" /></td>
		</tr>
		<tr>
			<td colspan="3">
				<s:select key="reaseguro.edocuentacfdi.impresion.contrato" 
		 							      name="contrato.id" id="contratoId"
		 							      labelposition="left" 
		 							      headerKey="0" headerValue="%{getText('midas.general.seleccione')}"
		 							      list="contratos" listKey="id" listValue="descripcion" 		   
									      cssClass=" txtfield"/>
									
			</td>
			<td colspan="3" width="40%">
				<midas:boton onclick="javascript: descargarPlantillaCancelaciones();"  tipo="agregar" key="reaseguro.edocuentacfdi.generacion.cancelaciones.descargar" style="width:70%"/>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<midas:boton onclick="javascript: descargarPlantillaECAbreviada();"  tipo="agregar" key="reaseguro.edocuentacfdi.generacion.abreviada.descargar" style="width:70%"/>
			</td>
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<midas:boton onclick="javascript: descargarPlantillaECEstandar();"  tipo="agregar" key="reaseguro.edocuentacfdi.generacion.estandar.descargar" style="width:70%"/>
			</td>
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="6"><s:text name="reaseguro.edocuentacfdi.generacion.cargamasiva" /></td>
		</tr>
		<tr>
			<td colspan="6">
				<midas:boton onclick="javascript: procesarInfo();"  tipo="agregar" key="reaseguro.edocuentacfdi.generacion.procesar" style="width:30%"/>
			</td>
		</tr>
	</table>
</s:form>