<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">
	var mostrarPath = '<s:url action="mostrar" namespace="/reaseguro/datoscontraparte"/>';
	var obtenerPlantillaPath = '<s:url action="obtenerPlantilla" namespace="/reaseguro/datoscontraparte"/>';
	var procesarInfoPath = '<s:url action="procesarinfo" namespace="/reaseguro/datoscontraparte"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/reaseguro/datosContraParte/importevida.js'/>"></script>

<style type="text/css">

.subtituloLeft {
    font-size: 10px;
    font-weight: bold;
    line-height: 20px;
    margin-bottom: 6px;
    margin-top: 6px;
    text-align: left;
    width: 5%;
}
</style>

<s:form action="/generacion/mostrar" namespace="/reaseguro/datoscontraparte" id="generacionForm">

	<s:hidden name="claveNegocio" />
	<s:hidden name="tipoPlantilla" />
	<s:hidden name="idToControlArchivo" />
	<s:hidden name="accion" />
	<s:hidden name="mensaje" id="mensaje" value ="%{mensaje}" />
		
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6"><s:text name="reaseguro.datoscontraparte.titulo"/></td>
		</tr>	
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="6"><s:text name="reaseguro.datoscontraparte.archivo" /></td>
		</tr>
		<tr>
		<td class="subtituloLeft" colspan=""><s:text name="reaseguro.rr6.reporte" /></td>
		<td class="subtituloLeft" colspan="" style="width: 35%">	
				<select id="tipoArchivo" name="tipoArchivo" Class="cajaTexto" style="width: 300px">
     			    <option value="0">Seleccione...</option>
     				<option value="1">Siniestros Pendientes</option>
     				<option value="2">Pólizas Facultadas</option>
     				<option value="3">Contratos Automaticos</option>
     				<option value="4">Archivo Sise</option>
     				<option value="5">Ajustes Manuales</option>
     			</select>
     		</td>
     	<td class="subtituloLeft" colspan="">&nbsp;</td>
		<td colspan="2">
				<midas:boton onclick="javascript: descargarPlantilla();"  tipo="agregar" key="reaseguro.rr6.descarga" style="width:63%"/>
		</td>
		<td class="subtituloLeft" colspan="">&nbsp;</td>         		
		</tr>
		<tr>
			<td>
			   	 <sj:datepicker key="reaseguro.datoscontraparte.fechacorte" name="fechaCorte" id="fechaCorte" buttonImage="../img/b_calendario.gif"			 
				   changeYear="true" changeMonth="true" maxlength="10" cssClass="cajaTextoM2 w100" 								   								  
				   onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
 				   onblur="esFechaValida(this);"></sj:datepicker>
			</td>
			<td class="subtituloLeft" colspan="">&nbsp;</td>			
			<th><s:text name="Tipo de Cambio"/></th>	
			<td><s:textfield name="tipoCambio" id="tipoCambio" value="0" cssClass="cajaTextoM2 w170" labelposition="left"/></td>
			<td class="subtituloLeft" colspan="">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="6"><s:text name="reaseguro.rr6.cargamasiva" /></td>
		</tr>
		<tr>
			<td colspan="6">
				<midas:boton onclick="javascript: procesarInfo('carga');"  tipo="agregar" key="reaseguro.datoscontraparte.procesar" style="width:30%"/>
			</td>
		</tr>
	</table>
</s:form>