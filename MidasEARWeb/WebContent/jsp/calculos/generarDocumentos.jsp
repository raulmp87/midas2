<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:include value="/jsp/reportesAgentes/reportesHeader.jsp"></s:include>
<s:include value="/jsp/calculos/generarDocumentosHeader.jsp"></s:include>
<script type="text/javascript"
	src="<s:url value='/js/midas2/agentes/reporteAgente.js'/>">	
</script>
<style type="text/css">
   ul { height: 60px; overflow: auto; width: 200px; border: 1px solid; border-color : #B2DBB2;
             list-style-type: none; margin: 0; padding: 0; overflow-x: hidden; }
   li { margin: 0; padding: 0; }   
   li label:hover { background-color: Highlight; color: HighlightText; } 
   
   .wwgrp {
    padding: 4px 5px;
    padding-left: 0px;}
   
</style> 

<div class="row">
	<div class="titulo c5">
		<label class="">Generar y Enviar Documentos</label>
	</div>
</div>
Indicar Par�metros para Generar Documentos
<s:form id="generarDocumentosForm">
	<table class="contenedorFormas">
	    <tr>
	    <th width="80px;"><font color="#FF6600">* </font><s:property value="labelFechaInicio" default="Tipo de Documento:"/>
			</th>
			<td width="250px;">			    
			    <s:select name="tipoDocumento" id="tiposDocs"					      
					      labelposition="left" required="true" 
					      value="tipoDocumento"
					      onchange="habilitarFiltrosCondicionales(); ocultarFormatoSalida();"
					      headerKey="" headerValue="Seleccione ..." 
					      list="tipoDocumentos" listKey="clave" listValue="valor" 					      	   
				          cssClass=" txtfield w200"/>
			</td><td width="80px;"></td>
		   <th width="80px;">
		   		<div id="lbFormatoSalida" style="display:none">
		   			<s:property value="formatoDeSalida" default="Formato de Salida:"/>
		   		</div>
			</th>
			<td><div id="txfFormatoSalida" style="display:none">
					<s:select name="tipoSalidaArchivo" id="formatosal" cssClass="cajaTextoM2 w150" disabled="true"
					list="#{'xls':'Excel','pdf':'PDF'}"/>
				</div>
			</td>
	    </tr>
		<!--<tr id="s_fechas">
			<th> <s:property value="labelFechaInicio" default="Fecha Inicio:"/>
			</th>
			<td width="250px;"><sj:datepicker name="fechaInicial"
					cssStyle="width: 170px;" required="#requiredField"
					buttonImage="../img/b_calendario.gif" id="fechaInicial" 
					maxDate="today" changeMonth="true" changeYear="true" maxlength="10"
					cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
			<td width="80px;"></td>					
			<th width="45px;"><s:property value="labelFechaFin" default="Fecha Fin:"/>
			</th>
			<td width="250px;"><sj:datepicker name="fechaFinal" cssStyle="width: 170px;"
					required="#requiredField" buttonImage="../img/b_calendario.gif" 
					id="fechaFinal" maxDate="today" changeMonth="true"
					changeYear="true" maxlength="10" cssClass="txtfield jQrequired" size="12"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>							
		</tr>
		   -->
		<tr>
		    <th><font color="#FF6600">* </font><s:text name="Mes:"></s:text>
			</th>
			<td width="250px;">
			    <select id="meses" name="mes" class="txtField w200" ></select>			   
			</td>		
			<td width="80px;"></td>	
			<th><font color="#FF6600">* </font><s:text name="A�o:"></s:text>
			</th>
			<td width="250px;">
			    <select id="anios" name="anio" class="txtField w200" ></select>
			</td>			
		</tr>
		<tr>
			<th><s:text name="midas.catalogos.centro.operacion.titulo" />:</th>
			<td width="250px;">
				<ul id="operacionesList" class="w250"
					style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="centroOperacionList"
						var="varListaCentroOperacion" status="stat">
						<li><label for="varListaCentroOperacion[%{#stat.index}].id">
								<input type="checkbox" onchange="" 
								onclick="loadGerenciasByCentroOperacionCascada();hiddenList('operacionesList');"
								name="centroOperacionesSeleccionados[${stat.index}].id"
								id="centroOperacionesSeleccionados${stat.index}"
								value="${varListaCentroOperacion.id}" class="js_checkEnable" />
								${varListaCentroOperacion.descripcion} </label></li>
					</s:iterator>
				</ul>
			</td>
			<td width="80px;">	
			    <a target="_self" href="javascript: selectAllChecks('operacionesList');loadGerenciasByCentroOperacionCascada();hiddenList('operacionesList');">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('operacionesList');loadGerenciasByCentroOperacionCascada();hiddenList('operacionesList');">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>                
			</td>			
			<th><s:text name="midas.catalogos.centro.operacion.gerencias" />:
			</th>
			<td width="250px;">
						<ul id="gerenciaList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
						    <s:iterator value="gerenciasSeleccionadas" var="varListaGerencias" status="stat" >
						           <li>
						                 <label for="varListaGerencias[%{#stat.index}].id">
						                  <input type="checkbox"
						                         onclick="loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');"
						                  		 name="gerenciaLong[${stat.index}]" 
						                  		 id="gerenciasSeleccionadas${stat.index}" 
						                  		 value="${varListaGerencias.id}" 
						                  		 class="js_checkEnable"/>                                                   
						                   ${varListaGerencias.descripcion}
						                 </label>
						          </li>
						    </s:iterator>
						</ul>                     
						<s:iterator value="gerenciasSeleccionadas" status="status">	
								<s:iterator value="listaGerencias" var ="varListaGerencias" status="stat">
									<s:if test="%{#varListaGerencias.id == configuracionBono.listaGerencias[#status.index].gerencia.id}">
										<script type="text/javascript">
											checarChec('gerenciasSeleccionadas${stat.index}');
										</script>
							    	</s:if>
							    </s:iterator>		
						    </s:iterator>	
					</td>
					<td width="80px;">
					    <a target="_self" href="javascript: selectAllChecks('gerenciaList');loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
	                </a>		    
				    <a target="_self" href="javascript: deselectAllChecks('gerenciaList');loadEjecutivoByGerenciaCascada();hiddenList('gerenciaList');">
	                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
	                </a>
					</td>							
		</tr>
		<tr>
			<th><s:text name="midas.fuerzaventa.negocio.ejecutivo"></s:text>:
			</th>
			<td width="250px;">
				<ul id="ejecutivoList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="ejecutivosSeleccionados" var="varListaEjecutivos"
						status="stat">
						<li>
							<label
							for="varListaEjecutivos[%{#stat.index}].id"> 
							    <input
								type="checkbox" name="ejecutivoLong[${stat.index}]"
								onclick="loadPromotoriaByEjecutivoCascada();hiddenList('ejecutivoList');"
								id="ejecutivosSeleccionados${stat.index}"
								value="${varListaEjecutivos.id}" class="js_checkEnable" />
								${varListaEjecutivos.nombreCompleto} </label></li>
					</s:iterator>
				</ul> <s:iterator value="configuracionBono.listaEjecutivos"
					status="status">
					<s:iterator value="ejecutivosSeleccionados" var="varListaEjecutivos"
						status="stat">
						<s:if
							test="%{#varListaEjecutivos.id == configuracionBono.listaEjecutivos[#status.index].ejecutivo.id}">
							<script type="text/javascript">
								checarChec('ejecutivosSeleccionados${stat.index}');
							</script>
						</s:if>
					</s:iterator>
				</s:iterator></td>
				<td width="80px;">
				    <a target="_self" href="javascript: selectAllChecks('ejecutivoList');loadPromotoriaByEjecutivoCascada();hiddenList('ejecutivoList');">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
	                </a>		    
				    <a target="_self" href="javascript: deselectAllChecks('ejecutivoList');loadPromotoriaByEjecutivoCascada();hiddenList('ejecutivoList');">
	                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
	                </a>
				</td>
		<th><s:text name="midas.prestamosAnticipos.promotorias"></s:text>:
			</th>
			<td width="250px;">
				<ul id="promotoriaList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
					<s:iterator value="promotoriasSeleccionadas" var="varListaPromotorias"
						status="stat">
						<li>
							<label
							for="varListaPromotorias[%{#stat.index}].id"> <input
								type="checkbox" name="promotoriaLong[${stat.index}]"
								id="promotoriasSeleccionadas${stat.index}"
								value="${varListaPromotorias.id}" class="js_checkEnable" />
								${varListaPromotorias.descripcion} </label></li>
					</s:iterator>
				</ul> <s:iterator value="configuracionBono.listaPromotorias"
					status="status">
					<s:iterator value="promotoriasSeleccionadas" var="varListaPromotorias"
						status="stat">
						<s:if
							test="%{#varListaPromotorias.id == configuracionBono.listaPromotorias[#status.index].promotoria.id}">
							<script type="text/javascript">
								checarChec('promotoriasSeleccionadas${stat.index}');
							</script>
						</s:if>
					</s:iterator>
				</s:iterator>
			</td>
			<td width="80px;">
			    <a target="_self" href="javascript: selectAllChecks('promotoriaList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('promotoriaList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
	</tr>
		<tr id="s_Agente">
			<th><s:text name="Tipo Promotoria"></s:text>:
			</th>
		<td width="250px;">
			<ul id="tipoPromotoriaList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
				<s:iterator value="tipoPromotoria"
					var="varListaTiposPromotoria" status="stat">
					<li><label for="varListaTiposPromotoria[%{#stat.index}].id">
							<input type="checkbox"
							name="tipoPromotoriasSeleccioadas[${stat.index}].id"
							id="tipoPromotoriasSeleccioadas${stat.index}"
							value="${varListaTiposPromotoria.id}" class="js_checkEnable" />
							${varListaTiposPromotoria.valor} </label></li>
				</s:iterator>
			</ul> <s:iterator value="tipoPromotoria"
				status="status">
				<s:iterator value="listaTiposPromotoria"
					var="varListaTiposPromotoria" status="stat">
					<s:if
						test="%{#varListaTiposPromotoria.id == configuracionBono.listaTiposPromotoria[#status.index].id}">
						<script type="text/javascript">
							checarChec('tipoPromotoriasSeleccioadas${stat.index}');
						</script>
					</s:if>
				</s:iterator>
			</s:iterator></td>
			<td width="80px;">
			    <a target="_self" href="javascript: selectAllChecks('tipoPromotoriaList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('tipoPromotoriaList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
		<th>
		    <s:text name="midas.fuerzaventa.negocio.clasificacionAgente"></s:text>:
		</th>
		<td width="250px;">
			<ul id="tipoAgenteList" class="w250" style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
				<s:iterator value="tipoAgente" var="varListaTiposAgente"
					status="stat">
					<li><label for="varListaTiposAgente[%{#stat.index}].id">
							<input type="checkbox"
							name="tipoAgentesSeleccionados[${stat.index}].id"
							id="tipoAgentesSeleccionados${stat.index}"
							value="${varListaTiposAgente.id}" class="js_checkEnable" />
							${varListaTiposAgente.valor} </label></li>
				</s:iterator>
			</ul> <s:iterator value="tipoAgente"
				status="status">
				<s:iterator value="listaTiposAgente" var="varListaTiposAgente"
					status="stat">
					<s:if
						test="%{#varListaTiposAgente.id == configuracionBono.listaTipoAgentes[#status.index].id}">
						<script type="text/javascript">
											checarChec('tipoAgentesSeleccionados${stat.index}');
						</script>
					</s:if>
				</s:iterator>
			</s:iterator></td>
			<td width="80px;">
			    <a target="_self" href="javascript: selectAllChecks('tipoAgenteList')">
                    <img border="0" src="../img/confirmAll.gif" title="Seleccionar Todos">
                </a>		    
			    <a target="_self" href="javascript: deselectAllChecks('tipoAgenteList')">
                    <img border="0" src="../img/b_limpiar.gif" title="Limpiar Selecci�n">
                </a>
			</td>
	</tr>
	<tr height="100px;">
		<th>
		    <s:text name="midas.fuerzaventa.configBono.agente" />:
		    <s:textfield id="txtIdAgenteBusqueda" name="txtIdAgenteBusqueda"
					onchange="addAgenteProduccion();" style="display:none;" />
		</th>
		<td>
			<div>				
				<input type="checkbox" name="chkTodosAgentes" value="true" id="chkTodosAgentes"/>Todos	
				<div id="b_sAgente" class="btn_back w100">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="pantallaModalBusquedaAgente();"> <s:text
							name="midas.boton.seleccionar" /> </a>
				</div>
			</div>
		</td>
		<td colspan="3" align="right">
			<ul id="listaProduccionAgentes" class="w420 h100"
				style="height: 100px; overflow-y: scroll; overflow-x: hidden;">
			</ul></td>
	</tr>	
</table>
<div id="spacer1" style="height: 10px"></div>
Seleccionar Informaci�n que desea incluir en el Estado de Cuenta Impreso
<table class="contenedorFormas">
    <tr>
        <td>           
			<input type="checkbox" name="chkDetalle" value="true" id="chkDetalle"/>Detalle			
        </td>
    </tr>
    <tr>
        <td>           
		    <input type="checkbox" name="chkSegAfirmeCom" value="true" onchange="activarAreaAfirmeCom()" id="chkSegAfirmeCom"/>Seguros Afirme Comunica 
        </td>
    </tr>
    <tr>
        <td>            
			<input type="checkbox" name="chkGuiaLectura" value="true" id="chkGuiaLectura"/>Guia de Lectura
        </td>
    </tr>
    <tr>
        <td>            
			<input type="checkbox" name="chkMostrarColAdicionales" value="true" id="chkMostrarColAdicionales"/>Mostrar Columnas Adicionales en Resumen
        </td>
    </tr>
    <tr>
        <td>
            <s:textarea key="Seguros Afirme Comunica" cols="100" 
			    			maxlength="500"
			    			name="textoSegAfirmeCom"
							labelposition="left"							
							rows="4"
							disabled="false"
							cssClass="textarea" 									
						    id="textoSegAfirmeCom"						   
						   />
        </td>
    </tr>
    <tr height="40px;">
	    <td colspan="6" align="right" valign="bottom">
	        <div class="btn_back w150">
			<a href="javascript: void(0);"
				id="botonTxt"
				onclick="buscarAgentesParaGeneracionDocs(1);">
				<s:text name="Generar Documento"/>
			</a>
		</div>
	    </td>
	</tr>
</table>
</s:form>
<div id="spacer1" style="height: 10px"></div>
Lista de Documentos Generados
<table style="width: 98%;">
    <tr>
        <td>
            <div id="tablaAgentesDocumentos" class="detalle" style="width: 98%;">			    
			    <div id="indicador"></div>
					<div id="gridAgentesDocumentosPaginado">
						<div id="agentesDocumentosGrid" style="height:300px"></div>
				        <div id="pagingArea"></div><div id="infoArea"></div>
			    </div>
		    </div>
        </td>
    </tr>
    <tr height="40px;">
	    <td colspan="6" align="center" valign="bottom">
	        <div class="btn_back w150">
			<a href="javascript: void(0);"
				onclick="enviarPorCorreo();">
				<s:text name="Enviar Por Correo"/>
			</a>
		</div>
	    </td>
	</tr>
</table>
<script type="text/javascript">
buscarAgentesParaGeneracionDocs(0);
</script>