<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>	
<script src="<s:url value='/js/midas2/calculos/generarDocumentos.js'/>"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<script type="text/javascript">
	var buscarCancelacionesPendientesPath = '<s:url action="listarCancelaciones" namespace="/endoso/previo"/>';
	var buscarCancelacionesPendientesPaginadoPath = '<s:url action="listarCancelacionesPaginado" namespace="/endoso/previo"/>';
	var actualizarBloqueoPath = '<s:url action="actualizarBloqueo" namespace="/endoso/previo"/>';	
	var emitirPendientePath = '<s:url action="emitirPendiente" namespace="/endoso/previo"/>';   
	var excelCancelacionesPendientesPath = '<s:url action="getExcelCancelacionesPendientes" namespace="/endoso/previo"/>';
	var aplicarAccionSeleccionadasPath = '<s:url action="aplicarAccionSeleccionadas" namespace="/endoso/previo"/>';
	var iniciarCancelacionAutomaticaPath = '<s:url action="iniciarCancelacionAutomatica" namespace="/endoso/previo"/>';
	var monitorearCancelacionAutomaticaPath = '<s:url action="monitorearProcesoCancelacionAutomatica" namespace="/endoso/previo"/>';
	var buscarAgentePath = '<s:url action="buscarAgente" namespace="/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente"/>';	
	
	var enviarDocsPath = '<s:url action="enviarDocumentos" namespace="/fuerzaventa/calculos/generarDocumentos"/>';	
	var buscarAgentesParaGenDocsPath = '<s:url action="listarDocumentos" namespace="/fuerzaventa/calculos/generarDocumentos"/>';
	var verDocumentoPath = '<s:url action="verDocumento" namespace="/fuerzaventa/calculos/generarDocumentos"/>';
</script>