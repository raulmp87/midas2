<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>			
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>50</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		<column id="selected" type="ch" width="30" sort="int" >#master_checkbox</column>
		<column id="id" type="ro" width="0px" sort="int" hidden="true">id</column>		
		<column id="numeroAgente" type="ro" width="70" sort="int" align="center"><s:text name="No. Agente" /></column>
		<column id="nombreAgente" type="ro" width="*" sort="int" align="center"><s:text name="Nombre del Agente" /></column>
		<column id="promotoria"  type="ro" width="300" sort="int" align="center"><s:text name="Promotoria" /></column>
		<column id="tipoDocumento" type="ro" width="175" sort="str"><s:text name="Tipo de documento"/></column>		
		<column id="ver" type="img" width="30" align="center"></column>				
	</head>	  		
	<s:iterator value="listaAgentes" status="stats">
		<row id="<s:property value="id"/>">
		    <cell><s:property value="" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>		
			<cell><s:property value="idAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreCompleto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="promotoria" escape="false" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="%{tipoDocumento}" escapeHtml="false" escapeXml="true"/></cell>			
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Ver^javascript: verDocumento(<s:property value="id"/>)^_self</cell>			
		</row>
	</s:iterator>
</rows>