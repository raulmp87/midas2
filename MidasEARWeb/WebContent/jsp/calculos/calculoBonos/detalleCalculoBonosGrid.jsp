<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
		<column id="tipoBeneficiario" type="ro" width="*" sort="int"><s:text name="midas.fuerzaventa.configBono.tipoBeneficiario"/> </column>
		<column id="beneficiario" type="ro" width="*" sort="date"><s:text name="midas.fuerzaventa.configBono.beneficiario"/></column>
		<column id="prima" type="ro" width="*" align="center" sort="int"><s:text name="midas.fuerzaventa.configBono.prima"/></column>
		<column id="descripcionBono" type="ro" width="*" align="right" sort="int"><s:text name="midas.fuerzaventa.configBono.bono"/></column>
		<column id="bonoPagar" type="ro" width="*" align="right" sort="int"><s:text name="midas.fuerzaventa.configBono.bonoPagar"/></column>
		<column id="estatusBono" type="ro" width="*" sort="str"><s:text name="midas.fuerzaventa.configBono.estatus"/></column>
		<s:if test="tipoAccion!=\"consulta\""> 
			<column id="accionRehab" type="img" width="70" sort="na" align="center">Acciones</column>
		</s:if> 
	</head>
	<s:iterator value="detalleCalculoBonoList" var="listCalc" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${listCalc.tipoBeneficiario.valor}]]></cell>
			<cell><![CDATA[${listCalc.beneficiario}]]></cell>
			<cell><fmt:formatNumber value="${listCalc.prima}" type="currency" currencySymbol=""/></cell>
			<cell><![CDATA[${listCalc.descripcionBono}]]></cell>
			<cell><fmt:formatNumber value="${listCalc.bonoPagar}" type="currency" currencySymbol=""/></cell>
			<cell><![CDATA[${listCalc.status.valor}]]></cell>
 			<s:if test="tipoAccion!=\"consulta\"">
 				<s:if test="#listCalc.status.valor==\"CANCELADO\"">
 					<cell><s:url value="/img/icons/ico_agregar.gif"/>^<s:text name="midas.fuerzaventa.bono.detalle.rehabilitar"/>^javascript:rehabilitarDetalleBono(${listCalc.id}, ${index.count})^_self</cell>
 				</s:if>
 				<s:else>
					<cell type="ro" />
				</s:else>
			</s:if>			 
		</row>
	</s:iterator>
</rows>