<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/catalogos/catalogosHeader.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/configuracionBono/configuracionBono.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/catalogos/configuracionBono/calculoBono.js'/>"></script>
 
<script type="text/javascript">
	var verPreviewBonosPath = '<s:url action="verPreviewBonos" namespace="/fuerzaventa/calculos/bonos"/>';
	var verPreviewBonosGridPath = '<s:url action="previewBonosGrid" namespace="/fuerzaventa/calculos/bonos"/>';
	
	var verDetalleCalculoBonosPath = '<s:url action="verDetalleCalculoBonos" namespace="/fuerzaventa/calculos/bonos"/>';
	var verDetalleCalculoBonosGridPath = '<s:url action="detalleCalculoBonosGrid" namespace="/fuerzaventa/calculos/bonos"/>';
	
	var rehabilitarDetalleBonoPath = '<s:url action="rehabilitarDetalleBono" namespace="/fuerzaventa/calculos/bonos"/>';
	
</script>
