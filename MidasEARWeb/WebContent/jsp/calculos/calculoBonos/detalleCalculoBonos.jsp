<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
// 	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=verDetalleCalculoBonosGridPath+'?detalleCalculoBono.calculoBono.id='+dwr.util.getValue("detalleCalculoBono.calculoBono.id");//+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro,"gridDetalleBonos", null,null);//,idField);
 });
</script>

<s:hidden name="detalleCalculoBono.calculoBono.id" id="idCalculo"/>
<s:hidden name="calculoBono.status.valor" />
<s:hidden name="tipoAccion" />
<s:form id="formulario">
<div class="titulo w800"><s:text name="midas.fuerzaventa.configBono.previewCalculoBono"/></div>

<table width="880px" class="contenedorConFormato">
	<tr>
		<th>
			<s:text name="midas.fuerzaventa.configBono.descripcionBono"/>
		</th>
		<td><%-- <s:text name="calculoBono.descripcionBono"/> --%>
			<s:textfield id="descripcion" name="calculoBono.descripcionBono" cssClass="W350" readonly="true"/>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configBono.totalBeneficiario"/>
		</td>
		<td>
			<s:textfield id="totalBeneficiarios" name="calculoBono.totalBeneficiarios" readonly="true"/>
		</td>		
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.tipoBono"/>
		</td>
		<td>
			<s:textfield id="tipoBono" name="calculoBono.tipoBono.valor" readonly="true"/>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configBono.importeTotalBono"/>
		</td>
		<td>
			<s:textfield id="totalBono" name="calculoBono.importeTotal" readonly="true"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.ultimoPago"/>
		</td>
		<td>
			<s:textfield id="ultimoPago" name="calculoBono.ultimoPago" readonly="true"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.fechaCorte"/>
		</td>
		<td>
			<s:textfield id="fechaCorte" name="calculoBono.fechaCorte" readonly="true"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.produccionSobre"/>
		</td>
		<td>
			<s:textfield id="produccionSobre" name="calculoBono.produccionSobre.valor" readonly="true"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.periodoProduccion"/>
		</td>
		<td>
			<div class="elementoInline">
				<s:text name="De:"/> 
			</div>	
			<div class="elementoInline">  		   
				<s:textfield id="periodoProduccion" name="calculoBono.produccionDesdeString" readonly="true"/>
			</div>  			
		</td>
		<td>
			<div class="elementoInline">
				<s:text name="A:"/> 
			</div>	
			<div class="elementoInline">
				<s:textfield id="periodoProduccion" name="calculoBono.produccionHastaString" readonly="true"/>
			</div>	
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.modoAplicacion"/>
		</td>
		<td>
			<s:textfield id="modoAplicacion" name="calculoBono.modoAplicacion.valor" readonly="true"/>
			<s:hidden id ="modoAplicacionId" name="calculoBono.modoAplicacion.id"/>
		</td>
		<td>
			<div class="elementoInline">
				<s:text name="Estatus:"/> 
			</div>	
			<div class="elementoInline">
				<s:textfield id="modoAplicacion" name="calculoBono.status.valor" readonly="true"/>
			</div>
		</td>
	</tr>
</table>
</s:form>
<br>
	<div class="titulo w800"><s:text name="midas.fuerzaventa.configBono.detalleBono"/></div>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="gridDetalleBonos" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>	
	<div align="right" class="w1100 inline" >
		<div class="btn_back w200">
			<a href="javascript: void(0);" class="icon_excel" 
				onclick="javascript:generarDetalleExcel(document.getElementById('idCalculo').value,'reporteRamoSubramo');">
				<s:text name="Detalle Ramo - Subramo"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			</a>
		</div>
	<!-- 
		**********detalle del calculo a nivel cobertura**********
	 -->
		<div class="btn_back w200">
			<a href="javascript: void(0);" class="icon_excel" 
				onclick="javascript:generarDetalleExcel(document.getElementById('idCalculo').value,'reporteNivelcobertura');">
				<s:text name="Detalle Bono Cobertura"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			</a>
		</div>
	<!-- 
		**********fin detalle del calculo a nivel cobertura**********
	 -->
		<div class="btn_back w200">
			<a href="javascript: void(0);" class="icon_excel" 
				onclick="javascript:generarDetalleExcel(document.getElementById('idCalculo').value,'reporteAgrupado');">
				<s:text name="Detalle Bono Recibo"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			</a>
		</div>
		
		<!-- 
		**********inicio detalle del calculo a nivel amis**********
		 -->
		 <s:if test="%{calculoBono.banderaExisteAmis>0}">
			 <div class="btn_back w200">
				<a href="javascript: void(0);" class="icon_excel" 
					onclick="javascript:generarDetalleExcel(document.getElementById('idCalculo').value,'reporteNivelAmis');">
					<s:text name="Detalle Bono Amis"/>
				</a>
			</div>
		</s:if>
		 <!-- 
		 **********fin detalle del calculo a nivel amis**********
		  -->
		<div class="btn_back w110">
			<a href="javascript:verPreviewBonos();" class="icon_regresar" 
				onclick="">
				<s:text name="midas.boton.regresar"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			</a>
		</div>
		<s:if test="calculoBono.status.valor==\"PENDIENTE-RECALCULAR\" && tipoAccion==2" >
			<div class="btn_back w110">
				<a href="javascript:recalcularBono();" class="icon_regresar" 
					onclick="">
					<s:text name="Recalcular"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
				</a>
			</div>
		</s:if>
		<s:else>
			<s:if test="calculoBono.status.valor==\"PENDIENTE POR AUTORIZAR\" || calculoBono.status.valor==\"EN PROCESO DE AUTORIZACION\"  && tipoAccion==2" >	
			<div class="btn_back w110">
				<a href="javascript:autorizarPagoBonos();" class="icon_regresar" 
					onclick="">
					<s:text name="Autorizar"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
				</a>
			</div>
			</s:if>
		</s:else>
		<s:if test="tipoAccion==3" >	
			<div class="btn_back w110">
				<a href="javascript:eliminarCalculoBono();" class="icon_regresar" 
					onclick="">
					<s:text name="Eliminar"/>
				</a>
			</div>
		</s:if>
	</div>