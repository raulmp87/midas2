<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<s:include value="/jsp/calculos/calculoBonos/calculoBonosHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
// 	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=verPreviewBonosGridPath;//+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro,"gridPreviewBonos", null,null);//,idField);
 });
</script>

<s:hidden name="configuracion.id"/>
<s:form id="formulario">
<div class="titulo w800"><s:text name="midas.catalogos.centro.operacion.busqueda"/></div>

<table width="880px" class="contenedorConFormato">
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.descripcionBono"/>
		</td>
		<td>
			<s:select id="idDescripcionBono" name="filtroCalculoBono.descripcionBono" cssClass="cajaTextoM2 w150"
				list="listaDescripcionBono" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="descripcion" listKey="descripcion" />	
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configuracionPagosComisiones.modoEjecucion"/>
		</td>
		<td>
			<s:select name="filtroCalculoBono.modoEjecucion.id" id ="comboModoEjecu" cssClass="cajaTextoM2 w150"
			headerKey="" headerValue="Seleccione.." disabled="#readOnly"
			list="modoEjecucion" listKey="id" onchange="botonEjecutar()" listValue="valor"/>
		</td>		
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configBono.tipoBono"/>
		</td>
		<td>
			<s:select name="filtroCalculoBono.tipoBono.id" id ="comboTipoBono" cssClass="cajaTextoM2 w150"
			headerKey="" headerValue="Seleccione.." disabled="#readOnly"
			list="tipoBonos" listKey="id" listValue="valor"/>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configuracionPagosComisiones.fechaCorteInicio"/>
		</td>
		<td>
			<sj:datepicker name="filtroCalculoBono.fechaCorte" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);">
			</sj:datepicker>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<div align="right" class="w880">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript:listarFiltradoGenerico(verPreviewBonosGridPath,'gridPreviewBonos', jQuery('#formulario'),null);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			</div>
		</td>
	</tr>
</table>
</s:form>
<br>	
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="gridPreviewBonos" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>	
<!-- 	<div align="right" class="w910 inline" > -->
<!-- 		<div class="btn_back w110"> -->
<%-- 			<a href="javascript:operacionGenericaConParams(verDetalleConfiguracionPagosComisionesaPath,4,{'configComisiones.id':${configuracion.id}});" class="icon_regresar"  --%>
<!-- 				onclick=""> -->
<%-- 				<s:text name="midas.boton.regresar"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) --> --%>
<!-- 			</a> -->
<!-- 		</div> -->
<!-- 	</div> -->