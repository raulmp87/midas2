<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 listarFiltradoGenerico(detallePagoComisionesGridPath+"?calculo.id="+dwr.util.getValue("calculo.id"),"gridDetalleComisiones", null,null);//,idField);
 });
</script>
<s:hidden name="moduloOrigen"></s:hidden>
<s:hidden name="calculo.id"/>
<s:hidden name="calculo.claveEstatus.valor"/>
<s:hidden name="configuracion.id"/>
<s:form>
	<table width="95%" class="contenedorConFormato" align="center">
		<tr>
			<td class="titulo" colspan="4" >
				<s:text name="midas.calculos.calculoPagoComisiones"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.calculos.fechaCorte"/>
			</td>
			<td>
				<s:textfield name="calculo.fechaCalculoString" disabled="true"/>
			</td>
			<td>	
				<s:text name="midas.negocio.estatus"/>
			</td>
			<td>
				<s:textfield name="calculo.claveEstatus.valor" cssClass="w200" disabled="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.calculos.importeTotalComisiones"/>
			</td>
			<td>
				<s:textfield name="calculo.importeTruncate" disabled="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.calculos.totalAgentesProcesados"/>
			</td>
			<td>
				<s:textfield name="calculo.numComisionesProcesadas" disabled="true"/>
			</td>
		</tr>
	</table>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="gridDetalleComisiones" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
</s:form>
<div align="right" class="95% inline" style="width: 95%">
	<s:if test="calculo.claveEstatus.valor==\"PENDIENTE-RECALCULAR\"">
		<div class="btn_back w150">
			<a href="javascript:recalcularPagoComisiones();"
				onclick="">
				<s:text name="Recalcular"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			</a>
		</div>
		<div class="btn_back w110">
			<a href="javascript:regrearPreviewComisiones();" class="icon_regresar"
				onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>
	</s:if>
	<s:else>
		<s:if test="calculo.claveEstatus.valor!=\"AUTORIZADO\" && calculo.claveEstatus.valor!=\"APLICADO\" && calculo.claveEstatus.valor!=\"ERROR AL PROCESAR\"">
		<div class="btn_back w150">
			<a href="javascript:autorizarPagoComisiones();"
				onclick="">
				<s:text name="Autorizar"/>
			</a>
		</div>
		</s:if>
		<s:if test="calculo.claveEstatus.valor==\"AUTORIZADO\" || calculo.claveEstatus.valor==\"APLICADO\"">
		<div class="btn_back w150">
			<a href="javascript:compararPagoComisiones();"
				onclick="">
				<s:text name="Comparar"/>
			</a>
		</div>
		</s:if>
		<s:if test="calculo.claveEstatus.valor!=\"ERROR AL PROCESAR\"">
		<div class="btn_back w140">
			<a href="javascript:generarReporteComisiones();" class="icon_excel" 
				onclick="">
				<s:text name="Generar Reporte"/>
			</a>
		</div>
		</s:if>
		<div class="btn_back w110">
			<a href="javascript:regrearPreviewComisiones();" class="icon_regresar"
				onclick="">
				<s:text name="midas.boton.regresar"/>
			</a>
		</div>	
	</s:else>
	</div>