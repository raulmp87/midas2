<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="idSolicitudCheque" type="ro" width="80" sort="str"><s:text name="TR/Cheque"/></column>
        <column id="id" type="ro" width="0" sort="int" hidden="true"></column>
        <column id="claveAgente" type="ro" width="80" sort="int" ><s:text name="No.Agente"/></column>
        <column id="agente" type="ro" width="225" sort="str" ><s:text name="Agente"/></column>
        <column id="importe" type="ro" width="80" sort="int"><s:text name="Importe Total"/></column>
	</head>
	<s:iterator value="comparacionDatosMizar" var="mizar" status="index">
				<row id="${index.count}" >
				<s:if test="#mizar.tieneDiferencia!=0">
					<cell ><![CDATA[TR-${mizar.idSolicitudChequeStr}]]></cell>
					<cell ><![CDATA[${mizar.id}]]></cell>
					<cell ><![CDATA[${mizar.clageAgente}]]></cell>
					<cell ><![CDATA[${mizar.nombreAgente}]]></cell>						
					<cell style="background-color:red"><![CDATA[${mizar.importe}]]></cell>
				</s:if>
				<s:else>
					<cell ><![CDATA[TR-${mizar.idSolicitudChequeStr}]]></cell>
					<cell><![CDATA[${mizar.id}]]></cell>
					<cell ><![CDATA[${mizar.clageAgente}]]></cell>
					<cell ><![CDATA[${mizar.nombreAgente}]]></cell>						
					<cell ><![CDATA[${mizar.importeString}]]></cell>
				</s:else>
				</row>		
	</s:iterator>
</rows>