<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 listarFiltradoGenerico(verDetallesDelCalculoPendientePagoGridPath+"?calculo.id="+dwr.util.getValue("calculo.id"),"gridDetalleComisiones", null,null);//,idField);
 });
</script>
<s:hidden name="calculo.id"/>
<s:hidden name="calculo.claveEstatus.valor"/>
<s:hidden name="configuracion.id"/>
<s:form>
	<table width="95%" class="contenedorConFormato" align="center">
		<tr>
			<td class="titulo" colspan="2" >
				<s:text name="midas.calculos.calculoPagoComisiones"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.calculos.fechaCorte"/>
			</td>
			<td>
				<s:textfield name="calculo.fechaCalculoString" disabled="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.calculos.importeTotalComisiones"/>
			</td>
			<td>
				<s:textfield name="calculo.importeTotalString" disabled="true"/>
			</td>
		</tr>
	</table>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="gridDetalleComisiones" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
</s:form>
<div align="right" class="95% inline" style="width: 95%">
	
	<div class="btn_back w110">
		<a href="javascript: salirDetalleCalculoComisiones();" class="icon_regresar"
			onclick="">
			<s:text name="midas.boton.regresar"/>
		</a>
	</div>

<%-- 	<s:if test="calculo.claveEstatus.valor==\"APLICADO\""> --%>
<!-- 		<div class="btn_back w140"> -->
<!-- 			<a href="javascript:generarLayout();" class="icon_excel"  -->
<!-- 				onclick=""> -->
<%-- 				<s:text name="Generar Layout"/> --%>
<!-- 			</a> -->
<!-- 		</div> -->
<%-- 	</s:if> --%>
</div>