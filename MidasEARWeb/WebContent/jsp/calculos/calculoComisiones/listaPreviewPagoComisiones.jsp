<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
// 	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=previewPagoComisionesGridPath;//+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 listarFiltradoGenerico(urlFiltro+"?filtroCalculoComisiones.configuracionComisiones.id="+dwr.util.getValue("configuracion.id"),"gridPreviewComisiones", null,null);//,idField);
 });
</script>
<s:hidden name="moduloOrigen" value="PREVIEW"></s:hidden> 
<s:hidden name="configuracion.id"/>
<s:form id="formulario">
<div class="titulo w800"><s:text name="midas.catalogos.centro.operacion.busqueda"/></div>

<table width="880px" id="filtrosM2">
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configuracionPagosComisiones.fechaCorteInicio"/>
		</td>
		<td>
			<sj:datepicker name="filtroCalculoComisiones.fechaCalculo" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);">
			</sj:datepicker>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configuracionPagosComisiones.fechaCortefin"/>
		</td>
		<td>
			<sj:datepicker name="filtroCalculoComisiones.fechaCalculoFin" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);">
			</sj:datepicker>
		</td>
		<td>
			<s:text name="midas.fuerzaventa.configuracionPagosComisiones.modoEjecucion"/>
		</td>
		<td>
			<s:select name="filtroCalculoComisiones.configuracionComisiones.modoEjecucion.id" id ="comboModoEjecu" cssClass="cajaTextoM2 w150"
			headerKey="" headerValue="Seleccione.." disabled="#readOnly"
			list="modoEjecucion" listKey="id" onchange="botonEjecutar()" listValue="valor"/>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="midas.fuerzaventa.configuracionPagosComisiones.pagarSinFactura"/>
		</td>
		<td>
			<s:checkbox name="checbox"/>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<div align="right" class="w880">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript:listarFiltradoGenerico(previewPagoComisionesGridPath,'gridPreviewComisiones', document.formulario,null);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			</div>
		</td>
	</tr>
</table>
</s:form>
<br>

	<div class="titulo w800"><s:text name="midas.fuerzaventa.configuracionPagosComisiones.listaPreviewComisiones"/></div>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="gridPreviewComisiones" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>

<div class="w880">
<table style="width:30%;" align="right">
	<tr>
		<td>
			<%-- 	<s:if test="#configuracion.id!=null"> --%>
			<div align="right" class="w910 inline" >
				<div class="btn_back w80">
				<a onclick="javascript:operacionGenerica(verDetalleConfiguracionPagosComisionesaPath,1);" 
				href="javascript: void(0);"><s:text name="Agregar"/></a>
			</div>
			</div>
			<%--	</s:if>--%>					
		</td>
		<td>
			<div class="btn_back w100" style="display: inline; margin-left: 1%; float: right; ">
				<a href="javascript: void(0);" class=""
				onclick="verMonitorDeCalculoComisiones();">
				<s:text name="Monitor"/>
		</a>
	</div>						
		</td>					
	</tr>
</table>
</div>