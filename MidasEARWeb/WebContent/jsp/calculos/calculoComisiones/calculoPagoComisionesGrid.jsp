<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>     
        </beforeInit>
        <afterInit>
        </afterInit>       
        <column id="nombreGerencia" type="ro" width="*" sort="int" ><s:text name="midas.negocio.gerencia"/></column>
        <column id="nombrePromotoria" type="ro" width="*" sort="str"><s:text name="midas.negocio.promotoria"/></column>
        <column id="nombreAgente" type="ro" width="*" sort="str"><s:text name="midas.negocio.agente"/></column>
        <column id="idAgente" type="ro" width="90" align="center" sort="int"><s:text name="midas.calculos.numeroAgente"/></column>
        <column id="importe" type="ro" width="90" align="right" sort="int"><s:text name="midas.fuerzaventa.configBono.importe"/></column>
        <column id="estatus" type="ro" width="90" align="right" sort="int"><s:text name="midas.fuerzaventa.grid.estatus"/></column>
	</head>
	<s:iterator value="listaDetalleCalculos" var="listDetalleCalculo" status="index">
		<row id="${index.count}">
			<cell><![CDATA[${listDetalleCalculo.promotoria.ejecutivo.gerencia.descripcion}]]></cell>
			<cell><![CDATA[${listDetalleCalculo.promotoria.descripcion}]]></cell>
			<cell><![CDATA[${listDetalleCalculo.agente.persona.nombreCompleto}]]></cell>
			<cell><![CDATA[${listDetalleCalculo.agente.idAgente}]]></cell>
			<cell><![CDATA[${listDetalleCalculo.importeTruncate}]]></cell>
			<s:if test="claveEstatus.valor!= null">
				<cell><![CDATA[${listDetalleCalculo.claveEstatus.valor}]]></cell>
			</s:if>
			<s:else>
				<cell>No Generado</cell>
			</s:else>
		</row>
	</s:iterator>
</rows>