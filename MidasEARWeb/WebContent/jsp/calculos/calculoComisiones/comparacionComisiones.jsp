<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<script type="text/javascript">
	var gridMidas;
	var gridMizar;
 jQuery(function(){
	 
	 dwr.engine.beginBatch();
	 gridMidas=listarFiltradoGenerico(comparacionMidasPath+"?calculo.id="+jQuery("#idCalculo").val(),"gridComparacionMidas", null,null);
	 gridMizar=listarFiltradoGenerico(comparacionMizarPath+"?calculo.id="+jQuery("#idCalculo").val(),"gridComparacionMizar", null,null);
	 dwr.engine.endBatch({async:false});
	 //showDifferences(gridMidas,gridMizar);
	 
 });
</script>
<s:form>
	<s:hidden name="calculo.id" id="idCalculo" />
	<div class="titulo" style="width: 95%">
		<s:text name="midas.calculos.comparacion"/>
	</div>
	<table width="95%" class="contenedorConFormato" align="center">
		<tr>
			<td>
				<s:text name="midas.calculos.fechaCorte"/>
			</td>
			<td>
				<s:textfield id="fechaCorte" name="calculo.fechaCalculo"/>
			</td>
			<td>
				<s:text name="midas.calculos.idPreview"/>
			</td>
			<td>
				<s:textfield id="idPreview" name="calculo.id"/>
			</td>	
		</tr>	
	</table>
	<div width="95%" height="500px">
		<div style="width: 50%;float: left">
		<div class="titulo" style="width: 78%;">
			<s:text name="midas.calculos.agentesProcesadosMidas"/>
		</div>
		<div style="clear: both;"></div>
		<div align="center" id="gridComparacionMidas" width="415px" height="160px" style="background-color:white;overflow:hidden;"></div>
		</div>
		<div style="width: 50%;float: left">
		<div class="titulo" style="width: 78%;">
			<s:text name="midas.calculos.agentesProcesadosMizar"/>
		</div>
		<div style="clear: both;"></div>
		<div align="center" id="gridComparacionMizar" width="445px" height="160px" style="background-color:white;overflow:hidden;"></div>
		</div>
	</div>
	<div style="clear: both;"></div>
	<table width="95%" class="contenedorConFormato" align="center">
		<tr>
			<td class="titulo">
				<s:text name="midas.calculos.detalleDiferencias"/>
			</td>
		</tr>
		<tr>
			<td>
				<div id="diferenciaDiv" style="overflow:auto;height:80px;width:500px;" class="contenedorConFormato">
					<!-- Corregir -->
					<s:iterator value="comparacionDatosMidas" var="midas" status="idx">
						<span>${index.count} .-</span>
						<span><b>Agente:</b>${midas.nombreAgente}</span>
						<span><b>->Diferencia:</b>${midas.importe}</span>
						<br/>
					</s:iterator>
				</div>
			</td>
			<td valign="middle">
				<div class="btn_back w150">
					<!-- 
					<a href="javascript:autorizarPagoComisiones();"
						onclick="">
						<s:text name="Corregir Diferencia"/>
					</a>
					 -->
				</div>
			</td>
		</tr>
	</table>
	<div align="right" class="95% inline" style="width: 95%">	
		<div class="btn_back w150">
			<a href="javascript:generarReporteComisiones();"  class="icon_excel" 
				onclick="">
				<s:text name="Generar Reporte"/>
			</a>
		</div>
		<!-- 
		<div class="btn_back w150">
			<a href="javascript:autorizarPagoComisiones();"
				onclick="">
				<s:text name="Solicitar Aplicar Pago"/>mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) 
			</a>
		</div>
		 -->
		<div class="btn_back w140">
			<a href="javascript:salirComparacionDetalleCalculo();" class="icon_regresar"  
				onclick="">
				<s:text name="Regresar"/><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			</a>
		</div>
	</div>
</s:form>