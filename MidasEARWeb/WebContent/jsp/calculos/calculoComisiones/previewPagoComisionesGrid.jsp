<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
		<column id="idCalculo" type="ro" width="*" sort="int">Id Cálculo</column>
		<column id="fechaCorte" type="ro" width="*" sort="date">Fecha de Corte</column>
		<column id="numComisiones" type="ro" width="*" align="center" sort="int"># Comisiones Procesadas</column>
		<column id="importeComisiones" type="ro" width="*" align="right" sort="int">Importe Total de Comisiones</column>
		<column id="estatus" type="ro" width="*" sort="str">Estatus Cálculo</column>
		<s:if test="tipoAccion!=\"consulta\"">
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
<%-- 			<column id="accionEditar" type="img" width="30" sort="na"/> --%>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		</s:if>
	</head>
	<s:iterator value="listaCalculosPreview" var="listCalc" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${listCalc.id}]]></cell>
			<cell><![CDATA[${listCalc.fechaCorteString}]]></cell>						
			<cell><![CDATA[${listCalc.numComisionesProcesadas}]]></cell>
			<cell><![CDATA[${listCalc.importeTotalComisiones}]]></cell>
			<cell><![CDATA[${listCalc.estatusCalculo}]]></cell>
			<s:if test="tipoAccion!=\"consulta\"">
				<s:if test="%{estatusCalculo!='ERROR AL PROCESAR'}">
					<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:verPreview(2,${listCalc.id})^_self</cell>
				</s:if>
				<s:else>
					<cell type="ro" />
				</s:else>
				<s:if test="%{estatusCalculo!='CANCELADO' && estatusCalculo!='ERROR AL PROCESAR'}">
					<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:eliminarPreview(${listCalc.id})^_self</cell>
				</s:if>
				<s:else>
					<cell type="ro" />
				</s:else>
			</s:if>			
		</row>
	</s:iterator>
</rows>