<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <afterInit>
        </afterInit>
        <column id="idSolicitudCheque" type="ro" width="0" sort="int" hidden="true"></column>
        <column id="claveAgente" type="ro" width="80" sort="int" ><s:text name="No.Agente"/></column>
        <column id="agente" type="ro" width="255" sort="str" ><s:text name="Agente"/></column>
        <column id="importe" type="ro" width="80" sort="int"><s:text name="Importe Total"/></column>
	</head>
	<s:iterator value="comparacionDatosMidas" var="midas" status="index">
				<row id="${index.count}" >
				<s:if test="#mizar.tieneDiferencia!=0">
					<cell ><![CDATA[${midas.idSolicitudCheque}]]></cell>
					<cell ><![CDATA[${midas.clageAgente}]]></cell>
					<cell ><![CDATA[${midas.nombreAgente}]]></cell>						
					<cell style="background-color:red"><![CDATA[${midas.importe}]]></cell>
				</s:if>
				<s:else>
					<cell ><![CDATA[${midas.idSolicitudCheque}]]></cell>
					<cell ><![CDATA[${midas.clageAgente}]]></cell>
					<cell ><![CDATA[${midas.nombreAgente}]]></cell>						
					<cell ><![CDATA[${midas.importe}]]></cell>
				</s:else>
				</row>		
	</s:iterator>
</rows>