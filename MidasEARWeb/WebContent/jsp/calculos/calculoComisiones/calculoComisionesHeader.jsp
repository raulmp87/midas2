<%@taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>

<script src="<s:url value='/js/midas2/catalogos/configuracionPagoComisiones/configuracionPagoComisiones.js'/>"></script> 

<script type="text/javascript">

var preViewPagosComisionesaPath = '<s:url action="listadoPreviewComisiones" namespace="/fuerzaventa/configuracionPagoComisiones"/>';
 
var listarPreViewPagocomisionesPath = '<s:url action="listarPreviewPagoComisiones" namespace="/fuerzaventa/configuracionPagoComisiones"/>';

var verDetalleConfiguracionPagosComisionesaPath = '<s:url action="verDetalle" namespace="/fuerzaventa/configuracionPagoComisiones"/>';

var previewPagoComisionesGridPath = '<s:url action="listarPreviewPagoComisiones" namespace="/fuerzaventa/calculos/comisiones"/>';

var verDetallePagoComisionesPath = '<s:url action="verDetalle" namespace="/fuerzaventa/calculos/comisiones"/>';

var detallePagoComisionesGridPath = '<s:url action="verDetalleCalculo" namespace="/fuerzaventa/calculos/comisiones"/>';

var comparacionMidasPath = '<s:url action="gridComparacionMidas" namespace="/fuerzaventa/calculos/comisiones"/>';

var comparacionMizarPath = '<s:url action="gridComparacionMizar" namespace="/fuerzaventa/calculos/comisiones"/>';

/************************Urls para manejo de Pago de Comisiones Pendientes*******************************************************************/
var preViewPagosComisionesPendientesPagoPath='<s:url action="mostrarCalculosPendientesDePago" namespace="/fuerzaventa/calculos/comisiones"/>';

var preViewPagosComisionesPendientesPagoGridPath='<s:url action="listarCalculosPendientesDePagoGrid" namespace="/fuerzaventa/calculos/comisiones"/>';

var mostrarDetallePagoComisionesPendientesPath='<s:url action="mostrarDetallePagoComisionesPendientes" namespace="/fuerzaventa/calculos/comisiones"/>';

var verDetallesDelCalculoPendientePagoGridPath='<s:url action="verDetallesDelCalculoPendientePagoGrid" namespace="/fuerzaventa/calculos/comisiones"/>';

</script>