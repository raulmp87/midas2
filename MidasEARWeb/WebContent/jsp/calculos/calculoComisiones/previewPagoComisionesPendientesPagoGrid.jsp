<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
		<column id="idCalculo" type="ro" width="*" sort="int">Id Cálculo</column>
		<column id="fechaCorte" type="ro" width="*" sort="date">Fecha de Corte</column>
		<column id="numComisiones" type="ro" width="*" align="center" sort="int"># Comisiones Procesadas</column>
		<column id="importeComisiones" type="ro" width="*" align="right" sort="int">Importe Total de Comisiones</column>
		<column id="estatus" type="ro" width="*" sort="str">Estatus Cálculo</column>
		<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
	</head>
	<s:iterator value="listaCalculos" var="calc" status="index">
	<row id="${index.count}">
			<cell><![CDATA[${calc.id}]]></cell>
			<cell><![CDATA[${calc.fechaCalculoString}]]></cell>						
			<cell><![CDATA[${calc.numComisionesProcesadas}]]></cell>
			<cell><![CDATA[${calc.importeTotalString}]]></cell>
			<cell><![CDATA[${calc.claveEstatus.valor}]]></cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:operacionGenericaConParams(mostrarDetallePagoComisionesPendientesPath, 2,{"calculo.id":${calc.id}})^_self</cell>
		</row>
	</s:iterator>
</rows>