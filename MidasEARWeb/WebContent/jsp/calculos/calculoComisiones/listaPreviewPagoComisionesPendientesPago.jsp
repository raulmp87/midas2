<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/calculos/calculoComisiones/calculoComisionesHeader.jsp"></s:include>
<script type="text/javascript">
 jQuery(function(){
	 /*Parametro en la url para saber si se va a mostrar propiedades del grid o no*/
// 	 var tipoAccion='<s:property value="tipoAccion"/>';
	 var urlFiltro=preViewPagosComisionesPendientesPagoGridPath;//+"?tipoAccion="+tipoAccion;
	 var idField='<s:property value="idField"/>';
	 //FIXME Revisar si depende de una configuracion o si va a mostrar todas los calculos independientemente de la configuracion.
	 //listarFiltradoGenerico(urlFiltro+"?filtroCalculoComisiones.configuracionComisiones.id="+dwr.util.getValue("configuracion.id"),"gridPreviewComisiones", null,null);//,idField);
	 listarFiltradoGenerico(urlFiltro,"gridPreviewComisiones", null,null);//,idField);
 });
</script>
<s:hidden name="moduloOrigen" value="PENDIENTES"></s:hidden>
<s:hidden name="configuracion.id"/>
<s:form id="formulario">
<div class="titulo w800"><s:text name="midas.agentes.calculo.comisiones.pendientesPago.titulo"/></div>

<table width="880px" id="filtrosM2">
	<tr>
		<td>
			<s:text name="midas.agentes.calculo.comisiones.pendientesPago.fechaCorteInicio"/>
		</td>
		<td>
			<sj:datepicker name="filtroCalculoComisiones.fechaCalculo" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);">
			</sj:datepicker>
		</td>
		<td>
			<s:text name="midas.agentes.calculo.comisiones.pendientesPago.fechaCorteFin"/>
		</td>
		<td>
			<sj:datepicker name="filtroCalculoComisiones.fechaCalculoFin" disabled="#readOnly"
				buttonImage="../img/b_calendario.gif"
				id="" maxlength="10" cssClass="w100 cajaTextoM2 jQdate-mx" 								   								  
				onkeypress="return soloFecha(this, event, false);"
				onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				onblur="esFechaValida(this);">
			</sj:datepicker>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<div align="right" class="w880">
				<div class="btn_back w110">
					<a href="javascript: void(0);" class="icon_buscar"
						onclick="javascript:listarFiltradoGenerico(previewPagoComisionesGridPath,'gridPreviewComisiones', document.formulario,null);">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>	
			</div>
		</td>
	</tr>
</table>
</s:form>
<br>

	<div class="titulo w800"><s:text name="midas.agentes.calculo.comisiones.pendientesPago.grid.titulo"/></div>
	<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="gridPreviewComisiones" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	<!--  
	<div align="right" class="w910 inline" >
		<div class="btn_back w110">
			<a href="javascript:operacionGenericaConParams(verDetalleConfiguracionPagosComisionesaPath,4,{'configComisiones.id':${configuracion.id}});" class="icon_regresar" 
				onclick="">
				<s:text name="midas.boton.regresar"/>--><!-- mostrarCatalogoGenerico(mostrarContenedorConfiguracionPagocomisionesPath) -->
			<!-- </a>
		</div>
	</div> -->
	