<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script src="<s:url value='/js/midas2/negocio/negocio.js'/>"></script>
<div class="mensaje_encabezado"></div>
<div class="mensaje_contenido">
 <div id="mensajeGlobal" class="mensaje_texto">
  <div id="mensajeImg">
  	<img src='/MidasWeb/img/b_ok.png'>
	<font color="gray" size=4 >		
		<s:text name="midas.componente.success.message"/>
	</font>
    </div>
  </div>
 <div class="btn_back w140"  style="display:inline; float: right; top:40%;">
			   <a href="javascript: void(0);"
				 onclick="javascript: parent.cerrarVentanaProductos();">
				  <s:text
				  name="midas.boton.aceptar" /> </a>
 </div>
 </div>