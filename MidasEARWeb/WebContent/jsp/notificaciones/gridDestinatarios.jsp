<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>        
		<column id="idDestinatarios" type="ro" hidden="true" width="*" sort="int" align="center">IdDestinatarios</column>
		<column id="modoEnvio" type="ro" width="*" sort="int" align="center">ModoEnvio</column>
		<column id="valorDest" type="ro" width="*" sort="int" align="center">ValorDestinatario</column>
		<column id="puesto" type="ro" width="*" sort="int" align="center">Puesto</column>
		<column id="nombre" type="ro" width="*" sort="date" align="center">Nombre</column>
		<column id="correo" type="ro" width="*" sort="int" align="center">Correo</column>
		<column id="accionBorrar" type="ro" width="69" sort="str">Acciones</column>
	</head>
	<s:iterator value="listHijoAgentes" var="hijo" status="index">
		<row id="${index.count}">
			<cell></cell>
			<cell></cell>
			<cell></cell>
			<cell></cell>			
			<cell></cell>
			<cell></cell>
			<cell></cell>
		</row>
	</s:iterator>
</rows>