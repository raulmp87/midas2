<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>    
        </beforeInit>
        <afterInit>
        </afterInit>		
        <column id="id" type="ro" width="50" sort="int">#</column>
		<column id="proceso" type="ro" width="*" sort="string"><s:text name="midas.configuracionNotificaciones.proceso"/> </column>
		<column id="movimiento" type="ro" width="*" sort="string"><s:text name="midas.configuracionNotificaciones.movimiento"/></column>
		<column id="fechaCreacion" type="ro" width="*" align="center" sort="string"><s:text name="midas.negocio.fechaCreacion"/></column>
		<column id="usuario" type="ro" width="*" align="center" sort="string"><s:text name="midas.suscripcion.solicitud.asignar.usuario"/></column>
		
			<column id="accionVer" type="img" width="70" sort="na" align="center">Acciones</column>
			<column id="accionEditar" type="img" width="30" sort="na"/>
			<column id="accionBorrar" type="img" width="30" sort="na"/>
		
	</head>
	<s:iterator value="notificacionesList" var="notList" status="index">
	<row id="${index.count}">
			<cell>${notList.id}</cell>
			<cell><![CDATA[${notList.idProceso.descripcion}]]></cell>
			<cell><![CDATA[${notList.idMovimiento.descripcion}]]></cell>						
			<cell><![CDATA[${notList.fechaCreacionString}]]></cell>
			<cell><![CDATA[${notList.usuario}]]></cell>
			<cell><s:url value="/img/icons/ico_verdetalle.gif"/>^<s:text name="midas.boton.consultar"/>^javascript:verConfigNotificaciones(${notList.id},0)^_self</cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:verConfigNotificaciones(${notList.id},1)^_self</cell>
			<cell><s:url value="/img/icons/ico_eliminar.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:eliminarNotificaciones(${notList.id})^_self</cell>
		</row>
	</s:iterator>
</rows>