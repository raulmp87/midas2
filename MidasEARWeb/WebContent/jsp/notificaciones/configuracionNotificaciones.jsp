<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/agentes/notificaciones.js'/>"></script>
<script src="<s:url value='/js/midas2/agentes/correccionComboIE8.js'/>"></script>
<script type="text/javascript">
var urlFiltro='<s:url action="gridNotificaciones" namespace="/notificaciones"/>';
listarFiltradoGenerico(urlFiltro,"notificacionesGrid", null,null);//,idField);
jQuery(".btnL").css("display","none");
</script>
<s:hidden name="configNotif.id"/>
<s:hidden id="accion"/>
<table width="98%" class="contenedorConFormato" align="center">		
		<tr>
			<td class="titulo" colspan="2" >
				<s:text name="midas.configuracionNotificaciones.titulo"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.configuracionNotificaciones.proceso"/>
			</td>
			<td>
				<s:select id="procesos" name="configNotif.idProceso.id"
				list="procsAgtsList" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
				listValue="descripcion" listKey="id" cssClass="cajaTextoM2 w200"
				onselect="onChangeProceso('movimientos')" onchange="onChangeProceso('movimientos')" />
				<s:hidden id="proceso"/>					
			</td>
			<td>
				<s:text name="midas.configuracionNotificaciones.movimiento"/>
			</td>
			<td>
				<s:select id="movimientos" name="configNotif.idMovimiento.id"
				list="movimientos" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}"
				onselect="onChangeMovimientos()" onchange="onChangeMovimientos()" 
				listValue="descripcion" listKey="id" cssClass="cajaTextoM2 w200 "/>
				<s:hidden id="movimiento"/>
			</td>			
		</tr>
		<tr>
			<td>
				<s:text name="midas.configuracionNotificaciones.modoEnvio"/>
			</td>
			<td>
				<s:select id="modoEnvio" name="configNotif.idTipoCorreoEnvio.id"
					list="modosEnvio" headerKey="%{getText('midas.general.defaultHeaderKey')}" headerValue="%{getText('midas.general.seleccione')}" 
					listValue="valor" listKey="id" cssClass="cajaTextoM2 w200 "/>
			</td>
			<td>
				<s:text name="midas.configuracionNotificaciones.tipoDestinatario"/>
			</td>
			<td class="contenedorConFormato">
				<s:iterator value="tiposDestinatario" status="stat" var="dest" id="dest">
					<s:if test="#dest.valor!=\"OTROS\"">
						<label for="dest[%{#stat.index}].id"> 
		 						 <input type="checkbox" value="${dest.id}" 
		 						 name="${dest.valor}"  
		 						 id="tiposDestinatario${stat.index}"
		 						 class="tipDest"/>                                                    
	 						 ${dest.valor}<br>
	 					</label> 
 					</s:if>
 					<s:else>
 						<label for="dest[%{#stat.index}].id"> 
		 						 <input type="checkbox" value="${dest.id}"
		 						 name="${dest.valor}" 
		 						 id="tiposDestinatario${stat.index}"		 						 		 						  
		 						 onchange='javascript:onChangeOtros();'
		 						 class="checkOtro tipDest"/>                                                    
	 						 ${dest.valor}<br>
	 					</label>
 					</s:else>
				</s:iterator>
			</td>
			<td class="contenedorConFormato">
				<s:iterator value="tiposCorreoDestino" status="stat" var="tip" id="tip">
					<s:if test="#tip.valor!=\"Correo Principal\"">
						<label>						
							<input type="radio" class="tCorreo" value="${tip.id}" name="correoDestino"/>
							${tip.valor}<br>
						</label>
					</s:if>	
					<s:else>
						<label>						
							<input type="radio" class="tCorreo" value="${tip.id}" name="correoDestino" checked="checked"/>
							${tip.valor}<br>
						</label>
					</s:else>				
				</s:iterator>
				<s:hidden id="correoDestino"/>	
			</td>			
		</tr>
</table>
<table style="display:none;" width="98%" class="otros contenedorConFormato" align="center">
		<tr>
			<td>
				<s:text name="midas.configuracionNotificaciones.puesto"/>
			</td>
			<td>
				<s:textfield id="otrosPuesto" name="otrosPuesto" cssClass="w250 attrOtros"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.configuracionNotificaciones.nombre"/>
			</td>
			<td>
				<s:textfield id="otrosNombre" name="otrosNombre" cssClass="w250 attrOtros"/>
			</td>
		</tr>
		<tr>
			<td>
				<s:text name="midas.configuracionNotificaciones.correo"/>
			</td>
			<td>
				<s:textfield id="otrosCorreo" name="otrosCorreo" cssClass="w250 jQemail attrOtros"/>
			</td>
		</tr>
</table>
<div class ="w100 btnH" align="right">
	<div class="btn_back w100">
		<a href="javascript: void(0);"
		onclick="agregarDestinatarios();">
		Agregar
		</a>
	</div>	
</div>
<div id="tablaDinamica"></div>
<table width="98%" class="contenedorConFormato" align="center">
  <tr>
    <td><s:text name="Notas"/></td>
    <td><s:textarea id="notas" name="configNotif.notas" cssClass="textarea" cssStyle="width:800px;height:50px;"/></td>
  </tr>
</table>
<div align="right" class="inline btnH" style="width: 95%">
<div class ="w130" align="right">
	<div class="btn_back w130">
		<a href="javascript: void(0);"
		onclick="limpiarNotificaciones();">
		Limpiar
		</a>
	</div>	
</div>
<div class ="w130" align="right">
	<div class="btn_back w130">
		<a href="javascript: void(0);" class="icon_guardar ." 
		onclick="guardarConfigNotif(0);">
		Guardar
		</a>
	</div>	
</div>
<div class ="w130" align="right">
	<div class="btn_back w130">
		<a href="javascript: void(0);" class="icon_guardar ." 
		onclick="guardarConfigNotif(1);">
		Guardar Y Copiar
		</a>
	</div>	
</div>
<div class ="w130" align="right">
	<div class="btn_back w130">
		<a href="javascript: void(0);" class="icon_buscar"
		onclick="buscarNotificaciones();">
		Buscar
		</a>
	</div>	
</div>
</div>
<div align="right" class="inline btnL" style="width: 95%">
<div class ="w130" align="right">
	<div class="btn_back w130">
		<a href="javascript: void(0);"
		onclick="limpiarNotificaciones();">
		Limpiar
		</a>
	</div>	
</div>
</div>
<div id="divCarga" style="position:absolute;"></div>
	<div align="center" id="notificacionesGrid" width="880px" height="200px" style="background-color:white;overflow:hidden"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>	