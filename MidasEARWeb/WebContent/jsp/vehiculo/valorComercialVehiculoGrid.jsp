<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows> 
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>
            <call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
        </beforeInit>
        <column id="proveedor"          type="ro"  width="80"   sort="int"  hidden="false"> <s:text name="midas.vehiculo.proveedor" /> </column>
        <column id="amis"               type="ro"  width="58"   sort="int"  hidden="false"> <s:text name="midas.vehiculo.amis" /> </column>
      	<column id="descripcion"        type="ro"  width="330"  sort="str"  hidden="false"> <s:text name="midas.vehiculo.descripcion.modelo" /> </column>
		<column id="anioModelo"         type="ro"  width="60"   sort="int"  hidden="false"> <s:text name="midas.vehiculo.anio.modelo" /> </column>
		<column id="valorComercial"     type="ro"  width="*"    format="0.00" sort="str"  hidden="false">	<s:text name="midas.vehiculo.valor.comercial" />  </column>
		<column id="mesValorComercial"  type="ro"  width="*"    sort="str"  hidden="false"> <s:text name="midas.vehiculo.mes.valor.comercial" />  </column>
		<column id="anioValorComercial" type="ro"  width="*"    sort="int"  hidden="false"> <s:text name="midas.vehiculo.anio.valor.comercial" />  </column>
		<column id="fechaRegistro"      type="ro"  width="*"    sort="date" hidden="false" format="%d/%m/%Y %H:%i:%s"> <s:text name="midas.vehiculo.fecha.registro" />  </column>
		<column id="estatus"            type="ro"  width="65"   sort="str"  hidden="false"> <s:text name="midas.vehiculo.estatus" />  </column>
		<column id="desactivar"         type="img"  width="80" align="center">Acciones</column>
		
	</head>
	<s:iterator value="valorComercialVehiculoGrid" status="row">
		<row id="<s:property value="#row.index"/>" >
			<cell><s:property value="proveedorId"         escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveAmis"           escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="modeloVehiculo"      escapeHtml="false" escapeXml="true"/></cell>
			<cell>$<s:property value="valorComercial"      escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:if test="valorComercialMes == 1">Enero</s:if><s:elseif test="valorComercialMes == 2">Febrero</s:elseif><s:elseif test="valorComercialMes == 3">Marzo</s:elseif><s:elseif test="valorComercialMes == 4">Abril</s:elseif><s:elseif test="valorComercialMes == 5">Mayo</s:elseif><s:elseif test="valorComercialMes == 6">Junio</s:elseif><s:elseif test="valorComercialMes == 7">Julio</s:elseif><s:elseif test="valorComercialMes == 8">Agosto</s:elseif><s:elseif test="valorComercialMes == 9">Septiembre</s:elseif><s:elseif test="valorComercialMes == 10">Octubre</s:elseif><s:elseif test="valorComercialMes == 11">Noviembre</s:elseif><s:elseif test="valorComercialMes == 12">Diciembre</s:elseif></cell>
			<cell><s:property value="valorComercialAnio"  escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaRegistro" format="dd/MM/yyyy HH:MM:ss" /> </cell>
			<cell><s:if test="estatusRegistro == 1">Activo</s:if><s:else >Inactivo</s:else></cell>
			<cell><s:if test="rolUsuario"><s:if test="estatusRegistro == 1" >../img/b_borrar.gif^Desactivar^javascript:desactivarRegistro(<s:property value="id" />);^_self</s:if><s:else>../img/ico_red2.gif^Registro Desactivado^javascript:registroDesactivado(1)^_self</s:else></s:if><s:else>../img/notAvailable.gif^No tiene permisos para esta funcionalidad^javascript:registroDesactivado(2)^_self</s:else></cell>
		</row>
	</s:iterator>
</rows>