<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:hidden name="logErrors" id="logErrors" />
<s:hidden name="idToControlArchivo" id="idToControlArchivo" />
<s:hidden name="esMasiva" id="logErrors" />

<s:form name="busqueda" id="busquedaServicio" method="post" action="busquedaServicio" >

	<table  width="99%" >
		<tr>
			<td class="titulo" colspan="2">
				<s:text name="midas.siniestros.catalogo.pieza.seccionAutomovil" />
			</td>
		</tr>
		<tr>
			<td width="80%" >
				<table id="filtros" width="99%">
					<tr>
						<th> <s:text name="midas.vehiculo.numeroamis" />: </th>
						<td> <s:textfield 
										name="valorComercialVehiculo.claveAmis" 
										id="numeroAmis" 
										cssClass="cajaTextoM2 w120" 
										maxlength="10"
										onkeypress="return soloNumeros(this, event, false)" />  
						</td>
						
						<th> <s:text name="midas.vehiculo.descripcion.modelo" />: </th>
						<td> <s:textfield 
										name="valorComercialVehiculo.descripcionVehiculo" 
										id="descripcionVehiculo" 
										maxlength="60"
										cssClass="cajaTextoM2 w120" />  
						</td>
						<th> &nbsp; </th>
						<td> &nbsp; </td>
												
					</tr>
					<tr>
						<th> <s:text name="midas.vehiculo.anio.valor.comercial" />: </th>
						<td> <s:select 
								list="ctgAnios" 
								headerKey="0" 
								headerValue="%{getText('midas.general.seleccione')}" 
								name="valorComercialVehiculo.valorComercialAnio" 
								id="anio" 
								cssClass="cajaTextoM2 w120" 
								onchange=""/> </td>
										
						<th> <s:text name="midas.vehiculo.mes.valor.comercial" />: </th>
						<td> <s:select 
								list="#{1:'Enero',2:'Febrero',3:'Marzo',4:'Abril',5:'Mayo','6':'Junio','7':'Julio','8':'Agosto','9':'Septiembre','10':'Octubre','11':'Noviembre','12':'Diciembre'}" 
								headerKey="0" 
								headerValue="%{getText('midas.general.seleccione')}" 
								name="valorComercialVehiculo.valorComercialMes" 
								id="meses" 
								cssClass="cajaTextoM2 w120" 
								onchange=""/> </td>
						
						<th> <s:text name="midas.vehiculo.mes.valor.modelo" />: </th>
						<td> <s:select 
								list="ctgModelos" 
								headerKey="0" 
								headerValue="%{getText('midas.general.seleccione')}" 
								name="valorComercialVehiculo.modeloVehiculo" 
								id="modelo" 
								cssClass="cajaTextoM2 w120" 
								onchange=""/> </td>
						
					</tr>
				</table>
			</td>
			<td  width="20%" >
				<table id="filtros">
					<tr>
						<th colspan="2"> <s:text name="midas.vehiculo.por.fecha" />: </th>
					</tr>
					<tr>
						<td> <s:radio name="valorComercialVehiculo.tipoBusquedaFecha" list="#{'1':'Reciente','2':'Todas'}" value="1" /> </td>
					</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
						<tr>
							<td  class= "guardar">
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="anexarArchivoAdjunto();" >
									 <s:text name="midas.vehiculo.carga.sumas.aseguradas" /> </a>
								</div>
								<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
									 <a href="javascript: void(0);" onclick="busquedaDatos();" >
									 <s:text name="midas.boton.buscar" /> </a>
								</div>
							</td>							
						</tr>
					</table>
			</td>
		</tr>
	</table>	
	
	<br>
	<div class="titulo">
		<s:text name="midas.vehiculo.lista.sumas.aseguradas" />
	</div>
</s:form>

<div id="indicador"></div>
<div id="valorComercialGrid" style="width: 98%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
<br>


<table id="exportar" style="padding: 0px; width: 98%; margin: 0px; border: none; display:none;">
	<tr>
		<td>
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcel();">
					<s:text name="midas.boton.exportarExcel" />
					&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar Excel' title='Exportar Excel' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
		</td>
	</tr>
</table>	


<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/vehiculo/valorComercialVehiculo.js'/>" ></script>

<script type="text/javascript">
	descargarLogCargaMasiva();
</script>

<s:if test="esMasiva">
	<script type="text/javascript">
		cargaPantallaPrincipal();
	</script>
</s:if>


<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>