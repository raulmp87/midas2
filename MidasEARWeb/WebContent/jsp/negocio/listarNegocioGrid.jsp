<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>
	<head>
        <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>            
			<call command="enablePaging">
				<param>true</param>
				<param>13</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>			 			
        </beforeInit>
        <column id="id" type="ro" width="100" sort="int" hidden="false" align="center" ><s:text name="midas.negocio.no" /></column>
		<column id="descripcionNegocio" type="ro" width="200" sort="str" hidden="false"><s:text name="midas.negocio.nombrenegocio" /></column>
		<column id="fechaCreacion" type="ro" width="170" sort="date_custom" hidden="false" align="center" ><s:text name="midas.negocio.fechaCreacion" /></column>
		<column id="codigoUsuarioActivacion" type="ro" width="170" sort="str" hidden="false" align="center" ><s:text name="midas.negocio.usuarioActivacion" /></column>
		<column id="fechaActivacion" type="ro" width="170" sort="date_custom" hidden="false" align="center" ><s:text name="midas.negocio.fechaActivacion" /></column>
		<column id="fechaFinVigencia" type="ro" width="170" sort="date_custom" hidden="false" align="center" ><s:text name="midas.negocio.fechaVencimiento" /></column>
		<column id="claveEstatus" type="co" width="150" sort="int"><s:text name="midas.negocio.estatus" />
			<option value="0">Negociando</option>
			<option value="1">Vigente</option>
			<option value="2">Descontinuado</option>
			<option value="3">Borrado</option>
		</column>
		<column id="accion" type="img" width="70" sort="na" align="center"><s:text name="midas.general.acciones"/></column>
		<column id="accionEliminar" type="img" width="30px" sort="na" align="center" />
		<column id="accionImprimir" type="img" width="30px" sort="na" align="center" />		
	</head>
	<% int a=0;%>
	<s:iterator value="negocioList" var="negocio">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegocio" escapeHtml="false"/></cell>
			<cell><![CDATA[<s:property value="descripcionNegocio"/>]]></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false"/></cell>
			<cell><s:property value="codigoUsuarioActivacion" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="fechaActivacion" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="fechaFinVigencia" escapeHtml="true" escapeXml="true"/></cell>
			<cell><s:property value="claveEstatus" escapeHtml="true" escapeXml="true"/></cell>
			<cell>../img/icons/ico_editar.gif^Editar^javascript:  TipoAccionDTO.getVer(verDetalleNegocio)^_self</cell>
			<s:if test="claveEstatus == 0">			
				<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarNegocio(<s:property value="idToNegocio"/>,<s:property value="claveEstatus"/>)^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/blank.gif</cell>
			</s:else>
			<s:if test="claveEstatus == 1">
				<cell>../img/b_printer.gif^Imprimir^javascript: imprimirNegocio(<s:property value="idToNegocio" escapeHtml="false"/>)^_self</cell>	
			</s:if>		
		</row>
	</s:iterator>
</rows>