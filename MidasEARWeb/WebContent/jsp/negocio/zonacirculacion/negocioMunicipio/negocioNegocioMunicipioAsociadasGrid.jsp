<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="negocioMunicipio.id" type="ro"  width="*"  sort="int" hidden="true">negocioMunicipio.id</column>
		<column id="negocioMunicipio.municipioDTO.municipalityId" type="ro"  width="*"  sort="int" hidden="true">negocioMunicipio.municipioDTO.municipalityId</column>
		<column id="negocioMunicipio.municipioDTO.municipalityName" type="ro"  width="*"  sort="str" >Ciudad</column>
	</head>
	
	<% int a=0;%>
	<s:iterator value="relacionesNegocioMunicipio.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
		    <cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
		    <cell><s:property value="municipioDTO.municipalityId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="municipioDTO.municipalityName" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>