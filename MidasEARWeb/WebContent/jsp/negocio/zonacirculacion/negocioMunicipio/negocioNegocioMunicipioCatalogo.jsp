    <%@taglib prefix="s" uri="/struts-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
   <script src="<s:url value='/js/midas2/negocio/negocio.js'/>"></script>
   <script src="<s:url value='/js/midas2/negocio/zonacirculacion/negocioMunicipio/negocioMunicipio.js'/>"></script>
<script type="text/javascript">
	var idNegocio=	'<s:property value="idToNegocio"/>';
	var tipoAccion = '<s:property value="tipoAccion"/>';
	var claveNegocio = '<s:property value="claveNegocio"/>';
	var mensaje = '<s:property value="mensaje" default="-1"/>';
	var tipoMensaje = '<s:property value="tipoMensaje" default="-1"/>';
	var nivelActivo = '<s:property value="nivelActivo"/>';
	function guardarMunicipios(node){
		parent.mostrarMensajeInformativo(node.firstChild.data,node.getAttribute("tipo")); // Details
	}
</script>
<s:form action="listar">
<div>
<s:hidden id="id" name="id"/>
	<center>
		<table id="desplegarDetalle" border="0">
	<tr>
		<td colspan="3">
			<s:select key="midas.general.zona" name="idEstado" id="idEstado" 
					  list="listados"
					  labelposition="left"
					  onchange="getEstadosPorNegocio($('idEstado'));" 
					  headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
		</td>
	</tr>		 
	<tr>
		  <td>
		  <s:text name="midas.negocio.negocio.zona.catalogo"/>
		  <br><br>
		  </td>
		  </tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.producto.negocio.municipio.al.negocio"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioZonaMunicipioAsociadasGrid" class="dataGridConfigurationClass" style="width:400px; height:100px;"></div>
					<div style="margin-left: 334px">
							<div id="b_guardar" style="100px;">
								<a href="javascript: void(0);"
									onclick="javascript: negocioZonaMunicipioProcessor.sendData()"> <s:text
										name="midas.boton.guardar" /> </a>
							</div>
						</div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.producto.municipio.disponibles.para.negocio"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioZonaMunicipioDisponiblesGrid" class="dataGridConfigurationClass" style="width:400px; height:100px;"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>
</div>	
</s:form>

<script type="text/javascript">
iniciaGridsZonasMunicipio();
</script>