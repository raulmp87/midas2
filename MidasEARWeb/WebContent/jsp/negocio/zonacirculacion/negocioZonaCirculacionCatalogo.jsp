    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
	<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxaccordion_dhx_blue.css'/>" rel="stylesheet" type="text/css">
	<sj:head/>
	<s:include value="/jsp/negocio/zonacirculacion/negocioZonaCirculacionHeader.jsp"></s:include>
<s:form action="listar">
<div>
<s:hidden id="id" name="id"/>
	<center>
		<table id="desplegarDetalle" border="0"> 
		 <tr>
		  <td>
		  <s:text name="midas.negocio.negocio.zona.catalogo"/>
		  <br><br>
		  </td>
		  </tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.producto.negocio.zona.al.negocio"/>
				</td>
				
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioZonaAsociadasGrid" class="dataGridConfigurationClass" style="width:400px; height:100px;"></div>
					<div style="margin-left: 334px">
							<div id="b_guardar" style="100px;">
								<a href="javascript: void(0);"
									onclick="javascript: negocioZonaProcessor.sendData()"> <s:text
										name="midas.boton.guardar" /> </a>
							</div>
						</div>
				</td>
				</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.producto.zona.disponibles.para.negocio"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioZonaDisponiblesGrid" class="dataGridConfigurationClass" style="width:400px; height:100px;"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>
</div>	
</s:form>
<script type="text/javascript">
iniciaGridsZonas();
</script>