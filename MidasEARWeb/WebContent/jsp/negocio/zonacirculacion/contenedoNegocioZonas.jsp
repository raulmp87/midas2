<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/negocio/negocioHeader.jsp"></s:include>
<script type="text/javascript">
	var idNegocio=	'<s:property value="id"/>';
	var tipoAccion = '<s:property value="tipoAccion"/>';
	var claveNegocio = '<s:property value="claveNegocio"/>';
	var mensaje = '<s:property value="mensaje" default="-1"/>';
	var tipoMensaje = '<s:property value="tipoMensaje" default="-1"/>';
	var nivelActivo = '<s:property value="nivelActivo"/>';
	function guardarZonasC(node){
		mostrarMensajeInformativo(node.firstChild.data,node.getAttribute("tipo")); // Details
		desplegaZonas(idNegocio,tipoAccion,claveNegocio,"","",nivelActivo);
	}
</script>

<s:hidden name="id"/>
<div align="left" class="subtitulo" id="labelValidaEstados" >
</div>
<center>
<div id="accordionZonasNegocio" style="position: relative; height:99%; width: 97%;"></div>
</center>
<script type="text/javascript">
inicializarZonasAccordion();
</script>