<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="negocioEstado.id" type="ro"  width="*"  sort="int" hidden="true">estadoDTO.stateId</column>
		<column id="negocioEstado.estadoDTO.stateId" type="ro"  width="*"  sort="int" hidden="true">"negocioestado.estadoDTO"</column>
		<column id="negocioEstado.estadoDTO.stateName" type="ro"  width="*"  sort="str" >Estado</column>
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioZonaCirculacion.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
		    <cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
		    <cell><s:property value="estadoDTO.stateId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estadoDTO.stateName" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>