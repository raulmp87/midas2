<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/negocio/tipopoliza/negocioTipoPolizaHeader.jsp"></s:include>

<s:form action="">
<s:hidden id="idToNegProducto" name="idToNegProducto"/>

	<center>
		<table id="desplegarDetalle" border="0">
		<tr>
		 <td><s:text name="midas.negocio.producto.tipoPoliza.productonegocio"/>
		<b> <s:property value="negocioProducto.productoDTO.nombreComercial" escapeHtml="false" /></b> <s:text name="midas.negocio.producto.tipoPoliza.version"/> <b><s:property value="negocioProducto.productoDTO.version" escapeHtml="false" /></b>
		 </td>
		 </tr>
		 
		 <tr>
		  <td>
		  <s:text name="midas.negocio.producto.tipoPoliza.catalogo"/>
		  <br><br>
		  </td>
		  </tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.producto.tipoPoliza.asociadas.al.negocio"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioTipoPolizaAsociadasGrid" class="dataGridConfigurationClass" style="width:70%;height:220px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.producto.tipoPoliza.disponibles.para.negocio"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioTipoPolizaDisponiblesGrid" class="dataGridConfigurationClass" style="width:70%;height:220px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>

</s:form>

<script type="text/javascript">
	 iniciaGridsNegocioTipoPoliza();
</script>