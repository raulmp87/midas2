<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioTipoPoliza.idToNegTipoPoliza" type="ro" width="0" sort="int" hidden="true">negocioTiposPolizaId</column>
		<column id="negocioTipoPoliza.tipoPolizaDTO.idToTipoPoliza" type="ro" width="0" sort="int" hidden="true">IdToTipoPoliza</column>
		<column id="negocioTipoPoliza.negocioProducto.idToNegProducto" type="ro" width="*" sort="str" hidden="true">IdToNegProducto</column>
		<column id="descripcion" type="ro" width="*" sort="str"><s:text name="midas.negocio.producto.tipoPoliza.descripcion"/>
		</column>
	</head>

	<% int a=0;%>
	<s:iterator value="relacionesNegocioTipoPolizaDTO.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegTipoPoliza" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoPolizaDTO.idToTipoPoliza" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocioProducto.idToNegProducto" escapeHtml="false"  escapeXml="true"/></cell>
			<cell><s:property value="tipoPolizaDTO.descripcion" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>