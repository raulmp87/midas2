<%@taglib prefix="s" uri="/struts-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
	
	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
	<script src="<s:url value='/js/midas2/negocio/deducibles/negociodeducibles.js'/>"></script>
	
	<script type="text/javascript">
		var mostrarDeduciblesCobertura = '<s:url action="mostrarDeduciblesCobertura" namespace="/negocio/deducibles"/>';
		var obtenerDeduciblesCobertura = '<s:url action="obtenerDeduciblesCobertura" namespace="/negocio/deducibles"/>';
		var accionSobreDeduciblesCobertura = '<s:url action="accionSobreDeduciblesCobertura" namespace="/negocio/deducibles"/>';			
	</script>