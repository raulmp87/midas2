<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
   
   <s:set id="readEditOnly" value="true" />
   <s:set id="readEditOnlyCheck" value="true" />
   <s:set id="readEditOnlyCheckMontoUDI" value="true" />
   <s:set id="readEditOnlyCheckPorcentajeUDI" value="true" />
   <s:set id="readEditOnlyCheckMonto" value="true" />
   <s:set id="readEditOnlyCheckPorcentaje" value="true" />
   <s:set id="readEditOnlyBonoPorcentaje" value="true" />
   <s:set id="readEditOnlyBonoMonto" value="true" />
    
    
	<s:if test="negocioBonoComision.negocio.claveEstatus == 1">
	   <s:set id="readEditOnlyboxCheck" value="true" />
	    <s:set id="readEditOnlyBono" value="true" />
	    <s:set id="readEditOnlyBonoComision" value="true" />
	    <s:set id="readEditOnlyUDI" value="true" />
	     <s:set id="readEditOnlyclaveSobreComision" value="true" />
	</s:if>
	<s:else>
 	  <s:set id="readEditOnlyCheck" value="false" />
    </s:else>   
    
 	<s:if test="negocioBonoComision.negocio.claveEstatus != 1">
 	  <s:if test="negocioBonoComision.claveBono==true">
	    <s:set id="readEditOnlyBono" value="false" />
	 </s:if>
	 <s:else>
 	  <s:set id="readEditOnlyBono" value="true" />
    </s:else>
	</s:if>
	
    
   	<s:if test="negocioBonoComision.negocio.claveEstatus != 1">
 	  <s:if test="negocioBonoComision.claveDerechos==true">
	    <s:set id="readEditOnlyBonoComision" value="false" />
	 </s:if>
	 	<s:else>
 	  <s:set id="readEditOnlyBonoComision" value="true" />
    </s:else> 
	</s:if>

    
     <s:if test="negocioBonoComision.negocio.claveEstatus != 1">
 	  <s:if test="negocioBonoComision.claveUDI==true">
	    <s:set id="readEditOnlyUDI" value="false" />
	 </s:if>
	 	<s:else>
 	  <s:set id="readEditOnlyUDI" value="true" />
    </s:else> 
	</s:if>

    
    
    <s:if test="negocioBonoComision.negocio.claveEstatus != 1">
 	  <s:if test="negocioBonoComision.claveSobreComision==true">
	    <s:set id="readEditOnlyclaveSobreComision" value="false" />
	 </s:if>
	 	<s:else>
 	  <s:set id="readEditOnlyclaveSobreComision" value="true" />
    </s:else>   
	</s:if>
	
	
	
	<s:if test="negocioBonoComision.negocio.claveEstatus != 1">
 	  <s:if test="negocioBonoComision.claveSobreComision==true">
	    <s:set id="readEditOnlyclaveSobreComision" value="false" />
	 </s:if>
	 	<s:else>
 	  <s:set id="readEditOnlyclaveSobreComision" value="true" />
    </s:else>   
	</s:if>
	
	
	<s:if test="negocioBonoComision.negocio.claveEstatus != 1">
	 <s:if test="negocioBonoComision.claveDerechos==true">
 	   <s:if test="negocioBonoComision.clavePorcentajeImporteDerechos==true">
	    <s:set id="readEditOnlyCheckPorcentaje" value="false" />
	    <s:set id="readEditOnlyCheckMonto" value="true" />
	 </s:if>
	 	<s:else>
	    <s:set id="readEditOnlyCheckPorcentaje" value="true" />
	    <s:set id="readEditOnlyCheckMonto" value="false" />
    </s:else> 
    </s:if>
   </s:if>
   
	<s:if test="negocioBonoComision.negocio.claveEstatus != 1">
	<s:if test="negocioBonoComision.claveUDI==true">
 	  <s:if test="negocioBonoComision.clavePorcentajeImporteUDI==true">
	    <s:set id="readEditOnlyCheckPorcentajeUDI" value="false" />
	    <s:set id="readEditOnlyCheckMontoUDI" value="true" />
	 </s:if>
	 	<s:else>
	    <s:set id="readEditOnlyCheckPorcentajeUDI" value="true" />
	    <s:set id="readEditOnlyCheckMontoUDI" value="false" />
    </s:else>   
	</s:if>		
 </s:if>
		
 <s:if test="negocioBonoComision.negocio.claveEstatus != 1">
  <s:if test="negocioBonoComision.claveBono==true">	
 	  <s:if test="negocioBonoComision.clavePorcentajeImporteBono==true">
	    <s:set id="readEditOnlyBonoMonto" value="true" />
	 </s:if> 
	 <s:else>
	  <s:set id="readEditOnlyBonoPorcentaje" value="true" />
 	  <s:set id="readEditOnlyBonoMonto" value="false" />
    </s:else>
	</s:if>
  </s:if>
<script type="text/javascript">
function guardarBonosComision() {


	var url = '/MidasWeb/negocio/bonocomision/guardar.action';
	sendRequestJQ(document.negocioBonoComisionForm,url,"contenido_bonos",'validaCampos();');
	

}
	
function validaCampos(){

}
function habilitarBonoComision(value){

//alert(value);
if(dwr.util.getValue("estatus")!=1){
	if(dwr.util.getValue("claveSobreComision")==true && value==1){
		jQuery("#pctSobreComision")           .attr("disabled","");
		jQuery("#claveIncluyeIVASobreomision").attr("disabled","");
		jQuery("#pctAgenteSobreComision")     .attr("disabled","");
		jQuery("#pctPromotorSobreComision")   .attr("disabled","");
		jQuery("#comentariosSobreComision")   .attr("disabled","");
		
	}
	 if(dwr.util.getValue("claveSobreComision")==false  && value==1){
		jQuery("#pctSobreComision")           .attr("disabled","disabled");
		jQuery("#claveIncluyeIVASobreomision").attr("disabled","disabled");
		jQuery("#pctAgenteSobreComision")     .attr("disabled","disabled");
		jQuery("#pctPromotorSobreComision")   .attr("disabled","disabled");
		jQuery("#comentariosSobreComision")   .attr("disabled","disabled");
		//jQuery("input[name$='negocioBonoComision.claveIncluyeIVASobreomision']").removeAttr("checked");		
	}


	 if(dwr.util.getValue("claveDerechos")==true && value==3){
	 	//alert("claveDerechos - true");
	  if (jQuery('#monto_1').val().length == 0 && jQuery('#porcentaje_1').val().length==0){
		  	jQuery('input[name="negocioBonoComision.clavePorcentajeImporteDerechos"]')[0].checked = true;
		  	jQuery("#porcentaje_1")         .attr("disabled","");
		 
		}
	  if (jQuery('#monto_1').val().length > 0 && jQuery('#porcentaje_1').val().length==0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteDerechos"]')[1].checked = true;
		  	jQuery("#monto_1")         .attr("disabled","");
		   
	  }
	  if (jQuery('#monto_1').val().length == 0 && jQuery('#porcentaje_1').val().length>0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteDerechos"]')[0].checked = true;
		  	jQuery("#porcentaje_1")         .attr("disabled","");
		  
	  }
	  
	  if (jQuery('#monto_1').val().length > 0 && jQuery('#porcentaje_1').val().length>0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteDerechos"]')[0].checked = true;
		  	jQuery("#porcentaje_1")         .attr("disabled","");
		  
	  }	  			
		
		jQuery("#comentariosDerechos").attr("disabled","");
		jQuery("#pctPromotorDerechos").attr("disabled","");
		jQuery("#pctAgenteDerechos")  .attr("disabled","");
        jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteDerechos']").removeAttr("disabled");
	}
	 if(dwr.util.getValue("claveDerechos")==false && value==3){
	 	//alert("claveDerechos - false")
		jQuery("#comentariosDerechos").attr("disabled","disabled");
		jQuery("#pctPromotorDerechos").attr("disabled","disabled");
		jQuery("#pctAgenteDerechos")  .attr("disabled","disabled");
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteDerechos']").attr("disabled","disabled");
		jQuery("#monto_1")            .attr("disabled","disabled");
		jQuery("#porcentaje_1")       .attr("disabled","disabled");	
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteDerechos']").removeAttr("checked");	
	}
	
	 if(dwr.util.getValue("claveBono_1")==true && value==2){
	 	//alert("clavebono_1 true");
	   if (jQuery('#monto_2').val().length == 0 && jQuery('#porcentaje_2').val().length==0){
		  	jQuery('input[name="negocioBonoComision.clavePorcentajeImporteBono"]')[0].checked = true;
		  	jQuery("#monto_2")         .attr("disabled","");
	   }
	  if (jQuery('#monto_2').val().length > 0 && jQuery('#porcentaje_2').val().length==0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteBono"]')[0].checked = true;
		  	jQuery("#monto_2")         .attr("disabled","");
	  }
	  if (jQuery('#monto_2').val().length == 0 && jQuery('#porcentaje_2').val().length>0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteBono"]')[1].checked = true;
		  	jQuery("#porcentaje_2")         .attr("disabled","");
	  }
	  
	  if (jQuery('#monto_2').val().length > 0 && jQuery('#porcentaje_2').val().length>0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteBono"]')[0].checked = true;
		  	jQuery("#monto_2")         .attr("disabled","");
	  }	  	  
	  
		
		jQuery("#pctAgenteBono")         .attr("disabled","");
        jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteBono']").removeAttr("disabled");	
		jQuery("#pctPromotorBono")       .attr("disabled","");
		jQuery("#comentariosBono")       .attr("disabled","");
	}
	 if(dwr.util.getValue("claveBono_1")==false && value==2){
	 	//alert("clavebono_1 false");
		jQuery("#monto_2")        .attr("disabled","disabled");
		jQuery("#porcentaje_2")   .attr("disabled","disabled");
		jQuery("#pctAgenteBono")  .attr("disabled","disabled");
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteBono']").attr("disabled","disabled");
		jQuery("#pctPromotorBono")            .attr("disabled","disabled");
		jQuery("#comentariosBono")           .attr("disabled","disabled");
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteBono']").removeAttr("checked");	
	}
	
	 if(dwr.util.getValue("claveBono")==true && value==4){
	 	//alert("clavebono");
	   if (jQuery('#monto_3').val().length == 0 && jQuery('#porcentaje_3').val().length==0){
		  	jQuery('input[name="negocioBonoComision.clavePorcentajeImporteUDI"]')[0].checked = true;
		  	jQuery("#porcentaje_3")         .attr("disabled","");

		}
	  if (jQuery('#monto_3').val().length > 0 && jQuery('#porcentaje_3').val().length==0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteUDI"]')[1].checked = true;
		  	jQuery("#monto_3")         .attr("disabled","");

	  }
	  if (jQuery('#monto_3').val().length == 0 && jQuery('#porcentaje_3').val().length>0){
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteUDI"]')[0].checked = true;
		  	jQuery("#porcentaje_3")         .attr("disabled","");

	  }
	  
	  if (jQuery('#monto_3').val().length > 0 && jQuery('#porcentaje_3').val().length>0){	
	 		jQuery('input[name="negocioBonoComision.clavePorcentajeImporteUDI"]')[0].checked = true;
		  	jQuery("#porcentaje_3")         .attr("disabled","");

	  }	  		 
		jQuery("#pctAgenteUDI")               .attr("disabled","");
        jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteUDI']").removeAttr("disabled");
		jQuery("#pctPromotorUDI")             .attr("disabled","");
		jQuery("#comentariosUDI")             .attr("disabled","");
	}
	 if(dwr.util.getValue("claveBono")==false && value==4){
	 	//alert("claveBono - false");
		jQuery("#pctAgenteUDI")  .attr("disabled","disabled");
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteUDI']").attr("disabled","disabled");
		jQuery("#pctPromotorUDI")            .attr("disabled","disabled");
		jQuery("#comentariosUDI")            .attr("disabled","disabled");
		jQuery("#porcentaje_3")               .attr("disabled","disabled");
		jQuery("#monto_3")                    .attr("disabled","disabled");	
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteUDI']").removeAttr("checked");		
	}	
   }
}

function disabledAtrribute(value,val){
if(dwr.util.getValue("estatus")!=1){	
	if(jQuery('#radio_1').val(true) && value=="true" && val==1){
		jQuery("#porcentaje_1").attr("disabled","");
		jQuery("#monto_1").attr("disabled","disabled");
	 }
	if(jQuery('#radio_1').val(false) && value=="false" && val==1){
		jQuery("#porcentaje_1").attr("disabled","disabled");
		jQuery("#monto_1").attr("disabled","");	    	 
	     
	  }
	  
     /*checar validacion */
	if(jQuery('#radio_2').val(false) && value=="false" && val==2 ){
		jQuery("#porcentaje_2").attr("disabled","");
		jQuery("#monto_2").attr("disabled","disabled");
	 }

	if(jQuery('#radio_2').val(true) && value=="true" && val==2){
		jQuery("#porcentaje_2").attr("disabled","disabled");
		jQuery("#monto_2").attr("disabled","");  	 
	     
	 }	 	
	
	if(jQuery('#radio_3').val(true) && value=="true" && val==3 ){
		jQuery("#porcentaje_3").attr("disabled","");
		jQuery("#monto_3").attr("disabled","disabled");
	 }
	if(jQuery('#radio_3').val(false) && value=="false" && val==3){
		jQuery("#porcentaje_3").attr("disabled","disabled");
		jQuery("#monto_3").attr("disabled","");
	  }	
	}
}



function iniciaPaginaBono(){
	habilitarBonoComision(1);
	habilitarBonoComision(2);
	habilitarBonoComision(3);
	habilitarBonoComision(4);
	if(jQuery("#claveBono_1").is(":checked")){
		jQuery("input[name$='negocioBonoComision.clavePorcentajeImporteBono']").each(function(){
			if(jQuery(this).is(":checked")){
				disabledAtrribute(jQuery(this).val(),2);
			}
		});
	}
}

function calculaPorcentajePromotor(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctPromotorBono').val("");
  
 if(valor>100){
   jQuery("#pctAgenteBonoHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
   jQuery('#pctPromotorBono').val("");
   return false;
  }
 jQuery('#pctPromotorBono').val(valorTotal);

}


function calculaPorcentajeAgenteBono(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctAgenteBono').val("");
  
 if(valor>100){
   jQuery("#pctPromotorBonoHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
   jQuery('#pctAgenteBono').val("");
   return false;
  }
 jQuery('#pctAgenteBono').val(valorTotal);

}

function calculaPorcentajePromotorUDi(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctPromotorUDI').val("");
  
 if(valor>100){
   jQuery("#pctAgenteUDIHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
   jQuery('#pctPromotorUDI').val("");
   return false;
  }
 jQuery('#pctPromotorUDI').val(valorTotal);

}


function calculaPorcentajeAgenteUDI(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
  valorTotal = parseFloat(valorTope) - parseFloat(valor);
  valorTotal=valorTotal.toFixed(2);
 jQuery('#pctAgenteUDI').val("");
  
 if(valor>100){
   jQuery("#pctPromotorUDIHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
   jQuery('#pctAgenteUDI').val("");
   return false;
  }
 jQuery('#pctAgenteUDI').val(valorTotal);
}


function calculaPorcentajepctPromotorDerechos(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctPromotorDerechos').val("");
  
 if(valor>100){
   jQuery("#pctAgenteDerechosHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
   jQuery('#pctPromotorDerechos').val("");
   return false;
  }
 jQuery('#pctPromotorDerechos').val(valorTotal);
}



function calculaPorcentajepctAgenteDerechos(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctAgenteDerechos').val("");
  
 if(valor>100){
   jQuery("#pctPromotorDerechosHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
    jQuery('#pctAgenteDerechos').val("");
   return false;
  }
 jQuery('#pctAgenteDerechos').val(valorTotal);
}



function calculaPorcentajepctPromotorSobreComision(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctPromotorSobreComision').val("");
  
 if(valor>100){
   jQuery("#pctAgenteSobreComisionHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
   jQuery('#pctPromotorSobreComision').val("");
   return false;
  }
 jQuery('#pctPromotorSobreComision').val(valorTotal);
}



function calculaPorcentajepctAgenteSobreComision(valor,campo){
if(valor.length==0)return;
 jQuery("#error").hide();
 var valorTope="";
 valorTope=100;
 var valorTotal="";
 valorTotal = parseFloat(valorTope) - parseFloat(valor);
 valorTotal=valorTotal.toFixed(2);
 jQuery('#pctAgenteSobreComision').val("");
  
 if(valor>100){
   jQuery("#pctPromotorSobreComisionHidden").after('<font color=red><span id="error">No debe de Exceder de 100%</span></font>');
    jQuery('#pctAgenteSobreComision').val("");
   return false;
  }
 jQuery('#pctAgenteSobreComision').val(valorTotal);
}	


					

</script> 	
<s:form action="guardar" id="negocioBonoComisionForm">
	<s:hidden name="idToNegocio" id="idToNegocio"/>
	<s:hidden	 name="negocioBonoComision.negocio.claveEstatus" id="estatus"/>
	<s:actionerror/>
<div style="width: 100%;align:left;">
	<table width="100%" style="margin-left: 0px;"  >

	<tr>
			<td class="titulo" colspan="7">
					<!--s:text name="midas.negocio.bonocomision.titulo" /-->
					Comisiones
			</td>
		</tr>	
		
		
				<tr>
			<td  colspan="3" valign="top">
				<table cellspacing="4" id="agregar"  style="margin-left: 0px; height: 80px; padding:0px;" >
					<tr height="50px">
						<th>
						% Comision al agente del negocio
						</th>
						<td>
							<s:textfield 
								name="negocioBonoComision.pctComisionAgte" id="pctComisionAgte" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true"
								cssClass="txtfield jQfloat jQrestrict jQrequired"										
								/>	
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="30px">
						<td colspan="4">&nbsp;</td>
					</tr>
							
				</table>			
			</td>
			
				<td  colspan="3" valign="top">
				<table  cellspacing="4" id="agregar" style="margin-left: 0px; height: 80px; padding:0px;" >
					
					<tr height="50px">
						<th>
						
						</th>
						<td>
						
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="30px">
						<td colspan="4">&nbsp;</td>
					</tr>
				</table>			
			</td>
		</tr>	
		<tr>
			<td class="titulo" colspan="7">
					<s:text name="midas.negocio.bonocomision.titulo" />
			</td>
		</tr>	
		
		<tr>
			<td  colspan="3" valign="top">
				<table cellspacing="4" id="agregar"  style="margin-left: 0px; height: 440px; padding:0px;" >
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sobrecomision.clave" /></th>
						<td>
							<s:checkbox cssClass="txtfield" onchange="habilitarBonoComision('1')" disabled="#readEditOnlyCheck"
								name="negocioBonoComision.claveSobreComision"
								id="claveSobreComision"/>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="30px">
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sobrecomision" /></th>
						<td>
							<s:textfield  disabled="#readEditOnlyclaveSobreComision"  name="negocioBonoComision.pctSobreComision" id="pctSobreComision" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 		
								cssClass="txtfield jQfloat jQrestrict jQrequired"				 
								/>						
						</td>
						<th><s:text name="midas.negocio.bonocomision.sobrecomision.incluyeIVA" /></th>
						<td>
							<s:checkbox  disabled="#readEditOnlyclaveSobreComision"  cssClass="txtfield" disabled="#readEditOnlyCheck"
								name="negocioBonoComision.claveIncluyeIVASobreomision"
								labelposition="bottom"
								id="claveIncluyeIVASobreomision" />						
						</td>
					</tr>				
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sobrecomision.pct.agente" /></th>
						<td colspan=3>
						 <s:hidden name="pctAgenteSobreComisionHidden" id="pctAgenteSobreComisionHidden"/>
							<s:textfield disabled="#readEditOnlyclaveSobreComision"  name="negocioBonoComision.pctAgenteSobreComision" id="pctAgenteSobreComision" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 	
								cssClass="txtfield jQfloat jQrestrict jQrequired"		
								onmousemove="calculaPorcentajepctPromotorSobreComision(this.value);" 
								onblur="calculaPorcentajepctPromotorSobreComision(this.value);" 	
								onclick="calculaPorcentajepctPromotorSobreComision(this.value);"			 
								/>						
						</td>
										
					</tr>	
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sobrecomision.pct.promotor" /></th>
						<td colspan=3>
						    <s:hidden name="pctPromotorSobreComisionHidden" id="pctPromotorSobreComisionHidden"/>
							<s:textfield disabled="#readEditOnlyclaveSobreComision" name="negocioBonoComision.pctPromotorSobreComision" id="pctPromotorSobreComision" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true"
								onkeydown="setDecimalForLength(this,7)" 	
								onmousemove="calculaPorcentajepctAgenteSobreComision(this.value);" 		
                                cssClass="txtfield jQfloat jQrestrict jQrequired"										
								onclick="calculaPorcentajepctAgenteSobreComision(this.value);" 			
								onblur="calculaPorcentajepctAgenteSobreComision(this.value);" 							 
								/>						
						</td>
											
					</tr>
					<tr height="150px">
						<th><s:text name="midas.negocio.bonocomision.sobrecomision.comentarios" /></th>
						<td colspan="3">
							<s:textarea disabled="#readEditOnlyclaveSobreComision" name="negocioBonoComision.comentariosSobreComision" id="comentariosSobreComision"
						    cssClass="txtfield" required="true" cols="40" rows="4" cssClass="jQalphanumeric"/>
						</td>							
					</tr>					
				</table>
			</td>
		  	<td  colspan="3" valign="top">
				<table  cellspacing="4" id="agregar" style="margin-left: 0px; height: 440px; padding:0px;" >
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.bonoespecial.clave" /></th>
						<td>
							<s:checkbox cssClass="txtfield" disabled="#readEditOnlyCheck"
								name="negocioBonoComision.claveBono"
								onclick="habilitarBonoComision('2');"
								id="claveBono_1"/>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="30px">
						<td>&nbsp;</td>
				  	        <td>
								<s:radio disabled="#readEditOnlyBono" name="negocioBonoComision.clavePorcentajeImporteBono"
									list="#{'true':'Porcentaje'}"
									id="radio_2"
									onclick="disabledAtrribute(this.value,2);"
									value="negocioBonoComision.clavePorcentajeImporteBono" />				
					       </td>
					        <td>
								<s:radio disabled="#readEditOnlyBono" name="negocioBonoComision.clavePorcentajeImporteBono"
									list="#{'false':'Monto'}"
									id="radio_2"
									onclick="disabledAtrribute(this.value,2);"
									value="negocioBonoComision.clavePorcentajeImporteBono" />				
					    </td>					  
					</tr>
					<tr valign="bottom" height="50px">
						<th valign="middle"><s:text name="midas.negocio.bonocomision.bonoespecial" /></th>
						<td>
							<s:textfield disabled="#readEditOnlyBonoPorcentaje" name="negocioBonoComision.pctBono" id="monto_2" 
								labelposition="left" maxlength="5" size="10"  cssStyle="width: 168px;"
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"						 
								/>						
						</td>
						<td valign="bottom">
							<s:textfield disabled="#readEditOnlyBonoMonto" name="negocioBonoComision.importeBono" id="porcentaje_2" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"						 
								/>						
						</td>						
						<td>&nbsp;</td>
					</tr>				
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.bonoespecial.pct.agente" /></th>
						<td>
						 <s:hidden name="pctAgenteBonop" id="pctAgenteBonoHidden" /> 
							<s:textfield disabled="#readEditOnlyBono" name="negocioBonoComision.pctAgenteBono" id="pctAgenteBono" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"
								onmousemove="calculaPorcentajePromotor(this.value);" 
								onclick="calculaPorcentajePromotor(this.value);"
								onblur="calculaPorcentajePromotor(this.value);"						 
								/>						
						</td>	
						<td colspan="2">&nbsp;</td>						
					</tr>	
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.bonoespecial.pct.promotor" /></th>
						
						<td>
						    <s:hidden name="pctAgenteBonop" id="pctPromotorBonoHidden" />
							<s:textfield disabled="#readEditOnlyBono" name="negocioBonoComision.pctPromotorBono" id="pctPromotorBono" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 	
								cssClass="txtfield jQfloat jQrestrict jQrequired"
								onmousemove="calculaPorcentajeAgenteBono(this.value);" 	
								onclick="calculaPorcentajeAgenteBono(this.value);" 		
								onblur="calculaPorcentajeAgenteBono(this.value);" 						 
								/>						
						</td>
						<td colspan="2">&nbsp;</td>						
					</tr>
					<tr height="150px">
						<th><s:text name="midas.negocio.bonocomision.bonoespecial.comentarios" /></th>
						<td colspan="3">
							<s:textarea disabled="#readEditOnlyBono" name="negocioBonoComision.comentariosBono" id="comentariosBono"
						    cssClass="txtfield" required="true" cols="40" rows="4" cssClass="jQalphanumeric"/>
						</td>						
					</tr>						
				</table>			
			</td>
		</tr>	
		<tr>
			<td colspan="3" valign="top">
				<table id="agregar" cellspacing="6"  style="margin-left: 0px; height: 450px; padding:0px;" >
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sesionderechos.clave" /></th>
						<td>
							<s:checkbox cssClass="txtfield" disabled="#readEditOnlyCheck"
								name="negocioBonoComision.claveDerechos"
								onclick="habilitarBonoComision(3);"
								id="claveDerechos"/>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="30px">
						<td>&nbsp;</td>
						<td>
							<s:radio disabled="#readEditOnlyBonoComision" name="negocioBonoComision.clavePorcentajeImporteDerechos"
							    id="radio_1"
								list="#{'true':'Porcentaje'}"
								onclick="disabledAtrribute(this.value,1);"
								value="negocioBonoComision.clavePorcentajeImporteDerechos" />
								
						</td>
						<td>
							<s:radio disabled="#readEditOnlyBonoComision" name="negocioBonoComision.clavePorcentajeImporteDerechos"
							    id="radio_1"
								list="#{'false':'Monto'}"
								onclick="disabledAtrribute(this.value,1);"
								value="negocioBonoComision.clavePorcentajeImporteDerechos" />
										
						</td>
					</tr>			
					<tr valign="bottom" height="50px">
						<th valign="middle"><s:text name="midas.negocio.bonocomision.sesionderechos" /></th>
						<td>	
							<s:textfield disabled="#readEditOnlyCheckPorcentaje" name="negocioBonoComision.pctDerechos" 
								labelposition="left" maxlength="5" size="10" 
								id="porcentaje_1" cssStyle="width: 168px;"
								cssClass="txtfield" required="true" 	
								cssClass="txtfield jQfloat jQrestrict jQrequired"					 
								/>						
						</td>
						<td>
							<s:textfield disabled="#readEditOnlyCheckMonto" name="negocioBonoComision.importeDerechos" 
								labelposition="left" maxlength="5" size="10" 
								id="monto_1"
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"						 
								/>						
						</td>						
						<td>&nbsp;</td>
					</tr>	
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sesionderechos.pct.agente" /></th>
						<td>
						    <s:hidden name="pctAgenteUDI" id="pctAgenteDerechosHidden"/>
							<s:textfield disabled="#readEditOnlyBonoComision" name="negocioBonoComision.pctAgenteDerechos" id="pctAgenteDerechos" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 	
								cssClass="txtfield jQfloat jQrestrict jQrequired"	
								onmousemove="calculaPorcentajepctPromotorDerechos(this.value);" 
								onclick="calculaPorcentajepctPromotorDerechos(this.value);" 	
								onblur="calculaPorcentajepctPromotorDerechos(this.value);" 						 
								/>						
						</td>
						<td colspan="2">&nbsp;</td>						
					</tr>	
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.sesionderechos.pct.promotor" /></th>
						<td>
						   <s:hidden name="pctAgenteUDI" id="pctPromotorDerechosHidden"/>
							<s:textfield disabled="#readEditOnlyBonoComision" name="negocioBonoComision.pctPromotorDerechos" id="pctPromotorDerechos" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 	
								cssClass="txtfield jQfloat jQrestrict jQrequired"	
								onmousemove="calculaPorcentajepctAgenteDerechos(this.value);" 	
								onclick="calculaPorcentajepctAgenteDerechos(this.value);" 	
								onblur="calculaPorcentajepctAgenteDerechos(this.value);" 							 
								/>						
						</td>
						<td colspan="2">&nbsp;</td>						
					</tr>
					<tr height="150px">
						<th><s:text name="midas.negocio.bonocomision.sesionderechos.comentarios" /></th>
						<td colspan="3">
							<s:textarea disabled="#readEditOnlyBonoComision" name="negocioBonoComision.comentariosDerechos" id="comentariosDerechos"
						    cssClass="txtfield" required="true" cols="40" rows="4" cssClass="jQalphanumeric"/>
						</td>
					</tr>				
				</table>
			</td>
			<td colspan="3" valign="top">
				<table id="agregar" cellspacing="6" style="margin-left: 0px; height: 450px; padding:0px;">
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.udi.clave" /></th>
						<td>
							<s:checkbox cssClass="txtfield" disabled="#readEditOnlyCheck"
								name="negocioBonoComision.claveUDI"
								onclick="habilitarBonoComision(4)"
								id="claveBono"/>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="30px">
					  <td>&nbsp;</td>
						<td>
							<s:radio disabled="#readEditOnlyUDI" name="negocioBonoComision.clavePorcentajeImporteUDI"
							id="radio_3"
								list="#{'true':'Porcentaje'}" 
								onclick="disabledAtrribute(this.value,3);"
								value="negocioBonoComision.clavePorcentajeImporteUDI" theme="simple" />	
						</td>

						<td>
			
							<s:radio disabled="#readEditOnlyUDI" name="negocioBonoComision.clavePorcentajeImporteUDI"
							id="radio_3"
								list="#{'false':'Monto'}" 
								onclick="disabledAtrribute(this.value,3);"
								value="negocioBonoComision.clavePorcentajeImporteUDI" theme="simple" />	
						</td>						
					</tr>
					<tr valign="bottom" height="50px">
						<th valign="middle"><s:text name="midas.negocio.bonocomision.udi" /></th>
						<td>
							<s:textfield disabled="#readEditOnlyCheckPorcentajeUDI" name="negocioBonoComision.pctUDI" id="pctUDI" 
							    id="porcentaje_3" cssStyle="width: 168px;"
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"						 
								/>						
						</td>
						<td>
							<s:textfield disabled="#readEditOnlyCheckMontoUDI" name="negocioBonoComision.importeUDI" id="importeUDI" 
							    id="monto_3"
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"						 
								/>						
						</td>						
						<td>&nbsp;</td>
					</tr>				
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.udi.pct.agente" /></th>
						<td>
						<s:hidden name="pctAgenteUDI" id="pctAgenteUDIHidden"/>
							<s:textfield disabled="#readEditOnlyUDI" name="negocioBonoComision.pctAgenteUDI" id="pctAgenteUDI" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"	
								onmousemove="calculaPorcentajePromotorUDi(this.value);"
								onclick="calculaPorcentajePromotorUDi(this.value);" 	
								onblur="calculaPorcentajePromotorUDi(this.value);" 	 						 
								/>						
						</td>
						<td colspan="2">&nbsp;</td>						
					</tr>	
					<tr height="50px">
						<th><s:text name="midas.negocio.bonocomision.udi.pct.promotor" /></th>
						<td>
						<s:hidden name="pctAgenteUDI" id="pctPromotorUDIHidden"/>
							<s:textfield disabled="#readEditOnlyUDI" name="negocioBonoComision.pctPromotorUDI" id="pctPromotorUDI" 
								labelposition="left" maxlength="5" size="10" 
								cssClass="txtfield" required="true" 
								cssClass="txtfield jQfloat jQrestrict jQrequired"	
								onmousemove="calculaPorcentajeAgenteUDI(this.value);" 
								onclick="calculaPorcentajeAgenteUDI(this.value);"
								onblur="calculaPorcentajeAgenteUDI(this.value);"							 
								/>						
						</td>
						<td colspan="2">&nbsp;</td>						
					</tr>
					<tr height="150px">
						<th><s:text name="midas.negocio.bonocomision.udi.comentarios" /></th>
						<td colspan="3">
							<s:textarea disabled="#readEditOnlyUDI" name="negocioBonoComision.comentariosUDI" id="comentariosUDI"
						    cssClass="txtfield" required="true" cols="40" rows="4" cssClass="jQalphanumeric"/>
						</td>
					</tr>	
				</table>			
			</td>
		</tr>	
	</table>	
	</div>
 <div class="alinearBotonALaDerecha">
	<div id="b_guardar" style="width:100px;" >
		<a href="javascript: void(0);"
			onclick="javascript:guardarBonosComision();">
			<s:text name="midas.boton.guardar"/>
		</a>
	</div>
 </div>	 
</s:form>
<script type="text/javascript">
	iniciaPaginaBono();
</script>