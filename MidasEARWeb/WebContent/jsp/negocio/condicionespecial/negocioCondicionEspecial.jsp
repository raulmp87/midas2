<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxcommon.js'/>"></script>

<%-- <script type="text/javascript" --%>
<%-- 	src="<s:url value='/js/dhtmlxgrid.js'/>"></script> --%>

<script type="text/javascript"
	src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>

<script type="text/javascript"
	src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>

<%-- <script type="text/javascript" --%>
<%-- 	src="<s:url value='/js/dhtmlxdataprocessor_debug.js'/>"></script> --%>

<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript"
	src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"
	charset="ISO-8859-1"></script>

<script	src="<s:url value='/js/midas2/negocio/condicionespecial/negocioCondicionEspecial.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>
<div id="contenido_condicionespecial">
	<div class="titulo">Condiciones Especiales de Negocio</div>
	<div class="filtros" >
		<div id="divS">
			<div id="divSI" style="width: 52.5%; height: 100%; float: left; position: relative;">
				<div style="width: 90%;height:100%;">
					<s:select id="idSeccion" label="Linea de Negocio" labelposition="left" labelposition="%{getText('label.position')}" name="idSeccion" list="secciones"  listKey="idToSeccion" listValue="nombreComercial" headerKey="0" headerValue="Todas..." cssStyle="width:300px;" onchange="changeComboCondicionesEspeciales();" />
					<s:textfield id="codigoCondicionEspecial" label="Código o condición Especial " labelposition="left" labelposition="%{getText('label.position')}" name="condicionEspecial.codigo" cssStyle="width:242px;" />
					<div class="btn_back w140" style="display: inline; float: right;">
						 <a href="javascript: void(0);" onclick="searchCondicionesEspeciales();">
						 Buscar </a>
					</div>
					
<!-- 					<button style="float:right;position:relative;margin-right:5px;" onclick="searchCondicionesEspeciales();">Buscar</button> -->
				</div>
				<div>				
				</div>
			</div>
			<div id="divSD" style="width: 40%; height: 100%; float: left; position: relative;">
				<div style="width: 100%;height:100%;">
					<s:checkbox id="aplicaExterno" onchange="setAplicaExterno();" name="aplicaExterno" fieldValue="1" label="Aplica para Externo" labelposition="left" labelposition="%{getText('label.position')}" />
				</div>
			</div>
		</div>
	</div>
	<div id="divM">
		<s:form id="negocioCondicionEspecialForm">
			<table width="100%" height="100%">
				<tbody>
					<tr align="center" valign="middle">
						<td>
							<div>
							<div class="titulo">Condiciones Disponibles</div>
								<div id="condicionesEspecialesDisponiblesGrid" style="width: 420px; height: 240px;">
								</div>
							</div>							
						</td>
						<td align="center" valign="middle" width="8%">
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="borrarAsociadas();" alt="Desligar todas" href="javascript: void(0);"> << </a>
							</div>
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="asociarTodas();" alt="Asociar todas" href="javascript: void(0);"> >> </a>
							</div>
						</td >
						<td>
							<div>
								<div class="titulo">Condiciones Asociadas</div>
								<div id="condicionesEspecialesAsociadasGrid" style="width: 420px; height: 240px;">
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</s:form>
	</div>
</div>

<style type="text/css">
	#contenido_condicionespecial{
		margin-left: 5px; 
		margin-top: 3px; 
		width: 99%; 
		height: 95%;
	}
	
	#divS{
		float: left; 
		width: 100%; 
		height: 20%;
	}
	
	#divM{
		float: left; 
		width: 100%; 
		height: 55%;
		margin-top: 10px;
	}
	
	.btnActionForAll{
		margin-top: 10px;
	}
	
</style>

<script type="text/javascript">
	initGridsNegocioCondicionEspecial();
</script>