<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="negocioCondicionEspecial.id" type="ro" width="0" sort="int" hidden="true">id</column>
		<column id="negocioCondicionEspecial.condicionEspecial.id" type="ro" width="0" sort="int" hidden="true">Id</column>
	    <column id="negocioCondicionEspecial.condicionEspecial.nombre" type="ro"  width="*"  sort="str">Nombre</column>
		<column id="negocioCondicionEspecial.condicionEspecial.codigo" type="ro"  width="60"  sort="str" >Codigo</column>
		<column id="midas.condicionespecial.nivelImpacto" type="ro" width="80" >Nivel Impacto</column>
		<column id="negocioCondicionEspecial.obligatoria" type="ch"  width="80">Obligatoria</column>
		<column id="negocioCondicionEspecial.produceExcepcion" type="ch" width="80" >Aplica Excepcion</column>
		
	</head>
	<% int a=0;%>
	<s:iterator value="condicionesAsociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="condicionEspecial.id" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="condicionEspecial.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="condicionEspecial.codigo" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="condicionEspecial.nivelAplicacion == 0">
				<cell><s:text name="midas.fuerzaventa.configBono.poliza" /></cell>
			</s:if>
			<s:elseif test="condicionEspecial.nivelAplicacion == 1">
				<cell><s:text name="midas.suscripcion.solicitud.autorizacion.inciso" /></cell>
			</s:elseif>
			<s:if test="obligatoria == 1">
				<cell>1</cell>
			</s:if>
			<s:elseif test="obligatoria == 0">
				<cell>0</cell>
			</s:elseif>
			<s:if test="produceExcepcion == 1">
				<cell>1</cell>
			</s:if>
			<s:elseif test="produceExcepcion == 0">
				<cell>0</cell>
			</s:elseif>			
		</row>
	</s:iterator>
</rows>