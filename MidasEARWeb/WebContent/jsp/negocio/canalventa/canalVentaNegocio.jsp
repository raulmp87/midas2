<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<script type="text/javascript">
	var URL_MOSTRAR_CAMPOS   = '<s:url action="obtenerCamposCotizador"                 namespace="/negocio/canalventa"/>';
	var URL_IMPRESIONES      = '<s:url action="obtenerImpresionCotizador"              namespace="/negocio/canalventa"/>';
	var URL_GUARDAR_CAMPOS   = '<s:url action="guardarVisibilidadCamposCotizador"      namespace="/negocio/canalventa"/>';
	var URL_TODOS_VISIBLES   = '<s:url action="guardarVisibilidadTodosCamposCotizador" namespace="/negocio/canalventa"/>';
	var URL_TODOS_INVISIBLES = '<s:url action="guardarVisibilidadTodosCamposCotizador" namespace="/negocio/canalventa"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/negocio/canalVenta/canalVenta.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">

	.divContenedor {
		float:left; 
		clear: left; 
		padding: 10px 10px; 
		width: 95%;		
	}
	
</style>

<br/>
<div id="">

	<div id="divM">
		<s:form id="negocioCanalVtaForm" name="canalventa">
			<div class="titulo"><s:text name='midas.negocio.configurador.titulo'/></div>			
			<div class="divContenedor" style="width:97%">
			<table><tr>
					<td>
					<div class="btn_back w140 btnActionForAll">
						<a class="" onclick="hacerInvisiblesTodos();" alt="Hacer invisibles todos los campos" href="javascript: void(0);"> Desmarcar todos </a>
					</div>
					</td>
					<td>
					<div class="btn_back w140 btnActionForAll">
						<a class="" onclick="hacerVisiblesTodos();" alt="Hacer visibles todos los campos" href="javascript: void(0);"> Marcar todos </a>
					</div>
					</td>
			</tr></table>
			</div>
			
			<div class="divContenedor" style="width:97%">
				<div id="paginador"></div>
				<div id="canalVentaCamposGrid" class="dataGridConfigurationClass" style="float:left; width:600px; height:350px;"></div>
				<div style="float:left; margin-left: 50px;"></div>
			</div>
			<%--
			<div class="titulo">Configurador de Impresion de Poliza</div>
			<div class="divContenedor" style="width:97%">
				<div id="paginador"></div>
				<div id="canalVentaImpresionesGrid" class="dataGridConfigurationClass" style="float:left; width:600px; height:350px;"></div>
				<div style="float:left; margin-left: 50px;"></div>
			</div>
			--%>
		</s:form>
	</div>
</div>
<script type="text/javascript">
	initGridCanalVenta();
	//initGridCanalVentaImpresiones();
</script>