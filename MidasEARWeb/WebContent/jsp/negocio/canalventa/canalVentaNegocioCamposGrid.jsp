<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="campoEditado.id"  type="ro" align="center" width="0"	hidden="true">Id</column>
		<column id="campoEditado.catalogo.descripcion"	 type="ro" align="left" width="*"	sort="str"	>Seccion</column>	
		<!-- descripcionHijo es un campo que no existe pero se agrega el set en la entidad para poder hacer aqui la sangria entre campos y secciones -->
		<column id="campoEditado.catalogo.descripcionHijo"	 type="ro" align="left" width="*"	sort="str"	>Campo</column>			
		<column id="campoEditado.status" type="ch" align="center" width="100"	sort="str"	>Visible</column>		
		<column id="campoEditado.catalogo.codigoPadre" type="ro" align="center" width="0"	sort="str"	hidden="true">Visible</column>
		<column id="campoEditado.catalogo.codigo" type="ro" align="center" width="0"	sort="str"	hidden="true">Visible</column>			
					
	</head>

 
	 <s:iterator value="listaCampos">
	<s:if test="catalogo.codigoPadre==\"GPO_GPOS\"" >	
		<row  id="<s:property value="id"/>" >
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="catalogo.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell></cell>
			<cell><s:property value="status" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catalogo.codigoPadre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catalogo.codigo" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	 <s:iterator value="listaCampos">
	 <!-- el [1] es para referir al iterador externo, el iterador interno esta en [0]
	  es decir en la cima en la pila para poder referir directamente -->
	<s:if test="catalogo.codigoPadre != \"GPO_GPOS\" && catalogo.codigoPadre == [1].catalogo.codigo" >	
		<row id="<s:property value="id"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell></cell>	
			<cell><s:property value="catalogo.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="status" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catalogo.codigoPadre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catalogo.codigo" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:if>
		</s:iterator>
		</s:if>	
	</s:iterator>
	
	
	<%-- <s:iterator value="listaCampos">
	
		<row id="<s:property value="id"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell></cell>	
			<cell><s:property value="catalogo.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="status" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catalogo.codigoPadre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catalogo.codigo" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator> --%>
</rows>