<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<s:include value="/jsp/negocio/negocioHeader.jsp"></s:include>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="-1">
<script type="text/javascript">
	var idNegocio=	'<s:property value="id"/>';
	var tipoAccion = '<s:property value="tipoAccion"/>';
	var claveNegocio = '<s:property value="claveNegocio"/>';
	var mensaje = '<s:property value="mensaje" default="-1"/>';
	var tipoMensaje = '<s:property value="tipoMensaje" default="-1"/>';
	var nivelActivo = '<s:property value="nivelActivo"/>';
</script>

<input type="hidden" id="idNegocioSapAmis" value="<s:property value="id"/>"/>
<s:hidden name="negocio.tipoPersona" id="tipoPersona" />

<table style="width:98%">
		<tr>
			<td class="titulo" >
				<s:if test="tipoAccion == 1">
					<s:text name="midas.negocio.agregarnegocio" />
				</s:if>
				<s:if test="tipoAccion == 2">
					<s:text name="midas.negocio.modificarnegocio" />
				</s:if>
			</td>
		</tr>
</table>
<div select="<s:property value='tabActiva'/>" hrefmode="ajax-html" style="height: 450px;" id="configuracionNegocioTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE">	
	<div width="100%" id="detalle" name="Inf General" href="http://void" extraAction="javascript: verInfoGeneral(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);">
	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	</div>
	<s:if test="id != null">
		<div width="100%" id="clientes" name="Clientes" href="http://void" extraAction="javascript: desplegarNegocioClientes(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);">
		</div>
		<div width="100%" id="agentes" name="Agentes" href="http://void" extraAction="javascript: deplegarNegocioAgentes();">
		</div>
		<div width="130%" id="parametrosGrales" name="<s:text name='midas.negocio.parametrosGenerales'/>" href="http://void" extraAction="javascript: desplegarParamGrales(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);">
			<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
		</div>
 	    <div width="100%" id="bonos" name="Bonos" href="http://void" extraAction="javascript: verDetalleBonos(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);"></div>
	    <div width="100%" id="zona" name="<s:text name='midas.negocio.zonaCirculacion'/>" href="http://void" extraAction="javascript: desplegaZonas(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);">
	    </div>
	    <div width="150%" id="descuentoPorEstado" name="<s:text name='midas.negocio.estadoDescuento'/>" href="http://void" extraAction="javascript: desplegaDescuentosPorEstado(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);"></div>
	    <div width="150%" id="renovacion" name="<s:text name='midas.negocio.renovacion.condicionesRenovacion'/>" href="http://void" extraAction="javascript: verCondicionesRenovacion(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);"></div>
	    <div width="150%" id="antecedentes" name="Antecedentes" href="http://void" extraAction="javascript: desplegarAntecedentes(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);"></div>
	    <div width="150%" id="negocioCondicionEspecial" name="Condiciones Especiales" href="http://void" extraAction="javascript: verNegocioCondicionEspecial();"></div>
	    <div width="150%" id="compensacionAdicional" name="Compensaciones" href="http://void" extraAction="javascript: verNegocioCompensacionAdicional(idNegocio);"></div>
	    <div width="200%" id="emailAlertasSAPAMIS" name="Email Alertas SAP-AMIS" href="http://void" extraAction="javascript: desplegarContenedorEmail();"></div>
	    <div width="150%" id="negocioUsuarioEdicionImpresion" name="<s:text name='midas.impresionpoliza.edicion.negociousuario.tab'/>" href="http://void" extraAction="javascript: verNegocioUsuarioEdicionImpresion();"></div>
	    <div width="150%" id="recuotificacion" name="<s:text name='midas.negocio.recuotificacion.tab'/>" href="http://void" extraAction="javascript: mostrarRecuotificacionContenedor();"></div>
	    <div width="150%" id="canalventa" name="<s:text name='midas.provisiones.canalVenta'/>" href="http://void" extraAction="javascript: mostrarContCanalVenta();"></div>
	    <div width="150%" id="tipoVigencia" name="<s:text name='midas.negocio.tipovigencia.tab'/>" href="http://void" extraAction="javascript: mostrarContenedorTiposVigencia();">
	    	<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
	    </div>
	</s:if>
</div>

<script type="text/javascript">
	dhx_init_tabbars();
</script>
<script type="text/javascript">
	jQuery('.dhx_tab_element[tab_id="detalle"]').click(function(){
		verInfoGeneral(idNegocio,tipoAccion,claveNegocio,mensaje,tipoMensaje,nivelActivo);
	});
</script>