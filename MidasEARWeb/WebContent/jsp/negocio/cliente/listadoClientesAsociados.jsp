<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
	
		<beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>  
        </beforeInit>
        
		<column id="id" type="ro" width="0" sort="na" hidden="true">id</column>
		<column id="idNegocio" type="ro" width="0" sort="na" hidden="true">idNegocio</column>
		<column id="idCliente" type="ro" width="0" sort="na" hidden="true">idCliente</column>
		<column id="clave" type="ro" width="90" sort="str" hidden="false">Clave</column>
		<column id="nombre" type="ro" width="*" sort="str" hidden="false">Nombre</column>
		<column id="codigoRFC" type="ro" width="170" sort="na" hidden="false">RFC</column>
		
	</head>
	<s:iterator value="listaNegocioCliente">
		<row id="<s:property value="idCliente" escapeHtml="false" escapeXml="true" />">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocio.idToNegocio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="idCliente" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="clienteDTO.idCliente" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="clienteDTO.nombre" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="clienteDTO.codigoRFC" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
</rows>