<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<tree id="0" order="asc">

	<s:iterator value="listaConfiguracionSeccion" >
	
		<item text="<s:property value="configuracionSeccion.descripcion" escapeHtml="false" escapeXml="true"/>" 
			id="CONFSEC_<s:property value="id" />"
			<s:if test="obligatoriedad == 1">
				checked="1"
			</s:if>
			child="1" im0="iconText2.gif" im1="tombs_open.gif" im2="tombs.gif">
		
		<s:iterator value="configuracionGrupoPorNegocios" >
		
			<item text="<s:property value="configuracionGrupo.descripcion" escapeHtml="false" escapeXml="true"/>" 
				id="CONFGPO_<s:property value="id" />"
				<s:if test="obligatoriedad == 1">
					checked="1"
				</s:if>
				child="1" im0="iconText2.gif" im1="leaf22.gif" im2="leaf12.gif">
			
			<s:iterator value="configuracionGrupo.configuracionCampos" >
				<item text="<s:property value="descripcion"  escapeHtml="false" escapeXml="true"/>"
					id="<s:property value="id" />"
					nocheckbox="true"
					child="0" im0="iconText2.gif" im1="folderOpen.gif" im2="folderClosed.gif"/>
			</s:iterator>
			</item>
		</s:iterator>
		</item>
	</s:iterator>
</tree>
