<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript">
	var idNegocio = '<s:property value="idNegocio"/>';
	var tipoAccion = '<s:property value="tipoAccion"/>';
	var claveNegocio = '<s:property value="claveNegocio"/>';
	var mensaje = '<s:property value="mensaje" default="-1"/>';
	var tipoMensaje = '<s:property value="tipoMensaje" default="-1"/>';
	var nivelActivo = '<s:property value="nivelActivo"/>';
	var tipoPersona = jQuery("#tipoPersona").val();
	var cambiarTipoPersonaURL = '<s:url action="actualizarTipoPersona" namespace="/negocio/cliente/asociar" />';
	var asociarClienteNegocioURL = '<s:url action="asociar.asociarCliente" namespace="/negocio/cliente/asociar" />';
	var asociarGrupoNegocioURL = '<s:url action="asociar.asociarGrupo" namespace="/negocio/cliente/asociar" />';
	var desasociarClienteNegocioURL = '<s:url action="asociar.desasociarCliente" namespace="/negocio/cliente/asociar" />';
	var desasociarGrupoNegocioURL = '<s:url action="asociar.desasociarGrupo" namespace="/negocio/cliente/asociar" />';
	var getClientesAsociadosURL = '<s:url action="getClientesAsociados" namespace="/negocio/cliente/asociar" />';
	var getGruposAsociadosURL = '<s:url action="getGruposAsociados" namespace="/negocio/cliente/asociar" />';
	var eliminarRelacionesNegocioURL = '<s:url action="asociar.eliminarRelacionesClienteGrupo" namespace="/negocio/cliente/asociar" />';
</script>
<script language="JavaScript" type="text/javascript" src="<s:url value='/js/negocio/cliente/asociarClienteYGrupo.js'/>"></script>

<s:hidden name="idNegocio" id="idNegocio" />

<s:textfield name="idClienteResponsable" id="idClienteResponsable" readonly="true" cssStyle="display:none;" onchange="registrarNuevoCliente(this.value)" />

<div id="indicador"></div>
<br></br>
<div id="tipoPersonaNegocio" style="padding-left: 16px;">
	<label class="small"><s:text name="midas.fuerzaventa.tipoPersona" /></label>
	<br/>
	<input name="radioTipoPersonaNegocio" id="claveTipoPersona1" value="1" type="radio"><label for="claveTipoPersona1">F�sica</label>
	<input name="radioTipoPersonaNegocio" id="claveTipoPersona2" value="2" type="radio"><label for="claveTipoPersona2">Moral</label>
	<br/>
	<div id="mensajeTipoPersonaNegocio"><s:text name="midas.negocio.cliente.ningunTipoPersona" /></div>
</div>
<div align="left" class="subtitulo" id="labelValidaClientes" >
</div>
<center>
<div id="accordionClientesNegocio" style="position: relative; height: 290px; width: 97%;"></div>
</center>

<br></br>

<div id="alinearBotonCatalogoDerecha">
	<div id="b_agregar" style="width:100px;" >
		<a href="javascript: void(0);" 
			onclick="javascript:mostrarAgregar();">
			<s:text name="midas.boton.agregar"/>
		</a>
	</div>
</div>		
<%-- <s:submit label="%{getText('midas.boton.agregar')}" onclick="javascript:mostrarAgregar();"  --%>
<!-- 		id="imgAgregar" cssClass="b_submit" type="button" cssStyle="margin-right: 15px;" /> -->

<div id="alinearBotonCatalogoDerecha">
	<div id="b_borrar" style="width:100px;" >
		<a href="javascript: void(0);"
			onclick="javascript:borrarSeleccionado();">
			<s:text name="midas.boton.borrar"/>
		</a>
	</div>
</div>	
<%-- <s:submit label="%{getText('midas.boton.borrar')}" onclick="javascript:borrarSeleccionado();"  --%>
<!-- 		id="imgBorrar" cssClass="b_submit" type="button" cssStyle="margin-right: 15px;" /> -->

<div style="float: left; margin: 0px 0px 0px 730px;">
	<div id="b_abrir" style="width:170px;" >
		<a href="javascript: void(0);"
			onclick="javascript:borrarRelaciones();">
			<s:text name="midas.negocio.cliente.borrarRelaciones"/>
		</a>
	</div>
</div>
<%-- <s:submit label="%{getText('midas.negocio.cliente.borrarRelaciones')}" onclick="javascript:borrarRelaciones();"  --%>
<!-- 		 cssClass="b_submit" type="button" id="imgAbrir" cssStyle="width:170px; margin-right: 15px;" /> -->

<script type="text/javascript">
		inicializarClientesAccordion();
		
		// llenar check Tipo de Persona, si es necesario
		if (tipoPersona == 1) {
			jQuery("#claveTipoPersona1").attr("checked", true);
			jQuery("#mensajeTipoPersonaNegocio").hide();
		} else if (tipoPersona == 2) {
			jQuery("#claveTipoPersona2").attr("checked", true);
			jQuery("#mensajeTipoPersonaNegocio").hide();
		}
		
		jQuery("#claveTipoPersona1").click(function(){
			if ( jQuery("#labelValidaClientes").html() == "Este negocio opera para los siguientes clientes �nicamente" ) {
				jQuery("#claveTipoPersona1").attr("checked", false);
				alert("Advertencia: Para realizar esta operaci�n la lista de clientes debe estar vac�a.");
			} else {
				actualizarTipoPersona("1");
			}
		});
		jQuery("#claveTipoPersona2").click(function(){
			if ( jQuery("#labelValidaClientes").html() == "Este negocio opera para los siguientes clientes �nicamente" ) {
				jQuery("#claveTipoPersona2").attr("checked", false);
				alert("Advertencia: Para realizar esta operaci�n la lista de clientes debe estar vac�a.");
			} else {
				actualizarTipoPersona("2");
			}
		});
</script>