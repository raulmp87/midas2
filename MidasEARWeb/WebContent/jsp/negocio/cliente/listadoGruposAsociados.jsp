<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
        
		<column id="idToNegocioGrupoCliente" type="ro" width="0" sort="na" hidden="true">idToNegocioGrupoCliente</column>
		<column id="idNegocio" type="ro" width="0" sort="na" hidden="true">idNegocio</column>
		<column id="grupoClientesDesc" type="ro" width="0" sort="na" hidden="true">grupoClientesDescripcion</column>
		
		<column id="nombre" type="ro" width="*" sort="str" hidden="false">Nombre</column>
		<column id="apellidoPaterno" type="ro" width="170" sort="str" hidden="true">apellidoPaterno</column>
		<column id="apellidoMaterno" type="ro" width="170" sort="str" hidden="true">apellidoMaterno</column>
		<column id="codigoCURP" type="ro" width="0" sort="na" hidden="true">codigoCURP</column>
		
	</head>
	<s:iterator value="listaNegocioGrupoCliente">
		<row id="<s:property value="grupoClientes.id" escapeHtml="false" escapeXml="true" />">
			<cell><s:property value="idToNegocioGrupoCliente" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocio.idToNegocio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="grupoClientes.idTcGrupoClientes" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="grupoClientes.descripcion" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
</rows>