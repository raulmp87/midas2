<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="-1">
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:include value="/jsp/negocio/negocioParametrosHeader.jsp"></s:include>    
<sj:head/>
<style>
<!--
div.ui-datepicker{
 font-size:10px;
}

-->
</style>
<script type="text/javascript">
function validarParametrosGrales(){
	var valido = true;

	if(!jQuery("#aplicaUsuarioExterno").is(":checked") && !jQuery("#aplicaUsuarioInterno").is(":checked")){
		valido = false;
	}	
	return valido;
}


function guardarParametrosGrales() {
	if(validarParametrosGrales()){
		var pctMaximoDescuento = jQuery("#pctMaximoDescuento").val();
		var pctDescuentoDefault = jQuery("#pctDescuentoDefault").val();
		var pctPrimaCeder = jQuery("#pctPrimaCeder").val();
		var pctUdiPromotor = jQuery("#pctUdiPromotor").val();
		var pctUdiAgente = jQuery("#pctUdiAgente").val();
		
		if(pctMaximoDescuento > 100 || pctDescuentoDefault > 100 || pctPrimaCeder > 100 ||
			pctUdiPromotor > 100 || pctUdiAgente > 100){
			parent.mostrarMensajeInformativo("No puede haber porcentajes mayores a 100.", "20", null);
		}else{ 		
			var url = '<s:url action="guardarParametrosGrales" namespace="/negocio"/>';
			url = url + '?' + jQuery(document.parametrosNegocioForm).serialize();	
			parent.gridDatosNeg001.attachURL(url);
		}
	}else{
		parent.mostrarMensajeInformativo("Favor de seleccionar al menos un Tipo de Usuario.", "20", null);
	}
}
</script>
<s:form action="guardarParametrosGrales" id="parametrosNegocioForm">
	<table border=0 id="agregar" bgcolor="#EDFAE1">			
		<tr>
			<td class="titulo">
				<s:hidden name="negocio.fechaInicioVigencia" format="dd/MM/yyyy"/>
				<s:hidden name="negocio.fechaFinVigencia" format="dd/MM/yyyy"//>
				<s:hidden name="negocio.fechaCreacion" format="dd/MM/yyyy"//>
				<s:hidden name="negocio.claveEstatus"/>
				<s:hidden name="claveNegocio"/>
				<s:hidden name="id" id="idNegocio" />	
				Par�metros Generales
			</td>
		</tr>		
		<tr>	
			<th width="5%">
				<s:text name="midas.negocio.paramGrales.pctMaximoDescuento"/>
				&nbsp;<font color="red">*</font>
			</th>			
		    <td>
		    	<s:textfield cssClass="txtfield jQpercent jQrestrict jQrequired jQpositive"
		    		name="negocio.pctMaximoDescuento" id="pctMaximoDescuento"
					maxlength="5"  labelposition="left"
					labelposition="%{getText('label.position')}"/>
			</td>
		</tr>	
		<tr>
		<th width="5%">
				<s:text name="midas.negocio.paramGrales.pctDefault"/>
				&nbsp;<font color="red">*</font>
			</th>			
		    <td>
		    	<s:textfield cssClass="txtfield jQrestrict jQrequired"
		    		name="negocio.pctDescuentoDefault" id="pctDescuentoDefault"
					maxlength="5"  labelposition="left"
					labelposition="%{getText('label.position')}"/>
			</td>
			<th>
				<s:text name="midas.negocio.paramGrales.pctPrimaCeder" />
				&nbsp;<font color="red">*</font>
			</th>			
			<td>
				<s:textfield cssClass="txtfield jQpercent jQrestrict jQrequired jQpositive"
					name="negocio.pctPrimaCeder" id="pctPrimaCeder"  
				    maxlength="5" labelposition="left"/>	
		    </td>
		</tr>
		<tr>
           	<th>
           		<s:text name="midas.negocio.paramGrales.formulaDividendos" />
           		&nbsp;<font color="red">*</font>
           	</th>			
			<td>
			    <s:textfield cssClass="txtfield alphaextra jQrequired jQpositive" 
			      name="negocio.formulaDividendos" id="negocio.formulaDividendos"  
			      labelposition="left" labelposition="%{getText('label.position')}"/>	
			</td>
			<th>
				<s:text name="midas.negocio.paramGrales.diasRetroactividad" />
				&nbsp;<font color="red">*</font>
			</th>		
			<td>
			    <s:textfield cssClass="txtfield jQnumeric jQrestrict jQrequired jQpositive" 
			       name="negocio.diasRetroactividad" id="negocio.diasRetroactividad"  
			       maxlength="5" labelposition="left"  
			       labelposition="%{getText('label.position')}"  
			      />	
			</td>					
		 </tr>	
		 <tr>
            	<th>
            		<s:text name="midas.negocio.paramGrales.diasDiferimiento" />
            		&nbsp;<font color="red">*</font>
            	</th>
			<td>
			    <s:textfield cssClass="txtfield jQnumeric jQrestrict jQrequired" 
			       name="negocio.diasDiferimiento" id="negocio.diasDiferimiento"  
			       maxlength="5" labelposition="left"
				   labelposition="%{getText('label.position')}"  
				   />	
			</td>
			<th>
				<s:text name="midas.negocio.paramGrales.diasGraciaPoliza" />
				&nbsp;<font color="red">*</font>
			</th>
			<td>
			    <s:textfield cssClass="txtfield jQnumeric jQrestrict jQrequired" 
			       name="negocio.diasGraciaPoliza" id="negocio.diasGraciaPoliza"  
			       maxlength="5" labelposition="left"  
				   labelposition="%{getText('label.position')}" 
				 />	
			</td>					
		</tr>
		<tr>
        	<th>
            	<s:text name="midas.negocio.paramGrales.diasGraciaPolizaSubsecuentes" />
            	&nbsp;<font color="red">*</font>
            </th>
			<td>
   				<s:textfield cssClass="txtfield jQnumeric jQrestrict jQrequired" 
     	 				name="negocio.diasGraciaPolizaSubsecuentes" 
     	 				id="negocio.diasGraciaPolizaSubsecuentes"  
     	 				maxlength="5" labelposition="left"  
   					labelposition="%{getText('label.position')}"
  				/>	
 			</td>
  			<th>
  				<s:text name="midas.negocio.paramGrales.diasPlazoAlUsuario" />
  				&nbsp;<font color="red">*</font>
  			</th>
			<td>
   				<s:textfield cssClass="txtfield jQnumeric jQrestrict jQrequired" 
      					name="negocio.diasPlazoUsuario" 
      					id="negocio.diasPlazoUsuario"  
      					maxlength="5" labelposition="left"  
   					labelposition="%{getText('label.position')}"  
   				/>
			</td>					
		</tr>		   
        <tr>
        	<th><s:text name="midas.negocio.tarifaBase" /></th>
		  	<td>
		  		<s:checkbox name="negocio.claveManejaUdi" 
		  	   		key="" 
		  	   		labelposition="left" 
		  	   		value="negocio.claveManejaUdi"></s:checkbox>
		  	</td>
			<th>
				<s:text name="midas.negocio.paramGrales.claveporcentajemontoudi" />
			</th>
			<td>
				<s:checkbox name="negocio.clavePorcentajeMontoUdi" 
			   		key="" 
			   		labelposition="left" 
			   		value="negocio.clavePorcentajeMontoUdi">
			  	</s:checkbox>	
			</td>					
		</tr>
		<tr>
        	<th><s:text name="midas.negocio.paramGrales.valorUdi" />&nbsp;<font color="red">*</font></th>
        
			<td>
	    		<s:textfield cssClass="txtfield jQfloat jQrestrict jQrequired jQpositive"
	       			name="negocio.valorUdi" id="negocio.valorUdi"  
	       			labelposition="%{getText('label.position')}"
	       			maxlength="5" labelposition="left" 
	       		/>
			</td>
	   		<th><s:text name="midas.negocio.paramGrales.pctUdiPromotor" />&nbsp;<font color="red">*</font></th>
			<td>
	    		<s:textfield cssClass="txtfield jQpercent jQrestrict jQrequired jQpositive"
	       			name="negocio.pctUdiPromotor" id="pctUdiPromotor"  
	       			maxlength="5" labelposition="left"  
	       			labelposition="%{getText('label.position')}"
		    	/>	
			</td>					
		</tr>
        <tr>
        	<th><s:text name="midas.negocio.paramGrales.pctUdiAgente" />&nbsp;<font color="red">*</font></th>
			<td>
			    <s:textfield cssClass="txtfield jQpercent jQrestrict jQrequired jQpositive"
			       name="negocio.pctUdiAgente" id="pctUdiAgente"  
			       maxlength="5" labelposition="left"  
			       labelposition="%{getText('label.position')}"
				/>	
			</td>
			<th><s:text name="midas.negocio.paramGrales.claveudiincluyeiva" /></th>
			<td>
	            <s:checkbox name="negocio.claveUdiIncluyeIva" 
	               key="" 
	               labelposition="left" 
	               value="negocio.claveUdiIncluyeIva">
	             </s:checkbox>	
			</td>					
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.paramGrales.descripcionNegocio" />
				 &nbsp;<font color="red">*</font>
			</th>
			<td>
				<s:textfield cssClass="txtfield jQalphaextra jQrequired"
					       name="negocio.descripcionNegocio" id="negocio.descripcionNegocio"  
					       maxlength="200"
					       labelposition="left"
				/>
			</td>
			<th><s:text name="midas.negocio.paramGrales.versionNegocio" />&nbsp;<font color="red">*</font></th>
			<td>
			    <s:textfield cssClass="txtfield jQrequired"
			       name="negocio.versionNegocio" id="negocio.versionNegocio"  
			       maxlength="5" labelposition="left"
			    />	
			</td>	
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.usuarioexterno" />
			</th>
			<td>
				<s:if test="!negocio.aplicaUsuarioExterno && !negocio.aplicaUsuarioInterno">
					<s:checkbox id="aplicaUsuarioExterno" name="negocio.aplicaUsuarioExterno" value="negocio.aplicaUsuarioExterno" checked="checked" />	
				</s:if>
				<s:else>
					<s:checkbox id="aplicaUsuarioExterno" name="negocio.aplicaUsuarioExterno" value="negocio.aplicaUsuarioExterno" />
				</s:else>
				
			</td>
			<th><s:text name="midas.negocio.usuariointerno" /></th>
			<td>
			    <s:checkbox id="aplicaUsuarioInterno" name="negocio.aplicaUsuarioInterno" value="negocio.aplicaUsuarioInterno" />	
			</td>	
		</tr>
		<tr>
			<th><s:text name="midas.negocio.aplicaPctPagoFraccionado" />
			</th>
			<td>
				<s:checkbox id="aplicaPagoFraccionado" name="negocio.aplicaPctPagoFraccionado" value="negocio.aplicaPctPagoFraccionado"/>
			</td>
			<th><s:text name="midas.negocio.fechaFinVigenciaFija"/></th>
			<td>
			
			<sj:datepicker name="negocio.fechaFinVigenciaFija" cssStyle="width: 170px;"
						required="#requiredField" buttonImage="../img/b_calendario.gif"
                        id="fechaFinVigenciaFija"
                        changeMonth="true"
                        changeYear="true"
						maxlength="10" cssClass="txtfield"
						size="12"
						disabled="negocioTieneVigenciasAsociadas"
						onkeypress="return soloFecha(this, event, false);"
 						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"></sj:datepicker>
			</td>
		</tr>
		<tr>
			<th><s:text name="midas.negocio.aplicaCurpObligatorio" /></th>
			<td>
				<s:checkbox id="aplicaCurpObligatorio" name="negocio.aplicaCURPObligatorio" value="negocio.aplicaCURPObligatorio"/>
			</td>
			<th>
				<s:text name="midas.negocio.reciboGlobal" />
			</th>
			<td>
				<s:checkbox id="aplicaReciboGlobal" name="negocio.aplicaReciboGlobal" value="negocio.aplicaReciboGlobal" />
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.aplicaValidacionExterno" />
			</th>
			<td>
				<s:checkbox id="aplicaValidacionExterno" name="negocio.aplicaValidacionExterno" value="negocio.aplicaValidacionExterno" />
			</td>
			<th><s:text name="midas.negocio.aplicaValidacionRecibo" /></th>
			<td>
			    <s:checkbox id="aplicaValidacionRecibo" name="negocio.aplicaValidacionRecibo" value="negocio.aplicaValidacionRecibo" />	
			</td>	
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.habilitaAgrupacionRecibos" />
			</th>
			<td>
				<s:checkbox id="habilitaAgrupacionRecibos" name="negocio.habilitaAgrupacionRecibos" value="negocio.habilitaAgrupacionRecibos" />
			</td>
			<th>
				<s:text name="midas.negocio.aplicaValidacionNumeroSerie" />
			</th>
			<td>
				<s:checkbox id="aplicaValidacionNumeroSerie" name="negocio.aplicaValidacionNumeroSerie" value="negocio.aplicaValidacionNumeroSerie" />
			</td>						
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.aplicaValidacionArticulo140" />
			</th>
			<td>
				<s:checkbox id="aplicaValidacionArticulo140" name="negocio.aplicaValidacionArticulo140" value="negocio.aplicaValidacionArticulo140" />
			</td>
			<th><s:text name="midas.negocio.aplicaValidacionIgualacionPrimas" /></th>
			<td>
			    <s:checkbox id="aplicaValidacionIgualacionPrimas" name="negocio.aplicaValidacionIgualacionPrimas" value="negocio.aplicaValidacionIgualacionPrimas" />	
			</td>	
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.identificacionPoliza" />
			</th>
			<td>
				<s:checkbox id="aplicaPolizaGobierno" name="negocio.aplicaPolizaGobierno" value="negocio.aplicaPolizaGobierno" />
			</td>
			<th>
				<s:text name="midas.negocio.aplicaEnlace" />
			</th>
			<td>
				<s:checkbox id="aplicaEnlace" name="negocio.aplicaEnlace" value="negocio.aplicaEnlace" />
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.numeroDiasEspera"/>
			</th>
			<td>
				<s:textfield cssClass="txtfield jQpositive" name="negocio.numeroDiasEspera" id="diasEspera" maxlength="2"  labelposition="left" labelposition="%{getText('label.position')}"/>
			</td>
			<th>
				<s:text name="midas.negocio.correoObligatorio"/>
			</th>
			<td>
				<s:checkbox id="correoObligatorio" name="negocio.correoObligatorio"/>
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.aplicaSucursal"/>
			</th>
			<td>
				<s:checkbox id="aplicaSucursal"  name="negocio.aplicaSucursal" value="negocio.aplicaSucursal" />
			</td>
			<th>
				<s:text name="midas.negocio.aplicaEndosoConductoCobro" />
			</th>
			<td>
				<s:checkbox id="aplicaEndosoConductoCobro" name="negocio.aplicaEndosoConductoCobro" value="negocio.aplicaEndosoConductoCobro" />
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.aplicaValidacionPersonaMoral"/>
			</th>
			<td>
				<s:checkbox id="aplicaValidacionPersonaMoral"  name="negocio.aplicaValidacionPersonaMoral" value="negocio.aplicaValidacionPersonaMoral" />
			</td>
			<th>
				<s:text name="midas.negocio.aplicaDiasRetroCanCfp"/>
			</th>
			<td>
				<s:checkbox id="aplicaDiasRetroCanCfp"  name="negocio.aplicaDiasRetroCanCfp" value="negocio.aplicaDiasRetroCanCfp" />
			</td>
		</tr>
		<tr>
		  	<td>&nbsp;</td>
		  	<td>&nbsp;</td>
		  	<td>&nbsp;</td>
		  	<td>
		  		<div style="float: left; margin: 0px 0px 0px 180px;">
					<div id="b_guardar" style="width:100px;" >
						<a href="javascript: void(0);"
							onclick="javascript:guardarParametrosGrales();">
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
				</div>
		  	</td>
<!-- 		  	<td>	 -->
<%-- 				<s:set id="claveTextoBoton" value="%{getText('midas.boton.guardar')}" />  --%>
<%-- 				<s:set id="accionJsBoton" value="'guardarParametrosGrales();'" />					 --%>
<%-- 				<s:submit onclick="if(validateAuto($(this), true)){return true;}else{return false;}"  --%>
<!-- 					value="%{#claveTextoBoton}" cssClass="b_submit"  -->
<!-- 					type="button" id="imgGuardar" />						 -->
<!-- 			</td>	 -->
		</tr>
		<tr height="90px">
		</tr>
	</table>	
</s:form>
<script>
jQuery(document).ready(function(){
	var fieldPicker = document.getElementById("fechaFinVigenciaFija");
		// iframePicker = document.getElementsByTagName("iframe")[0].contentWindow;
	if(fieldPicker.getAttribute("disabled")) {
		fieldPicker.parentNode.childNodes[1].setAttribute("disabled", "disabled");
		jQuery('.ui-datepicker-trigger').attr('disabled',true);
	}	
});

</script>
<style>
	input[disabled] + button {
		display: none;
		visibility: hidden;
		width: 0;
		overflow: hidden;
		height: 0;
		z-index: -99999;
		opacity: 0;
	}
</style>
		