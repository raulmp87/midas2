<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<s:include value="/jsp/negocio/derechos/derechosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/derechos/negocioderechos.js'/>"></script>
<script src="<s:url value='/js/midas2/negocio/negocio.js'/>"></script>
<script src="<s:url value='/js/validaciones.js'/>"></script>
<script src="<s:url value='/js/midas.js"'/>"></script>

<script type="text/javascript">
    var listarFiltradoNegocioPath = '<s:url action="listarFiltrado" namespace="/negocio"/>';
	var verDetalleNegocioPath = '<s:url action="verDetalle" namespace="/negocio"/>';
	var verMostrarVentanaProductosPath = '<s:url action="mostrarAsignarProducto" namespace="/negocio/producto"/>';
	var verListaProductosPath = '<s:url action="listarProductos" namespace="/negocio"/>';
	var guardarNegocioPath = '<s:url action="guardar" namespace="/negocio"/>';
	var listarNegocioPath = '<s:url action=" listar" namespace="/negocio"/>';
</script>

<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script src="<s:url value='/js/jQValidator.js'/>"></script>