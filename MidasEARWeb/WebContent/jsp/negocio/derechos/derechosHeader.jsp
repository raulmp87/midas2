<%@taglib prefix="s" uri="/struts-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
	<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
	
	<script src="<s:url value='/js/midas2/negocio/derechos/negocioderechos.js'/>"></script>
	
	<script type="text/javascript">
		var mostrarDerechosEndosoPath = '<s:url action="mostrarDerechosEndoso" namespace="/negocio/derechos"/>';
		var obtenerDerechosEndosoPath = '<s:url action="obtenerDerechosEndoso" namespace="/negocio/derechos"/>';
		var accionSobreDerechosEndosoPath = '<s:url action="accionSobreDerechosEndoso" namespace="/negocio/derechos"/>';
			
		var mostrarDerechosPolizaPath = '<s:url action="mostrarDerechosPoliza" namespace="/negocio/derechos"/>';
		var obtenerDerechosPolizaPath = '<s:url action="obtenerDerechosPoliza" namespace="/negocio/derechos"/>';
		var accionSobreDerechosPolizaPath = '<s:url action="accionSobreDerechosPoliza" namespace="/negocio/derechos"/>';			
	</script>