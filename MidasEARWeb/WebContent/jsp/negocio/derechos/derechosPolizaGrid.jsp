<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
<!-- 			<call command="attachEvent"><param>onRowCreated</param><param>registrarCoberturaPaquete</param></call> -->
		</beforeInit>
		<column id="idToNegDerechoPoliza" type="ro" width="0" hidden="true">idDerecho</column>
		<column id="importeDerecho" type="edn" format="0.00" width="*"  sort="int">Importe Derecho</column>
		<column id="claveDefault" type="ra" width="200" align ="center" sort="int">Clave Default</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="derechosPoliza">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegDerechoPoliza" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="importeDerecho" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="claveDefault" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>