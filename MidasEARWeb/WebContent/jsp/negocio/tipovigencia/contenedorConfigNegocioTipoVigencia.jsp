<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/js/midas2/negocio/tipovigencia/configNegocioTipoVigencia.js'/>"></script>
<script type="text/javascript">
	var URL_LISTAR_VIGENCIAS_ASOCIADAS = '<s:url action="obtenerTiposVigenciaAsociados" namespace="/negocio/tipovigencia"/>';
	var URL_LISTAR_VIGENCIAS_DISPONIBLES = '<s:url action="obtenerTiposVigenciaDisponibles" namespace="/negocio/tipovigencia"/>';
	var URL_CAMBIAR_VIGENCIA_DEFAULT = '<s:url action="cambiarVigenciaDefault" namespace="/negocio/tipovigencia"/>';
	var URL_ASOCIAR_TODAS = '<s:url action="asociarTodas" namespace="/negocio/tipovigencia"/>';
	var URL_DESASOCIAR_TODAS = '<s:url action="desasociarTodas" namespace="/negocio/tipovigencia"/>';
	var URL_ELIMINAR_VIGENCIA = '<s:url action="eliminarVigencia" namespace="/negocio/tipovigencia"/>';	
	var URL_DESASOCIAR_UNA = '<s:url action="desasociarUna" namespace="/negocio/tipovigencia"/>';
	var URL_RELACIONAR_VIGENCIA = '<s:url action="relacionarTipoVigencia" namespace="/negocio/tipovigencia"/>';
	var URL_MOSTRAR_VENTANA =  '<s:url action="mostrarAgregarVigencia" namespace="/negocio/tipovigencia"/>';	
</script>
<div id="contenido_vigencias">
	<div class="titulo">
		<s:text name='midas.negocio.tipovigencia.title' />
	</div>
	<div id="divM">
		<s:form id="configNegocioTipoVigenciaForm">
			<table width="100%" height="100%">
				<tbody>
					<tr align="center" valign="middle">
						<td>
							<div>
								<div class="titulo">
									<s:text name='midas.negocio.tipovigencia.title.asociadas' />
								</div>
								<div id="tiposVigenciaAsociadasGrid" class="dataGridConfigurationClass" style="float: left; width: 500px; height: 200px;">
								</div>
							</div>
						</td>
						<td align="center" valign="middle" width="10%">
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="asociarTodas();" href="javascript: void(0);"><<</a>
							</div>
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="desasociarTodas();" href="javascript: void(0);">>></a>
							</div>
						</td>
						<td>
							<div>
								<div class="titulo">
									<s:text name='midas.negocio.tipovigencia.title.disponibles' />
								</div>
								<div id="tiposVigenciaDisponiblesGrid" class="dataGridConfigurationClass" style="float: right; width: 500px; height: 200px;">
								</div>
							</div>
						</td>
						<td>
							<div>
								<div id="b_agregar" style="width:100px;" >
									<a href="javascript: void(0);" onclick="javascript:mostrarVentanaAgregar();"><s:text name="midas.boton.agregar"/></a>
								</div>								
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</s:form>
		
	</div>
</div>
<style type="text/css">
	#contenido_vigencias{
		margin-left: 5px; 
		margin-top: 3px; 
		width: 99%; 
		height: 95%;
	}
	#divM{
		float: left; 
		width: 100%; 
		height: 55%;
		margin-top: 10px;
	}
	.btnActionForAll{
		margin-top: 10px;
	}
</style>
<script type="text/javascript">	
	jQuery(document).ready(
		function() {		
			initGridsTiposVigencia();
	});
</script>