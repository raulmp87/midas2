<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css"/>
<s:head />
<script language="JavaScript" src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script src="<s:url value='/js/jQValidator.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/negocio/tipovigencia/configNegocioTipoVigencia.js"/>"></script>
<div id="detalle">
	<center>
		<s:hidden name="tipoVigencia" id="id" />
		<s:form method="post"id="guardarTipoVigenciaForm" action="/negocio/tipovigencia/agregarVigencia.action">
			<table id="agregar" width="100%">
				<tr>
					<th><s:text name="midas.negocio.tipovigencia.ventana.descripcion"></s:text>&nbsp;<font color="red">*</font></th>
					<td><s:textfield  id="descripcion" name="tipoVigencia.descripcion" onkeyup="javascript:this.value=this.value.toUpperCase();" size="30" maxLength="500" cssClass="txtfield jQalphaextra jQrequired" ></s:textfield></td>
				</tr>
				<tr>
					<th><s:text name="midas.negocio.tipovigencia.ventana.dias"></s:text>&nbsp;<font color="red">*</font></th>
					<td><s:textfield id="dias" name="tipoVigencia.dias" type="number" size="30" maxLength="10" cssClass="txtfield jQnumeric jQrequired"></s:textfield></td>
				</tr>
				<tr>
					<td colspan="2">						
						<div id="b_guardar">							
							<s:submit key="midas.boton.guardar" name="submit"></s:submit>
						</div>
					</td>
				</tr>
			</table>
		</s:form>
	</center>
</div>
<style>
#b_guardar {
	display: block;
	width: 70px;
	float: left;
	background-image: url(/MidasWeb/img/bg1.png);
	background-repeat: repeat-x;
	background-position: bottom;
}
#b_guardar input {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7.5pt;
	font-weight: bold;
	color: #666666;
	text-align: center;
	vertical-align: middle;
	line-height: 15px;
	display: block;
	text-decoration: none;
	background-image: url(/MidasWeb/img/btn_guardar.gif);
	background-repeat: no-repeat;
	background-position: right 50%;
	height: 18px;
	padding-right: 15px;
	padding-left: 5px;
	border: 1px solid #CCCCCC;
}
#b_guardar input:hover {
	color: #006600;
	border: 1px solid #999999;
}
</style>