<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>
<rows>	
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="negocioTipoVigencia.id" type="ro" align="center" width="0" hidden="true"><s:text name='midas.negocio.tipovigencia.title.id'/></column>
		<column id="negocioTipoVigencia.negocio.idToNegocio" type="ro" align="center" width="0" hidden="true"><s:text name='midas.negocio.tipovigencia.title.id'/></column>
		<column id="negocioTipoVigencia.tipoVigencia.id" type="ro" align="center" width="0" hidden="true"><s:text name='midas.negocio.tipovigencia.title.id'/></column>
		<column id="negocioTipoVigencia.tipoVigencia.descripcion" type="ro" align="center" width="*" sort="str"><s:text name='midas.negocio.tipovigencia.title.descripcion'/></column>
		<column id="negocioTipoVigencia.tipoVigencia.dias" type="ro" align="center" width="230" sort="str"><s:text name='midas.negocio.tipovigencia.title.dias'/></column>
		<column id="negocioTipoVigencia.esDefault" type="ra" align="center" width="100" sort="int" hidden="true"><s:text name='midas.negocio.tipovigencia.title.default'/></column>
		<column id="accionEliminar" type="img" width="30px" sort="na" align="center"></column>
	</head>
	
	<s:iterator value="vigenciasDisponibles" status="row">
		
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocio.idToNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoVigencia.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoVigencia.descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoVigencia.dias" escapeHtml="false" escapeXml="true"/></cell>
			<cell></cell>	
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarTipoVigencia(<s:property value="tipoVigencia.id"/>)^_self</cell>
		</row> 
	</s:iterator>
</rows>