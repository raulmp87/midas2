<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

	<center>
		<s:form action="relacionarNegocioTarifa"  id="negocioTarifaDetalleForm">
			<table id="desplegar" border="0" style="background-color: white;">
					<th><s:text name="midas.catalogos.descripcion"></s:text></th>
					<th>Agrupador de Tarifa</th>
					<th><s:text name="midas.general.version"></s:text></td></th>
					<th>Acciones</th>
				<s:iterator value="relacionesNegocioTarifaDTOList" status="stats"  >
				<s:hidden id="negocioSeccion_%{#stats.count}" name="idToNegSeccions" value="%{negocioSeccion.idToNegSeccion}"></s:hidden>
				<s:hidden id="monedaDTO_%{#stats.count}" name="idTcMonedas"  value="%{monedaDTO.idTcMoneda}"></s:hidden>
					<tr  class="bg_t2">
					<s:if test="negocioAgrupadorTarifaSeccion.idToAgrupadorTarifa != null">
						<td width="50%" ><s:property value="negocioSeccion.seccionDTO.descripcion" />- <s:property value="monedaDTO.descripcion" />
						</td>
					<td>
						
					<s:select id="idToNegAgrupTarifaSeccion_%{#stats.count}"
							list="posibles"
							name="idAgrupadores" 
							cssStyle="width:200px;max-width: 200px;"
							cssClass="cajaTexto jQrequired"
							listKey="id.idToAgrupadorTarifa" listValue="agrupadorTarifa.descripcionAgrupador"
							labelposition="left" value="negocioAgrupadorTarifaSeccion.idToAgrupadorTarifa"
							headerKey="" headerValue="Seleccione"
							 required="#requiredField"
							 onchange="onChangeAgrupadorTarifa(%{negocioSeccion.idToNegSeccion},%{monedaDTO.idTcMoneda},this.value,%{#stats.count});"
							 />
					</td>
				<td>
				<s:iterator value="posibles" status="stat">
					<s:if test="id.idToAgrupadorTarifa == negocioAgrupadorTarifaSeccion.idToAgrupadorTarifa">
					<s:select id="idToNegAgrupTarifaSeccionVersion_%{#stats.count}"
								list="posibles[#stat.index].agrupadorVersion"
 								name="idVersiones"  
 								cssStyle="width:200px;;max-width: 200px;"
 								cssClass="cajaTexto jQrequired"
								listKey="id.idVerAgrupadorTarifa" listValue="descripcionVersion+' '+descripcionEstatus"
								labelposition="left" value="negocioAgrupadorTarifaSeccion.idVerAgrupadorTarifa"
								headerKey="" headerValue="Seleccione"
								 required="#requiredField"
								 />
					</s:if>
				</s:iterator>

				</td>
				</s:if>
				<s:else>
				<td width="50%" ><s:property value="negocioSeccion.seccionDTO.descripcion" />- <s:property value="monedaDTO.descripcion" />
				</td>
				<td>
					<s:select id="idToNegAgrupTarifaSeccion_%{#stats.count}"
							list="posibles"
							name="idAgrupadores" 
							listKey="id.idToAgrupadorTarifa" listValue="agrupadorTarifa.descripcionAgrupador"
							labelposition="left" value="negocioAgrupadorTarifaSeccion.idToAgrupadorTarifa"
							headerKey="-1" headerValue="Seleccione"
							 required="#requiredField"
							 onchange="onChangeAgrupadorTarifa(%{negocioSeccion.idToNegSeccion},%{monedaDTO.idTcMoneda},this.value,%{#stats.count});"
							 />
				</td>
				<td>
						<s:select id="idToNegAgrupTarifaSeccionVersion_%{#stats.count}"
							list="posibles"
							name="idVersiones" 
							listKey="id.idToAgrupadorTarifa" listValue="agrupadorTarifa.descripcionAgrupador + ' ' + agrupadorTarifa.descripcionVersion+' '+agrupadorTarifa.descripcionEstatus"
							labelposition="left" value="negocioAgrupadorTarifaSeccion.idToAgrupadorTarifa"
							headerKey="-1" headerValue="Seleccione"
							 required="#requiredField"
							 />
				</td>
				</s:else>
					<td width="50px;">
						<div class="row">
							<div class="" style="width: 40px;">
								<div class="btn_back"
									style="display: block; float: left; width: 20px;">
									<a href="javascript: void(0);" class="icon_buscar"
										title="<s:text name="midas.boton.buscar" /> otras versiones"
										onclick="buscarOtrasVersiones(<s:property value='negocioSeccion.idToNegSeccion'/>,<s:property value='monedaDTO.idTcMoneda'/>,<s:property value='%{#stats.count}' />)">
									</a>
								</div>
							</div>
						</div></td>
				</tr>
				</s:iterator> 	
			</table>
		</s:form>
	<div class="row" style="margin-left: -31px;">
		<div class="btn_back w140" style="display: inline; float: right;">
			<a href="javascript: void(0);" onclick="guardarVersiones();"
				class="icon_guardar2"> <s:text name="midas.boton.guardar" /> </a>
		</div>
	</div>
</center>
	