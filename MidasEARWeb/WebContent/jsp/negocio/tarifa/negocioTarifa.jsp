<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/negocio/tarifa/negocioTarifaHeader.jsp"></s:include>

<div id="detalle" name="Detalle">
	<center>
		<s:form >
			<s:hidden name="idToNegProducto" /> 
			
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="3">
						<s:text name="midas.negocio.tarifa.tarifa" /> 
					</td>
				</tr>				 
				<tr>
					<th width="10%">
						<s:text name="midas.negocio.seccion.tiposPoliza" />
					</th>
					<td width="30%">
					    <s:select id="idToNegTipoPoliza" cssClass="cajaTexto" 
							      list="negocioTipoPolizaList"
							      name="idToNegTipoPoliza" 
							      listKey="idToNegTipoPoliza" listValue="tipoPolizaDTO.descripcion"
							      labelposition="left" headerKey="-1"
							      headerValue="Seleccione" required="#requiredField"
							      OnChange="onChangeComboTipoPoliza(this.value);" />
					</td>
					<td width="60%">&nbsp;</td>					
				</tr>
				<tr>
					<td colspan="3">
						<div id="loadingIndicatorComps" style="display: none;"></div>					
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div id= "detalleNegocioTarifa"></div>					
					</td>
				</tr>
			</table>
		</s:form>
		
	</center>
	
</div>
