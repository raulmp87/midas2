<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/tarifa/negocioTarifa.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript">
	var obtenerNegocioSeccionesPath = '<s:url action="obtenerNegocioSecciones" namespace="/negocio/tarifa"/>';
    var relacionarNegocioTarifaPath = '<s:url action="relacionarNegocioTarifa" namespace="/negocio/tarifa"/>';
</script>