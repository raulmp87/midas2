<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxcommon.js'/>"></script>

<script type="text/javascript"
	src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>

<script type="text/javascript"
	src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/dhtmlxgrid_excell_acheck.js'/>"></script>

<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>'></script>
<script type="text/javascript"
	src='<s:url value="/js/validaciones.js"/>'></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>"
	charset="ISO-8859-1"></script>

<script	src="<s:url value='/js/midas2/negocio/impresion/negocioUsuarioEdicionImpresion.js'/>"></script>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
</style>
<div id="contenido_condicionespecial">
	<div class="titulo"><s:text name='midas.impresionpoliza.edicion.negociousuario.titulo'/></div>
	
	<div id="divM">
		<s:form id="negocioUsuarioEdicionImpresionForm">
			<table width="100%" height="100%">
				<tbody>
					<tr align="center" valign="middle">
						<td>
							<div>
							<div class="titulo">Usuarios Disponibles</div>
								<div id="negocioUsuarioEdicionImpresionDisponibleGrid" style="width: 500px; height: 240px;">
								</div>
							</div>							
						</td>
						<td align="center" valign="middle" width="8%">
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="desasociarTodas();" alt="Desligar todas" href="javascript: void(0);"> << </a>
							</div>
							<div class="btn_back w40 btnActionForAll">
								<a class="" onclick="asociarTodas();" alt="Asociar todas" href="javascript: void(0);"> >> </a>
							</div>
						</td >
						<td>
							<div>
								<div class="titulo">Usuarios Asociados</div>
								<div id="negocioUsuarioEdicionImpresionAsociadoGrid" style="width: 500px; height: 240px;">
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</s:form>
	</div>
</div>

<style type="text/css">
	#contenido_condicionespecial{
		margin-left: 5px; 
		margin-top: 3px; 
		width: 99%; 
		height: 95%;
	}
	
	#divS{
		float: left; 
		width: 100%; 
		height: 20%;
	}
	
	#divM{
		float: left; 
		width: 100%; 
		height: 55%;
		margin-top: 10px;
	}
	
	.btnActionForAll{
		margin-top: 10px;
	}
	
</style>

<script type="text/javascript">
	initGridsNegocioUsuarioEdicionImpresion();
</script>