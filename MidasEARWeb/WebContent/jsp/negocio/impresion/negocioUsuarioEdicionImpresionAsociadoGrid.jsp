<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
			<call command="attachHeader"><param>,#text_search,#text_search,,</param></call>
		</beforeInit>
		<column id="negocioUsuarioImpresion.id" 			 type="ro" align="center" width="0"	hidden="true">id</column>
		<column id="negocioUsuarioImpresion.claveUsuario"	 type="ro" align="center" width="*"	sort="str"	>Clave Usuario</column>
	    <column id="negocioUsuarioImpresion.nombreUsuario"	 type="ro" align="center" width="230"	sort="str"	>Nombre Usuario</column>
		<column id="negocioUsuarioImpresion.permisoImprimir" type="ch" align="center" width="80" sort="int"	>Imprimir</column>
		<column id="negocioUsuarioImpresion.permisoEditar" 	 type="ch" align="center" width="80" sort="int"	>Editar</column>
	</head>
	
	<% int a=0;%>
	<s:iterator value="usuariosAsociados">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="claveUsuario" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuario" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="permisoImprimir" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="permisoEditar" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>