<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioSeccion.idToNegSeccion" type="ro" width="0" sort="int" hidden="true">negocioSeccionId</column>
		<column id="negocioSeccion.negocioTipoPoliza.idToNegTipoPoliza" type="ro" width="0" sort="int" hidden="true">idToTipoPoliza</column>
		<column id="negocioSeccion.seccionDTO.idToSeccion" type="ro" width="0" sort="int" hidden="true">idToSeccion</column>
		<column id="negocioSeccion.seccionDTO.descripcion" type="ro" width="560" sort="str" ><s:text name="midas.catalogos.descripcion"/></column>
		<column id="negocioSeccion.consultaNumSerie" type="ch"  width="80"  sort="int"><s:text name="midas.catalogos.consultaNumSerie"/></column>
		<column id="accionAsociarUso" type="img" width="80" sort="na" align="center">Acciones</column>
		<column id="accionAsociarServicio" type="img" width="30" sort="na"/>
		<column id="accionAsociarEstilo" type="img" width="40" sort="na"/>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioSeccionDTO.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioTipoPoliza.idToNegTipoPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionDTO.idToSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionDTO.descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="consultaNumSerie" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_editar.gif^Asociar Tipo Uso Vehiculo^javascript: asociarNegocioSeccionTipoUso()^_self</cell>
			<cell>../img/icons/ico_verdetalle.gif^Asociar Tipo Servicio Vehiculo^javascript: mostrarNegocioSeccionTipoServicioAsociadas()^_self</cell>
			<cell>../img/icons/auto_no_siniestrado.png^Estilos^javascript: mostrarVentanaEstilos(<s:property value="idToNegSeccion" escapeHtml="false" escapeXml="true"/>,null)^_self</cell>
		</row>
	</s:iterator>	
	
</rows>
