<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>

<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<script type="text/javascript">
    var obtenerSeccionEstiloAsociadosPath = '<s:url action="obtenerEstilosAsociados" namespace="/negocio/estilovehiculo"/>';
    var obtenerSeccionEstiloDisponiblesPath = '<s:url action="obtenerEstilosDisponibles" namespace="/negocio/estilovehiculo"/>';
    var relacionarSeccionEstiloPath = '<s:url action="relacionarSeccionEstilo" namespace="/negocio/estilovehiculo"/>';
</script>