<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/negocio/seccion/estilos/seccionEstilosHeader.jsp"></s:include>
<script type="text/javascript">
	/**
	 *  Id's asociados a los Grids
	 */
	var seccionEstiloAsociadosGrid;
	var seccionEstiloDisponiblesGrid;
	var seccionEstiloProcessor;
	
	function obtenerSeccionEstiloAsociados(){
		document.getElementById("seccionEstiloAsociadosGrid").innerHTML = '';
		seccionEstiloAsociadosGrid = new dhtmlXGridObject('seccionEstiloAsociadosGrid');
		seccionEstiloAsociadosGrid.load("/MidasWeb/negocio/estilovehiculo/obtenerEstilosAsociados.action" + "?id=" + document.getElementById('id').value);
		seccionEstiloProcessor = new dataProcessor(relacionarSeccionEstiloPath);
		seccionEstiloProcessor.enableDataNames(true);
		seccionEstiloProcessor.setTransactionMode("POST");
		seccionEstiloProcessor.setUpdateMode("cell");
		seccionEstiloProcessor.attachEvent("onAfterUpdate", refrescarGridsSeccionEstilo);	
		seccionEstiloProcessor.init(seccionEstiloAsociadosGrid);
	}
	function obtenerSeccionEstiloDisponibles(){
		document.getElementById("seccionEstiloDisponiblesGrid").innerHTML = '';
		seccionEstiloDisponiblesGrid = new dhtmlXGridObject('seccionEstiloDisponiblesGrid');		
		//mostrarIndicadorCarga('indicador');	
		//seccionEstiloDisponiblesGrid.attachEvent("onXLE", function(grid){
		//	ocultarIndicadorCarga('indicador');
	    //});		
		seccionEstiloDisponiblesGrid.load(obtenerSeccionEstiloDisponiblesPath + "?id=" + document.getElementById('id').value + "&marca="+dwr.util.getValue('marcas')+"&tipo="+dwr.util.getValue('tipos')+"&idMoneda="+dwr.util.getValue('idMoneda'));
	}
	function refrescarGridsSeccionEstilo(sid, action, tid, node){
		obtenerSeccionEstiloAsociados();
		obtenerSeccionEstiloDisponibles();
		return true;
	}
	function iniciaGridsSeccionEstilos(){	
		refrescarGridsSeccionEstilo(null, null, null, null);
	}
</script>	
<s:form action="mostrarVentana">
	
	<div class="titulo">
		<s:text name="midas.negocio.seccion.estilos.asociadas"/>
	</div>
	<s:hidden name="negocioSeccion.seccionDTO.tipoVehiculo.id.claveTipoBien" id="claveTipoBien"/>
	<s:hidden name="id" id="id"/>
	
	<div id="seccionEstiloAsociadosGrid" class="dataGridConfigurationClass"></div>
	<table>
		<tr>
			<td>
				<s:select list="marcas" headerKey="" cssClass="txtfield" labelposition="%{getText('label.position')}" 
					headerValue="%{getText('midas.general.seleccione')}" name="estiloVehiculoDTO.marcaVehiculoDTO.idTcMarcaVehiculo"
					id="marcas" key="midas.negocio.seccion.estilos.marcas" required="true" cssClass="cajaTexto" 
					onchange="">
				</s:select>	
			</td>
			<td>
				<s:select list="tipos" headerKey="" cssClass="txtfield" labelposition="%{getText('label.position')}" 
				 	headerValue="%{getText('midas.general.seleccione')}" name="estiloVehiculoDTO.idTcTipoVehiculo"
				 	id="tipos" key="midas.negocio.seccion.estilos.tipos" required="true" 
					onchange=""> 
				</s:select>				
			</td>
			<td>
				<s:select list="monedas" headerKey="" cssClass="txtfield" labelposition="%{getText('label.position')}" 
				 	headerValue="%{getText('midas.general.seleccione')}" name="idMoneda"
				 	id="idMoneda" key="midas.negocio.seccion.estilos.moneda" required="true" 
					onchange=""> 
				</s:select>				
			</td>			
			<td>
				<div id="b_buscar">
					<a href="javascript: void(0);"
						onclick="javascript:iniciaGridsSeccionEstilos();">
						<s:text name="midas.boton.buscar"/>
					</a>
				</div>			
			</td>
		</tr>
	</table>
	
	<div class="titulo">
		<s:text name="midas.negocio.seccion.estilos.disponibles"/>
	</div>
	<div id="indicador"></div>
	<div id="seccionEstiloDisponiblesGrid" class="dataGridConfigurationClass"></div>
	<script type="text/javascript">
		iniciaGridsSeccionEstilos();	 
	</script>		
</s:form>