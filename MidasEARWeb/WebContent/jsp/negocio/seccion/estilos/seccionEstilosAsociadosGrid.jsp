<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioEstiloVehiculo.idToNegEstiloVehiculo" type="ro" width="0" sort="int" hidden="true">idToNegEstiloVehiculo</column>
		<column id="negocioEstiloVehiculo.negocioSeccion.idToNegSeccion" type="ro" width="0" sort="int" hidden="true">idToNegSeccion</column>
		<column id="negocioEstiloVehiculo.estiloVehiculoDTO.id.claveTipoBien" type="ro" width="0" sort="int" hidden="true">claveTipoBien</column>
		<column id="negocioEstiloVehiculo.estiloVehiculoDTO.id.claveEstilo" type="ro" width="0" sort="int" hidden="true">claveEstilo</column>
		<column id="negocioEstiloVehiculo.estiloVehiculoDTO.id.idVersionCarga" type="ro" width="0" sort="int" hidden="true">idVersionCarga</column>
		<column id="negocioEstiloVehiculo.estiloVehiculoDTO.descripcionEstilo" type="ro" width="*" sort="str" >Descripcion</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioEstiloVehiculoDTO.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegEstiloVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioSeccion.idToNegSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estiloVehiculoDTO.id.claveTipoBien" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estiloVehiculoDTO.id.claveEstilo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estiloVehiculoDTO.id.idVersionCarga" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estiloVehiculoDTO.descripcionEstilo" escapeHtml="false" escapeXml="true" /></cell>			
		</row>
	</s:iterator>	
	
</rows>
