<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioSeccion.idToNegSeccion" type="ro" width="0" sort="int" hidden="true">negocioSeccionId</column>
		<column id="negocioSeccion.negocioTipoPoliza.idToNegTipoPoliza" type="ro" width="0" sort="int" hidden="true">idToTipoPoliza</column>
		<column id="negocioSeccion.seccionDTO.idToSeccion" type="ro" width="0" sort="int" hidden="true">idToSeccion</column>
		<column id="negocioSeccion.seccionDTO.descripcion" type="ro" width="*" sort="str" ><s:text name="midas.catalogos.descripcion"/></column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioSeccionDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioTipoPoliza.idToNegTipoPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionDTO.idToSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionDTO.descripcion" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>
