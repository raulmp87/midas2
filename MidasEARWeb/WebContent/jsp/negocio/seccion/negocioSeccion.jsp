<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/negocio/seccion/negocioSeccionHeader.jsp"></s:include>

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarNegocioSeccion">
			<s:hidden name="idToNegProducto" /> 	
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="midas.negocio.seccion.lineas" /> 
					</td>
				</tr>
				 
				<tr>
				    <th width="100px">
						<s:text name="midas.negocio.seccion.tiposPoliza" />
					</th>
					<td width="300px">
					    <s:select id="idToNegTipoPoliza" cssClass="cajaTexto" 
							list="negocioTipoPolizaList"
							name="negocioTipoPoliza.idToNegTipoPoliza" 
							listKey="idToNegTipoPoliza" listValue="tipoPolizaDTO.descripcion"
							labelposition="left" headerKey="-1"
							headerValue="Seleccione" required="#requiredField"
							OnChange="obtenerLineas(this.value);" />
					</td>
					<td width="300px" >&nbsp;</td>
					<td width="300px" >&nbsp;</td>
				</tr>
				
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.seccion.asociadas" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioSeccionAsociadasGrid" style="width:80%;height: 130px"
							class="dataGridConfigurationClass"></div></td>
							
							
				</tr>
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.seccion.disponibles" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioSeccionDisponiblesGrid" style="width:80%;height:130px"
							class="dataGridConfigurationClass"></div></td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje
							clave="configuracion.asociar.arrastrar.mensaje" />
					</td>
				</tr>
			</table>
		</s:form>
	</center>
</div>

<script type="text/javascript">
	iniciaGridsNegocioSeccion();
</script>