<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioTipoServicio.idToNegTipoServicio" type="ro" width="0" sort="int" hidden="true">idToNegTipoServicio</column>
		<column id="negocioTipoServicio.negocioSeccion.idToNegSeccion" type="ro" width="0" sort="int" hidden="true">idToNegSeccion</column>
		<column id="negocioTipoServicio.tipoServicioVehiculoDTO.idTcTipoServicioVehiculo" type="ro" width="*" sort="str">Id Tipo Servicio Vehiculo</column>
	    <column id="negocioTipoServicio.tipoServicioVehiculoDTO.descripcionTipoServVehiculo" type="ro" width="*" sort="str">Descripcion Tipo ServVehiculo</column>
		<column id="claveDefault" type="ra" width="200" align ="center">Clave Default</column>
	</head>

	<% int a=0;%>
	<s:iterator value="relacionesNegocioTipoServicioDTO.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegTipoServicio" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocioSeccion.idToNegSeccion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="tipoServicioVehiculoDTO.idTcTipoServicioVehiculo" escapeHtml="false"  escapeXml="true"/></cell>
			<cell><s:property value="tipoServicioVehiculoDTO.descripcionTipoServVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveDefault" escapeHtml="false" escapeXml="true" /></cell>
		</row>
	</s:iterator>
	
</rows>