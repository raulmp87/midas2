<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/negocio/seccion/tiposervicio/negocioTipoServicioHeader.jsp"></s:include>

<s:form action="mostrarNegocioSeccionTipoServicio" namespace="/negocio/seccion/tiposervicio" id="NegocioTipoServicioForm" >
<br/>
<s:hidden name="idToNegSeccion" id="idToNegSeccion" />
	<center>
		<table id="desplegarDetalle" border="0">
		 <tr>
		  <td>
		  	<s:text name="midas.negocio.tipopoliza.seccion.tiposervicio.seleccionetiposervicioasociado"/><br>
		  </td>
		  </tr>
             <tr>
				<td colspan="4">
					<s:text name="midas.negocio.tipopoliza.seccion.tiposervicio.asociadas"/>
					
				</td>
			 </tr>

			<tr>
				<td colspan="4">
					<div id="negocioSeccionTipoServicioAsociadasGrid" class="dataGridConfigurationClass"></div>
				</td>
			</tr>
			<tr>

			<td colspan="4">
					<s:text name="midas.negocio.tipopoliza.seccion.tiposervicio.disponibles"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioSeccionTipoServicioDisponiblesGrid" class="dataGridConfigurationClass"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>
</s:form>
<script type="text/javascript">
iniciaGridsNegocioTipoServicio();
</script>