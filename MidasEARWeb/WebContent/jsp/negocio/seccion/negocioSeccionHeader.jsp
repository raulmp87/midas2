<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/seccion/negocioSeccion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/validacionService.js'/>"></script>
<script type="text/javascript">
    var obtenerNegocioSeccionAsociadasPath = '<s:url action="obtenerNegocioSeccionAsociadas" namespace="/negocio/producto/tipopoliza/seccion"/>';
    var obtenerNegocioSeccionDisponiblesPath = '<s:url action="obtenerNegocioSeccionDisponibles" namespace="/negocio/producto/tipopoliza/seccion"/>';
    var relacionarNegocioSeccionPath = '<s:url action="relacionarNegocioSeccion" namespace="/negocio/producto/tipopoliza/seccion"/>';
</script>
<script type="text/javascript">
	function obtenerLineas(id){
		validacionService.obtenerLineasNegocioAsociadas(id,function(data){
			if (!data) {
				parent.mostrarMensajeInformativo("El tipo de P\u00F3liza no tiene L\u00EDneas de Negocio asociadas ", "20", null, null);
			}else{
				onChangeComboTipoPoliza(id);
			}
		});
	}
</script>