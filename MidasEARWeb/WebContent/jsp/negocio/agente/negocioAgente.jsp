<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/negocio/agente/negocioAgenteHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<s:form action="buscar" id="negocioAgenteForm">	
	<s:textfield name="idNuevoAgente" id="idNuevoAgente" readonly="true" cssStyle="display:none;" onchange="registrarNuevoAgente(this.value)" />
	<div id="parentId" style="position:absolute; width:100%; height: 100%; display: none;"></div>
	<div align="left" class="subtitulo" id="labelValidaAgentes" >
	</div>
	<div id="columns">
		<center>
			<div id="listadoAgentesAsociados" style="position: relative; height: 290px; width: 97%;"></div>
		</center>
	</div>		
	</br>
	<div id="alinearBotonCatalogoDerecha">
		<div style="float: left; margin: 0px 0px 0px 0px;">
			<div id="b_agregar" style="width: 100px;">
				<a href="javascript: void(0);"
					onclick="javascript:agregarNegocioAgente();"> <s:text
						name="midas.boton.agregar" />
				</a>
			</div>
		</div>
		<div style="float: left; margin: 0px 0px 0px 10px;">
			<div id="b_borrar" style="width: 100px;">
				<a href="javascript: void(0);"
					onclick="javascript:eliminarNegocioAgente();"> <s:text
						name="midas.boton.borrar" />
				</a>
			</div>
		</div>
		<div style="float: left; margin: 0px 0px 0px 10px;">
			<div id="b_abrir" style="width: 170px;">
				<a href="javascript: void(0);"
					onclick="javascript:cancelarNegocioAgente();"> <s:text
						name="midas.negocio.agente.eliminar" />
				</a>
			</div>
		</div>
	</div>
</s:form>
<script type="text/javascript">
	inicializarAgentesAsociados();
</script>