<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioAgente.idToNegAgente" type="ro" width="0" sort="int" hidden="true">idToNegAgente</column>
		<column id="negocioAgente.negocio.idToNegocio" type="ro" width="0" sort="int" hidden="true">idToNegocio</column>
		<column id="negocioAgente.agente.id" type="ro" width="0" sort="int" hidden="true">idTcAgente</column>
		<column id="negocioAgente.agente.persona.nombreCompleto" type="ro" width="395px" sort="str" >Nombre</column>
		
	</head>			
	<s:iterator value="agentesDisponibles" status="row">
		<row id="<s:property value="row.index"/>">
			<cell><s:property value="idToNegAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocio.idToNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agente.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell>
				&lt;div style='text-align:left;text-decoration: none;cursor:pointer;' onclick='javascript: mostrarResumenAgente(<s:property value="#row.index"/>, <s:property value="agente.id"/>)' &gt; 					
					<s:property value="agente.persona.nombreCompleto" escapeHtml="false" escapeXml="true" />					
				&lt;/div&gt;
			</cell>		
		</row>
	</s:iterator>
	
</rows>
