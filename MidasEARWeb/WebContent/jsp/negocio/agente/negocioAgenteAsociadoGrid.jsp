<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		
		<column id="negocioAgente.idToNegAgente" type="ro" width="0" sort="int" hidden="true">idToNegAgente</column>
		<column id="negocioAgente.negocio.idToNegocio" type="ro" width="0" sort="int" hidden="true">idToNegocio</column>
		<column id="negocioAgente.agente.id" type="ro" width="0" sort="int" hidden="true">idTcAgente</column>
		<column id="negocioAgente.agente.idAgente" type="ro" width="100" sort="int" ><s:text name="midas.fuerzaventa.negocio.numeroAgente"/></column>		
		<column id="negocioAgente.agente.persona.nombreCompleto" type="ro" width="*" sort="str" ><s:text name="midas.negocio.agente.nombre"/></column>
		<column id="negocioAgente.agente.persona.razonSocial" type="ro" width="*" sort="str" ><s:text name="midas.negocio.agente.rfc"/></column>
		<column id="negocioAgente.agente.persona.curp" type="ro" width="*" sort="str" ><s:text name="midas.fuerzaventa.curp"/></column>
		<column id="idchecke" type="ch" width="35" sort="int" ></column>
	</head>			
	<s:iterator value="agentesAsociados" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idToNegAgente" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocio.idToNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agente.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="agente.idAgente" escapeHtml="false" escapeXml="true"/></cell>				
			<cell><s:property value="agente.persona.nombreCompleto" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="agente.persona.rfc" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="agente.persona.curp" escapeHtml="false" escapeXml="true" /></cell>	
			<cell><s:property value="" escapeHtml="false" escapeXml="true" /></cell>	
				
		</row>
	</s:iterator>
	
</rows>
