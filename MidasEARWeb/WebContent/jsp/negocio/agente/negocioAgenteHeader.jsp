<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">	
	var listarAgentesAsociadosPath = '<s:url action="obtenerAgentesAsociados" namespace="/negocio/agente"/>';
 	var listarAgentesDisponiblesPath = '<s:url action="obtenerAgentesDisponibles" namespace="/negocio/agente"/>';
 	var relacionarNegocioAgentePath = '<s:url action="relacionarNegocioAgentes" namespace="/negocio/agente"/>';	 	
</script>

<script src="<s:url value='/js/midas2/negocio/agente/negocioAgente.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/transporteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<style type="text/css" >
	#columns {
        width: 100%;
    }
    #columns .column {
        position: relative;
        width: 46%;
        padding: 1%;       
    }

    #columns .left {
        float: left;
    }

    #columns .right {
        float: right;
    }
</style>


