<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/negocio/grupoCliente.js'/>"></script>

<script type="text/javascript">
    var listarClienteGruposDhtmlxUrl = '<s:url action="mostrarGruposClienteGrid" />';
</script>