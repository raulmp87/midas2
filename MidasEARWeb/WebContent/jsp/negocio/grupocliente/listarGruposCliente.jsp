<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/negocio/grupocliente/grupoClienteHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>	
<s:form  id="grupoClienteForm">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
					<s:text name="Listado de Grupos de Cliente"/>	
			</td>
		</tr>
	</table>
</s:form>
<div id="indicador"></div>
<div id ="grupoClienteGrid"  style="width:97%;height:315px"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<br></br>
<div id="alinearBotonCatalogoDerecha">
      <div id="b_agregar">
            <a href="javascript: void(0);"
                  onclick="javascript:TipoAccionDTO.getAgregarModificar(agregarGrupoCliente);return false;">
                  <s:text name="midas.boton.agregar"/>
            </a>
      </div>
</div>

<script type="text/javascript">
	listarGrupoCliente();
</script>
