<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%-- <script src="<s:url value='/js/negocio/grupoCliente.js'/>"></script> --%>

<s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:if test="grupoCliente.id != null">
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.grupocliente.modificar.titulo')}" />
	</s:if>
	<s:else>
		<s:set id="tituloAccion" value="%{getText('midas.catalogos.grupocliente.agregar.titulo')}" />
	</s:else>
</s:if>
<s:elseif test="tipoAccion == catalogoTipoAccionDTO.ver">
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.grupocliente.detalle.titulo')}" />
</s:elseif>
<s:else>
    <s:set id="tituloAccion" value="%{getText('midas.catalogos.grupocliente.borrar.titulo')}" />
</s:else>


<s:url action="mostrarClientesDhtmlx" namespace="/negocio/grupo-cliente" var="mostrarClientesDhtmlxUrl">
	<s:param name="id" value="id"/>
</s:url>

<s:url action="mostrarClienteGruposDhtmlx" var="mostrarClienteGruposDhtmlxUrl"/>

<script type="text/javascript">
	var guardarGrupoClienteUrl = '<s:url action="guardar" namespace="/negocio/grupo-cliente"/>';
	var mostrarClientesDhtmlxUrl = '<s:property value="#mostrarClientesDhtmlxUrl"/>';
	var mostrarClienteGruposDhtmlxUrl = '<s:property value="#mostrarClienteGruposDhtmlxUrl"/>';
	var borrarImgUrl = '<s:url value="/img/delete14.gif"/>';
	var borrarAltTxt = '<s:text name="midas.boton.borrar"/>';
</script>

<s:form id="grupoClienteForm" >
	<s:hidden name="tipoAccion" />
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="%{#tituloAccion}"/>
				<s:text name="label.position" var="labelPosition"/> 
				<jsp:include page="/jsp/catalogos/mensajesHeader.jsp"/>
				<s:hidden name="id" value="%{id}"/>
				<s:hidden name="grupoCliente.clave"/>
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.negocio.grupocliente.clave"></s:text>
			</th>
			<td>
				<s:textfield name="grupoCliente.id"  readonly="true"
					key="" labelposition="%{#labelPosition}" 
					size="10" maxlength="8" 
					onkeypress="return soloNumeros(this, event, false)"
					cssClass="jQToUpper jQnumeric jQRestric"/> 
			</td>
		</tr>
		<tr>
			<th style="width:100px; text-align:right;">
				<s:text name="midas.negocio.grupocliente.descripcion"></s:text>
			</th>
			<td> 
				<s:textfield name="grupoCliente.descripcion" key="" 
					labelposition="%{#labelPosition}" size="70" 
					maxlength="80" required="true"
					cssClass="jQToUpper jQalphaextra jQRestric w200"/> 
			</td>	
		</tr>
		<tr>				
			<th style="width:100px; text-align:right;">
				<s:text name="midas.negocio.grupocliente.califGrupoCliente"></s:text>
			</th>
			<td> 
				<s:select name="grupoCliente.califGrupoCliente.id" key="" 
					list="califGrupoClientes" listKey="id" 
					listValue="descripcion" cssClass="cajaTexto2 w200"
					labelposition="%{#labelPosition}" required="true"/>
			</td>	
		</tr>
		<tr>		
			<th style="width:100px; text-align:right;">
				<s:text name="midas.negocio.grupocliente.vip"></s:text>
			</th>
			<td> 
				<s:checkbox name="grupoCliente.vip" key="" 
					labelposition="%{#labelPosition}"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<div>
					<s:textfield id="idClienteResponsable" 
						onchange="agregarClienteGrupoCliente(this.value)" 
						cssStyle="display:none;" />
					<s:textfield id="nombreidClienteResponsable" 
						cssStyle="display:none;" />
					<s:text name="midas.negocio.grupocliente.clienteGrupoClientes"/>:
					<input type="button" 
						value="<s:text name='midas.boton.agregar'/>" 
						onclick="buscarClienteNoAgregar(agregarClienteGrupoCliente)" 
						class="b_submit"/>
				</div>
			</th>
		</tr>
		<tr>
			<td colspan="4">
				<div id="divGuardarBtn" style="display: block; float:right;">
					<div class="btn_back w100"  > 
            			<a href="javascript: void(0);"  class="icon_guardar"
                  			onclick="guardarGrupoCliente();return false;">
                  			<s:text name="midas.boton.guardar"/>
            			</a>
					</div>
  	 			</div>
  	 			</td>
		</tr>
		<tr>
			<td colspan="4"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back w100"  >
           				<a href="javascript: void(0);"  class="icon_regresar"
                  			onclick="regresarGrupoCliente();return false;">
                  			<s:text name="midas.boton.regresar"/>
            			</a>
					</div>
   	 			</div>
			</td>
		</tr>
	</table>
</s:form>

<br></br>
<div id="clienteGrupoClienteGrid" style="height: 200px;"></div>

<script>inicializarGrupoClienteDetalle();</script>
	