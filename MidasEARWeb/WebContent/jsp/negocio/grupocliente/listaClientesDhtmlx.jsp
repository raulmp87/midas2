<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
        <column id="grupos" type="sub_row_grid" width="75"><s:text name="midas.negocio.grupocliente.clienteGrupos"/></column>
		<column id="nombreCliente" type="ro" width="*"><s:text name="midas.cliente.nombreCliente"/></column>
		<column id="borrar" type="img" width="30"></column>
	</head>
	<s:iterator value="grupoCliente.clienteGrupoClientes" var="m">
		<s:url action="mostrarClienteGruposDhtmlx" var="mostrarClienteGruposDhtmlxUrl">
			<s:param name="id">${m.cliente.idCliente}</s:param>
		</s:url>
		<row id="${m.cliente.idCliente}">
			<cell><s:property value="#mostrarClienteGruposDhtmlxUrl"/></cell>
			<cell>${m.cliente.nombreCliente}</cell>
			<cell><s:url value="/img/delete14.gif"/>^<s:text name="midas.boton.borrar"/>^javascript:borrarFilaGrid(clienteGrupoClienteGrid, "${m.cliente.idCliente}")^_self</cell>
		</row>
	</s:iterator>
</rows>
