<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<rows>
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
        <column id="id" type="ro" width="0px" sort="int" hidden="true">id</column>
		<column id="clave" type="ro" width="100"><s:text name="midas.negocio.grupocliente.clave"/></column>
		<column id="descripcion" type="ro" width="*"><s:text name="midas.negocio.grupocliente.descripcion"/></column>
		<column id="accionEditar" type="img" width="30" sort="na"/>
		<column id="accionEliminar" type="img" width="30" sort="na"/>
	</head>
	<s:iterator value="grupoClientes" var="m">
		<row id="${m.id}">
			<cell>${m.id}</cell>
			<cell>${m.id}</cell>
			<cell>${m.descripcion}</cell>
			<cell><s:url value="/img/icons/ico_editar.gif"/>^<s:text name="midas.boton.editar"/>^javascript:TipoAccionDTO.getAgregarModificar(editarGrupoCliente)^_self</cell>
			<cell><s:url value="/img/delete14.gif"/>^<s:text name="midas.boton.borrar"/>^javascript: TipoAccionDTO.getEliminar(eliminarGrupoCliente)^_self</cell>
		</row>
	</s:iterator>
</rows>