<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<div style= "height: 100%; width:100%; overflow:auto" hrefmode="ajax-html"  id="configuracionProductoTarifaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="100%" id="productoInfo" name="Producto" href="http://void" extraAction="javascript: verDetalleProducto(<s:property value='tipoAccion' />,'<s:property value="claveNegocio" />');"><s:hidden name="id" id="idToNegProducto"/></div>
	<div width="100%" id="tipoPoliza" name="Tipo de Póliza" href="http://void" extraAction="javascript: desplegarNegocioTipoPoliza();"></div>
	<div width="100%" id="lineas" name="Líneas" href="http://void" extraAction="javascript: desplegarNegocioLineas();"></div>
	<div width="100%" id="paquetes" name="Paquetes" href="http://void" extraAction="javascript: desplegarNegocioPaquetes();"></div>
	<div width="100%" id="coberturas" name="Coberturas" href="http://void" extraAction="javascript: desplegarNegocioCobPaqSeccion();"></div>
	<div width="100%" id="tarifa" name="Tarifa" href="http://void" extraAction="javascript: desplegarNegocioTarifa();"></div>
	<div width="150%" id="condicionesCobranza" name="Condiciones Cobranza" href="http://void" extraAction="javascript: desplegarCobranzaFormaPago();"></div>
	<div width="150%" id="derechos" name="Derechos" href="http://void" extraAction="javascript: desplegarDerechos();"></div>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>