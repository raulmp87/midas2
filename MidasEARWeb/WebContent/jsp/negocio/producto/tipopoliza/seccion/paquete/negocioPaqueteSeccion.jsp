<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/negocio/producto/tipopoliza/seccion/paquete/negocioPaqueteSeccionHeader.jsp"></s:include>

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrar">
			<s:hidden name="idToNegProducto" /> 
			<table id="desplegarDetalle"  border="0">
				<tr>
					<td class="titulo" colspan="4">
						<s:text name="midas.negocio.asociar.tipopoliza.seccion.paquete" /> 
					</td>
				</tr>
				 
				<tr>
					<th width="100px">
						<s:text name="midas.negocio.seccion.tiposPoliza" />
					</th>					
					<td width="300px">
						<s:select name="idTipoPoliza" cssClass="cajaTexto" 
							id="tiposPoliza" list="negocioTipoPolizaList" 
							listKey="idToNegTipoPoliza" listValue="tipoPolizaDTO.descripcion" 
							onchange="getNegocioSeccionPorTipoPoliza(this, 'idLineas');" 
							headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
					</td>
					<td width="300px" >&nbsp;</td>
					<td width="300px" >&nbsp;</td>
				</tr>
				<tr>
					<th width="100px">
						<s:text name="midas.negocio.seccion.lineas" />
					</th>					
					<td width="300px">
						<s:select name="idLineas" cssClass="cajaTexto" 
						id="idLineas" list="negocioSeccionMap"
						onchange="iniciaGridsNegocioPaqueteSeccion();" 
						headerKey="" headerValue="%{getText('midas.general.seleccione')}"/>
					</td>
				</tr>
				
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.tipopoliza.seccion.paquete.asociadas" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioPaqueteSeccionAsociadosGrid" style="width:90%;height:220px"
							class="dataGridConfigurationClass"></div></td>
				</tr>
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.tipopoliza.seccion.paquete.disponibles" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioPaqueteSeccionDisponiblesGrid" style="width:90%;height:220px"
							class="dataGridConfigurationClass"></div></td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje
							clave="configuracion.asociar.arrastrar.mensaje" />
					</td>
				</tr>
			</table>
		</s:form>
	</center>
</div>

