<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/negocio/producto/tipopoliza/seccion/paquete/negociopaqueteseccion.js'/>"></script>


<script type="text/javascript">
    var obtenerNegocioPaqueteSeccionAsociadosPath = '<s:url action="obtenerNegocioPaqueteSeccionAsociadas" namespace="/negocio/paquete"/>';
    var obtenerNegocioPaqueteSeccionDisponiblesPath = '<s:url action="obtenerNegocioPaqueteSeccionDisponibles" namespace="/negocio/paquete"/>';
    var relacionarNegocioPaquetePath = '<s:url action="relacionarNegocioPaquete" namespace="/negocio/paquete"/>';
</script>