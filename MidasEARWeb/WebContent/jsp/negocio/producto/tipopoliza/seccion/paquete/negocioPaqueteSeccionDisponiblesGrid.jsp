<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>		
		<column id="negocioPaqueteSeccion.idToNegPaqueteSeccion" type="ro" width="0" sort="int" hidden="true">negocioPaqueteSeccionId</column>		
		<column id="negocioPaqueteSeccion.paquete.id" type="ro" width="0" sort="int" hidden="true">paqueteId</column>
		<column id="negocioPaqueteSeccion.negocioSeccion.idToNegSeccion" type="ro" width="0" sort="int" hidden="true">negocioSeccionId</column>
		<column id="negocioCobPaqSeccion.paquete.descripcion" type="ro" width="*" sort="str" ><s:text name="midas.catalogos.descripcion"/></column>
		<column id="negocioPaqueteSeccion.modeloAntiguedadMax" type="ro" width="*" sort="str" hidden="true"><s:text name="midas.negocio.paquete.seccion.modelo.antiguedad.max"/></column>
		<column id="negocioPaqueteSeccion.aplicaPctDescuentoEdo" type="ch" width="0" sort="str" hidden="true" ><s:text name="midas.negocio.paquete.seccion.modelo.aplicaPctDescuentoEdo"/></column>				
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioPaqueteDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegPaqueteSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="paquete.id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioSeccion.idToNegSeccion" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="paquete.descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="modeloAntiguedadMax" escapeHtml="false" escapeXml="true" /></cell>
			<s:if test="aplicaPctDescuentoEdo == 1">
				<cell>1</cell>
			</s:if>
			<s:elseif test="aplicaPctDescuentoEdo == 0">
				<cell>0</cell>
			</s:elseif>
		</row>
	</s:iterator>	
</rows>
