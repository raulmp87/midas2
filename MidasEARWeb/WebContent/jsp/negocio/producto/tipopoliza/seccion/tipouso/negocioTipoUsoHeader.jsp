<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">

		
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/producto/tipopoliza/seccion/tipouso/negociotipouso.js'/>"></script>


<script type="text/javascript">
    var obtenerNegocioTipoUsoAsociadasPath = '<s:url action="obtenerNegocioTipoUsoAsociadas" namespace="/negocio/producto/tipopoliza/seccion/tipouso"/>';
    var obtenerNegocioTipoUsoDisponiblesPath = '<s:url action="obtenerNegocioTipoUsoDisponibles" namespace="/negocio/producto/tipopoliza/seccion/tipouso"/>';
    var relacionarNegocioTipoUsoPath = '<s:url action="relacionarNegocioTipoUso" namespace="/negocio/producto/tipopoliza/seccion/tipouso"/>';
</script>