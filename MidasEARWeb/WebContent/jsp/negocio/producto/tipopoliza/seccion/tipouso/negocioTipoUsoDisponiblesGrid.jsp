<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
		<column id="negocioTipoUso.idToNegTipoUso" type="ro" width="0" sort="int" hidden="true">idToNegTipoUso</column>
		<column id="negocioTipoUso.tipoUsoVehiculoDTO.idTcTipoUsoVehiculo" type="ro" width="0" sort="int" hidden="true">idTcTipoUsoVehiculo</column>
		<column id="negocioTipoUso.negocioSeccion.idToNegSeccion" type="ro" width="0" sort="int" hidden="true">idToNegSeccion</column>
		<column id="negocioTipoUso.tipoUsoVehiculoDTO.descripcionTipoUsoVehiculo" type="ro" width="*" sort="str" >Descripcion</column>
		<column id="claveDefault" type="ra" width="200" align ="center" hidden="true">Clave Default</column>
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioTipoUsoDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegTipoUso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoUsoVehiculoDTO.idTcTipoUsoVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="negocioSeccion.idToNegSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoUsoVehiculoDTO.descripcionTipoUsoVehiculo" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="claveDefault" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>	
	
</rows>
