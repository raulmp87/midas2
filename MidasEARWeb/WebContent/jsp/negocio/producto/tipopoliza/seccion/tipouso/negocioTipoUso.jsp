<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/negocio/producto/tipopoliza/seccion/tipouso/negocioTipoUsoHeader.jsp"></s:include>

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarNegocioTipoUso"  >
			<s:hidden id="idToNegSeccion" name="idToNegSeccion" /> 
	
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4">
							<s:text name="midas.negocio.tipouso.asociar" /> 
					</td>
				</tr>				 
				
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.tipouso.asociadas" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioTipoUsoAsociadasGrid"
							class="dataGridConfigurationClass"></div></td>
				</tr>
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.tipouso.disponibles" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioTipoUsoDisponiblesGrid"
							class="dataGridConfigurationClass"></div>
						
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje
							clave="configuracion.asociar.arrastrar.mensaje" />
					</td>
				</tr>
			</table>
		</s:form>
	</center>
</div>
<script type="text/javascript">
	iniciaGridsNegocioTipoUso();
</script>