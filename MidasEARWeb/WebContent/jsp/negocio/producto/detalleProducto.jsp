<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<center>
	<s:form action="guardar" id="negocioProductoForm">
		<table id="desplegarDetalle">
			<tr>
				<td class="subtituloIzquierdaDiv" colspan="4">
					<s:text name="midas.negocio.producto.infogralproducto" /> 
					<s:hidden name="negocioProducto.idToNegProducto" id="idToNegProducto"/>
					<s:hidden name="id"/>
					<s:hidden name="tipoAccion"/>
					<s:hidden name="claveNegocio" id="claveNegocio"/>
				</td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.codigo" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.codigo" />	</td>
				<th>	<s:text name="midas.negocio.producto.version" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.version" />	</td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.estatus" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.claveEstatus" />	</td>
				<th>	<s:text name="midas.negocio.producto.descripcion" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.descripcion" />	</td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.nombre" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.nombreComercial" />	</td>
			</tr>
			<tr>
				<td class="subtituloIzquierdaDiv" colspan="4"><s:text name="midas.negocio.producto.informacionvigencia" /></td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.unidadvigencia" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.claveUnidadVigencia" />	</td>
				<th>	<s:text name="midas.negocio.producto.aplicaajustevigencia" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.claveAjusteVigencia" />	</td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.unidadminima" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.valorMinimoUnidadVigencia" />	</td>
				<th>	<s:text name="midas.negocio.producto.unidadmaxima" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.valorMaximoUnidadVigencia" />	</td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.unidaddefecto" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.valorDefaultUnidadVigencia" />	</td>
			</tr>
			<tr>
				<td class="subtituloIzquierdaDiv" colspan="4"><s:text name="midas.negocio.producto.infopoliticaventa" /></td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.diasgracia" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.diasGracia" />	</td>
				<th>	<s:text name="midas.negocio.producto.diasretroactividad" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.diasRetroactividad" />	</td>
			</tr>
			<tr>
				<th>	<s:text name="midas.negocio.producto.diasdiferimiento" />	</th>
				<td>	<s:property value="negocioProducto.productoDTO.diasDiferimiento" />	</td>
				<td>
				</td>
			</tr>
		</table>
			
		<div id="alinearBotonCatalogoDerecha">
			<div id="b_regresar">
<%-- 				<s:submit key="midas.boton.regresar" onclick="verDetalleNegocio(tipoAccion); return false;" --%>
<!-- 						  id="imgRegresar" cssClass="b_submit" type="button" style="width:90px;" /> -->
						<a href="javascript: void(0);" onclick="javascript:verDetalleNegocio(<s:property value='tipoAccion'/>, <s:property value='negocioProducto.negocio.idToNegocio'/>);" >	
							<s:text name="midas.boton.regresar"/>	
						</a>
			</div>
		</div>			
	</s:form>
</center>