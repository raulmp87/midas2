<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="mensajeInaccesible" style="display:none"><s:property value="mensaje"/></div>
<div id="tipoMensajeInaccesible" style="display:none"><s:property value="tipoMensaje"/></div>
<div id="nextFunctionInaccesible" style="display:none"><s:property value="nextFunction"/></div>
<script type="text/javascript">
	var mensajeInaccesible = jQuery("#mensajeInaccesible").html();
	var tipoMensajeInaccesible = jQuery("#tipoMensajeInaccesible").html();
	var nextFunctionInaccesible = jQuery("#nextFunctionInaccesible").html();
	jQuery("#mensaje").html(mensajeInaccesible);
	jQuery("#tipoMensaje").html(tipoMensajeInaccesible);
	jQuery("#nextFunction").html(nextFunctionInaccesible);
	
	if (typeof jQuery != 'undefined') {
		jQuery(document).ready(
				function() {
					// Muestra mensaje Confirm
					if (jQuery('#mensaje').html() != null
							&& jQuery('#mensaje').html() != 'undefined'
							&& jQuery('#mensaje').html() != '') {
						if (rexpConfirm.test(jQuery('#mensaje').html())){
							if (parent.mostrarMensajeConfirm != null) {
								parent.mostrarMensajeConfirm(parent.getMenssageConfirm(jQuery('#mensaje')
										.html()), jQuery('#tipoMensaje').html(),
										jQuery('#nextFunction').html());
							} else {
								mostrarMensajeConfirm(
										getMenssageConfirm(jQuery('#mensaje').html()), jQuery(
												'#tipoMensaje').html(), jQuery(
												'#nextFunction').html());
							}
						} else { // Muestra mensaje informativo
							if (parent.mostrarMensajeInformativo != null) {
								parent.mostrarMensajeInformativo(jQuery(
										'#mensaje').html(), jQuery(
										'#tipoMensaje').html(), jQuery(
										'#nextFunction').html());
							} else {
								mostrarMensajeInformativo(jQuery('#mensaje')
										.html(), jQuery('#tipoMensaje').html(),
										jQuery('#nextFunction').html());
							}
						}
					}
					//se limpian los divs de mensajes despues de mostrar el alert, para que no se ejecuten al realizar otra accion diferente
					jQuery('#mensaje').html("");
					jQuery('#tipoMensaje').html("");
				});
	}
</script>