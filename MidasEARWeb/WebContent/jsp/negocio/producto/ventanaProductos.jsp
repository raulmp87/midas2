<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
</style>
<script type="text/javascript" src="<s:url value="/js/midas2/negocio/asociarProductoNegocio.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/midas2/util.js"/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>

<script>
	var asociarProductoNegocioURL = '<s:url action="asociarProducto" namespace="/negocio/producto" />';
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"></s:url>'>	
</script>
<script language="JavaScript"
	src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'>
</script>
<s:action name="listarProductos" var="productos" namespace="/negocio/producto" >
	<s:param name="idNegocio" value="idNegocio" ></s:param>
</s:action>
<script type="text/javascript">
	/**
	* Crea dinamicamente el auto complete 
	**/
	var productosDisponibles = new Object();
	productosDisponibles.descripcion =  new Array();
	productosDisponibles.version = new Array();
	<s:iterator value="#productos.productosDisponibles">
		productosDisponibles.descripcion.push({label:'<s:property value="nombreComercial"/>',codigo:'<s:property value="codigo"/>'});
		productosDisponibles.version.push({codigo:'<s:property value="codigo"/>',version:'<s:property value="version"/>'});
	</s:iterator>
	productosDisponibles.version.sort();
	if(productosDisponibles.descripcion.length > 0){
		jQuery(function() {
			jQuery( "#descripcionBusqueda" ).autocomplete({
				source: productosDisponibles.descripcion,
				minLength : 3,
				select:function(event,ui){
					jQuery("#b_guardar").hide();
					listadoService.getListaVersionesPorCodigoProducto(ui.item.codigo, function(data){
						addOptions("idProducto", data);
					});
				}
			});
		});
	}	
</script>
<script type="text/javascript">
function limpiarDsc(){
	jQuery('#descripcionBusqueda').val('');
	jQuery('#idProducto').find('option[value!=""]').remove();
	jQuery("#b_guardar").hide();
}

function validaProducto(){
	var idProducto = jQuery("#idProducto").val();
	if(idProducto == null || idProducto == undefined || idProducto === ""){
		jQuery("#b_guardar").hide();
	}else{
		jQuery("#b_guardar").show();
	}
}

function dataSave(){
	 guardar();
	 //t=setTimeout('guardar();',50);	
}

function guardar() {
	//mostrarIndicadorCarga('indicador');	
	//document.forms[0].submit();
	var validarPolizaPath = "/MidasWeb/negocio/producto/asociarProducto.action";
	//parent.mainDhxWindow.window("productos").setDimension(300, 100);
	//parent.mainDhxWindow.window("productos").center();
	//parent.redirectVentanaModal("productos", validarPolizaPath, jQuery("#asociarProductoForm"));
	parent.sendRequestJQ(jQuery("#asociarProductoForm"),validarPolizaPath, 'contenido', null);	
	parent.cerrarVentanaProductos();
}

</script>
<s:form action="/negocio/producto/asociarProducto.action" namespace="/negocio/producto" id="asociarProductoForm" >

<div class="titulo">
	<h4><s:text name="midas.negocio.producto.relacionar.titulo"  /></h4>
</div>
<div id="indicador" style="text-align: center"></div>
<br/>
<s:hidden name="idNegocio" id="idNegocio" />
<s:hidden name="claveNegocio" id="claveNegocio" />
<s:hidden name="tipoAccion" id="tipoAccion" />
<table id="desplegarDetalle">
	<tr>
		<th>
			<s:text name="midas.negocio.producto.relacionar.negocio"/>
		</th>
		<td>
			<s:textfield name="negocio.descripcionNegocio" readonly="true" disabled="true"
				size="60"
				cssStyle="margin-left:-5px;"
 				cssClass="txtfield"  />
		</td>
	</tr>
	<tr>
		<th>
			<s:text name="midas.negocio.producto.relacionar.producto" />
		</th>
		<td>
			<div style="vertical-align: bottom;" >
			<div style="float: left;">
				<input type="text" name="descripcionBusqueda" id="descripcionBusqueda" class="txtfield" style="width:230px;"/>
			</div>
			<div style="float: left; vertical-align: bottom;">
				<img id="limpiar" src='<s:url value="/img/close2.gif"/>' alt="Limpiar descripción" 
				style="margin-left: 5px; border-bottom-width: 0px; "
				onclick ="limpiarDsc();"/>
			</div>
			</div>
		</td>
	</tr>
	<tr>
		<th>
			Versión
		</th>
		<td>
			<select id="idProducto" name="idProducto" class="txtfield" style="width:230px;"
				onchange="validaProducto();"
			>
				<option value="">SELECCIONE ...</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div class="btn_back alinearBotonALaDerecha w100" style="margin-right:5px;">
				<a href="javascript: void(0);"
					onclick="javascript: parent.cerrarVentanaProductos();">
				<s:text name="midas.boton.cancelar" /> </a>
			</div>
				<div id="b_guardar" style="display: none;">
					<a href="javascript: void(0);" onclick="javascript:dataSave();">
						<s:text name="midas.boton.guardar" /> </a>
			</div>
		</td>
	</tr>
</table>	
</s:form>