<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">
	<s:iterator value="listadoConfigDerecho" var="index">
		<row>
			<s:if test="importeDefault == true">
				<cell>../img/icons/check_in.png^<s:text name="midas.negocio.producto.configderecho.valordefault"/>^^_self</cell>
			</s:if>
			<s:else>
				<cell>../img/pixel.gif^<s:text name="midas.negocio.producto.configderecho.valordefault"/>^^_self</cell>
			</s:else> 
		    <cell><s:property value="tipoPolizaDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="seccionDescripcion" escapeHtml="false" escapeXml="true"/></cell>
 		    <cell><s:property value="paqueteDescripcion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="monedaDescripcion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoDerechoDescripcion" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="importeDerecho" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="stateName" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="cityName" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="tipoUsoDescripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_rechazar2.gif^<s:text name="midas.boton.borrar"/>^javascript:eliminarConfiguracionDerecho(<s:property value="idToNegConfigDerecho" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>../img/pixel.gif</cell>
		</row>
	</s:iterator>
</rows>