<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<sj:head/>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>

<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script src="<s:url value='/js/midas2/negocio/producto/derechos/negocioconfiguracionderechos.js'/>"></script>

<script type="text/javascript">
    var obtenerNegocioDerechosPath = '<s:url action="obtenerNegocioDerecho" namespace="/negocio/configuracionderecho"/>';
    var guardarNegocioDerechosPath = '<s:url action="guardarConfigDerecho" namespace="/negocio/configuracionderecho"/>';
    var eliminarNegocioDerechosPath = '<s:url action="eliminarConfigDerecho" namespace="/negocio/configuracionderecho"/>';
    var buscarConfiguracionDerechoPath = '<s:url action="buscarConfiguracionesDerecho" namespace="/negocio/configuracionderecho"/>';
</script>


<s:form name="configuracionDerechoForm" id="configuracionDerechoForm" action="buscarConfiguracionesDerecho" >
	<s:hidden name="idToNegProducto" id="idToNegProducto"/>
	<s:hidden name="negocioConfigDerecho.negocio.idToNegocio" id="idToNegocio" />
	
	<table id="desplegarDetalle" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.general.cobertura" /> 
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.seccion.tiposPoliza" />
			</th>
			<th>
				<s:text name="midas.negocio.seccion.disponibles" />
			</th>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.paquete" /> 
			</th> 
			<th>
				<s:text name="midas.general.moneda" /> 
			</th>
			<th>
				<s:text name="midas.negocio.producto.configderecho.tipoderecho" /> 
			</th> 
		</tr>
		<tr>
			<td>
				<s:select id="idToTipoPoliza" cssClass="cajaTexto jQrequired" 
					list="negocioTipoPolizaList"  
					name="negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza" 
					listKey="idToNegTipoPoliza" headerKey=""
					listValue="tipoPolizaDTO.descripcion"					 
					headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
					OnChange="obtenerLineasDeNegocio(this.value); obtenerMonedas(this.value);validateAll();" />
			</td>					
 			<td> 
				<s:select id="idToNegSeccion" cssClass="cajaTexto jQrequired"  
					name="negocioConfigDerecho.seccion.idToNegSeccion"  
					list="secciones"
					OnChange="obtenerPaquetes(this.value);obtenerTipoUso(this.value); validateAll();"
					headerKey="" headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 			
 			<td > 
				<s:select name="negocioConfigDerecho.paquete.idToNegPaqueteSeccion" id="idToNegPaqueteSeccion"  
					list="paquetes"  cssClass="cajaTexto jQrequired" 
					OnChange="onChangeComboPaquete(this.value);validateAll();" headerKey="" 
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 
 			<td>  
				<s:select name="negocioConfigDerecho.moneda.idTcMoneda" id="idTcMoneda"  cssClass="cajaTexto jQrequired" 
					list="monedaList" 
					OnChange="onChangeComboMoneda(this.value);validateAll();" headerKey=""  
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td>
 			<td>  
				<s:select name="negocioConfigDerecho.tipoDerecho" id="idTipoDerecho"  cssClass="cajaTexto jQrequired" 
					list="tipoDerechoList"  
					OnChange="validateAll(); obtenerImportesDerecho(); onChangeComboTipoDerecho(this.value);" headerKey=""  
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.estado" />
			</th>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.municipio" />
			</th>
			<th>
				<s:text name="midas.negocio.producto.configderecho.tipouso" /> 
			</th> 
			<th>
				<s:text name="midas.negocio.producto.configderecho.importesderecho" />
			</th> 
			<th>
				
			</th>
		</tr>
		<tr>
 			<td >  
 				<s:select cssClass="cajaTexto" name="negocioConfigDerecho.estado.stateId" id="stateId" 
 					 list="estadoList" listKey="stateId" 
 					listValue="stateName" 
 					OnChange="obtieneMunicipio(this.value); onChangeComboEstado(this.value);" headerKey=""  
 					headerValue="%{getText('midas.general.seleccione')}"
 				/> 
 			</td> 
 			<td >  
 				<s:select cssClass="cajaTexto" name="negocioConfigDerecho.municipio.cityId" id="cityId"  
 					 list="municipioList" headerKey="" 
 					OnChange="onChangeComboMunicipio(this.value);"  
 					headerValue="%{getText('midas.general.seleccione')}"
 				/> 
 			</td>
 			<td>  
				<s:select name="negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo" id="idTcTipoUsoVehiculo"  cssClass="cajaTexto" 
					
					list="tipoUsoList"  
					OnChange="onChangeComboTipoUsoVehiculo(this.value);"   headerKey=""  
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 
 			<td>  
				<s:select name="negocioConfigDerecho.idToNegDerecho" id="importeDerecho"  cssClass="cajaTexto jQrequired" 
					
					list="importesDerecho"  
					OnChange="validateAll(); onChangeComboImporteDerecho(this.value);" headerKey=""  
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td>
 			<td>
 				<div class="btn_back w140" style="display: inline;float: left: ;" id="b_guardar">
					<a href="javascript: void(0);" onclick="guardarConfiguracionDerecho();"> 
					<s:text name="midas.boton.guardar" /> </a>
				</div>
 			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.producto.configderecho.valordefault" />
			</th> 
		</tr>
		<tr>
			<td>
 				<s:checkbox id="importeDefault"
							name="negocioConfigDerecho.importeDefault"/>
			</td>
		</tr>
				<tr>
					<td colspan="4">
						<div id="tituloListado" style="display:none;" class="titulo" style="width: 98%;"><s:text name="midas.negocio.producto.configderecho.titulolistado" /></div>	
						<div id="negocioConfigDerechoGridContainer" >
							<div id="listadoConfigDerechoGrid" style="width:1024px;height:200px;"></div>
							<div id="pagingArea"></div><div id="infoArea"></div>
						</div>
					</td>
				</tr>
			</table>
		</s:form>

<s:include value="/jsp/negocio/producto/mensajesInaccesiblesHeader.jsp"></s:include>

