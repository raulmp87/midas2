<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/producto/cobranza/formapago/negocioformapago.js'/>"></script>


<script type="text/javascript">
    var obtenerFormaPagoAsociadasPath = '<s:url action="obtenerFormaPagoAsociadas" namespace="/negocio/formapago"/>';
    var obtenerFormaPagoDisponiblesPath = '<s:url action="obtenerFormaPagoDisponibles" namespace="/negocio/formapago"/>';
    var relacionarFormaPagoPath = '<s:url action="relacionarFormaPago" namespace="/negocio/formapago"/>';
</script>
