<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
 		<column id="negocioFormaPago.idToNegFormaPago" type="ro" width="0" sort="int" hidden="true">idToNegFormaPago</column>
		<column id="negocioFormaPago.negocioProducto.idToNegProducto" type="ro" width="0" sort="int" hidden="true">idToNegFormaPago</column>
		<column id="negocioFormaPago.formaPagoDTO.idFormaPago" type="ro" width="0" sort="int" hidden="true">idFormaPago</column>		
		<column id="negocioFormaPago.formaPagoDTO.descripcion" type="ro" width="*" sort="str"><s:text name="midas.catalogos.descripcion"/></column>
	</head>

	<% int a=0;%>
	<s:iterator value="relacionesNegocioFormaPagoDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegFormaPago" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocioProducto.idToNegProducto" escapeHtml="false" escapeXml="true"  /></cell>
			<cell><s:property value="formaPagoDTO.idFormaPago" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="formaPagoDTO.descripcion" escapeHtml="false" escapeXml="true" /></cell>			
		</row>
	</s:iterator>	
	
</rows>