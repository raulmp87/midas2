<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/negocio/producto/cobranza/formapago/negocioFormaPagoHeader.jsp"></s:include>
	
	<center>
		<table id="desplegarDetalle" border="0">	
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.cobranza.formaPago.asociadas"/>
					<s:hidden name="idToNegProducto"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioFormaPagoAsociadasGrid" class="dataGridConfigurationClass" style="height:220px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.cobranza.formaPago.disponibles"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioFormaPagoDisponiblesGrid" class="dataGridConfigurationClass" style="height:220px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>
<script type="text/javascript">
	iniciaGridsFormaPago();
</script>
