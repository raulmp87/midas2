<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		
 		<column id="negocioMedioPago.idToNegMedioPago" type="ro" width="0" sort="int" hidden="true">idToNegMedioPago</column>
		<column id="negocioMedioPago.negocioProducto.idToNegProducto" type="ro" width="0" sort="int" hidden="true">idToNegFormaPago</column>
		<column id="negocioMedioPago.medioPagoDTO.idMedioPago" type="ro" width="0" sort="int" hidden="true">idMedioPago</column>		
		<column id="negocioMedioPago.medioPagoDTO.descripcion" type="ro" width="*" sort="str"><s:text name="midas.catalogos.descripcion"/></column>
	</head>

	<% int a=0;%>
	<s:iterator value="relacionesNegocioMedioPagoDTO.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idToNegMedioPago" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="negocioProducto.idToNegProducto" escapeHtml="false" escapeXml="true"  /></cell>
			<cell><s:property value="medioPagoDTO.idMedioPago"  /></cell>
			<cell><s:property value="medioPagoDTO.descripcion" /></cell>		
		</row>
	</s:iterator>	
	
</rows>