<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/producto/cobranza/mediopago/negociomediopago.js'/>"></script>


<script type="text/javascript">
    var obtenerMedioPagoAsociadasPath = '<s:url action="obtenerMedioPagoAsociadas" namespace="/negocio/mediopago"/>';
    var obtenerMedioPagoDisponiblesPath = '<s:url action="obtenerMedioPagoDisponibles" namespace="/negocio/mediopago"/>';
    var relacionarMedioPagoPath = '<s:url action="relacionarMedioPago" namespace="/negocio/mediopago"/>';
</script>
