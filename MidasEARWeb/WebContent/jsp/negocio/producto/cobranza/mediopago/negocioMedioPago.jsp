<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/jsp/negocio/producto/cobranza/mediopago/negocioMedioPagoHeader.jsp"></s:include>
	<center>
		<table id="desplegarDetalle" border="0">	
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.cobranza.medioPago.asociadas"/>
					<s:hidden name="idToNegProducto"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioMedioPagoAsociadasGrid" class="dataGridConfigurationClass" style="height:220px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.cobranza.medioPago.disponibles"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioMedioPagoDisponiblesGrid" class="dataGridConfigurationClass" style="height:220px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
			</tr>
		</table>
	</center>	
	<script type="text/javascript">
		iniciaGridsMedioPago();
	</script>