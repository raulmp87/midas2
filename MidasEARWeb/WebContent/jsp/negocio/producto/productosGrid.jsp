<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
         <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
        
		<column id="id" type="ro" width="0" sort="int" hidden="false"><s:text name="midas.negocio.producto.id" /></column>
		<column id="idToProducto" type="ro" width="100" ><s:text name="midas.negocio.producto.idproducto" /></column>
		<column id="nombreComercial" type="ro" width="*" ><s:text name="midas.negocio.producto.nombreproducto" /></column>
		<column id="version" type="ro" width="*" ><s:text name="midas.negocio.producto.versionproducto" /></column>	
        <column id="accionEliminar" type="img" width="30px" sort="na"/>
		<column id="accionEditar" type="img" width="30px" sort="na"/>		
	</head>	
	<s:iterator value="productosAsignados">
		<row id="<s:property value="idToNegProducto" />">
			<cell><s:property value="idToNegProducto" /></cell>
			<cell><s:property value="productoDTO.idToProducto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="productoDTO.nombreComercial" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="productoDTO.version" escapeHtml="false" escapeXml="true"/></cell>
		 <s:if test="negocio.claveEstatus == 0">	
			<cell>../img/icons/ico_eliminar.gif^Eliminar^javascript: eliminarNegocioProducto(<s:property value="idToNegProducto"/>,<s:property value="negocio.idToNegocio" />,<s:property value="productoDTO.idToProducto" escapeHtml="false" escapeXml="true"/>,"<s:property value="claveNegocio" escapeHtml="false" escapeXml="true"/>",<s:property value="idNegocio" escapeHtml="false" escapeXml="true"/>,2)^_self</cell>
		</s:if>
			<cell>../img/icons/ico_editar.gif^Editar^javascript: TipoAccionDTO.getAgregarModificar(desplegarConfiguracion)^_self</cell>
		</row>
	</s:iterator>
</rows>