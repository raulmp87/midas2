<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">

<s:include value="/jsp/negocio/antecedentes/negocioAntecedentesHeader.jsp"></s:include>
<s:hidden name="idToNegocio" id="idNegocio" />

<div>
	<center>
		<table id="desplegarDetalle" border="0"> 
		
			<tr>
				<td>
					<font size="2"><b><s:text name="midas.negocio.antecedentes.tituloAnexos"/></b></font>
				</td>
				<td>
					<font size="2"><b><s:text name="midas.negocio.antecedentes.tituloComentarios"/></b></font>
				</td>
			</tr>
			
			<tr>
				<td>
					<div id="negocioAntecedentesAnexosGrid" class="dataGridConfigurationClass" style="width:500px; height:200px;"></div>
				</td>
				<td>
					<div id="negocioAntecedentesComentariosGrid" class="dataGridConfigurationClass" style="width:500px; height:200px;"></div>
				</td>
				
			</tr>
			
			<tr>
				<td>
					<div class="btn_back w100">
						<a href="javascript: void(0);" class="icon_guardar" onclick="actualizarGridCENAAnexos('negocioAntecedentesAnexosProcessor');">
							<s:text name="midas.boton.guardar"/>
						</a>
					</div>
					<br/>
					<div class="btn_back w170">
						<a href="javascript: void(0);" class="icon_adjuntarPDF" onclick="mostrarAnexarArchivoCenaWindow();">
							<s:text name="midas.negocio.antecedentes.boton.agregaas"/>
						</a>
					</div>
				</td>
				<td>
					<s:form id="cenaComentarioForm" name="cenaComentarioForm">
						<font size="1"><s:text name="midas.negocio.antecedentes.nuevoComentario"/></font>
						<s:textarea disabled="false" name="negocioCEAComentarios.comentario" id="negocioCEAComentarios.comentario" cssClass="txtfield" required="true" cols="54" rows="2" cssClass="jQalphanumeric"/>
						<br/>
						<div id="b_guardar">
							<a id="submit" href="javascript: void(0);"
								onclick="guardarComentario();"> <s:text
								name="midas.boton.guardar" /> </a>
						</div>
					</s:form>
				</td>
			</tr>
			
		</table>
	</center>
</div>
<script type="text/javascript">
	iniciaGridAntecedentesAnexos();
	iniciaGridAntecedentesComentarios();
</script>
