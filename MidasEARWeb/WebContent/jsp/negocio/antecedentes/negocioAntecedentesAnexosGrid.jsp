<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		<column id="negocioCEAAnexos.id" type="ro"  width="*"  sort="int" hidden="true">negocioCEAAnexos.id</column>
		<column id="negocioCEAAnexos.nombre" type="ro" width="100" sort="str"><s:text name="midas.negocio.antecedentes.anexos.nombre"/></column>
		<column id="negocioCEAAnexos.fechaRegistro" type="ro" width="70" sort="str"><s:text name="midas.negocio.antecedentes.anexos.fecha"/></column>
		<column id="negocioCEAAnexos.usuarioNombre" type="ro" width="80" sort="str"><s:text name="midas.negocio.antecedentes.anexos.usuario"/></column>
		<column id="negocioCEAAnexos.texto" type="ed" width="150" sort="str"><s:text name="midas.negocio.antecedentes.anexos.texto"/></column>
		<column id="descargar" type="img" width="70" align="center"><s:text name="midas.negocio.antecedentes.anexos.descargar"/></column>
	</head>
	
	<% int a=0;%>
		<s:iterator value="preparaListasNegocioAntecedentes.anexos">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaRegistro" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioNombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="texto" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Descargar^javascript:descargarDocumentoCENA("<s:property value="fortimaxDocNombre" escapeHtml="false" escapeXml="true"/>");^_self</cell>
		</row>
	</s:iterator>
</rows>
