<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="negocioCEAComentarios.id" type="ro"  width="*"  sort="int" hidden="true">negocioCEAComentarios.id</column>
		<column id="negocioCEAComentarios.comentario" type="ro" width="283" sort="str"><s:text name="midas.negocio.antecedentes.comentarios.comentario"/></column>
		<column id="negocioCEAComentarios.usuarioNombre" type="ro" width="80" sort="str"><s:text name="midas.negocio.antecedentes.comentarios.usuario"/></column>
		<column id="negocioCEAComentarios.fechaCreacion" type="ro" width="120" sort="str"><s:text name="midas.negocio.antecedentes.comentarios.fechaCreacion"/></column>
	</head>
	
	<% int a=0;%>
	<s:iterator value="preparaListasNegocioAntecedentes.comentarios">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
			<cell><s:property value="comentario" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="usuarioNombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
</rows>
