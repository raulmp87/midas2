<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style type="text/css">
	.wwlbl{
		padding-top: 3px;
		padding-left: 30px;
	}
	
	.wwctrl{
		float: left;
	}
	
	.txtfield{
		width:30px;
	}
</style>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"/>
<s:hidden name="negocio.pctMaximoDescuento" id="descuentoMax" />
<s:form id="condicionesRenovacionForm">
	<s:hidden name="condicionTipo" id="condicionTipo" />
	<s:hidden name="negocioRenovacion.id.tipoRiesgo" />
	<s:hidden name="negocioRenovacion.id.idToNegocio" />

	<div style="width: 98%; text-align: center;">
		<table id="agregar" style="white-space: normal;">
			<tr>
				<td colspan="2"><s:checkbox id="renuevaPoliza"
						label="%{getText('midas.negocio.renovacion.renuevaPoliza')}"
						name="negocioRenovacion.renuevaPoliza" />
				</td>
			</tr>
		</table>		
		<table id="agregar" style="white-space: normal;">
			<tr>
				<td style="vertical-align: top;">
					<table id="desplegarDetalle" style="white-space: normal;  float: left; width: 310px;">
						<tr>
							<td colspan="2"><s:text
									name="midas.negocio.renovacion.mantieneCondiciones" />:</td>
						</tr>
						<tr>
							<td colspan="2"><s:checkbox id="mantieneDescuentoComercial"  
									label="%{getText('midas.negocio.renovacion.mantieneDescuento')}" 
									name="negocioRenovacion.mantieneDescuentoComercial"
									onclick="onChangeCondicionRenovacion(this);" /></td>
						</tr>
						<tr>
							<td  style="width: 270px;" ><s:checkbox id="otorgaDescuentoComercial" 
								label="%{getText('midas.negocio.renovacion.otorgaDescuento')}" 
									name="negocioRenovacion.otorgaDescuentoComercial" 
									onclick="onChangeCondicionRenovacion(this);"/></td>
							<td >
							<s:textfield id="valorDescuentoComercial" cssStyle="width:40px;"
								onblur="validaDescuentoComercial();"
								onkeypress="return soloNumeros(this, event, true,false,3,2);"
									name="negocioRenovacion.valorDescuentoComercial" />&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2"><s:checkbox id="mantieneCesionComision" 
									label="%{getText('midas.negocio.renovacion.mantieneCesion')}" 
									name="negocioRenovacion.mantieneCesionComision" /></td>
						</tr>
						<tr>
							<td colspan="2"><s:checkbox id="aplicarPrimaAnterior" 
									label="%{getText('midas.negocio.renovacion.aplicarPrimaAnterior')}" 
									name="negocioRenovacion.aplicarPrimaAnterior" /></td>
						</tr>
						<tr>
							<td style="width: 270px;"><s:checkbox id="detenerNumeroIncisos" 
									label="%{getText('midas.negocio.renovacion.detenerNumeroIncisos')}" 
									name="negocioRenovacion.detenerNumeroIncisos" 
									onclick="onChangeCondicionRenovacion(this);"/></td>
							<td><s:textfield id="numeroIncisos" cssStyle="width:40px;"
								onkeypress="return soloNumeros(this, event, true,false,7,0);"
									name="negocioRenovacion.numeroIncisos" />&nbsp;
							</td>
						</tr>
						
						<tr>
							<td colspan="2">
								<s:select  labelposition="top" key="midas.negocio.seccion.asociadas"
								list="negSeccionList" name="idToNegSeccion" id="idToNegSeccion" 
								headerKey="" headerValue="%{getText('midas.general.seleccione')}"
								listKey="idToNegSeccion" listValue="seccionDTO.descripcion"		
								onchange="obtenerCoberturasCondRenovacion();"						 
								 />
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div id="condRenCoberturasGrid" style="width:100%;height:220px"
									class="dataGridConfigurationClass"></div>
							</td>
						</tr>
				</table></td>
				<td style="vertical-align: top;">
					<table id="desplegarDetalle" style="white-space: normal;  float: left;">
									<tr>
						<td colspan="4"><s:text
								name="midas.negocio.renovacion.comportamientoTarifa" /></td>
					</tr>
					<tr>
						<td colspan="2"><s:checkbox id="aumentarPctTarifa"
								label="%{getText('midas.negocio.renovacion.aumentarTarifa')}" 
								name="negocioRenovacion.aumentarPctTarifa" 
								onclick="onChangeCondicionRenovacion(this);"/></td>
						<td colspan="2"><s:textfield id="pctTarifa" cssStyle="width:40px;"
								onkeypress="return soloNumeros(this, event, true,false,3,2);"
								name="negocioRenovacion.pctTarifa" />%</td>
					</tr>
					<tr>
						<td><s:checkbox id="limiteIncrementoTarifa"
								label="%{getText('midas.negocio.renovacion.limiteIncremento')}" 
								name="negocioRenovacion.limiteIncrementoTarifa" 
								onclick="onChangeCondicionRenovacion(this);"/></td>
						<td style="width:25%;"><s:textfield id="pctLimiteIncremento" cssStyle="width:40px;"
								onkeypress="return soloNumeros(this, event, true,false,3,2);"
								name="negocioRenovacion.pctLimiteIncremento" />&nbsp;%</td>
						<td><s:checkbox id="sinLimiteSinietros"
								label="%{getText('midas.negocio.renovacion.sinLimiteSiniestro')}"
								name="negocioRenovacion.sinLimiteSinietros" 
								onclick="onChangeCondicionRenovacion(this);"/></td>
						<td><s:select id="limiteSiniestros"
								name="negocioRenovacion.limiteSiniestros"
								list="#{'0':'0','1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7'}" /></td>
					</tr>
					<tr>
						<td colspan="2"><s:checkbox id="aplicaDescuentoNoSinietro" onclick="cambiaDescuentoNoSiniestro();"
								label="%{getText('midas.negocio.renovacion.descuentoNoSiniestro')}"
								name="negocioRenovacion.aplicaDescuentoNoSinietro" /></td>
						<td colspan="2"><s:text
								name="midas.negocio.renovacion.descuentoNoSinietroLbl" /></td>
					</tr>
					<tr>
						<td colspan="4">
						<table id=t_riesgo border="0" style="width: 100%;" class="descuentoNoSiniestro" >
							<tr>
								<th><s:text name="midas.negocio.renovacion.numSiniestros" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren1" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren2" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren3" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren4" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren5" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren6" /></th>
							</tr>
							<s:iterator value="descuentosList" status="stat">
							<tr>
								<td class ="txt_v2">
									<s:hidden name="descuentosList[%{#stat.index}].id.idToNegocio" value="%{id.idToNegocio}" />
									<s:hidden name="descuentosList[%{#stat.index}].id.tipoRiesgo" value="%{id.tipoRiesgo}" />
									<s:hidden name="descuentosList[%{#stat.index}].id.tipoDescuento" value="%{id.tipoDescuento}" />
									<s:hidden name="descuentosList[%{#stat.index}].id.numSiniestros" value="%{id.numSiniestros}" />
									<s:property value="%{siniestroStr}" />
								</td>
								<td class ="txt_v2">
									<s:textfield name="descuentosList[%{#stat.index}].ren1" value="%{ren1}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="descuentosList[%{#stat.index}].ren2" value="%{ren2}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="descuentosList[%{#stat.index}].ren3" value="%{ren3}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="descuentosList[%{#stat.index}].ren4" value="%{ren4}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="descuentosList[%{#stat.index}].ren5" value="%{ren5}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="descuentosList[%{#stat.index}].ren6" value="%{ren6}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
							</tr>
							</s:iterator>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4"><s:checkbox id="aplicaDeducibleDanos" onclick="cambiaDeducibleDanos();"
								label="%{getText('midas.negocio.renovacion.deducibleDanos')}"
								name="negocioRenovacion.aplicaDeducibleDanos" /></td>
					</tr>
					<tr>
						<td colspan="4">
						<table id=t_riesgo border="0" style="width: 100%;" class="deduciblesDanos">
							<tr>
								<th><s:text name="midas.negocio.renovacion.numSiniestros" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren1" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren2" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren3" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren4" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren5" /></th>
								<th style="width: 50px;"><s:text name="midas.negocio.renovacion.ren6" /></th>
							</tr>
							<s:iterator value="deduciblesList" status="stat">
							<tr>
								<td class ="txt_v2">
									<s:hidden name="deduciblesList[%{#stat.index}].id.idToNegocio" value="%{id.idToNegocio}" />
									<s:hidden name="deduciblesList[%{#stat.index}].id.tipoRiesgo" value="%{id.tipoRiesgo}" />
									<s:hidden name="deduciblesList[%{#stat.index}].id.tipoDescuento" value="%{id.tipoDescuento}" />
									<s:hidden name="deduciblesList[%{#stat.index}].id.numSiniestros" value="%{id.numSiniestros}" />
									<s:property value="%{siniestroStr}" />
								</td>
								<td class ="txt_v2">
									<s:textfield name="deduciblesList[%{#stat.index}].ren1" value="%{ren1}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="deduciblesList[%{#stat.index}].ren2" value="%{ren2}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="deduciblesList[%{#stat.index}].ren3" value="%{ren3}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="deduciblesList[%{#stat.index}].ren4" value="%{ren4}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="deduciblesList[%{#stat.index}].ren5" value="%{ren5}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
								<td class ="txt_v2">
									<s:textfield name="deduciblesList[%{#stat.index}].ren6" value="%{ren6}" cssClass="txtfield" onkeypress="return soloNumeros(this, event, true,false,3,2);" />%
								</td>
							</tr>
							</s:iterator>
						</table>
						</td>
					</tr>
				</table></td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="btn_back w220" style="display: inline; float: right;">
						<a href="javascript: void(0);"
							onclick="guardaCondicionRenovacion();" class="icon_guardar2"> <s:text
								name="midas.boton.guardar" /> </a>
					</div></td>
			</tr>
		</table>
	</div>
</s:form>
<script type="text/javascript">
	initCondicionesRenovacion();
</script>