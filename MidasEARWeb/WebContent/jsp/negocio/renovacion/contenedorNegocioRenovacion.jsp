<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:include value="/jsp/negocio/renovacion/negocioRenovacionHeader.jsp"></s:include>

<s:hidden name="idNegocio" id="idNegocio" />
<div style= "height: 100%; width:100%; overflow:auto" hrefmode="ajax-html"  id="condicionesRenovacion" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="150%" id="riesgosNormales" name="<s:text name='midas.negocio.renovacion.riesgoNormal'/>" href="http://void" extraAction="javascript: desplegarRiesgosNormales();"></div>
	<div width="100%" id="altoRiesgo" name="<s:text name='midas.negocio.renovacion.altoRiesgo'/>" href="http://void" extraAction="javascript: desplegarAltoRiesgo();"></div>
	<div width="100%" id="altaFrecuencia" name="<s:text name='midas.negocio.renovacion.altaFrecuencia'/>" href="http://void" extraAction="javascript: desplegarAltaFrecuencia();"></div>
</div>
<script type="text/javascript">
	dhx_init_tabbars();
</script>