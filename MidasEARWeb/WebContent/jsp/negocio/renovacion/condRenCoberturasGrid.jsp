<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="negocioRenovacionCobertura.id.idToNegocio" type="ro" width="0" sort="int" hidden="true">idToNegocio</column>
		<column id="negocioRenovacionCobertura.id.tipoRiesgo" type="ro" width="0" sort="int" hidden="true">tipoRiesgo</column>
		<column id="negocioRenovacionCobertura.id.idToSeccion" type="ro" width="0" sort="int" hidden="true">idToSeccion</column>
		<column id="negocioRenovacionCobertura.id.idToCobertura" type="ro" width="0" sort="int" hidden="true">idToCobertura</column>		
		<column id="negocioRenovacionCobertura.nombreComercialCobertura" type="ro" width="200" sort="str" ><s:text name="midas.catologos.cobpaquetessecciones.descripcion"/></column>
		<column id="negocioRenovacionCobertura.aplicaDefault" type="ch"  width="100"  sort="int"><s:text name="midas.negocio.renovacion.aplicaSaDefault"/></column>
				
	</head>
		
	<% int a=0;%>
	<s:iterator value="negCobPaqSeccionList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.idToNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.tipoRiesgo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.idToSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.idToCobertura" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreComercialCobertura" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="aplicaDefault" escapeHtml="false" escapeXml="true"/></cell>
			
		</row>
	</s:iterator>	
</rows>
