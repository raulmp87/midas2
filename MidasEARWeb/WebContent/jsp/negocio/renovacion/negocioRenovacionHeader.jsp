<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxaccordion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script src="<s:url value='/js/midas2/negocio/renovacion/negocioRenovacion.js'/>"></script>

<script type="text/javascript">
    var condicionesRenovacionPath = '<s:url action="mostrarCondicionesRenovacion" namespace="/negocio/renovacion"/>';
    var guardarCondicionesRenovacionPath = '<s:url action="guardarCondicionesRenovacion" namespace="/negocio/renovacion"/>';
    var mostrarCoberturasCondRenPath = '<s:url action="mostrarCoberturasCondRen" namespace="/negocio/renovacion"/>';
    var guardarCoberturasCondRenPath = '<s:url action="guardarCoberturasCondRen" namespace="/negocio/renovacion"/>';
</script>