<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<s:include value="/jsp/negocio/copiarNegocioHeader.jsp"></s:include>


<div id="indicador"></div>
<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}
div{
overflow-x:visible;
overflow-y:visible;
}
</style>

<script type="text/javascript">

	function validaNombreNegocio(){
		var today= new Date();
		var day=0; var month=0; var year=0;
		var todayStr;
		day= today.getDate();
		month= today.getMonth()+1;
		year= today.getFullYear();
		todayStr=  day + "/" + "0"+month + "/" + year;
		
	    if(document.getElementById("nombreNegocio").value==null || document.getElementById("nombreNegocio").value==''){
	      alert('Es requerido el nombre para copiar el negocio');
	          return false;
	    }
	    
	    if(document.getElementById("fechaFinVigencia").value==null || document.getElementById("fechaFinVigencia").value==''){
		      alert('Es requerido la fecha de Vencimiento para copiar el negocio');
		          return false;
	    }

	    
	    return true;
	}


</script>
<s:form action="copiarNegocio" namespace="/negocio" id="copiarNegocio" >
	<s:hidden name="idToNegocio" id="idToNegocio" />	
	<s:hidden name="copiarEnCascada" value="true" id="copiarEnCascada" />
	<div id="indicador"></div>
	<div class="titulo">
		<h5><s:text name="midas.boton.copiarNegocio" /></h5>
	</div>
		<table id="desplegarDetalle" border="0">	
			<tr>						
				<td colspan="4">
					<s:textfield name="nombreNegocio" 
						key="midas.negocio.nombreNegocio" id="nombreNegocio" 
						labelposition="left" cssClass="txtfield"></s:textfield>
				</td>
			</tr>
			<tr>	
			 <td colspan="4">								
							<sj:datepicker name="negocio.fechaFinVigencia" key="midas.negocio.fechaVencimiento"
							   buttonImage="../img/b_calendario.gif" 
				               id="fechaFinVigencia" maxlength="10" cssClass="txtfield jQrequired"	
				               labelposition="%{getText('label.position')}" 
				               size="12" labelposition="left"
				               onkeypress="return soloFecha(this, event, false);"
				               onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
				               onblur="esFechaValida(this);"></sj:datepicker>			   		
			 </td>				
			</tr>	
			<tr>						
				<td>

					<div id="b_guardar" >
						<s:submit value="%{getText('midas.boton.guardar')}" 
							onclick="javascript:if(!validaNombreNegocio()) return false; else mostrarIndicadorCarga('indicador'); " 
							cssClass="b_submit"/>
					</div>
				</td>
			</tr>			
		</table>
</s:form>	
