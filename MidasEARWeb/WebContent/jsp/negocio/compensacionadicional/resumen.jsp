<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<s:include value="/jsp/negocio/negocioHeader.jsp"></s:include>

<table id="agregar" style="width: 97%">
   <tr>
   		<td>
   		    �Desea configurar Compensaciones Adicionales para este Negocio?
   		</td>
   		<td>
   		    <div id="b_agregar" style="width:70px;" >
	  			<a href="javascript: void(0);" onclick="javascript: compAdicAccordionChangeTo('idCompConfig');">Aceptar</a>
			</div>
   		</td>
   		<td>
   		    ID Compensaci&oacute;n <s:textfield name="compensacionAdicional.id" id="idCompensacion" readOnly="readonly"/>
   		</td>
   	</tr>
</table>

<div id ="compensacionesGrid" style="width:99%;height:200px"></div>
<div id="pagingArea"></div>
<div id="infoArea"></div>