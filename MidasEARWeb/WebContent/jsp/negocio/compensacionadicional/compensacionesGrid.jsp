<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/xml" %>

<rows>
	<head>
	     <beforeInit>
            <call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
            <call command="setSkin"><param>light</param></call>    
        </beforeInit>
        
		<column id="id" type="ro" width="100" sort="int" hidden="false"><s:text name="midas.negocio.producto.id" /></column>
		<column id="configuracion" type="ro" width="200" sort="str" hidden="false">Configuracion</column>
		<column id="tipoPersona" type="ro" width="200" sort="str" hidden="false">Tipo Persona</column>		
		<column id="nombre" type="ro" width="300" sort="str" hidden="false">Nombre</column>
		<column id="tipoCompensacion" type="ro" width="300" sort="str" hidden="false">Tipo Compensacion</column>
	</head>
	<s:iterator value="listaParametros">
	    <row id="<s:property value="id" />">
			<cell><![CDATA[<s:property value="caCompensacion.id"/>]]></cell>
			<s:if test="%{caCompensacion.contraprestacion}">
				<cell><![CDATA[CONTRAPRESTACION]]></cell>
			</s:if>
			<s:else>
				<cell><![CDATA[COMPENSACION]]></cell>
			</s:else>			
			<cell><![CDATA[<s:property value="caEntidadPersona.caTipoEntidad.nombre"/>]]></cell>
			<cell><![CDATA[<s:property value="caEntidadPersona.nombres"/>]]></cell>
			<cell><![CDATA[<s:property value="caTipoCompensacion.nombre"/>]]></cell>
		</row>
	</s:iterator>
</rows>