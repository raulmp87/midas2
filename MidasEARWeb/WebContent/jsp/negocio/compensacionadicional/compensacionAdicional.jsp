<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript">
    var idNegocioCompensacion = <s:property value="filtroCompensacion.idNegocio" />;
</script>
<input type="hidden" id="txtIdNegocio" value="<s:property value="filtroCompensacion.idNegocio" />"/>
<table id="agregar">
   <tr>
   		<td>
   		    ¿Desea configurar Compensaciones Adicionales para este Negocio?
   		</td>
   		<td colspan="3">
   		    <div id="b_agregar" style="width:70px;" >
	  			<a href="javascript: void(0);" onclick="javascript: wCompensacion(idNegocioCompensacion);">Aceptar</a>
			</div>
   		</td>
   	</tr>
</table>
<center>
	<div id ="compensacionesGrid" style="width:98%; height:200px; text-align:left;"></div>
	<div id="pagingArea"></div>
	<div id="infoArea"></div>
</center>
<div id="indicador"></div>

<center>
<div id="accordionCompensacionAdicional" style="position: relative; height:99%; width: 97%; display:none"></div>
</center>

<script type="text/javascript">
	
	function gridCompensaciones() {
    	var idNegocio = jQuery('#txtIdNegocio').val();
	    
	    document.getElementById("compensacionesGrid").innerHTML = '';
		document.getElementById("infoArea").innerHTML = '';
		var compensacionesGrid = new dhtmlXGridObject("compensacionesGrid");
		
		if (idNegocio > 0 ) {
			mostrarIndicadorCarga('indicador');	
			compensacionesGrid.attachEvent("onXLE", function(grid){
				ocultarIndicadorCarga('indicador');
			});	
			
		
			compensacionesGrid.load("/MidasWeb/negocio/compensacionadicional/mostrarResumen.action?filtroCompensacion.idNegocio="
						+ idNegocio);
		}
	}
	
    gridCompensaciones();
</script>