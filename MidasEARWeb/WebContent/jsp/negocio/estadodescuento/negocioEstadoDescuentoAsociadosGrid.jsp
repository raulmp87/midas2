<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="negocioEstadoDescuento.id" type="ro"  width="0"  sort="int" hidden="true">negocioEstadoDescuento.id</column>
	    <column id="negocioEstadoDescuento.estadoDTO.stateId" type="ro"  width="0"  sort="int" hidden="true">"negocioEstadoDescuento.estadoDTO.stateId"</column>
		<column id="negocioEstadoDescuento.estadoDTO.stateName" type="ro"  width="150"  sort="str" ><s:text name="midas.negocio.estadodescuento.estado"/></column>
		<column id="negocioEstadoDescuento.zona" type="ed"  width="100"  sort="str"><s:text name="midas.negocio.estadodescuento.zona"/></column>
		<column id="negocioEstadoDescuento.pctDescuento" type="ed"  width="80"  sort="int"><s:text name="midas.negocio.estadodescuento.pctMaximoDescuento"/></column>
		<column id="negocioEstadoDescuento.pctDescuentoDefault" type="ed"  width="80"  sort="int"><s:text name="midas.negocio.estadodescuento.pctDescuentoDefault"/></column>
		<column id="negocioEstadoDescuento.aplicaSeguroObligatorio" type="ch"  width="80"  sort="int"><s:text name="midas.negocio.estadodescuento.aplicaSeguroObligatorio"/></column>
		<column id="negocioEstadoDescuento.zipCode" type="ed"  width="100"  sort="int"><s:text name="midas.negocio.estadodescuento.zipCode"/></column>
	</head>
	
	<% int a=0;%>
	<s:iterator value="relacionesNegocioEstadoDescuento.asociadas">
		<% a+=1; %>
		<row id="<%=a%>">
		    <cell><s:property value="id" escapeHtml="false" escapeXml="false"/></cell>
		    <cell><s:property value="estadoDTO.stateId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estadoDTO.stateName" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="zona" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="pctDescuento" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="pctDescuentoDefault" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="aplicaSeguroObligatorio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="zipCode" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>