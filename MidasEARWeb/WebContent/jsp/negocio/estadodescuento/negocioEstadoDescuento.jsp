    <%@taglib prefix="s" uri="/struts-tags" %>
    <%@taglib prefix="sj" uri="/struts-jquery-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">	
	<link href="<s:url value='/css/dhtmlxgrid_skins.css'/>" rel="stylesheet" type="text/css">
	<sj:head/>
	<s:include value="/jsp/negocio/estadodescuento/negocioEstadoDescuentoHeader.jsp"></s:include>
	<script type="text/javascript">
		var mensaje = '<s:property value="mensaje" default="-1"/>';
		var tipoMensaje = '<s:property value="tipoMensaje" default="-1"/>';
		function guardarEstadoDescuento(node){
			mostrarMensajeInformativo(node.firstChild.data,node.getAttribute("tipo")); 
		}
	</script>
<s:form action="mostrar">
<div>
<s:hidden id="id" name="id"/>
	<center>
		<div align="left" class="subtitulo" id="labelValidaEstadoDescuento" >
		</div>
		<table id="desplegarDetalle" border="0"> 
		 <tr>
		  <td>
		  <s:text name="midas.negocio.estadodescuento.catalogo"/>
		  <br><br>
		  </td>
		  </tr>
			<tr>
				<td colspan="4">
					<s:text name="midas.negocio.estadodescuento.asociados"/>
				</td>
				
			</tr>
			<tr>
				<td colspan="4">
					<div id="negocioEstadoDescuentoAsociadosGrid" class="dataGridConfigurationClass" style="width:650px; height:300px;"></div>
						<div style="margin-left: 434px">
								<div id="b_guardar" style="100px;">
									<a href="javascript: void(0);"
										onclick="javascript: negocioEstadoDescuentoProcessor.sendData()"> <s:text
											name="midas.boton.guardar" /> </a>
								</div>
						</div>
				</td>
			</tr>
		</table>
	</center>
</div>	
</s:form>
<script type="text/javascript">
iniciaGridsEstadoDescuento();
</script>