<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="<s:url value='/dwr/interface/TipoAccionDTO.js'/>"></script>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/struts/css_xhtml/validation.js" type="text/javascript"></script>
<script src="<s:url value='/js/midas2/negocio/negocio.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/auto/incisovehiculo.js'/>"></script>
<script src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionExpress.js'/>"></script>

<script type="text/javascript">
    var listarFiltradoNegocioPath = '<s:url action="listarFiltrado" namespace="/negocio"/>';
	var verDetalleNegocioPath = '<s:url action="verDetalle" namespace="/negocio"/>';
	var verMostrarVentanaProductosPath = '<s:url action="mostrarAsignarProducto" namespace="/negocio/producto"/>';
	var verListaProductosPath = '<s:url action="listarProductos" namespace="/negocio"/>';
	var guardarNegocioPath = '<s:url action="guardar" namespace="/negocio"/>';
	var listarNegocioPath = '<s:url action="listar" namespace="/negocio"/>';
	var imprimirNegocioPath = '<s:url action="imprimirConfiguracionNegocio" namespace="/impresiones/poliza"/>';
	var mostrarImprimirPath = '<s:url action="mostrarImpresiones" namespace="/impresiones/poliza"/>';
</script>