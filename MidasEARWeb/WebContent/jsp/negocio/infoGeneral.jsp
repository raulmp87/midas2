<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="mx.com.afirme.midas2.domain.negocio.Negocio"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

    <%@taglib prefix="s" uri="/struts-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
    <script src="<s:url value='/js/midas2/negocio/negocio.js'/>"></script>
    <s:include value="/jsp/negocio/negocioHeader.jsp"></s:include>
    <s:if test="tipoAccion == catalogoTipoAccionDTO.agregarModificar">
	<s:set id="readOnly" value="false" />
	<s:set id="requiredField" value="true" />
	<s:if test="id != null">
		<s:set id="readEditOnly" value="true" />
		<s:set id="requiredEditField" value="false" />
	</s:if>
	<s:else>
		<s:set id="readEditOnly" value="false" />
		<s:set id="requiredEditField" value="true" />
	</s:else>
</s:if>
<s:form action="guardar" id="negocioForm">
	<s:hidden name="id" id="id" />
	<s:hidden name="tipoAccion" id="tipoAccion" />
	<s:hidden name="negocio.formulaDividendos" value="0"/>
	<s:hidden name="claveNegocio" id="claveNegocio"/>
	<table  id="agregar">
		<tr>
			<th width="110">
				<s:text name="midas.negocio.nombre" />&nbsp;<font color="red">*</font>
			</th>
			<td width="190">
				<s:textfield name="negocio.descripcionNegocio" id="txtDescripcion" 
					labelposition="left" maxlength="200" size="30" 
					cssClass="txtfield" required="true" 	
					cssClass="jQToUpper jQAlphaExtra jQRestric"/>
			</td>
			<th width="140">
				<s:text name="midas.negocio.fechaVencimiento" />
			</th>
			<td width="200">								
				<sj:datepicker name="negocio.fechaFinVigencia"
							   buttonImage="../img/b_calendario.gif"
							   id="fecha" maxlength="10" cssClass="txtfield"			
							   minDate="today+1"					   								  
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							   onblur="esFechaValida(this);"></sj:datepicker>			
			</td>
			<s:if test="negocio.fechaFinVigencia == null">
				<th align="left">
					<s:text name="midas.negocio.abierto" /> 
				</th>
				<td>&nbsp;</td>	
			</s:if>					
		</tr> 
		<tr>
			<th width="110">
				<s:text name="midas.negocio.idToNegocio" /> 
			</th>
			<td width="190">
				<s:property value="negocio.idToNegocio" />
			</td>					
			<th width="140">
				<s:text name="midas.negocio.fechaCreacion" /> 
			</th>
			<td width="200">
				<s:date name="negocio.fechaCreacion" format="dd/MM/yyyy hh:mm:ss" />
			</td>
		</tr>
		<tr>
			<th width="110">
				<s:text name="midas.negocio.usuarioActivacion" /> 
			</th>
			<td width="190">
					<s:property value="usuarioCreacion" default="MIDAS"/> 
			</td>					
			<th  width="140">
				<s:text name="midas.negocio.fechaOperacion" />&nbsp;<font color="red">*</font>
			</th>
			<td width="200">
				<sj:datepicker name="negocio.fechaInicioVigencia"  required="#requiredEditField" 
							   buttonImage="../img/b_calendario.gif"
							   id="fecha2" maxlength="10" cssClass="txtfield"	
							   onkeypress="return soloFecha(this, event, false);"
							   onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							   onblur="esFechaValida(this);"></sj:datepicker>
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
		 	<td align="right">
				<div id="b_guardar" style="width:70px;" >
					<a href="javascript: void(0);" onclick="javascript:if(!validaciones()) return false; else guardar();">	<s:text name="midas.boton.guardar"/>	</a>
                </div>						
			</td>			
		</tr>
		<tr>
		  <td colspan="2"> 
				<span style="color:red">
					<s:text name="midas.catalogos.mensaje.requerido"/>
				</span>
			</td>
		</tr>						
	</table>
	<table width="100%">
		<tr>
			<td width="11%">
				<s:if test="id != null">
					<div id="b_agregar" style="width:170px; margin-left: 8px" >
						<a href="javascript: void(0);" onclick="javascript:mostrarAsignarProducto();return false;">	
							<s:text name="midas.boton.agergarNuevoProducto"/>	
						</a>
                    </div>
                </s:if>	
                <s:else>&nbsp;</s:else>					
			</td>
			<td width="11%"></td>
			<td width="11%"></td>
			<td width="11%">
				<s:if test="id != null">
                </s:if>		
			</td>
			<td width="11%">
			
				<s:if test="id != null && negocio.claveEstatus == 0">
					<div id="b_generico" style="width:150px;" >
						<a href="javascript: void(0);" onclick="activarNegocio();" >	
							<s:text name="midas.boton.autorizarNegocio"/>	
						</a>
                    </div>
                </s:if>	
                <s:else>&nbsp;</s:else>		
            					
			</td>			
			<td width="11%">
			<s:if test="negocio.claveEstatus == 1">	
					<div id="b_nuevaVersion" style="width:150px;" >
						<a href="javascript: void(0);" onclick="javascript:desplegarCopiarNegocio();" >	
							<s:text name="midas.negocio.copiarNegocio"/>	
						</a>
                    </div>
            
                <s:else>&nbsp;</s:else>		
          </s:if>  		
			</td>	
		</tr>
		
	
		
	</table>

</s:form>
<div id="indicador"></div>
<center>
<div id ="productosGrid" style="width:99%;height:200px"></div>
<div id="pagingArea"></div><div id="infoArea"></div>
</center>
	
<script type="text/javascript">
	poblarProductos();
</script>


<table><tr>&nbsp;</tr>
		<tr>
			<td colspan="10"> 
				<div id="divRegresarBtn" style="display: block; float:right;">
					<div class="btn_back"  > 
						<s:submit key="midas.boton.regresar" onclick="listar(); return false;"
								  cssClass="b_submit icon_regresar w100"/> 
					</div>
   	 			</div>
			</td>
		</tr>
	
</table>			