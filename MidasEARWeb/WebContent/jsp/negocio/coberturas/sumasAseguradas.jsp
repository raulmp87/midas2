<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/negocio/coberturas/sumasAseguradasHeader.jsp"/>

<div id="detalle" name="Detalle">
	<center>
		<s:form action="mostrarSumasAseguradas">
			
			<s:hidden name="idToNegCobPaqSeccion"/>
		
			<table  id="agregar" bgcolor="#EDFAE1" border="0">
				<tr>
					<td class="titulo" colspan="6"><s:text name="midas.negocio.cobertura.sumasaseguradas" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="sumasAseguradasGrid" width="430px" height="250px" style="background-color:white;overflow:hidden"></div>
					</td>
					<td colspan="2">
						<div id="b_agregar">
							<a href="javascript: void(0);" onclick="javascript: agregarSumaAsegurada();">Agregar</a>
						</div>
						<br/><br/>
						<div id="b_borrar">
							<a href="javascript: void(0);" onclick="javascript: eliminarSumaAsegurada(); ">Eliminar</a>
						</div>	
					</td>
				</tr>
				<tr>
					<td colspan="6"><midas:mensaje clave="configuracion.eliminar.seleccionado.mensaje"/></td>
				</tr>			
			</table>
							
		</s:form>
		<script type="text/javascript">
			iniciaGridSumasAseguradasCobertura();
		</script>
	</center>
</div>