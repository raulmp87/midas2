<?xml version="1.0" encoding="iso-8859-1"?>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<%@ page isELIgnored="false"%>
<s:if test="respuesta.tipoMensaje !=null">
<data>

	<action type="mensajeGenerico"  
			sid="<s:property value="respuesta.idOriginal" escapeHtml="false" />" 
			tid="<s:property value="respuesta.idResultado" escapeHtml="false" />"
			operacionExitosa="<s:property value="respuesta.operacionExitosa" escapeHtml="false" />"
			tipoMensaje="<s:property value="respuesta.tipoMensaje" escapeHtml="false" />">
			
			
	
	<s:if test="respuesta.mensaje !=null">
		<s:text name="respuesta.mensaje"/>
	</s:if>
	<s:elseif test="respuesta.mensaje==null">
		<s:text name="midas.negocio.exito"/>
	</s:elseif>
	</action>
	<action type="mensaje" tipo="<s:property value='tipoMensaje'/>" nivel="<s:property value='nivelActivo'/>">
		<s:if test="tipoMensaje == 30">
			<s:text name="midas.negocio.exito"/>
		</s:if>
		<s:elseif test="tipoMensaje == 10">
			<s:text name="midas.negocio.error"/>
		</s:elseif> 
	</action>
 </data>
</s:if>