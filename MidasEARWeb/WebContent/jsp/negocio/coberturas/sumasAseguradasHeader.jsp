<%@taglib prefix="s" uri="/struts-tags" %>
	<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
	
	<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
	
	<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
	<script src="<s:url value='/js/midas2/negocio/sumasaseguradas/sumasAseguradas.js'/>"></script>
	
	<script type="text/javascript">
		var mostrarSumasAseguradasCobertura = '<s:url action="mostrarSumasAseguradasCobertura" namespace="/negocio/cobertura"/>';
		var obtenerSumasAseguradasCobertura = '<s:url action="obtenerSumasAseguradasCobertura" namespace="/negocio/cobertura"/>';
		var accionSobreSumasAseguradasCobertura = '<s:url action="accionSobreSumasAseguradasCobertura" namespace="/negocio/cobertura"/>';			
	</script>