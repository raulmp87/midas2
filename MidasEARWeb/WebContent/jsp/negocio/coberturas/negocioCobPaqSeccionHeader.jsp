<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="/jsp/catalogos/catalogosHeader.jsp"></s:include>
<script src="<s:url value='/js/midas2/negocio/coberturas/negocioCobPaqSeccion.js'/>"></script>


<script type="text/javascript">
    var obtenerNegocioSeccionCobPaqAsociadasPath = '<s:url action="obtenerNegocioCobPaqSeccionAsociadas" namespace="/negocio/cobertura"/>';
    var obtenerNegocioSeccionCobPaqDisponiblesPath = '<s:url action="obtenerNegocioCobPaqSeccionDisponibles" namespace="/negocio/cobertura"/>';
    var relacionarNegocioSeccionCobPaqPath = '<s:url action="relacionarNegocioCobPaqSeccionDisponibles" namespace="/negocio/cobertura"/>';
    var autocompletarBusquedaAgentesPath = '<s:url action="autocompletarBusquedaAgentes" namespace="/negocio/ligaAgente"/>';
</script>
