<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<column id="negocioCobPaqSeccion.coberturaDTO.descripcion" type="ro" width="*" sort="str" ><s:text name="midas.catologos.cobpaquetessecciones.descripcion"/></column>
		<column id="deducibles" type="img" width="80" align="center" hidden="true">Deducible</column>		
		<column id="negocioCobPaqSeccion.idToNegCobPaqSeccion" type="ro" width="0" sort="int" hidden="true">idToNegCobPaqSeccion</column>
		<column id="negocioCobPaqSeccion.coberturaDTO.idToCobertura" type="ro" width="0" sort="int" hidden="true">idToCobertura</column>
		<column id="negocioCobPaqSeccion.negocioPaqueteSeccion.idToNegPaqueteSeccion" type="ro" width="0" sort="int" hidden="true">idToNegPaqueteSeccion</column>
		<column id="negocioCobPaqSeccion.monedaDTO.idTcMoneda" type="ro" width="0" sort="int" hidden="true">idTcMoneda</column>
		<column id="negocioCobPaqSeccion.estadoDTO.stateId" type="ro" width="0" sort="str" hidden="true">stateId</column>
		<column id="negocioCobPaqSeccion.ciudadDTO.cityId" type="ro" width="0" sort="str" hidden="true">cityId</column>
		<column id="negocioCobPaqSeccion.claveTipoDeduciblePT" type="ron" width="95" hidden="true">Deducible PT</column>
		<column id="negocioCobPaqSeccion.claveTipoDeduciblePP" type="ron" width="95" hidden="true">Deducible PP</column>
		<column id="negocioCobPaqSeccion.claveTipoSumaAsegurada" type="ro" width="0" sort="int" hidden="true">Tipo SA</column>
		<column id="claveTipoSumaAseguradaStr" type="ro" width="120" sort="str" hidden="true" >Tipo SA</column>
		<column id="negocioCobPaqSeccion.valorSumaAseguradaMin"  type="ron" width="100"  sort="int" hidden="true">Valor SA Min</column>
		<column id="negocioCobPaqSeccion.valorSumaAseguradaMax"  type="ron" width="100"  sort="int" hidden="true">Valor SA Max</column>
		<column id="negocioCobPaqSeccion.valorSumaAseguradaDefault"  type="ron" width="120" sort="int" hidden="true">Valor SA Default</column>
		<column id="negocioCobPaqSeccion.tipoUsoVehiculo.idTcTipoUsoVehiculo" type="ro" width="0" sort="str" hidden="true">idTcTipoUsoVehiculo</column>
		<column id="negocioCobPaqSeccion.agente.id" type="ro" width="0" sort="str" hidden="true">id</column>
		<column id="negocioCobPaqSeccion.renovacionString" type="ro" width="0" sort="str" hidden="true">renovacion</column>
				
	</head>
		
	<% int a=0;%>
	<s:iterator value="relacionesNegocioCoberturaDTO.disponibles">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="coberturaDTO.descripcion" escapeHtml="false" escapeXml="true" /></cell>
			<cell>/MidasWeb/img/icons/ico_editar.gif^Editar^javascript: opcionCaptura(2,<s:property value="idToSolicitud"/>)^_self</cell>
			<cell><s:property value="idToNegCobPaqSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="coberturaDTO.idToCobertura" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="negocioPaqueteSeccion.idToNegPaqueteSeccion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="monedaDTO.idTcMoneda" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estadoDTO.stateId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="ciudadDTO.cityId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoDeduciblePT" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoDeduciblePP" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoSumaAsegurada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="claveTipoSumaAseguradaStr" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaMin" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaMax" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="valorSumaAseguradaDefault" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoUsoVehiculo.idTcTipoUsoVehiculo" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="agente.id" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="renovacionString" escapeHtml="false" escapeXml="true"/></cell>			
			
		</row>
	</s:iterator>	
</rows>
