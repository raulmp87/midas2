<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<s:include value="/jsp/negocio/copiarNegocioHeader.jsp"></s:include>
<s:include value="/jsp/negocio/coberturas/negocioCobPaqSeccionHeader.jsp"></s:include>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script language="JavaScript" src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 25px;
	position: relative;
}
.divFormulario {
	height: 25px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}

.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}
.textarea-link {
	border: 1px solid #CCCCCC;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	z-index: 1;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
</style>

<s:form action="mostrar" >
	<s:hidden name="idToNegProducto" /> 
	<s:hidden name="configuracion.agente.id" id="agenteId" />
	
	<table id="desplegarDetalle" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<s:text name="midas.general.cobertura" /> 
			</td>
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.seccion.tiposPoliza" />
			</th>
			<th>
				<s:text name="midas.negocio.seccion.disponibles" />
			</th>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.paquete" /> 
			</th> 
			<th>
				<s:text name="midas.general.moneda" /> 
			</th> 
		</tr>
		<tr>
			<td>
				<s:select id="idToTipoPoliza" cssClass="cajaTexto" 
					list="negocioTipoPolizaList"  
					name="idToTipoPoliza" headerKey="-1"
					listKey="idToNegTipoPoliza" headerKey="-1"
					listValue="tipoPolizaDTO.descripcion"					 
					headerValue="%{getText('midas.general.seleccione')}" required="#requiredField"
					OnChange="obtenerLineasDeNegocio(this.value); obtenerMonedas(this.value);" />
			</td>					
 			<td> 
				<s:select id="idToNegSeccion" cssClass="cajaTexto"  
					name="idToNegSeccion"  
					list="secciones" listKey="idToNegSeccion" listValue=""  
					OnChange="obtenerPaquetes(this.value); obtenerTipoUsoVehiculo(this.value);" 
					headerKey="-1" headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 			
 			<td > 
				<s:select name="idToNegPaqueteSeccion" id="idToNegPaqueteSeccion"  
					list="paquetes" listKey="idToNegPaqueteSeccion" listValue=""  cssClass="cajaTexto" 
					OnChange="onChangeComboPaquete(this.value);" headerKey="-1" 
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 
 			<td>  
				<s:select name="idTcMoneda" id="idTcMoneda"  cssClass="cajaTexto" 
					list="monedaList" listKey="idTcMoneda" listValue=""  
					OnChange="onChangeComboMoneda(this.value);" headerKey="-1"  
					headerValue="%{getText('midas.general.seleccione')}"/> 
 			</td> 
		</tr>
		<tr>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.estado" />
			</th>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.municipio" />
			</th>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.tipouso" />
			</th> 
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.agente" />
			</th>
		</tr>
		<tr>
 			<td >  
 				<s:select cssClass="cajaTexto" name="stateId" id="stateId" 
 					disabled="true" list="estadoList" listKey="stateId" 
 					listValue="stateName" 
 					OnChange="obtieneMunicipio(this.value);" headerKey=""  
 					headerValue="%{getText('midas.general.seleccione')}"
 				/> 
 			</td> 
 			<td >  
 				<s:select cssClass="cajaTexto" name="cityId" id="cityId"  
 					disabled="true" list="municipioList" listKey="cityId" 
 					listValue=""  headerKey="" 
 					OnChange="onChangeComboMunicipio(this.value);"  
 					headerValue="%{getText('midas.general.seleccione')}"
 				/> 
 			</td>	
 			<td >  
 				<s:select cssClass="cajaTexto" name="idTcTipoUsoVehiculo" id="idTcTipoUsoVehiculo"  
 					disabled="true" list="tipoUsoVehiculoList" listKey="idTcTipoUsoVehiculo" 
 					listValue=""  headerKey="-1"
 					OnChange="onChangeComboTipoUsoVehiculo(this.value);" 
 					headerValue="%{getText('midas.general.seleccione')}"
 				/> 
 			</td>
 			<td>
 				<input	id="descripcionBusquedaAgente"
 						value='${configuracion.agente.persona.nombreCompleto} - ${configuracion.agente.idAgente}' 
						class="txtfield" type="text"
						style="width:200px;"
						disabled="true"/>
				<img	id="iconoBorrar"
						src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" 
				    	title="Limpiar descripción" onclick ="limpiarAgente();"/>
 			</td>
		</tr>
		
		<tr>
			<th>
				<s:text name="midas.negocio.cobertura.paquete.seccion.renovacion" />
			</th> 
		</tr>
		<tr>
			<td>
							
				<s:select cssClass="cajaTexto" id="idRenovacion"  
 					disabled="true" list="#{'true':'SI','false':'NO'}"
 					OnChange="onChangeRenovacion(this);" 
 					headerKey="null" headerValue="%{getText('midas.general.seleccione')}"
 				/> 
				
			</td>
		</tr>
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.cobertura.paquete.seccion.asociadas" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioSeccionCobPaqAsociadasGrid" style="width:90%;height:220px"
							class="dataGridConfigurationClass"></div></td>
				</tr>
				<tr>
					<td colspan="4">
						<s:text name="midas.negocio.cobertura.paquete.seccion.disponibles" />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="negocioSeccionCobPaqDisponiblesGrid" style="width:90%;height:220px"
							class="dataGridConfigurationClass"></div></td>
				</tr>
				<tr>
					<td colspan="4">
						<midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"></midas:mensaje>
					</td>
				</tr>
			</table>
		</s:form>

<script>
	jQuery(document).ready(function() {
		habilitarAutocompletarBusquedaAgente();
		limpiarAgenteSiEsVacioAlInicio();
	});	
</script>
