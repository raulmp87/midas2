<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		<column id="idSumaAsegurada" type="ro" width="0" hidden="true">id</column>
		<column id="valorSumaAsegurada" type="edn"  width="*" >Valor</column>
		<column id="defaultValor" type="ra" width="200" align ="center">Clave Default</column>
		
	</head>
		
	<% int a=0;%>
	<s:iterator value="sumasAseguradasList">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="idSumaAsegurada" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="valorSumaAsegurada" escapeHtml="false" escapeXml="true" /></cell>
			<cell><s:property value="defaultValor" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
</rows>