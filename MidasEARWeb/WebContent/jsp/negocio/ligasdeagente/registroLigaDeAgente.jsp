<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/utileriasService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-1.4.3.min.js'/>"></script>
<script language="JavaScript" src='<s:url value="/js/midas2/jQuery/jquery-ui-1.8.16.custom.min.js"></s:url>'></script>
<script type="text/javascript">
	jquery143 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<link href="<s:url value="/css/jquery-ui-1.8.16.custom.css"/>" rel="stylesheet" type="text/css"/>

<style type="text/css">
div.ui-datepicker {
	font-size: 10px;
}

#divSup {
	height: 300px;
}

#divInf {
	height: 200px;
}

.divInterno {
	height: 25px;
	position: relative;
}
.divFormulario {
	height: 25px;
	position: relative;
}

.divContenedorO {
	border: 1px solid #28b31a;
}

.divContenedorU {
	border-bottom: 1px solid #28b31a;
	border-right: 1px solid #28b31a;
}

.estilodias {
	 background-color: #FFCCCC;
	 font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: red;
}


.floatRight {
	float: right;
	position: relative;
}

.floatLeft {
	float: left;
	position: relative;
}
.floatLeftNoLabel{
	float: left;
	position: relative;
	margin-top: 12px;
}
.divInfDivInterno {
	width: 17%;
}
.error {
	background-color: red;
	opacity: 0.4;
}
#b_copiar {
	background-image: url("../img/icons/ico_copiar1.gif");
	background-repeat: no-repeat;
	text-align: center;
}
.ui-autocomplete-loading { background: white url('<s:url value="/img/icons/ui-anim_basic_16x16.gif"/>') right center no-repeat; }

.ui-autocomplete {
	max-height: 120px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 10px;
}
.textarea-link {
	border: 1px solid #CCCCCC;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	z-index: 1;
}

/* IE 6 doesn't support max-height
                  * we use height instead, but this forces the menu to always be this tall
                  */
* html .ui-autocomplete {
	height: 120px;
}
</style>

<script type="text/javascript">
	var guardarConfiguracionLigaAgentePath = '<s:url action="guardarConfiguracionLigaAgente" namespace="/negocio/ligaAgente"/>';
	var mostrarBusquedaConfiguracionesLigaAgentePath = '<s:url action="mostrarContenedorLigasAgentes" namespace="/negocio/ligaAgente"/>';
	var enviarLigaAgentePath = '<s:url action="enviarLigaAgente" namespace="/negocio/ligaAgente"/>';
	var generarLigaAgentePath = '<s:url action="generarLigaAgente" namespace="/negocio/ligaAgente"/>';
	var autocompletarBusquedaAgentesPath = '<s:url action="autocompletarBusquedaAgentes" namespace="/negocio/ligaAgente"/>';
	var cargarInfoContactoAgentePath = '<s:url action="cargarInformacionContactoAgente" namespace="/negocio/ligaAgente"/>'; 
</script>

<s:form id="registroConfiguaracionForm" class="floatLeft">
<div id="contenido_registroConfiguracion" style="width:98%;">
	<s:hidden name="esConsulta" id="esConsulta" />
	<s:hidden name="configuracion.agente.id" id="agenteId" />
	<s:hidden name="configuracion.token" id="token" />
		
		<!--<s:hidden id="idReporteCabina" name="idReporteCabina" />-->
		
		<div class="titulo" align="left" >
		<s:text name="midas.ligaagente.config.registro.titulo" />
		</div>
		<div id="contenedorFiltros" >
			<table id="agregar" style="width:98%;" border="0">
				<tr>
					<th><s:text name="midas.ligaagente.config.registro.codigo"/></th>
					<td>
						<s:textfield id="codigo" name="configuracion.id" readonly="true" cssClass="txtfield w200" cssStyle="background-color:#E3E3E3;"></s:textfield>
					</td>
					<th><s:text name="midas.ligaagente.config.registro.fechaCreacion"/></th>
					<td>
						<s:textfield id="fechaCreacion" name="configuracion.fechaCreacion" readonly="true" cssClass="txtfield w200" cssStyle="background-color:#E3E3E3;"></s:textfield>
					</td>
					<th><s:text name="midas.ligaagente.config.registro.usuarioCreacion"/></th>
					<td>
						<s:textfield id="usuarioCreacion" name="configuracion.codigoUsuarioCreacion" readonly="true" cssClass="txtfield w200" cssStyle="background-color:#E3E3E3;"></s:textfield>
					</td>
					
				</tr>
				<tr>
					<th><s:text name="midas.ligaagente.config.registro.nombre"/></th>
					<td>
						<s:textfield id="nombre" name="configuracion.nombre" cssClass="txtfield w200 esConsulta jQrequired" ></s:textfield>
					</td>
					<th><s:text name="midas.ligaagente.config.registro.descripcion"/></th>
					<td colspan="3">
						<s:textfield id="descripcion" name="configuracion.descripcion" cssClass="txtfield w610 esConsulta" ></s:textfield>
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.ligaagente.config.registro.agente"/></th>
					<td>
						<s:if test="esConsulta" >
							<input id="descripcionBusquedaAgente" value="${configuracion.agente.persona.nombreCompleto} - ${configuracion.agente.idAgente}" 
								class="txtfield esConsulta jQrequired" style="width:200px;" type="text" onblur="cargarInformacionContactoAgente();"></input>
						</s:if>
						<s:else >
							<input id="descripcionBusquedaAgente" value='${configuracion.agente.persona.nombreCompleto} - ${configuracion.agente.idAgente}' 
								class="txtfield esConsulta jQrequired" type="text" style="width:200px;" onblur="cargarInformacionContactoAgente();"></input>
							<img id="iconoBorrar" src='<s:url value="/img/close2.gif"/>' style="vertical-align: bottom;"  alt="Limpiar descripción" 
				    					title="Limpiar descripción" onclick ="limpiarAgente();"/>
			    		</s:else>
					</td>
					<th><s:text name="midas.ligaagente.config.correo"/></th>
					<td>
						<s:textfield id="correo" name="configuracion.correo" cssClass="txtfield w200 esConsulta jQrequired" ></s:textfield>
					</td>
					<th><s:text name="midas.ligaagente.config.registro.telefono"/></th>
					<td>
						<s:textfield id="telefono" name="configuracion.telefono" cssClass="txtfield w200 esConsulta jQrequired jQnumeric jQrestrict" maxlength="15"></s:textfield>
					</td>
				</tr>
				<tr>
					<th><s:text name="midas.ligaagente.config.registro.negocio"/></th>
					<td>
						<s:select list="negociosAgente" id="negocios"
									name="configuracion.negocio.idToNegocio"
									listKey="idToNegocio" listValue="descripcionNegocio"
									cssClass="txtfield w200 deshabilitar jQrequired" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}" >
								</s:select>
					</td>
					<th><div id="permitir" ><s:text name="Permitir"/></div></th>
					<td colspan="1">
						<table class="formulario">
							<tr>
								<td>									
				            		<s:label for="cotizarReadOnly" name="cotizar" value= "cotizar" />
				            		<s:checkbox name="cotizarReadOnly" value="true" disabled="true" 
				            		title="Por default es permitido cotizar polizas mediante esta liga de agente"/>	            	
            		
								</td>								
								<td>									
				            		<s:label for="configuracion.emitir" name="emitir" value= "emitir"/>
				            		<s:checkbox name="configuracion.emitir" disabled="esConsulta"
				            		title="Seleccione esta opcion si desea permitir la emision de polizas desde esta liga de agente "/>	            	
            		
								</td>
							</tr>
						</table>            		          	
            		</td>	
            		<td colspan="1">
            		</td>
            		<td colspan="1">
            		</td>				
				</tr>
				<tr>
					<th><s:text name="midas.ligaagente.config.registro.estatus"/></th>
					<td colspan="1">
						<s:select list="listaEstatus" id="estatus"
									name="configuracion.estatus"
									cssClass="txtfield w200 deshabilitar jQrequired" headerKey=""
									headerValue="%{getText('midas.general.seleccione')}" 
									onchange="onChangeEstatus();">
								</s:select>
					</td>
					<th><div id="comentario_titulo" ><s:text name="midas.ligaagente.config.registro.comentario"/></div></th>
					<td colspan="3" rowspan="1">
						<s:textarea  
						    			maxlength="500"
						    			name="configuracion.comentarioSuspension"							
										rows="3"
										disabled="false"
										cssClass="textarea w610 esConsulta"			
									    id="comentario"					   
									   />
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<s:if test="configuracion.liga != null && configuracion.liga != \"\" ">
							<div id='btnCopiarLiga' class="btn_back w110">
								<a href="javascript: void(0);" onclick="copiarLigaAgente();">
									<s:text name="midas.ligaagente.config.registro.copiar" /> </a>
							</div>
						</s:if>
					</td>
					<th><s:text name="midas.ligaagente.config.registro.ligagenerada"/></th>
					<td colspan="3" rowspan="1">
						<s:textarea  
						    			maxlength="500"
						    			name="configuracion.liga"							
										rows="3"
										disabled="false"
										cssClass="textarea-link allowsLowerCase w610"
										cssStyle="text-transform: none;background-color:#E3E3E3;"							
									    id="liga"
									    readonly="true"		   
									   />
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<s:if test="configuracion.liga != null && configuracion.liga != \"\" ">
							<div id='btnEnviarLiga' class="btn_back w110">
								<a href="javascript: void(0);" onclick="enviarLigaAgente();">
									<s:text name="midas.ligaagente.config.registro.enviar" /> </a>
							</div>
						</s:if>
					</td>
					<td colspan="4">
					</td>
				</tr>	
			</table>
	
			<div class="divFormulario" style="padding-top: 10px;">

				<div id='btnCerrar' class="btn_back w80"
					style="display: inline; margin-left: 1%; float: right;">
					<a href="javascript: void(0);" onclick="mostrarBusquedaConfiguracionesLigaAgente();">
						<s:text name="midas.boton.cerrar" /> </a>
				</div>
				
				<div id='btnGuardar' class="btn_back w80"
					style="display: inline; margin-left: 1%; float: right;">
					<a href="javascript: void(0);" onclick="guardarConfiguracionLigaAgente();">
						<s:text name="midas.boton.guardar" /> </a>
				</div>
				
				<s:if test="configuracion.id != null ">
					<div id='btnGenerar'class="btn_back w80"
						style="display: inline; margin-left: 1%; float: right;">
						<a href="javascript: void(0);"
							onclick="generarLigaAgente();"> <s:text
								name="midas.ligaagente.config.registro.generarLiga" /> </a>
					</div>
				</s:if>
			</div>
						
		</div>
		
</div>
</s:form>
<script type="text/javascript" src="<s:url value='/js/negocio/ligasdeagente/ligasAgentes.js'/>"></script>
<script>
	jQuery(document).ready(function() {
		inicializarRegistroLigas();
		habilitarAutocompletarBusquedaAgente();
		limpiarAgenteSiEsVacioAlInicio();
	});	
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>