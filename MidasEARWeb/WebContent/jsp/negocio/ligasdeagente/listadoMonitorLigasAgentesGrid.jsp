<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>

<rows total_count="<s:property value="totalCount"/>" pos="<s:property value="posStart"/>">

	<s:iterator value="monitoreoLigasAgentes">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idAgente" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="descripcionNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="idCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="prospectoCotizacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="telefonoProspecto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="correoProspecto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="producto" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="tipoPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="lineaNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="marca" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="modelo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcionFinalVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroSerieVehiculo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaNeta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="primaTotal" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroPoliza" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaEmision" escapeHtml="false" escapeXml="true"/></cell>
		</row>
	</s:iterator>
	
	<userdata name="userData_numeroCotizaciones"><s:property value="numeroCotizaciones" escapeHtml="false" escapeXml="true"/></userdata>
	<userdata name="userData_numeroPolizas"><s:property value="numeroPolizas" escapeHtml="false" escapeXml="true"/></userdata>
	
</rows>