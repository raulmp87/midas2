<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript">
var listarLigasAgentesPath = '<s:url action="listarLigasAgentes" namespace="/negocio/ligaAgente"/>';
var exportarLigasAgentesPath = '<s:url action="exportarLigasAgentes" namespace="/negocio/ligaAgente"/>';
var monitorLigasAgentesPath = '<s:url action="mostrarContenedorMonitorLigas" namespace="/negocio/ligaAgente"/>';
var mostrarRegistroLigasAgentesPath = '<s:url action="mostrarRegistro" namespace="/negocio/ligaAgente"/>';
var enviarLigaAgentePath = '<s:url action="enviarLigaAgente" namespace="/negocio/ligaAgente"/>';
</script>

<s:form id="ligasAgentesForm" >

	<table  id="filtrosM2" width="98%">
            <tr>
                  <td class="titulo" colspan="6"><s:text name="midas.ligaagente.config.busqueda" /></td>
            </tr>
          
          	<tr>
            	<td>
	            	<s:textfield name="configLigaFiltro.id" id="codigo" cssClass="cajaTextoM2 w230 jQnumeric jQrestrict" 
	            		label="%{getText('midas.ligaagente.config.codigo')}" disabled="false" />
            	</td>
            	
            	<td>
	            	<s:textfield name="configLigaFiltro.nombre" id="nombre" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.ligaagente.config.nombre')}" disabled="false" />
            	</td>
            	
            	<td  align="left"> <sj:datepicker name="configLigaFiltro.fechaInicio"
					id="fechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					maxDate="today"	cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.ligaagente.config.fechaini')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
            	
            	<td colspan="3" align="left"> <sj:datepicker name="configLigaFiltro.fechaFin"
					id="fechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					maxDate="today"	cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.ligaagente.config.fechafin')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
            	
            </tr>
            
            <tr>
            	
            	<td >
					<s:select list="listaEstatus" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="configLigaFiltro.estatus" id="estatus" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.ligaagente.config.estatus')}"/>
            	</td>
            	
            	<td >
					<s:select list="negocios" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="configLigaFiltro.idNegocio" listKey="idToNegocio" listValue="descripcionNegocio" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.ligaagente.config.negocio')}"/>
            	</td>
            	
            	
            	
            	<td>
	            	<s:textfield name="configLigaFiltro.nombreAgente" id="nombreAgente" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.ligaagente.config.agente')}" disabled="false" />
            	</td>
            	
            	<td colspan="3">
            		<s:label for="configLigaFiltro.emitir" name="emitir" value= "emitir" />
            		<s:checkbox name="configLigaFiltro.emitir" 
            		title="filtrar ligas que permiten emitir polizas" />	            	
            	</td>
            </tr>
            
            <tr>		
					<td colspan="6">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="listarLigasAgentes(false);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltrosConfigLigasAgentes();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      
    <BR/>
	<BR/>
       
	<div id="tituloListado" style="display:inline;" class="titulo" style="width: 98%;"><s:text name="midas.ligaagente.config.listado" /></div>
	<div id="indicadorConfiguracionesLigasAgentes"></div>	
	<div id="configLigasAgentesGridContainer" style="display:inline;">
		<div id="listadoConfigLigasAgentesGrid" style="width:98%;height:220px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
 	
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcelConfigLigasAgentes();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar Ligas' title='Exportar Ligas' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="mostrarAltaConfiguracion();">
					<s:text name="midas.boton.nuevo" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Nueva Configuracion' title='Nueva Configuracion' src='/MidasWeb/img/common/b_mas_agregar.jpg' style="vertical-align: middle;"/> 
				</a>
			</div>
		</td>
	</tr>
</table>	

</s:form>

<script type="text/javascript" src="<s:url value='/js/negocio/ligasdeagente/ligasAgentes.js'/>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	listarLigasAgentes(true);
});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>