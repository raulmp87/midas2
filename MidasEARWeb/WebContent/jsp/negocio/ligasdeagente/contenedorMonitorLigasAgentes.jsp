<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>

<script type="text/javascript">
var listarMonitorLigasAgentesPath = '<s:url action="listarMonitorLigasAgentes" namespace="/negocio/ligaAgente"/>';
var exportarMonitorLigasAgentesPath = '<s:url action="exportarMonitorLigasAgentes" namespace="/negocio/ligaAgente"/>';
</script>

<s:form id="monitorLigasAgentesForm" >

	<table  id="filtrosM2" width="98%">
            <tr>
                  <td class="titulo" colspan="6"><s:text name="midas.ligaagente.monitor.titulo" /></td>
            </tr>
          
          	<tr>
            	<td>
	            	<s:textfield name="monitorLigaFiltro.id" id="codigo" cssClass="cajaTextoM2 w230 jQnumeric jQrestrict" 
	            		label="%{getText('midas.ligaagente.monitor.codigo')}" disabled="false" />
            	</td>
            	
            	<td>
	            	<s:textfield name="monitorLigaFiltro.nombreAgente" id="nombreAgente" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.ligaagente.monitor.agente')}" disabled="false" />
            	</td>
            	
            	<td>
					<s:select list="negocios" headerKey="" headerValue="%{getText('midas.general.seleccione')}" 
					name="monitorLigaFiltro.idNegocio" listKey="idToNegocio" listValue="descripcionNegocio" cssClass="cajaTextoM2 w230" onchange=""
					label="%{getText('midas.ligaagente.monitor.negocio')}"/>
            	</td>
            	
            	<td colspan="3">
            		<div style="width:230px;">
	            		<div style="width:30px;display:inline;float:left;" >
					     	<s:checkbox id="incluirPolizas" name="monitorLigaFiltro.incluirPolizas" onchange=""/>
					     </div>
					     <div style="width:90px;display:inline;float:left;margin-top:7px;" >
					     	<s:text name="midas.ligaagente.monitor.incpoliza"/>
					     </div>
				     </div>
            	</td>
            	
            </tr>
            
            <tr>
            	
            	<td >
					<s:textfield name="monitorLigaFiltro.prospectoCotizacion" id="prospecto" cssClass="cajaTextoM2 w230" 
	            		label="%{getText('midas.ligaagente.monitor.prospecto')}" disabled="false" />
            	</td>
            	
            	<td  align="left"> <sj:datepicker name="monitorLigaFiltro.fechaInicio"
					id="fechaInicio" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					maxDate="today"	cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.ligaagente.monitor.fechaini')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
            	
            	<td align="left"> <sj:datepicker name="monitorLigaFiltro.fechaFin"
					id="fechaFin" buttonImage="../img/b_calendario.gif"
					changeYear="true" changeMonth="true" maxlength="10"
					maxDate="today"	cssClass="w200 cajaTextoM2" 
					label="%{getText('midas.ligaagente.monitor.fechafin')}"
					onkeypress="return soloFecha(this, event, false);"
					onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
					></sj:datepicker>
				</td>
				
				<td colspan="3">
					<div style="display:inline;float:left"><s:textfield value="%{numeroCotizaciones}" id="numeroCotizaciones" cssClass="cajaTextoM2 w100" 
	            		label="%{getText('midas.ligaagente.monitor.numcotizaciones')}"  readonly="true" /></div>
	            	<div style="display:inline;float:left"><s:textfield value="%{numeroPolizas}" id="numeroPolizas" cssClass="cajaTextoM2 w100" 
	            		label="%{getText('midas.ligaagente.monitor.numpolizas')}"  readonly="true" /></div>
				</td>
            </tr>
            
            <tr>		
					<td colspan="6">		
						<table style="padding: 0px; width: 100%; margin: 0px; border: none;">
							<tr>
								<td  class= "guardar">
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;margin-left: 2%; " id="b_buscar">
										 <a href="javascript: void(0);" onclick="listarMonitorLigasAgentes(false);" >
										 <s:text name="midas.boton.buscar" /> </a>
									</div>
									<div class="alinearBotonALaDerecha" style="display: inline; float: right;" id="b_borrar">
										<a href="javascript: void(0);" onclick="limpiarFiltrosMonitorLigasAgentes();"> 
										<s:text name="midas.boton.limpiar" /> </a>
									</div>	
								</td>							
							</tr>
						</table>				
					</td>		
			</tr>

      </table>
      
    <BR/>
	<BR/>
       
	<div id="tituloListado" style="display:inline;" class="titulo" style="width: 98%;"><s:text name="midas.ligaagente.monitor.listado" /></div>
	<div id="indicadorMonitorLigasAgentes"></div>	
	<div id="monitorLigasAgentesGridContainer" style="display:inline;">
		<div id="listadoMonitorLigasAgentesGrid" style="width:98%;height:220px;"></div>
		<div id="pagingArea"></div><div id="infoArea"></div>
	</div>
 	
<table style="padding: 0px; width: 98%; margin: 0px; border: none;">
	<tr>
		<td>
			<div class="btn_back w120" style="display: inline; float: right;"  >
				<a href="javascript: void(0);" onclick="exportarExcelMonitorLigasAgentes();">
					<s:text name="midas.boton.exportarExcel" />&nbsp;&nbsp;<img align="middle" border='0px' alt='Exportar Ligas' title='Exportar Ligas' src='/MidasWeb/img/common/b_excel.gif' style="vertical-align: middle;"/> 
				</a>
			</div>
		</td>
	</tr>
</table>	

</s:form>

<script type="text/javascript" src="<s:url value='/js/negocio/ligasdeagente/ligasAgentes.js'/>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	listarMonitorLigasAgentes(true);
});
</script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>