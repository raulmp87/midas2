<?xml version="1.0" encoding="UTF-8"?>

<%@ taglib prefix="s" uri="/struts-tags" %>

<%@  page contentType="text/xml" %>
<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enablePaging">
				<param>true</param>
				<param>20</param>
				<param>5</param>
				<param>pagingArea</param>
				<param>true</param>
				<param>infoArea</param>
			</call>     
			<call command="setPagingSkin">
				<param>bricks</param>
			</call>
		</beforeInit>
		
          <column  type="ro"  width="70"  align="center" sort="int" > <s:text name="midas.ligaagente.config.codigo" />   </column>
          <column  type="ro"  width="110"  align="center" sort="str" > <s:text name="midas.ligaagente.config.nombre" />  </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="Id Agente" />  </column>
          <column  type="ro"  width="110"  align="center" sort="str"  > <s:text name="midas.ligaagente.config.nombreagente" />  </column>
          <column  type="ro"  width="110"  align="center" sort="str"  > <s:text name="midas.ligaagente.config.negocio" />  </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.ligaagente.config.estatus" />  </column>
          <column  type="ro"  width="100"  align="center" sort="date_custom" > <s:text name="midas.ligaagente.config.fecha" />   </column>
          <column  type="ro"  width="110"  align="center" sort="str" > <s:text name="midas.ligaagente.config.usuariocreacion" />   </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.ligaagente.config.descripcion" />   </column>
          <column  type="ro"  width="100"  align="center" sort="str" > <s:text name="midas.ligaagente.config.correo" />   </column>
          <column  type="ro"  width="60"  align="center" sort="str" > <s:text name="emitir" />   </column>
          <column  type="img"   width="40" sort="na" align="center" >Acciones</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>
          <column  type="img"   width="40" sort="na" align="center" >#cspan</column>

	</head>

	<s:iterator value="configuracionesLigasAgente">
		<row id="<s:property value="#row.index"/>">
		
		    <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombre" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="idAgente" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="nombreAgente" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="descripcionNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="fechaCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="codigoUsuarioCreacion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descripcion" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="correo" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="emitir?'SI':'NO'" escapeHtml="false" escapeXml="true"/></cell>
			<cell>../img/icons/ico_verdetalle.gif^Consultar^javascript:consultarConfigLigaAgente(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
			<cell>../img/icons/ico_editar.gif^Editar^javascript:editarConfigLigaAgente(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    <cell>../img/icons/ico_verdetalle.gif^Monitor^javascript:monitorConfigLigaAgente(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    <s:if test="liga != null && liga != \"\" ">
		    	<cell>../img/icons/email.png^Enviar^javascript:enviarConfigLigaAgente(<s:property value="id" escapeHtml="false" escapeXml="true"/>)^_self</cell>
		    </s:if>
		    <s:else>
		    	<cell>../img/pixel.gif^NA^^_self</cell>
		    </s:else>
		</row>
	</s:iterator>
	
</rows>