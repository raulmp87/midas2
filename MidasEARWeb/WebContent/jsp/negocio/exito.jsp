<%@ taglib prefix="s" uri="/struts-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<div id="mensajeImg">
	<img src='/MidasWeb/img/b_ok.png'>
</div>
<div id="agregar" class="mensaje_texto">
	<s:text name="midas.negocio.negocio.message.copiarNegocioUno"/>
	<b><s:property  value="%{negocio.descripcionNegocio}"/></b><br>
     <s:text name="midas.negocio.negocio.message.copiarNegocioDos"/>
    <b><s:property  value="%{nombreNegocio}"/></b><br>
    <s:text name="midas.negocio.negocio.message.copiarNegocioTres"/><br>
     <s:text name="midas.negocio.negocio.message.copiarNegocioCuatro"/>
</div>
