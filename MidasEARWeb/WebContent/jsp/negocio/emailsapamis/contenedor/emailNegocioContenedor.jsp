<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<s:include value="/jsp/negocio/emailsapamis/contenedor/emailNegocioHeader.jsp"></s:include>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<div id="guardarEmailNegocioDiv" style="display:none"></div>

	<div id="contenidoSapAmisEmail">
		<h1 id="labelSapAmisEmailTitulo" ></h1>
		<h2 id="labelSapAmisEmailSubtitulo" ></h2>
		<div id="agregarEmailALertas">
			<div id="listadoEmail" style="position: absolute; height: 250px; width: 435px; "></div>
			<div id="listadoAlertas" style="position: absolute; height: 250px; width: 435px; right: 10px;"></div>
		</div>
		<div id="listadoEmailConfig" style="position: absolute; height: 250px; width: 1186px; "></div>
		<div id="listadoCorreosEnviados" style="position: absolute; height: 250px; width: 1186px; ">
			<div id="listadoCorreosEnviadosGrid" style="position: absolute; height: 250px; width: 1186px; "></div>
		</div>
		
		<div id="btn_configurar_correos_alertas" style="width:150px; position: absolute; right: 10px; top: 90%;" class="btn">
			<a href="javascript: void(0);" onclick="opcionConfigurarCorreosAlertas();">Configurar Correos.</a>
		</div>
		<div id="btn_agregar_correos_alertas" style="width:150px; position: absolute; right: 150px; top: 90%;" class="btn">
			<a href="javascript: void(0);" onclick="opcionAgregarCorreosAlertas();">Agregar Correos y Alertas.</a>
		</div>
		<div id="btn_ver_log" style="width:150px; position: absolute; right: 310px; top: 90%;" class="btn">
			<a href="javascript: void(0);" onclick="opcionConsultarCorreosEnviados();">Consultar Correos Enviados</a>
		</div>
	</div>
<script type="text/javascript">
	inicializarSapAmisEmail();
</script>