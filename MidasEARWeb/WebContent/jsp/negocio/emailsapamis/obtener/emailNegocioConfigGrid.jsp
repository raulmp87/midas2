<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
			<call command="enableMultiline"><param>true</param></call>
		</beforeInit>
        <afterInit>
			<call command="setColAlign">
				<param>left</param>
			</call>
			<call command="enableResizing">
				<param>false,false,false,false</param>
			</call>
		</afterInit>
		<column id="idEmailNegocioConfig" type="ro" width="0" sort="int" hidden="true">idEmailNegocioConfig</column>
		<column id="sistemas" type="ro" width="150" sort="str" >Sistema</column>
		<column id="alerta" type="ro" width="*" sort="str" >Alerta</column>
 		<column id="email" type="ro" width="*" sort="str" >Email</column>
	</head>
	<s:iterator value="emailNegocioAlertasList" status="grid" var="emailNegocioAlertas" >
		<row id="${grid.count}"> <!-- Posicion en grid -->  <!-- Id Subgrid -->
			<cell><s:property value="idEmailNegocioAlertas" escapeHtml="false" escapeXml="true"/></cell> <!-- Id de la alerta -->
			<cell><s:property value="catAlertasSapAmis.catSistemas.descCatSistemas" escapeHtml="false" escapeXml="true"/></cell> 
			<cell><s:property value="catAlertasSapAmis.descCatAlertasSapAmis" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[
				<rows>
					<encabezado>
						<column id="Correos" type="ro" width="445" sort="int">Email</column>
						<column id="Eliminar" type="img" width="25" sort="int"></column>
					</encabezado>
					<s:iterator value="emailNegocioConfigList" var="emailNegocioConfigList" status="subGrid">
						<s:if test="%{#emailNegocioAlertas.idEmailNegocioAlertas == #emailNegocioConfigList.alertasEmailNegocio.idEmailNegocioAlertas}">
							<row id="${grid.count}|@|${subGrid.count}|@|<s:property value="#emailNegocio.email.email" escapeHtml="false" escapeXml="true"/>|@|<s:property value="#idEmailNegocio" escapeHtml="false" escapeXml="true"/>">
								<cell><s:property value="#emailNegocioConfigList.emailNegocio.email.email" escapeHtml="false" escapeXml="true"/></cell>
								<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript:guardarEmailNegocioConf(<s:property value="idNegocio" escapeHtml="false" escapeXml="true"/>,<s:property value="idEmailNegocioAlertas" escapeHtml="false" escapeXml="true"/>,<s:property value="idEmailNegocioConfig" escapeHtml="false" escapeXml="true"/>,"<s:property value="catAlertasSapAmis.descCatAlertasSapAmis" escapeHtml="false" escapeXml="true"/>",1,${grid.count});^_self</cell>
							</row>						
						</s:if>
					</s:iterator>
					<row id="nueva"><cell><s:iterator value="emailNegocioList" status="row" var="emailCombo"><s:property value="idEmailNegocio" escapeHtml="false" escapeXml="true"/>|@|<s:property value="email.email" escapeHtml="false" escapeXml="true"/>|_|</s:iterator></cell><cell>/MidasWeb/img/icons/ico_agregar.gif^Agregar^javascript:guardarEmailNegocioConf(<s:property value="idNegocio" escapeHtml="false" escapeXml="true"/>,<s:property value="idEmailNegocioAlertas" escapeHtml="false" escapeXml="true"/>,0,"<s:property value="catAlertasSapAmis.descCatAlertasSapAmis" escapeHtml="false" escapeXml="true"/>",0,${grid.count});^_self</cell></row>
				</rows>
			]]></cell>
		</row>
	</s:iterator>
</rows>