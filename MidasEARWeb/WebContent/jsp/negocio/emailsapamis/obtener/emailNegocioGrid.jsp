<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		<column id="emailNegocio.idEmail" type="ro" width="0" sort="int" hidden="true">idEmail</column>
		<column id="emailNegocio.email" type="ed" width="300" sort="str" >Email</column>
		<column id="emailNegocio.idEmailNegocioTipoUsuarioList" type="co" width="*" sort="str" >Puesto
			<s:iterator value="emailNegocioTipoUsuarioList" status="row">
				<option 
					value="<s:property value="idEmailNegocioTipoUsuario" escapeHtml="false" escapeXml="true"/>"
					text="<s:property value="emailNegocioTipoUsuario" escapeHtml="false" escapeXml="true"/>"
					>
						<s:property value="emailNegocioTipoUsuario" escapeHtml="false" escapeXml="true"/>
						
				</option>
			</s:iterator>
		</column>
		<column id="Editar" type="img" width="25"></column>
		<column id="Eliminar" type="img" width="25"></column>
	</head>
	<s:iterator value="emailNegocioList" status="row">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="idEmailNegocio" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="email.email" escapeHtml="false" escapeXml="true"/></cell>
			<cell xmlcontent="EmailNegocioTipoUsuario.idEmailNegocioTipoUsuario" editable="0"><s:property value="emailNegocioTipoUsuario.idEmailNegocioTipoUsuario" escapeHtml="false" escapeXml="true"/></cell>
			<cell>/MidasWeb/img/icons/ico_guardar.gif^Guardar^javascript:guardarEmailNegocio("<s:property value="idEmail" escapeHtml="false" escapeXml="true"/>", "", "", 0);^_self</cell>
			<cell>/MidasWeb/img/icons/ico_eliminar.gif^Eliminar^javascript:guardarEmailNegocio("<s:property value="idEmail" escapeHtml="false" escapeXml="true"/>", "", "", 1);^_self</cell>
		</row>
	</s:iterator>
	<row id="nueva">
		<cell>0</cell>
		<cell></cell>
		<cell xmlcontent="EmailNegocioTipoUsuario.idEmailNegocioTipoUsuario" editable="0"> </cell>
		<cell colspan="2">/MidasWeb/img/icons/ico_guardar.gif^Guardar^javascript:guardarEmailNegocio("", "", "", 3);^_self</cell>
	</row>
</rows>