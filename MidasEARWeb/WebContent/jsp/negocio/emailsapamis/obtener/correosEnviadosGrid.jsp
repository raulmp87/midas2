<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
		</beforeInit>
		<column id="sistema" type="ro" width="200" sort="int" >Sistema</column>
		<column id="alerta" type="ro" width="300" sort="str" >Alerta</column>
		<column id="email" type="ro" width="*" sort="str" >Email</column>
		<column id="fecha" type="ro" width="*" sort="str" >Fecha</column>
	</head>
	<s:iterator value="emailLogList" var="emailLogList" status="row">
		<row id="${row.count}">
			<cell><![CDATA[<s:property value="alerta.sapAmisSistemas.sapAmisSistemas" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="alerta.sapAmisAlertas" escapeHtml="false" escapeXml="true"/>]]></cell>
			<cell><![CDATA[<s:property value="email.email" escapeHtml="false" escapeXml="true"/>]]></cell>
		  	<cell><![CDATA[<s:property value="fecha" escapeHtml="false" escapeXml="true"/>]]></cell>
	 	</row>
	</s:iterator>
</rows>