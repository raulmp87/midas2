<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>false</param></call>
			<call command="enableMultiline"><param>true</param></call>
		</beforeInit>
		<column id="idAlerta" type="ro" width="0" sort="int" hidden="true">idAlerta</column>
		<column id="sistemas" type="ro" width="100" sort="str" >Sistema</column>
		<column id="alerta" type="ro" width="*" sort="str" >Alerta</column>
		<column id="agregarquitar" type="img" width="25"></column>
	</head>
	<s:iterator value="catAlertasSapAmisList" status="row" var="catAlertasSapAmis">
		<row id="<s:property value="#row.index"/>">
			<cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="catSistemas.descCatSistemas" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="descCatAlertasSapAmis" escapeHtml="false" escapeXml="true"/></cell>
			<cell><![CDATA[<s:set var = "breakLoop" value = "%{true}" /><s:iterator value="emailNegocioAlertasList" status="row" var="emailNegocioAlertas"><s:if test="%{#catAlertasSapAmis.id == #emailNegocioAlertas.catAlertasSapAmis.id}"><s:set var = "breakLoop" value = "%{false}" />/MidasWeb/img/icons/check_in.png^Desactivar^javascript:guardarEmailNegocioAlertas(<s:property value="idEmailNegocioAlertas" escapeHtml="false" escapeXml="true"/>, 1);^_self</s:if></s:iterator><s:if test="#breakLoop">/MidasWeb/img/icons/check_out.png^Activar^javascript:guardarEmailNegocioAlertas(0, 0);^_self</s:if>]]></cell>
		</row>
	</s:iterator>
</rows>