<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>

	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
			<call command="attachHeader"><param>,,#text_search,#text_search</param></call>
		</beforeInit>
		<column id="usuario.id.recuotificacionId"  type="ro" align="center" width="0"	hidden="true">recuotificacion Id</column>
		<column id="usuario.id.usuarioId" 			 type="ro" align="center" width="0"	hidden="true">usuario id</column>
		<column id="usuario.cveUsuario"	 type="ro" align="center" width="*"	sort="str"	>Clave Usuario</column>
	    <column id="usuario.nombreUsuario"	 type="ro" align="center" width="230"	sort="str"	>Nombre Usuario</column>
	</head>
	
	<% int a=0;%>
	<s:iterator value="usuariosAsociados">
		<% a+=1; %>
		<row id="<%=a%>">
			<cell><s:property value="id.recuotificacionId" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="id.usuarioId" escapeHtml="false" escapeXml="true"/></cell>
		    <cell><s:property value="cveUsuario" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuario" escapeHtml="false" escapeXml="true"/></cell>			
		</row>
	</s:iterator>
</rows>