<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>


 
<rows>
	<head>
	
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		
        <afterInit>
        </afterInit>
	    <column hidden="true" id="id"  type="ro" width="80" sort="str" ><s:text name="id"/></column>
        <column id="numProgPago"  type="ro" width="150" sort="str" ><s:text name="Numero de Programa"/></column>
        <column id="pctDistribucionPrima" type="ed" width="170" sort="int" format="100%"><s:text name="Distribucion Prima (%)"/></column>     
        <column id="pctDistribucionDerechos" type="ed" width="170" sort="int" format="100%"><s:text name="Distribucion Derechos (%)"/></column>      
 	    <column id="eliminar" type="img" width="30"   align="center"></column>
        <column id="detalle"  type="img" width="*"   valign="center"></column>
	</head>  		
	
    <s:iterator value="programas" status="stats">
       <row id="<s:property value="id"/>">
               <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="numProgPago" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="pctDistribucionPrima" escapeHtml="false" escapeXml="true"/></cell>	
	            <cell><s:property value="pcteDerechosTotales" escapeHtml="false" escapeXml="true"/></cell>						         
               <cell>/MidasWeb/img/delete16.gif^Eliminar^javascript:eliminarProgramaPago(<s:property value="id"/>)^_self</cell>                         
               <cell>/MidasWeb/img/details.gif^Detalle^javascript:verDetalleRecibos(<s:property value="id"/>)^_self</cell>  
            </row> 
         </s:iterator>
    </rows>
