<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="m" uri="/midas-tags" %>
<%@  page contentType="text/xml" %>


 
<rows>
	<head>
	
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>		
		
        <afterInit>
        </afterInit>
	    <column hidden="true" id="id"  type="ro" width="0" sort="str" ><s:text name="id"/></column>
        <column id="numExhibicion"  type="ro" width="90" sort="str" ><s:text name="Numero de Exhibicion"/></column>
        <column id="diaDuracion" type="ed" width="130" sort="int" format="100%"><s:text name="Dias de Duracion"/></column>
        <column id="primaNeta" type="ed" width="120" sort="int" format="100%"><s:text name="Prima Neta (%)"/></column>
        <column id="derechos" type="ed" width="120" sort="int" format="100%"><s:text name="Derechos (%) x total programas"/></column>      
 	    <column id="eliminar" type="img" width="*"   align="center"></column>        
	</head>  		
	
    <s:iterator value="recibos" status="stats">
       <row id="<s:property value="id"/>">
               <cell><s:property value="id" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="numExhibicion" escapeHtml="false" escapeXml="true"/></cell>
	           <cell><s:property value="diasDuracion" escapeHtml="false" escapeXml="true"/></cell>	
	           <cell><s:property value="pctDistPrimaNeta" escapeHtml="false" escapeXml="true"/></cell>	
	           <cell><s:property value="pctDistDerechos" escapeHtml="false" escapeXml="true"/></cell>				         
               <cell>/MidasWeb/img/delete16.gif^Eliminar^javascript:eliminarRecibo(<s:property value="id"/>)^_self</cell>                                          
            </row> 
         </s:iterator>
    </rows>
