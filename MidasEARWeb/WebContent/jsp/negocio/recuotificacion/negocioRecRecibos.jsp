<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<%-- <link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css"> --%>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<style type="text/css">

	body {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		background-color: #FFFFFF;
		background-position: 0px 85px;
		background-repeat: repeat;
		font-size: 7pt;
		z-index: 0;
		overflow-y: auto /*hidden*/;
		overflow-x: auto;
		margin: 0px;
		padding: 0px;
	}

	.divContenedor {
		float:left; 
		clear: left; 
		padding: 10px 10px; 
		width: 95%;		
	}	
	
</style>
<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src='<s:url value="/js/midas2/jQuery/jquery-1.4.3.js"/>'></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_filter.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_srnd.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_sub_row.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_group.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_excell_link.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/dhtmlxgrid_nxml.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>


<script type="text/javascript" src="<s:url value='/js/midas2/negocio/recuotificacion/negocioRecuotificacion.js'/>"></script>
<script type="text/javascript">
	var URL_OBTENER_RECIBOS = '<s:url action="obtenerRecibos" namespace="/negocio/recuotificacion"/>';
	var URL_MODIFICAR_RECIBO = '<s:url action="modificarRecibo" namespace="/negocio/recuotificacion"/>';
	var URL_SELECCIONAR_VERSION = '<s:url action="mostrarVersion" namespace="/negocio/recuotificacion"/>';
	var URL_OBTENER_SALDO = '<s:url action="obtenerSaldo" namespace="/negocio/recuotificacion"/>';
	var URL_NUEVO_RECIBO = '<s:url action="generarRecibo" namespace="/negocio/recuotificacion"/>';
	var URL_ELIMINA_RECIBO = '<s:url action="eliminarRecibo" namespace="/negocio/recuotificacion"/>';
	var URL_ACTIVAR_VERSION	= '<s:url action="activarVersion" namespace="/negocio/recuotificacion"/>';
	var URL_NUEVA_VERSION = '<s:url action="nuevaVersion" namespace="/negocio/recuotificacion"/>';
	var URL_OBTENER_STATUS = '<s:url action="obtenerStatus" namespace="/negocio/recuotificacion"/>';
</script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<div id="divM">
<s:form id="negocioRecuotificacionForm" name="automatica">		
	<s:hidden name="programa.automatica.id" id="versionId"/>
	<s:hidden name="programa.id" id="progPagoId"/>	
	<div class="divContenedor"  >		
		<div style="float: left; padding-right: 5px;">
			<s:text name="midas.negocio.recuotificacion.label.version"/>
			<s:label name="programa.automatica.version" />
		</div>
		<div style="float:left; margin-left: 25%; text-transform: uppercase;">
			<s:text name="midas.negocio.recuotificacion.label.usuariocreacion"/>
			<s:label name="programa.automatica.codigoUsuarioCreacion"/>
		</div>			
		<div style="float: right; overflow: auto; padding-right: 5px;">
			<s:text name="midas.negocio.recuotificacion.label.fechacreacion"/>
			<s:label name="programa.automatica.fechaCreacion"/>
		</div>			
	</div>	
	<div class="divContenedor"  >
		<div style="float: left; padding-right: 5px;">
			<s:text name="midas.negocio.recuotificacion.label.tipovigencia"/>
			<s:label name="programa.automatica.tipoVigencia"/>
			<s:hidden name="programa.automatica.tipoVigencia" id="tipoVigencia"/>
			
		</div>		
		<div style="float: right; overflow: auto; padding-right: 5px;">
			<s:text name="midas.negocio.recuotificacion.label.status"/>
			<s:label id="status" name="programa.automatica.status"/>
		</div>		
	</div>	
	<div class="divContenedor">
		<div id="comandos" style="float: left; padding-right: 5px; overflow: auto; text-align: left; width: 100%;">				
			<div class="btn_back w200 btnActionForAll" style="display: inline; float: right;">
				<a class="" onclick="cargarVersion(<s:property value='programa.automatica.id'/>);" alt="Asociar todas" href="javascript: void(0);">Regresar</a>
			</div>		
			<div class="btn_back w200 btnActionForAll" style="display: inline; float: right;">
				<a class="" onclick="generarNuevoRecibo(<s:property value='programa.id'/>);" alt="Asociar todas" href="javascript: void(0);">Nuevo Recibo</a>
			</div>			
		</div>			
	</div>	
	<div class="divContenedor" style="float:right;" align="center">
		<div id="recibosListadoGrid" style="width:550px;height:200px;" ></div>
	</div>	
</s:form>
</div>

<script type="text/javascript">
jQuery( document ).ready(function($) {
   getGridRecibos(<s:property value='programa.id'/>);
});
</script>