<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>



<script type="text/javascript">
	var URL_ACTIVAR_AUTOMATICA = '<s:url action="activarAutomatica" namespace="/negocio/recuotificacion"/>';
	var URL_ACTIVAR_MANUAL = '<s:url action="activarManual" namespace="/negocio/recuotificacion"/>'; 
	var URL_ACTIVAR_PRIMA_TOTAL = '<s:url action="activarPrimaTotal" namespace="/negocio/recuotificacion"/>';  
	var URL_MODIFICAR_PRIMA_TOTAL =  '<s:url action="modificarPrimaTotal" namespace="/negocio/recuotificacion"/>'; 	
	var URL_LISTAR_DISPONIBLES = '<s:url action="obtenerUsuariosDisponibles" namespace="/negocio/recuotificacion"/>'; 	
	var URL_LISTAR_ASOCIADIOS = '<s:url action="obtenerUsuariosAsociados" namespace="/negocio/recuotificacion"/>';
	var URL_RELACIONAR_USUARIO = '<s:url action="relacionarUsuario" namespace="/negocio/recuotificacion"/>';
	var URL_ASOCIAR_TODOS = '<s:url action="asociarTodos" namespace="/negocio/recuotificacion"/>';
	var URL_DESASOCIAR_TODOS = '<s:url action="desasociarTodos" namespace="/negocio/recuotificacion"/>'; 	
	var URL_CARGAR_VERSION = '<s:url action="mostrarNegRecVersion" namespace="/negocio/recuotificacion"/>';
</script>

<script type="text/javascript" src="<s:url value='/js/midas2/negocio/recuotificacion/negocioRecuotificacion.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/jquery.formatCurrency-1.4.0.min.js'/>"></script>

<style type="text/css">

	.divContenedor {
		float:left; 
		clear: left; 
		padding: 10px 10px; 
		width: 95%;		
	}
	
</style>

<br/>
<div id="">

	<div class="titulo"><s:text name='midas.negocio.recuotificacion.title'/></div>
	<div id="divM">
		<s:form id="negocioRecuotificacionForm" name="recuotificacion">
			<s:hidden name="recuotificacion.id" id="recuotificacionId"/>
			<s:hidden name="recuotificacion.negocio.idToNegocio" />
			<div class="subtitulo  divContenedor"  style="text-align: left;">
				<s:text name="midas.negocio.recuotificacion.label.habilitar.automatica"/>
			</div>
			<div class="divContenedor" >
				<div style="padding-top:4px; padding-left:10px; display: inline; float: left; vertical-align: middle; position: relative; margin-top: 4px; margin-right: 10px;">
					<s:hidden name="recuotificacion.activarAutomatica" id="inputActivarAutomatica" />
					<img alt=Activar Recuotificación Automática" title="Activar Recuotificación Automática" width="40px;" height="15px;" 
					src="<s:if test='%{recuotificacion.activarAutomatica == false}'>../img/maps/off.png</s:if><s:else>../img/maps/on.png</s:else>"
					id="activacionAutomatica" onclick="activarAutomatica(this.id);">
					<s:text name="midas.negocio.recuotificacion.label.activarautomatica"/>		
				</div>
			</div>
			<div class="divContenedor" >
				<div class="btn_back w300" style="float:left;" >
					<a href="javascript: abrirContenedorVersion();" class="icon_guardar" onclick="">
						<s:text name="midas.negocio.recuotificacion.btn.definir"/>
					</a>
				</div>
				<div style="float: right; overflow: auto; padding-right: 5px;">
					<s:text name="midas.negocio.recuotificacion.label.ultimacreacion"/>
					<s:label name="recuotificacion.ultimaVersionCreada" />
				</div>	
			</div>					
			<div class="subtitulo divContenedor"  style="text-align: left;">
				<s:text name="midas.negocio.recuotificacion.label.habilitar.manual"/>
			</div>
			<div class="divContenedor" >
				<s:hidden name="recuotificacion.activarManual" id="inputActivarManual"/>			
				<div style="padding-top:4px; padding-left:10px; display: inline; float: left; vertical-align: middle; position: relative; margin-top: 4px; margin-right: 10px;">
					<img alt=Activar Recuotificación Manual" title="Activar Recuotificación Manual" width="40px;" height="15px;" 
					src="<s:if test='%{recuotificacion.activarManual == false}'>../img/maps/off.png</s:if><s:else>../img/maps/on.png</s:else>"  
					id="activacionManual" onclick="activarManual(this.id)";">
					<s:text name="midas.negocio.recuotificacion.label.activarmanual"/>		
				</div>
			</div>
			<div class="divContenedor" style="width:97%">
				<div id="negocioRecUsuariosDisponiblesGrid" class="dataGridConfigurationClass" style="float:left; width:500px; height:200px;"></div>
				<div style="float:left; margin-left: 50px;">
					<div class="btn_back w40 btnActionForAll">
						<a class="" onclick="desAsociarTodosNegRecUsuarios();" alt="Desligar todas" href="javascript: void(0);"> << </a>
					</div>
					<div class="btn_back w40 btnActionForAll">
						<a class="" onclick="asociarTodosNegRecUsuarios();" alt="Asociar todas" href="javascript: void(0);"> >> </a>
					</div>				
				</div>
				<div id="negocioRecUsuariosPermisoManualGrid" class="dataGridConfigurationClass" style="float: right; width:500px; height:200px;"></div>
			</div>
			<div class="divContenedor" >
				<div style="float:left;">	
					<s:hidden name="recuotificacion.activarModificacionPrimaTotal" id="inputActivarModificacionPrimaTotal"/>				
					<img alt="Activación modificación prima total" title="Activación modificación prima total" width="40px;" height="15px;" 
					src="<s:if test='%{recuotificacion.activarModificacionPrimaTotal == false}'>../img/maps/off.png</s:if><s:else>../img/maps/on.png</s:else>" 
					id="activacionModificacionPrimaTotal" onclick="activarPrimaTotal(this.id)";">
					<s:text name="midas.negocio.recuotificacion.label.primatotal"/>
				</div>				
				<div >
				<s:textfield onblur="modificarPrimaTotal(this.value)" name="recuotificacion.montoPrimaTotal" id="montoPrimaTotal" 
				cssClass="txtfield  requerido formatCurrency"/>
				</div>
			</div>
		</s:form>
	</div>
</div>
<script type="text/javascript">
	initGridsNegocioRecuotificacionUsuarios();
	initCurrencyFormatOnTxtInput();
</script>
