<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="${pageContext.request.contextPath}/struts/utils.js" type="text/javascript"></script>

<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<%-- <script src="<s:url value='/js/midas2/componente/componente.js'/>"></script> --%>
<s:set id="estatusCreado" value="@mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_CREADO"/>
<s:set id="estatusActivo" value="@mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_ACTIVO"/>
<s:set id="estatusInactivo" value="@mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_INACTIVO"/> 
<s:set id="estatusBorrado" value="@mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_BORRADO"/>


<s:set id="claveTextoBotonBorrar" value="%{getText('midas.boton.borrar')}" />
<s:set id="accionJsBotonBorrar" value="'cambiaEstatusTarifa('+ @mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_BORRADO +');'" />

<s:if test="tarifaVersion.claveEstatus == @mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_ACTIVO">
	<s:set id="claveTextoBotonActivo" value="%{getText('midas.accion.desactivar')}" />
	<s:set id="accionJsBotonActivo" value="'cambiaEstatusTarifa('+ @mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_INACTIVO +');'" />
</s:if>
<s:else>
	<s:set id="claveTextoBotonActivo" value="%{getText('midas.accion.activar')}" />
	<s:set id="accionJsBotonActivo" value="'cambiaEstatusTarifa('+ @mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_ACTIVO +');'" />
</s:else>

<s:set id="claveTextoBotonNuevaVersion" value="%{getText('midas.accion.nuevaversion')}" />
<s:set id="accionJsBotonNuevaVersion" value="'nuevaVersionTarifa();'" />


<div id="detalleTarifaVersion" >
	<center>
		<s:form action="cargarTarifaVersion" id="tarifaVersionForm">
			<s:hidden name="tarifaVersion.id.idRiesgo"/>
			<s:hidden name="tarifaVersion.id.idConcepto"/>
			<s:hidden name="tarifaVersion.tarifaConcepto.claveNegocio"/>
			<s:hidden name = "valorMenu"/>
			<s:hidden name = "estatusBorrado"/>
			<s:hidden name = "estatus"/>
			<table id="frame" border="1" width="100%">
				<tr><td>
			<table id="desplegarDetalle" border="0">
				
				<tr>
					<th>
						<s:text name="configuracion.tarifa.version" />:
					</th>
					<td>
						<s:select id="tarifaVersion.id.version" name="tarifaVersion.id.version" value="tarifaVersion.id.version" 
							list="versiones"
							onchange="iniciaTarifas();"/>
						
					</td>
					<th>
						<s:text name="configuracion.tarifa.claveEstatus" />:
					</th>
					<td>
						<s:property value="tarifaVersion.descripcionEstatus"  />
					</td>
					<th>
						<s:text name="configuracion.tarifa.fechainiciovigencia" />:
					</th>
					<td>
						<s:property value="tarifaVersion.fechaInicioVigencia"  />
					</td>
					
					<s:if test='tarifaVersion.tarifaConcepto.claveNegocio == \"A\"'>
						<th>
							<s:text name="configuracion.tarifa.moneda" />:
						</th>
						<td>
							<s:select id="tarifaVersion.id.idMoneda" name="tarifaVersion.id.idMoneda" value="tarifaVersion.id.idMoneda" 
								list="monedas" 
								onchange="iniciaTarifas();"/>
						</td>
					</s:if>
					<s:else>
						<s:hidden name = "tarifaVersion.id.idMoneda"/>
					</s:else>
				</tr> 
				<tr>
					<s:if test='tarifaVersion.tarifaConcepto.claveNegocio == \"A\"'>
						<td colspan = "2">
						</td>
					</s:if>
					<td colspan = "3">
					</td>
					<td>
						<s:if test="tarifaVersion.claveEstatus == @mx.com.afirme.midas2.domain.tarifa.TarifaVersion@ESTATUS_CREADO">
							<s:submit onclick="%{#accionJsBotonBorrar} return false;" value="%{#claveTextoBotonBorrar}" />
						</s:if>
					</td>
					<td>
							<s:submit onclick="%{#accionJsBotonActivo} return false;" value="%{#claveTextoBotonActivo}" />
					</td>
					<td>
							<s:submit onclick="%{#accionJsBotonNuevaVersion} return false;" value="%{#claveTextoBotonNuevaVersion}" />
					</td>
					
				
				</tr>
				
			</table>
				</td></tr>
			</table>
					
		</s:form>
	</center>
</div>		