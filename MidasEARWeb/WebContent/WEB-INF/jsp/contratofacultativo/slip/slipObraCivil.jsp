<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
					
		<script type="text/javascript">
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
  </head>
  

<body bgcolor="white">

<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipObraCivil">
<midas:oculto propiedadFormulario="idToSlip"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
  <tr>
    <td><table width="100%" border="0" cellspacing="5" cellpadding="0" id="desplegar">
      <tr>
        <td align="center"><midas:mensaje clave="contratofacultativo.slip.obraCivil.datosSlip"/></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="8" cellpadding="0" id="desplegar">
      <tr>
        <td width="10%" align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.numeroInciso"/></td>
        <td width="25%" ><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="20" soloLectura="true" deshabilitado="true"  onkeypress="return soloNumeros(this, event, false)"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.direccionInciso"/></td>
        <td colspan="4"><midas:texto id="direccionInciso" propiedadFormulario="direccionInciso" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.dumaAsegurada"/></td>
        <td><midas:texto id="dumaAsegurada" propiedadFormulario="dumaAsegurada" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.tipoObraCaracteristicasObra"/></td>
        <td colspan="4"><midas:texto id="tipoObraCaracteristicasObra" propiedadFormulario="tipoObraCaracteristicasObra" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.titulodelContrato"/></td>
        <td colspan="2" ><midas:texto id="titulodelContrato" propiedadFormulario="titulodelContrato" longitud="20"/></td>
        <td align="right">
        <etiquetas:etiquetaError property="metodosymaterialesdeconstruccion" requerido="si"
					key="contratofacultativo.slip.obraCivil.metodosymaterialesdeconstruccion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
        <td><midas:texto id="metodosymaterialesdeconstruccion" propiedadFormulario="metodosymaterialesdeconstruccion" longitud="20"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.ingenieroConsultor"/></td>
        <td colspan="2"><midas:texto id="ingenieroConsultor" propiedadFormulario="ingenieroConsultor" longitud="20"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.cumpleRegulacionesSobreEstructurasResistentesaTerremotos"/></td>
        <td>
        <midas:combo propiedad="cumpleRegulacionesSobreEstructurasResistentesaTerremotos">
			<midas:opcionCombo valor="1">SI</midas:opcionCombo>
			<midas:opcionCombo valor="0">NO</midas:opcionCombo>
		</midas:combo>
        </td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.nombreContratista"/></td>
        <td colspan="2"><midas:texto id="nombreContratista" propiedadFormulario="nombreContratista" longitud="20"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.tienePolizaSeparadadeRC"/></td>
        <td>
        <midas:combo propiedad="tienePolizaSeparadadeRC">
			<midas:opcionCombo valor="1">SI</midas:opcionCombo>
			<midas:opcionCombo valor="0">NO</midas:opcionCombo>
		</midas:combo>
        </td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.direccionContratista"/></td>
        <td colspan="2"><midas:texto id="direccionContratista" propiedadFormulario="direccionContratista" longitud="20"/></td>
        <td align="right">
        <etiquetas:etiquetaError property="existenciaDeEstructurasAdyacentes" requerido="si"
					key="contratofacultativo.slip.obraCivil.existenciaDeEstructurasAdyacentes" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
        <td><midas:texto id="existenciaDeEstructurasAdyacentes" propiedadFormulario="existenciaDeEstructurasAdyacentes" longitud="20"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.nombreSubcontratista"/></td>
        <td colspan="2"><midas:texto id="nombreSubcontratista" propiedadFormulario="nombreSubcontratista" longitud="20"/></td>
        <td align="right">
        <etiquetas:etiquetaError property="fechaTerminacionPeriodoMantenimiento" requerido="si"
					key="contratofacultativo.slip.obraCivil.fechaTerminacionPeriodoMantenimiento" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
        <td>
        <midas:texto soloLectura="true" propiedadFormulario="fechaTerminacionPeriodoMantenimiento" id="fecha"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
		 					</td>
		 <td><a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
		  	   <image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
         </td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.direccionSubcontratista"/></td>
        <td colspan="2"><midas:texto id="direccionSubcontratista" propiedadFormulario="direccionSubcontratista" longitud="20"/></td>
        <td align="right">
        <etiquetas:etiquetaError property="edificiosyEstructurasdeTercerosquePuedanSerAfectados" requerido="si"
					key="contratofacultativo.slip.obraCivil.edificiosyEstructurasdeTercerosquePuedanSerAfectados" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
         <td><midas:texto id="edificiosyEstructurasdeTercerosquePuedanSerAfectados" propiedadFormulario="edificiosyEstructurasdeTercerosquePuedanSerAfectados" longitud="20"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.experienciadelcontratista"/></td>
        <td colspan="2"><midas:texto id="experienciadelcontratista" propiedadFormulario="experienciadelcontratista" longitud="20"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.rioLagoMarEtcMasCercano"/></td>
        <td><midas:texto id="rioLagoMarEtcMasCercano" propiedadFormulario="rioLagoMarEtcMasCercano" longitud="20"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.trabajosSubcontratista"/></td>
        <td colspan="2"><midas:texto id="trabajosSubcontratista" propiedadFormulario="trabajosSubcontratista" longitud="20"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.sumasAseguradasSublimitesAdyacentes"/></td>
        <td><midas:texto id="sumasAseguradasSublimitesAdyacentes" propiedadFormulario="sumasAseguradasSublimitesAdyacentes" longitud="20" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.condicionesMetereologicas"/></td>
        <td colspan="2"><midas:texto id="condicionesMetereologicas" propiedadFormulario="condicionesMetereologicas" longitud="20"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.anadirClausulaConsiderandoTercerosBienesPersonal"/></td>
        <td><midas:texto id="anadirClausulaConsiderandoTercerosBienesPersonal" propiedadFormulario="anadirClausulaConsiderandoTercerosBienesPersonal" longitud="20"/></td>
        </tr>
      <tr>
      <td>
       <etiquetas:etiquetaError property="trabajosPorContrato" requerido="si"
					key="contratofacultativo.slip.obraCivil.trabajosPorContrato" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
         <td colspan="2"><midas:texto id="trabajosPorContrato" propiedadFormulario="trabajosPorContrato" longitud="20"/></td>
          <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.valorPorContrato"/></td>
        <td colspan="2"><midas:texto id="valorPorContrato" propiedadFormulario="valorPorContrato" longitud="20" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.maquinariaEquiposFijosMoviles"/></td>
        <td colspan="2"><midas:texto id="maquinariaEquiposFijosMoviles" propiedadFormulario="maquinariaEquiposFijosMoviles" longitud="20"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.obraCivil.beneficiarioPreferente"/></td>
        <td colspan="2"><midas:texto id="beneficiarioPreferente" propiedadFormulario="beneficiarioPreferente" longitud="20"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td colspan="5" class="titulo"><midas:mensaje clave="contratofacultativo.slip.obraCivil.coberturas"/></td>
        </tr>
      <tr>

        </tr>
        <tr>
	    <td colspan="5">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
	  </tr>      
      <tr>
        <td>&nbsp;</td>
        <td align="right">
           <div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript:  guardarSlipObraCivil(document.slipObraCivilForm);">Guardar</a>
				</div>
			</div>
        </td>

        
		    <td align="left">
		    <div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipObraCivilForm"/>,0,9);">Cancelar</a>
				</div>
			</div>
		    </td>        
        
        
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      
    </table></td>
  </tr>
</table>
</midas:formulario>
</body>
</html>