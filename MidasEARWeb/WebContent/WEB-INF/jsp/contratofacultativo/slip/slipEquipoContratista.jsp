<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
					
		<script type="text/javascript">
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
  </head>
  

<body bgcolor="white">

<midas:formulario accion="/contratofacultativo/slip/guardarSlipEquipoContratista">
<table width="100%" border="0" cellspacing="10" cellpadding="0" bgcolor="White">
  <tr>
    <td><table width="100%" border="0" cellspacing="10" cellpadding="0" id="desplegar">
      <tr>
        <td align="center"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.datosdelSlip"/></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="10" cellpadding="0" id="desplegar">
  <tr>
    <td width="10%" align="right"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.cotizacion"/></td>
    <td width="20%"><midas:texto id="cotizacion" propiedadFormulario="cotizacion" longitud="10" soloLectura="true" deshabilitado="true"/></td>
    <td width="70%">&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.numeroInciso"/></td>
    <td><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="10" soloLectura="true" onkeypress="return soloNumeros(this, event, false)" deshabilitado="true"/></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.numeroSubinciso"/></td>
    <td><midas:texto id="numeroSubinciso" propiedadFormulario="numeroSubinciso" longitud="10" soloLectura="true" onkeypress="return soloNumeros(this, event, false)" deshabilitado="true"/></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="1" align="right"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.direccionInciso"/></td>
    <td colspan="2"><midas:texto id="direccionInciso" propiedadFormulario="direccionInciso" longitud="50" soloLectura="true" deshabilitado="true" /></td>
 </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.subincisoMayorValor"/></td>
    <td colspan="2"><midas:texto id="subincisoMayorValor" propiedadFormulario="subincisoMayorValor" longitud="50" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
  </tr>


   <tr>
    <td  colspan="2" width="100%" >
           <div id="resultados">
				<midas:tabla idTabla="documentosContratistas"
					claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexoEquipoContratista"
					claseCss="tablaConResultados" nombreLista="anexosContratista"
					urlAccion="/solicitud/listarDocumentos.do">
					<midas:columna propiedad="archivo"/>
					<midas:columna propiedad="acciones"/>
				</midas:tabla>
			</div></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
  <td colspan="2" align="right">
  <div class="alinearBotonALaDerecha"> 
     <div id="b_agregar">
					     <a href="javascript: void(0);"
					    	onclick="javascript:  mostrarAdjuntarArchivoSlipWindow(<midas:escribe propiedad="idToSlip" nombre="slipEquipoContratistaForm"/>)">Adjuntar Archivos
					     </a>
	 </div>
	 </div>
	 </td>
  </tr>



  <tr>
    <td colspan="3" class="titulo"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.coberturaInciso"/></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	    <td colspan="3">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturasInciso"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />						
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
  </tr>   
  
  <tr>
    <td colspan="3" class="titulo"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.bienesAsegurados"/></td>
    </tr>
  <tr>
	    <td colspan="3">
	     <div id="resultados">
					<midas:tabla idTabla="sumaAseguradaBien"					 
						claseCss="tablaConResultados" nombreLista="sumaAseguradaBien"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="SubInciso" propiedad="descripcion" />
						<midas:columna titulo="Suma Asegurada Bien"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
  </tr>   
  
  <tr>
    <td class="titulo"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.coberturaBienes"/></td>
    </tr>
  <tr>
	    <td colspan="3">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />						
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
  </tr>      
  <tr>
    <td class="guardar">
        	<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: guardarSlipEquipoContratista(document.slipEquipoContratistaFor);">Guardar</a>
				</div>
			</div>
        </td>
		<td align="left">
		    <div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipEquipoContratistaForm"/>,0,9);">Cancelar</a>
				</div>
			</div>
		</td>             
        <td>&nbsp;</td>
  </tr>
</table>
	</td>
  </tr>
</table>
</midas:formulario>
</body>
</html>