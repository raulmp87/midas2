<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
					
		<script type="text/javascript">
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
  </head>
  

<body bgcolor="white">


<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipIncendio">
<table width="100%" border="0" cellspacing="10" cellpadding="0" bgcolor="white">
<midas:oculto propiedadFormulario="idToSlip"/>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
        <td align="center"><midas:mensaje clave="contratofacultativo.slip.incendio.datosdelSlip"/></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
        <td width="15%" align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.numeroInciso"/></td>
        <td width="13%"><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="20"  deshabilitado="true"  soloLectura="true"  onkeypress="return soloNumeros(this, event, false);"/></td>
        <td width="72%">&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.sumaAseguradaTotal"/></td>
        <td colspan="2"><midas:texto id="sumaAseguradaTotal" propiedadFormulario="sumaAseguradaTotal" longitud="40" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.girodelAsegurado"/></td>
        <td colspan="2"><midas:texto id="girodelAsegurado" propiedadFormulario="girodelAsegurado" longitud="200"  soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.zonaTerremoto"/></td>
        <td colspan="2"><midas:texto id="zonaTerremoto" propiedadFormulario="zonaTerremoto" longitud="100"  soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.direccionInciso"/></td>
        <td colspan="2"><midas:texto id="direccionInciso" propiedadFormulario="direccionInciso" longitud="100" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.tipoConstruccion"/></td>
        <td colspan="2"><midas:texto id="tipoConstruccion" propiedadFormulario="tipoConstruccion" longitud="90" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.sumaAseguradaInciso"/></td>
        <td colspan="2"><midas:texto id="sumaAseguradaInciso" propiedadFormulario="sumaAseguradaInciso" longitud="80" soloLectura="true" deshabilitado="true"  onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

        
	  <tr>
	    <td colspan="3">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura"  />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
	  </tr>        
        
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.incendio.proteccionContraIncendio"/></td>
        <td colspan="2"><midas:texto id="proteccionContraIncendio" propiedadFormulario="proteccionContraIncendio" longitud="50"/></td>
      </tr>
      
	  <tr>
		    <td align="right">
	        <div class="alinearBotonALaDerecha">
					<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: guardarSlipIncendio(document.slipIncedioForm);">Guardar</a>
					</div>
			</div>
		    </td>
		    <td align="left">
		    <div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipIncedioForm"/>,0,9);">Cancelar</a>
				</div>
			</div>
		    </td>
		    <td>&nbsp;</td>
	  </tr>        
    </table></td>
  </tr>
</table>
</midas:formulario>
</body>
</html>