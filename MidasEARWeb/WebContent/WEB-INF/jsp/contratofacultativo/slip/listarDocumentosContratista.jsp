<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<midas:tabla idTabla="documentosContratistas" 
					claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexoEquipoContratista"
					claseCss="tablaConResultados" nombreLista="anexosContratista"
					urlAccion="/solicitud/listarDocumentos.do">
					<midas:columna propiedad="archivo"/>
					<midas:columna propiedad="acciones"/>
</midas:tabla>