<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
  </head>
  

<body bgcolor="white">
<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipRCFuncionarios">
<table width="100%" border="0" cellspacing="8" cellpadding="0" id="desplegar" bgcolor="white">
<midas:oculto propiedadFormulario="idToSlip"/>
  <tr>
    <td align="right">Inciso</td>
    <td ><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="10" soloLectura="true"  onkeypress="return soloNumeros(this, event, false)"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.interes"/></td>
    <td><midas:texto id="interes" propiedadFormulario="interes" longitud="20"/></td>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.periodoAdicionalNotificaciones"/></td>
    <td><midas:texto id="periodoAdicionalNotificaciones" propiedadFormulario="periodoAdicionalNotificaciones" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.fechaRetroactiva"/></td>
     <td><midas:texto soloLectura="true" propiedadFormulario="fechaRetroactiva" id="fecha"
							onkeypress="return soloFecha(this, event, false);" onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
							<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();"><image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
							</td>
<%--	 <td></td>--%>
	<td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.retencion"/></td>
    <td><midas:texto id="retencion" propiedadFormulario="retencion" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.limiteTerritorial"/></td>
    <td><midas:texto id="limiteTerritorial" propiedadFormulario="limiteTerritorial" longitud="20"/></td>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.accionLegal"/></td>
    <td><midas:texto id="accionLegal" propiedadFormulario="accionLegal" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.leyesJurisdiccion"/></td>
    <td><midas:texto id="leyesJurisdiccion" propiedadFormulario="leyesJurisdiccion" longitud="20"/></td>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.condicionesGenerales"/></td>
    <td><midas:texto id="condicionesGenerales" propiedadFormulario="condicionesGenerales" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.consideracionesReaseguro"/></td>
    <td><midas:texto id="consideracionesReaseguro" propiedadFormulario="consideracionesReaseguro" longitud="20"/></td>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.garantiaExpresa"/></td>
    <td><midas:texto id="garantiaExpresa" propiedadFormulario="garantiaExpresa" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.subjetividades"/></td>
    <td><midas:texto id="subjetividades" propiedadFormulario="subjetividades" longitud="20"/></td>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.terminosPagoPrimas"/></td>
    <td><midas:texto id="terminosPagoPrimas" propiedadFormulario="terminosPagoPrimas" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.comision"/></td>
    <td><midas:texto id="comision" propiedadFormulario="comision" longitud="20"/></td>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.impuestos"/></td>
    <td><midas:texto id="impuestos" propiedadFormulario="impuestos" longitud="20"/></td>
  </tr>
<%--  <tr>--%>
<%--    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.siniestralidadAnos"/></td>--%>
<%--    <td><midas:texto id="siniestralidadAnos" propiedadFormulario="siniestralidadAnos" longitud="20"/></td>--%>
<%--    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.informacionAdicional"/></td>--%>
<%--    <td><midas:texto id="informacionAdicional" propiedadFormulario="informacionAdicional" longitud="20"/></td>--%>
<%--  </tr>--%>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.informacionComplementaria"/></td>
    <td colspan="3">  <div id="resultados">
    <midas:tabla idTabla="documentosFuncionarios"
					claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexoRCFuncionarios"
					claseCss="tablaConResultados" nombreLista="anexosFuncionarios"
					urlAccion="/solicitud/listarDocumentos.do" >
					<midas:columna propiedad="archivo"/>
					<midas:columna propiedad="acciones"/>
          </midas:tabla>
          </div>	
     </td>
  </tr>
  <tr>
  <td colspan="2" align="right">
  <div class="alinearBotonALaDerecha"> 
     <div id="b_agregar"  style="width: 180px;">
					     <a href="javascript: void(0);"
					    	onclick="javascript:  mostrarAdjuntarArchivoSlipWindow(<midas:escribe propiedad="idToSlip" nombre="slipRCFuncionariosForm"/>)"> Guardar Archivos
					     </a>
	 </div>
	 </div>
	 </td>
  </tr>
  <tr>
    <td colspan="4" class="titulo"><midas:mensaje clave="contratofacultativo.slip.rcFuncionarios.incisos"/></td>
  </tr>
  <tr>
    <td colspan="4">
    <div id="slipGeneralGrid" width="98%" height="300px" style="background-color:white;overflow:hidden"></div>
    </td>
  </tr>
   <tr>
	    <td colspan="4">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
<%--						<midas:columna titulo="Numero SubInciso" propiedad="numeroSubinciso" />--%>
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
	  </tr>  	
  <tr>
    <td align="right">
    <div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: guardarSlipFuncionarios(document.slipRCFuncionariosForm);">Guardar</a>
				</div>
	</div>
    </td>
	<td align="left">
		    <div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipRCFuncionariosForm"/>,0,9);">Cancelar</a>
				</div>
			</div>
	</td>
    <td>&nbsp;</td>
  </tr>
</table>
</midas:formulario>
</body>
</html>