<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
  </head>
  

<body bgcolor="white">

<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipAviacion">
<midas:oculto propiedadFormulario="idToSlip"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar" bgcolor="White">
  <tr>
    <td>
<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
        <td align="center"><midas:mensaje clave="contratofacultativo.slip.aviacion.datoSlip"/></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="4" cellpadding="0" id="desplegar">
      <tr>
        <td width="15%" align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.numeroInciso"/></td>
        <td width="35%">
          <midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="20" soloLectura="true" deshabilitado="true"onkeypress="return soloNumeros(this, event, false)"/>
        </td>
        <td width="15%" align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.matricula"/></td>
        <td width="35%"><midas:texto id="matricula" propiedadFormulario="matricula" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.serie"/></td>
        <td><midas:texto id="serie" propiedadFormulario="serie" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.uso"/></td>
        <td><midas:texto id="uso" propiedadFormulario="uso" longitud="20" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.tipoAeronave"/></td>
        <td><midas:comboCatalogo propiedad="tipoAeronave" styleId="tipoAeronave"  size="1" styleClass="cajaTexto" nombreCatalogo="tctipoaeronave" idCatalogo="idTipoAeronave" descripcionCatalogo="descripcionTipoAeronave" readonly="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.capacidad"/></td>
        <td><midas:texto id="capacidad" propiedadFormulario="capacidad" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.marca"/></td>
        <td><midas:texto id="marca" propiedadFormulario="marca" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.limitesGeograficos"/></td>
        <td><midas:texto id="limitesGeograficos" propiedadFormulario="limitesGeograficos" longitud="20" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.modelo"/></td>
        <td><midas:texto id="modelo" propiedadFormulario="modelo" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.aeropuertoBase"/></td>
        <td><midas:texto id="aeropuertoBase" propiedadFormulario="aeropuertoBase" longitud="20" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.ano"/></td>
        <td><midas:texto id="ano" propiedadFormulario="ano" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.tiposdeAeropuertos"/></td>
        <td><midas:texto id="tiposdeAeropuertos" propiedadFormulario="tiposdeAeropuertos" longitud="20" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.pagosVoluntarios"/></td>
        <td><midas:texto id="pagosVoluntarios" propiedadFormulario="pagosVoluntarios" longitud="20"  onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.gastosMedicosAParaPasajeros"/>
        <td><midas:texto id="gastosMedicosAParaPasajeros" propiedadFormulario="gastosMedicosAParaPasajeros" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.gastosMedicosBParaTripulantesyPilotos"/></td>
        <td><midas:texto id="gastosMedicosBParaTripulantesyPilotos" propiedadFormulario="gastosMedicosBParaTripulantesyPilotos" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.horasTotales"/></td>
        <td><midas:texto id="horasTotales" propiedadFormulario="horasTotales" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
      </tr>
      <tr>
        <td colspan="4">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr >
            <td class="titulo"><midas:mensaje clave="contratofacultativo.slip.aviacion.InformacionPilotos"/>
            </td>
          </tr>
       
        </table></td>
        </tr>

      <tr>
             <td colspan="4" width="100%">
              <div id="resultados">
				<midas:tabla idTabla="documentosPiloto"					 
					claseCss="tablaConResultados" nombreLista="informacionPiloto"
					urlAccion="/solicitud/listarDocumentos.do">
					<midas:columna titulo="Nombre Piloto" />
					<midas:columna titulo="Licencia Piloto" />
					<midas:columna titulo="Tipo Licencia" />
					<midas:columna titulo="Horas Vuelo" />
					
		 		</midas:tabla>
			</div>
           </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
        <tr>
	    <td colspan="4">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />						
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.aviacion.horasMarcayTipo"/></td>
        <td><midas:texto id="horasMarcayTipo" propiedadFormulario="horasMarcayTipo" longitud="20"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td align="right">SubLimites</td>
        <td><midas:texto id="sublimites" propiedadFormulario="subLimites" longitud="20"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>&nbsp;</td>
        <td class="guardar">
        	<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: guardarSlipAviacion(document.slipAviacionForm);">Guardar</a>
				</div>
			</div>
        </td>
        <td class="guardar">
        <div class="alinearBotonALaDerecha">
			<div id="b_regresar">
				<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipAviacionForm"/>,0,-1);">Cancelar</a>
				</div>
			</div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
	</td>
  </tr>
</table>
</midas:formulario>
</body>
</html>