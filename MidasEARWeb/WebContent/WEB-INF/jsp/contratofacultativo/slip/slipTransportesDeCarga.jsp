<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
					
		<script type="text/javascript">
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
  </head>
  

<body bgcolor="white">

<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipTransportesDeCarga">

<table width="100%" border="0" cellspacing="5" cellpadding="0" bgcolor="white">
  <tr>
    <td><table width="100%" border="0" cellspacing="5" cellpadding="0" id="desplegar">
      <tr>
        <td align="center"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.datosSlip"/></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="4" cellpadding="0" id="desplegar">
      <tr>
        <td width="19%" align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.numeroInciso"/></td>
        <td width="13%"><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
        <td width="9%">&nbsp;</td>
        <td width="13%">&nbsp;</td>
        <td width="46%">&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.lugarOrigen"/></td>
        <td><midas:texto id="lugarOrigen" propiedadFormulario="lugarOrigen" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.lugarDestino"/></td>
        <td><midas:texto id="lugarDestino" propiedadFormulario="lugarDestino" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td>&nbsp;</td>
      </tr>      
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.bienesAsegurados"/></td>
        <td> <midas:areatexto renglones="5"  propiedadFormulario="bienesAsegurados"  deshabilitado="true"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.tipoEmpaqueEmbalaje"/></td>
        <td><midas:texto id="tipoEmpaqueEmbalaje" propiedadFormulario="tipoEmpaqueEmbalaje" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.limiteMaximoEmbarque"/></td>
        <td><midas:texto id="limiteMaximoEmbarque" propiedadFormulario="limiteMaximoEmbarque" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.volumenAnualEmbarque"/></td>
        <td><midas:texto id="volumenAnualEmbarque" propiedadFormulario="volumenAnualEmbarque" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.riesgoAmparados"/></td>
        <td><midas:texto id="riesgoAmparados" propiedadFormulario="riesgoAmparados" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.transporteCarga.mediosTransportes"/></td>
        <td><midas:texto id="mediosTransportes" propiedadFormulario="mediosTransportes" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

        
	  <tr>
	    <td colspan="3">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />						
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
				</div>
	    </td>
	  </tr>  
	  
	<tr>
	    <td align="right">
<%--	    <div class="alinearBotonALaDerecha">--%>
<%--			<div id="b_guardar">--%>
<%--			<a href="javascript: void(0);">Guardar</a>--%>
<%--			</div>--%>
<%--		</div>--%>
	    </td>
	    <td align="left">
	    <div class="alinearBotonALaDerecha">
			<div id="b_regresar">
				<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipTransportesDeCargaForm"/>,0,9);">Regresar</a>
			</div>
		</div>
	    </td>
	    <td>&nbsp;</td>
  </tr>  
	          
    </table></td>
  </tr>
  

  
</table>
</midas:formulario>
</body>
</html>