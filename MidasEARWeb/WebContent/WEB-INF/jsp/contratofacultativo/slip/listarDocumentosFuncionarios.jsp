<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
 		
<midas:tabla idTabla="documentosFuncionarios"
					claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexoRCFuncionarios"
					claseCss="tablaConResultados" nombreLista="anexosFuncionarios"
					urlAccion="/solicitud/listarDocumentos.do">
					<midas:columna propiedad="archivo"/>
					<midas:columna propiedad="acciones"/>
</midas:tabla>