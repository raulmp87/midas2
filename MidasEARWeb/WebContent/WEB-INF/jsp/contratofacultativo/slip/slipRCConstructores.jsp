<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
  </head>
  

<body bgcolor="white">

<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipRCConstructores">

<table width="100%" border="0" cellspacing="5" cellpadding="0" id="desplegar" bgcolor="white">
<midas:oculto propiedadFormulario="idToSlip"/>
  <tr>
    <td width="18%" align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.numeroInciso"/></td>
    <td width="19%"><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="10" soloLectura="true"  onkeypress="return soloNumeros(this, event, false)"/></td>
    <td width="63%">&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.descripcionObra"/></td>
    <td colspan="2"><midas:texto id="descripcionObra" propiedadFormulario="descripcionObra" longitud="15" soloLectura="true"  deshabilitado="true"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.duracionObra"/></td>
    <td colspan="2"><midas:texto id="duracionObra" propiedadFormulario="duracionObra" longitud="15" onkeypress="return soloNumeros(this, event, false)"  deshabilitado="true"/></td>
  </tr>
  <tr>
    <td align="right">
    <etiquetas:etiquetaError property="sitioTrabajo" requerido="si"
					key="contratofacultativo.slip.rcConstructores.sitioTrabajo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
    <td colspan="2"><midas:texto id="sitioTrabajo" propiedadFormulario="sitioTrabajo" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.colindantes"/></td>
    <td colspan="2"><midas:texto id="colindantes" propiedadFormulario="colindantes" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.sistemasPreventivosContraDanos"/></td>
    <td colspan="2"><midas:texto id="sistemasPreventivosContraDanos" propiedadFormulario="sistemasPreventivosContraDanos" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.experienciaParaRealizarObra"/></td>
    <td colspan="2"><midas:texto id="experienciaParaRealizarObra" propiedadFormulario="experienciaParaRealizarObra" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right">
    <etiquetas:etiquetaError property="valorEstimadoObra" requerido="si"
					key="contratofacultativo.slip.rcConstructores.valorEstimadoObra" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
     <td  colspan="2"><midas:texto id="valorEstimadoObra" propiedadFormulario="valorEstimadoObra" longitud="20" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.paraquienRealizaObra"/></td>
    <td colspan="2"><midas:texto id="paraquienRealizaObra" propiedadFormulario="paraquienRealizaObra" longitud="20"/></td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.tipoMaquinaria"/></td>
    <td colspan="2">
     <html:select property="tipoMaquinaria" styleClass="cajaTexto">
        <html:options  style="with: 20px;" collection="tipoMaquinariaList" property="idTcTipoMaquinaria" labelProperty="descripcionTipoMaquinaria" />
     </html:select>
     </td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="contratofacultativo.slip.rcConstructores.anexarcontratosPlanos"/></td>
     <td  colspan="2" width="100%" >
            <div id="resultados">
				 <midas:tabla idTabla="documentosConstructores"
					claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexoRCConstructores"
					claseCss="tablaConResultados" nombreLista="anexosConstructores">
					<midas:columna propiedad="archivo"/>
					<midas:columna propiedad="acciones"/>
                 </midas:tabla>		
			</div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td colspan="2" align="center">
  <div class="alinearBotonALaDerecha"> 
     <div id="b_agregar"  style="width: 180px;">
					     <a href="javascript: void(0);"
					    	onclick="javascript:  mostrarAdjuntarArchivoSlipWindow(<midas:escribe propiedad="idToSlip" nombre="slipRCConstructoresForm"/>)"> Guardar Archivos
					     </a>
	 </div>
	 </div>
	 </td>
  </tr>
   <tr>
	    <td colspan="3">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Subramo" propiedad="descripcionSubRamo" />
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada" propiedad="sumaAsegurada" formato="${0,number,#,##0.00}"/>
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
					</midas:tabla>
				</div>
	    </td>
	  </tr>   
  <tr>
    <td align="right">
    <div class="alinearBotonALaDerecha">
		<div id="b_guardar">
		<a href="javascript: void(0);" onclick="javascript: guardarSlipContructores(document.slipRCConstructoresForm);">Guardar</a>
		</div>
	</div>
    </td>
	<td align="left">
		    <div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipRCConstructoresForm"/>,0,9);">Cancelar</a>
				</div>
			</div>
	</td>
    <td>&nbsp;</td>
  </tr>
</table>
</midas:formulario>
</body>
</html>