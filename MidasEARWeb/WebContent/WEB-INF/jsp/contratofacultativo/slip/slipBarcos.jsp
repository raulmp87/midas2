<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
					
		<script type="text/javascript">
			function submitForm()
			{
				document.forms[0].submit();
			}
		</script>
  </head>
  

<body bgcolor="white">

<midas:formulario accion="/contratofacultativo/slip/mostrarEditarSlipBarco">
<midas:oculto propiedadFormulario="idToSlip"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
        <td align="center"><midas:mensaje clave="contratofacultativo.slip.equipoContratista.datosdelSlip"/></td>
      </tr>
    </table></td>
     </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="white">    
   <tr>
    <td><table border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
        <td width="15%" align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.numeroInciso"/></td>
        <td width="35%"><midas:texto id="numeroInciso" propiedadFormulario="numeroInciso" longitud="10" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
        <td width="15%" align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.banderadeConveniencia"/></td>
        <td width="35%"><midas:texto id="banderadeConveniencia" propiedadFormulario="banderadeConveniencia" longitud="10" soloLectura="true" deshabilitado="true"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.materialdelCasco"/></td>
        <td><midas:texto id="materialdelCasco" propiedadFormulario="materialdelCasco" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.tonelajeBruto"/></td>
        <td><midas:texto id="tonelajeBruto" propiedadFormulario="tonelajeBruto" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.tipodeEmbarcacion"/></td>
        <td><midas:texto id="tipodeEmbarcacion" propiedadFormulario="tipodeEmbarcacion" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.tonelajeNeto"/></td>
        <td><midas:texto id="tonelajeNeto" propiedadFormulario="tonelajeNeto" longitud="20" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.ano"/></td>
        <td><midas:texto id="ano" propiedadFormulario="ano" longitud="5" soloLectura="true" deshabilitado="true" onkeypress="return soloNumeros(this, event, false)"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.sumaAsegurada"/></td>
        <td><midas:texto id="sumaAsegurada" propiedadFormulario="sumaAsegurada" longitud="10" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.areadeNavegacion"/></td>
        <td><midas:texto id="areadeNavegacion" propiedadFormulario="areadeNavegacion" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.sumaAseguradaCasco"/></td>
        <td><midas:texto id="sumaAseguradaCasco" propiedadFormulario="sumaAseguradaCasco" longitud="10" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.uso"/></td>
        <td><midas:texto id="uso" propiedadFormulario="uso" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.sumaAseguradaRC"/></td>
        <td><midas:texto id="sumaAseguradaRC" propiedadFormulario="sumaAseguradaRC" longitud="10" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.puertodeRegistro"/></td>
        <td><midas:texto id="puertodeRegistro" propiedadFormulario="puertodeRegistro" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.sumaAseguradaValorIncrementado"/></td>
        <td><midas:texto id="sumaAseguradaValorIncrementado"  propiedadFormulario="sumaAseguradaValorIncrementado" longitud="10" soloLectura="true" deshabilitado="true" onkeypress="return teclaPresionadaEsNumerica(event, this.value)"/></td>
      </tr>
      <tr>
        <td align="right"><midas:mensaje clave="contratofacultativo.slip.barcos.clasificacion"/></td>
        <td><midas:texto id="clasificacion" propiedadFormulario="clasificacion" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
        <tr>
	    <td colspan="4">
	     <div id="resultados">
					<midas:tabla idTabla="documentosCoberturas"					 
						claseCss="tablaConResultados" nombreLista="coberturas"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna titulo="Coberturas" propiedad="descripcionCobertura" />
						<midas:columna titulo="Suma Asegurada"  propiedad="sumaAsegurada"  formato="${0,number,#,##0.00}" />						
						<midas:columna titulo="Coutas"  propiedad="cuota"  formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Primas"  propiedad="prima" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Deducibles" propiedad="deducible" formato="${0,number,#,##0.00}" />
						<midas:columna titulo="Coaseguros" propiedad="coaseguro" formato="${0,number,#,##0.00}" />						
					</midas:tabla>
		</div>
	    </td>
	  </tr>      
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><midas:mensaje clave="contratofacultativo.slip.barcos.eslora"/></td>
        <td><midas:texto id="eslora" propiedadFormulario="eslora" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td><midas:mensaje clave="contratofacultativo.slip.barcos.serie"/></td>
        <td><midas:texto id="serie" propiedadFormulario="serie" longitud="20" onkeypress="return soloNumeros(this, event, false)"/></td>
      </tr>
      <tr>
        <td><midas:mensaje clave="contratofacultativo.slip.barcos.manga"/></td>
        <td><midas:texto id="manga" propiedadFormulario="manga" longitud="20" soloLectura="true" deshabilitado="true"/></td>
        <td><midas:mensaje clave="contratofacultativo.slip.barcos.matricula"/></td>
        <td><midas:texto id="matricula" propiedadFormulario="matricula" longitud="20" onkeypress="return soloAlfanumericos(this, event, false)"/></td>
      </tr>
      <tr>
        <td><midas:mensaje clave="contratofacultativo.slip.barcos.puntal"/></td>
        <td><midas:texto id="puntal" propiedadFormulario="puntal" longitud="20" soloLectura="true" deshabilitado="true"/></td>
         <td><etiquetas:etiquetaError property="lugardeConstruccion" requerido="si"
					key="contratofacultativo.slip.barcos.lugardeConstruccion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" /></td>
        <td><midas:texto id="lugardeConstruccion" propiedadFormulario="lugardeConstruccion" longitud="20"/></td>
      </tr>
      <tr>
        <td><midas:mensaje clave="contratofacultativo.slip.barcos.ultimafechAmttoInspeccion"/></td>
         <td><midas:texto  propiedadFormulario="ultimafechAmttoInspeccion" id="fecha"
							onkeypress="return soloFecha(this, event, false);"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
		 					</td>
		 <td><a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
		  	   <image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
		</td>
         <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="guardar">
        	<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: guardarSlipBarco(document.slipBarcosForm);">Guardar</a>
				</div>
			</div>
        </td>
		<td align="left">
		    <div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="idToSlip" nombre="slipBarcosForm"/>,0,9);">Cancelar</a>
				</div>
			</div>
		</td> 
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
</table>
</midas:formulario>
</body>
</html>