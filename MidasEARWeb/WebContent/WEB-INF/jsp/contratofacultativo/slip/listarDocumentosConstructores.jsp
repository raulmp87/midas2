<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
 
<midas:tabla idTabla="documentosConstructores"
					claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexoRCConstructores"
					claseCss="tablaConResultados" nombreLista="anexosConstructores"
					urlAccion="/solicitud/listarDocumentos.do">
					<midas:columna propiedad="archivo"/>
					<midas:columna propiedad="acciones"/>
</midas:tabla>			