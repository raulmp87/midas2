<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page isELIgnored="false"%>

<html>
 <head>
   <title>SLIP</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
    
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Slip page">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/loginAfirme.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">		
		
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
  </head>

<body bgcolor="White" onload="javascript:mostrarEditarSlipGrid(<%=request.getParameter("idToSlip")%>,0)" >

<midas:formulario accion="/contratofacultativo/slip/editarSlip">
	<table id="desplegar" bgcolor="White">
		<tr>
			<!-- Datos no editables del formulario Slip -->
			<td>
				<midas:oculto propiedadFormulario="idToSlip" nombreFormulario="slipForm" />
          		<midas:oculto propiedadFormulario="idToCotizacion" nombreFormulario="slipForm" />
          		<midas:oculto propiedadFormulario="idTcSubRamo" nombreFormulario="slipForm" />
          		<midas:oculto propiedadFormulario="idToSeccion" nombreFormulario="slipForm" />
			</td>
		</tr>
		<!-- Datos de la cotizacion -->
		<tr><td class="titulo" colspan="4">
			<midas:mensaje clave="contratofacultativo.slip.datosCotizacion"/></td>
		</tr>
		<tr>
			<th> <midas:mensaje clave="contratofacultativo.slip.cotizacion"/></th>
          	<td><midas:escribe nombre="slipForm" propiedad="cotizacionForm.idToCotizacionFormateada" /></td>
		</tr>
		<tr>
			<th> <midas:mensaje clave="midas.cotizacion.vigencia"/>: del</th>
          	<td><midas:escribe nombre="slipForm" propiedad="cotizacionForm.fechaInicioVigencia" /></td>
          	<th>al</th>
          	<td><midas:escribe nombre="slipForm" propiedad="cotizacionForm.fechaFinVigencia" /></td>
		</tr>
		<tr height="10" ></tr>
		<!-- Datos de la persona asegurada -->
		<tr><td class="titulo" colspan="4"><midas:mensaje clave="contratofacultativo.slip.cotizacion.datosPersonaAsegurado"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.persona.tipo"/></th>
			<td><midas:escribe propiedad="cotizacionForm.personaAsegurado.descripcionTipoPersona" nombre="slipForm"/> </td>
		</tr>
		<tr>
			<th> <midas:mensaje clave="midas.persona.nombre"/></th>
			<th><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
			<th><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
		</tr>
		<tr>
			<td><midas:escribe propiedad="cliente.nombre" nombre="slipForm"/></td>
			<td> <midas:escribe propiedad="cliente.apellidoPaterno" nombre="slipForm"/></td>
			<td> <midas:escribe propiedad="cliente.apellidoMaterno" nombre="slipForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.persona.rfc"/></th>
			<th><midas:mensaje clave="midas.persona.fechaNacimiento"/></th>
			<th><midas:mensaje clave="midas.persona.telefono"/></th>
			<th><midas:mensaje clave="midas.persona.email"/></th>
		</tr>
		<tr>
			<td><midas:escribe propiedad="cliente.codigoRFC" nombre="slipForm"/></td>
			<td><midas:escribe propiedad="cliente.fechaNacimiento" nombre="slipForm"/></td>
			<td><midas:escribe propiedad="cliente.telefono" nombre="slipForm"/></td>
			<td><midas:escribe propiedad="cliente.email" nombre="slipForm"/></td>
		</tr>
		<!-- Direccion de la persona asegurada -->
       	<tr height="10" ></tr> <tr><td class="titulo" colspan="4"><midas:mensaje clave="midas.direccion"/></td></tr>	<tr height="10" ></tr>
		<tr>
			<th><midas:mensaje clave="midas.direccion.nombreCalle"/>:</th>
			<td colspan="3"><midas:escribe propiedad="cliente.nombreCalle" nombre="slipForm"/></td>
		<tr>
			<th><midas:mensaje clave="midas.direccion.nombreColonia"/>:</th>
			<td><midas:escribe propiedad="cliente.nombreColonia" nombre="slipForm"/></td>
			<th><midas:mensaje clave="midas.direccion.idMunicipio"/>:</th>
			<td><midas:escribe propiedad="cliente.nombreDelegacion" nombre="slipForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.direccion.idEstado"/>:</th>
			<td><midas:escribe propiedad="cliente.descripcionEstado" nombre="slipForm"/></td>
			<th><midas:mensaje clave="midas.direccion.codigoPostal"/>:</th>
			<td><midas:escribe propiedad="cliente.codigoPostal" nombre="slipForm"/></td>
		</tr>
		<!-- INFORMACION A CAPTURAR DEL SLIP -->
		<tr height="10" ></tr>
		<tr><td class="titulo" colspan="4">Informacion del SLIP</td></tr>	<tr height="10" ></tr>
		<tr>
			<th><midas:mensaje clave="contratofacultativo.slip.siniestrabilidadTresAnios"/>:</th>
			<td><midas:texto propiedadFormulario="siniestrabilidad" longitud="50"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="contratofacultativo.slip.informacionAdicional"/>:</th>
			<td><midas:texto propiedadFormulario="informacionAdicional" longitud="50"/></td>
		</tr>
		<!-- DOCUMENTOS ANEXOS SLIP -->
		<tr height="10" ></tr>
		<tr><td class="titulo" colspan="4"><midas:mensaje clave="contratofacultativo.slip.documentosAnexos"/></td></tr>
		<tr height="10" ></tr>
		<tr>
		</tr><tr>
			<td colspan="4">
				<div id="resultados">
					<midas:tabla idTabla="documentosAnexos" claseDecoradora="mx.com.afirme.midas.decoradores.SlipAnexo" claseCss="tablaConResultados" nombreLista="anexos">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="acciones"/>
					</midas:tabla>
				</div>
				<div id="botonAgregar">
					 <div class="alinearBotonALaDerecha"> 
					       <div id="b_agregar" style="width: 180px;" >
						     <a href="javascript: void(0);"
						    	onclick="javascript:  mostrarAdjuntarArchivoSlipWindow(<midas:escribe propiedad="idToSlip" nombre="slipForm"/>)">Agregar Archivo
						     </a>
					      </div>
			 		</div>
				</div>
			</td>
		</tr>
		<tr>   
			<td colspan="4">
   			<div id="editarSlipIncisoGrid" width="98%" height="200px" style="background-color:white;overflow:hidden"></div>
  		</tr>
      <tr>
        <td align="center">
        	<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: guardarSlip(document.slipForm);">Guardar</a>
				</div>
			</div>
        </td>
        <td>&nbsp;</td>
        <td align="center">
	        <div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: window.close();">Cerrar</a>
					</div>
			</div>
        </td>
      </tr>
		</table>
</midas:formulario>
</body>
</html>