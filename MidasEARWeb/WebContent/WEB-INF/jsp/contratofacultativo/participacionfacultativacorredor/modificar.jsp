<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>&nbsp;&nbsp; &nbsp; 
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>



<midas:formulario accion="/contratofacultativo/participacionFacultativaCorredor/mostrarModificar">
 <midas:oculto propiedadFormulario="idTdParticipacionFacultativo"/>
  <midas:oculto propiedadFormulario="participacionCorredorFacultativaForm.idTdParticipacionCorredorFac"/>
 <midas:oculto propiedadFormulario="reaseguradorCorredorForm.idTcReaseguradorCorredor"/>	
 <input id="valorOriginalPorcentaje" type="hidden" value="<midas:escribe propiedad="porcentajeParticipacion" nombre="participacionFacultativaForm"/>"/>
   	<table id="agregar">
		<tr>
			<td class="titulo" colspan="3">
				<midas:mensaje clave="midas.accion.agregarParticipacionCorredor" />			
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="reaseguradorCorredorForm.nombre" name="participacionFacultativaForm" requerido="no"
					key="contratos.participacioncorredor.nombreCorredor" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="reaseguradorCorredorForm.nombre" nombreFormulario="participacionFacultativaForm" deshabilitado="true" />
			</td>		
			<td></td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError property="reaseguradorCorredorForm.idTcReaseguradorCorredor" requerido="si"
				key="contratos.participacioncorredor.reasegurador" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:comboCatalogo   idCatalogo="idtcreaseguradorcorredor" styleId="reasegurador" propiedad="idParticipante" descripcionCatalogo="nombre" nombreCatalogo="tcreasegurador" size="1" styleClass="cajaTexto" />
			</td>
			<td></td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="reaseguradorCorredorForm.porcentajeParticipacion" requerido="si"
					key="contratos.participacioncorredor.porcentajeParticipacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="porcentajeParticipacion" propiedadFormulario="porcentajeParticipacion"
					caracteres="5" onkeypress="return soloNumeros(this, event, true)" />%
			</td>	
			<td></td>	
		</tr>
		<tr>
			<td>
			     <etiquetas:etiquetaError property="totalReasegurador" requerido="si"
					key="contratos.participacioncorredor.totalReasegurador" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto deshabilitado="true" id="totalReasegurador" propiedadFormulario="totalReasegurador"
					caracteres="5" onkeypress="return soloNumeros(this, event, true)" />%
			</td>	
			<td></td>	
		</tr>
		<tr>
			<td class="campoRequerido" colspan="1">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: closeAgregarParticipacionFacultativaCorredor();"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_modificar">
						<a href="javascript: void(0);" onclick="modificarParticipacionFacultativaCorredor(document.participacionFacultativaForm,<midas:escribe propiedad="idTmContratoFacultativo" nombre="participacionFacultativaForm"/>,<midas:escribe propiedad="idTdContratoFacultativo" nombre="participacionFacultativaForm"/>,<midas:escribe propiedad="idTdParticipacionFacultativo" nombre="participacionFacultativaForm"/>)"><midas:mensaje clave="midas.accion.modificar"/></a>
					</div>
				</div>
			</td> 
		</tr>		
	</table>
</midas:formulario>