<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div id="datosDetalleContratoEndosoAnterior" style="display:none">
<div id="linkOcultarDetalle">
	<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='none';document.getElementById('linkMostrarDetalle').style.display='block';" style="cursor:pointer" >
		<tr><th>Ocultar Detalle Contrato Anterior</th></tr>
	</table>
</div>
	<table id="desplegar" width="97%" style="font-weight:bold;">
		<!-- Datos de la cotizacion -->
		<tr><td class="titulo" colspan="5">
			  <midas:mensaje clave="contratofacultativo.slip.desplegar.cotizacion.anterior.titulo"/>
			</td>
		</tr>
		<tr height="15px"><td colspan="5">&nbsp;</td></tr>
       <tr>
           <td width="15%" align="right" >
              <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ramo"/>:
           </td>
           <td width="35%" style="font-weight: normal">
              <midas:escribe propiedad="contratoFacultativoAnterior.ramoForm.descripcion" nombre="contratoFacultativoForm"/>
           </td>
           <td width="5%"></td>
            <td width="15%" align="right">
                <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAsegurada"/>:
           </td>
           <td width="30%" style="font-weight: normal" align="right">
                <midas:escribe propiedad="contratoFacultativoAnterior.sumaAseguradaTotal" nombre="contratoFacultativoForm"/>
            </td>
       </tr>
       <tr>
           <td align="right">
              <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.subramo"/>:
           </td>
           <td style="font-weight:normal;overflow:hidden;">
               <midas:escribe propiedad="contratoFacultativoAnterior.subRamoForm.descripcion" nombre="contratoFacultativoForm"/>
           </td>
           <td></td>
           <td align="right">
              <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.facultativo"/>:
           </td>
           <td style="font-weight: normal" align="right">
				<logic:notEmpty name="contrato">
					<midas:escribe propiedad="contratoFacultativoAnterior.porcentajeFacultativo" nombre="contratoFacultativoForm" />
				</logic:notEmpty>
				<logic:empty name="contrato">
					<midas:escribe propiedad="contratoFacultativoAnterior.porcentajeFacultativo" nombre="contratoFacultativoForm" />
				</logic:empty>
           </td>
       </tr>
       <tr>
       		<td colspan="3"></td>
       		<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.montoAFacultar" />:</td>
			<td style="font-weight: normal" align="right">
				<logic:notEmpty name="contrato">
					<midas:escribe propiedad="contratoFacultativoAnterior.sumaFacultada" nombre="contratoFacultativoForm" />
				</logic:notEmpty>
				<logic:empty name="contrato">
					<midas:escribe propiedad="contratoFacultativoAnterior.sumaFacultada" nombre="contratoFacultativoForm" />
				</logic:empty>
			</td>
       </tr>
       <tr>
          <td align="right">
          	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaInicial"/>:
	 	  </td>
 		  <td style="font-weight: normal">
 		  	<midas:escribe propiedad="contratoFacultativoAnterior.fechaInicial" nombre="contratoFacultativoForm" />
		  </td>
		  <td></td>
	      <td align="right">
	       	<etiquetas:etiquetaError property="idFormaPago" requerido="si"
					key="contratofacultativo.slip.registrar.cotizacion.formaPago" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
	      </td>
	      <td>
	      	<logic:empty name="contrato">
	      		<midas:combo id="formaPago" propiedad="contratoFacultativoAnterior.idFormaPago" styleClass="cajaTexto" deshabilitado="true">	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>	
	       	</logic:empty>
	       	<logic:notEmpty name="contrato">
	       		<midas:combo id="formaPago" propiedad="contratoFacultativoAnterior.idFormaPago" styleClass="cajaTexto" deshabilitado="true">	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>	
	       	</logic:notEmpty>
		  </td>
       </tr>
       <tr>
       	   <td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaFinal"/>:
           </td>
           <td style="font-weight: normal">
	   	 		<midas:escribe propiedad="contratoFacultativoAnterior.fechaFinal" nombre="contratoFacultativoForm" />
            </td>
            <td></td>
	        <td align="right">
	        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.notaCobertura" />:			
	       </td> 
	       <td style="font-weight: normal">
	       		<logic:empty name="contrato">
       				<midas:escribe propiedad="contratoFacultativoAnterior.notaCobejrtura" nombre="contratoFacultativoForm" />
       			</logic:empty>
       			<logic:notEmpty name="contrato">
       				<midas:escribe propiedad="contratoFacultativoAnterior.notaCobejrtura" nombre="contratoFacultativoForm" />
       			</logic:notEmpty>
	       	</td>
       </tr>
       <tr>
       		<td colspan="4"></td>
       		<td align="right" style="font-weight: normal;font-size:10px">
       			<i>&iquest;<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.requiereContratoReclamos"/>?</i>
       			<logic:empty name="contrato">
       				<midas:checkBox valorEstablecido="1" propiedadFormulario="contratoFacultativoAnterior.requiereControlReclamos" deshabilitado="true"></midas:checkBox>
       			</logic:empty>
       			<logic:notEmpty name="contrato">
       				<midas:checkBox valorEstablecido="1" propiedadFormulario="contratoFacultativoAnterior.requiereControlReclamos" deshabilitado="true"></midas:checkBox>
       			</logic:notEmpty>
       		</td>
       </tr>
   </table>
</div>