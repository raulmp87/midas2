<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ page isELIgnored="false" %>

<midas:formulario accion="/contratofacultativo/cobertura/mostrarDetalle">
<c:if test="${not empty detalleContratoFacultativoForm.detalleContratoFacultativoAnterior.idTdContratoFacultativo}">
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Detalle Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="mostrarConfiguracionCobertura.jsp" flush="true"/>
	<hr/>
</c:if>

	<html:hidden property="idTmContratoFacultativo" name="detalleContratoFacultativoForm" styleId="idTmContratoFacultativo"/>
	<html:hidden property="idTdContratoFacultativo" name="detalleContratoFacultativoForm" styleId="idTdContratoFacultativo"/>
	<html:hidden property="planPagosStr" name="detalleContratoFacultativoForm" styleId="planPagosStr"/>
	<html:hidden property="estatusAutorizacionPlanPagos" name="detalleContratoFacultativoForm" styleId="estatusAutorizacionPlanPagos"/>
	
	<center>
		<table id="desplegar" width="97%" style="font-weight:bold;">
			<!-- Datos de la configuracion de la cobertura -->
			<tr>
				<td class="titulo" colspan="5"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.titulo" /></td>
			</tr>
			<tr>
				<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.seccion" />:</td>
				<td colspan="4">
					<html:textarea property="descripcionSeccion" readonly="true" rows="1" styleClass="cajaTexto" style="font-family:arial;font-size:12px" />
				</td>
			</tr>
			<tr>
				<td width="15%" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.cobertura" />:</td>
				<td width="35%">
					<html:textarea property="descripcionCobertura" readonly="true" rows="2" styleClass="cajaTexto" style="font-family:arial;font-size:12px" />
				</td>
				<td width="5%"></td>
				<td width="20%" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso" />:</td>
				<td width="25%"><midas:texto soloLectura="true" propiedadFormulario="numeroInciso" /></td>
			</tr>
			<tr>
				<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.sumaAsegurada" />:</td>
				<td><midas:texto soloLectura="true" propiedadFormulario="sumaAsegurada" /></td>
				<td width="5%"></td>
				<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.deducible" />:</td>
				<td>
					<midas:texto propiedadFormulario="deducibleCobertura" id="deducibleCobertura" caracteres="16" onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
				</td>
			</tr>
			<tr>
				<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.porcentajeFacultada" />:</td>
				<td>
					<table width="100%">
						<tr>
							<td width="90%">
								<midas:texto propiedadFormulario="porcentajeFacultativo"
									id="porcentajeFacultativo" soloLectura="false" caracteres="16"
									onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
							</td>
							<td width="10%">
								<a href="javascript:void(0)"
									onclick="javascript: recalcularPorcentajeMontoFacultativo(1)"><img
										src="/MidasWeb/img/calcIconPercent.jpg" style="border: none"
										title="Recalcular en base al porcentaje" />
								</a>
							</td>
						</tr>
					</table>
				</td>
				<td></td>
				<td align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaFacultada" />:
				</td>
				<td>
					<html:text property="primaFacultadaCobertura" styleClass="cajaTexto" styleId="primaFacultadaCobertura" 
						maxlength="16" onblur="formatearMontosRegistrarCotizacionFacultativa()" 
						readonly="${!detalleContratoFacultativoForm.primaFacultadaCobModificable}" />
				</td>
			</tr>
			<tr>
				<td align="right" rowspan="2"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.montoAFacultar" />:</td>
				<td rowspan="2">
					<table width="100%">
						<tr>
							<td width="90%">
								<midas:texto propiedadFormulario="sumaFacultada"
									id="sumaFacultada" soloLectura="false"
									onblur="formatearMontosRegistrarCotizacionFacultativa()" />
							</td>
							<td width="10%">
								<a href="javascript:void(0)"
									onclick="javascript: recalcularPorcentajeMontoFacultativo(2)"><img
										src="/MidasWeb/img/calcIconAmount.jpg" style="border: none"
										title="Recalcular en base al monto a facultar" />
								</a>
							</td>
						</tr>
					</table>
				</td>
				<td align="right" colspan = "2">
					<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional" />:
				</td>
				<td>
					<midas:texto propiedadFormulario="montoPrimaAdicional" onblur="validarMontoRegistrarEgreso(this)" id="montoPrimaAdicional" caracteres ="16" />
				</td>
			</tr>
			<tr>
				<td align="right" colspan = "2">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaTotal" />:
				</td>
				<td align="left">
					<midas:texto propiedadFormulario="primaTotalCobertura" id="primaTotalCobertura" caracteres="16" soloLectura="true"/>
				</td>
			</tr>
			<tr>
				<td align="right">
					<midas:mensaje
						clave="contratofacultativo.configuracion.cobertura.sumaAseguradaEndoso" />:
				</td>
				<td style="font-weight: normal">
					${detalleContratoFacultativoForm.contratoFacultativoForm.sumaAseguradaEndoso}
				</td>
				<td></td>
				<td align="right"><midas:mensaje clave="contratofacultativo.moneda"/>:</td>
				<td><midas:texto propiedadFormulario="moneda" nombreFormulario="detalleContratoFacultativoForm" soloLectura="true"/></td>
				<td colspan="3"></td>
			</tr>
		</table>
		<div id="gridboxFacultativo" class="dataGridConfigurationClass"
			style="width: 100%"></div>
		<div id="gridboxFacultativoCorredor" class="dataGridConfigurationClass"
			style="width: 100%; display: none"></div>
		<table align="right">
			<tr>
				<td>
					<div id="botonMostrarFacultativoCorredor">
						<div id="b_regresar">
							<a href="javascript: void(0);"
								onclick="javascript: mostrarGridAgregarFacultativo();"><midas:mensaje clave="midas.accion.regresar" />
							</a>
						</div>
					</div>
				</td>
				<c:if test="${not empty detalleContratoFacultativoForm.detalleContratoFacultativoAnterior.idTdContratoFacultativo}">
				<td>
					
					<div id="b_agregar" style="width: 165px">
						<a href="javascript: void(0);"
							onclick="copiarParticipacionesFacultativoCobertura(<bean:write name="detalleContratoFacultativoForm" property="idTmContratoFacultativo" />,
							 			<bean:write name="idTdContratoFacultativo"/>)">
							<midas:mensaje clave="contratofacultativo.configuracion.cobertura.mantenerParticipantes"/></a>
					</div>
				</td>
				</c:if>
				<td>
					<div id="b_agregar" style="width: 140px">
						<a href="javascript: void(0);"
							onclick="javascript: mostrarAgregarParticipacionFacultativa(<bean:write name="idTdContratoFacultativo"/>,null);">
							<midas:mensaje clave="midas.accion.boton.agregarParticipacionFacultativa"/></a>
					</div>
				</td>
			</tr>
			</table>
			<table id="desplegar" style="font-weight:bold;">
				<tr height="30px">
					<th class="titulo" colspan="6"><midas:mensaje clave="contratofacultativo.slip.coberturas.planPagos"/></th>
				</tr>
				<tr>
					<th width="20%"><midas:mensaje clave="contratofacultativo.slip.coberturas.numeroExhibiciones" /></th>
					<td><midas:texto id="numeroExhibiciones" propiedadFormulario="numeroExhibiciones" onkeypress="return soloNumeros(this,event,false);"/></td>
					<td width="10%">
						<div id="b_agregar" style="width: 140px">
							<a href="javascript: void(0);"
								onclick="javascript: regenerarPlanPagosCobertura();">
								<midas:mensaje clave="contratofacultativo.slip.coberturas.aplicar"/></a>
						</div>
					</td>
					<th width="20%">
						<midas:mensaje clave="contratofacultativo.slip.coberturas.estatusAutorizacion" />
					</th>
					<td>
						<logic:equal name="detalleContratoFacultativoForm" property="estatusAutorizacionPlanPagos" value="0">
							<img  id="imgAutorizacionPlanPagos" src="/MidasWeb/img/blank.gif" alt="" title="" height="12px" width="12px"/>										
						</logic:equal>									
						<logic:equal name="detalleContratoFacultativoForm" property="estatusAutorizacionPlanPagos" value="1">
							<img  id="imgAutorizacionPlanPagos"src="/MidasWeb/img/ico_yel.gif" alt="Requiere autorizaci\u00F3n" title="Requiere autorizaci&oacute;n" height="12px" width="12px" />										
						</logic:equal>
						<logic:equal name="detalleContratoFacultativoForm" property="estatusAutorizacionPlanPagos" value="7">
							<img id="imgAutorizacionPlanPagos" src="/MidasWeb/img/ico_green.gif" alt="Autorizado" title="Autorizado" height="12px" width="12px" />
						</logic:equal>
						<logic:equal name="detalleContratoFacultativoForm" property="estatusAutorizacionPlanPagos" value="8">
							<img id="imgAutorizacionPlanPagos" src="/MidasWeb/img/ico_red.gif" alt="Rechazado" title="Rechazado" height="12px" width="12px" />
						</logic:equal>
					</td>
					<td>
						<logic:equal name="detalleContratoFacultativoForm" property="mostrarBotonAutorizarPlanPagos" value="1">
							<div id="b_agregar" style="width: 140px">
								<a href="javascript: void(0);"
									onclick="javascript: autorizarPlanPagosCobertura();">
									<midas:mensaje clave="contratofacultativo.slip.coberturas.autorizar"/></a>
							</div>
						</logic:equal>
					</td>
				</tr>
				</table>			
				<div id="gridPlanPagosFacultativos" style="width: 330px; height: 200px"></div>
				
			<table align="right">
			<tr>
				<td>
					<div class="cajaTexto">
						<midas:checkBox propiedadFormulario="aplicarATodas"
							valorEstablecido="0">
							<midas:mensaje
								clave="contratofacultativo.slip.coberturas.aplicarATodas" />
						</midas:checkBox>
					</div>
				</td>
				<td>
					<div id="b_agregar">
						<a href="javascript: void(0);"
							onclick="javascript: guardarConfiguracionCoberturaContFacultativo(<midas:escribe propiedad="idTdContratoFacultativo" nombre="detalleContratoFacultativoForm"/>)"><midas:mensaje
								clave="midas.accion.guardar" />
						</a>
					</div>
				</td>
			</tr>
		</table>
	</center>
</midas:formulario>