<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/tarifa/configuracion/agregar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<table id="desplegar">
         	<tr>
	           	<td class="titulo" colspan="2"><midas:mensaje clave="midas.accion.agregar"/>
	           	<midas:oculto propiedadFormulario="idToRiesgo"/>
      			<midas:oculto propiedadFormulario="idConcepto"/>
      			</td>
         	</tr>
			<tr>
				<th><midas:etiquetaTarifa idBase="1"/></th>
				<td><midas:tarifa styleId="idBase1" propiedad="idBase1" idBase="1" size="1"/></td>						
			</tr>
			<tr>
				<th><midas:etiquetaTarifa idBase="2"/></th>
				<td><midas:tarifa  styleId="idBase2" propiedad="idBase2" idBase="2" size="1"/></td>												
			</tr>
			<tr>
				<th><midas:etiquetaTarifa  idBase="3"/></th>
				<td><midas:tarifa  styleId="idBase3" propiedad="idBase3" idBase="3" size="1"/></td>												
			</tr>
			<tr>
				<th><midas:etiquetaTarifa idBase="4"/></th>
				<td><midas:tarifa styleId="idBase4" propiedad="idBase4" idBase="4" size="1"/></td>												
			</tr>
			<tr>
				<th><midas:etiquetaTarifa idBase="5"/></th>
				<td><midas:tarifa styleId="valor" propiedad="valor" idBase="5" size="1"/></td>												
			</tr>
			<tr>
				<td class="campoRequerido">
		 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
				</td>	
				<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: cerrarVentanaTarifa();">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<div id="b_agregar">
						<a href="javascript: void(0);"

						onclick="javascript: agregarTarifa(document.tarifaForm);"/>

						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		</table>
</midas:formulario>
