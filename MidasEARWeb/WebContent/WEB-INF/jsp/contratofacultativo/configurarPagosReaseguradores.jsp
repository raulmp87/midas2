<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
<midas:formulario accion="/contratofacultativo/contrato/mostrarConfiguracionReaseguradores">
	<html:hidden property="idTmContratoFacultativo"	name="contratoFacultativoForm" styleId="idTmContratoFacultativo" />

	<table width="97%" style="font-weight: bold;">
		<!-- Datos de la configuracion de la cobertura -->
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="contratofacultativo.configuracion.pagosReaseguradores" />
			</td>
		</tr>
		<tr>
			<td><div id="loadingDataGrid"></div></td>
			<td height="15px" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div style="width:632px;height:170px">
					<div id="configuracionReaseguradoresContratoFacultativoGridDiv" style="width:100%;height:150px"></div>
				</div>
			</td>
		</tr>
		<tr><td height="10px">&nbsp;</td></tr>
	</table>
	<div id="configuracionReaseguradores" style="display:none">
		<table width="97%" style="font-weight: bold;">
			<tr>
				<td style="font-size:10px" align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.pagosreaseguradores.reasegurador" />:
				</td>
				<td colspan="3">
					<input type="text" class="cajaTexto" id="nombreReasegurador" readonly="readonly" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td width="22%"  style="font-size:10px" align="right"><midas:mensaje clave="contratofacultativo.configuracion.pagosreaseguradores.montoTotal" />:</td>
				<td width="35%" ><input type="text" class="cajaTexto" id="montoTotal" readonly="readonly" /></td>
				<td width="15%" style="font-size:10px" align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.pagosreaseguradores.formaPago" />:
				</td>
				<td width="15%">
					<input type="text" id="formaPago" class="cajaTexto" readonly="readonly"/>
				</td>
				<td width="13%">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="5" align="center">
					<div style="width:352px;height:290px">
						<div id="configuracionReaseguradorContratoFacultativoGridDiv" style="width:100%;height:284px"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="5">
					<div id="b_agregar" style="float:right">
				    	<a href="javascript: void(0);" onclick="guardarConfiguracionPagosFacultativoReasegurador()">
				    		<midas:mensaje clave="midas.accion.guardar"/>
				    	</a>
				    </div>
				</td>
			</tr>
		</table>
	</div>
</midas:formulario>