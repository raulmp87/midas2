<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<midas:formulario accion="/contratofacultativo/cobertura/mostrarDetalle">

<logic:present property="detalleContratoFacultativoAnterior" name="detalleContratoFacultativoForm" >
<c:if test="${not empty detalleContratoFacultativoForm.detalleContratoFacultativoAnterior.idTdContratoFacultativo}">
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Detalle Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="../mostrarConfiguracionCobertura.jsp" flush="true"/>
	<hr/>
</c:if>
</logic:present>

     <table id="desplegar" style="font-weight:bold">
		<!-- Datos de la configuracion de la cobertura -->
		<tr><td class="titulo" colspan="4">
			  <midas:mensaje clave="contratofacultativo.configuracion.cobertura.titulo"/>
			</td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.seccion" />:</td>
			<td colspan="4">
				<html:textarea property="descripcionSeccion" readonly="true" rows="1" styleClass="cajaTexto" style="font-family:arial;font-size:12px" />
			</td>
		</tr>
		<tr>
			<td width="20%" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.cobertura" />:</td>
			<td width="40%"><html:textarea property="descripcionCobertura" readonly="true" rows="2" styleClass="cajaTexto" style="font-family:arial;font-size:12px" /></td>
			<td width="5%"></td>
			<td width="15%" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso" />:</td>
			<td width="20%"><midas:texto soloLectura="true" propiedadFormulario="numeroInciso" /></td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.sumaAsegurada" />:</td>
			<td><midas:texto soloLectura="true" propiedadFormulario="sumaAsegurada" /></td>
			<td></td>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.deducible" />:</td>
			<td><midas:texto soloLectura="true" propiedadFormulario="deducibleCobertura" /></td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.porcentajeFacultada" />:</td>
			<td><midas:texto propiedadFormulario="porcentajeFacultativo" soloLectura="TRUE" /></td>
			<td></td>
			<td align="right"> <midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaFacultada" />:</td>
			<td><midas:texto soloLectura="true"propiedadFormulario="primaFacultadaCobertura" /></td>
		</tr>
		<tr>
			<td align="right" rowspan="2">Monto Facultado:</td>
			<td rowspan="2"><midas:texto propiedadFormulario="sumaFacultada" soloLectura="true"/></td>
			<td></td>
			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional" />:</td>
			<td><midas:texto propiedadFormulario="montoPrimaAdicional" soloLectura="true" /></td>
		</tr>
		<tr>
			<td colspan = "2" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaTotal" />:</td>
			<td><midas:texto soloLectura="true" propiedadFormulario="primaTotalCobertura" /></td>
		</tr>
		<tr>
			<td align="right"> <midas:mensaje clave="contratofacultativo.configuracion.cobertura.sumaAseguradaEndoso" />:</td>
			<td><midas:texto soloLectura="true" propiedadFormulario="contratoFacultativoForm.sumaAseguradaEndoso" nombreFormulario="detalleContratoFacultativoForm"/></td>
			<td></td>
			<td align="right"><midas:mensaje clave="contratofacultativo.moneda"/>:</td>
			<td><midas:texto propiedadFormulario="moneda" nombreFormulario="detalleContratoFacultativoForm" soloLectura="true"/></td>
			<td colspan="3"></td>
		</tr>
	</table>
      <div id="gridboxFacultativo" class="dataGridConfigurationClass" style="width: 100%"></div>
      <div id="gridboxFacultativoCorredor" class="dataGridConfigurationClass" style="width: 100%"></div>
      <div id="botonMostrarFacultativoCorredor">
       	  <div class="alinearBotonALaDerecha">
             <div id="b_regresar">
			 	<a href="javascript: void(0);"
					onclick="javascript: mostrarGridAgregarFacultativo();"><midas:mensaje clave="midas.accion.regresar" />
				</a>
			 </div>
		  </div>					
      </div>
       <div id="botonAutorizaContratoFacultativo">
        <!-- div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					    <a href="javascript: void(0);" onclick="javascript: sendRequest(document.detalleContratoFacultativoForm,'/MidasWeb/contratofacultativo/cotizafacultativa/mostrarDetalle.do?id=<midas:escribe propiedad="idTmContratoFacultativo" nombre="detalleContratoFacultativoForm"/>&idPadre=<midas:escribe propiedad="contratoFacultativoForm.slipForm.idToSlip" nombre="detalleContratoFacultativoForm"/>', 'configuracion_detalle',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
	   </div-->
      </div>
       
       
</midas:formulario>