<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
      	   
<midas:formulario accion="/contratofacultativo/modificarCotizacionFacultativa">

<logic:present property="contratoFacultativoAnterior" name="contratoFacultativoForm" >
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="mostrarCotizacionFacultativa.jsp" flush="true"/>
	<hr/>
</logic:present>

	 <html:hidden styleId="tipoMensaje" property="tipoMensaje"/>
     <html:hidden styleId="mensaje" property="mensaje"/>
     <midas:oculto propiedadFormulario="slipForm.idTmLineaSoporteReaseguro" />
	<midas:oculto propiedadFormulario="slipForm.idToSoporteReaseguro" />
	<midas:oculto propiedadFormulario="subRamoForm.idSubRamo" />
	<midas:oculto propiedadFormulario="slipForm.idToCotizacion" />
	<midas:oculto propiedadFormulario="slipForm.tipoDistribucion" />
	<midas:oculto propiedadFormulario="sumaAseguradaTotal" />
	<midas:oculto propiedadFormulario="subRamoForm.descripcion" />
	<midas:oculto propiedadFormulario="ramoForm.descripcion" />
	<html:hidden property="idTmContratoFacultativo" name="contratoFacultativoForm" styleId="idTmContratoFacultativo"/>
	<html:hidden property="slipForm.idToSlip" name="contratoFacultativoForm" styleId="idToSlipDetalleCotizacion"/>
    
    <table id="desplegar" width="97%" style="font-weight:bold;">
		<!-- Datos de la cotizacion -->
		<tr>
			<td class="titulo" colspan="5">
			  <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.titulo"/>
			</td>
		</tr>
		<tr height="15px"><td colspan="5">&nbsp;</td></tr>
		<tr>
			<td width="15%" align="right">
				<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ramo" />:
			</td>
			<td width="35%" style="font-weight: normal">
				<midas:escribe propiedad="ramoForm.descripcion"	nombre="contratoFacultativoForm" />
			</td>
			<td width="5%"></td>
			<td width="15%" align="right">
				<midas:mensaje
					clave="contratofacultativo.slip.registrar.cotizacion.sumaAsegurada" />:
			</td>
			<td width="30%" style="font-weight:normal">
				<midas:escribe propiedad="sumaAseguradaTotal" nombre="contratoFacultativoForm"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<midas:mensaje
					clave="contratofacultativo.slip.registrar.cotizacion.subramo" />:
			</td>
			<td style="font-weight: normal">
				<midas:escribe propiedad="subRamoForm.descripcion"
					nombre="contratoFacultativoForm" />
			</td>
			<td></td>
			<td align="right">
           		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAseguradaEndoso"/>:
           	</td>
           	<td style="font-weight: normal">
           		<midas:escribe nombre="contratoFacultativoForm" propiedad="sumaAseguradaEndoso"/>
           	</td>
       </tr>
       <tr>
       	 <td align="right">	
            <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaInicial" />:
		 </td>
		 <td>
		 	<midas:texto propiedadFormulario="fechaInicial" id="fechaInicial"/>
		 </td>
		 <td></td>
		 <td align="right">
				<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.facultativo" />:
		 </td>
			<td width="35%">
				<table width="100%">
					<tr>
						<td width="90%">
							<midas:texto propiedadFormulario="porcentajeFacultativo"
								id="porcentajeFacultativo" soloLectura="false" caracteres="16"
								onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
						</td>
						<td width="10%">
							<a href="javascript:void(0)"
								onclick="javascript: recalcularPorcentajeMontoFacultativo(1)">
								<img src="/MidasWeb/img/calcIconPercent.jpg" style="border: none"
									title="Recalcular en base al porcentaje"/>
							</a>
						</td>
					</tr>
				</table>
			</td>            
       </tr>
        <tr>
       		 <td align="right">
			 <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaFinal" />:
           </td>
           <td>
               <midas:texto propiedadFormulario="fechaFinal" id="fechaFinal"/>
           </td>
           <td></td>
           <td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajeRetencion"/>:
           </td>
           <td>
	   	 		<midas:texto propiedadFormulario="porcentajeRetencion" soloLectura="true" />
            </td>
       </tr>
        <tr>
       		<td align="right">
	       	<!--  	<etiquetas:etiquetaError property="idFormaPago" requerido="si"
						key="contratofacultativo.slip.registrar.cotizacion.formaPago" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
			-->
	       </td>
	       <td>
	       <!--
	       		<midas:combo id="formaPago" propiedad="idFormaPago" styleClass="cajaTexto" >	
						<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
						<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
						<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
						<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
						<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>
			-->				
		    </td>
		    <td></td>
		    <td align="right">
	        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajeCuotaParte" />:			
			</td> 
			<td>    				
	            <midas:texto propiedadFormulario="porcentajeCuotaParte" soloLectura="true"/>       		
			</td>
       </tr>
       <tr>
       		<td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaalcien"/>:
           </td>
           <td>
	   	 		<midas:texto id="primaCien" propiedadFormulario="primaCien" soloLectura="true" />
            </td>
            <td></td>
            <td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajePrimerExcedente"/>:
			</td>
			<td colspan="1">
	   	 		<midas:texto propiedadFormulario="porcentajePrimerExcedente" soloLectura="true" />
			</td>
       </tr>
       
       <tr>
       	   <td align="right">
	        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primafacultativo" />:			
	       </td> 
	       <td>
	       		<midas:texto id="primaFac" propiedadFormulario="primaFac" soloLectura="true" />	       		
	       </td>
	       <td></td> 
       		<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.montoAFacultar" />:</td>
			<td>
				<table width="100%">
					<tr>
						<td width="90%">
							<midas:texto propiedadFormulario="sumaFacultada"
								id="sumaFacultada" soloLectura="false"
								onblur="validarMontoRegistrarEgreso(this)" />
						</td>
						<td width="10%">
							<a href="javascript:void(0)"
								onclick="javascript: recalcularPorcentajeMontoFacultativo(2)">
								<img src="/MidasWeb/img/calcIconAmount.jpg" style="border: none"
									title="Recalcular en base al monto a facultar" />
							</a>
						</td>
					</tr>
				</table>
			</td>			
       	</tr>
       	<tr>
       		<td align="right">
       	   		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.notaCobertura" />:	
       	   	</td>
       	   <td>
       	   		<midas:texto propiedadFormulario="notaCobejrtura" />
       	   	</td>
       	   	<td align="right" colspan="2">
       	   		<etiquetas:etiquetaError property="montoPrimaAdicional" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="contratoFacultativoForm" key="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional" />:
       	   	</td>
       	   <td>
       	   		<midas:texto propiedadFormulario="montoPrimaAdicional" onblur="validarMontoRegistrarEgreso(this)" />
       	   	</td>
       	</tr>
       <tr>
       	   <td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.nombreasegurado"/>:
           </td>
           <td colspan="4">
	   	 		<midas:texto id="nombreAsegurado" propiedadFormulario="nombreAsegurado" soloLectura="true" />
            </td>
       </tr>
       <tr>
       		<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ajustadorNombrado" />:	</td>
       		<td colspan="4"><midas:texto propiedadFormulario="ajustadorNombrado" caracteres="100" longitud="100"  /></td>
       	</tr>
       	<tr>
       		<td colspan="2"></td>
			<td align="right" style="font-weight: normal;font-size:10px" colspan="3">
				<dl>
					<dt>
						<i>&iquest;<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.requiereContratoReclamos" />?</i>
						<midas:checkBox valorEstablecido="1" propiedadFormulario="requiereControlReclamos" />
					</dt>
					<dt>
						<i>&iquest;<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.aplicarEndosoComoBonoNoSiniestro" />?</i>			
						<midas:checkBox valorEstablecido="1" propiedadFormulario="esBonoPorNoSiniestro" />
					</dt>
				</dl>
			</td>
		</tr>		
		<tr>
			<td colspan="4"></td>
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar">
						<a href="javascript: void(0);"
							onclick="javascript: sendModificarDetalleFacultativo(document.contratoFacultativoForm);">
							<midas:mensaje clave="midas.accion.guardar" />
						</a>
					</div>
				</div>
			</td>

		</tr>
   </table>
</midas:formulario>