<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<midas:formulario accion="/contratofacultativo/cobertura/mostrarDetalle">
<logic:present property="detalleContratoFacultativoAnterior" name="detalleContratoFacultativoForm" >
<c:if test="${not empty detalleContratoFacultativoForm.detalleContratoFacultativoAnterior.idTdContratoFacultativo}">
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Detalle Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="mostrarConfiguracionCobertura.jsp" flush="true"/>
	<hr/>
</c:if>
</logic:present>
	<html:hidden property="idTmContratoFacultativo" name="detalleContratoFacultativoForm" styleId="idTmContratoFacultativo"/>
	<html:hidden property="idTdContratoFacultativo" name="detalleContratoFacultativoForm" styleId="idTdContratoFacultativo"/>
	<center>
		<table id="desplegar" width="97%" style="font-weight:bold;">
			<!-- Datos de la configuracion de la cobertura -->
			<tr>
				<td class="titulo" colspan="5">
					<midas:mensaje
						clave="contratofacultativo.configuracion.cobertura.titulo" /> <font color="red">CANCELADA</font>
				</td>
			</tr>
			<tr>
				<td align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.seccion" />:
				</td>
				<td colspan="4">
					<html:textarea property="descripcionSeccion" readonly="true" rows="1" styleClass="cajaTexto" style="font-family:arial;font-size:12px" />
				</td>
			</tr>
			<tr>
				<td width="15%" align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.cobertura" />:
				</td>
				<td width="35%">
					<html:textarea property="descripcionCobertura" readonly="true" rows="2" styleClass="cajaTexto" style="font-family:arial;font-size:12px" />
				</td>
				<td width="5%"></td>
				<td width="20%" align="right">
					<midas:mensaje
						clave="contratofacultativo.configuracion.cobertura.numeroInciso" />:
				</td>
				<td width="25%">
					<midas:texto soloLectura="true" propiedadFormulario="numeroInciso" />
				</td>
			</tr>
			<tr>
				<td align="right">
					<midas:mensaje
						clave="contratofacultativo.configuracion.cobertura.sumaAsegurada" />:
				</td>
				<td>
					<midas:texto soloLectura="true" propiedadFormulario="sumaAsegurada" />
				</td>
				<td width="5%"></td>
				<td align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.deducible" />:
				</td>
				<td>
					<midas:texto propiedadFormulario="deducibleCobertura" id="deducibleCobertura" soloLectura="true" caracteres="16"/>
				</td>
			</tr>
			<tr>
				<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.porcentajeFacultada" />:</td>
				<td>
					<table width="100%">
						<tr>
							<td width="90%">
								<midas:texto propiedadFormulario="porcentajeFacultativo"
									id="porcentajeFacultativo" soloLectura="true" caracteres="16"
									onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
							</td>
							<td width="10%">
								
							</td>
						</tr>
					</table>
				</td>
				<td></td>
				<td align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaFacultada" />:
				</td>
				<td>
					<midas:texto propiedadFormulario="primaFacultadaCobertura" id="primaFacultadaCobertura" caracteres="16" soloLectura="true"/>
				</td>
			</tr>
			<tr>
				<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.montoAFacultar" />:</td>
				<td>
					<table width="100%">
						<tr>
							<td width="90%">
								<midas:texto propiedadFormulario="sumaFacultada"
									id="sumaFacultada" soloLectura="true"
									onblur="formatearMontosRegistrarCotizacionFacultativa()" />
							</td>
							<td width="10%">
								
							</td>
						</tr>
					</table>
				</td>
				<td></td>
				<td align="right">
					<midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaTotal" />:
				</td>
				<td>
					<midas:texto propiedadFormulario="primaTotalCobertura" id="primaTotalCobertura" caracteres="16" soloLectura="true"/>
				</td>
			</tr>
			<tr>
				<td align="right"><midas:mensaje clave="contratofacultativo.moneda"/>:</td>
				<td><midas:texto propiedadFormulario="moneda" nombreFormulario="detalleContratoFacultativoForm" soloLectura="true"/></td>
				<td></td>
				<td align="right" style="color:red"><midas:mensaje clave="contratofacultativo.primaNoDevengada"/>:</td>
				<td><midas:texto id="primaNoDevengada" propiedadFormulario="primaNoDevengada" nombreFormulario="detalleContratoFacultativoForm" soloLectura="false" onblur="validarMontoRegistrarEgreso(this)" /></td>
			</tr>
		</table>
		<div id="gridboxFacultativo" class="dataGridConfigurationClass"
			style="width: 100%"></div>
		<div id="gridboxFacultativoCorredor" class="dataGridConfigurationClass"
			style="width: 100%; display: none"></div>
		<table align="right">
			<tr>
				<td>
					<div id="botonMostrarFacultativoCorredor">
						<div id="b_regresar">
							<a href="javascript: void(0);"
								onclick="javascript: mostrarGridAgregarFacultativo();"><midas:mensaje clave="midas.accion.regresar" />
							</a>
						</div>
					</div>
				</td>
				<td>
				</td>
			</tr>
			<tr height="30px"></tr>
			<tr>
				<td>
				</td>
				<td>
					<div id="b_agregar">
						<a href="javascript: void(0);"
							onclick="javascript: guardarPrimaNoDevengadaFacultativo()"><midas:mensaje
								clave="midas.accion.guardar" />
						</a>
					</div>
				</td>
			</tr>
		</table>
	</center>
</midas:formulario>