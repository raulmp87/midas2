<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/contratofacultativo/cotizafacultativa/mostrarDetalle">

<logic:present property="contratoFacultativoAnterior" name="contratoFacultativoForm" >
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="../mostrarCotizacionFacultativa.jsp" flush="true"/>
	<hr/>
</logic:present>

	<table id="desplegar" style="font-weight:bold">
	    <midas:oculto propiedadFormulario="slipForm.idToSlip"/>
	    <midas:oculto propiedadFormulario="idTmContratoFacultativo"/>
	    <midas:oculto propiedadFormulario="subRamoForm.idSubRamo"/>
	    <midas:oculto propiedadFormulario="slipForm.idToCotizacion"/>
	    <midas:oculto propiedadFormulario="slipForm.tipoDistribucion"/>
	    
		<!-- Datos de la cotizacion -->
		<tr>
			<td class="titulo" colspan="4">Cotizaci&oacute;n Facultativa</td>
		</tr>
		<tr><td height="15px">&nbsp;</td></tr>
       <tr>
       	   <td align="right" width="20%"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ramo"/>:</td>
       	   <td width="30%" style="font-weight:normal"><midas:escribe propiedad="ramoForm.descripcion" nombre="contratoFacultativoForm" /></td>
       	   <td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAsegurada"/>:</td>
	       <td style="font-weight:normal"><midas:escribe propiedad="sumaAseguradaTotal" nombre="contratoFacultativoForm"/></td></tr>
       </tr>
       <tr>
	       <td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.subramo"/>:</td>
	       <td style="font-weight:normal"><midas:escribe propiedad="subRamoForm.descripcion" nombre="contratoFacultativoForm"/></td>
	       <td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAseguradaEndoso"/>:</td>
           	<td style="font-weight: normal"><midas:escribe nombre="contratoFacultativoForm" propiedad="sumaAseguradaEndoso"/></td>
       <tr>
       		<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaInicial" />:</td>
	   		<td><midas:texto propiedadFormulario="fechaInicial" soloLectura="true" /></td>
	   		<td align="right" width="20%"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.facultativo"/>:</td>
			<td width="30%"><midas:texto propiedadFormulario="porcentajeFacultativo"id="porcentajeFacultativo" soloLectura="true" caracteres="16" /></td>
       </tr>
       <tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaFinal" />:</td>
			<td><midas:texto  propiedadFormulario="fechaFinal" soloLectura="true"/></td>
	       	<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajeRetencion"/>:</td>
			<td><midas:texto propiedadFormulario="porcentajeRetencion" soloLectura="true" /></td>
       </tr>
       <tr>
       		<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.formaPago"/>:</td>
			<td>
				<midas:combo id="formaPago" propiedad="idFormaPago" styleClass="cajaTexto" deshabilitado="true">
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>
			</td>
			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajeCuotaParte" />:</td> 
			<td><midas:texto propiedadFormulario="porcentajeCuotaParte" soloLectura="true"/></td>
       </tr>
       <tr>
       	    <td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaalcien"/>:</td>
			<td ><midas:texto propiedadFormulario="primaCien" soloLectura="true"/></td>
			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajePrimerExcedente"/>:</td>
			<td colspan="1"><midas:texto propiedadFormulario="porcentajePrimerExcedente" soloLectura="true" /></td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primafacultativo" />:</td>
			<td><midas:texto propiedadFormulario="primaFac" soloLectura="true"/></td>
			<td align="right">Monto facultado:</td>
			<td><midas:texto propiedadFormulario="sumaFacultada" id="sumaFacultada" soloLectura="true" /></td>
       </tr>
       <tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.notaCobertura" />:</td>
			<td><midas:texto propiedadFormulario="notaCobejrtura" soloLectura="true" /></td>
       		<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional" />:</td>
			<td><midas:texto propiedadFormulario="montoPrimaAdicional" soloLectura="true" /></td>
       </tr>
       <tr>
       		<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.nombreasegurado"/>:</td>
	        <td colspan="3"><midas:texto propiedadFormulario="nombreAsegurado" soloLectura="true"/></td>
       	</tr>
       	<tr>
   			<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ajustadorNombrado" />:	</td>
   			<td colspan="3"><midas:texto propiedadFormulario="ajustadorNombrado" soloLectura="true" id="ajustadorNombrado" /></td>
   		</tr>
       <!--  -->
       	<tr>
			<td align="right">Estatus:</td>
			<td style="font-weight:normal"><i>Contrato Autorizado</i></td>
			<td></td>
			<td align="right" style="font-weight: normal; font-size: 10px">
				<i>&iquest;<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.requiereContratoReclamos" />?</i>
				<midas:checkBox valorEstablecido="1" propiedadFormulario="requiereControlReclamos" onClick="return false"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
       		<table class="tablaConResultados" width="100%" >
       		<tr><th colspan="2" onclick="if(document.getElementById('tablaTotales').style.display == 'none')document.getElementById('tablaTotales').style.display='block';else document.getElementById('tablaTotales').style.display='none';" >
       			<center><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.mostrarOcultarTotales"/></center></th></tr>
       		</table>
       		<table id="tablaTotales" class="tablaConResultados" style="display:none;" width="100%" >
       			<tr>
       				<td width="50%" >
			    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaalcien"/>:
		           </td>
		           <td align="right" width="200px">
			       		<midas:escribe propiedad="primaCien" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
       				<td>
       					<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional"/>:
		       	   	</td>
		       	   	<td align="right">
			       		<midas:escribe propiedad="montoPrimaAdicional" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
	       			<td>
			        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primafacultativo" />:
			       </td>
			       <td align="right">
			       		<midas:escribe propiedad="primaFac" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
	       			<td>
			        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.comisionTotal" />:
			       </td> 
			       <td align="right">
			       		<midas:escribe propiedad="montoComisionTotal" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
	       			<td>
			        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaReaseguro" />:
			       </td> 
			       <td align="right">
			       		<midas:escribe propiedad="montoTotalPrimaReaseguro" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       		</table>
       		</td>
		</tr>
		<tr>
		    <td colspan="4">
		    <div class="alinearBotonALaDerecha">
		      <div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript:agregarCotizacionFacultativa_nota('<midas:escribe propiedad="idTmContratoFacultativo" nombre="contratoFacultativoForm" />')"><midas:mensaje clave="midas.accion.guardar"/></a>
			  </div>
			</div>
			</td>
		</tr>
   </table>
</midas:formulario>