<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>&nbsp;&nbsp; &nbsp; 
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>



<midas:formulario accion="/contratofacultativo/participacionFacultativa/mostrarAgregar">
  	 	<midas:oculto propiedadFormulario="idTdContratoFacultativo"/>
  	 	<midas:oculto propiedadFormulario="idTmContratoFacultativo"/>
  	 	<center>
	  	 	<table id="agregar" width="97%">
				<tr>
					<td class="titulo" colspan="6">
						<midas:mensaje clave="midas.accion.agregarParticipacionFacultativa" />
					</td>
				</tr>
				<tr>
					<td width="15%">
						<etiquetas:etiquetaError   property="tipoParticipante" requerido="si"
							key="contratofacultativo.participacion.tipoParticipante"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:combo id="tipoParticipante" propiedad="tipoParticipante"  styleClass="cajaTexto" 
						onchange="limpiarObjetos('participante'); llenarCombo(this,'participante','/MidasWeb/reaseguradorCorredor.do');">	
							<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
							<midas:opcionCombo valor="0"><midas:mensaje clave="contratofacultativo.participacion.tipoparticipante.corredorDescripcion" /></midas:opcionCombo>
							<midas:opcionCombo valor="1"><midas:mensaje clave="contratofacultativo.participacion.tipoparticipante.reaseguradorDescripcion" /></midas:opcionCombo>						
						</midas:combo>
					</td>
					<td width="5%"></td>
					<td width="15%">
						<etiquetas:etiquetaError property="idParticipante"   requerido="si"
							key="contratofacultativo.participacion.participante"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</td>
					<td width="40%">
						<midas:reaseguradorCorredor styleId="participante" size="1"  tipo="tipoParticipante" propiedad="idParticipante" styleClass="cajaTexto"
						 onchange="limpiarObjetos('cuentaBancoPesos,cuentaBancoDolares,contacto'); llenarCombo(this,'cuentaBancoPesos','/MidasWeb/cuentaBancoPesos.do'); 
						 			llenarCombo(this,'cuentaBancoDolares','/MidasWeb/cuentaBancoDolares.do'); llenarCombo(this,'contacto','/MidasWeb/contacto.do');"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<etiquetas:etiquetaError   property="idCuentaPesos" requerido="si"
							key="contratofacultativo.participacion.cuentaPesos"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</td>
					<td>
						<midas:cuentaBanco moneda="pesos" styleClass="cajaTexto" reaseguradorCorredor="idParticipante" styleId="cuentaBancoPesos" propiedad="idCuentaPesos" size="1"/>
					</td>
					<td width="5%"></td>
					<td>
						<etiquetas:etiquetaError property="idContacto"   requerido="no"
							key="contratofacultativo.participacion.contacto" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td>
						<midas:contacto styleId="contacto" styleClass="cajaTexto" reaseguradorCorredor="idParticipante" size="1" propiedad="idContacto"/>
					</td>
				</tr>
				<tr>
					<td>
						<etiquetas:etiquetaError property="idCuentaDolares"  requerido="si"
							key="contratofacultativo.participacion.cuentaDolares"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</td>
					<td>				
						<midas:cuentaBanco moneda="dolares" styleClass="cajaTexto" reaseguradorCorredor="idParticipante" styleId="cuentaBancoDolares" propiedad="idCuentaDolares" size="1"/>				
					</td>
					<td width="5%"></td>
					<td align="right">
						<midas:mensaje clave="contratofacultativo.participacion.comision" />
					</td>
					<td>
						<midas:texto id="comision"  propiedadFormulario="comision" caracteres="15" onkeypress="return soloNumeros(this, event, true)"  />
					</td>
					<td>%</td>
				</tr>	
				<tr>
					<td>
						<etiquetas:etiquetaError property="porcentajeParticipacion"
							requerido="si"
							key="contratofacultativo.participacion.porcentajeParticipacion"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</td>
					<td>
						<midas:texto id="porcentajeParticipacion"  propiedadFormulario="porcentajeParticipacion" caracteres="15" onkeypress="return soloNumeros(this, event, true)"  />
					</td>
					<td width="5%">%</td>
					<td colspan="3"></td>
				</tr>		
				<tr>
					<td class="campoRequerido" colspan="4">
						<midas:mensaje
							clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
					</td>
					<td class="guardar">
						<div class="alinearBotonALaDerecha">
						 		<div id="b_regresar">
									<a href="javascript: void(0);"
										onclick="javascript: closeAgregarParticipacionFacultativa();"><midas:mensaje
											clave="midas.accion.regresar" />
									</a>
								</div>
								<div id="b_agregar">
									<a href="javascript: void(0);"
										onclick="javascript: agregarParticipacionFacultativa(document.participacionFacultativaForm,<midas:escribe propiedad="idTmContratoFacultativo" nombre="participacionFacultativaForm"/>,<midas:escribe propiedad="idTdContratoFacultativo" nombre="participacionFacultativaForm"/>)"><midas:mensaje
											clave="midas.accion.agregar" />
									</a>
								</div>							
					 	</div>
					</td>
				</tr>
			</table>
		</center>
</midas:formulario>