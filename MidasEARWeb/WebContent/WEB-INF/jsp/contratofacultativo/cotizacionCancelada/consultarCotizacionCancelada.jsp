<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/contratofacultativo/cotizafacultativa/mostrarDetalle">

<logic:present property="contratoFacultativoAnterior" name="contratoFacultativoForm" >
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="../mostrarCotizacionFacultativa.jsp" flush="true"/>
	<hr/>
</logic:present>

	<table id="desplegar">
	    <midas:oculto propiedadFormulario="slipForm.idToSlip"/>
	    <midas:oculto propiedadFormulario="idTmContratoFacultativo"/>
	    <midas:oculto propiedadFormulario="subRamoForm.idSubRamo"/>
	    <midas:oculto propiedadFormulario="slipForm.idToCotizacion"/>
	    <midas:oculto propiedadFormulario="slipForm.tipoDistribucion"/>
 	     <tr>
			<!-- Datos no editables del formulario Slip -->
			<td>
			</td>
		</tr>
		<!-- Datos de la cotizacion -->
		<tr><td class="titulo" colspan="4">
			  <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.titulo"/>
			</td>
		</tr>
       <tr><td><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ramo"/></td><td><midas:escribe propiedad="ramoForm.descripcion" nombre="contratoFacultativoForm" /></td>
       <td><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.facultativo"/></td><td><input type="text" value="0" disabled="disabled"/></td></tr>
       <tr><td><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.subramo"/></td><td><midas:escribe propiedad="subRamoForm.descripcion" nombre="contratoFacultativoForm"/></td>
       <td><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAsegurada"/></td><td><midas:escribe propiedad="sumaAseguradaTotal" nombre="contratoFacultativoForm"/></td></tr>
       <tr>
          <td>	
            <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaInicial" /> 
		  </td>
		<td>
		    <midas:texto propiedadFormulario="fechaInicial" deshabilitado="true" />
		 </td>
       <td><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.formaPago"/></td>
       <td>
       		<midas:combo id="formaPago" propiedad="idFormaPago" styleClass="cajaTexto" deshabilitado="true">	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
			</midas:combo>				
	    </td>
       </tr>
       <tr><td>
			 <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaFinal" />
           </td>
           <td>
               <midas:texto  propiedadFormulario="fechaFinal" deshabilitado="true"/>
           </td>
       <td><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.notaCobertura"/></td><td><midas:texto propiedadFormulario="notaCobejrtura" deshabilitado="true"/></td></tr>
       <tr>
         <td>
         <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.requiereContratoReclamos"/>
         </td>
            <td>
             <input type="checkbox" disabled="disabled" />
           </td>
         <td>Estatus</td>
         <td>
			Cancelado
	    </td> 
		</tr>
		<tr><td></td><td> </td>
       <td>
          <td class="guardar">
				<!--div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="width: 180px;" >
					 <a href="javascript: void(0);"onclick="javascript: autorizarContratoFacultativo(<midas:escribe propiedad="idTmContratoFacultativo" nombre="contratoFacultativoForm"/>);"><midas:mensaje clave="contratofacultativo.autorizar.contrato.accion"/></a>
					</div>
				</div-->
			</td> 
		</tr>
   </table>
    
</midas:formulario>