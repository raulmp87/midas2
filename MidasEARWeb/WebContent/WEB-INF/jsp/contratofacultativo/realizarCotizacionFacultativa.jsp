<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/contratofacultativo/agregarCotizacionFacultativa">

<logic:present property="contratoFacultativoAnterior" name="contratoFacultativoForm" >
	<div id="linkMostrarDetalle" >
		<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='block';document.getElementById('linkMostrarDetalle').style.display='none';if(document.getElementById('gridboxFacultativoEndosoAnterior').innerHTML == '')mostrarGridParticipacionFacultativoEndosoAnterior(document.getElementById('detalleContratoFacultativoAnterior.idTdContratoFacultativo').value);" style="cursor:pointer" >
			<tr><th>Mostrar Contrato Anterior</th></tr>
		</table>
	</div>
	<jsp:include page="mostrarCotizacionFacultativa.jsp" flush="true"/>
	<hr/>
</logic:present>

	<table id="desplegar" width="97%" style="font-weight:bold;">
	    <midas:oculto propiedadFormulario="slipForm.idToCotizacion"/>
	    <midas:oculto propiedadFormulario="slipForm.idTmLineaSoporteReaseguro"/>
	    <midas:oculto propiedadFormulario="slipForm.idToSoporteReaseguro"/>
	    <midas:oculto propiedadFormulario="sumaAseguradaTotal"/>
	    <midas:oculto propiedadFormulario="subRamoForm.descripcion"/>
	    <midas:oculto propiedadFormulario="ramoForm.descripcion"/>
	    
	    <html:hidden property="idTmContratoFacultativo" name="contratoFacultativoForm" styleId="idTmContratoFacultativo"/>
	    <html:hidden property="slipForm.idToSlip" name="contratoFacultativoForm" styleId="idToSlipCotizacion"/>
		<!-- Datos de la cotizacion -->
		<tr><td class="titulo" colspan="5">
			  <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.titulo"/>
			</td>
		</tr>
		<tr height="15px"><td colspan="5">&nbsp;</td></tr>
       <tr>
           <td width="15%" align="right" >
              <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ramo"/>:
           </td>
           <td width="35%" style="font-weight: normal">
              <midas:escribe propiedad="ramoForm.descripcion" nombre="contratoFacultativoForm"/>
           </td>
           <td width="5%"></td>
            <td width="15%" align="right">
                <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAsegurada"/>:
           </td>
           <td width="30%" style="font-weight: normal">
                <midas:escribe propiedad="sumaAseguradaTotal" nombre="contratoFacultativoForm"/>
            </td>
       </tr>
       <tr>
           <td align="right">
              <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.subramo"/>:
           </td>
           <td style="font-weight:normal" style="overflow:hidden;">
               <midas:escribe propiedad="subRamoForm.descripcion" nombre="contratoFacultativoForm"/>
           </td>
           <td></td>
           <td align="right">
           		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.sumaAseguradaEndoso"/>:
           </td>
           <td style="font-weight: normal">
           		<midas:escribe nombre="contratoFacultativoForm" propiedad="sumaAseguradaEndoso"/>
           </td>
       </tr>
        <tr>
       	 <td align="right">	
            <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaInicial" />:
		 </td>
		 <td>
		 	<midas:texto propiedadFormulario="fechaInicial" id="fechaInicial"/>
		 </td>
		 <td></td>
		 	<td align="right">
              <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.facultativo"/>:
           </td>
           <td>
           		<table width="100%">
					<tr>
						<td width="90%">
							<logic:notEmpty name="contrato">
								<midas:texto propiedadFormulario="porcentajeFacultativo"
									id="porcentajeFacultativo" soloLectura="false" caracteres="16"
									onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
							</logic:notEmpty>		
							<logic:empty name="contrato">
								<midas:texto propiedadFormulario="porcentajeFacultativo"
									id="porcentajeFacultativo" soloLectura="true" caracteres="16"/>
							</logic:empty>
						</td>
						<td width="10%">
							<logic:notEmpty name="contrato">
								<a href="javascript:void(0)"
									onclick="javascript: recalcularPorcentajeMontoFacultativo(1)"><img
										src="/MidasWeb/img/calcIconPercent.jpg" style="border: none"
										title="Recalcular en base al porcentaje" /> </a>
							</logic:notEmpty>
						</td>
					</tr>
				</table>
           </td> 
       </tr>
        <tr>
       		 <td align="right">
			 <midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.fechaFinal" />:
           </td>
           <td>
               <midas:texto propiedadFormulario="fechaFinal" id="fechaFinal"/>
           </td>
           <td></td>
       		<td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajeRetencion"/>:
           </td>
           <td>
	   	 		<midas:texto propiedadFormulario="porcentajeRetencion" soloLectura="true" />
            </td>
       </tr>
        <tr>
       		<td align="right">
       		<!-- 
	       	<etiquetas:etiquetaError property="idFormaPago" requerido="si"
					key="contratofacultativo.slip.registrar.cotizacion.formaPago" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			-->
	      </td>
	      <td>
	      <!-- 
	      	<logic:empty name="contrato">
	      		<midas:combo id="formaPago" propiedad="idFormaPago" styleClass="cajaTexto" deshabilitado="false">	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>	
	       	</logic:empty>
	       	<logic:notEmpty name="contrato">
	       		<midas:combo id="formaPago" propiedad="idFormaPago" styleClass="cajaTexto" deshabilitado="true">	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>	
	       	</logic:notEmpty>
	       	 -->
		   </td>
		    <td></td>
		    <td align="right">
	        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajeCuotaParte" />:			
	       </td> 
	       <td>    				
	            <midas:texto propiedadFormulario="porcentajeCuotaParte" soloLectura="true"/>       		
	       </td>	       
       </tr>
       <tr>
       		<td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaalcien"/>:
           </td>
           <td>
	   	 		<midas:texto id="primaCien" propiedadFormulario="primaCien" soloLectura="true" />
            </td>
            <td></td>
            
           <td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.porcentajePrimerExcedente"/>:
           </td>
           <td colspan="1">
	   	 		<midas:texto propiedadFormulario="porcentajePrimerExcedente" soloLectura="true" />
            </td>
       </tr>
       
       <tr>
       	   <td align="right">
	        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primafacultativo" />:			
	       </td> 
	       <td>
	       		<midas:texto id="primaFac" propiedadFormulario="primaFac" soloLectura="true" />	       		
	       </td>
	       <td></td> 
	       
	       <td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.montoAFacultar" />:</td>
			<td>
				<table width="100%">
					<tr>
						<td width="90%">
							<midas:texto propiedadFormulario="sumaFacultada"
								id="sumaFacultada" soloLectura="false"
								onblur="validarMontoRegistrarEgreso(this)" />
						</td>
						<td width="10%">
							<a href="javascript:void(0)"
								onclick="javascript: recalcularPorcentajeMontoFacultativo(2)">
								<img src="/MidasWeb/img/calcIconAmount.jpg" style="border: none"
									title="Recalcular en base al monto a facultar" />
							</a>
						</td>
					</tr>
				</table>
			</td>
       	</tr>
       	<tr>
       		<td align="right">
       	   		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.notaCobertura" />:	
       	   	</td>
       	   <td>
       	   		<midas:texto propiedadFormulario="notaCobejrtura" />
       	   	</td>
       	   	<td align="right" colspan="2">
       	   		<etiquetas:etiquetaError property="montoPrimaAdicional" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="contratoFacultativoForm" key="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional" />:
       	   	</td>
       	   <td>
       	   		<midas:texto propiedadFormulario="montoPrimaAdicional" onblur="validarMontoRegistrarEgreso(this)" />
       	   	</td>
       	</tr>
       	<tr>
       	   <td align="right">
	    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.nombreasegurado"/>:
           </td>
           <td colspan="4">
	   	 		<midas:texto id="nombreAsegurado" propiedadFormulario="nombreAsegurado" soloLectura="true" />
            </td>
       </tr>
       <tr>
       		<td align="right"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.ajustadorNombrado" />:	</td>
       		<td colspan="4"><midas:texto propiedadFormulario="ajustadorNombrado" caracteres="100" longitud="100"  /></td>
       	</tr>
       <!--  -->
        <tr>
       		<td colspan="2">
       		<table class="tablaConResultados" width="100%" >
       		<tr><th colspan="2" onclick="if(document.getElementById('tablaTotales').style.display == 'none')document.getElementById('tablaTotales').style.display='block';else document.getElementById('tablaTotales').style.display='none';" >
       			<center><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.mostrarOcultarTotales"/></center></th></tr>
       		</table>
       		<table id="tablaTotales" class="tablaConResultados" style="display:none;" width="100%" >
       			<tr>
       				<td width="50%" >
			    		<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaalcien"/>:
		           </td>
		           <td align="right" width="200px">
			       		<midas:escribe propiedad="montoTotalPrimaNeta" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
       				<td>
       					<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.montoPrimaAdicional"/>:
		       	   	</td>
		       	   	<td align="right">
			       		<midas:escribe propiedad="montoPrimaAdicional" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
	       			<td>
			        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primafacultativo" />:
			       </td>
			       <td align="right">
			       		<midas:escribe propiedad="primaFac" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
	       			<td>
			        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.comisionTotal" />:
			       </td> 
			       <td align="right">
			       		<midas:escribe propiedad="montoComisionTotal" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       			<tr>
	       			<td>
			        	<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.primaReaseguro" />:
			       </td> 
			       <td align="right">
			       		<midas:escribe propiedad="montoTotalPrimaReaseguro" nombre="contratoFacultativoForm"/>
			       </td>
       			</tr>
       		</table>
       		</td>
       		<td colspan="3" align="right" style="font-weight: normal;font-size:10px">
       			<dl>
       				<dt>
       					<i>&iquest;<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.requiereContratoReclamos"/>?</i>
       					<logic:empty name="contrato">
       						<midas:checkBox valorEstablecido="1" propiedadFormulario="requiereControlReclamos" deshabilitado="false"></midas:checkBox>
       					</logic:empty>
       					<logic:notEmpty name="contrato">
       						<midas:checkBox valorEstablecido="1" propiedadFormulario="requiereControlReclamos" onClick="return false" deshabilitado="false"></midas:checkBox>
       					</logic:notEmpty>
       				</dt>
       				<dt>
       					<i>&iquest;<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.aplicarEndosoComoBonoNoSiniestro" />?</i>
       					<logic:empty name="contrato">
       						<midas:checkBox valorEstablecido="1" propiedadFormulario="esBonoPorNoSiniestro" />
       					</logic:empty>
       					<logic:notEmpty name="contrato">
       						<midas:checkBox valorEstablecido="1" propiedadFormulario="esBonoPorNoSiniestro" onClick="return false" />
       					</logic:notEmpty>
       				</dt>       				
       			</dl>       			
       		</td>
       </tr>       
       <tr>
      	 	<td colspan="1"></td>
      	 	<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<logic:empty name="contrato">
					 	<div id="b_agregar" style="margin-right:10px;width:160px">
							<a href="javascript: void(0);" onclick="mostrarModificarPorcentajesContratos(<midas:escribe propiedad="slipForm.idToSlip" nombre="contratoFacultativoForm"/>)">
								<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.modificarPorcentajes" />
							</a>
						</div>
					</logic:empty>
					<logic:notEmpty name="contrato">
					 	<div id="b_regresar" style="margin-right:10px;">
							<a href="javascript: void(0);" onclick="javascript: sendRequest(null, '/MidasWeb/contratofacultativo/autorizarCotizacion.do?idToSlip=<midas:escribe propiedad="slipForm.idToSlip" nombre="contratoFacultativoForm"/>',null,'procesarRespuestaAutorizacionCotizFac(transport.responseXML)')"><midas:mensaje clave="midas.accion.autorizar"/></a>
						</div>
					</logic:notEmpty>
					<logic:empty name="contrato">
					   <div id="b_guardar" style="margin-right:10px">
						<a href="javascript: void(0);" onclick="javascript: agregarCotizacionFacultativa(document.contratoFacultativoForm);"><midas:mensaje clave="midas.accion.guardar"/></a>
					   </div>
					</logic:empty>
				    <logic:notEmpty name="contrato">
					   <div id="b_borrar" style="margin-right:10px">
						<a href="javascript: void(0);" onclick="javascript:sendEliminarFacultativo(document.contratoFacultativoForm);"><midas:mensaje clave="midas.accion.borrar"/></a>
					   </div>
					</logic:notEmpty>
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="slipForm.idToSlip" nombre="contratoFacultativoForm"/>,0,-1);">Ver Slip</a>
						<!-- mostrarModificarSlip(<midas:escribe propiedad="slipForm.idToSlip" nombre="contratoFacultativoForm"/>); onclick="javascript:mostrarSlipGeneral(<midas:escribe propiedad="slipForm.idToSlip" nombre="contratoFacultativoForm"/>,0,-1);">Ver Slip</a>  -->
					</div>
					<logic:notEmpty name="contrato">
					 	<div id="b_regresar" style="margin-right:10px;width:170px">
							<a href="javascript: void(0);" onclick="javascript: configurarPagosReaseguradores()"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.configurarPagosReaseguradores"/></a>
						</div>
					</logic:notEmpty>
			 	</div>
			</td> 
		</tr>
   </table>
</midas:formulario>