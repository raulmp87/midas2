<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<div id="datosDetalleContratoEndosoAnterior" style="display:none">
<div id="linkOcultarDetalle">
	<table id="t_riesgo" width="100%" onclick="document.getElementById('datosDetalleContratoEndosoAnterior').style.display='none';document.getElementById('linkMostrarDetalle').style.display='block';" style="cursor:pointer" >
		<tr><th>Ocultar Detalle Contrato Anterior</th></tr>
	</table>
</div>
<html:hidden property="detalleContratoFacultativoAnterior.idTmContratoFacultativo" name="detalleContratoFacultativoForm" styleId="detalleContratoFacultativoAnterior.idTmContratoFacultativo"/>
<html:hidden property="detalleContratoFacultativoAnterior.idTdContratoFacultativo" name="detalleContratoFacultativoForm" styleId="detalleContratoFacultativoAnterior.idTdContratoFacultativo"/>
<center>
	<table id="desplegar" width="97%" style="font-weight:bold;">
		<!-- Datos de la configuracion de la cobertura -->
		<tr>
			<td class="titulo" colspan="5"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.titulo.anterior" /></td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.seccion" />:</td>
			<td colspan="4">
				<bean:write property="detalleContratoFacultativoAnterior.descripcionSeccion" name="detalleContratoFacultativoForm" />
<%--				<html:textarea property="descripcionSeccion" readonly="true" rows="1" styleClass="cajaTexto" style="font-family:arial;font-size:12px" />--%>
			</td>
		</tr>
		<tr>
			<td width="15%" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.cobertura" />:</td>
			<td width="40%" colspan="2">
				<bean:write property="detalleContratoFacultativoAnterior.descripcionCobertura" name="detalleContratoFacultativoForm" />
			</td>
			<td width="20%" align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso" />:</td>
			<td width="25%">
				<bean:write property="detalleContratoFacultativoAnterior.numeroInciso" name="detalleContratoFacultativoForm" />
			</td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.sumaAsegurada" />:</td>
			<td colspan="2">
				<bean:write property="detalleContratoFacultativoAnterior.sumaAsegurada" name="detalleContratoFacultativoForm" />
			</td>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.deducible" />:</td>
			<td>
				<bean:write property="detalleContratoFacultativoAnterior.deducibleCobertura" name="detalleContratoFacultativoForm" />
			</td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.porcentajeFacultada" />:</td>
			<td>
				<bean:write property="detalleContratoFacultativoAnterior.porcentajeFacultativo" name="detalleContratoFacultativoForm" />
			</td>
			<td></td>
			<td align="right">
				<midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaFacultada" />:
			</td>
			<td>
				<bean:write property="detalleContratoFacultativoAnterior.primaFacultadaCobertura" name="detalleContratoFacultativoForm" />
			</td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.configuracion.cobertura.montoAFacultar" />:</td>
			<td>
				
							<bean:write property="detalleContratoFacultativoAnterior.sumaFacultada" name="detalleContratoFacultativoForm" />
						
			</td>
			<td></td>
			<td align="right">
				<midas:mensaje clave="contratofacultativo.configuracion.cobertura.primaTotal" />:
			</td>
			<td>
				<bean:write property="detalleContratoFacultativoAnterior.primaTotalCobertura" name="detalleContratoFacultativoForm" />
			</td>
		</tr>
		<tr>
			<td align="right"><midas:mensaje clave="contratofacultativo.moneda"/>:</td>
			<td><bean:write property="detalleContratoFacultativoAnterior.moneda" name="detalleContratoFacultativoForm" /></td>
			<td colspan="3"></td>
		</tr>
	</table>
	<div id="gridboxFacultativoEndosoAnterior" class="dataGridConfigurationClass" style="width: 100%"></div>
	
	<div id="gridboxFacultativoCorredorEndosoAnterior" class="dataGridConfigurationClass" style="width: 100%; display: none"></div>
	<table align="right">
			<tr>
				<td>
					<div id="botonMostrarFacultativoCorredorEndosoAnterior" style="display:none;">
						<div id="b_regresar">
							<a href="javascript: void(0);"
								onclick="javascript: mostrarGridAgregarFacultativoEndosoAnterior();"><midas:mensaje clave="midas.accion.regresar" />
							</a>
						</div>
					</div>
				</td>
			</tr>
		</table>
</center>
</div>
