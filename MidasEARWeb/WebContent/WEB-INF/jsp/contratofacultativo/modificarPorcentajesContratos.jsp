<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script> 
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>

<midas:formulario accion="/contratofacultativo/mostrarModificarPorcentajesContratos">
	<html:hidden property="idToSlip" styleId="idToSlip" />
	<table id="desplegar" width="97%" >
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.modificarPorcentajes"/>
			</td>
		</tr>
		<tr><td height="15%">&nbsp;</td></tr>
		<tr>
			<th width="23%">
				<p align="center"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.modificarporcentajes.retencion"/></p>
			</th>
			<th width="2%" style="border-right:2px solid #73D54A;">&nbsp;</th>
			<th>
				<p align="center"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.modificarporcentajes.cuotaParte"/>
			</th>
			<th width="2%" style="border-right:2px solid #73D54A;">&nbsp;</th>
			<th>
				<p align="center"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.modificarporcentajes.primerExcedente"/>
			</th>
			<th width="2%" style="border-right:2px solid #73D54A;">&nbsp;</th>
			<th>
				<p align="center"><midas:mensaje clave="contratofacultativo.slip.registrar.cotizacion.modificarporcentajes.facultativo"/>
			</th>
			<th width="2%" style="border-right:2px solid #73D54A;">&nbsp;</th>
		</tr>
		<tr>
			<td>
				<midas:texto propiedadFormulario="porcentajeRetencion" id="porcentajeRetencion" soloLectura="false" caracteres="16" onblur="if (!validarPorcentaje(this.value))this.value='0.00'"/>
			</td>
			<td style="border-right:2px solid #73D54A;"><b>%</b></td>
			<td>
				<midas:texto propiedadFormulario="porcentajeCuotaParte" id="porcentajeCuotaParte" soloLectura="false" caracteres="16" onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
			</td>
			<td style="border-right:2px solid #73D54A;"><b>%</b></td>
			<td>
				<midas:texto propiedadFormulario="porcentajePrimerExcedente" id="porcentajePrimerExcedente" soloLectura="false" caracteres="16" onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
			</td>
			<td style="border-right:2px solid #73D54A;"><b>%</b></td>
			<td>
				<midas:texto propiedadFormulario="porcentajeFacultativo" id="porcentajeFacultativo" soloLectura="false" caracteres="16" onblur="if (!validarPorcentaje(this.value))this.value='0.00'" />
			</td>
			<td style="border-right:2px solid #73D54A;"><b>%</b></td>
		</tr>
		<tr><td colspan="8" height="20px">&nbsp;</td></tr>
		<tr>
			<td colspan="4"></td>
			<td colspan="2" align="right">
				<div id="b_regresar" style="margin-right:10px;float:right">
					<a href="javascript: void(0);" onclick="cerrarVentanaModificarPorcentajes(0)">
						<midas:mensaje clave="midas.accion.cerrar" />
					</a>
				</div>
			</td>
			<td colspan="2">
				<div id="b_agregar" style="margin-right:10px;">
					<a href="javascript: void(0);" onclick="modificarPorcentajesContratosReaseguro()">
						<midas:mensaje clave="midas.accion.guardar" />
					</a>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>