<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:formulario  accion="/contratos/contratoprimerexcedente/listarFiltradoEnGrid">
	<midas:oculto propiedadFormulario="fechaInicial"/>
	<midas:oculto propiedadFormulario="fechaFinal" />
	<midas:oculto propiedadFormulario="idTcRamo"/>
	<midas:oculto propiedadFormulario="idTcSubRamo"/>
	<midas:oculto propiedadFormulario="idTmContratoCuotaParte"/>
	<midas:oculto propiedadFormulario="modoDistribucion"/>
	<midas:oculto propiedadFormulario="maximo"/>
	<midas:oculto propiedadFormulario="porcentajeDeCesion"/>
	
	<html:hidden property="folioContratoCuotaParte" name="contratoPrimerExcedenteForm" styleId="folioContratoCuotaParte" />
	<html:hidden property="folioContratoPrimerExcedente" name="contratoPrimerExcedenteForm" styleId="folioContratoPrimerExcedente" />
	<html:hidden property="participacionesCP" name="contratoPrimerExcedenteForm" styleId="participacionesCP" />
	<html:hidden property="participacionesPE" name="contratoPrimerExcedenteForm" styleId="participacionesPE" />
	<html:hidden property="seleccionado" name="contratoPrimerExcedenteForm" styleId="seleccionado" />
	<html:hidden property="reglaNavegacion" name="contratoPrimerExcedenteForm"/>
	<center>
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="contratos.contratoprimerexcedente.listarContratosPE"/>
			</td>
		</tr>		
		<tr>			
			<th><midas:mensaje clave="contratos.contratoprimerexcedente.fechaInicial"/>:</th>
			<td>
				<midas:texto id="fechaInicial" propiedadFormulario="fechaInicial" deshabilitado="true" />
			</td>
			<th><midas:mensaje clave="contratos.contratoprimerexcedente.fechaFinal"/>:</th>
			<td>
				<midas:texto id="fechaFinal" propiedadFormulario="fechaFinal"  deshabilitado="true" />
			</td>					
		</tr>				 				
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	</center>
	<div style="height:25px"></div>
	<div id="resultados">
		<center><div id="contratosPrimerExcedenteGrid" width="852px" height="250px" style="background-color:white;overflow:hidden;margin-left: auto; margin-right: auto;"></div></center>	
		<br/>
		<div class="alinearBotonALaDerecha">
			<table>
				<tr>
					<td>
						<div id="b_regresar">
							<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/mostrarAgregar.do', 'contenido', 'initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids(), initParticipacionesCPGrids_recargarTemporal(),mostrarParticipacionesCPGrids(),formatearMontosRegistrarLinea()');"><midas:mensaje clave="midas.accion.regresar"/></a>
						</div>	
					</td>
					<td>									
						<div id="b_seleccionar">
							<a href="javascript: void(0);" onclick="javascript: seleccionarContratoPrimerExcedente()"><midas:mensaje clave="midas.accion.seleccionar"/></a>
						</div>
					</td>
					<td>
						<div id="b_agregar">
							<a href="javascript: void(0);"
								onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarInicio.do', 'contenido', null);">
								<midas:mensaje clave="midas.accion.agregar"/>
							</a>
						</div>
					</td>
				</tr>
			</table>													
			</div>
	</div>	
</midas:formulario>
