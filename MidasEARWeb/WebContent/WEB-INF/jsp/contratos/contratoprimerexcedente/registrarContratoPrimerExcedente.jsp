<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div id="registrarContratoPrimerExcedente">

<midas:formulario  accion="/contratos/contratoprimerexcedente/guardarContrato">
	<html:hidden property="idTmContratoPrimerExcedente" name="contratoPrimerExcedenteForm" styleId="idTmContratoPrimerExcedente"/>
	<html:hidden property="folioContratoPrimerExcedente" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="fechaInicial" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="fechaFinal" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="estatus" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="reglaNavegacion" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="idTcMoneda" name="contratoPrimerExcedenteForm"/>
	
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.registrarContratoPrimerExcedente"/>		
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="folioContratoPrimerExcedente" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.folio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="folioContratoPrimerExcedente" nombre="contratoPrimerExcedenteForm"/>						
			</td>
			<td colspan="2"></td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="fechaInicial" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.fechaInicial" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaInicial" nombre="contratoPrimerExcedenteForm"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="fechaFinal" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.fechaFinal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaFinal" nombre="contratoPrimerExcedenteForm"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="limiteMaximo" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.limiteMaximo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="limiteMaximo" propiedadFormulario="limiteMaximo" caracteres="22" onkeypress="return soloNumeros(this, event, true)" deshabilitado="true" />
			</td>
			<td>
				<etiquetas:etiquetaError property="numeroPlenos" name="contratoPrimerExcedenteForm" requerido="si"
					key="contratos.contratoprimerexcedente.numeroPlenos" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="numeroPlenos" onblur="javascript: if(!validarNumeroMaximoYDecimales(this.value,22,0,true)) this.value=0; calcularLimiteMaximoContratoPE(this);" propiedadFormulario="numeroPlenos" caracteres="22" onkeypress="return soloNumeros(this, event, true)" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcMoneda" requerido="no"
					key="contratos.contratoprimerexcedente.idTcMoneda" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />		
			</td>
			<td>
				<midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="contratoPrimerExcedenteForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="true" />			
			</td>
			<td>
				<etiquetas:etiquetaError property="formaPago" requerido="si" name="contratoPrimerExcedenteForm"
					key="contratos.contratoprimerexcedente.formaPago" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:combo id="formaPago" propiedad="formaPago" styleClass="cajaTexto" >	
					<midas:opcionCombo valor="">SELECCIONE ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>				
			</td>	
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="montoPleno" name="contratoPrimerExcedenteForm" requerido="si"
					key="contratos.contratoprimerexcedente.montoPleno" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="montoPleno" onblur="javascript: if(!validarNumeroMaximoYDecimales(this.value,20,8,true)) this.value=0; calcularLimiteMaximoContratoPE(this);" propiedadFormulario="montoPleno" caracteres="20" onkeypress="return soloNumeros(this, event, true)" />
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td>
			<td class="continuar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarListar.do', 'contenido','mostrarContratosPrimerExcedenteGrids()');"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_continuar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/crearContrato.do', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');"><midas:mensaje clave="midas.accion.continuar"/></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div id="resultados" style="display:none">
		<div id="participacionesPEGrid" width="98%" height="250px" style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto;"></div>
		<br clear="all"/>		
		<div class="alinearBotonALaDerecha">
			<logic:empty property="reglaNavegacion" name="contratoPrimerExcedenteForm">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarListar.do', 'contenido','mostrarContratosPrimerExcedenteGrids()');"><midas:mensaje clave="midas.accion.regresar"/></a>
				</div>
			</logic:empty>
			<logic:notEmpty property="reglaNavegacion" name="contratoPrimerExcedenteForm">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/mostrarAgregar.do', 'contenido', 'initParticipacionesCPGrids_recargarTemporal(),mostrarParticipacionesCPGrids(),initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids()');">
	 					<midas:mensaje clave="midas.accion.regresar"/>
	 				</a>
				</div>
			</logic:notEmpty>
			<div id="b_agregar" style="margin-left:5px;margin-right:5px;">
				<a href="javascript: void(0);"
					onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/participacion/mostrarAgregar.do?idContratoPE=<midas:escribe propiedad="idTmContratoPrimerExcedente" nombre="contratoPrimerExcedenteForm"/>', 'contenido',null);">
					<midas:mensaje clave="midas.accion.agregar"/>
				</a>
			</div>
			<div id="b_guardar">
				<logic:empty property="reglaNavegacion" name="contratoPrimerExcedenteForm">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/guardarContratoFinal.do', 'contenido','mostrarContratosPrimerExcedenteGrids()');"><midas:mensaje clave="midas.accion.guardar" />
					</a>
				</logic:empty>
				<logic:notEmpty property="reglaNavegacion" name="contratoPrimerExcedenteForm">
					<logic:equal value="1" property="reglaNavegacion" name="contratoPrimerExcedenteForm">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/contratoprimerexcedente/guardarContratoFinal.do', 'contenido','initParticipacionesPEGrids(),mostrarParticipacionesPEGrids(), initParticipacionesCPGrids_recargarTemporal(), mostrarParticipacionesCPGrids()');"><midas:mensaje clave="midas.accion.guardar" />
						</a>
					</logic:equal>
				</logic:notEmpty>
			</div>
		</div>									
	</div>				

</midas:formulario>
</div>