<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div id="registrarContratoPrimerExcedente">

<midas:formulario  accion="/contratos/contratoprimerexcedente/mostrarListar">
	<html:hidden property="idTmContratoPrimerExcedente" name="contratoPrimerExcedenteForm" styleId="idTmContratoPrimerExcedente"/>
	<html:hidden property="folioContratoPrimerExcedente" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="fechaInicial" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="fechaFinal" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="estatus" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="reglaNavegacion" name="contratoPrimerExcedenteForm"/>
	<html:hidden property="idTcRamo" name="contratoPrimerExcedenteForm"/>
	
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.registrarContratoPrimerExcedente"/>		
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="folioContratoPrimerExcedente" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.folio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="folioContratoPrimerExcedente" nombre="contratoPrimerExcedenteForm"/>						
			</td>
			<td colspan="2"></td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="fechaInicial" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.fechaInicial" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaInicial" nombre="contratoPrimerExcedenteForm"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="fechaFinal" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.fechaFinal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaFinal" nombre="contratoPrimerExcedenteForm"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="limiteMaximo" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.limiteMaximo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="limiteMaximo" propiedadFormulario="limiteMaximo" caracteres="22" deshabilitado="true" />
			</td>
			<td>
				<etiquetas:etiquetaError property="numeroPlenos" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.numeroPlenos" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="numeroPlenos" deshabilitado="true" propiedadFormulario="numeroPlenos" caracteres="22" onkeypress="return soloNumeros(this, event, true)" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcMoneda" requerido="no"
					key="contratos.contratoprimerexcedente.idTcMoneda" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />		
			</td>
			<td>
				<midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="contratoPrimerExcedenteForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="true" />			
			</td>
			<td>
				<etiquetas:etiquetaError property="formaPago" requerido="no" name="contratoPrimerExcedenteForm"
					key="contratos.contratoprimerexcedente.formaPago" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:combo id="formaPago" propiedad="formaPago" styleClass="cajaTexto" deshabilitado="true" >	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratoprimerexcedente.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>				
			</td>	
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="montoPleno" name="contratoPrimerExcedenteForm" requerido="no"
					key="contratos.contratoprimerexcedente.montoPleno" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="montoPleno" deshabilitado="true" propiedadFormulario="montoPleno" caracteres="20" onkeypress="return soloNumeros(this, event, true)" />
			</td>
		</tr>
	</table>
	
	<div id="resultados">
		<center><div id="participacionesPEGrid_desplegar" width="98%" height="250px" style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto;"></div></center>		
		<br />
		<div class="alinearBotonALaDerecha">
			<logic:notEqual name="formularioOrigen" value="AgregarLinea">
			<logic:equal name="ventanaAccionada" value="2">
			<div id="b_modificar">
					<a href="javascript: void(0);"
						onclick="javascript: autorizarPE();">
						<midas:mensaje clave="contratos.contratoprimerexcedente.autorizar" />
					</a>
			</div>
			</logic:equal>
			</logic:notEqual>
			<div id="b_regresar">
				<logic:notEmpty name="formularioOrigen">				
					<logic:equal name="formularioOrigen" value="ListarContrato">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/mostrarAsignarPE.do', 'contenido','mostrarContratosPrimerExcedenteGrids()');">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</logic:equal>
					<logic:equal name="formularioOrigen" value="ListarLineas">
						<logic:notEmpty name="ventanaAccionada">
						<logic:equal name="ventanaAccionada" value="2">
							<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/listarLineaNegociacion.do', 'contenido','cargarComponentesLineaNegociacion()');">
							<midas:mensaje clave="midas.accion.regresar"/></a>
						</logic:equal>
						<logic:equal name="ventanaAccionada" value="1">
							<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/listarLineaContrato.do', 'contenido','cargarComponentesLineaContrato()');">
							<midas:mensaje clave="midas.accion.regresar"/></a>
						</logic:equal>
						<logic:equal name="ventanaAccionada" value="3">
							<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/listarLineaVigencia.do', 'contenido','cargarComponentesLineaVigencia()');">
							<midas:mensaje clave="midas.accion.regresar"/></a>
						</logic:equal>
						</logic:notEmpty>
					</logic:equal>
					<logic:equal name="formularioOrigen" value="AgregarLinea">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoPrimerExcedenteForm,'/MidasWeb/contratos/linea/mostrarAgregar.do', 'contenido', 'initParticipacionesCPGrids_recargarTemporal(),mostrarParticipacionesCPGrids(),initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids(),formatearMontosRegistrarLinea()');">
 							<midas:mensaje clave="midas.accion.regresar"/>
 						</a>
					</logic:equal>
				</logic:notEmpty>
			</div>
		</div>									
	</div>				

</midas:formulario>
</div>