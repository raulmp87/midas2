<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div id="desplegar">

<midas:formulario  accion="/contratos/contratocuotaparte/mostrarListar">
	<html:hidden property="idTmContratoCuotaParte" styleId="idTmContratoCuotaParte" name="contratoCuotaParteForm"/>
	<html:hidden property="folioContratoCuotaParte" name="contratoCuotaParteForm"/>
	<html:hidden property="fechaInicial" name="contratoCuotaParteForm"/>
	<html:hidden property="fechaFinal" name="contratoCuotaParteForm"/>
	<html:hidden property="estatus" name="contratoCuotaParteForm"/>
	<html:hidden property="reglaNavegacion" name="contratoCuotaParteForm"/>
	<html:hidden property="idTcRamo" name="contratoCuotaParteForm"/>
	
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="contratos.contratocuotaparte.listarContratoCuotaParte"/>				
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="folioContrato" name="contratoCuotaParteForm" requerido="no"
					key="contratos.contratocuotaparte.folio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="folioContratoCuotaParte" nombre="contratoCuotaParteForm"/>						
			</td>
			<td colspan="2"></td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="fechaInicial" name="contratoCuotaParteForm" requerido="no"
					key="contratos.contratocuotaparte.fechaInicial" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaInicial" nombre="contratoCuotaParteForm"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="fechaFinal" name="contratoCuotaParteForm" requerido="no"
					key="contratos.contratocuotaparte.fechaFinal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaFinal" nombre="contratoCuotaParteForm"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="porcentajeDeRetencion" name="contratoCuotaParteForm" requerido="si"
					key="contratos.contratocuotaparte.porcentajeRetencion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="porcentajeDeRetencion" propiedadFormulario="porcentajeDeRetencion" soloLectura="true" />%
			</td>
			<td>
				<etiquetas:etiquetaError property="porcentajeDeCesion" name="contratoCuotaParteForm" requerido="si"
					key="contratos.contratocuotaparte.porcentajeCesion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="porcentajeDeCesion" propiedadFormulario="porcentajeDeCesion" soloLectura="true" />%
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcMoneda" requerido="no"
					key="contratos.contratocuotaparte.idTcMoneda" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />					
			</td>
			<td>
				<midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="contratoCuotaParteForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="true" />				
			</td>
			<td>
				<etiquetas:etiquetaError property="formaPago" requerido="si" name="contratoCuotaParteForm"
					key="contratos.contratocuotaparte.formaPago" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:combo id="formaPago" propiedad="formaPago" styleClass="cajaTexto" deshabilitado="true">	
					<midas:opcionCombo valor="">Seleccione ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>				
			</td>			
		</tr>
	</table>
	
	<div id="resultados">
		<div id="participacionesCPGrid_desplegar" width="62%" height="250px" style="background-color:white;overflow:hidden;margin-left: auto; margin-right: auto;"></div>
		<div class="alinearBotonALaDerecha">
			<div id="b_modificar">
				<a href="javascript: void(0);"
					onclick="javascript: autorizarCP();">
					<midas:mensaje clave="contratos.contratocuotaparte.autorizar" />
				</a>
			</div>
		
			<div id="b_regresar">
				<logic:notEmpty name="formularioOrigen">				
					<logic:equal name="formularioOrigen" value="ListarContrato">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(contratoCuotaParteForm,'/MidasWeb/contratos/linea/mostrarAsignarCP.do', 'contenido','mostrarContratosCuotaParteGrids()');"><midas:mensaje clave="midas.accion.regresar" />
						</a>
					</logic:equal>
					<logic:equal name="formularioOrigen" value="ListarLineas">
						<a href="javascript: void(0);" 
						onclick="javascript: sendRequest(contratoCuotaParteForm,'/MidasWeb/contratos/linea/listarLineaNegociacion.do', 'contenido','cargarComponentesLineaNegociacion()');">
						<midas:mensaje clave="midas.accion.regresar"/></a>
					</logic:equal>
				</logic:notEmpty>
			</div>
		</div>									
	</div>				

</midas:formulario>
</div>