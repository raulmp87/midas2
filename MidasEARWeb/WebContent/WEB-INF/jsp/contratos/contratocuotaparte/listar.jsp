<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario  accion="/contratos/contratocuotaparte/listarFiltrado">
	<midas:oculto propiedadFormulario="idTcRamo"/>
	<midas:oculto propiedadFormulario="idTcSubRamo"/>
	<midas:oculto propiedadFormulario="fechaInicial"/>
	<midas:oculto propiedadFormulario="fechaFinal" />
	<midas:oculto propiedadFormulario="modoDistribucion"/>
	<midas:oculto propiedadFormulario="maximo"/>
	<midas:oculto propiedadFormulario="numeroPlenos"/>
	<midas:oculto propiedadFormulario="montoPleno"/>
	<midas:oculto propiedadFormulario="idTmContratoPrimerExcedente"/>
	<html:hidden property="participacionesCP" name="contratoCuotaParteForm" styleId="participacionesCP" />
	<html:hidden property="participacionesPE" name="contratoCuotaParteForm" styleId="participacionesPE" />
	<html:hidden property="seleccionado" name="contratoCuotaParteForm" styleId="seleccionado" />
	<html:hidden property="reglaNavegacion" name="contratoCuotaParteForm"/>

	<table width="98%">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="contratos.contratocuotaparte.listarContratosCP"/>
			</td>
		</tr>
	</table>
	<br/>		
	<table width="98%" id="filtros">
		<tr>
			<th><midas:mensaje clave="contratos.contratocuotaparte.fechaInicial"/>:</th>
			<td>
				<midas:texto id="fechaInicial" propiedadFormulario="fechaInicial" deshabilitado="true" />
			</td>
			<th><midas:mensaje clave="contratos.contratocuotaparte.fechaFinal"/>:</th>
			<td>
				<midas:texto id="fechaFinal" propiedadFormulario="fechaFinal"  deshabilitado="true" />
			</td>					
		</tr>				 				
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div style="height:25px"></div>
	<div id="resultados">
		<center><div id="contratosCuotaParteGrid" width="582px" height="250px" style="background-color:white;overflow:hidden;margin-left: auto; margin-right: auto;"></div></center>
		<br/>
		<div class="alinearBotonALaDerecha">
			<table>
				<tr>
					<td>					
						<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/linea/mostrarAgregar.do', 'contenido', 'initParticipacionesCPGrids_recargarTemporal(),mostrarParticipacionesCPGrids(),initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids(),formatearMontosRegistrarLinea()');"><midas:mensaje clave="midas.accion.regresar"/></a>
						</div>
					</td>
					<td>					
						<div id="b_seleccionar">
							<a href="javascript: void(0);" onclick="javascript: seleccionarContratoCuotaParte()"><midas:mensaje clave="midas.accion.seleccionar"/></a>
						</div>
					</td>
					<td>
						<div id="b_agregar">
							<a href="javascript: void(0);"
								onclick="javascript: sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/mostrarInicio.do', 'contenido', null);">
								<midas:mensaje clave="midas.accion.agregar"/>
							</a>
						</div>
					</td>
				</tr>
			</table>													
		</div>
			
	</div>
	
</midas:formulario>
