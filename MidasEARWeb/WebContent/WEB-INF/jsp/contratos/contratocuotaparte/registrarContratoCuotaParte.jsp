
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div id="registrarContratoCuotaParte">

<midas:formulario  accion="/contratos/contratocuotaparte/guardarContrato">
	<html:hidden property="idTmContratoCuotaParte" styleId="idTmContratoCuotaParte"/>
	<html:hidden property="folioContratoCuotaParte"/>
	<html:hidden property="fechaInicial"/>
	<html:hidden property="fechaFinal"/>
	<html:hidden property="estatus"/>
	<html:hidden property="reglaNavegacion"/>
	<html:hidden property="idTcMoneda"/>
	
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.registrarContratoCuotaParte"/>			
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="folioContrato" name="contratoCuotaParteForm" requerido="no"
					key="contratos.contratocuotaparte.folio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="folioContratoCuotaParte" nombre="contratoCuotaParteForm"/>	
			</td>
			<td colspan="2"></td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="fechaInicial" name="contratoCuotaParteForm" requerido="no"
					key="contratos.contratocuotaparte.fechaInicial" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaInicial" nombre="contratoCuotaParteForm"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="fechaFinal" name="contratoCuotaParteForm" requerido="no"
					key="contratos.contratocuotaparte.fechaFinal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribe propiedad="fechaFinal" nombre="contratoCuotaParteForm"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="porcentajeDeRetencion" name="contratoCuotaParteForm" requerido="si"
					key="contratos.contratocuotaparte.porcentajeRetencion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="porcentajeDeRetencion" propiedadFormulario="porcentajeDeRetencion" caracteres="5" longitud="6"
				onblur="validarPorcentaje(this.value)" onkeypress="return soloNumeros(this, event, true);" onchange="completaElCien(1);"/>%
			</td>
			<td>
				<etiquetas:etiquetaError property="porcentajeDeCesion" name="contratoCuotaParteForm" requerido="si"
					key="contratos.contratocuotaparte.porcentajeCesion" normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="porcentajeDeCesion" propiedadFormulario="porcentajeDeCesion" caracteres="5" longitud="6"
				onblur="validarPorcentaje(this.value)" onkeypress="return soloNumeros(this, event, true)" onchange="completaElCien(2);"/>%
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcMoneda" requerido="no"
					key="contratos.contratocuotaparte.idTcMoneda" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />	
			</td>
			<td>
				<midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="contratoCuotaParteForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="true" />				
			</td>
			<td>
				<etiquetas:etiquetaError property="formaPago" requerido="si" name="contratoCuotaParteForm"
					key="contratos.contratocuotaparte.formaPago" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:combo id="formaPago" propiedad="formaPago" styleClass="cajaTexto" >	
					<midas:opcionCombo valor="">SELECCIONE ...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.contratocuotaparte.formapago.mensualDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.contratocuotaparte.formapago.trimestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.contratocuotaparte.formapago.semestralDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.contratocuotaparte.formapago.anualDescripcion" /></midas:opcionCombo>
				</midas:combo>				
			</td>			
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
			<td class="continuar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/mostrarListar.do', 'contenido','mostrarContratosCuotaParteGrids()');"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_continuar">
						<a href="javascript: void(0);" onclick="javascript: if((parseFloat($('porcentajeDeCesion').value) + parseFloat($('porcentajeDeRetencion').value)) == 100) sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/crearContrato.do','contenido','mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()'); else alert('El porcentaje de Cesion y el de Retencion deben sumar 100');"><midas:mensaje clave="midas.accion.continuar"/></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
	
	<div id="resultados" style="display:none">
		<center><div id="participacionesCPGrid" width="98%" height="250px" style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto;"></div></center>
		<br/>
		<div class="alinearBotonALaDerecha">
			<table>
				<tr>
					<td>
						<logic:empty property="reglaNavegacion" name="contratoCuotaParteForm">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/mostrarListar.do', 'contenido','mostrarContratosCuotaParteGrids()');"><midas:mensaje clave="midas.accion.regresar"/></a>
							</div>
						</logic:empty>
						<logic:notEmpty property="reglaNavegacion" name="contratoCuotaParteForm">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/linea/mostrarAgregar.do', 'contenido', 'initParticipacionesCPGrids_recargarTemporal(),mostrarParticipacionesCPGrids(),initParticipacionesPEGrids_recargarTemporal(),mostrarParticipacionesPEGrids()');">
 									<midas:mensaje clave="midas.accion.regresar"/>
 								</a>
							</div>
						</logic:notEmpty>
						<div id="b_agregar">
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/participacion/mostrarAgregar.do?idContratoCP=<midas:escribe propiedad="idTmContratoCuotaParte" nombre="contratoCuotaParteForm" />', 'contenido',null);"><midas:mensaje clave="midas.accion.agregar"/>
								</a>
						</div>
					</td>
					<td>
						<div id="b_guardar">
								<logic:empty property="reglaNavegacion" name="contratoCuotaParteForm">
									<a href="javascript: void(0);"
										onclick="javascript: if((parseFloat($('porcentajeDeCesion').value) + parseFloat($('porcentajeDeRetencion').value)) == 100) sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/guardarContratoFinal.do',null,'regresarRegistrarContratoCuotaParte(0, transport.responseXML)'); else alert('El porcentaje de Cesion y el de Retencion deben sumar 100');"><midas:mensaje clave="midas.accion.guardar" /> </a>
								</logic:empty>
								<logic:notEmpty property="reglaNavegacion"
									name="contratoCuotaParteForm">
									<a href="javascript: void(0);"
										onclick="javascript: if((parseFloat($('porcentajeDeCesion').value) + parseFloat($('porcentajeDeRetencion').value)) == 100) sendRequest(document.contratoCuotaParteForm,'/MidasWeb/contratos/contratocuotaparte/guardarContratoFinal.do',null,'regresarRegistrarContratoCuotaParte(1, transport.responseXML)'); else alert('El porcentaje de Cesion y el de Retencion deben sumar 100');"><midas:mensaje clave="midas.accion.guardar" /> </a>
								</logic:notEmpty>
							</div>
					</td>
				</tr>
			</table>
		</div>									
	</div>				

</midas:formulario>
</div>
