<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/contratos/participacion/borrar">
	<midas:oculto propiedadFormulario="idTmContratoCuotaParte" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="idTmContratoPrimerExcedente" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="nombreCorredor" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="idTdParticipacion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="folioContratoCuotaParte" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeRetencion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeCesion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="idTcMoneda" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="formaPago" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="fechaInicial" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="fechaFinal" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="estatus" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="numeroPlenos" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="montoPleno" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="folioContratoPrimerExcedente" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="reglaNavegacion" nombreFormulario="participacionForm"/>
	
	<table id="desplegar">		
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrarParticipacion" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.participacion.tipoParticipante" />:</th>
			<td>
				<midas:combo id="tipoParticipante" propiedad="tipoParticipante" styleClass="cajaTexto" deshabilitado="true">	
					<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
					<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.participacion.tipoparticipante.corredorDescripcion" /></midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.participacion.tipoparticipante.reaseguradorDescripcion" /></midas:opcionCombo>						
				</midas:combo>
			</td>
			<th><midas:mensaje clave="contratos.participacion.participante" />:</th>
			<td>
				<midas:reaseguradorCorredor styleId="participante" size="1" tipo="tipoParticipante" propiedad="idParticipante" readonly="true"/>
			</td>					
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.participacion.cuentaPesos" />:</th>
			<td>
				<midas:cuentaBanco moneda="pesos" reaseguradorCorredor="idParticipante" styleId="cuentaBancoPesos" propiedad="idCuentaPesos" size="1" readonly="true"/>
			</td>
			<th><midas:mensaje clave="contratos.participacion.cuentaDolares" />:</th>
			<td>				
				<midas:cuentaBanco moneda="dolares" reaseguradorCorredor="idParticipante" styleId="cuentaBancoDolares" propiedad="idCuentaDolares" size="1" readonly="true"/>				
			</td>					
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.participacion.contacto" />:</th>
			<td>
				<midas:contacto styleId="contacto" reaseguradorCorredor="idParticipante" size="1" propiedad="idContacto" readonly="true"/>
			</td>
			<th><midas:mensaje clave="contratos.participacion.porcentajeParticipacion" />:</th>
			<td>
				<midas:escribe propiedad="porcentajeParticipacion" nombre="participacionForm"/>%				
			</td>					
		</tr> 	
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<logic:empty property="idTmContratoPrimerExcedente" name="participacionForm">
						<div id="b_regresar">
							<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.participacionForm,'/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');"><midas:mensaje clave="midas.accion.regresar"/></a>						
						</div>
						<div id="b_borrar">
							<a href="javascript: void(0);"											
								onclick="javascript: desplegarErrorBorrarReasegurador(2);">
								<midas:mensaje clave="midas.accion.borrar" /> </a>
							
						</div>
					</logic:empty>
					<logic:empty property="idTmContratoCuotaParte" name="participacionForm">
							<div id="b_regresar">
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.participacionForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');"><midas:mensaje
										clave="midas.accion.regresar" />
								</a>
							</div>
							<div id="b_borrar">
								<a href="javascript: void(0);"
									onclick="javascript: desplegarErrorBorrarReasegurador(1)"><midas:mensaje
										clave="midas.accion.borrar" />
								</a>
							</div>							
						</logic:empty>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>