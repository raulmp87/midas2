<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/participacion/agregar">
	<midas:oculto propiedadFormulario="idTmContratoCuotaParte" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="idTmContratoPrimerExcedente" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="nombreCorredor" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="idTdParticipacion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="folioContratoCuotaParte" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeRetencion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeCesion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="idTcMoneda" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="formaPago" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="fechaInicial" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="fechaFinal" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="estatus" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="numeroPlenos" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="montoPleno" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="folioContratoPrimerExcedente" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="reglaNavegacion" nombreFormulario="participacionForm"/>
	<midas:oculto propiedadFormulario="sumaTotalParticipacion" nombreFormulario="participacionForm"/>
	
	<html:hidden property="formaPago" styleId="formaPago" name="participacionForm"/>
		<table id="agregar">
			<tr>
				<td class="titulo" colspan="5">
					<midas:mensaje clave="midas.accion.agregarParticipacion" />
				</td>
			</tr>
			<tr>
				<td width="10%">
					<etiquetas:etiquetaError property="tipoParticipante" requerido="si"
						key="contratos.participacion.tipoParticipante"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</td>
				<td width="20%">
					<midas:combo id="tipoParticipante" propiedad="tipoParticipante" styleClass="cajaTexto" 
					onchange="limpiarObjetos('participante'); llenarCombo(this,'participante','/MidasWeb/reaseguradorCorredor.do');">	
						<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
						<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.participacion.tipoparticipante.corredorDescripcion" /></midas:opcionCombo>
						<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.participacion.tipoparticipante.reaseguradorDescripcion" /></midas:opcionCombo>						
					</midas:combo>
				</td>
				<td width="2%"></td>
				<td width="10%">
					<etiquetas:etiquetaError property="idParticipante" requerido="si"
						key="contratos.participacion.participante"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</td>
				<td width="58%">
					<midas:reaseguradorCorredor styleId="participante" size="1" tipo="tipoParticipante" propiedad="idParticipante" styleClass="cajaTexto"
					 onchange="limpiarObjetos('cuentaBancoPesos,cuentaBancoDolares,contacto'); llenarCombo(this,'cuentaBancoPesos','/MidasWeb/cuentaBancoPesos.do'); 
					 			llenarCombo(this,'cuentaBancoDolares','/MidasWeb/cuentaBancoDolares.do'); llenarCombo(this,'contacto','/MidasWeb/contacto.do');"/>
				</td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="idCuentaPesos" requerido="si"
						key="contratos.participacion.cuentaPesos"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</td>
				<td>
					<midas:cuentaBanco moneda="pesos" styleClass="cajaTexto" reaseguradorCorredor="idParticipante" styleId="cuentaBancoPesos" propiedad="idCuentaPesos" size="1"/>
				</td>
				<td></td>
				<td>
					<etiquetas:etiquetaError property="idCuentaDolares" requerido="si"
						key="contratos.participacion.cuentaDolares"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</td>
				<td>				
					<midas:cuentaBanco moneda="dolares" reaseguradorCorredor="idParticipante" styleClass="cajaTexto" styleId="cuentaBancoDolares" propiedad="idCuentaDolares" size="1"/>				
				</td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="porcentajeParticipacion"
						requerido="si"
						key="contratos.participacion.porcentajeParticipacion"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</td>
				<td>
					<midas:texto id="porcentajeParticipacion" propiedadFormulario="porcentajeParticipacion" caracteres="5" onkeypress="return soloNumeros(this, event, true)"  />
				</td>
				<td>%</td>
				<td>
					<etiquetas:etiquetaError property="idContacto" requerido="no"
						key="contratos.participacion.contacto" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td>
					<midas:contacto styleId="contacto" reaseguradorCorredor="idParticipante" size="1" propiedad="idContacto" styleClass="cajaTexto"/>
				</td>
			</tr>			
			<tr>
				<td class="campoRequerido" colspan="4">
					<midas:mensaje
						clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
				</td>
				<td class="guardar">
					<div class="alinearBotonALaDerecha">
						
						<logic:empty property="idTmContratoPrimerExcedente" name="participacionForm">
							<div id="b_regresar">
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.participacionForm,'/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do?id=<midas:escribe propiedad="idTmContratoCuotaParte" nombre="participacionForm"/>', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');"><midas:mensaje
										clave="midas.accion.regresar" />
								</a>
							</div>
							<div id="b_agregar">
							<a href="javascript: void(0);"
								onclick="agregarParticipacionCuotaParte()"><midas:mensaje
									clave="midas.accion.agregar" />
							</a>
						</div>
						</logic:empty>
						<logic:empty property="idTmContratoCuotaParte" name="participacionForm">
							<div id="b_regresar">
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.participacionForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do?id=<midas:escribe propiedad="idTmContratoPrimerExcedente" nombre="participacionForm"/>', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');"><midas:mensaje
										clave="midas.accion.regresar" />
								</a>
							</div>
							<div id="b_agregar">
								<a href="javascript: void(0);"
									onclick="agregarParticipacionPrimerExcedente()"><midas:mensaje
										clave="midas.accion.agregar" />
								</a>
							</div>							
						</logic:empty>
					</div>
				</td>
			</tr>
		</table>
</midas:formulario>