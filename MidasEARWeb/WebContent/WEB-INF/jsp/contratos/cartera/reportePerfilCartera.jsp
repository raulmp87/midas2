<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/cartera/reportePerfilCartera">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo">
				<midas:mensaje clave="contratos.cartera.generarPerfil"/>
			</td>
		</tr>
		<tr>
			<td>
				<table style="font-size:10px" width="50%">
					<tr>
						<td align="right" width="25%">
							<b><midas:mensaje clave="contratos.movimiento.reporte.fecha.inicial" />:</b>
						</td>
						<td align="left" width="30%">
							<midas:texto propiedadFormulario="fechaInicial"  id="fechaInicial" soloLectura="true"/>
						</td>
						<td width="45%" align="left">
							<div id="b_calendario">
								<a href="#" id="mostrarCalendario"
									onclick="javascript: mostrarCalendarioDobleRptPerfilCartera()"></a>
							</div>
						</td>
					</tr>
					<tr>
						<td align="right" width="25%">
							<b><midas:mensaje clave="contratos.movimiento.reporte.fecha.final" />:</b>
						</td>
						<td align="left" width="30%">
							<midas:texto propiedadFormulario="fechaFinal"  id="fechaFinal" soloLectura="true"/>
						</td>
						<td width="45%" align="left">
							<div id="rangoDeFechas"
								style="position: absolute; z-index: 100; overflow: visible;">
								<div id="calendarioIzq"></div>
								<div id="calendarioDer"></div>
							</div>
						</td>
					</tr>
					<tr>
						<td width="15%" align="right">
							<b><midas:mensaje clave="contratos.cartera.tipoCambio"/>:</b>
						</td>
						<td width="30%">
							<midas:texto propiedadFormulario="tipoCambio" id="tipoCambio" longitud="30" nombreFormulario="reportePerfilCarteraForm" onkeypress="return soloNumeros(this, event, true);"/>
						</td>
						<td width="55%" colspan="2"></td>
					</tr>
					<tr>
						<td width="15%" align="right">
							<b>Tipo de reporte:</b>
						</td>
						<td width="30%">
							<html:select property="generarPerfil" styleClass="cajaTexto" styleId="tipoReportePerfilCatera" >
								<html:option value="1">Perfiles de Cartera/ Riesgos</html:option>
								<html:option value="2">Perfiles de Cartera/ Riesgos/ Total de siniestros</html:option>
							</html:select>
						</td>
						<td width="55%" colspan="2"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="97%">
		<tr>
			<td>
				<table width="100%" style="font-size:10px">
					<tr width="100%">
						<td colspan="4">
							<center>
								<div id="listarReportePerfilCartera" width="502px" height="250px"
									style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto;"></div>
							</center>
						</td>
					</tr>
					<tr>
						<td colspan="2" width="60%"></td>
						<td width="20%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: editarPerfilCartera();" tipo="agregar" style="margin-right:10px" />
							</div>
						</td>
						<td width="20%"><midas:boton onclick="javascript: borrarPerfilCarteraGrid();" tipo="borrar" texto="Eliminar" /></td>
					</tr>
					<tr height="20px"></tr>		
					<tr>
						<td colspan="3">
<%--							<div class="alinearBotonALaDerecha">--%>
<%--								<div id="b_reportePDF" style="width: 160px;margin-right:15px;">--%>
<%--									<a href="javascript: void(0);"--%>
<%--										onclick="javascript: validarMostrarRptPerfilCartera('PDF');"><midas:mensaje clave="reaseguro.reporte.generarreporte.pdf" />--%>
<%--									</a>--%>
<%--								</div>--%>
<%--							</div>--%>
						</td>
						<td style="margin-right:50px;">
							<div id="b_reporteXLS" style="width: 160px;margin-right:15px;">
								<a href="javascript: void(0);"
									onclick="javascript: validarMostrarRptPerfilCartera('XLS')"><midas:mensaje clave="reaseguro.reporte.generarreporte.xls" />
								</a>
							</div>
						</td>    
					</tr>
				</table>
			</td>
		</tr>
	</table>
</midas:formulario>