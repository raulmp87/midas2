<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/movimiento/reporteMovimiento">
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		id="desplegar">
		<tr>
			<td>
				<midas:mensaje clave="contratos.movimiento.tipoReporte"/>
				<select id="movimientos" size="1"
					onchange="javascript:ocultarReporteMovimientos(this.options[this.selectedIndex].value);"
					onkeyup="javascript:ocultarReporteMovimientos(this.options[this.selectedIndex].value);">
					<option value="1">
						Reporte De Movimientos de Reaseguro de Emisi&oacute;n por Contrato
					</option>
					<option value="2">
						Reporte De Movimientos de Reaseguro de Emisi&oacute;n por Reasegurador
					</option>
					<option value="3">
						Reporte De Movimientos de Reaseguro de Siniestros por Reasegurador
					</option>
					<option value="4">
						Reporte Para Comparaci&oacute;n de Primas de Emisi&oacute;n
					</option>
					<option value="5">
						Reporte Para Comparacion de Primas de Distribuci&oacute;n
					</option>
					<option value="6">
						Reporte Trimestral De Reaseguro
					</option>
				</select>
			</td>
		</tr>
	</table>
			<div id="movimientoMoneda" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		id="desplegar">

		<tr>
			<td width="50%">
				<midas:mensaje clave="contratos.movimiento.mostrarMovimiento" />
				<midas:combo propiedad="movimientos">
					<midas:opcionCombo valor="1">Movimiento 1</midas:opcionCombo>
					<midas:opcionCombo valor="2">Movimiento 2</midas:opcionCombo>
				</midas:combo>
			</td>
			<td width="45%">
				<midas:mensaje clave="contratos.movimiento.moneda" />
				<midas:combo propiedad="moneda">
					<midas:opcionCombo valor="1">Pesos</midas:opcionCombo>
					<midas:opcionCombo valor="2">D&oacute;lares</midas:opcionCombo>
				</midas:combo>
			</td>
		
		</tr>
		</table>
		</div>
		<div id="seleccionarPeriodo" style="display: none;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				id="desplegar">
				<tr>

					<td width="90%">
						<midas:mensaje clave="contratos.movimiento.seleccionaPeriodo" />
						<midas:combo propiedad="periodo" />
					</td>

				</tr>

			</table>
		</div>
		<tr>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				id="desplegar">
				<tr>
					<td width="30%">
						<midas:mensaje clave="contratos.movimiento.fechaInicial" />
					</td>
					<td width="20%">
						<midas:mensaje clave="contratos.movimiento.dia" />
						<midas:combo propiedad="diaI">
							<midas:opcionCombo valor="1">1</midas:opcionCombo>
							<midas:opcionCombo valor="2">2</midas:opcionCombo>
							<midas:opcionCombo valor="3">3</midas:opcionCombo>
							<midas:opcionCombo valor="4">4</midas:opcionCombo>
							<midas:opcionCombo valor="5">5</midas:opcionCombo>
							<midas:opcionCombo valor="6">6</midas:opcionCombo>
							<midas:opcionCombo valor="7">7</midas:opcionCombo>
							<midas:opcionCombo valor="8">8</midas:opcionCombo>
							<midas:opcionCombo valor="9">9</midas:opcionCombo>
							<midas:opcionCombo valor="10">10</midas:opcionCombo>
							<midas:opcionCombo valor="11">11</midas:opcionCombo>
							<midas:opcionCombo valor="12">12</midas:opcionCombo>
							<midas:opcionCombo valor="13">13</midas:opcionCombo>
							<midas:opcionCombo valor="14">14</midas:opcionCombo>
							<midas:opcionCombo valor="15">15</midas:opcionCombo>
							<midas:opcionCombo valor="16">16</midas:opcionCombo>
							<midas:opcionCombo valor="17">17</midas:opcionCombo>
							<midas:opcionCombo valor="18">18</midas:opcionCombo>
							<midas:opcionCombo valor="19">19</midas:opcionCombo>
							<midas:opcionCombo valor="20">20</midas:opcionCombo>
							<midas:opcionCombo valor="21">21</midas:opcionCombo>
							<midas:opcionCombo valor="22">22</midas:opcionCombo>
							<midas:opcionCombo valor="23">23</midas:opcionCombo>
							<midas:opcionCombo valor="24">24</midas:opcionCombo>
							<midas:opcionCombo valor="25">25</midas:opcionCombo>
							<midas:opcionCombo valor="26">26</midas:opcionCombo>
							<midas:opcionCombo valor="27">27</midas:opcionCombo>
							<midas:opcionCombo valor="28">28</midas:opcionCombo>
							<midas:opcionCombo valor="29">29</midas:opcionCombo>
							<midas:opcionCombo valor="30">30</midas:opcionCombo>
							<midas:opcionCombo valor="31">31</midas:opcionCombo>
						</midas:combo>
					</td>
					<td width="20%">
						<midas:mensaje clave="contratos.movimiento.mes" />
						<midas:combo propiedad="mesI">
							<midas:opcionCombo valor="1">1</midas:opcionCombo>
							<midas:opcionCombo valor="2">2</midas:opcionCombo>
							<midas:opcionCombo valor="3">3</midas:opcionCombo>
							<midas:opcionCombo valor="4">4</midas:opcionCombo>
							<midas:opcionCombo valor="5">5</midas:opcionCombo>
							<midas:opcionCombo valor="6">6</midas:opcionCombo>
							<midas:opcionCombo valor="7">7</midas:opcionCombo>
							<midas:opcionCombo valor="8">8</midas:opcionCombo>
							<midas:opcionCombo valor="9">9</midas:opcionCombo>
							<midas:opcionCombo valor="10">10</midas:opcionCombo>
							<midas:opcionCombo valor="11">11</midas:opcionCombo>
							<midas:opcionCombo valor="12">12</midas:opcionCombo>
						</midas:combo>
					</td>
					<td width="20%">
						<midas:mensaje clave="contratos.movimiento.ano" />
						<midas:combo propiedad="anoI">
							<midas:opcionCombo valor="1">2007</midas:opcionCombo>
							<midas:opcionCombo valor="2">2008</midas:opcionCombo>
							<midas:opcionCombo valor="3">2009</midas:opcionCombo>
						</midas:combo>
					</td>
				</tr>
				<tr>
					<td width="30%">
						<midas:mensaje clave="contratos.movimiento.fechaFinal" />
					</td>
					<td width="20%">
						<midas:mensaje clave="contratos.movimiento.dia" />
						<midas:combo propiedad="diaF">
							<midas:opcionCombo valor="1">1</midas:opcionCombo>
							<midas:opcionCombo valor="2">2</midas:opcionCombo>
							<midas:opcionCombo valor="3">3</midas:opcionCombo>
							<midas:opcionCombo valor="4">4</midas:opcionCombo>
							<midas:opcionCombo valor="5">5</midas:opcionCombo>
							<midas:opcionCombo valor="6">6</midas:opcionCombo>
							<midas:opcionCombo valor="7">7</midas:opcionCombo>
							<midas:opcionCombo valor="8">8</midas:opcionCombo>
							<midas:opcionCombo valor="9">9</midas:opcionCombo>
							<midas:opcionCombo valor="10">10</midas:opcionCombo>
							<midas:opcionCombo valor="11">11</midas:opcionCombo>
							<midas:opcionCombo valor="12">12</midas:opcionCombo>
							<midas:opcionCombo valor="13">13</midas:opcionCombo>
							<midas:opcionCombo valor="14">14</midas:opcionCombo>
							<midas:opcionCombo valor="15">15</midas:opcionCombo>
							<midas:opcionCombo valor="16">16</midas:opcionCombo>
							<midas:opcionCombo valor="17">17</midas:opcionCombo>
							<midas:opcionCombo valor="18">18</midas:opcionCombo>
							<midas:opcionCombo valor="19">19</midas:opcionCombo>
							<midas:opcionCombo valor="20">20</midas:opcionCombo>
							<midas:opcionCombo valor="21">21</midas:opcionCombo>
							<midas:opcionCombo valor="22">22</midas:opcionCombo>
							<midas:opcionCombo valor="23">23</midas:opcionCombo>
							<midas:opcionCombo valor="24">24</midas:opcionCombo>
							<midas:opcionCombo valor="25">25</midas:opcionCombo>
							<midas:opcionCombo valor="26">26</midas:opcionCombo>
							<midas:opcionCombo valor="27">27</midas:opcionCombo>
							<midas:opcionCombo valor="28">28</midas:opcionCombo>
							<midas:opcionCombo valor="29">29</midas:opcionCombo>
							<midas:opcionCombo valor="30">30</midas:opcionCombo>
							<midas:opcionCombo valor="31">31</midas:opcionCombo>
						</midas:combo>
					</td>
					<td width="20%">
						<midas:mensaje clave="contratos.movimiento.mes" />
						<midas:combo propiedad="mesF">
							<midas:opcionCombo valor="1">1</midas:opcionCombo>
							<midas:opcionCombo valor="2">2</midas:opcionCombo>
							<midas:opcionCombo valor="3">3</midas:opcionCombo>
							<midas:opcionCombo valor="4">4</midas:opcionCombo>
							<midas:opcionCombo valor="5">5</midas:opcionCombo>
							<midas:opcionCombo valor="6">6</midas:opcionCombo>
							<midas:opcionCombo valor="7">7</midas:opcionCombo>
							<midas:opcionCombo valor="8">8</midas:opcionCombo>
							<midas:opcionCombo valor="9">9</midas:opcionCombo>
							<midas:opcionCombo valor="10">10</midas:opcionCombo>
							<midas:opcionCombo valor="11">11</midas:opcionCombo>
							<midas:opcionCombo valor="12">12</midas:opcionCombo>
						</midas:combo>
					</td>
					<td width="20%">
						<midas:mensaje clave="contratos.movimiento.ano" />
						<midas:combo propiedad="anoF">
							<midas:opcionCombo valor="1">2007</midas:opcionCombo>
							<midas:opcionCombo valor="2">2008</midas:opcionCombo>
							<midas:opcionCombo valor="3">2009</midas:opcionCombo>
						</midas:combo>
					</td>
				</tr>
			</table>
		</tr>
		<div id="monedaCambio" style="display: none;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
				id="desplegar">
			<tr>				
				<td width="40%">
					<midas:mensaje clave="contratos.movimiento.moneda"/>
					<midas:combo propiedad="moneda">
							<midas:opcionCombo valor="1">Pesos</midas:opcionCombo>
					<midas:opcionCombo valor="2">D&oacute;lares</midas:opcionCombo>
					</midas:combo>
				</td>
			</tr>
			<tr>
				<td width="40%">
					<midas:mensaje clave="contratos.movimiento.tipoCambio" />
					<midas:texto propiedadFormulario="tipoCambio" longitud="30"/>
				</td>
				<td width="60%">
				&nbsp;
				</td>
			</tr>
			</table>
		</div>

		<tr>
			<td colspan="4" >
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: void(0);" tipo="consultar" texto="Generar Reporte"/>
				</div>
			</td>     
		</tr>
	</table>
	
</midas:formulario>