<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/ingresos/mostrarRegistrarIngresosReaseguro">
	<center>
		<table style="width:97%;overflow:scroll;table-layout:auto;">
	     	<tr>
		     	<td class="titulo" colspan="3">
					<midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.registrar"/>
				</td>
	     	</tr>
	     	<tr height="15px"><td>&nbsp;</td></tr>
	    </table>
		<div width="98%">
			<div id="registrarIGrid" width="767px" height="350px"
				style="background-color: white;"></div>
		</div>
		<br clear="all" />
		<table width="98%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="70%"></td>
				<td width="10%">
					<midas:boton onclick="javascript: registrarIngresos();"
						tipo="agregar" texto="Registrar" />
				</td>
				<td width="10%">
					<midas:boton onclick="javascript: borrarIngresoReaseguro();"
						tipo="borrar" texto="Eliminar" />
				</td>
				<td width="10%">
					<midas:boton
						onclick="javascript: modificarRegistrarIngresosReaseguro();"
						tipo="buscar" texto="Modificar" />
				</td>
			</tr>
		</table>
	</center>
</midas:formulario>
