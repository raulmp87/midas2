<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
	</head>
	<body>
		<midas:formulario accion="/contratos/ingresos/registrarIngresosReaseguro">
			<html:hidden property="idIngresoReaseguro" styleId="idIngresoReaseguro" />
			<table width="100%" style="font-size:9px;font-weight:bold" cellspacing="5px">
				<tr>
		        	<td width="30%" align="right">
		        		<div id="agregarRI1" style="display: none;">
		        			*<midas:mensaje clave="contratos.ingresos.montoIngreso"/>:
		        		</div>
		        	</td>
		       		<td width="70%" colspan="2">
						<div id="agregarRI2" style="display: none;">
							<midas:texto id="montoIngreso" soloLectura="false" propiedadFormulario="montoIngreso" longitud="30" onblur="this.value=formatCurrency4Decs(this.value)" />
						</div>
					</td>
		        </tr>
		        <tr>
		        	<td align="right">
		        		<div id="agregarRI3" style="display: none;">
		        			*<midas:mensaje clave="contratos.ingresos.moneda"/>:
		       			</div>
		       		</td>
		       		<td colspan="2">
		       			<div id="agregarRI4" style="display: none;">
		       				<html:select property="moneda" styleClass="cajaTexto" styleId="moneda">
			     				<html:option value="">Seleccione...</html:option>
			     				<html:optionsCollection property="monedaDTOList" label="descripcion" value="idTcMoneda" />
			     			</html:select>
						</div>
					</td>
				</tr>
		        <tr>
		        	<td colspan="3">
		        		<div id="modificarRI" style="display: none;"></div>
		        	</td>
		        <tr>
		        	<td align="right">
		        		<div id="modificarRI1" style="display: none;">
		        			*<midas:mensaje clave="contratos.ingresos.montoIngreso"/>:
		        		</div>
		        	</td>
		       		<td colspan="2">
		       			<div id="modificarRI2" style="display: none;">
		       				<midas:texto id="montoIngresoM" soloLectura="true" propiedadFormulario="montoIngreso" longitud="30"/>
		       			</div>
		       		</td>
		        </tr>
		        <tr>
		        	<td align="right">
		        		<div id="modificarRI3" style="display: none;">
		        			*<midas:mensaje clave="contratos.ingresos.moneda"/>:
		        		</div>
		        	</td>
		       		<td colspan="2">
		       			<div id="modificarRI4" style="display: none;">
		       				<html:select property="moneda" styleClass="cajaTexto" styleId="monedaM" disabled="true">
			     				<html:option value="">Seleccione...</html:option>
			     				<html:optionsCollection property="monedaDTOList" label="descripcion" value="idTcMoneda" />
			     			</html:select>
						</div>
					</td>
				</tr>
				<tr>
					<td align="right">
						<midas:mensaje clave="contratos.ingresos.fechaIngreso" />:
					</td>
					<td>
						<table width="100%">
							<tr>
								<td width="60%"><midas:texto soloLectura="true" id="fechaIngreso"
									propiedadFormulario="fechaIngreso" longitud="30" />
								</td>
								<td width="40%">
									<div id="b_calendario">
										<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioSimpleRegistrarIngresoReaseguro()"></a>
									</div>
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
					</td>
				</tr>
		        <tr>
					<td align="right">
						<midas:mensaje clave="contratos.ingresos.referencia" />:
					</td>
					<td colspan="2">
						<midas:texto id="referencia" propiedadFormulario="referencia" />
					</td>
				</tr>
		        <tr>
					<td align="right">
						<midas:mensaje clave="contratos.ingresos.numeroCuenta" />:
					</td>
					<td colspan="2">
						<midas:texto id="numeroCuenta" propiedadFormulario="numeroCuenta" longitud="30" />
					</td>
					<td></td>
				</tr>
				<tr height="20px">
					<td>&nbsp;</td>
				</tr>
		       	<tr>
					<td colspan="2" align="right">
						<midas:mensaje clave="contratos.ingresos.notificarReaseguro" />
					</td>
					<td><midas:checkBox valorEstablecido="1" id="notificarReaseguro" propiedadFormulario="notificarReaseguro" /></td>
				</tr>
		       	<tr>
					<td colspan="2" align="right">
						<midas:mensaje clave="contratos.ingresos.notificarSiniestros" />
					</td>
					<td><midas:checkBox valorEstablecido="1" id="notificarSiniestro" propiedadFormulario="notificarSiniestro" /></td>
				</tr>
				<tr height="15px">
					<td>&nbsp;</td>
				</tr>
		       	<tr>
		       		<td></td>
		       		<td align="right">
		       			<div id="agregarRIb" style="display: none;float:right;">
		       				<midas:boton onclick="javascript: verificarContratoRegistrarIngresosReaseguro(1)" tipo="agregar" texto="Aceptar"/>
		       			</div>
		       			<div id="modificarRIb" style="display: none;float:right;">
		       				<midas:boton onclick="javascript: verificarContratoRegistrarIngresosReaseguro(2)" tipo="agregar" texto="Aceptar"/>
		       			</div>
		       		</td>
		       		<td>
		       			<div id="agregarRIb2" style="display: none;">
		       				<midas:boton onclick="javascript: parent.dhxWins.window('Ingreso').close();" tipo="regresar" texto="Cancelar"/>
		       			</div>
		       			<div id="modificarRIb2" style="display: none;">
		       				<midas:boton onclick="javascript: parent.dhxWins.window('ModificarIngreso').close();" tipo="regresar" texto="Cancelar"/>
		       			</div>
		       		</td>
		       	</tr>
			</table>
		</midas:formulario>
</body>
</html>