<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario  accion="/contratos/participacioncorredor/listarReaseguradores">	
	<midas:oculto propiedadFormulario="idTmContratoCuotaParte" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="idTmContratoPrimerExcedente" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="nombreCorredor" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeParticipacionRestante" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="idTdParticipacion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="folioContratoCuotaParte" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeRetencion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeCesion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="idTcMoneda" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="formaPago" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="fechaInicial" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="fechaFinal" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="estatus" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="numeroPlenos" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="montoPleno" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="folioContratoPrimerExcedente" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="reglaNavegacion" nombreFormulario="participacionCorredorForm"/>
	
	<table width="100%" id="filtros">
		<tr>
			<td class="titulo" colspan="2">
				<midas:mensaje clave="midas.accion.listarParticipacionCorredor"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.corredor"/>:</th>
			<td><midas:escribe nombre="participacionCorredorForm" propiedad="nombreCorredor" /></td>			
		</tr> 				
	</table>
	<br/>
	<div id="resultados">
	
		<div id="reaseguradoresPorCorredorGrid" width="730px" height="250px" style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto;"></div>
		<br/>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<logic:empty name="participacionCorredorForm" property="idTmContratoPrimerExcedente">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do?id=<midas:escribe propiedad="idTmContratoCuotaParte" nombre="participacionCorredorForm"/>', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoCPGrids_registrarCP(),mostrarParticipantesCPGrids_registrarCP()');"><midas:mensaje
							clave="midas.accion.regresar" />
						</a>
					</logic:empty>
					<logic:empty name="participacionCorredorForm" property="idTmContratoCuotaParte">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do?id=<midas:escribe propiedad="idTmContratoCuotaParte" nombre="participacionCorredorForm"/>', 'contenido','mostrarDivParticipaciones(),initParticipantesContratoPEGrids_registrarPE(),mostrarParticipantesPEGrids_registrarPE(),calcularLimiteMaximoContratoPE()');"><midas:mensaje
							clave="midas.accion.regresar" />
						</a>
					</logic:empty>
				</div>
				
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>