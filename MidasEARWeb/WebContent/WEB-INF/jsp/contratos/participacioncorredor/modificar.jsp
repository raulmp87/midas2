<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/participacioncorredor/modificar">
	<midas:oculto propiedadFormulario="idTdParticipacionCorredor" nombreFormulario="participacionCorredorForm" />
	<midas:oculto propiedadFormulario="idTdParticipacion" nombreFormulario="participacionCorredorForm" />	
	<midas:oculto propiedadFormulario="idTmContratoCuotaParte" nombreFormulario="participacionCorredorForm" />
	<midas:oculto propiedadFormulario="idTmContratoPrimerExcedente" nombreFormulario="participacionCorredorForm" />
	<midas:oculto propiedadFormulario="nombreCorredor" nombreFormulario="participacionCorredorForm" />
	<midas:oculto propiedadFormulario="folioContratoCuotaParte" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeRetencion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeCesion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="idTcMoneda" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="formaPago" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="fechaInicial" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="fechaFinal" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="estatus" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="numeroPlenos" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="montoPleno" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="folioContratoPrimerExcedente" nombreFormulario="participacionCorredorForm"/>
	<html:hidden property="porcentajeParticipacionRestante" styleId="porcentajeParticipacionRestante" name="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="reglaNavegacion" nombreFormulario="participacionCorredorForm"/>
	
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="2">
				<midas:mensaje clave="midas.accion.modificarParticipacionCorredor" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="nombreCorredor" requerido="no"
					key="contratos.participacioncorredor.nombreCorredor" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="nombreCorredor" deshabilitado="true" />
			</td>				
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcReaseguradorCorredor" requerido="si"
				key="contratos.participacioncorredor.reasegurador" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:comboCatalogo idCatalogo="idtcreaseguradorcorredor" styleId="reasegurador" propiedad="idTcReaseguradorCorredor" descripcionCatalogo="nombre" nombreCatalogo="tcreasegurador" size="1" styleClass="cajaTexto" />				
			</td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="porcentajeParticipacion" requerido="si"
					key="contratos.participacioncorredor.porcentajeParticipacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="porcentajeParticipacion" propiedadFormulario="porcentajeParticipacion"
					caracteres="5" onblur="if (!validarPorcentaje(this.value)) this.value='0'" onkeypress="return soloNumeros(this, event, true)" />%
			</td>
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do?id=<midas:escribe propiedad="idTdParticipacion" nombre="participacionCorredorForm"/>', 'contenido','initReaseguradoresPorCorredor(<midas:escribe propiedad="idTdParticipacion" nombre="participacionCorredorForm"/>),mostrarReaseguradoresPorCorredorGrids()');"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: if(validarPorcentaje($('porcentajeParticipacion').value)) if($('porcentajeParticipacion').value > 0) if (parseFloat($('porcentajeParticipacion').value) <= parseFloat($('porcentajeParticipacionRestante').value)){sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/modificar.do', 'contenido','initReaseguradoresPorCorredor(<midas:escribe propiedad="idTdParticipacion" nombre="participacionCorredorForm"/>),mostrarReaseguradoresPorCorredorGrids()');}else alert('El porcentaje del reasegurador no debe ser mayor a <midas:escribe propiedad="porcentajeParticipacionRestante" nombre="participacionCorredorForm"/>% (porcentaje restante del corredor)');else alert('El porcentaje debe ser mayor a cero');"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>