<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/participacioncorredor/borrar">
	<midas:oculto propiedadFormulario="idTdParticipacionCorredor" nombreFormulario="participacionCorredorForm" />
	<midas:oculto propiedadFormulario="idTdParticipacion" nombreFormulario="participacionCorredorForm" />	
	<midas:oculto propiedadFormulario="idTmContratoCuotaParte" nombreFormulario="participacionCorredorForm" />
	<midas:oculto propiedadFormulario="idTmContratoPrimerExcedente" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="nombreCorredor" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="folioContratoCuotaParte" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeRetencion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="porcentajeDeCesion" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="idTcMoneda" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="formaPago" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="fechaInicial" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="fechaFinal" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="estatus" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="numeroPlenos" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="montoPleno" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="folioContratoPrimerExcedente" nombreFormulario="participacionCorredorForm"/>
	<midas:oculto propiedadFormulario="reglaNavegacion" nombreFormulario="participacionCorredorForm"/>
	
	<table id="desplegar">		
		<tr>
			<td class="titulo" colspan="2">
				<midas:mensaje clave="midas.accion.borrarParticipacionCorredor" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.participacioncorredor.nombreCorredor" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="nombreCorredor" nombre="participacionCorredorForm"/></td>								
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.participacioncorredor.reasegurador" />:</th>
			<td>
				<midas:comboCatalogo idCatalogo="idtcreaseguradorcorredor" styleId="reasegurador" propiedad="idTcReaseguradorCorredor" descripcionCatalogo="nombre" nombreCatalogo="tcreasegurador" size="1" styleClass="cajaTexto" readonly="true"/>				
			</td>								
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.participacioncorredor.porcentajeParticipacion" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="porcentajeParticipacion" nombre="participacionCorredorForm"/>%</td>								
		</tr> 	
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do?id=<midas:escribe propiedad="idTdParticipacion" nombre="participacionCorredorForm"/>', 'contenido','initReaseguradoresPorCorredor(<midas:escribe propiedad="idTdParticipacion" nombre="participacionCorredorForm"/>),mostrarReaseguradoresPorCorredorGrids()');"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);"											
							onclick="javascript: Confirma('<midas:mensaje clave="comun.confirmacionborrar" />', document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/borrar.do', 'contenido','initReaseguradoresPorCorredor(<midas:escribe propiedad="idTdParticipacion" nombre="participacionCorredorForm"/>),mostrarReaseguradoresPorCorredorGrids()');">
							<midas:mensaje clave="midas.accion.borrar" /> </a>						
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>