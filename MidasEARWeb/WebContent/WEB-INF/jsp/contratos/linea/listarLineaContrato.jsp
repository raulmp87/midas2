<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/contratos/linea/listarLineaContrato">
	<html:hidden property="estatus" value="1" styleId="estatus" />
	<html:hidden property="seleccionado" styleId="seleccionado"/>
	
	<table width="98%">
		<tr>
			<td class="titulo">
				<midas:mensaje clave="contratos.linea.contrato.titulo"/>
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">
		<tr>
			<td align="right"><b><midas:mensaje clave="contratos.contratocuotaparte.fechaInicial"/>:</b></td>
			<td>
				<midas:texto id="fechaInicial" propiedadFormulario="fechaInicial" deshabilitado="false" caracteres="10" onblur="esFechaValida(this);"/>
			</td>
			<td><input type="text" class="cajaTexto" id="horaInicial" maxlength="5" onblur="javascript: validaHoraInicialLineas()" value='<bean:write name="horaInicial"/>'/></td>
			<td width="5%">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td align="right"><b><midas:mensaje clave="contratos.contratocuotaparte.fechaFinal"/>:</b></td>
			<td>
				<midas:texto id="fechaFinal" propiedadFormulario="fechaFinal" deshabilitado="false" caracteres="10" onblur="esFechaValida(this);"/>
			</td> 
			<td><input type="text" class="cajaTexto" id="horaFinal" maxlength="5" onblur="javascript: validaHoraFinalLineas()" value='<bean:write name="horaFinal"/>'/></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td colspan="3"></td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="idTcRamo" requerido="si" name="configuracionLineaForm" normalClass="normal" errorClass="error" 
				key="catalogos.subramo.idRamo"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="2">			
				<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto" />
			</td>
			<td colspan="3"></td>
			<td class= "buscar">
				<div class="alinearBotonALaDerecha">

					<div id="b_buscar">							 
						<a href="javascript: void(0);"																														  	
						onclick="javascript: mostrarLineaContratoGrids();">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      							
		</tr>
			
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<br clear="all"/>
		<div id="lineaContratoParteGrid" width="98%" height="300px" style="background-color:white;overflow:auto;right-margin:auto;left-margin:auto;"></div>	
	<br clear="all"/>
	<div class="alinearBotonALaDerecha">		
		<div id="b_agregar" style="width:100px;margin-right:10px;">
			<a href="javascript: void(0);"
				onclick="javascript: rptLineasNegociacion(2,'PDF');">
				<midas:mensaje clave="midas.accion.reportePDF"/>
			</a>
		</div>
		<div id="b_agregar" style="width:100px;margin-right:10px;">
			<a href="javascript: void(0);"
				onclick="javascript: rptLineasNegociacion(2,'XLS');">
				<midas:mensaje clave="midas.accion.reporteExcel"/>
			</a>
		</div>	
		<div id="b_modificar" style="margin-right:10px;">
			<a href="javascript: void(0);"
				onclick="javascript: desautorizarLinea();">
				<midas:mensaje clave="midas.accion.modificar"/>
			</a>
		</div>															
	</div>
</midas:formulario>