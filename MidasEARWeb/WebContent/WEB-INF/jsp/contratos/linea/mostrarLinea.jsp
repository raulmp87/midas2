<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/contratos/linea/mostrarLinea">
		<html:hidden property="idTmLinea" styleId="idTmLinea" />
	<html:hidden property="idTcRamo" styleId="idTcRamo" />
	<html:hidden property="fechaInicial" styleId="fechaInicial" />
	<html:hidden property="fechaFinal" styleId="fechaFinal" />
	<html:hidden property="estatus" styleId="estatus" />
	<html:hidden property="participacionesCP" styleId="participacionesCP" />
	<html:hidden property="participacionesPE" styleId="participacionesPE" />
	<html:hidden property="idTmContratoCuotaParte" styleId="idTmContratoCuotaParte"/>
	<html:hidden property="idTmContratoPrimerExcedente" styleId="idTmContratoPrimerExcedente" />
	<html:hidden property="mensajeAutorizar" styleId="mensajeAutorizar" />
	<html:hidden property="estatusContratoCuotaParte" styleId="estatusContratoCuotaParte" />
	<html:hidden property="estatusContratoPrimerExcedente" styleId="estatusContratoPrimerExcedente" />
	<table id="agregar" width="97%">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogo.lineas.linea.consultar" />
			</td>
		</tr>
		<tr>
			<td width="15%">
				<midas:mensaje clave="contratos.linea.idTcSubRamo"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" property="ramo" styleClass="cajaTexto"/>  
			</td>
			<td width="15%"></td>
			<td></td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.modoDistribucion"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" property="modoDistribucion" styleClass="cajaTexto"/>
			</td>
			<td width="15%"></td>
			<td></td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.porcentajeCesion"/>:	
			</td>
			<td>			
				<html:text readonly="true" name="configuracionLineaDTO" property="porcentajeDeCesion" styleClass="cajaTexto"/>
			
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.cesionPrimerExcedente"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" styleId="cesionPrimerExcedente" property="cesionPrimerExcedente" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.cesionCuotaParte"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" styleId="cesionCuotaParte" property="cesionCuotaParte" styleClass="cajaTexto"/>
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.capacidadMaxima"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" property="capacidadMaximaLinea" styleId="capacidadMaximaLinea" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.montoPleno"/>:
			</td>
			<td>					
				<html:text readonly="true" name="configuracionLineaDTO" property="montoDelPleno" styleId="montoPleno" styleClass="cajaTexto"/>
			</td>	
			<td>
				<midas:mensaje clave="contratos.linea.totalRetencion"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" styleId="totalRetencion" property="totalRetencion" styleClass="cajaTexto"/>
				
			</td>				
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.numeroPlenos"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" property="numeroDePlenos" styleClass="cajaTexto"/>
			</td>	
			<td>
				<midas:mensaje clave="contratos.linea.totalCedido"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" styleId="totalCedido" property="totalCedido" styleClass="cajaTexto"/>
			</td>				
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.maximo"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" styleId="maximo" property="maximo" styleClass="cajaTexto"/>
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.estatus"/>:
			</td>
			<td>
				<html:text readonly="true" name="configuracionLineaDTO" property="estatus" styleClass="cajaTexto"/>
			</td>

		</tr>
		<tr>
			<table style="margin-left: auto; margin-right: auto">
				<tr>
					<td>
						<table>
							<tr>
								<th class="seccion">
									Participaciones Contrato Cuota Parte
								</th>
							</tr>
							<tr>
								<td>
									<div id="participacionesCPGrid" width="402px" height="120px"
										style="background-color: white; overflow: hidden"></div>
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<th class="seccion" colspan="6">
									Participaciones Contrato Primer Excedente
								</th>
							</tr>
							<tr>
								<td>
									<div id="participacionesPEGrid" width="402px" height="120px"
										style="background-color: white; overflow: hidden; font-size: 8px"></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</tr>
		<tr>
			<td colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<logic:notEmpty name="formularioOrigen">				
							<logic:equal name="formularioOrigen" value="Contrato">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(configuracionLineaForm,'/MidasWeb/contratos/linea/listarLineaContrato.do', 'contenido', 'cargarComponentesLineaContrato()');">
									<midas:mensaje clave="midas.accion.regresar"/>
								</a>
							</logic:equal>
							<logic:equal name="formularioOrigen" value="Vigencia">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(configuracionLineaForm,'/MidasWeb/contratos/linea/listarLineaVigencia.do', 'contenido', 'cargarComponentesLineaVigencia()');">
									<midas:mensaje clave="midas.accion.regresar"/>
								</a>
							</logic:equal>
						</logic:notEmpty>

					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>