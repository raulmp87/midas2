<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<midas:formulario accion="/contratos/linea/mostrarExtenderVigenciaLinea">	
	<html:hidden property="idTmLinea" styleId="idTmLinea" />
	<html:hidden property="estatus" styleId="estatus" />
	<html:hidden property="seleccionado" styleId="seleccionado" />
	
	<table id="agregar" width="98%">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogo.lineas.extender.linea" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.contratocuotaparte.fechaInicial"/>:</th>
			<td>
				<input readonly="true" type="text"  class="cajaTexto" id="fechaInicial" maxlength="10" value='<bean:write name="fechaInicial"/>' onblur="esFechaValida(this);"/> 
			</td>
			<td><input readonly="true" type="text" class="cajaTexto" id="horaInicial" maxlength="5" onblur="javascript: validaHoraInicialLineas()" value='<bean:write name="horaInicial"/>'/></td>					
			<td>
				<midas:mensaje clave="contratos.linea.porcentajeCesion"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeDeCesion" deshabilitado="true" />
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.cesionPrimerExcedente" />
			</td>
			<td>
				<midas:texto propiedadFormulario="cesionPrimerExcedente" deshabilitado="true" />
			</td>			
		</tr>
		<tr>
			<th><midas:mensaje clave="contratos.contratocuotaparte.fechaFinal"/>:</th>
			<td>
				<input type="text" class="cajaTexto" id="fechaFinal" maxlength="10" value='<bean:write name="fechaFinal"/>' onblur="validarFechaExtenderVigenciaEscrito(this)"/>
			</td> 
			<td>
				<input type="text" class="cajaTexto" id="horaFinal" maxlength="5" onblur="javascript: validaHoraFinalLineas()" value='<bean:write name="horaFinal"/>'/>
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.cesionCuotaParte"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="cesionCuotaParte"  deshabilitado="true" />
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.capacidadMaxima"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="capacidadMaximaLinea"  deshabilitado="true" />
			</td>			
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.idTcSubRamo"/>
			</td>
			<td>
				<midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" readonly="true"/>
			</td>
			<td></td>		
			<td>
				<midas:mensaje clave="contratos.linea.montoPleno"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoPleno" deshabilitado="true" />
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.totalRetencion"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="totalRetencion" deshabilitado="true" />
			</td>			
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="contratos.linea.tipoDistribucion"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="modoDistribucion" deshabilitado="true" />
			</td>
			<td></td>	
			<td>
				<midas:mensaje clave="contratos.linea.numeroPlenos"/>				
			</td>
			<td>
				<midas:texto propiedadFormulario="numeroPlenos" deshabilitado="true" />
			</td>
			<td>
				<midas:mensaje clave="contratos.linea.totalCedido"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="totalCedido" deshabilitado="true"/>
			</td>			
		</tr>
		<tr>			
			<td>
				<midas:mensaje clave="contratos.linea.maximo"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="maximo" deshabilitado="true" />
			</td>
			<td colspan="4"></td>						
		</tr>
	</table>
	<table width="98%" style="margin-left:auto;margin-right:auto">
		<tr>
			<td>
	 			<div id="lineaExtenderVigenciaParteGrid" width="98%" height="250px" style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto"></div>
			</td> 
		</tr>
		<tr>
		 	<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_modificar" style="width:130px">
						<a href="javascript: void(0);"
							onclick="javascript: extenderVigenciaLineas();">
							<midas:mensaje clave="catalogo.lineas.extender.linea"/>
						</a>
					</div>
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/contratos/linea/listarLineaVigencia.do','contenido','cargarComponentesLineaVigencia()');"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td> 
		</tr>		
	</table>
	<input type="hidden" id="fechaFinalOriginal" value="" />
</midas:formulario>