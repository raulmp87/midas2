<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario accion="/contratos/linea/agregar">	
	<html:hidden property="idTmLinea" styleId="idTmLinea" />
	<html:hidden property="idTcRamo" styleId="idTcRamo" />
	<html:hidden property="fechaInicial" styleId="fechaInicial" />
	<html:hidden property="fechaFinal" styleId="fechaFinal" />
	<html:hidden property="estatus" styleId="estatus" />
	<html:hidden property="participacionesCP" styleId="participacionesCP" />
	<html:hidden property="participacionesPE" styleId="participacionesPE" />
	<html:hidden property="idTmContratoCuotaParte" styleId="idTmContratoCuotaParte"/>
	<html:hidden property="idTmContratoPrimerExcedente" styleId="idTmContratoPrimerExcedente" />
	<html:hidden property="mensajeAutorizar" styleId="mensajeAutorizar" />
	<html:hidden property="estatusContratoCuotaParte" styleId="estatusContratoCuotaParte" />
	<html:hidden property="estatusContratoPrimerExcedente" styleId="estatusContratoPrimerExcedente" />
	
	<table id="agregar" style="border-width:0px;">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="contratos.linea.agregar" />
			</td>
		</tr>
		<tr height="10px"></tr>
		<tr>
			<td width="15%">
				<etiquetas:etiquetaError property="fechaInicial" requerido="no"
					key="contratos.linea.fechaInicial" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="fechaInicial" deshabilitado="true" />
			</td>
			<td width="15%">
				<etiquetas:etiquetaError property="porcentajeDeCesion" requerido="no"
					key="contratos.linea.porcentajeCesion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="15%">
				<midas:texto propiedadFormulario="porcentajeDeCesion" id="porcentajeDeCesion" soloLectura="true" />
			</td>
			<td width="15%">
				<etiquetas:etiquetaError property="cesionPrimerExcedente" requerido="no"
					key="contratos.linea.cesionPrimerExcedente" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="15%">
				<midas:texto propiedadFormulario="cesionPrimerExcedente" id="cesionPrimerExcedente" soloLectura="true" />
			</td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="fechaFinal" requerido="no"
					key="contratos.linea.fechaFinal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="fechaFinal" deshabilitado="true" />
			</td>
			<td>
				<etiquetas:etiquetaError property="cesionCuotaParte" requerido="no"
					key="contratos.linea.cesionCuotaParte" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="cesionCuotaParte" id="cesionCuotaParte" soloLectura="true"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="capacidadMaximaLinea" requerido="no"
					key="contratos.linea.capacidadMaxima" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="capacidadMaximaLinea" id="capacidadMaximaLinea" soloLectura="true" />
			</td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcSubRamo" requerido="si"
					key="contratos.linea.idTcSubRamo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" readonly="false" nombre="configuracionLineaForm" styleClass="cajaTexto"/>
			</td>			
			<td>
				<etiquetas:etiquetaError property="montoPleno" requerido="no"
					key="contratos.linea.montoPleno" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="montoPleno" id="montoPleno" soloLectura="true" />
			</td>
			<td>
				<etiquetas:etiquetaError property="totalRetencion" requerido="no"
					key="contratos.linea.totalRetencion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="totalRetencion" id="totalRetencion" soloLectura="true" />
			</td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="modoDistribucion" requerido="si"
					key="contratos.linea.tipoDistribucion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:combo id="modoDistribucion" propiedad="modoDistribucion" styleClass="cajaTexto" >	
					<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
					<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.linea.tipodistribucion.poliza" /></midas:opcionCombo>
					<midas:opcionCombo valor="2"><midas:mensaje clave="contratos.linea.tipodistribucion.ubicacion" /></midas:opcionCombo>
					<midas:opcionCombo valor="3"><midas:mensaje clave="contratos.linea.tipodistribucion.bien" /></midas:opcionCombo>
					<midas:opcionCombo valor="4"><midas:mensaje clave="contratos.linea.tipodistribucion.vida" /></midas:opcionCombo>									
				</midas:combo>
			</td>
			<td>
				<etiquetas:etiquetaError property="numeroPlenos" requerido="no"
					key="contratos.linea.numeroPlenos" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="numeroPlenos" id="numeroPlenos" soloLectura="true" />
			</td>
			<td>
				<etiquetas:etiquetaError property="totalCedido" requerido="no"
					key="contratos.linea.totalCedido" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="totalCedido" id="totalCedido" soloLectura="true"/>
			</td>			
		</tr>
		<tr>			
			<td>
				<etiquetas:etiquetaError property="maximo" requerido="si"
					key="contratos.linea.maximo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="maximo" id="maximo" onblur="validarMontoRegistrarEgreso(this);procesarCalculoMontosLinea()" onkeypress="return soloNumeros(this, event, true)" caracteres="20" />
			</td>
			<td colspan="4">
			</td> 						
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
	</table>
	<table style="margin-left:auto;margin-right:auto">
		<tr>
			<td>
				<table>
					<tr>
						<th  class="seccion"><midas:mensaje clave="contratos.linea.encabezado.asignarContratoCuotaParte"/></th>			
					</tr>
					<tr>
						<td>
							<table>
								<tr>
									<th><midas:mensaje clave="contratos.linea.folio"/>:</th>
									<td width="80px"><midas:texto propiedadFormulario="folioContratoCuotaParte" id="folioContratoCuotaParte" soloLectura="true"  /></td>
									<td>
										<div id="b_asignar">
											<a href="javascript: void(0);" onclick="javascript:eliminarFormatoMontosRegistrarLinea();formarStringDeParticipacionesCPyPE();sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/mostrarAsignarCP.do', 'contenido','mostrarContratosCuotaParteGrids()');"><midas:mensaje clave="midas.accion.asignar"/></a>																																		
										</div>
									</td>
									<td>
										<div id="b_consultar">
											<logic:notEqual value="1" property="estatusContratoCuotaParte" name="configuracionLineaForm">
												<a href="javascript: void(0);" onclick="javascript:eliminarFormatoMontosRegistrarLinea();formarStringDeParticipacionesCPyPE();sendRequestContrato(5,<midas:escribe nombre="configuracionLineaForm" propiedad="idTmContratoCuotaParte" /> + '',document.configuracionLineaForm);"><midas:mensaje clave="midas.accion.consultar"/></a>
											</logic:notEqual>
											<logic:equal value="1" property="estatusContratoCuotaParte" name="configuracionLineaForm">
												<a href="javascript: void(0);" onclick="javascript:if($('folioContratoCuotaParte').value != '0' && $('folioContratoCuotaParte').value != ''){eliminarFormatoMontosRegistrarLinea();formarStringDeParticipacionesCPyPE();sendRequestContrato(9,<midas:escribe nombre="configuracionLineaForm" propiedad="idTmContratoCuotaParte" />,null);}"><midas:mensaje clave="midas.accion.consultar"/></a>
											</logic:equal>
										</div>
									</td>
									<td><img src="/MidasWeb/img/delete-icon.jpg" title="<midas:mensaje clave="contratos.linea.eliminarcontrato.tooltip"/>" style="cursor:pointer" onclick="desasociarContratoCPLinea()" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<div id="participacionesCPGrid" width="402px" height="120px" style="background-color:white;overflow:hidden"></div>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<table>
					<tr>
						<th  class="seccion" colspan="6"><midas:mensaje clave="contratos.linea.encabezado.asignarContratoPrimerExcedente"/></th>			
					</tr>
					<tr>
						<td>
							<table>
								<tr>
									<th><midas:mensaje clave="contratos.linea.folio"/>:</th>
									<td width="80px"><midas:texto propiedadFormulario="folioContratoPrimerExcedente" id="folioContratoPrimerExcedente" soloLectura="true"  /></td>
									<td>
										<div id="b_asignar">
											<a href="javascript: void(0);" onclick="javascript:eliminarFormatoMontosRegistrarLinea();formarStringDeParticipacionesCPyPE();sendRequest(document.configuracionLineaForm,'/MidasWeb/contratos/linea/mostrarAsignarPE.do','contenido','mostrarContratosPrimerExcedenteGrids()')"><midas:mensaje clave="midas.accion.asignar"/></a>
										</div>
									</td>
									<td>
										<div id="b_consultar">
											<logic:notEqual value="1" property="estatusContratoPrimerExcedente" name="configuracionLineaForm">
												<a href="javascript: void(0);" onclick="javascript:if($('folioContratoPrimerExcedente').value != '0' && $('folioContratoPrimerExcedente').value != ''){eliminarFormatoMontosRegistrarLinea();formarStringDeParticipacionesCPyPE();sendRequestContrato(6,<midas:escribe nombre="configuracionLineaForm" propiedad="idTmContratoPrimerExcedente" />+'',configuracionLineaForm);}" ><midas:mensaje clave="midas.accion.consultar"/></a>
											</logic:notEqual>
											<logic:equal value="1" property="estatusContratoPrimerExcedente" name="configuracionLineaForm">
												<a href="javascript: void(0);" onclick="javascript:eliminarFormatoMontosRegistrarLinea();formarStringDeParticipacionesCPyPE();sendRequestContrato(10,<midas:escribe nombre="configuracionLineaForm" propiedad="idTmContratoPrimerExcedente" />,null);"><midas:mensaje clave="midas.accion.consultar"/></a>
											</logic:equal>
										</div>
									</td>
									<td><img src="/MidasWeb/img/delete-icon.jpg" title="<midas:mensaje clave="contratos.linea.eliminarcontrato.tooltip"/>" style="cursor:pointer" onclick="desasociarContratoPELinea()"/></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<div id="participacionesPEGrid" width="402px" height="120px" style="background-color:white;overflow:hidden;font-size:8px"></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		<table width="100%">
			<tr>
				 <td>
				 	<div class="alinearBotonALaDerecha">
				 		<table>
					 		<tr>
					 			<td style="font-size:10px;font-weight:bold">
									<midas:mensaje clave="contratos.linea.autorizar"/>
									<midas:check propiedadFormulario="autorizar" id="autorizar"></midas:check>
								</td>
								<td>
									<div id="b_regresar">
										<a href="javascript: void(0);" onclick="javascript: sendRequest(configuracionLineaForm,'/MidasWeb/contratos/linea/listarLineaNegociacion.do', 'contenido','cargarComponentesLineaNegociacion()');"><midas:mensaje clave="midas.accion.regresar"/></a>
									</div>
								</td>
								<td>
									<div id="b_guardar" style="margin-right:5px;">
										<a href="javascript: void(0);" onclick="javascript: guardarAutorizarLinea(document.configuracionLineaForm);"><midas:mensaje clave="midas.accion.guardar"/></a>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
</midas:formulario>
