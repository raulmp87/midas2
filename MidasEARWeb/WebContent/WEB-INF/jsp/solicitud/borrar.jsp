<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="detalle">
	<center>
		<midas:formulario accion="/solicitud/borrar">
			<table id="desplegarDetalle">
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Datos de la Solicitud
					</td>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<td width="18%">
								Solicitud: <%= "SOL-" + String.format("%08d", new Object[]{session.getAttribute("idSolicitud")}) %>
							</td>
						</c:when>
						<c:otherwise><td width="18%">&nbsp;</td></c:otherwise>
					</c:choose>			
					<td width="17%">
						<jsp:useBean id="now" class="java.util.Date" />
						Fecha: <fmt:formatDate value="${now}" dateStyle="short" />
					</td>
				</tr>
			</table>
			<table id="desplegar">
				<tr>
					<th>
						<etiquetas:etiquetaError property="tipoPersona" requerido="no"
							name="solicitudForm" key="solicitud.tipoPersona"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<c:choose>
					        <c:when test='${solicitudForm.tipoPersona == "1"}'>
					            F&iacute;sica
					        </c:when>
					        <c:otherwise>
					            Moral
					        </c:otherwise>
					    </c:choose>
					</td>
				</tr>
				<c:choose>
					<c:when test='${solicitudForm.tipoPersona == "1"}'>
			            <tr>
							<th>
								<etiquetas:etiquetaError property="nombres" requerido="no"
									name="solicitudForm" key="solicitud.nombres"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td><midas:escribe propiedad="nombres" nombre="solicitudForm"/></td>
							<th>
								<etiquetas:etiquetaError property="apellidoPaterno" requerido="no"
									name="solicitudForm" key="solicitud.apellidoPaterno"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td><midas:escribe propiedad="apellidoPaterno" nombre="solicitudForm"/></td>
							<c:if test='${!empty solicitudForm.apellidoMaterno}'>
								<th>
									<etiquetas:etiquetaError property="apellidoMaterno" requerido="no"
										name="solicitudForm" key="solicitud.apellidoMaterno"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
								</th>
								<td><midas:escribe propiedad="apellidoMaterno" nombre="solicitudForm"/></td>
							</c:if>
						</tr>
			        </c:when>
			        <c:otherwise>
			            <tr>
							<th>
								<etiquetas:etiquetaError property="razonSocial" requerido="no"
									name="solicitudForm" key="solicitud.razonSocial"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td><midas:escribe propiedad="razonSocial" nombre="solicitudForm"/></td>
						</tr>
			        </c:otherwise>
			    </c:choose>
				<tr>
					<th>
						<etiquetas:etiquetaError property="telefono" requerido="no"
							name="solicitudForm" key="solicitud.telefono"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:escribe propiedad="telefono" nombre="solicitudForm"/>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="producto" requerido="no"
							name="solicitudForm" key="solicitud.producto"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:escribe propiedad="nombreComercialProducto" nombre="solicitudForm"/>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="agenteSeguros" requerido="no"
							name="solicitudForm" key="solicitud.agenteSeguros"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:escribe propiedad="nombreAgente" nombre="solicitudForm"/>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="correos" requerido="no"
							name="solicitudForm" key="solicitud.correos"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td colspan="2">
						<midas:escribe propiedad="correos" nombre="solicitudForm"/>
					</td>
				</tr>
				<tr>
					<td>
						<midas:checkBox valorEstablecido="0" propiedadFormulario="claveOpcionEmision" deshabilitado="true" >
							<midas:mensaje clave="solicitud.claveOpcionEmision" />
						</midas:checkBox>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>
			</table>
			
			<c:if test="${!empty solicitudForm.numeroSolicitud}">
				<div id="resultadosDocumentos">
					<midas:tabla idTabla="solicitudes"
						claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalSolicitud"
						claseCss="tablaConResultados" nombreLista="documentos"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
						<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
					</midas:tabla>
				</div>
				<div class="alinearBotonALaDerecha">		
					<div id="botonAgregar">
						<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
						<midas:boton onclick="Confirma('�Realmente deseas borrar el registro seleccionado?',document.solicitudForm,'/MidasWeb/solicitud/borrar.do', 'contenido',null);" tipo="borrar"/>
					</div>
				</div>		
			</c:if>
		</midas:formulario>
	</center>
</div>