<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div id="detalle">
	<center>
		<midas:formulario accion="/solicitud/agregar">
			<midas:oculto propiedadFormulario="estatus"/>
			<midas:oculto propiedadFormulario="idUsuario"/>
			<midas:oculto propiedadFormulario="bloqueoDatosAgente"/>
			<midas:oculto propiedadFormulario="numeroSolicitud"/>
			<midas:oculto propiedadFormulario="idProducto"/>
			<html:hidden property="idPoliza" name="solicitudForm" styleId="idPoliza"/>
			<html:hidden property="esRenovacion" name="solicitudForm" styleId="esRenovacion"/>
			<table id="desplegarDetalle">
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Datos de la Solicitud
					</td>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<td width="18%">
								Solicitud: <%= "SOL-" + String.format("%08d", new Object[]{session.getAttribute("idSolicitud")}) %>
							</td>
						</c:when>
						<c:otherwise><td width="18%">&nbsp;</td></c:otherwise>
					</c:choose>			
					<td width="17%">
						<jsp:useBean id="now" class="java.util.Date" />
						Fecha: <fmt:formatDate value="${now}" dateStyle="short" />
					</td>
				</tr>
			</table>
			<table id="desplegar">
				<tr>
					<th>
						<etiquetas:etiquetaError property="tipoPersona" requerido="si"
							name="solicitudForm" key="solicitud.tipoPersona"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:radio valorEstablecido="1" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">F&iacute;sica</midas:radio>
						<midas:radio valorEstablecido="2" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">Moral</midas:radio>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<div id="nombreDiv">
							<table id="desplegarDetalle">
								<th width="10%">
									<etiquetas:etiquetaError property="nombres" requerido="si"
										name="solicitudForm" key="solicitud.nombres"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
								</th>
								<td width="20%"><midas:texto propiedadFormulario="nombres" /></td>
								<th width="10%">
									<etiquetas:etiquetaError property="apellidoPaterno" requerido="si"
										name="solicitudForm" key="solicitud.apellidoPaterno"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
								</th>
								<td width="20%"><midas:texto propiedadFormulario="apellidoPaterno"/></td>
								<th width="10%">
									<etiquetas:etiquetaError property="apellidoMaterno" requerido="no"
										name="solicitudForm" key="solicitud.apellidoMaterno"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
								</th>
								<td width="30%"><midas:texto propiedadFormulario="apellidoMaterno"/></td>
							</table>
						</div>
						<dir id="razonSocialDiv" style="display: none;">
							<table id="desplegarDetalle">
								<th width="15%">
									<etiquetas:etiquetaError property="razonSocial" requerido="si"
										name="solicitudForm" key="solicitud.razonSocial"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
								</th>
								<td colspan="2" width="35%"><midas:texto propiedadFormulario="razonSocial" /></td>
								<td colspan="3" width="60%">&nbsp;</td>
							</table>
						</dir>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="telefono" requerido="si"
							name="solicitudForm" key="solicitud.telefono"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td><midas:texto propiedadFormulario="telefono"/></td>
				</tr>
				<c:choose>
					<c:when test="${!empty solicitudForm.idPoliza}">
						<tr>
							<th>
								<etiquetas:etiquetaError property="producto" requerido="si"
									name="solicitudForm" key="solicitud.producto"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td colspan="2">
								<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote" readonly="true"/>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>
								<etiquetas:etiquetaError property="producto" requerido="si"
									name="solicitudForm" key="solicitud.producto"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td colspan="2">
								<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote"/>
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
				<tr>
				
					<c:choose>
						<c:when test="${solicitudForm.bloqueoDatosAgente}">
							<th><etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.codigoAgente"/></th>
							<td width="20%">
								<midas:texto propiedadFormulario="codigoAgente" caracteres="20" soloLectura="true" id="codigoAgente" />
							</td>
						</c:when>
						<c:otherwise>
							<th>
								<etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.codigoAgente"/></th>
							<td width="20%">
								<midas:texto propiedadFormulario="codigoAgente" caracteres="20" onblur="javascript: seleccionarAgente(this.value);" id="codigoAgente" />
							</td>
						</c:otherwise>
					</c:choose>
				
				</tr>
				<tr>
					
					<c:choose>
						<c:when test="${solicitudForm.bloqueoDatosAgente}">
							<th>
								<etiquetas:etiquetaError property="agenteSeguros" requerido="si"
									name="solicitudForm" key="solicitud.agenteSeguros"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td colspan="2">
								<html:select property="idAgente" styleClass="cajaTexto" styleId="idAgente"  > 
									<html:optionsCollection value="idTcAgente" property="agentes" name="solicitudForm" label="nombre"/>
								</html:select></td>
						</c:when>
						<c:otherwise>
							<th>
								<etiquetas:etiquetaError property="agenteSeguros" requerido="si"
									name="solicitudForm" key="solicitud.agenteSeguros"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td colspan="2">
								<html:select property="idAgente" styleClass="cajaTexto" styleId="idAgente" onchange="javascript: escribirCodigoAgente(this.value);">
									<html:option value="">SELECCIONE...</html:option>
									<html:optionsCollection value="idTcAgente" property="agentes" name="solicitudForm" label="nombre"/>
								</html:select></td>
						</c:otherwise>
					</c:choose>
				
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="correos" requerido="no"
							name="solicitudForm" key="solicitud.correos"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td colspan="2"><midas:areatexto propiedadFormulario="correos"/></td>
				</tr>
				<tr>
					<td>
						<midas:checkBox valorEstablecido="0" propiedadFormulario="claveOpcionEmision">
							<midas:mensaje clave="solicitud.claveOpcionEmision" />
						</midas:checkBox>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>
			</table>
			<c:if test="${!empty solicitudForm.numeroSolicitud}">
				<div id="resultadosDocumentos">
					<midas:tabla idTabla="solicitudes"
						claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalSolicitud"
						claseCss="tablaConResultados" nombreLista="documentos"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
						<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
						<midas:columna propiedad="acciones"/>
					</midas:tabla>
				</div>
				<div class="alinearBotonALaDerecha">				
					<div id="botonAgregar">
						<midas:boton onclick="javascript: sendRequest(document.solicitudForm,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
						<midas:boton onclick="javascript: mostrarAdjuntarArchivoSolicitudWindow();" tipo="agregar"/>
						<midas:boton onclick="javascript: sendRequest(document.solicitudForm,'/MidasWeb/solicitud/modificar.do', 'contenido',null);" tipo="guardar"/>
					</div>
				</div>
			</c:if>
			<div id="errores" style="display: none;"><html:errors/></div>
		</midas:formulario>	
	</center>
</div>
