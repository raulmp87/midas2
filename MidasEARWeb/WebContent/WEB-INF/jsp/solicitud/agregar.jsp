<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<div id="detalle">
	<center>
		<midas:formulario accion="/solicitud/agregar">
			<html:hidden styleId="tipoMensaje" property="tipoMensaje"/>
    		<html:hidden styleId="mensaje" property="mensaje"/>
			<html:hidden property="idPoliza" name="solicitudForm" styleId="idPoliza"/>
			<html:hidden property="estatusPoliza" name="solicitudForm" styleId="estatusPoliza" value="2"/>
			<midas:oculto propiedadFormulario="permitirAutoAsignacion"/>
			<midas:oculto propiedadFormulario="bloqueoDatosAgente"/>
			<midas:oculto propiedadFormulario="numeroSolicitud"/>
			<input type="hidden" id="descProducto" value="" name="descProducto">
			<input type="hidden" id="fechaInicioCotizacion" value="" name="fechaInicioCotizacion">
			<input type="hidden" id="fechaFinCotizacion" value="" name="fechaFinCotizacion">
			<input type="hidden" id="cveEstatusPoliza" value="" name="cveEstatusPoliza">
			<table id="desplegarDetalle" width="100%">
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Datos de la Solicitud
					</td>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<td width="18%">
								Solicitud: <%= "SOL-" + String.format("%08d", new Object[]{session.getAttribute("idSolicitud")}) %>
							</td>
						</c:when>
						<c:otherwise><td width="18%">&nbsp;</td></c:otherwise>
					</c:choose>			
					<td width="17%">
						<jsp:useBean id="now" class="java.util.Date" />
						Fecha: <fmt:formatDate value="${now}" dateStyle="short" />
					</td>
				</tr>
			</table>
			<table id="agregar" width="100%">
				<c:if test="${empty solicitudForm.numeroSolicitud}">
					<tr>
						<th width="30%">
							<midas:checkBox valorEstablecido="1" id="apartirPoliza" propiedadFormulario="apartirPoliza" onClick="javascript: mostrarBuscarPoliza(); limpiarCamposSolicitud();">Crear a partir de Poliza existente</midas:checkBox>
						</th>
						<td colspan="4">
							<div>
								<table id="buscarPoliza" style="border: none; padding: 0px; display: none;">
									<tr>
										<td style="text-align: left; font-weight: bold; padding: 3px; font-size: 9px;" width="20%">
											Es renovaci&oacute;n? *
										</td>									
										<td style="text-align:left; padding:3px; font-size:9px;" width="20%">
											<c:choose>
												<c:when test="${!empty solicitudForm.numeroSolicitud}">
													<html:radio property="esRenovacion" value="0" disabled="true">No</html:radio>
													<html:radio property="esRenovacion" value="1" disabled="true">Si</html:radio>
												</c:when>
												<c:otherwise>
													<html:radio property="esRenovacion" value="0">No</html:radio>
													<html:radio property="esRenovacion" value="1">Si</html:radio>
												</c:otherwise>
											</c:choose>														
										</td>										
										<td><midas:texto propiedadFormulario="numPoliza" id="numPoliza" 
											nombreFormulario="solicitudForm" onfocus="new Mask('####-########-##', 'string').attach(this)" 
											caracteres="16"  
											longitud="16" /></td>
										<td><midas:boton style="width:100px" onclick="javascript: validarPolizaAjax();" tipo="buscar" texto="Buscar P&oacute;liza"/></td>
										<td><midas:boton style="width:100px" onclick="javascript: mostrarVentanaPolizas($('estatusPoliza').value,true);" tipo="buscar" texto="Ver P&oacute;lizas"/></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</c:if>
				<tr>
					<th width="10%">
						<etiquetas:etiquetaError property="tipoPersona" requerido="si" normalClass="normal" errorClass="error"
							name="solicitudForm" key="solicitud.tipoPersona" errorImage="/img/information.gif" />
					</th>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<td width="20%">
								<html:radio property="tipoPersona" value="1" disabled="true" >F&iacute;sica</html:radio>
								<html:radio property="tipoPersona" value="2" disabled="true" >Moral</html:radio>
							</td>
						</c:when>
						<c:otherwise>
							<td width="20%">
								<midas:radio valorEstablecido="1" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">F&iacute;sica</midas:radio>
								<midas:radio valorEstablecido="2" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">Moral</midas:radio>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<td colspan="6">
								<logic:equal value="1" property="tipoPersona" name="solicitudForm" >
									<div id="nombreDiv">
										<table id="agregar" style="border: none; padding: 0px;">
											<th width="10%">
												<etiquetas:etiquetaError property="nombres" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
													name="solicitudForm" key="solicitud.nombres" /> </th>
											<td width="20%"><midas:texto propiedadFormulario="nombres" deshabilitado="true" /></td>
											<th width="15%"><etiquetas:etiquetaError property="apellidoPaterno" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
													name="solicitudForm" key="solicitud.apellidoPaterno" /> </th>
											<td width="20%"><midas:texto propiedadFormulario="apellidoPaterno" deshabilitado="true"/></td>
											<th width="15%"><etiquetas:etiquetaError property="apellidoMaterno" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
													name="solicitudForm" key="solicitud.apellidoMaterno" /> </th>
											<td width="20%"><midas:texto propiedadFormulario="apellidoMaterno" deshabilitado="true"/></td>
										</table>
									</div>
								</logic:equal>
								<logic:equal value="2" property="tipoPersona" name="solicitudForm" >
									<div id="razonSocialDiv" >
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="15%"><etiquetas:etiquetaError property="razonSocial" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="solicitudForm" key="solicitud.razonSocial" /></th>
										<td colspan="2" width="35%"><midas:texto propiedadFormulario="razonSocial" deshabilitado="true" /></td>
										<td colspan="3" width="60%">&nbsp;</td>
									</table>
								</div>
								</logic:equal>
							</td>
						</c:when>
						<c:otherwise>
							<td colspan="6">
								<div id="nombreDiv">
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="10%">
											<etiquetas:etiquetaError property="nombres" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudForm" key="solicitud.nombres" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="nombres" /></td>
										<th width="15%"><etiquetas:etiquetaError property="apellidoPaterno" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudForm" key="solicitud.apellidoPaterno" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="apellidoPaterno"/></td>
										<th width="15%"><etiquetas:etiquetaError property="apellidoMaterno" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudForm" key="solicitud.apellidoMaterno" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="apellidoMaterno"/></td>
									</table>
								</div>
								<div id="razonSocialDiv" style="display: none;">
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="15%"><etiquetas:etiquetaError property="razonSocial" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="solicitudForm" key="solicitud.razonSocial" /></th>
										<td colspan="2" width="35%"><midas:texto propiedadFormulario="razonSocial" /></td>
									</table>
								</div>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<th width="10%"><etiquetas:etiquetaError property="telefono" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.telefono"/></th>
							<td width="20%"><midas:texto propiedadFormulario="telefono" caracteres="20" deshabilitado="true" /></td>
							
						</c:when>
						<c:otherwise>
							<th width="10%">
								<etiquetas:etiquetaError property="telefono" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.telefono"/></th>
							<td width="20%"><midas:texto propiedadFormulario="telefono" caracteres="20" /></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<th><etiquetas:etiquetaError property="idProducto" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.producto"/> </th>
							<td colspan="3">
								<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote" readonly="true" />
							</td><td colspan="2">&nbsp;</td>
						</c:when>
						<c:otherwise>
							<th><etiquetas:etiquetaError property="idProducto" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.producto"/> </th>
							<td colspan="3">
								<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote"/>
							</td><td colspan="2">&nbsp;</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud || solicitudForm.bloqueoDatosAgente}">
							<th><etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.codigoAgente"/></th>
							<td width="20%">
								<midas:texto propiedadFormulario="codigoAgente" caracteres="20" soloLectura="true" id="codigoAgente" />
							</td>
						</c:when>
						<c:otherwise>
							<th>
								<etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.codigoAgente"/></th>
							<td width="20%">
								<midas:texto propiedadFormulario="codigoAgente" caracteres="20" onblur="javascript: seleccionarAgente(this.value);" id="codigoAgente" />
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud || solicitudForm.bloqueoDatosAgente}">
							<th><etiquetas:etiquetaError property="idAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.agenteSeguros"/></th>
							<td colspan="3">
								<html:select property="idAgente" styleClass="cajaTexto" styleId="idAgente"  > 
									<html:optionsCollection value="idTcAgente" property="agentes" name="solicitudForm" label="nombre"/>
								</html:select></td><td colspan="2">&nbsp;</td>
						</c:when>
						<c:otherwise>
							<th><etiquetas:etiquetaError property="idAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.agenteSeguros"/></th>
							<td colspan="3">
								<html:select property="idAgente" styleClass="cajaTexto" styleId="idAgente" onchange="javascript: escribirCodigoAgente(this.value);">
									<html:option value="">SELECCIONE...</html:option>
									<html:optionsCollection value="idTcAgente" property="agentes" name="solicitudForm" label="nombre"/>
								</html:select></td><td colspan="2">&nbsp;</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<th colspan="2"><etiquetas:etiquetaError property="correos" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.correos"/></th>
							<td colspan="3"><midas:areatexto renglones="4" propiedadFormulario="correos" deshabilitado="true" /></td><td colspan="1">&nbsp;</td>
						</c:when>
						<c:otherwise>
							<th colspan="2"><etiquetas:etiquetaError property="correos" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudForm" key="solicitud.correos"/></th>
							<td colspan="3"><midas:areatexto renglones="4" propiedadFormulario="correos"/></td><td colspan="1">&nbsp;</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudForm.numeroSolicitud}">
							<td colspan="2"><html:checkbox property="claveOpcionEmision" value="0" disabled="true">
								<midas:mensaje clave="solicitud.claveOpcionEmision" /></html:checkbox> </td>
						</c:when>
						<c:otherwise>
							<td colspan="2"><midas:checkBox valorEstablecido="0" propiedadFormulario="claveOpcionEmision">
									<midas:mensaje clave="solicitud.claveOpcionEmision" /></midas:checkBox></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td class="campoRequerido" colspan="2">
			 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
					</td>
					<td class="guardar" width="60%">
						<c:if test="${empty solicitudForm.numeroSolicitud}">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
								<c:choose>
									<c:when test="${solicitudForm.permitirAutoAsignacion}">	
										<midas:boton onclick="javascript: if(document.getElementById('apartirPoliza').checked) { sendRequest(document.solicitudForm,'/MidasWeb/solicitud/validarDatosSolicitud.do?idToProducto='+document.getElementById('idToProducto').value, 'contenido','popupValidarAgente(); mostrarTipoPersonaSolicitud(); mostrarBuscarPoliza(); existenErrores(\&#39;agregarSolicitudApartirPoliza()\&#39;)'); } else { sendRequest(document.solicitudForm,'/MidasWeb/solicitud/validarDatosSolicitud.do', 'contenido','popupValidarAgente(); mostrarTipoPersonaSolicitud(); mostrarBuscarPoliza(); existenErrores(\&#39;mostrarAccionesSolicitud()\&#39;)'); }" tipo="continuar"/>
									</c:when>
									<c:otherwise>
										<midas:boton onclick="javascript: if(document.getElementById('apartirPoliza').checked) { sendRequest(document.solicitudForm,'/MidasWeb/solicitud/validarDatosSolicitud.do?idToProducto='+document.getElementById('idToProducto').value, 'contenido','popupValidarAgente(); mostrarTipoPersonaSolicitud(); mostrarBuscarPoliza(); existenErrores(\&#39;agregarSolicitudApartirPoliza()\&#39;)'); } else { sendRequest(document.solicitudForm,'/MidasWeb/solicitud/validarDatosSolicitud.do', 'contenido','popupValidarAgente(); mostrarTipoPersonaSolicitud(); mostrarBuscarPoliza(); existenErrores(\&#39;agregarSolicitud()\&#39;)'); }" tipo="continuar"/>
									</c:otherwise>
								</c:choose>
							</div>
						</c:if>
					</td> 		
				</tr>
				<tr>
					<td colspan="6">
						<midas:mensajeUsuario/>
					</td>			
				</tr>
			</table>
			<c:if test="${!empty solicitudForm.numeroSolicitud}">
				<div id="resultadosDocumentos">
					<midas:tabla idTabla="solicitudes"
						claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalSolicitud"
						claseCss="tablaConResultados" nombreLista="documentos"
						urlAccion="/solicitud/listarDocumentos.do">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
						<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
						<midas:columna propiedad="acciones"/>
					</midas:tabla>
				</div>
				<div class="alinearBotonALaDerecha">				
					<div id="botonAgregar">
						<midas:boton onclick="javascript: sendRequest(document.solicitudForm,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
						<midas:boton onclick="javascript: mostrarAdjuntarArchivoSolicitudWindow();" tipo="agregar"/>
					</div>
				</div>
			</c:if>
			<div id="errores" style="display: none;"><html:errors/></div>
		</midas:formulario>
	</center>
</div>
