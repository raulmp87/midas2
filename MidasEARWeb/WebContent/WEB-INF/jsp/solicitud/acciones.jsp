<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/solicitud/agregar">
	<table id="agregar">
	<tr>
		<th>Acci&oacute;n: </th>
		<td width="90%">
			<midas:combo propiedad="accion" styleClass="cajaTexto">
				<midas:opcionCombo valor="1">Capturar Detalle Orden de Trabajo</midas:opcionCombo>
				<midas:opcionCombo valor="2">Agregar Orden de Trabajo</midas:opcionCombo>
				<midas:opcionCombo valor="3">Cancelar</midas:opcionCombo>
			</midas:combo>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="botonAgregar">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: parent.dhxWins.window('accionesSolicitud').close(); " tipo="regresar"/>
					<midas:boton onclick="javascript: parent.sendRequestSolicitud(null,'/MidasWeb/solicitud/agregar.do?accion=' + document.solicitudForm.accion.value,'contenido','dhxWins.window(\\'accionesSolicitud\\').close();',  document.solicitudForm.accion.value);" tipo="continuar"/>
				</div>
			</div>
		</td>
	</tr>
</table>
</midas:formulario>
 