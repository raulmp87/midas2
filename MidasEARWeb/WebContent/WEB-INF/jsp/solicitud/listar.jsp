<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario accion="/solicitud/listar">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="solicitudForm" property="totalRegistros"/>
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar"/> Solicitudes
			</td>
		</tr>
	</table>
	
	<table width="98%" id="filtros">
		<tr>
<%--		<td colspan="2">--%>
<%--		<table style="font-size: 9px;">--%>
			<th>N&uacute;mero de solicitud:</th>
			<td>
				<midas:texto propiedadFormulario="numeroSolicitud" nombreFormulario="solicitudForm" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<th >
				<midas:mensaje clave="midas.ordenesdetrabajo.estatus"/>:
			</th>
			<td >
				<midas:comboValorFijo grupoValores="29" propiedad="estatus"
					styleClass="cajaTexto" nombre="solicitudForm"/>
			</td>
<%--		</table>--%>
<%--		</td>--%>
		</tr>
		<tr><th colspan="6"><center>Datos del cliente</center></th></tr>
		<tr>
			<th>Nombre:</th>
			<td>
				<midas:texto propiedadFormulario="nombres" nombreFormulario="solicitudForm"/>
			</td>
			<th>Apellido Paterno:</th>
			<td>
				<midas:texto propiedadFormulario="apellidoPaterno" nombreFormulario="solicitudForm"/>
			</td>
			<th>Apellido Materno:</th>
			<td>
				<midas:texto propiedadFormulario="apellidoMaterno" nombreFormulario="solicitudForm"/>
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<th >
			<midas:boton onclick="javascript: sendRequest(document.solicitudForm, '/MidasWeb/solicitud/listarFiltrado.do', 'contenido', null);" tipo="buscar"/>
			</th>
		</tr>
	</table>
	<br/>	
	<div id="resultados">
		<midas:tabla idTabla="solicitudes"
			claseDecoradora="mx.com.afirme.midas.decoradores.Solicitud"
			claseCss="tablaConResultados" nombreLista="solicitudes"
			urlAccion="/solicitud/listarFiltradoPaginado.do"  exportar="false"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="idToSolicitud" titulo="Solicitud" />
			<midas:columna propiedad="fechaCreacion" titulo="Fecha" formato="{0,date,dd/MM/yyyy}"/>
			<midas:columna propiedad="nombreAsegurado" titulo="Nombre / Raz&oacute;n Social" maxCaracteres="20" />
			<midas:columna propiedad="productoDTO.nombreComercial" titulo="Producto" />
			<midas:columna propiedad="tipoSolicitud" titulo="Tipo de Solicitud" />
			<midas:columna propiedad="claveEstatus" titulo="Estatus" />
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		
			<div class="alinearBotonALaDerecha">
			<button onclick="javascript: sendRequest(null,'/MidasWeb/solicitud/mostrarAgregar.do', 'contenido',null);" type="button"
			style="cursor: pointer;">
				<table style="font-size: 9px;" align="center">
				<tr>
					<td align="center">
					Agregar Solicitud
					
					</td>	
				
				</tr>
				<tr>
					<td align="center">
					de Poliza
					
					</td>
				
				</tr>
				
				
				</table>
			
			
			</button>
			&nbsp;
			
			<button type="button" onclick="javascript: sendRequest(null,'/MidasWeb/endoso/solicitud/mostrarAgregar.do', 'contenido',null);" style="cursor: pointer;">
				<table style="font-size: 9px;" align="center">
				<tr>
					<td align="center">
					Agregar Solicitud
					
					</td>	
				
				</tr>
				<tr>
					<td align="center">
					de Endoso
					
					</td>
				
				</tr>
				
				
				</table>
			
			
			</button>
			
			
				
				
			
				</div>
				
			</div>
		
	</div>
</midas:formulario>
