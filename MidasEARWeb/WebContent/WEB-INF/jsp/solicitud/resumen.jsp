<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>

<midas:formulario accion="/endoso/solicitud/mostrarResumen">
<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.solicitud"/></div>
<div class="subtituloCotizacion"> <midas:mensaje clave="midas.solicitud.prefijo"/><midas:escribe propiedad="numeroSolicitudFormateado" nombre="solicitudForm"/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaSolicitud" nombre="solicitudForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>
<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="midas.ordendetrabajo.nombreSolicitante"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreSolicitante" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.estatus"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="descripcionEstatus" nombre="solicitudForm"/></td>
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.producto"/>:</th>
		<td class="txt_v"><midas:escribe propiedad="nombreComercialProducto" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.agente"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreAgente" nombre="solicitudForm"/></td>					
	</tr>	
</table>
<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.ordendetrabajo"/></div>
<div class="subtituloCotizacion"><midas:mensaje clave="configuracion.cobertura.ordentrabajo.prefijo"/><midas:escribe propiedad="numeroOrdenTrabajoFormateado" nombre="solicitudForm"/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaODT" nombre="solicitudForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>
<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="solicitud.resumen.usuario.asignacion"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreUsuarioAsignacionODT" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.estatus"/>:</th>
		<td class="txt_v"><midas:escribe propiedad="descripcionEstatusODT" nombre="solicitudForm"/></td>							
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.tipopoliza"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="tipoPoliza" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.oficina"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="oficina" nombre="solicitudForm"/></td>					
	</tr>	
	<tr>
		<th><midas:mensaje clave="midas.cotizacion.vigencia"/>:</th>
		<td class="txt_v"><midas:escribe propiedad="fechaInicioVigencia" formato="dd'/'MM'/'yyyy" nombre="solicitudForm"/> - <midas:escribe propiedad="fechaFinVigencia" formato="dd'/'MM'/'yyyy" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.moneda"/>:</th>
		<td class="txt_v"><midas:escribe propiedad="moneda" nombre="solicitudForm"/></td>				
	</tr>	
</table>

<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.cotizacion"/></div>
<div class="subtituloCotizacion"><midas:mensaje clave="midas.cotizacion.prefijo"/><midas:escribe propiedad="numeroOrdenTrabajoFormateado" nombre="solicitudForm"/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaCOT" nombre="solicitudForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>

<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="solicitud.resumen.contratante"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreContratante" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.asegurado"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreAsegurado" nombre="solicitudForm"/></td>
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.usuario.asignacion"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreUsuarioAsignacionCOT" nombre="solicitudForm"/></td>
		<th>&nbsp;</th>
		<td class="txt_v">&nbsp;</td>					
	</tr>	
</table>

<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.emision"/></div>
<div class="subtituloCotizacion"><midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaEmision" nombre="solicitudForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>

<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="solicitudEndoso.numPoliza"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="numPoliza" nombre="solicitudForm"/></td>
		<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.numeroEndose"/></th>
		<td class="txt_v"> <midas:escribe propiedad="numeroEndoso" nombre="solicitudForm"/></td>
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.usuario.asignacion"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreUsuarioAsignacionEmision" nombre="solicitudForm"/></td>
		<th>&nbsp;</th>
		<td class="txt_v">&nbsp;</td>
	</tr>	
</table>
</midas:formulario>