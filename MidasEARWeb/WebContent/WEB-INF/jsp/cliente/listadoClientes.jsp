<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ page isELIgnored="false"%>
<%@  page contentType="text/xml" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="nombre" type="ro" width="115" sort="int" align="center"><midas:mensaje clave="midas.persona.nombre" /></column>
		<column id="direccion" type="ro" width="180" sort="str" ><midas:mensaje clave="midas.direccion" /></column>
	</head>
	<c:choose>
		<c:when test="${respuestaXMLForm.resultados != null}">
			<nested:iterate id="resultado" name="respuestaXMLForm" property="resultados" indexId="indexVar" type="mx.com.afirme.midas.interfaz.cliente.ClienteDTO">
				<row id="<bean:write name="resultado" property="idToPersona"/>|<bean:write name="resultado" property="idToDireccion"/>">
					<cell><bean:write name="resultado" property="nombre"/></cell>
					<cell><bean:write name="resultado" property="direccionCompleta"/></cell>
				</row>
			</nested:iterate>		
		</c:when>
		<c:otherwise>
			<cell></cell>
			<cell>No se encontraron resultados con la busqueda especificada</cell>
		</c:otherwise>	
	</c:choose>		
</rows>
