<%@ page isELIgnored="false"%>
<%@page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>

<midas:formulario accion="/cotizacion/agregarPersona">
	<table id="desplegarDetalle">
		<td width="70%"><html:hidden property="idPadre" name="clienteForm" styleId="idCotizacion"/>
			<html:hidden property="codigoPersona" name="clienteForm" styleId="codigoPersona"/> </td>
		<td width="15%">
			<html:radio property="claveTipoPersona" value="1" onfocus="cambiaFiltros(this.value);" styleId="fisica" >
					<midas:mensaje clave="midas.persona.fisica"/></html:radio>
		</td>
		<td width="15%">
			<html:radio property="claveTipoPersona" value="2" onfocus="cambiaFiltros(this.value);" styleId="moral" >
					<midas:mensaje clave="midas.persona.moral"/></html:radio>																		
		</td>
	</table>
	<div id="filtrosPersonaFisica">
	<html:hidden property="idPadre" styleId="idPadre"/>
		<table width="98%" id="filtros">
			<tr>
				<th><midas:mensaje clave="midas.persona.nombre"/></th>
				<td><midas:texto propiedadFormulario="nombre" id="cajaNombre"/></td>
				<th><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
				<td><midas:texto propiedadFormulario="apellidoPaterno" id="cajaApellidoPaterno"/></td>
				<th><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
				<td><midas:texto propiedadFormulario="apellidoMaterno" id="cajaApellidoMaterno"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.persona.rfc"/></th>
				<td><midas:texto propiedadFormulario="codigoRFC" id="cajaCodigoRFC" /></td>
				<th><midas:mensaje clave="midas.persona.noAsegurado"/></th>
				<td><midas:texto propiedadFormulario="numeroAsegurado" id="cajaNumeroAsegurado" /></td>
				<td colspan="2">
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript: cargarClientesFiltradoDataGrid('fisica');">Buscar
						</a>
					</div>				
				</td>										
			</tr>
		</table>	
	</div>
	<div id="filtrosPersonaMoral" style="display: none;">
		<table width="98%" id="filtros">
			<tr>
				<th><midas:mensaje clave="solicitud.razonSocial"/></th>
				<td colspan="2"><midas:texto propiedadFormulario="nombre" id="cajaNombreMoral"/></td>
				<th><midas:mensaje clave="midas.persona.rfc" /></th>
				<td><midas:texto propiedadFormulario="codigoRFC" id="cajaCodigoRFCMoral"/></td>
				<th><midas:mensaje clave="midas.persona.noAsegurado"/></th>
				<td><midas:texto propiedadFormulario="numeroAsegurado" id="cajaNumeroAseguradoMoral" /></td>
				<td>
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript: cargarClientesFiltradoDataGrid('moral');">Buscar
						</a>
					</div>
				</td>
			</tr>
		</table>
	</div>	
	<div id="clientesGrid" width="98%" height="304" ></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
</midas:formulario>