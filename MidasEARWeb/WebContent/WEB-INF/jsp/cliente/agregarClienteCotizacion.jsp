<%@ page isELIgnored="false"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>

<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/curpRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cliente/cliente.js"/>"></script>			

<div id="formularioCliente">
<midas:formulario accion="/cliente/asociarClienteCotizacion">
		<html:hidden property="idPadre" name="clienteForm" styleId="idPadre"/>
		<html:hidden property="codigoPersona" name="clienteForm" styleId="codigoPersona"/>
		<html:hidden property="mensaje" name="clienteForm" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" name="clienteForm" styleId="tipoMensaje"/>
		<html:hidden property="idDomicilio" name="clienteForm" styleId="idDomicilio"/>
		<html:hidden property="idCliente" name="clienteForm" styleId="idCliente"/>
		<table id="desplegarDetalle">
			<tr>
				<th><etiquetas:etiquetaError normalClass="normal" errorClass="error" errorImage="/img/information.gif" name="clienteForm"
						property="claveTipoPersona" key="midas.persona.tipo" requerido="si"/>
				<%--<midas:mensaje clave="midas.persona.tipo"/>*:--%></th>
				<td colspan="2">
					<html:radio property="claveTipoPersona"  value="1"  onclick="mostrarAgregarClienteCotizacion(${clienteForm.idPadre},${clienteForm.codigoPersona},${clienteForm.idCliente},'F');"  name="clienteForm">
							<midas:mensaje clave="midas.persona.fisica"/></html:radio>
				</td>
				<td colspan="2">
					<html:radio property="claveTipoPersona" value="2"   onclick="mostrarAgregarClienteCotizacion(${clienteForm.idPadre},${clienteForm.codigoPersona},${clienteForm.idCliente},'M');" name="clienteForm">
							<midas:mensaje clave="midas.persona.moral"/></html:radio>
				</td>
				<td>&nbsp;</td>
			</tr>						
		</table>
		<%--Persona fisica--%>
		<logic:equal value="1" property="claveTipoPersona" name="clienteForm" >
			<div id="personaFisica">
				<div class="subtituloIzquierdaDiv">Datos Personales</div>
				<table id="agregar">
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="codigoCURP" key="midas.persona.curp" requerido="no" />
						</th>
						<td colspan="2">
							<midas:texto propiedadFormulario="codigoCURP" id="codigoCURP" />
						</td>
						<td>
							<midas:boton onclick="javascript: buscarClientesPorCURP();"
								tipo="buscar" />
						</td>
					</tr>
					<tr>
						<th id="etiquetaNombre">
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="nombre" key="midas.persona.nombre" requerido="si" />
						</th>
						<td id="cajaNombreTD">
							<midas:texto propiedadFormulario="nombre" id="cajaNombre"
								onblur="formatRFC(document.clienteForm);"
								onkeypress="return personaMoral(this,event,false)" />
						</td>
						<th id="etiquetaPaterno">
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="apellidoPaterno" key="midas.persona.apellidoPaterno"
								requerido="si" />
						</th>
						<td id="cajaApellidoPaterno">
							<midas:texto propiedadFormulario="apellidoPaterno"
								onblur="formatRFC(document.clienteForm);"
								onkeypress="return soloAlfanumericos(this,event,false)" />
						</td>
					</tr>
					<tr>
						<th id="etiquetaMaterno">
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="apellidoMaterno" key="midas.persona.apellidoMaterno"
								requerido="si" />
						</th>
						<td id="cajaApellidoMaterno">
							<midas:texto propiedadFormulario="apellidoMaterno"
								onblur="formatRFC(document.clienteForm);"
								onkeypress="return soloAlfanumericos(this,event,false)" />
						</td>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="sexo" key="midas.persona.sexo" />
						</th>
						<td>
							<html:select property="sexo" styleClass="cajaTexto" name="clienteForm">
								<html:option value="M">Masculino</html:option>
								<html:option value="F">Femenino</html:option>
							</html:select>					
						</td>					
					</tr>
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="fechaNacimiento" key="midas.persona.fechaNacimiento"
								requerido="si" />
						</th>
						<td>
							<a href="javascript: void(0);" id="mostrarCalendario"
								onclick="javascript: mostrarCalendarioOT();"> <image
									src="/MidasWeb/img/b_calendario.gif" border="0" />
							</a>						
							<midas:texto propiedadFormulario="fechaNacimiento" id="fecha"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="formatRFC(document.clienteForm);" />
						</td>

						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="telefono" key="midas.persona.telefono" requerido="si" />
						</th>
						<td colspan="2">
							<midas:texto propiedadFormulario="telefono"
								onkeypress="return soloNumeros(this,event,false)" />
						</td>
					</tr>
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="codigoRFC" key="midas.persona.rfc" requerido="si" />
						</th>

						<td>
							<midas:texto propiedadFormulario="codigoRFC" id="rfc"
								longitud="20" onblur="validaRFC(this);" />
						</td>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="idEstadoNacimiento"
								key="midas.persona.estadoNacimiento"/>
						</th>
						<td>
							<midas:estado styleId="idEstadoNacimiento" size="1" nombre="clienteForm"
								propiedad="idEstadoNacimiento" pais="PAMEXI"
								styleClass="cajaTexto" />
						</td>						
					</tr>
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="email" key="midas.persona.email" requerido="si" />
						</th>
						<td colspan="3">
							<midas:texto propiedadFormulario="email" />
						</td>
						<th>
							&nbsp;
						</th>
					</tr>
					<tr>
						<td>
							<div id='calendarioOT' style="position: absolute; z-index: 1;"></div>
						</td>
					</tr>
				</table>
			</div>
		</logic:equal>
		<%--Persona moral--%>
		<logic:equal value="2" property="claveTipoPersona" name="clienteForm" >
			<div id="personaMoral">
				<div class="subtituloIzquierdaDiv">Representante Legal</div>
				<table id="agregar">
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="codigoCURP" key="midas.persona.curp" requerido="no" />
						</th>
						<td colspan="2">
							<midas:texto propiedadFormulario="codigoCURP" id="codigoCURP" />
						</td>
						<td>
							<midas:boton onclick="javascript: buscarClientesPorCURP();"
								tipo="buscar" />
						</td>
					</tr>
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="nombreRepresentante" key="midas.persona.nombre" requerido="si" />
						</th>
						<td>
							<midas:texto propiedadFormulario="nombreRepresentante" id="nombreRepresentante"/>
						</td>
						<th id="etiquetaPaterno">
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="apellidoPaterno" key="midas.persona.apellidoPaterno"
								requerido="si" />
						</th>
						<td id="cajaApellidoPaterno">
							<midas:texto propiedadFormulario="apellidoPaterno"
								onblur="formatRFC(document.clienteForm);"
								onkeypress="return soloAlfanumericos(this,event,false)" />
						</td>
					</tr>
					<tr>
						<th id="etiquetaMaterno">
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="apellidoMaterno" key="midas.persona.apellidoMaterno"
								requerido="si" />
						</th>
						<td id="cajaApellidoMaterno">
							<midas:texto propiedadFormulario="apellidoMaterno"
								onblur="formatRFC(document.clienteForm);"
								onkeypress="return soloAlfanumericos(this,event,false)" />
						</td>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="sexo" key="midas.persona.sexo" />
						</th>
						<td>
							<html:select property="sexo" styleClass="cajaTexto" name="clienteForm">
								<html:option value="M">Masculino</html:option>
								<html:option value="F">Femenino</html:option>
							</html:select>					
						</td>
					</tr>	
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="fechaNacimientoRepresentante" key="midas.persona.fechaNacimiento"
								requerido="si" />

						</th>					
						<td>
							<a href="javascript: void(0);" id="mostrarCalendario"
								onclick="javascript: mostrarCalendarioOT2();"> <image
									src="/MidasWeb/img/b_calendario.gif" border="" />
							</a>						
							<midas:texto propiedadFormulario="fechaNacimientoRepresentante" id="fecha2"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" />						
						</td>									
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="idEstadoNacimiento"
								key="midas.persona.estadoNacimiento" requerido="si" />
						</th>
						<td>
							<div id='calendarioOT2' style="position:absolute;z-index: 1;"></div>
							<midas:estado styleId="idEstadoNacimiento" size="1" nombre="clienteForm"
								propiedad="idEstadoNacimiento" pais="PAMEXI"
								styleClass="cajaTexto" />
						</td>									
					</tr>										
				</table>
				<div class="subtituloIzquierdaDiv">Datos Personales</div>
				<table id="agregar">
					<tr>
						<th id="etiquetaNombre">
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="nombre" key="midas.persona.razonSocial" requerido="si" />
						</th>
						<td id="cajaNombreTD" colspan="5">
							<midas:texto propiedadFormulario="nombre" id="cajaNombre"
								onblur="formatRFC(document.clienteForm);"
								onkeypress="return personaMoral(this,event,false)" />
						</td>
					</tr>
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="fechaNacimiento" key="midas.persona.fechaConstitucion"
								requerido="si" />
						</th>
						<td colspan="2">
							<a href="javascript: void(0);" id="mostrarCalendario"
								onclick="javascript: mostrarCalendarioOT();"> <image
									src="/MidasWeb/img/b_calendario.gif" border="" />
							</a>						
							<midas:texto propiedadFormulario="fechaNacimiento" id="fecha"
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
								onblur="formatRFC(document.clienteForm);" />

						</td>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="telefono" key="midas.persona.telefono" requerido="si" />
						</th>
						<td colspan="2">
							<midas:texto propiedadFormulario="telefono"
								onkeypress="return soloNumeros(this, event, false)" />
						</td>
					</tr>
					<tr>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="codigoRFC" key="midas.persona.rfc" requerido="si" />
						</th>
						<td colspan="2">
							<midas:texto propiedadFormulario="codigoRFC" id="rfc"
								onblur="validaRFC(this);" />
						</td>
						<th>
							<etiquetas:etiquetaError normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" name="clienteForm"
								property="email" key="midas.persona.email" requerido="si" />
						</th>
						<td colspan="2">
							<midas:texto propiedadFormulario="email" />
						</td>
						<td><div id='calendarioOT' style="position:absolute;z-index: 1;"></div></td>
					</tr>
				</table>
			</div>								
		</logic:equal>		
		
		<div class="subtituloIzquierdaDiv">Datos Domicilio</div>
			<table id="agregar">
				<tr>
					<th><etiquetas:etiquetaError normalClass="normal" errorClass="error" errorImage="/img/information.gif" name="clienteForm"
						property="nombreCalle" key="midas.direccion.calleYNumero" requerido="si"/>
					<%--<midas:mensaje clave="midas.direccion.nombreCalle"/>*: --%></th>
					<td colspan="3"><midas:texto propiedadFormulario="nombreCalle"/></td>
				</tr>
				<%--<tr>
					<th><midas:mensaje clave="midas.direccion.numeroExterior"/>*:</th>
					<td><midas:texto propiedadFormulario="numeroExterior"/></td>
					<th><midas:mensaje clave="midas.direccion.numeroInterior"/>*:</th>
					<td><midas:texto propiedadFormulario="numeroInterior"/></td>
				</tr>
				<tr>
					<th><midas:mensaje clave="midas.direccion.entreCalles"/>*:</th>
					<td colspan="3"><midas:texto propiedadFormulario="entreCalles"/></td>
				</tr>--%>
				<tr>
					<th><etiquetas:etiquetaError normalClass="normal" errorClass="error" errorImage="/img/information.gif" name="clienteForm"
						property="idEstado" key="midas.direccion.idEstado" requerido="si"/>
					<%--<midas:mensaje clave="midas.direccion.idEstado"/>*: --%></th>
					<td width = "30%">
						<midas:estado styleId="estado" size="1" propiedad="idEstado" pais="PAMEXI" styleClass="cajaTexto"
						onchange="limpiarObjetos('ciudad,colonia,codigoPostal'); getCiudades(this,'ciudad');" /></td>
					<th><etiquetas:etiquetaError normalClass="normal" errorClass="error" errorImage="/img/information.gif" name="clienteForm"
						property="idMunicipio" key="midas.direccion.idMunicipio" requerido="si"/>
					<%--<midas:mensaje clave="midas.direccion.idMunicipio"/>*: --%></th>
					<td width = "25%">
						<midas:ciudad styleId="ciudad" size="1" propiedad="idMunicipio" estado="idEstado" styleClass="cajaTexto" 
							onchange="getColonias(this,'colonia')" /></td>
				</tr>
				<tr>
					<th><etiquetas:etiquetaError normalClass="normal" errorClass="error" errorImage="/img/information.gif" name="clienteForm"
						property="nombreColonia" key="midas.direccion.nombreColonia" requerido="si"/>
					<%--<midas:mensaje clave="midas.direccion.nombreColonia"/> --%></th>
					<td><midas:colonia styleId="colonia" size="1" 
										propiedad="nombreColonia" 
										ciudad="idMunicipio" 
										styleClass="cajaTexto" 
										onchange="setCodigoPostal(this.value);"/>
					</td>
					<th><etiquetas:etiquetaError normalClass="normal" errorClass="error" errorImage="/img/information.gif" name="clienteForm"
						property="codigoPostal" key="midas.direccion.codigoPostal" requerido="si"/>
					<%--<midas:mensaje clave="midas.direccion.codigoPostal"/>*:--%> </th>
					<td>
						<midas:texto id="codigoPostal" propiedadFormulario="codigoPostal" onkeypress="return soloNumeros(this, event, false)"
						onchange="if (this.value !== '')getColoniasPorCP(this.value, 'colonia','ciudad','estado');" />
					</td>
				</tr>
			</table>
		<div class="alinearBotonALaDerecha">
			<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: agregarPersonaCotizacion(clienteForm);">
				<midas:mensaje clave="midas.accion.guardar"/>
				</a>
			</div>
		</div>
</midas:formulario>
</div>