<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">


<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>

<midas:formulario accion="/cliente/listarClientePorCURP">
	<html:hidden property="idOriginal" styleId="idToCotizacion" name="respuestaXMLForm"/>
	<html:hidden property="idResultado" styleId="codigoCliente" name="respuestaXMLForm"/>
	<table id="t_riesgo" width="100%">
		<tr>
			<th width="20%">CURP</th>
			<th width="35%">Cliente</th>
			<th width="40%">Direcci&oacute;n</th>
			<th width="5%">&nbsp;</th>		
		</tr>
		<logic:iterate id="cliente" property="resultados"
			indexId="indexVar"
			name="respuestaXMLForm">
			<tr class="bg_t2">
				<td>${cliente.codigoCURP}</td>
				<td>${cliente.nombre}
					<c:if test="${cliente.claveTipoPersona == 1}">
						&nbsp;${cliente.apellidoPaterno}&nbsp;${cliente.apellidoMaterno} 
					</c:if>				
				</td>
				<td>
					${cliente.nombreCalle},&nbsp;
					COL.${cliente.nombreColonia},
					${cliente.nombreDelegacion},&nbsp;
					${cliente.descripcionEstado},&nbsp;
					C.P.${cliente.codigoPostal}
				</td>
				<td>
					<a href="javascript: void(0);" onclick="parent.asignarClienteCURPACotizacion(${respuestaXMLForm.idOriginal},${respuestaXMLForm.idResultado},${cliente.idCliente},${cliente.idDomicilio});">
						<img border="0" alt="Asignar Cliente" src="/MidasWeb/img/icons/ico_terminar.gif" title="Asignar Cliente"></a>
				</td>
			</tr>
		</logic:iterate>
	</table>
</midas:formulario>
