<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacionsololectura/mostrarTexAdicional">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion"/>
	<input type="hidden" id='fecha' value='<bean:write name="fecha"/>'/>
	<input type="hidden" id='usuario' value='<bean:write name="usuario"/>'/>
	
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6">
				Textos Adicionales
			</td>
		</tr>	
		<tr>
			<td colspan="6">
				<div id="contenido_textosAdicionalesGrid" class="dataGridQuotationClass" ></div>
			</td>
		</tr>
		<tr>
			<td colspan="6">	
				
			</td>
		</tr>
		<tr>
			<td colspan="6">
			</td>
		</tr>
	</table>	
</midas:formulario>