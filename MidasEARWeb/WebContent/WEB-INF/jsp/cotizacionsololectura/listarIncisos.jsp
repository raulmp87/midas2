<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<midas:formulario accion="/cotizacionsololectura/listarIncisos">
	<div id="mensajes">
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	</div>
	<midas:oculto propiedadFormulario="idCotizacion" nombreFormulario="incisoCotizacionForm"/>	
	<input type='hidden' id='origen' value='<bean:write name="origen" scope="session"/>' name='origen'/>
	<nested:equal value="ODT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Orden de Trabajo </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:  <%= "ODT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>
	<nested:equal value="COT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Cotizaci&oacute;n </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.cotizacion.cotizacion"/>:  <%= "COT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>		
	<div style="clear:both"></div>	
	</br>
	<div id="resultados">
		<display:table name="incisos" sort="page" defaultsort="1" defaultorder="ascending" pagesize="10" cellpadding="0" cellspacing="0"
			 requestURI="/cotizacionsololectura/listarIncisos.do?id=<bean:write name='incisoCotizacionForm' property='idCotizacion'/>" id="incisos" decorator="mx.com.afirme.midas.decoradores.IncisoCotizacion" class="tablaConResultados" >
			<display:column property="id.numeroInciso" title="Inciso" />
			<display:column property="ubicacion" />
			<display:column property="valorPrimaNeta" title="Prima Neta"  format="{0,number,$#,##0.00}"/>
			<display:column property="accionesSoloLectura" title="Acciones"/>
		</display:table>
	</div>
	</br>
			
</midas:formulario>
