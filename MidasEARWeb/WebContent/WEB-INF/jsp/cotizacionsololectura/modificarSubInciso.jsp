<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<midas:formulario accion="/cotizacion/subinciso/modificar">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion" />
	<html:hidden property="numeroInciso" styleId="numeroInciso" />
	<html:hidden property="idToSeccion" styleId="idToSeccion" />
	<html:hidden property="numeroSubInciso" styleId="numeroSubInciso" />
	<input type='hidden' id='mensaje' value='Debe introducir todos los datos. Favor de verificar.' name='mensaje'/>
	<input type='hidden' id='tipoMensaje' value='20' name='tipoMensaje'/>

	<div class="subtituloCotizacion">
		<midas:mensaje clave="midas.cotizacion.subInciso" /> de <midas:escribe nombre="subIncisoForm" propiedad="seccionNombreComercial" />
	</div>
	<div class="subtituloCotizacion">
		<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:  <%= "ODT-" + String.format("%08d", new Object[]{request.getAttribute("idToCotizacion")}) %>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<midas:mensaje clave="configuracion.cobertura.fecha"/>: <bean:write name="subIncisoForm" property="fechaCreacion"/>
	</div>
	<div style="clear:both"></div>	
	<table id="desplegarDetalle">
		<tr>
			<th>
				SubInciso:
			</th>
			<td>
				<midas:texto propiedadFormulario="subInciso" nombreFormulario="subIncisoForm"/>
			</td>
			<th>
				Suma Asegurada:
			</th>
			<td>
				<midas:texto propiedadFormulario="sumaAsegurada" nombreFormulario="subIncisoForm" id="sumaAsegurada"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="datosSubInciso"></div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="botonRegresar">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);"
								onclick="javascript: sendRequest(subIncisoForm,'/MidasWeb/cotizacionsololectura/mostrarModificarSecciones.do?idToCotizacion='+$('idToCotizacion').value,'configuracion_detalle','mostrarSeccionesPorIncisoCOTRO(<midas:escribe propiedad="numeroInciso" nombre="subIncisoForm"/>,<midas:escribe propiedad="idToCotizacion" nombre="subIncisoForm"/>)');">
								<midas:mensaje clave="midas.accion.regresar" /></a>
						</div>
						
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>