<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:formulario accion="/cotizacionsololectura/listarDocumentosDigitalesComplementarios">
	<html:hidden property="idToCotizacion" name="documentosCotizacionForm" styleId="idToCotizacion"/>
	<table id="agregar" width="600px">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.docsDigitales.deLaSolicitud" />		
			</td>
		</tr>
		<tr>
			<td colspan="4"><!-- Tabla de Documentos digitales (vienen desde la Solicitud) -->
				<midas:tabla idTabla="docsDigComps" 
					claseDecoradora="mx.com.afirme.midas.decoradores.CotizacionDatosGenerales"
					claseCss="tablaConResultados" nombreLista="documentosDigitalesSolicitudList">
					<midas:columna propiedad="controlArchivo.nombreArchivoOriginal" titulo="Archivo" />
					<midas:columna propiedad="nombreUsuarioCreacion" titulo="Anexado Por" />
					<midas:columna propiedad="fechaCreacion" titulo="Fecha" formato="{0,date,dd/MM/yyyy}"/>
				</midas:tabla>
			</td>
		</tr>
		<tr height="30px"></tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.docsDigitalesComplementarios.deLaCotizacion" />	
			</td>
		</tr>
		
		<tr>
			<td colspan="4"><!-- Tabla de Documentos digitales Complementarios -->
				<midas:tabla idTabla="docsDigComps" 
					claseDecoradora="mx.com.afirme.midas.decoradores.DocumentosDigitalesComplementariosCotizacion"
					claseCss="tablaConResultados" nombreLista="documentosDigitalesCotizacionList">
					<midas:columna propiedad="controlArchivo.nombreArchivoOriginal" titulo="Archivo" />
					<midas:columna propiedad="nombreUsuarioCreacion" titulo="Anexado Por" />
					<midas:columna propiedad="fechaCreacion" titulo="Fecha" formato="{0,date,dd/MM/yyyy}"/>
					
				</midas:tabla>
			</td>
		</tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.aclaraciones" />	
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<midas:areatexto propiedadFormulario="aclaraciones" id="aclaraciones" renglones="10" deshabilitado="true"/>
			</td>
		</tr>
	</table>
</midas:formulario>
