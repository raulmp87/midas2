<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario accion="/cotizacionsololectura/listarRecibos">
	<table id="desplegarDetalle" width="100%">
		<nested:present name="cotizacionForm" property="polizaDTO">
			<nested:iterate id="nextEndoso" name="cotizacionForm" property="polizaDTO.endosoDTOs" indexId="index">
				<c:choose>
					<c:when test="${index == 0}">
						<tr>
							<td colspan="4">
								<div class="subtituloCotizacion">
									P&oacute;liza Original
								</div>
								<div class="subtituloCotizacion">
									Vigencia: <bean:write name="nextEndoso" property="fechaInicioVigencia" format="dd/MM/yyyy"/> - <bean:write name="nextEndoso" property="fechaFinVigencia" format="dd/MM/yyyy"/> 
								</div>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="4">
								<div class="subtituloCotizacion">
									Endoso <bean:write name="index"/>
								</div>
								<div class="subtituloCotizacion">
									Vigencia: <bean:write name="nextEndoso" property="fechaInicioVigencia" format="dd/MM/yyyy"/> - <bean:write name="nextEndoso" property="fechaFinVigencia" format="dd/MM/yyyy"/> 
								</div>
							</td>
						</tr>
						<tr>
							<th colspan="4">
								Tipo Endoso: 
								<c:if test="${nextEndoso.cotizacionDTO.solicitudDTO.claveTipoEndoso == 1}">
									Cancelaci&oacute;n
								</c:if>
								<c:if test="${nextEndoso.cotizacionDTO.solicitudDTO.claveTipoEndoso == 2}">
									Rehabilitaci&oacute;n
								</c:if>
								<c:if test="${nextEndoso.cotizacionDTO.solicitudDTO.claveTipoEndoso == 3}">
									Modificaci&oacute;n 
									<c:if test="${nextEndoso.claveTipoEndoso == 1}">
									(B)
									</c:if>
									<c:if test="${nextEndoso.claveTipoEndoso == 2}">
									(A)
									</c:if>
									<c:if test="${nextEndoso.claveTipoEndoso == 3}">
									(D)
									</c:if>
								</c:if>
								<c:if test="${nextEndoso.cotizacionDTO.solicitudDTO.claveTipoEndoso == 4}">
									Declaraci&oacute;n
								</c:if>
								<c:if test="${nextEndoso.cotizacionDTO.solicitudDTO.claveTipoEndoso == 5}">
									Cambio de Forma de Pago
								</c:if>
							</th>
						</tr>
						<c:set var="nextEndoso" value="${nextEndoso}" scope="request"></c:set>
						<c:if test="${nextEndoso.cotizacionDTO.solicitudDTO.claveTipoEndoso == 3}">
							<tr>
								<td colspan="4">
									<c:if test="${not empty nextEndoso.movimientosGenerales}">
										<div class="subtitulo">Movimientos de Aplicaci&oacute;n General</div>
										<table id="desplegar" border="0">
											<nested:iterate id="nextMovimiento" name="nextEndoso" property="movimientosGenerales" indexId="indexVar" scope="request">
												<c:set var="tipoMovimiento" value="${nextMovimiento.claveTipoMovimiento}"></c:set>
												<nested:equal value="10" name="tipoMovimiento">
													<c:set var="noMovimiento" value="${indexVar + 1}"></c:set>
													<tr>
														<th>
															<bean:write name="noMovimiento"/>
														</th>
														<th>
															<bean:write name="nextMovimiento" property="descripcionMovimiento"/>
														</th>
													</tr>
												</nested:equal>
											</nested:iterate>
										</table>
										<br/>
									</c:if>
									<c:if test="${not empty nextEndoso.movimientosInciso}">			
										<div class="subtitulo">Movimientos al Inciso</div>
										<table id="t_riesgo" width="100%">
											<tr>
												<th width="20%">Inciso/Ubicaci&oacute;n</th>
												<th width="20%">Bien/Secci&oacute;n</th>
												<th width="60%">Movimiento Realizado</th>
											</tr>
											<c:set var="numInciso" value="0"/>
											<nested:iterate id="nextMovimiento" name="nextEndoso" property="movimientosInciso" indexId="indexVar" scope="request">
												<c:if test="${nextMovimiento.claveAgrupacion == 0}">
													<tr>
														<c:choose>
															<c:when test="${numeroInciso == 0 || nextMovimiento.numeroInciso != numInciso}">
																<td width="20%" align="center" class="txt_v">
																	${nextMovimiento.numeroInciso}
																	<c:set var="numInciso" value="${nextMovimiento.numeroInciso}"/>
															</c:when>
															<c:otherwise>
															<td width="20%" align="center">
																&nbsp;<c:set var="numInciso" value="${nextMovimiento.numeroInciso}"/></c:otherwise>									
														</c:choose>							
														</td>
														<td width="20%" align="center">
															<c:choose>
																<c:when test="${nextMovimiento.idToSeccion > 0}">
																	${nextMovimiento.descripcionSeccion}
																</c:when>
																<c:otherwise>&nbsp;</c:otherwise>
															</c:choose>
														</td>
														<td width="50%">
															${nextMovimiento.descripcionMovimiento}	
														</td>												
													</tr>
												</c:if>			
											</nested:iterate>
										</table>
									</c:if>
								</td>
							</tr>
						</c:if>
					</c:otherwise>
				</c:choose>
			</nested:iterate>
			<nested:iterate id="nextEndoso" name="cotizacionForm" property="polizaDTO.endosoDTOs" indexId="index">
				<c:choose>
					<c:when test="${index == 0}">
						<tr>
							<th colspan="4">
								Recibos
							</th>
						</tr>
						<tr>
							<td colspan="4">
								<c:set var="listaRecibosPoliza" value="${nextEndoso.recibos}" scope="request">
								</c:set>
								<display:table name="listaRecibosPoliza" 
									class="tablaConResultados" 
									export="false">
									<display:column property="origenRecibo" title="Origen" group="1"/>
									<display:column property="numeroFolioRecibo" title="No. de Folio"/>
									<display:column property="numeroExhibicion" title="No. Exhibici&oacute;n"/>
									<display:column property="fechaLimitePago" title="L&iacute;mite de Pago" format="{0,date,dd/MM/yyyy}"/>
									<display:column property="importe" title="Importe" format="{0,number,$#,##0.00}"/>
									<display:column property="situacion" title="Estatus"/>
									<display:column property="fechaSituacion" title="Fecha de Estatus" format="{0,date,dd/MM/yyyy}"/>
								</display:table>							
							</td>
						</tr>					
					</c:when>
				</c:choose>			
			</nested:iterate>
		</nested:present>
	</table>
</midas:formulario>