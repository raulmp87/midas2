<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<div class="subtituloCotizacion">Riesgos de <bean:write name="nombreCobertura" scope="request" /> </div>
<div class="subtituloCotizacion">
<bean:message key="configuracion.cobertura.cotizacion"/>: <bean:message key="midas.cotizacion.prefijo"/><%= String.format("%08d", new Object[]{request.getAttribute("idCotizacion")}) %>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<bean:message key="configuracion.cobertura.fecha"/>: <%= java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT).format((java.util.Date)request.getAttribute("fecha")) %>
</div>
<div style="clear:both"></div>
<table id="desplegarDetalle">
	<tr>
		<td colspan="3">
			<div id="cotizacionRiesgosGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
</table>
