<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacion/documento/docAnexoCot/mostrarDocAnexo">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion"/>
	
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6">
				Documentos Anexos
			</td>
		</tr>	
		<tr>
			<td colspan="6">
				<div id="contenido_documentosAnexosGrid" class="dataGridQuotationClass" ></div>
			</td>
		</tr>
	</table>	
</midas:formulario>