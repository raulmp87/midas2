<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<midas:formulario accion="/cotizacion/mostrarDatosLicitacion">    
	<html:hidden property="idToCotizacion" name="datosLicitacionForm" styleId="idToCotizacion" />
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<table id="desplegarDetalle" width="600px">		
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="2">
				<midas:mensaje clave="midas.cotizacion.licitacion.datosLicitacion"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.cotizacion.licitacion.textoLicitacion"/>:</th>
			<td> <midas:areatexto propiedadFormulario="textoLicitacion" nombreFormulario="datosLicitacionForm" renglones="6" columnas="70" deshabilitado="true" /></td>
		</tr>
		<tr>
			<td colspan="2"><midas:radio valorEstablecido="0" propiedadFormulario="claveImpresionSumaAsegurada" deshabilitado="true"><midas:mensaje clave="midas.cotizacion.licitacion.imprimirSACantidad"/></midas:radio></td>						
		</tr>
		<tr>
			<td colspan="2"><midas:radio valorEstablecido="1" propiedadFormulario="claveImpresionSumaAsegurada" deshabilitado="true"><midas:mensaje clave="midas.cotizacion.licitacion.imprimirSASEA"/></midas:radio></td>						
		</tr>							
	</table>	
	
	
</midas:formulario>