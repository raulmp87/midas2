<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/cotizacion/igualacion/igualarCuota">
			
			<html:hidden property="mostrarOriginales" name="igualacionPrimaNetaForm" styleId="mostrarOriginales"/>
			<midas:oculto propiedadFormulario="idToCotizacion"/>
			<midas:oculto propiedadFormulario="mensaje"/>
			<html:hidden property="aplicoPrimerRiesgo" styleId="aplicoPrimerRiesgo"/>
			<html:hidden property="soloLectura" styleId="soloLectura"/>
			<table id="desplegarDetalle">
				<tr>
					<td colspan="4">
						<table id="t_riesgo">
							<tr>
								<th>&nbsp;</th>
								<th>Suma Asegurada</th>
								
								<c:choose>
									<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
										<th>Cuota Original</th>
										<th>Prima Neta Original</th>
									</c:when>
								</c:choose>
								
								<th>Cuota Resultante</th>
								<th>Prima Neta Resultante</th>
							</tr>
							<tr>
								<td><b>Total Cotizaci&oacute;n</b></td>
								<td>&nbsp;</td>
								
								<c:choose>
									<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
										<td>&nbsp;</td>
										<td><b><bean:write name="igualacionPrimaNetaForm" property="totalCotizacionOriginal" format="$#,##0.00" locale="idiomaLocal"/></b></td>
									</c:when>
								</c:choose>
																								
								<td>&nbsp;</td>
								<td><b><bean:write name="igualacionPrimaNetaForm" property="totalCotizacion" format="$#,##0.00" locale="idiomaLocal"/></b></td>
							</tr>
							<nested:iterate id="nextSeccion" name="igualacionPrimaNetaForm" property="secciones" indexId="indexVarSecc">
								<tr>
									<td style="padding-left: 5px;"><bean:write name="nextSeccion" property="seccionDTO.nombreComercial"/></td>
									<td>&nbsp;</td>
									
									<c:choose>
										<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
											<td>&nbsp;</td>
											<td><b><bean:write name="nextSeccion" property="valorPrimaNetaOriginal" format="$#,##0.00" locale="idiomaLocal"/></b></td>
										</c:when>
									</c:choose>
																											
									<td>&nbsp;</td>
									<td><b><bean:write name="nextSeccion" property="valorPrimaNeta" format="$#,##0.00" locale="idiomaLocal"/></b></td>
								</tr>
								<nested:iterate id="nextCobertura" name="nextSeccion" property="coberturaCotizacionLista" indexId="indexVarCob">
									<tr>
										<td style="padding-left: 10px;"><bean:write name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.nombreComercial"/></td>
										<td>
											<nested:equal value="1" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												<bean:write name="nextCobertura" property="valorSumaAsegurada" format="$#,##0.00" locale="idiomaLocal"/>
											</nested:equal>
											<nested:equal value="2" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												Amparada
											</nested:equal>
											<nested:equal value="3" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												<bean:write name="nextCobertura" property="valorSumaAsegurada" format="$#,##0.00" locale="idiomaLocal"/>
											</nested:equal>
										</td>
										<c:set var="cuotaOriginal" value="${nextCobertura.valorCuotaOriginal * 1000}"></c:set>
										
										<c:choose>
											<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
												<td><bean:write name="cuotaOriginal" formatKey="midas.danios.formato.moneda.sinSimbolo"/></td>
												<td><bean:write name="nextCobertura" property="valorPrimaNetaOriginal" format="$#,##0.00" locale="idiomaLocal"/></td>
											</c:when>
										</c:choose>
																				
										<c:set var="cuota" value="${nextCobertura.valorCuota * 1000}"></c:set>
										<td><bean:write name="cuota" formatKey="midas.danios.formato.moneda.sinSimbolo"/></td>
										<td><bean:write name="nextCobertura" property="valorPrimaNeta" format="$#,##0.00" locale="idiomaLocal"/></td>
									</tr>
								</nested:iterate>
							</nested:iterate>
						</table>
					</td>
				</tr>
					<c:if test="${igualacionPrimaNetaForm.mensajeErrorIgualacionPrima == '0'}">
				<c:choose>
					<c:when test="${empty igualacionPrimaNetaForm.seccionesCombo}">
						<tr>
							<td colspan="4">No se puede igualar cuotas: Secciones en Primer Riesgo</td>
						</tr>
					</c:when>
					<c:otherwise>
				
<%--						<tr>--%>
<%--							<td>--%>
<%--								Indique la Cobertura que desea igualar--%>
<%--							</td>--%>
<%--							<td>--%>
<%--								<html:select property="idToSeccion2" styleId="idToSeccion" styleClass="cajaTexto" onchange="javascript: llenarComboCobertura(this,'idToCobertura','/MidasWeb/cotizacion/igualacion/escribeComboCobertura.do');">--%>
<%--									<html:optionsCollection property="seccionesCombo" value="id.idToSeccion" label="seccionDTO.nombreComercial" name="igualacionPrimaNetaForm" />--%>
<%--								</html:select>--%>
<%--							</td>--%>
<%--							<td>--%>
<%--								<html:select property="idToCobertura" styleClass="cajaTexto" styleId="idToCobertura">--%>
<%--									<html:optionsCollection property="coberturas" value="id.idToCobertura" label="coberturaSeccionDTO.coberturaDTO.nombreComercial" name="igualacionPrimaNetaForm" />--%>
<%--								</html:select>--%>
<%--							</td>--%>
<%--							<td>&nbsp;</td>--%>
<%--						</tr>--%>
<%--						<tr>--%>
<%--							<th>--%>
<%--								<etiquetas:etiquetaError property="primaNeta" requerido="si"--%>
<%--												name="igualacionPrimaNetaForm" key="cotizacion.igualacion.primaNeta"--%>
<%--												normalClass="normal" errorClass="error"--%>
<%--												errorImage="/img/information.gif" />--%>
<%--							</th>--%>
<%--							<td>--%>
<%--								<midas:texto propiedadFormulario="primaNeta" onkeypress="return soloNumeros(this, event, true)" />--%>
<%--								--%>
<%--							</td>--%>
<%--							<td>&nbsp;</td>--%>
<%--							<td>--%>
<%--								<div class="alinearBotonALaDerecha">				--%>
<%--									<div id="botonAgregar">--%>
<%--										<midas:boton onclick="javascript: igualacionCuotaRequest(document.igualacionPrimaNetaForm);" tipo="agregar" texto="Igualar Cuota"/>--%>
<%--									</div>--%>
<%--								</div>--%>
<%--							</td>--%>
<%--						</tr>--%>
					</c:otherwise>
				</c:choose>
			 </c:if>
			</table>
		</midas:formulario>
	</center>
</div>