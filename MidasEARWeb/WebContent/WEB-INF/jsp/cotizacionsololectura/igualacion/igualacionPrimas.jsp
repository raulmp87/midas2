<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>

<div id="detalle" name="Detalle">
	<midas:formulario accion="/cotizacion/igualacion/igualarPrimaNeta">
	 <html:hidden property="mensajeErrorIgualacionPrima" name="igualacionPrimaNetaForm" styleId="mensajeErrorIgualacionPrima"/>
		
		<html:hidden property="mostrarOriginales" name="igualacionPrimaNetaForm" styleId="mostrarOriginales"/>
		<midas:oculto propiedadFormulario="idToCotizacion"/>
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		<html:hidden property="aplicoPrimerRiesgo" styleId="aplicoPrimerRiesgo"/>
		<html:hidden property="soloLectura" styleId="soloLectura"/>	
	<table>
		<tr>
			<td> 
				<c:choose>
					<c:when test="${igualacionPrimaNetaForm.incisos != null }">
						<html:select property="numeroInciso" styleClass="cajaTexto"  styleId="numeroInciso" onchange="filtrarPorInciso(document.igualacionPrimaNetaForm);">
							<html:option value="-1">TODOS LOS INCISOS</html:option>
							<c:set var="leyendaInciso" value="Inciso"></c:set>
							<nested:iterate property="incisos" name="igualacionPrimaNetaForm" indexId="var" id="incisosId">				
								<html:option  value="${incisosId.id.numeroInciso}">${leyendaInciso} - <nested:write property="id.numeroInciso"/></html:option>
							</nested:iterate>
						</html:select>						
					</c:when>
					<c:otherwise>
						<html:hidden property="numeroInciso" styleId="numeroInciso" value="-1"/>
					</c:otherwise>
				</c:choose>										
			</td>
		</tr>
	</table>
	<center>
			<table id="desplegarDetalle">
				<tr>
					<td colspan="4">
						<table id="t_riesgo">
							<tr>
								<th>&nbsp;</th>
								<th>Suma Asegurada</th>
								
								<c:choose>
									<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
										<th>Cuota Original</th>
										<th>Prima Neta Original</th>
									</c:when>
								</c:choose>
								
								<th>Cuota Resultante</th>
								<th>Prima Neta Resultante</th>
							</tr>
							<tr>
								<td><b>Total Cotizaci&oacute;n</b></td>
								<td>&nbsp;</td>
								
								<c:choose>
									<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
										<td>&nbsp;</td>
										<td><b><bean:write name="igualacionPrimaNetaForm" property="totalCotizacionOriginal" format="$#,##0.00" locale="idiomaLocal"/></b></td>
									</c:when>
								</c:choose>
								
								<td>&nbsp;</td>
								<td><b><bean:write name="igualacionPrimaNetaForm" property="totalCotizacion" format="$#,##0.00" locale="idiomaLocal"/></b></td>
							</tr>
							<nested:iterate id="nextSeccion" name="igualacionPrimaNetaForm" property="secciones" indexId="indexVarSecc">
								<tr>
									<td style="padding-left: 5px;"><bean:write name="nextSeccion" property="seccionDTO.nombreComercial"/></td>
									<td>&nbsp;</td>
									
									<c:choose>
										<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
											<td>&nbsp;</td>
											<td><b><bean:write name="nextSeccion" property="valorPrimaNetaOriginal" format="$#,##0.00" locale="idiomaLocal"/></b></td>
										</c:when>
									</c:choose>
									
									<td>&nbsp;</td>
									<td><b><bean:write name="nextSeccion" property="valorPrimaNeta" format="$#,##0.00" locale="idiomaLocal"/></b></td>
								</tr>
								<nested:iterate id="nextCobertura" name="nextSeccion" property="coberturaCotizacionLista" indexId="indexVarCob">
									<tr>
										<td style="padding-left: 10px;"><bean:write name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.nombreComercial"/></td>
										<td>
											<nested:equal value="1" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												<bean:write name="nextCobertura" property="valorSumaAsegurada" format="$#,##0.00" locale="idiomaLocal"/>
											</nested:equal>
											<nested:equal value="2" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												Amparada
											</nested:equal>
											<nested:equal value="3" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												<bean:write name="nextCobertura" property="valorSumaAsegurada" format="$#,##0.00" locale="idiomaLocal"/>
											</nested:equal>
										</td>
										<c:set var="cuotaOriginal" value="${nextCobertura.valorCuotaOriginal * 1000}"></c:set>
										
										<c:choose>
											<c:when test="${igualacionPrimaNetaForm.mostrarOriginales == '1' }">
												<td><bean:write name="cuotaOriginal" format="##0.0000" locale="idiomaLocal"/></td>
												<td><bean:write name="nextCobertura" property="valorPrimaNetaOriginal" format="$#,##0.00" locale="idiomaLocal"/></td>
											</c:when>
										</c:choose>
										
										<c:set var="cuota" value="${nextCobertura.valorCuota * 1000}"></c:set>
										<td><bean:write name="cuota" format="##0.0000" locale="idiomaLocal"/></td>
										<td><bean:write name="nextCobertura" property="valorPrimaNeta" format="$#,##0.00" locale="idiomaLocal"/></td>
									</tr>
								</nested:iterate>
							</nested:iterate>
						</table>
					</td>
				</tr>		
			</table>
			
			<c:choose>
				<c:when test="${igualacionPrimaNetaForm.incisos != null && igualacionPrimaNetaForm.numeroInciso != '-1'}">
					&nbsp;
				</c:when>
				<c:otherwise>
					<div class="subtituloIzquierdaDiv">Totales de Cotizaci&oacute;n</div>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td width="50%">
					</td>
					<td width="50%">
						<table id="t_riesgo" border="0">
							<tr>
								<th width="50%">Prima Neta Anual</th>
								<td width="50%" class="txt_v"><bean:write name="igualacionPrimaNetaForm" property="primaNetaAnual"/></td>
							</tr>								
							<tr>
								<th width="50%">Prima Neta Cotizaci&oacute;n</th>
								<td width="50%" class="txt_v"><bean:write name="igualacionPrimaNetaForm" property="primaNetaCotizacion"/></td>
							</tr>			
							<tr>
								<th width="50%">Recargo Financiero</th>
								<td width="50%" class="txt_v"><span id="rpf"><bean:write name="igualacionPrimaNetaForm" property="montoRecargoPagoFraccionado"/></span></td>
							</tr>
							<tr>
								<th width="50%">Gastos de Expedici&oacute;n</th>
								<td width="50%" class="txt_v"><span id="derechos"><bean:write name="igualacionPrimaNetaForm" property="derechosPoliza"/></span></td>
							</tr>		
							<tr>
								<th width="50%">I.V.A a la tasa del <bean:write name="igualacionPrimaNetaForm" property="factorIVA"/></th>
								<td width="50%" class="txt_v"><span id="iva"><bean:write name="igualacionPrimaNetaForm" property="montoIVA"/></span></td>
							</tr>	
							<tr>
								<th width="50%">Prima Total</th>
								<td width="50%" class="txt_v"><span id="primaTotal"><bean:write name="igualacionPrimaNetaForm" property="primaNetaTotal"/></span></td>
							</tr>	
						</table>
					</td>
				</tr>
			</table>
				</c:otherwise>
			</c:choose>
			<table border="0">
				<td colspan = "2">
						<div class="alinearBotonALaDerecha">
							<div id="b_reporteXLS" style="width:140px;text-align:center">
								<a href="javascript: void(0);" onclick="imprimirMemoriaCalculo('application/vnd.ms-excel');">Imprimir</a>
							</div>
						</div>
					</td>
					<td colspan = "2">
						<div class="alinearBotonALaDerecha">
							<div id="b_reportePDF" style="width:140px;text-align:center">
								<a href="javascript: void(0);" onclick="imprimirMemoriaCalculo('application/pdf');">Imprimir</a>
							</div>
						</div>
					</td>	
			</table>
		</midas:formulario>
	</center>
</div>