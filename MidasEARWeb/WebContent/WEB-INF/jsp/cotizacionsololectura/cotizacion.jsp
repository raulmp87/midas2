<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<html:hidden property="idToCotizacion" name="cotizacionForm" styleId="idToCotizacion" />
<div style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="cotizaciontabbar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >		
		<div id="datosGenerales" name="Datos Generales" extraAction="cargaTabDatosGeneralesCotSoloLectura();" ></div>
		<logic:present name="consultaEndoso">
			<div id="endoso" name="Endoso" href="http://void" extraAction="sendRequest(null,'/MidasWeb/cotizacion/endoso/consultarDatosEndoso.do?id=<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>', 'contenido_endoso', null)" ></div>
		</logic:present>
		<div width="150px" id="resumenCotizacion" name="Cotizaci&oacute;n por Inciso" extraAction="sendRequest(null,'/MidasWeb/cotizacionsololectura/resumen/mostrarResumenCotizacion.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_resumenCotizacion', null);" row="1"></div>
		<div width="150px" id="resumenIncisos" name="Datos de Incisos" extraAction="sendRequest(null,'/MidasWeb/cotizacionsololectura/listarIncisos.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&origen=COT', 'contenido_resumenIncisos', null)" row="1"></div>
		<div width="150px" id="igualacion" name="Igualaci&oacute;n" href="http://void" extraAction="sendRequest(null,'/MidasWeb/cotizacion/igualacion/mostrarIgualaciones.do?id=<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>', 'contenido_igualacion', 'inicializaAccordionIgualacionSoloLectura(<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>)')" row="1"></div>
		<logic:present name="edicionComision">
			<div id="comisiones" name="Comisiones"	extraAction="sendRequest(null,'/MidasWeb/cotizacionsololectura/comision/mostrar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&tipo=<bean:write name="edicionComision"/>', 'contenido_comisiones', 'mostrarEdicionComisionesPorCotizacionGridSoloLectura(<bean:write name="cotizacionForm" property="idToCotizacion" />,\'<bean:write name="edicionComision"/>\');')" ></div>
		</logic:present>		
		<div id="documentosAnexos" name="Anexos" extraAction="cargaTabAnexosCot(true);" row="1"></div>
		<div width="250px" id="documentosDigitalesComplementarios" name="Documentos / Aclaraciones" 	extraAction="javascript: sendRequest(null, '/MidasWeb/cotizacionsololectura/listarDocumentosDigitalesComplementarios.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />','contenido_documentosDigitalesComplementarios',null);" ></div>
		<div id="textosAdicionales" name="Textos Adicionales" extraAction="sendRequest(null,'/MidasWeb/cotizacionsololectura/mostrarTexAdicional.do','contenido_textosAdicionales','cargaComponentesTexAdicionalSoloLectura()');" ></div>		
		<logic:present name="presentaRecibos">
			<div width="130px" id="recibosCot" name="Endosos" extraAction="sendRequest(null,'/MidasWeb/cotizacionsololectura/listarRecibos.do?idPoliza=<bean:write name="cotizacionForm" property="idToPoliza" />', 'contenido_recibosCot', null)" ></div>
		</logic:present>
		<div width="150px" id="datosLicitacion" name="Datos de licitaci&oacute;n" extraAction="javascript: sendRequest(null, '/MidasWeb/cotizacionsololectura/mostrarDatosLicitacion.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />','contenido_datosLicitacion',null);" ></div>				
</div>