<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<div id="configuracion" style="width: 200px; float:left; height: 460px; overflow: auto;">
	<table  width="100%">
		<tr>
			<td>
				<div id="loadingIndicatorComps" style="display:none"></div>
				<div id="treeboxbox_tree" class="dhtmlxTree" ></div>	
			</td>
		</tr>
	</table>
</div>
<div id="configuracion_detalle" style="width: 700px;  float:left; height: 450px; overflow: auto;">
	<jsp:include page="cotizacion.jsp" flush="true"/>
</div>	


<div style="clear:both"></div>
