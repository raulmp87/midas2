<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<midas:formulario accion="/cotizacion/comision/guardarComisionCotizacion">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion" name="comisionCotizacionForm"/>
	<div class="subtituloIzquierdaDiv">Comisiones </div>
	<table id="agregar" border="0">
		<tr>
			<td height="180">
				<div id="editarComisionesCotizacionGrid" class="dataGridConfigurationClass" height="95%" width="98%"></div>
			</td>
		</tr>
		
	</table>
	
</midas:formulario>