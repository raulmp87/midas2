<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:formulario accion="/cotizacion/comision/guardarComisionCotizacion">
	<midas:oculto propiedadFormulario="idToCotizacion" nombreFormulario="comisionCotizacionForm"/>
	<table id="agregar" width="100%" border="0">
		<tr>
			<td class="titulo" colspan="6">Comisiones <html:hidden property="idToCotizacion" styleId="idToCotizacion" /> </td>
		</tr>
		<tr>
			<td height="350">
				<div id="autorizarComisionesCotizacionGrid" class="dataGridConfigurationClass" height="90%" width="97%"></div>
			</td>
		</tr>
		
	</table>
</midas:formulario>