<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<input type='hidden' id='idToCotizacion' value='<bean:write name="idToCotizacion" scope="request" />' name=''/>
<div class="subtituloCotizacion"><bean:message key="configuracion.cobertura.de"/>:<bean:write name="tituloCoberturas" scope="request" /> </div>
<div class="subtituloCotizacion">
	<bean:message key="configuracion.cobertura.cotizacion"/>: <bean:message key="midas.cotizacion.prefijo"/><bean:write name="idToCotizacion" scope="request" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<bean:message key="configuracion.cobertura.fecha"/>: <bean:write name="fecha" scope="request" />
</div>
<div style="clear:both"></div>

<table id="desplegarDetalle">
	<tr>
		<td width="100%">
			<div id="autorizacionCotizacionCoberturasGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
	<tr>
		<td width="100%">
			<div class="alinearBotonALaDerecha">				
				<div id="botonAgregar">
					<midas:boton onclick="javascript: coberturaAutorizadoProcessor.sendData();" tipo="agregar" texto="Autorizar"/>
					<midas:boton onclick="javascript: coberturaRechazadoProcessor.sendData();" tipo="agregar" texto="Rechazar"/>
					<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id=' + document.getElementById('idCotizacion').value, 'contenido','creaArbolCotizacion(' + document.getElementById('idCotizacion').value +');dhx_init_tabbars();');" tipo="regresar"/>
				</div>
			</div>
		</td>
	</tr>	
</table>


