<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
	<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
	<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
	<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
	<link href="<html:rewrite page="/css/dhtmlxcalendar_yahoolike.css"/>" rel="stylesheet" type="text/css">		
	
	<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
	
	<sj:head/>
	<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree_sb.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_srnd.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_sub_row.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_nxml.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>	
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
	<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
	<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/colonia/colonia.js"/>"></script>	
	<script type="text/javascript" src="<html:rewrite page="/js/cliente/cliente.js"/>"></script>	
	<script type="text/javascript" src="<html:rewrite page="/js/danios/reportes/reporteMonitoreoSolicitudes.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/danios/cotizacion/datosRiesgo.js"/>"></script>	
	<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/anexos/anexos.js"/>"></script>		
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_dhxcalendar.js"/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>		
	<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
   	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
	<script src="<s:url value='/js/midas2/util.js'/>"></script>
	<script type="text/javascript" src="<html:rewrite page="/js/midas2/componente/componente.js"/>"></script>
	<script src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
	
	<script src="<s:url value='/js/midas2/cliente/busquedaCliente.js'/>"></script>
	<script src="<s:url value='/js/midas2/busquedaGenerica.js'/>"></script>
	<script src="<s:url value='/js/negocio/grupoCliente.js'/>"></script>
	
	<script src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionExpress.js'/>"></script>
	<script src="<s:url value='/js/midas2/impresiones/impresionesM2.js'/>"></script>		
	<script src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionExpress.js'/>"></script>
	<script src="<s:url value='/js/midas2/catalogos/persona.js'/>"></script>
	
    <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
    <script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>"></script>
	<script type="text/javascript">
		jQuery.noConflict();
	</script>
	
	<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
	<script src="<s:url value='/js/midas2/componente/direccionSiniestroMidas.js'/>"></script>