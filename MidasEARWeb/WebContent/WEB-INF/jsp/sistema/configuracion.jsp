<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<div id="configuracion" style="width: 200px; float:left; height: 460px; overflow: auto;">
	<table  width="100%">
		<tr>
			<td><div class="titulo" id="tituloSuperior" style="display:none"></div></td>
		</tr>
		<tr>
			<td>
				<div id="divSuperior" style="width:100%;" style="display:none">
					<!-- Div para insertar contenido dinámicamente, no removerlo -->
				</div>
				<hr/>
			</td>
		</tr>
		<tr>
			<td>
				<div id="loadingIndicatorComps" style="display:none"></div>
				<div class="dhtmlxTree" id="treeboxbox_tree" style="height:460px"></div>
			</td>
		</tr>
	</table>
</div>
<div id="configuracion_detalle" style="width: 700px;  float:left; height: 460px; overflow: auto;"></div>	
<div style="clear:both"></div>
