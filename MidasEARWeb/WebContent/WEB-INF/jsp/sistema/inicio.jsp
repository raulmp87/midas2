<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<%@ page isELIgnored="false"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<html>
	<head>
		<meta content="IE=9" http-equiv="X-UA-Compatible">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<title>Midas</title>
		
		
		
		<link href="<html:rewrite page="/css/estructura.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/epoch_styles.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtree.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxaccordion_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxmenu_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxtabbar.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxvault.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">	
		<link href="<html:rewrite page="/css/dhtmlxcalendar_yahoolike.css"/>" rel="stylesheet" type="text/css">		
		<link href="<html:rewrite page="/css/dhtmlxcombo.css"/>" rel="stylesheet" type="text/css">			
		<link href='<s:url value="/css/gridiculousLite.css"/>' rel="stylesheet" type="text/css">
		<link href="${pageContext.request.contextPath}/css/toastr.css" rel="stylesheet" type="text/css">
		<link href="${pageContext.request.contextPath}/img/favicon.ico" rel="shortcut icon" type="image/png" />	
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript">
			delete Array.prototype.toJSON;
			var config = {
				contextPath: '${pageContext.request.contextPath}',
	            baseUrl: location.protocol + "//" + location.host + '${pageContext.request.contextPath}'
	        };
	        var user = {
	        	userName : '<bean:write name="usuarioAccesoMIDAS" property="nombreUsuario" scope="session"/>',
	        	name : '<bean:write name="usuarioAccesoMIDAS" property="nombre" scope="session"/>'
	        };
	        //funcion que simula JSON nativo de javascript ya que no funciona en internet explorer 6,7 y 8
	        //la funcion JSON se utiliza en el jquery.cometd.js 
	        //ver la liga para mayor info https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/JSON
	        if (!window.JSON) {
				  window.JSON = {
				    parse: function (sJSON) { return eval("(" + sJSON + ")"); },
				    stringify: function (vContent) {
				      if (vContent instanceof Object) {
				        var sOutput = "";
				        if (vContent.constructor === Array) {
				          for (var nId = 0; nId < vContent.length; sOutput += this.stringify(vContent[nId]) + ",", nId++);
				            return "[" + sOutput.substr(0, sOutput.length - 1) + "]";
				        }
				        if (vContent.toString !== Object.prototype.toString) { 
				          return "\"" + vContent.toString().replace(/"/g, "\\$&") + "\"";
				        }
				        for (var sProp in vContent) { 
				          sOutput += "\"" + sProp.replace(/"/g, "\\$&") + "\":" + this.stringify(vContent[sProp]) + ",";
				        }
				        return "{" + sOutput.substr(0, sOutput.length - 1) + "}";
				     }
				     return typeof vContent === "string" ? "\"" + vContent.replace(/"/g, "\\$&") + "\"" : String(vContent);
				    }
				  };
				}
		</script>		
		<sj:head/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/cometd/cometd.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/cometd/jquery.cometd.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/cometd/cometdUtil.js"></script>	    
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/soundmanager2/soundmanager2-nodebug-jsmin.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/inicio.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/siniestro/movil/siniestroMovil.js"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/toastr.js"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptDataGrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>" charset="ISO-8859-1"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/epoch.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/mask.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtree_sb.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_splt.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_srnd.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_sub_row.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_nxml.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxmenu.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxvault.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxtabbar_start.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ext/dhtmlxgrid_pgn.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/reaseguroImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/reporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/correo/configCorreoSiniestro.js"/>"></script> 
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/desplegarReporteSiniestro.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxUtil.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dataProcessorEventHandlers.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestroFinanzasImports.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/endoso/solicitudEndoso.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacionsololectura/cotizacionSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ordentrabajosololectura/ordenTrabajoSoloLectura.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/colonia/colonia.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cliente/cliente.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/danios/reportes/reporteMonitoreoSolicitudes.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/danios/cotizacion/datosRiesgo.js"/>"></script>	
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/anexos/anexos.js"/>"></script>		
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_dhxcalendar.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_filter.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcombo.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_combo.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reportes/reporteHistorialPolizas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/reportes/reporteHistorialCobranza.js"/>"></script>
		<script	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/referenciaBancaria.js'/>"></script>
				
		<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
    	<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
		<script src="<s:url value='/js/midas2/util.js'/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/midas2/componente/componente.js"/>"></script>
		<script src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>
		
		<script src="<s:url value='/js/midas2/cliente/busquedaCliente.js'/>"></script>
		<script src="<s:url value='/js/midas2/busquedaGenerica.js'/>"></script>
		<script src="<s:url value='/js/negocio/grupoCliente.js'/>"></script>
		
		<script src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionExpress.js'/>"></script>
		<script src="<s:url value='/js/midas2/impresiones/impresionesM2.js'/>"></script>		
		<script src="<s:url value='/js/midas2/suscripcion/cotizacion/cotizacionExpress.js'/>"></script>
		<script src="<s:url value='/js/midas2/catalogos/persona.js'/>"></script>
		<script src="<s:url value='/js/midas2/catalogos/tipousovehiculo.js'/>"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/enlace/enlace.js"/></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/cliente/poliza/clientePoliza.js"/></script>	
		
	    <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
	    <script type="text/javascript" src="<s:url value="/js/jQValidator.js"/>" charset="ISO-8859-1"></script>
	    <script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-util.js'/>"></script>
	    <script src="<s:url value='/js/midas2/tarifa/tarifaversion.js'/>"></script>
	    <script type="text/javascript" src="<s:url value='/js/date.js'/>"></script>
	    
	    <script type="text/javascript" src="<html:rewrite page="/js/midas2/util.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/midas2/poliza/auto/poliza.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/midas2/cobranza/recibos/consolidar/operacionesConsolidadas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/jquery.mask.js"/>"></script>
	    
	


<script src="<s:url value='/js/midas2/componente/componentedireccion.js'/>"></script>
<script src="<s:url value='/js/midas2/componente/direccion/direccionSiniestroMidas.js'/>"></script>

		
	<script type="text/javascript">
		jQuery.noConflict();
		jQuery(function(){
			cargandoMenu(); 
		});
	</script>	
	</head>
	<body onload="">
		<jsp:useBean id="currentDate" class="java.util.Date" scope="page" />
		<div id="loading" style="display: none;">
			<img id="img_indicator" name="img_indicator"
				src="<s:url value='/img/as2.gif'/>" alt="Afirme" />
		</div>
		<div id="blockWindow" class="backgroundFilter" style="display: none;"></div>
		<div id="central_indicator" class="sh2" style="display: none;">
			<img id="img_indicator" name="img_indicator"
				src="<html:rewrite page='/img/as2.gif'/>" alt="Afirme" />
		</div>
		
		
		<div id="encabezado">
			<div id="logo"></div>
			<div id="contenedorDeDatosDeUsuarioRegistrado" style="">
				<div id="datosDeUsuarioRegistrado">
					<b>Bienvenido:</b>
					<bean:write name="usuarioAccesoMIDAS" property="nombre" scope="session"/>
					<bean:write name="currentDate" format="dd' de 'MMMM' de 'yyyy"
						scope="page" />
						| <b><html:link action="/sistema/logout.do" styleClass="">
								<font color="white">Salir</font>
							</html:link>
						</b>
				</div>
			</div>
		</div>
		<div id="menu">
			<div id="menuObj">
				menu
			</div>
		</div>
		<center>
			<div id="page">
				<div id="encabezado_bg"></div>
				<div id="contenido_bg">
					<div id="contenido">
						
					</div>
				</div>
				<div id="pie_bandeja_bg"></div>
			</div>
			<div id="pie_txt">
				<p>
					Seguros Afirme
					<bean:write name="currentDate" format="yyyy" />
				</p>
			</div>
		    <div id="mensaje_popup" >
		    	<jsp:include page="mensaje.jsp" flush="true" />	
		    </div>    	
	    </center>	
	    
	 <logic:present name="abreModal" scope="request">
		<script type="text/javascript">
			mostrarAcercaDeWindow();	 
		</script>	
	</logic:present>
		
	<logic:present name="ejecutaAgregar" scope="request">
		<script type="text/javascript">
		mostrarAdjuntarArchivoSistema();	 
		</script>	
	</logic:present>
	
	<logic:present name="ejecutaAsociarSol" scope="request">
		<script type="text/javascript">
		mostrarAdjuntarArchivoSolicitudSistemaWindow();
		</script>	
	</logic:present>
		
	<logic:present name="ejecutaAsociarCot" scope="request">
		<script type="text/javascript">
		mostrarAdjuntarDocumentoDigitalComplementarioSistemaWindow();
		</script>	
	</logic:present>
	
	<logic:present parameter="iniciarCotizadorAgentes">
		<script type="text/javascript">
			sendRequestJQ(null, '/MidasWeb/suscripcion/cotizacion/auto/cotizadoragente/mostrarContenedor.action','contenido', null);
		</script>
	</logic:present>
	<logic:present parameter="iniciarNuevoCotizadorAgentes">
		<script type="text/javascript">
			window.open('/MidasWeb/suscripcion/cotizacion/auto/agentes/mostrarContenedor.action');
		</script>
	</logic:present>
	    <%--		<script type="text/javascript">mostrarMensajesPendientesWindow();</script>--%>
	</body>
</html>