<?xml version="1.0" encoding="ISO-8859-1"?>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<%@  page contentType="text/xml" %>
<%@ page isELIgnored="false"%>
<data>
	<action type="${respuestaXMLForm.tipoRespuesta}" 
			sid="${respuestaXMLForm.idOriginal}" 
			tid="${respuestaXMLForm.idResultado}"
			operacionExitosa="${respuestaXMLForm.operacionExitosa}"
			tipoMensaje="${respuestaXMLForm.tipoMensaje}"
			>
		${respuestaXMLForm.mensaje}
	</action>
</data>