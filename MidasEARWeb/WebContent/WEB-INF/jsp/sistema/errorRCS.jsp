<html>
  <head>
    <title>Error</title>
    <style>
    .error_mensaje {
	height: 65px;
	width: 450px;
	border: 1px solid #01AA4F;
	padding: 10px;
}
.error_mensaje   h1{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #333333;
	font-size: 14px;
}

    .error_mensaje h2{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #666666;
	font-size: 12px;
}
.error_mensaje h3{
	color: #01AA4F;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
}

    </style>
  </head>
  
  <body>
  <div class="error_mensaje">
  	<h1><!-- 
  		<=request.getAttribute("titulo")%>
  		 -->
  	</h1>
  	<p>
  	<%=request.getAttribute("leyenda")%>
  	</p>
  	</div>
  </body>
</html>
