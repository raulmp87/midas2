<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html:hidden property="idToCotizacion" name="cotizacionForm" styleId="idToCotizacion" />

<div hrefmode="ajax-html"  style="height: 450px;width: 100%; " id="ordenTrabajoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#F4F3EE,#FCFBFC" >
	<div width="150px" id="desplegarDetalle" name="Datos Generales" extraAction="sendRequest(null,'/MidasWeb/ordentrabajosololectura/mostrarDatosODT.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_desplegarDetalle', 'inicializaObjetosEdicionOTSoloLectura(<bean:write name="cotizacionForm" property="idToCotizacion" />);');"></div>
	
	<logic:equal value="true" property="editaIncisos" name="cotizacionForm">
		<div width="150px" id="resumenIncisos" name="Datos de Incisos" extraAction="sendRequest(null,'/MidasWeb/ordentrabajosololectura/listarIncisos.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&origen=ODT', 'contenido_resumenIncisos', null)"></div>
	</logic:equal>
	
	
</div>
<div id="origen" style="display: none;">ODT</div>