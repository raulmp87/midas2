<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:tabla idTabla="subIncisoCotizacionTabla"
	claseDecoradora="mx.com.afirme.midas.decoradores.SubIncisoCotizacion"
	claseCss="tablaConResultados" nombreLista="subIncisoDTOList">
	<midas:columna propiedad="id.numeroSubInciso" titulo="Id" />
	<midas:columna propiedad="descripcionSubInciso" titulo="Bien" />
<%--	<midas:columna propiedad="cantidad" titulo="Cantidad"/>--%>
	<midas:columna propiedad="valorSumaAsegurada" titulo="Suma Asegurada" formato="${0,number,#,##0.00}"/>
	<midas:columna propiedad="accionesSoloLecturaODT" /> 
</midas:tabla>