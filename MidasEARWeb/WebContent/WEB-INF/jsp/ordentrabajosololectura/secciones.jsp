<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<html:messages id="struts1Messages"></html:messages>
<midas:formulario accion="/cotizacion/mostrarModificarSecciones">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion" />
	<html:hidden property="numeroInciso" styleId="numeroInciso" />
	<html:hidden property="idToSeccion" styleId="idToSeccion" />
	<html:hidden property="numeroSubInciso" styleId="numeroSubInciso" />
	
	<div class="subtituloCotizacion"><midas:mensaje clave="midas.cotizacion.secciones" /></div>
	<div class="subtituloCotizacion"><midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo" />: <bean:message key="configuracion.cobertura.ordentrabajo.prefijo"/><bean:write name="idToCotizacion" scope="request" />
	&nbsp;&nbsp;<midas:mensaje clave="midas.cotizacion.fecha" />: <midas:escribe propiedad="fechaCreacion" nombre="seccionCotForm" />
	</div>
	<div style="clear:both"></div>

	<table id="desplegarDetalle">
	<tr>
		<td width="100%">
			<div id="configuracion_secciones" style="display:block; width: 650px">
				<jsp:include page="seccionesPorInciso.jsp" flush="true" />
			</div>
		</td>
	</tr>
</table>
</midas:formulario>


