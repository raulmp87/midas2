<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<table id="desplegarDetalle">
	<tr>
		<td width=100%>
			<div id="seccionesPorIncisoGrid" width="650px" height="150px"
				style="background-color:white; overflow:hidden; margin-left:auto; margin-right:auto;"></div>
		</td>
	</tr>
	<tr height="30px"></tr>
	<tr>
		<td class="titulo" width="100%">
			<midas:mensaje clave="midas.cotizacion.subIncisos" />
			
		</td>
	</tr>
	<tr>
		<td>
			<div id="resultados">
				Seleccione una Secci&oacute;n para desplegar sus SubIncisos
			</div>
		</td>
	</tr>
</table>