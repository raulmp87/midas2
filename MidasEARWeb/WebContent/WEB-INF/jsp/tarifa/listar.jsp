<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<div id="displayTagTarifa">
<html:form action="/tarifa/configuracion/listar.do">
	<center>
       <div id="resultados">
       				<midas:oculto propiedadFormulario="idToRiesgo" nombreFormulario="riesgoForm"/>
					<midas:oculto propiedadFormulario="idConcepto" nombreFormulario="riesgoForm"/>
			<midas:tabla idTabla="tarifasTabla"
				claseDecoradora="mx.com.afirme.midas.decoradores.Tarifa" claseCss="tablaConResultados" nombreLista="riesgoForm.tarifas"
				urlAccion="/tarifa/configuracion/listar.do">
				<midas:columna propiedad="valor1" titulo="tarifa.valor1" />
				<midas:columna propiedad="valor2" titulo="tarifa.valor2" />
				<midas:columna propiedad="valor3" titulo="tarifa.valor3" />
				<midas:columna propiedad="valor4" titulo="tarifa.valor4" />
				<midas:columna propiedad="valor" titulo="tarifa.valor5" />
				<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
			</midas:tabla>
		</div>
	</center>
	<div id="tarifaGrid" width="97%" height="125"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	<div id="botonAgregar">
		<div class="alinearBotonALaDerecha">
			<div id="b_agregar">
				<a href="javascript: void(0);"
					onclick="javascript: mostrarAgregarTarifa(<midas:escribe propiedad="idToRiesgo" nombre="riesgoForm"/>,<midas:escribe propiedad="idConcepto" nombre="riesgoForm"/>,<midas:escribe propiedad="version" nombre="riesgoForm"/>);">
					<midas:mensaje clave="midas.accion.agregar"/>
				</a>
			</div>
		</div>
	</div>

</html:form>
</div>