<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>

<midas:formulario accion="/tarifa/configuracion/modificar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.modificar.tarifa" />
				<midas:oculto propiedadFormulario="idToRiesgo" nombreFormulario="tarifaForm"/>
				<midas:oculto propiedadFormulario="idConcepto" nombreFormulario="tarifaForm"/>
				<midas:oculto propiedadFormulario="idBase1" nombreFormulario="tarifaForm"/>
				<midas:oculto propiedadFormulario="idBase2" nombreFormulario="tarifaForm"/>
				<midas:oculto propiedadFormulario="idBase3" nombreFormulario="tarifaForm"/>
				<midas:oculto propiedadFormulario="idBase4" nombreFormulario="tarifaForm"/>
				<midas:oculto propiedadFormulario="version" nombreFormulario="tarifaForm"/>
				
			</td>
		</tr>
		<tr>
			<th><midas:etiquetaTarifa  idBase="1"/></th>
			<td>
				<logic:notEqual value="" property="descripcionBase1" name="tarifaForm" >
					<midas:texto propiedadFormulario="descripcionBase1" soloLectura="true"/>
				</logic:notEqual>
			</td>
		</tr>
		<tr>

			<th><midas:etiquetaTarifa idBase="2"/></th>
			<td>
				<logic:notEqual value="" property="descripcionBase2" name="tarifaForm" >
					<midas:texto propiedadFormulario="descripcionBase2" soloLectura="true"/>
				</logic:notEqual>
			</td>
		</tr>
		<tr>
			<th><midas:etiquetaTarifa  idBase="3"/></th>
			<td>
				<logic:notEqual value="" property="descripcionBase3" name="tarifaForm" >
					<midas:texto propiedadFormulario="descripcionBase3" soloLectura="true"/>
				</logic:notEqual>
			</td>
		</tr>
		<tr>
			<th><midas:etiquetaTarifa idBase="4"/></th>
			<td>
				<logic:notEqual value="" property="descripcionBase4" name="tarifaForm" >
					<midas:texto propiedadFormulario="descripcionBase4" soloLectura="true"/>
				</logic:notEqual>
			</td>
		</tr>
		<tr>
			<th><midas:etiquetaTarifa idBase="5"/></th>
			<td><midas:texto propiedadFormulario="valor"/></td>
		</tr> 	
		<tr>
			<td class="campoRequerido">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: cerrarVentanaTarifa();">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<div id="b_agregar">
						<a href="javascript: void(0);" onclick="javascript: modificaTarifa(document.tarifaForm);">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>				
				</div>
			</td>      		
		</tr>				
	</table>

</midas:formulario>