<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/cobertura/mostrarExclusionAumento">
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.aumento.riesgo" /></td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.riesgo" /></center></th>
				</tr>	
				<tr>
					<td colspan="4">
						<div id="exclusionesAumentoCoberturaAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>	
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.riesgo.disponible" /></center></th>
				</tr>
				<tr>
					<td>Riesgo:<html:hidden property="idToCobertura" styleId="idToCobertura" /><html:hidden property="idToSeccion" styleId="idToSeccion" /></td>
					<td>
						<midas:combo styleClass="cajaTexto" propiedad="descripcionSubRamo" id="riesgoSelect_aum" onchange="mostrarExclusionesAumentoCoberturaPorRiesgo(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="coberturaForm"  property="riesgosAsociados" value="idToRiesgo" label="nombreComercial"/>
						</midas:combo>
					</td>
					<td>Aumento:</td>
					<td>
						<midas:combo styleClass="cajaTexto" propiedad="descripcionSubRamo" id="aumentoSelect" onchange="mostrarExclusionesAumentoCoberturaPorAumento(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="coberturaForm"  property="aumentosAsociados" value="id.idtoaumentovario" label="aumentoVarioDTO.descripcionAumento"/>
						</midas:combo>
					</td>					
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesAumentoCoberturaNoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>	
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: exclusionAumentoCoberturaProcessor.sendData(); configuracionCoberturaTabBar.setTabActive('detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>				