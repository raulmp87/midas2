<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/cobertura/mostrarAsociarRiesgo">
			<midas:oculto propiedadFormulario="idToCobertura" nombreFormulario="coberturaForm"/>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.riesgo" /> Cobertura</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.riesgo.asociado" /> Cobertura</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="riesgosAsociadosCoberturaGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>	
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.riesgo.disponible" /> Cobertura</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="riesgosPorAsociarCoberturaGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>			
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: riesgoCoberturaProcessor.sendData(); if(!riesgoCoberturaError){ configuracionCoberturaTabBar.setTabActive('detalle'); refreshTree();}"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>			