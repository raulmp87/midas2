<%@ page isELIgnored="false"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<div id="ventana">
<midas:formulario accion="/configuracion/cobertura/editarRelacionRiesgoCobertura">
	<table id="desplegar">
       	<tr>
           	<td class="titulo" colspan="3">
           		<midas:mensaje clave="configuracion.cobertura.riesgo.titulo"/>
           		<midas:oculto propiedadFormulario="idtoseccion"/>
           		<midas:oculto propiedadFormulario="idtocobertura"/>
           		<midas:oculto propiedadFormulario="idtoriesgo"/>
           	</td>
       	</tr>
		<tr>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.descripcion"/> --%>
				<etiquetas:etiquetaError property="descripcionRiesgoCobertura" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.descripcion"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="3"><midas:texto propiedadFormulario="descripcionRiesgoCobertura" nombreFormulario="riesgoCoberturaForm" caracteres="200" onkeypress="soloAlfanumericos(this,event,false)" /></td>
		</tr>
		<tr>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.claveObligatoriedad"/>*: --%>
				<etiquetas:etiquetaError property="claveObligatoriedad" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.claveObligatoriedad"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="2"> <midas:comboValorFijo grupoValores="34" propiedad="claveObligatoriedad" nombre="riesgoCoberturaForm" styleClass="cajaTexto" /> </td>
			<td></td>
		</tr>
		<tr>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.claveTipoCoaseguro"/>*: --%>
				<etiquetas:etiquetaError property="claveTipoCoaseguro" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.claveTipoCoaseguro"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td width="25%"><midas:comboValorFijo grupoValores="36" propiedad="claveTipoCoaseguro" nombre="riesgoCoberturaForm" styleClass="cajaTexto"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMinimoCoaseguro"/>*: --%>
				<etiquetas:etiquetaError property="valorMinimoCoaseguro" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMinimoCoaseguro"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td width="17%"><midas:texto propiedadFormulario="valorMinimoCoaseguro" onkeypress="return soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMaximoCoaseguro"/>*: --%>
				<etiquetas:etiquetaError property="valorMaximoCoaseguro" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMaximoCoaseguro"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td width="9%"><midas:texto propiedadFormulario="valorMaximoCoaseguro" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
		</tr>
		<tr>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.claveTipoLimiteCoaseguro"/>*: --%>
				<etiquetas:etiquetaError property="claveTipoLimiteCoaseguro" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.claveTipoLimiteCoaseguro"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:comboValorFijo grupoValores="37" propiedad="claveTipoLimiteCoaseguro" nombre="riesgoCoberturaForm" styleClass="cajaTexto"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMinimoLimiteCoaseguro"/>*: --%>
				<etiquetas:etiquetaError property="valorMinimoLimiteCoaseguro" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMinimoLimiteCoaseguro"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMinimoLimiteCoaseguro" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMaximoLimiteCoaseguro"/>*: --%>
				<etiquetas:etiquetaError property="valorMaximoLimiteCoaseguro" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMaximoLimiteCoaseguro"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMaximoLimiteCoaseguro" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
		</tr>
		<tr>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.claveTipoDeducible"/>*: --%>
				<etiquetas:etiquetaError property="claveTipoDeducible" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.claveTipoDeducible"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:comboValorFijo grupoValores="38" propiedad="claveTipoDeducible" nombre="riesgoCoberturaForm" styleClass="cajaTexto"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMinimoDeducible"/>*: --%>
				<etiquetas:etiquetaError property="valorMinimoDeducible" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMinimoDeducible"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMinimoDeducible" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMaximoDeducible"/>*: --%>
				<etiquetas:etiquetaError property="valorMaximoDeducible" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMaximoDeducible"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
				</th>
			<td><midas:texto propiedadFormulario="valorMaximoDeducible" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
		</tr>
		<tr>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.claveTipoLimiteDeducible"/>*: --%>
				<etiquetas:etiquetaError property="claveTipoLimiteDeducible" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.claveTipoLimiteDeducible"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:comboValorFijo grupoValores="39" propiedad="claveTipoLimiteDeducible" nombre="riesgoCoberturaForm" styleClass="cajaTexto"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMinimoLimiteDeducible"/>*: --%>
				<etiquetas:etiquetaError property="valorMinimoLimiteDeducible" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMinimoLimiteDeducible"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMinimoLimiteDeducible" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
			<th>
<%-- 				<midas:mensaje clave="configuracion.cobertura.riesgo.valorMaximoLimiteDeducible"/>*: --%>
				<etiquetas:etiquetaError property="valorMaximoLimiteDeducible" requerido="si"
					name="riesgoCoberturaForm" key="configuracion.cobertura.riesgo.valorMaximoLimiteDeducible"
					normalClass="normal" errorClass="errorL"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMaximoLimiteDeducible" onblur="soloNumeros(this, event, true, false, 16,4)" caracteres="21"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.cobertura.riesgo.claveAutorizacion" />:</th>
			<td><midas:check propiedadFormulario="claveAutorizacion" id="claveAutorizacion"/></td>
			<th><midas:mensaje clave="configuracion.cobertura.riesgo.claveImporteCero" />:</th>
			<td><midas:check propiedadFormulario="claveImporteCero" id="claveImporteCero"/></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3" class="campoRequerido"><midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" /></td>
			<td class="guardar">
			<div class="alinearBotonALaDerecha">
<%--				<midas:boton onclick="javascript: guardarRiesgoCobertura(document.riesgoCoberturaForm);" tipo="guardar"/>--%>
				<div id="b_guardar">
					<a href="javascript: void(0);"
					onclick="javascript: guardarRiesgoCobertura(document.riesgoCoberturaForm);">
					<midas:mensaje clave="midas.accion.guardar"/>
					</a>
				</div>
			</div>
			</td>
		</tr>
		</table>
</midas:formulario>
</div>