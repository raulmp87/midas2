<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<midas:formulario  accion="/catalogos/cobertura/listar">
	<table width="98%" id="filtros">
			<midas:oculto propiedadFormulario="claveNegocio"/>
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Coberturas
				<logic:equal value="A" name="coberturaForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="coberturaForm" property="claveNegocio">
				Da�os
				</logic:notEqual>
				
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.cobertura.codigo"/>:</th>
			<td><midas:texto propiedadFormulario="codigo" 
					onkeypress="return soloNumeros(this, event, false, false)" id="codigo" caracteres="8"/>
			</td>
			<th><midas:mensaje clave="configuracion.cobertura.version"/>:</th>
			<td><midas:texto propiedadFormulario="version" 
					onkeypress="return soloNumeros(this, event, false)" id="version" caracteres="4"/>
			</td>
			<th><midas:mensaje clave="configuracion.cobertura.descripcion"/>:</th>
			<td>
				<html:text property="descripcion" maxlength="200"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="configuracion.cobertura.nombrecomercial"/>:</th>
			<td>
				<html:text property="nombreComercial" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" />

			</td>
			<th><midas:mensaje clave="configuracion.cobertura.clavetiposumaasegurada"/>:</th>
			<td>
				<midas:comboValorFijo grupoValores="24" styleClass="cajaTexto"
					propiedad="claveTipoSumaAsegurada" nombre="coberturaForm" 
					onchange="comboCoberturaSumaAsegurada(this)" />
			</td>
		</tr> 		
		<tr>
			<th><midas:mensaje clave="configuracion.cobertura.idtcramo"/>:</th>
			<td><midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto"
					onchange="getSubRamos(this,'idTcSubRamo');"	/>
			</td>
			<th><midas:mensaje clave="configuracion.cobertura.idtcsubramo"/>:</th>
			<td><midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto"/>
			</td>		
			<th><midas:mensaje clave="configuracion.cobertura.clavedesglosariesgos"/>:</th>
			<td><midas:check propiedadFormulario="claveDesglosaRiesgos" id="claveDesglosaRiesgos"/></td>					
		</tr>
		<tr>
			<th colspan="2"><midas:mensaje clave="configuracion.cobertura.claveimportecero"/>:
							<midas:check propiedadFormulario="claveImporteCero" id="claveImporteCero"/>
			</th>
			<th><midas:mensaje clave="configuracion.cobertura.mostrarInactivos"/></th>
			<td><midas:checkBox valorEstablecido="1" propiedadFormulario="mostrarInactivos"/></td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.coberturaForm,'/MidasWeb/catalogos/cobertura/listarFiltrado.do?claveNegocio=A', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
	</table>
	</br>
	<div id="resultados">
		<midas:tabla idTabla="productos"
			claseDecoradora="mx.com.afirme.midas.decoradores.Cobertura"
			claseCss="tablaConResultados" nombreLista="coberturas"
			urlAccion="/catalogos/cobertura/listarFiltrado.do">
			<midas:columna propiedad="codigo" titulo="C&oacute;digo"/>
			<midas:columna propiedad="version" titulo="Versi&oacute;n"/>
			<midas:columna propiedad="nombreComercial" titulo="Nombre" maxCaracteres="100" />
			<midas:columna propiedad="acciones" />
					
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.coberturaForm,'/MidasWeb/catalogos/cobertura/mostrarAgregar.do', 'contenido','desabilitaCampos()');">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
