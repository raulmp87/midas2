<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/cobertura/mostrarExclusionAumento">
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.descuento.riesgo" /></td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.riesgo" /></center></th>
				</tr>	
				<tr>
					<td colspan="4">
						<div id="exclusionesDescuentoCoberturaAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.riesgo.disponible" /></center></th>
				</tr>
				<tr>
					<td>Riesgo:<html:hidden property="idToCobertura" styleId="idToCobertura" /><html:hidden property="idToSeccion" styleId="idToSeccion" /></td>
					<td width="60%">
						<midas:combo styleClass="cajaTexto" propiedad="descripcionSubRamo" id="riesgoSelect_des" onchange="mostrarExclusionesDescuentoCoberturaPorRiesgo(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="coberturaForm"  property="riesgosAsociados" value="idToRiesgo" label="nombreComercial"/>
						</midas:combo>
					</td>
					<td>Descuento:</td>
					<td width="25%">
						<midas:combo styleClass="cajaTexto" propiedad="descripcionSubRamo" id="descuentoSelect" onchange="mostrarExclusionesDescuentoCoberturaPorDescuento(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="coberturaForm"  property="descuentosAsociados" value="id.idtodescuentovario" label="descuentoDTO.descripcion"/>
						</midas:combo>
					</td>					
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesDescuentoCoberturaNoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>			
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
					<a href="javascript: void(0);" 
						onclick="javascript: exclusionDescuentoCoberturaProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionCoberturaTabBar, 'detalle');">
						<midas:mensaje clave="midas.accion.guardar"/>
					</a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>		