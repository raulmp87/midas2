<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario accion="/catalogos/cobertura/modificar">
	<html:hidden property="mensaje" styleId="mensaje" name="coberturaForm"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje" name="coberturaForm"/>
	<midas:oculto propiedadFormulario="claveNegocio"/>
	
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.modificar" /> Cobertura 
				<logic:equal value="A" name="coberturaForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="coberturaForm" property="claveNegocio">
				Da�os
				</logic:notEqual>				
				<midas:oculto propiedadFormulario="idToCobertura" nombreFormulario="coberturaForm"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="codigo" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.codigo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="120px">
				<midas:texto propiedadFormulario="codigo"  soloLectura="true"
					id="codigo" caracteres="8"
					onkeypress="return soloNumeros(this, event, false, false)" />
			</td>
			<th width="100px">
				<etiquetas:etiquetaError property="nombreComercial" 
					requerido="si" name="coberturaForm"
					key="configuracion.cobertura.nombrecomercial"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="450px" colspan="3">
				<html:text property="nombreComercial" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="descripcion" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.descripcion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
				</th>
			<td colspan="5">
				<html:text property="descripcion" maxlength="200"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr> 		
		<tr>
			<th colspan="6" style="text-align: center;">Documento CNSF<br></th></tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="descripcionRegistroCNSF" requerido="si" name="coberturaForm" 
				key="configuracion.cobertura.descripcionRegistroCNSF" normalClass="normal" errorClass="error"
				errorImage="/img/information.gif" />
			</th>
			<td colspan=""><midas:texto propiedadFormulario="descripcionRegistroCNSF" 
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="200"/>
			</td>
					
			<th>
				<etiquetas:etiquetaError property="fechaRegistro" requerido="si" name="coberturaForm" 
				key="catalogos.configuracion.fecharegistro" normalClass="normal" errorClass="error"
				errorImage="/img/information.gif" />
			</th>
			<td colspan="">
				<midas:texto propiedadFormulario="fechaRegistro" 
					onkeypress="return soloFecha(this, event, false);"
							 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							 onblur="esFechaValida(this);"/>
			</td>
			
			<th>
				<etiquetas:etiquetaError property="numeroRegistro" requerido="si" name="coberturaForm" 
				key="catalogos.configuracion.numeroregistro" normalClass="normal" errorClass="error"
				errorImage="/img/information.gif" />
			</th>
			<td colspan="">
				<midas:texto propiedadFormulario="numeroRegistro" caracteres="32"/>
			</td>
			
		</tr>
		<tr>
			<td colspan="6"> 
				<div id="resultados_documento">
					<logic:equal value="0" property="idControlArchivoRegistroCNSF" name="coberturaForm" >
						<a href="javascript:void(0);" onclick="mostrarAdjuntarDocumentoCNSFCoberturaWindow()" >Adjuntar documento CNSF</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoRegistroCNSF" name="coberturaForm" >
						<table>
							<tr>
								<th>Archivo:<midas:oculto propiedadFormulario="idControlArchivoRegistroCNSF" nombreFormulario="coberturaForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoRegistroCNSF" nombreFormulario="coberturaForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarAdjuntarDocumentoCNSFCoberturaWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="numeroSecuencia" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.numerosecuencia"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="numeroSecuencia"  id="numeroSecuencia" caracteres="4" longitud="7" onkeypress="return soloNumeros(this, event, true)" />
			</td>
			<th>
				<etiquetas:etiquetaError property="idTcRamo" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.idtcramo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="3">
				<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto"
					onchange="getSubRamos(this,'idTcSubRamo');"	/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="claveTipoSumaAsegurada" requerido="si"
					name="productoForm" key="configuracion.cobertura.clavetiposumaasegurada"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="24" propiedad="claveTipoSumaAsegurada" nombre="coberturaForm" onchange="comboCoberturaSumaAsegurada('tipoSumaAsegurada',this.selectedIndex)" styleClass="cajaTexto"/>
			</td>
			<th><etiquetas:etiquetaError property="idTcSubRamo" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.idtcsubramo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto"/>
			</td>
		</tr>
	</table>
	
	<logic:notEqual value="A" name="claveNegocio" scope="session">		
	<table id="agregar">
		<tr>
			<th colspan="6" style="text-align: center;">Informaci&oacute;n de Producto Casa<br></th>
		</tr>
		<tr>
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.leyenda" /></div></th>
			<td colspan="3">
				<midas:texto propiedadFormulario="leyenda" caracteres="200" longitud="200" />
			</td>
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.editable" /></div></th>
			<td>
				<midas:check propiedadFormulario="editable" id="editable"/>
			</td>
		</tr>
		<tr>
			<th ><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.claveTipoValorSumaAsegurada" /></div></th>
			<td width="350px">
				<midas:comboValorFijo grupoValores="316" propiedad="claveTipoValorSumaAsegurada" nombre="coberturaForm" styleClass="cajaTexto" onchange="comboCoberturaSumaAsegurada('tipoValorSumaAsegurada',this.selectedIndex)" styleId="tipoValorSA" />
			</td>
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.minSumaAsegurada" /></div></th>
			<td width="50px">
				<midas:texto propiedadFormulario="minSumaAsegurada" caracteres="16" longitud="16" onkeypress="return soloNumeros(this, event, true)" />
			</td>
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.maxSumaAsegurada" /></div></th>
			<td width="50px">
				<midas:texto propiedadFormulario="maxSumaAsegurada" caracteres="16" longitud="16" onkeypress="return soloNumeros(this, event, true)" />
			</td>
		</tr>
		<tr>
			<logic:empty property="idCoberturaSumaAsegurada" name="coberturaForm" >
				<th id="celdaSumaAseguradaEtiqueta" style="visibility:hidden;"><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.idcoberturasumaasegurada" /></div></th>
				<td  id="celdaSumaAseguradaSelect" style="visibility:hidden;" >
				<html:select property="idCoberturaSumaAsegurada" styleId="SumaAseguradaSelect" styleClass="cajaTexto"  >
					<html:optionsCollection name="coberturaForm"  property="coberturasSumaAsegurada" value="idToCobertura" label="nombreComercial"/>
				</html:select>
				</td> 
			</logic:empty>
			
			<logic:notEmpty property="idCoberturaSumaAsegurada" name="coberturaForm" >
				<th id="celdaSumaAseguradaEtiqueta" ><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.idcoberturasumaasegurada" /></div></th>
				<td colspan="5" id="celdaSumaAseguradaSelect" >
					<html:select property="idCoberturaSumaAsegurada" styleId="SumaAseguradaSelect" styleClass="cajaTexto"  >
						<html:optionsCollection name="coberturaForm"  property="coberturasSumaAsegurada" value="idToCobertura" label="nombreComercial"/>
					</html:select>
				</td>
			</logic:notEmpty>
		</tr>
		<!-- Datos opcionales para coberturas tipo sublimite -->
		<tr id="datosConfiguracionSublimite">
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.factorSumaAsegurada" /></th>
			<td>
				<midas:texto propiedadFormulario="factorSumaAsegurada" caracteres="3" longitud="3" onkeypress="return soloNumeros(this, event, true)" />
			</td>
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.factorMinSumaAsegurada" /></th>
			<td>
				<midas:texto propiedadFormulario="factorMinSumaAsegurada" caracteres="3" longitud="3" onkeypress="return soloNumeros(this, event, true)" />
			</td>
			<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.factorMaxSumaAsegurada" /></th>
			<td>
				<midas:texto propiedadFormulario="factorMaxSumaAsegurada" caracteres="3" longitud="3" onkeypress="return soloNumeros(this, event, true)" />
			</td>
		</tr>
	</table>
	</logic:notEqual>
	<table id="agregar">
		<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Coaseguro<br></th></tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveTipoCoaseguro" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.clavetipocoaseguro"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="250px">
				<midas:comboValorFijo grupoValores="36" propiedad="claveTipoCoaseguro" nombre="coberturaForm" styleClass="cajaTexto" styleId="claveTipoCoaseguro" onchange="desabilitaCampos();"/>
			</td>
			<th width="150px"><etiquetas:etiquetaError property="valorMinimoCoaseguro" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valorminimocoaseguro"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="50px"><midas:texto propiedadFormulario="valorMinimoCoaseguro"  id="claveTipoCoaseguroValMin"
					onblur="validarDecimal(this, this.value, 16, 2)"
					caracteres="13"/>				
			</td>
			<th><etiquetas:etiquetaError property="valorMaximoCoaseguro" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valormaximocoaseguro"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="50px"><midas:texto propiedadFormulario="valorMaximoCoaseguro" 
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMaximoCoaseguro" caracteres="13"/>
			</td>
		</tr> 	
		<tr>
			<th><etiquetas:etiquetaError property="claveTipoLimiteCoaseguro" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.clavetipolimitecoaseguro"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="37" propiedad="claveTipoLimiteCoaseguro" nombre="coberturaForm" styleClass="cajaTexto" styleId="claveTipoLimiteCoaseguro" onchange="desabilitaCampos();"/></td>
			<th><etiquetas:etiquetaError property="valorMinimoLimiteCoaseguro" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valorminimolimitecoaseguro"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMinimoLimiteCoaseguro" 
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMinimoLimiteCoaseguro" caracteres="13"/>				
			</td>
			<th><etiquetas:etiquetaError property="valorMaximoLimiteCoaseguro" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valormaximolimitecoaseguro"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMaximoLimiteCoaseguro" 
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMaximoLimiteCoaseguro" caracteres="13"/>
			</td>
		</tr>
	</table>
	<table id="agregar">
		<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Deducible<br></th></tr>
		<tr>
			<th width="120px"><etiquetas:etiquetaError property="claveTipoDeducible" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.clavetipodeducible"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="250px">
				<midas:comboValorFijo grupoValores="38" propiedad="claveTipoDeducible" nombre="coberturaForm" styleClass="cajaTexto" styleId="claveTipoDeducible" onchange="desabilitaCampos();"/></td>
			<th width="150px"><etiquetas:etiquetaError property="valorMinimoDeducible" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valorminimodeducible"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="50px"><midas:texto propiedadFormulario="valorMinimoDeducible"  
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMinimoDeducible" caracteres="13"/>				
			</td>
			<th width="200px"><etiquetas:etiquetaError property="valorMaximoDeducible" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valormaximodeducible"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="50px"><midas:texto propiedadFormulario="valorMaximoDeducible"  
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMaximoDeducible" caracteres="13"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveTipoLimiteDeducible" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.clavetipolimitededucible"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="39" propiedad="claveTipoLimiteDeducible" nombre="coberturaForm" styleClass="cajaTexto" styleId="claveTipoLimiteDeducible" onchange="desabilitaCampos();"/></td>
			<th><etiquetas:etiquetaError property="valorMinimoLimiteDeducible" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valorminimolimitededucible"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMinimoLimiteDeducible" 
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMinimoLimiteDeducible" caracteres="13"/>				
			</td>
			<th><etiquetas:etiquetaError property="valorMaximoLimiteDeducible" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valormaximolimitededucible"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valorMaximoLimiteDeducible" 
					onblur="validarDecimal(this, this.value, 16, 2)"
					id="valorMaximoLimiteDeducible" caracteres="13"/>
			</td>
		</tr>
	</table>
	<table id="agregar">
		<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Despliegue<br></th></tr>		
		<tr><th colspan="6">Configuraci&oacute;n Para Mostrar en Pantalla</th></tr>	
		<tr>
			<td colspan="6">
				<table id="desplegarDetalle">
					<tr>
						<th>
							<etiquetas:etiquetaError property="claveMostrarPanCobertura"
								name="coberturaForm" key="configuracion.cobertura.mostrar.cobertura"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPanCobertura" id="claveMostrarPanCobertura"/></td>
						<th>
							<etiquetas:etiquetaError property="claveMostrarPanDescripcion"
								name="coberturaForm" key="configuracion.cobertura.mostrar.descripcion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPanDescripcion" id="claveMostrarPanDescripcion"/></td>		
						<th>
							<etiquetas:etiquetaError property="claveMostrarPanSumaAsegurada"
								name="coberturaForm" key="configuracion.cobertura.mostrar.sumaasegurada"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPanSumaAsegurada" id="claveMostrarPanSumaAsegurada"/></td>	
						
						<th>
							<etiquetas:etiquetaError property="claveMostrarPanCoaseguro"
								name="coberturaForm" key="configuracion.cobertura.mostrar.coaseguro"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPanCoaseguro" id="claveMostrarPanCoaseguro"/></td>
						<th>
							<etiquetas:etiquetaError property="claveMostrarPanDeducible" 
								name="coberturaForm" key="configuracion.cobertura.mostrar.deducible"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPanDeducible" id="claveMostrarPanDeducible"/></td>		
						<th>
							<etiquetas:etiquetaError property="claveMostrarPanPrimaNeta"
								name="coberturaForm" key="configuracion.cobertura.mostrar.primaneta"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPanPrimaNeta" id="claveMostrarPanPrimaNeta"/></td>												
					</tr>
				</table>
			</td>
		</tr>
		<tr><th colspan="6">Configuraci&oacute;n Para Mostrar en Cotizaci&oacute;n</th></tr>
		<tr>
			<td colspan="6">
				<table id="desplegarDetalle">
					<tr>
						<th>
							<etiquetas:etiquetaError property="claveMostrarCotCobertura" 
								name="coberturaForm" key="configuracion.cobertura.mostrar.cobertura"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarCotCobertura" id="claveMostrarCotCobertura"/></td>
						<th>
							<etiquetas:etiquetaError property="claveMostrarCotDescripcion"
								name="coberturaForm" key="configuracion.cobertura.mostrar.descripcion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarCotDescripcion" id="claveMostrarCotDescripcion"/></td>		
						<th>
							<etiquetas:etiquetaError property="claveMostrarCotSumaAsegurada"
								name="coberturaForm" key="configuracion.cobertura.mostrar.sumaasegurada"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarCotSumaAsegurada" id="claveMostrarCotSumaAsegurada"/></td>	
						
						<th>
							<etiquetas:etiquetaError property="claveMostrarCotCoaseguro" 
								name="coberturaForm" key="configuracion.cobertura.mostrar.coaseguro"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarCotCoaseguro" id="claveMostrarCotCoaseguro"/></td>
						<th>
							<etiquetas:etiquetaError property="claveMostrarCotDeducible"
								name="coberturaForm" key="configuracion.cobertura.mostrar.deducible"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarCotDeducible" id="claveMostrarCotDeducible"/></td>		
						<th>
							<etiquetas:etiquetaError property="claveMostrarCotPrimaNeta" 
								name="coberturaForm" key="configuracion.cobertura.mostrar.primaneta"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarCotPrimaNeta" id="claveMostrarCotPrimaNeta"/></td>												
					</tr>
				</table>
			</td>
		</tr>
		<tr><th colspan="6">Configuraci&oacute;n Para Mostrar en P&oacute;liza</th></tr>
		<tr>
			<td colspan="6">
				<table width="100%" id="desplegarDetalle">
					<tr>
						<th>
							<etiquetas:etiquetaError property="claveMostrarPolCobertura"
								name="coberturaForm" key="configuracion.cobertura.mostrar.cobertura"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPolCobertura" id="claveMostrarPolCobertura"/></td>
						<th>
							<etiquetas:etiquetaError property="claveMostrarPolDescripcion" 
								name="coberturaForm" key="configuracion.cobertura.mostrar.descripcion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPolDescripcion" id="claveMostrarPolDescripcion"/></td>		
						<th>
							<etiquetas:etiquetaError property="claveMostrarPolSumaAsegurada"
								name="coberturaForm" key="configuracion.cobertura.mostrar.sumaasegurada"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPolSumaAsegurada" id="claveMostrarPolSumaAsegurada"/></td>	
						
						<th>
							<etiquetas:etiquetaError property="claveMostrarPolCoaseguro"
								name="coberturaForm" key="configuracion.cobertura.mostrar.coaseguro"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPolCoaseguro" id="claveMostrarPolCoaseguro"/></td>
						<th>
							<etiquetas:etiquetaError property="claveMostrarPolDeducible"
								name="coberturaForm" key="configuracion.cobertura.mostrar.deducible"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPolDeducible" id="claveMostrarPolDeducible"/></td>		
						<th>
							<etiquetas:etiquetaError property="claveMostrarPolPrimaNeta"
								name="coberturaForm" key="configuracion.cobertura.mostrar.primaneta"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
						</th>
						<td><midas:check propiedadFormulario="claveMostrarPolPrimaNeta" id="claveMostrarPolPrimaNeta"/></td>												
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveAutorizacion"
					name="coberturaForm" key="configuracion.cobertura.claveautorizacion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveAutorizacion" id="claveAutorizacion"/></td>
			<th><etiquetas:etiquetaError property="claveImporteCero"
					name="coberturaForm" key="configuracion.cobertura.claveimportecero"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveImporteCero" id="claveImporteCero"/>
			</td>
			<th><etiquetas:etiquetaError property="claveDesglosaRiesgos"
					name="coberturaForm" key="configuracion.cobertura.clavedesglosariesgos"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveDesglosaRiesgos" id="claveDesglosaRiesgos"/></td>	
		</tr>
		<tr>				
			<th><etiquetas:etiquetaError property="claveIgualacion"
					name="coberturaForm" key="configuracion.cobertura.claveigualacion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveIgualacion" id="claveIgualacion"/></td>
			<th><etiquetas:etiquetaError property="clavePrimerRiesgo"
					name="coberturaForm" key="configuracion.cobertura.claveprimerriesgo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="clavePrimerRiesgo" id="clavePrimerRiesgo"/></td>						
			<th><etiquetas:etiquetaError property="factorRecargo" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.factorRecargo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="factorRecargo" id="factorRecargo" 
					onkeypress="return soloNumeros(this, event, true)"
					caracteres="13"/>				
			</td>
		</tr>	
		<tr>
			<th><etiquetas:etiquetaError property="coberturaEsPropia"
					name="coberturaForm" key="configuracion.cobertura.esPropia"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="coberturaEsPropia" id="coberturaEsPropia"/></td>
			<th><etiquetas:etiquetaError property="coberturaAplicaDescuentoRecargo"
					name="coberturaForm" key="configuracion.cobertura.aplicanDescuentosRecargos"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="coberturaAplicaDescuentoRecargo" id="coberturaAplicaDescuentoRecargo"/></td>
			<td colspan="2"></td>
		</tr>
		<logic:equal value="A" name="claveNegocio" scope="session">
		<tr>				
			<th ><etiquetas:etiquetaError property="reinstalable"
					name="coberturaForm" key="configuracion.cobertura.reinstalable"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="reinstalable" id="reinstalable"/></td>
			<th ><etiquetas:etiquetaError property="seguroObligatorio"
					name="coberturaForm" key="configuracion.cobertura.seguroObligatorio"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="seguroObligatorio" id="seguroObligatorio"/></td>
			<td colspan="2"></td>
		</tr>		
		</logic:equal>
		<tr>			
			<th colspan="2"><etiquetas:etiquetaError property="claveFuenteSumaAsegurada" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.tipoFuenteDatoSumaAsegurada"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="2"> 
				<midas:comboValorFijo grupoValores="328"  propiedad="claveFuenteSumaAsegurada" nombre="coberturaForm" styleClass="cajaTexto"/>
			</td>
			<td colspan="2"></td>		
		</tr>
		<tr>
			<th colspan="2"><etiquetas:etiquetaError property="valMinPrimaCobertura" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valorMinimoPrimaCobertura"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="2"><midas:texto propiedadFormulario="valMinPrimaCobertura" 
					onkeypress="return soloNumeros(this, event, true)"
					caracteres="13"/>				
			</td>
			<td colspan="2"></td>	
		</tr>
		<logic:equal value="A" name="claveNegocio" scope="session">
		<tr>
			<th colspan="2"><etiquetas:etiquetaError property="claveTipoCalculo" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.claveTipoCalculo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="2">
				<midas:combo propiedad="claveTipoCalculo" id="claveTipoCalculo" styleClass="cajaTexto" >
					<html:optionsCollection name="coberturaForm"  property="claveTipoCalculoList" value="id" label="descripcion"/>
				</midas:combo>
			</td>
			<td colspan="2"></td>	
		</tr>
		<tr>		
			<th colspan="2">
				<div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.idcoberturasumaasegurada" /></div>
			</th>
			<td colspan="2"  >
				<midas:combo propiedad="idCoberturaSumaAsegurada" id="SumaAseguradaSelect" styleClass="cajaTexto" >
					<html:optionsCollection name="coberturaForm"  property="coberturasSumaAsegurada" value="idToCobertura" label="descripcion"/>
				</midas:combo>
			</td>	
			<td colspan="2"></td>	
		</tr>
		</logic:equal>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.coberturaForm,'/MidasWeb/catalogos/cobertura/modificar.do?claveNegocio=A', 'contenido','validaGuardarModificarM1(),desabilitaCampos()');">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.coberturaForm,'/MidasWeb/catalogos/cobertura/listar.do?claveNegocio=<bean:write name="claveNegocio" scope="session"/>', 'contenido',null);">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
		</tr>					
	</table>
</midas:formulario>
<div id="errores" style="display: none;"><html:errors/></div>