<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/cobertura/asociarDescuento">
			<midas:oculto propiedadFormulario="idToCobertura" nombreFormulario="coberturaForm"/>
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.descuento" /> Cobertura</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.descuento.asociado" /> Cobertura</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="descuentosAsociadosCoberturaGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>		
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.descuento.disponible" /> Cobertura</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="descuentosPorAsociarCoberturaGrid" class="dataGridConfigurationClass"></div>
					</td>		
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>		
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
					<a href="javascript: void(0);" 
						onclick="actualizarGridCobertura('descuentosCoberturaProcessor', 'descuentosCoberturaError')">
						<midas:mensaje clave="midas.accion.guardar"/>
					</a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>