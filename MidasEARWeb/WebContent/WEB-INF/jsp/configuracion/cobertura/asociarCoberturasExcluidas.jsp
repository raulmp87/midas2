<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/cobertura/asociarCoberturasExcluidas">
			<midas:oculto propiedadFormulario="idToCobertura" nombreFormulario="coberturaForm"/>
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.cobertura" /></td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.excluir.lista.cobertura" /></td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="excluidosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.excluir.lista.cobertura.disponible" /></td>		
				</tr>
				<tr>
					<td colspan="4">
						<div id="porExcluirGrid" class="dataGridConfigurationClass"></div>
					</td>		
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>			
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
<!-- 					<a href="javascript: void(0);"  -->
<!-- 						onclick="javascript: exclusionCoberturaProcessor.sendData(); configuracionCoberturaTabBar.setTabActive('detalle');"> -->
<%-- 						<midas:mensaje clave="midas.accion.guardar"/> --%>
<!-- 					</a> -->
					<a href="javascript: void(0);" 
						onclick="javascript: exclusionCoberturaProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionCoberturaTabBar, 'detalle');">
						<midas:mensaje clave="midas.accion.guardar"/>
					</a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>