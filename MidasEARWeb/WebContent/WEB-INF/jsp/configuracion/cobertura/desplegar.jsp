<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div hrefmode="ajax-html" style= "height: 450px; overflow:auto" id="configuracionCoberturaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div id="detalle" name="Detalle">
		<center>	
			<midas:formulario accion="/catalogos/cobertura/listar">
				<midas:oculto propiedadFormulario="idToSeccion"/>
				<midas:oculto propiedadFormulario="nombreComercialSeccion"/>
				<midas:oculto propiedadFormulario="idToCobertura"/>		
				<table width="100%" border="0">
					<tr>
						<td class="titulo" colspan="6">
							<midas:escribe propiedad="nombreComercial" nombre="coberturaForm"/>
						</td>
					</tr>
				</table>
				<table id="desplegar" border="0"> 
					<tr>
						<th><midas:mensaje clave="configuracion.cobertura.numerosecuencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="numeroSecuencia" nombre="coberturaForm"/></td>
						<td class="txt_v">&nbsp;</td>
					</tr> 	
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.cobertura.idtcramo" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionRamo" nombre="coberturaForm"/></td>
						<th><midas:mensaje clave="configuracion.cobertura.idtcsubramo" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionSubRamo" nombre="coberturaForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.cobertura.clavetiposumaasegurada" />:</th>
						<td class="txt_v"><midas:escribe propiedad="tipoSumaAsegurada" nombre="coberturaForm"/></td>
						<td class="txt_v">&nbsp;</td>
					</tr> 	
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.cobertura.claveautorizacion" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveAutorizacion" id="claveAutorizacion" deshabilitado="true"/></td>
						<th><midas:mensaje clave="configuracion.cobertura.claveimportecero" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveImporteCero" id="claveImporteCero" deshabilitado="true"/></td>
					</tr>
					<tr>
						<th colspan="4"><midas:mensaje clave="configuracion.cobertura.clavetipocoaseguro" />:</th>
					</tr> 	
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.cobertura.valorminimocoaseguro" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMinimoCoaseguro" nombre="coberturaForm"/></td>
						<th><midas:mensaje clave="configuracion.cobertura.valormaximocoaseguro" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMaximoCoaseguro" nombre="coberturaForm"/></td>
					</tr>
					<tr>
						<th colspan="4"><midas:mensaje clave="configuracion.cobertura.clavetipodeducible" />:</th>
					</tr> 	
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.cobertura.valorminimodeducible" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMinimoDeducible" nombre="coberturaForm"/></td>
						<th><midas:mensaje clave="configuracion.cobertura.valormaximodeducible" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMaximoDeducible" nombre="coberturaForm"/></td>
					</tr>
					<tr>
						<th colspan="4"><midas:mensaje clave="configuracion.cobertura.clavetipolimitecoaseguro" />:</th>
					</tr>
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.cobertura.valorminimolimitecoaseguro" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMinimoLimiteCoaseguro" nombre="coberturaForm"/></td>
						<th><midas:mensaje clave="configuracion.cobertura.valormaximolimitecoaseguro" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMaximoLimiteCoaseguro" nombre="coberturaForm"/></td>
					</tr>
					<tr>
						<th colspan="4"><midas:mensaje clave="configuracion.cobertura.clavetipolimitededucible" />:</th>
					</tr> 	
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.cobertura.valorminimolimitededucible" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMinimoLimiteDeducible" nombre="coberturaForm"/></td>
						<th><midas:mensaje clave="configuracion.cobertura.valormaximolimitededucible" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMaximoLimiteDeducible" nombre="coberturaForm"/></td>
					</tr> 	
				
				</table>
				<div class="subtitulo" style="width: 98%;">Riesgos Asociados</div>
				<div id="riesgoGrid"  width="97%" height="125"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
			</midas:formulario>				
		</center>
	</div>
		<div width="100%" id="documentos" name="Documentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogos/cobertura/listarDocumentosAnexos.do?id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_documentos' , 'mostrarDocumentosCoberturaGrid(<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>)')"></div>
		<div width="100%" id="riesgos" name="Riesgos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/mostrarAsociarRiesgo.do?id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>&idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>', 'contenido_riesgos', 'mostrarRiesgosCobertura(<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>,<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>)')"></div>
		<div width="100%" id="coaseguros" name="Coaseguros" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/mostrarCoaseguroCobertura.do?idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_coaseguros' , 'mostrarCoasegurosCobertura(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>,0,0)')"></div>
		<div width="100%" id="deducibles" name="Deducibles" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/mostrarDeducibleCobertura.do?idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_deducibles' , 'mostrarDeduciblesCobertura(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>,0,0)')"></div>
		<div width="150px" id="exclusionCoberturas" name="Coberturas Excluyentes" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/asociarCoberturasExcluidas.do?idToSeccion=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_exclusionCoberturas' , 'mostrarExclusionCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>)')"></div>
		<div width="150px" id="coberturasRequeridas" name="Coberturas Requeridas" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/asociarCoberturasRequeridas.do?idToSeccion=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_coberturasRequeridas' , 'mostrarCoberturasRequeridasGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>)')"></div>
		<div width="100%" id="aumentos" name="Aumentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/asociarAumento.do?idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_aumentos', 'mostrarAumentosCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>)')"></div>
		<div width="150px" id="exclusionAumentos" name="Exclusi&oacute;n Aumentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/mostrarExclusionAumento.do?id=<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>&idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>', 'contenido_exclusionAumentos' , 'mostrarExclusionAumentosCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>,<midas:escribe propiedad="idToSeccion" nombre="coberturaForm"/>)')"></div>
		<div width="100%" id="descuentos" name="Descuentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/asociarDescuento.do?idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_descuentos', 'mostrarDescuentosCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>)')"></div>
		<div width="150px" id="exclusionDescuentos" name="Exclusi&oacute;n Descuentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/mostrarExclusionDescuento.do?id=<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>&idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>', 'contenido_exclusionDescuentos' , 'mostrarExclusionDescuentosCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>,<midas:escribe propiedad="idToSeccion" nombre="coberturaForm"/>)')"></div>
		<div width="100%" id="recargos" name="Recargos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/asociarRecargo.do?idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_recargos', 'mostrarRecargosCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>)')"></div>
		<div width="150px" id="exclusionRecargos" name="Exclusi&oacute;n Recargos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/cobertura/excluirRecargoCobertura.do?idPadre=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&id=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_exclusionRecargos', 'mostrarExclusionRecargosCoberturaGrids(<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>,<midas:escribe propiedad="idToSeccion" nombre="coberturaForm"/>)')"></div>
		<div width="170px" id="varModPrima" name="Variables Modif. Prima" href="http://void" extraAction="sendRequestJQ(null,'/MidasWeb/relaciones/cobertura/mostrarRelacionGruposVariables.action?idSeccion=<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>&idCobertura=<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>', 'contenido_varModPrima' , 'iniciaGridsGruposVariables()')"></div>
		<div width="190px" id="RecargoCP" name="Recargos y descuentos CP"     href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarRecargoCP.do?idPadre=<midas:escribe propiedad="idToSeccion" nombre="coberturaForm"/>', 'contenido_RecargoCP', 'mostrarRecargosCP(<midas:escribe propiedad='idToCobertura' nombre='coberturaForm'/>,<midas:escribe propiedad='idToSeccion' nombre='coberturaForm'/>)')"></div>
</div>
	

