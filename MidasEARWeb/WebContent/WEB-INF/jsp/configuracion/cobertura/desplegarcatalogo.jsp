<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario accion="/catalogos/cobertura/listar">
	<div id="centrarDesplegar">
	<midas:oculto propiedadFormulario="claveNegocio"/>
		<table id="desplegar">
			<tr>
				<td class="titulo" colspan="6">
					<midas:mensaje clave="midas.accion.detalle" /> Cobertura
					<logic:equal value="A" name="coberturaForm" property="claveNegocio">
					Autos
					</logic:equal>
					<logic:notEqual value="A" name="coberturaForm" property="claveNegocio">
					Da�os
					</logic:notEqual>	
				</td>
			</tr>
			<tr>
				<midas:oculto propiedadFormulario="idToCobertura" nombreFormulario="coberturaForm"/>
				<th><midas:mensaje clave="configuracion.cobertura.codigo" />:</th>
				<td><midas:escribe propiedad="codigo" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.nombrecomercial" />:</th>
				<td><midas:escribe propiedad="nombreComercial" nombre="coberturaForm"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.descripcion" />:</th>
				<td colspan="3"><midas:escribe propiedad="descripcion" nombre="coberturaForm"/></td>
			</tr>
			<tr><th colspan="6" style="text-align: center;"><br>Documento CNSF<br></th></tr>
			<tr><th><midas:mensaje clave="configuracion.cobertura.descripcionRegistroCNSF" /></th>
				<td colspan="2" > <midas:escribe propiedad="descripcionRegistroCNSF" nombre="coberturaForm"/> </td>
			</tr>
			<tr> 
				<logic:equal value="0" property="idControlArchivoRegistroCNSF" name="coberturaForm" >
					<td>Sin archivo CNSF.</td>
				</logic:equal>
				<logic:notEqual value="0" property="idControlArchivoRegistroCNSF" name="coberturaForm" >
					<th>Archivo:<midas:oculto propiedadFormulario="idControlArchivoRegistroCNSF" nombreFormulario="coberturaForm" /></th>
					<td><midas:texto propiedadFormulario="nombreArchivoRegistroCNSF" nombreFormulario="coberturaForm" deshabilitado="true" /></td>
					<td>
						<a href="javascript: void(0);" onclick="javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=<midas:escribe propiedad="idControlArchivoRegistroCNSF" nombre="coberturaForm"/>', 'download');"><img border='0px' title='Descargar' src='/MidasWeb/img/download-icon.jpg'/></a>
				</logic:notEqual>
			</tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.numerosecuencia" />:</th>
				<td><midas:escribe propiedad="numeroSecuencia" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.idtcramo" />:</th>
				<td><midas:escribe propiedad="descripcionRamo" nombre="coberturaForm"/></td>				
			</tr> 
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.clavetiposumaasegurada" />:</th>
				<td><midas:escribe propiedad="tipoSumaAsegurada" nombre="coberturaForm"/></td>
				<logic:notEqual value="" property="nombreCoberturaSumaAsegurada" name="coberturaForm" >
					<th><midas:mensaje clave="configuracion.cobertura.idcoberturasumaasegurada" />:</th>
					<td><midas:escribe propiedad="nombreCoberturaSumaAsegurada" nombre="coberturaForm"/></td>
				</logic:notEqual>
				<th><midas:mensaje clave="configuracion.cobertura.idtcsubramo" />:</th>
				<td><midas:escribe propiedad="descripcionSubRamo" nombre="coberturaForm"/></td>
			</tr>	
			<logic:notEqual value="A" name="claveNegocio" scope="session">	
			<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Producto Casa<br></th></tr>
			<tr>
				<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.leyenda" /></th>
				<td colspan="3"><midas:escribe propiedad="leyenda" nombre="coberturaForm"/></td>
				<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.editable" /></th>
				<td><midas:check propiedadFormulario="editable" id="editable" deshabilitado="true"/></td>
			</tr>
			<tr>
				<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.claveTipoValorSumaAsegurada" /></th>
				<td><midas:escribe propiedad="claveTipoValorSumaAsegurada" nombre="coberturaForm"/></td>
				<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.minSumaAsegurada" /></th>
				<td><midas:escribe propiedad="minSumaAsegurada" nombre="coberturaForm"/></td>
				<th><div class="etiqueta"><midas:mensaje clave="configuracion.cobertura.maxSumaAsegurada" /></th>
				<td><midas:escribe propiedad="maxSumaAsegurada" nombre="coberturaForm"/></td>
			</tr>
			</logic:notEqual>
			<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Coaseguro<br></th></tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.clavetipocoaseguro" />:</th>
				<td><midas:escribe propiedad="tipoCoaseguro" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valorminimocoaseguro" />:</th>
				<td><midas:escribe propiedad="valorMinimoCoaseguro" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valormaximocoaseguro" />:</th>
				<td><midas:escribe propiedad="valorMaximoCoaseguro" nombre="coberturaForm"/></td>
			</tr> 		
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.clavetipolimitecoaseguro" />:</th>
				<td><midas:escribe propiedad="tipoLimiteCoaseguro" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valorminimolimitecoaseguro" />:</th>
				<td><midas:escribe propiedad="valorMinimoLimiteCoaseguro" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valormaximolimitecoaseguro" />:</th>
				<td><midas:escribe propiedad="valorMaximoLimiteCoaseguro" nombre="coberturaForm"/></td>
			</tr> 	
			<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Deducible<br></th></tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.clavetipodeducible" />:</th>
				<td><midas:escribe propiedad="tipoDeducible" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valorminimodeducible" />:</th>
				<td><midas:escribe propiedad="valorMinimoDeducible" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valormaximodeducible" />:</th>
				<td><midas:escribe propiedad="valorMaximoDeducible" nombre="coberturaForm"/></td>
			</tr> 				
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.clavetipolimitededucible" />:</th>
				<td><midas:escribe propiedad="tipoLimiteDeducible" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valorminimolimitededucible" />:</th>
				<td><midas:escribe propiedad="valorMinimoLimiteDeducible" nombre="coberturaForm"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.valormaximolimitededucible" />:</th>
				<td><midas:escribe propiedad="valorMaximoLimiteDeducible" nombre="coberturaForm"/></td>
			</tr>
			<tr><th colspan="6" style="text-align: center;">Informaci&oacute;n de Despliegue<br></th></tr>
			<tr><th><br>Configuraci&oacute;n Para Mostrar en Pantalla<br></th></tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.cobertura" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPanCobertura" id="claveMostrarPanCobertura" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.descripcion" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPanDescripcion" id="claveMostrarPanDescripcion" deshabilitado="true"/></td>	
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.sumaasegurada" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPanSumaAsegurada" id="claveMostrarPanSumaAsegurada" deshabilitado="true"/></td>	
			</tr>		
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.coaseguro" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPanCoaseguro" id="claveMostrarPanCoaseguro" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.deducible" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPanDeducible" id="claveMostrarPanDeducible" deshabilitado="true"/></td>		
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.primaneta" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPanPrimaNeta" id="claveMostrarPanPrimaNeta" deshabilitado="true"/></td>
			</tr>
			<tr><th><br>Configuraci&oacute;n Para Mostrar en Cotizaci&oacute;n<br></th></tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.cobertura" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarCotCobertura" id="claveMostrarCotCobertura" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.descripcion" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarCotDescripcion" id="claveMostrarCotDescripcion" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.sumaasegurada" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarCotSumaAsegurada" id="claveMostrarCotSumaAsegurada" deshabilitado="true"/></td>
			</tr>
			<tr>	
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.coaseguro" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarCotCoaseguro" id="claveMostrarCotCoaseguro" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.deducible" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarCotDeducible" id="claveMostrarCotDeducible" deshabilitado="true"/></td>		
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.primaneta" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarCotPrimaNeta" id="claveMostrarCotPrimaNeta" deshabilitado="true"/></td>										
			</tr>		
			<tr><th><br>Configuraci&oacute;n Para Mostrar en P&oacute;liza<br></th></tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.cobertura" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPolCobertura" id="claveMostrarPolCobertura" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.descripcion" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPolDescripcion" id="claveMostrarPolDescripcion" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.sumaasegurada" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPolSumaAsegurada" id="claveMostrarPolSumaAsegurada" deshabilitado="true"/></td>
			</tr>
			<tr>	
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.coaseguro" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPolCoaseguro" id="claveMostrarPolCoaseguro" deshabilitado="true"/></td>
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.deducible" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPolDeducible" id="claveMostrarPolDeducible" deshabilitado="true"/></td>		
				<th><midas:mensaje clave="configuracion.cobertura.mostrar.primaneta" />:</th>
				<td><midas:check propiedadFormulario="claveMostrarPolPrimaNeta" id="claveMostrarPolPrimaNeta" deshabilitado="true"/></td>												
			</tr>			 					
			<logic:equal value="SUBL�MITE" property="tipoSumaAsegurada" name="coberturaForm" >
				<tr>
					<th><midas:mensaje clave="configuracion.cobertura.factorSumaAsegurada" />:</th>
					<td><midas:escribe propiedad="factorSumaAsegurada" nombre="coberturaForm"/></td>
					<th><midas:mensaje clave="configuracion.cobertura.factorMinSumaAsegurada" />:</th>
					<td><midas:escribe propiedad="factorMinSumaAsegurada" nombre="coberturaForm"/></td>
					<th><midas:mensaje clave="configuracion.cobertura.factorMaxSumaAsegurada" />:</th>
					<td><midas:escribe propiedad="factorMaxSumaAsegurada" nombre="coberturaForm"/></td>
				</tr>
			</logic:equal>
			<tr><th colspan="6" style="text-align: center;"><br></th></tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.claveautorizacion" />:</th>
				<td><midas:check propiedadFormulario="claveAutorizacion" id="claveAutorizacion" deshabilitado="true"/></td>
				
				<th><midas:mensaje clave="configuracion.cobertura.claveimportecero" />:</th>
				<td><midas:check propiedadFormulario="claveImporteCero" id="claveImporteCero" deshabilitado="true"/></td>
				
				<th><midas:mensaje clave="configuracion.cobertura.clavedesglosariesgos" />:</th>
				<td><midas:check propiedadFormulario="claveDesglosaRiesgos" id="claveDesglosaRiesgos" deshabilitado="true"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="configuracion.cobertura.claveigualacion" />:</th>
				<td><midas:check propiedadFormulario="claveIgualacion" id="claveIgualacion" deshabilitado="true"/></td>
				
				<th><midas:mensaje clave="configuracion.cobertura.claveprimerriesgo" />:</th>
				<td><midas:check propiedadFormulario="clavePrimerRiesgo" id="clavePrimerRiesgo" deshabilitado="true"/></td>				
			</tr>
			<tr>
			<th><etiquetas:etiquetaError property="coberturaEsPropia"
					name="coberturaForm" key="configuracion.cobertura.esPropia"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="coberturaEsPropia" id="coberturaEsPropia" deshabilitado="true"/></td>
			<th><etiquetas:etiquetaError property="coberturaAplicaDescuentoRecargo"
					name="coberturaForm" key="configuracion.cobertura.aplicanDescuentosRecargos"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="coberturaAplicaDescuentoRecargo" id="coberturaAplicaDescuentoRecargo" deshabilitado="true"/></td>
		</tr>
		<logic:equal value="A" name="claveNegocio" scope="session">
		<tr>				
			<th ><etiquetas:etiquetaError property="reinstalable"
					name="coberturaForm" key="configuracion.cobertura.reinstalable"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="reinstalable" id="reinstalable" deshabilitado="true"/></td>
			<th ><etiquetas:etiquetaError property="seguroObligatorio"
					name="coberturaForm" key="configuracion.cobertura.seguroObligatorio"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="seguroObligatorio" id="seguroObligatorio" deshabilitado="true"/></td>
		</tr>		
		</logic:equal>
		<tr>			
			<th><etiquetas:etiquetaError property="claveFuenteSumaAsegurada" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.tipoFuenteDatoSumaAsegurada"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td colspan="2"> 
				<midas:comboValorFijo grupoValores="328"  propiedad="claveFuenteSumaAsegurada" nombre="coberturaForm" styleClass="cajaTexto"
			 	 readonly="true"/>
			</td>			
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="valMinPrimaCobertura" requerido="si"
					name="coberturaForm" key="configuracion.cobertura.valorMinimoPrimaCobertura"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="valMinPrimaCobertura" 
					onkeypress="return soloNumeros(this, event, true)"
					deshabilitado="true"
					caracteres="13"/>				
			</td>
		</tr>
			<tr>
				<td class= "guardar" colspan="6">
					<div class="alinearBotonALaDerecha">							
						<div id="b_regresar">
							<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.coberturaForm,'/MidasWeb/catalogos/cobertura/listar.do?claveNegocio=<bean:write name="claveNegocio" scope="session"/>', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
							</a>
						</div>					
					</div>
				</td>      		
			</tr>				
		</table>
	</div>	
</midas:formulario>
