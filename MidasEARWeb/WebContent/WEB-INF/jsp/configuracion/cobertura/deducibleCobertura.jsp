<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/cobertura/mostrarDeducibleCobertura">
			<midas:oculto propiedadFormulario="idToCobertura" nombreFormulario="coberturaForm"/>
			<midas:oculto propiedadFormulario="permiteDeducibleCero" nombreFormulario="coberturaForm"/>
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="6"><midas:mensaje clave="configuracion.deducible" /> por Cobertura</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="deduciblesCoberturaDiv" width="430px" height="250px" style="background-color:white;overflow:hidden"></div>
					</td>
					<td colspan="2">
						<div id="b_agregar">
							<a href="javascript: void(0);" onclick="javascript: agregarDeducibleCobertura(0,<midas:escribe propiedad="idToCobertura" nombre="coberturaForm"/>,0);">Agregar</a>
						</div>
						<br/><br/>
						<div id="b_borrar">
							<a href="javascript: void(0);" onclick="javascript: deduciblesCoberturaGrid.deleteSelectedItem(); ">Eliminar</a>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="6"><midas:mensaje clave="configuracion.eliminar.seleccionado.mensaje"/></td>
				</tr>			
			</table>
		</midas:formulario>
	</center>
</div>