<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/tipopoliza/mostrarExclusionRecargo">
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.recargo.cobertura" /></td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.cobertura" /></center></th>
				</tr>	
				<tr>
					<td colspan="4">
						<div id="exclusionesRecargoTipoPolizaGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.cobertura.disponible" /></center></th>
				</tr>
				<tr>
					<td>Cobertura:<html:hidden property="idToTipoPoliza" styleId="idToTipoPoliza" /></td>
					<td width="60%">
						<midas:combo styleClass="cajaTexto" propiedad="claveTMPCombo" id="tipoPolizaSelect_rec" onchange="mostrarExclusionesRecargoTipoPolizaPorPoliza(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="tipoPolizaForm"  property="coberturas" value="idToCobertura" label="nombreComercial"/>
						</midas:combo>
					</td>
					<td>Recargo:</td>
					<td width="25%">
						<midas:combo styleClass="cajaTexto" propiedad="claveTMPCombo" id="recargoSelect" onchange="mostrarExclusionesRecargoTipoPolizaPorRecargo(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="tipoPolizaForm"  property="recargosAsociados" value="id.idtorecargovario" label="recargoVarioDTO.descripcionrecargo"/>
						</midas:combo>
					</td>					
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesRecargoTipoPolizaNoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>		
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>	
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" 
					onclick="javascript: exclusionRecargoTipoPolizaProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionTipoPolizaTabBar,'detalle');">
						<midas:mensaje clave="midas.accion.guardar"/>
				</a>
<!-- 					<a onclick="actualizarGridTipoPoliza('exclusionRecargoTipoPolizaProcessor', 'exclusionRecargosTipoPolizaError')"  -->
<!-- 						href="javascript: void(0);"> -->
<%-- 						<midas:mensaje clave="midas.accion.guardar"/> --%>
<!-- 					</a> -->
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>			