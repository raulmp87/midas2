<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/tipopoliza/guardarDocumentoAnexo">
			<midas:oculto propiedadFormulario="idToTipoPoliza" 
				nombreFormulario="tipoPolizaForm"/>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="2">
						<midas:mensaje clave="configuracion.tipopoliza.documentoanexo"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="documentosAnexosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="btn_back w170">
							<a href="javascript: void(0);" class="icon_adjuntarPDF"
								onclick="mostrarAnexarArchivoTipoPolizaWindow();">
								<midas:mensaje clave="midas.sistema.boton.agregar.documento"/>
							</a>
						</div>
					</td>
					<td>
						<div class="btn_back w170">
							<a href="javascript: void(0);" class="icon_eliminar"
								onclick="javascript: if(!documentoAnexoTipoPolizaError){ eliminarDocumentoAnexoTipoPoliza(); }else{ alert('Introduzca los datos solicitados.');}">
								<midas:mensaje clave="midas.sistema.boton.eliminar.documentoseleccionado"/>
							</a>
						</div>
					</td>
				</tr>
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" 
					onclick="javascript: documentoAnexoTipoPolizaProcessor.sendData(); if(!documentoAnexoTipoPolizaError){ configuracionTipoPolizaTabBar.setTabActive('detalle'); }else alert('Introduzca los datos solicitados.');">
					<midas:mensaje clave="midas.accion.guardar"/>
				</a>
				</div>
			</div>	
		</midas:formulario>
	</center>
</div>		