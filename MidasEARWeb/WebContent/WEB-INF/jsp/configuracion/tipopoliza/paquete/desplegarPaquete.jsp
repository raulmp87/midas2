<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html:form action="/configuracion/tipopoliza/paquete/agregar" styleId="formPaquete" >
	<table width="100%" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="configuracion.tipopoliza.paquete.titulo.agregar" />
			</td>
		</tr>	
	</table>
	<table id="agregar">
		<html:hidden property="idToTipoPoliza" styleId="idToTipoPoliza" />
		<html:hidden property="paquetePolizaForm.idToTipoPoliza" styleId="paquetePolizaForm.idToTipoPoliza" />
		<html:hidden property="paquetePolizaForm.idToPaquetePoliza" styleId="idToPaquetePoliza" />
		<html:hidden property="claveAccion" styleId="claveAccion" />
		<tr>
			<th><etiquetas:etiquetaError property="paquetePolizaForm.nombre" requerido="si"
					name="tipoPolizaForm" key="configuracion.tipopoliza.paquete.nombre"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="paquetePolizaForm.nombre" id="nombre" caracteres="150" 
					deshabilitado="<bean:write  name='tipoPolizaForm' property='paquetePolizaForm.soloLectura' />" />
			</td>
			<th><etiquetas:etiquetaError property="paquetePolizaForm.orden" requerido="si"
					name="tipoPolizaForm" key="configuracion.tipopoliza.paquete.orden"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" /></th>
			<td><midas:texto propiedadFormulario="paquetePolizaForm.orden" 
					onkeypress="return soloNumeros(this, event, false)" id="orden" caracteres="4" 
					deshabilitado="<bean:write  name='tipoPolizaForm' property='paquetePolizaForm.soloLectura' />" />
			</td>
		</tr> 
		<tr>
			<th><etiquetas:etiquetaError property="paquetePolizaForm.descripcion" requerido="no"
					name="tipoPolizaForm" key="configuracion.tipopoliza.paquete.descripcion"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td colspan="3">
				<midas:texto propiedadFormulario="paquetePolizaForm.descripcion" id="descripcion" caracteres="200"
					deshabilitado="<bean:write  name='tipoPolizaForm' property='paquetePolizaForm.soloLectura' />" />
			</td>
		</tr>
		<logic:equal value="agregarPaquete" name="tipoPolizaForm" property="claveAccion">
			<tr>
				<td class="campoRequerido" colspan="4"><midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" /></td>
			</tr>
		</logic:equal>
		<logic:equal value="editarPaquete" name="tipoPolizaForm" property="claveAccion">
			<tr>
				<td class="campoRequerido" colspan="4"><midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" /></td> 			
			</tr>
		</logic:equal>
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"  style="font-size: 7pt" 
							onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarPaquetes.do?id=<midas:escribe  nombre="tipoPolizaForm" propiedad="idToTipoPoliza"/>', 'contenido_paquetes','mostrarPaquetesTipoPoliza(<midas:escribe  nombre="tipoPolizaForm" propiedad="idToTipoPoliza"/>)');">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>					
					</div>
					<logic:equal value="agregarPaquete" name="tipoPolizaForm" property="claveAccion">
						<div id="b_guardar">
							<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.getElementById('formPaquete'),'/MidasWeb/configuracion/tipopoliza/paquete/agregar.do', 'contenido_paquetes','poblarCoberturasRegistradas();poblarCoberturasDisponibles();mostrarMensajeUsuarioRegistroPaquete();');">
								<midas:mensaje clave="midas.accion.guardar"/>
							</a>
						</div>
					</logic:equal>
					<logic:equal value="editarPaquete" name="tipoPolizaForm" property="claveAccion">
						<div id="b_guardar">
							<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.getElementById('formPaquete'),'/MidasWeb/configuracion/tipopoliza/paquete/modificar.do', 'contenido_paquetes','poblarCoberturasRegistradas();poblarCoberturasDisponibles();mostrarMensajeUsuarioRegistroPaquete();');">
								<midas:mensaje clave="midas.accion.guardar"/>
							</a>
						</div>
					</logic:equal>
				</div>
			</td>
		</tr>
		<tr>
			<td class="titulo" colspan="4" >
				<midas:mensaje clave="configuracion.tipopoliza.paquete.coberturasIncluidas"/>
			</td>
		</tr>
		<tr>
			<td>
				<div id="mensajeError" class="error" style="visibility: hidden"></div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="coberturasRegistradasGrid" class="dataGridConfigurationClass" style="height:150px;"></div>
			</td>
		</tr>
		<logic:equal value="editarPaquete" name="tipoPolizaForm" property="claveAccion">
			<tr>
				<td class="titulo" colspan="4">
					<midas:mensaje clave="configuracion.tipopoliza.paquete.coberturasDisponibles"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="coberturasDisponiblesGrid" class="dataGridConfigurationClass" style="height:150px;" ></div>
				</td>
			</tr>
		</logic:equal>
	</table>
	
	<div id="errores" style="display: none;"><html:errors/></div>
	<div id="errorRegistroPaquete" style="display: none;"><bean:write name="tipoPolizaForm" property="mensaje"/></div>
</html:form>

