<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div id="detalle" name="Detalle"><center>
	<midas:formulario accion="/configuracion/tipopoliza/mostrarPaquetes">
		<table  id="desplegarDetalle" border="0">
			<tr>
				<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.tipopoliza.paquete.titulo" /></td>
				<html:hidden property="idToTipoPoliza" styleId="idToTipoPoliza" />
			</tr>
			<tr>
				<td colspan="4">
					<div id="configuracionPaquetesGrid" class="dataGridConfigurationClass" style="height:360px;width:671px;" ></div>
				</td>
			</tr>
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/paquete/desplegarPaquete.do?claveAccion=agregarPaquete&idToTipoPoliza='+<bean:write name="tipoPolizaForm" property="idToTipoPoliza"/>, 'contenido_paquetes',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
				</div>
			</div>	
		</table>
		
	
	</midas:formulario>
</center></div>