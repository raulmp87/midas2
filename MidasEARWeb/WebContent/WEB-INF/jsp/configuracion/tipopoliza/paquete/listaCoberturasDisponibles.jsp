<?xml version="1.0" encoding="ISO-8859-1"?>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
			<call command="enableDragAndDrop"><param>true</param></call>
		</beforeInit>
		<!--afterInit>
			<call command="groupBy"><param>0</param></call>
		</afterInit-->
		
		<column id="idToPaquetePol" type="ro" width="0" sort="str" hidden="true">idPaquete</column>
		<column id="idCoberturaSecPaq" type="ro" width="0" sort="str" hidden="true">idPaquete</column>
		<column id="idToSeccion" type="ro" width="0" sort="str" hidden="true">idToSeccion</column>
		<column id="idToCobertura" type="ro" width="0" sort="str" hidden="true">idToCobertura</column>
		<column id="nombreSeccion" type="ro" width="150" sort="str">Secci&oacute;n</column>
		<column id="nombreCobertura" type="ro" width="*" sort="str">Cobertura</column>
		<column id="claveObligatoriedad" type="ro" width="100" sort="str" >Obligatoriedad</column>
		<column id="claveTipoSumaAsegurada" type="ro" width="80" sort="str" >Tipo SA</column>
		<column id="montoSumaAsegurada" type="ro" width="80" sort="na" hidden="true" >SA</column>
	</head>
	
	<nested:iterate id="cobertura" name="tipoPolizaForm" property="listaCoberturasDisponibles" indexId="indexCob">
		<row id="<bean:write name="cobertura" property="id.idtoseccion"/>_<bean:write name="cobertura" property="id.idtocobertura"/>">
			<cell><bean:write name="tipoPolizaForm" property="paquetePolizaForm.idToPaquetePoliza"/></cell>
			<cell></cell>
			<cell><bean:write name="cobertura" property="id.idtoseccion"/></cell>
			<cell><bean:write name="cobertura" property="id.idtocobertura"/></cell>
		
			<cell><bean:write name="cobertura" property="seccionDTO.nombreComercial"/></cell>
			<cell><bean:write name="cobertura" property="coberturaDTO.nombreComercial"/></cell>
			<cell>
				<logic:equal value="0" property="claveObligatoriedad" name="cobertura" >
					Opcional
				</logic:equal>
				<logic:equal value="1" property="claveObligatoriedad" name="cobertura" >
					Opcional Default
				</logic:equal>
				<logic:equal value="2" property="claveObligatoriedad" name="cobertura" >
					Obligatorio parcial
				</logic:equal>
				<logic:equal value="3" property="claveObligatoriedad" name="cobertura" >
					Obligatorio
				</logic:equal>
			</cell>
			<cell>
				<logic:equal value="1" property="coberturaDTO.claveTipoSumaAsegurada" name="cobertura" >
					B�sica
				</logic:equal>
				<logic:equal value="2" property="coberturaDTO.claveTipoSumaAsegurada" name="cobertura" >
					Amparada
				</logic:equal>
				<logic:equal value="3" property="coberturaDTO.claveTipoSumaAsegurada" name="cobertura" >
					Subl�mite
				</logic:equal>
			</cell>
			<cell>0</cell>
		</row>
	</nested:iterate>
</rows>