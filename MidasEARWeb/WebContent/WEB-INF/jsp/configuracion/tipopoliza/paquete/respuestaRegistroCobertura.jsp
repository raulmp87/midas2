<?xml version="1.0" encoding="ISO-8859-1"?>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<%@  page contentType="text/xml" %>
<%@ page isELIgnored="false"%>
<data>
	<action type="<bean:write name='tipoPolizaForm' property='respuestaActualizacionPaqueteForm.tipoRespuesta' />" 
			sid="<bean:write name='tipoPolizaForm' property='respuestaActualizacionPaqueteForm.idOriginal' />" 
			tid="<bean:write name='tipoPolizaForm' property='respuestaActualizacionPaqueteForm.idResultado' />"
			operacionExitosa="<bean:write name='tipoPolizaForm' property='respuestaActualizacionPaqueteForm.operacionExitosa' />"
			tipoMensaje="<bean:write name='tipoPolizaForm' property='respuestaActualizacionPaqueteForm.tipoMensaje' />"
			idToTipoPoliza="<bean:write name='tipoPolizaForm' property='respuestaActualizacionPaqueteForm.idToTipoPoliza' />"
			>
		<bean:write name="tipoPolizaForm" property="respuestaActualizacionPaqueteForm.mensaje" />
	</action>
</data>
