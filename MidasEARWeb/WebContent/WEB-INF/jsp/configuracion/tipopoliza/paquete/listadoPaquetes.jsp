<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@  page contentType="text/xml" %>
<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<afterInit>
		</afterInit>
		
		<column id="idPaquete" type="ro" width="0" sort="str" hidden="true">idPaquete</column>
		<column id="agrupacionPaquete" type="ro" width="0" sort="str" hidden="true">Agrupacion</column>
		<column id="nombrePaquete" type="ro" width="120" sort="str">Paquete</column>
		<column id="descPaquete" type="ro" width="120" sort="str">Descripci&oacute;n</column>
		<column id="idToCoberturaSeccionPaquete" type="ro" width="0" sort="str" hidden="true">idToCobSecPaq</column>
		<column id="idtoseccion" type="ro" width="0" sort="na" hidden="true">idtoseccion</column>
		<column id="idtocobertura" type="ro" width="0" sort="na" hidden="true">idtocobertura</column>
		<!--column id="codigoSeccion" type="ro" width="50" sort="na" >Codigo Seccion</column-->
		<column id="nombreSeccion" type="ro" width="140" sort="na" >Secci&oacute;n</column>
		<column id="nombreCobertura" type="ro" width="193" sort="na" >Cobertura</column>
		<column id="valorSumaAsegurada" type="ro" width="50" sort="na" >SA</column>
	</head>
	
	<nested:iterate id="paquete" name="tipoPolizaForm" property="listaPaquetes" indexId="indexPaq">
		<logic:notEmpty name="paquete" property="coberturaSeccionPaqueteDTOs" >
			<nested:iterate id="coberturaSeccion" name="paquete" property="coberturaSeccionPaqueteDTOs" indexId="indexCobSec">
				<row id="<bean:write name="coberturaSeccion" property="idToCoberturaSeccionPaquete"/>">
					<cell><bean:write name="paquete" property="idToPaquetePoliza"/></cell>
					<!-- Columna que muestra la descripcion del paquete e iconos disponibles -->
					<cell><bean:write name="paquete" property="orden"/>.-<bean:write name="paquete" property="nombre"/>
						<logic:equal value="0" property="estatus" name="paquete" >
							&lt;span style="padding-left:20px"&gt;
							&lt;a onclick="javascript:modificarPaqueteCliente(<bean:write name="tipoPolizaForm" property="idToTipoPoliza"/>,<bean:write name="paquete" property="idToPaquetePoliza"/>)" href="javascript: void(0)"&gt;
								&lt;img border="0" src="/MidasWeb/img/menu_icons/edit.gif" title="Modificar"&gt;
							&lt;/a&gt;
						
							&lt;span style="padding-left:20px"&gt;				
							&lt;a onclick="javascript:liberarPaquetePoliza(<bean:write name="paquete" property="idToPaquetePoliza"/>)" href="javascript: void(0)"&gt;
								&lt;img border="0" src="/MidasWeb/img/b_autorizar.gif" title="Autorizar"&gt;
							&lt;/a&gt;
							
						</logic:equal>			
						
						<logic:equal value="1" property="estatus" name="paquete" >
							&lt;span style="padding-left:20px"&gt;
							<c:choose>
								<c:when test="${paquete.idToPaquetePoliza == paquete.tipoPolizaDTO.idToPaqueteDefault}">
									&lt;img border="0" src="/MidasWeb/img/ico_green.gif" title="Paquete Default"&gt;
								</c:when>
								<c:otherwise>
									&lt;a onclick="javascript:definirPaqueteDefault(<bean:write name="paquete" property="idToPaquetePoliza"/>)" href="javascript: void(0)"&gt;
										&lt;img border="0" src="/MidasWeb/img/Save16.gif" title="Establecer Default"&gt;
									&lt;/a&gt;
								</c:otherwise>
							</c:choose>
						</logic:equal>
						&lt;span style="padding-left:20px"&gt;
						&lt;a onclick="javascript:eliminarPaquetePoliza(<bean:write name="paquete" property="idToPaquetePoliza"/>)" href="javascript: void(0)"&gt;
							&lt;img border="0" src="/MidasWeb/img/b_borrar.gif" title="Eliminar"&gt;
						&lt;/a&gt;
						
					</cell>
					<!-- Columna que muestra la descripcion del paquete -->
					<cell><bean:write name="paquete" property="orden"/>.-<bean:write name="paquete" property="nombre"/></cell>
					<cell><bean:write name="paquete" property="descripcion"/></cell>
					<cell><bean:write name="coberturaSeccion" property="idToCoberturaSeccionPaquete"/></cell>
					<cell><bean:write name="coberturaSeccion" property="coberturaSeccionDTO.id.idtoseccion"/></cell>
					<cell><bean:write name="coberturaSeccion" property="coberturaSeccionDTO.id.idtocobertura"/></cell>
					<!-- cell><bean:write name="coberturaSeccion" property="coberturaSeccionDTO.seccionDTO.codigo"/></cell-->
					<cell><bean:write name="coberturaSeccion" property="coberturaSeccionDTO.seccionDTO.nombreComercial"/></cell>
					<cell><bean:write name="coberturaSeccion" property="coberturaSeccionDTO.coberturaDTO.nombreComercial"/></cell>
					<cell><bean:write name="coberturaSeccion" property="valorSumaAsegurada"/></cell>
				</row>
			</nested:iterate>
		</logic:notEmpty>
		<logic:empty name="paquete" property="coberturaSeccionPaqueteDTOs" >
			<row id="<bean:write name="paquete" property="idToPaquetePoliza"/>">
				<cell><bean:write name="paquete" property="idToPaquetePoliza"/></cell>
				<cell><bean:write name="paquete" property="orden"/>.-<bean:write name="paquete" property="nombre"/></cell>
				<cell><bean:write name="paquete" property="descripcion"/></cell>
				<cell></cell>
				<cell></cell>
				<cell></cell>
				<cell></cell>
				<cell></cell>
				<cell></cell>
				<cell></cell>
			</row>
		</logic:empty>
	</nested:iterate>
</rows>