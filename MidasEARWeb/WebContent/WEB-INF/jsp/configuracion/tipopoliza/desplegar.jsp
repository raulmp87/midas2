<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div hrefmode="ajax-html" style="height: 450px" id="configuracionTipoPolizaTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="100%" id="detalle" name="Detalle">
		<center>
			<midas:formulario accion="/configuracion/tipopoliza/mostrarDetalle">
			
				<table width="100%" border="0">
					<tr>
						<td class="titulo" colspan="4">
							<midas:escribe propiedad="nombreComercial" nombre="tipoPolizaForm"/>
						</td>
					</tr>
				</table>		
				<table id="desplegar"  border="0">
					<midas:oculto propiedadFormulario="idToProducto"/>
					<midas:oculto propiedadFormulario="idToTipoPoliza"/>
					<midas:oculto propiedadFormulario="nombreComercialProducto"/>					
					<tr>
						<th><midas:mensaje clave="configuracion.tipopoliza.codigo" />:</th>
						<td class="txt_v"><midas:escribe propiedad="codigo" nombre="tipoPolizaForm"/></td>
						<th><midas:mensaje clave="configuracion.tipopoliza.diasgracia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasGracia" nombre="tipoPolizaForm"/></td>		
					</tr> 
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.tipopoliza.descripcion" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcion" nombre="tipoPolizaForm"/></td>
						<th><midas:mensaje clave="configuracion.tipopoliza.diasretroactividad" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasRetroactividad" nombre="tipoPolizaForm"/></td>												
					</tr>		
					<tr>
						<th><midas:mensaje clave="configuracion.tipopoliza.nombrecomercial" />:</th>
						<td class="txt_v"><midas:escribe propiedad="nombreComercial" nombre="tipoPolizaForm"/></td>
						<th><midas:mensaje clave="configuracion.tipopoliza.diasdiferimiento" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasDiferimiento" nombre="tipoPolizaForm"/></td>						
					</tr>
					<tr  class="bg_t2">
						<th><midas:mensaje clave="configuracion.producto.claveEstatus" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionEstatus" nombre="tipoPolizaForm"/></td>
						<th><midas:mensaje clave="configuracion.producto.diasgracia.recibossubsecuentes" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasGraciaSubsecuentes" nombre="tipoPolizaForm"/></td>					
					</tr> 		
					<tr>
						<th><midas:mensaje clave="configuracion.tipopoliza.tipoCalculoEmision" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionTipoCalculoEmision" nombre="tipoPolizaForm"/></td>													
						<th><midas:mensaje clave="configuracion.tipopoliza.clavepagoinmediato" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="clavePagoInmediato" id="clavePagoInmediato" deshabilitado="true"/></td>
					</tr>
					<tr  class="bg_t2">
						<th><midas:mensaje clave="configuracion.tipopoliza.tipoCalculoCancelacion" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionTipoCalculoCancelacion" nombre="tipoPolizaForm"/></td>
						<th><midas:mensaje clave="configuracion.tipopoliza.claverenovable" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveRenovable" id="claveRenovable" deshabilitado="true"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.producto.fechainiciovigencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="fechaInicioVigencia" nombre="tipoPolizaForm"/></td>						
						<td class="txt_v">&nbsp;</td>
					</tr>
					<tr class="bg_12">
						<th><midas:mensaje clave="configuracion.tipopoliza.claveaplicaflotillas" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveAplicaFlotillas" id="claveAplicaFlotillas" deshabilitado="true"/></td>
						<th><midas:mensaje clave="configuracion.tipopoliza.claveaplicaautoexpedible" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveAplicaAutoexpedible" id="claveAplicaAutoexpedible" deshabilitado="true"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.tipopoliza.claveProdServ"/>:</th>
					    <td class="txt_v"><midas:escribe propiedad="claveProdServ" nombre="tipoPolizaForm"/></td>
					    <th><midas:mensaje clave="configuracion.tipopoliza.validaClaveProd"/>:</th>
					    <td class="txt_v"><midas:escribe propiedad="validaClaveProd" nombre="tipoPolizaForm"/></td>
					</tr>
					
				</table>
				<div class="subtitulo" style="width: 98%;">Bienes/Secciones Asociadas</div>
				<div align="left">
					<input type="checkbox" name="inactivos" onClick="cargaDataGridtipopolizaHijos(document.tipoPolizaForm.idToTipoPoliza.value,document.tipoPolizaForm.idToProducto.value,this.checked)"/>Mostrar secciones inactivas
				</div>				
				<div id="seccionGrid"  width="97%" height="125"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
				<div id="botonAgregar">
					<div class="alinearBotonALaDerecha">
						<div id="b_agregar">
							<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarAgregar.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'configuracion_detalle',null);">
							<midas:mensaje clave="midas.accion.agregar"/>
							</a>
						</div>
					</div>
				</div>
			</midas:formulario>
		</center>							
	</div>
		<div width="100%" id="documentos" name="Documentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/listarDocumentosAnexos.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_documentos', 'mostrarDocumentosTipoPolizaGrid(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="100%" id="moneda" name="Monedas" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarAsociarMoneda.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_moneda' , 'mostrarMonedaTipoPolizaGrids(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="100%" id="ramos" name="Ramos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarAsociarRamo.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_ramos', 'mostrarRamosTipoPolizaGrid(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="100%" id="aumentos" name="Aumentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarAsociarAumento.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_aumentos', 'mostrarAumentosTipoPolizaGrids(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="150px" id="exclusionAumentos" name="Exclusi&oacute;n Aumentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarExclusionAumento.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_exclusionAumentos' , 'mostrarExclusionAumentoTipoPoliza(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="100%" id="descuentos" name="Descuentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarAsociarDescuento.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_descuentos', 'mostrarDescuentosTipoPolizaGrids(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="150px" id="exclusionDescuentos" name="Exclusi&oacute;n Descuentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarExclusionDescuento.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_exclusionDescuentos' , 'mostrarExclusionDescuentoTipoPoliza(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="100%" id="recargos" name="Recargos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarAsociarRecargo.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_recargos', 'mostrarRecargosTipoPolizaGrids(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<div width="150px" id="exclusionRecargos" name="Exclusi&oacute;n Recargos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarExclusionRecargo.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_exclusionRecargos' , 'mostrarExclusionRecargoTipoPoliza(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>)')"></div>
		<!--  <div width="150px" id="paquetes" name="Paquetes" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarPaquetes.do?id=<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>', 'contenido_paquetes' , 'mostrarPaquetesTipoPoliza(<midas:escribe propiedad="idToTipoPoliza" nombre="tipoPolizaForm"/>);')"></div>-->
</div>
