<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/configuracion/tipopoliza/agregar">
		
	<table id="agregar" width="100%" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
				Tipo de P&oacute;liza
			</td>
		</tr>	
	</table>
	<table id="agregar">
		<midas:oculto propiedadFormulario="idToProducto"/>
		<midas:oculto propiedadFormulario="nombreComercialProducto"/>
		<html:hidden property="claveEstatus" value="0"/>
		<tr><th colspan="4" class="lineaTitulo">
			Informaci&oacute;n General del Tipo de P&oacute;liza.
		</th></tr>
		<tr>
			<th width="22%"><etiquetas:etiquetaError property="codigo" requerido="si"
					name="tipoPolizaForm" key="configuracion.tipopoliza.codigo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="codigo" 
					onkeypress="return soloAlfanumericos(this, event, false)" id="codigo" caracteres="8"/>
			</td>
			<th><etiquetas:etiquetaError property="claveProdServ" requerido="si"
		                                 name="tipoPolizaForm" key="configuracion.tipopoliza.claveProdServ"
		                                 normalClass="normal" errorClass="error" errorImage="/img/information.gif"/>
		    </th>
		    <td>
		    <midas:texto propiedadFormulario="clave" onkeypress="return soloAlfanumericos(this, event, false)" 
		           id="claveProdServ" caracteres="10"/>
		    </td>
		</tr> 
		<tr>
			<th width="18%"><etiquetas:etiquetaError property="nombreComercial" requerido="si"
					name="tipoPolizaForm" key="configuracion.tipopoliza.nombrecomercial"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="35%">
				<html:text property="nombreComercial" maxlength="100"
						styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
			<th><etiquetas:etiquetaError property="descripcion" requerido="si"
					name="tipoPolizaForm" key="configuracion.tipopoliza.descripcion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /></th>
			<td width="17%">
				<html:text property="descripcion" maxlength="200"
						styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr>
		<tr><th colspan="4" class="lineaTitulo">
			Informaci&oacute;n de Pol&iacute;ticas de Venta.
		</th></tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveRenovable"
					name="tipoPolizaForm" key="configuracion.tipopoliza.claverenovable"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveRenovable" id="claveRenovable"/></td>
			<th><etiquetas:etiquetaError property="clavePagoInmediato"
					name="tipoPolizaForm" key="configuracion.tipopoliza.clavepagoinmediato"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="clavePagoInmediato" id="clavePagoInmediato"/></td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="diasRetroactividad" requerido="si"
								name="tipoPolizaForm" key="configuracion.tipopoliza.diasretroactividad"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasRetroactividad" 
								onkeypress="return soloNumerosM2(this, event, false)" id="diasRetroactividad" caracteres="4"/>	
			</td>
			<th><etiquetas:etiquetaError property="diasDiferimiento" requerido="si"
								name="tipoPolizaForm" key="configuracion.tipopoliza.diasdiferimiento"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasDiferimiento"
								onkeypress="return soloNumerosM2(this, event, false)" id="diasDiferimiento" caracteres="4"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="diasGracia" requerido="si"
								name="tipoPolizaForm" key="configuracion.tipopoliza.diasgracia"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasGracia" 
								onkeypress="return soloNumerosM2(this, event, false)" id="diasGracia" caracteres="4"/>
			</td>	
			
			<th><etiquetas:etiquetaError property="diasGraciaSubsecuentes" requerido="si"
								name="tipoPolizaForm" key="configuracion.producto.diasgracia.recibossubsecuentes"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasGraciaSubsecuentes" 
								onkeypress="return soloNumerosM2(this, event, false)" id="diasGraciaSubsecuentes" caracteres="4"/>
			</td>	
						
		</tr>
		<tr><th colspan="4" class="lineaTitulo">
			Informaci&oacute;n de Configuraci&oacute;n.
		</th></tr>
		<tr>
			<th><etiquetas:etiquetaError property="idTipoCalculoEmision" requerido="si" name="tipoPolizaForm" key="configuracion.tipopoliza.tipoCalculoEmision"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" /></th>
			<td>
				<midas:comboValorFijo grupoValores="28" propiedad="idTipoCalculoEmision" nombre="tipoPolizaForm" styleClass="cajaTexto"/>
			</td>	
			<th>
				<div id="etiquetaFecha"><etiquetas:etiquetaError normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" name="tipoPolizaForm"
					property="fechaInicioVigencia" key="configuracion.producto.fechainiciovigencia" requerido="no"/>
					
					
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
							<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
					
					 
				</div>
		   	</th>
		   	
		   		<th>
			   				<html:text property="fechaInicioVigencia"  maxlength="10" size="15"
			   				 styleId="fecha" name="tipoPolizaForm"  styleClass="cajaTexto"
			   				 readonly="true"
							 onkeypress="return soloFecha(this, event, false);"
							 onblur="esFechaValida(this);"
							 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
							 <!-- /> -->
							 
					
				</th>		
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="idTipoCalculoCancelacion" requerido="si" name="productoForm" key="configuracion.tipopoliza.tipoCalculoCancelacion"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" /></th>
			<td>
				<midas:comboValorFijo grupoValores="22" propiedad="idTipoCalculoCancelacion" nombre="tipoPolizaForm" styleClass="cajaTexto"/>
			</td>			
		</tr>
		
		<tr><th colspan="4" class="lineaTitulo">
			Informaci&oacute;n de Autos.
		</th></tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveAplicaFlotillas" requerido="no" name="tipoPolizaForm" key="configuracion.tipopoliza.claveaplicaflotillas"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" /></th>
			<td>
				<midas:check propiedadFormulario="claveAplicaFlotillas" id="claveAplicaFlotillas"/>
			</td>			
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveAplicaAutoexpedible" requerido="no" name="tipoPolizaForm" key="configuracion.tipopoliza.claveaplicaautoexpedible"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" /></th>
			<td>
				<midas:check propiedadFormulario="claveAplicaAutoexpedible" id="claveAplicaAutoexpedible"/>
			</td>			
		</tr>
				
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"  style="font-size: 7pt" 
							onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/producto/mostrarDetalle.do?id=<midas:escribe  nombre="tipoPolizaForm" propiedad="idToProducto"/>&idPadre=root', 'configuracion_detalle','dhx_init_tabbars();cargaDataGridproductoHijos(<midas:escribe  nombre="tipoPolizaForm" propiedad="idToProducto"/>,null,false);');">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>					
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);"
						 onclick="javascript: sendRequest(document.tipoPolizaForm,'/MidasWeb/configuracion/tipopoliza/agregar.do', 'configuracion_detalle','validaGuardarTipoPoliza();')">						
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
		</tr>					
	</table>
	<div id="errores" style="display: none;"><html:errors/></div>
	<html:hidden property="mensaje" name="tipoPolizaForm" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" name="tipoPolizaForm" styleId="tipoMensaje"/>
</midas:formulario>

