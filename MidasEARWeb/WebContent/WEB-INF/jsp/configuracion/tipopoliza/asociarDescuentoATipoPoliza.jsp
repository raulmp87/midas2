<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/tipopoliza/mostrarAsociarDescuento">
			<midas:oculto propiedadFormulario="idToTipoPoliza" nombreFormulario="tipoPolizaForm"/>
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.descuento" /> Tipo de P&oacute;liza</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.descuento.asociado" /> Tipo de P&oacute;liza</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="descuentosAsociadosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.descuento.disponible" /> Tipo de P&oacute;liza</td>
				</tr>		
				<tr colspan="4">
					<td >
						<div id="descuentosPorAsociarGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>		
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
					<a href="javascript: void(0);" onclick="actualizarGridTipoPoliza('descuentosTipoPolizaProcessor', 'descuentosTipoPolizaError')"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>		