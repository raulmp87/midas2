<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/configuracion/tipopoliza/borrar">

	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.borrar" />
				<midas:escribe propiedad="nombreComercial" nombre="tipoPolizaForm"/>
				&nbsp;asociada: <midas:escribe propiedad="nombreComercialProducto" nombre="tipoPolizaForm"/>
				<midas:oculto propiedadFormulario="idToProducto"/>
				<midas:oculto propiedadFormulario="idToTipoPoliza"/>
				<midas:oculto propiedadFormulario="nombreComercialProducto"/>
			</td>
		</tr>
		<tr><th colspan="6"><center><br/>Informaci&oacute;n General del Tipo de P&oacute;liza.<br/></center></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.codigo" />:</th>
			<td><midas:escribe propiedad="codigo" nombre="tipoPolizaForm"/></td>
			<th><midas:mensaje clave="configuracion.tipopoliza.version" />:</th>
			<td><midas:escribe propiedad="version" nombre="tipoPolizaForm"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.nombrecomercial" />:</th>
			<td><midas:escribe propiedad="nombreComercial" nombre="tipoPolizaForm"/></td>
			<th><midas:mensaje clave="configuracion.tipopoliza.descripcion" />:</th>
			<td colspan="3"><midas:escribe propiedad="descripcion" nombre="tipoPolizaForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.claveEstatus" />:</th>
			<td><midas:escribe propiedad="descripcionEstatus" nombre="tipoPolizaForm" /></td>
		</tr>
		<tr><th colspan="6"><center><br/>Informaci&oacute;n de Pol&iacute;ticas de Venta.<br/></center></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.claverenovable" />:</th>
			<td><midas:check propiedadFormulario="claveRenovable" id="claveRenovable" deshabilitado="true"/></td>
			<th><midas:mensaje clave="configuracion.tipopoliza.clavepagoinmediato" />:</th>
			<td><midas:check propiedadFormulario="clavePagoInmediato" id="clavePagoInmediato" deshabilitado="true"/></td>
		</tr> 		
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.diasretroactividad" />:</th>
			<td><midas:escribe propiedad="diasRetroactividad" nombre="tipoPolizaForm"/></td>
			<th><midas:mensaje clave="configuracion.tipopoliza.diasdiferimiento" />:</th>
			<td><midas:escribe propiedad="diasDiferimiento" nombre="tipoPolizaForm"/></td>
			<th><midas:mensaje clave="configuracion.tipopoliza.diasgracia" />:</th>
			<td><midas:escribe propiedad="diasGracia" nombre="tipoPolizaForm"/></td>
		</tr>
		<tr><th colspan="6"><center><br/>Informaci&oacute;n de Configuraci&oacute;n.<br/></center></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.tipoCalculoEmision" />:</th>
			<td><midas:escribe propiedad="descripcionTipoCalculoEmision" nombre="tipoPolizaForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.fechainiciovigencia" />:</th>
			<td><midas:escribe propiedad="fechaInicioVigencia" nombre="tipoPolizaForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.tipoCalculoCancelacion" />:</th>
			<td><midas:escribe propiedad="descripcionTipoCalculoCancelacion" nombre="tipoPolizaForm"/></td>
		</tr>
		<tr><th colspan="6"><center><br/>Informaci&oacute;n de Autos.<br/></center></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.claveaplicaflotillas" />:</th>
			<td><midas:check propiedadFormulario="claveAplicaFlotillas" id="claveAplicaFlotillas" deshabilitado="true"/></td>
			<th><midas:mensaje clave="configuracion.tipopoliza.claveaplicaautoexpedible" />:</th>
			<td><midas:check propiedadFormulario="claveAplicaAutoexpedible" id="claveAplicaAutoexpedible" deshabilitado="true"/></td>
		</tr>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">							
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/producto/mostrarDetalle.do?id=<midas:escribe  nombre="tipoPolizaForm" propiedad="idToProducto"/>', 'configuracion_detalle','dhx_init_tabbars();cargaDataGridproductoHijos(\'<midas:escribe  nombre="tipoPolizaForm" propiedad="idToProducto"/>\',null);');">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<c:choose>
						<c:when test="${tipoPolizaForm.claveEstatus==1}">
									<div id="b_borrar">
										<a href="javascript: void(0);"
								    		onclick="javascript: alert('El Tipo de P&oacute;liza se encuentra Activo en Producci&oacute;n');">
											<midas:mensaje clave="midas.accion.borrar"/>
										</a>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${tipoPolizaForm.claveEstatus==2}">
											<div id="b_borrar">
												<a href="javascript: void(0);"
								    				onclick="javascript: alert('El Tipo de P&oacute;liza se encuentra Inactivo');">
													<midas:mensaje clave="midas.accion.borrar"/>
												</a>
											</div>
										</c:when>
										<c:otherwise>
										<c:choose>
										<c:when test="${not empty tipoPolizaForm.secciones}">
											<div id="b_borrar">
											<a href="javascript: void(0);"
												onclick="javascript: Confirma('El Tipo de P&oacute;liza tiene Secciones registradas. �Desea continuar el Borrado?', document.tipoPolizaForm,'/MidasWeb/configuracion/tipopoliza/borrar.do?id=<midas:escribe  nombre="tipoPolizaForm" propiedad="idToTipoPoliza"/>', 'configuracion_detalle','validaBorrar();');">
												<midas:mensaje clave="midas.accion.borrar"/>
											</a>
											</div>
										</c:when>
										<c:otherwise>
											<div id="b_borrar">
												<a href="javascript: void(0);"
													onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?', document.tipoPolizaForm,'/MidasWeb/configuracion/tipopoliza/borrar.do?id=<midas:escribe  nombre="tipoPolizaForm" propiedad="idToTipoPoliza"/>', 'configuracion_detalle','validaBorrar();');">
													<midas:mensaje clave="midas.accion.borrar"/>
												</a>
											</div>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>
			</td>      		
		</tr>				
	</table>
	<html:hidden property="mensaje" name="tipoPolizaForm" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" name="tipoPolizaForm" styleId="tipoMensaje"/>
</midas:formulario>