<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/catalogos/riesgo/modificar">
	<midas:oculto propiedadFormulario="claveNegocio"/>
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.modificar" /> Riesgo
				<logic:equal value="A" name="riesgoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="riesgoForm" property="claveNegocio">
				Da�os
				</logic:notEqual>			
				<midas:oculto propiedadFormulario="idToRiesgo"/>
			</td>
		</tr>
		<tr><th colspan ="4"><br>Datos generales.<br></th><th colspan ="4"></th> </tr>
		<tr>
			<th><etiquetas:etiquetaError property="nombreComercial" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.nombre"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text property="nombreComercial" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
			<th>
			<etiquetas:etiquetaError property="codigo" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.codigo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="codigo"  soloLectura="true"
					onkeypress="return soloNumeros(this, event, false, false)" id="codigo" caracteres="8"/>
			</td>
		</tr> 
		<tr>
			<th><etiquetas:etiquetaError property="descripcion" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.descripcion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /></th>
			<td>
				<html:text property="descripcion" maxlength="200"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="idTcRamo" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.idTcRamo"
					normalClass="normal" errorClass="error" errorImage="/img/information.gif" /> </th>
			<td><midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto"
					onchange="getSubRamos(this,'idTcSubRamo');"	/>
			</td>
			
			<th><etiquetas:etiquetaError property="idTcSubRamo" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.idTcSubRamo"
					normalClass="normal" errorClass="error"	errorImage="/img/information.gif" /></th>
			<td><midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto"/></td>
		</tr>	
		<tr>
			<th><etiquetas:etiquetaError property="valorMinimoPrima" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.valorminimoprima"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /> </th>
			<td><midas:texto propiedadFormulario="valorMinimoPrima" 
					id="valorMinimoPrima" 
					onkeypress="return soloNumeros(this, event, false)" /> </td>

			<th><etiquetas:etiquetaError property="claveAplicaPerdidaTotal"
					name="riesgoForm" key="configuracion.riesgo.claveaplicaperdidatotal"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:checkBox propiedadFormulario="claveAplicaPerdidaTotal" 
					id="claveAplicaPerdidaTotal" valorEstablecido="false"	/> </td>
		</tr>		
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha" style="margin-right: 4px">
					<div id="b_guardar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/modificar.do', 'contenido', 'validaGuardarModificarM1()');">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class= "regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/listar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td>
		</tr>
	</table>
</midas:formulario>

