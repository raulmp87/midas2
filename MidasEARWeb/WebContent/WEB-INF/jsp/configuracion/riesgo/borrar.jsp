<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/catalogos/riesgo/borrar">
<midas:oculto propiedadFormulario="claveNegocio"/>
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.borrar" /> Riesgo
				<logic:equal value="A" name="riesgoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="riesgoForm" property="claveNegocio">
				Da�os
				</logic:notEqual>	
				<midas:oculto propiedadFormulario="idToRiesgo"/>
			</td>
		</tr>
		<tr><th colspan ="4"><br>Datos generales.<br></th><th colspan ="4"></th></tr>
		<tr>
			<midas:oculto propiedadFormulario="idToRiesgo" nombreFormulario="riesgoForm"/>
			<th><midas:mensaje clave="configuracion.riesgo.nombre" />:</th>
			<td><midas:escribe propiedad="nombreComercial" nombre="riesgoForm"/></td>
			<th><midas:mensaje clave="configuracion.riesgo.codigo" />:</th>
			<td><midas:escribe propiedad="codigo" nombre="riesgoForm"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="configuracion.riesgo.descripcion" />:</th>
			<td ><midas:escribe propiedad="descripcion" nombre="riesgoForm"/></td>
			<th><midas:mensaje clave="configuracion.riesgo.version" />:</th>
			<td><midas:escribe propiedad="version" nombre="riesgoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.riesgo.idTcRamo" />:</th>
			<td><midas:escribe propiedad="descripcionRamo" nombre="riesgoForm"/></td>			
			<th><midas:mensaje clave="configuracion.riesgo.idTcSubRamo" />:</th>
			<td><midas:escribe propiedad="descripcionSubRamo" nombre="riesgoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.riesgo.valorminimoprima" />:</th>
			<td><midas:escribe propiedad="valorMinimoPrima" nombre="riesgoForm"/></td>
			<th><midas:mensaje clave="configuracion.riesgo.claveaplicaperdidatotal" />:</th>
			<td><midas:checkBox propiedadFormulario="claveAplicaPerdidaTotal" 
					id="claveAplicaPerdidaTotal" valorEstablecido="false" deshabilitado="true"	/></td>
	    </tr>							
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<c:choose>
						<c:when test="${riesgoForm.esAsociadoTarifa}">
							<div id="b_borrar" style="margin-right: 4px">
								<a href="javascript: void(0);"
								onclick="javascript: alert('No se puede borrar el riesgo porque est&aacute; asociado a una Tarifa');">
								<midas:mensaje clave="midas.accion.borrar"/>
								</a>
							</div>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${riesgoForm.esAsociadoCobertura}">
									<div id="b_borrar" style="margin-right: 4px">
										<a href="javascript: void(0);"
										onclick="javascript: alert('No se puede borrar el riesgo porque est&aacute; asociado a una Cobertura');">
										<midas:mensaje clave="midas.accion.borrar"/>
										</a>
									</div>
								</c:when>
								<c:otherwise>
									<div id="b_borrar" style="margin-right: 4px">
										<a href="javascript: void(0);"
										onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.riesgoForm,'/MidasWeb/catalogos/riesgo/borrar.do', 'contenido','validaBorrarM1()');">
										<midas:mensaje clave="midas.accion.borrar"/>
										</a>
									</div>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>
			</td>
		</tr>			
		<tr>
			<td class= "regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/listar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>

</midas:formulario>