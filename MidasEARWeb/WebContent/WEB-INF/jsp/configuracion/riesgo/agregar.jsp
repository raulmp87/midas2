<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/catalogos/riesgo/agregar">
	<midas:oculto propiedadFormulario="claveNegocio"/>
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" /> Riesgo
				<logic:equal value="A" name="riesgoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="riesgoForm" property="claveNegocio">
				Da�os
				</logic:notEqual>
			</td>
		</tr>
		<tr><th colspan ="4"><br>Datos generales.<br></th></tr>
		<tr>
			<th><etiquetas:etiquetaError property="nombreComercial" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.nombre"
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text property="nombreComercial" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>
			<th><etiquetas:etiquetaError property="codigo" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.codigo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text property="codigo" maxlength="8"
					onkeypress="return soloNumeros(this, event, false, false)"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>
		</tr> 
		<tr>
			<th><etiquetas:etiquetaError property="descripcion" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.descripcion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /></th>
			<td>
				<html:text property="descripcion" maxlength="200"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />

			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="idTcRamo" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.idTcRamo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /> </th>
			<td><midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto w200"
					onchange="getSubRamos(this,'idTcSubRamo');"	/> </td>

			<th><etiquetas:etiquetaError property="idTcSubRamo" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.idTcSubRamo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto w200"/> </td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="valorMinimoPrima" requerido="si"
					name="riesgoForm" key="configuracion.riesgo.valorminimoprima"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /> </th>
			<td>
				<html:text property="valorMinimoPrima" 
					onkeypress="return soloNumeros(this, event, false)"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>

			<th><etiquetas:etiquetaError property="claveAplicaPerdidaTotal"
					name="riesgoForm" key="configuracion.riesgo.claveaplicaperdidatotal"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:checkBox propiedadFormulario="claveAplicaPerdidaTotal" 
					id="claveAplicaPerdidaTotal" valorEstablecido="0"	/> </td>
		</tr>
		<tr>
			<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/agregar.do', 'contenido','validaGuardarModificarM1()');">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/listar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>				
				</div>
			</td>
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
		</tr>
	</table>
	
	<html:hidden property="mensaje" styleId="mensaje" name="riesgoForm"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje" name="riesgoForm"/>
</midas:formulario>
<div id="errores" style="display: none;"><html:errors/></div>

