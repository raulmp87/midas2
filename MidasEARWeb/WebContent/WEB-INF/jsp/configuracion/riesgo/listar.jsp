<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario  accion="/catalogos/riesgo/listar">
<midas:oculto propiedadFormulario="claveNegocio"/>
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Riesgos
				<logic:equal value="A" name="riesgoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="riesgoForm" property="claveNegocio">
				Daños
				</logic:notEqual>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.riesgo.codigo"/>:</th>
			<td><midas:texto propiedadFormulario="codigo" 
					onkeypress="return soloNumeros(this, event, false, false)" id="codigo" caracteres="8"/>
			</td>
			
			<th><midas:mensaje clave="configuracion.riesgo.descripcion"/>:</th>
			<td colspan="3">
				<html:text property="descripcion" maxlength="200"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="configuracion.riesgo.version"/>:</th>
			<td><midas:texto propiedadFormulario="version" 
					onkeypress="return soloNumeros(this, event, false)" id="version" caracteres="4"/>
			</td>
			
			<th><midas:mensaje clave="configuracion.riesgo.nombre"/>:</th>
			<td>
				<html:text property="nombreComercial" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.riesgo.idTcRamo"/>:</th>
			<td><midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto"
					onchange="getSubRamos(this,'idTcSubRamo');"	/></td>
			
			<th><midas:mensaje clave="configuracion.riesgo.idTcSubRamo"/>:</th>
			<td><midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto"/></td>
		</tr>
		<tr>
			<th colspan="2"><midas:mensaje clave="configuracion.riesgo.mostrarInactivos"/>
				<midas:checkBox valorEstablecido="1" propiedadFormulario="mostrarInactivos"/>
			</th>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
	</table>
	</br>
	<div id="resultados">
		<midas:tabla idTabla="riesgo"
			claseDecoradora="mx.com.afirme.midas.decoradores.Riesgo"
			claseCss="tablaConResultados" nombreLista="listRiesgo"
			urlAccion="/catalogos/riesgo/listarFiltrado.do">
			<midas:columna propiedad="codigo" titulo="C&oacute;digo"/>
			<midas:columna propiedad="version" titulo="Versi&oacute;n"/>
			<midas:columna propiedad="nombreComercial" titulo="Nombre" maxCaracteres="100" />
			<midas:columna propiedad="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.riesgoForm,'/MidasWeb/catalogos/riesgo/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
