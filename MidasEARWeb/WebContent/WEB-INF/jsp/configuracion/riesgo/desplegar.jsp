<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


<div hrefmode="ajax-html"  style= "height: 450px; overflow:auto" id="configuracionRiesgoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/"  skinColors="#FCFBFC,#F4F3EE" >
	<div id="detalle" name="Detalle">
		<midas:formulario accion="/catalogos/riesgo/listar">
			<midas:oculto propiedadFormulario="idToSeccion"/>
			<midas:oculto propiedadFormulario="idToCobertura"/>
			<midas:oculto propiedadFormulario="idToRiesgo"/>		
			<table width="100%" border="0">
				<tr>
					<td class="titulo" colspan="4">
						<midas:escribe propiedad="nombreComercial" nombre="riesgoForm"/>
					</td>
				</tr>
			</table>		
			<table id="desplegar" border="0">
				<tr>
					<th><midas:mensaje clave="configuracion.riesgo.codigo" />:</th>
					<td class="txt_v"><midas:escribe propiedad="codigo" nombre="riesgoForm"/></td>			
				</tr> 
				<tr class="bg_t2">
					<th><midas:mensaje clave="configuracion.riesgo.descripcion" />:</th>
					<td class="txt_v"><midas:escribe propiedad="descripcion" nombre="riesgoForm"/></td>				
					<th><midas:mensaje clave="configuracion.riesgo.nombre" />:</th>
					<td class="txt_v"><midas:escribe propiedad="nombreComercial" nombre="riesgoForm"/></td>			
				</tr>
				<tr>
					<th><midas:mensaje clave="configuracion.riesgo.idTcRamo" />:</th>
					<td class="txt_v"><midas:escribe propiedad="descripcionRamo" nombre="riesgoForm"/></td>				
					<th><midas:mensaje clave="configuracion.riesgo.idTcSubRamo" />:</th>
					<td class="txt_v"><midas:escribe propiedad="descripcionSubRamo" nombre="riesgoForm"/></td>
				</tr>	
				<tr class="bg_t2">
					<th><midas:mensaje clave="configuracion.riesgo.valorminimoprima" />:</th>
					<td class="txt_v"><midas:escribe propiedad="valorMinimoPrima" nombre="riesgoForm"/></td>				
					<th><midas:mensaje clave="configuracion.riesgo.claveaplicaperdidatotal" />:</th>
					<td class="txt_v"><midas:check propiedadFormulario="claveAplicaPerdidaTotal" id="claveAplicaPerdidaTotal" deshabilitado="true"/></td>			
				</tr>
			</table>
		</midas:formulario>
	</div>
		<div width="100%" id="coaseguros" name="Coaseguros" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/riesgo/mostrarRegistroCoaseguro.do?id=<midas:escribe propiedad='idToRiesgo' nombre='riesgoForm'/>&idPadre=<midas:escribe propiedad="idToCobertura" nombre="riesgoForm"/>a<midas:escribe propiedad="idToSeccion" nombre="riesgoForm"/>', 'contenido_coaseguros', 'mostrarCoasegurosRiesgo(<midas:escribe propiedad='idToRiesgo' nombre='riesgoForm'/>,<midas:escribe propiedad="idToCobertura" nombre="riesgoForm"/>,<midas:escribe propiedad="idToSeccion" nombre="riesgoForm"/>)')"></div>
		<div width="100%" id="deducibles" name="Deducibles" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/riesgo/mostrarRegistroDeducible.do?id=<midas:escribe propiedad='idToRiesgo' nombre='riesgoForm'/>&idPadre=<midas:escribe propiedad="idToCobertura" nombre="riesgoForm"/>a<midas:escribe propiedad="idToSeccion" nombre="riesgoForm"/>', 'contenido_deducibles', 'mostrarDeduciblesRiesgo(<midas:escribe propiedad='idToRiesgo' nombre='riesgoForm'/>,<midas:escribe propiedad="idToCobertura" nombre="riesgoForm"/>,<midas:escribe propiedad="idToSeccion" nombre="riesgoForm"/>)')"></div>
</div>

