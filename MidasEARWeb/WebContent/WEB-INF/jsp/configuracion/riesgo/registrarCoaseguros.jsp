<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/configuracion/riesgo/mostrarRegistroCoaseguro">
	<table  id="desplegarDetalle" width="100%" border="0">
		<tr>
			<td class="titulo" colspan="6"><midas:mensaje clave="configuracion.coaseguro" /> por Riesgo</td>
		</tr>
		<tr>&nbsp;</tr>
		<tr>
			<td colspan="4">
				<div id="coasegurosRiesgoDiv" width="430px" height="250px" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>		
	</table>
</midas:formulario>