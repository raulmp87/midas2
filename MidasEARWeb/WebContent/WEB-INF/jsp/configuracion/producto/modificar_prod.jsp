<%@ page isELIgnored="false"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/catalogos/producto/modificar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	<midas:oculto propiedadFormulario="claveNegocio"/>
	<midas:oculto propiedadFormulario="fechaCreacion"/>
	
	<table width="100%" border="0" >
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.modificar" /> Producto
				<logic:equal value="A" name="productoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="productoForm" property="claveNegocio">
				Daños
				</logic:notEqual>
				<midas:oculto propiedadFormulario="idToProducto" nombreFormulario="productoForm"/>
				<midas:oculto propiedadFormulario="version" nombreFormulario="productoForm"/>
				<midas:oculto nombreFormulario="productoForm" propiedadFormulario="claveNegocio"/>
			</td>
		</tr>
	</table>
	
	<h4>
		Informaci&oacute;n General del Producto
	</h4>
	
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<th>
			    	<etiquetas:etiquetaError property="codigo" 
			    		requerido="si" name="productoForm" 
			    		key="configuracion.producto.codigo"
					    normalClass="normal" errorClass="error"
					    errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="codigo" styleId="codigo" 
						maxlength="8" readonly="true"
						onkeypress="return soloNumeros(this, event, false)" 
						styleClass="cajaTexto"  />
				</td>			
				<th>
					<etiquetas:etiquetaError property="version" 
						requerido="no" name="productoForm" 
						key="configuracion.producto.version" 
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<midas:escribe propiedad="version" 
						nombre="productoForm"  />
				</td>
			</tr> 
		
			<tr>
				<th><etiquetas:etiquetaError property="claveEstatus" 
							requerido="no" name="productoForm" 
							key="configuracion.producto.claveEstatus" 
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />		
				</th>
				<td width="200px">
					<midas:comboValorFijo grupoValores="33" 
							propiedad="claveEstatus" 
							nombre="productoForm" styleClass="cajaTexto"/>
				</td>
				<th width="100px">
					<etiquetas:etiquetaError property="nombreComercial" 
							requerido="si" name="productoForm" 
							key="configuracion.producto.nombrecomercial"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" 
						/>
					
				</th>
				<td width="450px" colspan="3">
					<html:text property="nombreComercial" maxlength="100"
								styleClass="jQalphaextra jQrestrict cajaTexto" readonly="true"
							/>
					
				</td>
			</tr>
			<tr>
				<th>
					<etiquetas:etiquetaError property="descripcion" 
							requerido="si" name="productoForm" 
							key="configuracion.producto.descripcion"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" 
						/>
				</th>
				<td colspan="5">
					<html:text property="descripcion" maxlength="200"
							styleClass="jQalphaextra jQrestrict cajaTexto" readonly="true"
						/>
				</td>
			</tr> 
		</table>	
	</div>
	<h4>
		Informaci&oacute;n de Vigencias
	</h4>	
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<th>
					<div id="etiquetaFecha">
						<etiquetas:etiquetaError normalClass="normal" 
							errorClass="error" 
							errorImage="/img/information.gif" 
							name="productoForm" 
							property="fechaInicioVigencia" 
							key="configuracion.producto.fechainiciovigencia" 
							requerido="si"  
						/>	
					  	
					</div>
		   		</th>		   	
		   		<th width="200px">
			   		<html:text property="fechaInicioVigencia"  
						maxlength="10" size="15" styleId="fecha" 
						name="productoForm"  styleClass="cajaTexto"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						onblur="esFechaValida(this);" 
						readonly="true" 
					/>
				</th>
				<th>
					<etiquetas:etiquetaError property="claveUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.claveunidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" 
					/>
				</th>
				<td>
					<midas:comboValorFijo grupoValores="23" propiedad="claveUnidadVigencia" nombre="productoForm" styleClass="cajaTexto" readonly="true"/>
				</td>
				<th>
					<etiquetas:etiquetaError property="claveAjusteVigencia" 
						requerido="no" name="productoForm" 
						key="configuracion.producto.ajusteporvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" 
					/>
				</th>
				<td>
					<html:checkbox property="claveAjusteVigencia" 
						styleId="claveAjusteVigencia" 
						disabled="true" >
					</html:checkbox>
					<midas:oculto propiedadFormulario="claveAjusteVigencia" 
						nombreFormulario="productoForm"
					/>
				</td>				
			</tr>
			<tr>
				<th>
					<etiquetas:etiquetaError property="valorMinimoUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.valorminimounidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td width="200px">
					<html:text property="valorMinimoUnidadVigencia" styleId="valorMinimoUnidadVigencia" maxlength="8" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>
				<th>
					<etiquetas:etiquetaError property="valorMaximoUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.valormaximounidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="valorMaximoUnidadVigencia" styleId="valorMaximoUnidadVigencia" maxlength="8" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>
				<th>
					<etiquetas:etiquetaError property="valorDefaultUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.valordefaultunidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="valorDefaultUnidadVigencia" styleId="valorDefaultUnidadVigencia" maxlength="8" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>
			</tr>
		</table>
	</div>
	<h4>
		Informaci&oacute;n de Pol&iacute;ticas de Venta
	</h4>		
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<th>
					<etiquetas:etiquetaError property="claveRenovable" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.claverenovable"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" 
					/>
				</th>
				<td>
					<html:checkbox property="claveRenovable" 
						styleId="claveRenovable" 
						disabled="true" ></html:checkbox>
					<midas:oculto propiedadFormulario="claveRenovable" nombreFormulario="productoForm"/>	
				</td>		
				<th>
					<etiquetas:etiquetaError property="diasRetroactividad" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.diasretroactividad"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="diasRetroactividad" 
						styleId="diasRetroactividad" maxlength="4" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>
				<th>
					<etiquetas:etiquetaError property="diasDiferimiento" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.diasdiferimiento"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="diasDiferimiento" 
						styleId="diasDiferimiento" maxlength="4" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>
			</tr> 		
			<tr>
				<th>
					<etiquetas:etiquetaError property="diasGracia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.diasgracia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="diasGracia" 
						styleId="diasGracia" maxlength="4" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>			
				<th>
					<etiquetas:etiquetaError property="diasGraciaSubsecuentes" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.diasgracia.recibossubsecuentes"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:text property="diasGraciaSubsecuentes" 
						styleId="diasGraciaSubsecuentes" maxlength="4" 
						onkeypress="return soloNumeros(this, event, false)"
						readonly="true" 
						styleClass="cajaTexto" />
				</td>				
				<th>
					<etiquetas:etiquetaError property="clavePagoInmediato" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.clavepagoinmediato"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<html:checkbox property="clavePagoInmediato" styleId="clavePagoInmediato" 
						disabled="true" ></html:checkbox>
					<midas:oculto propiedadFormulario="clavePagoInmediato" nombreFormulario="productoForm"/>
					
				</td>
			</tr>
		</table>
	</div>	
	<h4>
		Informaci&oacute;n de Configuraci&oacute;n
	</h4>		
	<div class="contenedor">
		<table width="100%" border="0" id="desplegarDetalle">
			<tr>
				<th>
					<etiquetas:etiquetaError property="descripcionRegistroCNFS" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.descripcionRegistroCNSF"
						normalClass="normal" errorClass="error" 
						errorImage="/img/information.gif" />
				</th>
				<td colspan="5">
					<html:textarea property="descripcionRegistroCNFS" cols="90" rows="3" 
						styleClass="cajaTexto" style="resize: none;"
						onkeypress="return soloAlfanumericosM1(this, event, false)"
						onkeydown="return maximaLongitud(this,2000)"
						readonly="true" >
					</html:textarea>
				</td>
			</tr>
		</table>
	</div>
	<h4>
		Documentos requeridos por la CNSF
	</h4>		
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
				<tr>
					<td colspan="6">Archivo registro CNSF</td>
				</tr>
				<tr>
					<td colspan="6">Archivo car&aacute;tula P&oacute;liza</td>
				</tr>
				<tr>
					<td colspan="6">Archivo condiciones del producto</td>
				</tr>
				<tr>
					<td colspan="6">Archivo nota t&eacute;cnica</td>
				</tr>
				<tr>
					<td colspan="6">Archivo an&aacute;lisis de congruencia</td>
				</tr>
				<tr>
					<td colspan="6">Archivo dict&aacute;men jur&iacute;dico</td>
				</tr>
					<tr>
						<td colspan="6">			
							<div id="ArchivoRegistroCNSF">
								<logic:notEqual value="0" property="idControlArchivoRegistroCNSF" name="productoForm" >
									<table>
										<tr>
											<th>Archivo registro CNSF:<midas:oculto propiedadFormulario="idControlArchivoRegistroCNSF" nombreFormulario="productoForm" /></th>
											<td><midas:texto propiedadFormulario="nombreArchivoRegistroCNSF" nombreFormulario="productoForm" deshabilitado="true" /></td>
										</tr>
									</table>
								</logic:notEqual>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="ArchivoCaratulaPoliza">
								<logic:notEqual value="0" property="idControlArchivoCaratulaPoliza" name="productoForm" >
									<table>
										<tr>
											<th>Archivo car&aacute;tula P&oacute;liza:<midas:oculto propiedadFormulario="idControlArchivoCaratulaPoliza" nombreFormulario="productoForm" /></th>
											<td><midas:texto propiedadFormulario="nombreArchivoCaratulaPoliza" nombreFormulario="productoForm" deshabilitado="true" /></td>
										</tr>
									</table>
								</logic:notEqual>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="ArchivoCondicionesProducto">
								
								<logic:notEqual value="0" property="idControlArchivoCondicionesProducto" name="productoForm" >
									<table>
										<tr>
											<th>Archivo condiciones del producto:<midas:oculto propiedadFormulario="idControlArchivoCondicionesProducto" nombreFormulario="productoForm" /></th>
											<td><midas:texto propiedadFormulario="nombreArchivoCondicionesProducto" nombreFormulario="productoForm" deshabilitado="true" /></td>
										</tr>
									</table>
								</logic:notEqual>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="ArchivoNotaTecnica">
								<logic:notEqual value="0" property="idControlArchivoNotaTecnica" name="productoForm" >
									<table>
										<tr>
											<th>Archivo nota t&eacute;cnica:<midas:oculto propiedadFormulario="idControlArchivoNotaTecnica" nombreFormulario="productoForm" /></th>
											<td><midas:texto propiedadFormulario="nombreArchivoNotaTecnica" nombreFormulario="productoForm" deshabilitado="true" /></td>
										</tr>
									</table>
								</logic:notEqual>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="ArchivoAnalisisCongruencia">
								<logic:notEqual value="0" property="idControlArchivoAnalisisCongruencia" name="productoForm" >
									<table>
										<tr>
											<th>Archivo an&aacute;lisis de congruencia:<midas:oculto propiedadFormulario="idControlArchivoAnalisisCongruencia" nombreFormulario="productoForm" /></th>
											<td><midas:texto propiedadFormulario="nombreArchivoAnalisisCongruencia" nombreFormulario="productoForm" deshabilitado="true" /></td>

										</tr>
									</table>
								</logic:notEqual>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="ArchivoDictamenJuridico">
								<logic:notEqual value="0" property="idControlArchivoDictamenJuridico" name="productoForm" >
									<table>
										<tr>
											<th>Archivo dictamen jur&iacute;dico:<midas:oculto propiedadFormulario="idControlArchivoDictamenJuridico" nombreFormulario="productoForm" /></th>
											<td><midas:texto propiedadFormulario="nombreArchivoDictamenJuridico" nombreFormulario="productoForm" deshabilitado="true" /></td>
										</tr>
									</table>
								</logic:notEqual>
							</div>
						</td>
					</tr>
		</table>
	</div>
	<table id="agregar">	
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.productoForm,'/MidasWeb/catalogos/producto/modificar.do', 'contenido','existenErrores(\'listarProductos()\')');">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.productoForm,'/MidasWeb/catalogos/producto/listar.do', 'contenido','cerrarCalendario();');">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
		</tr>					
	</table>
	 <div id="errores" style="display: none;"><html:errors/></div>	
</midas:formulario>