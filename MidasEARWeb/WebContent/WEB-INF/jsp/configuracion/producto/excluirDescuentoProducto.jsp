<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/producto/mostrarAsociarExclusionDescuento">
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.descuento.tipopoliza" /></td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.tipopoliza" /></center></th>
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesDescuentoProductoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.tipopoliza.disponible" /></center></th>
				</tr>
				<tr>
					<td>Tipo de P&oacute;liza:<html:hidden property="idToProducto" styleId="idToProducto" /></td>
					<td width="25%">
						<midas:combo styleClass="cajaTexto" propiedad="idToProducto" id="tipoPolizaSelect_des" onchange="mostrarExclusionesDescuentoProductoPorPoliza(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="productoForm"  property="tiposPoliza" value="idToTipoPoliza" label="nombreComercial"/>
						</midas:combo>
					</td>
					<td>Descuento:</td>
					<td width="25%">
						<midas:combo styleClass="cajaTexto" propiedad="idToProducto" id="descuentoSelect" onchange="mostrarExclusionesDescuentoProductoPorDescuento(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="productoForm"  property="descuentosAsociados" value="id.idToDescuentoVario" label="descuentoDTO.descripcion"/>
						</midas:combo>
					</td>					
				</tr>			
				<tr>
					<td colspan="4">
						<div id="exclusionesDescuentoProductoNoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>		
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>			
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: exclusionDescuentoProductoProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionProductoTabBar, 'detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>		