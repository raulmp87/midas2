<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/producto/mostrarAsociarExclusionRecargo">
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.recargo.tipopoliza" /></td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.tipopoliza" /></center></th>
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesRecargoProductoGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.tipopoliza.disponible" /></center></th>
				</tr>
				<tr>
					<td>Tipo de P&oacute;liza:<html:hidden property="idToProducto" styleId="idToProducto" /></td>
					<td width="25%">
						<midas:combo styleClass="cajaTexto" propiedad="idToProducto" id="tipoPolizaSelect_rec" onchange="mostrarExclusionesRecargoProductoPorPoliza(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="productoForm"  property="tiposPoliza" value="idToTipoPoliza" label="nombreComercial"/>
						</midas:combo>
					</td>
					<td>Recargo:</td>
					<td width="25%">
					<midas:combo styleClass="cajaTexto" propiedad="idToProducto" id="recargoSelect" onchange="mostrarExclusionesRecargoProductoPorRecargo(this)">
						<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
						<html:optionsCollection name="productoForm"  property="recargosAsociados" value="id.idToRecargoVario" label="recargoVarioDTO.descripcionrecargo"/>
					</midas:combo>
					</td>					
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesRecargoProductoNoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>			
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: exclusionRecargoProductoProcessor.sendData(); mostrarMensajeExito(); configuracionProductoTabBar.setTabActive('detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>		