<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/producto/asociarRecargo">
			<midas:oculto propiedadFormulario="idToProducto" nombreFormulario="productoForm"/>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.recargo" /> Producto</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.recargo.asociado" /> Producto</td>
				</tr>		
				<tr>
					<td colspan="4">
						<div id="recargosAsociadosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.recargo.disponible" /> Producto</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="recargosPorAsociarGrid" class="dataGridConfigurationClass"></div>
					</td>		
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: actualizarGridProducto('recargosProductoProcessor', 'recargosProductoError');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>