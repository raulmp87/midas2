<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/catalogos/producto/borrar">
	<table id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.borrar" /> Producto
				<logic:equal value="A" name="productoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="productoForm" property="claveNegocio">
				Da�os
				</logic:notEqual>
				<midas:oculto propiedadFormulario="idToProducto" nombreFormulario="productoForm"/>
				<midas:oculto nombreFormulario="productoForm" propiedadFormulario="claveNegocio"/>
				<midas:oculto propiedadFormulario="claveNegocio"/>
			</td>
		</tr>
			 <tr><th><br><b>Informaci&oacute;n General del Producto.</b><br></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.codigo" />:</th>
			<td><midas:escribe propiedad="codigo" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.version" />:</th>
			<td><midas:escribe propiedad="version" nombre="productoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.claveEstatus" />:</th>
			<td><midas:escribe propiedad="descripcionEstatus" nombre="productoForm" /></td>
			<th><midas:mensaje clave="configuracion.producto.nombrecomercial" />:</th>
			<td colspan="2"><midas:escribe propiedad="nombreComercial" nombre="productoForm"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="configuracion.producto.descripcion" />:</th>
			<td colspan="4"><midas:escribe propiedad="descripcion" nombre="productoForm"/></td>
		</tr>
		<tr><th><br>Informaci&oacute;n de Vigencias.<br></th></tr>		
		<tr>
			<th><midas:mensaje clave="configuracion.producto.fechainiciovigencia" />:</th>
			<td><midas:escribe propiedad="fechaInicioVigencia" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.claveunidadvigencia" />:</th>
			<td><midas:escribe propiedad="descripcionUnidadVigencia" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.ajusteporvigencia" />:</th>
			<td><midas:check propiedadFormulario="claveAjusteVigencia" id="claveAjusteVigencia" deshabilitado="true"/></td>						
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.valorminimounidadvigencia" />:</th>
			<td><midas:escribe propiedad="valorMinimoUnidadVigencia" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.valormaximounidadvigencia" />:</th>
			<td><midas:escribe propiedad="valorMaximoUnidadVigencia" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.valordefaultunidadvigencia" />:</th>
			<td><midas:escribe propiedad="valorDefaultUnidadVigencia" nombre="productoForm"/></td>
		</tr>
		<tr><th><br>Informaci&oacute;n de Pol&iacute;ticas de Venta.<br></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.claverenovable" />:</th>
			<td><midas:check propiedadFormulario="claveRenovable" id="claveRenovable" deshabilitado="true"/></td>		
			<th><midas:mensaje clave="configuracion.producto.diasretroactividad" />:</th>
			<td><midas:escribe propiedad="diasRetroactividad" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.diasdiferimiento" />:</th>
			<td><midas:escribe propiedad="diasDiferimiento" nombre="productoForm"/></td>
		</tr> 		
		<tr>
			<th><midas:mensaje clave="configuracion.producto.diasgracia" />:</th>
			<td><midas:escribe propiedad="diasGracia" nombre="productoForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.clavepagoinmediato" />:</th>
			<td><midas:check propiedadFormulario="clavePagoInmediato" id="clavePagoInmediato" deshabilitado="true"/></td>
		</tr>
		<tr><th><br>Informaci&oacute;n de Configuraci&oacute;n.<br></th></tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.diasgracia.recibossubsecuentes" />:</th>
			<td><midas:escribe propiedad="diasGraciaSubsecuentes" nombre="productoForm"/></td>	
			<th><midas:mensaje clave="configuracion.producto.descripcionRegistroCNSF" />:</th>
			<td colspan="2"><midas:escribe propiedad="descripcionRegistroCNFS" nombre="productoForm"/></td>
		</tr>
		<tr><th><br>Documentos requeridos por la CNFS<br></th></tr>
		<tr> 
			<logic:equal value="0" property="idControlArchivoRegistroCNSF" name="productoForm" >
				<td colspan="2"><midas:mensaje clave="configuracion.producto.idControlArchivoRegistroCNSF.noRegistrado" />.</td>
			</logic:equal>
			<logic:notEqual value="0" property="idControlArchivoRegistroCNSF" name="productoForm" >
				<th><midas:mensaje clave="configuracion.producto.idControlArchivoRegistroCNSF" />:<midas:oculto propiedadFormulario="idControlArchivoRegistroCNSF" nombreFormulario="productoForm" /></th>
				<td><midas:texto propiedadFormulario="nombreArchivoRegistroCNSF" nombreFormulario="productoForm" deshabilitado="true" /></td>
			</logic:notEqual>
		</tr>
		<tr> 
			<logic:equal value="0" property="idControlArchivoCaratulaPoliza" name="productoForm" >
				<td colspan="2"><midas:mensaje clave="configuracion.producto.idControlArchivoCaratulaPoliza.noRegistrado" />.</td>
			</logic:equal>
			<logic:notEqual value="0" property="idControlArchivoCaratulaPoliza" name="productoForm" >
				<th><midas:mensaje clave="configuracion.producto.idControlArchivoCaratulaPoliza" />:<midas:oculto propiedadFormulario="idControlArchivoCaratulaPoliza" nombreFormulario="productoForm" /></th>
				<td><midas:texto propiedadFormulario="nombreArchivoCaratulaPoliza" nombreFormulario="productoForm" deshabilitado="true" /></td>
			</logic:notEqual>
		</tr>
		<tr> 
			<logic:equal value="0" property="idControlArchivoCaratulaPoliza" name="productoForm" >
				<td colspan="2"><midas:mensaje clave="configuracion.producto.idControlArchivoCondicionesProducto.noRegistrado" />.</td>
			</logic:equal>
			<logic:notEqual value="0" property="idControlArchivoCondicionesProducto" name="productoForm" >
				<th><midas:mensaje clave="configuracion.producto.idControlArchivoCondicionesProducto" />:<midas:oculto propiedadFormulario="idControlArchivoCondicionesProducto" nombreFormulario="productoForm" /></th>
				<td><midas:texto propiedadFormulario="nombreArchivoCondicionesProducto" nombreFormulario="productoForm" deshabilitado="true" /></td>
			</logic:notEqual>
		</tr>
		<tr> 
			<logic:equal value="0" property="idControlArchivoNotaTecnica" name="productoForm" >
				<td colspan="2"><midas:mensaje clave="configuracion.producto.idControlArchivoNotaTecnica.noRegistrado" />.</td>
			</logic:equal>
			<logic:notEqual value="0" property="idControlArchivoNotaTecnica" name="productoForm" >
				<th><midas:mensaje clave="configuracion.producto.idControlArchivoNotaTecnica" />:<midas:oculto propiedadFormulario="idControlArchivoNotaTecnica" nombreFormulario="productoForm" /></th>
				<td><midas:texto propiedadFormulario="nombreArchivoNotaTecnica" nombreFormulario="productoForm" deshabilitado="true" /></td>
			</logic:notEqual>
		</tr>
		<tr> 
			<logic:equal value="0" property="idControlArchivoAnalisisCongruencia" name="productoForm" >
				<td colspan="2"><midas:mensaje clave="configuracion.producto.idControlArchivoAnalisisCongruencia.noRegistrado" />.</td>
			</logic:equal>
			<logic:notEqual value="0" property="idControlArchivoAnalisisCongruencia" name="productoForm" >
				<th><midas:mensaje clave="configuracion.producto.idControlArchivoAnalisisCongruencia" />:<midas:oculto propiedadFormulario="idControlArchivoAnalisisCongruencia" nombreFormulario="productoForm" /></th>
				<td><midas:texto propiedadFormulario="nombreArchivoAnalisisCongruencia" nombreFormulario="productoForm" deshabilitado="true" /></td>
			</logic:notEqual>
		</tr>
		<tr> 
			<logic:equal value="0" property="idControlArchivoDictamenJuridico" name="productoForm" >
				<td colspan="2"><midas:mensaje clave="configuracion.producto.idControlArchivoDictamenJuridico.noRegistrado" />.</td>
			</logic:equal>
			<logic:notEqual value="0" property="idControlArchivoDictamenJuridico" name="productoForm" >
				<th><midas:mensaje clave="configuracion.producto.idControlArchivoDictamenJuridico" />:<midas:oculto propiedadFormulario="idControlArchivoDictamenJuridico" nombreFormulario="productoForm" /></th>
				<td><midas:texto propiedadFormulario="nombreArchivoDictamenJuridico" nombreFormulario="productoForm" deshabilitado="true" /></td>
			</logic:notEqual>
		</tr>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<c:choose>
						<c:when test="${productoForm.claveEstatus==1}">
							<div id="b_borrar" style="margin-right: 4px">
								<a href="javascript: void(0);"
								    onclick="javascript: alert('El Producto se encuentra Activo en Producci\u00f3n');">
									<midas:mensaje clave="midas.accion.borrar"/>
								</a>
							</div>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${productoForm.claveEstatus==2}">
									<div id="b_borrar" style="margin-right: 4px">
										<a href="javascript: void(0);"
								    		onclick="javascript: alert('El Producto se encuentra Inactivo');">
											<midas:mensaje clave="midas.accion.borrar"/>
										</a>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${not empty productoForm.tiposPoliza}">
											<div id="b_borrar" style="margin-right: 4px">
												<a href="javascript: void(0);"
													onclick="javascript: Confirma('El Producto tiene tipos de P&oacute;liza registrados. �Desea continuar el Borrado?',document.productoForm,'/MidasWeb/catalogos/producto/borrar.do', 'contenido',null);">
													<midas:mensaje clave="midas.accion.borrar"/>
												</a>
											</div>
										</c:when>
										<c:otherwise>
											<div id="b_borrar" style="margin-right: 4px">
												<a href="javascript: void(0);"
													onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.productoForm,'/MidasWeb/catalogos/producto/borrar.do', 'contenido',null);">
													<midas:mensaje clave="midas.accion.borrar"/>
												</a>
											</div>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>
			</td>      		
		</tr>
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">							
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.productoForm,'/MidasWeb/catalogos/producto/listar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>				
	</table>

</midas:formulario>