<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario  accion="/catalogos/producto/listar">
<div id="propiedades">
	<midas:oculto propiedadFormulario="claveNegocio"/>
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Productos Afirme
				<logic:equal value="A" name="productoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="productoForm" property="claveNegocio">
				Daños
				</logic:notEqual>	
			</td>
		</tr>
		<tr>		
			<th><midas:mensaje clave="configuracion.tipopoliza.codigo"/>:</th>
			<td><midas:texto propiedadFormulario="codigo" 
					id="codigo" caracteres="8"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<th><midas:mensaje clave="configuracion.tipopoliza.version"/>:</th>
			<td><midas:texto propiedadFormulario="version" 
					id="version" caracteres="4"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>			
		</tr> 
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.descripcion"/>:</th>
			<td>
				<html:text property="descripcion" maxlength="200"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>
			</td>
			<th><midas:mensaje clave="configuracion.tipopoliza.nombrecomercial"/>:</th>
			<td>
				<html:text property="nombreComercial" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>
			</td>
		</tr> 
		
		<logic:equal value="A" property="claveNegocio" name="productoForm" >
			<tr>
				<th><midas:mensaje clave="configuracion.tipopoliza.claveaplicaflotillas"/>:</th>
				<td><midas:check propiedadFormulario="claveAplicaFlotillas" 
						id="claveAplicaFlotillas"/>
				</td>				
				<th><midas:mensaje clave="configuracion.tipopoliza.claveaplicaautoexpedible"/>:</th>
				<td><midas:check propiedadFormulario="claveAplicaAutoexpedible" 
						id="claveAplicaAutoexpedible"/>
				</td>
			</tr>
		</logic:equal>				
		<tr>
			<th><midas:mensaje clave="configuracion.tipopoliza.claverenovable"/>:</th>
			<td><midas:check propiedadFormulario="claveRenovable" id="claveRenovable"/></td>			
			<th><midas:mensaje clave="configuracion.producto.mostrarInactivos"/></th>
			<td><midas:checkBox valorEstablecido="1" propiedadFormulario="mostrarInactivos"></midas:checkBox></td>			
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: sendRequest(document.productoForm,'/MidasWeb/catalogos/producto/listarFiltrado.do', 'contenido',null);" tipo="buscar"/>
				</div>
			</td>      		
		</tr>
	</table>
	<br/>
	<div id="resultados" style="width:98%;height:270px">
		<midas:tabla idTabla="productos"
			claseDecoradora="mx.com.afirme.midas.decoradores.Producto"
			claseCss="tablaConResultados" nombreLista="productos"
			urlAccion="/catalogos/producto/listarFiltrado.do">
			<midas:columna propiedad="codigo" titulo="C&oacute;digo"/>
			<midas:columna propiedad="version" titulo="Versi&oacute;n"/>
			<midas:columna propiedad="nombreComercial" titulo="Nombre"/>
			<midas:columna propiedad="fechaCreacion" titulo="Fecha de Creaci&oacute;n"/>
			<midas:columna propiedad="acciones" />
					
		</midas:tabla>			
	</div>
	<div class="alinearBotonALaDerecha" style="margin-right: 20px">
<%-- 				<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/producto/mostrarAgregar.do?negocio=$('clavenegocio').value', 'contenido',null);" tipo="agregar"/> --%>
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: sendRequestJQ(null,'/MidasWeb/catalogos/producto/mostrarAgregar.do?negocio=<midas:escribe propiedad="claveNegocio" nombre="productoForm"/>', 'contenido',null);">
				<midas:mensaje clave="midas.accion.agregar"/>
			</a>
		</div>
	</div>
</div>	
</midas:formulario>
