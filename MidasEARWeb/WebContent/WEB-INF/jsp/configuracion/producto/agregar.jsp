<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>



<midas:formulario accion="/catalogos/producto/agregar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	<midas:oculto nombreFormulario="productoForm" propiedadFormulario="claveNegocio"/>
	<%--<midas:oculto propiedadFormulario="fechaInicioVigencia"/> --%>

	<table width="100%" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.agregar" />  Producto
				<logic:equal value="A" name="productoForm" property="claveNegocio">
				Autos
				</logic:equal>
				<logic:notEqual value="A" name="productoForm" property="claveNegocio">
				Daños
				</logic:notEqual>
			</td>
		</tr>	
	</table>
	<h4>
		Informaci&oacute;n General del Producto
	</h4>
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<th><etiquetas:etiquetaError property="codigo" requerido="si"
							name="productoForm" key="configuracion.producto.codigo"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" 
						/>		
				</th>
				<td width="120px">
					<midas:texto propiedadFormulario="codigo" 
							id="codigo" caracteres="8"
							onkeypress="return soloNumeros(this, event, false)" 
						/>
				</td>
				<th width="100px">
					<etiquetas:etiquetaError property="nombreComercial" 
							requerido="si" name="productoForm" 
							key="configuracion.producto.nombrecomercial"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" 
						/>
					
				</th>
				<td width="450px" colspan="3">
					<html:text property="nombreComercial" maxlength="100"
								styleClass="jQalphaextra jQrestrict cajaTexto" 
							/>
					
				</td>
			</tr>
			<tr>
				<th>
					<etiquetas:etiquetaError property="descripcion" 
							requerido="si" name="productoForm" 
							key="configuracion.producto.descripcion"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" 
						/>
				</th>
				<td colspan="5">
					<html:text property="descripcion" maxlength="200"
							styleClass="jQalphaextra jQrestrict cajaTexto" 
						/>
				</td>
			</tr> 		
		</table>	
	</div>
	<h4>
		Informaci&oacute;n de Vigencias
	</h4>	
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<th>
					<etiquetas:etiquetaError property="claveUnidadVigencia" 
						requerido="si" name="productoForm"
						key="configuracion.producto.claveunidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td width="200px">
					<midas:comboValorFijo grupoValores="23" 
						propiedad="claveUnidadVigencia" nombre="productoForm" 
						styleClass="cajaTexto"/>
				</td>
				<th>
					<etiquetas:etiquetaError property="valorMinimoUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.valorminimounidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<midas:texto propiedadFormulario="valorMinimoUnidadVigencia" 
						id="valorMinimoUnidadVigencia" caracteres="8"
						onkeypress="return soloNumeros(this, event, false)" />
				</td>
				<th>
					<etiquetas:etiquetaError property="valorMaximoUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.valormaximounidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<midas:texto propiedadFormulario="valorMaximoUnidadVigencia" 
						id="valorMaximoUnidadVigencia" caracteres="8"
						onkeypress="return soloNumeros(this, event, false)" />
				</td>			
			</tr>
			<tr>
				<th>
					<etiquetas:etiquetaError property="valorDefaultUnidadVigencia" 
						requerido="si" name="productoForm" 
						key="configuracion.producto.valordefaultunidadvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td width="200px">
					<midas:texto propiedadFormulario="valorDefaultUnidadVigencia" 
						onkeypress="return soloNumeros(this, event, false)"
						id="valorDefaultUnidadVigencia" caracteres="8"/>
				</td>
				<th>
					<etiquetas:etiquetaError property="claveAjusteVigencia" requerido="no"
						name="productoForm" key="configuracion.producto.ajusteporvigencia"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td>
					<midas:check propiedadFormulario="claveAjusteVigencia" 
						id="claveAjusteVigencia"/>
				</td>					
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<th>
					<div id="etiquetaFecha">
						<etiquetas:etiquetaError normalClass="normal" 
							errorClass="error" errorImage="/img/information.gif" 
							name="productoForm" property="fechaInicioVigencia" 
							key="configuracion.producto.fechainiciovigencia" 
							requerido="si"/>	
						<logic:equal value="false" property="soloLectura" name="productoForm" >										
							<logic:equal value="1" property="claveEstatus" name="productoForm" >	
								<a href="javascript: void(0);" id="mostrarCalendario" 
									onclick="javascript: mostrarCalendarioOT();">
									<image src="/MidasWeb/img/b_calendario.gif" border="0"/>
								</a>
							</logic:equal>	
							<logic:notEqual value="1" property="claveEstatus" name="productoForm" >	
								<a href="javascript: void(0);" id="mostrarCalendario" 
									onclick="javascript: mostrarCalendarioOT();">
									<image src="/MidasWeb/img/b_calendario.gif" border="0"/>
								</a>
							</logic:notEqual>						
					 	</logic:equal>
					</div>
		   		</th>		   	
		   		<th width="200px">
			   		<html:text property="fechaInicioVigencia"  maxlength="10" 
			   			size="15" styleId="fecha" name="productoForm"  
			   			styleClass="cajaTexto"
						onkeypress="return soloFecha(this, event, false);"
						onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
						onblur="esFechaValida(this);" 
						readonly="${productoForm.soloLectura}" />
				</th>
			</tr>		   					
		</table>
	</div>
	<h4>
		Informaci&oacute;n de Pol&iacute;ticas de Venta
	</h4>		
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<th><etiquetas:etiquetaError property="claveRenovable" requerido="no"
						name="productoForm" key="configuracion.producto.claverenovable"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td><html:checkbox property="claveRenovable" ></html:checkbox></td>		
				<th><etiquetas:etiquetaError property="diasRetroactividad" requerido="si"
									name="productoForm" key="configuracion.producto.diasretroactividad"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
				</th>
				<td><midas:texto propiedadFormulario="diasRetroactividad" 
									onkeypress="return soloNumeros(this, event, false)" id="diasRetroactividad" caracteres="4"/>	
				</td>
				<th><etiquetas:etiquetaError property="diasDiferimiento" requerido="si"
									name="productoForm" key="configuracion.producto.diasdiferimiento"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
				</th>
				<td><midas:texto propiedadFormulario="diasDiferimiento"
									onkeypress="return soloNumeros(this, event, false)" id="diasDiferimiento" caracteres="4"/>
				</td>
			</tr> 		
			<tr>
				<th><etiquetas:etiquetaError property="diasGracia" requerido="si"
									name="productoForm" key="configuracion.producto.diasgracia"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
				</th>
				<td><midas:texto propiedadFormulario="diasGracia" 
									onkeypress="return soloNumeros(this, event, false)" id="diasGracia" caracteres="4"/>
				</td>	
				
				<th><etiquetas:etiquetaError property="diasGraciaSubsecuentes" requerido="si"
									name="productoForm" key="configuracion.producto.diasgracia.recibossubsecuentes"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
				</th>
				<td><midas:texto propiedadFormulario="diasGraciaSubsecuentes" 
									onkeypress="return soloNumeros(this, event, false)" id="diasGraciaSubsecuentes" caracteres="4"/>
				</td>	
					
				<th><etiquetas:etiquetaError property="clavePagoInmediato" requerido="no"
						name="productoForm" key="configuracion.producto.clavepagoinmediato"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" />
				</th>
				<td><midas:check propiedadFormulario="clavePagoInmediato" id="clavePagoInmediato"/></td>
				<th></th>
				<td></td>
			</tr>
		
		</table>
	</div>	
	
	<h4>
		Informaci&oacute;n de Configuraci&oacute;n
	</h4>		
	<div class="contenedor">
		<table width="100%" border="0" id="desplegarDetalle">
			<tr>
				<th><etiquetas:etiquetaError property="descripcionRegistroCNFS" requerido="si"
						name="productoForm" key="configuracion.producto.descripcionRegistroCNSF"
						normalClass="normal" errorClass="error" errorImage="/img/information.gif" /></th>
				<td colspan="3">
					<midas:areatexto propiedadFormulario="descripcionRegistroCNFS" 
					 onkeypress="return soloAlfanumericosM1(this, event, false)" caracteres="2000" />
				</td>
			</tr>
		</table>
	</div>
	<h4>
		Documentos requeridos por la CNSF
	</h4>		
	<div class="contenedor">
		<table width="100%" border="0" id="agregar">
			<tr>
				<div id="ArchivoRegistroCNSF">
					<logic:equal value="0" property="idControlArchivoRegistroCNSF" name="productoForm" >
						<a href="javascript:void(0);" onclick="mostrarAdjuntarArchivoRegistroCNSFProductoWindow()" >Adjuntar archivo registro CNSF</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoRegistroCNSF" name="productoForm" >
						<table border="0">
							<tr>
								<th>Archivo registro CNSF:<midas:oculto propiedadFormulario="idControlArchivoRegistroCNSF" nombreFormulario="productoForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoRegistroCNSF" nombreFormulario="productoForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarAdjuntarArchivoRegistroCNSFProductoWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</tr>
			<tr>
				<div id="ArchivoCaratulaPoliza">
					<logic:equal value="0" property="idControlArchivoCaratulaPoliza" name="productoForm" >
						<a href="javascript:void(0);" onclick="mostrarAdjuntarArchivoCaratulaPolizaProductoWindow()" >Adjuntar archivo car&aacute;tula P&oacute;liza</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoCaratulaPoliza" name="productoForm" >
						<table border="0">
							<tr>
								<th>Archivo car&aacute;tula P&oacute;liza:<midas:oculto propiedadFormulario="idControlArchivoCaratulaPoliza" nombreFormulario="productoForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoCaratulaPoliza" nombreFormulario="productoForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarAdjuntarArchivoCaratulaPolizaProductoWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</tr>
			<tr>
				<div id="ArchivoCondicionesProducto">
					<logic:equal value="0" property="idControlArchivoCondicionesProducto" name="productoForm" >
						<a href="javascript:void(0);" onclick="mostrarArchivoCondicionesProductoProductoWindow()" >Adjuntar archivo de condiciones del producto</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoCondicionesProducto" name="productoForm" >
						<table border="0">
							<tr>
								<th>Archivo condiciones del producto:<midas:oculto propiedadFormulario="idControlArchivoCondicionesProducto" nombreFormulario="productoForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoCondicionesProducto" nombreFormulario="productoForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarArchivoCondicionesProductoProductoWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</tr>
			<tr>
				<div id="ArchivoNotaTecnica">
					<logic:equal value="0" property="idControlArchivoNotaTecnica" name="productoForm" >
						<a href="javascript:void(0);" onclick="mostrarArchivoNotaTecnicaProductoWindow()" >Adjuntar archivo nota t&eacute;cnica</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoNotaTecnica" name="productoForm" >
						<table border="0">
							<tr>
								<th>Archivo nota t&eacute;cnica:<midas:oculto propiedadFormulario="idControlArchivoNotaTecnica" nombreFormulario="productoForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoNotaTecnica" nombreFormulario="productoForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarArchivoNotaTecnicaProductoWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</tr>
			<tr>
				<div id="ArchivoAnalisisCongruencia">
					<logic:equal value="0" property="idControlArchivoAnalisisCongruencia" name="productoForm" >
						<a href="javascript:void(0);" onclick="mostrarArchivoAnalisisCongruenciaProductoWindow()" >Adjuntar an&aacute;lisis de congruencia</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoAnalisisCongruencia" name="productoForm" >
						<table border="0">
							<tr>
								<th>Archivo an&aacute;lisis de congruencia:<midas:oculto propiedadFormulario="idControlArchivoAnalisisCongruencia" nombreFormulario="productoForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoAnalisisCongruencia" nombreFormulario="productoForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarArchivoAnalisisCongruenciaProductoWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</tr>
			<tr>
				<div id="ArchivoDictamenJuridico">
					<logic:equal value="0" property="idControlArchivoDictamenJuridico" name="productoForm" >
						<a href="javascript:void(0);" onclick="mostrarArchivoDictamenJuridicoProductoWindow()" >Adjuntar dictamen jur&iacute;dico</a>
					</logic:equal>
					<logic:notEqual value="0" property="idControlArchivoDictamenJuridico" name="productoForm" >
						<table border="0">
							<tr>
								<th>Archivo dictamen jur&iacute;dico:<midas:oculto propiedadFormulario="idControlArchivoDictamenJuridico" nombreFormulario="productoForm" /></th>
								<td><midas:texto propiedadFormulario="nombreArchivoDictamenJuridico" nombreFormulario="productoForm" deshabilitado="true" /></td>
								<td><a href="javascript:void(0);" onclick="mostrarArchivoDictamenJuridicoProductoWindow()" >Actualizar documento CNSF</a></td>
							</tr>
						</table>
					</logic:notEqual>
				</div>
			</tr>
		</table>
	</div>
	<table id="agregar">	
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.productoForm,'/MidasWeb/catalogos/producto/agregar.do', 'contenido','existenErrores(\'listarProductos()\')');">
							<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/producto/listarFiltrado.do?negocio=<midas:escribe propiedad="claveNegocio" nombre="productoForm"/>', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td>
		</tr>	
	</table>
	 <div id="errores" style="display: none;"><html:errors/></div>	
</midas:formulario>

