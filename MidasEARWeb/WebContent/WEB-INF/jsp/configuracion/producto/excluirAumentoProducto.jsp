<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ page isELIgnored="false"%>

<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/producto/mostrarAsociarExclusionAumento">		
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.excluir.aumento.tipopoliza" /></td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.tipopoliza" /></center></th>
				</tr>
				<tr>
					<td colspan="4">
						<div id="exclusionesAumentoProductoGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<th colspan="4"><center><midas:mensaje clave="configuracion.excluir.lista.tipopoliza.disponible" /><html:hidden property="idToProducto" styleId="idToProducto" /></center></th>
				</tr>
				<tr>
					<td>Tipo de P&oacute;liza:</td>
					<td width="25%">
						<midas:combo styleClass="cajaTexto" propiedad="idToProducto" id="tipoPolizaSelect_aum" onchange="mostrarExclusionesAumentoProductoPorPoliza(this)">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="productoForm"  property="tiposPoliza" value="idToTipoPoliza" label="nombreComercial"/>
						</midas:combo>								
					</td>
					<td>Aumento:  </td>
					<td width="25%">
						<html:select property="idToProducto" styleClass="cajaTexto" styleId="aumentoSelect" onchange="mostrarExclusionesAumentoProductoPorAumento(this)" >
							<html:option value="">Seleccione...</html:option>
							<html:optionsCollection name="productoForm"  property="aumentosAsociados" value="id.idtoaumentovario" label="aumentoVarioDTO.descripcionAumento"/>
						</html:select>
					</td>					
				</tr>	
				<tr>
					<td colspan="4">
						<div id="exclusionesProductoNoAsociadasGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>						
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
					<a href="javascript: void(0);"
						onclick="javascript: exclusionAumentoProductoProcessor.sendData(); mostrarMensajeExito();configuracionProductoTabBar.setTabActive('detalle');"><midas:mensaje
							clave="midas.accion.guardar"/>
					</a>
				</div>
			</div>			
		</midas:formulario>			
	</center>
</div>
