<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
		

<div style= "height: 450px; overflow:auto" hrefmode="ajax-html"  id="configuracionProductoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div id="detalle" name="Detalle" extraAction="if(document.getElementById('mensaje') != null) document.getElementById('mensaje').style.display = 'none';">
		<center>
			<midas:formulario accion="/catalogos/producto/listar">		
				<table width="100%" border="0">
					<tr>
						<td class="agregar" colspan="4">
							<b><midas:escribe propiedad="nombreComercial" nombre="productoForm"/></b>
						</td>
					</tr>
				</table>
				
				<table id="desplegar" border="0">
					<tr>
						<midas:oculto propiedadFormulario="idToProducto" nombreFormulario="productoForm"/>
						<th><midas:mensaje clave="configuracion.producto.codigo"/>:</th>
						<td class="txt_v"><midas:escribe propiedad="codigo" nombre="productoForm" /></td>
						<th><midas:mensaje clave="configuracion.producto.valorminimounidadvigencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMinimoUnidadVigencia" nombre="productoForm"/></td>				
					</tr> 					
					<tr class="bg_t2">	
						<th><midas:mensaje clave="configuracion.producto.version" />:</th>
						<td class="txt_v"><midas:escribe propiedad="version" nombre="productoForm"/></td>
						<th><midas:mensaje clave="configuracion.producto.valormaximounidadvigencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="valorMaximoUnidadVigencia" nombre="productoForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.producto.claveEstatus" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionEstatus" nombre="productoForm" /></td>
						<th><midas:mensaje clave="configuracion.producto.diasretroactividad" />:</th>				
						<td class="txt_v"><midas:escribe propiedad="diasRetroactividad" nombre="productoForm"/></td>
					</tr>
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.producto.descripcion" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcion" nombre="productoForm"/></td>
						<th><midas:mensaje clave="configuracion.producto.diasdiferimiento" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasDiferimiento" nombre="productoForm"/></td>										
					</tr>				
					<tr>
						<th><midas:mensaje clave="configuracion.producto.nombrecomercial" />:</th>
						<td class="txt_v"><midas:escribe propiedad="nombreComercial" nombre="productoForm"/></td>
						<th><midas:mensaje clave="configuracion.producto.diasgracia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasGracia" nombre="productoForm"/></td>					
					</tr> 
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.producto.fechainiciovigencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="fechaInicioVigencia" nombre="productoForm"/></td>
						<th><midas:mensaje clave="configuracion.producto.diasgracia.recibossubsecuentes" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasGraciaSubsecuentes" nombre="productoForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.producto.claveunidadvigencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionUnidadVigencia" nombre="productoForm"/></td>
						<th ><midas:mensaje clave="configuracion.producto.clavepagoinmediato" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="clavePagoInmediato" id="clavePagoInmediato" deshabilitado="true"/></td>
					</tr>
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.producto.claverenovable" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveRenovable" id="claveRenovable" deshabilitado="true"/></td>
						<td>
							
								<div style = "float: right; width: 150px"  align="right" id="b_nuevaVersion">
									<a href="javascript: void(0);"
									    onclick="javascript: generarNuevaVersion(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)">
										Generar Version
									</a>
								</div>
												
						</td>
					</tr>
				</table>		
				<div class="subtitulo" style="width: 98%;">Tipos de P&oacute;liza Asociados</div>					
				<div align="left">
					<midas:check propiedadFormulario="mostrarTipoPolizaInactivas" id="mostrarTipoPolizaInactivas" onClick="cargaDataGridproductoBorrados(document.productoForm.idToProducto.value,document.productoForm.mostrarTipoPolizaInactivas);">Mostrar Inactivos</midas:check>
				</div>
				<div id="tipoPolizaGrid" width="97%" height="125"></div>
				<div id="pagingArea"></div><div id="infoArea"></div>
				<div id="botonAgregar">
					<div class="alinearBotonALaDerecha">
						<div id="b_agregar">
							<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarAgregar.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'configuracion_detalle',null);">
								<midas:mensaje clave="midas.accion.agregar"/>
							</a>
						</div>
					</div>
				</div>				
			</midas:formulario>
		</center>
	</div>
		<div width="100%" id="documentos" name="Documentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/catalogos/producto/listarDocumentosAnexos.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_documentos' , 'mostrarDocumentosProductoGrid(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="100%" id="moneda" name="Monedas" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/asociarMoneda.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_moneda' , 'mostrarMonedaProductoGrids(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="100%" id="formapago" name="Formas de Pago" href="http://void" extraAction="sendRequestJQ(null,'/MidasWeb/relaciones/producto/mostrarRelacionFormasPago.action?idProducto=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_formapago' , 'iniciaGridsFormaPago()')"></div>
		<div width="100%" id="mediopago" name="Medios de Pago" href="http://void" extraAction="sendRequestJQ(null,'/MidasWeb/relaciones/producto/mostrarRelacionMediosPago.action?idProducto=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_mediopago' , 'iniciaGridsMedioPago()')"></div>
		<div width="100%" id="ramos" name="Ramos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/mostrarAsociarRamo.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_ramos', 'mostrarRamosProductoGrid(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="100%" id="aumentos" name="Aumentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/asociarAumento.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_aumentos', ' mostrarAumentosProductoGrids(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="150px" id="exclusionAumentos" name="Exclusi&oacute;n Aumentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/mostrarAsociarExclusionAumento.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_exclusionAumentos' , 'mostrarExclusionAumentoProducto(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="100%" id="descuentos" name="Descuentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/asociarDescuento.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_descuentos', 'mostrarDescuentosProductoGrids(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="150px" id="exclusionDescuentos" name="Exclusi&oacute;n Descuentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/mostrarAsociarExclusionDescuento.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_exclusionDescuentos' , 'mostrarExclusionDescuentoProducto(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="100%" id="recargos" name="Recargos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/asociarRecargo.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_recargos', 'mostrarRecargosProductoGrids(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
		<div width="150px" id="exclusionRecargos" name="Exclusi&oacute;n Recargos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/producto/mostrarAsociarExclusionRecargo.do?id=<midas:escribe propiedad="idToProducto" nombre="productoForm"/>', 'contenido_exclusionRecargos' , 'mostrarExclusionRecargoProducto(<midas:escribe propiedad="idToProducto" nombre="productoForm"/>)')"></div>
</div>
