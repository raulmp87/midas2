<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/seccion/mostrarTipoDeCedula">
			<midas:oculto propiedadFormulario="idToSeccion" nombreFormulario="seccionForm"/>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.cedula" /> Secci�n</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.cedula.asociado" /></td>
				</tr>		
				<tr>
					<td colspan="4">
						<div id="cedulasAsociadasGrid" class="cedulasDataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.paquete.disponible" /> Secci�n</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="cedulasPorAsociarGrid" class="cedulasDataGridConfigurationClass"></div>
					</td>		
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>
			</table>
			<input id="idSeccion" type="hidden" value="">
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="guardarTipoDeCedulaGrids()"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>