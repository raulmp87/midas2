<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/configuracion/seccion/modificar">
	<table width="100%" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.modificar" />
				Secci&oacute;n:&nbsp; 
				<midas:escribe propiedad="nombreComercial" nombre="seccionForm"/>
				&nbsp;a Tipo de P&oacute;liza: <midas:escribe propiedad="nombreComercialTipoPoliza" nombre="seccionForm"/>
			</td>
		</tr>	
	</table>
	<table id="desplegar">
		<midas:oculto propiedadFormulario="idToTipoPoliza"/>
		<midas:oculto propiedadFormulario="nombreComercialTipoPoliza"/>
		<midas:oculto propiedadFormulario="idToSeccion"/>
		<midas:oculto propiedadFormulario="version"/>
		<tr>
			<th><etiquetas:etiquetaError property="codigo" requerido="si"
					name="seccionForm" key="configuracion.seccion.codigo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="25%"><midas:texto propiedadFormulario="codigo" soloLectura="true"
					onkeypress="return soloAlfanumericos(this, event, false)" id="codigo" caracteres="8"/>
			</td>
			<th><etiquetas:etiquetaError property="claveEstatus" requerido="si"
					name="seccionForm" key="configuracion.producto.claveEstatus" normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /></th>
			<td width="25%"> 
				<midas:comboValorFijo grupoValores="33" propiedad="claveEstatus" nombre="seccionForm" styleClass="cajaTexto"/>  
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="nombreComercial" requerido="si"
					name="seccionForm" key="configuracion.seccion.nombrecomercial"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text property="nombreComercial" maxlength="100" readonly="true"
						styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
			<th><etiquetas:etiquetaError property="descripcion" requerido="si"
					name="seccionForm" key="configuracion.seccion.descripcion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /></th>
			<td>
				<html:text property="descripcion" maxlength="200" readonly="true"
						styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
		</tr>
		
		<tr>
			<th><etiquetas:etiquetaError property="claveImpresionEncabezado" requerido="no"
					name="seccionForm" key="configuracion.seccion.claveimpresionencabezado"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:check propiedadFormulario="claveImpresionEncabezado" id="claveImpresionEncabezado" deshabilitado="true"/>
				<midas:oculto propiedadFormulario="claveImpresionEncabezado"/>
			</td>
			<th><etiquetas:etiquetaError property="claveBienSeccion" requerido="si"
								name="seccionForm" key="configuracion.seccion.clavebienseccion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="26" propiedad="claveBienSeccion" nombre="seccionForm" 
				styleClass="cajaTexto"  readonly="true" />
				<midas:oculto propiedadFormulario="claveBienSeccion"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveImpresionSumaAsegurada" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveimpresionsumaasegurada"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:check propiedadFormulario="claveImpresionSumaAsegurada" id="claveImpresionSumaAsegurada" deshabilitado="true"/>
				<midas:oculto propiedadFormulario="claveImpresionSumaAsegurada"/>
			</td>
			<th><etiquetas:etiquetaError property="claveImpresionPrimaNeta" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveimpresionprimaneta"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:check propiedadFormulario="claveImpresionPrimaNeta" id="claveImpresionPrimaNeta" deshabilitado="true"/>
				<midas:oculto propiedadFormulario="claveImpresionPrimaNeta"/>
			</td>
		</tr> 	
		<tr>
			<th><etiquetas:etiquetaError property="numeroSecuencia" requerido="si"
					name="seccionForm" key="configuracion.seccion.numerosecuencia"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroSecuencia"  soloLectura="true"
					onkeypress="return soloNumeros(this, event, false)" id="numeroSecuencia" caracteres="8"/>
			</td>
			<th><etiquetas:etiquetaError property="claveObligatoriedad" requerido="si"
					name="seccionForm" key="configuracion.seccion.claveobligatoriedad"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="27" propiedad="claveObligatoriedad" nombre="seccionForm" styleClass="cajaTexto" readonly="true"/>
				<midas:oculto propiedadFormulario="claveObligatoriedad"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveLuc" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveluc"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:check propiedadFormulario="claveLuc" id="claveLuc" deshabilitado="true"/>
				<midas:oculto propiedadFormulario="claveLuc"/>
			</td>
			<th><etiquetas:etiquetaError property="clavePrimerRiesgo" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveprimerriesgo"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:check propiedadFormulario="clavePrimerRiesgo" id="clavePrimerRiesgo" deshabilitado="true"/>
				<midas:oculto propiedadFormulario="clavePrimerRiesgo"/>
			</td>
		</tr>		
		<tr>
			<th><etiquetas:etiquetaError property="claveDependenciaOtrasPr" requerido="no"
								name="seccionForm" key="configuracion.seccion.clavedependenciaotraspr"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:check propiedadFormulario="claveDependenciaOtrasPr" id="claveDependenciaOtrasPr" deshabilitado="true"/>
				<midas:oculto propiedadFormulario="claveDependenciaOtrasPr"/>
			</td>
			<th><etiquetas:etiquetaError property="claveEdificioContenido" requerido="si"
								name="seccionForm" key="configuracion.seccion.claveEdificioContenido"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="5" propiedad="claveEdificioContenido" nombre="seccionForm" styleClass="cajaTexto" readonly="true"/>
				<midas:oculto propiedadFormulario="claveEdificioContenido"/>
			</td>
		</tr>	
		
		
		<tr>
			<th><etiquetas:etiquetaError property="diasGracia" requerido="si"
								name="seccionForm" key="configuracion.producto.diasgracia"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasGracia" soloLectura="true"
								onkeypress="return soloNumeros(this, event, false)" id="diasGracia" caracteres="4"/>
			</td>
			
			<th><etiquetas:etiquetaError property="diasGraciaSubsecuentes" requerido="si"
								name="seccionForm" key="configuracion.producto.diasgracia.recibossubsecuentes"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasGraciaSubsecuentes" soloLectura="true"
								onkeypress="return soloNumeros(this, event, false)" id="diasGraciaSubsecuentes" caracteres="4"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="diasRetroactividad" requerido="si"
								name="seccionForm" key="configuracion.producto.diasretroactividad"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasRetroactividad" soloLectura="true"
								onkeypress="return soloNumeros(this, event, false)" id="diasRetroactividad" caracteres="4"/>	
			</td>
			<th><etiquetas:etiquetaError property="diasDiferimiento" requerido="si"
								name="seccionForm" key="configuracion.producto.diasdiferimiento"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasDiferimiento" soloLectura="true"
								onkeypress="return soloNumeros(this, event, false)" id="diasDiferimiento" caracteres="4"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="porcentajeMaximoBonificacion" requerido="si"
								name="seccionForm" key="configuracion.seccion.porcentajemaximo.bonificacion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoBonificacion" soloLectura="true"
								onkeypress="return soloNumeros(this, event, true)" id="porcentajeMaximoBonificacion" caracteres="4"/>	
			</td>
			<th><etiquetas:etiquetaError property="porcentajeMaximoComision" requerido="si"
								name="seccionForm" key="configuracion.seccion.porcentajemaximo.comision"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoComision" soloLectura="true"
								onkeypress="return soloNumeros(this, event, true)" id="porcentajeMaximoComision" caracteres="4"/>
			</td>
		</tr>
		
		<tr>
			<th><etiquetas:etiquetaError property="porcentajeMaximoSobrecomision" requerido="si"
								name="seccionForm" key="configuracion.seccion.porcentajemaximo.sobrecomision"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoSobrecomision" soloLectura="true"
								onkeypress="return soloNumeros(this, event, true)" id="porcentajeMaximoSobrecomision" caracteres="4"/>	
			</td>
			
			<th>
				<div id="etiquetaFecha"><etiquetas:etiquetaError normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" name="seccionForm"
					property="fechaInicioVigencia" key="configuracion.producto.fechainiciovigencia" requerido="no"/>
					
					<logic:equal value="0" property="claveEstatus" name="seccionForm" >	
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
							<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
					</logic:equal>	
					 
				</div>
		   	</th>
		   	
		   	<logic:equal value="1" property="claveEstatus" name="seccionForm" >
		   		<th>
			   				<html:text property="fechaInicioVigencia"  maxlength="10" size="15"
			   				 styleId="fecha" name="seccionForm"  styleClass="cajaTexto"
							 onkeypress="return soloFecha(this, event, false);" readonly="true"
							 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							 onblur="esFechaValida(this);" />
				</th>
		   	</logic:equal>
		   	<logic:notEqual value="1" property="claveEstatus" name="seccionForm" >
		   	
		   	    <th>
			   				<html:text property="fechaInicioVigencia"  maxlength="10" size="15"
			   				 styleId="fecha"  name="seccionForm"  styleClass="cajaTexto"
							 onkeypress="return soloFecha(this, event, false);" readonly="true"
							 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							 onblur="esFechaValida(this);" />
				</th>

		   	</logic:notEqual>
			
		</tr>
				<tr>
			<th></th>
			<td></td>
			<th>
				<etiquetas:etiquetaError normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" name="seccionForm"
					property="fechaRegistro" key="catalogos.configuracion.fecharegistro" requerido="si"/>
			</th>
			<td>
				<html:text property="fechaRegistro"  maxlength="10" size="15"
			   				 styleId="fecha"  name="seccionForm"  styleClass="cajaTexto"
							 onkeypress="return soloFecha(this, event, false);"
							 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"
							 onblur="esFechaValida(this);" />
			</td>
		</tr>
		<tr>
			<th></th>
			<td></td>
			<th><etiquetas:etiquetaError property="numeroRegistro" requerido="si"
								name="seccionForm" key="catalogos.configuracion.numeroregistro"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroRegistro" id="numeroRegistro" caracteres="32"/>
			</td>
		</tr>
		<tr>
			<th></th>
			<td></td>
			<th><etiquetas:etiquetaError property="tipoValidacionNumSerie" requerido="si"
					name="seccionForm" key="configuracion.seccion.tipoValidacionNumSerie"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="504" propiedad="tipoValidacionNumSerie" nombre="seccionForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr><th colspan="4"><br>Informaci&oacute;n de Autos<br></th></tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveTipoBien" name="seccionForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td colspan="2">
						<midas:comboCatalogo propiedad="id_claveTipoBien" styleId="id_claveTipoBien" size="1" styleClass="cajaTexto"
						 nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" descripcionCatalogo="descripcionTipoBien" 
						 onchange="getTipoVehiculos(this,'tipoVehiculoForm.idTcTipoVehiculo')"
						 readonly="true" />
						 <midas:oculto propiedadFormulario="id_claveTipoBien"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="tipoVehiculoForm.id" name="seccionForm"
					key="configuracion.seccion.autos.tipovehiculo.default" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td colspan="2">
					<midas:tipoVehiculo styleId="tipoVehiculoForm.idTcTipoVehiculo" size="1" propiedad="tipoVehiculoForm.idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto" readonly="true" />
					<midas:oculto propiedadFormulario="tipoVehiculoForm.idTcTipoVehiculo"/>
			</td>
			
		</tr>
				
		
				
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"  style="font-size: 7pt" 
							onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id=<midas:escribe  nombre="seccionForm" propiedad="idToTipoPoliza"/>', 'configuracion_detalle','dhx_init_tabbars();cargaDataGridtipopolizaHijos(\'<midas:escribe  nombre="seccionForm" propiedad="idToTipoPoliza"/>\',null);');">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.seccionForm,'/MidasWeb/configuracion/seccion/modificar_prod.do', 'configuracion_detalle','existenErrores(\'refreshTree()\')');">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
		</tr>					
	</table>
	<div id="errores" style="display: none;"><html:errors/></div>	
</midas:formulario>