<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<script type="text/javascript">
	var mensaje = '<s:property value="mensaje" default="-1"/>';
	var tipoMensaje = '<s:property value="tipoMensaje" default="-1"/>';
	function guardarDocReferencia(node){
		mostrarMensajeInformativo(node.firstChild.data,node.getAttribute("tipo")); 
	}
</script>
<div id="documentosReferencias">
	<center>
		<midas:formulario accion="/configuracion/seccion/listarDocumentosReferencias">
			<midas:oculto propiedadFormulario="idToSeccion" nombreFormulario="seccionForm"/>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="2">
						<midas:mensaje clave="configuracion.anexos.seccion.titulo"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="documentosAnexosGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="btn_back w170">
							<a href="javascript: void(0);" class="icon_adjuntarPDF"
								onclick="mostrarAnexarDocumentosReferencias();">
								<midas:mensaje clave="midas.sistema.boton.agregar.documento"/>
							</a>
						</div>
					</td>				
					<td>
						<div class="btn_back w170">
							<a href="javascript: void(0);" class="icon_eliminar"
								onclick="javascript: eliminarDocumentoAnexoReferencias()">
								<midas:mensaje clave="midas.sistema.boton.eliminar.documentoseleccionado"/>
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<div class="alinearBotonALaDerecha">
						<div id="b_guardar" >
							<a href="javascript: void(0);" onclick="javascript: documentoAnexoTipoReferenciaProcessor.sendData()">
							<midas:mensaje clave="midas.accion.guardar"/>
							</a>
						</div>
					</div>	
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>