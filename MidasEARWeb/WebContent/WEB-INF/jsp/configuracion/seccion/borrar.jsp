<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/configuracion/seccion/borrar">
	<table width="100%" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.borrar" />
				Secci&oacute;n:&nbsp; 
				<midas:escribe propiedad="nombreComercial" nombre="seccionForm"/>
				&nbsp;a Tipo de P&oacute;liza: <midas:escribe propiedad="nombreComercialTipoPoliza" nombre="seccionForm"/>
			</td>
		</tr>	
	</table>
	<table id="desplegar">
		<midas:oculto propiedadFormulario="idToTipoPoliza"/>
		<midas:oculto propiedadFormulario="nombreComercialTipoPoliza"/>
		<midas:oculto propiedadFormulario="idToSeccion"/>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.codigo" />:</th>
			<td><midas:escribe propiedad="codigo" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.seccion.version" />:</th>
			<td><midas:escribe propiedad="version" nombre="seccionForm"/></td>
		</tr> 	
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.nombrecomercial" />:</th>
			<td><midas:escribe propiedad="nombreComercial" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.seccion.descripcion" />:</th>
			<td colspan="2"><midas:escribe propiedad="descripcion" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.claveimpresionencabezado" />:</th>
			<td><midas:check propiedadFormulario="claveImpresionEncabezado" id="claveImpresionEncabezado" deshabilitado="true"/></td>
			<th><midas:mensaje clave="configuracion.seccion.clavebienseccion" />:</th>
			<td><midas:escribe propiedad="descripcionBienSeccion" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.claveimpresionsumaasegurada" />:</th>
			<td><midas:check propiedadFormulario="claveImpresionSumaAsegurada" id="claveImpresionSumaAsegurada" deshabilitado="true"/></td>
			<th><midas:mensaje clave="configuracion.seccion.claveimpresionprimaneta" />:</th>
			<td><midas:check propiedadFormulario="claveImpresionPrimaNeta" id="claveImpresionPrimaNeta" deshabilitado="true"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.claveobligatoriedad" />:</th>
			<td><midas:escribe propiedad="descripcionObligatoriedad" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.seccion.numerosecuencia" />:</th>
			<td><midas:escribe propiedad="numeroSecuencia" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.claveluc" />:</th>
			<td><midas:check propiedadFormulario="claveLuc" id="claveLuc" deshabilitado="true"/></td>
			<th><midas:mensaje clave="configuracion.seccion.claveprimerriesgo" />:</th>
			<td><midas:check propiedadFormulario="clavePrimerRiesgo" id="clavePrimerRiesgo" deshabilitado="true"/></td>
		</tr>		
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.clavedependenciaotraspr" />:</th>
			<td><midas:check propiedadFormulario="claveDependenciaOtrasPr" id="claveDependenciaOtrasPr" deshabilitado="true"/></td>
			<th><midas:mensaje clave="configuracion.seccion.claveEdificioContenido" />:</th>
			<td><midas:escribe propiedad="descripcionEdificioContenido" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.diasgracia" />:</th>
			<td><midas:escribe propiedad="diasGracia" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.diasgracia.recibossubsecuentes" />:</th>
			<td><midas:escribe propiedad="diasGraciaSubsecuentes" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.producto.diasretroactividad" />:</th>
			<td><midas:escribe propiedad="diasRetroactividad" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.diasdiferimiento" />:</th>
			<td><midas:escribe propiedad="diasDiferimiento" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.porcentajemaximo.bonificacion" />:</th>
			<td><midas:escribe propiedad="porcentajeMaximoBonificacion" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.seccion.porcentajemaximo.comision" />:</th>
			<td><midas:escribe propiedad="porcentajeMaximoComision" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.porcentajemaximo.sobrecomision" />:</th>
			<td><midas:escribe propiedad="porcentajeMaximoSobrecomision" nombre="seccionForm"/></td>
			<th><midas:mensaje clave="configuracion.producto.fechainiciovigencia" />:</th>
			<td><midas:escribe propiedad="fechaInicioVigencia" nombre="seccionForm"/></td>
		</tr>
		<tr><th colspan="4"><br>Informaci&oacute;n de Autos<br></th></tr>
		<tr>
			<th><midas:mensaje clave="catalogos.estilovehiculo.id_claveTipoBien" />:</th>
			<td><midas:escribe propiedad="descripcionTipoBien" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="configuracion.seccion.autos.tipovehiculo.default" />:</th>
			<td><midas:escribe propiedad="descripcionTipoVehiculo" nombre="seccionForm"/></td>
		</tr>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">							
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id=<midas:escribe  nombre="seccionForm" propiedad="idToTipoPoliza"/>', 'configuracion_detalle','dhx_init_tabbars();cargaDataGridtipopolizaHijos(\'<midas:escribe  nombre="seccionForm" propiedad="idToTipoPoliza"/>\',null);');">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<c:choose>
						<c:when test="${seccionForm.claveEstatus==1}">
									<div id="b_borrar">
										<a href="javascript: void(0);"
								    		onclick="javascript: alert('El Bien/Secci&oacute;n se encuentra Activo en Producci&oacute;n');">
											<midas:mensaje clave="midas.accion.borrar"/>
										</a>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${seccionForm.claveEstatus==2}">
											<div id="b_borrar">
												<a href="javascript: void(0);"
								    				onclick="javascript: alert('El Bien/Secci&oacute;n se encuentra Inactivo');">
													<midas:mensaje clave="midas.accion.borrar"/>
												</a>
											</div>
										</c:when>
										<c:otherwise>
					<c:choose>
						<c:when test="${not empty seccionForm.coberturaSeccionAsociadas}">
							<div id="b_borrar">
								<a href="javascript: void(0);"
								onclick="javascript: Confirma('El Bien/Secci&oacute;n tiene coberturas asociadas. �Desea continuar el Borrado?', document.seccionForm,'/MidasWeb/configuracion/seccion/borrar.do', 'configuracion_detalle','refreshTree()');">
								<midas:mensaje clave="midas.accion.borrar"/>
								</a>
							</div>
						</c:when>
						<c:otherwise>
							<div id="b_borrar">
								<a href="javascript: void(0);"
								onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?', document.seccionForm,'/MidasWeb/configuracion/seccion/borrar.do', 'configuracion_detalle','refreshTree()');">
								<midas:mensaje clave="midas.accion.borrar"/>
								</a>
							</div>
						</c:otherwise>
					</c:choose>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>
			</td>      		
		</tr>				
	</table>
	
</midas:formulario>