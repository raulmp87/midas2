<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<div style= "height: 450px; overflow:auto" hrefmode="ajax-html"  id="configuracionSeccionTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div id="detalle" name="Detalle">
		<center>
			<midas:formulario accion="/configuracion/seccion/mostrarDetalle">
				<table width="100%" border="0">
					<tr>
						<td class="titulo" colspan="4">
							<midas:escribe propiedad="nombreComercial" nombre="seccionForm"/>
						</td>
					<midas:oculto propiedadFormulario="idToTipoPoliza"/>
					<midas:oculto propiedadFormulario="nombreComercialTipoPoliza"/>
					<midas:oculto propiedadFormulario="idToSeccion"/>					
					</tr>
				</table>			
				<table id="desplegar" border="0">
					<tr>
						<th><midas:mensaje clave="configuracion.seccion.codigo" />:</th>
						<td class="txt_v"><midas:escribe propiedad="codigo" nombre="seccionForm"/></td>
						<th><midas:mensaje clave="configuracion.seccion.version" />:</th>
						<td class="txt_v"><midas:escribe propiedad="version" nombre="seccionForm"/></td>
					</tr> 	
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.seccion.nombrecomercial" />:</th>
						<td class="txt_v"><midas:escribe propiedad="nombreComercial" nombre="seccionForm"/></td>
						<th><midas:mensaje clave="configuracion.seccion.descripcion" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcion" nombre="seccionForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.seccion.claveobligatoriedad" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionObligatoriedad" nombre="seccionForm"/></td>
						<th><midas:mensaje clave="configuracion.seccion.numerosecuencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="numeroSecuencia" nombre="seccionForm"/></td>
					</tr>
					<tr class="bg_t2">
						<th><midas:mensaje clave="configuracion.seccion.clavebienseccion" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionBienSeccion" nombre="seccionForm"/></td>
						<th><midas:mensaje clave="configuracion.seccion.claveimpresionencabezado" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveImpresionEncabezado" id="claveImpresionEncabezado" deshabilitado="true"/></td>					
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.seccion.claveimpresionsumaasegurada" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveImpresionSumaAsegurada" id="claveImpresionSumaAsegurada" deshabilitado="true"/></td>
						<th><midas:mensaje clave="configuracion.seccion.claveimpresionprimaneta" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveImpresionPrimaNeta" id="claveImpresionPrimaNeta" deshabilitado="true"/></td>
					</tr> 
					<tr  class="bg_t2">
						<th><midas:mensaje clave="configuracion.seccion.claveluc" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveLuc" id="claveLuc" deshabilitado="true"/></td>
						<th><midas:mensaje clave="configuracion.seccion.claveprimerriesgo" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="clavePrimerRiesgo" id="clavePrimerRiesgo" deshabilitado="true"/></td>
					</tr>		
					<tr>
						<th><midas:mensaje clave="configuracion.seccion.clavedependenciaotraspr" />:</th>
						<td class="txt_v"><midas:check propiedadFormulario="claveDependenciaOtrasPr" id="claveDependenciaOtrasPr" deshabilitado="true"/></td>						
						<th><midas:mensaje clave="configuracion.seccion.claveEdificioContenido" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionEdificioContenido" nombre="seccionForm"/></td>
					</tr>	
					
					
					<tr  class="bg_t2">
						<th><midas:mensaje clave="configuracion.producto.diasgracia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasGracia" nombre="seccionForm"/></td>						
						<th><midas:mensaje clave="configuracion.producto.diasgracia.recibossubsecuentes" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasGraciaSubsecuentes" nombre="seccionForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.producto.diasretroactividad" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasRetroactividad" nombre="seccionForm"/></td>						
						<th><midas:mensaje clave="configuracion.producto.diasdiferimiento" />:</th>
						<td class="txt_v"><midas:escribe propiedad="diasDiferimiento" nombre="seccionForm"/></td>
					</tr>
					<tr  class="bg_t2">
						<th><midas:mensaje clave="configuracion.seccion.porcentajemaximo.bonificacion" />:</th>
						<td class="txt_v"><midas:escribe propiedad="porcentajeMaximoBonificacion" nombre="seccionForm"/></td>						
						<th><midas:mensaje clave="configuracion.seccion.porcentajemaximo.comision" />:</th>
						<td class="txt_v"><midas:escribe propiedad="porcentajeMaximoComision" nombre="seccionForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="configuracion.seccion.porcentajemaximo.sobrecomision" />:</th>
						<td class="txt_v"><midas:escribe propiedad="porcentajeMaximoSobrecomision" nombre="seccionForm"/></td>						
						<th><midas:mensaje clave="configuracion.producto.claveEstatus" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionEstatus" nombre="seccionForm"/></td>
					</tr>
					<tr  class="bg_t2">
						<th><midas:mensaje clave="configuracion.producto.fechainiciovigencia" />:</th>
						<td class="txt_v"><midas:escribe propiedad="fechaInicioVigencia" nombre="seccionForm"/></td>						
						<td class="txt_v">&nbsp;</td>
						<td class="txt_v">&nbsp;</td>
					</tr>
					<tr>
						<th><midas:mensaje clave="catalogos.estilovehiculo.id_claveTipoBien" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionTipoBien" nombre="seccionForm"/></td>						
						<th><midas:mensaje clave="configuracion.seccion.autos.tipovehiculo.default" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionTipoVehiculo" nombre="seccionForm"/></td>
					</tr>
					<tr  class="bg_t2">
						<th><midas:mensaje clave="catalogos.configuracion.numeroregistro" />:</th>
						<td class="txt_v"><midas:escribe propiedad="numeroRegistro" nombre="seccionForm"/></td>
						<th><midas:mensaje clave="catalogos.configuracion.fecharegistro" />:</th>
						<td class="txt_v"><midas:escribe propiedad="fechaRegistro" nombre="seccionForm"/></td>
					</tr>
					<tr  class="bg_t2">
						<th></th>
						<td class="txt_v"></td>
						<th><midas:mensaje clave="configuracion.seccion.tipoValidacionNumSerie" />:</th>
						<td class="txt_v"><midas:escribe propiedad="descripcionTipoValidacionNumSerie" nombre="seccionForm"/></td>
					</tr>
					<tr class="">
					<td  colspan=8>
						<div   style = "float: right; width: 150px"  align="right" id="b_nuevaVersion">
							<a href="javascript: void(0);"
									    onclick="javascript: generarNuevaVersionSeccion(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)">
										Generar Versi&oacute;n
							</a>
					     </div>					
						</td>
					</tr>
				</table>
				<div class="subtitulo" style="width: 98%;">Coberturas Asociadas</div>
				<div id="coberturaGrid"  width="97%" height="125"></div>
			<div id="pagingArea"></div><div id="infoArea"></div>
			</midas:formulario>
		</center>		
	</div>
		<div width="100px" id="documentosReferencias" name="Documentos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/listarDocumentosReferencias.do?id=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_documentosReferencias', 'traerListaDocumentosReferencias(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)')"></div>
		<div width="100%" id="coberturas" name="Coberturas" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarAsociarCobertura.do', 'contenido_coberturas', 'mostrarCoberturasSeccion(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)')"></div>
		<div width="100%" id="ramos" name="Ramos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarAsociarRamo.do?id=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_ramos', 'mostrarRamosSeccionGrid(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)')"></div>
		<div width="150" id="seccionesRequeridas" name="Secciones Requeridas" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarAsociarSeccion.do?id=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_seccionesRequeridas', 'mostrarSeccionRequerida(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)')"></div>
		<div width="100%" id="paquetes" name="Paquetes" href="http://void" extraAction="sendRequestJQ(null,'/MidasWeb/relaciones/seccion/mostrarRelacionPaquetes.action?idSeccion=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_paquetes' , 'iniciaGridsPaquetes()')"></div>
		<div width="100%" id="derechosEndoso" name="Derechos Endoso" href="http://void" extraAction="sendRequestJQ(null,'/MidasWeb/subordinados/seccion/mostrarDerechosEndoso.action?idSeccion=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_derechosEndoso' , 'iniciaGridDerechosEndoso()')"></div>
		<div width="150px" id="derechosPoliza" name="Derechos Poliza" href="http://void" extraAction="sendRequestJQ(null,'/MidasWeb/subordinados/seccion/mostrarDerechosPoliza.action?idSeccion=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_derechosPoliza' , 'iniciaGridDerechosPoliza()')"></div>
		<div width="200px" id="condiciones" name="Condiciones Generales" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/listarDocumentosCondiciones.do?id=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_condiciones', 'mostrarDocumentosCgGrid(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)')"></div>
		<div width="150px" id="tipoDeCedula" name="Tipo de C&eacute;dula" href="http://void" extraAction="sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarTipoDeCedula.do?id=<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>', 'contenido_tipoDeCedula', 'mostrarTipoDeCedulaGrids(<midas:escribe propiedad="idToSeccion" nombre="seccionForm"/>)')"></div>				
</div>
