<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/seccion/mostrarAsociarRamo">
			<midas:oculto propiedadFormulario="idToSeccion" nombreFormulario="seccionForm"/>
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.ramo" /> Bien/Secci&oacute;n</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.ramo.asociado" /> la Bien/Secci&oacute;n</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="ramosAsociadosGrid"  class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.ramo.disponible" /> el Bien/Secci&oacute;n</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="ramosPorAsociarGrid"  class="dataGridConfigurationClass"></div>
					</td>		
				</tr>	
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>	
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: ramosSeccionProcessor.sendData(); mostrarMensajeExitoYCambiarTab(configuracionSeccionTabBar,'detalle');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>
		</midas:formulario>
	</center>
</div>	