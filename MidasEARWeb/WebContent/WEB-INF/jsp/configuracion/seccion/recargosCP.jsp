<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/seccion/mostrarRecargoCP">
				<input id=idToSeccion type="hidden">
				<input id=idToCobertura type="hidden">
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4">RECARGOS Y DESCUENTOS POR C&OacuteDIGO POSTAL</td>
				</tr>
				<tr>
				
					<td colspan="4">
					
						<div style="float:left; width: 450px;" id="recargosCpGrid" class="recargoCPDataGridConfigurationClass"></div>
					
					</td>
				

				</tr>
			</table>
			
			
				<div style="float: right; width: 28%; border: 0px solid black">
			<div class="alinearBotonALaDerecha_Recargos">
			     
                    <button type="button" style="width: 165px; font-weight:bold; font-size: 9px;"
                        onclick="javascript: downloadIt();">
                        Descargar Plantilla CP
                    </button> 
                 
			</div>
			
			
			<div class="alinearBotonALaDerecha_Recargos_c">
                    <button type="button" style="width: 165px; font-weight:bold; font-size: 9px;"
                        onclick="javascript: mostrarAdjuntarArchivoExcelSoporteWindow();">
                        Carga Valores por CP          

                    </button> 
			</div>
			</div>
			

		
		</midas:formulario>
	</center>
</div>