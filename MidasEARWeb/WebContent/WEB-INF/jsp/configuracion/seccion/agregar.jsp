<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/configuracion/seccion/agregar">
	<table width="100%" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.agregar" />
				Secci&oacute;n
			</td>
		</tr>	
	</table>
	<table id="agregar">
		<midas:oculto propiedadFormulario="idToTipoPoliza"/>
		<midas:oculto propiedadFormulario="nombreComercialTipoPoliza"/>
		<html:hidden property="claveEstatus" value="0"/>
		
		<tr>
			<th><etiquetas:etiquetaError property="codigo" requerido="si"
					name="seccionForm" key="configuracion.seccion.codigo"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="codigo" 
					onkeypress="return soloAlfanumericos(this, event, false)" id="codigo" caracteres="8"/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="nombreComercial" requerido="si"
					name="seccionForm" key="configuracion.seccion.nombrecomercial"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td width="28%">
				<html:text property="nombreComercial" maxlength="100"
						styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>
			<th><etiquetas:etiquetaError property="descripcion" requerido="si"
					name="seccionForm" key="configuracion.seccion.descripcion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" /></th>
			<td width="32%" colspan="2">
				<html:text property="descripcion" maxlength="200"
						styleClass="jQalphaextra jQrestrict cajaTexto" />
			</td>		
			
		</tr>				
		<tr>
			<th><etiquetas:etiquetaError property="claveImpresionEncabezado" requerido="no"
					name="seccionForm" key="configuracion.seccion.claveimpresionencabezado"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveImpresionEncabezado" id="claveImpresionEncabezado"/></td>	
			<th><etiquetas:etiquetaError property="claveBienSeccion" requerido="si"
								name="seccionForm" key="configuracion.seccion.clavebienseccion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="26" propiedad="claveBienSeccion" nombre="seccionForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveImpresionSumaAsegurada" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveimpresionsumaasegurada"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveImpresionSumaAsegurada" id="claveImpresionSumaAsegurada"/></td>
			<th><etiquetas:etiquetaError property="claveImpresionPrimaNeta" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveimpresionprimaneta"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveImpresionPrimaNeta" id="claveImpresionPrimaNeta"/></td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="numeroSecuencia" requerido="si"
					name="seccionForm" key="configuracion.seccion.numerosecuencia"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroSecuencia"  
					onkeypress="return soloNumerosM2(this, event, false)" id="numeroSecuencia" caracteres="8"/>
			</td>		
			<th><etiquetas:etiquetaError property="claveObligatoriedad" requerido="si"
					name="seccionForm" key="configuracion.seccion.claveobligatoriedad"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="27" propiedad="claveObligatoriedad" nombre="seccionForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="claveLuc" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveluc"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveLuc" id="claveLuc"/></td>
			<th><etiquetas:etiquetaError property="clavePrimerRiesgo" requerido="no"
								name="seccionForm" key="configuracion.seccion.claveprimerriesgo"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="clavePrimerRiesgo" id="clavePrimerRiesgo"/></td>
		</tr>		
		<tr>
			<th><etiquetas:etiquetaError property="claveDependenciaOtrasPr" requerido="no"
								name="seccionForm" key="configuracion.seccion.clavedependenciaotraspr"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:check propiedadFormulario="claveDependenciaOtrasPr" id="claveDependenciaOtrasPr"/></td>
			<th><etiquetas:etiquetaError property="claveEdificioContenido" requerido="si"
								name="seccionForm" key="configuracion.seccion.claveEdificioContenido"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:comboValorFijo grupoValores="5" propiedad="claveEdificioContenido" nombre="seccionForm" styleClass="cajaTexto"/></td>
		</tr>	
		
		<tr>
			<th><etiquetas:etiquetaError property="diasGracia" requerido="si"
								name="seccionForm" key="configuracion.producto.diasgracia"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasGracia" 
								onkeypress="return soloNumerosM2(this, event, false)" id="diasGracia" caracteres="4"/>
			</td>
			
			<th><etiquetas:etiquetaError property="diasGraciaSubsecuentes" requerido="si"
								name="seccionForm" key="configuracion.producto.diasgracia.recibossubsecuentes"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasGraciaSubsecuentes" 
								onkeypress="return soloNumerosM2(this, event, false)" id="diasGraciaSubsecuentes" caracteres="4"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="diasRetroactividad" requerido="si"
								name="seccionForm" key="configuracion.producto.diasretroactividad"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasRetroactividad" 
								onkeypress="return soloNumerosM2(this, event, false)" id="diasRetroactividad" caracteres="4"/>	
			</td>
			<th><etiquetas:etiquetaError property="diasDiferimiento" requerido="si"
								name="seccionForm" key="configuracion.producto.diasdiferimiento"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="diasDiferimiento"
								onkeypress="return soloNumerosM2(this, event, false)" id="diasDiferimiento" caracteres="4"/>
			</td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="porcentajeMaximoBonificacion" requerido="si"
								name="seccionForm" key="configuracion.seccion.porcentajemaximo.bonificacion"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoBonificacion" 
								onkeypress="return soloNumerosM2(this, event, true)" id="porcentajeMaximoBonificacion" caracteres="4"/>	
			</td>
			<th><etiquetas:etiquetaError property="porcentajeMaximoComision" requerido="si"
								name="seccionForm" key="configuracion.seccion.porcentajemaximo.comision"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoComision"
								onkeypress="return soloNumerosM2(this, event, true)" id="porcentajeMaximoComision" caracteres="4"/>
			</td>
		</tr>
		
		<tr>
			<th><etiquetas:etiquetaError property="porcentajeMaximoSobrecomision" requerido="si"
								name="seccionForm" key="configuracion.seccion.porcentajemaximo.sobrecomision"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoSobrecomision" 
								onkeypress="return soloNumerosM2(this, event, true)" id="porcentajeMaximoSobrecomision" caracteres="4"/>	
			</td>
			
			<th>
				<div id="etiquetaFecha"><etiquetas:etiquetaError normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" name="seccionForm"
					property="fechaInicioVigencia" key="configuracion.producto.fechainiciovigencia" requerido="no"/>
					
					
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
							<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
					
					 
				</div>
		   	</th>
		   	
		   		<th>
			   				<html:text property="fechaInicioVigencia"  maxlength="10" size="15"
			   				 styleId="fecha" name="seccionForm"  styleClass="cajaTexto"
			   				 readonly="true"
							 onkeypress="return soloFecha(this, event, false);"
							 onblur="esFechaValida(this);"
							 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
							 <!-- /> -->
							 
					
				</th>
		   
			
		</tr>
		<tr>
			<th></th>
			<td></td>
			<th><etiquetas:etiquetaError property="tipoValidacionNumSerie" requerido="si"
								name="seccionForm" key="configuracion.seccion.tipoValidacionNumSerie"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="504" propiedad="tipoValidacionNumSerie" nombre="seccionForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr><th colspan="4"><br>Informaci&oacute;n de Autos<br></th></tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveTipoBien" name="seccionForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td colspan="2">
						<midas:comboCatalogo propiedad="id_claveTipoBien" styleId="id_claveTipoBien" size="1" styleClass="cajaTexto"
						 nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" descripcionCatalogo="descripcionTipoBien" 
						 onchange="getTipoVehiculos(this,'tipoVehiculoForm.idTcTipoVehiculo')"/>
			</td>
		</tr>
		<tr>
		
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="tipoVehiculoForm.idTcTipoVehiculo" name="seccionForm"
					key="configuracion.seccion.autos.tipovehiculo.default" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td colspan="2">
					<midas:tipoVehiculo styleId="tipoVehiculoForm.idTcTipoVehiculo" size="1" propiedad="tipoVehiculoForm.idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto" />
			</td>
			
		</tr>
		
			
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"  style="font-size: 7pt" 
							onclick="javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id=<midas:escribe  nombre="seccionForm" propiedad="idToTipoPoliza"/>', 'configuracion_detalle','dhx_init_tabbars();cargaDataGridtipopolizaHijos(<midas:escribe  nombre="seccionForm" propiedad="idToTipoPoliza"/>,null,false);');">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>					
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.seccionForm,'/MidasWeb/configuracion/seccion/agregar.do', 'configuracion_detalle','validaGuardarSecciones()');">
						<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 			
		</tr>					
	</table>
	<div id="errores" style="display: none;"><html:errors/></div>
	<html:hidden property="mensaje" name="seccionForm" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" name="seccionForm" styleId="tipoMensaje"/>
</midas:formulario>

