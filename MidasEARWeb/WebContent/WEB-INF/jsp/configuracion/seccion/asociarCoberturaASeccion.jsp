<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/configuracion/seccion/mostrarAsociarCobertura">
			<midas:oculto propiedadFormulario="idToSeccion" nombreFormulario="seccionForm"/>
			<html:hidden property="mensaje" styleId="mensaje"/>
			<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
			<table  id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="4"><midas:mensaje clave="configuracion.asociar.cobertura" /> Bien/Secci&oacute;n</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.cobertura.asociado" /> Bien/Secci&oacute;n</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="coberturasAsociadasSeccionGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.lista.cobertura.disponible" /> el Bien/Secci&oacute;n</td>
				</tr>	
				<tr>
					<td colspan="4">
						<div id="coberturasPorAsociarSeccionGrid" class="dataGridConfigurationClass"></div>
					</td>		
				</tr>	
				<tr>
					<td colspan="4"><midas:mensaje clave="configuracion.asociar.arrastrar.mensaje"/></td>
				</tr>			
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="actualizarGridSeccion('coberturasSeccionPorcessor',null); refreshTree();"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
	</center>
</div>			