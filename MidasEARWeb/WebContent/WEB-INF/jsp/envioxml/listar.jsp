<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="estatusEnvio" type="img" width="*" sort="str" >Estatus Envio</column>
		<column id="fechaEnvio" type="ro" width="*" sort="date" >Fecha Envio</column>
		<column id="uuidCfdi" type="ro" width="*" sort="str" >UUID</column>
		<column id="folioCfdi" type="ro" width="*" sort="str" >Folio</column>
		<column id="rfc_emisorCfdi" type="ro" width="*" sort="str">RFC Emisor</column>
		<column id="rfc_receptorCfdi" type="ro" width="*" sort="str">RFC Receptor</column>
		<column id="monedaCfdi" type="ro" width="*" sort="str">Moneda</column>		
		<column id="tipoCambioCfdi" type="ro" width="*" sort="na">Tipo de Cambio</column>
		<column id="importeCfdi" type="ro" width="*" sort="int">Total</column>
		

</head>	
	
	<s:iterator value="xmls" status="stats">
		<row id="<s:property value="idEnvio"/>">
			<s:if test="estatusEnvio == 0">
				<cell>/MidasWeb/img/icons/ico_rechazar2.gif^Rechazado^javascript: parent.mostrarVentanaDetalleEnvio(<s:property value="idEnvio"/>)^_self</cell>
			</s:if>
			<s:elseif test="estatusEnvio == 1">
				<cell>/MidasWeb/img/icons/ico_aceptar.gif^Aceptado^javascript: parent.mostrarVentanaDetalleEnvio(<s:property value="idEnvio"/>)^_self</cell>
			</s:elseif>
			<cell><s:property value="fechaEnvio"/></cell>
			<cell><s:property value="uuidCfdi"/></cell>
			<cell><s:property value="serieCfdi"/><s:property value="folioCfdi"/></cell>
			<cell><s:property value="rfcEmisorCfdi"/></cell>
			<cell><s:property value="rfcReceptorCfdi"/></cell>
			<cell><s:property value="monedaCfdi"/></cell>
			<cell><s:property value="tipoCambioCfdi"/></cell>
			<cell><s:property value="importeCfdi"/></cell>
		</row>
	</s:iterator	
</rows>

