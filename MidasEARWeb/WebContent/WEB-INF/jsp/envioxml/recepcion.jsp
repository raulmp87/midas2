<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<script type="text/javascript">
	function mostrarCuentas() {
		var opcion = document.getElementById("tipo").selectedIndex;
		/* Se selecciona CHEQ */
		if(opcion == 0) {
		 	document.getElementById("cuentaAfectada").innerHTML="<option id='option1' value='101409848'>101409848 DLLS</option>" +
		 		"<option id='option2' value='101400255'>101400255 MN</option>";
			document.getElementById("cuentaAfectada").removeAttribute("disabled");
		}
		 /* Se selecciona TR */
		if(opcion == 1) {
			document.getElementById("cuentaAfectada").innerHTML="<option id='option1' value='159101347'>159101347 DLLS</option>" +
		 		"<option id='option2' value='159102564'>159102564 MN</option>";
			document.getElementById("cuentaAfectada").removeAttribute("disabled");
		}
	}
</script>
<center>
<div id="contenido_recepcion" style="width: 99%; position: relative;">
	<s:form id="envioFacturaForm" class="floatLeft">
		<s:hidden name="hideCargar" id="hideCargar"/>
	
			<div class="titulo" align="left">
					Recepci&oacute;n de Comprobantes
			</div>	
		

			<div id="contenedorFiltros" class="divContenedorO" style="width: 99%">
				<table id="agregar" border="0" class="fixTablaPoliza">
					<tr>						
						<td>
							<s:textfield id="folio" name="envioFactura.folioCfdi"
								cssClass="txtfield" label="Folio Factura" labelposition="top" cssStyle="width:33%;" ></s:textfield>							
						</td>
						<td>
							<s:textfield id="rfcEmisor" name="envioFactura.rfcEmisorCfdi"
								cssClass="txtfield" label="RFC" labelposition="top"	cssStyle="width:33%;" ></s:textfield>
						</td>
						<td> <!--  class="oculto"-->
							<s:if test="origenEnvio == 'FN_M2_SN_Siniestros_Vida'">
								<s:select id = "origenEnvio" label="Departamento" name="envioFactura.origenEnvio" 
									cssClass="txtfield" list="#{'LIQVID':'Liquidaci\u00F3n Siniestros Vida'}" cssStyle="width:45%;" labelposition="top"/>
							</s:if>
							<s:elseif test="origenEnvio == 'FN_M2_SN_Liquidacion_Autos'">
								<s:select id = "origenEnvio" label="Departamento" name="envioFactura.origenEnvio" 
									cssClass="txtfield" list="#{'LIQAUT':'Liquidaci\u00F3n Siniestros Auto'}" cssStyle="width:45%;" labelposition="top"/>
							</s:elseif>
							<s:elseif test="origenEnvio == 'FN_M2_SN_Liquidacion_Servicio_Publico'">
								<s:select id = "origenEnvio" label="Departamento" name="envioFactura.origenEnvio" 
									cssClass="txtfield" list="#{'LIQSP':'Liquidaci\u00F3n Siniestros Taxis/Servicio Publico'}" cssStyle="width:55%;" labelposition="top"/>
							</s:elseif>
							<s:elseif test="origenEnvio == 'FN_M2_SN_Rol_Op_Reaseguro_Aut'">
								<s:select id = "origenEnvio" label="Departamento" name="envioFactura.origenEnvio" 
									cssClass="txtfield" list="#{'REASEGU':'Reaseguro'}" cssStyle="width:33%;" labelposition="top"/>
								<script type="text/javascript">
									mostrarCuentas();
								</script>
							</s:elseif>
						</td>
					</tr>
					<tr>						
						<td>
							<sj:datepicker id="fechaDesde" name="fechaInicial" onkeypress="return soloFecha(this, event, false);" changeMonth="true" changeYear="true"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" onblur="esFechaValida(this);"
							buttonImage="../img/b_calendario.gif" cssClass="txtfield" label="Fecha de emision de" labelposition="top" cssStyle="width:33%;" ></sj:datepicker>
						</td>
						<td>
							<sj:datepicker id="fechaHasta" name="fechaFinal" onkeypress="return soloFecha(this, event, false);" changeMonth="true" changeYear="true"
							onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" onblur="esFechaValida(this);"
							buttonImage="../img/b_calendario.gif" cssClass="txtfield" label="Fecha de emision a" labelposition="top" cssStyle="width:33%;" ></sj:datepicker>
						</td>
						<td>
							<s:label id="labelTipo" value="Tipo:" />							
							<s:if test="origenEnvio == 'FN_M2_SN_Rol_Op_Reaseguro_Aut'">
								<s:select id="tipo" name="envioFactura.tipo" cssClass="txtfield" 
									list="#{'CHEQ':'No. Cheque', 'TR':'TR'}" cssStyle="width:33%;" onChange="mostrarCuentas()"/>
								<s:label id="labelCuenta" value="Cuenta Afectada:" />
								<s:select id="cuentaAfectada" name="envioFactura.cuentaAfectada" cssClass="txtfield"
									list="#{}" cssStyle="width:33%;" disabled="true"/>
							</s:if>
							<s:else>
								<s:select id="tipo" name="envioFactura.tipo" cssClass="txtfield" 
									list="#{'CHEQ':'No. Cheque', 'TR':'TR'}" cssStyle="width:33%;"/>
							</s:else>						
							<s:textfield id="idOrigenEnvio" name="envioFactura.idOrigenEnvio" cssClass="txtfield"
								cssStyle="width:33%;"></s:textfield>
						</td>
					</tr>
				</table>
			</div>
			<table>
				<tr>
					<td align="right">
						<div class="btn_back w100" style="display: inline; float: right;">
							<a href="javascript: void(0);" onclick="filtrarFacturas()">
								Buscar Factura
							</a>
						</div>
					</td>
					<td>
						<div class="btn_back w100" style="display: inline; float: right;">
							<s:if test="origenEnvio == 'FN_M2_SN_Rol_Op_Reaseguro_Aut'">
								<a href="javascript: void(0);" onclick="mostrarVentanaFacturaXML(jQuery('input[name=\'envioFactura.idOrigenEnvio\']',document.envioFacturaForm).val(), 
									jQuery('select[name=\'envioFactura.origenEnvio\']',document.envioFacturaForm).val(), jQuery('select[name=\'envioFactura.tipo\']',document.envioFacturaForm).val(), 
									jQuery('select[name=\'envioFactura.cuentaAfectada\']',document.envioFacturaForm).val())">							
									Cargar Facturas
								</a>
							</s:if>
							<s:else>
								<a href="javascript: void(0);" onclick="mostrarVentanaFacturaXML(jQuery('input[name=\'envioFactura.idOrigenEnvio\']',document.envioFacturaForm).val(), 
									jQuery('select[name=\'envioFactura.origenEnvio\']',document.envioFacturaForm).val(), jQuery('select[name=\'envioFactura.tipo\']',document.envioFacturaForm).val(), '')">
									Cargar Facturas
								</a>
							</s:else>
						</div>
						<div id="bottonToggleFilters" class="btn_back w100 oculto" >
							<a href="javascript: void(0);" onclick="toggleFilters()">
								Filtrar
							</a>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</s:form>

<div id="listaFacturasGrid" style="width:98%;height:300px"></div>
<div id="pagingArea" style="padding-top: 8px "></div>
			<div id="infoArea"></div>
</center>