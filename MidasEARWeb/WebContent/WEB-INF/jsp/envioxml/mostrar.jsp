<%@ page isELIgnored="false"%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxgrid.css'/>" rel="stylesheet" type="text/css">
<link href="<s:url value='/css/dhtmlxvault.css'/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_drag.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxvault.js'/>"></script>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>
<sj:head/>
<div id="contenedorFiltros" class="alinearBotonALaDerecha">
			<div id="indicador"></div>
			<div id="listaFacturasGrid" style="width:98%;height:300px"></div>
			<div id="pagingArea"><br/></div><div id="infoArea"><br/></div>
</div>

<s:form action="/envioxml/mostrar.action" id="envioFacturaForm">
	<s:hidden name="envioFactura.mensaje" id="mensaje"/>
	<s:hidden name="envioFactura.tipoMensaje" id="tipoMensaje"/>
	<s:hidden name="hideCargar" id="hideCargar"/>
	<s:hidden name="envioFactura.idOrigenEnvio" id="idOrigenEnvio" />
	<s:hidden name="envioFactura.origenEnvio" id="origenEnvio" />
	<s:hidden name="envioFactura.tipo" id="tipo" />
	<s:hidden name="envioFactura.cuentaAfectada" id="cuentaAfectada" />
	
	<div style="width:98%;">
		<div id="divSalirBtn" class="btn_back w100" style="display: inline, none; float: right;">
			<a href="javascript: void(0);" onclick="parent.mainDhxWindow.window($('#origenEnvio').val()).close();"><s:text name="midas.boton.salir" /> </a>
		</div>
		<s:if test="!hideCargar">
			<div class="btn_back w100" style="display: inline; float: right;">
				<a href="javascript: void(0);" onclick="parent.mostrarCargarFile(document.envioFacturaForm);" ><s:text name="midas.boton.cargar" /> </a>
			</div>
		</s:if>
	</div>
</s:form>
<script type="text/javascript">
parent.loadListFacturas(jQuery("#origenEnvio").val());	
</script>