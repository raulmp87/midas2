<?xml version="1.0" encoding="UTF-8"?>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@  page contentType="text/xml" %>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		<column id="idtipoRespuesta" type="ro" width="0" sort="na" hidden="true"></column>
		<column id="tipoRespuesta" type="ro" width="150" sort="str" ><![CDATA[Tipo de Validaci&oacute;n]]></column>
		<column id="descripcionRespuesta" type="ro" width="*" sort="str" ><![CDATA[Descripci&oacute;n]]></column>
	</head>	
	
	<s:iterator value="envioFactura.respuestas" status="stats">
		<row id="<s:property value='idTipoRespuesta'/>">
			<cell><s:property value="idTipoRespuesta"/></cell>
			<s:if test="tipoRespuesta == 2">
				<cell><![CDATA[Validaci&oacute;n Comercial]]></cell> 
			</s:if>
			<s:elseif test="tipoRespuesta == 1">
				<cell><![CDATA[Validaci&oacute;n Fiscal]]></cell>
			</s:elseif>				
			<cell><![CDATA[<s:property value="descripcionRespuesta"/>]]></cell>
		</row>
	</s:iterator>	
</rows>