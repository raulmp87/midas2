<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/siniestro/reportes/cargarReporteSiniestrosRRCSONORv7">	
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="siniestro.reportes.reporteSiniestrosRRCSONORv7"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="siniestro.reportes.reporteSiniestrosRRCSONORv7.fechaInicial"/>:</th>
			<td>
				<midas:texto id="fechaInicial" propiedadFormulario="fechaInicial" deshabilitado="false" caracteres="10" 
					onblur="esFechaValida(this);"/>
			</td>
			<td>
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendario()"></a>
				</div>	
			</td>
			<th><midas:mensaje clave="siniestro.reportes.reporteSiniestrosRRCSONORv7.fechaFinal"/>:</th>
			<td>
				<midas:texto id="fechaFinal" propiedadFormulario="fechaFinal" deshabilitado="false" caracteres="10" 
					onblur="esFechaValida(this);" />
			</td> 
		</tr>
		<tr>
		<td>
			<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
				<div id="calendarioIzq"></div>
				<div id="calendarioDer"></div>
			</div>
		</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>	
	<br/>
	<div class="alinearBotonALaDerecha">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: mostrarReporteSiniestrosRRCSONORv7('ReporteSiniestrosCalculoReservaRiesgos', document.getElementById('fechaInicial').value,document.getElementById('fechaFinal').value);">
				<midas:mensaje clave="midas.accion.reporteExcel"/>
			</a>
		</div>
	</div>
</midas:formulario>
