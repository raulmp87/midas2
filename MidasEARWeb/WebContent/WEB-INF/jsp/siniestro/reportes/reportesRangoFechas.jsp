<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<midas:formulario  accion="/siniestro/reportes/cargarReporteSPFechas">	
	<html:hidden property="nombrePlantilla" styleId="nombrePlantilla"/>
	<table width="98%" id="filtros" >
		<tr>
			<td class="titulo" colspan="7">
				Fechas
			</td>
		</tr>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: right" >
				<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.fechaInicial"/>:
			</td>
			<td>
				<midas:texto id="fechaInicial" propiedadFormulario="fechaInicial" soloLectura="true" caracteres="10" onblur="esFechaValida(this);"/>
			</td>
			<td style="text-align: center"  >
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendario()"></a>
				</div>	
			</td>
			<td style="text-align: right">
				<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.fechaFinal"/>:
			</td>
			<td>
				<midas:texto id="fechaFinal" propiedadFormulario="fechaFinal" soloLectura="true" caracteres="10" onblur="esFechaValida(this);" />
			</td> 
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td colspan="6">&nbsp;</td>
			<td>
			<midas:boton onclick="mostrarReporteSPFechas()" tipo="buscar"/>
			</td>
		</tr>
	</table>
</midas:formulario>
