<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/reportes/mostrarCaratulaSiniestro">
<div style="height: 450px; overflow: auto"> 
 	<table width="100%" border="0" class="agregar">
 		<tr>
			<td width="2%">&nbsp;</td>
			<td width="18%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="8%">&nbsp;</td>
			<td width="14%">&nbsp;</td>
			<td width="2%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.caratula.titulo" />			
			</td>
		</tr>	
		<tr>
			<td class="datoTabla" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.caratula.datosReporte" />
				<hr>		
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.numeroReporte" /> :</td>
			<td class="dato"><midas:escribe propiedad="numeroReporte" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.idAjustador" /> :</td>
			<td class="dato"><midas:escribe propiedad="idAjustadorReporte" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.ajustador" /> :</td>
			<td class="dato"><midas:escribe propiedad="nombreAjustadorReporte" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.fechaHoraReporte" /> :</td>
			<td class="dato"><midas:escribe propiedad="fechaHoraReporte" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.fechaOcurrencia" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="fechaOcurrioReporte" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.descripcionSiniestro" /> :</td>
			<td colspan="5" class="dato"><midas:escribe propiedad="descripcionSiniestro" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.calle" /> :</td>
			<td class="dato"><midas:escribe propiedad="calleSiniestro" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.colonia" /> :</td>
			<td class="dato"><midas:escribe propiedad="coloniaSiniestro" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.codigoPostal" /> :</td>
			<td class="dato"><midas:escribe propiedad="codigoPostalSiniestro" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.municipio" /> :</td>
			<td class="dato"><midas:escribe propiedad="municipioSiniestro" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.estado" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="estadoSiniestro" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		<tr>
			<td class="datoTabla" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.caratula.datosPoliza" />
				<hr>		
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.tipoMoneda" /> :</td>
			<td class="dato"><midas:escribe propiedad="tipoMonedaPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.numeroPoliza" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="numeroPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.claveOficina" /> :</td>
			<td class="dato"><midas:escribe propiedad="claveOficinaPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.endoso" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="endosoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.claveProducto" /> :</td>
			<td class="dato"><midas:escribe propiedad="claveProductoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.inciso" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="incisoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.claveSubRamo" /> :</td>
			<td class="dato"><midas:escribe propiedad="claveSubRamoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.descripcionSubRamo" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="descripcionSubRamoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.fechaEmision" /> :</td>
			<td class="dato"><midas:escribe propiedad="fechaEmisionPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.inicioVigencia" /> :</td>
			<td class="dato"><midas:escribe propiedad="fechaInicioVigenciaPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.finVigencia" /> :</td>
			<td class="dato"><midas:escribe propiedad="fechaTerminacionVigenciaPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.idAsegurado" /> :</td>
			<td class="dato"><midas:escribe propiedad="idAseguradoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.asegurado" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="nombreAseguradoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.calle" /> :</td>
			<td class="dato"><midas:escribe propiedad="callePoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.colonia" /> :</td>
			<td class="dato"><midas:escribe propiedad="coloniaPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.codigoPostal" /> :</td>
			<td class="dato"><midas:escribe propiedad="codigoPostalPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.municipio" /> :</td>
			<td class="dato"><midas:escribe propiedad="municipioPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.estado" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="estadoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.idAgente" /> :</td>
			<td class="dato"><midas:escribe propiedad="idAgentePoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.agente" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="nombreAgentePoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		
		<!-- *******************************************DATOS REPORTE *******************************************************-->
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="6" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.datosReporte" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.sumaAsegurada" /></th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaSumaAsegurada">
							<tr>
					            <td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
					        </tr>
					 	</logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaSumaAsegurada">
							<logic:iterate name="caratulaSiniestroForm" property="listaSumaAsegurada" id="registroSumaAsegurada">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="registroSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="registroSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.sumaAsegurada" nombre="registroSumaAsegurada" formato="###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		
		<!-- *******************************************PORCENTAJES FACULTATIVOS*******************************************************-->
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="7" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.listadoPorcentajesSumaAsegurada" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th>
									<midas:mensaje clave="siniestro.finanzas.caratula.porcentaje"/>
									&nbsp;<midas:mensaje clave="siniestro.finanzas.caratula.cuotaParte" />
								</th>
								<th>
									<midas:mensaje clave="siniestro.finanzas.caratula.porcentaje"/>
									&nbsp;<midas:mensaje clave="siniestro.finanzas.caratula.primerExcedente" />
								</th>
								<th>
									<midas:mensaje clave="siniestro.finanzas.caratula.porcentaje" />
									&nbsp;<midas:mensaje clave="siniestro.finanzas.caratula.facultativo" />
								</th>
								<th>
									<midas:mensaje clave="siniestro.finanzas.caratula.porcentaje" />
									&nbsp;<midas:mensaje clave="siniestro.finanzas.caratula.retencion"/>
								</th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaPorcentajesSumaAsegurada">
							<tr>
					       		<td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaPorcentajesSumaAsegurada">
							<logic:iterate name="caratulaSiniestroForm" property="listaPorcentajesSumaAsegurada" id="registroPorcentajesSumaAsegurada">
								<tr>
								 	<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="numeroSubInciso" nombre="registroPorcentajesSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionSeccion" nombre="registroPorcentajesSumaAsegurada"/>
					                </td>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionCobertura" nombre="registroPorcentajesSumaAsegurada"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoCuotaParte" nombre="registroPorcentajesSumaAsegurada" formato="###,###,##0.00########"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoPrimerExcedente" nombre="registroPorcentajesSumaAsegurada" formato="###,###,##0.00########"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoFacultativo" nombre="registroPorcentajesSumaAsegurada" formato="###,###,##0.00########"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoRetencion" nombre="registroPorcentajesSumaAsegurada" formato="###,###,##0.00########"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
	

		<!-- *******************************************ESTIMACION INICIAL *******************************************************-->
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.listadoEstimacionInicial" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.estimacionInicial" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cuotaParte" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.primerExcedente" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.facultativo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.retencion" /></th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaEstimacionInicial">
							<tr>
					       		<td class="datoTabla" align="center" colspan="9">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaEstimacionInicial">
							<logic:iterate name="caratulaSiniestroForm" property="listaEstimacionInicial" id="registroEstimacionInicial">
								<tr>
								 	<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="numeroSubInciso" nombre="registroEstimacionInicial"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionSeccion" nombre="registroEstimacionInicial"/>
					                </td>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionCobertura" nombre="registroEstimacionInicial"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionRiesgo" nombre="registroEstimacionInicial"/>
					                </td>
					                
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="monto" nombre="registroEstimacionInicial" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoCuotaParte" nombre="registroEstimacionInicial" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoPrimerExcedente" nombre="registroEstimacionInicial" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoFacultativo" nombre="registroEstimacionInicial" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoRetencion" nombre="registroEstimacionInicial" formato="###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		
	
	<!-- *******************************************INDEMNIZACIONES *******************************************************-->	
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="11" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.listadoIndemnizacion" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.indemnizacion.numeroIndemnizacion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.fecha" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.monto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cuotaParte" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.primerExcedente" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.facultativo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.retencion" /></th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaIndemnizaciones">
							<tr>
					       		<td class="datoTabla" align="center" colspan="11">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaIndemnizaciones">
							<logic:iterate name="caratulaSiniestroForm" property="listaIndemnizaciones" id="registroIndemnizacion">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="idToIndemnizacion" nombre="registroIndemnizacion"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="fechaHoraIndemnizacion" nombre="registroIndemnizacion"/>
					                </td>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="numeroSubInciso" nombre="registroIndemnizacion"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionSeccion" nombre="registroIndemnizacion"/>
					                </td>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionCobertura" nombre="registroIndemnizacion"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionRiesgo" nombre="registroIndemnizacion"/>
					                </td>
					                
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="monto" nombre="registroIndemnizacion" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoCuotaParte" nombre="registroIndemnizacion" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoPrimerExcedente" nombre="registroIndemnizacion" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoFacultativo" nombre="registroIndemnizacion" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoRetencion" nombre="registroIndemnizacion" formato="###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		
		<!-- *******************************************GASTOS *******************************************************-->
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="11" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.listadoGastos" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.fecha" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.gastos.prestadorServicio" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.ramo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.subRamo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.monto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cuotaParte" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.primerExcedente" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.facultativo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.retencion" /></th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaGastos">
							<tr>
					       		<td class="datoTabla" align="center" colspan="11">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaGastos">
							<logic:iterate name="caratulaSiniestroForm" property="listaGastos" id="registroGasto">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="fechaHoraGasto" nombre="registroGasto"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="nombrePrestadorServicio" nombre="registroGasto"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionRamo" nombre="registroGasto"/>
					                </td>
					                 <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionSubRamo" nombre="registroGasto"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="descripcionSeccion" nombre="registroGasto"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="descripcionCobertura" nombre="registroGasto"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="monto" nombre="registroGasto" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoCuotaParte" nombre="registroGasto" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoPrimerExcedente" nombre="registroGasto" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoFacultativo" nombre="registroGasto" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoRetencion" nombre="registroGasto" formato="###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		
		<!-- *******************************************INGRESOS *******************************************************-->
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="10" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.listadoIngresos" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.fecha" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.ramo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.subRamo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.monto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cuotaParte" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.primerExcedente" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.facultativo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.retencion" /></th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaIngresos">
							<tr>
					       		<td class="datoTabla" align="center" colspan="10">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaIngresos">
							<logic:iterate name="caratulaSiniestroForm" property="listaIngresos" id="registroIngreso">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="fechaHoraIngreso" nombre="registroIngreso"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionRamo" nombre="registroIngreso"/>
					                </td>
					                 <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionSubRamo" nombre="registroIngreso"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="descripcionSeccion" nombre="registroIngreso"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="descripcionCobertura" nombre="registroIngreso"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="monto" nombre="registroIngreso" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoCuotaParte" nombre="registroIngreso" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoPrimerExcedente" nombre="registroIngreso" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoFacultativo" nombre="registroIngreso" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoRetencion" nombre="registroIngreso" formato="###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		
		<!-- *******************************************SALVAMENTO *******************************************************-->
		<tr>
			<td colspan="8" align="center">
				<div style="width:800px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="10" align="center"><midas:mensaje clave="siniestro.finanzas.caratula.listadoSalvamentos" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.salvamento.nombreItem" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.ramo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.subRamo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.monto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.cuotaParte" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.primerExcedente" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.facultativo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.caratula.retencion" /></th>
							</tr>
						</thead>
						<logic:empty name="caratulaSiniestroForm" property="listaSalvamentos">
							<tr>
					       		<td class="datoTabla" align="center" colspan="10">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="caratulaSiniestroForm" property="listaSalvamentos">
							<logic:iterate name="caratulaSiniestroForm" property="listaSalvamentos" id="registroSalvamento">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="nombreItem" nombre="registroSalvamento"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionRamo" nombre="registroSalvamento"/>
					                </td>
					                 <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="descripcionSubRamo" nombre="registroSalvamento"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="descripcionSeccion" nombre="registroSalvamento"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="descripcionCobertura" nombre="registroSalvamento"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="monto" nombre="registroSalvamento" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoCuotaParte" nombre="registroSalvamento" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoPrimerExcedente" nombre="registroSalvamento" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoFacultativo" nombre="registroSalvamento" formato="###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="montoRetencion" nombre="registroSalvamento" formato="###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
	
		<tr>
			<td class="datoTabla" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.caratula.datosCobranza" />
				<hr>		
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.formaPago" /> :</td>
			<td colspan="5" class="dato"><midas:escribe propiedad="formaPagoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.ultimoReciboPagado" /> :</td>
			<td class="dato"><midas:escribe propiedad="ultimoReciboPagadoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.estatusCobranza" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="estatusCobranza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.fechaPago" /> :</td>
			<td class="dato"><midas:escribe propiedad="fechaPagoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.importePagado" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="montoPagoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.saldoPendiente" /> :</td>
			<td class="dato"><midas:escribe propiedad="saldoPendientePoliza" nombre="caratulaSiniestroForm"/></td>
			<td class="datoTabla"><midas:mensaje clave="siniestro.finanzas.caratula.saldoVencido" /> :</td>
			<td colspan="3" class="dato"><midas:escribe propiedad="saldoVencidoPoliza" nombre="caratulaSiniestroForm"/></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4"></td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a onclick="javascript: listarReportesSiniestro();" href="javascript: void(0);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
			<td colspan="8" align="center"">
				<midas:oculto propiedadFormulario="idReporteSiniestro"/>
				<midas:boton texto="Imprimir caratula" style="width:200px;" onclick="mostrarCaratulaSiniestroPDF();" tipo="guardar"/>
			</td>
		</tr>
 	</table>
</div>
</midas:formulario>
