<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/siniestro/reportes/cargarReporteSiniestralidadAnexo">	
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="siniestro.reportes.reporteSiniestralidadAnexo"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: right;width: 15%">
				<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.poliza"/>:
			</td>			
		    <td>
		    	<midas:texto id="numeroDePoliza" propiedadFormulario="numeroDePoliza" onfocus="new Mask('####-########-##', 'string').attach(this)"/>
		    </td>
			<td style="text-align: right;">
				<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.fechaInicial"/>:
			</td>
			<td>
				<midas:texto id="fechaInicial" propiedadFormulario="fechaInicial" soloLectura="true" caracteres="10" onblur="esFechaValida(this);"/>
			</td>
			<td style="text-align: center;">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendario()"></a>
				</div>	
			</td>
			<td style="text-align: right;">
				<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.fechaFinal"/>:
			</td>
			<td>
				<midas:texto id="fechaFinal" propiedadFormulario="fechaFinal" soloLectura="true" caracteres="10" onblur="esFechaValida(this);" />
			</td> 
		</tr>
		<tr>
			<td colspan="3"></td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td colspan="3"></td>
		</tr>
		<tr>
		    <td style="text-align: right;">
		    	<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.agente"/>:
		    </td>			
		    <td colspan="6">
		    	<midas:texto id="nombreDelAgente" propiedadFormulario="nombreDelAgente" onkeypress="return soloAlfanumericos(this, event, false)"/>
		    </td>
		</tr>
		<tr>
		    <td style="text-align: right;">
		    	<midas:mensaje clave="siniestro.reportes.reporteSiniestralidad.asegurado"/>:
		    </td>			
		    <td colspan="6">
		    	<midas:texto id="nombreDelAsegurado" propiedadFormulario="nombreDelAsegurado" onkeypress="return soloAlfanumericos(this, event, false)"/>
		    </td>
		</tr>		
	</table>
	<table width="98%">
		<tr>
			<td colspan="7">
				<div class="alinearBotonALaDerecha">
					<div id="b_agregar">
						<a href="javascript: void(0);"
							onclick="javascript: mostrarReporteSiniestralidadYAnexo('ReporteSiniestralidadAnexo',document.getElementById('fechaInicial').value,document.getElementById('fechaFinal').value,document.getElementById('nombreDelAgente').value,document.getElementById('numeroDePoliza').value,document.getElementById('nombreDelAsegurado').value);">
							<midas:mensaje clave="midas.accion.reportePDF"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>
