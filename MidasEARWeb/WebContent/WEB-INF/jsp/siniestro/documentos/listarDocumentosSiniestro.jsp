<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/documentos/listarDocumentosSiniestro.do">
	<table width="100%" border="0" cellspacing="4" cellpadding="0" id="agregar">
		<tr>
			<td width="25%"></td>
			<td width="50%"></td>
		</tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="siniestro.documento.listadoDocumento.titulo"/>	
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<br/><br/><br/>
				<div id="resultados">
				<midas:tabla idTabla="documentosSiniestroTabla"
					claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoSiniestro"
					claseCss="tablaConResultados" nombreLista="documentosSiniestro"
					urlAccion="/siniestro/documentos/listarDocumentosSiniestro.do" exportar="false">
					<midas:columna propiedad="nombreDocumentoSiniestro" titulo="Nombre" estilo="centrado"/>
					<midas:columna propiedad="tipoDocumentoSiniestro" titulo="Tipo de documento" estilo="centrado"/>
					<midas:columna propiedad="URL" titulo="Documento" estilo="centrado"/>
					<midas:columna propiedad="acciones" titulo="" estilo="centrado"/>
				</midas:tabla>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a onclick="javascript: listarReportesSiniestro();" href="javascript: void(0);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_agregar">
						<a onclick='javascript: mostrarAgregarDocumentosSiniestro(<midas:escribe propiedad="idReporteSiniestro" nombre="documentoSiniestroForm"/>);' href="javascript: void(0);"><midas:mensaje clave="midas.accion.agregar"/></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>