<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/documentos/mostrarAgregarDocumentoSiniestro">
<table id="agregar">
  <tr>
    <td align="right"><midas:mensaje clave="siniestro.documento.agregarDocumento.nombreDocumento"/>: </td>
    <td colspan="2">
		<midas:texto id="nombreDocumento" propiedadFormulario="nombreDocumento" caracteres="40"/>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><midas:mensaje clave="siniestro.documento.agregarDocumento.tipoDocumento"/>: </td>
    <td colspan="2">
    	<logic:equal name="documentoSiniestroForm" property="idTipoDocumento" value="" >
    	<midas:comboCatalogo nombreCatalogo="tctipodocumentosiniestro" idCatalogo="idTcTipoDocumentoSiniestro" descripcionCatalogo="descripcionTipoDocumento" propiedad="idTipoDocumento" styleId="idTipoDocumento" size="1" styleClass="cajaTexto"/>
    	</logic:equal>
    	<logic:notEqual name="documentoSiniestroForm" property="idTipoDocumento" value="" >
    	<midas:oculto propiedadFormulario="idTipoDocumento"/>
    	<midas:comboCatalogo readonly="true" nombreCatalogo="tctipodocumentosiniestro" idCatalogo="idTcTipoDocumentoSiniestro" descripcionCatalogo="descripcionTipoDocumento" propiedad="idTipoDocumento" styleId="idTipoDocumento" size="1" styleClass="cajaTexto"/>
    	</logic:notEqual>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">
    	&nbsp;
    	<midas:oculto propiedadFormulario="idReporteSiniestro"/>
    	<midas:oculto propiedadFormulario="idDocumentoSiniestro"/>
    	</td>
    <td width="20%">
    <midas:boton onclick="validaFormaDocumentoSiniestro(document.documentoSiniestroForm);" tipo="guardar" texto="Guardar documento" style="width:140px;"/>
    </td>
    <td width="20%">
    </td>
    <td width="40%">&nbsp;</td>
  </tr>
</table>
</midas:formulario>