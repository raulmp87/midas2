<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/siniestro/cabina/mostrarDetalle">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="siniestro.cabina.desplegarReporte" />
					</td>
				</tr>				
				<tr>				
					<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="numeroReporte" nombre="reporteSiniestroForm"/></td>			
					<td colspan="2"></td>
				</tr>					
				<tr>					
					<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaReporte" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="fechaReporte" nombre="reporteSiniestroForm"/></td>
					<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.horaReporte" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="horaReporte" nombre="reporteSiniestroForm"/></td>
				</tr>															
				<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.datosPersonaReporta" />
				</th>
			</tr>
			<tr>									
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.nombrePersonaReporta" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="nombrePersonaReporta" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.telefonoPersonaReporta" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="telefonoPersonaReporta" nombre="reporteSiniestroForm"/></td>												
			</tr>			
			<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.datosPoliza" />
				</th>
			</tr>
			<tr>									
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroPoliza" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="numeroPoliza" nombre="reporteSiniestroForm"/></td>				
				<td colspan="2"></td>												
			</tr>
			<tr>									
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.tipoPoliza" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="tipoPoliza" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.claveProducto" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="claveProducto" nombre="reporteSiniestroForm"/></td>												
			</tr>
			<tr>									
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreAsegurado" />:</th>
				<td colspan="3" class="fondoCajaTexto"><midas:escribe propiedad="nombreAsegurado" nombre="reporteSiniestroForm"/></td>																
			</tr>		
			<tr>													
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.direccionAsegurado" />:</th>
				<td colspan="3" class="fondoCajaTexto"><midas:escribe propiedad="direccionAsegurado" nombre="reporteSiniestroForm"/></td>												
			</tr>
			<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.datosSiniestro" />
				</th>
			</tr>							
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.estado" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionEstado" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.ciudad" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionCiudad" nombre="reporteSiniestroForm"/></td>												
			</tr>						
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.colonia" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionColonia" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.calle" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="calle" nombre="reporteSiniestroForm"/></td>												
			</tr>			
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreContacto" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="nombreContacto" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.telefonoContacto" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="telefonoContacto" nombre="reporteSiniestroForm"/></td>												
			</tr>			
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.descripcionSiniestro" />:</th>
				<td colspan="3" class="fondoCajaTexto"><midas:escribe propiedad="descripcionSiniestro" nombre="reporteSiniestroForm"/></td>																				
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.descripcionEventoCatastrofico" />:</th>
				<td colspan="3" class="fondoCajaTexto"><midas:escribe propiedad="descripcionEventoCatastrofico" nombre="reporteSiniestroForm"/></td>																				
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.observaciones" />:</th>
				<td colspan="3" class="fondoCajaTexto"><midas:escribe propiedad="observaciones" nombre="reporteSiniestroForm"/></td>																				
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreCoordinador" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="nombreCoordinador" nombre="reporteSiniestroForm"/></td>
				<td colspan="2"></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreAjustador" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="nombreAjustador" nombre="reporteSiniestroForm"/></td>
				<td colspan="2"></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreCabinero" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="nombreCabinero" nombre="reporteSiniestroForm"/></td>
				<td colspan="2"></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaSiniestro" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaSiniestro" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaAsignacionAjustador" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaAsignacionAjustador" nombre="reporteSiniestroForm"/></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaContactoAjustador" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaContactoAjustador" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaLlegadaAjustador" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaLlegadaAjustador" nombre="reporteSiniestroForm"/></td>																
			</tr>			
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaInspeccionAjustador" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaInspeccionAjustador" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaPreliminar" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaPreliminar" nombre="reporteSiniestroForm"/></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaDocumento" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaDocumento" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaInformeFinal" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaInformeFinal" nombre="reporteSiniestroForm"/></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaTerminadoSiniestro" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaTerminadoSiniestro" nombre="reporteSiniestroForm"/></td>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaCerradoSiniestro" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="fechaCerradoSiniestro" nombre="reporteSiniestroForm"/></td>																
			</tr>
			<tr>
				<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.terminoAjuste" />:</th>
				<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTerminoAjuste" nombre="reporteSiniestroForm"/></td>
				<td colspan="2"></td>																
			</tr> 			 				
			<tr>   	  				
				<td class="regresar" colspan="4">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);" onclick="javascript: regresarListaReportes();"><midas:mensaje clave="midas.accion.regresar"/></a>
							<html:hidden property="idToReporteSiniestro" name="reporteSiniestroForm"/>
						</div>
					</div>
				</td>
			</tr>   	  				
		</table>
	</div>
</midas:formulario>
