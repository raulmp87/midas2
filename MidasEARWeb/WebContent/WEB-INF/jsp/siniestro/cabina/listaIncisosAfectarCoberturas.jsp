<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<div style="width: 100%;height:150px; border: 1px, solid; overflow: auto;" class="tableContainer">
	<table style="white-space: normal;" class="tablaConResultados">
		<thead>
			<tr>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.inciso" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.ubicacion" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.inicioVigencia" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.finVigencia" />
				</th>
				<th>
					&nbsp;
				</th>
			</tr>
		</thead>
		<logic:empty name="coberturaRiesgoForm" property="listaIncisosPoliza">
			<tr>
				<td class="datoTabla" align="center" colspan="5">
					<midas:mensaje clave="lista.vacia" />
				</td>
			</tr>
		</logic:empty>
		<logic:notEmpty name="coberturaRiesgoForm" property="listaIncisosPoliza">
			<logic:iterate name="coberturaRiesgoForm" property="listaIncisosPoliza" id="listaIncisos" indexId="indice">
				<tr>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="numeroInciso" nombre="listaIncisos" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="direccionInciso" nombre="listaIncisos" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="fechaInicioVigencia" nombre="listaIncisos" formato="dd/MM/yyyy" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="fechaFinVigencia" nombre="listaIncisos" formato="dd/MM/yyyy" />
					</td>
					<td class="datoTabla" align="left">
						<logic:equal name="listaIncisos" property="seleccionado" value="1">
							<input type="radio" name="idInciso" checked="checked" value="<midas:escribe propiedad="numeroInciso" nombre="listaIncisos"/>" onclick="javascript: muestraListaSeccionSubIncisosCoberturasRiesgo(<midas:escribe propiedad="numeroInciso" nombre="listaIncisos"/>);"  />
						</logic:equal>
						<logic:notEqual name="listaIncisos" property="seleccionado" value="1">
							<input type="radio" name="idInciso" value="<midas:escribe propiedad="numeroInciso" nombre="listaIncisos"/>" onclick="javascript: muestraListaSeccionSubIncisosCoberturasRiesgo(<midas:escribe propiedad="numeroInciso" nombre="listaIncisos"/>);"  />
						</logic:notEqual>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</table>
</div>