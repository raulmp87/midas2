<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/siniestro/cabina/listar">
	
	<table width="100%" border="0" cellspacing="4" cellpadding="0" id="filtros">
	  <tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.cabina.listarReporte"/>
			</td>
	  </tr>
	  <tr>
	    <td width="13%" align="right">
	      <midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte"/>:
		</td>
	    <td width="13%">
	    	<midas:texto propiedadFormulario="numeroReporte" id="numeroReporte" onkeypress="return soloNumeros(this, event, false)" />
	    </td>
	    <td width="12%" align="right">
	    	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarfechaReporte"/>:
	    </td>
	    <td width="14%">
	    	<midas:texto propiedadFormulario="fechaReporte" id="fechaReporte" soloLectura="true"/>
    		<image src="<html:rewrite page='/img/b_calendario.gif'/>" border=0 />
	    </td>
	    <td width="48%">&nbsp;</td>
	  </tr>
	  <tr>
	    <td align="right">
	      <midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroPoliza"/>:
	    </td>
	    <td>
	    	<midas:texto propiedadFormulario="numeroPolizaBuscar" id="numeroPolizaBuscar" onfocus="new Mask('####-########-##', 'string').attach(this)" />
	    </td>
	    <td align="right">
	      	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarLugarSiniestro"/>:
	    </td>
	    <td>
	     <midas:estado styleId="idEstado" size="1" propiedad="idEstado"
					  pais="PAMEXI" styleClass="cajaTexto"
					 onchange="limpiarObjetos('idCiudad,idColonia'); getCiudades(this,'idCiudad');" />
	    </td>
	    <td>
	     <midas:ciudad styleId="idCiudad" size="1" propiedad="idCiudad"
					 estado="idEstado" styleClass="cajaTexto" onchange="getColonias(this,'idColonia')" />				
	    </td>
	  </tr>

	  <tr>
    	<td colspan="5">
    		 <table width="100%" border="0" cellspacing="4" cellpadding="0" id="desplegar">
			      <tr>
			        <td>
						<midas:radio valorEstablecido="1" propiedadFormulario="tipoPersona" onClick="setPersonaFisica();">
							<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarPersonaFisica"/>
						</midas:radio>
					</td>
			        <td>
				        <midas:radio valorEstablecido="2" propiedadFormulario="tipoPersona" onClick="setPersonaMoral();">
				        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarPersonaMoral"/>
				        </midas:radio>
					</td>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
			      </tr>
			      <tr>
			        <td>&nbsp;</td>
			        <td>
			        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarPrimerNombre"/>
			        </td>
			        <td>
			        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarSegundoNombre"/>
			        </td>
			        <td>
			        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarApellidoPaterno"/>
			        </td>
			        <td>
			        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarApellidoMaterno"/>
			        </td>
			        <td>&nbsp;</td>
			      </tr>
			      <tr>
			        <td>
			        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarNombre"/>
			        </td>
			        <td><midas:texto propiedadFormulario="primerNombre" id="primerNombre" soloLectura="true"/></td>
			        <td><midas:texto propiedadFormulario="segundoNombre" id="segundoNombre" soloLectura="true"/></td>
			        <td><midas:texto propiedadFormulario="apellidoPaterno" id="apellidoPaterno" soloLectura="true"/></td>
			        <td><midas:texto propiedadFormulario="apellidoMaterno" id="apellidoMaterno" soloLectura="true"/></td>
			        <td>&nbsp;</td>
			      </tr>
			      <tr>
			        <td>
			        	<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarRazonSocial"/>
			        </td>
			        <td><midas:texto propiedadFormulario="razonSocial" id="razonSocial" soloLectura="true"/></td>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
			        <td>&nbsp;</td>
			      </tr>
			  </table>
    	</td>
  	  </tr>
  	  <tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: listarFiltradoReportesSiniestroGrid();">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
	  </tr>
  	  
	</table>
	<div id="indicadorListaSiniestros"></div>
	<div id="resultadosListaSiniestros" style="width: 100%;height: 180px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
		<!-- 
		<display:table name="reporteSiniestros" decorator="mx.com.afirme.midas.decoradores.ReporteSiniestro" pagesize="10" class="tablaConResultados"
			requestURI="/siniestro/cabina/listarFiltrado.do" cellspacing="0" cellpadding="0">
		  	<display:column property="numeroReporte" title="No. de reporte"/>
		  	<display:column property="formatoFechaReporte" title="Fecha"/>
		  	<display:column property="numeroPolizaCliente" title="No. Poliza"/>
		  	<display:column property="nombreAsegurado" title="Nombre/Raz&oacute;n Social"/>
		  	<display:column property="acciones" title="Acciones"/>
		</display:table>
	</div>
		 -->
	
	<midas:oculto nombreFormulario="reporteSiniestroForm" propiedadFormulario="idToReporteSiniestro" />
	
</midas:formulario>
