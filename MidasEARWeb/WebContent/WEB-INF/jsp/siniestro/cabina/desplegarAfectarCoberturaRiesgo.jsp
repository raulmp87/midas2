<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/poliza/afectarCoberturaRiesgo">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.titulo" />				
			</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.numeroPoliza" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="numeroPoliza" nombre="coberturaRiesgoForm"/>
			</td>
			<td colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:95%;height:250px;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.inciso" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.seccion" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.afectar" /></th>
							</tr>
						</thead>
						<logic:empty name="coberturaRiesgoForm" property="listarCoberturasRiesgo">
							<tr>
	                            <td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="coberturaRiesgoForm" property="listarCoberturasRiesgo">
							<logic:iterate name="coberturaRiesgoForm" property="listarCoberturasRiesgo" id="lista"  indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="lista" />
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.sumaAsegurada" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
								</tr>
							</logic:iterate>
								
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td align="center">
				<midas:boton onclick="activaTabReporteSiniestro();" tipo="regresar" texto="Regresar" style="width:75px;"/>
			</td>
			<td colspan="3" >&nbsp;</td>
		</tr>
		
 	</table>
</midas:formulario>
