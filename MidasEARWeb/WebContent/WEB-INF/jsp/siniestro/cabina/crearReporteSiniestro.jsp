<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<div style="height: 450px; width:90%; overflow:auto" hrefmode="ajax-html"  id="crearReporteSiniestro" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div id="reporteSiniestro" name="Reporte de Siniestro" >
		<center>
			<midas:formulario accion="/siniestro/cabina/guardarReporte">
			<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
			<html:hidden property="numeroReporte" styleId="numeroReporte"/>
			<html:hidden property="fechaReporte" styleId="fechaReporte"/>
			<html:hidden property="horaReporte" styleId="horaReporte"/>
			<html:hidden property="idToPoliza" styleId="idToPoliza"/>
			<html:hidden property="numeroPoliza" styleId="numeroPoliza"/>
			<html:hidden property="tipoNegocio" styleId="idTipoNegocio"/>
			<html:hidden property="fechaAsignacionAjustador" styleId="fechaAsignacionAjustador"/>
			<html:hidden property="idStatus" styleId="idStatus"/>
			
			<html:hidden property="numeroEndoso" styleId="numeroEndoso"/>
			<html:hidden property="idAgente" styleId="idAgente"/>
			<html:hidden property="nombreAgente" styleId="nombreAgente"/>
			<html:hidden property="nombreOficina" styleId="nombreOficina"/>
			<html:hidden property="nombreAsegurado" styleId="nombreAsegurado"/>
			<html:hidden property="idCliente" styleId="idCliente"/>
			<html:hidden property="rolesUsuario" styleId="rolesUsuario"/>
			<html:hidden property="fechaAsignoPoliza" styleId="fechaAsignoPoliza"/>
			
				<table id="agregar" border="0" >
					<tr>
						<td class="titulo" colspan="7">
							<midas:mensaje clave="siniestro.cabina.crearReporte" />				
						</td>			
					</tr>		
					<tr>
						<td width="6%">&nbsp;</td>
						<td width="15%" align="left">
							<strong>
								<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte" />:
							</strong>
							<em id="numeroReporteDiv"> 
								<c:if test="${empty reporteSiniestroForm.numeroReporte}">
									<midas:mensaje clave="siniestro.cabina.reportesiniestro.sinAsignar" />
								</c:if>
								<c:if test="${not empty reporteSiniestroForm.numeroReporte}">
									<midas:escribe propiedad="numeroReporte" nombre="reporteSiniestroForm"/>
								</c:if>
							</em>
						</td>
						<td width="25%">&nbsp;</td>
						<td width="10%">&nbsp;</td>
						<td width="15%" align="right"><strong><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaReporte" />:</strong> <midas:escribe propiedad="fechaReporte" nombre="reporteSiniestroForm"/></td>
						<td width="25%"align="left"><strong><midas:mensaje clave="siniestro.cabina.reportesiniestro.horaReporte" />:</strong> <midas:escribe propiedad="horaReporte" nombre="reporteSiniestroForm"/></td>
						<td width="4%">&nbsp;</td>
					</tr>
<!--Datos de la persona que  reporta	-->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosPersonaCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccionTitulo" colspan="7">
											<midas:mensaje clave="siniestro.cabina.datosPersonaReporta" />
										</th>
									</tr>
									<tr>
										<td width="6%">&nbsp;</td>
										<th width="15%" align="left">
											<etiquetas:etiquetaError  property="nombrePersonaReporta" requerido="si"
												key="siniestro.cabina.reportesiniestro.nombrePersonaReporta" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</th>
										<td width="25%" colspan="3" >
											<midas:texto id="nombrePersonaReporta" propiedadFormulario="nombrePersonaReporta"
											onkeypress="return soloLetras(this, event, false)" 
											onblur="if (this.value !== '' && $('telefonoPersonaReporta').value !== '' && validaTelefonoReporta())
														crearReporteSiniestroNumero($('idToReporteSiniestro').value,this.value,$('telefonoPersonaReporta').value,$('fechaReporte').value,$('horaReporte').value);"
										 	/>
										</td>
										<td colspan="2" >&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th align="left">
											<etiquetas:etiquetaError property="telefonoPersonaReporta" requerido="si"
											key="siniestro.cabina.reportesiniestro.telefonoPersonaReporta" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</th>
										<td width="25%" >
											<midas:texto caracteres="19" id="telefonoPersonaReporta" propiedadFormulario="telefonoPersonaReporta"
											onkeypress="return soloNumeros(this, event, false);" 
											onblur="if (this.value !== '' && $('nombrePersonaReporta').value !== '' && validaTelefonoReporta())
														crearReporteSiniestroNumero($('idToReporteSiniestro').value,$('nombrePersonaReporta').value,this.value,$('fechaReporte').value,$('horaReporte').value);"/>
										</td>
										<td width="10%" >&nbsp;</td>
										<td width="15%" >&nbsp;</td>
										<td width="25%" >&nbsp;</td>
										<td width="4%" >&nbsp;</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
<!--Datos de la poliza-->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosPolizaCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccionTitulo" colspan="7">
											<midas:mensaje clave="siniestro.cabina.datosPoliza" />
										</th>
									</tr>
									<tr>
										<td width="6%">&nbsp;</td>
										<td class="msj" width="15%">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroPoliza" />:
										</td>
										<td width="25%">
											<midas:texto id="numeroPolizaBuscar" propiedadFormulario="numeroPolizaBuscar" onfocus="new Mask('####-########-##', 'string').attach(this)" />
										</td>
										<td width="54%" colspan="5">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td class="msj">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.Asegurado" />:
										</td>
										<td colspan="2">
											<midas:texto id="idNombreAsegurado" propiedadFormulario="nombreAsegurado" />
										</td>
										<td align="center" colspan="2">
											<div class="alinearBotonAlCentro">
												<div id="b_buscar_poliza" >							 
													<a href="javascript: void(0);"																														  	
													onclick="javascript: buscarListaPolizas();">
													<midas:mensaje clave="siniestro.cabina.reportesiniestro.buscarPoliza"/>
													</a>
												</div>
											</div>
										</td>
										<td >&nbsp;</td>
									</tr>
									<tr>
										<td colspan="7">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="7">
											<div id="indicadorCargaPolizas"></div>
											<div id="idListaPolizas"></div>
											<div id="pagingArea"></div><div id="infoArea"></div>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
<!-- Datos del Siniestro-->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosSiniestroCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccionTitulo" colspan="7">
											<midas:mensaje clave="siniestro.cabina.datosSiniestro" />
										</th>
									</tr>
									<tr >
										<td colspan="6" align="right">
											<div id="idDireccionPolizaCrear" style="display:none">
												<midas:mensaje clave="siniestro.cabina.reportesiniestro.direccionPolizaCrear" />
												<input  type="checkbox" name="direccionPolizaCrear"  value="1" onclick="cargarDireccionPoliza(this)">
											</div>
											<div id="procesandoSiniestros"></div>
										</td>
										<td >&nbsp;</td>
									</tr>
									<tr>
										<td width="3%">&nbsp;</td>
										<th width="12%" align="left">
											<etiquetas:etiquetaError property="calle" requerido="si"
											key="siniestro.cabina.reportesiniestro.calle" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</th>
										<td colspan="2">
											<midas:texto longitud="20" id="calle" propiedadFormulario="calle"/>
										</td>
										<th width="15%" align="left">
											<etiquetas:etiquetaError property="codigoPostal" requerido="si"
											key="siniestro.cabina.reportesiniestro.cp" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</th>
										<td  width="25%">
											<midas:texto longitud="20" id="codigoPostal" propiedadFormulario="codigoPostal"
												onkeypress="return soloNumeros(this, event, false)"
												onchange="if (this.value !== '')getColoniasPorCP(this.value, 'idColonia','idCiudad','idEstado');"/>
										</td>
										<td  width="4%">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="idEstado" requerido="si"
											key="siniestro.cabina.reportesiniestro.estado" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td colspan="2">
											<midas:estado styleId="idEstado" size="1" propiedad="idEstado"
											pais="PAMEXI" styleClass="cajaTexto"
											onchange="limpiarObjetos('idCiudad,idColonia'); getCiudades(this,'idCiudad');asignarCoordinadorPorZona(this.value);" />
										</td>
										<td>
											<etiquetas:etiquetaError property="idCiudad" requerido="si"
											key="siniestro.cabina.reportesiniestro.ciudad" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td >
											<midas:ciudad styleId="idCiudad" size="1" propiedad="idCiudad"
											estado="idEstado" styleClass="cajaTexto" onchange="getColonias(this,'idColonia');"/>				
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="idColonia" requerido="si"
											key="siniestro.cabina.reportesiniestro.colonia" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td colspan="2">
											<midas:colonia styleId="idColonia" size="1" propiedad="idColonia"
												ciudad="idCiudad" styleClass="cajaTexto"
												onchange="setCodigoPostal(this.value);"/>
										</td>
										<td>
											<etiquetas:etiquetaError property="fechaSiniestro" requerido="si"
												key="siniestro.cabina.reportesiniestro.fechaSiniestro" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td>
											<midas:texto id="fechaSiniestro" soloLectura="true" propiedadFormulario="fechaSiniestro" caracteres="10" longitud="25" onblur="asignaFechaAsignoPolizaReporte();"/>
												<img src="<html:rewrite page='/img/b_calendario.gif'/>"
													width="12" height="12" style="cursor:hand;"
													onclick="getCalendarioSiniestros(event,'fechaSiniestro');" />	
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="descripcionSiniestro" requerido="si"
												key="siniestro.cabina.reportesiniestro.descripcionSiniestro" normalClass="normal"
												errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td colspan="2">
											<midas:areatexto propiedadFormulario="descripcionSiniestro" renglones="5" columnas="50"/>
										</td>	
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="idEventoCatastrofico" requerido="no"
											key="siniestro.cabina.reportesiniestro.eventoCatastrofico" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td >
											<midas:comboCatalogo idCatalogo="idTcEventoCatastrofico" styleId="idEventoCatastrofico" propiedad="idEventoCatastrofico" descripcionCatalogo="descripcionEvento" nombreCatalogo="tceventocatastrofico" size="1" styleClass="cajaTexto"/>
										</td>
										<td colspan="4">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="nombreContacto" requerido="si"
												key="siniestro.cabina.reportesiniestro.nombreContacto" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td colspan="2">
											<midas:texto propiedadFormulario="nombreContacto" 
											onkeypress="return soloLetras(this, event, false)"/>
										</td>
										<td>
											<etiquetas:etiquetaError  property="telefonoContacto" requerido="si"
												key="siniestro.cabina.reportesiniestro.telefonoContacto" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td>
											<midas:texto  caracteres="19" id="telefonoContacto" propiedadFormulario="telefonoContacto" 
											onkeypress="return soloNumeros(this, event, false)"/>
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="observaciones" requerido="no"
												key="siniestro.cabina.reportesiniestro.observaciones" normalClass="normal"
												errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td colspan="2">
											<midas:areatexto propiedadFormulario="observaciones" renglones="5" columnas="50"/>
										</td>			
										<td colspan="3">&nbsp;</td>	
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="idCoordinador" requerido="si"
											key="siniestro.cabina.reportesiniestro.nombreCoordinador" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td>
											<midas:comboCatalogo idCatalogo="id" styleId="idCoordinador" propiedad="idCoordinador" descripcionCatalogo="nombreUsuario" nombreCatalogo="coordinadorSiniestros" size="1"/>
										</td>
										<td colspan="4">&nbsp;</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
<!--  Datos del Ajustador  -->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosAjustadorCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccionTitulo" colspan="7">
											<midas:mensaje clave="siniestro.cabina.asignacionAjustador" />
										</th>
									</tr>
									
									<tr>
										<td width="6%">&nbsp;</td>
										<th width="15%" align="left">
											<etiquetas:etiquetaError  property="tipoNegocio" requerido="no"
												key="siniestro.cabina.reportesiniestro.tipoNegocio" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</th>
										<td width="25%">
											<midas:texto deshabilitado="true" longitud="20" id="tipoNegocio" propiedadFormulario="tipoNegocio"	/>
										</td>
										
											<th  width="13%"> 
												<div id="idTituloAjustadorNombrado" style="display:none">
													Ajustador Nombrado 
												 </div>
											</td>
							                <td colspan="2">
							                	<div id="idAjustadorNombrado" style="display:none">
							                		<midas:texto id="nombreAjustadorNombrado" propiedadFormulario="nombreAjustadorNombrado" deshabilitado="true" />
							                	</div>
							                </td>
							          
						                <td >&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<etiquetas:etiquetaError property="idAjustador" requerido="no"
											key="siniestro.cabina.reportesiniestro.nombreAjustador" normalClass="normal"
											errorClass="error" errorImage="/img/information.gif" />
										</td>
										<td colspan="2">
											<midas:comboCatalogo  idCatalogo="idTcAjustador" styleId="idAjustador" propiedad="idAjustador" descripcionCatalogo="nombre" nombreCatalogo="tcajustador" size="1"/>
										</td>
										<td colspan="3">&nbsp;</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td  colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="campoRequerido"  colspan="2">
				 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
						</td> 
						<td>&nbsp;</td>
						<td class="guardar" colspan="3">
							<div class="alinearBotonALaDerecha">
								<div id="b_regresar">
									<a href="javascript: void(0);" onclick="javascript: regresarListaReportes();"><midas:mensaje clave="midas.accion.regresar"/></a>
								</div>					
								<div id="b_guardar">
									<a href="javascript: void(0);" onclick="javascript: validaGuardarReporteSiniestro(document.reporteSiniestroForm)">
										<midas:mensaje clave="midas.accion.guardar"/>
									</a>									
								</div>
							</div>
						</td> 
					</tr>
				</table>
			
			</midas:formulario>
		</center>
	</div>
	<div id="datosPoliza" name="Datos de la poliza" href="http://void" extraAction="datosPolizaTab()"></div>
</div>
