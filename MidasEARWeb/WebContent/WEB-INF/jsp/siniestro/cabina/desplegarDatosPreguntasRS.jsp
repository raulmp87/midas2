<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/curpRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/siniestro/cabina/desplegarDatosPreguntasEspeciales">
<table width="100%" border="0" cellspacing="15" cellpadding="0">
  <tr>
    <td>
<%--    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">--%>
<%--      <tr>--%>
<%--        <th width="19%"><midas:mensaje clave="siniestro.reporte.preguEspecial.noPoliza"/>:</th>--%>
<%--        <td width="21%">--%>
<%--       	  <midas:texto id="noPoliza" propiedadFormulario="noPoliza" deshabilitado="true"/>--%>
<%--       	</td>--%>
<%--       	<td width="60%">&nbsp;</td>--%>
<%--      </tr>--%>
<%--      <tr>--%>
<%--        <th>--%>
<%--        	<midas:mensaje clave="siniestro.reporte.preguEspecial.noCertificado"/>:--%>
<%--        </th>--%>
<%--        <td>--%>
<%--        	<midas:texto id="noCertificado" propiedadFormulario="noCertificado" deshabilitado="true"/>--%>
<%--        </td>--%>
<%--        <td>&nbsp;</td>--%>
<%--      </tr>--%>
<%--      <tr>--%>
<%--        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.nomFuncionario"/>: </th>--%>
<%--        <td>--%>
<%--        	<midas:texto id="nomFuncionario" propiedadFormulario="nomFuncionario" deshabilitado="true"/>        </td>--%>
<%--        <td>&nbsp;</td>--%>
<%--      </tr>--%>
<%--      <tr>--%>
<%--        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.direccionFuncionario"/>:</th>--%>
<%--        <td>--%>
<%--        	<midas:texto id="direccionFuncionario" propiedadFormulario="direccionFuncionario" deshabilitado="true"/>        </td>--%>
<%--        <td>&nbsp;</td>--%>
<%--      </tr>--%>
<%--      <tr>--%>
<%--        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.telefono"/>:</th>--%>
<%--        <td>--%>
<%--        	<midas:texto id="telefono" propiedadFormulario="telefono" deshabilitado="true"/>        </td>--%>
<%--        <td>&nbsp;</td>--%>
<%--      </tr>--%>
<%--      <tr>--%>
<%--        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.correo"/>: </th>--%>
<%--        <td>--%>
<%--        	<midas:texto id="correo" propiedadFormulario="correo" deshabilitado="true"/>        </td>--%>
<%--        <td>&nbsp;</td>--%>
<%--      </tr>--%>
<%--    </table>--%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
      	<td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.noPoliza"/>:</th>
        <td>
       	  <midas:texto id="noPoliza" propiedadFormulario="noPoliza" deshabilitado="true"/>
       	</td>
       	<th><midas:mensaje clave="siniestro.reporte.preguEspecial.noCertificado"/>:</th>
       	<td><midas:texto id="noCertificado" propiedadFormulario="noCertificado" deshabilitado="true"/></td>
       	<td>&nbsp;</td>
      </tr>
      <tr>
      	<td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.nomFuncionario"/>: </th>
        <td>
        	<midas:texto id="nomFuncionario" propiedadFormulario="nomFuncionario" deshabilitado="true"/>        
        </td>
        
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.correo"/>: </th>
        <td>
        	<midas:texto id="correo" propiedadFormulario="correo" deshabilitado="true"/>        
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
       <td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.telefono"/>:</th>
        <td>
        	<midas:texto id="telefono" propiedadFormulario="telefono" deshabilitado="true"/>        
        </td>
        <th>Calle:</th>
        <td>
        	<midas:texto id="direccionFuncionario" propiedadFormulario="direccionFuncionario" deshabilitado="true"/>        
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.idEstado"/></th>
        <td>
        	<midas:estado styleId="idEstadoPreguntas" size="1" propiedad="idEstado" pais="PAMEXI" styleClass="cajaTexto"
				onchange="limpiarObjetos('idCiudadPreguntas,idColoniaPreguntas'); getCiudades(this,'idCiudadPreguntas');" readonly="true" />
        </td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.idCiudad"/></th>
        <td>
        	<midas:ciudad  styleId="idCiudadPreguntas" size="1" propiedad="idCiudad" estado="idEstado" styleClass="cajaTexto" 
				onchange="getColonias(this,'idColoniaPreguntas');" readonly="true"/>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.idColonia"/></th>
        <td>
        	<midas:colonia styleId="idColoniaPreguntas" size="1" propiedad="idColonia" ciudad="idCiudad" 
        		styleClass="cajaTexto" onchange="setCodigoPostalSiniestro(this.value,'codigoPostalPreguntas');" readonly="true"/>
        </td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.CP"/></th>
        <td>
        	<midas:texto id="codigoPostalPreguntas" propiedadFormulario="codigoPostal" onkeypress="return soloNumeros(this, event, false)" 
				onchange="if (this.value !== '')getColoniasPorCP(this.value, 'idColoniaPreguntas','idCiudadPreguntas','idEstadoPreguntas');" deshabilitado="true"/>
		</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="10" cellpadding="0" id="desplegar">
          <tr>
            <th width="19%"><midas:mensaje clave="siniestro.reporte.preguEspecial.dependenciaTrabaja"/>:</th>
            <td colspan="3">
            	<midas:texto id="dependenciaTrabaja" propiedadFormulario="dependenciaTrabaja" deshabilitado="true"/>
            </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.cargoOcupa"/>: </th>
            <td colspan="3">
            	<midas:texto id="cargoOcupa" propiedadFormulario="cargoOcupa" deshabilitado="true"/>
            </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.tipoCobertura"/>: </th>
            <td colspan="3">
	            <midas:radio valorEstablecido="B" propiedadFormulario="tipoCobertura" deshabilitado="true">B&aacute;sica</midas:radio>
	            <midas:radio valorEstablecido="P" propiedadFormulario="tipoCobertura" deshabilitado="true">Potenciada</midas:radio>
	        </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.fechaNotificacion"/></th>
            <td colspan="3">
    			<midas:texto id="fechaNotificacion" propiedadFormulario="fechaNotificacion" deshabilitado="true"/>
            </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.tipoDocumentoRecibio"/>:</th>
            <td colspan="3">
            	<midas:radio valorEstablecido="O" propiedadFormulario="tipoDocumentoRecibio" deshabilitado="true" >Oficial</midas:radio>
	            <midas:radio valorEstablecido="P" propiedadFormulario="tipoDocumentoRecibio" deshabilitado="true">Particular</midas:radio>
            </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.detallar"/>:</th>
            <td colspan="3" ><midas:texto id="detallar" propiedadFormulario="detallar" deshabilitado="true"/></td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.numeroExpediente"/>:</th>
            <td width="16%">
            	<midas:texto id="numeroExpediente" propiedadFormulario="numeroExpediente" deshabilitado="true"/>
            </td>
            <td width="14%"><midas:mensaje clave="siniestro.reporte.preguEspecial.numeroOficio"/></td>
            <td width="51%">
            	<midas:texto id="numeroOficio" propiedadFormulario="numeroOficio" deshabilitado="true"/>
            </td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.lugarHizoOficio"/>:</th>
            <td>
            	<midas:texto id="" propiedadFormulario="lugarHizoOficio" deshabilitado="true"/>
            </td>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.nombreDependencia"/>:</th>
            <td>
            	<midas:texto id="nombreDependencia" propiedadFormulario="nombreDependencia" deshabilitado="true"/>
            </td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.personaRealizoOficio"/>: </th>
            <td>
            	<midas:texto id="personaRealizoOficio" propiedadFormulario="personaRealizoOficio"  deshabilitado="true"/>
            </td>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.descripcionPuesto"/>: </th>
            <td>
            	<midas:texto id="descripcionPuesto" propiedadFormulario="descripcionPuesto" deshabilitado="true"/>
            </td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.causaReclamacion"/>: </th>
            <td colspan="3">
            	<midas:areatexto propiedadFormulario="causaReclamacion" renglones="5" columnas="100" deshabilitado="true"/>
            </td>
            </tr>
          <tr>
            <th>
	            <midas:mensaje clave="siniestro.reporte.preguEspecial.tienePlazoReclamacion"/>: 
	        </th>
	        <td>
	            <midas:radio valorEstablecido="1" propiedadFormulario="tienePlazoReclamacion" deshabilitado="true">Si</midas:radio>
	            <midas:radio valorEstablecido="0" propiedadFormulario="tienePlazoReclamacion" deshabilitado="true">No</midas:radio>
	  		</td>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.plazoReclamacion"/>:</th>
            <td>
    			<midas:texto id="plazoReclamacion" propiedadFormulario="plazoReclamacion" deshabilitado="true"/>
            </td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.estaCubierto"/>:</th>
            <td colspan="3">
                <midas:radio valorEstablecido="1" propiedadFormulario="estaCubierto" deshabilitado="true">Si</midas:radio>
	            <midas:radio valorEstablecido="0" propiedadFormulario="estaCubierto" deshabilitado="true">No</midas:radio>
            </td>
            </tr>
          <tr>
            <th>
            	<midas:mensaje clave="siniestro.reporte.preguEspecial.serviciosProfesionales"/>:
            </th>
            <td>
            	<midas:radio valorEstablecido="S" propiedadFormulario="serviciosProfesionales" deshabilitado="true">Servicios asistencia legal</midas:radio>
	        </td>
            <td colspan="2">
				<midas:radio valorEstablecido="A" propiedadFormulario="serviciosProfesionales" deshabilitado="true">Abogado propio</midas:radio>
			</td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.comentario"/>: </th>
            <td colspan="3">
            	<midas:areatexto propiedadFormulario="comentario" renglones="5" columnas="100" deshabilitado="true"/>
            </td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</midas:formulario>
