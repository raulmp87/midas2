<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/curpRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/siniestro/cabina/reporteGuardarEspeciales">
<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestroPreguntas"/>

<table width="100%" border="0" cellspacing="15" cellpadding="2">
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="desplegar">
      <tr>
      	<td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.noPoliza"/>:</th>
        <td>
       	  <midas:texto id="noPoliza" propiedadFormulario="noPoliza" longitud="20"/>
       	</td>
       	<th><midas:mensaje clave="siniestro.reporte.preguEspecial.noCertificado"/>:</th>
       	<td><midas:texto id="noCertificado" propiedadFormulario="noCertificado" longitud="20"/></td>
       	<td>&nbsp;</td>
      </tr>
      <tr>
      	<td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.nomFuncionario"/>: </th>
        <td>
        	<midas:texto id="nomFuncionario" propiedadFormulario="nomFuncionario" longitud="70"/>        
        </td>
        
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.correo"/>: </th>
        <td>
        	<midas:texto id="correo" propiedadFormulario="correo" longitud="50"/>        
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
       <td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.preguEspecial.telefono"/>:</th>
        <td>
        	<midas:texto id="telefono" propiedadFormulario="telefono" longitud="50"/>        
        </td>
        <th>Calle:</th>
        <td>
        	<midas:texto id="direccionFuncionario" propiedadFormulario="direccionFuncionario" longitud="50"/>        
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.idEstado"/></th>
        <td>
        	<midas:estado styleId="idEstadoPreguntas" size="1" propiedad="idEstado" pais="PAMEXI" styleClass="cajaTexto"
				onchange="limpiarObjetos('idCiudadPreguntas,idColoniaPreguntas'); getCiudades(this,'idCiudadPreguntas');" />
        </td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.idCiudad"/></th>
        <td>
        	<midas:ciudad  styleId="idCiudadPreguntas" size="1" propiedad="idCiudad" estado="idEstado" styleClass="cajaTexto" 
				onchange="getColonias(this,'idColoniaPreguntas');" />
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.idColonia"/></th>
        <td>
        	<midas:colonia styleId="idColoniaPreguntas" size="1" propiedad="idColonia" ciudad="idCiudad" 
        		styleClass="cajaTexto" onchange="setCodigoPostalSiniestro(this.value,'codigoPostalPreguntas');"/>
        </td>
        <th><midas:mensaje clave="siniestro.reporte.repoSini.CP"/></th>
        <td>
        	<midas:texto id="codigoPostalPreguntas" propiedadFormulario="codigoPostal" onkeypress="return soloNumeros(this, event, false)" 
				onchange="if (this.value !== '')getColoniasPorCP(this.value, 'idColoniaPreguntas','idCiudadPreguntas','idEstadoPreguntas');"/>
		</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="10" cellpadding="0" id="desplegar">
          <tr>
            <th width="19%"><midas:mensaje clave="siniestro.reporte.preguEspecial.dependenciaTrabaja"/>:</th>
            <td colspan="4">
            	<midas:texto id="dependenciaTrabaja" propiedadFormulario="dependenciaTrabaja" longitud="100"/>
            </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.cargoOcupa"/>: </th>
            <td colspan="4">
            	<midas:texto id="cargoOcupa" propiedadFormulario="cargoOcupa" longitud="100"/>
            </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.tipoCobertura"/>: </th>
            <td colspan="4">
	            <midas:radio valorEstablecido="B" propiedadFormulario="tipoCobertura">B&aacute;sica</midas:radio>
	            <midas:radio valorEstablecido="P" propiedadFormulario="tipoCobertura">Potenciada</midas:radio>
	        </td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.fechaNotificacion"/></th>
            <td>
                <midas:texto id="fechaNotificacion" propiedadFormulario="fechaNotificacion"  longitud="12" soloLectura="true" />
            </td>
            <td colspan="3" align="left"><image src="../img/b_calendario.gif" border=0 /></td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.tipoDocumentoRecibio"/>:</th>
            <td colspan="4">
            	<midas:radio valorEstablecido="O" propiedadFormulario="tipoDocumentoRecibio">Oficial</midas:radio>
	            <midas:radio valorEstablecido="P" propiedadFormulario="tipoDocumentoRecibio">Particular</midas:radio>
            </td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <th colspan="4" align="left"><midas:mensaje clave="siniestro.reporte.preguEspecial.detallar"/>:<midas:texto id="detallar" propiedadFormulario="detallar" longitud="20"/></th>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.numeroExpediente"/>:</th>
            <td width="20%">
            	<midas:texto id="numeroExpediente" propiedadFormulario="numeroExpediente" longitud="20"/>
            </td>
            <th width="14%"><midas:mensaje clave="siniestro.reporte.preguEspecial.numeroOficio"/></th>
            <td width="20%">
            	<midas:texto id="numeroOficio" propiedadFormulario="numeroOficio" longitud="20"/>
            </td>
            <td width="27%">&nbsp;</td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.lugarHizoOficio"/>:</th>
            <td>
            	<midas:texto id="" propiedadFormulario="lugarHizoOficio" longitud="20"/>
            </td>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.nombreDependencia"/>:</th>
            <td colspan="2">
            	<midas:texto id="nombreDependencia" propiedadFormulario="nombreDependencia" longitud="20"/>
            </td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.personaRealizoOficio"/>: </th>
            <td>
            	<midas:texto id="personaRealizoOficio" propiedadFormulario="personaRealizoOficio" longitud="20"/>
            </td>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.descripcionPuesto"/>: </th>
            <td colspan="2">
            	<midas:texto id="descripcionPuesto" propiedadFormulario="descripcionPuesto" longitud="20"/>
            </td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.causaReclamacion"/>: </th>
            <td colspan="4">
            	<midas:areatexto propiedadFormulario="causaReclamacion" renglones="5" columnas="100"/>
            </td>
            </tr>
          <tr>
            <th>
	            <midas:mensaje clave="siniestro.reporte.preguEspecial.tienePlazoReclamacion"/>: 
	        </th>
	        <td>
	            <midas:radio valorEstablecido="1" propiedadFormulario="tienePlazoReclamacion">Si</midas:radio>
	            <midas:radio valorEstablecido="0" propiedadFormulario="tienePlazoReclamacion">No</midas:radio>
	  		</td>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.plazoReclamacion"/>:</th>
            <td>
            	<midas:texto id="plazoReclamacion" propiedadFormulario="plazoReclamacion"  longitud="12" soloLectura="true" />
            	
            </td>
             <td align="left"><image src="../img/b_calendario.gif" border=0 /></td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.estaCubierto"/>:</th>
            <td colspan="4">
                <midas:radio valorEstablecido="1" propiedadFormulario="estaCubierto">Si</midas:radio>
	            <midas:radio valorEstablecido="0" propiedadFormulario="estaCubierto">No</midas:radio>
            </td>
            </tr>
          <tr>
            <th>
            	<midas:mensaje clave="siniestro.reporte.preguEspecial.serviciosProfesionales"/>:
            </th>
           <td colspan="4">
            	<midas:radio valorEstablecido="S" propiedadFormulario="serviciosProfesionales">Servicios asistencia legal</midas:radio>
            	&nbsp;&nbsp;
				<midas:radio valorEstablecido="A" propiedadFormulario="serviciosProfesionales">Abogado propio</midas:radio>
			</td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.preguEspecial.comentario"/>: </th>
            <td colspan="4">
            	<midas:areatexto propiedadFormulario="comentario" renglones="5" columnas="100"/>
            </td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
    	<table width="15%">
    		<tr>
    			<td>
    			<midas:boton onclick="guardarPreguntasEspecialesSiniestro(document.preguntaespecialForm)" tipo="guardar"/>
    			</td>
    		</tr>
    	</table>
    </td>
  </tr>
</table>
</midas:formulario>
