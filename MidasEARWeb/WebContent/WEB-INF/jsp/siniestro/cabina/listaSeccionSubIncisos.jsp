<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<div style="width: 100%;height:150px; border: 1px, solid; overflow: auto;" class="tableContainer">
	<table style="white-space: normal;" class="tablaConResultados">
		<thead>
			<tr>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.subInciso" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.subIncisoDescripcion" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.nombreComercial" />
				</th>
				<th>
					&nbsp;
				</th>
			</tr>
		</thead>
		<logic:empty name="datospolizaForm" property="listaSeccionSubInciso">
			<tr>
				<td class="datoTabla" align="center" colspan="5">
					<midas:mensaje clave="lista.vacia" />
				</td>
			</tr>
		</logic:empty>
		<logic:notEmpty name="datospolizaForm" property="listaSeccionSubInciso">
			<logic:iterate name="datospolizaForm" property="listaSeccionSubInciso" id="lista" indexId="indice">
				<tr>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="numeroSubInciso" nombre="lista" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="descripcionSubInciso" nombre="lista" />
					</td>
<%--					<td class="datoTabla" align="left">--%>
<%--						<midas:escribe propiedad="idToSeccion" nombre="lista"/>--%>
<%--					</td>--%>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="nombreComercialSeccion" nombre="lista" />
					</td>
					<td class="datoTabla" align="left">
						<input type="checkbox" " name="subIncisoSeccion" value="<midas:escribe propiedad="numeroSubInciso" nombre="lista"/>,<midas:escribe propiedad="idToSeccion" nombre="lista"/>" />
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</table>
</div>