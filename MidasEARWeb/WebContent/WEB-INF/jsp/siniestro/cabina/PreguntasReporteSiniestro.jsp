<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="">
<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
<html:hidden property="numeroReporte" styleId="numeroReporte"/>
<html:hidden property="fechaReporte" styleId="fechaReporte"/>
<html:hidden property="horaReporte" styleId="horaReporte"/>

	<table  id="agregar" >
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="siniestro.cabina.crearReporte" />				
			</td>			
		</tr>		
		<tr>
			<th align="left">																			
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte" />:				
				<span id="numeroReporteDiv"><midas:mensaje clave="siniestro.cabina.reportesiniestro.sinAsignar" /></span>
			</th>
			
			<td colspan="2"></td>
			<td>
				<table >
					<tr>
						<th align="right">
							<midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaReporte" />:			 						
						</th>
						<td>
							<midas:escribe propiedad="fechaReporte" nombre="reporteSiniestroForm"/>
						</td>
					</tr>
					<tr>
							
						<th align="right">
							<midas:mensaje clave="siniestro.cabina.reportesiniestro.horaReporte" />: 									
							
						</th>
						<td>
							<midas:escribe propiedad="horaReporte" nombre="reporteSiniestroForm"/>
						</td>						
					</tr>
				</table>			
			</td>			
				
			
		</tr> 		
			<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.datosPersonaReporta" />
				</th>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="nombrePersonaReporta" requerido="si"
						key="siniestro.cabina.reportesiniestro.nombrePersonaReporta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:texto id="nombrePersonaReporta" propiedadFormulario="nombrePersonaReporta"
					onkeypress="return soloLetras(this, event, false)" 
					onblur="if (this.value !== '' && $('telefonoPersonaReporta').value !== '')
								crearReporteSiniestro($('idToReporteSiniestro').value,this.value,$('telefonoPersonaReporta').value,$('fechaReporte').value,$('horaReporte').value);"
				 	/>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="telefonoPersonaReporta" requerido="si"
					key="siniestro.cabina.reportesiniestro.telefonoPersonaReporta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:texto id="telefonoPersonaReporta" propiedadFormulario="telefonoPersonaReporta"
					onkeypress="return soloNumeros(this, event, false)" 
					onblur="if (this.value !== '' && $('nombrePersonaReporta').value !== '')
								crearReporteSiniestro($('idToReporteSiniestro').value,$('nombrePersonaReporta').value,this.value,$('fechaReporte').value,$('horaReporte').value);"/>
				</td>
				<td></td>
			</tr>
			<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.datosPoliza" />
				</th>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					
				</td>
			</tr>		
			<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.datosSiniestro" />
				</th>
			</tr>							
			<tr>
				<td>
					<etiquetas:etiquetaError property="idEstado" requerido="no"
					key="siniestro.cabina.reportesiniestro.estado" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td>
					<midas:estado styleId="idEstado" size="1" propiedad="idEstado"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('idCiudad,idColonia'); getCiudades(this,'idCiudad');" />
				</td>
				<td colspan="2"></td>
			</tr>						
			<tr>
				<td>
					<etiquetas:etiquetaError property="idCiudad" requerido="no"
					key="siniestro.cabina.reportesiniestro.ciudad" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td>
					<midas:ciudad styleId="idCiudad" size="1" propiedad="idCiudad"
					estado="idEstado" styleClass="cajaTexto" onchange="getColonias(this,'idColonia')" />				
				</td>
				<td colspan="2"></td>				
			</tr>			
			<tr>
				<td>
					<etiquetas:etiquetaError property="idColonia" requerido="no"
					key="siniestro.cabina.reportesiniestro.colonia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:colonia styleId="idColonia" size="1" propiedad="idColonia"
					ciudad="idCiudad" styleClass="cajaTexto"/>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="calle" requerido="no"
						key="siniestro.cabina.reportesiniestro.calle" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:texto propiedadFormulario="calle" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="fechaSiniestro" requerido="no"
						key="siniestro.cabina.reportesiniestro.fechaSiniestro" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td>
					<midas:texto id="fechaSiniestro" propiedadFormulario="fechaSiniestro" caracteres="10" longitud="25" 
								onkeypress="return soloFecha(this, event, false);"
								onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)" />
						<img src="<html:rewrite page='/img/b_calendario.gif'/>"
							width="12" height="12" style="cursor:hand;"
							onclick="getCalendarioSiniestros(event,'fechaSiniestro');" />								
				</td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="nombreContacto" requerido="no"
						key="siniestro.cabina.reportesiniestro.nombreContacto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:texto propiedadFormulario="nombreContacto" 
					onkeypress="return soloLetras(this, event, false)"/>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="telefonoContacto" requerido="no"
						key="siniestro.cabina.reportesiniestro.telefonoContacto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:texto propiedadFormulario="telefonoContacto" 
					onkeypress="return soloNumeros(this, event, false)"/>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="descripcionSiniestro" requerido="no"
						key="siniestro.cabina.reportesiniestro.descripcionSiniestro" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:areatexto propiedadFormulario="descripcionSiniestro" renglones="5" columnas="50"/>
				</td>			
				<td></td>	
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="descripcionEventoCatastrofico" requerido="no"
						key="siniestro.cabina.reportesiniestro.descripcionEventoCatastrofico" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:areatexto propiedadFormulario="descripcionEventoCatastrofico" renglones="5" columnas="50"/>
				</td>			
				<td></td>	
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="observaciones" requerido="no"
						key="siniestro.cabina.reportesiniestro.observaciones" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td colspan="2">
					<midas:areatexto propiedadFormulario="observaciones" renglones="5" columnas="50"/>
				</td>			
				<td></td>	
			</tr>
			<tr>
				<th>
					<etiquetas:etiquetaError property="idCoordinador" requerido="no"
						key="siniestro.cabina.reportesiniestro.nombreCoordinador" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</th>
				<td></td>
				<td colspan="2"></td>																
			</tr>
			<tr>
				<th class="seccion" colspan="4">
					<midas:mensaje clave="siniestro.cabina.asignacionAjustador" />
				</th>
			</tr>
			<tr>
				<td colspan="4"></td>
				
			</tr>			
	<tr>
		<td class="campoRequerido"  colspan="2">
	 		<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
		</td> 
		<td></td>
		<td class="guardar">
			<div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: regresarListaReportes();"><midas:mensaje clave="midas.accion.regresar"/></a>
				</div>					
				<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: if(validarFechas('','',$('fechaSiniestro').value,'del siniestro',$('fechaReporte').value,'del reporte')) 
															sendRequest(document.reporteSiniestroForm,'/MidasWeb/siniestro/cabina/guardarReporte.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.guardar"/>
					</a>									
				</div>
			</div>
		</td> 
	</tr>
</table>

</midas:formulario>
