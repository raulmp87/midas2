<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/correo/configCorreoSiniestro.js"/>"></script> 
 
 <table id="desplegarDetalle"> 
 	<tr>
 		<td class="titulo" colspan="4">
			<midas:mensaje clave="catalogos.configuracion.correo.ajustador" />
		</td>
	</tr>  
	<tr>
		<td width="100%">
			<div id="configCorreosAjustadorGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
</table>


