<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/correo/configCorreoSiniestro.js"/>"></script> 
 
 <table id="desplegarDetalle"> 
 	<tr>
 		<td width="100%">
 			<html:form action="/siniestro/cabina/correo/guardar.do" method="post" styleId="f_configTipoNegocio">
				<table width="71.5%" id="filtros">
					<tr>
						<td width="20%"></td>
						<td width="30%"></td>
						<td width="20%"></td>
						<td width="30%"></td>
					</tr>
					<tr>
						<td class="titulo" colspan="4">
							<midas:mensaje clave="catalogos.configuracion.correo.tipoNegocio" />
						</td>
					</tr>
				    <tr>
				    	<td>	
				    		<span id="form_tipoNegocio_span" title="Tipo de Negocio es requerido.">
				    			<div class="normal" id="d_etiquetaTipoNegocio">
				    				<div class="etiqueta">
				    					<img src="/MidasWeb/img/information.gif" style="display:none"/>
				    					<midas:mensaje clave="catalogos.configuracion.correo.tipoNegocio.tipoNegocio" />:
				    					<div></div>
									</div>
								</div>
							</span>
				    		
				    	</td>
						<td> 
				            <html:select property="idTcTipoNegocio" name="configCorreoTipoNegocioForm" styleId="s_tiposNegocio" styleClass="cajaTexto"  onchange="javascript:mostrarCorreosTipoNegocioFiltrado($('s_tiposNegocio'));">
				                 <html:option value="0">TODOS...</html:option>
								 <html:options collection="tiposNegocio" property="idTcTipoNegocio" labelProperty="description" />			
				            </html:select> 		
				    	</td>
				    	<td colspan="2"></td>
				    </tr>
				    <tr>
				    	<td>
				    		<span id="form_email_span" title=" ">
				    			<div class="normal" id="d_etiquetaCorreo">
				    				<div class="etiqueta">
				    					<img src="/MidasWeb/img/information.gif" style="display:none"/>
				    					<midas:mensaje clave="catalogos.configuracion.correo.tipoNegocio.correo" />:
				    					<div></div>
									</div>
								</div>
							</span>
						</td>       
						<td>
						    <midas:texto id="txt_correo" propiedadFormulario="correo"/>
						</td>
						<td class= "guardar" colspan="2">
							<div class="alinearBotonALaDerecha">
								<div id="b_guardar">
									<a href="javascript: void(0);"
									onclick="javascript: guardarConfigTipoNegocio();">
									<midas:mensaje clave="midas.accion.guardar"/>
									</a>
								</div>
							</div>
						</td> 
				    </tr>
				 </table>
			 </html:form>
		</td>
 	</tr>  
	<tr>
		<td width="100%">
			<div id="configCorreosTipoNegocioGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
	<tr>
		<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
		</td> 
	</tr>
</table>


