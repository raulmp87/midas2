<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script type="text/javascript" src="<html:rewrite page="/js/siniestro/cabina/correo/configCorreoSiniestro.js"/>"></script> 
 
 <table id="desplegarDetalle"> 
 	<tr>
 		<td width="100%">
 			<html:form action="/siniestro/cabina/correo/mostrarConfigCorreoPoliza.do" method="post" styleId="f_configPoliza">
				<table width="71.5%" id="filtros">
					<tr>
						<td width="20%"></td>
						<td width="30%"></td>
						<td width="20%"></td>
						<td width="30%"></td>
					</tr>
					<tr>
						<td class="titulo" colspan="4">
							<midas:mensaje clave="catalogos.configuracion.correo.poliza" />
						</td>
					</tr>
				    <tr>
				    	<td>
				    		<span id="form_numeroPoliza_span" title=" ">
				    			<div class="normal" id="d_etiquetaNumeroPoliza">
				    				<div class="etiqueta">
				    					<img src="/MidasWeb/img/information.gif" style="display:none"/>
				    					<midas:mensaje clave="catalogos.configuracion.correo.poliza.numeroPoliza" />:
				    					<div></div>
									</div>
								</div>
							</span>
						</td>   
						<td>
				            <midas:texto id="numeroPolizaBuscar" propiedadFormulario="numeroPolizaBuscar" onblur="javascript:mostrarCorreosPolizaFiltrado($('numeroPolizaBuscar'));" onfocus="new Mask('####-########-##', 'string').attach(this)" />	
				    	</td>
				    	<td colspan="2">	
				    	</td>
				    </tr>
				    <tr>
				    	<td>
				    		<span id="form_email_span" title=" ">
				    			<div class="normal" id="d_etiquetaCorreo">
				    				<div class="etiqueta">
				    					<img src="/MidasWeb/img/information.gif" style="display:none"/>
				    					<midas:mensaje clave="catalogos.configuracion.correo.poliza.correo" />:
				    					<div></div>
									</div>
								</div>
							</span>
						</td>         
						<td>
						    <midas:texto id="txt_correo" propiedadFormulario="correo" />
						</td>
						<td class= "buscar" colspan="2">
							<div class="alinearBotonALaDerecha">
								<div id="b_buscar">
									<a href="javascript: void(0);"
									onclick="javascript: guardarConfigPoliza();">
									<midas:mensaje clave="midas.accion.guardar"/>
									</a>
								</div>
							</div>
						</td> 
				    </tr>
				 </table>
			 </html:form>
		</td>
 	</tr>  
	<tr>
		<td width="100%">
			<div id="configCorreosPolizaGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
</table>


