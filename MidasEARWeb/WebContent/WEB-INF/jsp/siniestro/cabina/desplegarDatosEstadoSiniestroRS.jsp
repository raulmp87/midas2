<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/cabina/desplegarDatosEstadoSiniestro">
<table id="desplegar" width="50%" border="0">
  <tr>
    <th class="seccion" colspan="4"><midas:mensaje clave="siniestro.reporte.estadosinis.fechaSiniestro"/></th>
  </tr>
  <tr>
  	<td width="6%">&nbsp;</td>
  	<th width="25%">
  		<midas:mensaje clave="siniestro.reporte.estadosinis.fechaAsignacion"/>:
  	</th>
    <td width="25%">
   		<midas:texto id="fechaAsignacion" propiedadFormulario="fechaAsignacion" deshabilitado="true"/>
	</td>
	<td width="44%">&nbsp;</td>
   </tr>
   <tr>
   	 <td>&nbsp;</td>
     <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaContactoAjustador"/>:</th>
     <td>
     	<midas:texto id="fechaContactoAjustador" propiedadFormulario="fechaContactoAjustador" deshabilitado="true"/>
     </td>
     <td>&nbsp;</td>
   </tr>
   <tr>
   	 <td>&nbsp;</td>
     <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaLlegadaAjustador"/>:</th>
     <td>
     	<midas:texto id="fechaLlegadaAjustador" propiedadFormulario="fechaLlegadaAjustador" deshabilitado="true"/>
     </td>
     <td>&nbsp;</td>
   </tr>
   <tr>
   	<td>&nbsp;</td>
   	<th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaInspeccionAjustador"/>:</th>
    <td>
   		<midas:texto id="fechaInspeccionAjustador" propiedadFormulario="fechaInspeccionAjustador" deshabilitado="true"/>
    </td>
    <td>&nbsp;</td>
   </tr>
   <tr>
   	<td>&nbsp;</td>
    <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaPreliminar"/>:</th>
    <td>
    	<midas:texto id="fechaPreliminar" propiedadFormulario="fechaPreliminar" deshabilitado="true"/>
    </td>
    <td>&nbsp;</td>
   </tr>
   <tr>
   	 <td>&nbsp;</td>
     <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaDocumento"/>:</th>
     <td>
     	<midas:texto id="fechaDocumento" propiedadFormulario="fechaDocumento" deshabilitado="true"/>
     </td>
     <td>&nbsp;</td>
   </tr>
   <tr>
   	 <td>&nbsp;</td>
     <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaInformeFinal"/>:</th>
     <td>
     	<midas:texto id="fechaInformeFinal" propiedadFormulario="fechaInformeFinal" deshabilitado="true"/>
     </td>
     <td>&nbsp;</td>
   </tr>
   <tr>
   	 <td>&nbsp;</td>
     <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaTerminacionSiniestro"/>:</th>
     <td>
     	<midas:texto id="fechaTerminacionSiniestro" propiedadFormulario="fechaTerminacionSiniestro" deshabilitado="true"/>
     </td>
     <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <th><midas:mensaje clave="siniestro.reporte.estadosinis.fechaCerrado"/>:</th>
      <td>
      	<midas:texto id="fechaCerrado" propiedadFormulario="fechaCerrado" deshabilitado="true"/>
     </td>
     <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <th><midas:mensaje clave="siniestro.reporte.estadosinis.comentarios"/>:</th>
      <td>
       <midas:areatexto propiedadFormulario="descripcionSiniestro" renglones="8" columnas="40"  deshabilitado="true"/>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">
	    <table id="desplegar">
	      <tr>
		        <td width="6%"><midas:mensaje clave="siniestro.reporte.estadosinis.cabinero"/>:</td>
		        <td width="38%">
		        	<midas:texto id="descripcionCabinero" propiedadFormulario="descripcionCabinero" deshabilitado="true"/>
		        </td>
		        <td width="8%"><midas:mensaje clave="siniestro.reporte.estadosinis.terminoAjuste"/>: </td>
		        <td width="48%"><midas:texto id="terminoAjuste" propiedadFormulario="terminoAjuste" deshabilitado="true"/></td>
	       </tr>
	       <tr>
	       		<td width="10%"><midas:mensaje clave="siniestro.finanzas.terminarSiniestro.estatusSiniestro" />:</td>
		        <td width="40%">
		        	<midas:texto id="descripcionEstatusSiniestro" propiedadFormulario="descripcionEstatusSiniestro" deshabilitado="true"/>
		        </td>
		        <td width="10%"><midas:mensaje clave="siniestro.finanzas.terminarSiniestro.subEstatusSiniestro" />:</td>
		        <td width="40%">
		        	<midas:texto id="descripcionSubEstatusSiniestro" propiedadFormulario="descripcionSubEstatusSiniestro" deshabilitado="true"/>
		        </td>
	       </tr>
	      </table>  
      </td>
    </tr>
 </table>
</midas:formulario>