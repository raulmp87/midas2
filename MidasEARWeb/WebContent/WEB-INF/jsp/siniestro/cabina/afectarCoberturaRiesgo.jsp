<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/poliza/afectarCoberturaRiesgo">
	<html:hidden property="idToPoliza" styleId="idToPolizaCR"/>
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestroCR"/>
	<html:hidden property="seleccionados" styleId="idSeleccionadosCR"/>
	<html:hidden property="analisisInterno" styleId="idAnalisisInterno"/>
	
	<html:hidden property="polizaNumeroEndoso" styleId="idPolizaNumeroEndosoCR"/>
	<html:hidden property="numeroInciso" styleId="idNumeroIncisoCR"/>
	<html:hidden property="pantallaOrigen" styleId="pantallaOrigen"/>
	
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.titulo" />				
			</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.numeroPoliza" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="numeroPoliza" nombre="coberturaRiesgoForm"/>
			</td>
			<td colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">
				<table width="100%" cellspacing="8" id="desplegar" border="0">
					<tr>
						<th width="7%"><midas:mensaje clave="siniestro.cabina.reportesiniestro.estado"/></th>
						<td width="23%">
							<midas:estado styleId="idEstadoBuscarRiesgos" size="1" propiedad="idEstadoBuscar"
							pais="PAMEXI" styleClass="cajaTexto"
							onchange="limpiarObjetos('idCiudadBuscar,idColoniaBuscar'); getCiudades(this,'idCiudadBuscarRiesgos');" />
						</td>
						<th width="10%"><midas:mensaje clave="siniestro.cabina.reportesiniestro.ciudad"/></th>
						<td width="20%">
							<midas:ciudad styleId="idCiudadBuscarRiesgos" size="1" propiedad="idCiudadBuscar"
							estado="idEstadoBuscar" styleClass="cajaTexto" onchange="getColonias(this,'idColoniaBuscarRiesgos');" />				
						</td>
						<th width="10%"><midas:mensaje clave="siniestro.cabina.reportesiniestro.colonia"/></th>
						<td width="30%">
								<midas:colonia styleId="idColoniaBuscarRiesgos" size="1" propiedad="idColoniaBuscar"
											ciudad="idCiudadBuscar" styleClass="cajaTexto"
											onchange="setCodigoPostalSiniestro(this.value,'codigoPostalBuscarRiesgos');"/>
						</td>
					</tr>
					<tr>
						<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.cp"/></th>
						<td>
							<midas:texto longitud="20" id="codigoPostalBuscarRiesgos" propiedadFormulario="codigoPostalBuscar"
											onkeypress="return soloNumeros(this, event, false)"
											onchange="if (this.value !== '')getColoniasPorCP(this.value, 'idColoniaBuscarRiesgos','idCiudadBuscarRiesgos','idEstadoBuscarRiesgos');"/>
									
						</td>
						<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.calle"/></th>
						<td><midas:texto longitud="20" id="calleBuscar" propiedadFormulario="calleBuscar"/></td>
						<td colspan="2"><midas:boton onclick="muestraListarCoberturasRiesgoPorUbicacion();" tipo="buscar"  /></td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="listIncisosRiesgosAfectados"></div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div id="listSeccionSubIncisosRiesgosAfectados"></div>
						</td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
						<td align="right">
							<div id="idBotonBuscaRiesgosAfectados" style="display:none;">
								<midas:boton tipo="buscar" onclick="muestraListaCoberturasRiesgoAAfectar(document.coberturaRiesgoForm);" texto="Buscar"/>
							</div>
						</td>
					</tr>
<!--					<tr>-->
<!--						<td colspan="7">-->
<!--							<div id="listSeccionSubIncisos"></div>-->
<!--						</td>-->
<!--						<td valign="bottom">-->
<!--							<div id="idBotonBuscaRiesgosAfectados" style="display:none;">-->
<!--								<midas:boton tipo="buscar" onclick="muestraListaCoberturasRiesgoAAfectar(document.coberturaRiesgoForm);" texto="Buscar"/>-->
<!--							</div>-->
<!--						</td>-->
<!--					</tr>-->
					</tr>
					<tr>
						<td colspan="7">
							<div id="listCoberturasRiesgoAfectar"></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td  align="center">
<%--				<logic:notEqual name="coberturaRiesgoForm" property="analisisInterno" value="1">--%>
				<logic:equal name="coberturaRiesgoForm" property="permiteModificar" value="false">
					<midas:boton onclick="guardarAfectacionCoberturaRiesgo(document.coberturaRiesgoForm)" tipo="guardar" texto="Guardar afectaci&oacute;n" style="width:140px;"/>
				</logic:equal>
<%--				</logic:notEqual>--%>
			</td>
			<td >
				<midas:boton onclick="cancelarCoberturaRiesgo();" tipo="regresar" texto="Cancelar" style="width:75px;"/>
			</td>
			<td colspan="2" >&nbsp;</td>
		</tr>
		
 	</table>
</midas:formulario>
