<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<div style="width: 100%;height:150px; border: 1px, solid; overflow: auto;" class="tableContainer">
	<table style="white-space: normal;" class="tablaConResultados">
		<thead>
			<tr>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.subInciso" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.seccion" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.cobertura" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.riesgo" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.primaNeta" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.deducible" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.coaseguro" />
				</th>
				<th>
					<midas:mensaje clave="siniestro.polizas.coberturaRiesgo.sumaAsegurada" />
				</th>
			</tr>
		</thead>
		<logic:empty name="datospolizaForm" property="listaCoberturasRiesgo">
			<tr>
				<td class="datoTabla" align="center" colspan="6">
					<midas:mensaje clave="lista.vacia" />
				</td>
			</tr>
		</logic:empty>
		<logic:notEmpty name="datospolizaForm" property="listaCoberturasRiesgo">
			<logic:iterate name="datospolizaForm" property="listaCoberturasRiesgo" id="lista" indexId="indice">
				<tr>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="numeroSubInciso" nombre="lista" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="descripcionSeccion" nombre="lista" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="descripcionCobertura" nombre="lista" />
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="descripcionRiesgo" nombre="lista"/>
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="primaNeta" nombre="lista" formato="$###,###,##0.00"/>
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="deducible" nombre="lista" formato="$###,###,##0.00"/>
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="coaseguro" nombre="lista" formato="$###,###,##0.00"/>
					</td>
					<td class="datoTabla" align="left">
						<midas:escribe propiedad="sumaAsegurada" nombre="lista" formato="$###,###,##0.00"/>
					</td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</table>
</div>