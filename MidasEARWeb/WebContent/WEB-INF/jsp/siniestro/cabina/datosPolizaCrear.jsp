<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<midas:formulario accion="/siniestro/cabina/reportePoliza">

<html:hidden property="polizaNumeroEndoso" styleId="polizaNumeroEndoso"/>
<html:hidden property="polizaIdAgente" styleId="polizaIdAgente"/>
<html:hidden property="polizaNombreAgente" styleId="polizaNombreAgente"/>
<html:hidden property="polizaNombreOficina" styleId="polizaNombreOficina"/>
<html:hidden property="polizaNombreAsegurado" styleId="polizaNombreAsegurado"/>
<html:hidden property="polizaIdCliente" styleId="polizaIdCliente"/>
<html:hidden property="numeroInciso" styleId="idNumeroInciso"/>

<div width="100%" id="policita">
<table width="100%" border="0" cellspacing="12" id="detalle">
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" id="desplegar">
  		<tr>
    		<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" >
      				<tr>
        				<td>
        					<table width="100%" cellspacing="8" id="desplegar" border="0">
          						<tr>
            						<td colspan="5" class="titulo"><midas:mensaje clave="siniestro.reporte.desDatPol.datosPoliza"/></td>
          						</tr>
          						<tr>
					                <td width="25%" align="right">
					                	<midas:mensaje clave="siniestro.reporte.desDatPol.numPoliza"/>:
									</td>
                					<td width="20%">
                  						<midas:texto id="numeroPoliza" propiedadFormulario="numeroPoliza" longitud="20" deshabilitado="true"/>
                					</td>
					                <td width="20%"><midas:mensaje clave="siniestro.reporte.desDatPol.claveProducto"/>:</td>
					                <td width="20%">
					                	<midas:texto id="claveProducto" propiedadFormulario="claveProducto" longitud="20" deshabilitado="true"/>
					                </td>
					                <td width="15%">&nbsp;</td>
              					</tr>
              					<tr>
            						<td colspan="5" class="titulo">Datos del Asegurado </td>
          						</tr>
              					<tr>
					                <td align="right"><midas:mensaje clave="siniestro.reporte.desDatPol.nomAsegurado"/>: </td>
					                <td >
					                	<midas:texto id="polizaNombreAsegurado" propiedadFormulario="polizaNombreAsegurado" longitud="50" deshabilitado="true"/>
					                </td>
					                <td align="right">Telefono: </td>
					                <td colspan="2">
					                	<midas:texto id="telefonoAsegurado" propiedadFormulario="telefonoAsegurado" longitud="50" deshabilitado="true"/>
					                </td>
				                </tr>
              					<tr>
					                <td align="right"><midas:mensaje clave="siniestro.reporte.desDatPol.direcAsegurado"/>: </td>
					                <td colspan="4">
					                	<midas:texto id="direccionAsegurado" propiedadFormulario="direccionAsegurado" deshabilitado="true"/>
					                </td>
				                </tr>
				                <tr>
            						<td colspan="5" class="titulo">Datos del Contratante </td>
          						</tr>
          						<tr>
					                <td align="right">Nombre: </td>
					                <td >
					                	<midas:texto id="nombreContratante" propiedadFormulario="nombreContratante" deshabilitado="true"/>
					                </td>
					                <td align="right">Telefono: </td>
					                <td colspan="2">
					                	<midas:texto id="telefonoContratante" propiedadFormulario="telefonoContratante" deshabilitado="true"/>
					                </td>
				                </tr>
              					<tr>
					                <td align="right">Direccion: </td>
					                <td colspan="4">
					                	<midas:texto id="direccionContratante" propiedadFormulario="direccionContratante" deshabilitado="true"/>
					                </td>
				                </tr>
        				</table>
        			</td>
      			</tr>
      		<tr>
                <td>
<!--                  	<div id="indicadorCargaDetallePoliza"></div>-->
<!--					<div id="detallePolizaGrid" style="width: 100%;"></div>-->
<!--					<div id="pagingAreaDetallePoliza"></div><div id="infoAreaDetallePoliza"></div>-->
					<table width="100%" cellspacing="8" id="desplegar" border="0">
						<tr>
							<th width="7%"><midas:mensaje clave="siniestro.cabina.reportesiniestro.estado"/></th>
							<td width="23%">
								<midas:estado styleId="idEstadoBuscar" size="1" propiedad="idEstadoBuscar"
								pais="PAMEXI" styleClass="cajaTexto"
								onchange="limpiarObjetos('idCiudadBuscar,idColoniaBuscar'); getCiudades(this,'idCiudadBuscar');" />
							</td>
							<th width="10%"><midas:mensaje clave="siniestro.cabina.reportesiniestro.ciudad"/></th>
							<td width="20%">
								<midas:ciudad styleId="idCiudadBuscar" size="1" propiedad="idCiudadBuscar"
								estado="idEstadoBuscar" styleClass="cajaTexto" onchange="getColonias(this,'idColoniaBuscar');" />				
							</td>
							<th width="10%"><midas:mensaje clave="siniestro.cabina.reportesiniestro.colonia"/></th>
							<td width="30%">
									<midas:colonia styleId="idColoniaBuscar" size="1" propiedad="idColoniaBuscar"
												ciudad="idCiudadBuscar" styleClass="cajaTexto"
												onchange="setCodigoPostalSiniestro(this.value,'codigoPostalBuscar');"/>
							</td>
						</tr>
						<tr>
							<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.cp"/></th>
							<td>
								<midas:texto longitud="20" id="codigoPostalBuscar" propiedadFormulario="codigoPostalBuscar"
												onkeypress="return soloNumeros(this, event, false)"
												onchange="if (this.value !== '')getColoniasPorCP(this.value, 'idColonia','idCiudad','idEstado');"/>
										
							</td>
							<th><midas:mensaje clave="siniestro.cabina.reportesiniestro.calle"/></th>
							<td colspan="2"><midas:texto longitud="20" id="calleBuscar" propiedadFormulario="calleBuscar"/></td>
							<td><midas:boton onclick="muestraListarIncisosUbicacionPoliza();" tipo="buscar"  /></td>
						</tr>
						<tr>
							<td colspan="6">
								<div id="listIncisos"></div>
							</td>
						</tr>
						<tr>
							<td colspan="6">
								<div id="listSeccionSubIncisos"></div>
							</td>
							<td valign="bottom">
								<div id="idBotonBuscaCoberturas" style="display:none;"><midas:boton tipo="buscar" onclick="muestraListaCoberturasRiesgoPoliza(document.datospolizaForm);" texto="Buscar"/></div>
							</td>
						</tr>
						</tr>
						<tr>
							<td colspan="6">
								<div id="listCoberturasRiesgo"></div>
							</td>
						</tr>
					
					</table>
                </td>
      </tr>
    </table>
			</td>
  		</tr>
	</table>

	</td>
  </tr>
  <tr>
    <td><table width="100%"  cellspacing="0" bordercolor="#000000">
      <tr>
        <td><table width="100%" border="0" cellspacing="5" cellpadding="0" id="desplegar">
          <tr>
            <td colspan="7" class="seccion"><midas:mensaje clave="siniestro.reporte.desDatPol.datCobranza"/></td>
            </tr>
          <tr>
            <td width="12%"><midas:mensaje clave="siniestro.reporte.desDatPol.tipoMoneda"/>:</td>
            <td width="20%">
            	<midas:texto id="tipoMoneda" propiedadFormulario="tipoMoneda" longitud="20" deshabilitado="true"/>
            </td>
            <td width="10%">&nbsp;</td>
            <td width="12%">&nbsp;</td>
            <td width="20%"><midas:mensaje clave="siniestro.reporte.desDatPol.fecPago"/>:</td>
            <td width="20%">
        		<midas:texto id="fecPago" propiedadFormulario="fecPago" caracteres="10" longitud="25" deshabilitado="true"/>
            </td>
            <td width="6%">&nbsp;</td>
          </tr>
          <tr>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.claveOficina"/>:</td>
            <td colspan="2"><midas:texto id="claveOficina" propiedadFormulario="claveOficina" longitud="20" deshabilitado="true"/></td>
            <td>&nbsp;</td>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.imporPagado"/>:</td>
            <td>
            <midas:texto id="importePagado" propiedadFormulario="importePagado" longitud="20" deshabilitado="true"/>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.claveProducto"/>:</td>
            <td>
            <midas:texto id="claveProducto" propiedadFormulario="claveProducto" longitud="20" deshabilitado="true"/>
            </td>
            <td colspan="2">&nbsp;</td>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.saldoPendiente"/>:</td>
            <td>
            <midas:texto id="saldoPendiente" propiedadFormulario="saldoPendiente" longitud="20" deshabilitado="true"/>
            </td>
             <td>&nbsp;</td>
          </tr>
          <tr>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.formaPago"/>:</td>
            <td colspan="2">
            <midas:texto id="formaPago" propiedadFormulario="formaPago" longitud="20" deshabilitado="true"/>
            </td>
            <td>&nbsp;</td>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.saldoVencido"/>:</td>
            <td>
            <midas:texto id="saldoVencido" propiedadFormulario="saldoVencido" longitud="" deshabilitado="true"/>
            </td>
             <td>&nbsp;</td>
          </tr>
          <tr>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.ultimoReciboPagado"/>:</td>
            <td colspan="2"><midas:texto id="ultimoReciboPagado" propiedadFormulario="ultimoReciboPagado" longitud="20" deshabilitado="true"/></td>
            <td>&nbsp;</td>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.fecEmision"/>:</td>
            <td>
        		<midas:texto id="fecEmision" soloLectura="true" propiedadFormulario="fecEmision" caracteres="10" longitud="25" deshabilitado="true"/>
            </td>
             <td>&nbsp;</td>
          </tr>
          <tr>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.fecVigencia"/>:</td>
            <td>
				<midas:texto id="fecVigencia" deshabilitado="true" propiedadFormulario="fecVigencia" caracteres="10" longitud="25"/>
            </td>
            <td colspan="2">&nbsp;</td>
            <td><midas:mensaje clave="siniestro.reporte.desDatPol.al"/>:</td>
            <td>
        		<midas:texto id="al" deshabilitado="true" propiedadFormulario="al" caracteres="10" longitud="25"/>
            </td>
             <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Estatus Cobranza:</td>
            <td>
				<midas:texto id="estatusCobranza" deshabilitado="true" propiedadFormulario="estatusCobranza" caracteres="10" longitud="25"/>
            </td>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
          	<td colspan="7">
<!--          		<a href="javascript: void(0);" onclick="javascript: sendRequest(document.datospolizaForm,'/MidasWeb/siniestro/cabina/reportePoliza.do', 'contenido',null);"><midas:mensaje clave="siniestro.reporte.estadosinis.guardarReporte"/></a>-->
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar_poliza">
						<a href="javascript: void(0);" onclick="javascript: asignarPoliza('<midas:escribe propiedad="numeroPoliza" nombre="datospolizaForm"/>','<midas:escribe propiedad="codigoTipoNegocio" nombre="datospolizaForm"/>');">Asignar Poliza</a> 
					</div>					
				</div>
          	</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</midas:formulario>