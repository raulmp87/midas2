<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/cabina/reporteTransportista">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestroTransporte"/>
	<table width="100%" border="0" cellspacing="10" cellpadding="0"	id="desplegar">
		<tr>
			<td class="titulo">
				<midas:mensaje
					clave="siniestro.reporte.transportes.datosTransportistaCarga" />:
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="10" cellpadding="0"	id="desplegar">
								<tr>
									<td width="19%">
										<midas:mensaje clave="siniestro.reporte.transportes.nombreRazonSocial" />:
									</td>
									<td colspan="3">
										<midas:texto id="nombreRazonSocial"	propiedadFormulario="nombreRazonSocial" longitud="100" />
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.tipoPersona" />	:
									</td>
									<td width="25%">
										<midas:radio valorEstablecido="F" propiedadFormulario="tipoPersona">F&iacute;sica</midas:radio>
									</td>
									<td width="10%">
										<midas:radio valorEstablecido="M" propiedadFormulario="tipoPersona">Moral</midas:radio>
									</td>
									<td width="30%">
										&nbsp;
									</td>
									<td width="16%">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.estado" />:
									</td>
									<td>
										<midas:estado styleId="idEstadoTransporte" size="1" propiedad="idEstado" 
											pais="PAMEXI" styleClass="cajaTexto"
											onchange="limpiarObjetos('idCiudadTransporte,idColoniaTransporte'); getCiudades(this,'idCiudadTransporte');" />
									</td>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.municipio" />:
									</td>
									<td>
										<midas:ciudad styleId="idCiudadTransporte" size="1"
											propiedad="idCiudad" estado="idEstado" styleClass="cajaTexto"
											onchange="getColonias(this,'idColoniaTransporte')" />
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.colonia" />:
									</td>
									<td>
										<midas:colonia styleId="idColoniaTransporte" size="1"
											propiedad="idColonia" ciudad="idCiudad"	styleClass="cajaTexto"
											onchange="setCodigoPostalSiniestro(this.value,'codigoPostalTransporte');" />
									</td>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.cp" />:
									</td>
									<td>
										<midas:texto id="codigoPostalTransporte"
											propiedadFormulario="cp"
											onkeypress="return soloNumeros(this, event, false)" longitud="20"
											onchange="if (this.value !== '')getColoniasPorCP(this.value, 'idColoniaTransporte','idCiudadTransporte','idEstadoTransporte');" />
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.calle" />:
									</td>
									<td colspan="2">
										<midas:texto id="calle" propiedadFormulario="calle"	longitud="20" />
									</td>
									<td colspan="2">
										&nbsp;
									</td>
								</tr>

								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.telefono" />:
									</td>
									<td>
										<midas:texto id="telefono" propiedadFormulario="telefono" caracteres="20" onkeypress="return soloNumeros(this, event, false);"/>
									</td>
									<td colspan="3">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.rfc_curp" />:
									</td>
									<td>
										<midas:texto id="rfc_curp" propiedadFormulario="rfc_curp" caracteres="29" />
									</td>
									<td colspan="3">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<midas:mensaje clave="siniestro.reporte.transportes.noUnidad" />
									</td>
									<td >
										<midas:texto id="noUnidad" propiedadFormulario="noUnidad" caracteres="120" />
									</td>
									<td colspan="3">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<midas:boton onclick="guardarTransporteSinistros(document.transporteForm);" tipo="guardar"/>
			</td>
		</tr>
	</table>
</midas:formulario>