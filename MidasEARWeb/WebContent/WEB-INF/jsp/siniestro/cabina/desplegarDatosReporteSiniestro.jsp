<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div style= "height: 450px; width:100%; overflow:auto" hrefmode="ajax-html"  id="crearReporteSiniestro" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div id="reporteSiniestro" name="Reporte de Siniestro" >
		<center>
			<midas:formulario accion="/siniestro/cabina/guardarReporte">
				<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
				<html:hidden property="numeroReporte" styleId="numeroReporte"/>
				<html:hidden property="numeroPoliza" styleId="numeroPoliza"/>
				<html:hidden property="tipoNegocio" styleId="idTipoNegocio"/>
				<html:hidden property="idToPoliza" styleId="idToPoliza"/>
				<html:hidden property="productoTransporte" styleId="productoTransporte"/>
				
				<table id="desplegar" border="0" >
					<tr>
						<td width="6%">&nbsp;</td>
						<td width="18%" align="left"><strong><midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte" />:</strong><em id="numeroReporteDiv"> <midas:escribe propiedad="numeroReporte" nombre="reporteSiniestroForm"/></em></td>
						<td width="25%">&nbsp;</td>
						<td width="10%">&nbsp;</td>
						<td width="15%" align="right"><strong><midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaReporte" />:</strong> <midas:escribe propiedad="fechaReporte" nombre="reporteSiniestroForm"/></td>
						<td width="25%"align="left"><strong><midas:mensaje clave="siniestro.cabina.reportesiniestro.horaReporte" />:</strong> <midas:escribe propiedad="horaReporte" nombre="reporteSiniestroForm"/></td>
						<td width="4%">&nbsp;</td>
					</tr>
<!--Datos de la persona que  reporta	-->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosPersonaCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccion" colspan="7">
											<midas:mensaje clave="siniestro.cabina.datosPersonaReporta" />
										</th>
									</tr>
									<tr>
										<td width="6%">&nbsp;</td>
										<th width="15%" align="left">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.nombrePersonaReporta" />
										</th>
										<td width="25%" colspan="3" >
											<midas:texto id="nombrePersonaReporta" propiedadFormulario="nombrePersonaReporta" deshabilitado="true" />
										</td>
										<td colspan="2" >&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th align="left">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.telefonoPersonaReporta" />
										</th>
										<td width="25%" >
											<midas:texto id="telefonoPersonaReporta" propiedadFormulario="telefonoPersonaReporta" deshabilitado="true" />
										</td>
										<td width="10%" >&nbsp;</td>
										<td width="15%" >&nbsp;</td>
										<td width="25%" >&nbsp;</td>
										<td width="4%" >&nbsp;</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
<!-- Datos del Siniestro-->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosSiniestroCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccion" colspan="7">
											<midas:mensaje clave="siniestro.cabina.datosSiniestro" />
										</th>
									</tr>
									<tr>
										<td width="3%">&nbsp;</td>
										<th width="12%" align="left">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.calle" />
										</th>
										<td colspan="2">
											<midas:texto  id="calle" propiedadFormulario="calle" deshabilitado="true" />
										</td>
										<th width="15%" align="left">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.cp" />
										</th>
										<td  width="25%">
											<midas:texto id="codigoPostal" propiedadFormulario="codigoPostal" deshabilitado="true" />
										</td>
										<td  width="4%">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.estado" />
										</th>
										<td colspan="2">
											<midas:estado readonly="true" styleId="idEstado" size="1" propiedad="idEstado" pais="PAMEXI" styleClass="cajaTexto"
											onchange="limpiarObjetos('idCiudad,idColonia'); getCiudades(this,'idCiudad');asignarCoordinadorPorZona(this.value);" />
										</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.ciudad" />
										</th>
										<td >
											<midas:ciudad  readonly="true" styleId="idCiudad" size="1" propiedad="idCiudad" estado="idEstado" styleClass="cajaTexto" onchange="getColonias(this,'idColonia')" />				
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.colonia" />
										</th>
										<td colspan="2">
											<midas:colonia readonly="true" styleId="idColonia" size="1" propiedad="idColonia" ciudad="idCiudad" styleClass="cajaTexto"/>
										</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaSiniestro" />
										</th>
										<td>
											<midas:texto id="fechaSiniestro" propiedadFormulario="fechaSiniestro" deshabilitado="true" />
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.descripcionSiniestro" />
										</th>
										<td colspan="2">
											<midas:areatexto propiedadFormulario="descripcionSiniestro" renglones="5" columnas="50" deshabilitado="true"/>
										</td>	
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.eventoCatastrofico" />
										</th>
										<td >
											<midas:texto propiedadFormulario="descripcionEventoCatastrofico" deshabilitado="true"/>
										</td>
										<td colspan="4">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreContacto" />
										</th>
										<td colspan="2">
											<midas:texto propiedadFormulario="nombreContacto" deshabilitado="true" />
										</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.telefonoContacto" />
										</th>
										<td>
											<midas:texto  id="telefonoContacto" propiedadFormulario="telefonoContacto" deshabilitado="true" />
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.observaciones" />
										</th>
										<td colspan="2">
											<midas:areatexto propiedadFormulario="observaciones" renglones="5" columnas="50" deshabilitado="true"/>
										</td>			
										<td colspan="3">&nbsp;</td>	
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreCoordinador" />
										</th>
										<td>
											<midas:texto id="nombreCoordinador" propiedadFormulario="nombreCoordinador" deshabilitado="true" />
										</td>
										<td colspan="4">&nbsp;</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
<!--  Datos del Ajustador  -->
					<tr>
						<td colspan="7" >
							<div id="seccionDatosAjustadorCrear" align="center" >
								<table border="0" width="100%" id="agregar">
									<tr>
										<th class="seccion" colspan="7">
											<c:if test="${reporteSiniestroForm.idStatus==1}">
												<midas:mensaje clave="siniestro.cabina.reportesiniestro.desplegarReporte.pendienteAsignarAjustador" />
											</c:if>
											<c:if test="${reporteSiniestroForm.idStatus==2}">
												<midas:mensaje clave="siniestro.cabina.reportesiniestro.desplegarReporte.pendienteConfirmarAjustador" />
											</c:if>
											<c:if test="${reporteSiniestroForm.idStatus==3}">
												<midas:mensaje clave="siniestro.cabina.reportesiniestro.desplegarReporte.ajustadorConfirmado" />
											</c:if>
										</th>
									</tr>
									<tr>
										<td width="6%">&nbsp;</td>
										<th width="15%" align="left">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.tipoNegocio" />
										</th>
										<td width="25%">
											<midas:texto id="tipoNegocio" propiedadFormulario="tipoNegocio"	deshabilitado="true" />
										</td>
										<td colspan="4">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<th>
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.nombreAjustador" />
										</th>
										<td colspan="2">
											<midas:texto id="nombreAjustador" propiedadFormulario="nombreAjustador" deshabilitado="true" />
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="7" >
							<div class="alinearBotonALaDerecha">
								<div id="b_regresar">
									<a href="javascript: void(0);" onclick="javascript: regresarListaReportes();"><midas:mensaje clave="midas.accion.regresar"/></a>
								</div>					
							</div>
						</td> 
					</tr>
				</table>
			
			</midas:formulario>
		</center>
	</div>
	
	<div id="datosPoliza" name="Datos de la poliza" href="http://void" extraAction="sendRequest(document.datosPolizaForm,'/MidasWeb/siniestro/cabina/desplegarDatosPoliza.do?idToReporteSiniestro=<midas:escribe propiedad='idToReporteSiniestro' nombre='reporteSiniestroForm'/>&fechaSiniestro=<midas:escribe propiedad='fechaSiniestro' nombre='reporteSiniestroForm'/>', 'contenido_datosPoliza','muestraListaIncisosPolizaCrear();')"></div>
	
	<div id="estadoSiniestro" name="Bit&aacute;cora del Siniestro" href="http://void" extraAction="datosEstadoSiniestroDesplegar(document.estadosiniestroForm)"></div>
	
	<div id="preguntasEspeciales" name="Preguntas Especiales" href="http://void" extraAction="sendRequest(document.preguntaEspecialForm,'/MidasWeb/siniestro/cabina/desplegarDatosPreguntasEspeciales.do?idToReporteSiniestro=<midas:escribe propiedad='idToReporteSiniestro' nombre='reporteSiniestroForm'/>', 'contenido_preguntasEspeciales',null)"></div>
	
	<div id="transportes" name="Transporte" href="http://void" extraAction="sendRequest(document.transporteForm,'/MidasWeb/siniestro/cabina/desplegarDatosTransporte.do?idToReporteSiniestro=<midas:escribe propiedad='idToReporteSiniestro' nombre='reporteSiniestroForm'/>', 'contenido_transportes',null)"></div>
	
	 <div id="coberturaRiesgo" name="Cobertura Riesgo" href="http://void" extraAction="desplegarAfectarCoberturaRiesgo()"></div> 
	
</div>
