<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/cabina/desplegarDatosTransporte">
<table width="100%" border="0" cellspacing="10" cellpadding="0" id="desplegar">
  <tr>
      <th class="seccion"><midas:mensaje clave="siniestro.reporte.transportes.datosTransportistaCarga"/>: </th>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="10" cellpadding="0" id="desplegar">
          <tr>
            <th width="13%"><midas:mensaje clave="siniestro.reporte.transportes.nombreRazonSocial"/>:</th>
            <td colspan="2">
              <midas:texto id="nombreRazonSocial" propiedadFormulario="nombreRazonSocial" deshabilitado="true"/>
            </td>
            <td colspan="2">&nbsp;</td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.tipoPersona"/>:</th>
            <td colspan="2">
	            <midas:radio valorEstablecido="F" propiedadFormulario="tipoPersona" deshabilitado="true">F&iacute;sica</midas:radio>
	            <midas:radio valorEstablecido="M" propiedadFormulario="tipoPersona" deshabilitado="true">Moral</midas:radio>
			</td>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.estado"/>:</th>
            <td width="25%">
            	<midas:estado readonly="true" styleId="idEstado" size="1" propiedad="idEstado" pais="PAMEXI" styleClass="cajaTexto" />
										
            </td>
            <th width="15%"><midas:mensaje clave="siniestro.reporte.transportes.municipio"/>:</th>
            <td width="25%">
				<midas:ciudad  readonly="true" styleId="idCiudad" size="1" propiedad="idCiudad" estado="idEstado" styleClass="cajaTexto" onchange="getColonias(this,'idColonia')" />			
			</td>
			<td>&nbsp;</td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.colonia"/>:</th>
            <td>
            	<midas:colonia readonly="true" styleId="idColonia" size="1" propiedad="idColonia" ciudad="idCiudad" styleClass="cajaTexto"/>
            </td>
            <th><midas:mensaje clave="siniestro.reporte.transportes.cp"/>:</th>
            <td><midas:texto id="cp" propiedadFormulario="cp" deshabilitado="true"/></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.calle"/>:</th>
            <td colspan="2"><midas:texto id="calle" propiedadFormulario="calle" deshabilitado="true"/></td>
            <td colspan="2">&nbsp;</td>
          </tr>

          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.telefono"/>:</th>
            <td ><midas:texto id="telefono" propiedadFormulario="telefono" deshabilitado="true"/></td>
            <td colspan="3">&nbsp;</td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.rfc_curp"/>: </th>
            <td ><midas:texto id="rfc_curp" propiedadFormulario="rfc_curp" deshabilitado="true"/></td>
            <td colspan="3">&nbsp;</td>
            </tr>
          <tr>
            <th><midas:mensaje clave="siniestro.reporte.transportes.noUnidad"/></th>
            <td ><midas:texto id="noUnidad" propiedadFormulario="noUnidad" deshabilitado="true"/></td>
            <td colspan="3">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</midas:formulario>