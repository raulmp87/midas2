<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<midas:formulario accion="/siniestro/cabina/reporteEstadoSiniestro">
<midas:oculto propiedadFormulario="idToReporteSiniestro"/>
<table id="desplegar" width="50%">
  <tr>
    <td class="titulo" colspan="2"><midas:mensaje clave="siniestro.reporte.estadosinis.fechaSiniestro"/></td>
  </tr>
  <tr>
    	<td colspan="2" align="center">
			<b><font color="#FF0000">
			<logic:present name="estadosiniestroForm" property="descripcionDato">
			<midas:escribe propiedad="descripcionDato" nombre="estadosiniestroForm"/>
			</logic:present>
			</font></b>
    	</td>
  </tr>
  
  <tr>
  	<td width="25%">
  		<midas:mensaje clave="siniestro.reporte.estadosinis.fechaAsignacion"/>:
  	</td>
    <td width="75%">
    <html:text property="fechaAsignacion" styleId="fechaAsignacion" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
    <image src="../img/b_calendario.gif" border=0 />  
	</td>
   </tr>
   <tr>
     <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaContactoAjustador"/>:</td>
     <td>
     	<html:text property="fechaContactoAjustador"  styleId="fechaContactoAjustador" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
        <image src="../img/b_calendario.gif" border=0 />
     </td>
   </tr>
<!--   <tr>-->
<!--     <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaLlegadaAjustador"/>:</td>-->
<!--     <td>-->
<!--     	<html:text property="fechaLlegadaAjustador"  styleId="fechaLlegadaAjustador" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />-->
<!--        <a href="#" id="mostrarCalendario3" onclick="mostrarCalendarioSiniestro('3')" ><image src="../img/b_calendario.gif" border=0 /></a>-->
<!--     </td>-->
<!--   </tr>-->
   <tr>
   	<td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaInspeccionAjustador"/>:</td>
    <td>
    	<html:text property="fechaInspeccionAjustador"  styleId="fechaInspeccionAjustador" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
        <image src="../img/b_calendario.gif" border=0 />
    </td>
   </tr>
   <tr>
    <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaPreliminar"/>:</td>
    <td>
		<html:text property="fechaPreliminar"  styleId="fechaPreliminar" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
        <image src="../img/b_calendario.gif" border=0 />
    </td>
   </tr>
   <tr>
     <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaDocumento"/>:</td>
     <td>
		<html:text property="fechaDocumento"  styleId="fechaDocumento" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
		<image src="../img/b_calendario.gif" border=0 />
     </td>
   </tr>
   <tr>
     <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaInformeFinal"/>:</td>
     <td>
		<html:text property="fechaInformeFinal"  styleId="fechaInformeFinal" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
		<image src="../img/b_calendario.gif" border=0 />
     </td>
   </tr>
   <tr>
     <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaTerminacionSiniestro"/>:</td>
     <td>
      	<html:text property="fechaTerminacionSiniestro"  styleId="fechaTerminacionSiniestro" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
 		<image src="../img/b_calendario.gif" border=0 />
     </td>
    </tr>
    <tr>
      <td><midas:mensaje clave="siniestro.reporte.estadosinis.fechaCerrado"/>:</td>
      <td>
        <html:text property="fechaCerrado"  styleId="fechaCerrado" name="estadosiniestroForm" maxlength="10" size="10"  styleClass="cajaTextoSiniestro" readonly="true" />
      	<image src="../img/b_calendario.gif" border=0 />
     </td>
    </tr>
    <tr>
      <td><midas:mensaje clave="siniestro.reporte.estadosinis.comentarios"/>:</td>
      <td>
       <midas:areatexto propiedadFormulario="comentarios" renglones="8" columnas="40" />
      </td>
    </tr>
    <tr>
      <td colspan="2">
	    <table id="desplegar">
	      <tr>
		        <td width="10%"><midas:mensaje clave="siniestro.reporte.estadosinis.cabinero"/>:</td>
		        <td width="20%">
		        	<midas:texto id="descripcionCabinero" propiedadFormulario="descripcionCabinero" soloLectura="true"/>
		        </td>
		        <td width="10%"><midas:mensaje clave="siniestro.reporte.estadosinis.procesoActual"/>:</td>
		        <td width="60%" colspan="3">
		        	<midas:texto id="etapaReporte" propiedadFormulario="etapaReporte" soloLectura="true"/>
		        </td>
	       </tr>
	       <tr>
		       <td align="left">
					<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.terminoAjuste" />				
				</td>
				<td align="left" width="20%">
					<html:select name="estadosiniestroForm" property="idTcTerminoAjuste" styleClass="cajaTexto" styleId="idTcTerminoAjuste" onfocus="this.oldvalue = this.selectedIndex;" onchange="seleccionaTerminoAjusteEstadoSiniestro();">
					<html:option value="0"><midas:mensaje clave="midas.sistema.combo.seleccione"/></html:option>
				    	<html:optionsCollection name="estadosiniestroForm" property="listaTerminosAjuste"  value="idTcTerminoAjuste" label="descripcion" />
				    </html:select>
				</td>
				<td align="left">
					<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.estatusSiniestro" />				
				</td>
				<td align="left" width="20%">
					<html:select name="estadosiniestroForm" property="idTcEstatusSiniestro" styleClass="cajaTexto"  styleId="idTcEstatusSiniestro" onfocus="this.oldvalue = this.selectedIndex;" onchange="seleccionaEstatusEstadoSiniestro();" >
				    	<html:optionsCollection name="estadosiniestroForm" property="listaEstatusSiniestro"  value="idTcEstatusSiniestro" label="descripcion" />
				    </html:select>
				</td>
				<td align="left" >
					<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.subEstatusSiniestro" />
				</td>
				<td align="left" width="20%">
					<html:select name="estadosiniestroForm" property="idTcSubEstatusSiniestro" styleClass="cajaTexto" styleId="idTcSubEstatusSiniestro" onchange="seleccionaSubEstatusEstadoSiniestro();">
						<html:option value="0"><midas:mensaje clave="midas.sistema.combo.seleccione"/></html:option>
				    	<html:optionsCollection name="estadosiniestroForm" property="listaSubEstatusSiniestro"  value="idTcSubEstatusSiniestro" label="descripcion" />
				    </html:select>
				</td>
			</tr>
	      </table>  
      </td>
    </tr>
	<tr>
        <td colspan="2" align="center">
        	<div class="alinearBotonALaDerecha">
        		<div id="b_guardar">
          			<a href="javascript: void(0);" onclick="javascript: validarReporteEstadoSiniestro()"><midas:mensaje clave="siniestro.reporte.estadosinis.guardarReporte"/></a>
          		</div>
          	</div>
        </td>
    </tr>
 </table>
</midas:formulario>