<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/siniestro/salvamento/listar">
	<midas:oculto propiedadFormulario="idToReporteSiniestro"/>
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="siniestro.salvamento.listarSalvamento.titulo" />
			</td>
		</tr>
		<tr>
			<td><midas:mensaje clave="siniestro.salvamento.item.valorEstimadoTotal"/>:</td>
			<td><midas:texto propiedadFormulario="valorEstimadoTotal" deshabilitado="true"/></td>
			<td><midas:mensaje clave="siniestro.salvamento.item.valorVentaTotal"/>: </td>
			<td><midas:texto propiedadFormulario="valorVentaTotal" deshabilitado="true"/></td>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="salvamentoSiniestroTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.SalvamentoSiniestro"
			claseCss="tablaConResultados" nombreLista="items"
			urlAccion="/siniestro/salvamento/listar.do">
			<midas:columna propiedad="articulo" titulo="Art&iacute;culo"/>
			<midas:columna propiedad="cantidad" titulo="Cantidad"/>
			<midas:columna propiedad="valorEstimado" titulo="Valor Estimado"/>
			<midas:columna propiedad="valorVenta" titulo="Valor Venta" />
			<midas:columna propiedad="numFactura" titulo="Factura"/>
			<midas:columna propiedad="estatus" titulo="Estatus"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
	</div>
	
	<br clear="all" />
	<div style="width: 98%">
		<table>
			<tr>
				<td style="width: 85%"></td>
				<td>
					<div id="b_regresar">
						<a onclick="javascript: listarReportesSiniestro();" href="javascript: void(0);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</td>
				<td>
					<div id="b_agregar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.salvamentoSiniestroForm,'/MidasWeb/siniestro/salvamento/mostrarAgregar.do',
								'contenido','inicializaComponentesSalvamentoSiniestro(SALVAMENTO_SINIESTRO_AGREGAR)');">
							<midas:mensaje clave="midas.accion.agregar"/>
						</a>
					</div>
				</td>
			</tr>
		</table>
	</div>

</midas:formulario>
