<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/siniestro/salvamento/mostrarAgregar">
 	<midas:oculto propiedadFormulario="idToReporteSiniestro"/>
 	<midas:oculto propiedadFormulario="idToSalvamentoSiniestro"/>
 	
	<table id="agregar" >
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />&nbsp;
				<midas:mensaje clave="siniestro.salvamento.titulo.salvamento" />
			</td>
		</tr>
		<tr>
			<td style="width: 10%">
				<etiquetas:etiquetaError 
					requerido="si" 
					property="articulo"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.articulo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto onkeypress="return soloAlfanumericos(this, event, false)" propiedadFormulario="articulo" caracteres="99"/></td>
			<td style="width: 10%">
				<etiquetas:etiquetaError 
					requerido="si" 
					property="descripcion"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.descripcion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto onkeypress="return soloAlfanumericos(this, event, false)" propiedadFormulario="descripcion" caracteres="239"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="ubicacion"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.ubicacion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto onkeypress="return soloAlfanumericos(this, event, false)" propiedadFormulario="ubicacion" caracteres="499"/></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="cantidad"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.cantidad" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto onkeypress="return soloNumeros(this, event, true)" propiedadFormulario="cantidad" caracteres="8"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="unidad"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.unidad" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>			
			<td><midas:texto propiedadFormulario="unidad" id="unidad" caracteres="99"/></td>
			
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="estatusSalvamentoSeleccionado"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.estatus" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:select name="salvamentoSiniestroForm" property="estatusSalvamentoSeleccionado" styleClass="cajaTexto" >
			    	<html:optionsCollection name="salvamentoSiniestroForm" property="listaEstatusSalvamento"  value="idTcSalvamentoEstatus" label="descripcion" />
			    </html:select>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="valorEstimado"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.valorEstimado" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="valorEstimado" id="valorEstimado" 
			onkeypress="return soloNumeros(this, event, true)" caracteres="14" 
			onfocus="new Mask('$#,###.00', 'number').attach(this)" onblur="javascript:calculaMontosSalvamentoSiniestro(this);"/></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="fechaProvision"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.fechaProvision" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="fechaProvision" id="fechaProvision" soloLectura="true"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="valorVenta"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.valorVenta" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<table>
					<tr>
						<td><midas:texto propiedadFormulario="valorVenta" onfocus="new Mask('$#,###.00', 'number').attach(this)" soloLectura="true"/></td>
						<td>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="inspeccionado"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.inspeccionado" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:check propiedadFormulario="inspeccionado" id="inspeccionado"/></td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="valorVenta"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.impuestos" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
				
			</td>
			<td>
				<midas:mensaje clave="siniestro.salvamento.item.iva"/>
				<midas:check propiedadFormulario="tieneIVA" id="tieneIVA" onClick="javascript:habilitaCamposSalvamento(this, 'porcentajeIVA', 'montoIVA');" />
				<midas:mensaje clave="siniestro.salvamento.item.ivaRet"/>
				<midas:check propiedadFormulario="tieneIVARet" id="tieneIVARet" onClick="javascript:habilitaCamposSalvamento(this, 'porcentajeIVARet', 'montoIVARet');" />
				<midas:mensaje clave="siniestro.salvamento.item.isr"/>
				<midas:check propiedadFormulario="tieneISR" id="tieneISR" onClick="javascript:habilitaCamposSalvamento(this, 'porcentajeISR', 'montoISR');" />
			</td>
			<td>							
				<midas:mensaje clave="siniestro.salvamento.item.isrRet" />
				<midas:check propiedadFormulario="tieneISRRet" id="tieneISRRet" onClick="javascript:habilitaCamposSalvamento(this, 'porcentajeISRRet', 'montoISRRet');" />
				<midas:mensaje clave="siniestro.salvamento.item.otros"/>
				<midas:check propiedadFormulario="tieneOtros" id="tieneOtros" onClick="javascript:habilitaCamposSalvamento(this, 'porcentajeOtros', 'montoOtros');" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeIVA"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeIVA" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeIVA" id="porcentajeIVA" soloLectura="true"
				onblur="calculaMontoPorcentajeSalvamentos(this, 'montoIVA')" caracteres="3" onkeypress="return soloNumeros(this, event, true)" />
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoIVA"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoIVA" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoIVA" id="montoIVA" soloLectura="true"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeIVARet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeIVARet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeIVARet" id="porcentajeIVARet" soloLectura="true" 
				onkeypress="return soloNumeros(this, event, true)"  caracteres="3" onblur="calculaMontoPorcentajeSalvamentos(this, 'montoIVARet')"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoIVARet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoIVARet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoIVARet" id="montoIVARet" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeISR"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeISR" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeISR" id="porcentajeISR" soloLectura="true" 
				onkeypress="return soloNumeros(this, event, true)" caracteres="3" onblur="calculaMontoPorcentajeSalvamentos(this, 'montoISR')"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoISR"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoISR" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoISR" id="montoISR" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeISRRet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeISRRet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajeISRRet" id="porcentajeISRRet" soloLectura="true"  
				onkeypress="return soloNumeros(this, event, true)" caracteres="3" onblur="calculaMontoPorcentajeSalvamentos(this, 'montoISRRet')"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoISRRet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoISRRet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="montoISRRet" id="montoISRRet" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeOtros"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.otros" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" soloLectura="true"  
				onkeypress="return soloNumeros(this, event, true)" caracteres="3" onblur="calculaMontoPorcentajeSalvamentos(this, 'montoOtros')"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoOtros"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoOtros" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="montoOtros" id="montoOtros" soloLectura="true"/>
			</td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeCuatoParte"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeCuotaParte" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajeCuatoParte" soloLectura="true"/>
			</td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeRetencion"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeRetencion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeRetencion" soloLectura="true"/>
			</td>		
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajePrimerExcedente"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajePrimerExcedente" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajePrimerExcedente" soloLectura="true"/>
			</td>	
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeFacultativo"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeFacultativo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajeFacultativo" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="observaciones"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.observaciones" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="4">
				<midas:areatexto id="observaciones" propiedadFormulario="observaciones" renglones="3" caracteres="239"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="numFactura"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.numFactura" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>				
			</td>
			<td>
				<midas:texto propiedadFormulario="numFactura" id="numFactura" soloLectura="true"/>
			</td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="comprador"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.comprador" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>	
			</td>
			<td>
				<midas:texto propiedadFormulario="comprador" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="tienePerecederos"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.tienePerecederos" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>	
			</td>
			<td>
				<midas:check propiedadFormulario="tienePerecederos" id="tienePerecederos" onClick="javascript: tienePerecederosSalvamentoSiniestro(this);"/>
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick='javascript:mostrarSalvamentosPorReporteSiniestro(<midas:escribe propiedad="idToReporteSiniestro" nombre="salvamentoSiniestroForm"/>);'>
						<midas:mensaje clave="midas.accion.regresar"/>
					</a>
					</div>
					<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.salvamentoSiniestroForm,'/MidasWeb/siniestro/salvamento/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>
