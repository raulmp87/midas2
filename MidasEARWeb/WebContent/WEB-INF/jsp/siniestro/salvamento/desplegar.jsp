<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/siniestro/salvamento/mostrarDetalle">
 	<midas:oculto propiedadFormulario="idToReporteSiniestro"/>
 	<midas:oculto propiedadFormulario="idToSalvamentoSiniestro"/>
 	
	<table id="agregar" >
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />&nbsp; 
				<midas:mensaje clave="siniestro.salvamento.titulo.salvamento" />
			</td>
		</tr>
		<tr>
			<td style="width: 10%">
				<etiquetas:etiquetaError 
					requerido="si" 
					property="articulo"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.articulo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto deshabilitado="true" propiedadFormulario="articulo"/></td>
			<td style="width: 10%">
				<etiquetas:etiquetaError 
					requerido="si" 
					property="descripcion"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.descripcion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto deshabilitado="true" propiedadFormulario="descripcion"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="ubicacion"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.ubicacion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto deshabilitado="true" propiedadFormulario="ubicacion" /></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="cantidad"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.cantidad" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto deshabilitado="true" propiedadFormulario="cantidad"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="unidad"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.unidad" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>			
			<td><midas:texto deshabilitado="true" propiedadFormulario="unidad" id="unidad"/></td>
			
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="estatusSalvamentoSeleccionado"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.estatus" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto deshabilitado="true" propiedadFormulario="estatusSalvamentoSeleccionadoDescripcion" id="estatusSalvamentoSeleccionadoDescripcion"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="valorEstimado"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.valorEstimado" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="valorEstimado" id="valorEstimado" 
			deshabilitado="true"/></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="fechaProvision"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.fechaProvision" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="fechaProvision" id="fechaProvision" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="valorVenta"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.valorVenta" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<table>
					<tr>
						<td><midas:texto deshabilitado="true" propiedadFormulario="valorVenta"/></td>
						<td></td>
					</tr>
				</table>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="inspeccionado"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.inspeccionado" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:check deshabilitado="true" propiedadFormulario="inspeccionado" id="inspeccionado"/></td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="valorVenta"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.impuestos" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
				
			</td>
			<td>
				<midas:mensaje clave="siniestro.salvamento.item.iva"/>
				<midas:check propiedadFormulario="tieneIVA" id="tieneIVA" deshabilitado="true"/>
				<midas:mensaje clave="siniestro.salvamento.item.ivaRet"/>
				<midas:check propiedadFormulario="tieneIVARet" id="tieneIVARet" deshabilitado="true"/>
				<midas:mensaje clave="siniestro.salvamento.item.isr"/>
				<midas:check propiedadFormulario="tieneISR" id="tieneISR" deshabilitado="true"/>
			</td>
			<td>							
				<midas:mensaje clave="siniestro.salvamento.item.isrRet" />
				<midas:check propiedadFormulario="tieneISRRet" id="tieneISRRet" deshabilitado="true"/>
				<midas:mensaje clave="siniestro.salvamento.item.otros"/>
				<midas:check propiedadFormulario="tieneOtros" id="tieneOtros" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeIVA"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeIVA" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeIVA" id="porcentajeIVA" deshabilitado="true"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoIVA"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoIVA" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoIVA" id="montoIVA" deshabilitado="true"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeIVARet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeIVARet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeIVARet" id="porcentajeIVARet" deshabilitado="true" />
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoIVARet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoIVARet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoIVARet" id="montoIVARet" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeISR"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeISR" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeISR" id="porcentajeISR" deshabilitado="true" />
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoISR"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoISR" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="montoISR" id="montoISR" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeISRRet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeISRRet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajeISRRet" id="porcentajeISRRet" deshabilitado="true"  />
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoISRRet"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoISRRet" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="montoISRRet" id="montoISRRet" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeOtros"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.otros" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" deshabilitado="true"  />
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="montoOtros"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.montoOtros" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="montoOtros" id="montoOtros" deshabilitado="true"/>
			</td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeCuatoParte"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeCuotaParte" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajeCuatoParte" deshabilitado="true"/>
			</td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeRetencion"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeRetencion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentajeRetencion" deshabilitado="true"/>
			</td>		
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajePrimerExcedente"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajePrimerExcedente" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajePrimerExcedente" deshabilitado="true"/>
			</td>	
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="porcentajeFacultativo"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.porcentajeFacultativo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto  propiedadFormulario="porcentajeFacultativo" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="observaciones"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.observaciones" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="4">
				<midas:areatexto id="observaciones" propiedadFormulario="observaciones" deshabilitado="true" renglones="3" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="numFactura"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.numFactura" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>				
			</td>
			<td>
				<midas:texto deshabilitado="true" propiedadFormulario="numFactura" id="numFactura"/>
			</td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="comprador"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.comprador" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>	
			</td>
			<td>
				<midas:texto propiedadFormulario="comprador" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="tienePerecederos"
					name="salvamentoSiniestroForm"
					key="siniestro.salvamento.item.tienePerecederos" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>	
			</td>
			<td>
				<midas:check propiedadFormulario="tienePerecederos" id="tienePerecederos" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick='javascript:mostrarSalvamentosPorReporteSiniestro(<midas:escribe propiedad="idToReporteSiniestro" nombre="salvamentoSiniestroForm"/>);'>
						<midas:mensaje clave="midas.accion.regresar"/>
					</a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>
