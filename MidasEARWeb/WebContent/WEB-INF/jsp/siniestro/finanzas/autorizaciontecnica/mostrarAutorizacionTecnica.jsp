<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/autorizaciontecnica/mostrarAutorizacionTecnica">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="numeroAutorizacionTecnica" styleId="numeroAutorizacionTecnica"/>
	<html:hidden property="idTipoAutorizacionTecnica" styleId="idTipoAutorizacionTecnica"/>
	<html:hidden property="pantalla" styleId="pantalla"/>
	<html:hidden property="idToConcepto" styleId="id"/>
	
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.titulo" />				
			</td>
		</tr>	
		<tr>
			<td align="left" >
				&nbsp;
			</td>		
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;border :1px ,solid;" class="tableContainer">  
					<table width="100%" border="0" class="grid">
						<thead>
							<tr>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.numeroAutorizacion" /></th>								
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.tipoAutorizacion" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.ordenDePago" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.montoNeto" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.estatus" /></th>
								<th><midas:mensaje clave="midas.accion.detalle" /></th>
							</tr>
						</thead>
						<tr>
							<td class="datoTabla" align="left">
                                <midas:escribe propiedad="autorizacionTecnica.idToAutorizacionTecnica" nombre="autorizacionTecnicaForm"/>                                        
                             </td>
                             <td class="datoTabla" align="left">
                                <midas:escribe propiedad="autorizacionTecnica.tipoAutorizacionTecnica" nombre="autorizacionTecnicaForm"/>
                             </td>
                             <td class="datoTabla"  align="left">
                             	<midas:escribe propiedad="autorizacionTecnica.idToOrdenPago" nombre="autorizacionTecnicaForm"/>
                             </td>
                             <td class="datoTabla"  align="right">
                                <midas:escribe propiedad="autorizacionTecnica.montoNeto" nombre="autorizacionTecnicaForm" formato="###,###,##0.00"/>
                             </td>
                             <td class="datoTabla"  align="left">
                                <midas:escribe propiedad="autorizacionTecnica.descripcionEstatus" nombre="autorizacionTecnicaForm"/>
                                <input type="hidden" id="estatusAT" value='<midas:escribe propiedad="autorizacionTecnica.estatus" nombre="autorizacionTecnicaForm"/>' />
                             </td>
                             <td class="datoTabla"  align="right">
                             	<a href="javascript: void(0);" onclick="mostrarAutorizacionTecnica()">
                             			<img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/>
                             	</a>
                             </td>                             
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td  align="center">
				<midas:boton onclick="autorizarAutorizacionTecnica()" tipo="guardar" texto="Autorizar AT" style="width:120px;"/>
			</td>
			<td  align="center">
				<midas:boton onclick="cancelarAutorizacionTecnica()" tipo="guardar" texto="Cancelar AT" style="width:120px;"/>
			</td>			
			<td >
				<midas:boton onclick="goHome()" tipo="guardar" texto="Salir" style="width:120px;"/>
			</td>
			<td colspan="" >&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
