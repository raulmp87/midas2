<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesPorCancelar">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.titulo.cancelar" />				
			</td>
		</tr>	
		<tr>
			<td align="left" >
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.header.titulo" />
			</td>		
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.numeroAutorizacion" /></th>								
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.tipoAutorizacion" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.ordenDePago" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.montoNeto" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.estatus" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.header.cancelar" /></th>
							</tr>
						</thead>
						<logic:empty name="autorizacionTecnicaForm" property="autorizacionesTecnicas">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="autorizacionTecnicaForm" property="autorizacionesTecnicas">
							<logic:iterate name="autorizacionTecnicaForm" property="autorizacionesTecnicas" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="idToAutorizacionTecnica" nombre="lista"/>                                        
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoAutorizacionTecnica" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="idToOrdenPago" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="montoNeto" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcionEstatus" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
										<input type="checkbox" name="cancelarAT" id="cancelarAT" value="<midas:escribe propiedad="idToAutorizacionTecnica" nombre="lista"/>"/>
                                    </td>                                                                       
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td  align="center">
				<midas:boton onclick="cancelarAutorizacionesTecnicas()" tipo="guardar" texto="Cancelar AT" style="width:120px;"/>
			</td>
			<td >
				<midas:boton onclick="javaScript:alert('salir');" tipo="guardar" texto="Salir" style="width:120px;"/>
			</td>
			<td colspan="2" >&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
