<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario
	accion="/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro" />
	<html:hidden property="idToConcepto" styleId="idToConcepto" />
	<table>
		<tr>
			<td>
				<a href="#" onclick="enviarAutorizacionGasto()">Ir a Autorizacion de Gasto</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="#" onclick="enviarAutorizacionIngreso()">Ir a Autorizacion de Ingreso</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="#" onclick="enviarAutorizacionIndemnizacion()">Ir a Autorizacion de Indemnizacion</a>
			</td>
		</tr>
		
		<tr>
			<td>
				<a href="#" onclick="listarAutorizacionesPorCancelar()">Autorizaciones por Cancelar</a>
			</td>
		</tr>
		<tr>
			<td>
				<a href="#" onclick="listarAutorizacionesPorAutorizar()">Autorizaciones por Autorizar</a>
			</td>
		</tr>						
		<tr>
			<td>
				<a href="#" onclick="mostrarAutorizacionTecnicaLigas()">Mostrar Autorizacion Tecnica</a>
			</td>
		</tr>								
								
	</table>
</midas:formulario>
