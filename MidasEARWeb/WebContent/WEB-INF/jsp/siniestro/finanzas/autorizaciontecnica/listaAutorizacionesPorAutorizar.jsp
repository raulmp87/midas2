<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesTecnicas">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<input type="hidden" id="estatusAT" value="-1" />
	<input type="hidden" id="pantalla" value="4"/>	
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.titulo.autorizar" />				
			</td>
		</tr>	
		<tr>
			<td align="left" >
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.header.titulo" />
			</td>		
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.numeroAutorizacion" /></th>								
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.tipoAutorizacion" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.ordenDePago" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.idSolicitudCheque" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.montoNeto" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.listado.estatus" /></th>
								<th><midas:mensaje clave="midas.accion.detalle" /></th>
								<th><midas:mensaje clave="midas.accion.seleccionar" /></th>								
							</tr>
						</thead>
						<logic:empty name="autorizacionTecnicaForm" property="autorizacionesTecnicas">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="autorizacionTecnicaForm" property="autorizacionesTecnicas">
							<logic:iterate name="autorizacionTecnicaForm" property="autorizacionesTecnicas" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="right">
                                        <midas:escribe propiedad="idToAutorizacionTecnica" nombre="lista"/>                                        
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoAutorizacionTecnica" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="idToOrdenPago" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="idSolicitudCheque" nombre="lista"/>
                                    </td>                                    
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="montoNeto" formato="$###,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcionEstatus" nombre="lista"/>
                                    </td>
		                             <td class="datoTabla"  align="left">
		                             	<a href="javascript: void(0);" 
		                             		onclick="verDetalleATecnica('<midas:escribe propiedad="idTipoAutorizacionTecnica" nombre="lista"/>', 
		                             									'<midas:escribe propiedad="idConcepto" nombre="lista"/>', 
		                             									'<midas:escribe propiedad="idToAutorizacionTecnica" nombre="lista"/>')">
		                             		<img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/>
		                             	</a>
		                             </td>                                                                 
                                    <td class="datoTabla" align="center">
										<input type="radio" name="numeroAutorizacionTecnica" id="numeroAutorizacionTecnica" 
											   value="<midas:escribe propiedad="idToAutorizacionTecnica" nombre="lista"/>"
											   onclick="asignaEstatusAT('<midas:escribe propiedad="estatus" nombre="lista"/>')"/>																						   																		
                                    </td>                                                                       
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>			
			<logic:equal name="autorizacionTecnicaForm" property="permitirAutorizar" value="1">
				<td align="center">
					<midas:boton onclick="autorizarAutorizacionTecnica()" tipo="guardar" texto="Autorizar AT" style="width:120px;"/>
				</td>
			</logic:equal>
			<logic:notEqual name="autorizacionTecnicaForm" property="permitirAutorizar" value="1">
				<td>&nbsp;</td>
			</logic:notEqual>			
			<logic:equal name="autorizacionTecnicaForm" property="permitirCancelar" value="1">						
				<td align="center">
					<midas:boton onclick="cancelarAutorizacionTecnica()" tipo="modificar" texto="Cancelar AT" style="width:120px;"/>
				</td>
			</logic:equal>
			<logic:notEqual name="autorizacionTecnicaForm" property="permitirCancelar" value="1">
				<td>&nbsp;</td>
			</logic:notEqual>											
			<td>
				<midas:boton onclick="listarReportesSiniestro()" tipo="regresar" texto="Regresar" style="width:120px;"/>
			</td>
			<td>&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
