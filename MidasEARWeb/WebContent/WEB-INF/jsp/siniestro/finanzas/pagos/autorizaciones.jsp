<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/siniestro/finanzas/mostrarGenerarOrdenDePago">
	<html:hidden property="mensaje" styleId="mensaje" />
	<html:hidden property="tipoMensaje" styleId="tipoMensaje" />
	</br>
	<table width="98%" id="filtros" align="center">
		<tr>
			<th>
				<midas:mensaje
					clave="siniestros.finanzas.pagos.autorizacion.noreporte" />
			</th>
			<td>
				<midas:texto propiedadFormulario="soportePagosDTO.numeroReporte"
					nombreFormulario="soportePagosForm" caracteres="14" longitud="16"
					onkeypress="return soloNumeros(this,event,false)" />
			</td>
			<th width="10%">
				<center>
					<midas:mensaje clave="siniestros.finanzas.pagos.autorizacion" />
					<a href="javascript: void(0);" id="mostrarCalendario"
						onclick="mostrarCalendarioDobleLinea();"> <image
							src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /> </a>
				</center>
			</th>
			<th width="8%">
				<etiquetas:etiquetaError property="soportePagosDTO.fechaDesdeString"
					requerido="no" key="siniestros.finanzas.pagos.autorizacion.desde"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaInicial"
					property="soportePagosDTO.fechaDesdeString" name="soportePagosForm"
					size="10" onblur="esFechaValida(this);" styleClass="cajaTexto" />
			</td>
			<th>
				<etiquetas:etiquetaError property="soportePagosDTO.fechaHastaString"
					requerido="no" key="siniestros.finanzas.pagos.autorizacion.hasta"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaFinal"
					property="soportePagosDTO.fechaHastaString" name="soportePagosForm"
					size="10" onblur="esFechaValida(this);" styleClass="cajaTexto" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje
					clave="siniestros.finanzas.pagos.autorizacion.asegurado" />
			</th>
			<td colspan="2">
				<midas:texto propiedadFormulario="soportePagosDTO.nombreAsegurado"
					onkeypress="return soloAlfanumericos(this,event,false)"
					nombreFormulario="soportePagosForm" />
			</td>
			<th>
				<midas:mensaje
					clave="siniestros.finanzas.pagos.autorizacion.proveedor" />				
			</th>
			<td colspan="3">
				<div id="rangoDeFechas" style="position: absolute; z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="fechaFinal"></div>
				</div>
				<midas:comboCatalogo idCatalogo="idPrestadorServicios"
					styleId="idPrestadorServicios"
					propiedad="soportePagosDTO.idPrestadorServicios"
					descripcionCatalogo="nombrePrestador"
					nombreCatalogo="tcprestadorservicios" size="1"
					styleClass="cajaTexto" />				
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.moneda" />
			</th>
			<td>
				<midas:comboCatalogo idCatalogo="idTcMoneda" styleId="idMoneda"
					propiedad="soportePagosDTO.idMoneda"
					descripcionCatalogo="descripcion" nombreCatalogo="vnmoneda"
					size="1" styleClass="cajaTexto" />			
			</td>
			<td colspan="5">
				<div class="alinearBotonALaDerecha">
					<midas:boton
						onclick="javascript: poblarAutorizacionesTecnicas(document.soportePagosForm);"
						tipo="buscar" />
				</div>
			</td>			
		</tr>
	</table>
	<table width="98%" align="center">
		<tr>
			<td colspan="7">
				<div id="autorizacionesTecnicasGrid" class="dataGridConfigurationClass" style="height:290px; width: 900px"></div>
			</td>
			
		</tr>		
	</table>
	<table width="98%" align="center">
		<tr>
			<td colspan="7">
				<div class="alinearBotonALaDerecha">
					<button type="button" onclick="javascript: generarOrdenDePago('generar');" style="cursor: pointer;">
						<table style="font-size: 9px;" align="center">
							<tr>
								<td align="center">
									Generar Orden de Pago
								</td>	
							</tr>
						</table>
					</button>				
				</div>
			</td>
		</tr>		
	</table>	
</midas:formulario>