<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ page isELIgnored="false"%>
<%@  page contentType="text/xml" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>
		
		<column id="claveSeleccion" type="ch" width="30" align="center">#master_checkbox</column>
		<column id="noReporte" type="ro" width="90" sort="str" align="center"><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.noreporte" /></column>
		<column id="noAutorizacion" type="ro" width="115" sort="int" align="center">No.<midas:mensaje clave="siniestros.finanzas.pagos.autorizacion" /></column>
		<column id="proveedor" type="ro" width="180" sort="str" ><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.nombreproveedor" /></column>
		<column id="asegurado" type="ro" width="180" sort="str" ><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.asegurado" /></column>
		<column id="fechaAutorizacion" type="ro" width="135" sort="date" align="center">Fecha <midas:mensaje clave="siniestros.finanzas.pagos.autorizacion" /></column>
		<column id="moneda" type="ro" width="80" sort="int" align="center">Moneda</column>
		<column id="monto" type="ro" width="80" sort="int" align="center">Monto</column>
<%--		<column id="detalle" type="img" width="30" align="center"></column>			--%>
	</head>
	<c:choose>
		<c:when test="${soportePagosForm.autorizaciones != null}">
			<nested:iterate id="autorizacion" name="soportePagosForm" property="autorizaciones" indexId="indexVar" type="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO">
				<row id="<bean:write name="autorizacion" property="idToAutorizacionTecnica"/>">
					<cell><bean:write name="autorizacion" property="claveSeleccion"/></cell>
					<cell><bean:write name="autorizacion" property="reporteSiniestroDTO.numeroReporte"/></cell>
					<cell><bean:write name="autorizacion" property="idToAutorizacionTecnica"/></cell>
					<cell><bean:write name="autorizacion" property="gastoSiniestroDTO.nombrePrestadorServicios"/></cell>
					<cell><bean:write name="autorizacion" property="reporteSiniestroDTO.nombreAsegurado"/></cell>
					<cell><bean:write name="autorizacion" property="fechaAutorizacion" format="dd/MM/yyyy"/></cell>
					<cell>
						<nested:equal value="484" property="reporteSiniestroDTO.polizaDTO.cotizacionDTO.idMoneda">PESOS</nested:equal>
						<nested:equal value="840" property="reporteSiniestroDTO.polizaDTO.cotizacionDTO.idMoneda">DOLARES</nested:equal>
					</cell>
					<cell><bean:write name="autorizacion" property="montoNeto" format="$#,##0.00"/></cell>
<%--					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Detalle^javascript: mostrarDetalleVentanaOrdenDePago(<bean:write name="autorizacion" property="gastoSiniestroDTO.idToGastoSiniestro"/>,<bean:write name="autorizacion" property="idToAutorizacionTecnica"/>)^_self</cell>--%>
				</row>
			</nested:iterate>		
		</c:when>
		<c:otherwise></c:otherwise>	
	</c:choose>
		
</rows>
