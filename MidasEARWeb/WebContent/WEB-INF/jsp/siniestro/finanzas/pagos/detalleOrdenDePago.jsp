<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/siniestro/finanzas/ordenDePago.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/siniestro/siniestro.js"/>"></script>

<div id="blockWindow" class="backgroundFilter" style="display: none;"></div>
<div id="central_indicator" class="sh2" style="display: none;">
	<img id="img_indicator" name="img_indicator"
		src="<html:rewrite page='/img/as2.gif'/>" alt="Afirme" />
</div>
<div id="contenido">
	<midas:formulario accion="/siniestro/finanzas/generarOrdenPago">
		<logic:notEmpty name="soportePagosForm"  property="mensaje">
			<table id="t_riesgo" align="center" width="98%">
				<td style="font-size: small; font-style: italic; color: red;">
					${soportePagosForm.mensaje}
				</td>
			 </table>
		</logic:notEmpty>
		<logic:notEmpty name="soportePagosForm" property="autorizaciones" >
			<div class="subtituloIzquierdaDiv"><midas:escribe propiedad="tituloVentana" nombre="soportePagosForm"/></div>
			<logic:empty name="soportePagosForm" property="autorizaciones" >
				<div class="subtituloIzquierdaDiv"><midas:mensaje clave="siniestros.finanzas.pagos.sininformacion" /></div>
			</logic:empty>		
			<logic:notEmpty name="soportePagosForm" property="autorizaciones" >
				<nested:hidden styleId="soportePagosDTO.idsAutorizaciones" property="soportePagosDTO.idsAutorizaciones" name="soportePagosForm"/>
				<nested:hidden styleId="soportePagosDTO.numeroOrdenDePago" property="soportePagosDTO.numeroOrdenDePago" name="soportePagosForm"/>
				
				<table id="desplegarDetalle" width="98%">
					<tr>
						<th>
							<midas:mensaje clave="siniestros.finanzas.pagos.reportes" />: 
						</th>
						<td>
							<logic:iterate id="autorizacion" name="soportePagosForm" property="autorizaciones" indexId="index">
								<bean:write name="autorizacion" property="reporteSiniestroDTO.numeroReporte"/> |
								<c:set var="moneda" value="${autorizacion.reporteSiniestroDTO.polizaDTO.cotizacionDTO.idMoneda}"></c:set>
							</logic:iterate>					
						</td>
					</tr>
					<tr>
						<th>
							<midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.nombreproveedor" />:
						</th>
						<td>
							<bean:write name="soportePagosForm" property="autorizaciones[0].gastoSiniestroDTO.nombrePrestadorServicios"/>
						</td>
					</tr>			
					<tr>
						<th><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.moneda" />:</th>					
						<c:choose>
							<c:when test="${moneda == 484}">
								<td class="error"><midas:mensaje clave="siniestros.finanzas.pagos.pesos" /></td>
								
							</c:when>
							<c:otherwise>
								<td class="error"><h3><midas:mensaje clave="siniestros.finanzas.pagos.dolares" /></h3></td>
							</c:otherwise>
						</c:choose>			
					</tr>	
					<tr>
						<th><midas:mensaje clave="siniestros.finanzas.pagos.polizasAfectadas" /></th>
						<td>
							<logic:iterate id="autorizacion" name="soportePagosForm" property="autorizaciones" indexId="index">
								<bean:write name="autorizacion" property="reporteSiniestroDTO.polizaDTO.numeroPolizaFormateada"/> |
							</logic:iterate>
						</td>							
					</tr>					
				</table>			
				<table id="t_riesgo" border="0" width="98%">
					<tr>
						<th><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.noreporte" /></th>
						<th>No.<midas:mensaje clave="siniestros.finanzas.pagos.autorizacion" /></th>
						<th><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.asegurado" /></th>
						<th><midas:mensaje clave="siniestros.finanzas.pagos.montoNeto" /></th>					
						<th><midas:mensaje clave="siniestros.finanzas.pagos.fechaGasto" /></th>
					</tr>
					<logic:iterate id="autorizacion" name="soportePagosForm" property="autorizaciones" indexId="index">
						<tr class="bg_t2">
							<td><bean:write name="autorizacion" property="reporteSiniestroDTO.numeroReporte"/></td>
							<td><bean:write name="autorizacion" property="idToAutorizacionTecnica"/></td>
							<td><bean:write name="autorizacion" property="reporteSiniestroDTO.nombreAsegurado"/></td>
							<td><bean:write name="autorizacion" property="montoNeto" format="$#,##0.00"/></td>
							<td><bean:write name="autorizacion" property="gastoSiniestroDTO.fechaGasto" format="dd/MM/yyyy"/></td>
						</tr>				
					</logic:iterate>	
				</table>
				<table id="desplegarDetalle" border="0" width="95%">
					<tr>
						<td width="50%">&nbsp;</td>
						<td width="50%">
							<table  id="t_riesgo" border="0" width="98%">
								<tr>
									<th><midas:mensaje clave="siniestros.finanzas.pagos.montoNeto" /></th>
									<td><bean:write name="soportePagosForm" property="gastoAcumulado.montoGasto" format="$#,##0.00"/></td>
								</tr>
								<tr>
									<th>+ IVA</th>
									<td><bean:write name="soportePagosForm" property="gastoAcumulado.montoIVA" format="$#,##0.00"/></td>
								</tr>								
								<tr>
									<th>- IVA Retenido</th>
									<td><bean:write name="soportePagosForm" property="gastoAcumulado.montoIVARetencion" format="$#,##0.00"/></td>
								</tr>
								<tr>
									<th>- ISR Retenido</th>
									<td><bean:write name="soportePagosForm" property="gastoAcumulado.montoISRRetencion" format="$#,##0.00"/></td>
								</tr>
								<tr>															
									<th>Total</th>
									<td><bean:write name="soportePagosForm" property="gastoAcumulado.montoOtros" format="$#,##0.00"/></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="98%" align="center">
					<tr>
						<td colspan="7">
							<div class="alinearBotonALaDerecha">
								<button type="button" onclick="javascript: parent.ventanaOrdenDePago.close();" style="cursor: pointer;">
									<table style="font-size: 9px;" align="center">
										<tr>
											<td align="center">
												Regresar
											</td>	
										</tr>
									</table>
								</button>																							
								<c:if test="${soportePagosForm.tipoOperacion == 'generar'}">
									<button type="button" onclick="javascript: confirmarGuardarOrdenDePago('${soportePagosForm.soportePagosDTO.idsAutorizaciones}');" style="cursor: pointer;">
										<table style="font-size: 9px;" align="center">
											<tr>
												<td align="center">
													Generar Orden de Pago
												</td>	
											</tr>
										</table>
									</button>																				
								</c:if>
								<c:if test="${soportePagosForm.tipoOperacion == 'cancelar'}">
									<button type="button" onclick="javascript: confirmarCancelarOrdenDePago('${soportePagosForm.soportePagosDTO.numeroOrdenDePago}');" style="cursor: pointer;">
										<table style="font-size: 9px;" align="center">
											<tr>
												<td align="center">
													Cancelar Orden de Pago
												</td>	
											</tr>
										</table>
									</button>																				
								</c:if>					
							</div>
						</td>
					</tr>		
				</table>					
			</logic:notEmpty>
		</logic:notEmpty>
	</midas:formulario>	
</div>
