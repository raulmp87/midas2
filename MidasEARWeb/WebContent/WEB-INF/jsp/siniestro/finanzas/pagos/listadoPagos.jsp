<?xml version="1.0" encoding="ISO-8859-1"?>
<%@ page isELIgnored="false"%>
<%@  page contentType="text/xml" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<rows>
	<head>
		<beforeInit>
			<call command="setImagePath"><param>/MidasWeb/img/dhtmlxgrid/</param></call>
			<call command="setSkin"><param>light</param></call>
		</beforeInit>

		<column id="noReporte" type="ro" width="90" sort="str" align="center"><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.noreporte" /></column>
		<column id="ordenDePago" type="ro" width="150" sort="int" align="center"><midas:mensaje clave="siniestros.finanzas.pagos.noOrdenPago" /></column>
		<column id="proveedor" type="ro" width="180" sort="str" ><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.nombreproveedor" /></column>
		<column id="asegurado" type="ro" width="180" sort="str" ><midas:mensaje clave="siniestros.finanzas.pagos.autorizacion.asegurado" /></column>
		<column id="fechaPago" type="ro" width="100" sort="date" align="center">Fecha <midas:mensaje clave="siniestros.finanzas.pagos.pago" /></column>
		<column id="estatusPago" type="ro" width="80" sort="date" align="center"><midas:mensaje clave="siniestros.finanzas.pagos.estatus" /></column>
		<column id="detalle" type="img" width="30" align="center"></column>			
		<column id="cancelar" type="img" width="30" align="center"></column>
		<column id="imprimir" type="img" width="30" align="center"></column>
	</head>
	<c:choose>
		<c:when test="${soportePagosForm.ordenesDePago != null}">
			<nested:iterate id="ordenDePago" name="soportePagosForm" property="ordenesDePago" indexId="indexVar" type="mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO">
				<row id="<bean:write name="ordenDePago" property="ordenDePagoDTO.idToOrdenPago"/>">
					<cell><bean:write name="ordenDePago" property="autorizacionTecnicaDTO.reporteSiniestroDTO.numeroReporte"/></cell>
					<cell><bean:write name="ordenDePago" property="ordenDePagoDTO.idToOrdenPago"/></cell>
					<cell><bean:write name="ordenDePago" property="autorizacionTecnicaDTO.gastoSiniestroDTO.nombrePrestadorServicios"/></cell>
					<cell><bean:write name="ordenDePago" property="autorizacionTecnicaDTO.reporteSiniestroDTO.nombreAsegurado"/></cell>
					<cell><bean:write name="ordenDePago" property="ordenDePagoDTO.fechaPago" format="dd/MM/yyyy"/></cell>					
					<cell><bean:write name="ordenDePago" property="ordenDePagoDTO.descripcionEstatus"/></cell>
					<cell>/MidasWeb/img/icons/ico_verdetalle.gif^Detalle^javascript: mostrarDetalleVentanaOrdenDePago(<bean:write name="ordenDePago" property="ordenDePagoDTO.idToOrdenPago"/>)^_self</cell>
					<nested:equal value="0" name="ordenDePago" property="ordenDePagoDTO.estatus">
						<cell>/MidasWeb/img/icons/ico_rechazar1.gif^Cancelar^javascript: mostrarCancelacionVentanaOrdenDePago(<bean:write name="ordenDePago" property="ordenDePagoDTO.idToOrdenPago"/>)^_self</cell>
					</nested:equal>		
					<nested:equal value="1" name="ordenDePago" property="ordenDePagoDTO.estatus">
						<cell>/MidasWeb/img/blank.png^ ^javascript: void()^_self</cell>
					</nested:equal>		
					<nested:equal value="2" name="ordenDePago" property="ordenDePagoDTO.estatus">
						<cell>/MidasWeb/img/blank.png^ ^javascript:void()^_self</cell>
					</nested:equal>														
						<cell>/MidasWeb/img/logoPDF_small.png^Imprimir^javascript: imprimirOrdenDePagoV2(<bean:write name="ordenDePago" property="ordenDePagoDTO.idToOrdenPago"/>)^_self</cell>
<%--					<cell>/MidasWeb/img/logoPDF_small.png^Imprimir^javascript: imprimirOrdenDePago(&#34;Esta funcionalidad se encuentra pendiente de desarrollo.&#34;)^_self</cell>--%>
				</row>
			</nested:iterate>		
		</c:when>
		<c:otherwise></c:otherwise>	
	</c:choose>
		
</rows>
