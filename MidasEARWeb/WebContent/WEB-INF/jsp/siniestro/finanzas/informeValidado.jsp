<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<div style="border: 0px solid; width: 250px; margin: auto; text-align: center;">
	<p style="font-size: 16px">El informe ha sido marcado como v&aacute;lido.</p>
	<div style="border: 0px solid; width: 250px; margin: auto; text-align: center;">
		<midas:boton onclick="javascript:document.location.href = '/MidasWeb/sistema/inicio.do';" tipo="regresar" texto="Aceptar" style="width:75px;"/>
	</div>
</div>
