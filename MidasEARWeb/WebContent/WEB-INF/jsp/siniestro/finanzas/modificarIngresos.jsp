<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/finanzas/guardarModificarIngresosSinietro">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="idToIngresoSiniestro" styleId="idToIngresoSiniestro"/>
	<html:hidden property="indIva" styleId="indIva"/>
	<html:hidden property="indIvaRet" styleId="indIvaRet"/>
	<html:hidden property="indIsr" styleId="indIsr"/>
	<html:hidden property="indIsrRet" styleId="indIsrRet"/>
	<html:hidden property="indOtros" styleId="indOtros"/>
	<table id="agregar" border="0" >
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.ingresos.agregar.titulo" />				
			</td>			
		</tr>	
		<tr>
			<td align="center" colspan="8" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.ingresos.agregar.tipoMoneda" />				
				<midas:escribe propiedad="tipoMoneda" nombre="ingresosForm"/>
			</td>
		</tr>
		<tr>
			<td width="5%" >&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="1%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
			<td width="24%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaCobro" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.fechaCobro" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaCobro" soloLectura="true" propiedadFormulario="fechaCobro" caracteres="10" longitud="25"/>
					<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIngreso" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.montoIngreso" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>			
				<logic:equal name="modoLectura" scope="request" value="true">
					<td><midas:texto propiedadFormulario="montoIngreso" id="idMontoIngreso" deshabilitado="true" /></td>				
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">
					<td><midas:texto propiedadFormulario="montoIngreso" id="idMontoIngreso"
						onkeypress="return soloNumeros(this, event, true);" 
						onblur="recalcularPorcentajes(this.value)" 
						onfocus="new Mask('$#,###.00', 'number').attach(this)" /></td>				
				</logic:equal>										
			<td colspan="5">&nbsp;</td>						
		</tr>				
		<%-- <tr>
			<td>&nbsp;</td>
			<th>
				<etiquetas:etiquetaError  property="iva" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.iva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="iva" onkeypress="return soloNumeros(this, event, false);"
			onfocus="new Mask('$#,###.00', 'number').attach(this)"/></td>
			<td colspan="5">&nbsp;</td>
		</tr> --%>		
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right"><midas:mensaje clave="siniestro.finanzas.ingresos.agregar.impuestos"/></td>			
			<logic:equal name="modoLectura" scope="request" value="true">
				<td align="left" colspan="5">
					&nbsp;<midas:checkBox valorEstablecido="1" deshabilitado="true" id="idIndIva">IVA</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="2" deshabilitado="true" id="idIndIvaRet">IVA Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="3" deshabilitado="true" id="idIndIsr">ISR</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="4" deshabilitado="true" id="idIndIsrRet">ISR Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="5" deshabilitado="true" id="idIndOtros">Otros</midas:checkBox>
				</td>						
			</logic:equal>
			<logic:equal name="modoLectura" scope="request" value="false">
				<td align="left" colspan="5">
					&nbsp;<midas:checkBox valorEstablecido="1" onClick="habilitaCamposPorcentaje(this)" id="idIndIva">IVA</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="2" onClick="habilitaCamposPorcentaje(this)" id="idIndIvaRet">IVA Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="3" onClick="habilitaCamposPorcentaje(this)" id="idIndIsr">ISR</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="4" onClick="habilitaCamposPorcentaje(this)" id="idIndIsrRet">ISR Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="5" onClick="habilitaCamposPorcentaje(this)" id="idIndOtros">Otros</midas:checkBox>
				</td>			
			</logic:equal>												
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIva" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.porcentajeIva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIva" id="porcentajeIva" soloLectura="true" />								
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">			
					<midas:texto propiedadFormulario="porcentajeIva" id="porcentajeIva"  
						onkeypress="return soloNumeros(this, event, false);"
						onblur="calculaMontoPorcentaje(this,'montoIva','idMontoIngreso')"/>				
				</logic:equal>			
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIva" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.montoIva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIva" id="montoIva" soloLectura="true" /></td>
			<td colspan="2">&nbsp;</td>
		</tr>
				<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIvaRetencion" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.porcentajeIvaRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIvaRetencion" id="porcentajeIvaRetencion" soloLectura="true"/>				
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeIvaRetencion" id="porcentajeIvaRetencion" onkeypress="return soloNumeros(this, event, false);"
						onkeypress="return soloNumeros(this, event, false);"
						onblur="calculaMontoPorcentaje(this,'montoIvaRetencion','idMontoIngreso')"/>				
				</logic:equal>													
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIvaRetencion" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.montoIvaRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIvaRetencion" id="montoIvaRetencion" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIsr" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.porcentajeIsr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIsr" id="porcentajeIsr" soloLectura="true" />								
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeIsr" id="porcentajeIsr" onkeypress="return soloNumeros(this, event, false);"
										onkeypress="return soloNumeros(this, event, false);"
						onblur="calculaMontoPorcentaje(this,'montoIsr','idMontoIngreso')"/>				
				</logic:equal>									
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIsr" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.montoIsr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIsr" id="montoIsr" soloLectura="true" /></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIsrRetencion" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.porcentajeIsrRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIsrRetencion" id="porcentajeIsrRetencion" soloLectura="true"/>								
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeIsrRetencion" id="porcentajeIsrRetencion" onkeypress="return soloNumeros(this, event, false);"
										onkeypress="return soloNumeros(this, event, false);"
						onblur="calculaMontoPorcentaje(this,'montoIsrRetencion','idMontoIngreso')"/>				
				</logic:equal>								
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIsrRetencion" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.montoIsrRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIsrRetencion" id="montoIsrRetencion" soloLectura="true" /></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeOtros" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.porcentajeOtros" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" soloLectura="true" />								
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" onkeypress="return soloNumeros(this, event, false);"
										onkeypress="return soloNumeros(this, event, false);"
						onblur="calculaMontoPorcentaje(this,'montoOtros','idMontoIngreso')"/>				
				</logic:equal>									
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoOtros" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.montoOtros" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoOtros" id="montoOtros" soloLectura="true" /></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="conceptoIngreso" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.conceptoIngreso" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td colspan="2">&nbsp;			
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:comboCatalogo  propiedad="conceptoIngreso"  styleId="conceptoIngreso" idCatalogo="idTcConceptoIngreso"  descripcionCatalogo="descripcion" nombreCatalogo="tcconceptoingreso" size="1" readonly="true"/>
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:comboCatalogo  propiedad="conceptoIngreso"  styleId="conceptoIngreso" idCatalogo="idTcConceptoIngreso"  descripcionCatalogo="descripcion" nombreCatalogo="tcconceptoingreso" size="1"/>
				</logic:equal>			
							
			</td>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="descripcionIngreso" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.descripcionIngreso" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:areatexto propiedadFormulario="descripcionIngreso" id="descripcionIngreso" renglones="5" columnas="50"/></td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="transferencia" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.tranferencia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="transferencia" id="transferencia" onkeypress="return soloNumeros(this, event, false);"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
		<td colspan="4">&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fichaDeposito" requerido="no"
					key="siniestro.finanzas.ingresos.agregar.fichaDeposito" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="fichaDeposito" onkeypress="return soloNumeros(this, event, false);"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td>
				<midas:boton onclick="regresarListadoIngresos()" tipo="guardar" texto="Regresar" style="width:80px;"/>
			</td>	
			<td colspan="3">
				<midas:boton onclick="guardarModificarIngresos(document.ingresosForm)" tipo="guardar" texto="Guardar Ingreso" style="width:160px;"/>&nbsp;&nbsp;&nbsp;				
			</td> 
			<td colspan="2">&nbsp;</td>
		</tr>
	</table>
</midas:formulario>