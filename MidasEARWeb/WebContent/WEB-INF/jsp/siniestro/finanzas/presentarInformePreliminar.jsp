<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/presentarInformePreliminar">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:oculto propiedadFormulario="idDeReporte"/>
				<midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.tipoMoneda" />				
			</td>			
			<td align="left">
				<midas:escribe propiedad="tipoMoneda" nombre="validarInformePreliminarForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.valido" />
			</td>
			<td align="left" class="datoTabla">				
				<midas:radio propiedadFormulario="valido" valorEstablecido="true">
					<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.si"/>
				</midas:radio>
				<midas:radio propiedadFormulario="valido" valorEstablecido="false">
					<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.no"/>
				</midas:radio>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>	
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<%--<tr>
			<td colspan="5" align="center">
				<div  style="width: 100%; height: 250px; border: 1px solid; overflow: auto;" class="tableContainer">
					<table width="100%" border="0" class="tablaConResultados" >
						<logic:empty name="validarInformePreliminarForm" property="listadoCoberturas">
							<tr>
	                            <td class="" align="center" colspan="7">
									<midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.listaVacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="validarInformePreliminarForm" property="listadoCoberturas">
							<thead>
								<tr>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.inciso" /></th>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.subInciso" /></th>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.seccion" /></th>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.cobertura" /></th>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.riesgo" /></th>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.sumaAsegurada" /></th>
									<th><midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.tipoSA" /></th>
								</tr>
							</thead>
							<logic:iterate name="validarInformePreliminarForm" property="listadoCoberturas" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="inciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="subInciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="cobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="sumaAsegurada" nombre="lista" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoSA" nombre="lista"/>
                                    </td>
								</tr>
							</logic:iterate>
<!--								<tr>-->
<!--									<td align="right" colspan="5" class="datoTabla">-->
<!--										<midas:mensaje clave="siniestro.finanzas.validarInformePreliminar.lstCoberturas.total" />-->
<!--									</td>-->
<!--									<td class="datoTabla" align="left">-->
<!--										<midas:escribe propiedad="total" nombre="validarInformePreliminarForm"/>-->
<!--									</td>-->
<!--									<td>&nbsp;</td>-->
<!--								</tr>-->
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr> --%>				
		<tr>
			<td colspan="5" align="center">
				<div  style="width:80%;height:150px;border :1px ,solid;" class="tableContainer">
					<table width="100%;" style="white-space: normal;" border="0" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.inciso" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.seccion" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.polizas.coberturaRiesgo.lista.afectar" /></th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<logic:empty name="validarInformePreliminarForm" property="listado">
							<tr>
	                            <td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="validarInformePreliminarForm" property="listado">
							<logic:iterate name="validarInformePreliminarForm" property="listado" id="lista"  indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="numeroInciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcionSubInciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                       <midas:escribe propiedad="descripcionSeccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcionCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcionRiesgo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                    	<midas:escribe propiedad="sumaAsegurada" formato="$###,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td>
                                    	<input type="checkbox" name="coberturasRiesgo" id="coberturasRiesgo" value='<bean:write name="indice"/>' />
                                    </td>
								</tr>
							</logic:iterate>								
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>							
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center">
				<midas:boton onclick="javascript: procedeAnalisisInternoInformePreliminar();" tipo="guardar" texto="Procede an&aacute;lisis interno" style="width:160px;"/>
			</td>
			<td>
				<midas:boton onclick="javascript: sendRequest(document.validarInformePreliminarForm, '/MidasWeb/siniestro/finanzas/noProcedeElAnalisisInterno.do', 'contenido', 'mensajeNoProcedeAnalisisInterno()');" tipo="guardar" texto="No procede an&aacute;lisis interno" style="width:180px;"/>
			</td>
			<td>
				<midas:boton onclick="javascript:document.location.href = '/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=cabinero';" tipo="regresar" texto="Cancelar" style="width:75px;"/>
			</td>
			<td >&nbsp;</td>
		</tr>
		
 	</table>
</midas:formulario>
