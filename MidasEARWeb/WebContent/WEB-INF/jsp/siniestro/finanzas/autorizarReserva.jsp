<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/mostrarReservaAutorizar">
	<html:hidden property="idToReservaEstimada" styleId="idToReservaEstimada"/>
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="totalReservaAutorizar" styleId="totalReservaAutorizar"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.tipoMoneda" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="tipoMoneda" nombre="reservaForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<th class="seccionTitulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.listaSumaAsegurada" />
			</th>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;height:150px;border :1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.sumaAsegurada" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.sumaAseguradaDisponible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.tipoSA" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.cobertura.basica" /></th>
							</tr>
						</thead>
						<logic:empty name="reservaForm" property="listaSumaAseguradaCobertura">
							<tr>
	                            <td class="" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="reservaForm" property="listaSumaAseguradaCobertura">
							<logic:iterate name="reservaForm" property="listaSumaAseguradaCobertura" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="coberturaBasicaSoporteDanosDTO.sumaAsegurada" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
                                   <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="sumaAseguradaDisponible" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="nombreCoberturaBasica" nombre="lista"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="3" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.total" />
			</td>
			<td class="datoTabla" align="left">
				<midas:escribe propiedad="totalSumaAseguradaFormato" nombre="reservaForm" />
			</td>
			<td>&nbsp;</td>
		</tr>				
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<th class="seccionTitulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.listaReservaActual" />
			</th>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;height:150px;border :1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.reserva" /></th>
							</tr>
						</thead>
						<logic:empty name="reservaForm" property="listaEstimacionInicialCobertura">
							<tr>
	                            <td class="" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="reservaForm" property="listaEstimacionInicialCobertura">
							<logic:iterate name="reservaForm" property="listaEstimacionInicialCobertura" id="lstReservaActual" indexId="indiceRAct" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.id.numeroinciso" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.id.numerosubinciso" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="lstReservaActual"/>                                        
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                    	<logic:equal value="inicial" name="reservaForm" property="identificador">
                                    		<label>$0.00 </label>
                                    	</logic:equal>
                                    	<logic:notEqual value="inicial" name="reservaForm" property="identificador">
                                    		<midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="lstReservaActual" formato="$###,###,##0.00"/>
                                    	</logic:notEqual>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="4" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.total" />
			</td>
			<td class="datoTabla">
				<midas:escribe propiedad="totalEstimacionInicialFormato" nombre="reservaForm"/>
			</td>
			<td>&nbsp;</td>
		</tr>				
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<th class="seccionTitulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.listaReservaAutorizar" />
			</th>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;height:150px;border :1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.reserva" /></th>
							</tr>
						</thead>
						<logic:empty name="reservaForm" property="listaReservaPorAutorizar">
							<tr>
	                            <td class="" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="reservaForm" property="listaReservaPorAutorizar">
							<logic:iterate name="reservaForm" property="listaReservaPorAutorizar" id="lstAutorizar" indexId="indiceRAut" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.id.numeroinciso" nombre="lstAutorizar"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.id.numerosubinciso" nombre="lstAutorizar"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="lstAutorizar"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="lstAutorizar"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="lstAutorizar" />
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="lstAutorizar" formato="$###,###,##0.00"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="4" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.total" />
			</td>
			<td class="datoTabla">
				<midas:escribe propiedad="totalReservaAutorizarFormato" nombre="reservaForm" />
			</td>
			<td>&nbsp;</td>
		</tr>			
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.tipoAutorizacion" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="tipoAjuste" nombre="reservaForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.descripcionAjuste" />				
			</td >
			<td width="20%"align="left" >
				<midas:areatexto propiedadFormulario="descripcionEstimacion" nombreFormulario="reservaForm" renglones="4" deshabilitado="true"/>
			</td>
			<td width="20%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td  align="center">
				<midas:boton onclick="autorizarReserva()" tipo="guardar" texto="Autorizar reserva" style="width:127px;"/>
			</td>
			<td >
				<midas:boton onclick="listarReportesSiniestro()" tipo="regresar" texto="Cancelar" style="width:75px;"/>
			</td>
			<td colspan="2" >&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
