<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/cartarechazo/mostrarImprimirCartaRechazo">
 	<table width="90%" border="0">
 		<tr>
			<td width="5%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="siniestro.documento.imprimirCartaRechazo.titulo" />			
			</td>
		</tr>	
		<tr>
			<td class="datoTabla" colspan="6">
				<midas:mensaje clave="siniestro.documento.asociarCartaRechazo.datosReporte" /><br/><br/>	
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.numeroReporte" nombre="documentoSiniestroForm"/>
			</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroPoliza" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="numeroPoliza" nombre="documentoSiniestroForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td></td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaSiniestro" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.fechaSiniestro" nombre="documentoSiniestroForm" formato="dd/MM/yyyy HH:mm:ss"/>
			</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.descripcionSiniestro" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.descripcionSiniestro" nombre="documentoSiniestroForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td></td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarfechaReporte" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.fechaHoraReporte" nombre="documentoSiniestroForm" formato="dd/MM/yyyy HH:mm:ss"/>
			</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.observaciones" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.observaciones" nombre="documentoSiniestroForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td class="datoTabla" colspan="6">
				<midas:oculto propiedadFormulario="idReporteSiniestro"/>
				<input type="hidden" id="mensaje" name="mensaje" value="<midas:escribe propiedad="mensaje" nombre="documentoSiniestroForm"/>">
				<input type="hidden" id="tipoMensaje" name="tipoMensaje" value="<midas:escribe propiedad="tipoMensaje" nombre="documentoSiniestroForm"/>">
				<br/><br/>
				<midas:mensaje clave="siniestro.documento.aprobarCartaRechazo.download" /> : &nbsp;&nbsp;
				<a href="javascript: void(0);"
				onclick="window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo=<midas:escribe propiedad="idControlArchivo" nombre="documentoSiniestroForm"/>', 'download');">
				<midas:escribe propiedad="nombreDocumento" nombre="documentoSiniestroForm"/>
				<img border='0px' title='Descargar' src='/MidasWeb/img/download-icon.jpg'/></a>
				<br/><br/>
			</td>
		</tr>
		<tr>
			<td colspan="6">			
				<midas:boton texto="Marcar como impresa la carta de rechazo" style="width:300px;" onclick="sendRequest(document.documentoSiniestroForm,'/MidasWeb/siniestro/finanzas/cartarechazo/impresaCartaRechazo.do', 'contenido', 'redireccionaCartaRechazoRechazada();');" tipo="guardar"/>
			</td>
		</tr>
 	</table>
</midas:formulario>
