<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/cartarechazo/asociarCartaRechazo">
 	<table width="90%" border="0">
 		<tr>
			<td width="5%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="siniestro.documento.asociarCartaRechazo.titulo" />			
			</td>
		</tr>	
		<tr>
			<td class="datoTabla" colspan="6">
				<midas:mensaje clave="siniestro.documento.asociarCartaRechazo.datosReporte" /><br/><br/>	
			</td>
		</tr>
		<tr>
			<td></td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.numeroReporte" nombre="documentoSiniestroForm"/>
			</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroPoliza" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="numeroPoliza" nombre="documentoSiniestroForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td></td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.fechaSiniestro" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.fechaSiniestro" nombre="documentoSiniestroForm" formato="dd/MM/yyyy HH:mm:ss"/>
			</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.descripcionSiniestro" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.descripcionSiniestro" nombre="documentoSiniestroForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td></td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.listarfechaReporte" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.fechaHoraReporte" nombre="documentoSiniestroForm" formato="dd/MM/yyyy HH:mm:ss"/>
			</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.cabina.reportesiniestro.observaciones" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="reporteSiniestroDTO.observaciones" nombre="documentoSiniestroForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td class="datoTabla" colspan="6">
				<midas:oculto propiedadFormulario="idReporteSiniestro"/>
				<midas:oculto propiedadFormulario="idTipoDocumento"/>
				<br/><br/><midas:boton texto="Asociar carta de rechazo" style="width:200px;" onclick="sendRequest(document.documentoSiniestroForm,'/MidasWeb/siniestro/documentos/mostrarAgregarDocumentoSiniestro.do', 'contenido',null);" tipo="guardar"/>
			</td>
		</tr>
 	</table>
</midas:formulario>
