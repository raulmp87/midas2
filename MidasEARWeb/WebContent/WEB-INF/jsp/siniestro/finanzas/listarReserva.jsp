<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/cabina/detalleReserva.do">
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.listarReserva.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" >
				<midas:mensaje clave="siniestro.finanzas.listarReserva.sumaReserva" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="sumaAsegurada" nombre="listarReservaForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>	
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" >
				<midas:mensaje clave="siniestro.finanzas.listarReserva.tipoMoneda" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="tipoMoneda" nombre="listarReservaForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>	
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;height:250px;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.fecha" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.tipo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.cuota" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.excedente" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.facultativo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.retencion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.reserva" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.descripcion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaModidicaciones.ver" /></th>
							</tr>
						</thead>
						<logic:empty name="listarReservaForm" property="listaModificacionesReserva">
							<tr>
	                            <td class="" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="listarReservaForm" property="listaModificacionesReserva">
							<logic:iterate name="listarReservaForm" property="listaModificacionesReserva" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor1" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor2" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor3" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor4" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor5" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor6" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor7" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor8" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left"> 
                                        <html:radio property="valorRadio" value="lista" onclick="submit();"></html:radio>
                                       
                                    <br></td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;height:250px;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaDetalle.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaDetalle.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaDetalle.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaDetalle.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaDetalle.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.modificarReserva.listaDetalle.reserva" /></th>
							</tr>
						</thead>
						<logic:empty name="listarReservaForm" property="listaDetalleReserva">
							<tr>
	                            <td class="" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="listarReservaForm" property="listaDetalleReserva">
							<logic:iterate name="listarReservaForm" property="listaDetalleReserva" id="listaDetalle" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor1" nombre="listaDetalle"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor2" nombre="listaDetalle"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor3" nombre="listaDetalle"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor4" nombre="listaDetalle"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor5" nombre="listaDetalle"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="valor6" nombre="listaDetalle"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
 	</table>
</midas:formulario>
