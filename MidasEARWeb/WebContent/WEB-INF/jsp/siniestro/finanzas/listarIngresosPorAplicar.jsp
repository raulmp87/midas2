<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/finanzas/listarIngresosPorAplicar">	
	<table width="95%" border="0" cellspacing="4" cellpadding="0" id="filtros">
	  <tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.ingresos.aplicacioncontable.titulo"/>
			</td>
	  </tr>
	  <tr>
	    <td width="13%" align="right">
	      <midas:mensaje clave="siniestro.cabina.reportesiniestro.numeroReporte"/>
		</td>
	    <td width="13%">
	    	<midas:texto propiedadFormulario="idToReporteSiniestro" id="idToReporteSiniestro" onkeypress="return soloNumeros(this, event, false)" />
	    </td>
	    <td width="12%" align="right">
	    	<midas:mensaje clave="siniestro.finanzas.ingresos.aplicacioncontable.criterio.fechaIngreso"/>
	    </td>
	    <td width="14%">
	    	<midas:texto propiedadFormulario="fechaCobro" id="fechaCobro" soloLectura="true"/>
    		<image src="<html:rewrite page='/img/b_calendario.gif'/>" border=0 />
	    </td>
	    <td width="48%">&nbsp;</td>
	  </tr>
	  <tr>
	    <td align="right">
	      <midas:mensaje clave="siniestro.finanzas.ingresos.aplicacioncontable.criterio.monto"/>
	    </td>
	    <td>
	    	<midas:texto propiedadFormulario="montoIngreso" id="montoIngreso" onkeydown="setDecimalForLength(this,7)"
						onfocus="new Mask('$#,###.00', 'number').attach(this)" onkeypress="return soloNumeros(this, event, true)" />
	    </td>
	    <td align="right">
	      	<midas:mensaje clave="siniestro.finanzas.ingresos.aplicacioncontable.criterio.conceptoIngreso"/>
	    </td>
	    <td>
	    	<midas:comboCatalogo  propiedad="conceptoIngreso"  styleId="conceptoIngreso" idCatalogo="idTcConceptoIngreso"  
	    		descripcionCatalogo="descripcion" nombreCatalogo="tcconceptoingreso" size="1" />
	    </td>
	</tr>
  	  <tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: mostrarListaIngresosPorAplicarGrids();">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
	  </tr>
  	  
	</table>
	<div id="indicadorIngresosPorAplicar"></div>
	<div id="listaIngresosGrid" style="width: 95%;height: 300px;"></div>
	<div id="pagingArea"></div><div id="infoArea"></div>
	
	<midas:oculto nombreFormulario="ingresosForm" propiedadFormulario="idToReporteSiniestro" />
	
</midas:formulario>
