<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/finanzas/estimarReserva">
	<html:hidden property="totalSumaAsegurada" styleId="totalSumaAsegurada"/>
	<html:hidden property="tipoCambioPromedio" styleId="tipoCambioPromedio"/>
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.estimarReserva.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.estimarReserva.tipoMoneda" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="tipoMoneda" nombre="reservaForm"/>
			</td>
			<td colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:100%;height:150px;border :1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.sumaAsegurada" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.sumaAseguradaDisponible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstSumaAsegurada.tipoSA" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.cobertura.basica" /></th>
							</tr>
						</thead>
						<logic:empty name="reservaForm" property="listaSumaAseguradaCobertura">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="reservaForm" property="listaSumaAseguradaCobertura">
							<logic:iterate name="reservaForm" property="listaSumaAseguradaCobertura" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="coberturaBasicaSoporteDanosDTO.sumaAsegurada" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="sumaAseguradaDisponible" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="nombreCoberturaBasica" nombre="lista"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="3" class="datoTabla"><midas:mensaje clave="siniestro.finanzas.estimarReserva.total" /></td>
			<td class="datoTabla" align="left">
				<midas:escribe propiedad="totalSumaAseguradaFormato" nombre="reservaForm" formato="$###,###,##0.00"/>
			</td>
			<td width="20%" >&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:80%;height:150px;border :1px ,solid;overflow: auto; " class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstEstimacionInicial.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstEstimacionInicial.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstEstimacionInicial.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstEstimacionInicial.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstEstimacionInicial.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.estimarReserva.lstEstimacionInicial.estimacion" /></th>
							</tr>
						</thead>
						<logic:empty name="reservaForm" property="listaEstimacionInicialCobertura">
							<tr>
	                            <td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="reservaForm" property="listaEstimacionInicialCobertura">
							<logic:iterate name="reservaForm" property="listaEstimacionInicialCobertura" id="listaEstimacion" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="listaEstimacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="listaEstimacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                       <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="listaEstimacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="listaEstimacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="listaEstimacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                       	<midas:texto  id="estimacionInicial" propiedadFormulario="estimacion"
                                       		onkeypress="return soloNumeros(this, event, true);"
                                       		onfocus="new Mask('$#,###.00', 'number').attach(this)"
                                       		onblur="realizaSumaEstimada()" caracteres="17"/>
                                    </td>
                                    
								</tr>
							</logic:iterate>
								
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" colspan="3" class="datoTabla"><midas:mensaje clave="siniestro.finanzas.estimarReserva.total" /></td>
			<td class="datoTabla">
				$<midas:texto id="idTotalEstimacionInicial"  propiedadFormulario="totalEstimacionInicial" deshabilitado="true"/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.estimarReserva.descripcionEstimacion" />				
			</td >
			<td width="20%"align="left" >
				<midas:areatexto propiedadFormulario="descripcionEstimacion" nombreFormulario="reservaForm" renglones="4"/>
			</td>
			<td colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td >&nbsp;</td>
			<td  align="center">
				<midas:boton onclick="validarInformacionReserva()" tipo="guardar" texto="Guardar estimaci&oacute;n" style="width:140px;"/>
			</td>
			<td >
				<midas:boton onclick="" tipo="regresar" texto="Cancelar" style="width:75px;"/>
			</td>
			<td colspan="2" >&nbsp;</td>
		</tr>
		
 	</table>
</midas:formulario>
