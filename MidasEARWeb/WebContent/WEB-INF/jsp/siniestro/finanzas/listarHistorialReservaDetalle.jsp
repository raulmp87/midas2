<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/listarReservaDetalle">	
<table style="white-space:normal;" class="tablaConResultados" >
	<thead>
		<tr>
			<th colspan="10" align="center"><midas:mensaje clave="siniestro.finanzas.historialReserva.listaDetalle" /></th>
		</tr>
		<tr>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>			
			<th ><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoCuotaParte" /></th>
			<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoPrimExc" /></th>
			<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoFacultativo" /></th>
			<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoRetencion" /></th>					
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.reserva" /></th>
		</tr>
	</thead>
	<logic:empty name="historialDetalleReservaForm" property="listaReservaDetalleBean">
		<tr>
			<td class="datoTabla" align="center" colspan="10">
				<midas:mensaje clave="lista.vacia" />
			</td>
		</tr>
	</logic:empty>
	<logic:notEmpty name="historialDetalleReservaForm" property="listaReservaDetalleBean">
		<logic:iterate name="historialDetalleReservaForm" property="listaReservaDetalleBean" id="listaDetalle">
			<tr>
				<td class="datoTabla" align="left">
                    <midas:escribe propiedad="reservaDetalleDTO.id.numeroinciso" nombre="listaDetalle"/>
                </td>
                <td class="datoTabla" align="left">
                    <midas:escribe propiedad="reservaDetalleDTO.id.numerosubinciso" nombre="listaDetalle"/>
                </td>
                <td class="datoTabla" align="left">
                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="listaDetalle"/>
                </td>
                <td class="datoTabla" align="left">
                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="listaDetalle"/>
                </td>
                <td class="datoTabla" align="left">
                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="listaDetalle"/>
                </td>
                <td class="datoTabla" align="right">
                    <midas:escribe propiedad="montoCuotaParte" formato="$###,###,##0.00" nombre="listaDetalle"/>
                </td>																		
                <td class="datoTabla" align="right">
                    <midas:escribe propiedad="montoPrimerExcedente" formato="$###,###,##0.00" nombre="listaDetalle"/>
                </td>																		
                <td class="datoTabla" align="right">
                	<midas:escribe propiedad="montoFacultativo" formato="$###,###,##0.00" nombre="listaDetalle"/>
                </td>
                <td class="datoTabla" align="right">
                    <midas:escribe propiedad="montoRetencion" formato="$###,###,##0.00" nombre="listaDetalle"/>
                </td>                
                <td class="datoTabla" align="left">
                    <midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="listaDetalle" formato="$###,##0.00" local="localeMexico"/>
                </td>
			</tr>
		</logic:iterate>
	</logic:notEmpty>
		
</table>			
</midas:formulario>
