<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:formulario accion="/siniestro/finanzas/listarReserva">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="95%" border="0">
 	
 		<tr>
			<td width="10%">&nbsp;</td>
			<td width="40%">&nbsp;</td>
			<td width="40%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.titulo" />			
			</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.sumaAsegurada" />	:			
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="sumaAsegurada" nombre="historialReservaForm" formato="$###,###,##0.00"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.tipoMoneda" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="tipoMoneda" nombre="historialReservaForm"/>
			</td>
			<td></td>		
		</tr>		
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" align="center">
				<div style="width:900px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" align="center">
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.historialReserva.listaModificaciones" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.fechaMovimiento" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.tipoMovimiento" /></th>
								<%-- <th><midas:mensaje clave="siniestro.finanzas.historialReserva.cuotaParte" /></th> --%>
								<%-- <th><midas:mensaje clave="siniestro.finanzas.historialReserva.primerExc" /></th> --%>
								<%-- <th><midas:mensaje clave="siniestro.finanzas.historialReserva.facultativo" /></th> --%>
								<%-- <th><midas:mensaje clave="siniestro.finanzas.historialReserva.retencion" /></th> --%>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.reserva" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.descripcion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.ver" /></th>
							</tr>
						</thead>
						<logic:empty name="historialReservaForm" property="modificacionesReserva">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="historialReservaForm" property="modificacionesReserva">
							<logic:iterate name="historialReservaForm" property="modificacionesReserva" id="lista">
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDTO.fechaestimacion" nombre="lista" formato="dd/MM/yyyy HH:mm:ss" local="localeMexico"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcionTipoMovimiento" nombre="lista"/>
                                    </td>
                                    <%-- <td class="datoTabla" align="left">
                                        0
                                    </td>
                                    <td class="datoTabla" align="left">
                                        0
                                    </td>
                                    <td class="datoTabla" align="left">
                                        0
                                    </td>
                                    <td class="datoTabla" align="left">
                                        0
                                    </td> --%>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="montoReserva" nombre="lista" formato="$###,###,##0.00" local="localeMexico"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDTO.descripcion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                    	<input type="radio" name="reservaSeleccionada" value="<midas:escribe propiedad="reservaDTO.idtoreservaestimada" nombre="lista"/>" onclick="mostarDetalleHistorialReserva()">
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
</midas:formulario>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" align="center">
				<div id="detalleHistorialRes" style="width:900px;height:150px;border :1px,solid;display:none;overflow: auto;" class="tableContainer">
					
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<logic:equal name="pantallaOrigen" scope="request" value="informeFinal">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href='javascript: void(0);' 
								onclick='javascript :mostrarAprobarInformeFinal();'>
								<midas:mensaje clave="midas.accion.regresar"/>
							</a>
						</div>
					</div>
				</logic:equal>
				<logic:notEqual name="pantallaOrigen" scope="request" value="informeFinal">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href='javascript: void(0);' 
								onclick='javascript :listarReportesSiniestro();'>
								<midas:mensaje clave="midas.accion.regresar"/>
							</a>
						</div>
					</div>
				</logic:notEqual>
			</td>
		</tr>
 	</table>

