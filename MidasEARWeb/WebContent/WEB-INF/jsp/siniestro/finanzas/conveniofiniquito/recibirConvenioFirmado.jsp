<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/siniestro/finanzas/conveniofiniquito/mostrarRecibirConvenioFirmado">
 	<midas:oculto propiedadFormulario="idToConvenioFiniquito"/>
 	<midas:oculto propiedadFormulario="idToIndemnizacion"/> 	
 	<html:hidden property="pagoConCheque" styleId="valorPagoConCheque"/>
 	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>

	<table id="agregar" >
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.recibirFirmado" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="numReporteSiniestro"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.numReporteSiniestro" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="numReporteSiniestro" soloLectura="true"/></td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="fechaReporte"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.fechaReporte" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="fechaReporte" soloLectura="true"/></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="ajustador"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.ajustador" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>			
			<td><midas:texto propiedadFormulario="ajustador" soloLectura="true"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="numPoliza"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.numPoliza" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="numPoliza" soloLectura="true"/></td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="claveProducto"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.claveProducto" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="claveProducto" soloLectura="true"/></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="claveSubRamo"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.claveSubRamo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>			
			<td><midas:texto propiedadFormulario="claveSubRamo" soloLectura="true"/></td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="fechaInicioVigencia"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.fechaInicioVigencia" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="fechaInicioVigencia" soloLectura="true"/></td>		
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="fechaFinVigencia"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.fechaFinVigencia" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="fechaFinVigencia" soloLectura="true"/></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="asegurado"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.asegurado" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto propiedadFormulario="asegurado" soloLectura="true"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="descripcionDanio"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.descripcionDanio" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="5">
				<midas:areatexto propiedadFormulario="descripcionDanio" id="descripcionDanio" renglones="3" onKeyDown="return false;"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="indemnizacion"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.indemnizacion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:texto propiedadFormulario="indemnizacion" id="indemnizacion" soloLectura="true"/>
			</td>	
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="indemnizacionLetra"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.indemnizacionLetra" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:areatexto propiedadFormulario="indemnizacionLetra" id="indemnizacionLetra" renglones="3" onKeyDown="return false;"/>
			</td>	
		</tr>
		
		<tr>
			<td colspan="6" align="left">
				<midas:checkBox id="pagoConCheque" valorEstablecido="0" propiedadFormulario="pagoConCheque" onClick="isPagoChequeConvenio();">
					<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.cheque" />
				</midas:checkBox>
			</td>
		</tr>
		
		
		<tr>
			<td style="text-align: right;">
				<midas:radio propiedadFormulario="tipoCuenta" valorEstablecido="cuentaBanco" deshabilitado="true">
					<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.cuentaBancaria" />
				</midas:radio>
			</td>
			<td style="text-align: left;">
				<midas:radio propiedadFormulario="tipoCuenta" valorEstablecido="clabe" deshabilitado="true">
					<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.clabe"/>
				</midas:radio>
			</td>
			<td></td>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="numeroCuenta"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.numero" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:texto propiedadFormulario="numeroCuenta" id="numeroCuenta" soloLectura="true"/>
			</td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="fechaConvenio"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.fechaConvenio" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<table>
					<tr>
						<td><midas:texto propiedadFormulario="fechaConvenio" id="fechaConvenio" soloLectura="true"/></td>
						<td><img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/></td>
					</tr>
				</table>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="bancoReceptor"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.bancoReceptor" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:texto propiedadFormulario="bancoReceptor" id="bancoReceptor" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="lugarConvenio"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.lugarConvenio" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:areatexto propiedadFormulario="lugarConvenio" id="lugarConvenio" renglones="3" onKeyDown="return false;"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="nombreBeneficiario"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.nombreBeneficiario" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:texto propiedadFormulario="nombreBeneficiario" id="nombreBeneficiario" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="tipoMoneda"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.tipoMoneda" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:texto propiedadFormulario="tipoMoneda" id="tipoMoneda" soloLectura="true"/>
			</td>
			<td>
				<etiquetas:etiquetaError 
					requerido="no" 
					property="nombreBeneficiario"
					name="convenioFiniquitoForm"
					key="siniestro.finanzas.conveniofiniquito.sumaAsegurada" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td colspan="2">
				<midas:texto propiedadFormulario="sumaAsegurada" id="sumaAsegurada" soloLectura="true"/>
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<!-- 
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick='javascript:mostrarSalvamentosPorReporteSiniestro(1);'>
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					 -->
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick='javascript: recibirConvenioFirmado(<midas:escribe propiedad="idToReporteSiniestro" nombre="convenioFiniquitoForm"/>);'>
							<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.recibirConvenio"/>
						</a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>
