<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario
	accion="/siniestro/finanzas/conveniofiniquito/mostrarGenerarConvenioFiniquito">
	<midas:oculto propiedadFormulario="idToReporteSiniestro" />
	<midas:oculto propiedadFormulario="idToConvenioFiniquito" />
	<midas:oculto propiedadFormulario="idToIndemnizacion" />
	<html:hidden property="pagoConCheque" styleId="valorPagoConCheque"/>

	<table id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje	clave="siniestro.finanzas.conveniofiniquito.generarFiniquito" />
			</td>
		</tr>
		<tr>
			<th width="15">
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.numReporteSiniestro" />:
			</th>
			<td width="15%">
				<midas:texto propiedadFormulario="numReporteSiniestro"	soloLectura="true" />
			</td>
			<th width="10%">
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.fechaReporte" />:
			</th>
			<td width="15%">
				<midas:texto propiedadFormulario="fechaReporte" soloLectura="true" />
			</td>
			<th width="15%">
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.ajustador" />:
			</th>
			<td width="30%">
				<midas:texto propiedadFormulario="ajustador" soloLectura="true" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.numPoliza" />:
			</th>
			<td>
				<midas:texto propiedadFormulario="numPoliza" soloLectura="true" />
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.claveProducto" />:
			</th>
			<td>
				<midas:texto propiedadFormulario="claveProducto" soloLectura="true" />
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.claveSubRamo" />:
			</th>
			<td>
				<midas:texto propiedadFormulario="claveSubRamo" soloLectura="true" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.fechaInicioVigencia" />:
			</th>
			<td>
				<midas:texto propiedadFormulario="fechaInicioVigencia" soloLectura="true" />
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.fechaFinVigencia" />:
			</th>
			<td>
				<midas:texto propiedadFormulario="fechaFinVigencia" soloLectura="true" />
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.asegurado" />:
			</th>
			<td>
				<midas:texto propiedadFormulario="asegurado" soloLectura="true" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.descripcionDanio" />:
			</th>
			<td colspan="5">
				<midas:areatexto propiedadFormulario="descripcionDanio" id="descripcionDanio" renglones="3" caracteres="254" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.indemnizacion" />:
			</th>
			<td colspan="2">
				<midas:texto propiedadFormulario="indemnizacion" id="indemnizacion" soloLectura="true" />
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.indemnizacionLetra" />:
			</th>
			<td colspan="2">
				<midas:areatexto propiedadFormulario="indemnizacionLetra" id="indemnizacionLetra" renglones="3" onKeyDown="return false;" />
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left">
				<midas:checkBox id="pagoConCheque" valorEstablecido="0" propiedadFormulario="pagoConCheque" onClick="isPagoChequeConvenio();">
					<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.cheque" />
				</midas:checkBox>
			</td>
		</tr>




		<tr>
			<td style="text-align: right;">
				<midas:radio propiedadFormulario="tipoCuenta" valorEstablecido="cuentaBanco"> 
					<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.cuentaBancaria" />
				</midas:radio>
			</td>
			<td style="text-align: left;">
				<midas:radio propiedadFormulario="tipoCuenta" valorEstablecido="clabe">
					<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.clabe" />
				</midas:radio>
			</td>
			<td>&nbsp;</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.numero" />:
			</th>
			<td colspan="2">
				<midas:texto propiedadFormulario="numeroCuenta" id="numeroCuenta" caracteres="29" onkeypress="return soloNumeros(this, event, false)" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.fechaConvenio" />:
			</th>
			<td colspan="2">
				<table>
					<tr>
						<td>
							<midas:texto propiedadFormulario="fechaConvenio" id="fechaConvenio" soloLectura="true" />
						</td>
						<td>
							<img src="<html:rewrite page='/img/b_calendario.gif'/>" width="12" height="12" style="cursor: hand;" />
						</td>
					</tr>
				</table>
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.bancoReceptor" />:
			</th>
			<td colspan="2">
				<midas:texto propiedadFormulario="bancoReceptor" id="bancoReceptor" caracteres="99" onkeypress="return soloLetras(this, event, false)" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.lugarConvenio" />:
			</th>
			<td colspan="2">
				<midas:areatexto propiedadFormulario="lugarConvenio" id="lugarConvenio" caracteres="99" renglones="3" />
			</td>
			<th>
				<midas:mensaje clave="siniestro.finanzas.conveniofiniquito.nombreBeneficiario" />:
			</th>
			<td colspan="2">
				<midas:texto propiedadFormulario="nombreBeneficiario" id="nombreBeneficiario" caracteres="99" onkeypress="return soloLetras(this, event, false)" />
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td class="guardar">			
				<div class="alinearBotonALaDerecha">													
                     <logic:notEqual name="convenioFiniquitoForm" property="estatus" value="0">
						<div id="b_regresar">
							<a onclick="javascript: listarReportesSiniestro();" href="javascript: void(0);">
								<midas:mensaje clave="midas.accion.regresar"/>
							</a>
						</div>
                     </logic:notEqual>
					 <logic:equal name="convenioFiniquitoForm" property="estatus" value="0"> 
						<div id="b_guardar">
							<a href="javascript: void(0);"
								onclick='javascript:generarConvenioFiniquito(<midas:escribe propiedad="idToReporteSiniestro" nombre="convenioFiniquitoForm"/>);'>
								<midas:mensaje
									clave="siniestro.finanzas.conveniofiniquito.generarConvenio" />
							</a>
						</div>
					</logic:equal>                                                                                                                                                      						 					 				
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>
