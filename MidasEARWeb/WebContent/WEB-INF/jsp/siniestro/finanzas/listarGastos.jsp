<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/gastosAjuste">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.gasto.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.gasto.tipoMoneda" />:				
			</td >
			<td width="20%"align="left" class="datoTabla">
				<midas:escribe propiedad="tipoMoneda" nombre="gastosForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:880px;height:150px;border :1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.fechaGasto" /></th>								
								<th><midas:mensaje clave="siniestro.finanzas.gasto.concepto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.beneficiario" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.montoGasto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.descripcion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.numeroCheque" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.estatus" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.eliminar" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.cancelar" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gasto.modificar" /></th>								
							</tr>
						</thead>
						<logic:empty name="gastosForm" property="listaGastos">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="gastosForm" property="listaGastos">
							<logic:iterate name="gastosForm" property="listaGastos" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="fechaGasto" nombre="lista" formato="dd/MM/yyyy"/>                                        
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="conceptoGasto.descripcion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="nombrePrestadorServicios" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="montoGasto" nombre="lista" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="numeroCheque" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="estatusFinanzas.descripcion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                    	<logic:equal name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                    		<input type="checkbox" name="eliminarGasto" id="eliminarGasto" value="<midas:escribe propiedad="idToGastoSiniestro" nombre="lista"/>"/>
                                    	</logic:equal>
                                    	
                                    	<logic:notEqual name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                    		<input type="checkbox" name="eliminarGasto" id="eliminarGasto" value="<midas:escribe propiedad="idToGastoSiniestro" nombre="lista"/>" disabled="true"/>
                                    	</logic:notEqual>                                    	                                    
                                    </td>                                                                       
                                    <td class="datoTabla" align="left">                                    
                                    	<logic:equal name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                    		<input type="checkbox" name="cancelarGasto" value="<midas:escribe propiedad="idToGastoSiniestro" nombre="lista"/>"  disabled="true"/>
                                    	</logic:equal>             
                                    	                       	
                                    	<logic:notEqual name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                    		<logic:equal name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="4">
                                    			<input type="checkbox" name="cancelarGasto" value="<midas:escribe propiedad="idToGastoSiniestro" nombre="lista"/>" disabled="true"/>
                                    		</logic:equal>
                                    		<logic:notEqual name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="4">
                                    			<input type="checkbox" name="cancelarGasto" value="<midas:escribe propiedad="idToGastoSiniestro" nombre="lista"/>" />
                                    		</logic:notEqual>
                                    	</logic:notEqual>                                                                                                            	
                                    </td>
                                    <td class="datoTabla" align="left">                                   
							   			 <a href="javascript: void(0);" onclick="javascript:sendRequest(document.gastosForm,'/MidasWeb/siniestro/finanzas/modificarGasto.do?idGastoSiniestro=<midas:escribe propiedad="idToGastoSiniestro" nombre="lista"/>','contenido', 'inicializaPaginaDeEdicion()');">
											<img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/>
										</a>
                                    </td>																										
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a onclick="javascript: listarReportesSiniestro();" href="javascript: void(0);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_agregar">
						<a onclick='javascript: mostrarAgregarGastos(<midas:escribe propiedad="idToReporteSiniestro" nombre="gastosForm"/>);' href="javascript: void(0);"><midas:mensaje clave="midas.accion.agregar"/></a>
					</div>
				</div>
			</td>
			<td  align="center">
				<midas:boton onclick="eliminarGastosSiniestro()" tipo="guardar" texto="Eliminar Gastos" style="width:120px;"/>
			</td>
			<td >
				<midas:boton onclick="cancelarGastosSiniestro()" tipo="guardar" texto="Cancelar Gastos" style="width:120px;"/>
			</td>
			<td colspan="1" >&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
