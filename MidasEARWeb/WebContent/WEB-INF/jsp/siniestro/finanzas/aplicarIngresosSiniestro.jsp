<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/finanzas/mostrarIngresoPorAplicar">
	<html:hidden property="idToIngresoSiniestro" styleId="idToIngresoSiniestro"/>
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="idToReporteSiniestroFiltro" styleId="idToReporteSiniestroFiltro"/>
	<html:hidden property="fechaCobroFiltro" styleId="fechaCobroFiltro"/>
	<html:hidden property="montoIngresoFiltro" styleId="montoIngresoFiltro"/>
	<html:hidden property="conceptoIngresoFiltro" styleId="conceptoIngresoFiltro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="6">
				Aplicar Ingreso				
			</td>
		</tr>	
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%" align="left">
				ingreso
			</td>
			<td width="20%" align="left">
				<midas:texto id="idToIngresoSiniestro" propiedadFormulario="idToIngresoSiniestro" deshabilitado="true"/>				
			</td>
			<td colspan="3" align="left">
				<midas:texto propiedadFormulario="descripcionIngreso" id="descripcionIngreso" deshabilitado="true"/> 
			</td>						
		</tr>		
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.producto" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="producto" propiedadFormulario="producto" deshabilitado="true"/>
			</td>
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.numeroAutorizacion" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="numeroAutorizacionTecnica" propiedadFormulario="numeroAutorizacionTecnica" deshabilitado="true"/>
			</td>			
			<td width="20%" colspan="2">&nbsp;</td>		
		</tr>		
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.cveAsegurado" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="cveAsegurado" propiedadFormulario="cveAsegurado" deshabilitado="true"/>
			</td>
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.asegurado" />				
			</td>
			<td width="20%" align="left" colspan="2">
				<midas:texto id="asegurado" propiedadFormulario="asegurado" deshabilitado="true"/>
			</td>			
			<td width="20%" >&nbsp;</td>		
		</tr>		
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.numeroReporte" />				
			</td>
			<td width="20%" align="left">
				<midas:texto id="idToReporteSiniestro" propiedadFormulario="idToReporteSiniestro" deshabilitado="true"/>
			</td>
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.numeroPoliza" />				
			</td>
			<td width="20%" align="left">
				<midas:texto id="numeroPoliza" propiedadFormulario="numeroPoliza" deshabilitado="true"/>
			</td>			
			<td width="10%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.numeroEndose" />				
			</td>
			<td width="10%" align="left">
				<midas:texto id="numeroEndoso" propiedadFormulario="numeroEndoso" deshabilitado="true"/>
			</td>								
		</tr>					
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.fechaOcurrio" />				
			</td>
			<td width="20%" align="left">
				<midas:texto id="fechaOcurrio" propiedadFormulario="fechaOcurrio" deshabilitado="true"/>
			</td>
			<td width="30%" colspan="4">&nbsp;</td>		
		</tr>		
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.idAgente" />				
			</td>
			<td width="20%" align="left">
				<midas:texto id="idAgente" propiedadFormulario="idAgente" deshabilitado="true"/>
			</td>
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.agente" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="agente" propiedadFormulario="agente" deshabilitado="true"/>
			</td>			
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>				
		<tr>			
			<td width="20%" align="left" >
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.ingreso.noFactura" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="numeroFactura" propiedadFormulario="numeroFactura" deshabilitado="true"/>
			</td>
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.fechaSolicitud" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="fechaSolicitud" propiedadFormulario="fechaSolicitud" deshabilitado="true"/>
			</td>			
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>										
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.beneficiario" />				
			</td>
			<td width="20%" align="left">
				<midas:texto id="beneficiario" propiedadFormulario="beneficiario" deshabilitado="true"/>
			</td>			
			<td width="30%" colspan="4">&nbsp;</td>		
		</tr>		
		<tr>			
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.fechaEstimadaPago" />				
			</td >
			<td width="20%" align="left">
				<midas:texto id="fechaEstimadaPago" propiedadFormulario="fechaEstimadaPago" deshabilitado="true"/>
			</td>
			<td width="20%" align="left">
				<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.ingreso.conceptoIngreso" />				
			</td>
			<td width="20%" align="left">
				<midas:texto id="conceptoIngreso" propiedadFormulario="conceptoIngreso" deshabilitado="true"/>
			</td>			
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>	
		<tr>			
			<td align="left">
				Observaciones:				
			</td>
			<td align="left" colspan="3">
				<midas:areatexto id="observaciones" propiedadFormulario="observaciones" renglones="3"/>
			</td>			
			<td colspan="2">&nbsp;</td>
		</tr>	
		<tr>
			<td align="left">
				Referencias:		
			</td>
			<td align="left" colspan="3">
				 <midas:areatexto id="referencias" propiedadFormulario="referencias"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td align="left">
				Referencia del ingreso*:
			</td>
			<td align="left" colspan="3">
				<html:select property="idReferenciaExterna" styleId="idReferenciaExterna" styleClass="cajaTexto" >
					<html:option value="">Seleccione ...</html:option>
					<html:optionsCollection property="listaReferenciasIngreso" label="descripcionReferenciaExterna" value="idReferenciaExterna" ></html:optionsCollection>
				</html:select>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>																		
		<tr>
			<td colspan="6" align="center">
			 <div style="width:85%;height:150px;border :1px ,solid;" class="tableContainer">
				<table id="contabilidad" border="0" class="grid" width="100%">
					<thead>
						<tr>
							<%-- <th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.cuentaContable" /></th> --%>
							<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.apropiacion" /></th>
							<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.monto" /></th>
							<%-- <th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.haber" /></th> --%>
						</tr>
					</thead>
					<tr>
						<%-- <td class="datoTabla">X</td> --%>
						<td class="datoTabla">Gasto</td>
						<td class="datoTabla" align="right"><midas:escribe propiedad="montoIngreso" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/></td>
						<%-- <td class="datoTabla">&nbsp;</td> --%>
					</tr>
					<tr>
						<%-- <td class="datoTabla">X</td> --%>
						<td class="datoTabla">IVA</td>
						<td class="datoTabla" align="right">
							<logic:empty name="aplicarIngresosForm" property="montoIVA" scope="request">
								&nbsp;
							</logic:empty>	
							<midas:escribe propiedad="montoIVA" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/>
						</td>
						<%-- <td class="datoTabla">&nbsp;</td> --%>
					</tr>
					<tr>
						<%-- <td class="datoTabla">X</td> --%>
						<td class="datoTabla">ISR</td>
						<td class="datoTabla" align="right">
							<logic:empty name="aplicarIngresosForm" property="montoISR" scope="request">
									&nbsp;
							</logic:empty>							
							<midas:escribe propiedad="montoISR" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/>
						</td>
						<%-- <td class="datoTabla">&nbsp;</td> --%>
					</tr>
					<tr>
						<%-- <td class="datoTabla">X</td> --%>
						<td class="datoTabla">IVA Ret.</td>
						<td class="datoTabla" align="right">
							<midas:escribe propiedad="montoIVARetencion" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/>
						</td>
						<%-- <td class="datoTabla">&nbsp;</td> --%>
					</tr>
					<tr>
						<%-- <td class="datoTabla">X</td> --%>
						<td class="datoTabla">ISR Ret.</td>
						<td class="datoTabla" align="right">
							<midas:escribe propiedad="montoISRRetencion" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/>
						</td>
						<%-- <td class="datoTabla">&nbsp;</td> --%>
					</tr>
					<tr>
						<%-- <td class="datoTabla">X</td> --%>
						<td class="datoTabla">Otros</td>
						<td class="datoTabla" align="right">
							<midas:escribe propiedad="montoOtros" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/>
						</td>
						<%-- <td class="datoTabla">&nbsp;</td> --%>
					</tr>
					<tr>
						<%--<td class="datoTabla" align="right">&nbsp;</td> --%>
						<td class="datoTabla" align="right"><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.total" /></td>
						<td class="datoTabla" align="right">
							<midas:escribe propiedad="montoTotal" formato="$###,###,##0.00" nombre="aplicarIngresosForm"/>
						</td>
						<%-- <td class="datoTabla" align="right">&nbsp;</td> --%>
					</tr>																			
				</table>
				</div>
			</td>
		</tr>		
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>												
		<tr>		
			<td colspan="6" align="center">
				<div style="width:95%;height:150px;border :1px ,solid;" class="tableContainer">
					<table width="95%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="configuracion.cobertura.idtcramo" /></th>
								<th><midas:mensaje clave="configuracion.cobertura.idtcsubramo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.cobertura" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.monto" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoCuotaParte" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoRetencion" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoPrimExc" /></th>
								<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.montoGastoFacultativo" /></th>
							</tr>
						</thead>
						<logic:empty name="aplicarIngresosForm" property="detalleDistribucion">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="aplicarIngresosForm" property="detalleDistribucion">
							<logic:iterate name="aplicarIngresosForm" property="detalleDistribucion" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaDTO.descripcionRamo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaDTO.descripcionSubramo" nombre="lista"/>
                                    </td>
									<td class="datoTabla" align="left">
										<midas:escribe propiedad="seccionDTO.descripcionSeccion" nombre="lista"/>
                                    </td>                                                                        
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaDTO.descripcionCobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="monto" formato="###,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="montoRetenido" formato="###,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="montoCuotaParte" formato="###,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="montoPrimerExcedente" formato="###,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                    	<midas:escribe propiedad="montoFacultativo" formato="###,###,##0.00" nombre="lista"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>								
		</tr>		
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<logic:equal name="aplicarIngresosForm" property="permiteAplicarIngreso" value="0">	
			<tr>
				<td colspan="6" align="center" class="error">
					<midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.aplicarIngreso.enProceso" />
				</td>
			</tr>	
		</logic:equal>					
		<tr>		
			<td colspan="2">&nbsp;</td>					
			<td  align="center">
				<logic:equal name="aplicarIngresosForm" property="permiteAplicarIngreso" value="1">
					<logic:equal name="aplicarIngresosForm" property="estatusIngreso" value="3">	
						<midas:boton onclick="aplicarIngresoSiniestro()" tipo="guardar" texto="Aplicar Ingreso" style="width:197px;"/>
					</logic:equal>
					<logic:equal name="aplicarIngresosForm" property="estatusIngreso" value="6">	
						<midas:boton onclick="cancelarIngresoSiniestro()" tipo="guardar" texto="Cancelar Ingreso" style="width:197px;"/>
					</logic:equal>
				</logic:equal>
			</td>
			<td >
				<midas:boton onclick="regresarMostrarListaIngresosPorAplicar()" tipo="regresar" texto="Regresar" style="width:207px;"/>
			</td>										
			<td colspan="2">&nbsp;</td>				
		</tr>
 	</table>
</midas:formulario>
