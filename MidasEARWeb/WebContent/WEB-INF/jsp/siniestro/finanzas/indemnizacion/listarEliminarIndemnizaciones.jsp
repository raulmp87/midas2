<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/indemnizacion/listarIndemnizaciones">
 	<table width="90%" border="0">
 		<tr>
			<td width="10%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
			<td width="40%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.pagoParcial.listar.titulo" />			
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla" colspan="2">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.tipoMoneda" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="tipoMoneda" nombre="indemnizacionForm"/>
			</td>
			<td></td>		
		</tr>	
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div style="width:100%;height:150px;border:1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" align="center">
						<thead>
							<tr>
								<th colspan="8" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaPagosParciales" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.fechaPago" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.beneficiario" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.montoNeto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.totalPago" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.estatus" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.eliminar" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaIndemnizaciones">
							<tr>
	                            <td class="datoTabla" align="center" colspan="8">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="indemnizacionForm" property="listaIndemnizaciones">
							<logic:iterate name="indemnizacionForm" property="listaIndemnizaciones" id="regIndemnizacion">
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="indemnizacionDTO.fechaDelPago" nombre="regIndemnizacion" formato="dd/MMMM/yyyy"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="indemnizacionDTO.beneficiario" nombre="regIndemnizacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="totalPerdida" nombre="regIndemnizacion" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="totalDeducible" nombre="regIndemnizacion" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="totalCoaseguro" nombre="regIndemnizacion" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="totalIndemnizacion" nombre="regIndemnizacion" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="indemnizacionDTO.estatusFinanzasDTO.descripcion" nombre="regIndemnizacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                    	<input type="radio" name="idIndemnizacion" value="<midas:escribe propiedad="indemnizacionDTO.idToIndemnizacion" nombre="regIndemnizacion"/>" onclick="sendRequest(this.form,'/MidasWeb/siniestro/finanzas/indemnizacion/listarDetalleIndemnizacion.do','detalleIndemnizacion',null);">
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">
			<input type="hidden" id="mensaje" name="mensaje" value="<midas:escribe propiedad="mensaje" nombre="indemnizacionForm"/>">
			<input type="hidden" id="tipoMensaje" name="tipoMensaje" value="<midas:escribe propiedad="tipoMensaje" nombre="indemnizacionForm"/>">
			<midas:oculto propiedadFormulario="idReporteSiniestro"/>&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div id="detalleIndemnizacion">
					
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td class="dato" colspan="2">
				<midas:boton texto="Eliminar pago parcial de indemnizaci&oacute;n" style="width:270px;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/eliminarPagoParcial.do', 'contenido', 'eliminarPagoParcial_CB();');" tipo="guardar"/>
			</td>
			<td>&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
