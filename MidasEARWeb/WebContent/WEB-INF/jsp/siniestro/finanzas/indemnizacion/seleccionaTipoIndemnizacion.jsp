<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/indemnizacion/agregarIndemnizacion">
 	<table width="90%" border="0">
 		<tr>
			<td width="10%">&nbsp;</td>
			<td width="40%">&nbsp;</td>
			<td width="40%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.agregar.titulo" />
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<midas:oculto propiedadFormulario="idReporteSiniestro"/>
				<input type="hidden" id="mensaje" name="mensaje" value="<midas:escribe propiedad="mensaje" nombre="indemnizacionForm"/>">
				<input type="hidden" id="tipoMensaje" name="tipoMensaje" value="<midas:escribe propiedad="tipoMensaje" nombre="indemnizacionForm"/>">
				<br/><br/><br/>
			</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.tipo" /> :			
			</td>
			<td align="left" class="dato">
				<midas:radio valorEstablecido="false" propiedadFormulario="tipoIndemnizacion"
				onClick="sendRequest(indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/mostrarAgregarIndemnizacion.do','contenido','redireccionaMostrarAgregarIndemnizacion();');">
				</midas:radio>
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.unPago"/>
				<midas:radio valorEstablecido="true" propiedadFormulario="tipoIndemnizacion"
				onClick="sendRequest(indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/pendientePagosParciales.do','contenido','generaPendienteVariosPagos();');">
				</midas:radio>
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.variosPagos"/>
			</td>
			<td></td>		
		</tr>
 	</table>
</midas:formulario>
