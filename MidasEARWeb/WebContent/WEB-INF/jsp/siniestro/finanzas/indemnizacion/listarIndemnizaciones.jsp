<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/indemnizacion/listarIndemnizaciones">
 	<table width="900px" border="0">
 		<tr>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.pagoParcial.listar.titulo" />			
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla" colspan="2">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.tipoMoneda" /> :				
			</td>
			<td align="left" class="dato">
				<midas:escribe propiedad="tipoMoneda" nombre="indemnizacionForm"/>
			</td>
			<td></td>		
		</tr>	
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div style="width:900px;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" align="center">
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaPagosParciales" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.fechaPago" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.beneficiario" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.montoNeto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.totalPago" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.tipoPago" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.estatus" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.ver" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaIndemnizaciones">
							<tr>
	                            <td class="datoTabla" align="center" colspan="9">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="indemnizacionForm" property="listaIndemnizaciones">
							<logic:iterate name="indemnizacionForm" property="listaIndemnizaciones" id="regIndemnizacion">
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="indemnizacionDTO.fechaDelPago" nombre="regIndemnizacion" formato="dd/MM/yyyy"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="indemnizacionDTO.beneficiario" nombre="regIndemnizacion"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="totalPerdida" nombre="regIndemnizacion" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="totalDeducible" nombre="regIndemnizacion" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="totalCoaseguro" nombre="regIndemnizacion" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="totalIndemnizacion" nombre="regIndemnizacion" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoPago" nombre="regIndemnizacion" />
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="indemnizacionDTO.estatusFinanzasDTO.descripcion" nombre="regIndemnizacion"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                    	<input type="radio" name="idIndemnizacion" value="<midas:escribe propiedad="indemnizacionDTO.idToIndemnizacion" nombre="regIndemnizacion"/>" onclick="seleccionaPagoParcial(<midas:escribe propiedad="indemnizacionDTO.estatusFinanzasDTO.idTcEstatusfinanzas" nombre="regIndemnizacion"/>,'<midas:escribe propiedad="indemnizacionDTO.ultimoPago" nombre="regIndemnizacion"/>');">
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">
			<input type="hidden" id="mensaje" name="mensaje" value="<midas:escribe propiedad="mensaje" nombre="indemnizacionForm"/>">
			<input type="hidden" id="tipoMensaje" name="tipoMensaje" value="<midas:escribe propiedad="tipoMensaje" nombre="indemnizacionForm"/>">
			<midas:oculto propiedadFormulario="idReporteSiniestro"/>&nbsp;
			</td>
		</tr>
		</midas:formulario>
		<tr>
			<td colspan="5" align="center">
				<div id="detalleIndemnizacion">
				
				</div>
			</td>
		</tr>
		<tr>
			<td><midas:boton texto="Cancelar pago parcial" style="width:150px;display:none;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/cancelarPagoParcial.do', 'contenido', 'cancelarPagoParcial_CB();');" tipo="guardar"/></td>
			<td><midas:boton texto="Eliminar pago parcial" style="width:150px;display:none;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/eliminarPagoParcial.do', 'contenido', 'eliminarPagoParcial_CB();');" tipo="borrar"/></td>
			<td><midas:boton texto="Modificar pago parcial" style="width:150px;display:none;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/mostrarModificarPagoParcial.do', 'contenido', 'mostrarModificarPagoParcial_CB();');" tipo="modificar"/></td>
			<td>
				<logic:notEqual name="permitePagoParcial" scope="request" value="false">
					<midas:boton texto="Agregar pago parcial" style="width:150px;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/mostrarAgregarPagoParcial.do', 'contenido', 'mostrarAgregarPagoParcial();');" tipo="agregar"/>
				</logic:notEqual>
			</td>
			<td><midas:boton texto="Regresar" style="width:80px;" onclick="listarReportesSiniestro();" tipo="regresar"/></td>
			
			
<%--				<midas:boton texto="Cancelar pago parcial" style="width:150px;display:none;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/cancelarPagoParcial.do', 'contenido', 'cancelarPagoParcial_CB();');" tipo="guardar"/>--%>
<%--				&nbsp;&nbsp;<midas:boton texto="Eliminar pago parcial" style="width:150px;display:none;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/eliminarPagoParcial.do', 'contenido', 'eliminarPagoParcial_CB();');" tipo="borrar"/>--%>
<%--			--%>
<%--				&nbsp;&nbsp;<midas:boton texto="Modificar pago parcial" style="width:150px;display:none;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/mostrarModificarPagoParcial.do', 'contenido', 'mostrarModificarPagoParcial_CB();');" tipo="modificar"/>--%>
<%--			--%>
<%--				<logic:notEqual name="permitePagoParcial" scope="request" value="false">--%>
<%--					&nbsp;&nbsp;<midas:boton texto="Agregar pago parcial" style="width:150px;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/mostrarAgregarPagoParcial.do', 'contenido', 'mostrarAgregarPagoParcial();');" tipo="agregar"/>--%>
<%--				</logic:notEqual>--%>
<%--			--%>
<%--				&nbsp;&nbsp;<midas:boton texto="Regresar" style="width:80px;" onclick="listarReportesSiniestro();" tipo="regresar"/>--%>
			
		</tr>
 	</table>

