<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ page import="mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO" %>
<%@ page import="mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleBean" %>
<%@ page import="java.text.DecimalFormat" %>

<midas:formulario accion="/siniestro/finanzas/indemnizacion/agregarIndemnizacion">
 	<table width="95%" border="0">
 		<tr>
			<td width="5%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="7%">&nbsp;</td>
			<td width="9%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="9">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.autorizar.titulo" />			
			</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla" colspan="3">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.tipoMoneda" /> :				
			</td>
			<td align="left" class="dato" colspan="4">
				<midas:escribe propiedad="tipoMoneda" nombre="indemnizacionForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div style="width:100%;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaRiesgos" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.sumaAsegurada" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.sumaAseguradaDisponible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.tipoSA" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.cobertura.basica" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaRiesgo">
							<tr>
					            <td class="datoTabla" align="center" colspan="9">
									<midas:mensaje clave="lista.vacia" />
								</td>
					        </tr>
					 	</logic:empty>
					   	<logic:notEmpty name="indemnizacionForm" property="listaRiesgo">
							<logic:iterate name="indemnizacionForm" property="listaRiesgo" id="registroRiesgo">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="coberturaBasicaSoporteDanosDTO.sumaAsegurada" nombre="registroRiesgo" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="sumaAseguradaDisponible" nombre="registroRiesgo" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoCobertura" nombre="registroRiesgo"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="nombreCoberturaBasica" nombre="registroRiesgo"/>
                                    </td>
								</tr>
							</logic:iterate>
								<tr>
									<td class="datoTabla" align="right"" colspan="6">
					                    <midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
					                </td>
					                <td class="datoTabla" align="left" colspan="2">
					                    <midas:escribe propiedad="totalSumaAsegurada" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
								</tr>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div style="width:100%;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="6" align="center"><midas:mensaje clave="siniestro.finanzas.historialReserva.listaDetalle" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.reserva" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaReserva">
							<tr>
					       		<td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="indemnizacionForm" property="listaReserva">
							<logic:iterate name="indemnizacionForm" property="listaReserva" id="registroReserva">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="reservaDetalleDTO.id.numeroinciso" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="reservaDetalleDTO.id.numerosubinciso" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="registroReserva" formato="$###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
								<tr>
									<td class="datoTabla" align="right" colspan="5">
					                    <midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="totalReserva" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
								</tr>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div style="width:100%;height:155px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaPerdidas" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.determinacionPerdida" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.indemnizacion" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaDetalleIndemnizaciones">
							<tr>
					            <td class="datoTabla" align="center" colspan="9">
									<midas:mensaje clave="lista.vacia" />
								</td>
					        </tr>
					 	</logic:empty>
					   	<logic:notEmpty name="indemnizacionForm" property="listaDetalleIndemnizaciones">
							<logic:iterate name="indemnizacionForm" property="listaDetalleIndemnizaciones" id="registroDetalleIndemnizaciones" indexId="indice">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.id.numeroInciso" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.id.numeroSubinciso" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                	<%
					                	IndemnizacionRiesgoCoberturaDTO registro= ((ReservaDetalleBean)registroDetalleIndemnizaciones).getIndemnizacionRiesgoCoberturaDTO();
					                	double perdidaReg = registro.getMontoPago().doubleValue() + registro.getDeducible().doubleValue() + registro.getCoaseguro().doubleValue();
					                	DecimalFormat formateadorMonto = new DecimalFormat("$###,###,##0.00");
					                	%>
					                	<%=formateadorMonto.format(perdidaReg) %>	
					                </td>
					                <td class="datoTabla" align="left">
					                	<midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.deducible" nombre="registroDetalleIndemnizaciones" formato="$###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="left">
					                	<midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.coaseguro" nombre="registroDetalleIndemnizaciones" formato="$###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.montoPago" nombre="registroDetalleIndemnizaciones" formato="$###,###,##0.00"/>
					                </td>
								</tr>
							</logic:iterate>
								<tr>
									<td class="datoTabla" align="right" colspan="5">
					                    <midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
					                </td>
					                <td class="datoTabla" align="left">
					                	<midas:escribe propiedad="totalPerdidas" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="left">
					                	<midas:escribe propiedad="totalDeducibles" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="left">
					                	<midas:escribe propiedad="totalCoaseguros" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
					                <td class="datoTabla" align="left">
					                	<midas:escribe propiedad="totalIndemnizaciones" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
								</tr>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right" colspan="2">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.fechaPago"/> :
			</td>
			<td colspan="3" class="dato">
				<midas:escribe propiedad="fechaPago" nombre="indemnizacionForm"/>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right" colspan="2">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.estatus"/> :
			</td>
			<td colspan="3" class="dato">
				<midas:escribe propiedad="estatusIndemnizacion" nombre="indemnizacionForm"/>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right" colspan="2">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.beneficiario"/> :
			</td>
			<td colspan="3" class="dato">
				<midas:escribe propiedad="beneficiario" nombre="indemnizacionForm"/>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right" colspan="2">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.comentariosIndemnizacion" /> :
			</td>
			<td colspan="3" class="dato">
				<midas:escribe propiedad="comentarios" nombre="indemnizacionForm"/>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
			<td colspan="3">
				<midas:oculto propiedadFormulario="idReporteSiniestro"/>
				<midas:oculto propiedadFormulario="tipoIndemnizacion"/>
				<midas:oculto propiedadFormulario="idIndemnizacion"/>
				<input type="hidden" id="mensaje" name="mensaje" value="<midas:escribe propiedad="mensaje" nombre="indemnizacionForm"/>">
				<input type="hidden" id="tipoMensaje" name="tipoMensaje" value="<midas:escribe propiedad="tipoMensaje" nombre="indemnizacionForm"/>">			
				<midas:boton texto="Autorizar indemnizaci&oacute;n" style="width:200px;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/autorizarIndemnizacion.do', 'contenido', 'redireccionaAutorizarIndemnizacion();');" tipo="guardar"/>
			</td>
			<td colspan="3">			
				<midas:boton texto="Cancelar indemnizaci&oacute;n" style="width:200px;" onclick="sendRequest(document.indemnizacionForm,'/MidasWeb/siniestro/finanzas/indemnizacion/cancelarIndemnizacion.do', 'contenido', 'redireccionaCancelarIndemnizacion();');" tipo="guardar"/>
			</td>
		</tr>
 	</table>
</midas:formulario>
