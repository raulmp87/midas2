<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/siniestro/finanzas/indemnizacion/listarDetalleIndemnizacion">
<div style="width:900px;height:150px;border :1px,solid;overflow: auto;" class="tableContainer">
<table style="white-space:normal;"  class="tablaConResultados" >
	<thead>
		<tr>
			<th colspan="8" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaDetalle" /></th>
		</tr>
		<tr>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" /></th>
			<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.indemnizacion" /></th>
		</tr>
	</thead>
	<logic:empty name="indemnizacionDetalleForm" property="listaDetalleIndemnizaciones">
		<tr>
	   		<td class="datoTabla" align="center" colspan="8">
				<midas:mensaje clave="lista.vacia" />
			</td>
	    </tr>
	</logic:empty>
	<logic:notEmpty name="indemnizacionDetalleForm" property="listaDetalleIndemnizaciones">
		<logic:iterate name="indemnizacionDetalleForm" property="listaDetalleIndemnizaciones" id="registroDetalleIndemnizaciones" indexId="indice">
			<tr>
				<td class="datoTabla" align="left">
	                   <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.id.numeroInciso" nombre="registroDetalleIndemnizaciones"/>
	               </td>
	               <td class="datoTabla" align="left">
	                   <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.id.numeroSubinciso" nombre="registroDetalleIndemnizaciones"/>
	               </td>
	               <td class="datoTabla" align="left">
	                   <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroDetalleIndemnizaciones"/>
	               </td>
	               <td class="datoTabla" align="left">
	                   <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroDetalleIndemnizaciones"/>
	               </td>
	               <td class="datoTabla" align="left">
	                   <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroDetalleIndemnizaciones"/>
	               </td>
	               <td class="datoTabla" align="right">
	               	<midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.deducible" nombre="registroDetalleIndemnizaciones" formato="$###,###,##0.00"/>
	               </td>
	               <td class="datoTabla" align="right">
	               	<midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.coaseguro" nombre="registroDetalleIndemnizaciones" formato="$###,###,##0.00"/>
	               </td>
	               <td class="datoTabla" align="right">
	                   <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.montoPago" nombre="registroDetalleIndemnizaciones" formato="$###,###,##0.00"/>
	               </td>
			</tr>
		</logic:iterate>
			<tr>
				<td class="datoTabla" align="right" colspan="5">
                	<midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
               	</td>
               	<td class="datoTabla" align="right">
               		<midas:escribe propiedad="totalDeducibles" nombre="indemnizacionDetalleForm" formato="$###,###,##0.00"/>
               	</td>
               	<td class="datoTabla" align="right">
               		<midas:escribe propiedad="totalCoaseguros" nombre="indemnizacionDetalleForm" formato="$###,###,##0.00"/>
               	</td>
               	<td class="datoTabla" align="right">
               		<midas:escribe propiedad="totalIndemnizaciones" nombre="indemnizacionDetalleForm" formato="$###,###,##0.00"/>
               	</td>
			</tr>
	</logic:notEmpty>
</table>
</div>
<table width="900px" border="0">
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="25%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="50%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
	</tr>
	<tr>
		<td class="datoTabla" align="right" colspan="2">
			<midas:mensaje clave="siniestro.finanzas.indemnizacion.comentariosIndemnizacion" /> :
		</td>
		<td class="dato" colspan="2">
			<midas:areatexto propiedadFormulario="comentarios" columnas="30" renglones="5"/>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5">
		<midas:oculto propiedadFormulario="idIndemnizacion"/>&nbsp;
		</td>
	</tr>
</table>		
</midas:formulario>
