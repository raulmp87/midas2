<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<midas:formulario accion="/siniestro/finanzas/indemnizacion/agregarPagoParcial">
 	<table width="95%" border="0">
 		<tr>
			<td width="5%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="7%">&nbsp;</td>
			<td width="9%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
			<td width="16%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td class="titulo" colspan="9">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.pagoParcial.modificar.titulo" />			
			</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<td align="right" class="datoTabla" colspan="3">
				<midas:mensaje clave="siniestro.finanzas.historialReserva.tipoMoneda" /> :				
			</td>
			<td align="left" class="dato" colspan="4">
				<midas:escribe propiedad="tipoMoneda" nombre="indemnizacionForm"/>
			</td>
			<td></td>		
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div style="width:100%;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados"  border="0" >
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaRiesgos" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.sumaAsegurada" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.sumaAseguradaDisponible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.tipoSA" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.cobertura.basica" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaRiesgo">
							<tr>
					            <td class="datoTabla" align="center" colspan="9">
									<midas:mensaje clave="lista.vacia" />
								</td>
					        </tr>
					 	</logic:empty>
					   	<logic:notEmpty name="indemnizacionForm" property="listaRiesgo">
							<logic:iterate name="indemnizacionForm" property="listaRiesgo" id="registroRiesgo">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.id.numeroinciso" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoAfectadoDTO.id.numerosubinciso" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroRiesgo"/>
					                </td>
					                <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="coberturaBasicaSoporteDanosDTO.sumaAsegurada" nombre="registroRiesgo" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="sumaAseguradaDisponible" nombre="registroRiesgo" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoCobertura" nombre="registroRiesgo"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="nombreCoberturaBasica" nombre="registroRiesgo"/>
                                    </td>
								</tr>
							</logic:iterate>
								<tr>
									<td class="datoTabla" align="right" colspan="6">
					                    <midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="totalSumaAsegurada" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
					                <td colspan="2">&nbsp;</td>
								</tr>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div style="width:100%;height:150px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="6" align="center"><midas:mensaje clave="siniestro.finanzas.historialReserva.listaDetalle" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.reserva" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaReserva">
							<tr>
					       		<td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
					       	</tr>
					    </logic:empty>
					   	<logic:notEmpty name="indemnizacionForm" property="listaReserva">
							<logic:iterate name="indemnizacionForm" property="listaReserva" id="registroReserva">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="reservaDetalleDTO.id.numeroinciso" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="reservaDetalleDTO.id.numerosubinciso" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroReserva"/>
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="registroReserva" formato="$###,###,##0.00"/>
					                    <input type="hidden" id="estimacionesReserva" name="estimacionesReserva" value="<midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="registroReserva"/>">
					                </td>
								</tr>
							</logic:iterate>
								<tr>
									<td class="datoTabla" align="right" colspan="5">
					                    <midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
					                </td>
					                <td class="datoTabla" align="right">
					                    <midas:escribe propiedad="totalReserva" nombre="indemnizacionForm" formato="$###,###,##0.00"/>
					                </td>
								</tr>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center" class="datoTabla" colspan="3">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.incluir" /> :				
			</td>
			<td align="left" class="datoTabla" colspan="4">
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" />&nbsp;&nbsp;
					<midas:checkBox propiedadFormulario="habilitaDeducible" id="habilitaDeducible" valorEstablecido="1" onClick="toggleDeducibles();"></midas:checkBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" />&nbsp;&nbsp;
					<midas:checkBox propiedadFormulario="habilitaCoaseguro" id="habilitaCoaseguro" valorEstablecido="1" onClick="toggleCoaseguros();"></midas:checkBox>
				</logic:equal>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" />&nbsp;&nbsp;
					<midas:checkBox  propiedadFormulario="habilitaDeducible" id="habilitaDeducible" valorEstablecido="1" deshabilitado="true"></midas:checkBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" />&nbsp;&nbsp;
					<midas:checkBox propiedadFormulario="habilitaCoaseguro" id="habilitaCoaseguro" valorEstablecido="1" deshabilitado="true"></midas:checkBox>
				</logic:equal>
			</td>
			<td></td>		
		</tr>	
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div style="width:100%;height:155px;border:1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th colspan="9" align="center"><midas:mensaje clave="siniestro.finanzas.indemnizacion.listaPerdidas" /></th>
							</tr>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.subinciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.historialReserva.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.determinacionPerdida" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.deducible" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.coaseguro" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.indemnizacion.indemnizacion" /></th>
							</tr>
						</thead>
						<logic:empty name="indemnizacionForm" property="listaDetalleIndemnizaciones">
							<tr>
					            <td class="datoTabla" align="center" colspan="9">
									<midas:mensaje clave="lista.vacia" />
								</td>
					        </tr>
					 	</logic:empty>
					   	<logic:notEmpty name="indemnizacionForm" property="listaDetalleIndemnizaciones">
							<logic:iterate name="indemnizacionForm" property="listaDetalleIndemnizaciones" id="registroDetalleIndemnizaciones" indexId="indice">
								<tr>
									<td class="datoTabla" align="left">
					                    <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.id.numeroInciso" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="indemnizacionRiesgoCoberturaDTO.id.numeroSubinciso" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <td class="datoTabla" align="left">
					                    <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="registroDetalleIndemnizaciones"/>
					                </td>
					                <logic:equal name="modoLectura" scope="request" value="false">
						                <td class="datoTabla" align="right">
						                	<%String perdidaValor = "perdidas["+indice+"]"; %>
						                	<input type="text" class="cajaTexto" style="text-transform: none;"
						                	value="<bean:write format="$########0.00" name="indemnizacionForm"
						                	property="<%=perdidaValor %>"/>"
						                	onfocus="new Mask('$#,###.00', 'number').attach(this)"
						                	onblur="calculaSumaPerdidas('<bean:write name="indice"/>');"
						                	onkeypress="return soloNumeros(this, event, true)" name="perdidas"/>
						                </td>
						                <td class="datoTabla" align="right">
					                		<%String deducibleValor = "deducibles["+indice+"]"; %>
						                	<input type="text" class="cajaTexto" style="text-transform: none;" disabled="disabled"
						                	value="<bean:write format="$########0.00" name="indemnizacionForm" property="<%=deducibleValor %>"/>"
						                	onfocus="new Mask('$#,###.00', 'number').attach(this)"
						                	onblur="calculaSumaDeducibles('<bean:write name="indice"/>');"
						                	onkeypress="return soloNumeros(this, event, true)" name="deducibles"/>
					                	</td>
					               		<td class="datoTabla" align="right">
						                	<%String coaseguroValor = "coaseguros["+indice+"]"; %>
						                	<input type="text" class="cajaTexto" style="text-transform: none;" disabled="disabled"
						                	value="<bean:write format="$########0.00" name="indemnizacionForm" property="<%=coaseguroValor %>"/>"
						                	onfocus="new Mask('$#,###.00', 'number').attach(this)"
						                	onblur="calculaSumaCoaseguros('<bean:write name="indice"/>');"
						                	onkeypress="return soloNumeros(this, event, true)" name="coaseguros"/>
					                	</td>
					                </logic:equal>
					                <logic:equal name="modoLectura" scope="request" value="true">
						                <td class="datoTabla" align="right">
						                	<%String perdidaValor = "perdidas["+indice+"]"; %>
						                	<input type="text" class="cajaTexto" style="text-transform: none;"
						                	value="<bean:write format="$########0.00" name="indemnizacionForm"
						                	property="<%=perdidaValor %>"/>"
						                	readonly="readonly" name="perdidas"/>
						                </td>
						                <td class="datoTabla" align="right">
					                		<%String deducibleValor = "deducibles["+indice+"]"; %>
						                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly"
						                	value="<bean:write format="$########0.00" name="indemnizacionForm" property="<%=deducibleValor %>"/>"
						                	name="deducibles"/>
					                	</td>
					               		<td class="datoTabla" align="right">
						                	<%String coaseguroValor = "coaseguros["+indice+"]"; %>
						                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly"
						                	value="<bean:write format="$########0.00" name="indemnizacionForm" property="<%=coaseguroValor %>"/>"
						                	name="coaseguros"/>
					                	</td>
					                </logic:equal>
					                
					                <td class="datoTabla" align="right">
					                	<%String indemnizacionValor = "indemnizaciones["+indice+"]"; %>
					                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly"
					                	value="<bean:write format="$########0.00" name="indemnizacionForm" property="<%=indemnizacionValor %>"/>"
					                	name="indemnizaciones"/>
					                </td>
								</tr>
							</logic:iterate>
								<tr>
									<td class="datoTabla" align="right" colspan="5">
					                    <midas:mensaje clave="siniestro.finanzas.indemnizacion.total" />
					                </td>
					                <td class="datoTabla" align="right">
					                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly" value="<bean:write format="$########0.00" name="indemnizacionForm" property="totalPerdidas"/>" name="totalPerdidas" id="totalPerdidas"/>
					                </td>
					                <td class="datoTabla" align="right">
					                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly" disabled="disabled" value="<bean:write format="$########0.00" name="indemnizacionForm" property="totalDeducibles"/>" name="totalDeducibles" id="totalDeducibles"/>
					                </td>
					                <td class="datoTabla" align="right">
					                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly"  disabled="disabled" value="<bean:write format="$########0.00" name="indemnizacionForm" property="totalCoaseguros"/>" name="totalCoaseguros" id="totalCoaseguros"/>
					                </td>
					                <td class="datoTabla" align="right">
					                	<input type="text" class="cajaTexto" style="text-transform: none;" readonly="readonly" value="<bean:write format="$########0.00" name="indemnizacionForm" property="totalIndemnizaciones"/>" name="totalIndemnizaciones" id="totalIndemnizaciones"/>
					                </td>
								</tr>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="9" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.impuestos" />
			</td>
		</tr>
		<logic:equal name="modoLectura" scope="request" value="false">
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.iva" />&nbsp;&nbsp;
				
				<midas:checkBox propiedadFormulario="habilitaIVA" id="habilitaIVA" valorEstablecido="1" onClick="toggleIVA();"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.iva" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeIVA" id="porcentajeIVA" deshabilitado="true" onchange="calculaMontoIva();" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoIVA" id="montoIVA" deshabilitado="true" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaRet" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaIVARet" id="habilitaIVARet" valorEstablecido="1" onClick="toggleIVARet();"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaRetencion" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeIVARet" id="porcentajeIVARet" deshabilitado="true" onchange="calculaMontoIvaRet();" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaRetencionMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoIVARet" id="montoIVARet" deshabilitado="true" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isr" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaISR" id="habilitaISR" valorEstablecido="1" onClick="toggleISR();"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.isr" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeISR" id="porcentajeISR" deshabilitado="true" onchange="calculaMontoIsr();" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isrMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoISR" id="montoISR" deshabilitado="true" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isrRet" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaISRRet" id="habilitaISRRet" valorEstablecido="1" onClick="toggleISRRet();"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.isrRetencion" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeISRRet" id="porcentajeISRRet" deshabilitado="true" onchange="calculaMontoIsrRet();" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isrRetencionMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoISRRet" id="montoISRRet" deshabilitado="true" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.otros" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaOtros" id="habilitaOtros" valorEstablecido="1" onClick="toggleOtros();"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.otros" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" deshabilitado="true" onchange="calculaMontoOtros();" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.otrosMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoOtros" id="montoOtros" deshabilitado="true" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		</logic:equal>
		<logic:equal name="modoLectura" scope="request" value="true">
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.iva" />&nbsp;&nbsp;
				
				<midas:checkBox propiedadFormulario="habilitaIVA" id="habilitaIVA" valorEstablecido="1" deshabilitado="true"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.iva" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeIVA" id="porcentajeIVA" soloLectura="true" />
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoIVA" id="montoIVA" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaRet" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaIVARet" id="habilitaIVARet" valorEstablecido="1" deshabilitado="true"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaRetencion" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeIVARet" id="porcentajeIVARet"  soloLectura="true" />
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ivaRetencionMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoIVARet" id="montoIVARet" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isr" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaISR" id="habilitaISR" valorEstablecido="1" deshabilitado="true" ></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.isr" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeISR" id="porcentajeISR"  soloLectura="true" />
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isrMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoISR" id="montoISR" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isrRet" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaISRRet" id="habilitaISRRet" valorEstablecido="1" deshabilitado="true"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.isrRetencion" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeISRRet" id="porcentajeISRRet" soloLectura="true" />
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.isrRetencionMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoISRRet" id="montoISRRet" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.otros" />&nbsp;&nbsp;
				<midas:checkBox propiedadFormulario="habilitaOtros" id="habilitaOtros" valorEstablecido="1" deshabilitado="true"></midas:checkBox>
			</td>
			<td class="datoTabla" align="right">
				% <midas:mensaje clave="siniestro.finanzas.indemnizacion.otros" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros"  soloLectura="true" />
			</td>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.otrosMonto" />
			</td>
			<td class="datoTabla">
				<midas:texto propiedadFormulario="montoOtros" id="montoOtros" soloLectura="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		</logic:equal>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.ultimoPago"/>
			</td>
			<td colspan="3" class="datoTabla">
				<logic:equal name="indemnizacionForm" property="habilitaUltimoPago" value="true">
					<midas:checkBox propiedadFormulario="ultimoPago" id="ultimoPago" valorEstablecido="true"></midas:checkBox>
				</logic:equal>
				<logic:notEqual name="indemnizacionForm" property="habilitaUltimoPago" value="true">
					<midas:checkBox propiedadFormulario="ultimoPago" id="ultimoPago" valorEstablecido="true" deshabilitado="true"></midas:checkBox>
				</logic:notEqual>
				
			</td>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.fechaPago"/>
			</td>
			<td colspan="2" class="datoTabla" align="right">
				<midas:texto propiedadFormulario="fechaPago" id="fechaPago" soloLectura="true"/>
			</td>
			<td>
				<image src="../img/b_calendario.gif" border=0 />
			</td>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.beneficiario"/>
			</td>
			<td colspan="4" class="datoTabla">
				<midas:texto propiedadFormulario="beneficiario" id="beneficiario" caracteres="150"/>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right">
				<midas:mensaje clave="siniestro.finanzas.indemnizacion.comentariosIndemnizacion" />
			</td>
			<td colspan="5" class="datoTabla">
				<midas:areatexto propiedadFormulario="comentarios" columnas="30" renglones="5"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a onclick='javascript: redireccionaIndemnizacionesDeReporteSiniestro(<bean:write name="indemnizacionForm" property="idReporteSiniestro"/>);' href="javascript: void(0);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
			<td colspan="4" align="center"">
				<midas:oculto propiedadFormulario="idReporteSiniestro"/>
				<midas:oculto propiedadFormulario="tipoIndemnizacion"/>
				<midas:oculto propiedadFormulario="idIndemnizacion"/>
				<input type="hidden" id="mensaje" name="mensaje" value="<midas:escribe propiedad="mensaje" nombre="indemnizacionForm"/>">
				<input type="hidden" id="tipoMensaje" name="tipoMensaje" value="<midas:escribe propiedad="tipoMensaje" nombre="indemnizacionForm"/>">			
				<midas:boton texto="Modificar pago parcial de indemnizaci&oacute;n" style="width:270px;" onclick="validaModificarPagoParcial(document.indemnizacionForm);" tipo="guardar"/>
			</td>
		</tr>
 	</table>
</midas:formulario>
