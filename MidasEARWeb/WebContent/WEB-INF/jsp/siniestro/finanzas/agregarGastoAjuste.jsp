<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/finanzas/gastosAjuste">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="indIva" styleId="indIva"/>
	<html:hidden property="indIvaRet" styleId="indIvaRet"/>
	<html:hidden property="indIsr" styleId="indIsr"/>
	<html:hidden property="indIsrRet" styleId="indIsrRet"/>
	<html:hidden property="indOtros" styleId="indOtros"/>	
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		
	<table id="agregar" border="0" >
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.gastos.agregar.titulo" />
			</td>			
		</tr>	
		<tr>
			<td align="center" colspan="8" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.gastos.agregar.tipoMoneda" />				
				<midas:escribe propiedad="tipoMoneda" nombre="gastosForm"/>
			</td>
		</tr>
		<tr>
			<td width="5%" >&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="29%">&nbsp;</td>
			<td width="1%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.fechaGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaGasto" soloLectura="true" propiedadFormulario="fechaGasto" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td colspan="2">&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaRecepcion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.fechaRecepcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaRecepcion" soloLectura="true" propiedadFormulario="fechaRecepcion" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td>&nbsp;</td>
 		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaEstimacionPago" requerido="no"
					key="siniestro.finanzas.gastos.agregar.fechaEstimacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaEstimacionPago" soloLectura="true" propiedadFormulario="fechaEstimacionPago" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td colspan="2">&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaEntrega" requerido="no"
					key="siniestro.finanzas.gastos.agregar.fechaEntrega" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaEntrega" soloLectura="true" propiedadFormulario="fechaEntrega" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td >&nbsp;</td>
		</tr>
				<tr>
			<td>&nbsp;</td>
			<th>
				<etiquetas:etiquetaError  property="montoGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.montoGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoGasto" id="montoGasto" caracteres="10" 
				onkeypress="return soloNumeros(this, event, true);" 
				onblur="recalcularPorcentajes(this.value)"
				onfocus="new Mask('$#,###.00', 'number').attach(this)" 
				/>
			</td>
			<td colspan="5">&nbsp;</td>
		</tr>
				<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right"><midas:mensaje clave="siniestro.finanzas.gastos.agregar.impuestos"/></td>
			<td align="left" colspan="5">
				&nbsp;<midas:checkBox valorEstablecido="1" onClick="habilitaCamposPorcentaje(this)">IVA</midas:checkBox>
				&nbsp;<midas:checkBox valorEstablecido="2" onClick="habilitaCamposPorcentaje(this)">IVA Ret.</midas:checkBox>
				&nbsp;<midas:checkBox valorEstablecido="3" onClick="habilitaCamposPorcentaje(this)">ISR</midas:checkBox>
				&nbsp;<midas:checkBox valorEstablecido="4" onClick="habilitaCamposPorcentaje(this)">ISR Ret.</midas:checkBox>
				&nbsp;<midas:checkBox valorEstablecido="5" onClick="habilitaCamposPorcentaje(this)">Otros</midas:checkBox>
			</td>
			<td>&nbsp;</td>
		</tr>
				<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIva" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="porcentajeIva" id="porcentajeIva" caracteres="5" deshabilitado="true"
				onkeypress="return soloNumeros(this, event, true);"
				onblur="calculaMontoPorcentaje(this,'montoIva', 'montoGasto')"/>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIva" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIva" id="montoIva" onfocus="new Mask('$#,###.00', 'number').attach(this)"  soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
				<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIvaRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIvaRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="porcentajeIvaRetencion" id="porcentajeIvaRetencion" caracteres="5" deshabilitado="true"
				onkeypress="return soloNumeros(this, event, true);"
				onblur="calculaMontoPorcentaje(this,'montoIvaRetencion','montoGasto')"/>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIvaRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIvaRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIvaRetencion" id="montoIvaRetencion" onfocus="new Mask('$#,###.00', 'number').attach(this)"  soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIsr" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIsr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="porcentajeIsr" id="porcentajeIsr" caracteres="5" deshabilitado="true" 
				onkeypress="return soloNumeros(this, event, true);"
				onblur="calculaMontoPorcentaje(this,'montoIsr','montoGasto')"/>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIsr" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIsr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIsr" id="montoIsr" onfocus="new Mask('$#,###.00', 'number').attach(this)"  soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIsrRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIsrRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="porcentajeIsrRetencion" id="porcentajeIsrRetencion" caracteres="5" deshabilitado="true"
				onkeypress="return soloNumeros(this, event, true);"
				onblur="calculaMontoPorcentaje(this,'montoIsrRetencion','montoGasto')"/>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIsrRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIsrRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIsrRetencion" id="montoIsrRetencion" onfocus="new Mask('$#,###.00', 'number').attach(this)" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeOtros" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeOtros" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" caracteres="5" deshabilitado="true"
				onkeypress="return soloNumeros(this, event, trus);"
				onblur="calculaMontoPorcentaje(this,'montoOtros','montoGasto')"/>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoOtros" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoOtros" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoOtros" id="montoOtros" onfocus="new Mask('$#,###.00', 'number').attach(this)" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="conceptoGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.conceptoGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td colspan="2">&nbsp;
				<midas:comboCatalogo  propiedad="conceptoGasto"  styleId="conceptoGasto" idCatalogo="idTcConceptoGasto"  descripcionCatalogo="descripcion" nombreCatalogo="tcconceptogasto" size="1" styleClass="comboBoxSiniestro"/>
			</td>
			<th align="left">
				<etiquetas:etiquetaError  property="prestadorServicios" requerido="si"
					key="siniestro.finanzas.gastos.agregar.prestadorServicios" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td colspan="2">&nbsp;
				<midas:comboCatalogo propiedad="prestadorServicios"  styleId="prestadorServicios" idCatalogo="idPrestadorServicios"  descripcionCatalogo="nombrePrestador" nombreCatalogo="tcprestadorservicios" size="1" styleClass="comboBoxSiniestro"/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="descripcionGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.descripcionGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:areatexto propiedadFormulario="descripcionGasto" id="descripcionGasto" renglones="5" columnas="50" caracteres="230"/></td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="numeroCheque" requerido="no"
					key="siniestro.finanzas.gastos.agregar.numeroCheque" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroCheque" longitud="10" caracteres="10" onkeypress="return soloNumeros(this, event, false);"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="lugarEnvio" requerido="no"
					key="siniestro.finanzas.gastos.agregar.lugarEnvio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="lugarEnvio" longitud="240" caracteres="230"/></td>
			<td>&nbsp;</td>						
			<th align="left">
				<etiquetas:etiquetaError  property="numeroTransferencia" requerido="no"
					key="siniestro.finanzas.gastos.agregar.transferencia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroTransferencia" longitud="10" caracteres="10" onkeypress="return soloNumeros(this, event, false);"/></td>
			<td  colspan="2">&nbsp;</td>									
		</tr>		
		
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td align="center" >
				<div id="b_regresar">
					<a href='javascript: void(0);' 
						onclick='javascript:listarGastosSiniestro(<bean:write name="gastosForm" property="idToReporteSiniestro"/>);'>
						<midas:mensaje clave="midas.accion.regresar"/>
					</a>
				</div>
			</td> 
			<td align="center" >
				<midas:boton onclick="guardarGastoSiniestro(document.gastosForm)" tipo="guardar" texto="Guardar gasto de ajuste" style="width:160px;"/>&nbsp;&nbsp;&nbsp;				
			</td> 
			<td>&nbsp;</td>			
		</tr>
	</table>
</midas:formulario>
