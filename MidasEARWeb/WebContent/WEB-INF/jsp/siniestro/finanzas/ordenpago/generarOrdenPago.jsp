<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/finanzas/generarOrdenPago">
	<html:hidden property="idToAutorizacionTecnica" styleId="idToAutorizacionTecnica"/>
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="id" styleId="id"/>
	<table width="70%" align="center">
		<tr>
			<td>
			 	<table  id="agregar" border="0">
					<tr>
						<td class="titulo" colspan="5">
							<midas:mensaje clave="siniestro.finanzas.ordenPago.titulo" />				
						</td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<th class="seccion" colspan="5"><midas:mensaje clave="siniestro.finanzas.ordenPago.lista.titulo"/></th>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<td width="20%">&nbsp;</td>
						<th width="20%"><midas:mensaje clave="siniestro.finanzas.ordenPago.lista.numero"/></th>
						<th width="20%"><midas:mensaje clave="siniestro.finanzas.ordenPago.lista.tipo"/></th>
						<th width="20%"><midas:mensaje clave="siniestro.finanzas.ordenPago.lista.monto"/></th>
						<td width="20%">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<midas:escribe propiedad="idToAutorizacionTecnica" nombre="ordenPagoForm"/>
							<logic:equal name="ordenPagoForm" property="idTipoAutorizacionTecnica" value="Gasto">
								<a href="javascript: void(0);" onclick="visualizarATGasto();" ><img border='0px' alt='Autorizacion Tecnica' src='/MidasWeb/img/Search16.gif'/></a>
							</logic:equal>
							<logic:equal name="ordenPagoForm" property="idTipoAutorizacionTecnica" value="Indemnizacion">
								<a href="javascript: void(0);" onclick="visualizarATIndemnizacion();" ><img border='0px' alt='Autorizacion Tecnica' src='/MidasWeb/img/Search16.gif'/></a>
							</logic:equal>
						</td>
						<td><midas:escribe propiedad="tipoAutorizacionTecnica" nombre="ordenPagoForm"/></td>
						<td><midas:escribe propiedad="montoNeto" nombre="ordenPagoForm"/></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<td >&nbsp;</td>
						<td  align="center">
							<midas:boton onclick="guardarOrdenDePago()" tipo="guardar" texto="Generar orden de pago" style="width:160px;"/>
						</td>
						<td >
							<midas:boton onclick="regresarListaOrdenesDePago()" tipo="regresar" texto="Salir" style="width:75px;"/>
						</td>
						<td colspan="2" >&nbsp;</td>
					</tr>
					
			 	</table>
			 </td>
		</tr>
	</table>
</midas:formulario>
