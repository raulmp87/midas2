<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario accion="/siniestro/finanzas/listarOrdenesDePago">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.titulo" />				
			</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" align="center">
				<div  style="width:95%;height:250px;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.noOrden" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.tipoAT" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.numeroAT" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.monto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.estatus" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.imprimir" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.cancelar" /></th>
								<th>Revisar Estatus</th>
							</tr>
						</thead>
						<logic:empty name="ordenPagoForm" property="listaOrdenesDePago">
							<tr>
	                            <td class="datoTabla" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="ordenPagoForm" property="listaOrdenesDePago">
							<logic:iterate name="ordenPagoForm" property="listaOrdenesDePago" id="lista"  indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="numeroOrdenPago" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <logic:notEmpty name="lista" property="autorizacionTecnica.gastoSiniestroDTO">
                                        	<midas:mensaje clave="siniestro.finanzas.ordenPago.tipo.gasto" />
                                        </logic:notEmpty>
                                        <logic:notEmpty name="lista" property="autorizacionTecnica.indemnizacionDTO">
                                        	<midas:mensaje clave="siniestro.finanzas.ordenPago.tipo.indemnizacion" />
                                        </logic:notEmpty>
                                    </td>
                                    <td class="datoTabla" align="right">
                                       <midas:escribe propiedad="autorizacionTecnica.idToAutorizacionTecnica" nombre="lista"/>
                                       		<logic:notEmpty name="lista" property="autorizacionTecnica.gastoSiniestroDTO">
                                       			<a href="javascript: void(0);" onclick="visualizarATGastoLista('<midas:escribe propiedad="autorizacionTecnica.gastoSiniestroDTO.idToGastoSiniestro" nombre="lista"/>','<midas:escribe propiedad="autorizacionTecnica.idToAutorizacionTecnica" nombre="lista"/>');" ><img border='0px' alt='Autorizacion Tecnica' src='/MidasWeb/img/Search16.gif'/></a>
                                       		</logic:notEmpty>
                                       		<logic:notEmpty name="lista" property="autorizacionTecnica.indemnizacionDTO">
                                       			<a href="javascript: void(0);" onclick="visualizarATIndemnizacionLista('<midas:escribe propiedad="autorizacionTecnica.indemnizacionDTO.idToIndemnizacion" nombre="lista"/>','<midas:escribe propiedad="autorizacionTecnica.idToAutorizacionTecnica" nombre="lista"/>');" ><img border='0px' alt='Autorizacion Tecnica' src='/MidasWeb/img/Search16.gif'/></a>
                                       		</logic:notEmpty>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="autorizacionTecnica.montoNeto" formato="$#,###,##0.00" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <logic:equal name="lista" property="estatus" value="0">
                                        	<midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.estatus.pendiente" />
                                        </logic:equal>
                                        <logic:equal name="lista" property="estatus" value="1">
                                        	<midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.estatus.pagado" />
                                        </logic:equal>
                                        <logic:equal name="lista" property="estatus" value="2">
                                        	<midas:mensaje clave="siniestro.finanzas.listarOrdenesPago.lista.estatus.cancelado" />
                                        </logic:equal>
                                    </td>
                                    <td>
                                   		<logic:notEmpty name="lista" property="autorizacionTecnica.gastoSiniestroDTO">
                                      		<center><a href="javascript: void(0);" onclick="mostrarReporteAutorizacionTecnicaGastos('<midas:escribe propiedad="autorizacionTecnica.gastoSiniestroDTO.idToGastoSiniestro" nombre="lista"/>','<midas:escribe propiedad="autorizacionTecnica.idToAutorizacionTecnica" nombre="lista"/>');" >
                                      			<img border='0px' alt='Imprimir' src='/MidasWeb/img/b_printer.gif'/>
                                      		</a></center>
                                      	</logic:notEmpty>
                                      	<logic:notEmpty name="lista" property="autorizacionTecnica.indemnizacionDTO">
                                      		
                                      		<center><a href="javascript: void(0);" onclick="mostrarReporteAutorizacionTecnicaIndemnizacion('<midas:escribe propiedad="autorizacionTecnica.indemnizacionDTO.idToIndemnizacion" nombre="lista"/>','<midas:escribe propiedad="autorizacionTecnica.idToAutorizacionTecnica" nombre="lista"/>');" >
                                      			<img border='0px' alt='Imprimir' src='/MidasWeb/img/b_printer.gif'/>
                                      		</a></center>
                                      	</logic:notEmpty>
                                    </td>
                                    <td align="center">
                                    	<input type="radio" name="idCancelar" id="idCancelar" value="<midas:escribe propiedad="idToOrdenPago" nombre="lista"/>#<midas:escribe propiedad="estatus" nombre="lista"/>"/> 
                                    </td>
                                    <td align="center">
                                    	<center>
                                    		<a href="javascript: void(0);" onclick="verificarEstatusOP(<midas:escribe propiedad="idToOrdenPago" nombre="lista"/>,<midas:escribe propiedad="idToReporteSiniestro" nombre="ordenPagoForm"/>);">
                                      			<img border='0px' alt='Revisar Estatus' src='/MidasWeb/img/information.gif'/>
                                      		</a>
                                      	</center>                                     
                                    </td>                                    
								</tr>
							</logic:iterate>
								
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td align="center" width="20%">
				<midas:boton onclick="listarReportesSiniestro()" tipo="regresar" texto="Regresar" style="width:75px;"/>
			</td>
			<td  align="center" width="20%">
				<midas:boton onclick="cancelarOrdenDePago()" tipo="guardar" texto="Cancela OP" style="width:140px;"/>
			</td>			
			<td width="30%">&nbsp;</td>
		</tr>
		
 	</table>
</midas:formulario>
