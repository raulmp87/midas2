<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/presentarInformeFinal">
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:oculto propiedadFormulario="idDeReporte"/>
				<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.tipoMoneda" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="tipoMoneda" nombre="aprobarInformeFinalForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td width="25%">
				<a href="javascript:void(0);" onclick='javascript: mostrarHistorialReserva(<bean:write name="aprobarInformeFinalForm" property="idDeReporte"/>,"informeFinal")'>
					<midas:mensaje clave="siniestro.finanzas.procedeAIndemnizar.verHistorialReserva"/>
				</a>
				&nbsp;
				<a href="javascript:void(0);" onclick='mostrarDocumentosAsociadosReporteSiniestro(<bean:write name="aprobarInformeFinalForm" property="idDeReporte"/>)'>
					<midas:mensaje clave="siniestro.finanzas.procedeAIndemnizar.verListaDocumentos"/>
				</a>
			</td>
			<td width="25%" align="right" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.valido" />				
			</td>
			<td width="25%" align="left" class="datoTabla">
				<midas:radio propiedadFormulario="valido" valorEstablecido="true">
					<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.si"/>
				</midas:radio>
				<midas:radio propiedadFormulario="valido" valorEstablecido="false">
					<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.no"/>
				</midas:radio>
			</td>
			<td width="25%" colspan="2">&nbsp;</td>		
		</tr>	
		<tr>
			<td colspan="5" align="center">
				<div style="width: 100%; height: 150px; border: 1px solid; overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados">
						<logic:empty name="aprobarInformeFinalForm" property="listadoCoberturas">
							<tr>
	                            <td class="" align="center" colspan="7">
									<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.listaVacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="aprobarInformeFinalForm" property="listadoCoberturas">
							<thead>
								<tr>
		                            <th colspan="7">
										<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.titulo" />
									</th>
		                        </tr>
								<tr>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.inciso" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.subInciso" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.seccion" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.cobertura" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.riesgo" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.sumaAsegurada" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstCoberturas.tipoSA" /></th>
								</tr>
							</thead>
							<logic:iterate name="aprobarInformeFinalForm" property="listadoCoberturas" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="inciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="subInciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="cobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="sumaAsegurada" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="tipoSA" nombre="lista"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
				<br /><br />
				<div style="width: 100%; height: 150px; border: 1px solid; overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados">
						<logic:empty name="aprobarInformeFinalForm" property="listadoDeReservas">
							<tr>
	                            <td class="" align="center" colspan="7">
									<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.listaVacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="aprobarInformeFinalForm" property="listadoDeReservas">
							<thead>
								<tr>
		                            <th colspan="7">
										<midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.titulo" />
									</th>
		                        </tr>
								<tr>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.inciso" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.subInciso" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.seccion" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.cobertura" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.riesgo" /></th>
									<th nowrap><midas:mensaje clave="siniestro.finanzas.aprobarInformeFinal.lstReservas.reserva" /></th>
								</tr> 
							</thead>
							<logic:iterate name="aprobarInformeFinalForm" property="listadoDeReservas" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="inciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="subInciso" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="cobertura" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgo" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="reserva" nombre="lista" formato="$###,###,##0.00"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div id="b_modificar">
					<a href='javascript: void(0);' 
						onclick='javascript: mostrarModificarHistorialReserva(<bean:write name="aprobarInformeFinalForm" property="idDeReporte"/>);'>
						<midas:mensaje clave="midas.accion.modificar"/>
					</a>
				</div>
			</td>
			<td align="center">
				<midas:boton onclick="javascript: sendRequest(document.aprobarInformeFinalForm, '/MidasWeb/siniestro/finanzas/procedeAIndemnizar.do', 'contenido', 'mensajeProcedeIndemnizacion()');" tipo="guardar" texto="Procede a indemnizaci&oacute;n" style="width:160px;"/>
			</td>
			<td>
				<midas:boton onclick="javascript: sendRequest(document.aprobarInformeFinalForm, '/MidasWeb/siniestro/finanzas/noProcedeAIndemnizar.do', 'contenido', 'mensajeNoProcedeIndemnizacion()');" tipo="guardar" texto="No procede a indemnzaci&oacute;n" style="width:180px;"/>
			</td>
			<td>
				<midas:boton onclick="javascript:document.location.href = '/MidasWeb/sistema/inicio.do?idSesion=1234A76G9324HQ36&userName=cabinero';" tipo="regresar" texto="Cancelar" style="width:75px;"/>
			</td>
			<td >&nbsp;</td>
		</tr>
		
 	</table>
</midas:formulario>
