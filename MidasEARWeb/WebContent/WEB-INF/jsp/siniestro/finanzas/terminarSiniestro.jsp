<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/mostrarReporteSiniestro">
	<html:hidden property="idReporteSiniestro" styleId="idReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="left" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.numeroReporte" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="numeroReporte" nombre="terminarSiniestroForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>				
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="left" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.sumaAsegurada" />				
			</td >
			<td width="20%"align="left" >
				<midas:escribe propiedad="sumaAsegurada" nombre="terminarSiniestroForm" formato="###,###,##0.00"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="left" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.autorizarReserva.tipoMoneda" />				
			</td >
			<td width="20%"align="left">
				<midas:escribe propiedad="tipoMoneda" nombre="terminarSiniestroForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>			
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="left" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.terminoAjuste" />				
			</td >
			<td width="20%"align="left" >
				<html:select name="terminarSiniestroForm" property="idTerminoAjuste" styleClass="cajaTexto" styleId="idTerminoAjuste">
			    	<html:optionsCollection name="terminarSiniestroForm" property="terminosAjuste"  value="idTcTerminoAjuste" label="descripcion" />
			    </html:select>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="left" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.terminarSiniestro.descripcionAjuste" />				
			</td >
			<td width="20%"align="left">
				<midas:areatexto propiedadFormulario="descripcionTerminoAjuste" id="descripcionTerminoAjuste" nombreFormulario="terminarSiniestroForm" renglones="4" />
			</td>
			<td width="20%" colspan="2">&nbsp;</td>		
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:85%;height:150px;border :1px ,solid;" class="tableContainer">
					<table width="100%" border="0" class="grid" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.inciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.subInciso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.seccion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.cobertura" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.riesgo" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.autorizarReserva.listas.reserva" /></th>
							</tr>
						</thead>
						<logic:empty name="terminarSiniestroForm" property="listaCoberturas">
							<tr>
	                            <td class="" align="center" colspan="6">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="terminarSiniestroForm" property="listaCoberturas">
							<logic:iterate name="terminarSiniestroForm" property="listaCoberturas" id="lstReservaActual" indexId="indiceRAct" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.id.numeroinciso" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.id.numerosubinciso" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="seccionSoporteDanosDTO.descripcionSeccion" nombre="lstReservaActual"/>                                        
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="coberturaSoporteDanosDTO.descripcionCobertura" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="riesgoSoporteDanosDTO.descripcionRiesgo" nombre="lstReservaActual"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="reservaDetalleDTO.estimacion" nombre="lstReservaActual"/>
                                    </td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>			
		<tr>
			<td >&nbsp;</td>
			<td  align="center">
				<midas:boton onclick="terminarSiniestro(document.terminarSiniestroForm)" tipo="guardar" texto="Terminar siniestro" style="width:197px;"/>
			</td>
			<td >
				<midas:boton onclick="cancelarSiniestro()" tipo="regresar" texto="Cancelar" texto="Cancelar terminaci&oacute;n de siniestro" style="width:207px;"/>
			</td>
			<td >
				<midas:boton onclick="listarReportesSiniestro()" tipo="regresar" texto="Cancelar" style="width:100px;"/>
			</td>			
			<td >&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
