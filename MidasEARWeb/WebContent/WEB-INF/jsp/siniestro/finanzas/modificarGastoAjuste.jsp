<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/siniestro/finanzas/gastosAjuste">
	<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
	<html:hidden property="idToGastoSiniestro" styleId="idToGastoSiniestro"/>
	<html:hidden property="indIva" styleId="indIva"/>
	<html:hidden property="indIvaRet" styleId="indIvaRet"/>
	<html:hidden property="indIsr" styleId="indIsr"/>
	<html:hidden property="indIsrRet" styleId="indIsrRet"/>
	<html:hidden property="indOtros" styleId="indOtros"/>	
	
	<table id="agregar" border="0" >
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="siniestro.finanzas.gastos.agregar.titulo" />				
			</td>			
		</tr>	
		<tr>
			<td align="center" colspan="8" class="datoTabla">
				<midas:mensaje clave="siniestro.finanzas.gastos.agregar.tipoMoneda" />				
				<midas:escribe propiedad="tipoMoneda" nombre="gastosForm"/>
			</td>
		</tr>
		<tr>
			<td width="5%" >&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="29%">&nbsp;</td>
			<td width="1%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
			<td width="15%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="5%">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.fechaGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaGasto" soloLectura="true" propiedadFormulario="fechaGasto" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td colspan="2">&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaRecepcion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.fechaRecepcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaRecepcion" soloLectura="true" propiedadFormulario="fechaRecepcion" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td>&nbsp;</td>
 		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaEstimacionPago" requerido="no"
					key="siniestro.finanzas.gastos.agregar.fechaEstimacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaEstimacionPago" soloLectura="true" propiedadFormulario="fechaEstimacionPago" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td colspan="2">&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="fechaEntrega" requerido="no"
					key="siniestro.finanzas.gastos.agregar.fechaEntrega" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<midas:texto id="fechaEntrega" soloLectura="true" propiedadFormulario="fechaEntrega" caracteres="10" longitud="25"/>
				<img src="<html:rewrite page='/img/b_calendario.gif'/>"	width="12" height="12" style="cursor:hand;"/>
			</td>
			<td >&nbsp;</td>
		</tr>
				<tr>
			<td>&nbsp;</td>
			<th>
				<etiquetas:etiquetaError  property="montoGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.montoGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="montoGasto" id="montoGasto" longitud="10" deshabilitado="true"  										 			
						onfocus="new Mask('$#,###.00', 'number').attach(this)"/>
				</logic:equal>		
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="montoGasto" id="montoGasto" longitud="10"  				
						onkeypress="return soloNumeros(this, event, true);" 
						onblur="recalcularPorcentajes(this.value)"
						onfocus="new Mask('$#,###.00', 'number').attach(this)"/>
				</logic:equal>														
			</td>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="datoTabla" align="right"><midas:mensaje clave="siniestro.finanzas.gastos.agregar.impuestos"/></td>
			<td align="left" colspan="5">			
				<logic:equal name="modoLectura" scope="request" value="true">
					&nbsp;<midas:checkBox valorEstablecido="1" id="idIndIva" deshabilitado="true">IVA</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="2" id="idIndIvaRet"deshabilitado="true">IVA Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="3" id="idIndIsr" deshabilitado="true">ISR</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="4" id="idIndIsrRet" deshabilitado="true">ISR Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="5" id="idIndOtros" deshabilitado="true">Otros</midas:checkBox>				
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">
					&nbsp;<midas:checkBox valorEstablecido="1" onClick="habilitaCamposPorcentaje(this)" id="idIndIva">IVA</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="2" onClick="habilitaCamposPorcentaje(this)" id="idIndIvaRet">IVA Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="3" onClick="habilitaCamposPorcentaje(this)" id="idIndIsr">ISR</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="4" onClick="habilitaCamposPorcentaje(this)" id="idIndIsrRet">ISR Ret.</midas:checkBox>
					&nbsp;<midas:checkBox valorEstablecido="5" onClick="habilitaCamposPorcentaje(this)" id="idIndOtros">Otros</midas:checkBox>
				</logic:equal>													
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIva" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>			
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIva" soloLectura="true" id="porcentajeIva" 
					longitud="5"/>												
				</logic:equal>								
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeIva" id="porcentajeIva" longitud="5" deshabilitado="true"
					onkeypress="return soloNumeros(this, event, true);"
					onblur="calculaMontoPorcentaje(this,'montoIva', 'montoGasto')"/>												
				</logic:equal>								
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIva" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIva" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
			<midas:texto propiedadFormulario="montoIva" id="montoIva" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIvaRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIvaRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>			
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIvaRetencion" id="porcentajeIvaRetencion" longitud="5" soloLectura="true"/>				
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeIvaRetencion" id="porcentajeIvaRetencion" longitud="5" deshabilitado="true"
					onkeypress="return soloNumeros(this, event, true);"
					onblur="calculaMontoPorcentaje(this,'montoIvaRetencion','montoGasto')"/>				
				</logic:equal>						
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIvaRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIvaRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIvaRetencion" id="montoIvaRetencion" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIsr" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIsr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>			
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIsr" id="porcentajeIsr" longitud="5" soloLectura="true"/>								
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeIsr" id="porcentajeIsr" longitud="5" deshabilitado="true"
					onkeypress="return soloNumeros(this, event, true);"
					onblur="calculaMontoPorcentaje(this,'montoIsr','montoGasto')"/>				
				</logic:equal>			
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIsr" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIsr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIsr" id="montoIsr" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeIsrRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeIsrRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeIsrRetencion" id="porcentajeIsrRetencion" longitud="5"  soloLectura="true"/>													
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">				
					<midas:texto propiedadFormulario="porcentajeIsrRetencion" id="porcentajeIsrRetencion" longitud="5" deshabilitado="true"
					onkeypress="return soloNumeros(this, event, true);"
					onblur="calculaMontoPorcentaje(this,'montoIsrRetencion','montoGasto')"/>									
				</logic:equal>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoIsrRetencion" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoIsrRet" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoIsrRetencion" id="montoIsrRetencion" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="porcentajeOtros" requerido="no"
					key="siniestro.finanzas.gastos.agregar.porcentajeOtros" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" longitud="5" deshabilitado="true" soloLectura="true"/>				
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:texto propiedadFormulario="porcentajeOtros" id="porcentajeOtros" longitud="5" deshabilitado="true"
					onkeypress="return soloNumeros(this, event, true);"
					onblur="calculaMontoPorcentaje(this,'montoOtros','montoGasto')"/>				
				</logic:equal>
			</td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="montoOtros" requerido="no"
					key="siniestro.finanzas.gastos.agregar.montoOtros" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="montoOtros" id="montoOtros" soloLectura="true"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="conceptoGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.conceptoGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td colspan="2">&nbsp;			
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:comboCatalogo 
						propiedad="conceptoGasto"  
						styleId="conceptoGasto" 
						idCatalogo="idTcConceptoGasto"  
						descripcionCatalogo="descripcion" 
						nombreCatalogo="tcconceptogasto" 
						size="1" 
						styleClass="comboBoxSiniestro"
						readonly="true"
						/>								
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:comboCatalogo 
						propiedad="conceptoGasto"  
						styleId="conceptoGasto" 
						idCatalogo="idTcConceptoGasto"  
						descripcionCatalogo="descripcion" 
						nombreCatalogo="tcconceptogasto" 
						size="1" 
						styleClass="comboBoxSiniestro"
						/>				
				</logic:equal>									
			</td>
			<th align="left">
				<etiquetas:etiquetaError  property="prestadorServicios" requerido="si"
					key="siniestro.finanzas.gastos.agregar.prestadorServicios" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td colspan="2">&nbsp;
			
				<logic:equal name="modoLectura" scope="request" value="true">
					<midas:comboCatalogo 
						propiedad="prestadorServicios"  
						styleId="prestadorServicios" 
						idCatalogo="idPrestadorServicios"  
						descripcionCatalogo="nombrePrestador" 
						nombreCatalogo="tcprestadorservicios" 
						size="1" 
						styleClass="comboBoxSiniestro"
						readonly = "true"/>												
				</logic:equal>				
				<logic:equal name="modoLectura" scope="request" value="false">
					<midas:comboCatalogo 
						propiedad="prestadorServicios"  
						styleId="prestadorServicios" 
						idCatalogo="idPrestadorServicios"  
						descripcionCatalogo="nombrePrestador" 
						nombreCatalogo="tcprestadorservicios" 
						size="1" 
						styleClass="comboBoxSiniestro"/>								
				</logic:equal>										
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="descripcionGasto" requerido="si"
					key="siniestro.finanzas.gastos.agregar.descripcionGasto" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:areatexto propiedadFormulario="descripcionGasto" id="descripcionGasto" renglones="5" columnas="50"/></td>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="numeroCheque" requerido="no"
					key="siniestro.finanzas.gastos.agregar.numeroCheque" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroCheque" id="numeroCheque" longitud="10" onkeypress="return soloNumeros(this, event, false);"/></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<th align="left">
				<etiquetas:etiquetaError  property="lugarEnvio" requerido="no"
					key="siniestro.finanzas.gastos.agregar.lugarEnvio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="lugarEnvio" longitud="240"/></td>
			<td>&nbsp;</td>						
			<th align="left">
				<etiquetas:etiquetaError  property="numeroTransferencia" requerido="no"
					key="siniestro.finanzas.gastos.agregar.transferencia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td><midas:texto propiedadFormulario="numeroTransferencia" id="numeroTransferencia" longitud="10" onkeypress="return soloNumeros(this, event, false);"/></td>
			<td  colspan="2">&nbsp;</td>									
		</tr>		
		
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td align="center" >
				<div id="b_regresar">
					<a href='javascript: void(0);' 
						onclick='javascript:listarGastosSiniestro(<bean:write name="gastosForm" property="idToReporteSiniestro"/>);'>
						<midas:mensaje clave="midas.accion.regresar"/>
					</a>
				</div>
			</td> 
			<td align="center" >
				<midas:boton onclick="guardarCambiosGastoSiniestro(document.gastosForm)" tipo="guardar" texto="Guardar gasto de ajuste" style="width:160px;"/>&nbsp;&nbsp;&nbsp;				
			</td> 
			<td>&nbsp;</td>
		</tr>
	</table>
</midas:formulario>
