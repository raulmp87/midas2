<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<link href="../css/midas.css" rel="stylesheet" type="text/css" />
<midas:formulario accion="/siniestro/finanzas/listarIngresos">
<html:hidden property="idToReporteSiniestro" styleId="idToReporteSiniestro"/>
 	<table width="90%" id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="siniestro.finanzas.ingresos.titulo" />				
			</td>
		</tr>	
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="20%" align="center" class="datoTabla" >
				<midas:mensaje clave="siniestro.finanzas.ingresos.tipoMoneda" />:				
			</td >
			<td width="20%"align="left" class="datoTabla">
				<midas:escribe propiedad="tipoMoneda" nombre="ingresosForm"/>
			</td>
			<td width="30%" colspan="2">&nbsp;</td>		
		</tr>		
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<div  style="width:880px;height:150px;border :1px ,solid;overflow: auto;" class="tableContainer">
					<table style="white-space:normal;" class="tablaConResultados" >
						<thead>
							<tr>
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.fechaCobro" /></th>								
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.concepto" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.montoIngreso" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.descripcion" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.fichaDeposito" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.gastos.agregar.transferencia" /></th>								
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.estatus" /></th>																
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.eliminar" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.cancelar" /></th>
								<th><midas:mensaje clave="siniestro.finanzas.ingresos.modificar" /></th>								
							</tr>
						</thead>
						<logic:empty name="ingresosForm" property="listaIngresos">
							<tr>
	                            <td class="datoTabla" align="center" colspan="7">
									<midas:mensaje clave="lista.vacia" />
								</td>
	                        </tr>
	                    </logic:empty>
	                    <logic:notEmpty name="ingresosForm" property="listaIngresos">
							<logic:iterate name="ingresosForm" property="listaIngresos" id="lista" indexId="indice" >
								<tr>
									<td class="datoTabla" align="left">
                                        <midas:escribe propiedad="fechaCobro" nombre="lista" formato="dd/MM/yyyy"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="conceptoIngreso.descripcion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="right">
                                        <midas:escribe propiedad="monto" nombre="lista" formato="###,###,##0.00"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="descripcion" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="fichaDeposito" nombre="lista"/>
                                    </td>
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="transferencia" nombre="lista"/>
                                    </td>                                    
                                    <td class="datoTabla" align="left">
                                        <midas:escribe propiedad="estatusFinanzas.descripcion" nombre="lista"/>
                                    </td>       
                                   	<td class="datoTabla" align="left">
                                   		<logic:equal name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                   			<input type="checkbox" name="eliminarIngreso" value="<midas:escribe propiedad="idToIngresoSiniestro" nombre="lista"/>"/>
                                   		</logic:equal>            
                                   		<logic:notEqual name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                   			<input type="checkbox" name="eliminarIngreso" value="<midas:escribe propiedad="idToIngresoSiniestro" nombre="lista"/>" disabled="true"/>
                                   		</logic:notEqual>
                                   	</td>
                                    <td class="datoTabla" align="left">
                                    	<logic:equal name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                    		<input type="checkbox" name="cancelarIngreso" value="<midas:escribe propiedad="idToIngresoSiniestro" nombre="lista"/>" disabled="true"/>
                                    	</logic:equal>
                                    	<logic:notEqual name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="1">
                                    		<logic:equal name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="4">
                                    			<input type="checkbox" name="cancelarIngreso" value="<midas:escribe propiedad="idToIngresoSiniestro" nombre="lista"/>" disabled="true"/>
                                    		</logic:equal>
                                    		<logic:notEqual name="lista" property="estatusFinanzas.idTcEstatusfinanzas" value="4">
                                    			<input type="checkbox" name="cancelarIngreso" value="<midas:escribe propiedad="idToIngresoSiniestro" nombre="lista"/>" />
                                    		</logic:notEqual>
                                    	</logic:notEqual>
                                    </td>
                                    <td class="datoTabla" align="left">                                   
                                        <a href="javascript: void(0);" onclick="javascript:sendRequest(document.ingresosForm,'/MidasWeb/siniestro/finanzas/modificarIngresos.do?idToIngresoSiniestro=<midas:escribe propiedad="idToIngresoSiniestro" nombre="lista"/>','contenido','funcionesModificarIngresos()');">
											<img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/>
										</a>
                                    </td>																										                                                                                                                                                                                 																										
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a onclick="javascript: listarReportesSiniestro();" href="javascript: void(0);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
			<td >
				<div class="alinearBotonALaDerecha">
					<div id="b_agregar">
						<a onclick='javascript: mostrarAgregarIngresos(<midas:escribe propiedad="idToReporteSiniestro" nombre="ingresosForm"/>);' href="javascript: void(0);"><midas:mensaje clave="midas.accion.agregar"/></a>
					</div>
				</div>
			</td>
			<td  align="center">
				<midas:boton onclick="eliminarIngresos()" tipo="guardar" texto="Eliminar Ingresos" style="width:120px;"/>
			</td>
			<td >
				<midas:boton onclick="cancelarIngresos()" tipo="guardar" texto="Cancelar Ingresos" style="width:120px;"/>
			</td>			
			<td colspan="1" >&nbsp;</td>
		</tr>
 	</table>
</midas:formulario>
