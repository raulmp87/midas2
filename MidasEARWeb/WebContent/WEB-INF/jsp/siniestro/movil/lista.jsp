<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>

<s:if test="estatusId == 1">
	<s:set var="titulo" value="'Sin Asignar'"/>
</s:if>
<s:elseif test="estatusId == 2">
	<s:set var="titulo" value="'Asignados'"/>	
</s:elseif>
<s:elseif test="estatusId == 3">
	<s:set var="titulo" value="'Terminados'"/>		
</s:elseif>

<h2>Listado de Reporte de Siniestros Móvil <s:property value="#titulo"/></h2>

<s:if test="estatusId == 2">
Asignados a mí: <input id="asignadosAMiCheckbox" type="checkbox"/>
</s:if>
<div id="reporteSiniestroMovilGrid" style="height: 275px;width:98%"></div>
<div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div>
<s:url action="listarDhtmlx" namespace="/siniestro/reporte-movil" var="listarDhtmlxUrl">
	<s:param name="estatusId" value="estatusId"/>
</s:url>

<script>
	var reporteSiniestroMovilDhtmlxListUrl = "<s:property value='#listarDhtmlxUrl'/>";
	var reporteSiniestroMovilGrid;
	var reporteSiniestroMovilListaEstatusId = <s:property value='estatusId'/>;
	var paginationEnabled = <s:property value='paginationEnabled'/>;
	var cabinaId = "<s:property value='usuarioActual.cabina.id'/>";
	var restringirCabina = "<s:property value='restringirCabina'/>";
	inicializarListaSiniestroMovil();
</script>
