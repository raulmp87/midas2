<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script
	src="<s:url value='/js/midas2/siniestros/cabina/reportecabina/reportecabina.js'/>"></script>
<h2>Reporte de Siniestro Móvil</h2>
<s:set name="siguienteEstatus" value="reporteSiniestroMovil.estatus.id + 1"/>
<s:form id="reporteSiniestroMovilForm" action="%{'cambiarEstatus' + #siguienteEstatus}">
	<div style="width: 50%;float:left">
	<s:hidden name="id" value="%{reporteSiniestroMovil.id}"/>
	<s:hidden name="cveOficina" value="%{reporteSiniestroMovil.id}"/>
	<s:hidden name="consReporte" value="%{reporteSiniestroMovil.id}"/>
	<s:hidden name="anioReporte" value="%{reporteSiniestroMovil.id}"/>

	<s:textfield name="reporteSiniestroMovil.id" key="midas.reportesiniestromovil.numeroReporteSiniestroMovil" readonly="true" labelposition="left" cssClass="txtfield" size="10"/>
	<s:textfield name="reporteSiniestroMovil.cabina.nombre" key="midas.reportesiniestromovil.cabina" readonly="true" labelposition="left" cssClass="txtfield" size="25"/>
	<s:textfield name="reporteSiniestroMovil.nombreUsuarioAsignado" key="midas.reportesiniestromovil.nombreUsuarioAsignado" readonly="true" labelposition="left" cssClass="txtfield" size="25"/>
	<s:textfield name="reporteSiniestroMovil.tipoSiniestroMovil.productoReporteSiniestroMovil.nombre" key="midas.reportesiniestromovil.producto" readonly="true" labelposition="left"  cssClass="txtfield" size="45"/>
	<s:textfield name="reporteSiniestroMovil.tipoSiniestroMovil.nombre" key="midas.reportesiniestromovil.tipoSiniestro" readonly="true" labelposition="left" cssClass="txtfield" size="45"/>
	<s:textfield name="reporteSiniestroMovil.polizaDTO.numeroPolizaFormateada" key="midas.reportesiniestromovil.numeroPoliza" readonly="true" labelposition="left" cssClass="txtfield" size="45"/>
	<s:textfield name="reporteSiniestroMovil.numeroInciso" key="midas.reportesiniestromovil.numeroInciso" readonly="true" labelposition="left" cssClass="txtfield" size="45"/>
	<s:textfield name="reporteSiniestroMovil.numeroCelular" key="midas.reportesiniestromovil.numeroCelular" readonly="true" labelposition="left" cssClass="txtfield" size="45"/>
	<s:textfield name="reporteSiniestroMovil.nombrePersonaReporta" key="midas.reportesiniestromovil.nombrePersonaReporta" readonly="true" labelposition="left" cssClass="txtfield" size="45"/>
	<s:date name="reporteSiniestroMovil.fechaCreacion" var="formattedDate" format="dd/MM/yyyy hh:mm a"/>
	<s:textfield name="reporteSiniestroMovil.fechaCreacion" key="midas.reportesiniestromovil.fechaCreacion" value="%{#formattedDate}" readonly="true" labelposition="left" cssClass="txtfield" size="45"/>
	<s:select list="oficinas" id="oficinas" labelposition="left"
		name="reporteSiniestroMovil.oficina.id" key="midas.servicio.siniestros.oficina"
		cssClass="requerido setNew txtfield txtfield" headerKey=""
		headerValue="%{getText('midas.general.seleccione')}">
	</s:select>
	<s:checkbox id="falsaAlarma" name="reporteSiniestroMovil.falsaAlarma" key="midas.reportesiniestromovil.falsaAlarma" labelposition="left" cssClass="txtfield" />
	<s:if test="reporteSiniestroMovil.estatus == @mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil$Estatus@TERMINADA && !reporteSiniestroMovil.falsaAlarma">
		<s:textfield id="numeroReporteSiniestro" readonly="readonly" name="reporteSiniestroMovil.numeroReporteSiniestro" key="midas.reportesiniestromovil.numeroReporteSiniestro" labelposition="left" cssClass="txtfield" size="45"/>		
	</s:if>
	</div>
	<div style="width: 50%;float:left">
		<s:if test="reporteSiniestroMovil.coordenadas != null">
			<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?q=<s:property value="reporteSiniestroMovil.coordenadas.latitud"/>,<s:property value="reporteSiniestroMovil.coordenadas.longitud"/>&amp;ie=UTF8&amp;t=m&amp;z=17&amp;output=embed"></iframe>
		</s:if>
		<s:else>
			Sin coordenadas registradas.
		</s:else>
	</div>
	<div style="clear:both"></div>
	<br/>
	<div id="divRegresarBtn" style="display: block; float: right;">
		<div class="btn_back">
			<%-- Solo el usuario asignado puede terminar el reporte. Esta validación tambien se debería hacer en el action para mayor seguridad. se quita pruebas no liberar y regresar la validacion correcta && usuarioActual.nombreUsuario == reporteSiniestroMovil.nombreUsuarioAsignado--%>
			<s:if test="reporteSiniestroMovil.estatus == @mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil$Estatus@ASIGNADA ">
			<s:submit key="midas.reportesiniestromovil.terminar"
				onclick="sendRequestJQ(jQuery('#reporteSiniestroMovilForm'), jQuery('#reporteSiniestroMovilForm').attr('action'), 'contenido'); return false;"
				cssClass="b_submit icon_regresar w100" />
			</s:if>
		</div>
	</div>
</s:form>