<?xml version="1.0" encoding="UTF-8"?>
<%@ page pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<%-- Necesario hacer esto para que se invoque el getter de reporteSiniestros que inicializa la propiedad total utilizada en total_count--%>
<s:set value="reporteSiniestros" var="x"/>

<rows total_count="<s:property value="total"/>" pos="<s:property value="posStart"/>">
	<s:if test="includeDhtmlxHeader">
	<head>
		<beforeInit>
            <call command="setImagePath"><param><s:url value="/img/dhtmlxgrid/"/></param></call>
            <call command="setSkin"><param>light</param></call>
        </beforeInit>
        <column id="id" type="ro" width="65"><s:text name="midas.reportesiniestromovil.numeroReporteSiniestroMovil"/></column>
        <column id="negocio" type="ro" width="75"><s:text name="midas.reportesiniestromovil.producto"/></column>
		<column id="tipoSiniestro" type="ro" width="150"><s:text name="midas.reportesiniestromovil.tipoSiniestro"/></column>
		<column id="numeroPoliza" type="ro" width="100"><s:text name="midas.reportesiniestromovil.numeroPoliza"/></column>
		<column id="numeroInciso" type="ro" width="50"><s:text name="midas.reportesiniestromovil.numeroInciso"/></column>
		<column id="numeroCelular" type="ro" width="75"><s:text name="midas.reportesiniestromovil.numeroCelular"/></column>
		<column id="nombrePersonaReporta" type="ro" width="200"><s:text name="midas.reportesiniestromovil.nombrePersonaReporta"/></column>
		<column id="fechaCreacion" type="ro" width="140"><s:text name="midas.reportesiniestromovil.fechaCreacion"/></column>
		<column id="cabina" type="ro" width="120"><s:text name="midas.reportesiniestromovil.cabina"/></column>
		<column id="estatus" type="ro" width="120"><s:text name="midas.reportesiniestromovil.estatus"/></column>
		<column id="nombreUsuarioAsignado" type="ro" width="80"><s:text name="midas.reportesiniestromovil.nombreUsuarioAsignado"/></column>
		<column id="falsaAlarma" type="ro" width="80"><s:text name="midas.reportesiniestromovil.falsaAlarma.corto"/></column>
		<column id="numeroReporteSiniestro" type="ro" width="80"><s:text name="midas.reportesiniestromovil.numeroReporteSiniestro.corto"/></column>
		<column id="detalle" type="img" width="25"></column>
		<column id="asignarYVer" type="img" width="25"></column>
	</head>
	</s:if>
	<s:iterator value="reporteSiniestros" var="reporteSiniestro">
		<row id="<s:property value="id"/>">
			<cell><s:property value="id"/></cell>
			<cell><s:property value="tipoSiniestroMovil.productoReporteSiniestroMovil.nombre" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="tipoSiniestroMovil.nombre" escapeHtml="false" escapeXml="true"/></cell>	
			<cell><s:property value="polizaDTO.numeroPolizaFormateada" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroInciso" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="numeroCelular" escapeHtml="false" escapeXml="true"/></cell>			
			<cell><s:property value="nombrePersonaReporta" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:date name="fechaCreacion" format="dd/MM/yyyy hh:mm a" /></cell>
			<cell><s:property value="cabina.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="estatus.nombre" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:property value="nombreUsuarioAsignado" escapeHtml="false" escapeXml="true"/></cell>
			<cell><s:if test="falsaAlarma != null && falsaAlarma">Sí</s:if><s:elseif test="falsaAlarma != null && !falsaAlarma">No</s:elseif></cell>
			<cell><s:property value="numeroReporteSiniestro" escapeHtml="false" escapeXml="true"/></cell>
			<s:if test="estatus != @mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil$Estatus@SIN_ASIGNAR ">
			<cell><s:url value='/img/icons/ico_verdetalle.gif'/>^Ver detalle^javascript:sendRequestJQ(null, "<s:url action='detalle'/>?id=<s:property value='id'/>", "contenido", null)^_self</cell>
			</s:if>
			<s:else>
			<cell type="ro"></cell>
			</s:else>
			<s:if test="estatus == @mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil$Estatus@SIN_ASIGNAR">
			<cell><s:url value='/img/icons/ico_asignar.gif'/>^Asignar^javascript:sendRequestJQ(null, "<s:url action='cambiarEstatus2'/>?id=<s:property value='id'/>&amp;reporteSiniestroMovil.version=<s:property value='version'/>", "contenido", null)^_self</cell>
			</s:if>
			<s:else>
			<cell type="ro"></cell>
			</s:else>
		</row>
	</s:iterator>
</rows>
