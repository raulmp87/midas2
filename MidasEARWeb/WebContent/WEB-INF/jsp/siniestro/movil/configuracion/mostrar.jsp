<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<style type="text/css">
	.errors {
		background-color: #FFCCCC;
		border: 1px solid #CC0000;
		width: 1000px;
		padding: 10px;
	}
	
	.success {
		background-color: #DDFFDD;
		border: 1px solid #009900;
		width: 1000px;
		padding: 10px;
	}
	#listaEstados {
		width: 250px;
		height: 400px;
	}
</style>
<script type="text/javascript">
	function guardarConfiguracionSiniestroMovil() {
		sendRequestJQ('#configuracionSiniestroMovilForm', '/MidasWeb/siniestro/reporte-movil/configuracion/guardar.action','contenido');
	}
</script>
</head>
<body>
	<div class="titulo w800">Configuraci&#243;n Siniestro M&#243;vil</div>
	<s:if test="hasActionErrors()">
		<div class="errors">
			<s:actionerror />
		</div>
	</s:if>
	<s:if test="hasActionMessages()">
		<div class="success">
			<s:actionmessage />
		</div>
	</s:if>
	<s:form action="guardar" id="configuracionSiniestroMovilForm">
		<fieldset>
			<legend>Estados donde el cliente puede dar seguimiento de su
				ajustador</legend>
			<s:select id="listaEstados" list="states" listKey="id"
				listValue="descripcion" name="selectedStates" multiple="true" />
			<span>* Para seleccionar varios elementos presiona la tecla <i>CTRL</i>
				mientras haces clic
			</span>
			<div class="btn_back w130" style="display: inline; float: right;">
				<a href="javascript: void(0);" onclick="guardarConfiguracionSiniestroMovil();">
					Guardar </a>
			</div>
		</fieldset>
	</s:form>
</body>
</html>