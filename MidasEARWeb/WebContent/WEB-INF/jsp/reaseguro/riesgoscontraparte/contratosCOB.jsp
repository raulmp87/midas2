<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>

<div id="detalle" name="Detalle">
	<%
	String negocio = request.getAttribute("negocio").toString();
	%>
	
	<div class="subtituloIzquierdaDiv">
		Contratos de Cobertura de Exceso de Perdida
	</div>
	<center>
		<midas:formulario accion="/reaseguro/riesgoscontraparte/mostrarContratosCOB">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Corte
						
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="contratosCOBForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td>
					
				</tr>
				<tr>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th>Ramo</th>
					<td>
						
							<midas:ramorcs styleId="id_ramo" size="1" propiedad="id_ramo" styleClass="cajaTexto" claveNegocio="<%=negocio%>"/>
						
					</td>
				</tr>
				<tr>
					<th>Clave Esquemas REA</th>
					<td>
						<html:text styleId="claveEsquemas" property="claveEsquemas" name="contratosCOBForm" size="10"  styleClass="cajaTexto"/>
					</td>
					
				</tr>
				<tr>
					<th>Monto</th>
					<td>
						<html:text styleId="monto" property="monto" name="contratosCOBForm" size="10"  styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<th>Calificación</th>
					<td>
						<html:text styleId="calificacion" property="calificacion" name="contratosCOBForm" size="10"  styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<div class="alinearBotonALaDerecha">
						  <div style="margin-left: 0px;">
							<midas:boton onclick="agregarContratosCobertura(document.contratosCOBForm);esFechaValida(fechaInicial)" tipo="agregar" texto="Guardar"/>
						  </div>
					  </div>
					</td>
					
				</tr>
				<tr>
					<td colspan="4">
						&nbsp;
						<input type="hidden" value="<%=negocio%>" id="cveNegocio">
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>