<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>

<div id="resultados">
	<midas:tabla idTabla="solicitudes"
		claseDecoradora="mx.com.afirme.midas.decoradores.ContratosCobertura"
		claseCss="tablaConResultados" nombreLista="documentos"
		urlAccion="/MidasWeb/reaseguro/riesgoscontraparte/listarDocumentos.do">
		<midas:columna propiedad="fechaCorte" titulo="Fecha Corte" />
		<midas:columna propiedad="ramo" titulo="Ramo" />
		<midas:columna propiedad="cveEsquema" titulo="Clave Esq. REA" />
		<midas:columna propiedad="monto" titulo="Monto" />
		<midas:columna propiedad="calificacion" titulo="Calificación" />
		<midas:columna propiedad="acciones"/>
	</midas:tabla>

</div>