<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<table width="100%">
	<tr>
		<td class="titulo" colspan="6">
			<midas:mensaje clave="reaseguro.egreso.ordenpago.titulo" />
		</td>
	</tr>
	<tr>
		<td
			style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px;">
			<!-- midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso"/-->
			Polizas Relacionadas
		</td>
	</tr>
	<tr>
		<td width="100%">
			<div id="gridboxPolizaRelacionada" class="dataGridConfigurationClass"
				style="width: 100%; height: 100px;"></div>
		</td>
	</tr>
	<tr>
		<td width="100%">
			<table id="filtros" style="width: 100%;">
				<tr>
					<td>
						Nombre del Contrato
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>
					<td>
						Vigencia
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>

				</tr>
				<tr>
					<td>
						Monto del Pago
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>
					<td>
						Moneda
					</td>
					<td>
						<select>
							Seleccione...
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td
			style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px;">
			<!-- midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso"/-->
			Participacion de los Reaseguradores del Corredor
		</td>
	</tr>
	<tr>
		<td width="100%">
			<div id="gridboxParticipacionReaseguradores"
				class="dataGridConfigurationClass"
				style="width: 99%; height: 100px;"></div>
		</td>
	</tr>
	<tr>
		<td
			style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px;">
			<!-- midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso"/-->
			Participacion de los Reaseguradores del Corredor
		</td>
	</tr>
	<tr>
		<td width="100%">
			<div id="gridboxParticipacionReaseguradoresCorredor"
				class="dataGridConfigurationClass"
				style="width: 99%; height: 100px;"></div>
		</td>
	</tr>
	<tr>
		<td
			style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px;">
			<!-- midas:mensaje clave="contratofacultativo.configuracion.cobertura.numeroInciso"/-->
			Pagos
		</td>
	</tr>
	<tr>
		<td width="100%">
			<div id="gridboxOrdenDePagos2" class="dataGridConfigurationClass"
				style="width: 99%; height: 100px;"></div>
		</td>
	</tr>
	<tr>
		<td
			style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px;">
			Datos Bancarios
		</td>
	</tr>
	<tr>
		<td>
			<table id="filtros" style="width: 100%;">
				<tr>
					<td>
						Moneda
					</td>
					<td>
						<select>
							Seleccione...
						</select>
					</td>

					<td>
						Banco
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>
					<td>
						Cuenta
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>

				</tr>
				<tr>
					<td>
						ABA
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>
					<td>
						Beneficiario
					</td>
					<td>
						<input type="text" class="cajaTextoAdminEgresos" />
					</td>
					<td>
						SWIFT
					</td>
					<td>
						<input type="text" class="cajaTexto" />
					</td>

				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="100%" class="guardar">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(null,'/MidasWeb/reaseguro/egresos/administrarEgresos.do',contenido,'mostraGridEgresosAseguradorSimple();mostraGridEgresosAseguradorFiltrado();mostraGridListaEgresos();');">Regresar</a>
							</div>
							<div id="b_guardar">
								<a href="javascript: void(0);"
									onclick="javascript: closeMostrarRegistrarEgreso();">Imprimir</a>
							</div>
		
							<div id="b_guardar">
								<a href="javascript: void(0);">Exportar</a>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
