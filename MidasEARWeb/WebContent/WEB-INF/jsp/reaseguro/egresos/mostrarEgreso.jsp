<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>



<midas:formulario accion="/reaseguro/egresos/agregarEgreso">
	<html:hidden property="idEgreso" styleId="idEgreso"/>
	<html:hidden property="idTcMonedaEdosCta" styleId="idTcMonedaEdosCta" />
	<html:hidden property="tipoCambioNecesario" styleId="tipoCambioNecesario" />
	<center>
	    <table id="filtros" width="97%" style="margin-left:auto;margin-right:auto;text-align:left">
		 	<tr>
		 	 	<td class="titulo" colspan="4">
		 	 		<midas:mensaje clave="reaseguro.egreso.registrar.egreso" />
		 	 	</td>
		 	</tr>
		 	<tr>
		 		<td width="25%"></td>
				<td width="15%" align="right">
					<b><midas:mensaje clave="reaseguro.egreso.registrar.monto" />:</b>
				</td>
				<td width="35%">
					<midas:texto propiedadFormulario="monto" id="monto" nombreFormulario="administrarEgresosForm" caracteres="16" soloLectura="true" />
					<html:hidden property="montoEgresoValor" styleId="montoEgresoValor" />
				</td>
				<td width="25%"></td>	
			</tr>		
			<tr>
				<td></td>
				<td align="right">
					<b><midas:mensaje clave="reaseguro.egreso.registrar.referencia"/>:</b>
				</td>
				<td>
					<midas:texto propiedadFormulario="referencia" nombreFormulario="administrarEgresosForm" deshabilitado="false" />
				</td>
				<td></td>	
			</tr>	
			<tr>
				<td></td>
				<td align="right">
					<b><midas:mensaje clave="reaseguro.egreso.registrar.cuentaafirme"/>:</b>
				</td>
				<td>
					<midas:texto propiedadFormulario="cuentaAfirme" nombreFormulario="administrarEgresosForm" deshabilitado="false" />
				</td>
				<td></td>	
			</tr>	
			<tr>
				<td></td>
				<td align="right">
					<b><midas:mensaje clave="reaseguro.egreso.registrar.fecha" />:</b>
				</td>
				<td>
					<table>
						<tr>
							<td width="60%">
								<midas:texto propiedadFormulario="fecha" nombreFormulario="administrarEgresosForm" id="fecha" soloLectura="true" onfocus="mostrarCalendarioAgregarEgreso()" />
							</td>
							<td width="10%">
									<img src="/MidasWeb/img/b_calendario.gif" onclick="mostrarCalendarioAgregarEgreso()" style="cursor:pointer">
							</td>
							<td width="30%">
								<div id="calendarioDiv" style="position:absolute;z-index:1;"></div>
							</td>
						</tr>
					</table>
				</td>
				<td></td>
			</tr>			
			<tr>
				<td></td>
				<td align="right">
					<b><midas:mensaje clave="reaseguro.egreso.registrar.moneda"/>:</b>
				</td>
				<td>					
					<midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="administrarEgresosForm" styleClass="cajaTexto"
										 nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="false" onchange="mostrarOcultarTipoCambioEgresosReaseguro(this.value)"/>					
				</td>
				<td></td>	
			</tr>
			<tr>
				<td></td>
				<td align="right">					
					<div id="leyendaTipoCambioDiv" style="display:none">
						<b><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.tipoDeCambio" />:</b>
					</div>					
				</td>
				<td>
					<div id="tipoCambioDiv" style="display:none">					
						<midas:texto propiedadFormulario="tipoCambio" id="tipoCambio"
									 nombreFormulario="administrarEgresosForm" deshabilitado="false"
									 caracteres="10"
									 onblur="actualizarMontoEgreso();"
									 onchange="actualizarMontoEgreso();"
									 onkeypress="return soloNumeros(this,event,true)" />
					</div>
				</td>		
				<td>					
					<div id="pesosXDolarDiv" style="display:none">
						<b><i><midas:mensaje clave="reaseguro.egreso.administrar.egresos.pesosxdolar"/></i></b>					
					</div>
				</td>
			</tr>			
		</table>
	</center>
	<center>
		<html:hidden property="mostrarGridEdosCuentaAutomaticos" name="administrarEgresosForm" styleId="mostrarGridEdosCuentaAutomaticos" />
		<logic:equal property="mostrarGridEdosCuentaAutomaticos" name="administrarEgresosForm" value="true">
			<table width="97%">
				<tr>
					<td class="titulo" colspan="4">
		 	 			<midas:mensaje clave="reaseguro.egreso.registrar.estadoscuentaautomaticos" />
		 	 		</td>
		 	 	</tr>
		 	 </table>			
			 <div id="gridboxEdosCuentaAutomaticos"></div>				   			
		</logic:equal>
		<html:hidden property="mostrarGridEdosCuentaFacultativos" name="administrarEgresosForm" styleId="mostrarGridEdosCuentaFacultativos" />
		<logic:equal property="mostrarGridEdosCuentaFacultativos" name="administrarEgresosForm" value="true">
			<table width="97%">
				<tr>
					<td class="titulo" colspan="4">
		 	 			<midas:mensaje clave="reaseguro.egreso.registrar.estadoscuentafacultativos" />
		 	 		</td>
		 	 	</tr>
		 	</table>			
			<div id="gridboxEdosCuentaFacultativos"></div>				    				   
			<table width="97%">
				<tr>
					<td class="b_ver_coberturas">
						<div class="alinearBotonALaDerecha">						
							<div id="b_ver_coberturas">
								<a href="javascript: void(0);" onclick="javascript: mostrarCoberturasEstadoCuenta();"><midas:mensaje clave="midas.accion.vercoberturas"/></a>
							</div>
						</div>
					</td>
				</tr>
			</table>			
			<div id="coberturasEdosCuentaFacultativos">
				<div id="gridboxCoberturasEdosCuentaFacultativos"></div>
				<table width="97%">
					<tr>
						<td class="b_ver_exhibiciones">
							<div class="alinearBotonALaDerecha">						
								<div id="b_ver_exhibiciones">
									<a href="javascript: void(0);" onclick="javascript: mostrarExhibicionesCoberturaEstadoCuenta();"><midas:mensaje clave="midas.accion.verexhibiciones"/></a>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id="exhibicionesCoberturasEdosCuentaFacultativos">	
				<table width="97%">
					<tr>		
						<td width="25%"></td>		
						<th style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;" align="right" valign="bottom" width="5%">
							<midas:mensaje clave="reaseguro.egreso.registrar.seccion" />:
						</th>
						<td width="20%">					
							<html:select property="idToSeccion" styleId="idToSeccion" styleClass="cajaTexto" 
										 onchange="javascript: llenarCombo(this,'idToCobertura','/MidasWeb/reaseguro/egresos/cargarComboCoberturas.do');" >
								<html:option value="">SELECCIONE...</html:option>
							</html:select>		 				
						</td>
						<th style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;" align="right" valign="bottom" width="5%">
							<midas:mensaje clave="reaseguro.egreso.registrar.cobertura" />:
						</th>
						<td width="20%">
							<html:select property="idToCobertura" styleId="idToCobertura" styleClass="cajaTexto">
								<html:option value="">SELECCIONE...</html:option>														
							</html:select>
						</td>
						<td class="b_seleccionar" width="15%">
							<div class="alinearBotonALaDerecha">							
								<div id="b_seleccionar">
									<a href="javascript: void(0);" onclick="javascript: cambiarEstatusSeleccionExhibiciones('1');"><midas:mensaje clave="midas.accion.seleccionar"/></a>
								</div>
							</div>
						</td>
						<td class="b_deseleccionar" width="20%">
							<div class="alinearBotonALaDerecha">						
								<div id="b_deseleccionar">
									<a href="javascript: void(0);" onclick="javascript: cambiarEstatusSeleccionExhibiciones('0');"><midas:mensaje clave="midas.accion.deseleccionar"/></a>
								</div>
							</div>
						</td>
					</tr>
				</table>									
				<div id="gridboxExhibicionesCoberturasEdosCuentaFacultativos"></div>					
			</div>	
		</logic:equal>		
		<table width="97%">			
			<tr>
				<td class="b_programar_pago">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);" onclick="javascript: closeMostrarRegistrarEgreso();"><midas:mensaje clave="midas.accion.regresar"/></a>
						</div>
						<div id="b_programar_pago">
							<a href="javascript: void(0);" onclick="javascript: registrarEgresoReaseguro();"><midas:mensaje clave="midas.accion.programarpago"/></a>
						</div>
					</div>
				</td>
			</tr>		
		</table>
	</center>
</midas:formulario>