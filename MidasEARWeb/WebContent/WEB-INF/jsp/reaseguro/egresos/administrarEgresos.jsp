<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>

<midas:formulario accion="/reaseguro/egresos/administrarEgresos.do">
<table width="100%">
	<tr>
		<td width="70%">
			<table id="filtros" style="width:100%;overflow:scroll;table-layout:auto;">
		     	<tr>
			     	<td class="titulo" colspan="6">
						<midas:mensaje clave="contratos.egresos.administrar.Egresos"/>	
					</td>
		     	</tr>
		     	<tr>
					<td width="5%">
		     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.ejercicio"/>:
		     		</td>
		     		<td  width="50%">
		     			<%--html:select property="idEjercicio" styleId="idEjercicio" styleClass="cajaTexto" onchange="manejarComboSuscripcionEdoCtaTipoReaseguro()"--%> 
		     			<html:select property="idEjercicio" styleId="idEjercicio" styleClass="cajaTexto">
		     				<html:option value="">Seleccione...</html:option>
		     				<html:optionsCollection property="ejercicioDTOList" label="descripcion" value="anio" />
		     			</html:select>
		     		</td>
		     		<td width="5%">
		     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.tipoReaseguro"/>:
		     		</td>
		     		<td width="40%">
		     			<%--html:select property="tipoReaseguro" styleId="tipoReaseguro" onchange="interacCmbsTipoReaseguroReasegurador(this);manejarComboSuscripcionEdoCtaTipoReaseguro()" size="1" styleClass="cajaTexto"--%>
		     			<html:select property="tipoReaseguro" styleId="tipoReaseguro" onchange="interacCmbsTipoReaseguroReasegurador(this);" size="1" styleClass="cajaTexto">
		     				<html:option value="">Seleccione...</html:option>
		     				<html:optionsCollection property="tipoReaseguroDTOList" label="descripcion" value="tipoReaseguro" />
		     			</html:select>		     					     		
		     		</td>
		     	</tr>
		     	<tr>
		     		<td>
		     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.suscripcion"/>:
		     		</td>
		     		<td>
		     		<%-- 
		     			<html:select property="idSuscripcion" styleClass="cajaTexto"  disabled="true" styleId="idSuscripcion">
		     				<html:option value="">Seleccione...</html:option>
		     			</html:select>
		     		--%>		     			
		     			<html:select property="idSuscripcion" styleId="idSuscripcion" styleClass="cajaTexto" >
		     				<html:option value="">Seleccione...</html:option>
							<html:option value="1"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre1"/></html:option>
							<html:option value="2"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre2"/></html:option>
							<html:option value="3"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre3"/></html:option>
							<html:option value="4"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre4"/></html:option>
						</html:select>
		     		</td>		     		
		     		<td>
		     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.nombreReaseguradorCorredorRetencion"/>:
		     		</td>
		     		<td>
		     			<div id ="comboReasegurador">
		     				<midas:comboCatalogo propiedad="idtcReaseguradorCorredor" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
		     			</div>
		    			<div id="comboRetencion" style="display:none">
		     				<html:select property="idRetencion" styleClass="cajaTexto">
		     					<html:option value=""><midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.combo.retencion"/></html:option>
		     				</html:select>
		     			</div>
		     		</td>
		     	</tr>
		     	<tr>
		     		<td>
		     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.ramo"/>:
		     		</td>
		     		<td>
		     			<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo"
							 onchange="limpiarObjetos('idTcSubRamo'); llenarCombo(this,'idTcSubRamo','/MidasWeb/subRamo.do');" styleClass="cajaTexto"/>
		     		</td>		     		
		     		<td>
		    			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.moneda"/>:
		    		</td>
		     		<td>
		    			<html:select property="idTcMoneda" styleClass="cajaTexto">
		    				<html:option value="">Seleccione...</html:option>
		    				<html:optionsCollection property="monedaDTOList" label="descripcion" value="idTcMoneda" />
		     			</html:select>
		    		</td>		     		
		     	</tr>
		     	<tr>
		    		<td>
		     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.subRamo"/>:
		     		</td>
		     		<td>
		     			<%--midas:subramo propiedad="idTcSubRamo" ramo="idTcRamo" size="1" styleId="idTcSubRamo" styleClass="cajaTexto" onchange="manejarComboSuscripcionEdoCtaTipoReaseguro()"/--%>
		     			<midas:subramo propiedad="idTcSubRamo" ramo="idTcRamo" size="1" styleId="idTcSubRamo" styleClass="cajaTexto" />
		     		</td>
		     		<td colspan="2"></td>
		     	</tr>
			</table>
		   <table width="100%" style="table-layout:auto;" >
				<tr>
				  <td width="60%">
					    <div onclick="mostrarOcultarDivsIngresosEgresos(document.getElementById('divBusquedaPolizas'), document.getElementById('divGridBusquedaPolizas'))" style="cursor: pointer;background-image: url('/MidasWeb/img/bg_tabla.jpg');background-repeat:repeat-x;width:100%;font-size:12px;font-weight:bold;color:white;border-style:dashed;border-width:1px;border-color:lightgreen">
					    	<img id="botonMostrarOcultarDiv" border="0" alt="Mostrar Filtro Polizas" style="margin: 0px 11px 0pt 0pt; cursor: pointer;" src="/MidasWeb/img/add_green.png"/>
					    	<midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.mostrarocultarfiltro"/>
					    </div>
						  <table id="tablaConResultados" width="100%">
							  <tr>
							    <td>
							    	<div id="divBusquedaPolizas" style="display:none">
								    	<table width="100%" style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;">
								    		<tr>
								    			<td width="30%" align="right">
											    	<div style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;">
											    		<midas:mensaje clave="contratos.egresos.administrar.nombreAsegurador"/>
											    	</div>
									    		</td>
									    		<td width="30%">
									    			<input id="busquedaNomAsegurado" style="border:1px solid #A0E0A0; padding-left:3px;" class="cajaTexto" type="text" onkeypress="return stopRKey(event)"/>
									    		</td>
									    		<td width="25%" align="right">
													<midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.numeropoliza" />:
												</td>
												<td width="15%">
													<input id="busquedaNumPoliza"
														style="border: 1px solid #A0E0A0; padding-left: 3px;"
														type="text" onkeypress="return stopRKey(event)"
														onfocus="new Mask('####-########-##', 'string').attach(this)" class="cajaTexto" />
												</td>
											</tr>
											<tr>
												<td colspan="3"></td>
										    	<td>
													<div id="b_regresar" style="width:120px;">
													     <a href="javascript: void(0);" onclick="javascript: $('tipoReaseguro').value=3;mostrarPolizaAdministrarEgresosGrid($('busquedaNomAsegurado').value, $('busquedaNumPoliza').value, $('tipoReaseguro').value)"><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.filtrarPoliza"/></a>
												    </div>
											    </td>
										    </tr>
									    </table>
								     </div>
							    </td>  
							 </tr>
							 <tr>
							    <td>
							    	<div id="divGridBusquedaPolizas" style="width:100%;overflow:auto;margin-left: auto; margin-right: auto;display:none">
							    		<div id="loadingPolizas" style="display:none"></div>
							    		<div id="polizaAdministrarEgresosGrid" style="width:540px;height:125px;"></div>
							    	</div>
							    </td>
							 </tr>
							 <tr>
							    <td>
							      <hr width="100%" style="color:lightgreen;background-color:green;height:2px;" />
								  <table width="100%" id="tablaConResultados">
									  <tr>
									  	<th style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;" align="left" valign="bottom" width="25%">									  		
									  		<input id="seleccionarTodosChk" type="checkbox" onchange="seleccionarTodosEdosCtaEgresos(this.checked);"> Seleccionar todos									  											  	
									  	</th>
									  	<th style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;" align="right" valign="bottom" width="20%">
									  		Saldo total: 
									  	</th>
									  	<td style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:11px;" align="left" valign="bottom" width="30%">
									  		<span id="saldoTecnicoTotal"></span>
									  	</td>
									    <td width="25%"> 									    
									      <div class="alinearBotonALaDerecha">
											<div id="b_regresar" style="width:140px;">
											     <a href="javascript: void(0);" 
											           onclick="javascript:buscarEstadosCuentaAdministrarEgresos();"><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.filtrarEstadosCuenta"/>
											     </a>
											 </div>
										  </div>
									      </td>  
									   </tr>
									   <tr>
									       <td colspan="4">
									       		<div id="loadingEstadosCuenta" style="display:none"></div>
									      		<div style="width:100%;overflow:auto;margin-left:auto;margin-right:auto;">
									       			<div id="detallePolizaAdministrarEgresosGrid" style="width:540px;height:250px;" ></div>
									       		</div>
									       </td>
									  </tr>
								   </table>
								</td>
							</tr>
						  </table>
				    </td>
				    <td width="40%">
				   		<div style="width:100%;overflow:auto;margin-left:auto;margin-right:auto;margin-top:0px">
				    		<div id="gridboxListaEgresos" class="dataGridConfigurationClass" style="width:350px;height:320px;"></div>
				    	</div>
				    </td>
			  	</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<div class="alinearBotonALaDerecha">
				 <div id="b_agregar" style="width: 140px;margin-right:15px">
			         <a href="javascript: void(0);" onclick="validarRelacionEgresoEdosCta();"><midas:mensaje clave="contrato.egresos.accion.estado.menu.administrar.registrarEgreso" /></a>
			      </div>
				  <div id="b_agregar" style="width:160px;margin-right:15px">
					 <a href="javascript: void(0);"  onclick="javascript: mostrarReporteOrdenPagoReaseguro()"><midas:mensaje clave="contrato.egresos.accion.estado.menu.administrar.imprimirOrden"/></a>
				  </div>
				  <div id="b_agregar" style="width:160px;margin-right:15px">
					 <a href="javascript: void(0);"  onclick="javascript: mostrarReporteSoporteOrdenPagoReaseguro()"><midas:mensaje clave="contrato.egresos.accion.imprimirSoporteOrdenPago"/></a>
				  </div>
				  <div id="b_agregar" style="width:120px;margin-right:15px">
					 <a href="javascript: void(0);" onclick="registrarEgresos();"><midas:mensaje clave="contrato.egresos.accion.estado.menu.administrar.cancelar"/></a>
				  </div><!-- mostraGridListaEgresosCancel -->				  
				  
			</div>
		</td>
	</tr>
</table>
</midas:formulario>