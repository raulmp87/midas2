<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div hrefmode="ajax-html" style= "height: 100%; width:95%; overflow:auto;margin-left: auto; margin-right: auto;" id="autorizarEgresosTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="250px" id="facultativoEdoCta" name="Pagos facultativos por estado de cuenta" href="http://void" extraAction="limpiarDivsAutorizarEgresos();sendRequest(null,'/MidasWeb/reaseguro/egresos/autorizaciones/mostrarAutorizaciones.do?tipo=pagoFacultativo','contenido_facultativoEdoCta','inicializaComponentesListarAutorizaciones(0);')"></div>
	<div width="250px" id="exhibicionVencida" name="Pago de exhibiciones vencidas" href="http://void" extraAction="limpiarDivsAutorizarEgresos();sendRequest(null,'/MidasWeb/reaseguro/egresos/autorizaciones/mostrarAutorizaciones.do?tipo=pagosVencidos','contenido_exhibicionVencida','inicializaComponentesListarAutorizaciones(1);')"></div>
	<div width="250px" id="exhibicionConPagoVencido" name="Pago de exhibiciones sin pago de asegurado" href="http://void" extraAction="limpiarDivsAutorizarEgresos();sendRequest(null,'/MidasWeb/reaseguro/egresos/autorizaciones/mostrarAutorizaciones.do?tipo=pagoSinIngresoAsegurado','contenido_exhibicionConPagoVencido','inicializaComponentesListarAutorizaciones(2);')"></div>
</div>