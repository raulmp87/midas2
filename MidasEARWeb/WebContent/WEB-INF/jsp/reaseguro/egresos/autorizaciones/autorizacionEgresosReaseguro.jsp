<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<midas:formulario accion="/reaseguro/egresos/autorizaciones/mostrarAutorizaciones">
	<table>
		<tr>
		<td><html:hidden property="tipoConsultaAutorizaciones" styleId="tipoConsulta"/></td>
		<td><html:hidden property="csvAutorizacionesPagosPendientes" styleId="csvContenidoDataGrid"/></td>
		</tr>
	</table>
	<br>
	<center>
     	<div id="cargandoAutorizaciones" style="display:none"></div>
     	<div id="autorizacionPagosGrid" width="95%" height="350px" style="background-color:white;overflow:hidden;margin-left:auto; margin-right:auto;" align="center"></div>
     </center>
		<table width="98%">
			<tr>
				<td width="70%"></td>
				<td>
					<div class="alinearBotonALaDerecha">
					<div id="b_seleccionar">
				    	<a href="javascript: void(0);" onclick="javascript: autorizarPagosPendientes(true)">
				    		<midas:mensaje clave="midas.accion.autorizar"/>
				    	</a>
				    </div>
				    </div>
				</td>
				<td>
					<div class="alinearBotonALaDerecha">
				 	<div id="b_deseleccionar">
				 		<a href="javascript: void(0);" onclick="javascript: autorizarPagosPendientes(false)">
				 			<midas:mensaje clave="midas.accion.rechazar"/>
				 		</a>
				    </div>
				    </div>
				</td>
			</tr>
		</table>	
</midas:formulario>