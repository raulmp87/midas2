<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/reaseguro/ingresos/mostrarIngreso">
<html:hidden property="idIngreso" styleId="idIngreso"/>
	<center>
	    <table id="filtros" style="width:97%">
	   		<tr>
		 	 	<td class="titulo" colspan="4">
		 	 		<midas:mensaje clave="reaseguro.ingreso.administrar.ingreso.menu.administrar.relacionarIngreso" />
		 	 	</td>
		 	</tr>
		 	<tr>
				<td width="10%" align="right">
					<b><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.concepto"/>:</b>
				</td>
				<td width="40%">
					<html:select property="idConcepto" styleClass="cajaTexto" name="administrarIngresosForm" disabled="false" styleId="idConcepto" >
	     				<html:option value="">Seleccione...</html:option>
	     				<html:optionsCollection property="conceptoIngresoReaseguroDTOList" label="descripcion" value="idConcepto" />
	     			</html:select>
				</td>		
				<td colspan="2" width="50%"></td>
			</tr>
			<tr>
				<td align="right">
					<logic:equal property="tipoCambioNecesario" name="administrarIngresosForm" value="true">
						<b><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.tipoDeCambio" />:</b>
					</logic:equal>
				</td>
				<td>
					<logic:equal property="tipoCambioNecesario" name="administrarIngresosForm" value="true">
						<midas:texto propiedadFormulario="tipoCambio" id="tipoCambio" nombreFormulario="administrarIngresosForm" 
						deshabilitado="false" caracteres="10" onblur="this.value=formatCurrency4Decs(this.value)" onkeypress="return  stopRKey(event)"/> 
					</logic:equal>
				</td>		
				<td>
					<logic:equal property="tipoCambioNecesario" name="administrarIngresosForm" value="true">
						<b><i><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.pesosxdolar"/></i></b>
					</logic:equal>
				</td>
				<td></td>
			</tr>
			<tr>
				<td width="10%" align="right">
					<b>Referencia del ingreso*:</b>
				</td>
				<td colspan="3">
					<html:select property="idReferenciaExterna" styleId="idReferenciaExterna" styleClass="cajaTexto" >
						<html:option value="">Seleccione ...</html:option>
						<html:optionsCollection property="listaReferenciasIngreso" label="descripcionReferenciaExterna" value="idReferenciaExterna" ></html:optionsCollection>
					</html:select>
				</td>
			</tr>
		</table>
		<table style="width:97%" style="margin-left: auto; margin-right: auto;">
			<tr>
			   <td colspan="3">
			   	  <div width="100%" style="overflow:auto;margin-left: auto; margin-right: auto;">
			      	<div id="gridboxListaEstadosCuentaRelacionarIngreso" style="width:883px;height:250px;"></div>
			      </div>
			   </td>
			</tr>
			<tr>
				<td class="guardar">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);" onclick="javascript: closeMostrarRegistrarIngreso();"><midas:mensaje clave="midas.accion.regresar"/></a>
						</div>
						<div id="b_guardar">
							<a href="javascript: void(0);" onclick="javascript: relacionarIngresoReaseguro('<midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.relacionaringreso.comboConceptoNoSeleccionado"/>');"><midas:mensaje clave="midas.accion.agregar"/></a>
						</div>
					</div>
				</td> 
			</tr>		
		</table>
	</center>
</midas:formulario>