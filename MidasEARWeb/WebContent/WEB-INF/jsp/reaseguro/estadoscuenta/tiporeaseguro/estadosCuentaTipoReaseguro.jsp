<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
  
<midas:formulario accion="/reaseguro/estadoscuenta/listarEstadoCuentaTipoReaseguro">
	<br clear="all" />
     <table id="filtros" style="width:97%;overflow:auto;table-layout:auto;margin-left: auto; margin-right: auto;">
     	<tr>
     		<td width="5%">
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.ejercicio"/>:
     		</td>
     		<td  width="20%">
     			<html:select property="idEjercicio" styleId="idEjercicio" styleClass="cajaTexto"> 
     				<html:option value="">Seleccione...</html:option>
     				<html:optionsCollection property="ejercicioDTOList" label="descripcion" value="anio" />
     			</html:select>
     		</td>
     		<td width="10%">
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.nombreReaseguradorCorredorRetencion"/>:
     		</td>
     		<td width="30%">
     			<div id ="comboReasegurador">
     				<midas:comboCatalogo propiedad="idtcReaseguradorCorredor" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
     			</div>
     			<div id="comboRetencion" style="display:none">
     				<html:select property="idRetencion" styleClass="cajaTexto">
     					<html:option value=""><midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.combo.retencion"/></html:option>
     				</html:select>
     			</div>
     		</td>
     		<td width="5%">
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.ramo"/>:
     		</td>
     		<td width="40%">
     			<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo"
					 onchange="limpiarObjetos('idTcSubRamo'); llenarCombo(this,'idTcSubRamo','/MidasWeb/subRamo.do');" styleClass="cajaTexto"/>
     		</td>
     	</tr>
     	<tr>
     		<td>
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.suscripcion"/>:
     		</td>
     		<td>
<%--     			<html:select property="idSuscripcion" styleClass="cajaTexto"  disabled="true" styleId="idSuscripcion">--%>
<%--     				<html:option value="">Seleccione...</html:option>--%>
<%--     			</html:select>--%>
     			<html:select property="idSuscripcion" styleId="idSuscripcion" styleClass="cajaTexto" >
					<html:option value="1"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre1"/></html:option>
					<html:option value="2"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre2"/></html:option>
					<html:option value="3"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre3"/></html:option>
					<html:option value="4"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre4"/></html:option>
				</html:select>
     		</td>
     		<td>
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.tipoReaseguro"/>:
     		</td>
     		<td>
<%--     			<html:select property="tipoReaseguro" styleId="tipoReaseguro" onchange="interacCmbsTipoReaseguroReasegurador(this);manejarComboSuscripcionEdoCtaTipoReaseguro()" size="1" styleClass="cajaTexto">--%>
     			<html:select property="tipoReaseguro" styleId="tipoReaseguro" onchange="interacCmbsTipoReaseguroReasegurador(this);" size="1" styleClass="cajaTexto">
     				<html:option value="">Todos...</html:option>
     				<html:optionsCollection property="tipoReaseguroDTOList" label="descripcion" value="tipoReaseguro" />
     			</html:select>
     		</td>
     		<td>
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.subRamo"/>:
     		</td>
     		<td>
<%--     			<midas:subramo propiedad="idTcSubRamo" ramo="idTcRamo" size="1" styleId="idTcSubRamo" styleClass="cajaTexto" onchange="manejarComboSuscripcionEdoCtaTipoReaseguro()"/>--%>
     			<midas:subramo propiedad="idTcSubRamo" ramo="idTcRamo" size="1" styleId="idTcSubRamo" styleClass="cajaTexto" />
     		</td>
     	</tr>
     	<tr>
     		<td>
     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.moneda"/>:
     		</td>
     		<td>
     			<html:select property="idTcMoneda" styleClass="cajaTexto" styleId="idTcMoneda" >
     				<html:option value="">Seleccione...</html:option>
     				<html:optionsCollection property="monedaDTOList" label="descripcion" value="idTcMoneda" />
     			</html:select>
     		</td>
     		<td colspan="3"></td>
     		<td>
     			<div class="alinearBotonALaDerecha" style="margin-right:24px;">
	     			<div id="b_buscar">
	     				<a href="javascript: void(0);" onclick="buscarEstadosCuentaTipoReaseguro('<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.eligeUnEjercicio"/>')">
	     					<midas:mensaje clave="midas.contacto.accion.filtrar"/>
	     				</a>
	     			</div>
     			</div>
     		</td>
     	</tr>
    </table>
   <div style="height:10px;"></div>
   <div id="loadingEstadosCuenta" style="display:none"></div>
   	 <center><div id="estadoCuentaTipoReaseguroGrid" width="97%" height="250px" style="background-color:white;overflow:hidden;margin-left:auto;margin-right:auto;"></div></center>
   <div style="height:10px;"></div>
    <div class="alinearBotonALaDerecha">
     	<table>
     		<tr>
     			<td>
<%--	     			<table id="filtros">--%>
<%--				    	<tr>--%>
<%--				    		<td>--%>
<%--				  				<html:checkbox property="mostrarEstadoCuentaConEjerciciosAnteriores" styleId="checkEjerciciosAnteriores"><midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.incluirEjerciciosAnteriores"/></html:checkbox>--%>
<%--				  			</td>--%>
<%--				  		</tr>--%>
<%--				    </table>--%>
     			</td>
	     		<td>
				     <div id="b_consultar">
				     	<a href="javascript: void(0);" onclick="mostrarDetalleEstadoCuentaTipoReaseguro('<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.eligeUnEstadoCuenta"/>')">
				     		<midas:mensaje clave="midas.accion.mostrar"/>
				     	</a>
				     </div>
				</td>
				<td>
				     <div id="b_imprimir" style="width:150px">
				     	<a href="javascript: void(0);" onclick="mostrarRptEstadoCuentaMasivo()">
				     		<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.boton.impresionMasiva"/>
				     	</a>
				     </div>
			    </td>
		     </tr>
	     </table>
     </div>
</midas:formulario> 
