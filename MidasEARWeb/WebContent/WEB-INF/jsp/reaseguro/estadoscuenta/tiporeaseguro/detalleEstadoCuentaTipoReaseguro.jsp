<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<midas:formulario accion="/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarDetalle">
	<html:hidden property="idEstadoCuenta" styleId="idEstadoCuenta" />
   	<html:hidden property="suscripcion" styleId="suscripcion" />
   	<html:hidden property="fechaInicial" styleId="fechaInicial" />
 	<html:hidden property="fechaFinal" styleId="fechaFinal"/>
 	<html:hidden property="formaPago" styleId="formaPago"/>
 	<html:checkbox property="mostrarEstadoCuentaConEjerciciosAnteriores" styleId="checkEjerciciosAnteriores" style="display:none;" ></html:checkbox>
   	 
 	  <table width="97%" style="margin-left: auto; margin-right: auto;"> 
     	<tr>
	     	<td class="titulo" colspan="4">
				<midas:mensaje clave="reaseguro.estadodecuenta.titulo"/>/
				<midas:mensaje clave="reaseguro.estadodecuenta.titulo.en"/>
			</td>
     	</tr>
     </table>
     <center>
	     <table width="97%" id="filtros" style="margin-left: auto; margin-right: auto;">
	     	<tr>
	     		<th colspan="3">
	     			<midas:texto propiedadFormulario="nombreReasegurador" soloLectura="true"/>
	     		</th>
	     		<th>
	     			<midas:mensaje clave="reaseguro.estadodecuenta.suscripcion"/>/
					<midas:mensaje clave="reaseguro.estadodecuenta.suscripcion.en"/>
	     		</th>
	     		<td><midas:texto propiedadFormulario="suscripcion" soloLectura="true"/></td>
	     	</tr>
	     	<tr>
	     		<th colspan="3">
	     			<midas:texto propiedadFormulario="direccionReasegurador" soloLectura="true"/>
	     		</th>
	     		<th>
	     			<midas:mensaje clave="catalogo.reaseguradorcorredor.rfc"/>					
	     		</th>
	     		<th>
	     			<midas:texto propiedadFormulario="rfc" soloLectura="true"/>
	     		</th>
	     	</tr>
	     	<tr>
	     		<th>
	     			<midas:mensaje clave="reaseguro.estadodecuenta.contrato"/>/
	     			<midas:mensaje clave="reaseguro.estadodecuenta.contrato.en"/>
	     		</th>
	     		<td colspan="2"><midas:texto propiedadFormulario="tipoReaseguro" soloLectura="true"/></td> <!-- Contrato = Tipo de Reaseguro  -->
	     		<th>
	     			<midas:mensaje clave="reaseguro.estadodecuenta.ramo"/>/
					<midas:mensaje clave="reaseguro.estadodecuenta.ramo.en"/>
	     		</th>
	     		<td>
	     			<midas:texto propiedadFormulario="descripcionSubRamo" soloLectura="true"/>
	     		</td>
	     	</tr>
	     	<tr>
	     		<th>
	     			<midas:mensaje clave="reaseguro.estadodecuenta.periodo"/>/
					<midas:mensaje clave="reaseguro.estadodecuenta.periodo.en"/>
	     		</th>
	     		<td>
		     		<table>
	     				<tr>
	     					<td>
			     				<midas:texto propiedadFormulario="fechaInicial" soloLectura="true"/>
			     			</td>
			     			<td>
			     				<midas:texto propiedadFormulario="fechaFinal" soloLectura="true"/>
			     			</td>
		     			</tr>
		     		</table>
	     		</td>
	     		<td></td>
	     		<th>
	     			<midas:mensaje clave="reaseguro.estadodecuenta.moneda"/>/
					<midas:mensaje clave="reaseguro.estadodecuenta.moneda.en"/>
	     		</th>
	     		<td>
	     			<midas:texto propiedadFormulario="descripcionMoneda" soloLectura="true"/>
				</td>
	     	</tr>
	     </table>
     </center>
     <br clear="all" />
     <center>
	     <div style="width:97%;overflow:auto;background-color:white;margin-left: auto;margin-right: auto;">
	    	<div id="detalleAcumuladorEstadoCuentaGrid" width="862px" height="285px"></div>
	    </div>
    </center>
    <midas:mensaje clave="reaseguro.estadodecuenta.observaciones"/>
	<html:textarea styleId="observaciones" property="observaciones" rows="3" onkeypress="return limiteMaximoCaracteres(this.value, event, 300)" style="width: 97%"></html:textarea>
	<div class="alinearBotonALaDerecha">	
		<table>
			<tr>
				<td>
					<div id="b_regresar">
			     		<a href="javascript: void(0);"
							onclick="javascript: cargarEstadosCuentaAdmonMovs('tipoReaseguro');">
			 	 		<midas:mensaje clave="midas.accion.regresar"/>
			 	 		</a>	
			 	 	</div>
				</td>
				<td>
					<div id="b_agregar" style="width:180px;">
			     		<a href="javascript: void(0);"
							onclick="javascript: guardarObservacionesEstadoCuentaTipoReaseguro();">
			 	 		<midas:mensaje clave="reaseguro.estadodecuenta.guardarObservaciones"/>
			 	 		</a>	
	 	 			</div>
				</td>
				<td>
					<div id="b_reportePDF" style="width:100px">
			      		<a href="#"	onclick="javascript:abrirReporteEstadoCuenta(document.getElementById('idEstadoCuenta').value,document.getElementById('checkEjerciciosAnteriores').checked,'application/pdf')"><midas:mensaje clave="reaseguro.estadodecuenta.imprimir"/></a>
			 	 	</div>
				</td>
				<td>
					<div id="b_reporteXLS" style="width:100px">
			      		<a href="#"	onclick="javascript:abrirReporteEstadoCuenta(document.getElementById('idEstadoCuenta').value,document.getElementById('checkEjerciciosAnteriores').checked,'application/vnd.ms-excel')"><midas:mensaje clave="reaseguro.estadodecuenta.imprimir"/></a>
			 	 	</div>
				</td>
			</tr>
		</table>	
	</div>
	<br />
</midas:formulario> 