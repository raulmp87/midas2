<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
  
<midas:formulario accion="/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle">
<%--	<html:hidden property="idEstadoCuenta" styleId="idEstadoCuenta" />--%>
	<html:hidden property="busquedaNomAsegurado"/>
	<html:hidden property="clavePolizaSubramo" styleId="clavePolizaSubramo"/>
	<html:hidden property="idPoliza" styleId="idPoliza"/>
	<html:hidden property="hasta" styleId="hasta"/>
	<html:hidden property="separarConceptosPorAcumulador" styleId="separarPorReasegurador"/>
<%--	<html:hidden property="suscripcion" styleId="suscripcion" />--%>
<%--   	<html:hidden property="fechaInicial" styleId="fechaInicial" />--%>
<%-- 	<html:hidden property="fechaFinal" styleId="fechaFinal"/>--%>
<%-- 	<html:hidden property="formaPago" styleId="formaPago"/>--%>
 	
      <table width="97%" style="margin-left: auto;margin-right: auto;"> 
     	<tr>
	     	<td class="titulo" colspan="4">
				<midas:mensaje clave="reaseguro.estadodecuenta.porpoliza.titulo"/>
<%--				<midas:mensaje clave="reaseguro.estadodecuenta.titulo.en"/>--%>
			</td>
     	</tr>
     </table>
     <table width="97%" id="filtros" style="margin-left: auto;margin-right: auto;">
     	<tr>
     		<th align="right"><midas:mensaje clave="reaseguro.estadodecuenta.poliza"/>:</th>
     		<td><midas:texto propiedadFormulario="poliza" soloLectura="true"/></td>
<%--     		<th>--%>
<%--     			<midas:mensaje clave="reaseguro.estadodecuenta.suscripcion"/>/--%>
<%--				<midas:mensaje clave="reaseguro.estadodecuenta.suscripcion.en"/>:--%>
<%--     		</th>--%>
<%--     		<td><midas:texto propiedadFormulario="suscripcion" soloLectura="true"/></td>--%>
     	</tr>
     	<tr>
     		<th><midas:mensaje clave="reaseguro.estadodecuenta.endoso"/>:</th>
     		<td><midas:texto propiedadFormulario="endoso" soloLectura="true"/></td>
     	</tr>
     	<tr>
     		<th><midas:mensaje clave="reaseguro.estadodecuenta.nomAsegurado"/>:</th>
     		<td colspan="2"><midas:texto propiedadFormulario="nomAsegurado" soloLectura="true"/></td>
<%--     		<th><midas:mensaje clave="reaseguro.estadodecuenta.notaCobertura"/>:</th>--%>
<%--     		<td><midas:texto propiedadFormulario="notaCobertura" soloLectura="true"/></td>--%>
     	</tr>
<logic:notEmpty property="descripcionSubRamo" name="estadoCuentaContratoFacultativoForm" >
     	<tr>
     		<th>
     			<midas:mensaje clave="reaseguro.estadodecuenta.ramo"/>/<midas:mensaje clave="reaseguro.estadodecuenta.ramo.en"/>:
     		</th>
     		<td colspan="2">
     			<midas:texto propiedadFormulario="descripcionSubRamo" soloLectura="true"/>
     		</td>
     	</tr>
</logic:notEmpty>
     	<tr>
     		<th>
     			<midas:mensaje clave="reaseguro.estadodecuenta.moneda"/>/
				<midas:mensaje clave="reaseguro.estadodecuenta.moneda.en"/>:
     		</th>
     		<td colspan="2">
     			<midas:texto propiedadFormulario="descripcionMoneda" soloLectura="true"/>
			</td>
     	</tr>
<logic:notEmpty property="hasta" name="estadoCuentaContratoFacultativoForm" >
     	<tr>
			<th><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.corte" /> :</th>
			<td><midas:texto propiedadFormulario="hasta" soloLectura="true"/></td>
		</tr>
</logic:notEmpty>
     	<tr>
     		<th>
<%--     			<midas:mensaje clave="reaseguro.estadodecuenta.contrato"/>/--%>
<%--     			<midas:mensaje clave="reaseguro.estadodecuenta.contrato.en"/>:--%>
     		</th>
<%--     		<td colspan="2"><midas:texto propiedadFormulario="contrato" soloLectura="true"/></td>--%>
     		<th>
<%--     			<midas:mensaje clave="reaseguro.estadodecuenta.periodo"/>/--%>
<%--				<midas:mensaje clave="reaseguro.estadodecuenta.periodo.en"/>:--%>
     		</th>
     		<td>
<%--     			<table>--%>
<%--     				<tr>--%>
<%--     					<td>--%>
<%--		     				<midas:texto propiedadFormulario="fechaInicial" soloLectura="true"/>--%>
<%--		     			</td>--%>
<%--		     			<td>--%>
<%--		     				<midas:texto propiedadFormulario="fechaFinal" soloLectura="true"/>--%>
<%--		     			</td>--%>
<%--	     			</tr>--%>
<%--	     		</table>--%>
     		</td>
     	</tr>
     </table>
     <br clear="all" />
     <center>
	     <div id="detalleAcumuladorEstadoCuentaGrid" width="862px" height="285px" style="margin-left: auto;margin-right: auto;"></div>
	    <br clear="all" />
<%--	    <b><midas:mensaje clave="reaseguro.estadodecuenta.pagos"/></b>--%>
<%--	    <div id="detallePagosEstadoCuentaGrid" width="602px" height="125px" style="margin-left: auto;margin-right: auto;"></div>--%>
    </center>
    <br clear="all" />
<%--    <b><midas:mensaje clave="reaseguro.estadodecuenta.observaciones"/></b>--%>
<%--	<html:textarea styleId="observaciones" property="observaciones" rows="3" onkeypress="return limiteMaximoCaracteres(this.value, event, 300)" style="width: 97%"></html:textarea>--%>
     
	<div class="alinearBotonALaDerecha">	
		<table>
			<tr>
				<td>
					<div id="b_regresar">
			     		<a href="javascript: void(0);"
							onclick="javascript: cargarEstadosCuentaAdmonMovs('contratosFacultativosNuevo',null);">
			 	 		<midas:mensaje clave="midas.accion.regresar"/>
			 	 		</a>	
			 	 	</div>
				</td>
				<td>
<%--					<div id="b_modificar" style="width:180px;">--%>
<%--			     		<a href="javascript: void(0);"--%>
<%--							onclick="javascript: guardarObservacionesEstadoDeCuenta();">--%>
<%--			 	 		<midas:mensaje clave="reaseguro.estadodecuenta.guardarObservaciones"/>--%>
<%--			 	 		</a>	--%>
<%--	 	 			</div>--%>
				</td>
				<td>
					<div id="b_imprimir">
						<a href="#"
							onclick="javascript:imprimirEstadoCuentaFacultativo()"><midas:mensaje
								clave="reaseguro.estadodecuenta.imprimir" />
						</a>
					</div>
				</td>
				<td>
					<div id="b_imprimir" style="width:130px">
						<a href="#"
							onclick="javascript:imprimirReporteSoporteEstadoCuentaFacultativo()">
							<midas:mensaje clave="reaseguro.estadodecuenta.imprimir.reporteSoporteEstadoCuenta" />
						</a>
					</div>
				</td>
			</tr>
		</table>	
	</div>
</midas:formulario> 
