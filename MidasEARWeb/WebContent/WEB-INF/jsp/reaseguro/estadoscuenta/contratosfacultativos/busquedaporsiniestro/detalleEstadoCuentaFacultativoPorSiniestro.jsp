<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
  
<midas:formulario accion="/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle">
<%--	<html:hidden styleId="idReporteSiniestro" property="caratulaSiniestroForm.idReporteSiniestro"/>--%>
	<html:hidden styleId="idReporteSiniestro" property="idReporteSiniestro"/>
	<html:hidden styleId="claveSiniestroSubRamo" property="claveSiniestroSubRamo"/>
	<html:hidden styleId="hasta" property="hasta"/>
	<html:hidden styleId="separarPorReasegurador" property="separarConceptosPorAcumulador"/>
	<table width="97%" style="margin-left: auto;margin-right: auto;"> 
		<tr>
			<td class="titulo"><midas:mensaje clave="reaseguro.estadodecuenta.porsiniestro.titulo"/></td>
		</tr>
	</table>
	<table width="97%" id="filtros" style="margin-left: auto;margin-right: auto;">
		<tr>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.numeroPoliza" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.numeroPoliza" soloLectura="true"/></td>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.endoso" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.endosoPoliza" soloLectura="true"/></td>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.moneda" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.tipoMonedaPoliza" soloLectura="true"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.fechaEmision" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.fechaEmisionPoliza" soloLectura="true"/></td>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.inicioVigencia" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.fechaInicioVigenciaPoliza" soloLectura="true"/></td>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.finVigencia" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.fechaTerminacionVigenciaPoliza" soloLectura="true"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.asegurado" /> :</td>
			<td colspan="3"><midas:texto propiedadFormulario="caratulaSiniestroForm.nombreAseguradoPoliza" soloLectura="true"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.numeroReporte" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.numeroReporte" soloLectura="true"/></td>
			<td><midas:mensaje clave="siniestro.finanzas.caratula.fechaHoraReporte" /> :</td>
			<td><midas:texto propiedadFormulario="caratulaSiniestroForm.fechaHoraReporte" soloLectura="true"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porsiniestro.subRamosIncluidos"/> :</td>
			<td colspan="3"><midas:texto propiedadFormulario="busquedaNomAsegurado" soloLectura="true"/></td>
		</tr>
<logic:notEmpty property="hasta" name="estadoCuentaContratoFacultativoForm" >
     	<tr>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.corte" /> :</td>
			<td><midas:texto propiedadFormulario="hasta" soloLectura="true"/></td>
		</tr>
</logic:notEmpty>
	</table>
	<br clear="all" />
     <center>
	     <div id="detalleAcumuladorEstadoCuentaGrid" width="862px" height="285px" style="margin-left: auto;margin-right: auto;"></div>
	    <br clear="all" />
    </center>
    <br clear="all" />
	<div class="alinearBotonALaDerecha">	
		<table>
			<tr>
				<td>
					<div id="b_regresar">
			     		<a href="javascript: void(0);"
							onclick="javascript: cargarEstadosCuentaAdmonMovs('contratosFacultativosNuevo',null);">
			 	 		<midas:mensaje clave="midas.accion.regresar"/>
			 	 		</a>	
			 	 	</div>
				</td>
				<td>
					<div id="b_imprimir">
						<a href="#"
							onclick="javascript:imprimirEstadoCuentaFacultativo()"><midas:mensaje
								clave="reaseguro.estadodecuenta.imprimir" />
						</a>
					</div>
				</td>
				<td>
					<div id="b_imprimir" style="width:130px">
						<a href="#"
							onclick="javascript:imprimirReporteSoporteEstadoCuentaFacultativo()">
							<midas:mensaje clave="reaseguro.estadodecuenta.imprimir.reporteSoporteEstadoCuenta" />
						</a>
					</div>
				</td>
			</tr>
		</table>
	</div>
</midas:formulario>
