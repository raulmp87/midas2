<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
  
<midas:formulario accion="/reaseguro/estadoscuenta/listarEstadoCuentaContratoFacultativo">
	<br clear="all" />
	<center>
     <table width="95%" id="filtros" height="50px">
     	<tr>
     		<td width="20%" align="right"><b><midas:mensaje clave="reaseguro.estadodecuenta.nomAsegurado"/>:</b></td>
     		<td width="25%"  align="left">
     			<midas:texto propiedadFormulario="busquedaNomAsegurado" id="busquedaNomAsegurado" onkeypress="return stopRKey(event)"/>
     		</td>
     		<td width="15%" align="right">
				<b><midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.numeropoliza" />:</b>
			</td>
			<td width="20%">
				<input id="busquedaNumPoliza" style="border: 1px solid #A0E0A0; padding-left: 3px;"
					type="text" onkeypress="return stopRKey(event)" onfocus="new Mask('####-########-##', 'string').attach(this)" class="cajaTexto" />
			</td>
     	</tr>
     	<tr>
     		<td width="15%" align="right">
				<b><midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.numerosiniestro" />:</b>
			</td>
			<td width="20%">
				<input id="busquedaNumSiniestro" style="border: 1px solid #A0E0A0; padding-left: 3px;" type="text" onkeypress="return soloNumeros(this, event, false)" class="cajaTexto" />
			</td>
			<td width="15%" align="right">
				<b><midas:mensaje clave="siniestro.cabina.reportesiniestro.listarfechaReporte" />:</b>
			</td>
			<td width="20%">
				<input id="busquedaFechaSiniestro" style="border: 1px solid #A0E0A0; padding-left: 3px;" type="text" onkeypress="return soloNumeros(this, event, false)" class="cajaTexto" />
			</td>
     		<td width="20%" align="left">
     			<div id="b_buscar"> <a href="javascript: void(0);" onclick="javascript:mostrarGridSiniestro();/*mostrarEstadoCuentaFacultativoPorSiniestro();*/">
	 	 				<midas:mensaje clave="midas.accion.filtrar"/></a></div>
 	 		</td>
     	</tr>
     </table>
     </center>
     <br clear="all"/>
     <center>
     	<div id="loadingSiniestros" style="display:none"></div>
     	<div id="siniestroEstadoCuentaGrid" width="732px" height="110px" style="background-color:white;overflow:hidden;margin-left:auto; margin-right:auto;" align="center"></div>
     </center>
     <center>
     <br clear="all"/>
     <table width="95%" id="filtros" height="50px">
	     <tr>
	     	<td width="35%">
	     		<b><html:checkbox property="separarConceptosPorAcumulador" styleId="checkSepararConceptosPorAcumulador">
	     			<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.separarporreasegurador"/></html:checkbox></b>
	     	</td>
	     	<td width="15%">
		     	<b><midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.hasta"/>(<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.mesanio"/>)
			     	<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioHasta();/*parent.mostrarCalendariofechaHasta();*/">
					<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>:</b>
	     	</td>
			<td width="15%"><midas:texto propiedadFormulario="hasta" id="fechaHasta" onblur="esFechaValidaFormatoMesAnio(this);" /></td>
			<td width="15%">
		     	<div id="b_consultar">
			    	<a href="javascript: void(0);" onclick="mostrarDetalleEstadoCuentaFacultativoPorSiniestro('<midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.eligeUnSiniestro"/>')">
			    		<midas:mensaje clave="midas.accion.mostrar"/></a>
			    </div>
	     	</td>
	     	<td width="20%">
		     	<div id="b_buscar" style="width:130px">
		     		<a href="javascript: "
						onclick="javascript: mostrarSubRamosAfectadosGrid('<midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.eligeUnSiniestro" />');"><midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.porsiniestro.buscarSubRamos"/></a>
				</div>
	     	</td>
	     </tr>
     </table>
     </center>
     <div style="height:10px"></div>
     <center>
     	<div id="loadingEstadosCuentaFacultativoSiniestro" style="display:none"></div>
     	<div id="siniestroEstadoCuenta" width="95%" height="145px"  align="center"></div>
     </center>
     <div style="height:10px"></div>
     <div class="alinearBotonALaDerecha">	
		<table>
			<tr>
				<td>
					<div id="b_consultar">
				    	<a href="javascript: void(0);" onclick="mostrarDetalleEstadoCuentaFacultativoPorSiniestroSubRamo('<midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.eligeUnSubRamo"/>')">
				    		<midas:mensaje clave="midas.accion.mostrar"/>
				    	</a>
				    </div>
				</td>
				<td>
<%--				 	<div id="b_imprimir" style="width:150px">--%>
<%--				 		<a href="javascript: void(0);" onclick="javascript: mostrarRptEstadoCuentaFacultativoMasivo()">--%>
<%--				 			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.boton.impresionMasiva"/>--%>
<%--				 		</a>--%>
<%--				    </div>--%>
				</td>
			</tr>
		</table>	
	</div>
</midas:formulario> 
