<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
  
<midas:formulario accion="/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle">
	<html:hidden styleId="idReporteSiniestro" property="idReporteSiniestro"/>
	<html:hidden styleId="idReasegurador" property="reaseguradorForm.idTcReaseguradorCorredor"/>
	<html:hidden styleId="idMoneda" property="idTcMoneda"/>
	<html:hidden styleId="idSubRamo" property="idTcSubRamo"/>
	<html:hidden styleId="hasta" property="hasta"/>
	<html:hidden styleId="tipoConsulta" property="tipoConsulta"/>
	<html:hidden styleId="idPoliza" property="idPoliza"/>
	
	<table width="97%" style="margin-left: auto;margin-right: auto;"> 
		<tr>
			<td class="titulo"><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.titulo"/></td>
		</tr>
	</table>
	<table width="97%" id="filtros" style="margin-left: auto;margin-right: auto;">
		<tr>
			<td><midas:mensaje clave="reaseguro.estadisticafiscal.nombrereaseguradorcorredor" /> :</td>
			<td><midas:texto propiedadFormulario="reaseguradorForm.nombre" soloLectura="true"/></td>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.subRamo"/></td>
			<td><midas:texto propiedadFormulario="descripcionSubRamo" soloLectura="true"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.direccion" /> :</td>
			<td><midas:texto propiedadFormulario="reaseguradorForm.ubicacion" soloLectura="true"/></td>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.moneda" /> :</td>
			<td><midas:texto propiedadFormulario="descripcionMoneda" soloLectura="true"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="reaseguro.estadodecuenta.porreasegurador.corte" /> :</td>
			<td><midas:texto propiedadFormulario="corteAl" soloLectura="true"/></td>
		</tr>
		<logic:notEqual value="TODAS" property="numeroReporteSiniestro" name="estadoCuentaContratoFacultativoForm" >
			<tr>
				<td><midas:mensaje clave="siniestro.finanzas.caratula.numeroReporte" /> :</td>
				<td><midas:texto propiedadFormulario="numeroReporteSiniestro" soloLectura="true"/></td>
			</tr>
		</logic:notEqual>
		<logic:notEqual value="TODAS" property="numeroPoliza" name="estadoCuentaContratoFacultativoForm" >
			<tr>
				<td><midas:mensaje clave="reaseguro.estadodecuenta.poliza" /> :</td>
				<td><midas:texto propiedadFormulario="numeroPoliza" soloLectura="true"/></td>
			</tr>
		</logic:notEqual>
	</table>
	<br clear="all" />
     <center>
	     <div id="detalleAcumuladorEstadoCuentaGrid" width="862px" height="285px" style="margin-left: auto;margin-right: auto;"></div>
	    <br clear="all" />
    </center>
    <br clear="all" />
	<div class="alinearBotonALaDerecha">	
		<table>
			<tr>
				<td>
					<div id="b_regresar">
			     		<a href="javascript: void(0);"
							onclick="javascript: cargarEstadosCuentaAdmonMovs('contratosFacultativosNuevo',null/*'porReasegurador'*/);">
			 	 		<midas:mensaje clave="midas.accion.regresar"/>
			 	 		</a>	
			 	 	</div>
				</td>
				<td>
					<div id="b_imprimir">
						<a href="#"
							onclick="javascript:imprimirEstadoCuentaFacultativo()"><midas:mensaje
								clave="reaseguro.estadodecuenta.imprimir" />
						</a>
					</div>
				</td>
<logic:notEqual value="saldoTecnico" property="tipoConsulta" name="estadoCuentaContratoFacultativoForm"  >
				<td>
					<div id="b_imprimir" style="width:130px">
						<a href="#"
							onclick="javascript:imprimirReporteSoporteEstadoCuentaFacultativo()">
							<midas:mensaje clave="reaseguro.estadodecuenta.imprimir.reporteSoporteEstadoCuenta" />
						</a>
					</div>
				</td>
</logic:notEqual>
<logic:equal value="saldoTecnico" property="tipoConsulta" name="estadoCuentaContratoFacultativoForm">
				<td>
					<div id="b_imprimir" style="width:130px">
						<a href="#"
							onclick="javascript:imprimirReporteSoporteEstadoCuentaFacultativo('saldoTotalPolizas')">
							<midas:mensaje clave="reaseguro.estadodecuenta.imprimir.reporteSoporteEstadoCuentaCxP" />
						</a>
					</div>
				</td>
				<td>
					<div id="b_imprimir" style="width:130px">
						<a href="#"
							onclick="javascript:imprimirReporteSoporteEstadoCuentaFacultativo('saldoTotalSiniestros')">
							<midas:mensaje clave="reaseguro.estadodecuenta.imprimir.reporteSoporteEstadoCuentaCxC" />
						</a>
					</div>
				</td>
</logic:equal>
			</tr>
		</table>
	</div>
</midas:formulario>