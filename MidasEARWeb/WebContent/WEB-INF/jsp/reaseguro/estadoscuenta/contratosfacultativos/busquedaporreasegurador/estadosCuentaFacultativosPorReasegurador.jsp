<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
  
<midas:formulario accion="/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle">
	<br clear="all" />
     <table id="filtros" style="width:97%;overflow:auto;table-layout:auto;margin-left: auto; margin-right: auto;">
     	<tr>
	     	<td width="10%"><b><midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.reaseguradorcorredor"/>:</b></td>
	   		<td width="30%">
	   			<midas:comboCatalogo propiedad="idtcReaseguradorCorredor" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
	   		</td>
	   		<td><b><midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.moneda"/>:</b></td>
	   		<td>
	   			<html:select property="idTcMoneda" styleClass="cajaTexto" styleId="comboIdMoneda" >
	   				<html:optionsCollection property="monedaDTOList" label="descripcion" value="idTcMoneda" />
	   			</html:select>
	   		</td>
   		</tr>
   		<tr>
   			<td><b><midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.ramo"/>:</b></td>
     		<td>
     			<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo"
					 onchange="limpiarObjetos('idTcSubRamo'); llenarCombo(this,'idSubRamo','/MidasWeb/subRamo.do');" styleClass="cajaTexto"/>
     		</td>
     		<td><b><midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.subRamo"/>:</b></td>
     		<td>
     			<midas:subramo propiedad="idTcSubRamo" ramo="idTcRamo" size="1" styleId="idSubRamo" styleClass="cajaTexto"/>
     		</td>
   		</tr>
   		<tr>
   			<td>
		     	<b><midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.hasta"/>(<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.mesanio"/>)
			     	<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioHasta();/*parent.mostrarCalendariofechaHasta();*/">
					<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>:</b>
	     	</td>
			<td width="15%"><midas:texto propiedadFormulario="hasta" id="fechaHasta" onblur="esFechaValidaFormatoMesAnio(this);" /></td>
   		</tr>
   		<tr>
   			<td colspan="4">
     			<div class="alinearBotonALaDerecha" style="margin-right:24px;" >
	     			<div id="b_consultar" style="width:180px">
	     				<a href="javascript: void(0);" onclick="javascript: consultarEstadoCuentaPorReasegurador('saldoTecnico');">
	     					<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.mostrarSaldoTecnico"/></a>
	     			</div>
     			</div>
     		</td>
   		</tr>
    </table>

<div style="height:10px;"></div>

     <table width="97%" id="filtros" style="margin-left: auto;margin-right: auto;">
     	<tr>
			<td class="datoTabla" colspan="6" onclick="var visible=document.getElementById('porPoliza').style.display;if(visible=='block'){document.getElementById('porPoliza').style.display='none';document.getElementById('porSiniestro').style.display='block';}else{document.getElementById('porPoliza').style.display='block';document.getElementById('porSiniestro').style.display='none';}" style="cursor:pointer" >
				<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.busquedaPorPoliza" /></td>
		</tr>
	</table>
	<center>
	<div id="porPoliza" style="display:block;background-color: white;width:97%;border: 1px solid #74D54B;">
	<table width="97%" id="filtros" height="50px" style="margin-left: auto;margin-right: auto;">
     	<tr>
     		<td width="20%" align="right"><b><midas:mensaje clave="reaseguro.estadodecuenta.nomAsegurado"/>:</b></td>
     		<td width="25%"  align="left"><midas:texto propiedadFormulario="busquedaNomAsegurado" id="busquedaNomAsegurado2" onkeypress="return stopRKey(event)"/></td>
     		<td width="15%" align="right"><b><midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.numeropoliza" />:</b></td>
			<td width="20%">
				<input id="busquedaNumPoliza2"
					style="border: 1px solid #A0E0A0; padding-left: 3px;"
					type="text" onkeypress="return stopRKey(event)"
					onfocus="new Mask('####-########-##', 'string').attach(this)" class="cajaTexto" />
			</td>
     		<td width="20%" align="left">
     			<div id="b_buscar">
     				<a href="javascript: void(0);"
						onclick="javascript:mostrarPolizaEstadoCuentaGrid($('busquedaNomAsegurado2').value, $('busquedaNumPoliza2').value);">
	 	 				<midas:mensaje clave="midas.accion.filtrar"/>
 	 				</a>
 	 			</div>
 	 		</td>
     	</tr>
	</table>
	<br clear="all"/>
     <center>
     	<div id="loadingPolizas" style="display:none"></div>
     	<div id="polizaEstadoCuentaGrid" width="732px" height="110px" style="background-color:white;overflow:hidden;margin-left:auto; margin-right:auto;" align="center"></div>
     <br clear="all"/>
     <table width="95%" height="50px">
	     <tr>
			<td width="80%">
				<div class="alinearBotonALaDerecha" style="margin-right:24px;" >
		     	<div id="b_consultar" style="width:180px">
			    	<a href="javascript: void(0);" onclick="javascript: consultarEstadoCuentaPorReasegurador('saldoTotalPolizas')">
			    		<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.mostrarTodasLasPolizas"/></a>
			    </div>
			    </div>
	     	</td>
	     	<td>
	     		<div class="alinearBotonALaDerecha" style="margin-right:24px;" >
		     	<div id="b_consultar">
		     		<a href="javascript: " onclick="javascript: consultarEstadoCuentaPorReasegurador('saldoPorPoliza')">
						<midas:mensaje clave="midas.accion.mostrar"/></a>
				</div>
				</div>
	     	</td>
	     </tr>
     </table>
     </center>
	</div>
	</center>
	
     <table width="97%" id="filtros" style="margin-left: auto;margin-right: auto;">
     	<tr><td class="datoTabla" colspan="6" onclick="var visible=document.getElementById('porSiniestro').style.display;if(visible=='block'){document.getElementById('porSiniestro').style.display='none';document.getElementById('porPoliza').style.display='block'}else{document.getElementById('porSiniestro').style.display='block';document.getElementById('porPoliza').style.display='none';}" style="cursor:pointer" >
				<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.busquedaPorSiniestro" /></td></tr>
	</table>
	<center>
	<div id="porSiniestro" style="display:none;background-color: white;width:97%;border: 1px solid #74D54B;">
	<table width="97%" id="filtros" style="margin-left: auto;margin-right: auto;" height="50px">	
		<tr>
     		<td width="20%" align="right"><b><midas:mensaje clave="reaseguro.estadodecuenta.nomAsegurado"/>:</b></td>
     		<td width="25%"  align="left">
     			<midas:texto propiedadFormulario="busquedaNomAsegurado" id="busquedaNomAsegurado" onkeypress="return stopRKey(event)"/>
     		</td>
     		<td width="15%" align="right">
				<b><midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.numeropoliza" />:</b>
			</td>
			<td width="20%">
				<input id="busquedaNumPoliza" style="border: 1px solid #A0E0A0; padding-left: 3px;"
					type="text" onkeypress="return stopRKey(event)" onfocus="new Mask('####-########-##', 'string').attach(this)" class="cajaTexto" />
			</td>
     	</tr>
     	<tr>
     		<td width="15%" align="right">
				<b><midas:mensaje clave="reaseguro.estadosdecuenta.contratofacultativo.numerosiniestro" />:</b>
			</td>
			<td width="20%">
				<input id="busquedaNumSiniestro" style="border: 1px solid #A0E0A0; padding-left: 3px;" type="text" onkeypress="return soloNumeros(this, event, false)" class="cajaTexto" />
			</td>
			<td width="15%" align="right">
				<b><midas:mensaje clave="siniestro.cabina.reportesiniestro.listarfechaReporte" />:</b>
			</td>
			<td width="20%">
				<input id="busquedaFechaSiniestro" style="border: 1px solid #A0E0A0; padding-left: 3px;" type="text" onkeypress="return soloNumeros(this, event, false)" class="cajaTexto" />
			</td>
     		<td width="20%" align="left">
     			<div id="b_buscar"> <a href="javascript: void(0);" onclick="javascript:mostrarGridSiniestro();/*mostrarEstadoCuentaFacultativoPorSiniestro();*/">
	 	 				<midas:mensaje clave="midas.accion.filtrar"/></a></div>
 	 		</td>
     	</tr>
	</table>
	<br clear="all"/>
    <center>
    	<div id="loadingSiniestros" style="display:none;background-color: white;"></div>
    	<div id="siniestroEstadoCuentaGrid" width="732px" height="110px" style="background-color:white;overflow:hidden;margin-left:auto; margin-right:auto;" align="center"></div>
    <br clear="all"/>
     <table width="95%" height="50px">
	     <tr>
			<td width="80%">
				<div class="alinearBotonALaDerecha" style="margin-right:24px;" >
		     	<div id="b_consultar" style="width:180px">
			    	<a href="javascript: void(0);" onclick="javascript: consultarEstadoCuentaPorReasegurador('saldoTotalSiniestros')">
			    		<midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.mostrarTodosLosSiniestros"/></a>
			    </div>
			    </div>
	     	</td>
	     	<td>
	     		<div class="alinearBotonALaDerecha" style="margin-right:24px;" >
		     	<div id="b_consultar">
		     		<a href="javascript: " onclick="javascript: consultarEstadoCuentaPorReasegurador('saldoPorSiniestro')">
						<midas:mensaje clave="midas.accion.mostrar"/></a>
				</div>
				</div>
	     	</td>
	     </tr>
     </table>
     </center>
	</div>
	</center>
</midas:formulario> 
