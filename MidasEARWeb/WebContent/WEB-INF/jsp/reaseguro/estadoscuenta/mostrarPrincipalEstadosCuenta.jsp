<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<div hrefmode="ajax-html" style= "height: 100%; width:95%; overflow:auto;margin-left: auto; margin-right: auto;" id="estadosCuentaFacultativoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
	<div width="350px" id="tipoReaseguro" name="Estados de Cuenta por Tipo de Reaseguro" href="http://void" extraAction="limpiarDiv('contenido_contratosFacultativosNuevo');sendRequest(null,'/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaTipoReaseguro.do', 'contenido_tipoReaseguro', 'inicializarCombosEdoCtaTipoReasegurador(),mostrarEstadosCuentaTipoReaseguroGrid()')"></div>
<%--	<div width="350px" id="contratosFacultativos" name="Estados de Cuenta Contratos Facultativos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaContratoFacultativo.do','contenido_contratosFacultativos','inicializaComponentesListarEstadosCuentaContratoFacultativo()')"></div>--%>
	<div width="350px" id="contratosFacultativosNuevo" name="Estados de Cuenta Contratos Facultativos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuentaFacultativo.do','contenido_contratosFacultativosNuevo','dhx_init_tabbars();cargarPestaniaEstadosCuentaFacultativoTabBar();')"></div>
</div>