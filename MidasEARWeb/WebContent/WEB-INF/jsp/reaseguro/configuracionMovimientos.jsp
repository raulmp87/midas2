<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>&nbsp;&nbsp; &nbsp; 
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>

<table>
<tr>
<td>	
<div id="configuracionMovimiento" style="width: 150px; float:left; height: 460px; overflow: auto;">
	<table  width="100%" height="70%" class="tablaConResultados">
		<tr>
			<td align="center">
				<a href="#" onclick="javascript: sendRequest(null, '/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do','configuracionMovimiento_detalle','dhx_init_tabbars()');"><midas:mensaje clave="contrato.egresos.accion.estado.menu.estadosDeCuenta"/></a>
			</td>
		</tr>
	 	<tr>
			<td align="center">
				<a href="#" onclick="javascript: void(0)"><midas:mensaje clave="contrato.egresos.accion.estado.menu.registrar.ingresos" /> </a>	
			</td>
		</tr>
		<tr>
			<td align="center">
				<a href="javascript: sendRequestFromAccordion(null,'/MidasWeb/reaseguro/egresos/administrarEgresos.do', 'configuracionMovimiento_detalle','mostraGridEgresosAseguradorSimple();mostraGridEgresosAseguradorFiltrado();');" ><midas:mensaje clave="contrato.egresos.accion.estado.menu.administrar.ingresos" /></a>	
			</td>
		</tr>
		<tr>
			<td align="center">
				<a href="#" onclick="javascript: void(0)"><midas:mensaje clave="contrato.egresos.accion.estado.menu.administrar.egresos"/> </a>	
			</td>
		</tr>	
	</table>
</div>
</td>
<td width="5"></td>
<td>
<div id="configuracionMovimiento_detalle" style="width: 650px;  float:left; height: 460px;"/>	
<div style="clear:both"></div>
</td>
</tr>
</table>