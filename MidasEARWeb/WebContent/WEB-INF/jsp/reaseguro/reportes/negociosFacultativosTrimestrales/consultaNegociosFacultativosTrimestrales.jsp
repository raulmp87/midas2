<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/reaseguro/reportes/mostrarReporteSaldosPorTrimestre">
<html:hidden property="tipoReporte" name="reportesReaseguroForm" styleId="tipoReporte"/>
    <table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td class="titulo" colspan="4"><midas:mensaje clave="reaseguro.reporte.negociosFacultativosPorTrimestre"/></td>
     	</tr>
     	<tr height="15px"></tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.reaseguradorcorredor"/>:</b></td>
			<td align="left"><midas:comboCatalogo propiedad="idPrioridadSiniestro" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" /></td>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.ejercicio"/>:</b></td>
			<td align="left"><midas:texto propiedadFormulario="idEjercicio" id="idEjercicio" onkeypress="return soloNumeros(this, event, false)" longitud="4" /></td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.suscripcion"/>:</b></td>
			<td align="left"><html:select property="idSuscripcion" name="reportesReaseguroForm" styleId="idSuscripcion" styleClass="cajaTexto" >
								<html:option value="1"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre1"/></html:option>
								<html:option value="2"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre2"/></html:option>
								<html:option value="3"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre3"/></html:option>
								<html:option value="4"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre4"/></html:option>
							</html:select>  </td>
		</tr>
	</table>
	<div class="alinearBotonALaDerecha">
			<div id="b_reporteXLS">
				<a href="javascript: void(0);" onclick="javascript: generarReporteNegociosFacultativosTrimestrales();">
				<midas:mensaje clave="midas.accion.reporteExcel"/>
				</a>
			</div>
		</div>
</midas:formulario>