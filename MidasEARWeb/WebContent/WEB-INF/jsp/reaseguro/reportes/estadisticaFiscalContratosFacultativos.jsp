<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/contratofacultativo/mostrarFiltroRptEstadisticaFiscal">
    <table id="filtros" width="97%" style="right-margin:auto;left-margin:auto;font-size:10px">	
   		<tr>
	     	<td class="titulo" colspan="3">
				<midas:mensaje clave="reaseguro.estadisticafiscal.facultativo"/>
			</td>
     	</tr>
     	<br clear="all"/>
     	<tr>
     		<td width="10%" align="right"><b><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.etiqueta"/>:</b></td>
     		<td width="20%">
     			<select id="comboTipoReporte" onchange="actualizarFiltroReporteEstadisticaFiscalFacultativo()">
     				<option value="0"><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.rptporfechas"/></option>
     				<option value="1"><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.rptporfechasreasegurador"/></option>
     				<option value="2"><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.rptporfechaspoliza"/></option>
     			</select>
     		</td>
     		<td width="70%"></td>
     	</tr>
   </table>
   <br clear="all"/>
    <table width="100%" style="right-margin:auto;left-margin:auto;font-size:10px">
		<tr>
			<td align="right" width="10%"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="15%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="3%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td width="72%">&nbsp;</td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly" />
			</td> 
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
		  <td colspan="4">
			     <table id="tablaConResultados" width="100%">
					  <tr>
					    <td colspan="4">
					    	<div id="divBusquedaPorReasegurador" style="display:none">
						    	<table width="100%" style="font-size:10px">
						    		<tr>
						    			<td width="10%" align="right"><b><midas:mensaje clave="reaseguro.estadisticafiscal.tiporeaseguradorcorredor"/></b></td>
							    		<td width="20%">
							    			<midas:combo id="tipoParticipante" propiedad="tipoParticipante" styleClass="cajaTexto" 
												onchange="limpiarObjetos('idtcReaseguradorCorredor'); llenarCombo(this,'idtcReaseguradorCorredor','/MidasWeb/reaseguradorCorredor.do');">	
													<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
													<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.participacion.tipoparticipante.corredorDescripcion" /></midas:opcionCombo>
													<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.participacion.tipoparticipante.reaseguradorDescripcion" /></midas:opcionCombo>						
											</midas:combo>
							    		</td>
							    		<td width="5%">&nbsp;</td>
							    		<td width="15%" align="right">
							    			<b><midas:mensaje clave="reaseguro.estadisticafiscal.nombrereaseguradorcorredor"/></b>
							    		</td>
							    		<td width="50%">
									    	<div style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;">
									    		<midas:reaseguradorCorredor styleId="idtcReaseguradorCorredor" styleClass="cajaTexto" size="1" tipo="tipoParticipante" propiedad="idtcReaseguradorCorredor"/>
									    	</div>
							    		</td>
								    </tr>
							    </table>
						     </div>
					    </td>  
					 </tr>
					 <tr style="font-size:10px">
						<td align="right" width="15%">
							<div id="busquedaPorPoliza01" style="display: none">
								<b><midas:mensaje clave="reaseguro.estadodecuenta.nomAsegurado" />:</b>
							</div>
						</td>
						<td align="left" width="20%">
							<div id="busquedaPorPoliza02" style="display:none">
								<input class="cajaTexto" style="" id="busquedaNomAsegurado"
									onkeypress="return stopRKey(event)"/>
							</div>
						</td>
						<td width="35%" align="right">
							<div id="busquedaPorPoliza03" style="display:none;font-family: Verdana, Arial, Helvetica, sans-serif; padding: 3px; font-size: 10px;">
								<b><midas:mensaje
										clave="contratos.movimiento.reporte.movtosporreasegurador.numeropoliza" />:</b>
								<input id="busquedaNumPoliza"
									style="border: 1px solid #A0E0A0; padding-left: 3px;"
									type="text" onkeypress="return stopRKey(event)"
									onfocus="new Mask('####-########-##', 'string').attach(this)" />
							</div>
						</td>
						<td align="left" width="30%">
				     			<div id="b_buscar" style="display:none">
				     				<a href="javascript: void(0);"
										onclick="javascript: mostrarIndicadorCargaGenerico('loadingPolizas');mostrarPolizaRptEstadisticaFiscalFacultativoGrid($('busquedaNomAsegurado').value, $('busquedaNumPoliza').value);">
					 	 				<midas:mensaje clave="midas.accion.filtrar"/>
				 	 				</a>
				 	 			</div>
				 	 	</td>
			     	</tr>
     			</table>
			    <br clear="all"/>
			    <div id="loadingPolizas" style="display:none"></div>
			    <center>
			    	<div id="polizaRptEstadisticaFiscalFacultativoGrid" width="732px" height="250px" style="display:none;background-color:white;overflow:hidden;margin-left:auto; margin-right:auto;" align="center"></div>
			    </center>
		   </td>
 	  	</tr>
 	  	<br clear="all"/>
 	  	<tr>
 	  		<td colspan="3"></td>
 	  		<td>
 	  			<div class="alinearBotonALaDerecha" >
					<div id="b_reporteXLS" style="width:140px;margin-right:50px;">
						<a href="javascript: void(0);" onclick="javascript: mostrarReporteEstadisticaFiscalFacultativo()">
							<midas:mensaje clave="contratos.movimiento.generarreporte.porfechas"/>
						</a>
					</div>
				</div>
			</td>
 	  	</tr>
	</table>
</midas:formulario>