<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
<midas:formulario accion="/reaseguro/reportes/movimientosPorContrato">
    <table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td class="titulo" colspan="5">
				<midas:mensaje clave="reaseguro.reporte.movtosContrato"/>
			</td>
     	</tr>
		<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="reaseguro.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="18%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="15%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly"/>
			</td> 
			<td colspan="2"></td>
		</tr>
		<tr>
			<td align="right">
				<b><midas:mensaje clave="reaseguro.reporte.trimestralReaseguro.moneda"/>:</b>
			</td>
			<td>
				<midas:comboCatalogo propiedad="moneda" size="1" styleId="moneda" nombre="reportesReaseguroForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="false"/>				
			</td>
			<td colspan="2">
				<div id="b_reporteXLS" style="width:140px;">
					<a href="javascript: void(0);" onclick="rptMovtosPorContrato();"><midas:mensaje clave="reaseguro.reporte.generarreporte"/></a>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>