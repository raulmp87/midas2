<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/reaseguro/reportes/mostrarFiltroRptCumulos">
	<table style="width:97%;overflow:scroll;table-layout:auto;" id="filtros">
     	<tr>
	     	<td class="titulo" colspan="3">
				<midas:mensaje clave="reaseguro.reporte.cumulos"/>
			</td>
     	</tr>
     	<tr height="15px"><td>&nbsp;</td></tr>
    </table>
    <br clear="all" />
    <br clear="all" />
    <table width="97%" style="right-margin:auto;left-margin:auto;font-size:10px">
    	<tr>
     		<td align="right" width="15%">
				<b><midas:mensaje clave="reaseguro.reporte.cumulos.tipoCumulo"/>:</b>
			</td>
			<td width="30%">
				<select id="tipoCumulo" class="cajaTexto">
					<option value="0"><midas:mensaje clave="reaseguro.reporte.cumulos.tipocumulo.huracan"/></option>
					<option value="1"><midas:mensaje clave="reaseguro.reporte.cumulos.tipocumulo.sismo"/></option>
				</select>
			</td>
			<td colspan="2" width="55%"></td>
     	</tr>
    	<tr>
     		<td align="right" width="15%">
				<b><midas:mensaje clave="reaseguro.reporte.cumulos.tipoDeducible"/>:</b>
			</td>
			<td width="30%">
				<select id="tipoDeducible" class="cajaTexto">
					<option value="-1">Todos</option>
					<option value="1"><midas:mensaje clave="reaseguro.reporte.cumulos.tipodeducible.dentroTarifaAmis"/></option>
					<option value="2"><midas:mensaje clave="reaseguro.reporte.cumulos.tipodeducible.otros"/></option>
				</select>
			</td>
			<td colspan="2" width="55%"></td>
     	</tr>
     	<tr>
     		<td align="right">
				<b><midas:mensaje clave="reaseguro.reporte.cumulos.tipoContrato"/>:</b>
			</td>
			<td>
				<html:select property="tipoContrato" styleId="tipoContrato" styleClass="cajaTexto">
					<html:optionsCollection property="tipoReaseguroList" name="reportesReaseguroForm" value="tipoReaseguro" label="descripcion"/>
				</html:select>
			</td>
			<td colspan="2"></td>
     	</tr>	
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.fecha.inicial"/>:</b></td>
			<td align="left">
				<midas:texto propiedadFormulario="fechaInicial"  id="fechaInicial" soloLectura="true"/>				
			</td>
			<td width="10%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td width="45%"></td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<midas:texto propiedadFormulario="fechaFinal"  id="fechaFinal" soloLectura="true"/>
			</td> 
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td colspan="2"></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td></td>
			<td align="right">
				<div id="b_reporteXLS" style="width:140px;text-align:center">
					<a href="javascript: void(0);" onclick="rptCumulosReaseguro();"><midas:mensaje clave="reaseguro.reporte.generarreporte"/></a>
				</div>
			</td>
			<td colspan="2"></td>
		</tr>
	</table>
</midas:formulario>