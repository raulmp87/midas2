<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/reaseguro/reportes/movimientoSiniestrosReservasPendientes">
	<table style="width:97%;overflow:scroll;table-layout:auto;">
     	<tr>
	     	<td class="titulo" colspan="5">
				<midas:mensaje clave="reaseguro.reporte.movimientosSiniestrosReservasPendientes"/>
			</td>
     	</tr>
    </table>
    &nbsp;
	<table width="97%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td colspan="5">
	     		<midas:mensaje clave="reaseguro.reporte.movimientosSiniestrosReservasPendientes.parametrosReporte"/>
				<midas:combo propiedad="parametrosReporte"
					onchange="javascript: ocultaMovimientoSiniestroReservasPendientes(this.value);">
					<midas:opcionCombo valor="1">Rango de Fechas</midas:opcionCombo>
					
				</midas:combo>
			</td>
     	</tr>		
	</table>
	&nbsp;
	<div id="ramoOficial" style="display: none;">
	<table width="97%" id="filtros" style="right-margin:auto;left-margin:auto;">
	<tr>
		<td class="titulo" colspan="5">
			<midas:mensaje clave="reaseguro.reporte.movimientosSiniestrosReservasPendientes.ramoOficial"/>
		</td>
	</tr>
	<tr>
		<td>
			<midas:mensaje clave="reaseguro.reporte.movimientosSiniestrosReservasPendientes.ramo"/>
		</td>
		<td colspan="2">			
			<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto" />
		</td>
	</tr>
	</table>
	</div>

    <div id="porFechas" style="display: ;">
    <table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    <tr>&nbsp;</tr>
    <tr>    	
    	<td class="titulo" colspan="5">
    		<midas:mensaje clave="reaseguro.reporte.movimientosSiniestrosReservasPendientes.porfechas"/>
    	</td>
    </tr>
    <tr>&nbsp;</tr>
	</table>
	</div>
	<table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    	<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="18%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="15%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly"/>
			</td> 
			<td colspan="2">
				<div id="b_reporteXLS" style="width:140px; ">
					<a href="javascript: void(0);" onclick="javascript: rptSinReservaPendiente();"><midas:mensaje clave="contratos.movimiento.generarreporte.porfechas"/></a>
				</div>
			</td>
		</tr>
		</table>
</midas:formulario>