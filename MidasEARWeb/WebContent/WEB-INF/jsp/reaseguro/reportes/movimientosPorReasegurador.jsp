<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
<midas:formulario accion="/reaseguro/egresos/administrarEgresos.do">
 
	<table style="width:97%;overflow:scroll;table-layout:auto;" id="filtros">
     	<tr>
	     	<td class="titulo" colspan="3">
				<midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador"/>
			</td>
     	</tr>
     	<tr><td colspan="3">&nbsp;</td></tr>
    	<tr>
     		<td width="20%" align="right">
     			<b><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.etiqueta"/>:</b>
     		</td>
     		<td width="30%">
     			<select id="comboTipoReporte" onchange="actualizarFiltroReporteMovimientosPorReasegurador()" class="cajaTexto">
     				<option value="0"><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.combotiporeporte.porfechas"/></option>
     				<option value="1"><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.combotiporeporte.porfechaspoliza"/></option>
     				<option value="2"><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.combotiporeporte.porfechassubramo"/></option>
     			</select>
     		</td>
     		<td width="50%"></td>
     	</tr>
     	<tr>
     		<td align="right"><b><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.titulocomboretencion"/>:</b></td>
     		<td>
     			<input type="radio" name="reporteAMostrar" value="0" onclick="mostrarRadiosRetencionRptReas(0)"><b>Normal</b>
     		</td>
     		<td>
     			
     		</td>
     	</tr>
     	<tr>
     		<td></td>
     		<td><input type="radio" name="reporteAMostrar" value="1" onclick="mostrarRadiosRetencionRptReas(1)"><b>Detallado</b>
     			<div id="divComboRetencion" style="display:none">
     				<select id="comboRetencion" class="cajaTexto">
     					<option value="0"><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.comboretencion.opcionsinretencion"/></option>
     					<option value="1"><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.comboretencion.opcionconretencion"/></option>
     				</select>
     			</div>
     		</td>
     		<td>
     			
     		</td>
     	</tr>
    </table>
    <br clear="all" />
    <br clear="all" />
    <table width="98%" style="right-margin:auto;left-margin:auto;font-size:10px;">	
   		<tr>
	     	<td class="titulo" colspan="5">
				<midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.porfechas"/>
			</td>
     	</tr>
		<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="18%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="5%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td width="32%">
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td width="30%">
			</td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly"/>
			</td> 
			<td colspan="2"></td>
		</tr>
	</table>
    <br clear="all" />
    <div id="divBusquedaPolizas" style="display:none">
		<table style="width: 98%; overflow: scroll; table-layout: auto;">
			<tr>
				<td class="titulo" colspan="5">
					<midas:mensaje
						clave="contratos.movimiento.reporte.movtosporreasegurador.porpoliza" />
				</td>
			</tr>
		</table>
		<table width="98%" style="table-layout: auto;">
			<tr>
				<td width="70%">
					<table id="tablaConResultados" width="100%">
						<tr>
							<td width="80%">
								<table width="100%">
										<tr>
											<td align="right">
												<div style="font-family: Verdana, Arial, Helvetica, sans-serif; padding: 3px; font-size: 10px;">
													<b><midas:mensaje clave="contratos.egresos.administrar.nombreAsegurador" />:</b>
													<input id="busquedaNomAsegurado"
														style="border: 1px solid #A0E0A0; padding-left: 3px;"
														type="text" onkeypress="return stopRKey(event);"/>
												</div>
											</td>
											<td align="right">
												<div style="font-family: Verdana, Arial, Helvetica, sans-serif; padding: 3px; font-size: 10px;">
													<b><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.numeropoliza" />:</b>
													<input id="busquedaNumPoliza"
														style="border: 1px solid #A0E0A0; padding-left: 3px;"
														type="text" onkeypress="return stopRKey(event);" onfocus="new Mask('####-########-##', 'string').attach(this)" />
												</div>
											</td>
											<td>
												<div id="b_regresar" style="width: 120px;">
													<a href="javascript: void(0);"
														onclick="javascript: mostrarIndicadorCargaGenerico('loadingPolizas');mostrarPolizaAdministrarReporteGrid($('busquedaNomAsegurado').value, $('busquedaNumPoliza').value);"><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.filtrarPoliza" />
													</a>
												</div>
											</td>
										</tr>
									</table>
							</td>
							<td width="20%"></td>
						</tr>
						<tr>
							<td colspan="2">
								<div id="loadingPolizas" style="display: none"></div>
								<center>
									<div id="polizaReporteMovtosPorReaseguradorGrid"
										style="width: 840px; height: 275px; overflow: auto;"></div>
								</center>
							</td>
						</tr>
						<tr>
							<td>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<br clear="all"/>
	<div id="divBusquedaSubRamo" style="display:none">
		<table style="width: 98%; overflow: scroll; table-layout: auto;font-size:10px;">
			<tr>
				<td class="titulo" colspan="4">
					<midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.porsubramo" />
				</td>
			</tr>
			<tr>
				<td width="10%" align="right">
	     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.ramo"/>:
	     		</td>
	     		<td width="30%">
	     			<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo"
						 onchange="limpiarObjetos('idTcSubRamo'); llenarCombo(this,'idTcSubRamo','/MidasWeb/subRamo.do');" styleClass="cajaTexto"/>
	     		</td>
	     		<td width="10%" align="right">
	     			<midas:mensaje clave="reaseguro.estadosdecuenta.tiporeaseguro.subRamo"/>:
	     		</td>
	     		<td width="50%">
	     			<midas:subramo propiedad="idTcSubRamo" ramo="idTcRamo" size="1" styleId="idTcSubRamo" styleClass="cajaTexto" />
	     		</td>
     		</tr>
		</table>
	</div>
	<br clear="all"/>
	<table>
		<tr>
			<td width="60%"></td>
			<td width="20%">
				<div id="b_reportePDF" style="width: 160px;margin-right:15px;">
					<a href="javascript: void(0);"
						onclick="javascript: mostrarReporteMovtosPorReasegurador('PDF');"><midas:mensaje clave="reaseguro.reporte.generarreporte.pdf" />
					</a>
				</div>
			</td>
			<td width="20%">
				<div id="b_reporteXLS" style="width: 160px;margin-right:50px;float:left">
					<a href="javascript: void(0);"
						onclick="javascript: mostrarReporteMovtosPorReasegurador('XLS');"><midas:mensaje clave="reaseguro.reporte.generarreporte.xls" />
					</a>
				</div>
			</td>
		</tr>
		<tr height="10px"><td colspan="3"></td></tr>
		<tr>
			<td width="60%"></td>
			<td width="20%">
				<div id="b_reporteXLS" style="width: 160px;margin-right:15px;">
					<a href="javascript: void(0);"
						onclick="javascript: mostrarReporteMovtosPorReasegurador('CSV');">Generar Reporte CSV
					</a>
				</div>
			</td>
			<td width="20%">
				<div id="b_imprimir" style="width: 160px;margin-right:50px;float:left">
					<a href="javascript: void(0);"
						onclick="javascript: mostrarReporteMovtosPorReasegurador('TXT');">Generar Reporte Texto
					</a>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>