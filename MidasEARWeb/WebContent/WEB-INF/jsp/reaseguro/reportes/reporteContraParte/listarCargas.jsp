<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>

<div id="resultados">
	<midas:tabla idTabla="solicitudes"
		claseDecoradora="mx.com.afirme.midas.decoradores.CargasContraParte"
		claseCss="tablaConResultados" nombreLista="cargas"
		urlAccion="/MidasWeb/reaseguro/reportes/reportercscontraparte/listarCargas.do">
		<midas:columna propiedad="fechaFin" titulo="Fecha Fin Corte" />
		<midas:columna propiedad="fechaCarga" titulo="Fecha de Carga" />
		<midas:columna propiedad="reporte" titulo="Reporte" />
		<midas:columna propiedad="estatus" titulo="Estatus" />
		<midas:columna propiedad="usuario" titulo="Usuario" />
		<midas:columna propiedad="acciones"/>
	</midas:tabla>

</div>