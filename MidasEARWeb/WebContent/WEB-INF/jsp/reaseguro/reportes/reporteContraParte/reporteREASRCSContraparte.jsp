<%@ page isELIgnored="false"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>

<style type="text/css">

div#resultadosDocumentos {
    border: 0 solid red;
    height: 120px;
    overflow: auto;
    width: 96%;
}
div#resultadosCargas {
    border: 0 solid red;
    height: 230px;
    overflow: auto;
    width: 96%;
}

.mensaje_contenido {
	height: 120px;
}

.mensaje_texto {
font-size: 11px;
font-weight: normal;
height: 100px;
}
</style>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Reporte Importe Recuperable REAS
	</div>
	
	<center>
		<midas:formulario accion="/reaseguro/reportes/reportercscontraparte/mostrarRCSContraparte">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Corte</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaCorte" name="reporteRCSContraparteForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td>					
					<td colspan="">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
					<th>Tipo de Cambio</th>
				    <td>
						<html:text styleId="tipoCambio" property="tipoCambio" name="reporteRCSContraparteForm" value="0" size="10" styleClass="cajaTexto" />
					</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<div class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: actualizarEstatusCargaContraParte(document.reporteRCSContraparteForm);" style="width:90%" tipo="continuar" texto="Actualizar Estatus Cargas"/>
						</div>
					</td>
					<td>
						<div class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: actualizarEstatusContraParte(document.reporteRCSContraparteForm);" style="width:90%" tipo="continuar" texto="Actualizar Estatus Midas"/>
						</div>
					</td>
					<td>&nbsp;</td>
				    <td>
					     <div id="procesar" class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: procesarReporteContraParte(document.reporteRCSContraparteForm, 1);" style="width:80%" tipo="continuar" texto="Iniciar Proceso"/>
						</div>					
					</td>
					<td>
						<div class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: generarReporte(document.reporteRCSContraparteForm);" style="width:80%" tipo="continuar" texto="Generar Reporte"/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="5">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
				<tr>
					<td colspan="5">
					<div id="resultadosCargas"></div>
					</td>
				</tr>	
			</table>
		</midas:formulario>
	</center>
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>