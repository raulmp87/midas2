<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>

<div id="resultadosDocumentos">
	<midas:tabla idTabla="solicitudes"
		claseDecoradora="mx.com.afirme.midas.decoradores.ReporteContraParte"
		claseCss="tablaConResultados" nombreLista="documentos"
		urlAccion="/MidasWeb/reaseguro/reportes/reportercscontraparte/listarDocumentos.do">
		<midas:columna propiedad="fechaCorte" titulo="Fecha de Corte" />
		<midas:columna propiedad="iniEjecucion" titulo="Inicio Ejecución" />
		<midas:columna propiedad="finEjecucion" titulo="Fin Ejecución" />
		<midas:columna propiedad="reporte" titulo="Reporte" />
		<midas:columna propiedad="estatus" titulo="Estatus" />
	</midas:tabla>

</div>