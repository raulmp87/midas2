<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/contratos/mostrarFiltroRptEstadisticaFiscal">
    <table id="filtros" width="97%" style="right-margin:auto;left-margin:auto;font-size:10px">	
   		<tr>
	     	<td class="titulo" colspan="3">
				<midas:mensaje clave="reaseguro.estadisticafiscal.automatico"/>
			</td>
     	</tr>
     	<br clear="all"/>
     	<tr>
     		<td width="10%" align="right"><b><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.etiqueta"/>:</b></td>
     		<td width="20%">
     			<select id="comboTipoReporte" onchange="actualizarFiltroReporteEstadisticaFiscalAutomaticos()">
     				<option value="0"><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.rptporfechas"/></option>
     				<option value="1"><midas:mensaje clave="reaseguro.estadisticafiscal.combotiporpt.rptporfechasreasegurador"/></option>
     			</select>
     		</td>
     		<td width="70%"></td>
     	</tr>
     </table>
     	<br clear="all"/><br clear="all"/>
     <table width="97%" style="right-margin:auto;left-margin:auto;font-size:10px">	
		<tr>
			<td align="right" width="10%">
				<b><midas:mensaje
						clave="contratos.movimiento.reporte.fecha.inicial" />:</b>
			</td>
			<td align="left" width="15%">
				<input type="text" id="fechaInicial" readonly="readonly" />
			</td>
			<td width="3%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario"
						onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" width="10%">
				<b><midas:mensaje
						clave="contratos.movimiento.reporte.fecha.final" />:</b>
			</td>
			<td align="left" width="15%">
				<input type="text" id="fechaFinal" readonly="readonly" />
			</td>
			<td>
				<div id="rangoDeFechas"
					style="position: absolute; z-index: 100; overflow: visible;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
		</tr>
		<tr>
		  <td colspan="6">
			     <table id="tablaConResultados" width="100%">
					  <tr>
					    <td colspan="3">
					    	<div id="divBusquedaPorReasegurador" style="display:none">
						    	<table width="98%" style="font-size:10px">
						    		<tr>
						    			<td width="10%" align="right"><b><midas:mensaje clave="reaseguro.estadisticafiscal.tiporeaseguradorcorredor"/></b></td>
							    		<td width="20%">
							    			<midas:combo id="tipoParticipante" propiedad="tipoParticipante" styleClass="cajaTexto" deshabilitado="true"
												onchange="limpiarObjetos('idtcReaseguradorCorredor'); llenarCombo(this,'idtcReaseguradorCorredor','/MidasWeb/reaseguradorCorredor.do');">	
													<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
													<midas:opcionCombo valor="0"><midas:mensaje clave="contratos.participacion.tipoparticipante.corredorDescripcion" /></midas:opcionCombo>
													<midas:opcionCombo valor="1"><midas:mensaje clave="contratos.participacion.tipoparticipante.reaseguradorDescripcion" /></midas:opcionCombo>						
											</midas:combo>
							    		</td>
							    		<td width="8%"></td>
							    		<td width="20%" align="right">
							    			<b><midas:mensaje clave="reaseguro.estadisticafiscal.nombrereaseguradorcorredor"/></b>
							    		</td>
							    		<td width="40%">
									    	<div style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;">
									    		<midas:reaseguradorCorredor styleId="idtcReaseguradorCorredor" styleClass="cajaTexto" size="1" tipo="tipoParticipante" propiedad="idtcReaseguradorCorredor"/>
									    	</div>
							    		</td>
								    </tr>
							    </table>
						     </div>
					    </td>  
					 </tr>
     			</table>
		   </td>
 	  	</tr>
 	  	<br clear="all"/>
 	  	<tr>
 	  		<td colspan="5"></td>
 	  		<td>
 	  			<div class="alinearBotonALaDerecha" >
					<div id="b_reporteXLS" style="width:140px;margin-right:50px;">
						<a href="javascript: void(0);" onclick="javascript: mostrarReporteEstadisticaFiscalAutomaticos()"><midas:mensaje clave="contratos.movimiento.generarreporte.porfechas"/></a>
					</div>
				</div>
			</td>
 	  	</tr>
	</table>
</midas:formulario>