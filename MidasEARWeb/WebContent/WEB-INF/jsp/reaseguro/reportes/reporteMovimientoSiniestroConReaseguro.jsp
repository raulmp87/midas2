<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/reaseguro/reportes/movimientoSiniestroReaseguro">
    <table width="97%" id="filtros" style="right-margin:auto;left-margin:auto;">
    	<tr>
	     	<td class="titulo" colspan="2">
				<midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador"/>
			</td>
     	</tr>
   		<tr>
	     	<td width="40%">
	     		<midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.parametrosReporte"/>
				<midas:combo propiedad="parametrosReporte" id="parametrosReporte"
					onchange="javascript: ocultaMovimientoSiniestroConReaseguro(this.value);" styleClass="cajaTexto">
					<midas:opcionCombo valor="1">Rango de Fechas</midas:opcionCombo>
					<midas:opcionCombo valor="2">Numero de Siniestro Y Rango de Fechas</midas:opcionCombo>
					<midas:opcionCombo valor="4">Reasegurador y Rango de Fechas</midas:opcionCombo>
				</midas:combo>
			</td>
			<td width="60%"></td>
     	</tr>		
	</table>
    <br clear="all" />
    <div id="numeroSiniestro" style="display: none;">
    	<table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    	 	<tr>
		     	<td class="titulo" colspan="5">
					<midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.numeroSiniestro"/>
				</td>
			</tr>
			<tr>&nbsp;</tr>
			<tr>
				<td width="25%" align="right">
					<b><midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.siniestro"/>:</b>
				</td>
				<td width="25%">
					<midas:texto propiedadFormulario="numeroSiniestro" id="pNoSiniestro" onkeypress="return soloNumeros(this, event, false)" caracteres="16"/>
				</td>
				<td width="50%">
					&nbsp;
				</td>
   	 	 	</tr>
   	 	 	<tr>&nbsp;</tr>
	    </table>
    </div>
    <div id="tipoMovimiento" style="display: none;">
    	<table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    	 	<tr>
		     	<td class="titulo" colspan="5">
					<midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.tipoMovimiento"/>
				</td>
			</tr>
			<tr>&nbsp;</tr>
			<tr>
				<td width="25%" align="right">
					<b><midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.movimiento"/>:</b>
				</td>
				<td width="25%">
					<midas:combo propiedad="movimiento" id="movimiento" styleClass="cajaTexto">
						<midas:opcionCombo valor="0">Indemnizaci&oacute;n</midas:opcionCombo>
						<midas:opcionCombo valor="1">Recuperaci&oacute;n</midas:opcionCombo>
						<midas:opcionCombo valor="2">Gasto</midas:opcionCombo>
						<midas:opcionCombo valor="3">Salvamento</midas:opcionCombo>
						<midas:opcionCombo valor="4">Reserva</midas:opcionCombo>
					</midas:combo>
				</td>
				<td width="50%">
				</td>
   	 	 	</tr>
   	 	 	<tr>&nbsp;</tr>
	    </table>
    </div>
    <div id="reasegurador" style="display: none;">
    	<table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    	 	<tr>
		     	<td class="titulo" colspan="5">
					<midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.porReasegurador"/>
				</td>
			</tr>
			<tr>&nbsp;</tr>
			<tr>
				<td width="25%" align="right">
					<b><midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.reasegurador"/>:</b>
				</td>
				<td width="40%">
					<midas:comboCatalogo propiedad="idtcReaseguradorCorredor" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
				</td>
				<td width="35%">
				</td>
   	 	 	</tr>
   	 	 	<tr>&nbsp;</tr>
	    </table>
    </div>
    <div id="porFechas" style="display: ;">
    <table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    <tr>&nbsp;</tr>
    <tr>    	
    	<td class="titulo" colspan="5">
    		<midas:mensaje clave="contratos.movimiento.reporte.movtosdesiniestrosconreasegurador.porFechas"/>
    	</td>
    </tr>
    <tr>&nbsp;</tr>
	</table>
	</div>
	<table id="filtros" style="width:97%;overflow:scroll;table-layout:auto;">
    	<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="18%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="15%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly"/>
			</td> 
			<td colspan="2">
				<div id="b_reporteXLS" style="width:140px; ">
					<a href="javascript: void(0);" onclick="javascript: rptSiniestrosConReaseguro();"><midas:mensaje clave="contratos.movimiento.generarreporte.porfechas"/></a>
				</div>
			</td>
		</tr>
		</table>
</midas:formulario>