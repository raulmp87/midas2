<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/reaseguro/reportes/mostrarReporteSaldosPorPolizaSiniestro">
<html:hidden property="tipoReporte" name="reportesReaseguroForm" styleId="tipoReporte"/>
    <table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td class="titulo" colspan="4">
				<logic:equal value="siniestro" property="tipoReporte" name="reportesReaseguroForm" >
					<midas:mensaje clave="reaseguro.reporte.saldosPorSiniestro"/>
				</logic:equal>
				<logic:equal value="poliza" property="tipoReporte" name="reportesReaseguroForm" >
					<midas:mensaje clave="reaseguro.reporte.saldosPorPoliza"/>
				</logic:equal>
			</td>
     	</tr>
     	<tr height="15px"></tr>
		<tr>
			<td align="right" ><b><midas:mensaje clave="reaseguro.reporte.fecha.inicial"/>:</b></td>
			<td align="left" >
				<midas:texto propiedadFormulario="fechaInicial" id="fechaInicial" soloLectura="true" />
			</td>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<midas:texto propiedadFormulario="fechaFinal" id="fechaFinal" soloLectura="true" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()">
					<img border="1" src="/MidasWeb/img/b_calendario.gif" />
				</a>
			</td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" width="15%">
				<b><midas:mensaje clave="reaseguro.reporte.trimestralReaseguro.moneda"/>:</b>
			</td>
			<td width="20%">
				<midas:comboCatalogo propiedad="moneda" size="1" styleId="moneda" nombre="reportesReaseguroForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="false"/>				
			</td>
			<td>
				<label for="radioSoloFacultativo">
				<html:radio property="tipoContrato" value="soloFacultativo" styleId="radioSoloFacultativo" >
					<midas:mensaje clave="reaseguro.reporte.saldosPorPoliza.soloFacultativo"/></html:radio>
				</label>
			</td>
			<td>
				<label for="radioSoloAutomatico">
				<html:radio property="tipoContrato" value="soloAutomatico" styleId="radioSoloAutomatico" >
					<midas:mensaje clave="reaseguro.reporte.saldosPorPoliza.soloAutomatico"/></html:radio>
				</label>
			</td>
		</tr>
		<tr>
<%--			<td>--%>
<%--				<html:radio property="tipoContrato" value="todo" >--%>
<%--					<midas:mensaje clave="reaseguro.reporte.saldosPorPoliza.todo"/></html:radio>--%>
<%--			</td>--%>
			
		</tr>
	</table>
	<div class="alinearBotonALaDerecha">
			<div id="b_reporteXLS">
				<a href="javascript: void(0);" onclick="javascript: generarReporteSaldos();">
				<midas:mensaje clave="midas.accion.reporteExcel"/>
				</a>
			</div>
		</div>
</midas:formulario>