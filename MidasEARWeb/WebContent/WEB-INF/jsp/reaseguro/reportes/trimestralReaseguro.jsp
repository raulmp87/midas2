<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
 
 <midas:formulario accion="/reaseguro/reportes/mostrarFiltroRptTrimestralReaseguro">
	<table style="width:97%;overflow:scroll;table-layout:auto;" id="filtros">
     	<tr>
	     	<td class="titulo" colspan="3">
				<midas:mensaje clave="reaseguro.reporte.trimestralReaseguro"/>
			</td>
     	</tr>
     	<tr>
     		<td align="right" width="20%">
				<b><midas:mensaje clave="reaseguro.reporte.trimestralReaseguro.tipoReporteTrim"/>:</b>
			</td>
			<td width="45%">
				<select id="tipoReporteTrim" class="cajaTexto">
					<option value="0">Total Prima Cedida Operaci&oacute;n y Ramo (FOR)</option>
					<option value="1">Negocios m&aacute;s importantes (Distribuci&oacute;n-Negocios) (FDN)</option>
					<option value="2">Negocios m&aacute;s importantes (Distribuci&oacute;n-Reaseguradoras) (FDR)</option>
					<option value="3">Resumen Principales Reaseguradores (FRE)</option>
				</select>
			</td>
			<td width="35%"></td>
     	</tr>
    </table>
    <br clear="all" />
    <br clear="all" />
    <table width="97%" style="right-margin:auto;left-margin:auto;font-size:10px">	
		<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="reaseguro.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="40%">
				<midas:texto propiedadFormulario="fechaInicial"  id="fechaInicial" soloLectura="true"/>				
			</td>
			<td width="10%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td colspan="2" width="35%"></td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<midas:texto propiedadFormulario="fechaFinal"  id="fechaFinal" soloLectura="true"/>
			</td> 
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>&nbsp;</tr>
		<tr>
			<td align="right">
				<b><midas:mensaje clave="reaseguro.reporte.trimestralReaseguro.moneda"/>:</b>
			</td>
			<td>
				<midas:comboCatalogo propiedad="moneda" size="1" styleId="moneda" nombre="trimestralReaseguroForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="false"/>				
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td align="right">
				<b><midas:mensaje clave="reaseguro.reporte.trimestralReaseguro.tipoCambio"/></b>
			</td>
			<td>
				<midas:texto propiedadFormulario="tipoCambio" id="tipoCambio" longitud="30" nombreFormulario="trimestralReaseguroForm" onkeypress="return soloNumeros(this, event, true);"/>
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td>
				<div id="b_reporteXLS" style="width:140px;">
					<a href="javascript: void(0);" onclick="rptTrimestralReaseguro();"><midas:mensaje clave="reaseguro.reporte.generarreporte"/></a>
				</div>
			</td>
			<td></td>
		</tr>
	</table>
</midas:formulario>