<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 

<midas:formulario accion="/reaseguro/reportes/mostrarReporteSaldosPorTrimestre">
<html:hidden property="tipoReporte" name="reportesReaseguroForm" styleId="tipoReporte"/>
    <table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td class="titulo" colspan="4"><midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre"/></td>
     	</tr>
     	<tr height="15px"></tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.estadosdecuenta.tipofacultativo.reaseguradorcorredor"/>:</b></td>
			<td align="left"><midas:comboCatalogo propiedad="idPrioridadSiniestro" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" /></td>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.ejercicio"/>:</b></td>
			<td align="left"><midas:texto propiedadFormulario="idEjercicio" id="idEjercicio" onkeypress="return soloNumeros(this, event, false)" longitud="4" /></td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.suscripcion"/>:</b></td>
			<td align="left"><html:select property="idSuscripcion" name="reportesReaseguroForm" styleId="idSuscripcion" styleClass="cajaTexto" >
								<html:option value="1"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre1"/></html:option>
								<html:option value="2"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre2"/></html:option>
								<html:option value="3"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre3"/></html:option>
								<html:option value="4"><midas:mensaje clave="reaseguro.reporte.suscripcion.trimestre4"/></html:option>
							</html:select>  </td>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.moneda"/>:</b></td>
			<td align="left">
				<html:select property="moneda" name="reportesReaseguroForm" styleId="idMoneda" styleClass="cajaTexto">
				  	<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
					<html:optionsCollection name="reportesReaseguroForm"  property="listaMonedas" value="idTcMoneda" label="descripcion"/>
					<midas:opcionCombo valor="-1">Todas</midas:opcionCombo>
			 	</html:select>
			</td>
		</tr>
		<tr>
			<td colspan="4"><label for="incluirSaldosCuotaParte" > <html:checkbox property="incluirSaldosCuotaParte" styleId="incluirSaldosCuotaParte" >
				<midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre.incluirSaldosCuotaParte"/></html:checkbox> </label> </td>
		</tr>
		<tr>
			<td colspan="4"><label for="incluirSaldosPrimerExcedente" > <html:checkbox property="incluirSaldosPrimerExcedente" styleId="incluirSaldosPrimerExcedente">
				<midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre.incluirSaldosPrimerExcedente"/></html:checkbox> </label> </td>
		</tr>
		<tr>
			<td colspan="4"><label for="incluirSaldosFacultativo" > <html:checkbox property="incluirSaldosFacultativo" styleId="incluirSaldosFacultativo">
				<midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre.incluirSaldosFacultativo"/></html:checkbox> </label> </td>
		</tr>
		<tr>
			<td colspan="4"><label for="incluirSaldosEjerciciosAnteriores" > <html:checkbox property="incluirSaldosEjerciciosAnteriores" styleId="incluirSaldosEjerciciosAnteriores">
				<midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre.incluirSaldosEjerciciosAnteriores"/></html:checkbox> </label> </td>
		</tr>
		<tr>
			<td colspan="4"><label for="desglosarPorMes" > <html:checkbox property="desglosarPorMes" styleId="desglosarPorMes">
				<midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre.desglosarPorMes"/></html:checkbox> </label> </td>
		</tr>
<%--		<tr>--%>
<%--			<td colspan="4"><label for="desglosarPorConceptos" > <html:checkbox property="desglosarPorConceptos" styleId="desglosarPorConceptos">--%>
<%--				<midas:mensaje clave="reaseguro.reporte.saldosPorTrimestre.desglosarSaldoPorConceptos"/></html:checkbox> </label> </td>--%>
<%--		</tr>--%>
	</table>
	<div class="alinearBotonALaDerecha">
			<div id="b_reporteXLS">
				<a href="javascript: void(0);" onclick="javascript: generarReporteSaldosTrimestrales();">
				<midas:mensaje clave="midas.accion.reporteExcel"/>
				</a>
			</div>
		</div>
</midas:formulario>