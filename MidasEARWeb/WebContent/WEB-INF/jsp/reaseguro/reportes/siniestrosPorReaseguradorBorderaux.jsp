<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
 
<midas:formulario accion="/reaseguro/reportes/siniestrosReaseguradorBorderaux">
    <table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td class="titulo" colspan="4">
				<midas:mensaje clave="reaseguro.reporte.siniestrosReaseguradorBorderaux"/>
			</td>
     	</tr>
     	<tr height="15px"></tr>
		<tr>
			<td align="right" width="15%">
				<b><midas:mensaje clave="reaseguro.reporte.fecha.inicial"/>:</b>
			</td>
			<td align="left" width="20%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="10%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td width="55%"></td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly"/>
			</td> 
			<td width="55%" align="left" colspan="2">
				<div id="rangoDeFechas" style="position:absolute;z-index:1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right">
				<b><midas:mensaje clave="reaseguro.reporte.siniestrosReaseguradorBorderaux.moneda"/></b>
			</td>
			<td>
				<midas:comboCatalogo propiedad="moneda" size="1" styleId="moneda" nombre="siniestrosReaseguradorBorderauxForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" readonly="false"/>				
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="reaseguro.reporte.siniestrosReaseguradorBorderaux.tipo"/></b></td>
			<td>
				<midas:combo id="tipoParticipante" propiedad="tipoParticipante" styleClass="cajaTexto" 
					onchange="limpiarObjetos('idtcReaseguradorCorredor'); llenarCombo(this,'idtcReaseguradorCorredor','/MidasWeb/reaseguradorCorredorOrdenado.do');">	
					<midas:opcionCombo valor="">SELECCIONAR...</midas:opcionCombo>
					<midas:opcionCombo valor="0">Corredor</midas:opcionCombo>
					<midas:opcionCombo valor="1">Reasegurador</midas:opcionCombo>		
					<midas:opcionCombo valor="2">Todos</midas:opcionCombo>					
				</midas:combo>
			</td>
			<td colspan="2">
				<table width="100%" style="font-size:10px">
					<tr>
						<td width="15%" align="right">
							<b><midas:mensaje clave="reaseguro.reporte.siniestrosReaseguradorBorderaux.nombre"/>:</b>
						</td>
						<td width="85%">
						  	<div style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;">
						    	<midas:reaseguradorCorredor styleId="idtcReaseguradorCorredor" styleClass="cajaTexto" size="1" tipo="tipoParticipante" propiedad="idtcReaseguradorCorredor"/>
						   	</div>
			   			</td>
			   		</tr>
			   	</table>
			</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td>
				<div id="b_reportePDF" style="width:160px;">
					<a href="javascript: void(0);" onclick="rptSiniestrosPorReasegurador('PDF');"><midas:mensaje clave="reaseguro.reporte.generarreporte.pdf"/></a>
				</div>
			</td>
			<td>
				<div id="b_reporteXLS" style="width:160px;float:left">
					<a href="javascript: void(0);" onclick="rptSiniestrosPorReasegurador('XLS');"><midas:mensaje clave="reaseguro.reporte.generarreporte.xls"/></a>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>