<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
 
 <midas:formulario accion="/reaseguro/reportes/mostrarFiltroRptSiniestrosEventoCatastrofico">
    <table width="97%" style="right-margin:auto;left-margin:auto;font-size:10px" id="filtros">
   		 <tr>
	     	<td class="titulo" colspan="5">
				<midas:mensaje clave="reaseguro.reporte.siniestroEventoCatastrofico"/>
			</td>
     	</tr>
     	<br clear="all" />
    	<tr>
	    	<td align="right">
	    		<b><midas:mensaje clave="reaseguro.reporte.siniestroeventocatastrofico.eventoCatastrofico"/>:</b>
	    	</td>
	    	<td><select id="eventoCatastrofico" class="cajaTexto"></select></td>
    	</tr>
    	
		<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="reaseguro.reporte.fecha.corte"/>:</b></td>
			<td align="left" width="20%">
				<midas:texto propiedadFormulario="fechaCorte"  id="fechaCorte" soloLectura="true"/>				
			</td>
			<td width="10%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioSimpleGenerico()"></a>
				</div>
			</td>
			<td colspan="2" width="55%"></td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<td width="10%">
				<div id="calendarioSimpleGenericoDiv" style="position:absolute;z-index:1;">
				</div>
			</td>
			<td colspan="2" width="55%"></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td>
				<div id="b_reporteXLS" style="width:140px;">
					<a href="javascript: void(0);" onclick="rptSiniestrosEventoCatastrofico();"><midas:mensaje clave="reaseguro.reporte.generarreporte"/></a>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>