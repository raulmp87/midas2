<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
 
    <table width="98%" id="filtros" style="right-margin:auto;left-margin:auto;">	
   		<tr>
	     	<td class="titulo" colspan="5">
				<midas:mensaje clave="reaseguro.reporte.ingresosReaseguro"/>
			</td>

     	</tr>
		<tr>
			<td align="right" width="15%"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.inicial"/>:</b></td>
			<td align="left" width="18%">
				<input type="text" id="fechaInicial" readonly="readonly"/>
			</td>
			<td width="15%" align="left">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
			<td>
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td align="right"><b><midas:mensaje clave="contratos.movimiento.reporte.fecha.final"/>:</b></td>
			<td align="left">
				<input type="text" id="fechaFinal" readonly="readonly"/>
			</td> 
			<td colspan="2">
				<div id="b_reporteXLS" style="width:140px;">
					<a href="javascript: void(0);" onclick="rptIngresosReaseguro();"><midas:mensaje clave="contratos.movimiento.generarreporte.porfechas"/></a>
				</div>
			</td>
		</tr>
	</table>