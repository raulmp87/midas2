<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%> 
<midas:formulario accion="/reaseguro/egresos/administrarEgresos.do">

	<table width="97%" id="filtros">
		<tr>
			<td>
				<div class="titulo"><midas:mensaje clave="contratos.movimiento.reporte.soporteContratosPoliza"/></div>
			</td>
		</tr>
		<tr>
		  <td width="70%">
			 <table id="tablaConResultados" width="100%">
				  <tr>
				    <td>
				    	<div id="divBusquedaPolizas">
					    	<table width="98%">
					    		<tr>
					    			<td width="35%">
								    	<div style="font-family:Verdana,Arial,Helvetica,sans-serif; padding:3px; font-size:10px;">
								    		<b><midas:mensaje clave="contratos.egresos.administrar.nombreAsegurador"/>:</b>
								    		<input id="busquedaNomAsegurado" style="border:1px solid #A0E0A0; padding-left:3px;" type="text" onkeypress="return stopRKey(event)"/> 
								    	</div>
						    		</td>
						    		<td  width="35%" align="right">
												<div style="font-family: Verdana, Arial, Helvetica, sans-serif; padding: 3px; font-size: 10px;">
													<b><midas:mensaje clave="contratos.movimiento.reporte.movtosporreasegurador.numeropoliza" />:</b>
													<input id="busquedaNumPoliza"
														style="border: 1px solid #A0E0A0; padding-left: 3px;"
														type="text" onkeypress="return stopRKey(event);" onfocus="new Mask('####-########-##', 'string').attach(this)" />
												</div>
									</td>
							    	<td width="30%">
										<div id="b_regresar" style="width:120px;">
										     <a href="javascript: void(0);" onclick="javascript: mostrarIndicadorCargaGenerico('loadingPolizas');mostrarPolizaAdministrarReporteGridContrato($('busquedaNomAsegurado').value, $('busquedaNumPoliza').value)"><midas:mensaje clave="reaseguro.ingreso.administrar.ingresos.filtrarPoliza"/></a>
									    </div>
								    </td>
							    </tr>
						    </table>
					     </div>
				    </td>  
				 </tr>
			</table>
		  </td>
 	  	</tr>
	</table>
	<br clear="all" />
	<div id="loadingPolizas" style="display: none"></div>
	<center>
		<div id="polizaReporteMovtosPorReaseguradorGrid"
			style="width: 810px; height: 275px; overflow: auto;"></div>
	</center>
</midas:formulario>