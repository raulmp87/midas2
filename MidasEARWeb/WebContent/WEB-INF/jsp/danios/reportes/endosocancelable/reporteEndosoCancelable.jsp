<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<div id="detalle" name="Detalle">
	<midas:formulario accion="/danios/reportes/mostrarReporteEndososCancelables">
	<div class="subtituloIzquierdaDiv">
		Reporte de endosos cancelables
	</div>
	<center>
			<table id="filtros" width="100%">
				<tr>
					<th>
						<etiquetas:etiquetaError property="fechaCorte" requerido="si" key="midas.danios.reportes.pml.fechaCorte" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: parent.mostrarCalendarioOT();">
						<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
					</th>
					<td>
						<html:text styleId="fecha" property="fechaCorte" name="reportePmlForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><div id='fecha' style="position:absolute;z-index: 1;"></div></td>
				</tr>
				<tr  id="divGenerarReporte">
					<td colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_imprimir" style="width:150" >
								<a href="javascript: void(0);"
									onclick="javascript: generarReporteEndososCancelables();">
									Generar reporte
								</a>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<div id="errores" style="display: none;"><html:errors/></div>
		</midas:formulario>
	</center>
</div>