<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Carga Caducidades AMIS
	</div>
	<%
	
	String negocio = "";
	
	if(request.getAttribute("leyenda") != null)
		negocio = request.getAttribute("leyenda").toString();
	%>
	<center>
			<table id="filtros" width="100%">
				<tr>
					<th>Carga Archivo</th>
					<td>
						<midas:boton onclick="javascript: AdjuntarArchivoREASWindow('24');" tipo="agregar" texto="Cargar Caducidades"/>
					</td>
				</tr>
				<tr>
					<th>Anio Vigencia</th>
					<td>
						<div style="margin-top: 15px;">
							<input type="text" id="anioContrato" size="10"  class="cajaTexto"/>
						</div>     
					</td> 
				</tr>
				<tr>
					<td colspan="2">
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: eliminarContratos();" tipo="agregar" texto="Eliminar Contratos"/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="msgDocumentos">
					<%=negocio %>
					</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
			</table>
	</center>
</div>