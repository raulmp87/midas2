<%@ page isELIgnored="false"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<script type="text/javascript" src='<s:url value="/js/reaseguro/reaseguro.js"/>' ></script>

<style type="text/css">

div#resultadosDocumentos {
    border: 0 solid red;
    height: 200px;
    overflow: auto;
    width: 96%;
}
div#resultadosCargas {
    border: 0 solid red;
    height: 200px;
    overflow: auto;
    width: 96%;
}

.mensaje_contenido {
	height: 120px;
}

.mensaje_texto {
font-size: 11px;
font-weight: normal;
height: 100px;
}
</style>



<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
	<s:text name="danios.reporteRR6.titulo"/>
	</div>
	
	<center>
		<midas:formulario accion="/danios/reportes/reporterr6/mostrarReporteRR6">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Inicial</th>
				    <td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="reporteRR6Form" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioDobleBasesEmision()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td>
					<th>Fecha Final</th>
				    <td>
						<html:text styleId="fechaFinal" property="fechaFinal" name="reporteRR6Form" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
				</tr>
			    <tr>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>				
				<tr>
					<th>Reporte a generar</th>
					 <td>
					    <html:select property="reporte" styleId="reporte" onchange="interacCmbsRR6(this);deshabBotonRR6(this);" size="1" styleClass="cajaTexto">
					 		<html:option value="0">Seleccione el reporte...</html:option>
					 		<html:option value="1">Esquemas de Reaseguro - (RTRE)</html:option>
					 		<html:option value="2">Contratos Autom�ticos - (RTRC)</html:option>
					 		<html:option value="3">Operaciones Facultativas - (RTRF)</html:option>
					 		<html:option value="4">Reporte de Resultados - (RTRR)</html:option>
					 		<html:option value="5">Reporte de Siniestros - (RTRS)</html:option>
					 		<html:option value="6">Reaseguradores no Registrados - (RARN)</html:option>
					 		<html:option value="7">Reporte Intermediaci�n - (RTRI)</html:option>
					 		<html:option value="8">Reporte C�mulos de Fiados - (CUMF)</html:option>
					 	</html:select>
		     		</td>
		     	
		     		<th><div id="titulo" style="display:none">Tipo de Cambio</div></th>
				    <td>
						<html:text styleId="tipoCambio" property="tipoCambio" name="reporteRR6Form" value="0" size="10" styleClass="cajaTexto" disabled="true" style="display:none"/>
					</td>
				
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
					<div class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: actualizarEstatusCargaReporteRR6(document.reporteRR6Form);" style="width:70%" tipo="continuar" texto="Actualizar Estatus Cargas"/>
					</div>
					</td>
					<td>
						<div class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: actualizarEstatusReporteRR6(document.reporteRR6Form);" style="width:60%" tipo="continuar" texto="Actualizar Estatus Midas"/>
						</div>
					</td>
					<td>
					     <div id="procesar" class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: procesarReporteRR6(document.reporteRR6Form, 1, 1);" style="width:60%" tipo="continuar" texto="Iniciar Proceso"/>
						</div>					
					</td>
					<td>
						<div class="alinearBotonALaIzquierda">
							<midas:boton onclick="javascript: generarReporteRR6(document.reporteRR6Form, 1);" style="width:60%" tipo="continuar" texto="Generar Reporte"/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosCargas"></div>
					</td>
				</tr>				
			</table>
		</midas:formulario>
	</center>
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>