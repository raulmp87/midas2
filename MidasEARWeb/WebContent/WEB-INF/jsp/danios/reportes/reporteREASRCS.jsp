<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Reporte de REAS RCS
	</div>
	<%	
	String nomenclatura = request.getAttribute("nomenclatura").toString();
	
	%>
	<center>
		<midas:formulario accion="/danios/reportes/reporteRCS/generarReporteREASRCS">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Corte
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="reporteRCSForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th>N�mero de Cortes</th>
					<td>
						<html:text styleId="numeroCortes" property="numeroCortes" name="reporteRCSForm" size="10"  styleClass="cajaTexto"/>
					</td>

				</tr>
				<tr>
					<th>Nomenclatura nombre archivo</th>
					<td>
						<html:text styleId="nomenclatura" property="nomenclatura" name="reporteRCSForm" size="10"  styleClass="cajaTexto" value="<%=nomenclatura %>"/>
					</td>
					
				</tr>
				<tr>
					<td colspan="3">
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: imprimirReporteREASRCS(document.reporteREASRCSForm);" tipo="agregar" texto="Generar Reporte"/>
						</div>
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>