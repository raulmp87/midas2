<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<div id="detalle" name="Detalle">
	<midas:formulario accion="/danios/reportes/reportePML/generarReportePML">
	<div class="subtituloIzquierdaDiv">
		<bean:write name="reportePmlForm" property="tituloReporte" />
	</div>
	<center>
			<table id="filtros" width="100%">
				<tr>
					<th><etiquetas:etiquetaError property="claveTipoReporte" requerido="si" key="midas.danios.reportes.pml.claveTipoReporte" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" /></th>
					<td> <midas:comboValorFijo grupoValores="315" propiedad="claveTipoReporte" nombre="reportePmlForm" styleId="comboClaveTipoReporte" /> </td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="fechaCorte" requerido="si" key="midas.danios.reportes.pml.fechaCorte" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: parent.mostrarCalendarioOT();">
						<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
					</th>
					<td>
						<html:text styleId="fecha" property="fechaCorte" name="reportePmlForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
					<th>
						<etiquetas:etiquetaError property="tipoCambio" requerido="si" key="midas.danios.reportes.pml.tipoCambio" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>
					<td>
						<html:text styleId="tipoCambio" property="tipoCambio" name="reportePmlForm" size="17" styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><div id='fecha' style="position:absolute;z-index: 1;"></div></td>
					<td> <html:hidden property="tipoReporte" name="reportePmlForm" styleId="tipoReporte"/> </td>
					<td> <html:hidden property="tituloReporte" name="reportePmlForm" styleId="tituloReporte"/></td>
				</tr>
				<tr  id="divGenerarReporte">
					<td colspan="4">
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: mostrarImprimirReportePML(document.reportePmlForm);" tipo="agregar" texto="Generar Reporte"/>
						</div>
					</td>
				</tr>
				<tr id="divGenerarReporteEspecifico" style="display: none;" >
					<logic:equal value="terremoto" property="tipoReporte" name="reportePmlForm">
						<td>
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: imprimirReportePMLEspecifico('TEV_Ind');" tipo="agregar" texto="Reporte Independientes" style="width:180"/>
							</div>
						</td>
						<td>
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: imprimirReportePMLEspecifico('TEV_SemiAgr');" tipo="agregar" texto="Reporte SemiAgrupados" style="width:180"/>
							</div>
						</td>
						<td>
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: imprimirReportePMLEspecifico('TEV_Tec');" tipo="agregar" texto="Reporte Tecnicos" style="width:180"/>
							</div>
						</td>
					</logic:equal>
					<logic:equal value="hidro" property="tipoReporte" name="reportePmlForm">
						<td>
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: imprimirReportePMLEspecifico('HIDRO_Ind');" tipo="agregar" texto="Reporte Independientes" style="width:180"/>
							</div>
						</td>
						<td>
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: imprimirReportePMLEspecifico('HIDRO_SemiAgr');" tipo="agregar" texto="Reporte SemiAgrupados" style="width:180" />
							</div>
						</td>
						<td>
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: imprimirReportePMLEspecifico('HIDRO_Tec');" tipo="agregar" texto="Reporte Tecnicos" style="width:180"/>
							</div>
						</td>
					</logic:equal>
					<td></td>
				</tr>
			</table>
			<div id="errores" style="display: none;"><html:errors/></div>
		</midas:formulario>
	</center>
</div>