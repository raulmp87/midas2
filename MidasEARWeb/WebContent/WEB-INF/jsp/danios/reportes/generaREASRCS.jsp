<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

	<%
	String negocio = request.getAttribute("claveNegocio").toString();
	String negocioStr = "Autos";
	
	if(negocio.equals("2"))
		negocioStr = "Da�os";
	else if(negocio.equals("4"))
		negocioStr = "Vida";
	%>
<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Genera Info RCS <%= negocioStr %>
	</div>
	<center>
		<midas:formulario accion="/danios/reportes/reporteRCS/generarReporteREASRCS">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Corte
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="reporteRCSForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<!-- 
				<tr>
					<td colspan="1">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				 -->
				<tr>
					<td colspan="6">
						<div class="divInterno" style="float: left; position: relative; width: 100%; margin-top: 1%;height: 40px;">
							<div id="nuevo" class="btn_back w80" style="display: inline; float: left;margin-left: 8%;">
								<a href="javascript: void(0);" onclick="javascript: consultaProceso(document.reporteREASRCSForm);"> Consulta Log Proceso </a>
							</div>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
							<a href="javascript: void(0);" onclick="javascript: procesoRCS(document.reporteREASRCSForm,0);"> 
								Inicio Proceso RCS 
							</a>
							</div>
							<div class="btn_back w150" style="display: inline; margin-left: 2%; float: left;">
								<a href="javascript: void(0);" onclick="javascript: procesoRCS(document.reporteREASRCSForm,1);"> Reprocesar Corte </a>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="6">
					<div id="resultadosDocumentos"></div>
					<div id="msgDocumentos"></div>
					<input type="hidden" value="<%= negocio %>" id="negocio">
					</td>
					
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>