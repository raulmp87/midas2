<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>

<div id="resultados">
	<midas:tabla idTabla="solicitudes"
		claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalEsquemas"
		claseCss="tablaConResultados" nombreLista="documentos"
		urlAccion="/MidasWeb/danios/reporte/esquemas/listarDocumentos.do">
		<midas:columna propiedad="archivo" />
		<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
		<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
		<midas:columna propiedad="descripcion" titulo="Descripcion" />
		<midas:columna propiedad="acciones"/>
	</midas:tabla>

</div>