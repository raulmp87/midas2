<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div id="detalle" name="Detalle">
	<%
	
	String negocio = request.getAttribute("negocio").toString();
	String nomenclatura = request.getAttribute("nomenclatura").toString();
	String negocioStr = "Autos";
	
	if(negocio.equals("2"))
		negocioStr = "Da�os";
	else if(negocio.equals("4"))
		negocioStr = "Vida";
	else if(negocio.equals("5"))
		negocioStr = "Vigor Vida";
	%>
	<div class="subtituloIzquierdaDiv">
		Reporte de Insumos RCS <%= negocioStr %>
	</div>
	<center>
		<midas:formulario accion="/danios/reportes/reporteRCS/generarReporteRCS">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Corte
						
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="reporteRCSForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td> 
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th>Ramo</th>
					<td>
							<midas:ramorcs styleId="id_ramo" size="1" propiedad="id_ramo" styleClass="cajaTexto" claveNegocio="<%=negocio%>"	/>
					</td>
				</tr>
				<tr>
					<th>N�mero de Cortes</th>
					<td>
						<html:text styleId="numeroCortes" property="numeroCortes" name="reporteRCSForm" size="10"  styleClass="cajaTexto"/>
					</td>
					
				</tr>
				<tr>
					<th>Nomenclatura nombre archivo</th>
					<td>
						<html:text styleId="nomenclatura" property="nomenclatura" name="reporteRCSForm" size="10"  styleClass="cajaTexto" value="<%=nomenclatura %>"/>
					</td>
					 
				</tr>
				<tr>
					<td colspan="2">
						<div class="divInterno" style="float: left; position: relative; width: 100%; margin-top: 1%;height: 40px;">
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: right;">
								<a href="javascript: void(0);" onclick="javascript: imprimirReporteRCS(document.reporteRCSForm);"> 
									Generar Reporte 
								</a>
							</div>
							<% if(negocio.equals("10")) {  %>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: AdjuntarArchivoRCSWindow();"> 
									Cargar Archivo Rangos
								</a>
							</div>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: actualizaRangos(document.reporteRCSForm);"> 
									Actualizar Rangos
								</a>
							</div>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: eliminarRangos(document.reporteRCSForm);"> 
									Eliminar Rangos
								</a>
							</div>
							<% }  %>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						&nbsp;
						<input type="hidden" value="<%=negocio%>" id="cveNegocio">
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>