<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Reporte Monitoreo de Solicitudes
	</div>
	<center>
		<midas:formulario accion="/danios/reportes/mostrarReporteMonitoreoSolicitudes">
			<html:hidden property="codigoAgente" name="reporteMonitoreoSolicitudesForm" styleId="claveAgente"/>
				<html:hidden property="sinDatos" name="reporteMonitoreoSolicitudesForm" styleId="sinDatos"/>
			<table id="filtros" width="100%" border="0">
				<tr>
					<th>
						<etiquetas:etiquetaError property="fechaInicioStr" requerido="si" key="danios.reporte.monitoreosolicitudes.fechainicial" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
							
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioDobleBasesEmision()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicioStr" name="reporteMonitoreoSolicitudesForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
					<th><etiquetas:etiquetaError property="fechaFinalStr" requerido="si" key="danios.reporte.monitoreosolicitudes.fechafinal" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" /></th>
					<td>
						<html:text styleId="fechaFinal" property="fechaFinalStr" name="reporteMonitoreoSolicitudesForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th><etiquetas:etiquetaError property="idProductoStr" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="reporteMonitoreoSolicitudesForm" key="danios.reporte.monitoreosolicitudes.producto"/> </th>
					<td>
						<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProductoStr" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote" />
					</td>	
					<th><etiquetas:etiquetaError property="idOficina" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="reporteMonitoreoSolicitudesForm" key="danios.reporte.monitoreosolicitudes.ejecutivo"/> </th>
					<td>
						<html:select styleId="ejecutivos" property="idOficina" styleClass="cajaTexto" onchange="cargaAgentesPorEjecutivo(this,agentesFiltrado);" > 	
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="idOficina" property="listaEjecutivos" name="reporteMonitoreoSolicitudesForm" label="nombreOficina"/>
									
						</html:select>
					</td>
				</tr>
				<tr>
					<th><etiquetas:etiquetaError property="codigoAgenteStr" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="reporteMonitoreoSolicitudesForm" key="danios.reporte.monitoreosolicitudes.agente"/> </th>
					<td>
						<html:select  name="reporteMonitoreoSolicitudesForm" styleId="agentesFiltrado" property="codigoAgenteStr" styleClass="cajaTexto" > <html:option value="">SELECCIONE...</html:option>
									<html:optionsCollection value="idTcAgente" property="listaAgentes" name="reporteMonitoreoSolicitudesForm" label="nombre"/>
						</html:select>
					</td>
				
				<th><etiquetas:etiquetaError property="codigoUsuarioSolicitud" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="reporteMonitoreoSolicitudesForm" key="danios.reporte.monitoreosolicitudes.usuario"/> </th>
				<td>
						<html:select property="codigoUsuarioSolicitud" styleClass="cajaTexto" > <html:option value="">SELECCIONE...</html:option>
									<html:optionsCollection value="usuarionombre" property="listaUsuarios" name="reporteMonitoreoSolicitudesForm" label="usuarionombre"/>
						</html:select>
				</td>
				
				</tr>
				<tr>
				
				</tr>
				<tr>
				
				</tr>
				<tr>
					<td colspan="4">
						<div class="alinearBotonALaDerecha">
							<button type="button" onclick="javascript: generaReporteMonitoreoSol(document.reporteMonitoreoSolicitudesForm);" 
							style="cursor: pointer;">
								<table style="font-size: 9px;" align="center">
									<tr>
										<td align="center">	Generar	</td>	
									</tr>
									<tr>
										<td align="center">	Reporte	</td>
									</tr>
								</table>
					    	</button>
						</div>	
				  </td>
				</tr>
			</table>
			<div id="errores" style="display: none;"><html:errors/></div>
				
		</midas:formulario>
	
	</center>
</div>