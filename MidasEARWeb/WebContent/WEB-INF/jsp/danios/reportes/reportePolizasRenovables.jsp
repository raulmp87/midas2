<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario accion="/danios/reportes/mostrarRenovables">
	<nested:hidden property="polizasListadas" name="renovacionPolizaForm"/>
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<html:hidden property="esReporte" styleId="esReporte" value="true"/>
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				Reporte de P&oacute;lizas Renovables
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros">
		<tr>
			<th> 
				<midas:mensaje clave="poliza.renovacion.numeroPoliza" />
			</th>
			<td>
				<midas:texto propiedadFormulario="numeroPoliza"
					nombreFormulario="renovacionPolizaForm" caracteres="14" longitud="16"
					onfocus="new Mask('####-########-##', 'string').attach(this)" />
			</td>
			<th width="10%">
				<center>
					Fin <midas:mensaje clave="poliza.renovacion.vigencia" />
					<a href="javascript: void(0);" id="mostrarCalendario"
						onclick="mostrarCalendarioDobleLinea()"> <image
							src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /> </a>
				</center>
			</th>
			<th width="8%">
				<etiquetas:etiquetaError property="fechaInicioVigencia"
					requerido="si" key="poliza.renovacion.desde"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaInicial" property="fechaInicioVigencia"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />
			</td>
			<th>
				<etiquetas:etiquetaError property="fechaFinVigencia" requerido="si"
					key="poliza.renovacion.hasta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaFinal" property="fechaFinVigencia"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="poliza.renovacion.nombreAsegurado" />
			</th>
			<td colspan="4">
				<midas:texto propiedadFormulario="nombreAsegurado"
					nombreFormulario="renovacionPolizaForm" />
			</td>
			<td colspan="2">
				<div id="rangoDeFechas" style="position: absolute; z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="fechaFinal"></div>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="poliza.renovacion.producto" />
			</th>
			<td colspan="3">
				<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote" readonly="false" />			
			</td>	
			<th>
				<midas:mensaje clave="poliza.renovacion.esFacultativa" />
			</th>
			<td>
				<midas:check propiedadFormulario="esFacultativa" id="esFacultativa"/>
			</td>				
			<th>
				<div class="alinearBotonALaDerecha">
					<midas:boton
						onclick="javascript: sendRequest(document.renovacionPolizaForm, '/MidasWeb/danios/reportes/listarRenovables.do', 'contenido', 'manipulaCalendarioLineas();');"
						tipo="buscar" />				
				</div>
			</th>	
		</tr>
	</table>
	<br/>
	<div id="resultadosOverFlow">
		<display:table name="polizas" id="registros" 
			decorator="mx.com.afirme.midas.decoradores.Poliza"
			requestURI="/danios/reportes/listarRenovables.do"
			class="tablaConResultados" 
			export="true"
			pagesize="${polizasListadas}">
				<display:column property="cotizacionDTO.solicitudDTO.nombreOficina" title="Nombre Oficina" maxLength="15"/>
				<display:column property="cotizacionDTO.solicitudDTO.idOficina" title="Codigo Oficina" maxLength="10"/>
				<display:column property="cotizacionDTO.solicitudDTO.nombreAgente" title="Nombre Agente" maxLength="15"/>
				<display:column property="cotizacionDTO.solicitudDTO.codigoAgente" title="Codigo Agente" maxLength="10"/>
				<display:column property="cotizacionDTO.solicitudDTO.nombreOficinaAgente" title="Ejecutivo Venta" maxLength="15"/>
				<display:column property="nombreProducto" title="Producto" maxLength="35" />				
				<display:column property="numeroPolizaFormateada" title="N&uacute;mero de P&oacute;liza" maxLength="15"/>
				<display:column property="nombreAsegurado" title="Nombre del Asegurado" maxLength="35" />
				<display:column property="cotizacionDTO.idToPersonaAsegurado" title="# Asegurado" maxLength="10" />
				<display:column property="cotizacionDTO.fechaInicioVigencia" title="Inicio Vigencia" format="{0,date,dd/MM/yyyy}" maxLength="15" />
				<display:column property="cotizacionDTO.fechaFinVigencia" title="Fin Vigencia" format="{0,date,dd/MM/yyyy}" maxLength="15" />
				<display:column property="cotizacionDTO.diasPorDevengar" title="Dias de Vigencia" maxLength="5" />
				<display:column property="cotizacionDTO.primaNetaCotizacion" title="Prima Neta" format="$ {0,number,0,000.00}"/>
				<display:column property="esFacultativa" title="Facultativa" maxLength="5" />
				<c:choose>
					<c:when test="${registros.montoReclamado > 0}">
						<display:column value="SI" title="Siniestro" maxLength="5" />
					</c:when>
					<c:otherwise>
						<display:column value="NO" title="Siniestro" maxLength="5" />
					</c:otherwise>
				</c:choose>
				<display:column property="numeroSiniestro" title="No Siniestro" maxLength="5" />
				<display:column property="montoReclamado" title="Monto Reclamado" format="$ {0,number,0,000.00}"/>
				<c:choose>
					<c:when test="${registros.montoReclamado > 0}">
						<display:column value="${registros.cotizacionDTO.primaNetaCotizacion/registros.montoReclamado}"  format="{0,number,percent}" title="%Siniestralidad" />
					</c:when>
					<c:otherwise>
						<display:column format="{0,number,percent}" title="%Siniestralidad"/>
					</c:otherwise>
				</c:choose>						
				<display:column property="fechaCreacion" title="Emision" format="{0,date,dd/MM/yyyy}" maxLength="15" />
				<display:column property="claveEstatus" title="Estatus" maxLength="15"/>
				<display:column property="statusRenovacion" title="Estatus Renovación" maxLength="15"/>		
		</display:table>		
	</div>
</midas:formulario>