<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div id="detalle" name="Detalle">
	<%
	String negocioStr = "Da�os";
	
	String dp = request.getAttribute("durPro").toString();
	String dr = request.getAttribute("durRem").toString();
	
	%>
	<div class="subtituloIzquierdaDiv">
		Carga Rangos RCS <%= negocioStr %>
	</div>
	<center>
		<midas:formulario accion="/danios/reportes/reporteRCS/rangosPorDuracion">
			<table id="filtros" width="100%">
				<tr><th>&nbsp;</th></tr>
				<tr>
					<th>Rangos por Duracion</th>
				</tr>
				<tr>
					<th>Remanente</th>
					<td>
						<html:text styleId="numeroCortes" property="numeroCortes" name="reporteRCSForm" size="10"  styleClass="cajaTexto" value="<%= dp %>"/>
					</td>
				</tr>
				<tr>
					<th>Promedio</th>
					<td>
						<html:text styleId="nomenclatura" property="nomenclatura" name="reporteRCSForm" size="10"  styleClass="cajaTexto" value="<%= dr %>"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="divInterno" style="float: left; position: relative; width: 100%; margin-top: 1%;height: 40px;">
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: right; width: 150px">
								<a href="javascript: void(0);" onclick="javascript: rangosPorDuracion();"> 
									Aplicar Rangos por Duracion
								</a>
							</div>
						</div>
					</td>
				</tr>
				<tr><th>&nbsp;</th></tr>
				<tr><th>&nbsp;</th></tr>
				<tr>
					<th>Rangos por Ramo</th>
				</tr>
				<tr>
					<th>Fecha Corte
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="reporteRCSForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td> 
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th>Ramo</th>
					<td>
							<midas:ramorcs styleId="id_ramo" size="1" propiedad="id_ramo" styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="divInterno" style="float: left; position: relative; width: 100%; margin-top: 1%;height: 40px;">
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: consultaRangos();"> 
									Consulta Rangos
								</a>
							</div>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: AdjuntarArchivoRCSWindow();"> 
									Cargar Archivo Rangos
								</a>
							</div>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: actualizaRangos(document.reporteRCSForm);"> 
									Actualizar Rangos
								</a>
							</div>
							<div class="btn_back w100" style="display: inline;margin-left: 2%;float: left;">
								<a href="javascript: void(0);" onclick="javascript: eliminarRangos(document.reporteRCSForm);"> 
									Eliminar Rangos
								</a>
							</div>
						</div>
					</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>
<s:include value="/jsp/catalogos/mensajesHeader.jsp"></s:include>