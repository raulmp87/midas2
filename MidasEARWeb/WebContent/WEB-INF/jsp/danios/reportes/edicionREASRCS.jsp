<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page buffer = "16kb" %>
<midas:formulario  accion="/catalogos/esquemasreas/listar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>

	<table width="95%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.esquemasreas.listaEsquemas"/>
				<hr>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.esquemasreas.idContrato"/></th>
			<td><midas:texto propiedadFormulario="idContrato" onkeypress="return stopRKey(event)"/></td>
			<th><midas:mensaje clave="catalogos.esquemasreas.cer"/></th>
			<td><midas:texto propiedadFormulario="cer" onkeypress="return stopRKey(event)"/></td>
			<th><midas:mensaje clave="catalogos.esquemasreas.anio"/></th>
			<td><midas:texto propiedadFormulario="anio" onkeypress="return stopRKey(event)"/></td>
		</tr> 
		
		<tr>
			<td colspan="5">&nbsp;</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.esquemasReasForm,'/MidasWeb/catalogos/esquemasreas/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.contacto.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
	</table>
	<div id="mensajeResultadoOperacion" class="notificacionResultadoOperacion" style="display:none;margin-top:15px;margin-bottom:15px;width:95%"></div>
	<br clear="all" />
	<div id="resultados" style="width:95%">
		<midas:tabla idTabla="contactoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.EsquemasReas"
			claseCss="tablaConResultados" nombreLista="contactos"
			urlAccion="/catalogos/esquemasreas/listarFiltrado.do">
			<midas:columna propiedad="idContrato" titulo="Id Contrato" />
			<midas:columna propiedad="cer" titulo="CER"/>
			<midas:columna propiedad="nivel" titulo="Nivel"/>
			<midas:columna propiedad="ordenEntrada" titulo="Orden de Entrada"/>
			<midas:columna propiedad="reinstalaciones" titulo="Reinstalaciones" />
			<midas:columna propiedad="acciones"/>
	
		</midas:tabla> 
		
		<!-- 		
		<div class="alinearBotonALaDerecha">
			<div id="b_agregar">
				<a href="javascript: void(0);"
					onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/esquemasreas/mostrarAgregar.do', 'contenido',null);">
					<midas:mensaje clave="midas.accion.agregar"/>
				</a>
			</div>
		</div>
		 -->
	</div>
	<midas:mensajeUsuario/>
 
</midas:formulario>
