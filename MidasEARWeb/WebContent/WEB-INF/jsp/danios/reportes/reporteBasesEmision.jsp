<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Reporte de Bases de Emisi&oacute;n
	</div>
	<center>
		<midas:formulario accion="/danios/reportes/reporteBasesEmision/generarReporteMovimientoEmision">
			<table id="filtros" width="100%">
				<tr>
					<th>Fecha Inicial
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioDobleBasesEmision()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</th>
					<td>
						<html:text styleId="fechaInicial" property="fechaInicio" name="reporteBasesEmisionForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
					<th>Fecha Final</th>
					<td>
						<html:text styleId="fechaFinal" property="fechaFin" name="reporteBasesEmisionForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th>Ramo</th>
					<td>
						<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto" onchange="getSubRamos(this,'idTcSubRamo');" claveNegocio="D"	/>
					</td>
					<th>
						SubRamo
					</th>
					<td>
						<midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto"/>
					</td>
				</tr>
				<tr>
					<th>Nivel de Contrato de Reaseguro</th>
					<td>
						<midas:escribeCatalogo styleId="idTcTipoReaseguro" size="1" propiedad="idTcTipoReaseguro" styleClass="cajaTexto"
							clase="mx.com.afirme.midas.contratos.movimiento.TipoReaseguroFacadeRemote" />	
					</td>
					<th>Nivel de Agrupamiento para los movimientos</th>
					<td>
						<select id="nivelAgrupamiento" class="cajaTexto" size="1">
							<option value="0">Nivel de Contrato de Reaseguro</option>
							<option value="1">Endoso</option>
							<option value="2">P&oacute;liza</option>
							<option value="3">SubRamo</option>
							<option value="4">Ramo</option>
							<option value="5">Oficina de Emisi&oacute;n</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: imprimirReporteBasesEmision(document.reporteBasesEmisionForm);" tipo="agregar" texto="Generar Reporte"/>
						</div>
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>