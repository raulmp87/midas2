<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario  accion="/cotizacion/endoso/listar">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="cotizacionForm" property="totalRegistros"/>

	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar"/> Cotizaci&oacute;n de Endosos
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros">
		<tr>
			<th>
				N&uacute;mero de cotizaci&oacute;n:
			</th>
			<td>
				<midas:texto propiedadFormulario="idToCotizacion"
					nombreFormulario="cotizacionForm"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<th>
				<midas:mensaje clave="midas.cotizacion.estatus" />
				:
			</th>
			<td>
				<midas:comboValorFijo grupoValores="31" propiedad="claveEstatus"
					styleClass="cajaTexto" nombre="cotizacionForm" />
			</td>
		</tr>
		<tr>
			<th>
				Nombre del asegurado:
			</th>
			<td>
				<midas:texto propiedadFormulario="nombreSolicitante"
					nombreFormulario="cotizacionForm" />
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<th>
				<midas:boton
					onclick="javascript: sendRequest(document.cotizacionForm, '/MidasWeb/cotizacion/endoso/listarCotizacionesFiltrado.do', 'contenido', null);"
					tipo="buscar" />
			</th>
		</tr>
	</table>
	<br />

	<div id="resultados">
		<midas:tabla idTabla="cotizacionesTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Endoso"
			claseCss="tablaConResultados" nombreLista="listaCotizacion"
			urlAccion="/cotizacion/endoso/listarFiltradoPaginado.do"  exportar="false"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="cotizacion" />
			<midas:columna propiedad="fechaCreacion" />
			<midas:columna propiedad="cliente" maxCaracteres="20" />
			<midas:columna propiedad="tipoEndoso" />
			<midas:columna propiedad="tipoPoliza" maxCaracteres="15"/>
			<midas:columna propiedad="estatus" maxCaracteres="15"/>	
			<midas:columna propiedad="acciones" />
		</midas:tabla>
	</div>
</midas:formulario>
