<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/cotizacion/endoso/inciso/copiar">
	<midas:oculto propiedadFormulario="idCotizacion" nombreFormulario="incisoCotizacionForm"/>
	<midas:oculto propiedadFormulario="numeroInciso" nombreFormulario="incisoCotizacionForm"/>
	<html:hidden property="claveTipoEndoso" name="incisoCotizacionForm" styleId="claveTipoEndoso"/>
	<table id="agregar">
	<tr>
		<th>N&uacute;mero de copias: </th>
		<td>
			<midas:texto propiedadFormulario="numeroCopias" onkeypress="return soloNumeros(this,event,false);" caracteres="3" nombreFormulario="incisoCotizacionForm"/>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="botonAgregar">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: parent.dhxWins.window('copiarInciso').close();" tipo="regresar"/>
					<midas:boton onclick="javascript: if(document.incisoCotizacionForm.numeroCopias.value == '') {alert('Debe introducir un n&uacute;mero'); } else {parent.cerrarVentanaCopiarIncisoEndoso(document.incisoCotizacionForm);}" tipo="continuar" texto="Copiar"/>
				</div>
			</div>
		</td>
	</tr>
</table>
</midas:formulario>
