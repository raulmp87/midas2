<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<div class="subtituloCotizacion"><bean:message key="configuracion.cobertura.de"/>:<bean:write name="tituloCoberturas" scope="request" /> </div>
<div class="subtituloCotizacion">
	<bean:message key="configuracion.cobertura.cotizacion"/>: <bean:message key="midas.cotizacion.prefijo"/><bean:write name="idToCotizacionS" scope="request" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<bean:message key="configuracion.cobertura.fecha"/>: <bean:write name="fecha" scope="request" />
</div>
<div style="clear:both"></div>
<input type="hidden" name="idToCotizacion" value='<bean:write name="idToCotizacion" scope="request" />' id="idToCotizacion" />
<input type="hidden" name="numeroInciso" value='<bean:write name="numeroInciso" scope="request" />' id="numeroInciso" />
<input type="hidden" name="idToSeccion" value='<bean:write name="idToSeccion" scope="request" />' id="idToSeccion" />

<table id="desplegarDetalle">
	<tr>
		<td width="100%" colspan="3">
			<div id="cotizacionCoberturasGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
	<tr>
		<td width="80%">&nbsp;</td>
		<td width="10%">
			<midas:boton onclick="javascript: mostrarAutorizacionesCobertura();" tipo="regresar" texto="Autorizaciones"/>
		</td>		
		<td width="10%">
			<midas:boton onclick="javascript: coberturaProcessor.sendData();" tipo="guardar" texto="Guardar"/>
		</td>
	</tr>
</table>


