<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<midas:formulario accion="/cotizacion/mostrarModificar">
	<html:hidden property="idToCotizacion" name="cotizacionForm"/>
	<html:hidden property="claveTipoEndoso" name="cotizacionForm"/>
	<table id="desplegarDetalle" width="600px">
		<logic:equal value="3" property="claveTipoEndoso" name="cotizacionForm">
			<tr>
				<td colspan="3"></td>
				<td class="modificar">
				<div class="alinearBotonALaDerecha">
					<div id="b_asignar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/endoso/mostrarODTCotizacion.do?esCotizacion=1&editaDatoGeneral=true&id=<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>', 'contenido_datosGenerales','inicializaObjetosEdicionOT(<bean:write name="cotizacionForm" property="idToCotizacion" />)')" ><midas:mensaje clave="midas.accion.modificar"/></a>
					</div>
				</div>
				</td>
			</tr>			
		</logic:equal>
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="4">
				Datos de la Solicitud
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.ordendetrabajo.nombreSolicitante"/>:</th>
			<td> <midas:escribe propiedad="nombreSolicitante" nombre="cotizacionForm"/><html:hidden property="nombreSolicitante"/></td>
			<th><midas:mensaje clave="midas.ordendetrabajo.telefonoContacto"/>:</th>
			<td><midas:escribe propiedad="telefonoContacto" nombre="cotizacionForm"/><html:hidden property="telefonoContacto"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.ordendetrabajo.productoPorContratar"/>:</th>
			<td><midas:escribe propiedad="producto" nombre="cotizacionForm"/><html:hidden property="producto"/></td>
			<th><midas:mensaje clave="midas.ordendetrabajo.oficinaVenta"/>:</th>
			<td> <midas:escribe propiedad="oficina" nombre="cotizacionForm"/><html:hidden property="oficina"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.ordendetrabajo.agenteSeguro"/>:</th>
			<td><midas:escribe propiedad="nombreAgente" nombre="cotizacionForm"/><html:hidden property="nombreAgente"/></td>
			<th></th><td></td>
		</tr>
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="4">
				<midas:mensaje clave="midas.cotizacion.datosCotizacion"/>
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="midas.cotizacion.producto" />:
			</th>
			<td>
				<midas:escribe propiedad="producto" nombre="cotizacionForm"/>
			</td>
			<th>
				<midas:mensaje clave="midas.cotizacion.moneda" />:
			</th>
			<td>
				<midas:escribe propiedad="moneda" nombre="cotizacionForm"/>
			</td>
		</tr>
		<tr>
			<th>
			<midas:mensaje clave="midas.cotizacion.tipoPoliza" />:
			</th>
			<td>
				<midas:escribe propiedad="tipoDePoliza" nombre="cotizacionForm"/>
			</td>
			<th>
			<midas:mensaje clave="midas.cotizacion.vigencia" />:
			</th>
			<td>
				<midas:escribe propiedad="fechaInicioVigencia" nombre="cotizacionForm"/> - <midas:escribe propiedad="fechaFinVigencia" nombre="cotizacionForm"/> 
			</td>
		</tr>
		<tr>
			<th>
			<midas:mensaje clave="midas.cotizacion.formaDePago" />:
			</th>
			<td>
				<midas:escribe propiedad="formaPago" nombre="cotizacionForm"/>
			</td>
			<th>
			<midas:mensaje clave="midas.cotizacion.oficina" />:
			</th>
			<td>
				<midas:escribe propiedad="oficina" nombre="cotizacionForm"/>
			</td>
		</tr>
<!--		<tr>-->
<!--			<th>-->
<!--			<midas:mensaje clave="midas.cotizacion.medioDePago" />:-->
<!--			</th>-->
<!--			<td>-->
<!--				<midas:escribe propiedad="medioDePago" nombre="cotizacionForm"/>-->
<!--			</td>-->
<!--			<td colspan="2">-->
<!--			</td>-->
<!--		</tr>-->
	</table>
	<center>
		<div id="accordionCotizacion" style="position: relative; height: 250px; width: 97%;"></div>
	</center>
</midas:formulario>
