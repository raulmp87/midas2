<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<input type='hidden' id='idCotizacion' value='<bean:write name="idCotizacion" scope="request" />' name=''/>
<div class="subtituloCotizacion">Riesgos de <bean:write name="nombreCobertura" scope="request" /> </div>
<div class="subtituloCotizacion">
<bean:message key="configuracion.cobertura.cotizacion"/>: <bean:message key="midas.cotizacion.prefijo"/><%= String.format("%08d", new Object[]{request.getAttribute("idCotizacion")}) %>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<bean:message key="configuracion.cobertura.fecha"/>: <%= java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT).format((java.util.Date)request.getAttribute("fecha")) %>
</div>
<div style="clear:both"></div>
<table id="desplegarDetalle">
	<tr>
		<td colspan="3">
			<div id="autorizacionCotizacionRiesgosGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
	<tr>
		<td colspan="3">
				<div class="alinearBotonALaDerecha">				
					<div id="botonAgregar">
						<midas:boton onclick="javascript: riesgoAutorizadoProcessor.sendData();" tipo="agregar" texto="Autorizar"/>
						<midas:boton onclick="javascript: riesgoRechazadoProcessor.sendData();" tipo="agregar" texto="Rechazar"/>
						<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id=' + document.getElementById('idCotizacion').value, 'contenido','creaArbolCotizacion(' + document.getElementById('idCotizacion').value +');dhx_init_tabbars();');" tipo="regresar"/>
					</div>
				</div>
		</td>
	</tr>
</table>
