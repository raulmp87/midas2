<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<midas:formulario accion="/cotizacion/endoso/guardarEndoso">
	<div class="subtituloIzquierdaDiv">Datos del Endoso</div>
	<html:hidden property="claveTipoEndoso" name="cotizacionEndosoForm"/>
	<html:hidden property="idToCotizacion" name="cotizacionEndosoForm"/>
	<!-- Endoso de Cancelacion -->
	<logic:equal value="1" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<table id="desplegarDetalle">
			<tr>
				<th width="25%">
					P&oacute;liza a endosar:
				</th>
				<td width="20%">
					<bean:write name="cotizacionEndosoForm" property="numeroPolizaFormateada"/>
				</td>
				<th width="25%">
					Vigencia:
				</th>
				<td width="30%">
					<bean:write name="cotizacionEndosoForm" property="fechaInicioVigencia"/> - <bean:write name="cotizacionEndosoForm" property="fechaFinVigencia"/> 
				</td>
			</tr>
			<tr>
				<th width="25%">
					<midas:mensaje clave="midas.ordendetrabajo.vigenciaFechaInicial" /> de Vigencia del Endoso:
				</th>
				<td width="20%">
					<html:text styleId="fechaInicioVigenciaEndoso" 
							property="fechaInicioVigenciaEndoso" 
							name="cotizacionEndosoForm" size="10"
							disabled="true" 
							styleClass="cajaTexto"/>
					<html:hidden property="fechaInicioVigenciaEndoso" name="cotizacionEndosoForm"/>
				</td>
				<th width="25%">
					Motivo de Cancelaci&oacute;n:
				</th>
				<td width="30%">
					<midas:comboValorFijo grupoValores="42" propiedad="claveMotivo"   
					  nombre="cotizacionEndosoForm"  styleClass="cajaTexto" valoresExcluidos="7"/>
				</td>
			</tr>				
			<tr>
				<c:choose>
					<c:when test="${cotizacionEndosoForm.claveEstatus == '10' && cotizacionEndosoForm.claveMotivo != null}">
						<html:hidden property="mensaje" styleId="mensaje"/>
						<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
						<td colspan="2" width="50%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'distribuyeReaseguroEndoso(document.cotizacionEndosoForm.idToCotizacion.value);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');" tipo="continuar" texto="Guardar"/>
							</div>
						</td>
						<td colspan="2" width="50%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/resumen/liberarCotizacion.do?id=' + document.cotizacionEndosoForm.idToCotizacion.value, 'contenido','mostrarVentanaMensajeValidar(); listarCotizacionesEndoso();');" tipo="agregar" texto="Liberar"/>
							</div>
						</td>																								
					</c:when>
					<c:otherwise>
						<td colspan="4" width="100%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'distribuyeReaseguroEndoso(document.cotizacionEndosoForm.idToCotizacion.value);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');" tipo="continuar" texto="Guardar"/>
							</div>
						</td>								
					</c:otherwise>
				</c:choose>			
			</tr>
		</table>
		<div class="subtituloIzquierdaDiv">Totales de Cotizaci&oacute;n</div>
		<table id="desplegarDetalle" border="0">
			<tr>
				<td width="50%">&nbsp;</td>
				<td width="50%">
					<table id="t_riesgo" border="0">
						<tr>
							<th width="50%">Prima Neta Cotizaci&oacute;n</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="primaNetaCotizacion"/></td>
						</tr>			
						<tr>
							<th width="50%">Recargo Financiero</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="montoRecargoPagoFraccionado"/></td>
						</tr>
						<tr>
							<th width="50%">Gastos de Expedici&oacute;n</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="derechosPoliza"/></td>
						</tr>		
						<tr>
							<th width="50%">I.V.A a la tasa del <bean:write name="cotizacionEndosoForm" property="factorIVA"/></th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="montoIVA"/></td>
						</tr>	
						<tr>
							<th width="50%">Prima Total</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="primaNetaTotal"/></td>
						</tr>	
					</table>
				</td>
			</tr>
		</table>
		
	</logic:equal>
	<!-- Endoso de Rehabilitacion -->
	<logic:equal value="2" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<table id="desplegarDetalle">
			<tr>
				<tr>
					<th width="25%">
						P&oacute;liza a endosar:
					</th>
					<td width="20%">
						<bean:write name="cotizacionEndosoForm" property="numeroPolizaFormateada"/>
					</td>
					<th width="25%">
						Vigencia:
					</th>
					<td width="30%">
						<bean:write name="cotizacionEndosoForm" property="fechaInicioVigencia"/> - <bean:write name="cotizacionEndosoForm" property="fechaFinVigencia"/> 
					</td>
				</tr>
				<tr>
					<th width="25%">
						<midas:mensaje clave="midas.ordendetrabajo.vigenciaFechaInicial" /> de Vigencia del Endoso:
				</th>
				<td width="20%">
					<html:text styleId="fechaInicioVigenciaEndoso" 
							property="fechaInicioVigenciaEndoso" 
							name="cotizacionEndosoForm" size="10"
							disabled="true" 
							styleClass="cajaTexto"/>
					<html:hidden property="fechaFinVigencia" name="cotizacionEndosoForm"/>
				</td>
				<th width="25%">
					Motivo de Rehabilitaci&oacute;n:
				</th>
				<td width="30%">
					<midas:comboValorFijo grupoValores="43" propiedad="claveMotivo"   
					  nombre="cotizacionEndosoForm" onchange="validarTipoMovimiento(this.value);" styleClass="cajaTexto"/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="datosFinVigencia" style="display: none;">
						<table id="desplegarDetalle">
							<tr>
								<th width="25%">
									Fecha Fin de Vigencia:
												<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
													<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>						
								</th>
								<td width="20%">
					   				<html:text	property="fechaFinVigencia"  maxlength="10" size="15"
					   				 styleId="fecha" name="cotizacionEndosoForm"  styleClass="cajaTexto"
									 onkeypress="return soloFecha(this, event, false);"
									 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>					
								</td>	
								<th width="25%">&nbsp;</th>
								<td width="30%">&nbsp;</td>								
							
							</tr>
						</table>
					</div>
				</td>
			</tr>			
			<tr>
				<c:choose>
					<c:when test="${cotizacionEndosoForm.claveEstatus == '10' && cotizacionEndosoForm.claveMotivo != null}">
						<html:hidden property="mensaje" styleId="mensaje"/>
						<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>					
						<td colspan="2" width="50%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'distribuyeReaseguroEndoso(document.cotizacionEndosoForm.idToCotizacion.value);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');" tipo="continuar" texto="Guardar"/>
							</div>
						</td>
						<td colspan="2" width="50%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="liberarCotizacionEndoso();" tipo="continuar" texto="Liberar"/>
							</div>
						</td>																								
					</c:when>
					<c:otherwise>
						<td colspan="4" width="100%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value)');" tipo="continuar" texto="Guardar"/>
							</div>
						</td>								
					</c:otherwise>
				</c:choose>
			</tr>			
		</table>
	</logic:equal>
	<!-- Endoso de Modificacion -->
	<logic:equal value="3" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
		<table id="desplegarDetalle">
			<tr>
				<tr>
					<th width="25%">
						P&oacute;liza a endosar:
					</th>
					<td width="20%">
						<bean:write name="cotizacionEndosoForm" property="numeroPolizaFormateada"/>
					</td>
					<th width="25%">
						Vigencia:
					</th>
					<td width="30%">
						<bean:write name="cotizacionEndosoForm" property="fechaInicioVigencia"/> - <bean:write name="cotizacionEndosoForm" property="fechaFinVigencia"/> 
					</td>
				</tr>
				<tr>
					<th width="25%">
						<midas:mensaje clave="midas.ordendetrabajo.vigenciaFechaInicial" /> de Vigencia del Endoso:
				</th>
				<td width="20%">
					<html:text styleId="fechaInicioVigenciaEndoso" 
							property="fechaInicioVigenciaEndoso" 
							name="cotizacionEndosoForm" size="10"
							disabled="true" 
							styleClass="cajaTexto"/>
					<html:hidden property="fechaFinVigencia" name="cotizacionEndosoForm"/>							
				</td>
				<th width="25%">
					Motivo del Cambio:
				</th>
				<td width="30%">
					<midas:comboValorFijo grupoValores="43" propiedad="claveMotivo"   
					  nombre="cotizacionEndosoForm" onchange="" styleClass="cajaTexto"/>
				</td>			
			</tr>
			<tr>
				<td colspan="2" width="50%">&nbsp;</td>
				<td colspan="2" width="50%">
					<div id="botonAgregar">
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'procesarRespuesta(null);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');" tipo="continuar" texto="Guardar"/>
						</div>
					</div>						
				</td>	
			</tr>			
		</table>
		<div class="subtituloIzquierdaDiv">Totales del Endoso</div>
		<table id="desplegarDetalle" border="0" width="100%">
			<tr>
				<td colspan="2">
					<table id="t_riesgo" border="0" width="100%">
						<tr>
							<th></th>
							<th colspan="3">P�liza Origen + Endosos Emitidos Anteriores</th>
							<th colspan="2">Endoso</th>
							<th colspan="3">Movimiento a quedar</th>
						</tr>
						<tr>
							<th></th>
							<th>Suma Asegurada</th>
							<th>Cuota</th>
							<th>Prima Neta</th>
							<th>Suma Asegurada</th>
							<th>Prima Neta</th>
							<th>Suma Asegurada</th>
							<th>Cuota</th>
							<th>Prima Neta</th>
						</tr>
						<nested:iterate id="nextDiferencia" name="cotizacionEndosoForm" property="diferenciaCotizacionEndosoDTOs" indexId="index">
							<tr style="<c:out value="${nextDiferencia.rowStyle}" />">
								<td style="padding-left: <c:out value="${nextDiferencia.padding}" />"><bean:write name="nextDiferencia" property="tituloPrimeraColumna"/></td>
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="originalSumaAsegurada" format="$#,##0.00"/></td>
								<c:set var="cuotaOriginal" value="${nextDiferencia.originalCuota * 1000}"></c:set>
								<c:choose>
									<c:when test="${cuotaOriginal != 0}">
										<td style="font-size: 9px;"><bean:write name="cuotaOriginal" format="##0.0000"/></td>
									</c:when>
									<c:otherwise><td style="font-size: 9px;"></td></c:otherwise>
								</c:choose>
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="originalPrimaNeta" format="$#,##0.00"/></td>
								
								<td style="border-left: 1px solid #B2DBB2;"><bean:write name="nextDiferencia" property="diferenciaSumaAsegurada" format="$#,##0.00"/>&nbsp;</td>
								<td style="border-right: 1px solid #B2DBB2;"><bean:write name="nextDiferencia" property="diferenciaPrimaNeta" format="$#,##0.00"/>&nbsp;</td>
								
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="nuevaSumaAsegurada" format="$#,##0.00"/></td>
								<c:set var="nuevaCuota" value="${nextDiferencia.nuevaCuota * 1000}"></c:set>
								<c:choose>
									<c:when test="${not empty nextDiferencia.tieneCuotasDiferentes}">
											<c:choose>
												<c:when test="${nextDiferencia.originalCuota != 0}">
													<td style="font-size: 9px; font-weight: bold;">
													<a href="#" onclick="parent.mostrarCuotasEndoso(document.cotizacionEndosoForm.idToCotizacion.value,${nextDiferencia.idToSeccion},${nextDiferencia.idToCobertura});">
												</c:when>
												<c:otherwise>
													<td style="color: red; font-size: 9px;">
												</c:otherwise>
											</c:choose>										
									</c:when>
									<c:otherwise>
										<td style="font-size: 9px;">
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${nuevaCuota != 0}">
										<bean:write name="nuevaCuota" format="##0.0000"/>
										<c:choose>
											<c:when test="${not empty nextDiferencia.tieneCuotasDiferentes && nextDiferencia.originalCuota != 0}">
												</a>
											</c:when>
											<c:otherwise></c:otherwise>
										</c:choose>
										</td>
									</c:when>
									<c:otherwise></td></c:otherwise>
								</c:choose>
								<td style="font-size: 9px;">
								<bean:write name="nextDiferencia" property="nuevaPrimaNeta" format="$#,##0.00"/></td>
							</tr>
						</nested:iterate>
						<tr style="font-size: 9px;">
							<td colspan="4" rowspan="7">
								<table id="desplegarDetalle">
								<tr>
									<th colspan = "3">
										Recargo financiero&nbsp;&nbsp;&nbsp;									
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionRPF" value="0">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/blank.gif" alt="" title="" height="12px" width="12px" />
										</logic:equal> 							
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionRPF" value="1">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/ico_yel.gif" alt="Requiere autorizaci&oacute;n" title="Requiere autorizaci&oacute;n"  height="12px" width="12px" />
										</logic:equal>
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionRPF" value="7">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/ico_green.gif" alt="Autorizado" title="Autorizado" height="12px" width="12px" />
										</logic:equal>
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionRPF" value="8">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/ico_red.gif" alt="Rechazado" title="Rechazado" height="12px" width="12px" />
										</logic:equal>									
									</th>								
								</tr>
								<tr>
									<td colspan="3"	>
										<midas:radio valorEstablecido="0" propiedadFormulario="tipoCalculoRPF">Autom&aacute;tico</midas:radio>
									</td>
								</tr>
								<tr>
									<td width="40%">														
										<midas:radio valorEstablecido="1" propiedadFormulario="tipoCalculoRPF">Manual ($)</midas:radio>
									</td>
									<td>
										<midas:texto id="valorRPFEditable" propiedadFormulario="valorRPFEditable" onkeypress="return soloNumeros(this, event, true, true);"/>
									</td>
									<td>
										<midas:texto id="porcentajeRPFSoloLectura" propiedadFormulario="porcentajeRPFSoloLectura" soloLectura="true" deshabilitado="true"/>
									</td>
								</tr>
								<tr>
									<td>														
										<midas:radio valorEstablecido="2" propiedadFormulario="tipoCalculoRPF">Manual (%)</midas:radio>
									</td>
									<td>
										<midas:texto id="porcentajeRPFEditable" propiedadFormulario="porcentajeRPFEditable" onkeypress="return soloNumeros(this, event, true, true);"/>
									</td>
									<td>
										<midas:texto id="valorRPFSoloLectura" propiedadFormulario="valorRPFSoloLectura" soloLectura="true" deshabilitado="true"/>
									</td>								
								</tr>
								<tr>
									<td colspan = "3"> </td>
								</tr>
								<tr>
									<th colspan = "3">
										Gastos de expedici&oacute;n&nbsp;&nbsp;&nbsp;
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionDerechos" value="0">
											<img  id="imgAutorizacionDerechos" src="/MidasWeb/img/blank.gif" alt="" title="" height="12px" width="12px"/>										
										</logic:equal>									
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionDerechos" value="1">
											<img  id="imgAutorizacionDerechos"src="/MidasWeb/img/ico_yel.gif" alt="Requiere autorizaci&oacute;n" title="Requiere autorizaci&oacute;n" height="12px" width="12px" />										
										</logic:equal>
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionDerechos" value="7">
											<img id="imgAutorizacionDerechos" src="/MidasWeb/img/ico_green.gif" alt="Autorizado" title="Autorizado" height="12px" width="12px" />
										</logic:equal>
										<logic:equal name="cotizacionEndosoForm" property="claveAutorizacionDerechos" value="8">
											<img id="imgAutorizacionDerechos" src="/MidasWeb/img/ico_red.gif" alt="Rechazado" title="Rechazado" height="12px" width="12px" />
										</logic:equal>		
									</th>
								</tr>
								<tr>
									<td colspan="3">
										<midas:radio valorEstablecido="0" propiedadFormulario="tipoCalculoDerechos">Autom&aacute;tico</midas:radio>
									</td>
								</tr>
								<tr>
									<td>														
										<midas:radio valorEstablecido="1" propiedadFormulario="tipoCalculoDerechos">Manual ($)</midas:radio>
									</td>
									<td>
										<midas:texto id="valorDerechos" propiedadFormulario="valorDerechos" onkeypress="return soloNumeros(this, event, true, true);"/>
									</td>								
								</tr>
								<tr>	
									<td colspan="3">
										<div class="alinearBotonALaDerecha">				
											<div id="botonGuardar">
									    		<midas:boton style="width:100px" onclick="javascript: modificarRPFDerechosEndoso(document.cotizacionEndosoForm);" tipo="guardar" texto="Guardar"/>
											</div>
										</div>
									</td>
								</tr>								
							</table>
							</td>
							<th style="text-align: right;">D�as</th>
							<td><bean:write name="cotizacionEndosoForm" property="diasPorDevengar"/></td>
							<td colspan="3"></td>
						</tr>													
						<tr style="font-size: 9px;">												
							<th style="text-align: right;">Prima Neta Anual</th>
							<td><bean:write name="cotizacionEndosoForm" property="primaNetaAnual"/></td>
							<td colspan="3"></td>
						</tr>								
						<tr style="font-size: 9px;">							
							<th style="text-align: right;">Prima Neta Cotizaci&oacute;n</th>
							<td><bean:write name="cotizacionEndosoForm" property="primaNetaCotizacion"/></td>
							<td colspan="3"></td>
						</tr>			
						<tr style="font-size: 9px;">							
							<th style="text-align: right;">Recargo Financiero</th>
							<td><span id="rpf"><bean:write name="cotizacionEndosoForm" property="montoRecargoPagoFraccionado"/></span></td>
							<td colspan="3"></td>
						</tr>
						<tr style="font-size: 9px;">							
							<th style="text-align: right;">Gastos de Expedici&oacute;n</th>
							<td><span id="derechos"><bean:write name="cotizacionEndosoForm" property="derechosPoliza"/></span></td>
							<td colspan="3"></td>
						</tr>		
						<tr style="font-size: 9px;">							
							<th style="text-align: right;" width="100px">I.V.A. (<bean:write name="cotizacionEndosoForm" property="factorIVA"/>)</th>
							<td><span id="iva"><bean:write name="cotizacionEndosoForm" property="montoIVA"/></span></td>
							<td colspan="3"></td>
						</tr>
						<tr style="font-size: 9px;">							
							<th style="text-align: right;">Prima Total</th>
							<td><span id="primaTotal"><bean:write name="cotizacionEndosoForm" property="primaNetaTotal"/></span></td>
							<td colspan="3"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	
		<nested:notEmpty name="cotizacionEndosoForm" property="movimientosGenerales">
			<div class="subtituloIzquierdaDiv">Movimientos de Aplicaci&oacute;n General</div>
			<table id="desplegar" border="0">
				<nested:iterate id="nextMovimiento" name="cotizacionEndosoForm" property="movimientosGenerales" indexId="indexVar" scope="request">
					<c:set var="tipoMovimiento" value="${nextMovimiento.claveTipoMovimiento}"></c:set>
					<nested:equal value="10" name="tipoMovimiento">
						<c:set var="noMovimiento" value="${indexVar + 1}"></c:set>
						<tr>
							<th>
								<bean:write name="noMovimiento"/>
							</th>
							<th>
								<bean:write name="nextMovimiento" property="descripcionMovimiento"/> 
							</th>
						</tr>					
					</nested:equal>				
				</nested:iterate>
			</table>
			<br/>
		</nested:notEmpty>
		<nested:notEmpty name="cotizacionEndosoForm" property="movimientosInciso">			
			<div class="subtituloIzquierdaDiv">Movimientos al Inciso</div>
			<table id="t_riesgo" width="100%">
				<tr>
					<th width="20%">Inciso/Ubicaci&oacute;n</th>
					<th width="20%">Bien/Secci&oacute;n</th>
					<th width="60%">Movimiento Realizado</th>
				</tr>
				<c:set var="numInciso" value="0"/>
				<nested:iterate id="nextMovimiento" name="cotizacionEndosoForm" property="movimientosInciso" indexId="indexVar" scope="request">
					<c:if test="${nextMovimiento.claveAgrupacion == 0}">
						<tr>
							<c:choose>
								<c:when test="${numeroInciso == 0 || nextMovimiento.numeroInciso != numInciso}">
									<td width="20%" align="center" class="txt_v">
										${nextMovimiento.numeroInciso}
										<c:set var="numInciso" value="${nextMovimiento.numeroInciso}"/>
								</c:when>
								<c:otherwise>
								<td width="20%" align="center">
									&nbsp;<c:set var="numInciso" value="${nextMovimiento.numeroInciso}"/></c:otherwise>									
							</c:choose>							
							</td>
							<td width="20%" align="center">
								<c:choose>
									<c:when test="${nextMovimiento.idToSeccion > 0}">
										${nextMovimiento.descripcionSeccion}
									</c:when>
									<c:otherwise>&nbsp;</c:otherwise>
								</c:choose>
							</td>
							<td width="50%">
								${nextMovimiento.descripcionMovimiento}	
							</td>												
						</tr>
					</c:if>			
				</nested:iterate>
			</table>
		</nested:notEmpty>
	</logic:equal>
	<!-- Endoso de Declaracion -->
	<logic:equal value="4" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		<table  id="desplegarDetalle">
			<tr>
				<th>No. P&oacute;liza:</th>
				<td>${cotizacionEndosoForm.numeroPolizaFormateada}</td>
				<th>Moneda:</th>
				<td>${cotizacionEndosoForm.moneda}</td>				
			</tr>
		</table>
		<table  id="desplegarDetalle" border="0">
			<tr>
				<td colspan="4">
					<div id="declaracionEmbarquesDiv" class="dataGridQuotationClass"></div>
				</td>
			</tr>
			<tr>
				<td width="25%">
					<div class="alinearBotonALaDerecha">
						<midas:boton onclick="javascript: borrarEmbarque();" tipo="borrar" texto="Borrar"/>
					</div>
				</td>
				<td width="25%">
					<div class="alinearBotonALaDerecha">
						<midas:boton onclick="javascript: agregarEmbarque(${cotizacionEndosoForm.idToCotizacion});" tipo="agregar" texto="Agregar"/>
					</div>
				</td>
				<td width="25%">
					<div class="alinearBotonALaDerecha">
						<midas:boton onclick="javascript: endosoDeclaracionProcessor.sendData();" tipo="guardar" texto="Guardar"/>
					</div>
				</td>				
				<td width="25%">
					<c:choose>
						<c:when test="${cotizacionEndosoForm.claveEstatus == '10'}">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: esEndosoValido();" tipo="continuar" texto="Liberar"/>											
							</div>						
						</c:when>
						<c:otherwise>&nbsp;</c:otherwise>
					</c:choose>				
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<midas:mensaje clave="configuracion.eliminar.seleccionado.mensaje"/> posteriormente guardar.
				</td>
				<td colspan="1">&nbsp;</td>
			</tr>	
		</table>	
	</logic:equal>	
	<!-- Endoso de Cambio Forma de Pago -->
	<logic:equal value="5" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<table id="desplegarDetalle">
			<tr>
				<td colspan="6">
					<table id="desplegarDetalle">
						<tr>
							<th width="10%">
								P&oacute;liza a endosar:
							</th>
							<td width="20%">
								<bean:write name="cotizacionEndosoForm" property="numeroPolizaFormateada"/>
							</td>
							<th width="10%">
								Vigencia:
							</th>
							<td width="25%" colspan="2">
								<bean:write name="cotizacionEndosoForm" property="fechaInicioVigencia"/> - <bean:write name="cotizacionEndosoForm" property="fechaFinVigencia"/> 
							</td>
							<th width="15%">
								Forma de Pago Anterior:
							</th>
							<td width="20%">
								<html:select property="idFormaPagoAnterior" name="cotizacionEndosoForm" styleClass="cajaTexto" styleId="idFormaPagoAnterior" disabled= "true">
									<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
									<html:optionsCollection name="cotizacionEndosoForm" property="listaFormaPago" value="idFormaPago" label="descripcion" />
								</html:select>
							</td>					
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<th width="20%">
					<midas:mensaje clave="midas.ordendetrabajo.vigenciaFechaInicial" /> de Vigencia del Endoso:
				</th>
				<td width="20%">
					<html:text styleId="fechaInicioVigenciaEndoso" 
							property="fechaInicioVigenciaEndoso" 
							name="cotizacionEndosoForm" size="10"
							disabled="true" 
							styleClass="cajaTexto"/>
					<html:hidden property="fechaInicioVigenciaEndoso" name="cotizacionEndosoForm"/>
				</td>
				<td width="20%">
					<div style="display: none;">					
						<html:select property="idMoneda" name="cotizacionEndosoForm" styleId="idTcMoneda" styleClass="cajaTexto"
						  	onchange="getFormasPagoPorMoneda(this,'idFormaPago');" disabled="${cotizacionEndosoForm.editaDatoGeneral}" >
						  	<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="cotizacionEndosoForm"  property="listaMoneda" value="idTcMoneda" label="descripcion"/>
					 	</html:select>										
					</div>
				</td>
				<th width="20%">
					Forma de Pago:
				</th>
				<td width="20%">
					<html:select property="idFormaPago" name="cotizacionEndosoForm" styleClass="cajaTexto" styleId="idFormaPago" >
						<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
						<html:optionsCollection name="cotizacionEndosoForm" property="listaFormaPago" value="idFormaPago" label="descripcion" />
					</html:select>
				</td>
			</tr>				
			<tr>
				<c:choose>
					<c:when test="${cotizacionEndosoForm.claveEstatus == '10' && cotizacionEndosoForm.idFormaPago != null}">
						<html:hidden property="mensaje" styleId="mensaje"/>
						<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
						<td colspan="2" width="50%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'distribuyeReaseguroEndoso(document.cotizacionEndosoForm.idToCotizacion.value);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');" tipo="continuar" texto="Guardar"/>
							</div>
						</td>
						<td colspan="2" width="50%">
							<c:if test="${cotizacionEndosoForm.idFormaPago != null || cotizacionEndosoForm.idFormaPago != ''}">
								<div class="alinearBotonALaDerecha">
									<midas:boton onclick="javascript: sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/resumen/liberarCotizacion.do?id=' + document.cotizacionEndosoForm.idToCotizacion.value, 'contenido','mostrarVentanaMensajeValidar(); listarCotizacionesEndoso();');" tipo="agregar" texto="Liberar"/>
								</div>							
							</c:if >
						</td>																								
					</c:when>
					<c:otherwise>
						<td colspan="4" width="100%">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: parent.sendRequest(document.cotizacionEndosoForm,'/MidasWeb/cotizacion/endoso/guardarEndoso.do','contenido_endoso', 'distribuyeReaseguroEndoso(document.cotizacionEndosoForm.idToCotizacion.value);validarTipoMovimiento(document.cotizacionEndosoForm.claveMotivo.value,document.cotizacionEndosoForm.idToCotizacion.value)');" tipo="continuar" texto="Guardar"/>
							</div>
						</td>								
					</c:otherwise>
				</c:choose>	
				<td>&nbsp;</td>		
			</tr>
		</table>
		<div class="subtituloIzquierdaDiv">Totales de Cotizaci&oacute;n</div>
		<table id="desplegarDetalle" border="0">
			<tr>
				<td width="50%">&nbsp;</td>
				<td width="50%">
					<table id="t_riesgo" border="0">
						<tr>
							<th width="50%">Prima Neta Cotizaci&oacute;n</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="primaNetaCotizacion"/></td>
						</tr>			
						<tr>
							<th width="50%">Recargo Financiero</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="montoRecargoPagoFraccionado"/></td>
						</tr>
						<tr>
							<th width="50%">Gastos de Expedici&oacute;n</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="derechosPoliza"/></td>
						</tr>		
						<tr>
							<th width="50%">I.V.A a la tasa del <bean:write name="cotizacionEndosoForm" property="factorIVA"/></th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="montoIVA"/></td>
						</tr>	
						<tr>
							<th width="50%">Prima Total</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="primaNetaTotal"/></td>
						</tr>	
					</table>
				</td>
			</tr>
		</table>		
		<nested:notEmpty name="cotizacionEndosoForm" property="movimientosGenerales">
			<div class="subtituloIzquierdaDiv">Movimientos de Aplicaci&oacute;n General</div>
			<table id="desplegar" border="0">
				<nested:iterate id="nextMovimiento" name="cotizacionEndosoForm" property="movimientosGenerales" indexId="indexVar" scope="request">
					<c:set var="tipoMovimiento" value="${nextMovimiento.claveTipoMovimiento}"></c:set>
					<nested:equal value="10" name="tipoMovimiento">
						<c:set var="noMovimiento" value="${indexVar + 1}"></c:set>
						<tr>
							<th>
								<bean:write name="noMovimiento"/>
							</th>
							<th>
								<bean:write name="nextMovimiento" property="descripcionMovimiento"/> 
							</th>
						</tr>					
					</nested:equal>				
				</nested:iterate>
			</table>
			<br/>
		</nested:notEmpty>		
	</logic:equal>
	<!-- Endoso de Cambio Forma de Pago -->
	
</midas:formulario>