<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<table id="desplegarDetalle">
	<tr>
		<td width=100%>
			<div id="seccionesPorIncisoGrid" width="650px" height="150px"
				style="background-color:white; overflow:hidden; margin-left:auto; margin-right:auto;"></div>
		</td>
	</tr>
	<tr height="30px"></tr>
	<tr>
		<td class="titulo" width="100%">
			<midas:mensaje clave="midas.cotizacion.subIncisos" />
			<div id="botonAgregar">
				<div class="alinearBotonALaDerecha">
					<div id="b_agregar" style="display:none">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(seccionCotForm,'/MidasWeb/cotizacion/subinciso/mostrarAgregar.do?origen=END','configuracion_detalle','mostrarDatosSubInciso(0, 0);');">
							<midas:mensaje clave="midas.accion.agregar"/> </a>
					</div>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div id="resultados">
				Seleccione una Secci&oacute;n para desplegar sus SubIncisos
			</div>
		</td>
	</tr>
</table>