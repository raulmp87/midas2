<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<midas:formulario accion="/cotizacion/endoso/guardarEndoso">
	<html:hidden property="claveTipoEndoso" name="cotizacionEndosoForm"/>
	<html:hidden property="idToCotizacion" name="cotizacionEndosoForm"/>
	<!-- Endoso de Modificacion -->
	<logic:equal value="3" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
		<div class="subtituloIzquierdaDiv">Totales del Endoso</div>
		<table id="desplegarDetalle" border="0" width="100%">
			<tr>
				<td colspan="2">
					<table id="t_riesgo" border="0" width="100%">
						<tr>
							<th></th>
							<th colspan="3">P�liza Origen + Endosos Emitidos Anteriores</th>
							<th colspan="2">Endoso</th>
							<th colspan="3">Movimiento a quedar</th>
						</tr>
						<tr>
							<th></th>
							<th>Suma Asegurada</th>
							<th>Cuota</th>
							<th>Prima Neta</th>
							<th>Suma Asegurada</th>
							<th>Prima Neta</th>
							<th>Suma Asegurada</th>
							<th>Cuota</th>
							<th>Prima Neta</th>
						</tr>
						<nested:iterate id="nextDiferencia" name="cotizacionEndosoForm" property="diferenciaCotizacionEndosoDTOs" indexId="index">
							<tr style="<c:out value="${nextDiferencia.rowStyle}" />">
								<td style="padding-left: <c:out value="${nextDiferencia.padding}" />"><bean:write name="nextDiferencia" property="tituloPrimeraColumna"/></td>
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="originalSumaAsegurada" format="$#,##0.00"/></td>
								<c:set var="cuotaOriginal" value="${nextDiferencia.originalCuota * 1000}"></c:set>
								<c:choose>
									<c:when test="${cuotaOriginal != 0}">
										<td style="font-size: 9px;"><bean:write name="cuotaOriginal" format="##0.0000"/></td>
									</c:when>
									<c:otherwise><td style="font-size: 9px;"></td></c:otherwise>
								</c:choose>
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="originalPrimaNeta" format="$#,##0.00"/></td>
								
								<td style="border-left: 1px solid #B2DBB2;"><bean:write name="nextDiferencia" property="diferenciaSumaAsegurada" format="$#,##0.00"/>&nbsp;</td>
								<td style="border-right: 1px solid #B2DBB2;"><bean:write name="nextDiferencia" property="diferenciaPrimaNeta" format="$#,##0.00"/>&nbsp;</td>
								
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="nuevaSumaAsegurada" format="$#,##0.00"/></td>
								<c:set var="nuevaCuota" value="${nextDiferencia.nuevaCuota * 1000}"></c:set>
								<c:choose>
									<c:when test="${not empty nextDiferencia.tieneCuotasDiferentes}">
										<td style="color: red; font-size: 9px;">
									</c:when>
									<c:otherwise>
										<td style="font-size: 9px;">
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${nuevaCuota != 0}">
										<bean:write name="nuevaCuota" format="##0.0000"/></td>
									</c:when>
									<c:otherwise></td></c:otherwise>
								</c:choose>
								<td style="font-size: 9px;"><bean:write name="nextDiferencia" property="nuevaPrimaNeta" format="$#,##0.00"/></td>
							</tr>
						</nested:iterate>
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">D�as</th>
							<td><bean:write name="cotizacionEndosoForm" property="diasPorDevengar"/></td>
							<td colspan="3"></td>
						</tr>													
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">Prima Neta Anual</th>
							<td><bean:write name="cotizacionEndosoForm" property="primaNetaAnual"/></td>
							<td colspan="3"></td>
						</tr>								
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">Prima Neta Cotizaci&oacute;n</th>
							<td><bean:write name="cotizacionEndosoForm" property="primaNetaCotizacion"/></td>
							<td colspan="3"></td>
						</tr>			
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">Recargo Financiero</th>
							<td><bean:write name="cotizacionEndosoForm" property="montoRecargoPagoFraccionado"/></td>
							<td colspan="3"></td>
						</tr>
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">Gastos de Expedici&oacute;n</th>
							<td><bean:write name="cotizacionEndosoForm" property="derechosPoliza"/></td>
							<td colspan="3"></td>
						</tr>		
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">I.V.A. (<bean:write name="cotizacionEndosoForm" property="factorIVA"/>)</th>
							<td><bean:write name="cotizacionEndosoForm" property="montoIVA"/></td>
							<td colspan="3"></td>
						</tr>
						<tr style="font-size: 9px;">
							<td colspan="4"></td>
							<th style="text-align: right;">Prima Total</th>
							<td><bean:write name="cotizacionEndosoForm" property="primaNetaTotal"/></td>
							<td colspan="3"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<nested:notEmpty name="cotizacionEndosoForm" property="movimientosGenerales">
			<div class="subtituloIzquierdaDiv">Movimientos de Aplicaci&oacute;n General</div>
			<table id="desplegar" border="0">
				<nested:iterate id="nextMovimiento" name="cotizacionEndosoForm" property="movimientosGenerales" indexId="indexVar" scope="request">
					<c:set var="tipoMovimiento" value="${nextMovimiento.claveTipoMovimiento}"></c:set>
					<nested:equal value="10" name="tipoMovimiento">
						<c:set var="noMovimiento" value="${indexVar + 1}"></c:set>
						<tr>
							<th>
								<bean:write name="noMovimiento"/>
							</th>
							<th>
								<bean:write name="nextMovimiento" property="descripcionMovimiento"/> 
							</th>
						</tr>					
					</nested:equal>				
				</nested:iterate>
			</table>
			<br/>
		</nested:notEmpty>
		<nested:notEmpty name="cotizacionEndosoForm" property="movimientosInciso">			
			<div class="subtituloIzquierdaDiv">Movimientos al Inciso</div>
			<table id="t_riesgo" width="100%">
				<tr>
					<th width="20%">Inciso/Ubicaci&oacute;n</th>
					<th width="20%">Bien/Secci&oacute;n</th>
					<th width="60%">Movimiento Realizado</th>
				</tr>
				<c:set var="numInciso" value="0"/>
				<nested:iterate id="nextMovimiento" name="cotizacionEndosoForm" property="movimientosInciso" indexId="indexVar" scope="request">
					<c:if test="${nextMovimiento.claveAgrupacion == 0}">
						<tr>
							<c:choose>
								<c:when test="${numeroInciso == 0 || nextMovimiento.numeroInciso != numInciso}">
									<td width="20%" align="center" class="txt_v">
										${nextMovimiento.numeroInciso}
										<c:set var="numInciso" value="${nextMovimiento.numeroInciso}"/>
								</c:when>
								<c:otherwise>
								<td width="20%" align="center">
									&nbsp;<c:set var="numInciso" value="${nextMovimiento.numeroInciso}"/></c:otherwise>									
							</c:choose>							
							</td>
							<td width="20%" align="center">
								<c:choose>
									<c:when test="${nextMovimiento.idToSeccion > 0}">
										${nextMovimiento.descripcionSeccion}
									</c:when>
									<c:otherwise>&nbsp;</c:otherwise>
								</c:choose>
							</td>
							<td width="50%">
								${nextMovimiento.descripcionMovimiento}	
							</td>												
						</tr>
					</c:if>			
				</nested:iterate>
			</table>
		</nested:notEmpty>
	</logic:equal>
	<!-- Endoso de Cancelacion -->
	<logic:equal value="1" property="claveTipoEndoso" name="cotizacionEndosoForm">
		<table id="desplegarDetalle">
			<tr>
				<th width="25%">
					P&oacute;liza Endosada:
				</th>
				<td width="20%">
					<bean:write name="cotizacionEndosoForm" property="numeroPolizaFormateada"/>
				</td>
				<th width="25%">
					Vigencia:
				</th>
				<td width="30%">
					<bean:write name="cotizacionEndosoForm" property="fechaInicioVigencia"/> - <bean:write name="cotizacionEndosoForm" property="fechaFinVigencia"/> 
				</td>
			</tr>
			<tr>
				<th width="25%">
					<midas:mensaje clave="midas.ordendetrabajo.vigenciaFechaInicial" /> de Vigencia del Endoso:
				</th>
				<td width="20%">
					<html:text styleId="fechaInicioVigenciaEndoso" 
							property="fechaInicioVigenciaEndoso" 
							name="cotizacionEndosoForm" size="10"
							disabled="true" 
							styleClass="cajaTexto"/>
					<html:hidden property="fechaInicioVigenciaEndoso" name="cotizacionEndosoForm"/>
				</td>
				<th width="25%">
					Motivo de Cancelaci&oacute;n:
				</th>
				<td width="30%">
					<midas:comboValorFijo grupoValores="42" propiedad="claveMotivo"   
					  nombre="cotizacionEndosoForm"  styleClass="cajaTexto" readonly="true"/>
				</td>
			</tr>				
		</table>
		<div class="subtituloIzquierdaDiv">Informaci&oacute;n Coberturas</div>	
		<table id="t_riesgo" border="0" width="100%">
			<tr>
				<th>Nombre Secci&oacute;n</th>
				<th>Nombre Cobertura</th>
				<th>Valor Prima Neta</th>
			</tr>
			<nested:iterate id="nextCobertura" name="cotizacionEndosoForm" property="coberturasAgrupadas" indexId="index">
				<tr>
					<td><bean:write name="nextCobertura" property="nombreComercialSeccion"/></td>
					<td><bean:write name="nextCobertura" property="nombreComercialCobertura"/></td>
					<td><bean:write name="nextCobertura" property="valorPrimanetaString"/></td>
				</tr>
			</nested:iterate>
		</table>
		<div class="subtituloIzquierdaDiv">Totales del Endoso</div>
		<table id="desplegarDetalle" border="0">
			<tr>
				<td width="50%">&nbsp;</td>
				<td width="50%">
					<table id="t_riesgo" border="0">
						<tr>
							<th width="50%">Prima Neta Endoso</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="primaNetaCotizacion"/></td>
						</tr>			
						<tr>
							<th width="50%">Recargo Financiero</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="montoRecargoPagoFraccionado"/></td>
						</tr>
						<tr>
							<th width="50%">Gastos de Expedici&oacute;n</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="derechosPoliza"/></td>
						</tr>		
						<tr>
							<th width="50%">I.V.A a la tasa del <bean:write name="cotizacionEndosoForm" property="factorIVA"/></th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="montoIVA"/></td>
						</tr>	
						<tr>
							<th width="50%">Prima Total</th>
							<td width="50%" class="txt_v"><bean:write name="cotizacionEndosoForm" property="primaNetaTotal"/></td>
						</tr>	
					</table>
				</td>
			</tr>
		</table>			
	</logic:equal>
</midas:formulario>