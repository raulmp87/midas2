<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/cotizacion/endoso/modificarCuotas">
	<midas:oculto propiedadFormulario="datosCambioCuota.idToCotizacion"/>
	<midas:oculto propiedadFormulario="datosCambioCuota.idToCotizacionAnterior"/>	
	<table id="desplegarDetalle">
		<tr>
			<th colspan="2">
				Secci&oacute;n : <midas:escribe propiedad="datosCambioCuota.descripcionSeccion" nombre="cotizacionEndosoForm"/>
			</th>
		</tr>
		<tr>
			<th colspan="2">
				Cobertura: <midas:escribe propiedad="datosCambioCuota.descripcionCobertura" nombre="cotizacionEndosoForm"/>
			</th>			
		</tr>
	</table>

	<table id="t_riesgo" border="0" width="95%">
		<tr>
			<th>Inciso</th>
			<th>Cuota Antes</th>
			<th>Cuota Ahora</th>
			<th>&nbsp;</th>
		</tr>	
		<logic:notEmpty name="cotizacionEndosoForm" property="datosCambioCuota.coberturasInvolucradas" >
			<logic:iterate id="cobertura" name="cotizacionEndosoForm" property="datosCambioCuota.coberturasInvolucradas" indexId="index">
				<tr class="bg_t2">
					<c:set var="cuota" value="${cobertura.valorCuota * 1000}"></c:set>
					<c:set var="cuotaOriginal" value="${cobertura.valorCuotaOriginal * 1000}"></c:set>
					<c:set var="primaNeta" value="${cobertura.valorCuotaOriginal * cobertura.valorSumaAsegurada}"></c:set>
					<td>Inciso <bean:write name="cobertura" property="id.numeroInciso"/></td>
					<td align="center"><bean:write name="cuotaOriginal" format="##0.0000"/></td>
					<td align="center"><bean:write name="cuota" format="##0.0000"/></td>
					<td align="center">
						<a href="#"
							onclick="javascript: modificarCuota(${cobertura.id.idToCotizacion}, ${cobertura.id.numeroInciso},${cobertura.id.idToSeccion},${cobertura.id.idToCobertura}, ${primaNeta},'SGL');">
							Aplicar	Cuota Original
						</a>
					</td>
				</tr>
			</logic:iterate>
			<logic:notEmpty name="cotizacionEndosoForm" property="datosCambioCuota"> 
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4">
						<div class="alinearBotonALaDerecha">
							<button
								onclick="javascript: modificarCuota(${cobertura.id.idToCotizacion}, ${cobertura.id.numeroInciso},${cobertura.id.idToSeccion},${cobertura.id.idToCobertura}, ${primaNeta},'ALL');"
								type="button" style="cursor: pointer;">
								<table style="font-size: 9px;" align="center">
									<tr>
										<td align="center">
											Aplicar Todas
										</td>
									</tr>
								</table>
							</button>
						</div>				
					</td>
				</tr>
			</logic:notEmpty>		
		</logic:notEmpty>
	</table>
	<logic:empty name="cotizacionEndosoForm" property="datosCambioCuota.coberturasInvolucradas" >
		<div class="subtituloIzquierdaDiv">No existen registros que mostrar, El cambio de cuota fue provocado por nuevos Incisos</div>
	</logic:empty>		
</midas:formulario>