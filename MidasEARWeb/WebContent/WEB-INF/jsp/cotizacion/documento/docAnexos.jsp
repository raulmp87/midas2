<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacion/documento/docAnexoCot/mostrarDocAnexo">
	<table width="95%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6">
				Documentos Anexos
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="contenido_documentosAnexosGrid" class="dataGridAccordionClass" ></div>
			</td>
		</tr>
	</table>
	<table>
		<tr><td with="90%" ></td><td width="10%" >
			<midas:boton onclick="javascript: if(!docAnexoProcessor.getSyncState()){parent.showIndicatorSimple(); docAnexoProcessor.sendData();}" tipo="guardar"/>
			</td>
		</tr>
	</table>
</midas:formulario>