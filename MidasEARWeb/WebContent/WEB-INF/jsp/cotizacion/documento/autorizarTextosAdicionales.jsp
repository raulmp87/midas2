<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacion/documento/texAdicionalCot/mostrarTexAdicional">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion"/>
	<input type="hidden" id='fecha' value='<bean:write name="fecha"/>'/>
	<input type="hidden" id='usuario' value='<bean:write name="usuario"/>'/>
	<table width="98%" id="desplegarDetalle">
		<tr>
			<td class="titulo" colspan="6">
				Autorizaci&oacute;n de Textos Adicionales
			</td>
		</tr>	
		<tr>
			<td colspan="6">
				<div id="contenido_textosAdicionalesPorAutorizarGrid" class="dataGridQuotationClass" ></div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div class="alinearBotonALaDerecha">				
					<div id="botonAgregar">
						<midas:boton onclick="javascript: procesarTextosAdicionales(7);" tipo="modificar" texto="Aurotizar" />
						<midas:boton onclick="javascript: procesarTextosAdicionales(8);" tipo="modificar" texto="Rechazar" />
					</div>
				</div>
			</td>
		</tr>
	</table>	
</midas:formulario>