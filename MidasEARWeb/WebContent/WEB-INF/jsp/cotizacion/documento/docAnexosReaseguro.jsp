<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<!--<div id="detalle" name="Detalle">-->
<!--	<center>-->
		<midas:formulario accion="/cotizacion/documento/docAnexoCot/guardarDocumentoAnexoReaseguro">
			<midas:oculto propiedadFormulario="idToCotizacion" nombreFormulario="anexosCotizacionForm"/>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td class="titulo" colspan="2">Documentos Anexos Reaseguro</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="documentosAnexosReaseguroGrid" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<td><a href="javascript: void(0);" onclick="alert('Favor de asegurare que los Anexos de la cotizaci\u00f3n sean consistentes con los Anexos de Reaseguro.');mostrarAnexarArchivoReaseguroWindow();">Agregar Documentos</a></td>
					<td><a href="javascript: void(0);" onclick="documentos.deleteSelectedItem();documentoAnexoReaseguroProcessor.sendData();">Eliminar seleccionado</a></td>
				</tr>
			</table>
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
				<a href="javascript: void(0);" onclick="javascript: actualizarGridAnexosReaseguro('documentoAnexoReaseguroProcessor', 'documentoAnexoReaseguroSecuenciaError');"><midas:mensaje clave="midas.accion.guardar"/></a>
				</div>
			</div>			
		</midas:formulario>
<!--	</center>-->
<!--</div>			-->