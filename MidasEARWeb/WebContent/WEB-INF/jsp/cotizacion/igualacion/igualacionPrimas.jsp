<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>

<div id="detalle" name="Detalle">
	<midas:formulario accion="/cotizacion/igualacion/igualarPrimaNeta">
	 <html:hidden property="mensajeErrorIgualacionPrima" name="igualacionPrimaNetaForm" styleId="mensajeErrorIgualacionPrima"/>
	
		<midas:oculto propiedadFormulario="idToCotizacion"/>
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		<html:hidden property="aplicoPrimerRiesgo" styleId="aplicoPrimerRiesgo"/>	
	<table>
		<tr>
			<td> 
				<c:choose>
					<c:when test="${igualacionPrimaNetaForm.incisos != null }">
						<html:select property="numeroInciso" styleClass="cajaTexto"  styleId="numeroInciso" onchange="filtrarPorInciso(document.igualacionPrimaNetaForm);">
							<html:option value="-1">TODOS LOS INCISOS</html:option>
							<c:set var="leyendaInciso" value="Inciso"></c:set>
							<nested:iterate property="incisos" name="igualacionPrimaNetaForm" indexId="var" id="incisosId">				
								<html:option  value="${incisosId.id.numeroInciso}">${leyendaInciso} - <nested:write property="id.numeroInciso"/></html:option>
							</nested:iterate>
						</html:select>						
					</c:when>
					<c:otherwise>
						<html:hidden property="numeroInciso" styleId="numeroInciso" value="-1"/>
					</c:otherwise>
				</c:choose>										
			</td>
		</tr>
	</table>
	<center>
			<table id="desplegarDetalle">
				<tr>
					<td colspan="4">
						<table id="t_riesgo">
							<tr>
								<th>&nbsp;</th>
								<th>Suma Asegurada</th>
								<th>Cuota Original</th>
								<th>Prima Neta Original</th>
								<th>Cuota Resultante</th>
								<th>Prima Neta Resultante</th>
							</tr>
							<tr>
								<td><b>Total Cotizaci&oacute;n</b></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><b><bean:write name="igualacionPrimaNetaForm" property="totalCotizacionOriginal" format="$#,##0.00" locale="idiomaLocal"/></b></td>
								<td>&nbsp;</td>
								<td><b><bean:write name="igualacionPrimaNetaForm" property="totalCotizacion" format="$#,##0.00" locale="idiomaLocal"/></b></td>
							</tr>
							<nested:iterate id="nextSeccion" name="igualacionPrimaNetaForm" property="secciones" indexId="indexVarSecc">
								<tr>
									<td style="padding-left: 5px;"><bean:write name="nextSeccion" property="seccionDTO.nombreComercial"/></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><b><bean:write name="nextSeccion" property="valorPrimaNetaOriginal" format="$#,##0.00" locale="idiomaLocal"/></b></td>
									<td>&nbsp;</td>
									<td><b><bean:write name="nextSeccion" property="valorPrimaNeta" format="$#,##0.00" locale="idiomaLocal"/></b></td>
								</tr>
								<nested:iterate id="nextCobertura" name="nextSeccion" property="coberturaCotizacionLista" indexId="indexVarCob">
									<tr>
										<td style="padding-left: 10px;"><bean:write name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.nombreComercial"/></td>
										<td>
											<nested:equal value="1" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												<bean:write name="nextCobertura" property="valorSumaAsegurada" format="$#,##0.00" locale="idiomaLocal"/>
											</nested:equal>
											<nested:equal value="2" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												Amparada
											</nested:equal>
											<nested:equal value="3" name="nextCobertura" property="coberturaSeccionDTO.coberturaDTO.claveTipoSumaAsegurada">
												<bean:write name="nextCobertura" property="valorSumaAsegurada" format="$#,##0.00" locale="idiomaLocal"/>
											</nested:equal>
										</td>
										<c:set var="cuotaOriginal" value="${nextCobertura.valorCuotaOriginal * 1000}"></c:set>
										<td><bean:write name="cuotaOriginal" format="##0.0000" locale="idiomaLocal"/></td>
										<td><bean:write name="nextCobertura" property="valorPrimaNetaOriginal" format="$#,##0.00" locale="idiomaLocal"/></td>
										<c:set var="cuota" value="${nextCobertura.valorCuota * 1000}"></c:set>
										<td><bean:write name="cuota" format="##0.0000" locale="idiomaLocal"/></td>
										<td><bean:write name="nextCobertura" property="valorPrimaNeta" format="$#,##0.00" locale="idiomaLocal"/></td>
									</tr>
								</nested:iterate>
							</nested:iterate>
						</table>
					</td>
				</tr>		
				<c:if test="${igualacionPrimaNetaForm.mensajeErrorIgualacionPrima == '0'}">			
				<tr>
					<td colspan="4">
						<midas:radio valorEstablecido="1" propiedadFormulario="tipoIgualacion">
							<midas:escribe propiedad="mensajeIgualacion" nombre="igualacionPrimaNetaForm"/>
						</midas:radio>
					</td>
				</tr>
				<c:choose>
					<c:when test="${empty igualacionPrimaNetaForm.seccionesCombo}">
						<tr>
							<td colspan="4">Secciones en Primer Riesgo</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td width = "40%">
								<midas:radio valorEstablecido="2" propiedadFormulario="tipoIgualacion">Igualaci&oacute;n de Prima a nivel Secci&oacute;n</midas:radio>
							</td>
							<td>
								<html:select property="idToSeccion1" styleClass="cajaTexto">
									<html:optionsCollection property="seccionesCombo" value="id.idToSeccion" label="seccionDTO.nombreComercial" name="igualacionPrimaNetaForm" />
								</html:select>
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<midas:radio valorEstablecido="3" propiedadFormulario="tipoIgualacion">Igualaci&oacute;n de Prima a nivel Cobertura</midas:radio>
							</td>
							<td>
								<html:select property="idToSeccion2" styleId="idToSeccion" styleClass="cajaTexto" onchange="javascript: llenarComboCobertura(this,'idToCobertura','/MidasWeb/cotizacion/igualacion/escribeComboCobertura.do');">
									<html:optionsCollection property="seccionesCombo" value="id.idToSeccion" label="seccionDTO.nombreComercial" name="igualacionPrimaNetaForm" />
								</html:select>
							</td>
							<td width = "50%">
								<html:select property="idToCobertura" styleClass="cajaTexto" styleId="idToCobertura">
									<html:optionsCollection property="coberturas" value="id.idToCobertura" label="coberturaSeccionDTO.coberturaDTO.nombreComercial" name="igualacionPrimaNetaForm" />
								</html:select>
							</td>
							<td>&nbsp;</td>
						</tr>
					</c:otherwise>
				</c:choose>
				
				<tr>
					
					<th>
						<etiquetas:etiquetaError property="primaNeta" requerido="si"
										name="igualacionPrimaNetaForm" key="cotizacion.igualacion.primaNeta"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
					</th>
					
					<td width = "30%">
						<midas:texto propiedadFormulario="primaNeta" onkeypress="return soloNumeros(this, event, true)" />
					</td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan = "2">
						<div class="alinearBotonALaDerecha">				
							<div id="botonAgregar">
							    <midas:boton style="width:150px" onclick="javascript: eliminaIgualacionPrimaRequest(document.igualacionPrimaNetaForm);" tipo="borrar" texto="Eliminar Igualaci&oacute;n"/>
							</div>
						</div>
					</td>				
					<td colspan = "2">
						<div class="alinearBotonALaDerecha">				
							<div id="botonAgregar">
							    <midas:boton style="width:150px" onclick="javascript: igualacionPrimaRequest(document.igualacionPrimaNetaForm);" tipo="agregar" texto="Igualar Prima Neta"/>
							</div>
						</div>
					</td>				
				</tr>
				<tr>	
					<td colspan = "2">
						<div class="alinearBotonALaDerecha">
							<div id="b_reporteXLS" style="width:140px;text-align:center">
								<a href="javascript: void(0);" onclick="imprimirMemoriaCalculo('application/vnd.ms-excel');">Imprimir</a>
							</div>
						</div>
					</td>
					<td colspan = "2">
						<div class="alinearBotonALaDerecha">
							<div id="b_reportePDF" style="width:140px;text-align:center">
								<a href="javascript: void(0);" onclick="imprimirMemoriaCalculo('application/pdf');">Imprimir</a>
							</div>
						</div>
					</td>	
				</tr>
				 </c:if>
			</table>
			
			<script type="text/javascript">
				validaMensajeErrorIgualacion();
			</script>
			<c:choose>
				<c:when test="${igualacionPrimaNetaForm.incisos != null && igualacionPrimaNetaForm.numeroInciso != '-1'}">
					&nbsp;
				</c:when>
				<c:otherwise>
					<div class="subtituloIzquierdaDiv">Totales de Cotizaci&oacute;n</div>
			<table id="desplegarDetalle" border="0">
				<tr>
					<td width="50%">
						<logic:equal name="igualacionPrimaNetaForm" property="mostrarEdicionDerechosRPF" value="1">						
							<table id="t_riesgo">
								<tr>
									<th colspan = "3">
										Recargo financiero&nbsp;&nbsp;&nbsp;									
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionRPF" value="0">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/blank.gif" alt="" title="" height="12px" width="12px" />
										</logic:equal> 							
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionRPF" value="1">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/ico_yel.gif" alt="Requiere autorizaci&oacute;n" title="Requiere autorizaci&oacute;n"  height="12px" width="12px" />
										</logic:equal>
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionRPF" value="7">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/ico_green.gif" alt="Autorizado" title="Autorizado" height="12px" width="12px" />
										</logic:equal>
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionRPF" value="8">
											<img id="imgAutorizacionRPF" src="/MidasWeb/img/ico_red.gif" alt="Rechazado" title="Rechazado" height="12px" width="12px" />
										</logic:equal>									
									</th>								
								</tr>
								<tr>
									<td colspan="3"	>
										<midas:radio valorEstablecido="0" propiedadFormulario="tipoCalculoRPF">Autom&aacute;tico</midas:radio>
									</td>
								</tr>
								<tr>
									<td width="40%">														
										<midas:radio valorEstablecido="1" propiedadFormulario="tipoCalculoRPF">Manual ($)</midas:radio>
									</td>
									<td>
										<midas:texto id="valorRPFEditable" propiedadFormulario="valorRPFEditable" onkeypress="return soloNumeros(this, event, true);"/>
									</td>
									<td>
										<midas:texto id="porcentajeRPFSoloLectura" propiedadFormulario="porcentajeRPFSoloLectura" soloLectura="true" deshabilitado="true"/>
									</td>
								</tr>
								<tr>
									<td>														
										<midas:radio valorEstablecido="2" propiedadFormulario="tipoCalculoRPF">Manual (%)</midas:radio>
									</td>
									<td>
										<midas:texto id="porcentajeRPFEditable" propiedadFormulario="porcentajeRPFEditable" onkeypress="return soloNumeros(this, event, true);"/>
									</td>
									<td>
										<midas:texto id="valorRPFSoloLectura" propiedadFormulario="valorRPFSoloLectura" soloLectura="true" deshabilitado="true"/>
									</td>								
								</tr>
								<tr>
									<td colspan = "3"> </td>
								</tr>
								<tr>
									<th colspan = "3">
										Gastos de expedici&oacute;n&nbsp;&nbsp;&nbsp;
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionDerechos" value="0">
											<img  id="imgAutorizacionDerechos" src="/MidasWeb/img/blank.gif" alt="" title="" height="12px" width="12px"/>										
										</logic:equal>									
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionDerechos" value="1">
											<img  id="imgAutorizacionDerechos"src="/MidasWeb/img/ico_yel.gif" alt="Requiere autorizaci&oacute;n" title="Requiere autorizaci&oacute;n" height="12px" width="12px" />										
										</logic:equal>
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionDerechos" value="7">
											<img id="imgAutorizacionDerechos" src="/MidasWeb/img/ico_green.gif" alt="Autorizado" title="Autorizado" height="12px" width="12px" />
										</logic:equal>
										<logic:equal name="igualacionPrimaNetaForm" property="claveAutorizacionDerechos" value="8">
											<img id="imgAutorizacionDerechos" src="/MidasWeb/img/ico_red.gif" alt="Rechazado" title="Rechazado" height="12px" width="12px" />
										</logic:equal>		
									</th>
								</tr>
								<tr>
									<td colspan="3">
										<midas:radio valorEstablecido="0" propiedadFormulario="tipoCalculoDerechos">Autom&aacute;tico</midas:radio>
									</td>
								</tr>
								<tr>
									<td>														
										<midas:radio valorEstablecido="1" propiedadFormulario="tipoCalculoDerechos">Manual ($)</midas:radio>
									</td>
									<td>
										<midas:texto id="valorDerechos" propiedadFormulario="valorDerechos" onkeypress="return soloNumeros(this, event, true);"/>
									</td>								
								</tr>
								<tr>	
									<td colspan="3">
										<div class="alinearBotonALaDerecha">				
											<div id="botonGuardar">
									    		<midas:boton style="width:100px" onclick="javascript: modificarRPFDerechos(document.igualacionPrimaNetaForm);" tipo="guardar" texto="Guardar"/>
											</div>
										</div>
									</td>
								</tr>
								
							</table>
						</logic:equal>
					</td>
					<td width="50%">
						<table id="t_riesgo" border="0">
							<tr>
								<th width="50%">Prima Neta Anual</th>
								<td width="50%" class="txt_v"><bean:write name="igualacionPrimaNetaForm" property="primaNetaAnual"/></td>
							</tr>								
							<tr>
								<th width="50%">Prima Neta Cotizaci&oacute;n</th>
								<td width="50%" class="txt_v"><bean:write name="igualacionPrimaNetaForm" property="primaNetaCotizacion"/></td>
							</tr>			
							<tr>
								<th width="50%">Recargo Financiero</th>
								<td width="50%" class="txt_v"><span id="rpf"><bean:write name="igualacionPrimaNetaForm" property="montoRecargoPagoFraccionado"/></span></td>
							</tr>
							<tr>
								<th width="50%">Gastos de Expedici&oacute;n</th>
								<td width="50%" class="txt_v"><span id="derechos"><bean:write name="igualacionPrimaNetaForm" property="derechosPoliza"/></span></td>
							</tr>		
							<tr>
								<th width="50%">I.V.A a la tasa del <bean:write name="igualacionPrimaNetaForm" property="factorIVA"/></th>
								<td width="50%" class="txt_v"><span id="iva"><bean:write name="igualacionPrimaNetaForm" property="montoIVA"/></span></td>
							</tr>	
							<tr>
								<th width="50%">Prima Total</th>
								<td width="50%" class="txt_v"><span id="primaTotal"><bean:write name="igualacionPrimaNetaForm" property="primaNetaTotal"/></span></td>
							</tr>	
						</table>
					</td>
				</tr>
			</table>
				</c:otherwise>
			</c:choose>					
		</midas:formulario>
	</center>
</div>