<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<midas:tabla idTabla="docsDigAnexos"
	claseDecoradora="mx.com.afirme.midas.decoradores.DocumentosAnexosInspeccion"
	claseCss="tablaConResultados" nombreLista="documentosAnexosList">
	<midas:columna propiedad="controlArchivo.nombreArchivoOriginal" titulo="Archivo" />
	<midas:columna propiedad="nombreUsuarioCreacion" titulo="Anexado Por" />
	<midas:columna propiedad="fechaCreacion" titulo="Fecha"
		formato="{0,date,dd/MM/yyyy}" />
	<midas:columna propiedad="acciones" />
</midas:tabla>