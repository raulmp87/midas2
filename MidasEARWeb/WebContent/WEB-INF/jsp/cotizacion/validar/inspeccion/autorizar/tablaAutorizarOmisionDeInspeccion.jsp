<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_pgn_bricks.css"/>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_drag.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_group.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_excell_link.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxdataprocessor.js"/>"></script>

<div id="autorizacionesPendientesYRechazadas" width="550px" height="250px" style="background-color:white;overflow:hidden;margin-left: auto; margin-right: auto;">Aqui va la tabla</div>
<div id="botonesIniciales" style="padding-top:15px;padding-right:180px;">
	<div class="alinearBotonALaDerecha">
		<midas:boton onclick="javascript: mostrarInspeccion();" tipo="regresar" texto="Regresar"/>
		<div id="b_regresar" style="margin-right:10px;">
			<a href="javascript: void(0);"
				onclick="javascript: actualizarAutorizacionesPendientes(parent.document.getElementById('idToCotizacion').value,'<midas:mensaje clave="midas.cotizacion.inspeccion.validar.mensaje.seleccioneAlMenosUnInciso"/>','autorizar','Desea autorizar esta inspecci&oacute;n?');">
				<midas:mensaje clave="midas.accion.autorizar" />
			</a>
		</div>
		<div id="b_agregar" style="margin-right:10px;">
			<a href="javascript: void(0);"
				onclick="javascript: actualizarAutorizacionesPendientes(parent.document.getElementById('idToCotizacion').value,'<midas:mensaje clave="midas.cotizacion.inspeccion.validar.mensaje.seleccioneAlMenosUnInciso"/>','rechazar','Desea rechazar esta inspecci&oacute;n?');">
				<midas:mensaje clave="midas.accion.rechazar" />
			</a>
		</div>
	</div>			
</div>

<script type="text/javascript">mostrarAutorizacionesPendientesYRechazadas(parent.document.getElementById('idToCotizacion').value);</script>