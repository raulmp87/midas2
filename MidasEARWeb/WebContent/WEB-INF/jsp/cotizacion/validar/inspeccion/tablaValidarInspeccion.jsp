<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
		<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="<html:rewrite page="/js/midas.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptComponents.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
		<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>

<midas:tabla idTabla="tablaIncisoCotizacion"
	claseDecoradora="mx.com.afirme.midas.decoradores.ValidarInspeccion"
	claseCss="tablaConResultados" nombreLista="incisoCotizacionList">
	<midas:columna propiedad="codigoUsuarioEstInspeccion" titulo="Inciso"/> <!-- Este campo almacena temporalmente el consecutivo requerido en el GUI por cada inciso -->
	<midas:columna propiedad="direccionDTO.entreCalles" titulo="Ubicaci&oacute;n" /> <!-- Este campo almacena temporalmente la direcci&oacute;n completa del inciso -->
	<midas:columna propiedad="direccionDTO.nombreColonia" titulo="Status" />  <!-- Este campo almacena temporalmente el estatus del inciso -->
	<midas:columna propiedad="acciones" />
</midas:tabla>

<table id="desplegarDetalle">
	<tr>
		<td width="90%">&nbsp;</td>
		<td width="10%">
			<logic:present name="autorizaInspecciones">
				<midas:boton onclick="javascript: mostrarAutorizacionesInspeccion();" tipo="regresar" texto="Autorizaciones"/>
			</logic:present>
		</td>		
	</tr>
</table>