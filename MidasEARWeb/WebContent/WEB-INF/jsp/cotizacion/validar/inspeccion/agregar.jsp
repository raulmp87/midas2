<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacion/validar/inspeccion/guardarReporteInspeccion">
	<html:hidden property="idToInspeccionIncisoCotizacion" styleId="idToInspeccionIncisoCotizacion" />
	<html:hidden property="idToCotizacion" styleId="idToCotizacion" />
	<html:hidden property="numeroInciso" styleId="numeroInciso" />
	<html:hidden property="nombre" />
	<html:hidden property="tipoDePersona" />
	<html:hidden property="telefono" />
	<html:hidden property="agenteDeSeguros" />
	<html:hidden property="calleYNumero" />
	<html:hidden property="colonia" />
	<html:hidden property="codigoPostal" />
	<html:hidden property="ciudad" />
	<html:hidden property="estado" />
	
	<table id="agregar" width="600px">
		<tr>
			<td colspan="2"></td>
			<td align="right"><midas:mensaje clave="midas.cotizacion.inspeccion.cotizacion"/>: <midas:escribe propiedad="idToCotizacionFormateada" nombre="inspeccionForm"/></td>
			<td align="right"><midas:mensaje clave="midas.cotizacion.inspeccion.fecha"/>: <midas:escribe propiedad="fechaActual" nombre="inspeccionForm"/></td>
		</tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.inspeccion.reporteDeInspeccion" />
			</td>
		</tr>
		<tr height="30px"></tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.inspeccion.datosDelAsegurado" />
			</td>
		</tr>
		<tr height="15px"></tr>
		<tr>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.nombre" />:</td>
			<td><midas:escribe propiedad="nombre" nombre="inspeccionForm"/></td>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.tipoDePersona" />:</td>
			<td><midas:escribe propiedad="tipoDePersona" nombre="inspeccionForm"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.telefono" />:</td>
			<td><midas:escribe propiedad="telefono" nombre="inspeccionForm"/></td>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.agenteDeSeguros" />:</td>
			<td><midas:escribe propiedad="agenteDeSeguros" nombre="inspeccionForm"/></td>
		</tr>
		<tr height="15px"></tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.inspeccion.datosDeLaUbicacion" />
			</td>
		</tr>
		<tr height="15px"></tr>
		<tr>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.calleYNumero" />:</td>
			<td><midas:escribe propiedad="calleYNumero" nombre="inspeccionForm"/></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.colonia" />:</td>
			<td><midas:escribe propiedad="colonia" nombre="inspeccionForm"/></td>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.cp" />:</td>
			<td><midas:escribe propiedad="codigoPostal" nombre="inspeccionForm"/></td>
		</tr>
		<tr>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.ciudad" />:</td>
			<td><midas:escribe propiedad="ciudad" nombre="inspeccionForm"/></td>
			<td><midas:mensaje clave="midas.cotizacion.inspeccion.estado" />:</td>
			<td><midas:escribe propiedad="estado" nombre="inspeccionForm"/></td>
		</tr>
		<tr height="15px"></tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.inspeccion.datosDeLaInspeccion" />
			</td>
		</tr>
		<tr height="15px"></tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="numeroDeReporte" requerido="si"
					key="midas.cotizacion.inspeccion.numeroDeReporte"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td><midas:texto propiedadFormulario="numeroDeReporte" caracteres="22" nombreFormulario="inspeccionForm" onkeypress="return soloNumeros(this, event, true)"/></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="idProveedorInspeccion" requerido="si"
					key="midas.cotizacion.inspeccion.inspector"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:escribeCatalogo styleId="idProveedorInspeccion" styleClass="cajaTexto" size="" propiedad="idProveedorInspeccion" clase="mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionFacadeRemote"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="personaEntrevistada" requerido="si"
					key="midas.cotizacion.inspeccion.personaEntrevistada"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td><midas:texto propiedadFormulario="personaEntrevistada" caracteres="100" nombreFormulario="inspeccionForm"/></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="fechaDeLaInspeccion" requerido="si"
					key="midas.cotizacion.inspeccion.fechaDeLaInspeccion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td>
				<table>
					<tr>
						<td>
							<midas:texto id="fechaDeLaInspeccion" onfocus="mostrarCalendariosReporteInspeccion('fechaDeLaInspeccion')" soloLectura="true" propiedadFormulario="fechaDeLaInspeccion" deshabilitado="false" longitud="10" />
						</td>
						<td>
							<div id="b_calendario">
								<a href="javascript:void();" onclick="mostrarCalendariosReporteInspeccion('fechaDeLaInspeccion')" ></a>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="fechaDeLaInspeccionDiv" style="position:absolute;z-index:1;"></div>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<etiquetas:etiquetaError property="fechaElaboracionDocumento" requerido="si"
					key="midas.cotizacion.inspeccion.fechaElaboracionDocumento"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td>
				<table>
					<tr>
						<td>
							<midas:texto id="fechaElaboracionDocumento" onfocus="mostrarCalendariosReporteInspeccion('fechaElaboracionDocumento')" propiedadFormulario="fechaElaboracionDocumento"soloLectura="true" longitud="10" />
						</td>
						<td>
							<div id="b_calendario"">
									<a href="javascript:void();" onclick="mostrarCalendariosReporteInspeccion('fechaElaboracionDocumento')" ></a>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="fechaElaboracionDocumentoDiv" style="position:absolute;z-index:1;" >
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="resultadoDeLaInspeccion" requerido="si"
					key="midas.cotizacion.inspeccion.resultadoDeLaInspeccion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td>
				<table>
					<tr>
						<td style="font-size:9px"><midas:radio valorEstablecido="7" propiedadFormulario="resultadoDeLaInspeccion"><midas:mensaje clave="midas.cotizacion.inspeccion.inspeccionAprobada"/></midas:radio></td>
					</tr>
					<tr>
						<td style="font-size:9px"><midas:radio valorEstablecido="8" propiedadFormulario="resultadoDeLaInspeccion"><midas:mensaje clave="midas.cotizacion.inspeccion.inspeccionRechazada"/></midas:radio></td>
					</tr>
				</table>
			</td>
			<td>
				<etiquetas:etiquetaError property="comentariosDeLaInspeccion" requerido="no"
					key="midas.cotizacion.inspeccion.comentariosDeLaInspeccion"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td><midas:areatexto caracteres="4000" propiedadFormulario="comentariosDeLaInspeccion" nombreFormulario="inspeccionForm"/></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td>
				<logic:equal value="-1" property="idToInspeccionIncisoCotizacion" name="inspeccionForm" >
					<div id="botonesIniciales">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);"
<%--									onclick="javascript: destruirCalendarioReporteInspeccion();sendRequest(null, '/MidasWeb/cotizacion/validar/inspeccion/mostrarValidar.do?idToCotizacion=<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>','contenido','initAccordionVI(<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>)');">--%>
										onclick="javascript: destruirCalendarioReporteInspeccion();sendRequest(null, '/MidasWeb/cotizacion/cotizacion/mostrar.do?id=<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>','contenido','creaArbolCotizacion(<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>);dhx_init_tabbars();');">
									<midas:mensaje clave="midas.accion.regresar" />
								</a>
							</div>
							<div id="b_agregar">
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(inspeccionForm,'/MidasWeb/cotizacion/validar/inspeccion/guardarReporteInspeccion.do','contenido','cargarCalendariosReporteInspeccion(),mostrarTablaArchivosAdjuntosInspeccion()');">
									<midas:mensaje clave="midas.accion.continuar" />
								</a>
							</div>
						</div>			
					</div>
				</logic:equal>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<logic:notEqual value="-1" property="idToInspeccionIncisoCotizacion" name="inspeccionForm">
					<table width="600px" id="documentosAnexos">
						<tr>
							<td class="titulo">
								<midas:mensaje clave="midas.cotizacion.inspeccion.archivosAdjuntos" />
							</td>
						</tr>
						<tr>
							<td class="agregar">
								<div class="alinearBotonALaDerecha">
									<div id="b_asignar">
										<a href="javascript: void(0);" onclick="javascript: mostrarAdjuntarDocumentoAnexoInspeccionIncisoCotizacionWindow($('idToInspeccionIncisoCotizacion').value);" >
										<midas:mensaje clave="midas.accion.agregar"/></a>																																		
									</div>
								</div>
							</td>					
						</tr>
						<tr>
							<td width="600px"><!-- Tabla de Documentos digitales Anexos -->
								<div id ="tablaDocumentosAnexosInspeccion">
									<jsp:include page="tablaDocumentosAnexosInspeccion.jsp"></jsp:include>
								</div>
							</td>
						</tr>
						<tr height="30px"></tr>
						<tr>
							<td>
								<div id="botonModificar">
									<div class="alinearBotonALaDerecha">
										<div id="b_regresar">
											<a href="javascript: void(0);"
												onclick="javascript: destruirCalendarioReporteInspeccion();sendRequest(null, '/MidasWeb/cotizacion/cotizacion/mostrar.do?id=<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>','contenido','creaArbolCotizacion(<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>);dhx_init_tabbars();');">
												<midas:mensaje clave="midas.accion.regresar" />
											</a>
										</div>
										<div id="b_agregar">
											<a href="javascript: void(0);"
												onclick="javascript: destruirCalendarioReporteInspeccion();sendRequest(inspeccionForm,'/MidasWeb/cotizacion/validar/inspeccion/modificarReporteInspeccion.do','contenido','creaArbolCotizacion(<midas:escribe propiedad="idToCotizacion" nombre="inspeccionForm"/>);dhx_init_tabbars();');">
												<midas:mensaje clave="midas.accion.guardar" />
											</a>
										</div>
									</div>			
								</div>
							</td>
						</tr>
					</table>
				</logic:notEqual>
			</td>
		</tr>
	</table>
</midas:formulario>