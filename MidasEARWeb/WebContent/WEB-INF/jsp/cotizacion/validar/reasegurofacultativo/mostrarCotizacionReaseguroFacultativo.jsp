<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
<midas:formulario accion="/cotizacion/validar/mostrarCotizacionReaseguroFacultativo">
	<midas:oculto propiedadFormulario="idToCotizacion" nombreFormulario="cotizacionReaseguroFacultativoForm"/>
	<midas:oculto propiedadFormulario="idLineaSoporteReaseguro" nombreFormulario="cotizacionReaseguroFacultativoForm"/>
	<div class="subtituloCotizacion">Cotizaci&oacute;n de Reaseguro Facultativo</div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.cotizacion.cotizacion"/>:  <midas:escribe propiedad="idToCotizacionFormateada" nombre="cotizacionReaseguroFacultativoForm"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fecha" nombre="cotizacionReaseguroFacultativoForm"/>
		</div>
	<div style="clear:both"></div>
	<table  id="desplegarDetalle" border="0">
		<tr>
			<th colspan="2">TIPO DE NEGOCIO:</th>
			<td colspan="4"> <midas:escribe propiedad="descripcionTipoNegocio" nombre="cotizacionReaseguroFacultativoForm"/> </td>
		</tr>
	</table>
	<div class="subtituloIzquierdaDiv">Informaci&oacute;n de la L&iacute;nea</div>
		
	<table id="desplegarDetalle" border="0">
		<tr>
			<th>Inciso:</th>
			<td> <midas:escribe propiedad="numeroInciso" nombre="cotizacionReaseguroFacultativoForm"/> </td>
			<th>Linea:</th>
			<td> <midas:escribe propiedad="descripcinoLinea" nombre="cotizacionReaseguroFacultativoForm"/> </td>
			<th>Suma asegurada: </th>
			<td> <midas:escribe propiedad="descripcionSumaAsegurada" nombre="cotizacionReaseguroFacultativoForm"/> </td>
		</tr>
	</table>
	
	<div class="subtituloIzquierdaDiv">Informaci&oacute;n de Coberturas</div>

	<table id="t_riesgo">
		<tr>
			<th>Cobertura</th>
			<th>Prima Neta</th>
			<th>Prima por Contratos</th>
			<th>Prima Facultada</th>
			<th>Coaseguro</th>
			<th>Deducible</th>
		</tr>
		<nested:iterate id="detalleCobertura" name="cotizacionReaseguroFacultativoForm" property="listaCoberturas" indexId="indexDetalle">
			<tr>
				<td><bean:write name="detalleCobertura" property="descripcionCobertura"/></td>
				<td><bean:write name="detalleCobertura" property="primaNeta"/></td>
				<td><bean:write name="detalleCobertura" property="primaPorContrato"/>&nbsp;</td>
				<td><bean:write name="detalleCobertura" property="primaFacultada"/></td>
				<td><bean:write name="detalleCobertura" property="coaseguro"/></td>
				<td><bean:write name="detalleCobertura" property="deducible"/></td>
			</tr>
		</nested:iterate>
	</table>
	<table>
		<tr>
			<td align="right"> <midas:boton onclick="javascript: cerrarVentanaReaseguroFacultativo();" tipo="regresar" /> </td>
			<td align="right"> <midas:boton onclick="javascript: parent.sendRequest(document.cotizacionReaseguroFacultativoForm, '/MidasWeb/cotizacion/validar/aceptarFacultativo.do', 'contenido_validar', 'ventanaPersona.close(); inicializaAccordionValidarCotizacion(' + document.cotizacionReaseguroFacultativoForm.idToCotizacion.value + ', \&#39;cell2\&#39;, true);');" tipo="guardar" texto="Aceptar facultativo" /></td>
		</tr>
	</table>
</midas:formulario>