<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/dhtmlxgrid.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxgrid_skins.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_black.css"/>" rel="stylesheet" type="text/css">
		<link href="<html:rewrite page="/css/dhtmlxwindows_clear_green.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgridcell.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxaccordion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/validar/validarInspeccion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/reaseguro/contratofacultativo/slip/slip.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxgrid_nxml.js"/>"></script>

<midas:formulario accion="/cotizacion/validar/mostrarValidarReaseguroFacultativo">
            <html:hidden property="mensajeErrorCumulo" name="validarCotizacionForm" styleId="mensajeErrorCumulo"/>

			<table  id="desplegarDetalle" border="0">
				<tr>
					<th class="titulo" colspan="4">C&uacute;mulo por P&oacute;liza</th>
				</tr>	
				<tr>
					<td style="display:none;">
						<div id="registrosCumuloPoliza" style="display:none;"><midas:escribe propiedad="cumuloPoliza" nombre="validarCotizacionForm"/></div>
					</td>
					<td colspan="4" style="width:650">
						<div id="cumuloPorPolizaGrid" width="100%" height="125" class="dataGridConfigurationClass"></div>
					</td>
				</tr>	
				<tr>
					<th class="titulo" colspan="4">C&uacute;mulo por Inciso</th>
				</tr>
				<tr>
					<td style="display:none;">
						<div id="registrosCumuloInciso" style="display:none;"><midas:escribe propiedad="cumuloInciso" nombre="validarCotizacionForm"/></div>
					</td>
					<td colspan="4">
						<div id="cumuloPorIncisoGrid"  width="100%" height="125" class="dataGridConfigurationClass"></div>
					</td>
				</tr>
				<tr>
					<th class="titulo" colspan="4">C&uacute;mulo por Subinciso</th>
				</tr>
				<tr>
					<td style="display:none;">
						<div id="registrosCumuloSubInciso" style="display:none;"><midas:escribe propiedad="cumuloSubInciso" nombre="validarCotizacionForm"/></div>
					</td>
					<td colspan="4">
						<div id="cumuloPorSubIncisoGrid"  width="100%" height="125" class="dataGridConfigurationClass" ></div>
					</td>
				</tr>
			</table>
			<script type="text/javascript">
				initDataGridReaseguroFacultativo();
				validaMensajeErrorCumulo();
			</script>
			
</midas:formulario>