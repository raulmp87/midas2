<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<midas:formulario
	accion="/cotizacion/validar/mostrarAutorizacionesPendientesPorCotizacion">
	<table id="desplegarDetalle" width="100%">
		<logic:equal value="false" property="contienePendientes"
			name="validarCotizacionForm">
			<tr>
				<td class="titulo">
					<midas:mensaje clave="midas.cotizacion.sinAutorizacionPendiente" />
				</td>
			</tr>
		</logic:equal>

		<logic:equal value="true" property="contienePendientes"
			name="validarCotizacionForm">
			<tr>
				<td>
					<table id="desplegarDetalle">
						<tr>
							<th>
								<midas:mensaje clave="midas.cotizacion.autorizacionPendiente" />
							</th>
						</tr>
						<nested:iterate id="nextPendiente" name="validarCotizacionForm"
							property="listaPendientes" indexId="indexVar">
							<tr>
								<td>
									<bean:write name="nextPendiente" property="mensaje" />
								</td>
							</tr>
						</nested:iterate>
					</table>
				</td>
			</tr>
		</logic:equal>
	</table>
</midas:formulario>
