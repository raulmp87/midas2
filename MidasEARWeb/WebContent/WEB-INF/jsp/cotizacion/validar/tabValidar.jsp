<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/cotizacion/validar/mostrarValidar">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion"/>
	<html:hidden property="mensaje" styleId="mensajeValidar"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensajeValidar"/>
	<div id="accordionValidarCot" style="position: relative; height: 310px;"></div>
	<nested:equal value="10" name="cotizacionReaseguroFacultativoForm" property="claveEstatus">
		<div class="subtituloIzquierdaDiv">Liberar Cotizaci&oacute;n</div>
		<table id="desplegarDetalle">
			<tr>
				<th width="20%">
					<etiquetas:etiquetaError property="correos" requerido="no"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" name="cotizacionReaseguroFacultativoForm"
						key="solicitud.correos" />
				</th>
				<td width="50%">
					<midas:areatexto renglones="2" propiedadFormulario="correos" deshabilitado="false" id="correos" />
				</td>
				<td width="30%">
					<div class="alinearBotonALaDerecha">
						<logic:equal name="esEndoso" value="true">
							<midas:boton onclick="javascript: confirmaLiberacion(document.getElementById('idToCotizacion').value, true);" tipo="agregar" texto="Liberar Cotizacion"/>
						</logic:equal>
						<logic:notEqual name="esEndoso" value="true">
							<midas:boton onclick="javascript: confirmaLiberacion(document.getElementById('idToCotizacion').value, false);" tipo="agregar" texto="Liberar Cotizacion"/>
						</logic:notEqual>
					</div>
				</td>
			</tr>							
		</table>
	</nested:equal>
	<nested:equal value="11" name="cotizacionReaseguroFacultativoForm" property="claveEstatus">
		<div class="subtituloIzquierdaDiv">Liberar Cotizaci&oacute;n</div>
		<table id="desplegarDetalle">	
			<tr>
				<th width="20%">
					<etiquetas:etiquetaError property="correos" requerido="no"
						normalClass="normal" errorClass="error"
						errorImage="/img/information.gif" name="cotizacionReaseguroFacultativoForm"
						key="solicitud.correos" />
				</th>
				<td width="50%">
					<midas:areatexto renglones="2" propiedadFormulario="correos" deshabilitado="false" />
				</td>
				<td width="30%">
					<div class="alinearBotonALaDerecha">
						<logic:equal name="esEndoso" value="true">
							<midas:boton onclick="javascript: confirmaLiberacion(document.getElementById('idToCotizacion').value, true);" tipo="agregar" texto="Liberar Cotizacion"/>
						</logic:equal>
						<logic:notEqual name="esEndoso" value="true">
							<midas:boton onclick="javascript: confirmaLiberacion(document.getElementById('idToCotizacion').value, false);" tipo="agregar" texto="Liberar Cotizacion"/>
						</logic:notEqual>
					</div>
				</td>
			</tr>
		</table>				
	</nested:equal>	
</midas:formulario>