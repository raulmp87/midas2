<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>

<midas:formulario  accion="/cotizacion/proveedorInspeccion/listar">
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">Lista de Proveedores de inspeccion</td>
		</tr>
		<tr><td class="campoRequerido" ><midas:escribe propiedad="mensaje" nombre="proveedorInspeccionForm"/></td></tr>
	</table>
	<br/>
	<div id="resultados">
		<midas:tabla idTabla="proveedoresInspeccionTabla" claseDecoradora="mx.com.afirme.midas.decoradores.ProveedorInspeccion"
			claseCss="tablaConResultados" nombreLista="listaProveedoresInspeccion"
			urlAccion="/cotizacion/proveedorInspeccion/listar.do">
			<midas:columna propiedad="nombre" titulo="Nombre o Raz&oacute;n Social"/>
			<midas:columna propiedad="direccion" titulo="Direcci&oacute;n"/>
			<midas:columna propiedad="telefono" titulo="Telefono" />
			<midas:columna propiedad="rfc" titulo="R.F.C."/>
			<midas:columna propiedad="acciones" />
		</midas:tabla>
	</div>
	<table width="890"><tr><td>
		<div class="alinearBotonALaDerecha">
		<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?accion=agrega', 'contenido',null);" 
			tipo="agregar"/>
			</div>
		
	</td></tr></table>
</midas:formulario>
