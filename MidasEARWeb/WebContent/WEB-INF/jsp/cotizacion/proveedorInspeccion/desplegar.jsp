<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion">
	<div id="editaProveedorInspeccion"> <html:hidden property="accion" styleId="accion" /> </div>
	<div id="areaLectura">
		<div class="subtituloIzquierdaDiv">Datos Personales</div>	
	 	<midas:oculto propiedadFormulario="idToProveedorInspeccion"/>
	 	<midas:oculto propiedadFormulario="direccion.idToDireccion"/>
	 	<midas:oculto propiedadFormulario="persona.idToPersona"/>
	 	<midas:oculto propiedadFormulario="persona.descripcionPadre"/>
	 	<midas:oculto propiedadFormulario="direccion.descripcionPadre"/>
		<table id="desplegar">
			<tr>
				<th><midas:mensaje clave="midas.persona.tipo"/></th>
				<td class="txt_v"><midas:escribe propiedad="persona.descripcionTipoPersona" nombre="proveedorInspeccionForm"/></td>
				<th>&nbsp;</th><td class="txt_v">&nbsp;</td><th>&nbsp;</th><td class="txt_v">&nbsp;</td>									
			</tr>
			<logic:equal value="1" property="persona.claveTipoPersona" name="proveedorInspeccionForm" >
				<tr class="bg_t2">
					<th><midas:mensaje clave="midas.persona.nombre"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.nombre" nombre="proveedorInspeccionForm"/></td>
					<th><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.apellidoPaterno" nombre="proveedorInspeccionForm"/></td>
					<th><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.apellidoMaterno" nombre="proveedorInspeccionForm"/></td>			
				</tr>
				<tr>
					<th><midas:mensaje clave="midas.persona.rfc"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.codigoRFC" nombre="proveedorInspeccionForm"/></td>
					<th><midas:mensaje clave="midas.persona.fechaNacimiento"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.fechaNacimiento" nombre="proveedorInspeccionForm"/></td>
					<th><midas:mensaje clave="midas.persona.telefono"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.telefono" nombre="proveedorInspeccionForm"/></td>
				</tr>
			</logic:equal>
			<logic:equal value="2" property="persona.claveTipoPersona" name="proveedorInspeccionForm" >
				<tr class="bg_t2">
					<th><midas:mensaje clave="midas.persona.razonSocial"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.nombre" nombre="proveedorInspeccionForm"/></td>
					<th></th><td class="txt_v"></td><th></th><td class="txt_v"></td>
					</tr>
				<tr>
					<th><midas:mensaje clave="midas.persona.rfc"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.codigoRFC" nombre="proveedorInspeccionForm"/></td>
					<th><midas:mensaje clave="midas.persona.fechaConstitucion"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.fechaNacimiento" nombre="proveedorInspeccionForm"/></td>
					<th><midas:mensaje clave="midas.persona.telefono"/></th>
					<td class="txt_v"><midas:escribe propiedad="persona.telefono" nombre="proveedorInspeccionForm"/></td>
				</tr>
			</logic:equal>
			<tr class="bg_t2">
				<th><midas:mensaje clave="midas.persona.email"/></th>
				<td class="txt_v"><midas:escribe propiedad="persona.email" nombre="proveedorInspeccionForm"/></td>
				<th>&nbsp;</th><td class="txt_v">&nbsp;</td><th>&nbsp;</th><td class="txt_v">&nbsp;</td>
			</tr>
		</table>
		
		<table width="890">
		<tr>
		<td width="*">
			<logic:equal value="edita" property="accion" name="proveedorInspeccionForm" >
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: mostrarRegistroPersona(document.proveedorInspeccionForm.elements.namedItem('persona.idToPersona').value,document.proveedorInspeccionForm.elements.namedItem('persona.descripcionPadre').value,document.proveedorInspeccionForm.elements.namedItem('idToProveedorInspeccion').value,0);" 
					tipo="modificar"/>
					</div>
			</logic:equal>
			<logic:equal value="agrega" property="accion" name="proveedorInspeccionForm" >
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: mostrarRegistroPersona(document.proveedorInspeccionForm.elements.namedItem('persona.idToPersona').value,document.proveedorInspeccionForm.elements.namedItem('persona.descripcionPadre').value,0,0);"
					 tipo="agregar"/>
					</div>
			</logic:equal>
		</td>
		</tr>
		</table>
		
		<div class="subtituloIzquierdaDiv">Datos Domicilio</div>
		<table id="desplegar">
			<tr>
				<th><midas:mensaje clave="midas.direccion.nombreCalle"/>:</th>
				<td colspan="3" class="txt_v"><midas:escribe propiedad="direccion.nombreCalle" nombre="proveedorInspeccionForm"/></td>
			</tr>
			<tr class="bg_t2">
				<th><midas:mensaje clave="midas.direccion.numeroExterior"/>:</th>
				<td class="txt_v"><midas:escribe propiedad="direccion.numeroExterior" nombre="proveedorInspeccionForm"/></td>
				<th><midas:mensaje clave="midas.direccion.numeroInterior"/>:</th>
				<td class="txt_v"><midas:escribe propiedad="direccion.numeroInterior" nombre="proveedorInspeccionForm"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.entreCalles"/>:</th>
				<td colspan="3" class="txt_v"><midas:escribe propiedad="direccion.entreCalles" nombre="proveedorInspeccionForm"/></td>
			</tr>
			<tr class="bg_t2">
				<th><midas:mensaje clave="midas.direccion.nombreColonia"/>:</th>
				<td class="txt_v"><midas:escribe propiedad="direccion.colonia" nombre="proveedorInspeccionForm"/><midas:oculto propiedadFormulario="direccion.idColonia"/> </td>
				<th><midas:mensaje clave="midas.direccion.idMunicipio"/>:</th>
				<td class="txt_v"><midas:escribe propiedad="direccion.municipio" nombre="proveedorInspeccionForm"/> <midas:oculto propiedadFormulario="direccion.idMunicipio" /></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.idEstado"/>:</th>
				<td class="txt_v"><midas:escribe propiedad="direccion.estado" nombre="proveedorInspeccionForm"/> <midas:oculto propiedadFormulario="direccion.idEstado"/> </td>
				<th><midas:mensaje clave="midas.direccion.codigoPostal"/>:</th>
				<td class="txt_v"><midas:escribe propiedad="direccion.codigoPostal" nombre="proveedorInspeccionForm"/></td>
			</tr>			
		</table>
		<table width="890">
		<tr><td width="*">
		<logic:equal value="agrega" property="accion" name="proveedorInspeccionForm" >
			<div class="alinearBotonALaDerecha"><div id="b_guardar">
				<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/proveedorInspeccion/agregar.do','contenido',null);" 
					tipo="regresar"/>
				</div></div>
		</logic:equal>
		<logic:notEqual value="agrega" property="accion" name="proveedorInspeccionForm" >
			<div class="alinearBotonALaDerecha"><div id="b_guardar">
				<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/proveedorInspeccion/listar.do','contenido',null);" tipo="regresar"/>
				</div></div>
		</logic:notEqual>
		</td>
		<td width="100">
			<logic:equal value="edita" property="accion" name="proveedorInspeccionForm" >
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: mostrarRegistroDireccion(document.proveedorInspeccionForm.elements.namedItem('direccion.idToDireccion').value,document.proveedorInspeccionForm.elements.namedItem('direccion.descripcionPadre').value,document.proveedorInspeccionForm.elements.namedItem('idToProveedorInspeccion').value,0);" 
					tipo="modificar"/>
				</div>
			</logic:equal>
			<logic:equal value="agrega" property="accion" name="proveedorInspeccionForm" >
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: mostrarRegistroDireccion(document.proveedorInspeccionForm.elements.namedItem('direccion.idToDireccion').value,document.proveedorInspeccionForm.elements.namedItem('direccion.descripcionPadre').value,0,0);"
						tipo="agregar"/></div>
			</logic:equal>
			<logic:equal value="elimina" property="accion" name="proveedorInspeccionForm" >
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: Confirma('Desea eliminar el registro seleccionado?',null,'/MidasWeb/cotizacion/proveedorInspeccion/borrar.do?id='+document.proveedorInspeccionForm.elements.namedItem('idToProveedorInspeccion').value,'contenido',null);" 
						tipo="borrar"/>
						</div>
			</logic:equal>
		</td>
		</tr>
		</table>
	</div>
	
</midas:formulario>