<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<div class="subtituloCotizacion">Riesgos de <bean:write name="nombreCobertura" scope="request" /> </div>
<div class="subtituloCotizacion">
<bean:message key="configuracion.cobertura.cotizacion"/>: <bean:message key="midas.cotizacion.prefijo"/><%= String.format("%08d", new Object[]{request.getAttribute("idCotizacion")}) %>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<bean:message key="configuracion.cobertura.fecha"/>: <%= java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT).format((java.util.Date)request.getAttribute("fecha")) %>
</div>
<div style="clear:both"></div>
<input type="hidden" name="idCotizacion" value='<bean:write name="idCotizacion" scope="request" />' id="idCotizacion" />
<input type="hidden" name="numeroInciso" value='<bean:write name="numeroInciso" scope="request" />' id="numeroInciso" />
<input type="hidden" name="idToSeccion" value='<bean:write name="idToSeccion" scope="request" />' id="idToSeccion" />
<input type="hidden" name="idToCobertura" value='<bean:write name="idToCobertura" scope="request" />' id="idToCobertura" />
<table id="desplegarDetalle">
	<tr>
		<td width="100%" colspan="3">
			<div id="cotizacionRiesgosGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
	<tr>
		<td width="80%">&nbsp;</td>
		<td width="10%">
			<midas:boton onclick="javascript: mostrarAutorizacionesRiesgos();" tipo="regresar" texto="Autorizaciones"/>
		</td>
		<td width="10%">
			<midas:boton onclick="javascript: riesgoProcessor.sendData();" tipo="guardar" texto="Guardar"/>
		</td>
	</tr>
</table>
