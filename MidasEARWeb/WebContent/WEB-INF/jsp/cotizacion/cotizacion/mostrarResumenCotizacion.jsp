<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/cotizacion/comision/guardarBonificacion">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<table width="100%" id="t_riesgo">
		<tr>
		<th colspan="3"></th><th>Suma Asegurada</th><th>Cuota</th><th>Prima Neta</th>
		</tr>
		<nested:present name="resumenCotizacionForm" property="listaIncisos" >
			<nested:iterate id="incisoCot" name="resumenCotizacionForm" property="listaIncisos" indexId="indexInciso">
				<tr>
					<th colspan="3"><bean:write name="incisoCot" property="descripcionInciso"/></th>
					<th>&nbsp;</th><th>&nbsp;</th>
					<th align="right" ><bean:write name="incisoCot" property="primaNeta"/></th>
				</tr>
				<tr>
					<td colspan="6"> <bean:write name="incisoCot" property="descripcionGiro"/> </td>
				</tr>
				<nested:iterate id="seccionCot" name="incisoCot" property="listaSecciones">
					<tr>
						<td>&nbsp;</td>
						<td colspan="2"><bean:write name="seccionCot" property="descripcionSeccion"/></td>
						<td>&nbsp;</td><td>&nbsp;</td>
						<td align="right"><bean:write name="seccionCot" property="primaNeta"/> </td>
					</tr>
					<nested:iterate id="coberturaCot" name="seccionCot" property="listaCoberturas">
						<tr>
							<td>&nbsp;</td><td>&nbsp;</td>
							<td><bean:write name="coberturaCot" property="descripcionCobertura"/></td>
							<td align="right"><bean:write name="coberturaCot" property="sumaAsegurada"/></td>
							<td align="right"><bean:write name="coberturaCot" property="cuota"/></td>
							<td align="right"><bean:write name="coberturaCot" property="primaNeta"/></td>
						</tr>
					</nested:iterate>
				</nested:iterate>
			</nested:iterate>		
		</nested:present>
	</table>
	<div class="subtituloIzquierdaDiv">Totales de Cotizaci&oacute;n</div>
	<table id="desplegarDetalle" border="0">
		<tr>
			<td width="50%">&nbsp;</td>
			<td width="50%">
				<table id="t_riesgo" border="0">
					<tr>
						<th width="50%">Prima Neta Anual</th>
						<td width="50%" class="txt_v"><bean:write name="resumenCotizacionForm" property="primaNetaAnual"/></td>
					</tr>								
					<tr>
						<th width="50%">Prima Neta Cotizaci&oacute;n</th>
						<td width="50%" class="txt_v"><bean:write name="resumenCotizacionForm" property="primaNetaCotizacion"/></td>
					</tr>			
					<tr>
						<th width="50%">Recargo Financiero</th>
						<td width="50%" class="txt_v"><bean:write name="resumenCotizacionForm" property="montoRecargoPagoFraccionado"/></td>
					</tr>
					<tr>
						<th width="50%">Gastos de Expedici&oacute;n</th>
						<td width="50%" class="txt_v"><bean:write name="resumenCotizacionForm" property="derechosPoliza"/></td>
					</tr>		
					<tr>
						<th width="50%">I.V.A a la tasa del <bean:write name="resumenCotizacionForm" property="factorIVA"/></th>
						<td width="50%" class="txt_v"><bean:write name="resumenCotizacionForm" property="montoIVA"/></td>
					</tr>	
					<tr>
						<th width="50%">Prima Total</th>
						<td width="50%" class="txt_v"><bean:write name="resumenCotizacionForm" property="primaNetaTotal"/></td>
					</tr>	
				</table>
			</td>
		</tr>
	</table>
</midas:formulario>
