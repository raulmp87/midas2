<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:formulario accion="/cotizacion/listarDocumentosDigitalesComplementarios">
	<html:hidden property="idToCotizacion" name="documentosCotizacionForm" styleId="idToCotizacion"/>
	<html:hidden property="mensaje" name="documentosCotizacionForm" styleId="mensajeAclaraciones"/>
	<html:hidden property="tipoMensaje" name="documentosCotizacionForm" styleId="tipoMensajeAclaraciones"/>
	<table id="agregar" width="600px">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.docsDigitales.deLaSolicitud" />		
			</td>
		</tr>
		<tr>
			<td colspan="4"><!-- Tabla de Documentos digitales (vienen desde la Solicitud) -->
				<midas:tabla idTabla="docsDigComps" 
					claseDecoradora="mx.com.afirme.midas.decoradores.CotizacionDatosGenerales"
					claseCss="tablaConResultados" nombreLista="documentosDigitalesSolicitudList">
					<midas:columna propiedad="controlArchivo.nombreArchivoOriginal" titulo="Archivo" />
					<midas:columna propiedad="nombreUsuarioCreacion" titulo="Anexado Por" />
					<midas:columna propiedad="fechaCreacion" titulo="Fecha" formato="{0,date,dd/MM/yyyy}"/>
					<midas:columna propiedad="acciones" />
				</midas:tabla>
			</td>
		</tr>
		<tr height="30px"></tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.docsDigitalesComplementarios.deLaCotizacion" />	
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td class="agregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_asignar">
					<a href="javascript: void(0);" onclick="javascript: mostrarAdjuntarDocumentoDigitalComplementarioWindow($('idToCotizacion').value);" >
					<midas:mensaje clave="midas.accion.agregar"/></a>																																		
				</div>
			</div>
			</td>
			
		</tr>
		<tr>
			<td colspan="4"><!-- Tabla de Documentos digitales Complementarios -->
				<midas:tabla idTabla="docsDigComps" 
					claseDecoradora="mx.com.afirme.midas.decoradores.DocumentosDigitalesComplementariosCotizacion"
					claseCss="tablaConResultados" nombreLista="documentosDigitalesCotizacionList">
					<midas:columna propiedad="controlArchivo.nombreArchivoOriginal" titulo="Archivo" />
					<midas:columna propiedad="nombreUsuarioCreacion" titulo="Anexado Por" />
					<midas:columna propiedad="fechaCreacion" titulo="Fecha" formato="{0,date,dd/MM/yyyy}"/>
					<midas:columna propiedad="acciones" />
				</midas:tabla>
			</td>
		</tr>
		<tr height="30px"></tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.aclaraciones" />	
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<midas:areatexto propiedadFormulario="aclaraciones" id="aclaraciones" renglones="10" onKeyDown="if(event.keyCode == 8 || event.keyCode == 46) return true; if(this.value.length == 2000) return false;" deshabilitado="true"/>
			</td>
		</tr>
<!--		<tr>-->
<!--			<td colspan="4">-->
<!--				<div class="alinearBotonALaDerecha">-->
<!--					<midas:boton onclick="sendRequest(document.documentosCotizacionForm, '/MidasWeb/cotizacion/guardarAclaraciones.do', 'contenido_documentosDigitalesComplementarios', 'mostrarVentanaMensajeAclaraciones(null);');" tipo="Guardar"/>-->
<!--				</div>-->
<!--			</td>-->
<!--		</tr>-->
	</table>
	
</midas:formulario>
