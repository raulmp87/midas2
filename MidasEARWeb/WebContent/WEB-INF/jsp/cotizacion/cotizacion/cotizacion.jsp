<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<html:hidden property="idToCotizacion" name="cotizacionForm" styleId="idToCotizacion" />
<div style= "height: 450px; width:100% overflow:auto" hrefmode="ajax-html"  id="cotizaciontabbar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
		<div width="150px" id="igualacion" name="Igualaci&oacute;n" href="http://void" extraAction="sendRequest(null,'/MidasWeb/cotizacion/igualacion/mostrarIgualaciones.do?id=<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>', 'contenido_igualacion', 'inicializaAccordionIgualaciones(<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>)')"></div>
		<div width="150px" id="resumenCotizacion" name="Cotizaci&oacute;n por Inciso" extraAction="sendRequest(null,'/MidasWeb/cotizacion/resumen/mostrarResumenCotizacion.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_resumenCotizacion', null);"></div>
		<div id="documentosAnexos" name="Anexos" extraAction="cargaTabAnexosCot(false);"></div>
		<div id="textosAdicionales" name="Textos Adicionales" extraAction="sendRequest(null,'/MidasWeb/cotizacion/documento/texAdicionalCot/mostrarTexAdicional.do','contenido_textosAdicionales','cargaComponentesTexAdicional()');"></div>
		<logic:present name="autorizarTexAdicional">
			<div width="250px" id="textosAdicionalesPorAutorizar" name="Autorizaci&oacute;n de Textos Adicionales" extraAction="sendRequest(null,'/MidasWeb/cotizacion/documento/texAdicionalCot/mostrarTexAdicionalPorAutorizar.do','contenido_textosAdicionalesPorAutorizar','cargaComponentesTexAdicionalPorAutorizar()');"></div>
		</logic:present>
		<logic:present name="edicionComision">
			<div id="comisiones" name="Comisiones" extraAction="sendRequest(null,'/MidasWeb/cotizacion/comision/mostrar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&tipo=<bean:write name="edicionComision"/>', 'contenido_comisiones', 'mostrarEdicionComisionesPorCotizacionGrid(<bean:write name="cotizacionForm" property="idToCotizacion" />,\'<bean:write name="edicionComision"/>\');')"></div>
		</logic:present>
		<logic:present name="autorizacionComision">
			<div id="autorizarcomision" name="Autorizar Comisiones" extraAction="sendRequest(null,'/MidasWeb/cotizacion/comision/mostrar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&tipo=<bean:write name="autorizacionComision"/>', 'contenido_autorizarcomision', 'mostrarAutorizarComisionesPorCotizacionGrid(<bean:write name="cotizacionForm" property="idToCotizacion" />,\'<bean:write name="autorizacionComision"/>\');')"></div>
		</logic:present>		
		<div width="150px" id="primerRiesgoLUC" name="Primer Riesgo / LUC" extraAction="sendRequest(null,'/MidasWeb/cotizacion/primerRiesgoLUC/mostrar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_primerRiesgoLUC', 'inicializaPrimerRiesgoLUC(<bean:write name="cotizacionForm" property="idToCotizacion" />);');"></div>		
		<div width="150px" id="validar" name="Validar" extraAction="sendRequest(null,'/MidasWeb/cotizacion/validar/mostrarValidar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_validar', 'inicializaAccordionValidarCotizacion(<bean:write name="cotizacionForm" property="idToCotizacion" />, \'cell2\');');" row="1" ></div>
		<div id="datosGenerales" name="Datos Generales" extraAction="cargaTabDatosGeneralesCot();" row="1"></div>
		<div width="150px" id="resumenIncisos" name="Datos de Incisos" extraAction="sendRequest(null,'/MidasWeb/cotizacion/inciso/listar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&origen=COT', 'contenido_resumenIncisos', null)" row="1"></div>
		<div width="250px" id="documentosDigitalesComplementarios" name="Documentos / Aclaraciones" extraAction="javascript: sendRequest(null, '/MidasWeb/cotizacion/listarDocumentosDigitalesComplementarios.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />','contenido_documentosDigitalesComplementarios',null);" row="1"></div>
		<div width="150px" id="datosLicitacion" name="Datos de licitaci&oacute;n" extraAction="javascript: sendRequest(null, '/MidasWeb/cotizacion/mostrarDatosLicitacion.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />','contenido_datosLicitacion',null);" row="1"></div>
</div>
<%--<div><a href="javascript: void(0);" onclick="javascript: sendRequest(null, '/MidasWeb/cotizacion/facultativo/mostrarCotizacionFacultativo.do?idToCotizacion=1&idTcSubRamo=1', 'configuracion_detalle', null);">Cotizaci&oacute;n Facultativo</a></div>--%>
