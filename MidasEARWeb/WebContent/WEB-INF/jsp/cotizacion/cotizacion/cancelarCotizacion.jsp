<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>

<midas:formulario accion="/cotizacion/cotizacion/cancelarCotizacion">
	<table id="desplegar">
         	<tr>
	           	<td class="titulo" colspan="2"><midas:mensaje clave="midas.cotizacion.cancelarRechazar"/>
	           	<midas:oculto propiedadFormulario="idToCotizacion"/>
	           	</td>
         	</tr>
			<tr>
				<th><midas:mensaje clave="midas.ordendetrabajo.motivoRechazoCancelacion"/>*:</th>
				<td>
					<midas:escribeCatalogo size="" styleId="idMotivoRechazo" styleClass="cajaTexto" 
						propiedad="idMotivoRechazo" clase="mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionFacadeRemote"/>	
				</td>						
			</tr>
			<tr>
				<td class="campoRequerido">
		 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
				</td>	
				<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
						onclick="javascript: ocultarVentanaCot();">
						<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<midas:boton onclick="cancelarCotizacion(document.cotizacionForm);" tipo="guardar"/>
				</div>
			</td>
		</tr>
		</table>
</midas:formulario>