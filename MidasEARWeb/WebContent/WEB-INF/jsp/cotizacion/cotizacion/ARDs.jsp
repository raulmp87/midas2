<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="subtituloCotizacion"><midas:mensaje clave="configuracion.cobertura.ard.de"/>:<bean:write name="tituloCoberturas" scope="request" /> </div>
<div class="subtituloCotizacion">
	<midas:mensaje clave="configuracion.cobertura.cotizacion"/>: <midas:mensaje clave="midas.cotizacion.prefijo"/><bean:write name="idToCotizacionCadena" scope="request" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <bean:write name="fecha" scope="request" />
</div>
<div style="clear:both"></div>
<input type='hidden' id='idToCotizacion' value='<bean:write name="idToCotizacion" scope="request"/>' name=''/>
<input type='hidden' id='numeroInciso' value='<bean:write name="numeroInciso" scope="request"/>' name=''/>
<input type='hidden' id='idToSeccion' value='<bean:write name="idToSeccion" scope="request"/>' name=''/>
<input type='hidden' id='idToCobertura' value='<bean:write name="idToCobertura" scope="request"/>' name=''/>
<input type='hidden' id='claveTipoEndoso' value='<bean:write name="claveTipoEndoso" scope="request"/>' name=''/>

<div class="subtituloIzquierdaDiv"><bean:message key="configuracion.cobertura.ard.aumentos"/></div>
<table id="t_riesgo" width="100%">
	<tr>
		<th width="5%">
			&nbsp;
		</th>
		<th width="40%">
			<midas:mensaje clave="configuracion.cobertura.ard.descripcion"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.tipo"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.nivel"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.aplicacion"/>
		</th>
		<th width="20%">
			<midas:mensaje clave="configuracion.cobertura.ard.valor"/>
		</th>
		<th width="5%">
			&nbsp;
		</th>
	</tr>
	<nested:present name="aumentos" scope="request">
		<nested:iterate id="aumentosId" name="aumentos" scope="request" 
						type="mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO"
						indexId="indexVar">
			<tr>
				<td align="center">
					<nested:hidden property="id.idToAumentoVario" name="aumentosId" styleId='<%="idAumento[" + indexVar + "]"%>'></nested:hidden>
					<nested:lessThan value="0" property="aumentoVarioDTO.idAumentoVario" name="aumentosId">
						<nested:hidden name="aumentosId" property="claveContrato" styleId='<%="checkboxA[" + indexVar + "]"%>' value="on"/>
							<a href="javascript: void(0);"><img alt="" src="/MidasWeb/img/dhtmlxgrid/icons_books/item_chk0_dis.gif"> </a>					
					</nested:lessThan>
					<nested:greaterThan value="0" property="aumentoVarioDTO.idAumentoVario" name="aumentosId">
						<c:if test="${aumentosId.claveContrato == 1}">
							<c:set var="claveContrato" value="${aumentosId.claveContrato}"></c:set>
						</c:if>
						<nested:equal name="aumentosId" value="0" property="claveObligatoriedad">
							<nested:checkbox name="aumentosId" property="claveContrato" styleId='<%="checkboxA[" + indexVar + "]"%>' value="${claveContrato}" onclick='<%="saveARD(" + indexVar + ",\'A\')"%>'/>
						</nested:equal>				
						<nested:equal name="aumentosId" value="1"property="claveObligatoriedad">
							<nested:checkbox name="aumentosId" property="claveContrato" styleId='<%="checkboxA[" + indexVar + "]"%> ' value="${claveContrato}" onclick='<%="saveARD(" + indexVar + ",\'A\')"%>'/>
						</nested:equal>
						<nested:equal name="aumentosId" value="7" property="claveObligatoriedad">
							<nested:checkbox name="aumentosId" property="claveContrato" styleId='<%="checkboxA[" + indexVar + "]"%>' value="on" onclick='<%="saveARD(" + indexVar + ",\'A\')"%>'/>
						</nested:equal>
						<nested:equal name="aumentosId" value="8" property="claveObligatoriedad">
							<nested:checkbox name="aumentosId" property="claveContrato" styleId='<%="checkboxA[" + indexVar + "]"%>' value="on" disabled="true"/>
						</nested:equal>							
					</nested:greaterThan>
				</td>
				<td>
					<nested:hidden name="aumentosId" property="aumentoVarioDTO.descripcionAumento"/>
					<bean:write name="aumentosId" property="aumentoVarioDTO.descripcionAumento"/>
				</td>	
				<td>
					<nested:hidden name="aumentosId" property="claveNivel"/>	
					<nested:equal name="aumentosId" value="1" property="claveNivel">
						P
					</nested:equal>
					<nested:equal name="aumentosId" value="2" property="claveNivel">
						T
					</nested:equal>
					<nested:equal name="aumentosId" value="3" property="claveNivel">
						C
					</nested:equal>															
				</td>				
				<td>
					<nested:hidden name="aumentosId" property="aumentoVarioDTO.claveTipoAumento"/>	
					<nested:equal name="aumentosId" value="1" property="aumentoVarioDTO.claveTipoAumento">
						Comercial
					</nested:equal>
					<nested:equal name="aumentosId" value="2" property="aumentoVarioDTO.claveTipoAumento">
						T&eacute;cnico
					</nested:equal>					
				</td>
				<td>
					<nested:hidden name="aumentosId" property="aumentoVarioDTO.claveTipoAumento"/>
					<nested:equal name="aumentosId" value="1" property="aumentoVarioDTO.claveTipoAumento">
						%
					</nested:equal>
					<nested:equal name="aumentosId" value="2" property="aumentoVarioDTO.claveTipoAumento">
						$
					</nested:equal>
				</td>
				<td>
					<nested:text name="aumentosId" 
								 property="valorAumento"
								 disabled="true"
								 onblur='<%="setIcon(" + indexVar + ",\'A\')"%>'
								 styleId='<%="valorA[" + indexVar + "]"%>' 
								 styleClass="cajaTexto" readonly="true"/>
				</td>
				<td>
					<div id='<%="autorizacionA[" + indexVar + "]"%>'>
						<nested:hidden name="aumentosId" property="claveAutorizacion"/>	
						<nested:equal name="aumentosId" value="1" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Autorizaci&oacute;n Solicitada" title="Autorizaci&oacute;n Solicitada" src="/MidasWeb/img/ico_yel.gif"></a>
						</nested:equal>
						<nested:equal name="aumentosId" value="7"property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Autorizado" title="Autorizado" src="/MidasWeb/img/ico_green.gif"></a>
						</nested:equal>	
						<nested:equal name="aumentosId" value="8" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Rechazado" title="Rechazado" src="/MidasWeb/img/ico_red.gif"></a>
						</nested:equal>		
					</div>
				</td>
			</tr>
		</nested:iterate>
	</nested:present>
</table>	
<div class="subtituloIzquierdaDiv"><bean:message key="configuracion.cobertura.ard.recargos"/></div>
<table id="t_riesgo" width="100%">
	<tr>
		<th width="5%">
			&nbsp;
		</th>
		<th width="40%">
			<midas:mensaje clave="configuracion.cobertura.ard.descripcion"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.tipo"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.nivel"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.aplicacion"/>
		</th>		
		<th width="20%">
			<midas:mensaje clave="configuracion.cobertura.ard.valor"/>
		</th>		
		<th width="5%">
			&nbsp;
		</th>			
	</tr>
	<nested:present name="recargos" scope="request">
		<nested:iterate id="recargosId" name="recargos" scope="request" 
						type="mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO"
						indexId="indexVar">
			<tr>
				<td align="center">
					<nested:hidden property="id.idToRecargoVario" name="recargosId"  styleId='<%="idRecargo[" + indexVar + "]"%>'></nested:hidden>
					<c:choose>
						<c:when test="${recargosId.recargoVarioDTO.idtorecargovario < 0}">
							<nested:hidden name="recargosId" property="claveContrato" styleId='<%="checkboxR[" + indexVar + "]"%>' value='on'/>
								<a href="javascript: void(0);"><img alt="" src="/MidasWeb/img/dhtmlxgrid/icons_books/item_chk0_dis.gif"> </a>							
						</c:when>
						<c:otherwise>
							<c:if test="${recargosId.claveContrato == 1}">
								<c:set var="claveContrato" value="${recargosId.claveContrato}"></c:set>
							</c:if>
							<c:choose>
								<c:when test="${recargosId.claveObligatoriedad == 0}">
									<nested:checkbox name="recargosId" property="claveContrato" styleId='<%="checkboxR[" + indexVar + "]"%>' value="${claveContrato}" onclick='<%="saveARD(" + indexVar + ",\'R\')"%>'/>	
								</c:when>
								<c:when test="${recargosId.claveObligatoriedad == 1}">
									<nested:checkbox name="recargosId" property="claveContrato" styleId='<%="checkboxR[" + indexVar + "]"%>' value="${claveContrato}" onclick='<%="saveARD(" + indexVar + ",\'R\')"%>'/>	
								</c:when>
								<c:when test="${recargosId.claveObligatoriedad == 7}">
									<nested:checkbox name="recargosId" property="claveContrato" styleId='<%="checkboxR[" + indexVar + "]"%>' value="on" onclick='<%="saveARD(" + indexVar + ",\'R\')"%>'/>	
								</c:when>
								<c:otherwise>
									<nested:hidden name="recargosId" property="claveContrato" styleId='<%="checkboxR[" + indexVar + "]"%>' value="on"/>
									<a href="void(0)"><img alt="" src="/MidasWeb/img/dhtmlxgrid/icons_books/item_chk1_dis.gif"> </a>								
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>				
				</td>
				<td>
					<nested:hidden name="recargosId" property="recargoVarioDTO.descripcionrecargo"/>	
					<bean:write name="recargosId" property="recargoVarioDTO.descripcionrecargo"/>
				</td>	
				<td>
					<nested:hidden name="recargosId" property="claveNivel"/>	
					<nested:equal name="recargosId" value="1" property="claveNivel">
						P
					</nested:equal>
					<nested:equal name="recargosId" value="2" property="claveNivel">
						T
					</nested:equal>
					<nested:equal name="recargosId" value="3" property="claveNivel">
						C
					</nested:equal>															
				</td>				
				<td>
					<nested:hidden name="recargosId" property="recargoVarioDTO.clavetiporecargo"/>	
					<nested:equal name="recargosId" value="1" property="recargoVarioDTO.clavetiporecargo">
						Comercial
					</nested:equal>
					<nested:equal name="recargosId" value="2" property="recargoVarioDTO.clavetiporecargo">
						T&eacute;cnico
					</nested:equal>					
				</td>
				<td>
					<nested:hidden name="recargosId" property="recargoVarioDTO.clavetiporecargo"/>	
					<nested:equal name="recargosId" value="1" property="recargoVarioDTO.clavetiporecargo">
						%
					</nested:equal>
					<nested:equal name="recargosId" value="2" property="recargoVarioDTO.clavetiporecargo">
						$
					</nested:equal>	
				</td>
				<td>
					<nested:text name="recargosId" 
								property="valorRecargo"
								disabled="true"
								onblur='<%="setIcon(" + indexVar + ",\'R\')"%>' 
								styleId='<%="valorR[" + indexVar + "]"%>' 
								styleClass="cajaTexto" readonly="true"/>
				</td>
				<td>
					<div id='<%="autorizacionR[" + indexVar + "]"%>'>
						<nested:hidden name="recargosId" property="claveAutorizacion"/>	
						<nested:equal name="recargosId" value="1" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Autorizaci&oacute;n Solicitada" title="Autorizaci&oacute;n Solicitada" src="/MidasWeb/img/ico_yel.gif"></a>
						</nested:equal>
						<nested:equal name="recargosId" value="7" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Autorizado" title="Autorizado" src="/MidasWeb/img/ico_green.gif"></a>
						</nested:equal>	
						<nested:equal name="recargosId" value="8" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Rechazado" title="Rechazado" src="/MidasWeb/img/ico_red.gif"></a>
						</nested:equal>		
					</div>
				</td>				
			</tr>
		</nested:iterate>
	</nested:present>
</table>	
<div class="subtituloIzquierdaDiv"><bean:message key="configuracion.cobertura.ard.descuentos"/></div>
<table id="t_riesgo" width="100%">
	<tr>
		<th width="5%">
			&nbsp;
		</th>
		<th width="40%">
			<midas:mensaje clave="configuracion.cobertura.ard.descripcion"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.tipo"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.nivel"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.aplicacion"/>
		</th>		
		<th width="20%">
			<midas:mensaje clave="configuracion.cobertura.ard.valor"/>
		</th>		
		<th width="5%">
			&nbsp;
		</th>			
	</tr>
	<nested:present name="descuentos" scope="request">
		<nested:iterate id="descuentosId" name="descuentos" scope="request" 
						type="mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO" 
						indexId="indexVar">
			<tr>
				<td align="center">
					<nested:hidden property="id.idToDescuentoVario" name="descuentosId" styleId='<%="idDescuento[" + indexVar + "]"%>'></nested:hidden>
					<c:choose>
						<c:when test="${descuentosId.descuentoDTO.idToDescuentoVario < 0}">
							<nested:hidden name="descuentosId" property="claveContrato" styleId='<%="checkboxD[" + indexVar + "]"%>' value='on'/>
								<a href="javascript: void(0);"><img alt="" src="/MidasWeb/img/dhtmlxgrid/icons_books/item_chk0_dis.gif"> </a>							
						</c:when>
						<c:otherwise>
							<c:if test="${descuentosId.claveContrato == 1}">
								<c:set var="claveContrato" value="${descuentosId.claveContrato}"></c:set>
							</c:if>
							<c:choose>
								<c:when test="${descuentosId.claveObligatoriedad == 0}">
									<nested:checkbox name="descuentosId" property="claveContrato" styleId='<%="checkboxD[" + indexVar + "]"%>' value="${claveContrato}" onclick='<%="saveARD(" + indexVar + ",\'D\')"%>' />	
								</c:when>
								<c:when test="${descuentosId.claveObligatoriedad == 1}">
									<nested:checkbox name="descuentosId" property="claveContrato" styleId='<%="checkboxD[" + indexVar + "]"%>' value="${claveContrato}" onclick='<%="saveARD(" + indexVar + ",\'D\')"%>'/>	
								</c:when>
								<c:when test="${descuentosId.claveObligatoriedad == 7}">
									<nested:checkbox name="descuentosId" property="claveContrato" styleId='<%="checkboxD[" + indexVar + "]"%>' value="on"  onclick='<%="saveARD(" + indexVar + ",\'D\')"%>' />	
								</c:when>
								<c:otherwise>
									<nested:hidden name="descuentosId" property="claveContrato" styleId='<%="checkboxD[" + indexVar + "]"%>' value="on"/>
									<a href="void(0)"><img alt="" src="/MidasWeb/img/dhtmlxgrid/icons_books/item_chk1_dis.gif"> </a>								
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</td>
				<td>
					<nested:hidden name="descuentosId" property="descuentoDTO.descripcion"/>	
					<bean:write name="descuentosId" property="descuentoDTO.descripcion"/>
				</td>	
				<td>
					<nested:hidden name="descuentosId" property="claveNivel"/>	
					<nested:equal name="descuentosId" value="1" property="claveNivel">
						P
					</nested:equal>
					<nested:equal name="descuentosId" value="2" property="claveNivel">
						T
					</nested:equal>
					<nested:equal name="descuentosId" value="3" property="claveNivel">
						C
					</nested:equal>															
				</td>				
				<td>
					<nested:hidden name="descuentosId" property="descuentoDTO.claveTipo"/>	
					<nested:equal name="descuentosId" value="1" property="descuentoDTO.claveTipo">
						Comercial
					</nested:equal>
					<nested:equal name="descuentosId" value="2" property="descuentoDTO.claveTipo">
						T&eacute;cnico
					</nested:equal>					
				</td>
				<td>
					<nested:hidden name="descuentosId" property="descuentoDTO.claveTipo"/>	
					<nested:equal name="descuentosId" value="1"property="descuentoDTO.claveTipo">
						%
					</nested:equal>
					<nested:equal name="descuentosId" value="2" property="descuentoDTO.claveTipo">
						$
					</nested:equal>	
				</td>
				<td>
					<nested:text name="descuentosId" 
								property="valorDescuento"
								disabled="true"
								onblur='<%="setIcon(" + indexVar + ",\'D\')"%>' 
								styleId='<%="valorD[" + indexVar + "]"%>' 
								styleClass="cajaTexto"/>
				</td>
				<td>
					<div id='<%="autorizacionD[" + indexVar + "]"%>'>
						<nested:hidden name="descuentosId" property="claveAutorizacion"/>	
						<nested:equal name="descuentosId" value="1" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Autorizaci&oacute;n Solicitada" title="Autorizaci&oacute;n Solicitada" src="/MidasWeb/img/ico_yel.gif"></a>
						</nested:equal>
						<nested:equal name="descuentosId" value="7"property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Autorizado" title="Autorizado" src="/MidasWeb/img/ico_green.gif"></a>
						</nested:equal>	
						<nested:equal name="descuentosId" value="8" property="claveAutorizacion">
							<a href="javascript: void(0);"><img alt="Rechazado" title="Rechazado" src="/MidasWeb/img/ico_red.gif"></a>
						</nested:equal>		
					</div>
				</td>				
			</tr>
		</nested:iterate>
	</nested:present>
</table>
<div id="botonCalcular">
	<div class="alinearBotonALaDerecha">
		<midas:boton onclick="javascript: regresarACoberturas();" tipo="regresar" texto="regresar"/>	
		<midas:boton onclick="javascript: sendRequest(null, '/MidasWeb/cotizacion/cobertura/mostrarAutorizarARD.do?idToCotizacion='+idToCotizacion.value+'&numeroInciso='+numeroInciso.value+'&idToSeccion='+idToSeccion.value+'&idToCobertura='+idToCobertura.value, 'configuracion_detalle', null);" tipo="guardar" texto="Autorizaciones" />
	</div>
</div>