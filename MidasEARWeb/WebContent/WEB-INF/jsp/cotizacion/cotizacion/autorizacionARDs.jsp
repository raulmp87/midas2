<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@page import="mx.com.afirme.midas.sistema.Sistema"%>

<div class="subtituloCotizacion"><midas:mensaje clave="configuracion.cobertura.ard.de"/>:<bean:write name="tituloCoberturas" scope="request" /> </div>
<div class="subtituloCotizacion">
	<midas:mensaje clave="configuracion.cobertura.cotizacion"/>: <midas:mensaje clave="midas.cotizacion.prefijo"/><bean:write name="idToCotizacionCadena" scope="request" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <bean:write name="fecha" scope="request" />
</div>
<div style="clear:both"></div>
<input type='hidden' id='idToCotizacion' value='<bean:write name="idToCotizacion" scope="request"/>' name=''/>
<input type='hidden' id='numeroInciso' value='<bean:write name="numeroInciso" scope="request"/>' name=''/>
<input type='hidden' id='idToSeccion' value='<bean:write name="idToSeccion" scope="request"/>' name=''/>
<input type='hidden' id='idToCobertura' value='<bean:write name="idToCobertura" scope="request"/>' name=''/>
<input type='hidden' id='aumentosSize' value='<bean:write name="aumentosSize" scope="request"/>' name=''/>
<input type='hidden' id='descuentosSize' value='<bean:write name="descuentosSize" scope="request"/>' name=''/>
<input type='hidden' id='recargosSize' value='<bean:write name="recargosSize" scope="request"/>' name=''/>
<input type='hidden' id='claveTipoEndoso' value='<bean:write name="claveTipoEndoso" scope="request"/>' name=''/>

<div class="subtituloIzquierdaDiv"><bean:message key="configuracion.cobertura.ard.aumentos"/></div>
<table id="t_riesgo" width="100%">
	<tr>
		<th width="5%">
			&nbsp;
		</th>
		<th width="40%">
			<midas:mensaje clave="configuracion.cobertura.ard.descripcion"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.tipo"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.nivel"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.aplicacion"/>
		</th>		
		<th width="20%">
			<midas:mensaje clave="configuracion.cobertura.ard.valor"/>
		</th>	
		<th width="5%">
			&nbsp;
		</th>	
	</tr>
	<nested:present name="aumentos" scope="request">
		<nested:iterate id="aumentosId" name="aumentos" scope="request" 
						type="mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO"
						indexId="indexVar">
			<tr>
				<td>
					<nested:hidden property="id.idToAumentoVario" name="aumentosId" styleId='<%="idAumento[" + indexVar + "]"%>'></nested:hidden>
					<nested:checkbox name="aumentosId" property="claveContrato" styleId='<%="checkboxA[" + indexVar + "]"%>' value="on" disabled="true"/>
				</td>
				<td>
					<nested:hidden name="aumentosId" property="aumentoVarioDTO.descripcionAumento"/>	
					<bean:write name="aumentosId" property="aumentoVarioDTO.descripcionAumento"/>
				</td>	
				<td>
					<nested:hidden name="aumentosId" property="claveComercialTecnico"/>	
					<nested:equal name="aumentosId" value="1" property="claveComercialTecnico">
						Comercial
					</nested:equal>
					<nested:equal name="aumentosId" value="2" property="claveComercialTecnico">
						T&eacute;cnico
					</nested:equal>					
				</td>				
				<td>
					<nested:hidden name="aumentosId" property="claveNivel"/>	
					<nested:equal name="aumentosId" value="1" property="claveNivel">
						P
					</nested:equal>
					<nested:equal name="aumentosId" value="2" property="claveNivel">
						T
					</nested:equal>
					<nested:equal name="aumentosId" value="3" property="claveNivel">
						C
					</nested:equal>															
				</td>				
				<td>
					<nested:hidden name="aumentosId" property="aumentoVarioDTO.claveTipoAumento"/>	
					<nested:equal name="aumentosId" value="1" property="aumentoVarioDTO.claveTipoAumento">
						%
					</nested:equal>
					<nested:equal name="aumentosId" value="2" property="aumentoVarioDTO.claveTipoAumento">
						$
					</nested:equal>	
				</td>
				<td>
					<nested:text name="aumentosId" 
								 property="valorAumento"
								 disabled="true"
								 onblur='<%="setIcon(" + indexVar + ",\'A\')"%>'
								 styleId='<%="valorA[" + indexVar + "]"%>' 
								 styleClass="cajaTexto" readonly="true"/>
				</td>
				<td>
					<nested:equal name="aumentosId" value="1" property="claveAutorizacion">
						<nested:checkbox name="aumentosId" property="claveContrato" styleId='<%="checkboxAutA[" + indexVar + "]"%>'/>
					</nested:equal>
				</td>
			</tr>
		</nested:iterate>
	</nested:present>
</table>	
<div class="subtituloIzquierdaDiv"><bean:message key="configuracion.cobertura.ard.recargos"/></div>
<table id="t_riesgo" width="100%">
	<tr>
		<th width="5%">
			&nbsp;
		</th>
		<th width="40%">
			<midas:mensaje clave="configuracion.cobertura.ard.descripcion"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.tipo"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.nivel"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.aplicacion"/>
		</th>		
		<th width="20%">
			<midas:mensaje clave="configuracion.cobertura.ard.valor"/>
		</th>		
		<th width="5%">
			&nbsp;
		</th>			
	</tr>
	<nested:present name="recargos" scope="request">
		<nested:iterate id="recargosId" name="recargos" scope="request" 
						type="mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO"
						indexId="indexVar">
			<tr>
				<td>
					<nested:hidden property="id.idToRecargoVario" name="recargosId"  styleId='<%="idRecargo[" + indexVar + "]"%>'></nested:hidden>
					<nested:checkbox name="recargosId" property="claveContrato" styleId='<%="checkboxR[" + indexVar + "]"%>' value="on" disabled="true"/>
				</td>
				<td>
					<nested:hidden name="recargosId" property="recargoVarioDTO.descripcionrecargo"/>	
					<bean:write name="recargosId" property="recargoVarioDTO.descripcionrecargo"/>
				</td>				
				<td>
					<nested:hidden name="recargosId" property="claveComercialTecnico"/>	
					<nested:equal name="recargosId" value="1" property="claveComercialTecnico">
						Comercial
					</nested:equal>
					<nested:equal name="recargosId" value="2" property="claveComercialTecnico">
						T&eacute;cnico
					</nested:equal>					
				</td>
				<td>
					<nested:hidden name="recargosId" property="claveNivel"/>	
					<nested:equal name="recargosId" value="1" property="claveNivel">
						P
					</nested:equal>
					<nested:equal name="recargosId" value="2" property="claveNivel">
						T
					</nested:equal>
					<nested:equal name="recargosId" value="3" property="claveNivel">
						C
					</nested:equal>															
				</td>					
				<td>
					<nested:hidden name="recargosId" property="recargoVarioDTO.clavetiporecargo"/>	
					<nested:equal name="recargosId" value="1" property="recargoVarioDTO.clavetiporecargo">
						%
					</nested:equal>
					<nested:equal name="recargosId" value="2" property="recargoVarioDTO.clavetiporecargo">
						$
					</nested:equal>	
				</td>
				<td>
					<nested:text name="recargosId" 
								property="valorRecargo"
								disabled="true"
								onblur='<%="setIcon(" + indexVar + ",\'R\')"%>' 
								styleId='<%="valorR[" + indexVar + "]"%>' 
								styleClass="cajaTexto" readonly="true"/>
				</td>
				<td>
					<nested:equal name="recargosId" value="1" property="claveAutorizacion">
						<nested:checkbox name="recargosId" property="claveContrato" styleId='<%="checkboxAutR[" + indexVar + "]"%>'/>
					</nested:equal>
				</td>				
			</tr>
		</nested:iterate>
	</nested:present>
</table>	
<div class="subtituloIzquierdaDiv"><bean:message key="configuracion.cobertura.ard.descuentos"/></div>
<table id="t_riesgo" width="100%">
	<tr>
		<th width="5%">
			&nbsp;
		</th>
		<th width="40%">
			<midas:mensaje clave="configuracion.cobertura.ard.descripcion"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.tipo"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.nivel"/>
		</th>
		<th width="10%">
			<midas:mensaje clave="configuracion.cobertura.ard.aplicacion"/>
		</th>		
		<th width="20%">
			<midas:mensaje clave="configuracion.cobertura.ard.valor"/>
		</th>		
		<th width="5%">
			&nbsp;
		</th>			
	</tr>
	<nested:present name="descuentos" scope="request">
		<nested:iterate id="descuentosId" name="descuentos" scope="request" 
						type="mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO" 
						indexId="indexVar">
			<tr>
				<td>
					<nested:hidden property="id.idToDescuentoVario" name="descuentosId" styleId='<%="idDescuento[" + indexVar + "]"%>'></nested:hidden>
					<nested:checkbox name="descuentosId" property="claveContrato" styleId='<%="checkboxD[" + indexVar + "]"%>' value="on" disabled="true"/>
				</td>
				<td>
					<nested:hidden name="descuentosId" property="descuentoDTO.descripcion"/>	
					<bean:write name="descuentosId" property="descuentoDTO.descripcion"/>
				</td>					
				<td>
					<nested:hidden name="descuentosId" property="claveComercialTecnico"/>	
					<nested:equal name="descuentosId" value="1" property="claveComercialTecnico">
						Comercial
					</nested:equal>
					<nested:equal name="descuentosId" value="2" property="claveComercialTecnico">
						T&eacute;cnico
					</nested:equal>					
				</td>
				<td>
					<nested:hidden name="descuentosId" property="claveNivel"/>	
					<nested:equal name="descuentosId" value="1" property="claveNivel">
						P
					</nested:equal>
					<nested:equal name="descuentosId" value="2" property="claveNivel">
						T
					</nested:equal>
					<nested:equal name="descuentosId" value="3" property="claveNivel">
						C
					</nested:equal>															
				</td>				
				<td>
					<nested:hidden name="descuentosId" property="descuentoDTO.claveTipo"/>	
					<nested:equal name="descuentosId" value="1"property="descuentoDTO.claveTipo">
						%
					</nested:equal>
					<nested:equal name="descuentosId" value="2" property="descuentoDTO.claveTipo">
						$
					</nested:equal>	
				</td>
				<td>
					<nested:text name="descuentosId" 
								property="valorDescuento"
								disabled="true"
								onblur='<%="setIcon(" + indexVar + ",\'D\')"%>' 
								styleId='<%="valorD[" + indexVar + "]"%>' 
								styleClass="cajaTexto"/>
				</td>
				<td>
					<nested:equal name="descuentosId" value="1" property="claveAutorizacion">
						<nested:checkbox name="descuentosId" property="claveContrato" styleId='<%="checkboxAutD[" + indexVar + "]"%>'/>
					</nested:equal>				
				</td>				
			</tr>
		</nested:iterate>
	</nested:present>
</table>
<div id="botonCalcular">
	<div class="alinearBotonALaDerecha">
		<midas:boton onclick="javascript: autorizarARD()" tipo="agregar" texto="Autorizar"/>
		<midas:boton onclick="javascript: rechazarARD()" tipo="agregar" texto="Rechazar"/>	
		<midas:boton onclick="javascript: regresarACoberturas();" tipo="regresar" texto="regresar"/>	
	</div>
</div>