<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<midas:formulario accion="/cotizacion/inciso/agregar">
	<html:hidden property="idCotizacion" name="incisoCotizacionForm" styleId="idCotizacion"/>
	<input type='hidden' id='origen' value='<bean:write name="origen" scope="session"/>' name='origen'/>
	<input type='hidden' id='mensaje' value='Debe introducir todos los datos. Favor de verificar.' name='mensaje'/>
	<input type='hidden' id='tipoMensaje' value='20' name='tipoMensaje'/>	
	<nested:equal value="ODT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Orden de Trabajo </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:  <%= "ODT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>
	</nested:equal>
	<nested:equal value="COT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Cotizaci&oacute;n </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.cotizacion.cotizacion"/>:  <%= "COT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>		
	<div style="clear:both"></div>	
	
	<table id="desplegarDetalle">
		<tr>
			<td>
				<div id="ubicacion">
					<table id="agregar">
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.nombreCalle"/>:</th>
							<td colspan="4">
								<midas:escribe propiedad="direccionGeneral.nombreCalle" nombre="incisoCotizacionForm"/>
								<midas:oculto propiedadFormulario="direccionGeneral.idToDireccion"/>
							</td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.numeroExterior"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.numeroExterior" nombre="incisoCotizacionForm"/></td>
							<th class="normal"><midas:mensaje clave="midas.direccion.numeroInterior"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.numeroInterior" nombre="incisoCotizacionForm"/></td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.entreCalles"/>:</th>
							<td colspan="4"><midas:escribe propiedad="direccionGeneral.entreCalles" nombre="incisoCotizacionForm"/></td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.nombreColonia"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.colonia" nombre="incisoCotizacionForm"/><midas:oculto propiedadFormulario="direccionGeneral.idColonia"/> </td>
							<th class="normal"><midas:mensaje clave="midas.direccion.idMunicipio"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.municipio" nombre="incisoCotizacionForm"/> <midas:oculto propiedadFormulario="direccionGeneral.idMunicipio" /></td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.idEstado"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.estado" nombre="incisoCotizacionForm"/> <midas:oculto propiedadFormulario="direccionGeneral.idEstado"/> </td>
							<th class="normal"><midas:mensaje clave="midas.direccion.codigoPostal"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.codigoPostal" nombre="incisoCotizacionForm"/></td>
						</tr>
						<tr>
							<td colspan="6">
								<nested:empty property="direccionGeneral.idToDireccion" name="incisoCotizacionForm">
									<a href="javascript: void(0);" onclick="mostrarRegistroDireccion(0, 'incisoCotizacion', 0, 0)">Agregar direccion</a>
								</nested:empty>
								<nested:greaterThan value="0" property="direccionGeneral.idToDireccion" name="incisoCotizacionForm">
									<a href="javascript: void(0);" onclick="mostrarRegistroDireccion(<midas:escribe propiedad="direccionGeneral.idToDireccion" nombre="incisoCotizacionForm"/>, 'incisoCotizacion', 0, 0)">Actualizar direccion</a>
								</nested:greaterThan>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<table id= "desplegarDetalle">
					<th width="25%" class="normal">Bien o giro asegurado: </th>
					<td width="75%">
						<midas:areatexto id="descripcionGiroAsegurado"
							propiedadFormulario="descripcionGiroAsegurado"
							nombreFormulario="incisoCotizacionForm" caracteres="250"
							onKeyDown="if(event.keyCode == 8 || event.keyCode == 46) return true; if(this.value.length == 250) return false;" />
					</td>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="datosInciso"></div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="botonAgregar">
		<div class="alinearBotonALaDerecha">			
			<nested:equal value="ODT" name="origen" scope="session">
				<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/mostrarODTDetalle.do?id=${idCotizacion}&origen=ODT', 'configuracion_detalle','setTabIncisosODT(${idCotizacion});');" tipo="regresar"/>			
				<midas:boton onclick="javascript: validarDatosInciso(\'ODT\','agregar');" tipo="guardar"/>
			</nested:equal>
			<nested:equal value="COT" name="origen" scope="session">
				<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/mostrarDetalle.do?id=${idCotizacion}', 'configuracion_detalle','setTabIncisosCOT(${idCotizacion});');" tipo="regresar"/>
				<midas:boton onclick="javascript: validarDatosInciso(\'COT\','agregar');" tipo="guardar"/>			
			</nested:equal>					
		<%--
			<div width="150px" id="resumenIncisos" name="Resumen de Incisos" extraAction="sendRequest(null,'/MidasWeb/cotizacion/inciso/listar.do?id=<bean:write name="idToCotizacion"/>', 'contenido_resumenIncisos', null)"></div>
			configuracionProductoTabBar.setTabActive('detalle');					
		--%>		
		</div>
	</div>	
</midas:formulario>
