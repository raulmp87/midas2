<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<midas:formulario accion="/cotizacion/inciso/mostrarCargaMasiva">
		<html:hidden property="mensaje" styleId="mensajeInciso"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensajeInciso"/>
	<midas:oculto propiedadFormulario="idCotizacion" nombreFormulario="incisoCotizacionForm"/>
	<input type='hidden' id='origen' value='<bean:write name="origen" scope="session"/>' name='origen'/>

	<table id="desplegarDetalle">
		<tr>
			<td class="titulo" width="100%" colspan="3">Carga Masiva</td>
		</tr>
		<tr>
			<td width="100%" colspan="3">
				<div id="cargaMasivaGrid" class="dataGridCompletePageClass"></div>
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="1">&nbsp;</td>
			<td width="50%" colspan="2">
				<table width="100%">
					<td>
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: confirmaBorrarCarga(${idCotizacion},'${origen}');" tipo="borrar" texto="Borrar Carga"/>
						</div>
					</td>
					<td>
						<c:if test="${incisoCotizacionForm.cargaMasivaValida == 'true'}">						
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: procesarCargaMasiva(${idCotizacion},'${origen}');" tipo="continuar" texto="Procesar Carga"/>
							</div>
						</c:if>					
					</td>
					<td>
						<div class="alinearBotonALaDerecha">
							<midas:boton onclick="javascript: cargaMasivaProcessor.sendData();" tipo="guardar" texto="Guardar"/>						
						</div>					
					</td>
				</table>
			</td>
		</tr>
	</table>		
</midas:formulario>
