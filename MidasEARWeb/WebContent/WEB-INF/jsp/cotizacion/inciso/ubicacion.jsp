<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacion/inciso/mostrarUbicacion">
	<table id="desplegar">
		<tr>
			<th class="normal"><midas:mensaje clave="midas.direccion.nombreCalle"/>:</th>
			<td colspan="4">
				<midas:escribe propiedad="direccionGeneral.nombreCalle" nombre="incisoCotizacionForm"/>
				<midas:oculto propiedadFormulario="direccionGeneral.idToDireccion"/>
			</td>
		</tr>
		<tr>
			<th class="normal"><midas:mensaje clave="midas.direccion.numeroExterior"/>:</th>
			<td><midas:escribe propiedad="direccionGeneral.numeroExterior" nombre="incisoCotizacionForm"/></td>
			<th class="normal"><midas:mensaje clave="midas.direccion.numeroInterior"/>:</th>
			<td><midas:escribe propiedad="direccionGeneral.numeroInterior" nombre="incisoCotizacionForm"/></td>
		</tr>
		<tr>
			<th class="normal"><midas:mensaje clave="midas.direccion.entreCalles"/>:</th>
			<td colspan="4"><midas:escribe propiedad="direccionGeneral.entreCalles" nombre="incisoCotizacionForm"/></td>
		</tr>
		<tr>
			<th class="normal"><midas:mensaje clave="midas.direccion.nombreColonia"/>:</th>
			<td><midas:escribe propiedad="direccionGeneral.colonia" nombre="incisoCotizacionForm"/><midas:oculto propiedadFormulario="direccionGeneral.idColonia"/> </td>
			<th class="normal"><midas:mensaje clave="midas.direccion.idMunicipio"/>:</th>
			<td><midas:escribe propiedad="direccionGeneral.municipio" nombre="incisoCotizacionForm"/> <midas:oculto propiedadFormulario="direccionGeneral.idMunicipio" /></td>
		</tr>
		<tr>
			<th class="normal"><midas:mensaje clave="midas.direccion.idEstado"/>:</th>
			<td><midas:escribe propiedad="direccionGeneral.estado" nombre="incisoCotizacionForm"/> <midas:oculto propiedadFormulario="direccionGeneral.idEstado"/> </td>
			<th class="normal"><midas:mensaje clave="midas.direccion.codigoPostal"/>:</th>
			<td><midas:escribe propiedad="direccionGeneral.codigoPostal" nombre="incisoCotizacionForm"/></td>
		</tr>
		<tr>
			<td colspan="6">
				<a href="javascript: void(0);" onclick="mostrarRegistroDireccion(<midas:escribe propiedad="direccionGeneral.idToDireccion" nombre="incisoCotizacionForm"/>, 'incisoCotizacion', 0, 0, null)">Actualizar Direcci&oacute;n</a>
			</td>
		</tr>
	</table>
</midas:formulario>