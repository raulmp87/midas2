<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/cotizacion/inciso/listar">
		<html:hidden property="mensaje" styleId="mensajeInciso"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensajeInciso"/>
	<midas:oculto propiedadFormulario="idCotizacion" nombreFormulario="incisoCotizacionForm"/>	
	<input type='hidden' id='origen' value='<bean:write name="origen" scope="session"/>' name='origen'/>
	<nested:equal value="ODT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Orden de Trabajo </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:  <%= "ODT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>
	<nested:equal value="COT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Cotizaci&oacute;n </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.cotizacion.cotizacion"/>:  <%= "COT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>		
	<div style="clear:both"></div>	
	</br>
	<div id="resultados">
		<nested:equal value="COT" name="origen" scope="session">
			<display:table name="incisos" sort="page" defaultsort="1" defaultorder="ascending" pagesize="10" cellpadding="0" cellspacing="0"
				 requestURI="/cotizacion/inciso/listar.do?id=<bean:write name='incisoCotizacionForm' property='idCotizacion'/>" id="incisos" decorator="mx.com.afirme.midas.decoradores.IncisoCotizacion" class="tablaConResultados" >
				<display:column property="id.numeroInciso" title="Inciso" />
				<display:column property="ubicacion" />
				<display:column property="valorPrimaNeta" title="Prima Neta"  format="{0,number,$#,##0.00}"/>
				<display:column property="acciones" title="Acciones"/>
			</display:table>
		</nested:equal>
		<nested:equal value="ODT" name="origen" scope="session">
			<display:table name="incisos" sort="page" defaultsort="1" defaultorder="ascending" pagesize="10" cellpadding="0" cellspacing="0"
				 requestURI="/cotizacion/inciso/listar.do?id=<bean:write name='incisoCotizacionForm' property='idCotizacion'/>" id="incisos" decorator="mx.com.afirme.midas.decoradores.IncisoCotizacion" class="tablaConResultados" >
				<display:column property="id.numeroInciso" title="Inciso" />
				<display:column property="ubicacion" />
				<display:column property="acciones" title="Acciones"/>
			</display:table>
		</nested:equal>
	</div>
	</br>
	<div class="alinearBotonALaDerecha">
		<div id="botonAgregar">
			<c:if test="${not empty incisos}">
				<c:choose>
					<c:when test="${incisoCotizacionForm.cargaMasivaExistente == 'true'}">
						<midas:boton onclick="javascript: mostrarCargaMasiva(${idCotizacion},'${origen}');" tipo="agregar" texto="Carga Masiva" style="width: 100px;"/>	
					</c:when>
					<c:otherwise>
						<midas:boton onclick="javascript: mostrarCargaMasivaIncisosWindow(${idCotizacion}, '${origen}');" tipo="agregar" texto="Carga Masiva" style="width: 100px;"/>					
					</c:otherwise>
				</c:choose>
			</c:if>
			<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/inciso/mostrarAgregar.do', 'configuracion_detalle', 'mostrarDatosInciso(${idCotizacion}, 0, 0, 0);');" tipo="agregar"/>
		</div>
	</div>		
</midas:formulario>
