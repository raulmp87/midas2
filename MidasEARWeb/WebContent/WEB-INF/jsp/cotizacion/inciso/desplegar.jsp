<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<midas:formulario accion="/cotizacion/inciso/agregar">
	<midas:oculto propiedadFormulario="idCotizacion" nombreFormulario="incisoCotizacionForm"/>
	<midas:oculto propiedadFormulario="numeroInciso" nombreFormulario="incisoCotizacionForm"/>
	<input type='hidden' id='origen' value='<bean:write name="origen" scope="session"/>' name='origen'/>
	<nested:equal value="ODT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Orden de Trabajo </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:  <%= "ODT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>
	<nested:equal value="COT" name="origen" scope="session">
		<div class="subtituloCotizacion">Incisos de la Cotizaci&oacute;n </div>
		<div class="subtituloCotizacion">
			<midas:mensaje clave="midas.cotizacion.cotizacion"/>:  <%= "COT-" + String.format("%08d", new Object[]{session.getAttribute("idCotizacion")}) %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<midas:mensaje clave="configuracion.cobertura.fecha"/>: <fmt:formatDate value="${incisoCotizacionForm.fechaOrdenTrabajo}" dateStyle="short" />
		</div>		
	</nested:equal>		
	<div style="clear:both"></div>	
	<table id="desplegarDetalla">
		<tr>
			<td colspan="6">
				<div id="ubicacion">
					<table id="modificar">
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.nombreCalle"/>:</th>
							<td colspan="4">
								<midas:escribe propiedad="direccionGeneral.nombreCalle" nombre="incisoCotizacionForm"/>
								<midas:oculto propiedadFormulario="direccionGeneral.idToDireccion"/>
							</td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.numeroExterior"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.numeroExterior" nombre="incisoCotizacionForm"/></td>
							<th class="normal"><midas:mensaje clave="midas.direccion.numeroInterior"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.numeroInterior" nombre="incisoCotizacionForm"/></td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.entreCalles"/>:</th>
							<td colspan="4"><midas:escribe propiedad="direccionGeneral.entreCalles" nombre="incisoCotizacionForm"/></td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.nombreColonia"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.colonia" nombre="incisoCotizacionForm"/><midas:oculto propiedadFormulario="direccionGeneral.idColonia"/> </td>
							<th class="normal"><midas:mensaje clave="midas.direccion.idMunicipio"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.municipio" nombre="incisoCotizacionForm"/> <midas:oculto propiedadFormulario="direccionGeneral.idMunicipio" /></td>
						</tr>
						<tr>
							<th class="normal"><midas:mensaje clave="midas.direccion.idEstado"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.estado" nombre="incisoCotizacionForm"/> <midas:oculto propiedadFormulario="direccionGeneral.idEstado"/> </td>
							<th class="normal"><midas:mensaje clave="midas.direccion.codigoPostal"/>:</th>
							<td><midas:escribe propiedad="direccionGeneral.codigoPostal" nombre="incisoCotizacionForm"/></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<table id= "desplegarDetalle">
					<th width="25%" class="normal">Bien o giro asegurado: </th>
					<td width="75%">
						<midas:areatexto id="descripcionGiroAsegurado"
							propiedadFormulario="descripcionGiroAsegurado"
							nombreFormulario="incisoCotizacionForm" deshabilitado="true" />
					</td>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div id="datosInciso"></div>
			</td>
		</tr>
		<tr>
			<td class="guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<nested:equal value="ODT" name="origen" scope="session">
						<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/mostrarODTDetalle.do?id=${idCotizacion}&origen=ODT', 'configuracion_detalle','setTabIncisosODT(${idCotizacion});');" tipo="regresar"/>			
					</nested:equal>
					<nested:equal value="COT" name="origen" scope="session">
						<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/mostrarDetalle.do?id=${idCotizacion}', 'configuracion_detalle','setTabIncisosCOT(${idCotizacion});');" tipo="regresar"/>		
					</nested:equal>		
									
					
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
