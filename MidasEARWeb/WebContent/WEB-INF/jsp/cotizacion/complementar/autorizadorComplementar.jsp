<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<midas:formulario accion="/cotizacion/complementar/guardar">
		<div id="mensajes">
			<html:hidden property="mensaje" styleId="mensaje"/>
			<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		</div>
		<table>
			<tr>
				<td><div class="titulo">Complementar cotizacion</td>
			</tr>
		</table>
		<table id="datos" class="marco">
			<tr>
				<td>
					<etiquetas:etiquetaError property="diasGracia" requerido="si"
								key="cotizacion.complementar.periodoGracia"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
				</td>
				<td>
					<html:text property="diasGracia" styleClass="cajaTexto2" size="3" maxlength="3" onkeypress="return soloNumeros(this, event, false)" readonly="true"/>
				</td>
				<td>
					<etiquetas:etiquetaError property="folioPolizaAsociada" requerido="no"
							key="cotizacion.complementar.polizaAsociada"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
				</td>
				<td>
					<html:text property="folioPolizaAsociada" onkeypress="return teclaEsIgualALaExpresionRegular(event, /[0-9\-]/ )" size="16" styleClass="cajaTexto2" readonly="true"/>
				</td>
			</tr>
			<tr>
				<td>
					
					<etiquetas:etiquetaError property="idTcTipoNegocio" requerido="si"
							key="cotizacion.complementar.tipoNegocio"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
				</td>
				<td>
					<html:select property="idTcTipoNegocio" styleClass="cajaTexto2" disabled="true">
						<html:optionsCollection name="tiposNegocios" label="descripcionTipoNegocio" value="idTcTipoNegocio"/>
					</html:select>	
				</td>
				<td>
					<etiquetas:etiquetaError property="idMedioPago" requerido="si"
							key="cotizacion.complementar.conductoPago"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />				
				</td>
				<td>
					<html:select property="idMedioPago" styleClass="cajaTexto2" disabled="true">
						<html:optionsCollection name="mediosPago" label="descripcion" value="idMedioPago"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="generaComision" requerido="si"
							key="cotizacion.complementar.politicaComision"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
				</td>
				<td>
					<html:select property="generaComision" disabled="true" styleClass="cajaTexto2">
						<html:option value="V">
							<midas:mensaje clave="cotizacion.complementar.politicaComision.siGenera"/>
						</html:option>
						<html:option value="F">
							<midas:mensaje clave="cotizacion.complementar.politicaComision.noGenera"/>
						</html:option>
					</html:select>
				</td>
			</tr>
		</table>
		
		<div id="accordionOT" style="position: relative; height: 250px; margin: 9px"></div>
		<h4>
			<midas:mensaje clave="cotizacion.complementar.complementaPrevencionOpIlic"/>
		</h4>
		<table id="informacionOperacionesIlicitas" class="marco">
			<tr>
				<td style="width:15%">
					<etiquetas:etiquetaError property="autorizadoOmitirDocumentos" requerido="si"
							key="cotizacion.complementar.autorizacionOmitirDocumentos"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />					
				</td>
				<td>
					<c:choose>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 0}">
							<c:set var="imagenAutorizacion" value="/img/blank.gif"/>
						</c:when>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 1}">
							<c:set var="imagenAutorizacion" value="/img/ico_yel.gif"/>
						</c:when>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 7}">
							<c:set var="imagenAutorizacion" value="/img/ico_green.gif"/>
						</c:when>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 8}">
							<c:set var="imagenAutorizacion" value="/img/ico_red.gif"/>
						</c:when>
					</c:choose>
					<img src="<c:url value='${imagenAutorizacion}'/>"/>
				</td>
			</tr>
		</table>
		
		<div class="alinearBotonALaDerecha">
			<div id="b_regresar" style="margin:3px">
				<a href="javascript: void(0);"
					onclick="regresarListarCotizaciones()">
					<midas:mensaje clave="midas.accion.regresar"/>
				</a>
			</div>
			<div id="b_guardar" style="margin:3px">
				<a href="javascript: void(0);"
					onclick="autorizarOmitirDocumentosComplementar()">
					<midas:mensaje clave="cotizacion.complementar.autorizador.autorizar"/>
				</a>
			</div>
			<div id="b_guardar" style="margin:3px">
				<a href="javascript: void(0);" 
					onclick="rechazarOmitirDocumentosComplementar()">
					<midas:mensaje clave="cotizacion.complementar.autorizador.rechazar"/>
				</a>
			</div>
		</div>
		
		<div id="errores" class="oculto"><html:errors/></div>	
		<html:hidden property="idToCotizacion"/>
		<html:hidden property="claveAutoEmisionDocOperIlicitas"/>
		<html:hidden property="aseguradoTipoPersona"/>
		<html:hidden property="tipoDeAccion" />
		<html:hidden property="identificacionAseguradoIdArchivo" />
		<html:hidden property="rfcAseguradoIdArchivo" />
		<html:hidden property="comprobanteDomicilioAseguradoIdArchivo" />
		<html:hidden property="actaConstitutivaEmpresaIdArchivo" />
		<html:hidden property="identificacionRepresentanteEmpresaIdArchivo" />
		<html:hidden property="poderRepresentanteEmpresaIdArchivo" />
		<html:hidden property="comprobanteDomicilioEmpresaIdArchivo" />
		<html:hidden property="cedulaFiscalEmpresaIdArchivo" />	
		<html:hidden property="diasGraciaDefault" />
</midas:formulario>