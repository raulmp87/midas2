<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<midas:formulario accion="/cotizacion/complementar/guardar">
		<div id="mensajes">
			<html:hidden property="mensaje" styleId="mensaje"/>
			<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
			<html:hidden property="idToPoliza" styleId="idToPoliza"/>
			<html:hidden property="noEndoso" styleId="noEndoso"/>
			<html:hidden property="fechaCreacionPoliza" styleId="fechaCreacionPoliza"/>
			<html:hidden property="origen" styleId="origen"/>

			<input id="idMedioPago" type="hidden" value="15" name="idMedioPago"/>
		</div>
		<table>
			<tr>
				<td><div class="titulo">Complementar cotizacion</td>
			</tr>
		</table>
		<table id="datos" class="marco">
			<tr>
				<td>
					<etiquetas:etiquetaError property="diasGracia" requerido="si"
								key="cotizacion.complementar.periodoGracia"
								normalClass="normal" errorClass="error"
								errorImage="/img/information.gif" />
				</td>
				<td>
					<html:text property="diasGracia" styleClass="cajaTexto2" size="3" maxlength="3" onkeypress="return soloNumeros(this, event, false)"/>
				</td>
				<td>
					<etiquetas:etiquetaError property="folioPolizaAsociada" requerido="no"
							key="cotizacion.complementar.polizaAsociada"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
				</td>
				<td>
					<html:text property="folioPolizaAsociada" 
						onkeypress="return soloNumeros(this, event, false);"
						onfocus="javascript: new Mask('####-########-##', 'string').attach(this)"
						maxlength="16"   
						size="20" styleClass="cajaTexto2"/>
				</td>
			</tr>
			<tr>
				<td>
					
					<etiquetas:etiquetaError property="idTcTipoNegocio" requerido="si"
							key="cotizacion.complementar.tipoNegocio"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
				</td>
				<td>
					<html:select property="idTcTipoNegocio" styleClass="cajaTexto2">
						<html:optionsCollection name="tiposNegocios" label="descripcionTipoNegocio" value="idTcTipoNegocio"/>
					</html:select>	
				</td>
				<td colspan="2">&nbsp;</td>
				<%--<td>
					<etiquetas:etiquetaError property="idMedioPago" requerido="si"
							key="cotizacion.complementar.conductoPago"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />				
				</td>
				<td>
					<html:select property="idMedioPago" styleClass="cajaTexto2">
						<html:optionsCollection name="mediosPago" label="descripcion" value="idMedioPago"/>
					</html:select>
				</td>--%>
			</tr>
			<tr>
				<td>
					<etiquetas:etiquetaError property="generaComision" requerido="si"
							key="cotizacion.complementar.politicaComision"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
				</td>
				<td>
					<html:select property="generaComision" disabled="true" styleClass="cajaTexto2">
						<html:option value="V">
							<midas:mensaje clave="cotizacion.complementar.politicaComision.siGenera"/>
						</html:option>
						<html:option value="F">
							<midas:mensaje clave="cotizacion.complementar.politicaComision.noGenera"/>
						</html:option>
					</html:select>
				</td>
			</tr>
		</table>
		
		<div id="accordionOT" style="position: relative; height: 250px; margin: 9px"></div>
		<h4>
			<midas:mensaje clave="cotizacion.complementar.complementaPrevencionOpIlic"/>
		</h4>
		<table id="informacionOperacionesIlicitas" class="marco">
			<tr>
				<td style="width:15%">
					<etiquetas:etiquetaError property="autorizadoOmitirDocumentos" requerido="si"
							key="cotizacion.complementar.autorizacionOmitirDocumentos"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />					
				</td>
				<td>
					<c:choose>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 0}">
							<c:set var="imagenAutorizacion" value="/img/blank.gif"/>
						</c:when>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 1}">
							<c:set var="imagenAutorizacion" value="/img/ico_yel.gif"/>
						</c:when>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 7}">
							<c:set var="imagenAutorizacion" value="/img/ico_green.gif"/>
						</c:when>
						<c:when test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 8}">
							<c:set var="imagenAutorizacion" value="/img/ico_red.gif"/>
						</c:when>
					</c:choose>
					<img src="<c:url value='${imagenAutorizacion}'/>"/>
				</td>
				<td>
					<c:if test="${complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 0 or 
					complementarCotizacionForm.claveAutoEmisionDocOperIlicitas == 8}">
						<div id="b_guardar" style="width:300px">
							<a href="javascript: void(0);" 
								onclick="solicitarAutorizacion()">
								<midas:mensaje clave="cotizacion.complementar.solicitarAutorizacionOmitirDocumentos" />
							</a>
						</div>
					</c:if>	
				</td>
			</tr>
		</table>
		
		<c:if test="${complementarCotizacionForm.aseguradoTipoPersona == 1}">
			<h4><midas:mensaje clave="cotizacion.complementar.docAsegurado"/></h4>
			<table id="desplegarDetalle">
				<tr>
					<td>
						<div id="identificacionAsegurado">
							<midas:mensaje clave="cotizacion.complementar.docAsegurado.identificacion"/>
							<c:if test="${empty complementarCotizacionForm.identificacionAseguradoIdArchivo}">
								<html:text property="identificacionAseguradoArchivo" styleClass="oculto" size="30"/>
								<a href="javascript: void(0);" 
									onclick="guardarPrevencionOperacionesArchivo(1)">
										<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
								</a>
							</c:if>
							<c:if test="${not empty complementarCotizacionForm.identificacionAseguradoIdArchivo}">
								<html:text property="identificacionAseguradoArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
								<a href="javascript: void(0);" 
									onclick="guardarPrevencionOperacionesArchivo(1)">
										<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
								</a>
							</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="rfcAsegurado">
						<midas:mensaje clave="cotizacion.complementar.docAsegurado.rfc"/>
							<c:if test="${empty complementarCotizacionForm.rfcAseguradoIdArchivo}">
								<html:text property="rfcAseguradoArchivo" styleClass="oculto" size="30"/>
								<a href="javascript: void(0);" 
									onclick="guardarPrevencionOperacionesArchivo(2)">
										<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
								</a>
							</c:if>
							<c:if test="${not empty complementarCotizacionForm.rfcAseguradoIdArchivo}">
								<html:text property="rfcAseguradoArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
								<a href="javascript: void(0);" 
									onclick="guardarPrevencionOperacionesArchivo(2)">
										<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
								</a>
							</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="comprobanteDomicilioAsegurado">
						<midas:mensaje clave="cotizacion.complementar.docAsegurado.comprobanteDomicilio"/>
						<c:if test="${empty complementarCotizacionForm.comprobanteDomicilioAseguradoIdArchivo}">
							<html:text property="comprobanteDomicilioAseguradoArchivo" styleClass="oculto" size="30"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(3)">
									<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
							</a>
						</c:if>
						<c:if test="${not empty complementarCotizacionForm.comprobanteDomicilioAseguradoIdArchivo}">
							<html:text property="comprobanteDomicilioAseguradoArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(3)">
									<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
							</a>
						</c:if>
						</div>
					</td>
				</tr>
			</table>
		</c:if>
		<c:if test="${complementarCotizacionForm.aseguradoTipoPersona == 2}">
			<h4><midas:mensaje clave="cotizacion.complementar.docEmpresa"/></h4>
			<table id="desplegarDetalle">
				<tr>
					<td>
						<div id="actaConstitutiva">
						<midas:mensaje clave="cotizacion.complementar.docEmpresa.actaConstitutiva"/>
						<c:if test="${empty complementarCotizacionForm.actaConstitutivaEmpresaIdArchivo}">
							<html:text property="actaConstitutivaEmpresaArchivo" styleClass="oculto" size="30"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(4)">
									<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
							</a>
						</c:if>
						<c:if test="${not empty complementarCotizacionForm.actaConstitutivaEmpresaIdArchivo}">
							<html:text property="actaConstitutivaEmpresaArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(4)">
									<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
							</a>
						</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="identificacionRepresentante">
						<midas:mensaje clave="cotizacion.complementar.docEmpresa.identificacionRepresentante"/>
						<c:if test="${empty complementarCotizacionForm.identificacionRepresentanteEmpresaIdArchivo}">
							<html:text property="identificacionRepresentanteEmpresaArchivo" styleClass="oculto" size="30"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(6)">
									<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
							</a>
						</c:if>
						<c:if test="${not empty complementarCotizacionForm.identificacionRepresentanteEmpresaIdArchivo}">
							<html:text property="identificacionRepresentanteEmpresaArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(6)">
									<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
							</a>
						</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="poderRepresentante">
						<midas:mensaje clave="cotizacion.complementar.docEmpresa.poderRepresentante"/>
						<c:if test="${empty complementarCotizacionForm.poderRepresentanteEmpresaIdArchivo}">
							<html:text property="poderRepresentanteEmpresaArchivo" styleClass="oculto" size="30"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(5)">
									<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
							</a>
						</c:if>
						<c:if test="${not empty complementarCotizacionForm.poderRepresentanteEmpresaIdArchivo}">
							<html:text property="poderRepresentanteEmpresaArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(5)">
									<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
							</a>
						</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="comprobanteDomicilioEmpresa">
						<midas:mensaje clave="cotizacion.complementar.docEmpresa.comprobanteDomicilio"/>
						<c:if test="${empty complementarCotizacionForm.comprobanteDomicilioEmpresaIdArchivo}">
							<html:text property="comprobanteDomicilioEmpresaArchivo" styleClass="oculto" size="30"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(7)">
									<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
							</a>
						</c:if>
						<c:if test="${not empty complementarCotizacionForm.comprobanteDomicilioEmpresaIdArchivo}">
							<html:text property="comprobanteDomicilioEmpresaArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(7)">
									<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
							</a>
						</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="cedulaFiscal">
						<midas:mensaje clave="cotizacion.complementar.docEmpresa.cedulaFiscal"/>
						<c:if test="${empty complementarCotizacionForm.cedulaFiscalEmpresaIdArchivo}">
							<html:text property="cedulaFiscalEmpresaArchivo" styleClass="oculto" size="30"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(8)">
									<midas:mensaje clave="cotizacion.complementar.agregarDocumento"/>
							</a>
						</c:if>
						<c:if test="${not empty complementarCotizacionForm.cedulaFiscalEmpresaIdArchivo}">
							<html:text property="cedulaFiscalEmpresaArchivo" styleClass="cajaTexto2" size="30" readonly="true"/>
							<a href="javascript: void(0);" 
								onclick="guardarPrevencionOperacionesArchivo(8)">
									<midas:mensaje clave="cotizacion.complementar.actualizarDocumento"/>
							</a>
						</c:if>
						</div>
					</td>
				</tr>
			</table>
		</c:if>		
		<div class="alinearBotonALaDerecha">
			<c:choose>
				<c:when test="${complementarCotizacionForm.origen == 'COT'}">
					<div id="b_regresar" style="margin: 3px">
						<a href="javascript: void(0);"
							onclick="regresarListarCotizaciones()">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</c:when>
				<c:otherwise>
					<div id="b_regresar" style="margin: 3px">
						<a href="javascript: void(0);"
							onclick="listarCotizacionesEndoso()">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</c:otherwise>
			</c:choose>
			<div id="b_guardar" style="margin: 3px">
				<a href="javascript: void(0);"
					onclick="emitirComplementarCotizacion()">
					<midas:mensaje clave="cotizacion.complementar.emitir"/>
				</a>
			</div>
			<div id="b_guardar" style="margin: 3px">
				<a href="javascript: void(0);" 
					onclick="guardarComplementarCotizacion()">
					<midas:mensaje clave="midas.accion.guardar"/>
				</a>
			</div>
		</div>
		
		<div id="errores" class="oculto"><html:errors/></div>	
		<html:hidden property="idToCotizacion" styleId="idToCotizacion"/>
		<html:hidden property="claveAutoEmisionDocOperIlicitas"/>
		<html:hidden property="aseguradoTipoPersona"/>
		<html:hidden property="tipoDeAccion" />
		<html:hidden property="identificacionAseguradoIdArchivo" />
		<html:hidden property="rfcAseguradoIdArchivo" />
		<html:hidden property="comprobanteDomicilioAseguradoIdArchivo" />
		<html:hidden property="actaConstitutivaEmpresaIdArchivo" />
		<html:hidden property="identificacionRepresentanteEmpresaIdArchivo" />
		<html:hidden property="poderRepresentanteEmpresaIdArchivo" />
		<html:hidden property="comprobanteDomicilioEmpresaIdArchivo" />
		<html:hidden property="cedulaFiscalEmpresaIdArchivo" />	
		<html:hidden property="diasGraciaDefault" />
</midas:formulario>