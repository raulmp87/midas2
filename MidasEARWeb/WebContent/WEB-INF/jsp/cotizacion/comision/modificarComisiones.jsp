<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<midas:formulario accion="/cotizacion/comision/guardarComisionCotizacion">
	<html:hidden property="idToCotizacion" styleId="idToCotizacion" name="comisionCotizacionForm"/>
	<div class="subtituloIzquierdaDiv">Comisiones </div>
	<table id="agregar" border="0">
		<tr>
			<td height="180">
				<div id="editarComisionesCotizacionGrid" class="dataGridConfigurationClass" height="95%" width="98%"></div>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: comisionProcessor.sendData(); if(!comisionCotizacionError){ }else alert('error');" tipo="guardar" />
				</div>
			</td>
		</tr>
	</table>
	<div class="subtituloIzquierdaDiv"><midas:mensaje clave="midas.cotizacion.bonificacionComision"/></div>
	<table id="agregar">
		<tr>
			<th width="30%">
				<midas:mensaje clave="midas.cotizacion.bonificacionComision"/>% 
			</th>
			<td width="10%"><midas:texto propiedadFormulario="bonificacionComision" id="bonificacionComision"
			 					onblur="javascript:if(this.value > 100) alert('El porcentaje de Bonificaci&oacute;n debe ser Menor o Igual que 100');" 
								onkeypress="return soloNumeros(this, event, true)"/>
			</td>
			<td width="60%">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: parent.sendRequest(document.comisionCotizacionForm,'/MidasWeb/cotizacion/comision/guardarBonificacion.do', 'contenido_comisiones','mostrarEdicionComisionesPorCotizacionGrid(${comisionCotizacionForm.idToCotizacion},\&#39;comision\&#39;);');" tipo="guardar" texto="Guardar"/>				
				</div>			
			</td>
		</tr>
	</table>	
</midas:formulario>