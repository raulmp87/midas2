<%@ page isELIgnored="false"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>

<div id="detalle" name="Detalle">
	<midas:formulario accion="/cotizacion/primerRiesgoLUC/calcularPrimerRiesgoODT.do">
		<html:hidden property="primerRiesgoLUCDTO.idCotizacion" styleId="idCotizacion"/>
		<html:hidden name="primerRiesgoLUCForm"  property="primerRiesgoLUCDTO.size" styleId="size"/>
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		<table id="t_riesgo" width="100%">
			<tr>
				<th width="10%">
					&nbsp;
				</th>
				<th width="40%">
					<midas:mensaje clave="cotizacion.primerRiesgo.seccion"/>
				</th>
				<th width="20%">
					<midas:mensaje clave="cotizacion.primerRiesgo.totales"/>
				</th>
				<th width="30%">
					<midas:mensaje clave="cotizacion.primerRiesgo.totalesPrimerRiesgo"/>
				</th>
			</tr>	
			<nested:iterate id="nextSeccion" name="primerRiesgoLUCForm" property="primerRiesgoLUCDTO.seccionesPrimerRiesgo" indexId="indexVar">
				<tr>
					<nested:hidden property="seccionDTO.idToSeccion"/>	
					<nested:hidden property="seccionDTO.claveDependenciaOtrasPr" styleId='<%="claveDependenciaOtrasPr[" + indexVar + "]"%>'/>
					<td><nested:checkbox property="claveContrato" styleId='<%="checkbox[" + indexVar + "]"%>' onclick='<%="setTotalPrimerRiesgo(" + indexVar + ")"%>'/></td>
					<td>
						<nested:hidden property="seccionDTO.nombreComercial"/>	
						<bean:write name="nextSeccion" property="seccionDTO.nombreComercial"/>
					</td>
					<td>
						<nested:hidden  property="valorSumaAsegurada" styleId='<%="valorSumaAsegurada[" + indexVar + "]"%>'/>
						<bean:write name="nextSeccion" property="valorSumaAsegurada" format="$#,##0.00"/>
					</td>
					<td><div id='<%="totalPrimerRiesgo[" + indexVar + "]"%>'>$0.00</div></td>
				</tr>	
			</nested:iterate>
			<nested:notEmpty name="primerRiesgoLUCForm" property="primerRiesgoLUCDTO.seccionesPrimerRiesgo">
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td width="10%">
						&nbsp;
					</td>
					<td width="40%">
						&nbsp;
					</td>
					<th width="20%">
						<html:hidden name="primerRiesgoLUCForm" property="primerRiesgoLUCDTO.totalSecciones"/>
						<fmt:formatNumber value="${primerRiesgoLUCForm.primerRiesgoLUCDTO.totalSecciones}" pattern="$##,##0.00"/>
					</th>
					<th width="30%"><div id="totalSeccionesPrimerRiesgo"></div></th>
				</tr>
			</nested:notEmpty>			
		</table>
		<table id="desplegarDetalle">
			<tr>
				<th width="70%"><midas:mensaje clave="cotizacion.primerRiesgo.leyenda"/></th>
				<td width="30%">
					<midas:texto propiedadFormulario="primerRiesgoLUCDTO.sumaAseguradaPrimerRiesgo" 
								 id="sumaAseguradaPrimerRiesgo"
								 caracteres="25"
								 onblur="javascript:if(this.value <= 0) alert('La Suma Asegurada debe ser Mayor que Cero');" 
								 onkeypress="return soloNumeros(this, event, true)"/>
				</td>
			</tr>
		</table>
		<div id="botonCalcular" style="display: none;">
			<div class="alinearBotonALaDerecha">		
				<midas:boton onclick="javascript:validaSecciones();" tipo="agregar" texto="calcular"/>
			</div>			
		</div>
		</br>
		</br>
		
		<%--<logic : greaterThan value="1" name="primerRiesgoLUCForm" property="agrupacionCotDTO.valorSumaAsegurada">
		</logic : greaterThan>--%>
		
		<logic:notEmpty name="primerRiesgoLUCForm" property="agrupacionCotDTOs">
			<table id="t_riesgo" width="100%">
				<tr>
					<th width="5%">
						&nbsp;
					</th>				
					<th width="20%">
						Agrupaci&oacute;n
					</th>		
					<th width="20%">
						Suma Asegurada
					</th>	
					<th width="10%">
						Cuota
					</th>	
					<th width="20%">
						Prima Neta
					</th>				
					<th width="10%">
						Acciones
					</th>										
				</tr>		
				<logic:iterate id="agrupacion" name="primerRiesgoLUCForm" property="agrupacionCotDTOs" indexId="index">
					<tr>					
						<td>&nbsp;</td>
						<td>
							Primer Riesgo De: <nested:write property="descripcionSeccion" name="agrupacion"/>
						</td>
						<td><nested:write property="valorSumaAsegurada" name="agrupacion" format="$#,##0.00"/></td>
						<c:set var="cuota" value="${agrupacion.valorCuota * 1000}"></c:set>
						<td><nested:write name="cuota" format="##0.0000"/></td>
						<td><nested:write property="valorPrimaneta" name="agrupacion" format="$#,##0.00"/></td>
						<td align="center">
							<a href="javascript: "	
							onclick="eliminarPRR(${agrupacion.id.idToCotizacion},${agrupacion.id.numeroAgrupacion},${agrupacion.idToSeccion});">
								<img border="0px" title="Eliminar Primer Riesgo" alt="Eliminar Primer Riesgo" src="/MidasWeb/img/delete14.gif">
							</a>			
						</td>						
					</tr>						
				</logic:iterate>											
			</table>		
		</logic:notEmpty>
	</midas:formulario>
<div>