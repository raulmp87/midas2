<%@ page isELIgnored="false"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/cotizacion/cotizacion.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>

<div id="detalle" name="Detalle">
	<midas:formulario accion="/cotizacion/primerRiesgoLUC/calcularLUCODT.do">
		<html:hidden property="primerRiesgoLUCDTO.idCotizacion" styleId="idCotizacion"/>
		<html:hidden name="primerRiesgoLUCForm"  property="primerRiesgoLUCDTO.size" styleId="size"/>
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		<table id="t_riesgo" width="100%">
			<tr>
				<th width="10%">
					&nbsp;
				</th>
				<th width="70%">
					<midas:mensaje clave="cotizacion.primerRiesgo.seccion"/>
				</th>
				<th width="30%">
					<midas:mensaje clave="cotizacion.luc.sumaAsegurada"/>
				</th>
			</tr>	
			<nested:iterate id="nextSeccion" name="primerRiesgoLUCForm" property="primerRiesgoLUCDTO.seccionesLUC" indexId="indexVar">
				<tr>
					<nested:hidden property="id.idToSeccion"/>		
					<td><nested:checkbox property="claveContrato" styleId='<%="checkbox[" + indexVar + "]"%>' onclick="mostrarDivLUC();"/></td>
					<td>
						<nested:hidden property="seccionDTO.nombreComercial"/>	
						<bean:write name="nextSeccion" property="seccionDTO.nombreComercial"/>
					</td>
					<td>
						<nested:text property="valorSumaAsegurada" styleId='<%="valorSumaAsegurada[" + indexVar + "]"%>' styleClass="cajaTexto"/>
					</td>
				</tr>	
			</nested:iterate>
			<nested:empty name="primerRiesgoLUCForm" property="primerRiesgoLUCDTO.seccionesLUC">
				<tr>		
					<td colspan="3">
					 	No existen secciones para calcular LUC
					</td>
				</tr>
			</nested:empty>
		</table>
		<div id="botonCalcular" style="display: none;">
			<div class="alinearBotonALaDerecha">
				<midas:boton onclick="javascript: agregarLUC(primerRiesgoLUCForm);" tipo="agregar" texto="calcular"/>				
			</div>
		</div>
		</br>
		</br>
		<logic:notEmpty name="primerRiesgoLUCForm" property="agrupacionCotDTOs">
			<table id="t_riesgo" width="100%">
				<tr>
					<th width="30%"  colspan="2">
						Agrupaci&oacute;n
					</th>		
					<th width="20%">
						Suma Asegurada
					</th>	
					<th width="20%">
						Cuota
					</th>	
					<th width="20%">
						Prima Neta
					</th>
					<th width="10%">Acciones</th>					
				</tr>				
				<logic:iterate id="agrupacion" name="primerRiesgoLUCForm" property="agrupacionCotDTOs" indexId="index">
					<tr>
						<td colspan="2">
							<nested:write property="descripcionSeccion" name="agrupacion"/>
						</td>
						<td><nested:write property="valorSumaAsegurada" name="agrupacion" format="$#,##0.00"/></td>
						<c:set var="cuota" value="${agrupacion.valorCuota * 1000}"></c:set>
						<td><nested:write name="cuota" format="##0.0000"/></td>
						<td><nested:write property="valorPrimaneta" name="agrupacion" format="$#,##0.00"/></td>
						<td align="center">
							<a href="javascript: "	
							onclick="eliminarLUC(${agrupacion.id.idToCotizacion},${agrupacion.id.numeroAgrupacion},${agrupacion.idToSeccion});">
								<img border="0px" title="Eliminar LUC" alt="Eliminar LUC" src="/MidasWeb/img/delete14.gif">
							</a>			
						</td>
					</tr>				
				</logic:iterate>								
			</table>		
		</logic:notEmpty>
	</midas:formulario>
<div>
