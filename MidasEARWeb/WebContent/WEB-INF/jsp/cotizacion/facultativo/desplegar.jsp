<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/cotizacion/facultativo/aceptarFacultativo">
			<html:hidden property="mensaje" styleId="mensaje"/>
			<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
			<table id="desplegarDetalle">
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Cotizaci&oacute;n de Reaseguro Facultativo
					</td>
					<td width="18%">
						Cotizacion: <bean:write name="cotizacionFacultativoForm" property="codigoCotizacion"/>
					</td>
					<td width="17%">
						Fecha: <fmt:formatDate value="${cotizacionFacultativoForm.cotizacionDTO.fechaCreacion}" dateStyle="short" />
					</td>
				</tr>
			</table>
			<table id="desplegar">
				<tr>
					<td colspan="4">
						TIPO DE NEGOCIO: <bean:write name="cotizacionFacultativoForm" property="tipoNegocioDTO.codigoTipoNegocio" /> - <bean:write name="cotizacionFacultativoForm" property="tipoNegocioDTO.descripcionTipoNegocio" />
					</td>
				</tr>
				<tr>
					<td class="subtitulo" colspan="4">
						Informaci&oacute;n de L&iacute;nea
					</td>
				</tr>
				<tr>
					<td>
						L&iacute;nea: <bean:write name="cotizacionFacultativoForm" property="nombreLinea"/>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty cotizacionFacultativoForm.numeroInciso}">
								Inciso: <bean:write name="cotizacionFacultativoForm" property="numeroInciso"/>
							</c:when>
							<c:otherwise>
								&nbsp;
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${!empty cotizacionFacultativoForm.numeroSubInciso}">
								SubInciso: <bean:write name="cotizacionFacultativoForm" property="numeroSubInciso"/>
							</c:when>
							<c:otherwise>
								&nbsp;
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						Suma Asegurada: <bean:write name="cotizacionFacultativoForm" property="sumaAsegurada" format="$#,##0.00"/>
					</td>
				</tr>
			</table>
			<table id="desplegar">
				<tr>
					<td class="subtitulo">
						Informaci&oacute;n de Coberturas
					</td>
				</tr>
				<tr>
					<td>
						<table id="t_riesgo">
							<tr>
								<th>Cobertura</th>
								<th>Prima Neta</th>
								<th>Prima por Contratos</th>
								<th>Prima Facultada</th>
								<th>Coaseguro</th>
								<th>Deducible</th>
							</tr>
							<nested:iterate id="nextCobertura" name="cotizacionFacultativoForm" property="coberturasTipoPoliza" indexId="indexVar">
								<tr>
									<td><bean:write name="nextCobertura" property="cobertura.nombreComercial" /></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNeta" format="$#,##0.00"/></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNetaContratos" format="$#,##0.00"/></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNetaFacultada" format="$#,##0.00"/></td>
									<td><bean:write name="cotizacionFacultativoForm" property="coaseguros[${indexVar}]"/></td>
									<td><bean:write name="cotizacionFacultativoForm" property="deducibles[${indexVar}]"/></td>
								</tr>
							</nested:iterate>
							<nested:iterate id="nextCobertura" name="cotizacionFacultativoForm" property="coberturasInciso" indexId="indexVar">
								<tr>
									<td><bean:write name="nextCobertura" property="cobertura.nombreComercial" /></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNeta" format="$#,##0.00"/></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNetaContratos" format="$#,##0.00"/></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNetaFacultada" format="$#,##0.00"/></td>
									<td><bean:write name="cotizacionFacultativoForm" property="coaseguros[${indexVar}]"/></td>
									<td><bean:write name="cotizacionFacultativoForm" property="deducibles[${indexVar}]"/></td>
								</tr>
							</nested:iterate>
							<nested:iterate id="nextCobertura" name="cotizacionFacultativoForm" property="coberturasSubInciso" indexId="indexVar">
								<tr>
									<td><bean:write name="nextCobertura" property="cobertura.nombreComercial" /></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNeta" format="$#,##0.00"/></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNetaContratos" format="$#,##0.00"/></td>
									<td><bean:write name="nextCobertura" property="valorPrimaNetaFacultada" format="$#,##0.00"/></td>
									<td><bean:write name="cotizacionFacultativoForm" property="coaseguros[${indexVar}]"/></td>
									<td><bean:write name="cotizacionFacultativoForm" property="deducibles[${indexVar}]"/></td>
								</tr>
							</nested:iterate>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<div class="alinearBotonALaDerecha">				
							<div id="botonAgregar">
								<midas:boton onclick="javascript: sendRequest(null,'','',null);" tipo="regresar"/>
								<midas:boton onclick="javascript: sendRequest(null,'','',null); " tipo="continuar" texto="Aceptar Facultativo" />
							</div>
						</div>
					</td>
				</tr>
			</table>
		</midas:formulario>
	</center>
</div>
