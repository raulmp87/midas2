<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/curpRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/cotizacion/agregarPersona">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
          		<midas:mensaje clave="midas.datos.cliente"/>
          		<midas:oculto propiedadFormulario="idPadre"/>
           		<midas:oculto propiedadFormulario="idToPersona"/>
           		<midas:oculto propiedadFormulario="codigoPersona"/>
           		<midas:oculto propiedadFormulario="descripcionPadre"/>
          	</td>
		</tr>
		<tr>
			<td colspan="2"></td>
			<th colspan="2"><center><midas:mensaje clave="midas.persona.tipo"/>*:</center></th>
		</tr>
		<tr>
			<td colspan= "2">
			</td>
			<td><html:radio property="claveTipoPersona" value="1" onclick="configurarFormularioPersonaVentana(this);" >
					<midas:mensaje clave="midas.persona.fisica"/></html:radio></td>
			<td><html:radio property="claveTipoPersona" value="2" onclick="configurarFormularioPersonaVentana(this);" >
					<midas:mensaje clave="midas.persona.moral"/></html:radio></td>
		</tr>
		<logic:equal value="1" property="claveTipoPersona" name="personaForm" >
		<tr>
			<th colspan="2" id="etiquetaNombre"><midas:mensaje clave="midas.persona.nombre"/>*</th>
			<th id="etiquetaPaterno" ><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
			<th id="etiquetaMaterno" ><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
		</tr>
		<tr>
			<td colspan="2"><midas:texto propiedadFormulario="nombre" id="cajaNombre" onblur="formatRFC(document.personaForm);" /></td>
			<td id="cajaApellidoPaterno"><midas:texto propiedadFormulario="apellidoPaterno" onblur="formatRFC(document.personaForm);"/></td>
			<td id="cajaApellidoMaterno"><midas:texto propiedadFormulario="apellidoMaterno" onblur="formatRFC(document.personaForm);"/></td>
		</tr>
		<tr>
			<th><div id="etiquetaFecha"><midas:mensaje clave="midas.persona.fechaNacimiento"/></div><a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();"><image src="/MidasWeb/img/b_calendario.gif" border=0 /></a></th>
			<th><midas:mensaje clave="midas.persona.rfc"/></th>
			<th><midas:mensaje clave="midas.persona.telefono"/></th>
			<th><midas:mensaje clave="midas.persona.email"/></th>
		</tr>
		<tr>
			<td><midas:texto propiedadFormulario="fechaNacimiento" id="fecha" onblur="formatRFC(document.personaForm);"/></td>
			<td><midas:texto propiedadFormulario="codigoRFC" id="rfc"/></td>
			<td><midas:texto propiedadFormulario="telefono"/></td>
			<td><midas:texto propiedadFormulario="email"/></td>
		</tr>
		</logic:equal>
		<logic:equal value="2" property="claveTipoPersona" name="personaForm" >
		<tr>
			<th colspan="2" id="etiquetaNombre"><midas:mensaje clave="midas.persona.razonSocial"/>*</th>
			<th id="etiquetaPaterno" style="visibility:hidden"><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
			<th id="etiquetaMaterno" style="visibility:hidden"><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
		</tr>
		<tr>
			<td colspan="2"><midas:texto propiedadFormulario="nombre" id="cajaNombre" onblur="formatRFC(document.personaForm);" /></td>
			<td id="cajaApellidoPaterno"><midas:texto propiedadFormulario="apellidoPaterno" onblur="formatRFC(document.personaForm);"/></td>
			<td id="cajaApellidoMaterno"><midas:texto propiedadFormulario="apellidoMaterno" onblur="formatRFC(document.personaForm);"/></td>
		</tr>
		<tr>
			<th><div id="etiquetaFecha"><midas:mensaje clave="midas.persona.fechaConstitucion"/></div><a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();"><image src="/MidasWeb/img/b_calendario.gif" border=0 /></a></th>
			<th><midas:mensaje clave="midas.persona.rfc"/></th>
			<th><midas:mensaje clave="midas.persona.telefono"/></th>
			<th><midas:mensaje clave="midas.persona.email"/></th>
		</tr>
		<tr>
			<td><midas:texto propiedadFormulario="fechaNacimiento" id="fecha" onblur="formatRFC(document.personaForm);"/></td>
			<td><midas:texto propiedadFormulario="codigoRFC" id="rfc"/></td>
			<td><midas:texto propiedadFormulario="telefono"/></td>
			<td><midas:texto propiedadFormulario="email"/></td>
		</tr>
		</logic:equal>
		<tr>
		<td><div id='calendarioOT' style="position:absolute;z-index: 1;"></div></td>
		</tr>
		<tr>
			<td colspan="3" class="campoRequerido">
		 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
				</td>
			<td class="guardar">
			<div class="alinearBotonALaDerecha">
				<div id="b_guardar">
					<midas:boton onclick="javascript: guardarPersona(document.personaForm);" tipo="guardar"/>
				</div>
			</div>
			</td>
		</tr>
		</table>
</midas:formulario>