<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<div id="detalle" name="Detalle">
	<center>		
		<midas:formulario accion="/cotizacion/guardarOrdenTrabajo">
		<html:hidden property="idToCotizacion" styleId="idToCotizacion"/>
		<html:hidden property="esCotizacion" styleId="esCotizacion" name="cotizacionForm"/>
		<html:hidden property="idToProducto"/>
		<html:hidden property="mensaje" styleId="mensaje"/>
		<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
		<html:hidden property="editaDatoGeneral" name="cotizacionForm"/>
			<logic:empty property="esCotizacion" name="cotizacionForm">
				<div class="subtituloCotizacion">
					Datos de la Solicitud
				</div>
				<div class="subtituloCotizacion">
					<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:<midas:escribe propiedad="idOrdenTrabajoFormateada" nombre="cotizacionForm"/>
			</logic:empty>
			<logic:notEmpty property="esCotizacion" name="cotizacionForm">
				<logic:equal property="esCotizacion" name="cotizacionForm" value="0">
					<div class="subtituloCotizacion">
						Datos de la Solicitud
					</div>
					<div class="subtituloCotizacion">
						<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo"/>:<midas:escribe propiedad="idOrdenTrabajoFormateada" nombre="cotizacionForm"/>
				</logic:equal>
				<logic:equal property="esCotizacion" name="cotizacionForm" value="1">
					<div class="subtituloCotizacion">
						<midas:mensaje clave="midas.cotizacion.datosCotizacion"/>
					</div>
					<div class="subtituloCotizacion">
						<midas:mensaje clave="midas.cotizacion.cotizacion"/>: <midas:escribe propiedad="idToCotizacionFormateada" nombre="cotizacionForm"/>
				</logic:equal>
			</logic:notEmpty>
				<html:hidden property="idOrdenTrabajoFormateada"/>
				&nbsp;&nbsp;<midas:mensaje clave="midas.ordendetrabajo.fecha"/>:<c:out value="${cotizacionForm.fechaCreacion}"></c:out>
			</div>
			<div style="clear:both"></div>
			<table id="desplegar" border="0">
				<tr>
					<th><midas:mensaje clave="midas.ordendetrabajo.nombreSolicitante"/>:</th>
					<td class="txt_v"> <midas:escribe propiedad="nombreSolicitante" nombre="cotizacionForm"/><html:hidden property="nombreSolicitante"/></td>
					<th><midas:mensaje clave="midas.ordendetrabajo.telefonoContacto"/>:</th>
					<td class="txt_v"><midas:escribe propiedad="telefonoContacto" nombre="cotizacionForm"/><html:hidden property="telefonoContacto"/></td>							
				</tr>
				<tr class="bg_t2">
					<th><midas:mensaje clave="midas.ordendetrabajo.productoPorContratar"/>:</th>
					<td class="txt_v"><midas:escribe propiedad="producto" nombre="cotizacionForm"/><html:hidden property="producto"/></td>
					<th><midas:mensaje clave="midas.ordendetrabajo.oficinaVenta"/>:</th>
					<td class="txt_v"> <midas:escribe propiedad="oficina" nombre="cotizacionForm"/><html:hidden property="oficina"/></td>					
				</tr>
				<tr>
					<th><etiquetas:etiquetaError property="idTcAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="cotizacionForm" key="solicitud.agenteSeguros"/> <html:hidden property="nombreAgente"/></th>
					<td>
						<html:select property="idTcAgente" styleClass="cajaTexto" >
							<html:optionsCollection value="idTcAgente" property="agentes" name="cotizacionForm" label="nombre"/>
						</html:select>
					</td>
					<th></th><td></td>					
				</tr>
			</table>
			<div class="subtituloIzquierdaDiv"><midas:mensaje clave="midas.ordendetrabajo.datosProducto"/></div>
			<html:hidden property="permiteCambiarPoliza" name="cotizacionForm" styleId="permiteCambiarPoliza"/>
			<input type="hidden" id="poliza" value="<midas:escribe propiedad="idToTipoPoliza" nombre="cotizacionForm"/>" />
      			<table id="desplegarDetalle">
      				<tr>
      					<th width="15%">
      						<etiquetas:etiquetaError property="idToTipoPoliza" requerido="si" key="midas.ordendetrabajo.tipoPoliza" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />	</th>
					<td width="25%">
					
						<html:select property="idToTipoPoliza" styleId="selectTipoPoliza" name="cotizacionForm" styleClass="cajaTexto" onchange="if (validarCambioEnTipoPoliza() )getMonedasTipoPoliza(this,'idTcMoneda');" disabled="${cotizacionForm.editaDatoGeneral}" >
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="cotizacionForm"  property="listaTipoPoliza" value="idToTipoPoliza" label="nombreComercial"/>
						</html:select>
					</td>
					<th width="15%">
						<etiquetas:etiquetaError property="idMoneda" requerido="si" key="midas.ordendetrabajo.moneda" normalClass="normal" errorClass="error" 
						errorImage="/img/information.gif" /></th>
					<td width="25%">
						<html:select property="idMoneda" name="cotizacionForm" styleId="idTcMoneda" styleClass="cajaTexto"
						  	onchange="getFormasPagoPorMoneda(this,'idFormaPago');" disabled="${cotizacionForm.editaDatoGeneral}" >
						  	<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="cotizacionForm"  property="listaMoneda" value="idTcMoneda" label="descripcion"/>
					 	</html:select>
					</td>
					<td width="10%">&nbsp;</td>															 					
					<td width="10%">&nbsp;</td>
      				</tr>
      				<tr>
      					<th>
	      					<etiquetas:etiquetaError property="idFormaPago" requerido="si" key="midas.ordendetrabajo.formaPago" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
      					</th>
					<td>
						<html:select property="idFormaPago" name="cotizacionForm" styleClass="cajaTexto" styleId="idFormaPago" disabled="${cotizacionForm.deshabilitaCambioFP}">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="cotizacionForm" property="listaFormaPago" value="idFormaPago" label="descripcion" />
						</html:select>	
					</td>
					<th><center><midas:mensaje clave="midas.ordendetrabajo.vigencia"/>	
				        <logic:equal value="5" property="claveTipoEndoso" name="cotizacionForm">
							&nbsp;
						</logic:equal>
						<logic:notEqual value="5" property="claveTipoEndoso" name="cotizacionForm">						    
							<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioDobleOT()">
								<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a></center>
						</logic:notEqual>  
					</th>
					<th><etiquetas:etiquetaError property="fechaInicioVigencia" requerido="si"
							key="midas.ordendetrabajo.vigenciaFechaInicial" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>
					<th colspan="2"><etiquetas:etiquetaError property="fechaFinVigencia" requerido="si"
							key="midas.ordendetrabajo.vigenciaFechaFinal" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>													   					
      				</tr>    
      				<tr>
					<th>&nbsp;</th>
					<td>&nbsp;</td>
					<html:hidden property="idMedioPago" value="15" name="cotizacionForm"/>
					<td>&nbsp;</td>	
					<logic:equal value="5" property="claveTipoEndoso" name="cotizacionForm">
						<td>
							<html:hidden property="fechaInicioVigencia" name="cotizacionForm"/>
							<html:text styleId="fechaInicial" property="fechaInicioVigencia" name="cotizacionForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto" disabled="true"/>
						</td>
						<td colspan="2">
							<html:hidden property="fechaFinVigencia" name="cotizacionForm"/>
							<html:text styleId="fechaFinal" property="fechaFinVigencia" name="cotizacionForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto" disabled="true"/>
						</td>							         									
					</logic:equal>
					<logic:notEqual value="5" property="claveTipoEndoso" name="cotizacionForm">
						<td>
							<html:text styleId="fechaInicial" property="fechaInicioVigencia" name="cotizacionForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						</td>
						<td colspan="2">
							<html:text styleId="fechaFinal" property="fechaFinVigencia" name="cotizacionForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"/>
						</td>							         									
					</logic:notEqual>							         				
      				</tr>  
				<tr>
					<td></td><td></td>
					<td colspan="4">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div></td>
				</tr>
				<tr> 
					<td class="campoRequerido" colspan="4"><midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" /></td>
					<td colspan="2">&nbsp;</td>
				</tr>
      			</table>
      			<logic:notEmpty name="esEndoso" scope="request">
					<div class="subtituloIzquierdaDiv">
						Datos de la P&oacute;liza
					</div>
					<table id="desplegar" border="0">
						<tr>
							<th>N&uacute;mero de P&oacute;liza:</th>
							<td class="txt_v"><bean:write name="numeroPoliza" scope="request"/></td>
							<th>Fecha de Emisi&oacute;n:</th>
							<td class="txt_v"><bean:write name="fechaEmision" scope="request" format="dd/MM/yyyy"/></td>							
						</tr>
					</table>
				</logic:notEmpty>
      			<div id="errores" style="display: none;"><html:errors/></div>
      			<div id="accordionOT" style="position: relative; height: 250px; width: 97%;"></div>
      			<br/>								
			<div class="alinearBotonALaDerecha">
				<div id="b_regresar">
					<%--<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/listarOrdenesTrabajo.do','contenido',cerrarObjetosEdicionOT());">
					<midas:mensaje clave="midas.accion.regresar"/>
					</a>--%>
					<logic:notEmpty property="esCotizacion" name="cotizacionForm">
						<logic:equal property="esCotizacion" name="cotizacionForm" value="0">
							<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/listarOrdenesTrabajo.do','contenido',cerrarObjetosEdicionOT());" tipo="regresar"/>
						</logic:equal>
						<logic:equal property="esCotizacion" name="cotizacionForm" value="1">
							<midas:boton onclick="javascript: cerrarObjetosEdicionOT();cargaTabDatosGeneralesCot();" tipo="regresar"/>
						</logic:equal>
					</logic:notEmpty>
					
				</div>
				<div id="b_guardar">
					<%--<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/guardarOrdenTrabajo.do','contenido',cerrarObjetosEdicionOT());">
					<midas:mensaje clave="midas.accion.guardar"/>
					</a>--%>
					<logic:notEmpty property="esCotizacion" name="cotizacionForm">
						<logic:equal property="esCotizacion" name="cotizacionForm" value="0">
							<midas:boton onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/guardarOrdenTrabajo.do','contenido_desplegarDetalle',';validarModificarOrdenTrabajo();');" tipo="guardar"/>
						</logic:equal>
						<logic:equal property="esCotizacion" name="cotizacionForm" value="1">
						<!-- SE HA DUPLICADO EL BOTON PARA QUE CUALQUIER CAMBIO AL BOTON ORIGINAL(EL DE OT) NO AFECTE LA NAVEGACION DE LA COTIZACION -->
							<midas:boton onclick="javascript: guardarCotizacionDatosGenerales();" tipo="guardar"/>
						</logic:equal>
					</logic:notEmpty>
				</div>					
			</div>       							
		</midas:formulario>
	</center>
</div>
