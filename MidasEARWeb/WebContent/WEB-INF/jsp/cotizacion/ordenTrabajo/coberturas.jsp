<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<div class="subtituloCotizacion"><bean:message key="configuracion.cobertura.de"/>: <bean:write name="tituloCoberturas" scope="request" /> </div>
<div class="subtituloCotizacion">
	<bean:message key="configuracion.cobertura.ordentrabajo"/>: <bean:message key="configuracion.cobertura.ordentrabajo.prefijo"/> <bean:write name="idToCotizacionS" scope="request" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<bean:message key="configuracion.cobertura.fecha"/>: <bean:write name="fecha" scope="request" />
</div>
<div style="clear:both"></div>

<table id="desplegarDetalle">
	<tr>
		<td width="100%">
			<div id="cotizacionCoberturasGrid" class="dataGridQuotationClass"></div>
		</td>
	</tr>
</table>


