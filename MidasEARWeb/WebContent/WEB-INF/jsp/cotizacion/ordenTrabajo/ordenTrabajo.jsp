<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html:hidden property="idToCotizacion" name="cotizacionForm" styleId="idToCotizacion" />

<div hrefmode="ajax-html"  style="height: 450px;width: 100%; " id="ordenTrabajoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#F4F3EE,#FCFBFC" >
	<div width="150px" id="desplegarDetalle" name="Datos Generales" extraAction="sendRequest(null,'/MidasWeb/cotizacion/mostrarDatosODT.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_desplegarDetalle', 'inicializaObjetosEdicionOT(<bean:write name="cotizacionForm" property="idToCotizacion" />);');"></div>
	
	<logic:equal value="true" property="editaIncisos" name="cotizacionForm">
		<div width="150px" id="resumenIncisos" name="Datos de Incisos" extraAction="sendRequest(null,'/MidasWeb/cotizacion/inciso/listar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />&origen=ODT', 'contenido_resumenIncisos', null)"></div>
	</logic:equal>
	
	<logic:equal value="true" property="editaPrimerRiesgo" name="cotizacionForm">
		<div width="150px" id="primerRiesgoLUC" name="Primer Riesgo / LUC" extraAction="sendRequest(null,'/MidasWeb/cotizacion/primerRiesgoLUC/mostrar.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_primerRiesgoLUC', 'inicializaPrimerRiesgoLUC(<bean:write name="cotizacionForm" property="idToCotizacion" />);');"></div>
	</logic:equal>
	<div width="150px" id="aclaraciones" name="Aclaraciones" extraAction="sendRequest(null, '/MidasWeb/ordenTrabajo/mostrarAclaraciones.do?idToCotizacion=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_aclaraciones', null);"></div>
</div>
<div id="origen" style="display: none;">ODT</div>