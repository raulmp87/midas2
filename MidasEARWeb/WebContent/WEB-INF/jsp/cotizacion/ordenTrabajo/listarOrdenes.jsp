<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario  accion="/cotizacion/listarOrdenesTrabajo">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="cotizacionForm" property="totalRegistros"/>
	
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar"/> Ordenes de Trabajo
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros">
		<tr>
			<th>N&uacute;mero Orden de Trabajo:</th>
			<td><midas:texto propiedadFormulario="idToCotizacion" nombreFormulario="cotizacionForm" onkeypress="return soloNumeros(this, event, false)"/></td>
			<td>&nbsp</td>
			<td>&nbsp</td>
			<td>&nbsp</td>
			<td>&nbsp</td>						
		</tr>
		<tr>
			<th>Nombre Asegurado:</th>
			<td colspan="3">
				<midas:texto propiedadFormulario="nombreEmpresaAsegurado" nombreFormulario="cotizacionForm"/>
			</td>
			<th><midas:mensaje clave="midas.ordenesdetrabajo.estatus"/>:</th>
			<td> <midas:comboValorFijo grupoValores="30" propiedad="claveEstatus" styleClass="cajaTexto" nombre="cotizacionForm" /> </td>			
		</tr>
		<tr>
			<th>Nombre Solicitante:</th>
			<td>
				<midas:texto propiedadFormulario="nombreSolicitante" nombreFormulario="cotizacionForm"/>
			</td>
			<th>A.Paterno Solicitante:</th>
			<td>
				<midas:texto propiedadFormulario="apellidoPaternoSolicitante" nombreFormulario="cotizacionForm"/>
			</td>
			<th>A.Materno Solicitante:</th>
			<td>
				<midas:texto propiedadFormulario="apellidoMaternoSolicitante" nombreFormulario="cotizacionForm"/>
			</td>					
		</tr>		
		<tr>
			<td colspan="5"></td>
			<th>
			<midas:boton onclick="javascript: sendRequest(document.cotizacionForm, '/MidasWeb/cotizacion/listarOrdenesTrabajoFiltrado.do', 'contenido', null);" tipo="buscar"/>
			</th>
		</tr>
	</table>
	<br/>
	<div id="resultados">
		<midas:tabla idTabla="ordenesDeTrabajoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.OrdenTrabajo"
			claseCss="tablaConResultados" nombreLista="listaOrdenDeTrabajo"
			urlAccion="/cotizacion/ordenTrabajo/listarFiltradoPaginado.do"  exportar="false"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="ordenTrabajo" />
			<midas:columna propiedad="fechaCreacion" formato="{0,date,dd/MM/yyyy}"/>
			<midas:columna propiedad="cliente" maxCaracteres="20" />
			<midas:columna propiedad="producto" maxCaracteres="20" />
			<midas:columna propiedad="tipoPoliza" maxCaracteres="15"/>
			<midas:columna propiedad="tipoOrden" maxCaracteres="10"/>
			<midas:columna propiedad="estatus" maxCaracteres="15"/>
			<midas:columna propiedad="acciones" />
		</midas:tabla>
	</div>
</midas:formulario>
