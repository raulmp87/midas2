<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<midas:formulario accion="/cotizacion/listarDocumentosDigitalesComplementarios">
	<html:hidden property="idToCotizacion" name="documentosCotizacionForm" styleId="idToCotizacion"/>
	<html:hidden property="mensaje" name="documentosCotizacionForm" styleId="mensajeAclaraciones"/>
	<html:hidden property="tipoMensaje" name="documentosCotizacionForm" styleId="tipoMensajeAclaraciones"/>
	<table id="agregar" width="600px">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.cotizacion.aclaraciones" />	
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<midas:areatexto propiedadFormulario="aclaraciones" id="aclaraciones" renglones="10" onKeyDown="if(event.keyCode == 8 || event.keyCode == 46) return true; if(this.value.length == 2000) return false;"/>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="sendRequest(document.documentosCotizacionForm, '/MidasWeb/ordenTrabajo/guardarAclaraciones.do', 'contenido_aclaraciones', 'mostrarVentanaMensajeAclaraciones(null);');" tipo="Guardar"/>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>
