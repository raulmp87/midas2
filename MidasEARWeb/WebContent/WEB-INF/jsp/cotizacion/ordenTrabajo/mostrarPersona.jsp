<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxwindows_dhx_blue.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/curpRFC.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<midas:formulario accion="/cotizacion/mostrarPersona">
 	<midas:oculto propiedadFormulario="idToCotizacion"/>
 	<midas:oculto propiedadFormulario="clienteGeneral.idCliente"/>
 	<midas:oculto propiedadFormulario="clienteGeneral.descripcionPadre"/>
 	<midas:oculto propiedadFormulario="clienteGeneral.idPadre"/>
 	<midas:oculto propiedadFormulario="clienteGeneral.codigoPersona"/>
 	<html:hidden property="mensaje" name="cotizacionForm" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" name="cotizacionForm" styleId="tipoMensaje"/>
	<div id="areaLectura">
		<div class="subtituloIzquierdaDiv">Datos Personales</div>	
			<table id="desplegar">
				<logic:equal value="1" property="clienteGeneral.claveTipoPersona" name="cotizacionForm" >
					<tr>
						<th><midas:mensaje clave="midas.persona.tipo"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.descripcionTipoPersona" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.email"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.email" nombre="cotizacionForm"/></td>
						<th>&nbsp;</th><td class="txt_v">&nbsp;</td>
					</tr>				
					<tr class="bg_t2">
						<th><midas:mensaje clave="midas.persona.nombre"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.nombre" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.apellidoPaterno" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.apellidoMaterno" nombre="cotizacionForm"/></td>			
					</tr>
					<tr>
						<th><midas:mensaje clave="midas.persona.rfc"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.codigoRFC" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.fechaNacimiento"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.fechaNacimiento" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.telefono"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.telefono" nombre="cotizacionForm"/></td>
					</tr>
					<tr class="bg_t2">
						<th><midas:mensaje clave="midas.persona.sexo"/></th>
						<td class="txt_v">
							<logic:equal value="M" property="clienteGeneral.sexo" name="cotizacionForm">
								Masculino
							</logic:equal>
							<logic:equal value="F" property="clienteGeneral.sexo" name="cotizacionForm">
								Femenino
							</logic:equal>							
						</td>					
						<th><midas:mensaje clave="midas.persona.curp"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.codigoCURP" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.estadoNacimiento"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.descripcionEstadoNacimiento" nombre="cotizacionForm"/></td>				
					</tr>				
				</logic:equal>
				<logic:equal value="2" property="clienteGeneral.claveTipoPersona" name="cotizacionForm" >
					<tr>
						<th><midas:mensaje clave="midas.persona.tipo"/></th>
						<td class="txt_v" colspan="2"><midas:escribe propiedad="clienteGeneral.descripcionTipoPersona" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.email"/></th>
						<td class="txt_v" colspan="2" ><midas:escribe propiedad="clienteGeneral.email" nombre="cotizacionForm"/></td>	
					</tr>				
					<tr class="bg_t2">
						<th><midas:mensaje clave="midas.persona.razonSocial"/></th>
						<td class="txt_v" colspan="5"><midas:escribe propiedad="clienteGeneral.nombre" nombre="cotizacionForm"/></td>
					</tr>
					<tr>
						<th><midas:mensaje clave="midas.persona.fechaConstitucion"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.fechaNacimiento" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.rfc"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.codigoRFC" nombre="cotizacionForm"/></td>						
						<th><midas:mensaje clave="midas.persona.telefono"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.telefono" nombre="cotizacionForm"/></td>
					</tr>
					<tr class="bg_t2">
						<th colspan="2"><midas:mensaje clave="midas.persona.nombre"/> Respresentante</th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.nombreRepresentante" nombre="cotizacionForm"/></td>
						<th colspan="2"><midas:mensaje clave="midas.persona.apellidoPaterno"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.apellidoPaterno" nombre="cotizacionForm"/></td>
					</tr>
					<tr>
						<th colspan="2"><midas:mensaje clave="midas.persona.apellidoMaterno"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.apellidoMaterno" nombre="cotizacionForm"/></td>						
						<th colspan="2"><midas:mensaje clave="midas.persona.curp"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.codigoCURP" nombre="cotizacionForm"/></td>
					</tr>					
					<tr class="bg_t2">
						<th colspan="2"><midas:mensaje clave="midas.persona.fechaNacimiento"/></th>
						<td class="txt_v"><midas:escribe propiedad="clienteGeneral.fechaNacimientoRepresentante" nombre="cotizacionForm"/></td>
						<th><midas:mensaje clave="midas.persona.estadoNacimiento"/></th>
						<td class="txt_v" colspan="2"><midas:escribe propiedad="clienteGeneral.descripcionEstadoNacimiento" nombre="cotizacionForm"/></td>
					</tr>
					<tr>																																								
						<th><midas:mensaje clave="midas.persona.sexo"/></th>
						<td class="txt_v">
							<logic:equal value="M" property="clienteGeneral.sexo" name="cotizacionForm">
								Masculino
							</logic:equal>
							<logic:equal value="F" property="clienteGeneral.sexo" name="cotizacionForm">
								Femenino
							</logic:equal>							
						</td>						
					</tr>				
				</logic:equal>
			</table>
		<div class="subtituloIzquierdaDiv">Datos Domicilio</div>	
			<table id="desplegar">
				<tr>
					<th><midas:mensaje clave="midas.direccion.nombreCalle"/>:</th>
					<td colspan="3" class="txt_v"><midas:escribe propiedad="clienteGeneral.nombreCalle" nombre="cotizacionForm"/></td>
				</tr>
				<tr>
					<th><midas:mensaje clave="midas.direccion.nombreColonia"/>:</th>
					<td class="txt_v" width= "30%" ><midas:escribe propiedad="clienteGeneral.nombreColonia" nombre="cotizacionForm"/><%--<midas:oculto propiedadFormulario="direccionGeneral.idColonia"/> --%></td>
					<th><midas:mensaje clave="midas.direccion.idMunicipio"/>:</th>
					<td class="txt_v" width= "30%"><midas:escribe propiedad="clienteGeneral.nombreDelegacion" nombre="cotizacionForm"/> <midas:oculto propiedadFormulario="clienteGeneral.idMunicipio" /></td>
				</tr>
				<tr class="bg_t2">
					<th><midas:mensaje clave="midas.direccion.idEstado"/>:</th>
					<td class="txt_v"><midas:escribe propiedad="clienteGeneral.descripcionEstado" nombre="cotizacionForm"/> <midas:oculto propiedadFormulario="clienteGeneral.idEstado"/> </td>
					<th><midas:mensaje clave="midas.direccion.codigoPostal"/>:</th>
					<td class="txt_v"><midas:escribe propiedad="clienteGeneral.codigoPostal" nombre="cotizacionForm"/></td>
				</tr>			
			</table>
			<% if(request.getAttribute("origen") == null ) { %>
				<div class="alinearBotonALaDerecha">	
					<div id="b_guardar">
						<a href="javascript: void(0);"
							onclick="mostrarAgregarClienteCotizacion(document.cotizacionForm.elements.namedItem('clienteGeneral.idPadre').value,document.cotizacionForm.elements.namedItem('clienteGeneral.codigoPersona').value,document.cotizacionForm.elements.namedItem('clienteGeneral.idCliente').value);">
						<midas:mensaje clave="midas.accion.modificar"/>
						</a>
					</div>
				</div>
			<% } %>
	</div>
</midas:formulario>