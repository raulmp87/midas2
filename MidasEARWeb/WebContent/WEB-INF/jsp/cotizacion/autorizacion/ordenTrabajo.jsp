<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<html:hidden property="idToCotizacion" name="cotizacionForm" styleId="idToCotizacion" />

<div hrefmode="ajax-html"  style="height: 450px;width: 100%; " id="ordenTrabajoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#F4F3EE,#FCFBFC" >
	<div width="150px" id="desplegarDetalle" name="Datos de la solicitud" extraAction="sendRequest(null,'/MidasWeb/cotizacion/autorizacion/mostrarDatosODT.do?id=<bean:write name="cotizacionForm" property="idToCotizacion" />', 'contenido_desplegarDetalle', 'inicializaObjetosEdicionOT(<bean:write name="cotizacionForm" property="idToCotizacion" />);');"></div>
</div>
