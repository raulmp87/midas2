<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="detalle" name="Detalle">
	<center>
		<midas:formulario accion="/cotizacion/guardarOrdenTrabajo">
			<html:hidden property="idToCotizacion" styleId="idToCotizacion" />
			<html:hidden property="esCotizacion" styleId="esCotizacion"
				name="cotizacionForm" />
				<html:hidden property="claveEstatus" styleId="claveEstatus"
				name="cotizacionForm" />
			<html:hidden property="idToProducto" />
			<logic:empty property="esCotizacion" name="cotizacionForm">
				<div class="subtituloCotizacion">
					Datos de la Orden de Trabajo
				</div>
				<div class="subtituloCotizacion">
					<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo" />
					:
					<midas:escribe propiedad="idOrdenTrabajoFormateada"
						nombre="cotizacionForm" />
			</logic:empty>
			<logic:notEmpty property="esCotizacion" name="cotizacionForm">
				<logic:equal property="esCotizacion" name="cotizacionForm" value="0">
					<div class="subtituloCotizacion">
						Datos de la Orden de Trabajo
					</div>
					<div class="subtituloCotizacion">
						<midas:mensaje clave="midas.ordendetrabajo.ordenTrabajo" />
						:
						<midas:escribe propiedad="idOrdenTrabajoFormateada"
							nombre="cotizacionForm" />
				</logic:equal>
				<logic:equal property="esCotizacion" name="cotizacionForm" value="1">
					<div class="subtituloCotizacion">
						<midas:mensaje clave="midas.cotizacion.datosCotizacion" />
					</div>
					<div class="subtituloCotizacion">
						<midas:mensaje clave="midas.cotizacion.cotizacion" />
						:
						<midas:escribe propiedad="idToCotizacionFormateada"
							nombre="cotizacionForm" />
				</logic:equal>
			</logic:notEmpty>
			<html:hidden property="idOrdenTrabajoFormateada" />
				&nbsp;&nbsp;<midas:mensaje clave="midas.ordendetrabajo.fecha" />:<c:out
				value="${cotizacionForm.fechaCreacion}"></c:out>
			</div>
			<div style="clear: both"></div>
			<table id="desplegar" border="0">
				<tr>
					<th>
						<midas:mensaje clave="midas.ordendetrabajo.nombreSolicitante" />
						:
					</th>
					<td class="txt_v">
						<midas:escribe propiedad="nombreSolicitante"
							nombre="cotizacionForm" />
						<html:hidden property="nombreSolicitante" />
					</td>
					<th>
						<midas:mensaje clave="midas.ordendetrabajo.telefonoContacto" />
						:
					</th>
					<td class="txt_v">
						<midas:escribe propiedad="telefonoContacto"
							nombre="cotizacionForm" />
						<html:hidden property="telefonoContacto" />
					</td>
				</tr>
				<tr class="bg_t2">
					<th>
						<midas:mensaje clave="midas.ordendetrabajo.productoPorContratar" />
						:
					</th>
					<td class="txt_v">
						<midas:escribe propiedad="producto" nombre="cotizacionForm" />
						<html:hidden property="producto" />
					</td>
					<th>
						<midas:mensaje clave="midas.ordendetrabajo.oficinaVenta" />
						:
					</th>
					<td class="txt_v">
						<midas:escribe propiedad="oficina" nombre="cotizacionForm" />
						<html:hidden property="oficina" />
					</td>
				</tr>
				<tr>
					<th>
						<midas:mensaje clave="midas.ordendetrabajo.agenteSeguro" />
						:
					</th>
					<td class="txt_v">
						<midas:escribe propiedad="nombreAgente" nombre="cotizacionForm" />
						<html:hidden property="nombreAgente" />
					</td>
					<th></th>
					<td></td>
				</tr>
			</table>
			<div class="subtituloIzquierdaDiv">
				<midas:mensaje clave="midas.ordendetrabajo.datosProducto" />
			</div>
			<html:hidden property="permiteCambiarPoliza" name="cotizacionForm"
				styleId="permiteCambiarPoliza" />
			<input type="hidden" id="poliza"
				value="<midas:escribe propiedad="idToTipoPoliza" nombre="cotizacionForm"/>" />
			<table id="desplegarDetalle">
				<tr>
					<th width="15%">
						<etiquetas:etiquetaError property="idToTipoPoliza" requerido="si"
							key="midas.ordendetrabajo.tipoPoliza" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>
					<td width="25%">
						<midas:combo id="selectTipoPoliza" propiedad="idToTipoPoliza" deshabilitado="true"
							nombreFormulario="cotizacionForm"
							onchange="if (validarCambioEnTipoPoliza() )getMonedasTipoPoliza(this,'idTcMoneda');"
							styleClass="cajaTexto">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="cotizacionForm"
								property="listaTipoPoliza" value="idToTipoPoliza"
								label="nombreComercial" />
						</midas:combo>
					</td>
					<th width="15%">
						<etiquetas:etiquetaError property="idMoneda" requerido="si"
							key="midas.ordendetrabajo.moneda" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>
					<td width="25%">
						<midas:combo propiedad="idMoneda" deshabilitado="true"
							nombreFormulario="cotizacionForm" id="idTcMoneda"
							styleClass="cajaTexto">
							<midas:opcionCombo valor="">Seleccione...</midas:opcionCombo>
							<html:optionsCollection name="cotizacionForm"
								property="listaMoneda" value="idTcMoneda" label="descripcion" />
						</midas:combo>
					</td>
					<td width="10%">
						&nbsp;
					</td>
					<td width="10%">
						&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="idFormaPago" requerido="si"
							key="midas.ordendetrabajo.formaPago" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>
					<td>
						<html:select property="idFormaPago" name="cotizacionForm" disabled="true"
							styleClass="cajaTexto">
							<html:optionsCollection name="cotizacionForm"
								property="listaFormaPago" value="idFormaPago"
								label="descripcion" />
						</html:select>
					</td>
					<th>
						<center>
							<midas:mensaje clave="midas.ordendetrabajo.vigencia" />
							<a href="javascript: void(0);" id="mostrarCalendario"
								onclick=
	mostrarCalendarioDobleOT();
> <image
									src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /> </a>
						</center>
					</th>
					<th>
						<etiquetas:etiquetaError property="fechaInicioVigencia"
							requerido="si" key="midas.ordendetrabajo.vigenciaFechaInicial"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<th colspan="2">
						<etiquetas:etiquetaError property="fechaFinVigencia"
							requerido="si" key="midas.ordendetrabajo.vigenciaFechaFinal"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="idMedioPago" requerido="si"
							key="midas.ordendetrabajo.medioPago" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:combo propiedad="idMedioPago" deshabilitado="true"
							nombreFormulario="cotizacionForm" styleClass="cajaTexto">
							<html:optionsCollection name="cotizacionForm"
								property="listaMedioPago" value="idMedioPago"
								label="descripcion" />
						</midas:combo>
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						<midas:texto id="fechaInicial" deshabilitado="true"
							propiedadFormulario="fechaInicioVigencia"
							nombreFormulario="cotizacionForm" longitud="10"
							onblur="esFechaValida(this);" />
					</td>
					<td colspan="2">
						<midas:texto id="fechaFinal" deshabilitado="true"
							propiedadFormulario="fechaFinVigencia"
							nombreFormulario="cotizacionForm" longitud="10"
							onblur="esFechaValida(this);" />
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td colspan="4">
						<div id="rangoDeFechas" style="position: absolute; z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="campoRequerido" colspan="4">
						<midas:mensaje
							clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
					</td>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
			<div class="subtituloIzquierdaDiv">
				Autorizaciones Pendientes
			</div>
			<table id="agregar">
				<logic:equal property="mostrarChkAutRetroDifer" name="cotizacionForm" value="1">					
					<tr>
						<td>
							<html:checkbox property="claveAutRetroacDifer" name="cotizacionForm"/>
						</td>
						<td class="campoRequerido" colspan="5">
							Retroactividad o Diferimiento <logic:equal value="1" property="esRenovacion" name="cotizacionForm" > de Renovaci&oacute;n</logic:equal>
						</td>					
					</tr>
				</logic:equal>
				<logic:equal property="mostrarChkAutVigMaxMin" name="cotizacionForm" value="1">
					<tr>
						<td width="10%">
							<html:checkbox property="claveAutVigenciaMaxMin" name="cotizacionForm"/>
						</td>
						<td class="campoRequerido" colspan="5">
							Vigencia Min&iacute;ma o Max&iacute;ma
						</td>					
					</tr>					
				</logic:equal>
				<logic:equal property="mostrarChkAutRPF" name="cotizacionForm" value="1">
					<tr>
						<td width="10%">
							<html:checkbox property="claveAutorizacionRPF" name="cotizacionForm"/>
						</td>
						<td class="campoRequerido" colspan="5">
							Recargo financiero
						</td>					
					</tr>					
				</logic:equal>	
				<logic:equal property="mostrarChkAutDerechos" name="cotizacionForm" value="1">
					<tr>
						<td width="10%">
							<html:checkbox property="claveAutorizacionDerechos" name="cotizacionForm"/>
						</td>
						<td class="campoRequerido" colspan="5">
							Gastos de expedici&oacute;n
						</td>					
					</tr>					
				</logic:equal>
				<logic:equal property="mostrarChkAutCliente" name="cotizacionForm" value="1">
					<tr>
						<td width="10%">
							<html:checkbox property="claveAutorizacionCliente" name="cotizacionForm"/>
						</td>
						<td class="campoRequerido" colspan="5">
							Renovaci&oacute;n aceptada por el Cliente
						</td>					
					</tr>					
				</logic:equal>
				<logic:equal property="mostrarChkAutDiasGracia" name="cotizacionForm" value="1">
					<tr>
						<td width="10%">
							<html:checkbox property="claveAutorizacionDiasGracia" name="cotizacionForm"/>
						</td>
						<td class="campoRequerido" colspan="5">
							Autorizar dias de gracia (<bean:write property="diasGracia" name="cotizacionForm"/>).
						</td>
					</tr>
				</logic:equal>
			</table>

			<div id="errores" style="display: none;">
				<html:errors />
			</div>
			
			<br>
			<div class="alinearBotonALaDerecha">
				<logic:present name="esEndoso">
					<div id="b_regresar">
						<midas:boton
							onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/endoso/listar.do','contenido','cerrarObjetosEdicionOT();');"
							tipo="regresar" />
					</div>
					<div id="b_guardar">
						<midas:boton
							onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/autorizacion/autorizarOrdenTrabajo.do','contenido','cerrarObjetosEdicionOT(); listarCotizacionesEndoso();');"
							tipo="guardar" texto="Autorizar"/>
					</div>
					<div id="b_guardar">
						<midas:boton
							onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/autorizacion/rechazarOrdenTrabajo.do','contenido','cerrarObjetosEdicionOT(); listarCotizacionesEndoso();');"
							tipo="guardar" texto="Rechazar"/>
					</div>						
				</logic:present>
				<logic:notPresent name="esEndoso">
					<div id="b_regresar">
						<midas:boton
							onclick="javascript: regresarListado();"
							tipo="regresar" />
					</div>
					<div id="b_guardar">
						<midas:boton
							onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/autorizacion/autorizarOrdenTrabajo.do',null,'regresarListado();');"
							tipo="guardar" texto="Autorizar"/>
					</div>
					<div id="b_guardar">
						<midas:boton
							onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/autorizacion/rechazarOrdenTrabajo.do',null,'regresarListado();');"
							tipo="guardar" texto="Rechazar"/>
					</div>											
				</logic:notPresent>		
			</div>
		</midas:formulario>
	</center>
</div>
