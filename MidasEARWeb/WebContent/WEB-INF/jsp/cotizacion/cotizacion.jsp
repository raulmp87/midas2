<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/cotizacion/guardarOrdenTrabajo">
<center>
	<div hrefmode="ajax-html"  id="configuracionProductoTabBar" class="dhtmlxTabBar" imgpath="/MidasWeb/img/imgs_tabbar/" skinColors="#FCFBFC,#F4F3EE" >
		<div id="detalle" name="Detalle">
			<table id="desplegarDetalle" width="98%" border="0">
				<tr>
					<td class="titulo" colspan="4">
		           		<midas:mensaje clave="midas.ordendetrabajo.datosOrdenTrabajo"/>
		           		<midas:oculto propiedadFormulario="idToCotizacion"/>
					</td>
				</tr>			
				<tr>
					<td class="campoRequerido" colspan="2">www
			 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
					</td>
					<td class="guardar">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);"
								onclick="javascript: sendRequest(null,'/MidasWeb/cotizacion/listarOrdenesTrabajo.do','contenido',cerrarObjetosEdicionOT());">
								<midas:mensaje clave="midas.accion.regresar"/>
								</a>
							</div>
							<midas:boton onclick="" tipo="guardar"/>
						</div>
					</td>
					<td class="guardar">
						<div class="alinearBotonALaDerecha">
							<div id="b_guardar">
								<a href="javascript: void(0);"
								onclick="javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/guardarOrdenTrabajo.do','contenido',cerrarObjetosEdicionOT());">
								<midas:mensaje clave="midas.accion.guardar"/>
								</a>
							</div>
							<midas:boton onclick="" tipo="guardar"/>
						</div>
					</td>
				</tr>			
			</table>		
		</div>
		<div width="150px" id="resumenIncisos" name="Datos de Incisos" href="http://void" extraAction="sendRequest(null,'/MidasWeb/cotizacion/inciso/listar.do?id=<midas:escribe propiedad="idToCotizacion" nombre="cotizacionForm"/>', 'contenido_resumenIncisos', null)"></div>
	</div>
</center>	
</midas:formulario>