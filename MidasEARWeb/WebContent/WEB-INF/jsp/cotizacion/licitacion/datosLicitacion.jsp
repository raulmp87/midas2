<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario accion="/cotizacion/mostrarDatosLicitacion">    
	<html:hidden property="idToCotizacion" name="datosLicitacionForm" styleId="idToCotizacion" />
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<table id="desplegarDetalle" width="600px">		
		<tr>
			<td class="subtituloIzquierdaDiv" colspan="2">
				<midas:mensaje clave="midas.cotizacion.licitacion.datosLicitacion"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="midas.cotizacion.licitacion.textoLicitacion"/>:</th>
			<td> <midas:areatexto propiedadFormulario="textoLicitacion" nombreFormulario="datosLicitacionForm" renglones="6" columnas="70"/></td>
		</tr>
		<logic:equal name="datosLicitacionForm" property="mostrarRadioBttnClaveImpresionSA" value="1">
		<tr>
			<td colspan="2"><midas:radio valorEstablecido="0" propiedadFormulario="claveImpresionSumaAsegurada"><midas:mensaje clave="midas.cotizacion.licitacion.imprimirSACantidad"/></midas:radio></td>						
		</tr>
		<tr>
			<td colspan="2"><midas:radio valorEstablecido="1" propiedadFormulario="claveImpresionSumaAsegurada"><midas:mensaje clave="midas.cotizacion.licitacion.imprimirSASEA"/></midas:radio></td>						
		</tr>
		</logic:equal>	
		<tr>
			<td colspan="2">
				<div class="alinearBotonALaDerecha">				
					<div id="b_guardar">									
						<midas:boton onclick="javascript: sendRequest(document.datosLicitacionForm,'/MidasWeb/cotizacion/guardarDatosLicitacion.do','contenido_datosLicitacion','mostrarMensajeDatosLicitacion()');" tipo="guardar"/>												
					</div>					
				</div>
			</td>
		</tr>				
	</table>	
	
	
</midas:formulario>