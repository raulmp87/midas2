
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<link href="<html:rewrite page="/css/dhtmlxcalendar.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcalendar.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/popup.js"/>"></script>

<midas:formulario  accion="/endoso/cancelacion/listar">
	<div id="contenido">
		<midas:oculto propiedadFormulario="idPoliza" nombreFormulario="cancelacionEndosoForm"/>
		<table width="98%">
			<tr>
				<td class="titulo" colspan="6">
					<midas:mensaje clave="endoso.cancelacion.titulo"/>
				</td>
			</tr>
		</table>
		<div id="resultados" style="width: 98%; border: 0px solid red; font-size: 10px;" >
		
			<display:table 
				pagesize="10" 
				name="listaEndososCancelables" sort="page" defaultsort="1" defaultorder="ascending" 
				cellpadding="0" cellspacing="0"
				requestURI="/endoso/cancelacion/listar.do" id="endosos" 
				decorator="mx.com.afirme.midas.decoradores.CancelacionRehabilitacionEndoso" 
				class="tablaConResultados">
				<midas:columna propiedad="numeroEndoso" titulo="N&uacute;mero de Endoso" maxCaracteres="20" />
				<midas:columna propiedad="motivoEndoso" titulo="Motivo del endoso" />
				<midas:columna propiedad="claveTipoEndoso" titulo="Tipo de endoso" />
				<midas:columna propiedad="fechaCreacion" titulo="Fecha de Emisi�n" formato="{0,date,dd/MM/yyyy}"/>
				<midas:columna propiedad="fechaInicioVigencia" titulo="Fecha de Inicio Vigencia"/>
				<midas:columna propiedad="acciones" titulo="Cancelar" />
			</display:table>
		
		
		
		
		
		</div>
	</div>
</midas:formulario>

