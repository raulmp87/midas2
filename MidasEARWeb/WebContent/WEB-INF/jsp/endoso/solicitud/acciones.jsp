<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/endoso/solicitud/autoAsignarOrden">
	<table id="agregar">
	<tr>
		<th>Acci&oacute;n: </th>
		<td width="90%">
			<midas:combo propiedad="accion" styleClass="cajaTexto" nombreFormulario="solicitudEndosoForm">
				<midas:opcionCombo valor="1">Capturar Detalle Orden de Trabajo</midas:opcionCombo>
				<midas:opcionCombo valor="2">Agregar Orden de Trabajo</midas:opcionCombo>
				<midas:opcionCombo valor="3">Cancelar</midas:opcionCombo>
			</midas:combo>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="botonAgregar">
				<div class="alinearBotonALaDerecha">
					<midas:boton onclick="javascript: parent.dhxWins.window('accionesSolicitudEndoso').close(); " tipo="regresar"/>
					<midas:boton onclick="javascript: parent.sendRequestSolicitudEndoso(null,'/MidasWeb/endoso/solicitud/autoAsignarOrden.do?accion=' + document.solicitudEndosoForm.accion.value,'contenido','parent.dhxWins.window(\&#39;accionesSolicitudEndoso\&#39;).close();',  document.solicitudEndosoForm.accion.value);" tipo="continuar"/>
				</div>
			</div>
		</td>
	</tr>
</table>
</midas:formulario>
 