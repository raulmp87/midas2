<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="detalle">
	<center>
		<midas:formulario accion="/endoso/solicitud/agregar">
		<html:hidden property="presentaComboMotivo" name="solicitudEndosoForm" styleId="presentaComboMotivo" />
			<table id="desplegarDetalle">
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Datos de la Solicitud de Endoso
					</td>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<td width="18%">
								Solicitud: <%= "SOL-" + String.format("%08d", new Object[]{session.getAttribute("idSolicitud")}) %>
							</td>
						</c:when>
						<c:otherwise><td width="18%">&nbsp;</td></c:otherwise>
					</c:choose>			
					<td width="17%">
						Fecha: <fmt:formatDate value="${solicitudEndosoForm.fechaSolicitud}" dateStyle="short" />
					</td>
				</tr>
			</table>
			<table id="desplegar" border="0">
			
			
					<td colspan="4">
					 <table id="tbEndoso" style="border: none; padding: 0px;font-size: 9px;">
					 <tr>
				  
					   <th width="25%">
								<etiquetas:etiquetaError property="numPoliza" 
														normalClass="normal" errorClass="error" errorImage="/img/information.gif"
														name="solicitudEndosoForm" 
														key="solicitudEndoso.numPoliza" />
								
							</th>			
						<td><midas:escribe propiedad="numPoliza" nombre="solicitudEndosoForm"/></td>
								
						
					</tr>
					<tr>
					 
					  
					  	<th width="25%">
					  			<etiquetas:etiquetaError property="fechaInicioCotizacion" 
														normalClass="normal" errorClass="error" errorImage="/img/information.gif"
														name="solicitudEndosoForm" 
														key="solicitudEndoso.vigenciaCotizacion" />
								
								
						</th>
								<td colspan="2">
								<midas:escribe propiedad="fechaInicioCotizacion" nombre="solicitudEndosoForm"/>
								&nbsp;-&nbsp;
								<midas:escribe propiedad="fechaFinCotizacion" nombre="solicitudEndosoForm"/>
								</td>
						
					</tr>
			
				
					 <tr>
						<th>
							<etiquetas:etiquetaError  property="tipoEndoso" 
									 normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									 name="solicitudEndosoForm" key="solicitudEndoso.tipoEndoso" />
								               
						</th>
						
								
						<td >
								<midas:comboValorFijo grupoValores="40" propiedad="tipoEndoso" styleId="tipoEndoso" 
												readonly="true" nombre="solicitudEndosoForm" styleClass="cajaTexto"/>
								</td>
								
								<td colspan="3">
								<div>
									<table id="datosMotivo" style="width: 360px;display: none;" border="0">
											<tr>
									<th style="font-size: 9px;">
									Motivo de Cancelaci&oacute;n:
									</th>
											<th>
											<midas:comboValorFijo grupoValores="42" propiedad="claveMotivoEndoso" styleId="claveMotivoEndoso" 
															readonly="true" nombre="solicitudEndosoForm" styleClass="cajaTexto"/>
											</th>
										
									</tr>
								</table>
							</div>
						</td>
						</tr>
						
						<tr>
						 	<th>
								<etiquetas:etiquetaError normalClass="normal" errorClass="error" 
									errorImage="/img/information.gif" name="solicitudEndosoForm"
									property="fechaInicioVigenciaEndoso" key="solicitudEndoso.fechaInicioVigencia" requerido="si"/> 
								
						   	</th>
							<td >
								<midas:escribe propiedad="fechaInicioVigenciaEndoso" nombre="solicitudEndosoForm"/>
							</td>
						</tr>
							
					</tr>
			
				<tr>
					<th>
						<etiquetas:etiquetaError property="tipoPersona" requerido="no"
							name="solicitudEndosoForm" key="solicitud.tipoPersona"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<c:choose>
					        <c:when test='${solicitudEndosoForm.tipoPersona == "1"}'>
					            F&iacute;sica
					        </c:when>
					        <c:otherwise>
					            Moral
					        </c:otherwise>
					    </c:choose>
					</td>
				</tr>
				<c:choose>
					<c:when test='${solicitudEndosoForm.tipoPersona == "1"}'>
			            <tr>
							<th>
								<etiquetas:etiquetaError property="nombres" requerido="no"
									name="solicitudEndosoForm" key="solicitud.nombres"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td><midas:escribe propiedad="nombres" nombre="solicitudEndosoForm"/></td>
							<th>
								<etiquetas:etiquetaError property="apellidoPaterno" requerido="no"
									name="solicitudEndosoForm" key="solicitud.apellidoPaterno"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td><midas:escribe propiedad="apellidoPaterno" nombre="solicitudEndosoForm"/></td>
							<c:if test='${!empty solicitudEndosoForm.apellidoMaterno}'>
								<th>
									<etiquetas:etiquetaError property="apellidoMaterno" requerido="no"
										name="solicitudEndosoForm" key="solicitud.apellidoMaterno"
										normalClass="normal" errorClass="error"
										errorImage="/img/information.gif" />
								</th>
								<td><midas:escribe propiedad="apellidoMaterno" nombre="solicitudEndosoForm"/></td>
							</c:if>
						</tr>
			        </c:when>
			        <c:otherwise>
			            <tr>
							<th>
								<etiquetas:etiquetaError property="razonSocial" requerido="no"
									name="solicitudEndosoForm" key="solicitud.razonSocial"
									normalClass="normal" errorClass="error"
									errorImage="/img/information.gif" />
							</th>
							<td><midas:escribe propiedad="razonSocial" nombre="solicitudEndosoForm"/></td>
						</tr>
			        </c:otherwise>
			    </c:choose>
				<tr>
					<th>
						<etiquetas:etiquetaError property="telefono" requerido="no"
							name="solicitudEndosoForm" key="solicitud.telefono"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:escribe propiedad="telefono" nombre="solicitudEndosoForm"/>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="producto" requerido="no"
							name="solicitudEndosoForm" key="solicitud.producto"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:escribe propiedad="nombreComercialProducto" nombre="solicitudEndosoForm"/>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="agenteSeguros" requerido="no"
							name="solicitudEndosoForm" key="solicitud.agenteSeguros"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td>
						<midas:escribe propiedad="nombreAgente" nombre="solicitudEndosoForm"/>
					</td>
				</tr>
				<tr>
					<th>
						<etiquetas:etiquetaError property="correos" requerido="no"
							name="solicitudEndosoForm" key="solicitud.correos"
							normalClass="normal" errorClass="error"
							errorImage="/img/information.gif" />
					</th>
					<td colspan="2">
						<midas:escribe propiedad="correos" nombre="solicitudEndosoForm"/>
					</td>
				</tr>
				<tr>
					<td>
						<midas:checkBox valorEstablecido="0" propiedadFormulario="claveOpcionEmision" deshabilitado="true" >
							<midas:mensaje clave="solicitud.claveOpcionEmision" />
						</midas:checkBox>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>
			</table>
			
			<c:if test="${!empty solicitudEndosoForm.numeroSolicitud}">
				<div id="resultadosDocumentos">
					<midas:tabla idTabla="solicitudes"
						claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalSolicitud"
						claseCss="tablaConResultados" nombreLista="documentos"
						urlAccion="/endoso/solicitud/listarDocumentos.do">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
						<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
						<midas:columna propiedad="descargar" titulo="Fecha" />
					</midas:tabla>
				</div>
				<div class="alinearBotonALaDerecha">		
					<div id="botonAgregar">
						<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
					</div>
				</div>		
			</c:if>
		</midas:formulario>
	</center>
</div>