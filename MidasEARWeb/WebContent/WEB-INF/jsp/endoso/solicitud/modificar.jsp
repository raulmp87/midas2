<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

 
<div id="detalle">
	<center>
		<midas:formulario accion="/endoso/solicitud/agregar">
			<midas:oculto propiedadFormulario="permitirAutoAsignacion"/>
			<midas:oculto propiedadFormulario="bloqueoDatosAgente"/>
			<midas:oculto propiedadFormulario="numeroSolicitud"/>
			<html:hidden property="estatusPoliza" name="solicitudEndosoForm" styleId="estatusPoliza"/>
			<html:hidden property="idPoliza" name="solicitudEndosoForm" styleId="idPoliza"/>
			<html:hidden property="idProducto" name="solicitudEndosoForm" styleId="idProducto"/>
			<html:hidden property="fechaStrSolicitud" name="solicitudEndosoForm" styleId="fechaStrSolicitud" />
			<html:hidden property="cveEstatusPoliza" name="solicitudEndosoForm" styleId="cveEstatusPoliza" />
			<html:hidden property="presentaComboMotivo" name="solicitudEndosoForm" styleId="presentaComboMotivo" />
			
			<table id="desplegarDetalle" border="0">
			
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Datos de la Solicitud del Endoso
					</td>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<td width="18%">
								Solicitud: <%= "SOL-" + String.format("%08d", new Object[]{session.getAttribute("idSolicitud")}) %>
							</td>
						</c:when>
						<c:otherwise><td width="18%">&nbsp;</td></c:otherwise>
					</c:choose>			
					<td width="17%">
						<jsp:useBean id="now" class="java.util.Date" />
						Fecha: <fmt:formatDate value="${now}"  pattern="dd/MM/yyyy"/>
					</td>
				</tr>
			</table>
			<table id="agregar" border="0">
			
				<tr>
				  <td colspan="4">
					  <table id="tbPoliza" style="border: none; padding: 0px;font-size: 9px;">
					   <th width="25%">
								<etiquetas:etiquetaError property="numPoliza" requerido="si" 
														normalClass="normal" errorClass="error" errorImage="/img/information.gif"
														name="solicitudEndosoForm" 
														key="solicitudEndoso.numPoliza" />
								
							</th>
						
								<th width="45%">
										<midas:texto propiedadFormulario="numPoliza" id="numPoliza" 
										nombreFormulario="solicitudEndosoForm" onfocus="new Mask('####-########-##', 'string').attach(this)" 
										caracteres="16"  
										longitud="16" /> 
										
									</th>
							<th width="15%" align="left">
							   <div class="alinearBotonALaIzquierda">
									<div id="b_buscar_poliza">
										<a href="javascript: void(0);" 	onclick="javascript:buscaPolizaAjax();">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.buscarPoliza"/>
										</a>
									</div>
								
								</div>
							</th> 
							
							<th width="15%" align="left">
							 <div class="alinearBotonALaIzquierda">
									<div id="b_buscar_poliza">
										<a href="javascript: void(0);" 	onclick="javascript: mostrarVentanaPolizas($('estatusPoliza').value);">
											<midas:mensaje clave="solicitudEndoso.verPolizas"/>
										</a>
									</div>
								
								</div>
							
							
							</th>
						
					    </table>
					  </td>
					</tr>
					<tr>
					 <td colspan="5" style="border: none; padding: 0px;font-size: 9px;">
					  <table border="0">
					  
					  	<th width="18%" style="font-size: 9px;" align="right">
								<midas:mensaje clave="solicitudEndoso.vigenciaCotizacion"/>
								
						</th>
						<th width="5%" align="left" style="font-size: 9px;">
								<midas:mensaje clave="solicitudEndoso.de"/>
								
						</th>
						
							  <th width="15%" align="left">
										<html:text property="fechaInicioCotizacion" styleId="fechaInicioCotizacion" 
										name="solicitudEndosoForm"  readonly="true" maxlength="10" size="10" styleClass="cajaTexto"/> 
										
								</th>
						
						<th width="5%" align="left" style="font-size: 9px;">
								<midas:mensaje clave="solicitudEndoso.a"/>
								
						</th>
							<th width="20%" align="left">
										<html:text property="fechaFinCotizacion" styleId="fechaFinCotizacion" 
										name="solicitudEndosoForm"  readonly="true"  styleClass="cajaTexto"
										maxlength="10" 
										size="10" /> 
										
								</th>
								<th width="30%" align="left" style="font-size: 9px;">
								&nbsp;
								
								</th>
							
					  </table>
					 </td>
				</tr>
				<tr>
					<td colspan="4">
					 <table id="tbEndoso"  border="0"  style="border: none; padding: 0px;font-size: 9px;">
					 <tr>
						<th width="25%">
							<etiquetas:etiquetaError  property="tipoEndoso" requerido="si" 
									 normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									 name="solicitudEndosoForm" key="solicitudEndoso.tipoEndoso" />
								               
						</th>
						
								<th width="25%">
								<midas:comboValorFijo grupoValores="40" propiedad="tipoEndoso" styleId="tipoEndoso"
												nombre="solicitudEndosoForm"  onchange="javascript:validaCambioTipoEndoso(this.value);"
												styleClass="cajaTexto"/>
								</th>
							
							
							<td colspan="3">
						<div>
								<table id="datosMotivo" style="width: 360px;display: none;" border="0">
									<tr>
							<th style="font-size: 9px;">
							Motivo de Cancelaci&oacute;n:
							</th>
							
									<th>
									<midas:comboValorFijo grupoValores="42" propiedad="claveMotivoEndoso" styleId="claveMotivoEndoso"
													nombre="solicitudEndosoForm"  	styleClass="cajaTexto"/>
									</th>
								
							</tr>
							</table>
						</div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					
						 	<th>
								<div id="etiquetaFecha"><etiquetas:etiquetaError normalClass="normal" errorClass="error" 
									errorImage="/img/information.gif" name="solicitudEndosoForm"
									property="fechaInicioVigenciaEndoso" key="solicitudEndoso.fechaInicioVigencia" requerido="si"/> 
								<c:if test="${empty solicitudEndosoForm.numeroSolicitud}">
									<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
										<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
								</c:if>
								</div>
						   	</th>
						   
									<th width="60%">
								   				<html:text	property="fechaInicioVigenciaEndoso"  maxlength="10" size="15"
								   				 styleId="fecha" name="solicitudEndosoForm"  styleClass="cajaTexto"
												 onkeypress="return soloFecha(this, event, false);"
												 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
									</th>
								
					</tr>
				
					
					
				<tr>
					<th width="10%">
						<etiquetas:etiquetaError property="tipoPersona" requerido="si" normalClass="normal" errorClass="error"
							name="solicitudEndosoForm" key="solicitud.tipoPersona" errorImage="/img/information.gif" />
					</th>
					
							<td width="30%" colspan="2">
								<midas:radio valorEstablecido="1" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">F&iacute;sica</midas:radio>
								<midas:radio valorEstablecido="2" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">Moral</midas:radio>
							</td>
						
					<td width="6%" colspan="3"></td>
				</tr>
				<tr>
					
							<td colspan="6">
								<div id="nombreDiv">
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="10%">
											<etiquetas:etiquetaError property="nombres" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudEndosoForm" key="solicitud.nombres" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="nombres" /></td>
										<th width="10%"><etiquetas:etiquetaError property="apellidoPaterno" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudEndosoForm" key="solicitud.apellidoPaterno" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="apellidoPaterno"/></td>
										<th width="10%"><etiquetas:etiquetaError property="apellidoMaterno" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudEndosoForm" key="solicitud.apellidoMaterno" /> </th>
										<td width="30%"><midas:texto propiedadFormulario="apellidoMaterno"/></td>
									</table>
								</div>
								<div id="razonSocialDiv" style="display: none;">
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="15%"><etiquetas:etiquetaError property="razonSocial" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="solicitudEndosoForm" key="solicitud.razonSocial" /></th>
										<td colspan="2" width="35%"><midas:texto propiedadFormulario="razonSocial" /></td>
										<td colspan="3" width="60%">&nbsp;</td>
									</table>
								</div>
							</td>
						
				</tr>
				<tr>
					
							<th>
								<etiquetas:etiquetaError property="telefono" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.telefono"/></th>
							<td width="20%"><midas:texto propiedadFormulario="telefono" caracteres="20" /></td><td colspan="3" width="70%">&nbsp;</td>
					
				</tr>
				<tr >
					
							<th><etiquetas:etiquetaError property="descProducto" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitudEndoso.producto"/> </th>
							<td colspan="3" style="font-size: 9px;">
								<html:text property="descProducto" readonly="true"  styleClass="cajaTexto" 	name="solicitudEndosoForm" styleId="descProducto" 
								maxlength="50" size="50"></html:text>
							</td><td colspan="2">&nbsp;</td>
						
				</tr>
				<tr>
							
							<c:choose>
								<c:when test="${solicitudEndosoForm.bloqueoDatosAgente}">
									<th><etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
											name="solicitudEndosoForm" key="solicitud.codigoAgente"/></th>
									<td width="20%">
										<midas:texto propiedadFormulario="codigoAgente" caracteres="20" soloLectura="true" />
									</td>
									<td colspan="3" width="70%">&nbsp;</td>
								</c:when>
								<c:otherwise>
									<th>
										<etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
											name="solicitudEndosoForm" key="solicitud.codigoAgente"/></th>
									<td width="20%">
										<midas:texto propiedadFormulario="codigoAgente" caracteres="20" onblur="javascript: seleccionarAgente(this.value);" id="codigoAgente" />
									</td>
									<td colspan="3" width="70%">&nbsp;</td>
								</c:otherwise>
							</c:choose>
						
				</tr>
				<tr>
							
							<c:choose>
								<c:when test="${solicitudEndosoForm.bloqueoDatosAgente}">
									<th><etiquetas:etiquetaError property="idAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
											name="solicitudEndosoForm" key="solicitud.agenteSeguros"/></th>
									<td colspan="3">
										
										<html:select property="idAgente"  styleId="idAgente"  styleClass="cajaTexto" name="solicitudEndosoForm"> 
											<html:optionsCollection value="idTcAgente" property="agentes" name="solicitudEndosoForm" label="nombre"/>
										</html:select></td><td colspan="2">&nbsp;</td>
								</c:when>
								<c:otherwise>
									<th><etiquetas:etiquetaError property="idAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
											name="solicitudEndosoForm" key="solicitud.agenteSeguros"/></th>
									<td colspan="3">
										<html:select property="idAgente" styleId="idAgente" styleClass="cajaTexto" name="solicitudEndosoForm"  onchange="javascript: escribirCodigoAgente(this.value);"> <html:option value="">SELECCIONE...</html:option>
											<html:optionsCollection  value="idTcAgente" property="agentes" name="solicitudEndosoForm" label="nombre"/>
										</html:select></td><td colspan="2">&nbsp;</td>
								</c:otherwise>
							</c:choose>	
						
				</tr>
				<tr>
							<th colspan="1"><etiquetas:etiquetaError property="correos" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.correos"/></th>
							<td colspan="4"><midas:areatexto propiedadFormulario="correos"/></td><td colspan="1">&nbsp;</td>
				</tr>
				<tr>
						<td colspan="2"><midas:checkBox valorEstablecido="0" propiedadFormulario="claveOpcionEmision">
									<midas:mensaje clave="solicitud.claveOpcionEmision" /></midas:checkBox></td><td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="campoRequerido" colspan="2">
			 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
					</td>
							
				</tr>
				<tr>
					<td colspan="6">
						<midas:mensajeUsuario/>
					</td>			
				</tr>
			</table>
				<div id="resultadosDocumentosEndoso">
					<midas:tabla idTabla="solicitudes"
						claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalSolicitud"
						claseCss="tablaConResultados" nombreLista="documentos"
						urlAccion="/endoso/solicitud/listarDocumentos.do">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
						<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
						<midas:columna propiedad="accionesEndoso" titulo="Acciones"/>
					</midas:tabla>
				</div>
				<div class="alinearBotonALaDerecha">				
					<div id="botonAgregar">
					<midas:boton onclick="javascript: sendRequest(document.solicitudEndosoForm,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
						<midas:boton onclick="javascript: mostrarAdjuntarArchivoSolicitudEndosoWindow();" tipo="agregar"/>
						&nbsp;
						<midas:boton onclick="if(esValidaFechaVigencia()){javascript: sendRequest(document.solicitudEndosoForm,'/MidasWeb/endoso/solicitud/modificar.do', 'contenido',null)};" tipo="guardar"/>
					</div>
				</div>
			
			<div id="errores" style="display: none;"><html:errors/></div>
		</midas:formulario>
	</center>
</div>
