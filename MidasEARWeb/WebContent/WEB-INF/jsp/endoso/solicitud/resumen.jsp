<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>

<midas:formulario accion="/endoso/solicitud/mostrarResumen">
<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.solicitud"/></div>
<div class="subtituloCotizacion"> <midas:mensaje clave="midas.solicitud.prefijo"/><midas:escribe propiedad="numeroSolicitudFormateado" nombre="solicitudEndosoForm"/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaSolicitud" nombre="solicitudEndosoForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>
<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="solicitudEndoso.numPoliza"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="numPoliza" nombre="solicitudEndosoForm"/></td>
		<th><midas:mensaje clave="solicitudEndoso.vigenciaCotizacion"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="fechaInicioCotizacion" nombre="solicitudEndosoForm"/>
								&nbsp;-&nbsp;
		<midas:escribe propiedad="fechaFinCotizacion" nombre="solicitudEndosoForm"/></td>
	</tr>
	<tr>
		<th><midas:mensaje clave="solicitudEndoso.tipoEndoso"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="descTipoEndoso" nombre="solicitudEndosoForm"/></td>
		<th><midas:mensaje clave="solicitudEndoso.fechaInicioVigencia"/>:</th>
		<td class="txt_v"><midas:escribe propiedad="fechaInicioVigenciaEndoso" nombre="solicitudEndosoForm"/></td>
	</tr>
	<tr>
		<th><midas:mensaje clave="midas.ordendetrabajo.nombreSolicitante"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreSolicitante" nombre="solicitudEndosoForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.estatus"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="descripcionEstatus" nombre="solicitudEndosoForm"/></td>
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.producto"/>:</th>
		<td class="txt_v"><midas:escribe propiedad="nombreComercialProducto" nombre="solicitudEndosoForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.agente"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreAgente" nombre="solicitudEndosoForm"/></td>					
	</tr>	
</table>

<div style="clear:both"></div>

<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.cotizacion"/></div>
<div class="subtituloCotizacion"><midas:mensaje clave="midas.cotizacion.prefijo"/><midas:escribe propiedad="numeroOrdenTrabajoFormateado" nombre="solicitudEndosoForm"/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaCOT" nombre="solicitudEndosoForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>

<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="solicitud.resumen.contratante"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreContratante" nombre="solicitudEndosoForm"/></td>
		<th><midas:mensaje clave="solicitud.resumen.asegurado"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreAsegurado" nombre="solicitudEndosoForm"/></td>
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.usuario.asignacion"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreUsuarioAsignacionCOT" nombre="solicitudEndosoForm"/></td>
		<th>&nbsp;</th>
		<td class="txt_v">&nbsp;</td>					
	</tr>	
</table>

<div class="subtituloCotizacion"><midas:mensaje clave="solicitud.resumen.titulo.emision"/></div>
<div class="subtituloCotizacion"><midas:mensaje clave="configuracion.cobertura.fecha"/>: <midas:escribe propiedad="fechaEmision" nombre="solicitudEndosoForm" formato="dd'/'MM'/'yyyy"/>
</div>
<div style="clear:both"></div>

<table id="desplegar" border="0">
	<tr>
		<th><midas:mensaje clave="solicitudEndoso.numPoliza"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="numPoliza" nombre="solicitudEndosoForm"/></td>
		<th><midas:mensaje clave="mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.numeroEndose"/></th>
		<td class="txt_v"> <midas:escribe propiedad="numeroEndoso" nombre="solicitudEndosoForm"/></td>
	</tr>
	<tr class="bg_t2">
		<th><midas:mensaje clave="solicitud.resumen.usuario.asignacion"/>:</th>
		<td class="txt_v"> <midas:escribe propiedad="nombreUsuarioAsignacionEmision" nombre="solicitudEndosoForm"/></td>
		<th>&nbsp;</th>
		<td class="txt_v">&nbsp;</td>	
	</tr>	
</table>
</midas:formulario>