<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

 
<div id="detalle">
	<center>
		<midas:formulario accion="/endoso/solicitud/agregar">
			<html:hidden styleId="tipoMensaje" property="tipoMensaje"/>
    		<html:hidden styleId="mensaje" property="mensaje"/>
			<midas:oculto propiedadFormulario="permitirAutoAsignacion"/>
			<midas:oculto propiedadFormulario="bloqueoDatosAgente"/>
			<midas:oculto propiedadFormulario="numeroSolicitud"/>
			<html:hidden property="estatusPoliza" name="solicitudEndosoForm" styleId="estatusPoliza"/>
			<html:hidden property="idPoliza" name="solicitudEndosoForm" styleId="idPoliza"/>
			<html:hidden property="idProducto" name="solicitudEndosoForm" styleId="idProducto"/>
			<html:hidden property="fechaStrSolicitud" name="solicitudEndosoForm" styleId="fechaStrSolicitud" />
			<html:hidden property="cveEstatusPoliza" name="solicitudEndosoForm" styleId="cveEstatusPoliza" />
			<html:hidden property="presentaComboMotivo" name="solicitudEndosoForm" styleId="presentaComboMotivo" />
			
			<table id="desplegarDetalle" border="0">
			
				<tr>
					<td class="titulo" colspan="4" width="65%">
						Datos de la Solicitud del Endoso
					</td>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<td width="18%">
								Solicitud: <%= "SOL-" + String.format("%08d", new Object[]{session.getAttribute("idSolicitud")}) %>
							</td>
						</c:when>
						<c:otherwise><td width="18%">&nbsp;</td></c:otherwise>
					</c:choose>			
					<td width="17%">
						<jsp:useBean id="now" class="java.util.Date" />
						Fecha: <fmt:formatDate value="${now}"  pattern="dd/MM/yyyy"/>
					</td>
				</tr>
			</table>
			<table id="agregar" border="0">
			
				<tr>
				  <td colspan="4">
					  <table id="tbPoliza" style="border: none; padding: 0px;font-size: 9px;">
					   <th width="25%">
								<etiquetas:etiquetaError property="numPoliza" requerido="si" 
														normalClass="normal" errorClass="error" errorImage="/img/information.gif"
														name="solicitudEndosoForm" 
														key="solicitudEndoso.numPoliza" />
								
							</th>
							<c:choose>
								<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
									<th width="45%">
								
										<midas:texto propiedadFormulario="numPoliza" id="numPoliza" 
										nombreFormulario="solicitudEndosoForm" deshabilitado="true"
										caracteres="16"  
										longitud="16" /> 
										
									</th>
								</c:when>
								<c:otherwise>
								<th width="45%">
										<midas:texto propiedadFormulario="numPoliza" id="numPoliza" 
										nombreFormulario="solicitudEndosoForm" onfocus="new Mask('####-########-##', 'string').attach(this)" 
										caracteres="16"  
										longitud="16" /> 
										
									</th>
								
								
								</c:otherwise>
							</c:choose>	
						<c:if test="${empty solicitudEndosoForm.numeroSolicitud}">	
							<th width="15%" align="left">
							   <div class="alinearBotonALaIzquierda">
									<div id="b_buscar_poliza">
										<a href="javascript: void(0);" 	onclick="javascript:buscaPolizaAjax();">
											<midas:mensaje clave="siniestro.cabina.reportesiniestro.buscarPoliza"/>
										</a>
									</div>
								
								</div>
							</th> 
							
							<th width="15%" align="left">
							 <div class="alinearBotonALaIzquierda">
									<div id="b_buscar_poliza">
										<a href="javascript: void(0);" 	onclick="javascript: mostrarVentanaPolizas($('estatusPoliza').value);">
											<midas:mensaje clave="solicitudEndoso.verPolizas"/>
										</a>
									</div>
								
								</div>
							
							
							</th>
						</c:if>	
					    </table>
					  </td>
					</tr>
			<tr>
					 <td colspan="5" style="border: none; padding: 0px;font-size: 9px;">
					  <table border="0">
					  
					  	<th width="18%" style="font-size: 9px;" align="right">
								<midas:mensaje clave="solicitudEndoso.vigenciaCotizacion"/>
								
						</th>
						<th width="5%" align="left" style="font-size: 9px;">
								<midas:mensaje clave="solicitudEndoso.de"/>
								
						</th>
						<c:choose>
							 <c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
								<th width="15%" align="left">
										<midas:texto propiedadFormulario="fechaInicioCotizacion" id="fechaInicioCotizacion" 
										nombreFormulario="solicitudEndosoForm"  deshabilitado="true"  
										caracteres="10"  
										longitud="10" /> 
										
								</th>
							</c:when>
							<c:otherwise>
							  <th width="15%" align="left">
										<html:text property="fechaInicioCotizacion" styleId="fechaInicioCotizacion" 
										name="solicitudEndosoForm"  readonly="true" maxlength="10" size="10" styleClass="cajaTexto"/> 
										
								</th>
							
							</c:otherwise>
						</c:choose>
						<th width="5%" align="left" style="font-size: 9px;">
								<midas:mensaje clave="solicitudEndoso.a"/>
								
						</th>
						<c:choose>
							<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
								<th width="57%" align="left">
										<midas:texto propiedadFormulario="fechaFinCotizacion" id="fechaFinCotizacion" 
										nombreFormulario="solicitudEndosoForm" deshabilitado="true"  
										caracteres="10" 
										longitud="10" /> 
										
								</th>
								
							</c:when>
							<c:otherwise>
								<th width="20%" align="left">
										<html:text property="fechaFinCotizacion" styleId="fechaFinCotizacion" 
										name="solicitudEndosoForm"  readonly="true"  styleClass="cajaTexto"
										maxlength="10" 
										size="10" /> 
										
								</th>
								<th width="30%" align="left" style="font-size: 9px;">
								&nbsp;
								
								</th>
							
							</c:otherwise>
					  </c:choose>
					  </table>
					 </td>
				</tr>
				<tr>
					<td width="100%" colspan="4">
					 <table id="tbEndoso"  border="0"  style="border: none; padding: 0px;font-size: 9px;">
					 <tr>
						<th width="25%">
							<etiquetas:etiquetaError  property="tipoEndoso" requerido="si" 
									 normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									 name="solicitudEndosoForm" key="solicitudEndoso.tipoEndoso" />
								               
						</th>
						<c:choose>
							<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
								
								<th width="25%">
								<midas:comboValorFijo grupoValores="40" propiedad="tipoEndoso" styleId="tipoEndoso" 
												readonly="true" nombre="solicitudEndosoForm" styleClass="cajaTexto"/>
								</th>
							</c:when>
							<c:otherwise>
								<th width="25%">
								<midas:comboValorFijo grupoValores="40" propiedad="tipoEndoso" styleId="tipoEndoso"
												nombre="solicitudEndosoForm"  onchange="javascript:validaCambioTipoEndoso(this.value);"
												styleClass="cajaTexto"/>
								</th>
							</c:otherwise>
						</c:choose>
							
							<th width="60%">
						<div width="100%">
								<table id="datosMotivo" style="width: 100%;display: none;" border="0">
									<tr>
							<th width="35%" style="font-size: 9px;">
							Motivo de Cancelaci&oacute;n:
							</th>
							<c:choose>
								<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
									<th width="65%">
									<midas:comboValorFijo grupoValores="42" propiedad="claveMotivoEndoso" styleId="claveMotivoEndoso" 
													readonly="true" nombre="solicitudEndosoForm" styleClass="cajaTexto"/>
									</th>
								</c:when>
								<c:otherwise>
									<th width="65%">
									<midas:comboValorFijo grupoValores="42" propiedad="claveMotivoEndoso" styleId="claveMotivoEndoso"
													nombre="solicitudEndosoForm"  	styleClass="cajaTexto"/>
									</th>
								</c:otherwise>
							</c:choose>
							</tr>
							</table>
						</div>
						</th>
						</tr>
						</table>
					</td>
					</tr>
					<tr>
					
						 	<th width="38%">
								<div id="etiquetaFecha"><etiquetas:etiquetaError normalClass="normal" errorClass="error" 
									errorImage="/img/information.gif" name="solicitudEndosoForm"
									property="fechaInicioVigenciaEndoso" key="solicitudEndoso.fechaInicioVigencia" requerido="si"/> 
								<c:if test="${empty solicitudEndosoForm.numeroSolicitud}">
									<a href="javascript: void(0);" id="mostrarCalendario" onclick="javascript: mostrarCalendarioOT();">
										<image src="/MidasWeb/img/b_calendario.gif" border="0"/></a>
								</c:if>
								</div>
						   	</th>
						   	<c:choose>
							   	<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
								   
									<th width="40%">
								   				<html:text	property="fechaInicioVigenciaEndoso"  maxlength="10" size="12"
								   				 styleId="fecha" name="solicitudEndosoForm" disabled="true" styleClass="cajaTexto" 	/>
									</th>
									
								</c:when>
								<c:otherwise>
									<th width="40%">
								   				<html:text	property="fechaInicioVigenciaEndoso"  maxlength="10" size="15"
								   				 styleId="fecha" name="solicitudEndosoForm"  styleClass="cajaTexto"
												 onkeypress="return soloFecha(this, event, false);"
												 onfocus="javascript: new Mask('dd/mm/yyyy', 'date').attach(this)"/>
									</th>
								
								</c:otherwise>
							</c:choose>
					
					
					
					
					</tr>
				
					
					
				<tr>
					<th width="10%">
						<etiquetas:etiquetaError property="tipoPersona" requerido="si" normalClass="normal" errorClass="error"
							name="solicitudEndosoForm" key="solicitud.tipoPersona" errorImage="/img/information.gif" />
					</th>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<td width="30%" colspan="2">
								<html:radio property="tipoPersona" value="1" disabled="true" >F&iacute;sica</html:radio>
								<html:radio property="tipoPersona" value="2" disabled="true" >Moral</html:radio>
							</td>
						</c:when>
						<c:otherwise>
							<td width="30%" colspan="2">
								<midas:radio valorEstablecido="1" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">F&iacute;sica</midas:radio>
								<midas:radio valorEstablecido="2" propiedadFormulario="tipoPersona" onClick="javascript: mostrarTipoPersonaSolicitud();">Moral</midas:radio>
							</td>
						</c:otherwise>
					</c:choose>
					<td width="6%" colspan="3"></td>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<td colspan="6">
								<logic:equal value="1" property="tipoPersona" name="solicitudEndosoForm" >
									<div id="nombreDiv">
										<table id="agregar" style="border: none; padding: 0px;">
											<th width="10%">
												<etiquetas:etiquetaError property="nombres" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
													name="solicitudEndosoForm" key="solicitud.nombres" /> </th>
											<td width="20%"><midas:texto propiedadFormulario="nombres" deshabilitado="true" /></td>
											<th width="15%"><etiquetas:etiquetaError property="apellidoPaterno" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
													name="solicitudEndosoForm" key="solicitud.apellidoPaterno" /> </th>
											<td width="20%"><midas:texto propiedadFormulario="apellidoPaterno" deshabilitado="true"/></td>
											<th width="15%"><etiquetas:etiquetaError property="apellidoMaterno" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
													name="solicitudEndosoForm" key="solicitud.apellidoMaterno" /> </th>
											<td width="30%"><midas:texto propiedadFormulario="apellidoMaterno" deshabilitado="true"/></td>
										</table>
									</div>
								</logic:equal>
								<logic:equal value="2" property="tipoPersona" name="solicitudEndosoForm" >
									<div id="razonSocialDiv" >
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="15%"><etiquetas:etiquetaError property="razonSocial" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="solicitudEndosoForm" key="solicitud.razonSocial" /></th>
										<td colspan="2" width="35%"><midas:texto propiedadFormulario="razonSocial" deshabilitado="true" /></td>
									</table>
								</div>
								</logic:equal>
							</td>
						</c:when>
						<c:otherwise>
							<td colspan="6">
								<div id="nombreDiv">
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="10%">
											<etiquetas:etiquetaError property="nombres" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudEndosoForm" key="solicitud.nombres" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="nombres" /></td>
										<th width="15%"><etiquetas:etiquetaError property="apellidoPaterno" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudEndosoForm" key="solicitud.apellidoPaterno" /> </th>
										<td width="20%"><midas:texto propiedadFormulario="apellidoPaterno"/></td>
										<th width="15%"><etiquetas:etiquetaError property="apellidoMaterno" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
												name="solicitudEndosoForm" key="solicitud.apellidoMaterno" /> </th>
										<td width="30%"><midas:texto propiedadFormulario="apellidoMaterno"/></td>
									</table>
								</div>
								<div id="razonSocialDiv" style="display: none;">
									<table id="agregar" style="border: none; padding: 0px;">
										<th width="15%"><etiquetas:etiquetaError property="razonSocial" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif" 
												name="solicitudEndosoForm" key="solicitud.razonSocial" /></th>
										<td colspan="2" width="35%"><midas:texto propiedadFormulario="razonSocial" /></td>
									</table>
								</div>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<th><etiquetas:etiquetaError property="telefono" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.telefono"/></th>
							<td width="20%"><midas:texto propiedadFormulario="telefono" caracteres="20" deshabilitado="true" /></td>
						</c:when>
						<c:otherwise>
							<th>
								<etiquetas:etiquetaError property="telefono" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.telefono"/></th>
							<td width="20%"><midas:texto propiedadFormulario="telefono" caracteres="20" /></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr >
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<th><etiquetas:etiquetaError property="idProducto" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.producto"/> </th>
							<td colspan="3" style="font-size: 9px;">
									<html:text property="descProducto" disabled="true" name="solicitudEndosoForm" styleId="descProducto"  styleClass="cajaTexto"
								maxlength="50" size="50"></html:text>
							</td>
						</c:when>
						<c:otherwise>
							<th><etiquetas:etiquetaError property="descProducto" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitudEndoso.producto"/> </th>
							<td colspan="3" style="font-size: 9px;">
								<html:text property="descProducto" readonly="true"  styleClass="cajaTexto" 	name="solicitudEndosoForm" styleId="descProducto" 
								maxlength="50" size="50"></html:text>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud || solicitudEndosoForm.bloqueoDatosAgente}">
							<th><etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.codigoAgente"/></th>
							<td width="20%">
								<midas:texto propiedadFormulario="codigoAgente" caracteres="20" soloLectura="true" />
							</td>
							
						</c:when>
						<c:otherwise>
							<th>
								<etiquetas:etiquetaError property="codigoAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.codigoAgente"/></th>
							<td width="20%">
								<midas:texto propiedadFormulario="codigoAgente" caracteres="20" onblur="javascript: seleccionarAgente(this.value);" id="codigoAgente" />
							</td>
							
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud || solicitudEndosoForm.bloqueoDatosAgente}">
							<th><etiquetas:etiquetaError property="idAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.agenteSeguros"/></th>
							<td colspan="2">
								
								<html:select property="idAgente"  styleId="idAgente"  styleClass="cajaTexto" name="solicitudEndosoForm"> 
									<html:optionsCollection value="idTcAgente" property="agentes" name="solicitudEndosoForm" label="nombre"/>
								</html:select></td>
						</c:when>
						<c:otherwise>
							<th><etiquetas:etiquetaError property="idAgente" requerido="si" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.agenteSeguros"/></th>
							<td colspan="3">
								<html:select property="idAgente" styleId="idAgente" styleClass="cajaTexto" name="solicitudEndosoForm"  onchange="javascript: escribirCodigoAgente(this.value);"> <html:option value="">SELECCIONE...</html:option>
									<html:optionsCollection  value="idTcAgente" property="agentes" name="solicitudEndosoForm" label="nombre"/>
								</html:select></td>
						</c:otherwise>
					</c:choose>
				</tr>
				
				
				
				
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<th colspan="1"><etiquetas:etiquetaError property="correos" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.correos"/></th>
							<td colspan="4"><midas:areatexto renglones="4" propiedadFormulario="correos" deshabilitado="true" /></td>
						</c:when>
						<c:otherwise>
							<th colspan="1"><etiquetas:etiquetaError property="correos" requerido="no" normalClass="normal" errorClass="error" errorImage="/img/information.gif"
									name="solicitudEndosoForm" key="solicitud.correos"/></th>
							<td colspan="4"><midas:areatexto renglones="4" propiedadFormulario="correos"/></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${!empty solicitudEndosoForm.numeroSolicitud}">
							<td><html:checkbox property="claveOpcionEmision" value="0" disabled="true">
								<midas:mensaje clave="solicitud.claveOpcionEmision" /></html:checkbox> </td>
						</c:when>
						<c:otherwise>
							<td><midas:checkBox valorEstablecido="0" propiedadFormulario="claveOpcionEmision">
									<midas:mensaje clave="solicitud.claveOpcionEmision" /></midas:checkBox></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td class="campoRequerido">
			 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
					</td>
					<td width="10%"/>
					<td class="guardar" width="60%">
						<c:if test="${empty solicitudEndosoForm.numeroSolicitud}">
							<div class="alinearBotonALaDerecha">
								<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
								<midas:boton onclick="validaAgregarSolicitudEndoso();" tipo="continuar"/>
									
							</div>
						</c:if>
					</td> 		
				</tr>
				<tr>
					<td colspan="6">
						<midas:mensajeUsuario/>
					</td>			
				</tr>
			</table>
			
			
			<c:if test="${!empty solicitudEndosoForm.numeroSolicitud}">
				<div id="resultadosDocumentosEndoso">
					<midas:tabla idTabla="solicitudes"
						claseDecoradora="mx.com.afirme.midas.decoradores.DocumentoDigitalSolicitud"
						claseCss="tablaConResultados" nombreLista="documentos"
						urlAccion="/endoso/solicitud/listarDocumentos.do">
						<midas:columna propiedad="archivo" />
						<midas:columna propiedad="anexadoPor" titulo="Anexado por" />
						<midas:columna propiedad="fechaCreacion" titulo="Fecha" />
						<midas:columna propiedad="accionesEndoso" titulo="Acciones"/>
					</midas:tabla>
				</div>
				<div class="alinearBotonALaDerecha">				
					<div id="botonAgregar">
						<midas:boton onclick="javascript: sendRequest(document.solicitudEndosoForm,'/MidasWeb/solicitud/listar.do', 'contenido',null);" tipo="regresar"/>
						<midas:boton onclick="javascript: mostrarAdjuntarArchivoSolicitudEndosoWindow();" tipo="agregar"/>
					</div>
				</div>
			</c:if>
			<div id="errores" style="display: none;"><html:errors/></div>
		</midas:formulario>
	</center>
</div>
