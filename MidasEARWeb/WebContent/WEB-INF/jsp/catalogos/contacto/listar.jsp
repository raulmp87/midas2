<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page buffer = "16kb" %>

<midas:formulario  accion="/catalogos/contacto/listar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	<table width="95%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.contacto.listaDeContactos"/>
				<hr>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.contacto.idtcreaseguradorcorredor"/></th>
			<td>
				<midas:comboCatalogo propiedad="idtcReaseguradorCorredor" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
			</td>
			<th><midas:mensaje clave="catalogos.contacto.nombre"/></th>
			<td><midas:texto propiedadFormulario="nombre" onkeypress="return stopRKey(event)"/></td>
			<th><midas:mensaje clave="catalogos.contacto.telefono"/></th>
			<td><midas:texto propiedadFormulario="telefono" onkeypress="return stopRKey(event)"/></td>
		</tr> 
		
		<tr>
			<td colspan="5">&nbsp;</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.contactoForm,'/MidasWeb/catalogos/contacto/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.contacto.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
	</table>
	<div id="mensajeResultadoOperacion" class="notificacionResultadoOperacion" style="display:none;margin-top:15px;margin-bottom:15px;width:95%"></div>
	<br clear="all" />
	<div id="resultados" style="width:95%">
		<midas:tabla idTabla="contactoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Contacto"
			claseCss="tablaConResultados" nombreLista="contactos"
			urlAccion="/catalogos/contacto/listar.do">
			<midas:columna propiedad="reaseguradorCorredor.nombre" titulo="Reasegurador/Corredor" />
			<midas:columna propiedad="nombre" titulo="Nombre" />
			<midas:columna propiedad="telefono" titulo="Tel&eacute;fono"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		
				
		<div class="alinearBotonALaDerecha">
			<div id="b_agregar">
				<a href="javascript: void(0);"
					onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/contacto/mostrarAgregar.do', 'contenido',null);">
					<midas:mensaje clave="midas.accion.agregar"/>
				</a>
			</div>
		</div>
	</div>
	<midas:mensajeUsuario/>
</midas:formulario>
