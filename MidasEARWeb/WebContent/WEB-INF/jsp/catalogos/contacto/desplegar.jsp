<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/contacto/listar">
    	<div id="centrarDesplegar">
		  	<table id="agregar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.contacto.accion.detalle" /> <midas:escribe nombre="contactoForm" propiedad="nombre"/>
						<hr>
					</td>
				</tr>
				
	 			<tr>
				<td width="25%">
					<etiquetas:etiquetaError property="idtcReaseguradorCorredor" requerido="si"
						key="catalogos.contacto.idtcreaseguradorcorredor" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td width="25%">
					<midas:comboCatalogo propiedad="idtcReaseguradorCorredor" readonly="true" styleId="idtcReaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
				</td>
				<td width="25%">
					<etiquetas:etiquetaError property="nombre" requerido="si"
						key="catalogos.contacto.nombre" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td width="25%">
					<midas:texto propiedadFormulario="nombre"
						onkeypress="return soloAlfanumericos(this, event, false)"  deshabilitado="true"/>
				</td>
			</tr>
			
			<tr>
				<td width="25%">
					<etiquetas:etiquetaError property="telefono" requerido="si"
						key="catalogos.contacto.telefono" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif" />
				</td>
				<td width="25%">
					<midas:texto propiedadFormulario="telefono" onkeypress="return soloNumeros(this, event, false)" deshabilitado="true"/>
				</td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td class="guardar" colspan="4" >
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);"
								onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/contacto/listar.do', 'contenido',null);">
								<midas:mensaje clave="midas.accion.regresar"/>
							</a>
						</div>
					</div>
				</td> 
			</tr>	  				
		</table>
	  	</div>
    </midas:formulario>
