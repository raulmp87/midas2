<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/productoscnsf/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<midas:oculto propiedadFormulario="id"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.productoscnsf.claveProdServi"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="claveProdServ" nombre="productoCnsfForm"/></td>
			
			<th><midas:mensaje clave="catalogos.productoscnsf.descripcionProd"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionProd" nombre="productoCnsfForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.productoscnsf.ivaTrasladado"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="ivaTrasladado" nombre="productoCnsfForm"/></td>
			
			<th><midas:mensaje clave="catalogos.productoscnsf.iepsTrasladadoProd"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="iepsTrasladado" nombre="productoCnsfForm"/></td>
		</tr>
		<tr>
		    <th><midas:mensaje clave="catalogos.productoscnsf.complementoProd"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="complementoProd" nombre="productoCnsfForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.productoCnsfForm,'/MidasWeb/catalogos/productoscnsf/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('¿Realmente deseas borrar el registro seleccionado?',document.productoCnsfForm,'/MidasWeb/catalogos/productoscnsf/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>