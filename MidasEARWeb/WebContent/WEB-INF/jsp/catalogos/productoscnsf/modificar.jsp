<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/productoscnsf/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<midas:oculto propiedadFormulario="id"/>
			</td>
		</tr>
		<tr>
			<td> <etiquetas:etiquetaError requerido="si" property="claveProdServ" name="productoCnsfForm" key="catalogos.productoscnsf.claveProdServi" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td> <midas:texto  onkeypress="return soloNumeros(this, event, false)" caracteres="25" 
				propiedadFormulario="claveProdServ"/></td>
			<td> <etiquetas:etiquetaError requerido="si" property="descripcionProd" name="productoCnsfForm" key="catalogos.productoscnsf.descripcionProd" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td colspan="2"> <midas:texto caracteres="250" 
				propiedadFormulario="descripcionProd"/></td>
		</tr>
		<tr>
			<td> <etiquetas:etiquetaError requerido="si" property="ivaTrasladado" name="productoCnsfForm" key="catalogos.productoscnsf.ivaTrasladado" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td> <midas:texto  onkeypress="return soloNumeros(this, event, false)" caracteres="60" 
				propiedadFormulario="ivaTrasladado"/></td>
			<td> <etiquetas:etiquetaError requerido="si" property="iepsTrasladado" name="productoCnsfForm" key="catalogos.productoscnsf.iepsTrasladadoProd" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td colspan="2"> <midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="60" 
				propiedadFormulario="iepsTrasladado"/></td>
		</tr> 
		<tr>
			<td> <etiquetas:etiquetaError requerido="si" property="complementoProd" name="productoCnsfForm" key="catalogos.productoscnsf.complementoProd" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td> <midas:texto caracteres="150" 
				propiedadFormulario="complementoProd"/></td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/productoscnsf/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.productoCnsfForm,'/MidasWeb/catalogos/productoscnsf/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>