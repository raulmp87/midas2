<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/productoscnsf/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.productoscnsf.claveProdServi"/>:</th>
			<td><midas:texto propiedadFormulario="claveProdServ" caracteres="22" onkeypress="return soloNumeros(this, event, false)"/> </td>
			<th><midas:mensaje clave="catalogos.productoscnsf.descripcionProd"/>:</th>
			<td colspan="2"><midas:texto propiedadFormulario="descripcionProd" caracteres="250" /> </td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.productoCnsfForm,'/MidasWeb/catalogos/productoscnsf/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="productosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ProductoCnsf"
			claseCss="tablaConResultados" nombreLista="productos"
			urlAccion="/catalogos/productoscnsf/listar.do">
			<midas:columna propiedad="claveProdServ" titulo="Clave"/>
			<midas:columna propiedad="descripcionProd" titulo="Descripción"/>
			<midas:columna propiedad="ivaTrasladado" titulo="IVA"/>
			<midas:columna propiedad="iepsTrasladado" titulo="IEPS"/>
			<midas:columna propiedad="complementoProd" titulo="Complemento"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/productoscnsf/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>