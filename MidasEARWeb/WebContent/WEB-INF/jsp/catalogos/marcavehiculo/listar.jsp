<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario  accion="/catalogos/marcavehiculo/listar">
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Marca Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.marcavehiculo.clavetipobien"/>:</th>
			<td class="fondoCajaTexto">
				<midas:comboCatalogo propiedad="tipoBienAutosForm.claveTipoBien" 
					styleId="" size="" styleClass="cajaTexto" 
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.marcavehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto">
				<midas:texto caracteres="5" propiedadFormulario="codigoMarcaVehiculo" 
					onkeypress="return soloAlfanumericos(this, event, false)"/>
			</td>
			<th><midas:mensaje clave="catalogos.marcavehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto">
				<html:text property="descripcionMarcaVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.marcaVehiculoForm,'/MidasWeb/catalogos/marcavehiculo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listMarcaVehiculo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorMarcaVehiculo"
			claseCss="tablaConResultados" nombreLista="listMarcaVehiculo"
			urlAccion="/catalogos/marcavehiculo/listarFiltrado.do">
			<midas:columna propiedad="tipoBienAutosDTO.descripcionTipoBien" titulo="Tipo Bien" />			
			<midas:columna propiedad="codigoMarcaVehiculo" titulo="C&oacute;digo" />
			<midas:columna propiedad="descripcionMarcaVehiculo" titulo="Descripci&oacute;n" />
			<midas:columna propiedad="acciones" titulo="Acciones" estilo="acciones" />
		</midas:tabla>
	</div>

	<div class="alinearBotonALaDerecha" style="margin-right: 20px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/marcavehiculo/mostrarAgregar.do', 'contenido');">
				<midas:mensaje clave="midas.accion.agregar"/>
			</a>
		</div>
	</div>

</midas:formulario>
