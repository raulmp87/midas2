<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/marcavehiculo/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" /> Marca de Veh&iacute;culo
				<midas:oculto propiedadFormulario="idTcMarcaVehiculo"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.marcavehiculo.clavetipobien"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="tipoBienAutosForm.descripcionTipoBien"  nombre="marcaVehiculoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.marcavehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoMarcaVehiculo"  nombre="marcaVehiculoForm"/></td>
			<th><midas:mensaje clave="catalogos.marcavehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionMarcaVehiculo" nombre="marcaVehiculoForm"/></td>
		</tr>
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_borrar" style="margin-right: 4px">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.marcaVehiculoForm,'/MidasWeb/catalogos/marcavehiculo/borrar.do', 'contenido','validaBorrarM1()');"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.marcaVehiculoForm,'/MidasWeb/catalogos/marcavehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>