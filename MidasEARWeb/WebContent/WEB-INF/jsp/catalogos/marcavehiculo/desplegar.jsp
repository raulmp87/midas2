<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<style type="text/css">  
table#gridSubMarca {
	font-size: 9px;
	margin: auto;
	padding: 10px;
	white-space: nowrap;
	width: 98%;
	margin: 10px;
	border: 1px solid #73D54A;		
}
</style>
<midas:formulario accion="/catalogos/marcavehiculo/listar">
	<s:hidden id="isEditable" value="false" />	
	
	<midas:oculto propiedadFormulario="idTcMarcaVehiculo"/>
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" /> Marca Veh&iacute;culo 
				
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.marcavehiculo.clavetipobien"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="tipoBienAutosForm.descripcionTipoBien"  nombre="marcaVehiculoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.marcavehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoMarcaVehiculo"  nombre="marcaVehiculoForm"/></td>
			<th><midas:mensaje clave="catalogos.marcavehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionMarcaVehiculo" nombre="marcaVehiculoForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/marcavehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<table  id="gridSubMarca" border="0">
				<tr>
					<td class="titulo" colspan="6">Tipo (SubMarcas) </td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="subMarcaGrid" width="430px" height="250px" style="background-color:white;overflow:hidden"></div>
					</td>
					
					<td colspan="4" style="width: 55%" >&nbsp </td>
				</tr>
							
	</table>
</midas:formulario>