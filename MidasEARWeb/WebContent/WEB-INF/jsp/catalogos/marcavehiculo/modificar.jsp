<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="m" uri="/midas-tags" %>
<link href="<s:url value="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<s:url value='/dwr/engine.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/componenteService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/dwr/interface/listadoService.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery-1.4.3.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/jQuery/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/ajaxScriptWindow.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/midas2/dwr/listado.js'/>"></script>            
<script type="text/javascript" src="<s:url value='/js/midas2/util.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxdataprocessor.js'/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<s:url value='/js/mask.js'/>"></script>
<script type="text/javascript" src='<s:url value="/js/ajaxScript.js"/>' ></script>
<script type="text/javascript" src='<s:url value="/js/validaciones.js"/>' ></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="<s:url value='/js/jQValidator.js'/>" charset="ISO-8859-1"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxcommon.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxwindows.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgrid_group.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/dhtmlxgridcell.js'/>"></script>

<style type="text/css">  
table#gridSubMarca {
	font-size: 9px;
	margin: auto;
	padding: 10px;
	white-space: nowrap;
	width: 98%;
	margin: 10px;
	border: 1px solid #73D54A;		
}
</style>

 <midas:formulario accion="/catalogos/marcavehiculo/modificar">
 <s:hidden id="isEditable" value="true" />
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" /> Marca Veh&iacute;culo
				<midas:oculto propiedadFormulario="idTcMarcaVehiculo" nombreFormulario="marcaVehiculoForm" />
				
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="tipoBienAutosForm.claveTipoBien" name="marcaVehiculoForm"
					key="catalogos.marcavehiculo.clavetipobien" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:comboCatalogo propiedad="tipoBienAutosForm.claveTipoBien" 
					styleId="" size="" 
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					styleClass="cajaTexto w200" />
			</td>
			<td colspan="2" style="width:50%">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="codigoMarcaVehiculo" name="marcaVehiculoForm"
					key="catalogos.marcavehiculo.codigo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text property="codigoMarcaVehiculo" maxlength="5"
					onkeypress="return soloAlfanumericos(this, event, false)"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionMarcaVehiculo" name="marcaVehiculoForm"
					key="catalogos.marcavehiculo.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text property="descripcionMarcaVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.marcaVehiculoForm,'/MidasWeb/catalogos/marcavehiculo/modificar.do', 'contenido','validaGuardarModificarM1();');">
							<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/marcavehiculo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	<html:hidden property="mensaje" name="marcaVehiculoForm" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" name="marcaVehiculoForm" styleId="tipoMensaje"/>	
	</table>
	
	<table  id="gridSubMarca" border="0">
				<tr>
					<td class="titulo" colspan="6">Tipo (SubMarcas) </td>
				</tr>
				<tr>
					<td colspan="4">
						<div id="subMarcaGrid" width="430px" height="250px" style="background-color:white;overflow:hidden"></div>
					</td>
					<td colspan="2">
						<div id="b_agregar">
							<a href="javascript: void(0);" onclick="javascript: agregarColumnaTablaActualizaGrid();">Agregar</a>
						</div>
						<br/><br/>
						<div id="b_borrar">
							<a href="javascript: void(0);" onclick="javascript: eliminarSubmarca(); ">Eliminar</a>
						</div>
					</td>
					<td colspan="4" style="width: 55%" >&nbsp </td>
				</tr>
				<tr>
					<td class="campoRequerido" colspan="4"><midas:mensaje clave="configuracion.eliminar.seleccionado.mensaje"/></td>
				</tr>			
	</table>
</midas:formulario>	
<script>
		obtenerGridSubMarcas();
</script>





	





