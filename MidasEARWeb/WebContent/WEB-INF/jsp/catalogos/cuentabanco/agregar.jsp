<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/cuentabanco/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.cuentabanco.tituloAgregar"/>
				<hr>
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="nombreBanco" requerido="si"
					key="catalogos.cuentabanco.filtro.nombrebanco" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td	style="width:25%">
				<midas:texto  propiedadFormulario="nombreBanco" onkeypress="return soloAlfanumericosException(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="numeroCuenta" requerido="si"
					key="catalogos.cuentabanco.filtro.numerocuenta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td style="width:25%">
				<midas:texto propiedadFormulario="numeroCuenta"  onkeypress="return soloAlfanumericosException(this, event, false)" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="numeroAba" requerido="no"
					key="catalogos.cuentabanco.filtro.numeroaba" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="numeroAba" onkeypress="return soloAlfanumericosException(this, event, false)"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="numeroSwift" requerido="no"
					key="catalogos.cuentabanco.filtro.numeroswift" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="numeroSwift" onkeypress="return soloAlfanumericosException(this, event, false)" />
			</td>
		</tr>
		
		<tr>

			<td>
				<etiquetas:etiquetaError property="beneficiarioCuenta" requerido="si"
					key="catalogos.cuentabanco.filtro.beneficiariocuenta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="beneficiarioCuenta" onkeypress="return soloAlfanumericosException(this, event, false)"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="numeroClabe" requerido="si"
					key="catalogos.cuentabanco.filtro.numeroclabe" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="numeroClabe" onkeypress="return soloAlfanumericosException(this, event, false)" />
			</td>
		</tr>

		<tr>
			<td>
				<etiquetas:etiquetaError property="reaseguradorCorredor" requerido="si"
					key="catalogos.cuentabanco.filtro.aseguradorcorredor" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
			<midas:comboCatalogo propiedad="reaseguradorCorredor" styleId="reaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
			</td>
			<td>
				<etiquetas:etiquetaError property="pais" requerido="si"
					key="catalogos.cuentabanco.filtro.pais" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:pais styleId="pais" size="1" propiedad="pais" styleClass="cajaTexto"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="ffc" requerido="no"
					key="catalogos.cuentabanco.filtro.ffc" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="ffc" onkeypress="return soloAlfanumericosException(this, event, false)" />
			</td>			
			<td>
				<etiquetas:etiquetaError property="cuentaFfc" requerido="no"
					key="catalogos.cuentabanco.filtro.cuentaffc" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="cuentaFfc"  />
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="direccionBanco" requerido="si"
					key="catalogos.cuentabanco.filtro.direccionbanco" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="direccionBanco" onkeypress="return soloAlfanumericosException(this, event, false)" />
			</td>
			
		</tr>

		<tr>
			<td>
				<etiquetas:etiquetaError property="idTcMoneda" requerido="no"
					key="catalogos.cuentabanco.filtro.tipomoneda" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />					
			</td>
			<td>
				<midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="cuentabancoForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" />				
			</td>
		</tr>
		<tr>
			<td class="guardar" colspan="3">
			</td> 
			<td style="align:right">
				<div id="botonAgregar">
					<div class="alinearBotonALaDerecha"> 
					<div id="b_regresar">
						<a href="javascript: void(0);"
							onclick="javascript:sendRequest(null,'/MidasWeb/catalogos/cuentabanco/listar.do','contenido',null)">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.cuentabancoForm,'/MidasWeb/catalogos/cuentabanco/agregar.do', 'contenido','notificacionResultadoOperacion()');">
							<midas:mensaje clave="midas.cuentabanco.accion.guardar"/>
						</a>
						</div>
					</div>	
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>

