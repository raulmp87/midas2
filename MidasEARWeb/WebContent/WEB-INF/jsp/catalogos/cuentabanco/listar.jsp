<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page buffer = "16kb" %>

<midas:formulario  accion="/catalogos/cuentabanco/listar">
    <html:hidden styleId="tipoMensaje" property="tipoMensaje"/>
    <html:hidden styleId="mensaje" property="mensaje"/>
 	<table id="filtros"  style="width:95%;overflow:scroll;table-layout:auto;">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.cuentabanco.tituloListar"/>
			</td>
		</tr>
		
		<tr>
					<th><midas:mensaje clave="catalogos.cuentabanco.filtro.aseguradorcorredor"/></th>
			<td>
			<midas:comboCatalogo propiedad="reaseguradorCorredor" styleId="reaseguradorCorredor"  size="1" styleClass="cajaTexto" nombreCatalogo="treaseguradorcorredor" idCatalogo="idtcreaseguradorcorredor" descripcionCatalogo="nombre" />
			</td>
			<th><midas:mensaje clave="catalogos.cuentabanco.filtro.pais"/></th>
			<td><midas:pais styleId="pais" size="1" propiedad="pais" styleClass="cajaTexto" /></td>
		</tr>
	
		<tr>
			<th width="10%"><midas:mensaje clave="catalogos.cuentabanco.filtro.nombrebanco"/></th>
			<td width="16%"><midas:texto  propiedadFormulario="nombreBanco"/></td>
			<th width="15%"><midas:mensaje clave="catalogos.cuentabanco.filtro.numerocuenta"/></th>
			<td width="25%"><midas:texto propiedadFormulario="numeroCuenta"/></td>
			<td colspan="2"></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.cuentabanco.filtro.numeroclabe"/></th>
			<td><midas:texto  propiedadFormulario="numeroClabe" onkeypress="return soloNumeros(this, event, false)"/></td>
			<th><midas:mensaje clave="catalogos.cuentabanco.filtro.tipomoneda"/></th>
			<td><midas:comboCatalogo propiedad="idTcMoneda" size="1" styleId="idTcMoneda" nombre="cuentabancoForm" styleClass="cajaTexto"
				nombreCatalogo="vnmoneda" idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" /></td>
		</tr>
		
		<tr>
			<td colspan="3"></td>
			<td>
				<div  class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.cuentabancoForm,'/MidasWeb/catalogos/cuentabanco/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.cuentabanco.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>  
		</tr>
	</table>
	<div id="mensajeResultadoOperacion" class="notificacionResultadoOperacion" style="display:none;margin-top:15px;margin-bottom:15px;width:98%"></div>
    <br clear="all" />
 	<div id="resultados" style="overflow:auto;width:95%">
		<midas:tabla idTabla="cuentabancoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.CuentaBanco"
			claseCss="tablaConResultados" nombreLista="cuentasbancos"
			urlAccion="/catalogos/cuentabanco/listarFiltrado.do">
	
			<midas:columna propiedad="reaseguradorCorredor.nombre" titulo="Nombre Reasegurador/Corredor" />
			<midas:columna propiedad="pais" titulo="Pa&iacute;s" />
			<midas:columna propiedad="nombrebanco" titulo="Nombre del Banco" />
			<midas:columna propiedad="numerocuenta" titulo="Numero de Cuenta" />
			<midas:columna propiedad="numeroclabe" titulo="N&uacute;mero CLABE" />
			<midas:columna propiedad="ffc" titulo="Moneda" />
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
	</div>
	<br clear="all"/>
		<div style="width:95%">
			<div id="botonAgregar">
				<div class="alinearBotonALaDerecha"> 
					<div id="b_agregar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do', 'contenido',null);">
							<midas:mensaje clave="midas.cuentabanco.accion.agregar"/>
						</a>
					</div>	
				</div>	
			</div>	
		</div>
</midas:formulario>

