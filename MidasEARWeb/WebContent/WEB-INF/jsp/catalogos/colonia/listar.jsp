<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario accion="/catalogos/colonia/listarColonias">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="coloniaForm" property="totalRegistros"/>
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar" />
				Colonias
			</td>
		</tr>
	</table>

	<table width="98%" id="filtros">
		<tr>
			<th>
				Colonia:
			</th>
			<td>
				<midas:texto propiedadFormulario="descripcionColonia"
					nombreFormulario="coloniaForm" />
			</td>

			<th>
				Codigo Postal:
			</th>
			<td>
				<midas:texto propiedadFormulario="codigoPostal"
					nombreFormulario="coloniaForm"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>
			

		</tr>

		<tr>
			<td colspan="5"></td>
			<th>
				<midas:boton
					onclick="javascript: sendRequest(document.coloniaForm, '/MidasWeb/catalogos/colonia/listarFiltrado.do', 'contenido', null);"
					tipo="buscar" />
			</th>
		</tr>
	</table>
	<br />
	<div id="resultados">
		<midas:tabla idTabla="colonias"
			claseDecoradora="mx.com.afirme.midas.decoradores.Colonia"
			claseCss="tablaConResultados" nombreLista="listaColonias"
			urlAccion="/MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="colonyName" titulo="Descripcion Colonia"	maxCaracteres="20" />
			<midas:columna propiedad="zipCode" titulo="Codigo Postal" />
			<midas:columna propiedad="nombreMunicipio" titulo="Ciudad" />
			<midas:columna propiedad="zipCodeUserId" titulo="Codigo de Colonia" />
			<midas:columna propiedad="acciones" titulo="Acciones" />

		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/colonia/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
				
			</div>
		</div>

	</div>

	</div>
</midas:formulario>
