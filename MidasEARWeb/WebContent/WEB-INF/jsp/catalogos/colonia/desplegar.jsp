<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<midas:formulario accion="/catalogos/colonia/listarColonias">
    	
    		<midas:oculto propiedadFormulario="idColonia" nombreFormulario="coloniaForm"/>
    		<midas:oculto propiedadFormulario="zipCodeUserId" nombreFormulario="coloniaForm"/>
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="coloniaForm" propiedad="descripcionColonia"/>'
					</td>
				</tr>
	 			<tr>
					<th><midas:mensaje clave="catalogos.colonia.codigopostal" />:</th>
					<td class="fondoCajaTexto"><midas:texto   propiedadFormulario="codigoPostal" nombreFormulario="coloniaForm" deshabilitado="true"/></td>
													
				   <th><midas:mensaje clave="catalogos.colonia.descripcioncolonia" />:</th>
					<td class="fondoCajaTexto"><midas:texto   propiedadFormulario="descripcionColonia" nombreFormulario="coloniaForm" deshabilitado="true"/></td>
																				
				</tr>
				<tr>
				<th><midas:mensaje clave="catalogos.colonia.idzonahidro"/>:</th>
			        <td><midas:comboCatalogo propiedad="idZonaHidro" styleId="idZonaHidro"  
			        size="1" styleClass="cajaTexto" nombreCatalogo="tczonahidro" 
			        idCatalogo="idTcZonaHidro" descripcionCatalogo="descripcionZonaHidro" readonly="true"/></td>	
			        
				<th><midas:mensaje clave="catalogos.colonia.idzonasismo"/>:</th>
				<td><midas:comboCatalogo propiedad="idZonaSismo" styleId="idZonaSismo"  size="1" 
				styleClass="cajaTexto" nombreCatalogo="tczonasismo" idCatalogo="idZonaSismo" 
				descripcionCatalogo="descripcionZonaSismo" readonly="true"/></td>	
				
				
				</tr>
				<tr>
					<th>
						<midas:mensaje clave="catalogos.colonia.iva" />:
					</th>
					<td>
						<midas:texto propiedadFormulario="valorIVA" id="valorIVA" nombreFormulario="coloniaForm" deshabilitado="true" />
					</td>
					<td colspan="2">
					</td>
				</tr>
				<tr>  
				<td colspan="3"/>
					<td class="guardar">
						<div class="alinearBotonALaDerecha">				
							<div id="botonAgregar">
								<midas:boton onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/colonia/listarColonias.do', 'contenido',null);" tipo="regresar"/>
							</div>
					  </div>		
					</td>
				</tr>   	  				
		  	</table>
	  	</div>
</midas:formulario>