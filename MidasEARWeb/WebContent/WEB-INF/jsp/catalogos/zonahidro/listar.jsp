<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/zonahidro/listar">
	<midas:oculto nombreFormulario="zonaHidroForm" propiedadFormulario="idTcZonaHidro"/>
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonahidro.codigoZonaHidro"/>:</th>
			<td><midas:texto propiedadFormulario="codigoZonaHidro" onkeypress="return soloNumeros(this, event, false)" /></td>			
			<th><midas:mensaje clave="catalogos.zonahidro.descripcionZonaHidro"/>:</th>
			<td><midas:texto  propiedadFormulario="descripcionZonaHidro" caracteres="200"/></td>			
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.zonaHidroForm,'/MidasWeb/catalogos/zonahidro/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="zonaHidroTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ZonaHidro"
			claseCss="tablaConResultados" nombreLista="zonaHidros"
			urlAccion="/catalogos/zonahidro/listar.do">
			<midas:columna propiedad="codigoZonaHidro" titulo="C&oacute;digo de Zona Hidro"/>
			<midas:columna propiedad="descripcionZonaHidro" titulo="Descripci&oacute;n"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />			
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonahidro/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>