<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/zonahidro/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />			
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="codigoZonaHidro" requerido="si"
					key="catalogos.zonahidro.codigoZonaHidro" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="codigoZonaHidro" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="descripcionZonaHidro" requerido="si"
					key="catalogos.zonahidro.descripcionZonaHidro" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionZonaHidro"
					caracteres="200" />
			</td>
		</tr>	
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="valorDefaultCoaseguro" requerido="si"
					key="catalogos.zonahidro.valorDefaultCoaseguro" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultCoaseguro" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="valorDefaultCoaseguroBMCE" requerido="si"
					key="catalogos.zonahidro.valorDefaultCoaseguroBMCE" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultCoaseguroBMCE" onkeypress="return soloNumeros(this, event, false)" />
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="valorDefaultDeducible" requerido="si"
					key="catalogos.zonahidro.valorDefaultDeducible" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultDeducible" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="valorDefaultDeducibleBMCE" requerido="si"
					key="catalogos.zonahidro.valorDefaultDeducibleBMCE" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultDeducibleBMCE" onkeypress="return soloNumeros(this, event, false)" />
			</td>
		</tr>	
		
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonahidro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaHidroForm,'/MidasWeb/catalogos/zonahidro/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>		
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>