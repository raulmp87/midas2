<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/zonahidro/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="zonaHidroForm" propiedad="descripcionZonaHidro"/>'
					</td>
				</tr>
	 			<tr>
	 				<th><midas:mensaje clave="catalogos.zonahidro.codigoZonaHidro" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="codigoZonaHidro" nombre="zonaHidroForm"/></td>
					<th><midas:mensaje clave="catalogos.zonahidro.descripcionZonaHidro" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionZonaHidro" nombre="zonaHidroForm"/></td>
				</tr>	
				<tr>
	 				<th><midas:mensaje clave="catalogos.zonahidro.valorDefaultCoaseguro" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultCoaseguro" nombre="zonaHidroForm"/></td>
					<th><midas:mensaje clave="catalogos.zonahidro.valorDefaultCoaseguroBMCE" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultCoaseguroBMCE" nombre="zonaHidroForm"/></td>
				</tr>
				
				<tr>
	 				<th><midas:mensaje clave="catalogos.zonahidro.valorDefaultDeducible" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultDeducible" nombre="zonaHidroForm"/></td>
					<th><midas:mensaje clave="catalogos.zonahidro.valorDefaultDeducibleBMCE" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultDeducibleBMCE" nombre="zonaHidroForm"/></td>
				</tr>
				
				
				
				 			 	 			 	 			 	 			 					
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonahidro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
								<html:hidden property="idTcZonaHidro" name="zonaHidroForm"/>
							</div>
						</div>
					</td>
				</tr>   	  	
				<tr>
					<midas:mensajeUsuario/>
				</tr>			
		  	</table>
	  	</div>
    </midas:formulario>