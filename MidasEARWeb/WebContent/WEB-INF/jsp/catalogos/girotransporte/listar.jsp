<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/girotransporte/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.girotransporte.descripcion"/></th> 
			<td><midas:texto onkeypress="return soloLetras(this, event, false)" caracteres="200" 
				propiedadFormulario="descripcionGiroTransporte"/></td>
			
			<th><midas:mensaje clave="catalogos.girotransporte.codigo"/></th> 
			<td><midas:texto onkeypress="return soloNumeros(this, event, false)" 
				propiedadFormulario="codigoGiroTransporte"/></td>
				
			<th><midas:mensaje clave="catalogos.girotransporte.idGrupoROT"/></th> 
			<td><midas:texto onkeypress="return soloNumeros(this, event, false)" 
				propiedadFormulario="idGrupoROT"/></td>
			
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroTransporteForm,'/MidasWeb/catalogos/girotransporte/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="giroTransporteTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.GiroTransporte"
			claseCss="tablaConResultados" nombreLista="giroTransporteLista"
			urlAccion="/catalogos/girotransporte/listar.do">
			<midas:columna propiedad="descripcionGiroTransporte" titulo="Descripci&oacute;n del Giro-Transporte" estilo="descripcionGiroTransporte"/>
			<midas:columna propiedad="codigoGiroTransporte" titulo="C&oacute;digo del Giro Transporte" estilo="codigoGiroTransporte"/>
			<midas:columna propiedad="idGrupoROT" titulo="ID del Grupo ROT" estilo="idGrupoROT"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/girotransporte/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/></a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
