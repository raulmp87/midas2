<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/girotransporte/modificar">
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
			</td>
		</tr>
		<tr>
			<td><etiquetas:etiquetaError requerido="si"
					property="descripcionGiroTransporte" name="giroTransporteForm"
					key="catalogos.girotransporte.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/></td>
			<td><midas:texto onkeypress="return soloLetras(this, event, false)" caracteres="200" longitud="100"
				propiedadFormulario="descripcionGiroTransporte" /></td>
			
			<td><etiquetas:etiquetaError requerido="si" property="codigoGiroTransporte" name="giroTransporteForm"
					key="catalogos.girotransporte.codigo" normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td><midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="22" propiedadFormulario="codigoGiroTransporte"/></td>			
		</tr>
		<tr>
			<td><etiquetas:etiquetaError requerido="si" property="idGrupoROT" name="giroTransporteForm"
					key="catalogos.girotransporte.idGrupoROT" normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td><midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="22" propiedadFormulario="idGrupoROT"/></td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/girotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroTransporteForm,'/MidasWeb/catalogos/girotransporte/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<midas:oculto nombreFormulario="giroTransporteForm" propiedadFormulario="idGiroTransporte" />
</midas:formulario>
