<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/girotransporte/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
				<midas:oculto propiedadFormulario="idGiroTransporte"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.girotransporte.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionGiroTransporte"  nombre="giroTransporteForm"/></td>
			
			<th><midas:mensaje clave="catalogos.girotransporte.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoGiroTransporte" nombre="giroTransporteForm"/></td>
		</tr>
		<tr>
		<th><midas:mensaje clave="catalogos.girotransporte.idGrupoROT"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idGrupoROT"  nombre="giroTransporteForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroTransporteForm,'/MidasWeb/catalogos/girotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>