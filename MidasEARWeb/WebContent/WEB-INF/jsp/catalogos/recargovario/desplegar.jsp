<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/recargovario/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="recargoVarioForm" propiedad="descripcionrecargo"/>'
					</td>
				</tr>
	 			<tr>
	 				<th><midas:mensaje clave="catalogos.recargovario.claveTipoRecargo" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionClave" nombre="recargoVarioForm"/></td>
					<th><midas:mensaje clave="catalogos.recargovario.descripcionRecargo" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionrecargo" nombre="recargoVarioForm"/></td>	
				</tr>	 			 	 			 	 			 	 			 					
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/recargovario/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
								<html:hidden property="idtcrecargovario" name="recargoVarioForm"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>  
		  	</table>
	  	</div>
    </midas:formulario>
