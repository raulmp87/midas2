<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/recargovario/listar">
	<midas:oculto nombreFormulario="recargoVarioForm" propiedadFormulario="idtcrecargovario"/>
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.recargovario.filtro.claveTipoRecargo"/>:</th>
			<td>
				<midas:combo propiedad="clavetiporecargo">
					<midas:opcionCombo valor="1">PORCENTAJE</midas:opcionCombo>
					<midas:opcionCombo valor="2">IMPORTE</midas:opcionCombo>
				</midas:combo>
			</td>
			<th><midas:mensaje clave="catalogos.recargovario.filtro.descripcionRecargo"/>:</th>
			<td><midas:texto  propiedadFormulario="descripcionrecargo" caracteres="200"/></td>	
			<input type="text" id="hiddenPatch" style="display: none;"/>		
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.recargoVarioForm,'/MidasWeb/catalogos/recargovario/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="recargoVarioTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.RecargoVario"
			claseCss="tablaConResultados" nombreLista="recargoVarios"
			urlAccion="/catalogos/recargovario/listar.do">
			<midas:columna propiedad="clavetiporecargo" titulo="Clave"/>
			<midas:columna propiedad="descripcionrecargo" titulo="Descripci&oacute;n"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />			
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/recargovario/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
