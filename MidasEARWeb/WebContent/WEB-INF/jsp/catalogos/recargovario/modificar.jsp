<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/recargovario/modificar">
	<midas:oculto nombreFormulario="recargoVarioForm" propiedadFormulario="idtcrecargovario"/>
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" /> '<midas:escribe nombre="recargoVarioForm" propiedad="descripcionrecargo"/>'
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="clavetiporecargo" requerido="si"
					key="catalogos.recargovario.claveTipoRecargo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>			
			<td>
				<midas:combo propiedad="clavetiporecargo">
					<midas:opcionCombo valor="1">PORCENTAJE</midas:opcionCombo>
					<midas:opcionCombo valor="2">IMPORTE</midas:opcionCombo>
				</midas:combo>
			</td>
			<td>
				<etiquetas:etiquetaError property="descripcionrecargo" requerido="si" 
					key="catalogos.recargovario.descripcionRecargo" normalClass="normal"  
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionrecargo"  onkeypress="return soloAlfanumericos(this, event, false)" caracteres="200"/>
			</td>							
		</tr>		
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/recargovario/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.recargoVarioForm,'/MidasWeb/catalogos/recargovario/modificar.do', 'contenido',null);"> <midas:mensaje clave="midas.accion.guardar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>

