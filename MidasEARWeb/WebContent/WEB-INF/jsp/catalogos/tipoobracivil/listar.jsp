<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/tipoobracivil/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<td><midas:mensaje clave="catalogos.tipoobracivil.descripcion"/></td> 
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcion"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td><midas:mensaje clave="catalogos.tipoobracivil.id"/></td> 
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)" 
				propiedadFormulario="idObra"/>
			</td>
			<td colspan="1">&nbsp;</td>
			<td><midas:mensaje clave="catalogos.tipoobracivil.claveAutorizacion"/></td> 
			<td>
				<midas:combo propiedad="claveAutorizacion">
					<midas:opcionCombo valor="0">NO REQUERIDA</midas:opcionCombo>
					<midas:opcionCombo valor="1">AREA TECNICA</midas:opcionCombo>
					<midas:opcionCombo valor="2">AREA DE REASEGURO</midas:opcionCombo>
				</midas:combo>
			</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoObraCivilForm,'/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="tipoObrasCivilesTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoObraCivil"
			claseCss="tablaConResultados" nombreLista="obras"
			urlAccion="/catalogos/tipoobracivil/listar.do">
			<midas:columna propiedad="codigoTipoObraCivil" titulo="ID Tipo de Obra Civil"/>
			<midas:columna propiedad="descripcionTipoObraCivil" titulo="Descripcion"/>
			<midas:columna propiedad="claveAutorizacion" titulo="Clave de Autorizacion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipoobracivil/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
