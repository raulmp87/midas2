<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipoobracivil/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<html:hidden property="idTabla"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipoobracivil.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idObra"  nombre="tipoObraCivilForm"/></td>
			<th><midas:mensaje clave="catalogos.tipoobracivil.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcion" nombre="tipoObraCivilForm"/></td>
			
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipoobracivil.claveAutorizacion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionClave"  nombre="tipoObraCivilForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipoobracivil.periodoNormalConstruccion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionPeriodoNormalConstruccion" nombre="tipoObraCivilForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoObraCivilForm,'/MidasWeb/catalogos/tipoobracivil/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.tipoObraCivilForm,'/MidasWeb/catalogos/tipoobracivil/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>