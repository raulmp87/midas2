<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipoobracivil/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<html:hidden property="idTabla"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="idObra" name="ramoForm"
					key="catalogos.ramo.id" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)" caracteres="100" 
				propiedadFormulario="idObra"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcion" name="tipoObraCivilForm"
					key="catalogos.tipoobracivil.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcion"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="claveAutorizacion" name="tipoObraCivilForm"
					key="catalogos.tipoobracivil.claveAutorizacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:combo propiedad="claveAutorizacion" styleClass="cajaTexto">
					<midas:opcionCombo valor="0">NO REQUERIDA</midas:opcionCombo>
					<midas:opcionCombo valor="1">AREA TECNICA</midas:opcionCombo>
					<midas:opcionCombo valor="2">AREA DE REASEGURO</midas:opcionCombo>
				</midas:combo>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="periodoNormalConstruccion" name="tipoObraCivilForm"
					key="catalogos.tipoobracivil.periodoNormalConstruccion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:comboValorFijo grupoValores="45" propiedad="periodoNormalConstruccion" nombre="tipoObraCivilForm" styleClass="cajaTexto"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipoobracivil/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoObraCivilForm,'/MidasWeb/catalogos/tipoobracivil/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
