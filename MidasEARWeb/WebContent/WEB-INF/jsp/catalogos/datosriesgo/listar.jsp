<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario  accion="/catalogos/datosriesgo/listarFiltrado">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="9">
				<midas:mensaje clave="catalogos.datosriesgo.titulo"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.datosriesgo.ramo"/>:</th>
			
			<td colspan="3">
				<midas:ramo styleId="idTcRamo" size="1" propiedad="idTcRamo" styleClass="cajaTexto"
					onchange="getSubRamos(this,'idTcSubRamo');"	/>
			</td>
									
			<th><midas:mensaje clave="catalogos.datosriesgo.subramo"/>:</th>
			
			<td colspan="3">
				<midas:subramo styleId="idTcSubRamo" ramo="idTcRamo" size="1" propiedad="idTcSubRamo" styleClass="cajaTexto"/>
			</td>
			<td>
				<input type="text" id="hiddenPatch" style="display: none;"/>
				<midas:boton onclick="javascript: sendRequest(document.datosRiesgoForm,'/MidasWeb/catalogos/datosriesgo/listarFiltrado.do', 'contenido',null);" 
				tipo="buscar"/>
			</td>
		</tr> 
			
	</table>
	<div id="resultados">
		<midas:tabla idTabla="datosRiesgoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.DatosRiesgo"
			claseCss="tablaConResultados" nombreLista="listaDatosRiesgo"
			urlAccion="/catalogos/datosriesgo/listarFiltrado.do"
			>
			<midas:columna maxCaracteres="60" propiedad="descripcionRiesgo" titulo="Riesgo"/>
			<midas:columna propiedad="descripcionEtiqueta" titulo="Dato de Riesgo"/>
			<midas:columna propiedad="claveImpresionPoliza" titulo="Se imprime?" />
			<midas:columna propiedad="codigoFormato" titulo="Formato" />
		</midas:tabla>
	</div>
	
</midas:formulario>