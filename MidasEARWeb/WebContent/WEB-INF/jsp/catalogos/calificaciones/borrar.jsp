<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/calificacionesreas/borrar">
	<midas:oculto propiedadFormulario="idtcContacto"/>    
		<table id="agregar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.contacto.accion.borrar" />
					</td>
				</tr>
	 			<tr>
					<td width="25%">
						<etiquetas:etiquetaError property="idagencia" requerido="si"
							key="catalogos.contacto.idtcreaseguradorcorredor" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:comboCatalogo propiedad="idAgencia" readonly="true" styleId="idAgencia"  size="1" styleClass="cajaTexto" nombreCatalogo="trAgencia" idCatalogo="idagencia" descripcionCatalogo="nombreAgencia" />
					</td>
					<td width="25%">
						<etiquetas:etiquetaError property="calificacion" requerido="si"
							key="catalogos.contacto.nombre" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:texto propiedadFormulario="calificacion"
							onkeypress="return soloAlfanumericos(this, event, false)"  deshabilitado="true"/>
					</td>
				</tr>
				
				<tr>
					<td width="25%">
						<etiquetas:etiquetaError property="valor" requerido="si"
							key="catalogos.contacto.telefono" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:texto propiedadFormulario="valor" onkeypress="return soloNumeros(this, event, false)" deshabilitado="true"/>
					</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td class="guardar" colspan="4" >
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar"> 
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/calificacionesreas/listar.do', 'contenido',null);">
									<midas:mensaje clave="midas.accion.regresar"/>
								</a>
							</div>
							<div id="b_guardar">
								<a href="javascript: void(0);"
									 								
									onclick="javascript: Confirma('<midas:mensaje clave="comun.confirmacionborrar" />', document.calificacionForm,'/MidasWeb/catalogos/calificacionesreas/borrar.do', 'contenido','notificacionResultadoOperacionModificado()');">
									
									<midas:mensaje clave="midas.accion.borrar"/>
								</a>
							</div>
						</div>
					</td> 
				</tr>	  				
		</table>
    </midas:formulario>