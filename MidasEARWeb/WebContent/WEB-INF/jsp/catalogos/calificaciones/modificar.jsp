<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/calificacionesreas/modificar">
	<midas:oculto propiedadFormulario="idtcContacto" />
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.calificaciones.modificar" /> <midas:escribe nombre="calificacionForm" propiedad="calificacion"/>				
			</td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="idagencia" requerido="si"
					key="catalogos.calificaciones.idagencia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:comboCatalogo propiedad="idAgencia" styleId="idAgencia"  size="1" styleClass="cajaTexto" nombreCatalogo="trAgencia" idCatalogo="idagencia" descripcionCatalogo="nombreAgencia" />
			</td>
			<td width="25%">
				<etiquetas:etiquetaError property="calificacion" requerido="si"
					key="catalogos.calificaciones.calificacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="calificacion"
					 />
			</td>
		</tr>
		
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="valor" requerido="si"
					key="catalogos.calificaciones.valor" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="valor" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td class="guardar" colspan="4" >
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/calificacionesreas/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.calificacionForm,'/MidasWeb/catalogos/calificacionesreas/modificar.do', 'contenido','notificacionResultadoOperacion()');">
							<midas:mensaje clave="midas.accion.guardar" />
						</a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
	
</midas:formulario>

