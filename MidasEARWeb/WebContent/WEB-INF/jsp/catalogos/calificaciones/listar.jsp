<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page buffer = "16kb" %>

<midas:formulario  accion="/catalogos/calificacionesreas/listar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	<table width="95%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.calificaciones.listaCal"/><!-- catalogos.calificaciones.listaCa -->
				<hr>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.calificaciones.entidadCal"/></th>
			<td>
				<midas:comboCatalogo propiedad="idAgencia" styleId="idagencia"  size="1" styleClass="cajaTexto" nombreCatalogo="trAgencia" idCatalogo="idagencia" descripcionCatalogo="nombreAgencia" />
			</td>
			<th><midas:mensaje clave="catalogos.calificaciones.calificacion"/></th>
			<td><midas:texto propiedadFormulario="calificacion" onkeypress="return stopRKey(event)"/></td>
			<th><midas:mensaje clave="catalogos.calificaciones.valor"/></th>
			<td><midas:texto propiedadFormulario="valor" onkeypress="return stopRKey(event)"/></td>
		</tr> 
		
		<tr>
			<td colspan="5">&nbsp;</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.calificacionForm,'/MidasWeb/catalogos/calificacionesreas/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.contacto.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
	</table>
	<div id="mensajeResultadoOperacion" class="notificacionResultadoOperacion" style="display:none;margin-top:15px;margin-bottom:15px;width:95%"></div>
	<br clear="all" />
	<div id="resultados" style="width:95%">
		<midas:tabla idTabla="contactoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.CalificacionAgencia"
			claseCss="tablaConResultados" nombreLista="contactos"
			urlAccion="/catalogos/calificacionesreas/listarFiltrado.do">
			<midas:columna propiedad="agencia.nombreAgencia" titulo="Agencia" />
			<midas:columna propiedad="calificacion" titulo="Calificaci&oacute;n" />
			<midas:columna propiedad="valor" titulo="Valor N&uacute;merico"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div class="alinearBotonALaDerecha">
			<div id="b_agregar">
				<a href="javascript: void(0);"
					onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/calificacionesreas/mostrarAgregar.do', 'contenido',null);">
					<midas:mensaje clave="midas.accion.agregar"/>
				</a>
			</div>
		</div>
	</div>
	<midas:mensajeUsuario/>
</midas:formulario>
