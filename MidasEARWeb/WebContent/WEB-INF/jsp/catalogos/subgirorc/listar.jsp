<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/subgirorc/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.subgirorc.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgirorc.codigoSubGiroRC"/></th> 
			<td>
				<midas:texto 
					caracteres="22"
					propiedadFormulario="codigoSubGiroRC"
					onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgirorc.descripcionSubGiroRC"/></th> 
			<td colspan="2">
				<midas:texto
				caracteres="200"
				propiedadFormulario="descripcionSubGiroRC"/>
			</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="2">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subGiroRCForm,'/MidasWeb/catalogos/subgirorc/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="subRamosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.SubGiroRC"
			claseCss="tablaConResultados" nombreLista="subGirosRC"
			urlAccion="/catalogos/subgirorc/listar.do">
			<midas:columna propiedad="codigoSubGiroRC" titulo="Codigo"/>
			<midas:columna propiedad="descripcionSubGiroRC" titulo="Descripcion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subgirorc/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
