<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subgirorc/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.subgirorc.detalle" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgirorc.codigoSubGiroRC"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoSubGiroRC" nombre="subGiroRCForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgirorc.descripcionSubGiroRC"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionSubGiroRC" nombre="subGiroRCForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgirorc.claveTipoRiesgo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="claveTipoRiesgo" nombre="subGiroRCForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgirorc.claveAutorizacion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="claveAutorizacion" nombre="subGiroRCForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subGiroRCForm,'/MidasWeb/catalogos/subgirorc/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>