<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subgirorc/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.subgirorc.agregar" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroRCForm" 
					key="catalogos.subgirorc.codigoSubGiroRC" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="codigoSubGiroRC"/>
			</th>
			<td>
				<midas:texto 
					propiedadFormulario="codigoSubGiroRC" 
					caracteres="200" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					property="idGiroRC" 
					requerido="si"
					key="catalogos.subgirorc.idGiroRC" 
					normalClass="normal"
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</th>
			<td>
				<midas:comboCatalogo 
					propiedad="idGiroRC" 
					styleClass="cajaTexto" 
					size="1"
					nombreCatalogo="tcgirorc" 
					idCatalogo="idTcGiroRC"
					descripcionCatalogo="descripcionGiroRC" 
					styleId="giroRCDTO" />
			</td>
			<td></td><td></td>
		</tr> 
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroRCForm" 
					key="catalogos.subgirorc.descripcionSubGiroRC" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="descripcionSubGiroRC"/>
			</th>
			<td colspan="3">
				<midas:texto 
					propiedadFormulario="descripcionSubGiroRC" 
					caracteres="200" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroRCForm" 
					key="catalogos.subgirorc.claveTipoRiesgo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="claveTipoRiesgo" />
			</th>
			<td>
				<midas:texto 
					propiedadFormulario="claveTipoRiesgo" 
					caracteres="10" 
					onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroRCForm" 
					key="catalogos.subgirorc.claveAutorizacion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="claveAutorizacion" />
			</th>
			<td>
				<midas:texto 
					propiedadFormulario="claveAutorizacion" 
					caracteres="4" 
					onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subgirorc/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subGiroRCForm,'/MidasWeb/catalogos/subgirorc/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:escribe propiedad="mensaje" nombre="subGiroRCForm"/>
		</tr>
	</table>
</midas:formulario>
