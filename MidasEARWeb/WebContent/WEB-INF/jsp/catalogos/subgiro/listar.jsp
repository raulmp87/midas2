<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/subgiro/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.codigosubgiro"/></th> 
			<td>
				<midas:texto 
					caracteres="22"
					propiedadFormulario="codigoSubGiro"
					onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.descripcion"/></th> 
			<td colspan="2">
				<midas:texto
				caracteres="200"
				propiedadFormulario="descripcionSubGiro"/>
			</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="2">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subGiroForm,'/MidasWeb/catalogos/subgiro/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="subRamosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.SubGiro"
			claseCss="tablaConResultados" nombreLista="subGiros"
			urlAccion="/catalogos/subgiro/listar.do">
			<midas:columna propiedad="codigoSubGiro" titulo="C&oacute;digo"/>
			<midas:columna propiedad="giroDTO.descripcionGiro" titulo="Giro"/>
			<midas:columna propiedad="descripcionSubGiro" titulo="Descripci&oacute;n"/>
			<midas:columna propiedad="acciones" titulo="Acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subgiro/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
