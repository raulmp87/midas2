<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subgiro/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.subgiro.agregar" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.codigosubgiro" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="codigoSubGiro"/>
			</th>
			<td>
				<midas:texto 
					propiedadFormulario="codigoSubGiro" 
					caracteres="200" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					property="idGiro" 
					requerido="si"
					key="catalogos.subgiro.giroDTO" 
					normalClass="normal"
					errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<midas:comboCatalogo 
					propiedad="idTcGiro" 
					styleClass="cajaTexto" 
					size="1"
					nombreCatalogo="tcgiro" 
					idCatalogo="idTcGiro"
					descripcionCatalogo="descripcionGiro" 
					styleId="giroDTO" />
			</td>
			<td></td><td></td>
		</tr> 
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.descripcion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="descripcionSubGiro"/>
			</th>
			<td colspan="3">
				<midas:texto 
					propiedadFormulario="descripcionSubGiro" 
					caracteres="200" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.idGrupoRobo" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="idGrupoRobo" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="10" propiedad="idGrupoRobo" nombre="subGiroForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.idGrupoDineroValores" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="idGrupoDineroValores" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="11" propiedad="idGrupoDineroValores" nombre="subGiroForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.idGrupoCristales" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="idGrupoCristales" />
			</th>
			<td>
				<midas:comboValorFijo grupoValores="12" propiedad="idGrupoCristales" nombre="subGiroForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.idGrupoPlenos" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="idGrupoPlenos" />
			</th>
			<td>
				<midas:escribeCatalogo styleId="idGrupoPlenos" size="" propiedad="idGrupoPlenos" 
				clase="mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroFacadeRemote" styleClass="cajaTexto" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					requerido="si" 
					name="subGiroForm" 
					key="catalogos.subgiro.claveInspeccion" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif" 
					property="claveInspeccion" />
			</th>
			<td>
				<midas:texto 
					propiedadFormulario="claveInspeccion" 
					caracteres="10" 
					onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subgiro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subGiroForm,'/MidasWeb/catalogos/subgiro/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:escribe propiedad="mensaje" nombre="subGiroForm"/>
		</tr>
	</table>
</midas:formulario>
