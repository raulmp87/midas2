<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subgiro/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.subgiro.detalle" />
				<midas:oculto propiedadFormulario="idSubGiro"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.codigosubgiro"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoSubGiro" nombre="subGiroForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.descripcionsubgiro"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionSubGiro" nombre="subGiroForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.idGrupoRobo"/>:</th>
			<td><midas:comboValorFijo grupoValores="10" propiedad="idGrupoRobo" nombre="subGiroForm" styleClass="cajaTexto" readonly="true"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.idGrupoDineroValores"/>:</th>
			<td><midas:comboValorFijo grupoValores="11" propiedad="idGrupoDineroValores" nombre="subGiroForm" styleClass="cajaTexto" readonly="true"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.idGrupoCristales"/>:</th>
			<td><midas:comboValorFijo grupoValores="12" propiedad="idGrupoCristales" nombre="subGiroForm" styleClass="cajaTexto" readonly="true"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.idGrupoPlenos"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idGrupoPlenos" nombre="subGiroForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subgiro.claveInspeccion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="claveInspeccion" nombre="subGiroForm"/></td>
			<td></td><td></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subGiroForm,'/MidasWeb/catalogos/subgiro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.subGiroForm,'/MidasWeb/catalogos/subgiro/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>