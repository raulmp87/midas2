<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/materialcombustible/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<html:hidden property="idtcmaterialcombustiblesistema"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.materialcombustible.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idtcmaterialcombustible"  nombre="materialCombustibleForm"/></td>
			<th><midas:mensaje clave="catalogos.materialcombustible.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionmaterialcombustible" nombre="materialCombustibleForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.materialCombustibleForm,'/MidasWeb/catalogos/materialcombustible/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.materialCombustibleForm,'/MidasWeb/catalogos/materialcombustible/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>