<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/descuento/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.agregar" />
			</td>
		</tr>
		<tr>
			<td>
			<etiquetas:etiquetaError
				property="claveTipo" name="descuentoForm" requerido="si"
				key="catalogos.descuento.clavetipo" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:combo propiedad="claveTipo">
					<midas:opcionCombo valor="1">PORCENTAJE</midas:opcionCombo>
					<midas:opcionCombo valor="2">IMPORTE</midas:opcionCombo>
				</midas:combo>
			</td>
			<td>
				<etiquetas:etiquetaError
					property="descripcion" name="descuentoForm" requerido="si"
					key="catalogos.descuento.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcion" caracteres="200"/>
			</td>
		</tr>
		<tr>
		<td class="campoRequerido" colspan="3">
 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
		</td> 
		<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/descuento/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.descuentoForm,'/MidasWeb/catalogos/descuento/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
		</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
