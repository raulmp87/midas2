<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/descuento/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>	
		<tr>
			<th><midas:mensaje clave="catalogos.descuento.filtro.clavetipo"/>:</th>
			<td>
				<midas:combo propiedad="claveTipo">
					<midas:opcionCombo valor="1">PORCENTAJE</midas:opcionCombo>
					<midas:opcionCombo valor="2">IMPORTE</midas:opcionCombo>
				</midas:combo>
			</td>
			<th><midas:mensaje clave="catalogos.descuento.filtro.descripcion"/>:</th>
			<td><midas:texto propiedadFormulario="descripcion"/></td>
			<input type="text" id="hiddenPatch" style="display: none;"/>
		</tr> 	
		<tr>
			<td  class="buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.descuentoForm,'/MidasWeb/catalogos/descuento/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>			
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="descuentosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Descuento"
			claseCss="tablaConResultados" nombreLista="descuentos"
			urlAccion="/catalogos/descuento/listar.do">
			<midas:columna propiedad="claveTipo" titulo="Clave de Tipo" estilo="claveTipo"/>
			<midas:columna propiedad="descripcion" titulo="Descripci&oacute;n" estilo="descripcion"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones"/>	
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/descuento/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
