<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/descuento/borrar">
	<div id="centrarDesplegar">
		<table id="desplegar">
			<tr>
				<td class="titulo" colspan="4">
					<midas:mensaje clave="midas.accion.borrar" /> '<midas:escribe nombre="descuentoForm" propiedad="descripcion" />'
					<html:hidden property="idDescuento" name="descuentoForm" />
				</td>
			</tr>
			<tr>
				<th>
					<midas:mensaje clave="catalogos.descuento.clavetipo" />:
				</th>
				<td class="fondoCajaTexto">
					<midas:escribe nombre="descuentoForm" propiedad="descripcionClave" />
				</td>

				<th>
					<midas:mensaje clave="catalogos.descuento.descripcion" />:
				</th>
				<td class="fondoCajaTexto">
					<midas:escribe nombre="descuentoForm" propiedad="descripcion" />
				</td>
			</tr>
			<tr>
				<td class="regresar" colspan="4">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);"
								onclick="javascript: sendRequest(document.descuentoForm,'/MidasWeb/catalogos/descuento/listar.do', 'contenido',null);">
								<midas:mensaje clave="midas.accion.regresar"/>
							</a>
						</div>
						<div id="b_borrar">
							<a href="javascript: void(0);"
								onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.descuentoForm,'/MidasWeb/catalogos/descuento/borrar.do', 'contenido',null);">
								<midas:mensaje clave="midas.accion.borrar" /> </a>
						</div>
					</div>
				</td>
			</tr>
			<tr>
			<midas:mensajeUsuario/>
		</tr>
		</table>
	</div>
</midas:formulario>
