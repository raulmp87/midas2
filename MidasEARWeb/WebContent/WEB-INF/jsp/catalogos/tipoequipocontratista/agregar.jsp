<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipoequipocontratista/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="codigoTipoEquipoContratista" requerido="si"
					key="catalogos.tipoequipocontratista.codigoTipoEquipoContratista" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="codigoTipoEquipoContratista"
				onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="descripcionTipoEqContr" requerido="si"
					key="catalogos.tipoequipocontratista.descripcionTipoEqContr" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionTipoEqContr"
				onkeypress="return soloLetras(this, event, false)"/>
			</td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipoequipocontratista/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoEquipoContratistaForm,'/MidasWeb/catalogos/tipoequipocontratista/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:escribe propiedad="mensajeUsuario" nombre="tipoEquipoContratistaForm"/>
		</tr>
	</table>
</midas:formulario>
