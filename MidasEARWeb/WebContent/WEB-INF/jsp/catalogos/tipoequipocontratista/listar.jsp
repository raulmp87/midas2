<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/tipoequipocontratista/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th colspan="2"><midas:mensaje clave="catalogos.tipoequipocontratista.codigoTipoEquipoContratista"/>:</th>
			<td><midas:texto propiedadFormulario="codigoTipoEquipoContratista" onkeypress="return soloNumeros(this, event, false)"/></td>
			<th><midas:mensaje clave="catalogos.tipoequipocontratista.descripcionTipoEqContr"/>:</th>
			<td><midas:texto propiedadFormulario="descripcionTipoEqContr" onkeypress="return soloLetras(this, event, false)"/></td>	
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoEquipoContratistaForm,'/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do','contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="tipoEquipoContratistaTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoEquipoContratista"
			claseCss="tablaConResultados" nombreLista="tipoEquipoContratista"
			urlAccion="/catalogos/tipoequipocontratista/listar.do">
				<midas:columna propiedad="codigoTipoEquipoContratista" titulo="C&oacute;digo Tipo"/>
				<midas:columna propiedad="descripcionTipoEqContr" titulo="Descripci&oacute;n" estilo="descripcion"/>
			<midas:columna propiedad="acciones" titulo=""/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipoequipocontratista/mostrarAgregar.do','contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>