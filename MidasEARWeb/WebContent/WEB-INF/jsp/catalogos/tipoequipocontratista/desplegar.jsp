<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipoequipocontratista/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
				<html:hidden property="idTipoEquipoContratista"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipoequipocontratista.codigoTipoEquipoContratista"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoTipoEquipoContratista"  nombre="tipoEquipoContratistaForm"/></td>
			<th><midas:mensaje clave="catalogos.tipoequipocontratista.descripcionTipoEqContr"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoEqContr" nombre="tipoEquipoContratistaForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoEquipoContratistaForm,'/MidasWeb/catalogos/tipoequipocontratista/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>