<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/mediotransporte/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<html:hidden property="idtcmediotransportesistema"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.mediotransporte.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idtcmediotransporte"  nombre="medioTransporteForm"/></td>
			<th><midas:mensaje clave="catalogos.mediotransporte.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionmediotransporte" nombre="medioTransporteForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.medioTransporteForm,'/MidasWeb/catalogos/mediotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.medioTransporteForm,'/MidasWeb/catalogos/mediotransporte/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>