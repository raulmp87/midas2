midas<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/mediotransporte/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
				<html:hidden property="idtcmediotransportesistema"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="idtcmediotransporte" name="medioTransporteForm"
					key="catalogos.mediotransporte.id" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)" caracteres="100" 
				propiedadFormulario="idtcmediotransporte"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionmediotransporte" name="medioTransporteForm"
					key="catalogos.mediotransporte.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionmediotransporte"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/mediotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.medioTransporteForm,'/MidasWeb/catalogos/mediotransporte/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
