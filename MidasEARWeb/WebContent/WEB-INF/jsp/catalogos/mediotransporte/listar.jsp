<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/mediotransporte/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<td><midas:mensaje clave="catalogos.mediotransporte.descripcion"/></td> 
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionmediotransporte"/>
			</td>
			<td colspan="2">&nbsp;</td>
			<td><midas:mensaje clave="catalogos.mediotransporte.id"/></td> 
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)" 
				propiedadFormulario="idtcmediotransporte"/>
			</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.medioTransporteForm,'/MidasWeb/catalogos/mediotransporte/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="medioTransporteTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.MedioTransporte"
			claseCss="tablaConResultados" nombreLista="transportes"
			urlAccion="/catalogos/mediotransporte/listar.do">
			<midas:columna propiedad="codigoMedioTransporte" titulo="ID de Medio de Transporte"/>
			<midas:columna propiedad="descripcionMedioTransporte" titulo="Descripcion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/mediotransporte/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
