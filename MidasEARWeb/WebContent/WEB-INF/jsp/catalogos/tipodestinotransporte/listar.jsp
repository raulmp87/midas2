<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/tipodestinotransporte/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipodestinotransporte.codigo"/>:</th>
			<td><midas:texto propiedadFormulario="codigoTipoDestinoTransporte" caracteres="22" onkeypress="return soloNumeros(this, event, false)" /> </td>
			
			<th><midas:mensaje clave="catalogos.tipodestinotransporte.descripcion"/>:</th>
			<td><midas:texto propiedadFormulario="descripcionTipoDestinoTransporte" caracteres="200" onkeypress="return soloLetras(this, event, false)"/> </td>
		</tr>
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoDestinoTransporteForm,'/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="tipoDestinoTransporteTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoDestinoTransporte"
			claseCss="tablaConResultados" nombreLista="tipoDestinoTransporteList"
			urlAccion="/catalogos/tipodestinotransporte/listar.do">
			<midas:columna propiedad="codigoTipoDestinoTransporte" titulo="Codigo"/>
			<midas:columna propiedad="descripcionTipoDestinoTransporte" titulo="Descripcion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipodestinotransporte/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
