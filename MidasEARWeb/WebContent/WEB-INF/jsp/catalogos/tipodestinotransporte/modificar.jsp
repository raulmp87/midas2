<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipodestinotransporte/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<midas:oculto propiedadFormulario="idTipoDestinoTransporte"/>
			</td>
		</tr>
		<tr>
			<td><etiquetas:etiquetaError requerido="si" property="codigoTipoDestinoTransporte" name="tipoDestinoTransporteForm"
					key="catalogos.tipodestinotransporte.codigo" normalClass="normal"	errorClass="error" errorImage="/img/information.gif"/></td>
			<td><midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="22" propiedadFormulario="codigoTipoDestinoTransporte"/></td>
		</tr>
		<tr>
			<td><etiquetas:etiquetaError requerido="si" property="descripcionTipoDestinoTransporte" name="tipoDestinoTransporteForm"
					key="catalogos.tipodestinotransporte.descripcion" normalClass="normal"	errorClass="error" errorImage="/img/information.gif"/></td>
			<td><midas:texto onkeypress="return soloLetras(this, event, false)" caracteres="200" propiedadFormulario="descripcionTipoDestinoTransporte"/></td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipodestinotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoDestinoTransporteForm,'/MidasWeb/catalogos/tipodestinotransporte/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
