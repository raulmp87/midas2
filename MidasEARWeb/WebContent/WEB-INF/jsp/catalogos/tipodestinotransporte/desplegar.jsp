<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipodestinotransporte/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
				<midas:oculto propiedadFormulario="idTipoDestinoTransporte"/>
			</td>
		</tr> 
		<tr>
			<th> <midas:mensaje clave="catalogos.tipodestinotransporte.codigo"/> </th>
			<td class="fondoCajaTexto"> <midas:escribe propiedad="codigoTipoDestinoTransporte" nombre="tipoDestinoTransporteForm"/> </td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipodestinotransporte.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoDestinoTransporte" nombre="tipoDestinoTransporteForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoDestinoTransporteForm,'/MidasWeb/catalogos/tipodestinotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>