<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>

<midas:formulario  accion="/catalogos/modelovehiculo/listar">
	<table style="width:98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Modelo Veh&iacute;culo
			</td>
		</tr>
		<tr>	
			<th><midas:mensaje clave="catalogos.estilovehiculo.id_claveTipoBien"/>:</th>
			<td class="fondoCajaTexto" colspan="2">
				<midas:comboCatalogo propiedad="id_claveTipoBien" 
					styleId="id_claveTipoBien" 
					size="1" styleClass="cajaTexto"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'idTcTipoVehiculo');validaFiltroModeloVeh();"
				/>
			</td>
			<th><midas:mensaje clave="catalogos.estilovehiculo.tipoVehiculo"/>:</th>
			<td class="fondoCajaTexto" colspan="2">
				<midas:tipoVehiculo styleId="idTcTipoVehiculo" 
					size="1" propiedad="idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto"
					onchange="getMarcaVehiculos(this,'idTcMarcaVehiculo'); validaFiltroModeloVeh();" 
				/>
			</td>
		</tr>
		<tr>			
			<th><midas:mensaje clave="catalogos.estilovehiculo.marcaVehiculo"/>:</th>
			<td class="fondoCajaTexto" colspan="2">
				<midas:marcaVehiculo styleId="idTcMarcaVehiculo" 
					size="1" propiedad="idTcMarcaVehiculo"
					tipoVehiculo="idTcTipoVehiculo" 
					styleClass="cajaTexto"
					onchange="getVersionCarga(this,'id_claveTipoBien','idTcTipoVehiculo','id_idVersionCarga');validaFiltroModeloVeh();"
				/>
			</td>
			<th><midas:mensaje clave="catalogos.estilovehiculo.id_idVersionCarga"/>:</th>
			<td class="fondoCajaTexto" colspan="2">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto" 
					tipoBien="id_claveTipoBien" 
					marcaVehiculo="idTcMarcaVehiculo" 
					tipoVehiculo="idTcTipoVehiculo"
					onchange="getEstiloVehiculos(this,'idTcTipoVehiculo','idTcMarcaVehiculo','comboEstiloVehiculo');validaFiltroModeloVeh();"
				/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.modelovehiculo.id_estiloVehiculo"/>:</th>
			<td colspan="2">
				<midas:estiloVehiculo styleId="comboEstiloVehiculo" size="1" 
					propiedad="idCompuestoTcVehiculo" styleClass="cajaTexto"
					tipoVehiculo="idTcTipoVehiculo" 
					marcaVehiculo="idTcMarcaVehiculo" 
					versionCarga="id_idVersionCarga"  
					onchange="validaFiltroModeloVeh();"
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.modelovehiculo.id_idMoneda"/>:</th>
			<td colspan="2">
				<midas:comboCatalogo propiedad="id_idMoneda" size="1" 
					styleId="comboMoneda" nombre="modeloVehiculoForm" 
					styleClass="cajaTexto" nombreCatalogo="vnmoneda" 
					idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" 
					readonly="false"
				/>
			</td>
			<th><midas:mensaje clave="catalogos.modelovehiculo.id_modeloVehiculo"/>:</th>
			<td colspan="2">
				<midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="4" id="modeloVehiculo"
				propiedadFormulario="id_modeloVehiculo" nombreFormulario="modeloVehiculoForm"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.modelovehiculo.valorNuevo"/>:</th>
			<td colspan="2">
				<midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="8" id="valorNuevo"
				propiedadFormulario="valorNuevo" nombreFormulario="modeloVehiculoForm"/>				
			</td>
			<th><midas:mensaje clave="catalogos.modelovehiculo.valorComercial"/>:</th>
			<td colspan="2">
				<midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="8" id="valorComercial"
				propiedadFormulario="valorComercial" nombreFormulario="modeloVehiculoForm"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.modelovehiculo.claveCondRiesgo"/>:</th>
			<td colspan="2">
				<midas:texto caracteres="4" id="claveCondRiesgo"
				propiedadFormulario="claveCondRiesgo" nombreFormulario="modeloVehiculoForm" onkeypress="return soloAlfanumericos(this, event, false)"/>
			</td>
			<th><midas:mensaje clave="catalogos.modelovehiculo.valorCaratula"/>:</th>
			<td colspan="2">
				<midas:texto onkeypress="return soloNumeros(this, event, false)" caracteres="8" id="valorCaratula"
				propiedadFormulario="valorCaratula" nombreFormulario="modeloVehiculoForm"/>
			</td>
		</tr>
		<tr>
			<td style="font-size: 7pt; color: #990000;font-weight: bold;" colspan="5">
				Para activar la b&uacute;squeda debe ingresar: <midas:mensaje clave="catalogos.estilovehiculo.id_claveTipoBien"/>, 
				<midas:mensaje clave="catalogos.estilovehiculo.tipoVehiculo"/>,
				<midas:mensaje clave="catalogos.estilovehiculo.marcaVehiculo"/>,
				<midas:mensaje clave="catalogos.estilovehiculo.id_idVersionCarga"/>,
				<midas:mensaje clave="catalogos.modelovehiculo.id_estiloVehiculo"/>
				
			</td>
			<td class="buscar" colspan="2">
				<div id="busquedaModelos" class="alinearBotonALaDerecha" style="display: none">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.modeloVehiculoForm,'/MidasWeb/catalogos/modelovehiculo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
				<div id="b_descargar" class="alinearBotonALaDerecha" style="display: none;float: right;width: 70px; margin: 0px 2px 0px 2px;">
					<a href="javascript: void(0);" onclick="javascript: downloadExcelModel();">
						<midas:mensaje clave="midas.accion.descargar"/>
					</a>
				</div>
				<div id="b_actualizar" class="alinearBotonALaDerecha" style="display: none;float: right;width: 70px; margin: 0px 2px 0px 2px;">
					<a href="javascript: void(0);" onclick="javascript: loadExcelModel();">
						<midas:mensaje clave="midas.accion.actualizar"/>
					</a>
				</div>
			</td>      		
		</tr>
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensajeUsuario/>
			</td>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listModeloVehiculo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorModeloVehiculo"
			claseCss="tablaConResultados" nombreLista="listModeloVehiculo"
			urlAccion="/catalogos/modelovehiculo/listarFiltrado.do">
			<midas:columna propiedad="id.claveTipoBien" titulo="Clave Tipo Bien" />
			<midas:columna propiedad="id.claveEstilo" titulo="Clave Estilo" />
			<midas:columna propiedad="id.idVersionCarga" titulo="Versi&oacute;n Carga" />
			<midas:columna propiedad="id.modeloVehiculo" titulo="Modelo " />
			<midas:columna propiedad="valorNuevo" titulo="Valor Nuevo" />
			<midas:columna propiedad="valorComercial" titulo="Valor Comercial" />
			<midas:columna propiedad="claveCondRiesgo" titulo="Clave Cond Riesgo" />
			<midas:columna propiedad="valorCaratula" titulo="Valor Caratula" />
			<midas:columna propiedad="acciones" titulo="Acciones" estilo="acciones" />
		</midas:tabla>		
	</div>
	
	<div id="b_agregar" class="alinearBotonALaDerecha">
		<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/mostrarAgregar.do', 'contenido','validaFiltro();');">
			<midas:mensaje clave="midas.accion.agregar"/>
		</a>
	</div>
</midas:formulario>
