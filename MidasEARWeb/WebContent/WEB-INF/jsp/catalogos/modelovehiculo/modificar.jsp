<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/modelovehiculo/modificar"> 
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" /> Modelo Veh&iacute;culo
				<midas:oculto propiedadFormulario="idCompuestoTcVehiculo" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="id_claveTipoBien" nombreFormulario="modeloVehiculoForm" />				
				<midas:oculto propiedadFormulario="id_idMoneda" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="id_modeloVehiculo" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="descripcionEstiloVehiculo" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="descripcionMoneda" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="id_idVersionCarga" nombreFormulario="modeloVehiculoForm"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError 
					property="id_claveTipoBien" requerido="si"
					name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:comboCatalogo propiedad="id_claveTipoBien" size="1"
					styleId="id_claveTipoBien" styleClass="cajaTexto w200"
					nombreCatalogo="tctipobienautos" 
					idCatalogo="claveTipoBien" readonly="true"
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'idTcTipoVehiculo')"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="tipoVehiculoForm.idTcTipoVehiculo" 
					name="modeloVehiculoForm" 
					key="catalogos.estilovehiculo.tipoVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>	
			<td class="fondoCajaTexto">
				<midas:tipoVehiculo styleId="tipoVehiculoForm.idTcTipoVehiculo" 
					size="1" propiedad="idTcTipoVehiculo" readonly="true"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto w200"					
					onchange="getMarcaVehiculos(this,'idTcMarcaVehiculo')" 
				/>
				<midas:oculto propiedadFormulario="idTcTipoVehiculo" 
					nombreFormulario="modeloVehiculoForm" 
				/>
			</td>			
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="estiloVehiculoForm.idTcMarcaVehiculo" 
					name="modeloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.marcaVehiculo" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:marcaVehiculo styleId="idTcMarcaVehiculo" size="1" 
					propiedad="idTcMarcaVehiculo" readonly="true"
					tipoVehiculo="idTcTipoVehiculo" styleClass="cajaTexto w200"
					onchange="getVersionCarga(this,'id_claveTipoBien','tipoVehiculoForm.idTcTipoVehiculo','id_idVersionCarga');"
				/>
				<midas:oculto propiedadFormulario="idTcMarcaVehiculo" 
					nombreFormulario="modeloVehiculoForm" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_idVersionCarga" name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto w200" 
					tipoBien="id_claveTipoBien" readonly="true"
					marcaVehiculo="idTcMarcaVehiculo" 
					tipoVehiculo="idTcTipoVehiculo"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>	
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="idCompuestoTcVehiculo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_estiloVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>			
			<td>
				<midas:estiloVehiculo styleId="comboEstiloVehiculo" size="1" 
					propiedad="idCompuestoTcVehiculo" styleClass="cajaTexto w200"
					tipoVehiculo="idTcTipoVehiculo" readonly="true"
					marcaVehiculo="idTcMarcaVehiculo" 
					versionCarga="id_idVersionCarga" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_idMoneda" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_idMoneda" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<midas:comboCatalogo propiedad="id_idMoneda" size="1" 
					styleId="moneda" nombre="modeloVehiculoForm" 
					styleClass="cajaTexto w200" nombreCatalogo="vnmoneda" 
					idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" 
					readonly="true"/>
			</td>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_modeloVehiculo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_modeloVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>	  					
			</th>
			<td>
				<html:text property="id_modeloVehiculo" disabled="true"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>	
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorNuevo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorNuevo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="valorNuevo" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)" 
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorComercial" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorComercial" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="valorComercial" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)" 
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="claveCondRiesgo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.claveCondRiesgo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>
			</th>
			<td>
				<html:text property="claveCondRiesgo" maxlength="4"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/> 			
			</td>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorCaratula" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorCaratula" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="valorCaratula" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)" 
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
		</tr> 
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.modeloVehiculoForm,'/MidasWeb/catalogos/modelovehiculo/modificar.do', 'contenido','validaGuardarModificarM1()');">
							<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
		<tr>
			<td colspan="2">
				<midas:mensajeUsuario/>
			</td>
		</tr>
	</table>
</midas:formulario>
