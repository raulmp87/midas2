<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/modelovehiculo/borrar"> 
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" /> Modelo Veh&iacute;culo
				<midas:oculto propiedadFormulario="idCompuestoTcVehiculo" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="id_idMoneda" nombreFormulario="modeloVehiculoForm" />
				<midas:oculto propiedadFormulario="id_modeloVehiculo" nombreFormulario="modeloVehiculoForm" />
			</td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"  
					property="id_claveTipoBien" 
					name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td class="fondoCajaTexto">
				<midas:comboCatalogo propiedad="id_claveTipoBien" 
					size="1" readonly="true"
					styleId="id_claveTipoBien" styleClass="cajaTexto w200"
					nombreCatalogo="tctipobienautos" 
					idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'idTcTipoVehiculo')"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="idTcTipoVehiculo" 
					name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.tipoVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>	
			<td class="fondoCajaTexto">
				<midas:tipoVehiculo styleId="idTcTipoVehiculo" 
					readonly="true" size="1" 
					propiedad="idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto w200"
					onchange="getMarcaVehiculos(this,'idTcMarcaVehiculo')" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"  
					property="idTcMarcaVehiculo" 
					name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.marcaVehiculo" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td class="fondoCajaTexto">
				<midas:marcaVehiculo styleId="idTcMarcaVehiculo" size="1"  readonly="true"
					propiedad="idTcMarcaVehiculo"
					tipoVehiculo="idTcTipoVehiculo" styleClass="cajaTexto w200"
					onchange="getVersionCarga(this,'id_claveTipoBien','idTcTipoVehiculo','id_idVersionCarga');"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="id_idVersionCarga" name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td class="fondoCajaTexto">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto w200" readonly="true"
					tipoBien="id_claveTipoBien" 
					marcaVehiculo="idTcMarcaVehiculo" 
					tipoVehiculo="idTcTipoVehiculo"
					onchange="getEstiloVehiculos(this,'idTcTipoVehiculo','idTcMarcaVehiculo','comboEstiloVehiculo');"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>				
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="idCompuestoTcVehiculo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_estiloVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<midas:estiloVehiculo styleId="comboEstiloVehiculo" size="1" 
					propiedad="idCompuestoTcVehiculo" styleClass="cajaTexto w200"
					tipoVehiculo="idTcTipoVehiculo" readonly="true"
					marcaVehiculo="idTcMarcaVehiculo" 
					versionCarga="id_idVersionCarga" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="id_idMoneda" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_idMoneda" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td >
				<midas:comboCatalogo propiedad="id_idMoneda" size="1" 
					styleId="moneda" nombre="modeloVehiculoForm" 
					styleClass="cajaTexto w200" nombreCatalogo="vnmoneda" 
					idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" 
					readonly="true"/>
			</td>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="id_modeloVehiculo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_modeloVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td >
				<html:text property="id_modeloVehiculo" readonly="true"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="valorNuevo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorNuevo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:text property="valorNuevo" readonly="true"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="valorComercial" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorComercial" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:text property="valorComercial" readonly="true"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="claveCondRiesgo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.claveCondRiesgo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text property="claveCondRiesgo" readonly="true"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>			
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorCaratula" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorCaratula" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="valorCaratula" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)" 
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
		</tr>
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha" style="margin-right: 4px">
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.modeloVehiculoForm,'/MidasWeb/catalogos/modelovehiculo/borrar.do', 'contenido','validaBorrarM1()');"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>