<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/modelovehiculo/agregar"> 
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" /> Modelo Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveTipoBien" name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:comboCatalogo propiedad="id_claveTipoBien" size="1"
					styleId="id_claveTipoBien" styleClass="cajaTexto w200"
					nombreCatalogo="tctipobienautos" 
					idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'idTcTipoVehiculo')"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="idTcTipoVehiculo" 
					name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.tipoVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>	
			<td class="fondoCajaTexto">
				<midas:tipoVehiculo styleId="idTcTipoVehiculo" 
					size="1" propiedad="idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto w200"
					onchange="getMarcaVehiculos(this,'idTcMarcaVehiculo')" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si" 
					property="idTcMarcaVehiculo" name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.marcaVehiculo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:marcaVehiculo styleId="idTcMarcaVehiculo" size="1" 
					propiedad="idTcMarcaVehiculo"
					tipoVehiculo="idTcTipoVehiculo" styleClass="cajaTexto w200"
					onchange="getVersionCarga(this,'id_claveTipoBien','idTcTipoVehiculo','id_idVersionCarga');"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_idVersionCarga" name="modeloVehiculoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto w200" 
					tipoBien="id_claveTipoBien" 
					marcaVehiculo="idTcMarcaVehiculo" 
					tipoVehiculo="idTcTipoVehiculo"
					onchange="getEstiloVehiculos(this,'idTcTipoVehiculo','idTcMarcaVehiculo','comboEstiloVehiculo');"
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="idCompuestoTcVehiculo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_estiloVehiculo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td >
				<midas:estiloVehiculo styleId="comboEstiloVehiculo" size="1" 
					propiedad="idCompuestoTcVehiculo" styleClass="cajaTexto w200"
					tipoVehiculo="idTcTipoVehiculo" 
					marcaVehiculo="idTcMarcaVehiculo" 
					versionCarga="id_idVersionCarga" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_idMoneda" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_idMoneda" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<midas:comboCatalogo propiedad="id_idMoneda" size="1" 
					styleId="moneda" nombre="modeloVehiculoForm" 
					styleClass="cajaTexto w200" nombreCatalogo="vnmoneda" 
					idCatalogo="idTcMoneda" descripcionCatalogo="descripcion" 
					readonly="false"/>	
			</td>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_modeloVehiculo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.id_modeloVehiculo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:text property="id_modeloVehiculo" maxlength="4"
					onkeypress="return soloNumeros(this, event, false)"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>	
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorNuevo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorNuevo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>	
			</th>
			<td>
				<html:text property="valorNuevo" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>	
			</td>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorComercial" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorComercial" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="valorComercial" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>	
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="claveCondRiesgo" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.claveCondRiesgo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td>	
				<html:text property="claveCondRiesgo" maxlength="4"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/> 	
			</td>
			<th>
				<etiquetas:etiquetaError requerido="no"
					property="valorCaratula" name="modeloVehiculoForm"
					key="catalogos.modelovehiculo.valorCaratula" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="valorCaratula" maxlength="8"
					onkeypress="return soloNumeros(this, event, false)" 
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200"
				/>
			</td>
		</tr>
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.modeloVehiculoForm,'/MidasWeb/catalogos/modelovehiculo/agregar.do', 'contenido','validaGuardarModificarM1()');"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
		<tr>
			<td colspan="4">
				<midas:mensajeUsuario/>
			</td>
		</tr>
	</table>
	
	<html:hidden property="mensaje" styleId="mensaje" name="modeloVehiculoForm"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje" name="modeloVehiculoForm"/>
</midas:formulario>
<div id="errores" style="display: none;"><html:errors/></div>	
