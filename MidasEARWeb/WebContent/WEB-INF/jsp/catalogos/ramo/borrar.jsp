<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/ramo/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<midas:oculto propiedadFormulario="idRamo"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ramo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigo" nombre="ramoForm"/></td>
			
			<th><midas:mensaje clave="catalogos.ramo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcion" nombre="ramoForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.ramoForm,'/MidasWeb/catalogos/ramo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.ramoForm,'/MidasWeb/catalogos/ramo/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>