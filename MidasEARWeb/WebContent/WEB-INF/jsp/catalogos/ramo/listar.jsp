<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/ramo/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.ramo.codigo"/>:</th>
			<td><midas:texto propiedadFormulario="codigo" caracteres="22" onkeypress="return soloNumeros(this, event, false)"/> </td>
			<th><midas:mensaje clave="catalogos.ramo.descripcion"/>:</th>
			<td colspan="2"><midas:texto propiedadFormulario="descripcion" caracteres="200" /> </td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.ramoForm,'/MidasWeb/catalogos/ramo/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="ramosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Ramo"
			claseCss="tablaConResultados" nombreLista="ramos"
			urlAccion="/catalogos/ramo/listar.do">
			<midas:columna propiedad="codigo" titulo="Codigo"/>
			<midas:columna propiedad="descripcion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/ramo/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
