<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/ramo/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<midas:oculto propiedadFormulario="idRamo"/>
			</td>
		</tr>
		<tr>
			<td> <etiquetas:etiquetaError requerido="si" property="codigo" name="ramoForm" key="catalogos.ramo.codigo" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td> <midas:texto  onkeypress="return soloNumeros(this, event, false)" caracteres="200" 
				propiedadFormulario="codigo"/></td>
			<td> <etiquetas:etiquetaError requerido="si" property="descripcion" name="ramoForm" key="catalogos.ramo.descripcion" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif"/></td>
			<td colspan="2"> <midas:texto caracteres="200" 
				propiedadFormulario="descripcion"/></td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/ramo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.ramoForm,'/MidasWeb/catalogos/ramo/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
