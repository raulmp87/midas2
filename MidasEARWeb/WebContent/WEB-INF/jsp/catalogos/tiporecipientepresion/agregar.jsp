<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tiporecipientepresion/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="codigoTipoRecPresion" requerido="si"
					key="catalogos.tiporecipientepresion.codigoTipoRecPresion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="codigoTipoRecPresion"
				onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="descripcionTipoRecPresion" requerido="si"
					key="catalogos.tiporecipientepresion.descripcionTipoRecPresion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionTipoRecPresion"
				caracteres="200"/>
			</td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tiporecipientepresion/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoRecipientePresionForm,'/MidasWeb/catalogos/tiporecipientepresion/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
