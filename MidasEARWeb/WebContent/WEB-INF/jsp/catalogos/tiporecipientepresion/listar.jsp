<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/tiporecipientepresion/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th colspan="2"><midas:mensaje clave="catalogos.tiporecipientepresion.codigoTipoRecPresion"/>:</th>
			<td><midas:texto propiedadFormulario="codigoTipoRecPresion" onkeypress="return soloNumeros(this, event, false)"/></td>
			<th><midas:mensaje clave="catalogos.tiporecipientepresion.descripcionTipoRecPresion"/>:</th>
			<td><midas:texto propiedadFormulario="descripcionTipoRecPresion" caracteres="200"/></td>	
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoRecipientePresionForm,'/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do','contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="ramosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoRecipientePresion"
			claseCss="tablaConResultados" nombreLista="tipoRecipientePresion"
			urlAccion="/catalogos/tiporecipientepresion/listar.do">
				<midas:columna propiedad="codigoTipoRecPresion" titulo="C&oacute;digo Tipo"/>
				<midas:columna propiedad="descripcionTipoRecPresion" titulo="Descripci&oacute;n" estilo="descripcion"/>
			<midas:columna propiedad="acciones" titulo=""/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tiporecipientepresion/mostrarAgregar.do','contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>