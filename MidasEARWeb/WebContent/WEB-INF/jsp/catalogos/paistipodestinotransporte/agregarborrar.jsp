<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


 <midas:formulario accion="/catalogos/paistipodestinotransporte/agregar">
	<table id="asociar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.paistipodestinotransporte.tipoDestinoTransporte" /></th>
			<td colspan="2"><midas:comboCatalogo onchange="if (processor.getSyncState() == false){guardarAsociaciones();} if(getElementById('idTipoDestinoTransporte').selectedIndex==0){porAsociar.destructor();asociados.destructor();}
				else{mostrarGridsPaisTipoDestinoTransporte(getElementById('idTipoDestinoTransporte').value);}" propiedad="idTipoDestinoTransporte" styleId="idTipoDestinoTransporte"  size="1" styleClass="cajaTexto" nombreCatalogo="tctipodestinotransporte" idCatalogo="idTipoDestinoTransporte" descripcionCatalogo="descripcionTipoDestinoTransporte" />
			</td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="catalogos.paistipodestinotransporte.paisesAsociados" />
			</td>
			<td width="100px" align="center">&nbsp;
			</td>
			<td>
				<midas:mensaje clave="catalogos.paistipodestinotransporte.paisesNoAsociados" />
			</td>
		</tr>
		<tr>
			<td width="45%">
				<div id="paisesAsociadosGrid"  width="95%" height="250px" style="background-color:white;overflow:hidden;width:100%"></div>
			</td>
			<td width="10%" align="center"></td>
			<td width="45%">
				<div id="paisesPorAsociarGrid" width="95%" height="250px" style="background-color:white;overflow:hidden;width:100%"></div>
			</td>
		</tr>
		<tr>
			<td class="guardar" colspan="3">
				<div class="alinearBotonALaDerecha" width="95%">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/paistipodestinotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a id="linkGuardar" href="javascript: void(0);" onclick="javascript: if (processor.getSyncState() == false && getElementById('idTipoDestinoTransporte').selectedIndex > 0){guardarAsociaciones(); if(!error){ alert('Los datos se han guardado correctamente');}}"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
