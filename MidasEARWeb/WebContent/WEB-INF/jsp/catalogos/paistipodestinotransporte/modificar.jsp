<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/paistipodestinotransporte/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<midas:oculto propiedadFormulario="idPais"/>
				<midas:oculto propiedadFormulario="idTipoDestinoTransporte"/>
			</td>
		</tr>
		<tr>
			<td><etiquetas:etiquetaError requerido="si" property="idPais" name="paisTipoDestinoTransporteForm"
					key="catalogos.paistipodestinotransporte.pais" normalClass="normal"	errorClass="error" 
					errorImage="/img/information.gif"/>
				<midas:escribe propiedad="countryName" nombre="paisTipoDestinoTransporteForm"/>
			</td>
		</tr>
		<tr>
			<td><etiquetas:etiquetaError requerido="si" property="idTipoDestinoTransporte" name="paisTipoDestinoTransporteForm"
					key="catalogos.paistipodestinotransporte.tipoDestinoTransporte" normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" />
				<midas:comboCatalogo propiedad="idTipoDestinoTransporte" styleId="idTipoDestinoTransporte" size="1" styleClass="cajaTexto" nombreCatalogo="tctipodestinotransporte" idCatalogo="idTipoDestinoTransporte" descripcionCatalogo="descripcionTipoDestinoTransporte" />
			</td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/paistipodestinotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.paisTipoDestinoTransporteForm,'/MidasWeb/catalogos/paistipodestinotransporte/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>
