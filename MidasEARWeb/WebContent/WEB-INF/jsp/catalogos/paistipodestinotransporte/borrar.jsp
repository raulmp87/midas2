<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/paistipodestinotransporte/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<midas:oculto propiedadFormulario="idPais"/>
				<midas:oculto propiedadFormulario="idTipoDestinoTransporte"/>
			</td>
		</tr> 
		<tr>
			<th> <midas:mensaje clave="catalogos.paistipodestinotransporte.pais"/></th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="countryName" nombre="paisTipoDestinoTransporteForm"/></td>
			
			<th> <midas:mensaje clave="catalogos.tipodestinotransporte.codigo"/></th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoTipoDestinoTransporte" nombre="paisTipoDestinoTransporteForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipodestinotransporte.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoDestinoTransporte" nombre="paisTipoDestinoTransporteForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.paisTipoDestinoTransporteForm,'/MidasWeb/catalogos/paistipodestinotransporte/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.paisTipoDestinoTransporteForm,'/MidasWeb/catalogos/paistipodestinotransporte/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>