<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/paistipodestinotransporte/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
		<th><midas:mensaje clave="catalogos.paistipodestinotransporte.idPais"/>:</th>
			<td><midas:texto propiedadFormulario="idPais" caracteres="6" onkeypress="return soloLetras(this, event, false)" /> </td>
			
			<th><midas:mensaje clave="catalogos.paistipodestinotransporte.pais"/>:</th>
			<td><midas:texto propiedadFormulario="countryName" caracteres="6" onkeypress="return soloLetras(this, event, false)" /> </td>
			
			<th><midas:mensaje clave="catalogos.tipodestinotransporte.codigo"/>:</th>
			<td><midas:texto propiedadFormulario="codigoTipoDestinoTransporte" caracteres="22" onkeypress="return soloNumeros(this, event, false)"/> </td>
			
			<th><midas:mensaje clave="catalogos.tipodestinotransporte.descripcion"/>:</th>
			<td><midas:texto propiedadFormulario="descripcionTipoDestinoTransporte" caracteres="22" onkeypress="return soloLetras(this, event, false)"/> </td>			
		</tr>
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.paisTipoDestinoTransporteForm,'/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="paisTipoDestinoTransporteTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.PaisTipoDestinoTransporte"
			claseCss="tablaConResultados" nombreLista="paisTipoDestinoTransporteList"
			urlAccion="/catalogos/paistipodestinotransporte/listar.do">
			<midas:columna propiedad="pais.countryId" titulo="Codigo Pa&iacute;s"/>
			<midas:columna propiedad="pais.countryName" titulo="Pa&iacute;s"/>
			<midas:columna propiedad="tipoDestinoTransporte.codigoTipoDestinoTransporte" titulo="C&oacute;digo Tipo Destino Transporte"/>
			<midas:columna propiedad="tipoDestinoTransporte.descripcionTipoDestinoTransporte" titulo="Descripci&oacute;n Tipo Destino Transporte"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha"> 
				<div id="b_agregar">
					<a href="javascript: void(0);"
					onclick="javascript: initPaisPorTipoDestinoTransporteGrids();
					sendRequest(document.paisTipoDestinoTransporteForm,'/MidasWeb/catalogos/paistipodestinotransporte/agregarborrar.do', 'contenido');">
						<midas:mensaje clave="midas.accion.asociarpaisesydestinos" />
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
