<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/esquemasreas/modificar">
	<midas:oculto propiedadFormulario="idContrato" />
	<midas:oculto propiedadFormulario="cer" />
	<midas:oculto propiedadFormulario="anio" />
	<midas:oculto propiedadFormulario="nivel" />
	<midas:oculto propiedadFormulario="ordenEntrada" />
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.esquemasreas.modificar" /> 
				<midas:escribe nombre="esquemasReasForm" propiedad="cer"/>				
			</td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="anio" requerido="si"
					key="catalogos.esquemasreas.anio" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:escribe propiedad="anio" nombre="esquemasReasForm"
					 />
			</td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="cer" requerido="si"
					key="catalogos.esquemasreas.cer" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:escribe propiedad="cer" nombre="esquemasReasForm" />
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="nivel" requerido="si"
					key="catalogos.esquemasreas.nivel" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:escribe propiedad="nivel" nombre="esquemasReasForm" />
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="ordenEntrada" requerido="si"
					key="catalogos.esquemasreas.ordenEntrada" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:escribe propiedad="ordenEntrada" nombre="esquemasReasForm" />
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="reinstalaciones" requerido="si"
					key="catalogos.esquemasreas.reinst" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="reinstalaciones" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td colspan="2"></td>
		</tr>
		
		<tr>
			<td class="guardar" colspan="4" >
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/esquemasreas/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div> 
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.esquemasReasForm,'/MidasWeb/catalogos/esquemasreas/modificar.do', 'contenido','null');">
							<midas:mensaje clave="midas.accion.guardar" />
						</a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
	
</midas:formulario>

