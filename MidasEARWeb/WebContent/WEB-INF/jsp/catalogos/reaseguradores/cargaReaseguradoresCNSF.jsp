<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<script type="text/javascript">
	var mostrarPath = '<s:url action="mostrar" namespace="/vida/cargaArchivos"/>';
	var obtenerPlantillaPath = '<s:url action="obtenerPlantilla" namespace="/vida/cargaArchivos"/>';
	var procesarInfoPath = '';
</script>
<div id="detalle" name="Detalle">
	<div class="subtituloIzquierdaDiv">
		Carga Masiva Reaseguradoras CNSF
	</div>
	<center>
			<table id="filtros" width="100%">
				<tr>
					<th>Negocio</th>
					<td>
						<select id="cve_negocio" class="cajaTexto">
							<option value="1">Autos</option>
							<option value="2">Da&ntilde;os</option>
							<option value="4">Vida</option>
						</select> 
					</td> 
					<td>
						<div class="alinearBotonALaDerecha">
							
						</div>
					</td>
				</tr>
				<tr>
					<th>Fecha Corte</th>
					<td>
						<input type="text" id="fechaInicial" size="10" onblur="esFechaValida(this);"  class="cajaTexto" />
						<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioRCS()">
							<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /></a>
					</td> 
					<td colspan="3">
						<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
							<div id="calendarioIzq"></div>
							<div id="fechaFinal"></div>
						</div>
					</td>
					<td>
					</td>
				</tr> 
				<tr>
					<td></td>
					<td colspan="2">
						<div style="float: left;margin-left: 2%;">
							<midas:boton onclick="javascript: AdjuntarArchivoReaseguradoresCNSFWin('20');" tipo="agregar" texto="Cargar Esquemas"/>
						</div>	
						<div style="float: left;margin-left: 5%;">
							<midas:boton onclick="javascript: AdjuntarArchivoREASWindow('10');" tipo="agregar" texto="Actualizar Calificacion"/>
						</div>	
						<div style="float: left;margin-left: 5%;">
							<midas:boton onclick="javascript: eliminarContratos();" tipo="agregar" texto="Eliminar Contratos"/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<div id="resultadosDocumentos"></div>
					</td>
				</tr>
			</table>
	</center>
</div>