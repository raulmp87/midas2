<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/reaseguradoresCNSF/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.contacto.calificacionescnsf.guardar" />
			</td>
		</tr>		
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="nombreReas" requerido="si"
					key="catalogos.calificacionescnsf.nombreReas" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="nombreReas" />
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			 <td width="25%">
				<etiquetas:etiquetaError property="idagencia" requerido="si"
					key="catalogos.calificacionescnsf.entidadCal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			
			</td>
			<!-- 
			<td width="25%">
				<midas:comboCatalogo propiedad="idAgencia" styleId="idAgencia"  size="1" styleClass="cajaTexto" nombreCatalogo="trAgencia" idCatalogo="idagencia" descripcionCatalogo="nombreAgencia" />
			</td>
			 -->
			<td width="25%">
			<midas:agenciaCalificacion styleId="agenciaCalificacion" size="1" propiedad="agenciaCal"
					 styleClass="cajaTexto" 
					onchange="getCalificaciones(this,'calificacion')" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="calificacion" requerido="si"
					key="catalogos.calificacionescnsf.calificacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			
			</td>
			<td width="22%">
				<midas:calificacionCNSF styleId="calificacion" size="1" propiedad="calificacion"
					 styleClass="cajaTexto" agenciaCalificacion="agenciaCalificacion"/>						
			</td>	
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="cveReas" requerido="si"
					key="catalogos.calificacionescnsf.cvereas" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="cveReas"
					 />
			</td>
			
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError property="cveReasAnt" requerido="si"
					key="catalogos.calificacionescnsf.cvereasant" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td width="25%">
				<midas:texto propiedadFormulario="cveReasAnt" deshabilitado="true"/> 
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td class="guardar" colspan="4" >
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradoresCNSF/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.reaseguradoresCNSFForm,'/MidasWeb/catalogos/reaseguradoresCNSF/agregar.do', 'contenido','notificacionResultadoOperacion()');">
							<midas:mensaje clave="midas.contacto.accion.guardar"/>
						</a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>