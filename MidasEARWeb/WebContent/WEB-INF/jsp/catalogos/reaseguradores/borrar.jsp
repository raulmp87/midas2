<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/reaseguradoresCNSF/borrar">
	<midas:oculto propiedadFormulario="idReasegurador"/>    
		<table id="agregar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.contacto.accion.borrar" />
					</td>
				</tr>
				<tr>
					<td width="25%">
						<etiquetas:etiquetaError property="nombreReas" requerido="si"
							key="catalogos.calificacionescnsf.nombreReas" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:texto propiedadFormulario="nombreReas" deshabilitado="true"/>
					</td>
					<td colspan="2"></td>
				</tr>
	 			<tr>
					<td width="25%">
						<etiquetas:etiquetaError property="idagencia" requerido="si"
							key="catalogos.calificacionescnsf.entidadCal" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:agenciaCalificacion styleId="agenciaCalificacion" size="1" propiedad="agenciaCalificacion"
						 styleClass="cajaTexto" 
						onchange="getCalificaciones(this,'calificacion')" />
					</td>
					<td width="25%">
						<etiquetas:etiquetaError property="calificacion" requerido="si"
							key="catalogos.calificacionescnsf.calificacion" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:calificacionCNSF styleId="calificacion" size="1" propiedad="calificacion"
					 	styleClass="cajaTexto" agenciaCalificacion="agenciaCalificacion"/>
					</td>
				</tr>
				
				<tr>
					<td width="25%">
						<etiquetas:etiquetaError property="cveReas" requerido="si"
							key="catalogos.calificacionescnsf.cvereas" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:texto propiedadFormulario="cveReas" onkeypress="return soloNumeros(this, event, false)" deshabilitado="true"/>
					</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td width="25%">
						<etiquetas:etiquetaError property="cveReasAnt" requerido="si"
							key="catalogos.calificacionescnsf.cvereasant" normalClass="normal"
							errorClass="error" errorImage="/img/information.gif" />
					</td>
					<td width="25%">
						<midas:texto propiedadFormulario="cveReasAnt" onkeypress="return soloNumeros(this, event, false)" deshabilitado="true"/>
					</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td class="guardar" colspan="4" >
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar"> 
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradoresCNSF/listar.do', 'contenido',null);">
									<midas:mensaje clave="midas.accion.regresar"/>
								</a>
							</div>
							<div id="b_guardar">
								<a href="javascript: void(0);"
									 								
									onclick="javascript: Confirma('<midas:mensaje clave="comun.confirmacionborrar" />', document.reaseguradoresCNSFForm,'/MidasWeb/catalogos/reaseguradoresCNSF/borrar.do', 'contenido','notificacionResultadoOperacionModificado()');">
									
									<midas:mensaje clave="midas.accion.borrar"/>
								</a>
							</div>
						</div>
					</td> 
				</tr>	  				
		</table>
    </midas:formulario>