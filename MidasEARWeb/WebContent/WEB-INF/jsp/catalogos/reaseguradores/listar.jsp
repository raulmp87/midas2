<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<midas:formulario  accion="/catalogos/reaseguradoresCNSF/listar">
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	<table width="95%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.calificacionescnsf.listaCal"/><!-- catalogos.calificaciones.listaCa -->
				<hr>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.calificacionescnsf.entidadCal"/></th>
			<td>
				<midas:agenciaCalificacion styleId="agenciaCalificacion" size="1" propiedad="agenciaCalificacion"
					 styleClass="cajaTexto" 
					onchange="getCalificaciones(this,'calificacion')" />
			</td>
			<th><midas:mensaje clave="catalogos.calificacionescnsf.calificacion"/></th>
			<td>
				<midas:calificacionCNSF styleId="calificacion" size="1" propiedad="calificacion"
					 styleClass="cajaTexto" agenciaCalificacion="agenciaCalificacion"/>
			</td>
			<th><midas:mensaje clave="catalogos.calificacionescnsf.cvereas"/></th>
			<td><midas:texto propiedadFormulario="cveReas" onkeypress="return stopRKey(event)"/></td>
			 
		</tr> 
		<tr>
			<td colspan="5">&nbsp;</td>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);" onclick='javascript: sendRequest(document.reaseguradoresCNSFForm,"/MidasWeb/catalogos/reaseguradoresCNSF/listarFiltrado.do", "contenido","getCalificaciones(document.getElementById(\"agenciaCalificacion\"),\"calificacion\")");'>
							<midas:mensaje clave="midas.contacto.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
	</table>
	<div id="mensajeResultadoOperacion" class="notificacionResultadoOperacion" style="display:none;margin-top:15px;margin-bottom:15px;width:95%"></div>
	<br clear="all" />
	<div id="resultados" style="width:95%">
		<midas:tabla idTabla="contactoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ReaseguradoresCNSF"
			claseCss="tablaConResultados" nombreLista="contactos"
			urlAccion="/catalogos/reaseguradoresCNSF/listarFiltrado.do">
		
			<midas:columna propiedad="nombreReas" titulo="Nombre Reaseguradora" />
			<midas:columna propiedad="claveCnsf" titulo="Clave CNSF"/>
			<midas:columna propiedad="claveCnsfAnt"/>
			<midas:columna propiedad="agenciaCalificadoraDTO.nombreAgencia" titulo="Agencia"/>
			<midas:columna propiedad="calificacionAgenciaDTO.calificacion" titulo="Calificación"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla> 
	 	
				
		<div class="alinearBotonALaDerecha">
			<div id="b_agregar">
				<a href="javascript: void(0);"
					onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradoresCNSF/mostrarAgregar.do', 'contenido',null);">
					<midas:mensaje clave="midas.accion.agregar"/>
				</a>
			</div>
			<div id="b_agregar" style="padding-left: 8px;">
				<a href="javascript: void(0);"
					onclick="javascript: imprimirBitacora(document.reporteRCSForm);">
					<midas:mensaje clave="catalogos.calificacionescnsf.bitacora"/>
				</a>
			</div>
		</div>
	</div>
	<midas:mensajeUsuario/>
</midas:formulario>
