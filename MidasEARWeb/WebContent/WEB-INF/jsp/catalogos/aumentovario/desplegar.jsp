<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/aumentovario/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="aumentoVarioForm" propiedad="descripcionaumento"/>'
					</td>
				</tr>
	 			<tr>
	 				<th><midas:mensaje clave="catalogos.aumentovario.claveTipoAumento" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionClave" nombre="aumentoVarioForm"/></td>
					<th><midas:mensaje clave="catalogos.aumentovario.descripcionAumento" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionaumento" nombre="aumentoVarioForm"/></td>
				</tr>	 			 	 			 	 			 	 			 					
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/aumentovario/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
								<html:hidden property="idtcaumentovario" name="aumentoVarioForm"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>   	  				
		  	</table>
	  	</div>
    </midas:formulario>
