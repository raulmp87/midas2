<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario accion="/catalogos/aumentovario/listar">
	
	<table width="90%" id="filtros">
		<tr>
		<midas:oculto nombreFormulario="aumentoVarioForm" propiedadFormulario="idtcaumentovario"/>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.aumentovario.filtro.claveTipoAumento"/>:</th>
			<td>
				<midas:combo propiedad="clavetipoaumento">
					<midas:opcionCombo valor="1">PORCENTAJE</midas:opcionCombo>
					<midas:opcionCombo valor="2">IMPORTE</midas:opcionCombo>
				</midas:combo>
			</td>			
			<th><midas:mensaje clave="catalogos.aumentovario.filtro.descripcionAumento"/>:</th>
			<td>
				<midas:texto propiedadFormulario="descripcionaumento" caracteres="200"/>
				<input type="text" id="hiddenPatch" style="display: none;"/>
			</td>
						
		</tr> 
		<tr>
			<td>
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.aumentoVarioForm,'/MidasWeb/catalogos/aumentovario/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="aumentoVarioTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.AumentoVario"
			claseCss="tablaConResultados" nombreLista="aumentoVarios"
			urlAccion="/catalogos/aumentovario/listar.do">
			<midas:columna propiedad="claveTipoAumento" titulo="Id de Tipo de Aumento"/>
			<midas:columna propiedad="descripcionAumento" titulo="Descripci&oacute;n"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />			
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/aumentovario/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
