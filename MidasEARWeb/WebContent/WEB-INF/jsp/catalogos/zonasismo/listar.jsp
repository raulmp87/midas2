<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/zonasismo/listar">
	<midas:oculto nombreFormulario="zonaSismoForm" propiedadFormulario="idZonaSismo"/>
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonasismo.codigoZonaSismo"/>:</th>
			<td><midas:texto propiedadFormulario="codigoZonaSismo" onkeypress="return soloNumeros(this, event, false)" /></td>			
			<th><midas:mensaje clave="catalogos.zonasismo.descripcionZonaSismo"/>:</th>
			<td><midas:texto  propiedadFormulario="descripcionZonaSismo" caracteres="200"/></td>		
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.zonaSismoForm,'/MidasWeb/catalogos/zonasismo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="zonaSismoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ZonaSismo"
			claseCss="tablaConResultados" nombreLista="zonaSismo"
			urlAccion="/catalogos/zonasismo/listar.do">
			<midas:columna propiedad="codigoZonaSismo" titulo="C&oacute;digo de Zona Sismo" estilo="codigoZonaSismo"/>
			<midas:columna propiedad="descripcionZonaSismo" titulo="Descripci&oacute;n" estilo="descripcion"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />			
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>