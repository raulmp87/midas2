<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/zonasismo/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="zonaSismoForm" propiedad="descripcionZonaSismo"/>'
						<html:hidden property="idZonaSismo" name="zonaSismoForm"/>
					</td>
				</tr>
	 			<tr>
	 				<th><midas:mensaje clave="catalogos.zonasismo.codigoZonaSismo" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="codigoZonaSismo" nombre="zonaSismoForm"/></td>
					<th><midas:mensaje clave="catalogos.zonasismo.descripcionZonaSismo" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionZonaSismo" nombre="zonaSismoForm"/></td>
				</tr>	
				<tr>
	 				<th><midas:mensaje clave="catalogos.zonasismo.valorDefaultCoaseguro" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultCoaseguro" nombre="zonaSismoForm"/></td>
					<th><midas:mensaje clave="catalogos.zonasismo.valorDefaultCoaseguroROCH" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultCoaseguroROCH" nombre="zonaSismoForm"/></td>
				</tr>
				<tr>
	 				<th><midas:mensaje clave="catalogos.zonasismo.valorDefaultDeducible" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultDeducible" nombre="zonaSismoForm"/></td>
					<th><midas:mensaje clave="catalogos.zonasismo.valorDefaultDeducibleROCH" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="valorDefaultDeducibleROCH" nombre="zonaSismoForm"/></td>
				</tr> 			 	 			 	 			 	 			 					
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
							</div>
						</div>
					</td>
				</tr>   	  				
		  	</table>
	  	</div>
    </midas:formulario>