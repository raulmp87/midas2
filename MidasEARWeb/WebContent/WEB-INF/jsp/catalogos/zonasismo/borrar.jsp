<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/zonasismo/borrar">
	<table id="desplegar">		
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="zonaSismoForm" propiedad="descripcionZonaSismo" />'
				<html:hidden property="idZonaSismo" name="zonaSismoForm"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonasismo.codigoZonaSismo" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoZonaSismo" nombre="zonaSismoForm"/></td>
			<th><midas:mensaje clave="catalogos.zonasismo.descripcionZonaSismo" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionZonaSismo" nombre="zonaSismoForm"/></td>					
		</tr> 	
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>						
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);"											
							onclick="javascript: Confirma('<midas:mensaje clave="comun.confirmacionborrar" />', document.zonaSismoForm,'/MidasWeb/catalogos/zonasismo/borrar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.borrar" /> </a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>