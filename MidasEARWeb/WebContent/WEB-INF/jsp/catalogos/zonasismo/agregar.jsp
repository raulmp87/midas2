<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/zonasismo/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />			
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="codigoZonaSismo" requerido="si"
					key="catalogos.zonasismo.codigoZonaSismo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="codigoZonaSismo" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="descripcionZonaSismo" requerido="si"
					key="catalogos.zonasismo.descripcionZonaSismo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionZonaSismo"
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="200"/>
			</td>
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError property="valorDefaultCoaseguro" requerido="si"
					key="catalogos.zonasismo.valorDefaultCoaseguro" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultCoaseguro" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="valorDefaultCoaseguroROCH" requerido="si"
					key="catalogos.zonasismo.valorDefaultCoaseguroROCH" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultCoaseguroROCH" onkeypress="return soloNumeros(this, event, false)" />
			</td>
		</tr>
		
		<tr>
			<td>
				<etiquetas:etiquetaError property="valorDefaultDeducible" requerido="si"
					key="catalogos.zonasismo.valorDefaultDeducible" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultDeducible" onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="valorDefaultDeducibleROCH" requerido="si"
					key="catalogos.zonasismo.valorDefaultDeducibleROCH" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="valorDefaultDeducibleROCH" onkeypress="return soloNumeros(this, event, false)" />
			</td>
		</tr>	
		
		
		
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaSismoForm,'/MidasWeb/catalogos/zonasismo/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>		
	</table>
</midas:formulario>