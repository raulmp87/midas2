<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/subtiporecipientepresion/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th colspan="2"><midas:mensaje clave="catalogos.subtiporecipientepresion.codigoSubTipoRecPresion"/>:</th> 
			<td><midas:texto propiedadFormulario="codigoSubTipoRecPresion"onkeypress="return soloNumero(this, event, false)"/></td>
			<th><midas:mensaje clave="catalogos.subtiporecipientepresion.descripcionSubTipoRecPresion"/>:</th> 
			<td><midas:texto propiedadFormulario="descripcionSubTipoRecPresion" caracteres="200"/></td>			
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subTipoRecPresionForm,'/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="subTipoRecipientePresionTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.SubTipoRecPresion"
			claseCss="tablaConResultados" nombreLista="subTipoRecipientePresion"
			urlAccion="/catalogos/subtiporecipientepresion/listar.do">
			<midas:columna propiedad="codigoSubtipoRecipientePresion" titulo="C&oacute;digo Subtipo"/>
			<midas:columna propiedad="descripcionSubtipoRecPresion" titulo="Descripci&oacute;n" estilo="descripcion"/>
			<midas:columna propiedad="acciones" titulo=""/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subtiporecipientepresion/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
