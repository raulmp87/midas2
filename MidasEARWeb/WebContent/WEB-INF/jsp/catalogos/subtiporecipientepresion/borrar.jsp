<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subtiporecipientepresion/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<html:hidden property="idSubTipoRecipientePresion" name="subTipoRecPresionForm"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.subtiporecipientepresion.codigoSubTipoRecPresion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoSubTipoRecPresion"  nombre="subTipoRecPresionForm"/></td>
			<th><midas:mensaje clave="catalogos.subtiporecipientepresion.descripcionSubTipoRecPresion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionSubTipoRecPresion" nombre="subTipoRecPresionForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subTipoRecPresionForm,'/MidasWeb/catalogos/subtiporecipientepresion/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.subTipoRecPresionForm,'/MidasWeb/catalogos/subtiporecipientepresion/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>