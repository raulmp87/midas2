<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subtiporecipientepresion/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="tipoRecipientePresionDTO" requerido="si"
					key="catalogos.subtiporecipientepresion.tipoRecipientePresionDTO" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:comboCatalogo propiedad="tipoRecipientePresionDTO" styleClass="cajaTexto" size="1"
					nombreCatalogo="tctiporecipientepresion" idCatalogo="idTipoRecipientePresion"
					descripcionCatalogo="descripcionTipoRecPresion" styleId="tipoRecipientePresionDTO"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="codigoSubTipoRecPresion" requerido="si"
					key="catalogos.subtiporecipientepresion.codigoSubTipoRecPresion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="codigoSubTipoRecPresion" onkeypress="return soloNumeros(this, event, false)"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="descripcionSubTipoRecPresion" requerido="si"
					key="catalogos.subtiporecipientepresion.descripcionSubTipoRecPresion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionSubTipoRecPresion" caracteres="200"/>
			</td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subtiporecipientepresion/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subTipoRecPresionForm,'/MidasWeb/catalogos/subtiporecipientepresion/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
