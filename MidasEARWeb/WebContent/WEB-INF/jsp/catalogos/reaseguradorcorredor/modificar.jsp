<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
		
 <midas:formulario accion="/catalogos/reaseguradorcorredor/modificar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="catalogo.reaseguradorcorredor.tituloModificar" />
				<html:hidden property="idTcReaseguradorCorredor" name="reaseguradorCorredorForm"/>
			</td>
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError requerido="si"
					property="nombre" name="reaseguradorCorredorForm"
					key="catalogo.reaseguradorcorredor.nombre" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td width="22%">
				<midas:texto 
				onkeypress="return soloAlfanumericos(this, event, false)" caracteres="100" 
				propiedadFormulario="nombre"/>
			</td>
			
			<td width="2%"></td>
			<td width="25%">
				<etiquetas:etiquetaError requerido="no"
					property="rfc" name="reaseguradorCorredorForm"
					key="catalogo.reaseguradorcorredor.rfc" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />  					
			</td>
			<td width="22%">
				<midas:texto propiedadFormulario="rfc"
				onkeypress="return soloAlfanumericos(this, event, false)"
				caracteres="13"/>
				
			</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
				property="nombreCorto" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.nombreCorto" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="nombreCorto"
				onkeypress="return soloAlfanumericos(this, event, false)"
				caracteres="30"/>
			</td>
			<td width="2%"></td>
			<td width="25%">
				<etiquetas:etiquetaError requerido="no"
					property="cnfs" name="reaseguradorCorredorForm"
					key="catalogo.reaseguradorcorredor.cnfs" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td width="22%">
				<midas:texto propiedadFormulario="cnfs"
					onkeypress="return teclaEsIgualALaExpresionRegular(event, /[A-z0-9\-]/)"
					caracteres="50"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
					property="ubicacion" name="reaseguradorCorredorForm"
					key="catalogo.reaseguradorcorredor.ubicacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<html:textarea styleId="ubicacion" property="ubicacion"	rows="3"
					onkeypress="return limiteMaximoCaracteres(this.value, event, 250)" styleClass="cajaTexto"/>
			</td>
			<td width="2%"></td>
			<td>
				<etiquetas:etiquetaError requerido="si"
				property="pais" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.pais" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:pais styleId="pais" size="1" propiedad="pais"
				styleClass="cajaTexto"
				onchange="limpiarObjetos('estado,ciudad');getEstados(this,'estado')"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
				property="estado" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.estado" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />
			</td> 
			<td>
				<midas:estado styleId="estado" size="1" propiedad="estado"
					pais="pais" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudad'); getCiudades(this,'ciudad');" />
			</td>
			<td width="2%"></td>
			 <td>
				<etiquetas:etiquetaError requerido="no"
					property="ciudad" name="reaseguradorCorredorForm"
					key="catalogo.reaseguradorcorredor.ciudad" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />	
			</td> 
			<td>
				<midas:ciudad styleId="ciudad" size="1" propiedad="ciudad"
					estado="estado" styleClass="cajaTexto"
					onchange="getColonias(this,'colonia')" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
				property="telefonoFijo" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.telefonoFijo" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="telefonoFijo" 
				onkeypress="return soloNumeros(this, event, false)" caracteres="15"/>
			</td>
			<td width="2%"></td>
			<td>
				<etiquetas:etiquetaError
				property="telefonoMovil" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.telefonoMovil" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="telefonoMovil"
				onkeypress="return soloNumeros(this, event, false)" caracteres="15"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
				property="correoElectronico" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.correoElectronico" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="correoElectronico"
				caracteres="250"/>
			</td>
			<td width="2%"></td>
			<td>
				<etiquetas:etiquetaError requerido="si"
				property="procedencia" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.procedencia" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:combo propiedad="procedencia" styleClass="cajaTexto">
					<midas:opcionCombo valor="0">Nacional</midas:opcionCombo>
					<midas:opcionCombo valor="1">Extranjero</midas:opcionCombo>
				</midas:combo>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
				property="tipo" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.tipo" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:combo propiedad="tipo" styleClass="cajaTexto">
					<midas:opcionCombo valor="0">Corredor</midas:opcionCombo>
					<midas:opcionCombo valor="1">Reasegurador</midas:opcionCombo>
				</midas:combo>
			</td>
			<td width="2%"></td>
			<td>
			<etiquetas:etiquetaError requerido="si"
				property="estatus" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.estatus" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:combo propiedad="estatus" styleClass="cajaTexto">
					<midas:opcionCombo valor="1">Activo</midas:opcionCombo>
					<midas:opcionCombo valor="0">inactivo</midas:opcionCombo>
				</midas:combo>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no"
				property="idContable" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.idContable" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="idContable"
					onkeypress="return soloNumeros(this, event, false)"
					caracteres="50"/>
			</td>
			<td></td>
			<td>
				<etiquetas:etiquetaError requerido="no"
				property="calificacionCNSF" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.agenciaCalificadora" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td>
				<midas:agenciaCalificacion styleId="agenciaCalificacion" size="1" propiedad="agenciaCalificacion"
					 styleClass="cajaTexto" 
					onchange="getCalificaciones(this,'calificacionCNSF')" />
			</td>
			
		</tr>
		<tr>
			<td width="25%">
				<etiquetas:etiquetaError requerido="si"
				property="idTcImpuestoResidenciaFiscal" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.pctImpuestoResidenciaFiscal" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td width="22%">
				<html:select property="idTcImpuestoResidenciaFiscal" styleClass="cajaTexto">
					<html:option value="">SELECCIONE...</html:option>
					<html:optionsCollection name="impuestosResidenciaFiscal" />
				</html:select>
			</td>
			<td width="2%"></td>				
			<td width="25%">
				<etiquetas:etiquetaError requerido="no"
				property="tieneConstanciaResidenciaFiscal" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.calificacionAgencia" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td width="22%">
				<midas:calificacionCNSF styleId="calificacionCNSF" size="1" propiedad="calificacionCNSF"
					 styleClass="cajaTexto" agenciaCalificacion="agenciaCalificacion" 
					 />						
			</td>					
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
				property="fechaFinVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.vigenciaConstanciaResidenciaFiscal" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td width="22%">
					<table align="left" width="100%">
						<tr>
							<td>
								<html:text styleId="fechaInicial" property="fechaInicioVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto" maxlength="10" />
							</td>
							<td width="10px">
							 	-
							</td>
							<td>
								<html:text styleId="fechaFinal" property="fechaFinVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"  maxlength="10"/>
							</td>
							<td>
								<a href="javascript: void(0);" id="mostrarCalendario" onclick="mostrarCalendarioDoble()">
									<image src="/MidasWeb/img/icons/ico_calendario.gif" border=0 />
								</a>
								<div id="calendarioDoble" style="position:absolute;z-index: 1;"></div>
							</td>
						</tr>													
					</table>						
			</td>
			<td width="2%"></td>				
			<td width="25%">
				<etiquetas:etiquetaError requerido="no"
				property="tieneConstanciaResidenciaFiscal" name="reaseguradorCorredorForm"
				key="catalogo.reaseguradorcorredor.tieneConstanciaResidenciaFiscal" normalClass="normal"
				errorClass="error" errorImage="/img/information.gif" />		  					
			</td>
			<td width="22%">
				<midas:checkBox valorEstablecido="1" propiedadFormulario="tieneConstanciaResidenciaFiscal" />					
			</td>
		</tr>			
		</td>
		<td colspan="3"></td>			
		</tr>				
		<tr>
			<td class="campoRequerido" colspan="2">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="3">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.reaseguradorCorredorForm,'/MidasWeb/catalogos/reaseguradorcorredor/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.reaseguradorCorredorForm,'/MidasWeb/catalogos/reaseguradorcorredor/modificar.do', 'contenido','notificacionResultadoOperacion()');"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
		
