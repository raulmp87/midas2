<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/reaseguradorcorredor/borrar">
	<div id="centrarDesplegar">
	  	<table id="desplegar">
			<tr>
				<td class="titulo" colspan="5">
					<midas:mensaje clave="catalogo.reaseguradorcorredor.tituloDetalle" />
				</td>
			</tr>
			<tr>
 				<th width="10%"><midas:mensaje clave="catalogo.reaseguradorcorredor.nombre"/>:</th>
 				<td width="40%"><midas:texto propiedadFormulario="nombre" deshabilitado="true" /></td>
 				<td width="10%"></td>
				<th width="10%"><midas:mensaje clave="catalogo.reaseguradorcorredor.rfc"/>:</th>
				<td width="30%"><midas:texto propiedadFormulario="rfc" deshabilitado="true"/></td>
 			</tr> 
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.nombreCorto"/>:</th>
				<td><midas:texto propiedadFormulario="nombreCorto" deshabilitado="true"/></td>
				<td width="10%"></td>
				<th width="10%"><midas:mensaje clave="catalogo.reaseguradorcorredor.cnfs"/>:</th>
				<td width="30%"><midas:texto propiedadFormulario="cnfs" deshabilitado="true"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.ubicacion"/>:</th>
				<td><html:textarea styleId="ubicacion" property="ubicacion"	rows="3" disabled="true" styleClass="cajaTexto"/></td>
				<td width="10%"></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.pais"/>:</th>
				<td>	<midas:pais styleId="pais" readonly="true" size="1" propiedad="pais"
				styleClass="cajaTexto"
				onchange="limpiarObjetos('estado,ciudad');getEstados(this,'estado')"/>
				</td>
			</tr>
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.estado"/>:</th>
				<td class="fondoCajaTexto">		<midas:estado readonly="true" styleId="estado" size="1" propiedad="estado"
					pais="" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudad'); getCiudades(this,'ciudad');" />
				</td>
				<td width="10%"></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.ciudad" />:</th>
				<td>	
				<midas:ciudad styleId="ciudad" readonly="true" size="1" propiedad="ciudad"
					estado="estado" styleClass="cajaTexto"
					onchange="getColonias(this,'colonia')" />
		</td>
			</tr>				
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.telefonoFijo"/>:</th>
				<td><midas:texto propiedadFormulario="telefonoFijo" deshabilitado="true" /></td>
				<td width="10%"></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.telefonoMovil" />:</th>
				<td><midas:texto propiedadFormulario="telefonoMovil" deshabilitado="true"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.correoElectronico"/>:</th>
				<td><midas:texto propiedadFormulario="correoElectronico" deshabilitado="true" /></td>
				<td width="10%"></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.procedencia"/>:</th>
				<td><midas:combo deshabilitado="true" propiedad="procedencia" styleClass="cajaTexto">
						<midas:opcionCombo valor="0">Nacional</midas:opcionCombo>
						<midas:opcionCombo valor="1">Extranjero</midas:opcionCombo>
					</midas:combo>
				</td>
			</tr>
			
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.tipo"/>:</th>
				<td><midas:combo deshabilitado="true" propiedad="tipo" styleClass="cajaTexto">
					<midas:opcionCombo valor="0">Corredor</midas:opcionCombo>
					<midas:opcionCombo valor="1">Reasegurador</midas:opcionCombo>
				</midas:combo></td>
				<td width="10%"></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.estatus"/>:</th>
				<td><midas:combo deshabilitado="true" propiedad="estatus" styleClass="cajaTexto">
					<midas:opcionCombo valor="1">Activo</midas:opcionCombo>
					<midas:opcionCombo valor="0">inactivo</midas:opcionCombo>
				</midas:combo></td>
			</tr>
			
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.idContable"/>:</th>
				<td><midas:texto propiedadFormulario="idContable" deshabilitado="true" nombreFormulario="reaseguradorCorredorForm"/></td>
				<td width="10%"></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.agenciaCalificadora"/>:</th>
				<td>	<midas:agenciaCalificacion styleId="agenciaCalificacion" readonly="true" size="1" propiedad="agenciaCalificacion"
				styleClass="cajaTexto"
				onchange="getCalificaciones(this,'calificacionCNSF')"/>
				</td>
			</tr>				
			<tr>			
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.pctImpuestoResidenciaFiscal"/>:</th>						  								
				<td>
					<html:select property="idTcImpuestoResidenciaFiscal" styleClass="cajaTexto" disabled="true">
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection name="impuestosResidenciaFiscal" />
					</html:select>
				</td>
				<td></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.calificacionAgencia"/>:</th>
				<td>	<midas:calificacionCNSF styleId="calificacionCNSF" readonly="true" size="1" propiedad="calificacionCNSF"
				styleClass="cajaTexto" agenciaCalificacion="agenciaCalificacion"/>
				</td>					
			</tr>
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.vigenciaConstanciaResidenciaFiscal"/>:</th>			
				<td>
					<table align="left" width="100%">
						<tr>
							<td>
								<html:text styleId="fechaInicial" property="fechaInicioVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto" maxlength="10" disabled="true"/>
							</td>
							<td width="10px">
								-
							</td>
							<td>
								<html:text styleId="fechaFinal" property="fechaFinVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"  maxlength="10" disabled="true"/>
							</td>							
						</tr>													
					</table>						
				</td>
				<td></td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.tieneConstanciaResidenciaFiscal"/>:</th>			
				<td>
					<midas:checkBox valorEstablecido="1" propiedadFormulario="tieneConstanciaResidenciaFiscal" deshabilitado="true"/>					
				</td>
			</tr>			
		</td>
		<td colspan="3"></td>			
		</tr>							
			<tr>   	  				
				<td class="regresar" colspan="5">
					<div class="alinearBotonALaDerecha">
						<div id="b_regresar">
							<a href="javascript: void(0);" onclick="javascript: sendRequest(document.reaseguradorCorredorForm,'/MidasWeb/catalogos/reaseguradorcorredor/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<midas:mensajeUsuario/>
			</tr>   	  	
		</table>
	</div>
</midas:formulario>
		
				