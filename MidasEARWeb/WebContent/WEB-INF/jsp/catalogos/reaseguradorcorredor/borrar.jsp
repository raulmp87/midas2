<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/reaseguradorcorredor/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogo.reaseguradorcorredor.tituloBorrar" />
				<html:hidden property="idTcReaseguradorCorredor" name="reaseguradorCorredorForm"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.nombre"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="nombre" nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.rfc"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="rfc" nombre="reaseguradorCorredorForm"/></td>
			</tr> 
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.nombreCorto"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="nombreCorto"  nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.cnfs"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="cnfs" nombre="reaseguradorCorredorForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.ubicacion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="ubicacion" nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.pais"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="pais" nombre="reaseguradorCorredorForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.estado"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="estado" nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.ciudad" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="ciudad" nombre="reaseguradorCorredorForm"/></td>
		</tr>				
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.telefonoFijo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="telefonoFijo" nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.telefonoMovil" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="telefonoMovil" nombre="reaseguradorCorredorForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.correoElectronico"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="correoElectronico"  nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.procedencia"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="procedencia" nombre="reaseguradorCorredorForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.tipo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="tipo"  nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.estatus"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="estatus" nombre="reaseguradorCorredorForm"/></td>
		</tr>
		
		<tr>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.idContable"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idContable"  nombre="reaseguradorCorredorForm"/></td>
			<th><midas:mensaje clave="catalogo.reaseguradorcorredor.agenciaCalificadora"/>:</th>
			<td>	<midas:agenciaCalificacion styleId="agenciaCalificacion" readonly="true" size="1" propiedad="agenciaCalificacion"
			styleClass="cajaTexto"
			onchange="getCalificaciones(this,'calificacionCNSF')"/>
			</td>
		</tr>	
		<tr>			
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.pctImpuestoResidenciaFiscal"/>:</th>						  								
				<td>
					<html:select property="idTcImpuestoResidenciaFiscal" styleClass="cajaTexto" disabled="true">
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection name="impuestosResidenciaFiscal" />
					</html:select>
				</td>				
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.calificacionAgencia"/>:</th>
				<td>	<midas:calificacionCNSF styleId="calificacionCNSF" readonly="true" size="1" propiedad="calificacionCNSF"
				styleClass="cajaTexto" agenciaCalificacion="agenciaCalificacion"/>
				</td>					
			</tr>
			<tr>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.vigenciaConstanciaResidenciaFiscal"/>:</th>			
				<td>
					<table align="left" width="100%">
						<tr>
							<td>
								<html:text styleId="fechaInicial" property="fechaInicioVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto" maxlength="10" disabled="true"/>
							</td>
							<td width="10px">
								-
							</td>
							<td>
								<html:text styleId="fechaFinal" property="fechaFinVigenciaConstanciaResidenciaFiscal" name="reaseguradorCorredorForm" size="10" onblur="esFechaValida(this);"  styleClass="cajaTexto"  maxlength="10" disabled="true"/>
							</td>							
						</tr>													
					</table>						
				</td>
				<th><midas:mensaje clave="catalogo.reaseguradorcorredor.tieneConstanciaResidenciaFiscal"/>:</th>			
				<td>
					<midas:checkBox valorEstablecido="1" propiedadFormulario="tieneConstanciaResidenciaFiscal" deshabilitado="true"/>					
				</td>
			</tr>			
		</td>
		<td colspan="2"></td>			
		</tr>			
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.reaseguradorCorredorForm,'/MidasWeb/catalogos/reaseguradorcorredor/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.reaseguradorCorredorForm,'/MidasWeb/catalogos/reaseguradorcorredor/borrar.do', 'contenido','notificacionResultadoOperacionModificado()');"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>