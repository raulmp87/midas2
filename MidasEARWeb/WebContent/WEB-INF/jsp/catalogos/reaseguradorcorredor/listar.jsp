<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<midas:formulario  accion="/catalogos/reaseguradorcorredor/listar">
 <html:hidden styleId="tipoMensaje" property="tipoMensaje"/>
 <html:hidden styleId="mensaje" property="mensaje"/>
  	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="catalogo.reaseguradorcorredor.tituloListar" />
			</td>
		</tr>
		<tr>
			<td width="10%" align="right"><midas:mensaje clave="catalogo.reaseguradorcorredor.nombre"/>:</td>
			<td width="40%"><midas:texto propiedadFormulario="nombre" onkeypress="return stopRKey(event)"/></td>
			<td width="10%" align="right"><midas:mensaje clave="catalogo.reaseguradorcorredor.telefonoFijo"/>:</td>
			<td width="30%"><midas:texto  propiedadFormulario="telefonoFijo" onkeypress="return soloNumeros(this,event,false)"/></td>
			<td width="10%">&nbsp;</td>
		</tr> 	
		<tr>
			<td align="right"><midas:mensaje clave="catalogo.reaseguradorcorredor.tipo"/>:</td>
			<td>
				<midas:combo propiedad="tipo" styleClass="cajaTexto">
					<midas:opcionCombo valor="">Seleccione una opci&oacute;n</midas:opcionCombo>
					<midas:opcionCombo valor="0">Corredor</midas:opcionCombo>
					<midas:opcionCombo valor="1">Reasegurador</midas:opcionCombo>
				</midas:combo>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.reaseguradorCorredorForm,'/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>  
			<td></td>    	   		
		</tr>
		<tr>
			<td colspan="5"><midas:mensajeUsuario/></td>
		</tr>		
	</table>
 	 <div id="mensajeResultadoOperacion" class="notificacionResultadoOperacion" style="display:none;margin-top:15px;margin-bottom:15px;width:98%"></div>
    <br clear="all" />
 	<div id="resultados">
		<midas:tabla idTabla="reaseguradorescorredoresTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ReaseguradorCorredor"
			claseCss="tablaConResultados" nombreLista="reaseguradores"
			urlAccion="/catalogos/reaseguradorcorredor/listarFiltrado.do">
			<midas:columna propiedad="nombre" estilo="nombre"/>
			<midas:columna propiedad="telefonofijo" titulo="Tel&eacute;fono Fijo" estilo="telefonoFijo"/>
			<midas:columna propiedad="correoelectronico" titulo="Correo Electr&oacute;nico" estilo="correoElectronico"/>
			<midas:columna propiedad="tipo" estilo="tipo"/>
			<midas:columna propiedad="estatus" estilo="estatus"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do', 'contenido', 'reiniciarCalendarioDoble();');">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
