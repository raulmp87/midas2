<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subtipoequipocontratista/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<html:hidden property="idSubtipoEquipoContratista"/>
			</td>
		</tr>
		<tr>
		<td>
				<etiquetas:etiquetaError property="tipoEquipoContratistaDTO" requerido="si"
					key="catalogos.subtipoequipocontratista.tipoEquipoContratistaDTO" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:comboCatalogo propiedad="tipoEquipoContratistaDTO" styleClass="cajaTexto" size="1"
					nombreCatalogo="tctipoequipocontratista" idCatalogo="idTipoEquipoContratista"
					descripcionCatalogo="descripcionTipoEqContr" styleId="tipoEquipoContratistaDTO"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="codigoSubtipoEquipoContratista" requerido="si"
					key="catalogos.subtipoequipocontratista.codigoSubtipoEquipoContratista" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="codigoSubtipoEquipoContratista"
				onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError property="descripcionSubtipoEqCont" requerido="si"
					key="catalogos.subtipoequipocontratista.descripcionSubtipoEqCont" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcionSubtipoEqCont"
				onkeypress="return soloLetras(this, event, false)"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveAutorizacion" requerido="si"
					key="catalogos.subtipoequipocontratista.claveAutorizacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto propiedadFormulario="claveAutorizacion"
				onkeypress="return soloNumeros(this, event, false)"/>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subtipoequipocontratista/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subtipoEquipoContratistaForm,'/MidasWeb/catalogos/subtipoequipocontratista/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
	</table>
</midas:formulario>
