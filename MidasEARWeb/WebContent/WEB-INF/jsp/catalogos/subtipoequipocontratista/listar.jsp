<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/subtipoequipocontratista/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subtipoequipocontratista.codigoSubtipoEquipoContratista"/>:</th>
			<td><midas:texto propiedadFormulario="codigoSubtipoEquipoContratista" onkeypress="return soloNumeros(this, event, false)"/></td>
			<th><midas:mensaje clave="catalogos.subtipoequipocontratista.descripcionSubtipoEqCont"/>:</th>
			<td><midas:texto propiedadFormulario="descripcionSubtipoEqCont" onkeypress="return soloLetras(this, event, false)"/></td>
			<th><midas:mensaje clave="catalogos.subtipoequipocontratista.claveAutorizacion"/>:</th>
			<td><midas:texto propiedadFormulario="claveAutorizacion" onkeypress="return soloNumeros(this, event, false)"/></td>	
		</tr> 
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subtipoEquipoContratistaForm,'/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do','contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="subtipoEquipoContratistaTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.SubtipoEquipoContratista"
			claseCss="tablaConResultados" nombreLista="subtipoEquipoContratista"
			urlAccion="/catalogos/subtipoequipocontratista/listar.do">
				<midas:columna propiedad="codigoSubtipoEquipoContratista" titulo="C&oacute;digo Tipo"/>
				<midas:columna propiedad="descripcionSubtipoEqCont" titulo="Descripci&oacute;n" estilo="descripcion"/>
				<midas:columna propiedad="claveAutorizacion" titulo="Clave Autorizaci&oacute;n"/>
			<midas:columna propiedad="acciones" titulo=""/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subtipoequipocontratista/mostrarAgregar.do','contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>