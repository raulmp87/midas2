<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subtipoequipocontratista/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
				<html:hidden property="idSubtipoEquipoContratista"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.subtipoequipocontratista.codigoSubtipoEquipoContratista"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoSubtipoEquipoContratista"  nombre="subtipoEquipoContratistaForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subtipoequipocontratista.descripcionSubtipoEqCont"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionSubtipoEqCont" nombre="subtipoEquipoContratistaForm"/></td>
			<th><midas:mensaje clave="catalogos.subtipoequipocontratista.claveAutorizacion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="claveAutorizacion"  nombre="subtipoEquipoContratistaForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subtipoEquipoContratistaForm,'/MidasWeb/catalogos/subtipoequipocontratista/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>