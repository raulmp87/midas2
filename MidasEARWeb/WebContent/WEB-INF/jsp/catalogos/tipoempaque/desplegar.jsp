<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipoempaque/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" /> 
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipoempaque.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idTipoEmpaque"  nombre="tipoEmpaqueForm"/></td>
			<th><midas:mensaje clave="catalogos.tipoempaque.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoEmpaque" nombre="tipoEmpaqueForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoEmpaqueForm,'/MidasWeb/catalogos/tipoempaque/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>