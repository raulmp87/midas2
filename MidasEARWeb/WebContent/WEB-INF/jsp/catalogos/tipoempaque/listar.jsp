<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/tipoempaque/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipoempaque.id"/>:</th>
			<td><midas:texto 
				onkeypress="return soloNumeros(this, event, false)" caracteres="4" 
				propiedadFormulario="idTipoEmpaque"/></td>
			<th><midas:mensaje clave="catalogos.tipoempaque.descripcion"/>:</th>
			<td><midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionTipoEmpaque"/></td>
		</tr> 	
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoEmpaqueForm,'/MidasWeb/catalogos/tipoempaque/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="empaquesTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoEmpaque"
			claseCss="tablaConResultados" nombreLista="empaques"
			urlAccion="/catalogos/tipoempaque/listar.do">
			<midas:columna propiedad="codigoTipoEmpaque" titulo="Id tipo empaque"/>
			<midas:columna propiedad="descripcionTipoEmpaque" titulo="Descripcion" />
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipoempaque/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
