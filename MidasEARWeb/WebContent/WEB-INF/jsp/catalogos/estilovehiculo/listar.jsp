<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<midas:formulario  accion="/catalogos/estilovehiculo/listar">
	<table width="98%" id="filtros" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Estilo Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<th ><midas:mensaje clave="catalogos.estilovehiculo.id_claveTipoBien"/>:</th>
			<td colspan="2">
				<midas:comboCatalogo propiedad="id_claveTipoBien" styleId="id_claveTipoBien" 
					size="1" styleClass="cajaTexto"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'tipoVehiculoForm.idTcTipoVehiculo'); validaFiltroEstilos();"/>
			</td>
			<th ><midas:mensaje clave="catalogos.estilovehiculo.tipoVehiculo"/>:</th>
			<td colspan="2">
				<midas:tipoVehiculo styleId="tipoVehiculoForm.idTcTipoVehiculo" 
					size="1" propiedad="tipoVehiculoForm.idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto"
					onchange="getMarcaVehiculos(this,'marcaVehiculoForm.idTcMarcaVehiculo'); validaFiltroEstilos();" 
				/>
			</td>
 		</tr>
		<tr>			
			<th ><midas:mensaje clave="catalogos.estilovehiculo.marcaVehiculo"/>:</th>
			<td colspan="2">
				<midas:marcaVehiculo styleId="marcaVehiculoForm.idTcMarcaVehiculo" 
					size="1" propiedad="marcaVehiculoForm.idTcMarcaVehiculo"
					tipoVehiculo="tipoVehiculoForm.idTcTipoVehiculo" 
					styleClass="cajaTexto"
					onchange="getVersionCarga(this,'id_claveTipoBien','tipoVehiculoForm.idTcTipoVehiculo','id_idVersionCarga');validaFiltroEstilos();"
				/>
			</td>
			<th ><midas:mensaje clave="catalogos.estilovehiculo.id_idVersionCarga"/>:</th>
			<td colspan="2">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto" 
					tipoBien="id_claveTipoBien" 
					marcaVehiculo="marcaVehiculoForm.idTcMarcaVehiculo" 
					tipoVehiculo="tipoVehiculoForm.idTcTipoVehiculo"
					onchange="validaFiltroEstilos();"
				/>				
			</td>
			
		</tr>
		<tr>
			<th ><midas:mensaje clave="catalogos.estilovehiculo.id_claveEstilo"/>:</th>
			<td colspan="2">
				<midas:texto caracteres="8" propiedadFormulario="id_claveEstilo" onkeypress="return soloAlfanumericos(this, event, false)"/>
			</td>
			<th ><midas:mensaje clave="catalogos.estilovehiculo.subMarcaNulo"/>:</th>
			<td colspan="2">
				<midas:checkBox valorEstablecido="true" id="tipoSubMarcaVacio" propiedadFormulario="tipoSubMarcaVacio" />
			</td>
		</tr>
		<tr>
			<td style="font-size: 7pt; color: #990000;font-weight: bold;" colspan="4">
				Para activar la busqueda debe ingresar: <midas:mensaje clave="catalogos.estilovehiculo.id_claveTipoBien"/>, 
				<midas:mensaje clave="catalogos.estilovehiculo.tipoVehiculo"/>,
				<midas:mensaje clave="catalogos.estilovehiculo.marcaVehiculo"/>,
				<midas:mensaje clave="catalogos.estilovehiculo.id_idVersionCarga"/>
			</td>
			<td class= "buscar" colspan="2">
				<div id="busquedaEstilos" class="alinearBotonALaDerecha" style="display: none">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.estiloVehiculoForm,'/MidasWeb/catalogos/estilovehiculo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
				<div id="b_descargar" class="alinearBotonALaDerecha" style="display: none;float: right;width: 70px; margin: 0px 2px 0px 2px;">
					<a href="javascript: void(0);" onclick="javascript: downloadExcelModelSubmarcas();">
						<midas:mensaje clave="midas.accion.descargar"/>
					</a>
				</div>
				<div id="b_actualizar" class="alinearBotonALaDerecha" style="display: none;float: right;width: 70px; margin: 0px 2px 0px 2px;">
					<a href="javascript: void(0);" onclick="javascript: loadExcelModelSubmarcas();">
						<midas:mensaje clave="midas.accion.actualizar"/>
					</a>
				</div>
			</td>      		
		</tr>
<!-- 		<tr> -->
<%-- 			<midas:mensajeUsuario/> --%>
<!-- 		</tr> -->
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listMarcaVehiculo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorEstiloVehiculo"
			claseCss="tablaConResultados" nombreLista="listEstiloVehiculo"
			urlAccion="/catalogos/estilovehiculo/listarFiltrado.do">
			<midas:columna propiedad="tipoBienAutosDTO.descripcionTipoBien" titulo="Clave Tipo Bien" />
			<midas:columna propiedad="id.claveEstilo" titulo="Clave Estilo" />
			<midas:columna propiedad="id.idVersionCarga" titulo="Versi&oacute;n Carga" />
			<midas:columna propiedad="tipoVehiculoDTO.descripcionTipoVehiculo" titulo="Tipo Veh&iacuteculo" />
			<midas:columna propiedad="marcaVehiculoDTO.descripcionMarcaVehiculo" titulo="Marca" />
			<midas:columna propiedad="subMarcaVehiculoDTO.descripcionSubMarcaVehiculo" titulo="Tipo" />
			<midas:columna propiedad="acciones" titulo="Acciones" estilo="acciones" />
		</midas:tabla>
	</div>

	<div class="alinearBotonALaDerecha" style="margin-right: 20px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/mostrarAgregar.do', 'contenido','manipulaCalendarioLineas()');">
				<midas:mensaje clave="midas.accion.agregar"/>
			</a>
		</div>
	</div>

</midas:formulario>
