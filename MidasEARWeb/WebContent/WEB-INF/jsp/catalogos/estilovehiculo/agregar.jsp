<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
 <midas:formulario accion="/catalogos/estilovehiculo/agregar">
 	<table id="agregar" >
 		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.agregar" /> Estilo Veh&iacute;culo
			</td>
		</tr>
 		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveTipoBien" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<midas:comboCatalogo propiedad="id_claveTipoBien" 
					styleId="id_claveTipoBien" size="1" styleClass="cajaTexto w200"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'tipoVehiculoForm.idTcTipoVehiculo')"
				/>
			</td>
			<td style="width:10%"></td>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="tipoVehiculoForm.idTcTipoVehiculo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.tipoVehiculo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<midas:tipoVehiculo styleId="tipoVehiculoForm.idTcTipoVehiculo" 
					size="1" propiedad="tipoVehiculoForm.idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto w200"
					onchange="getMarcaVehiculos(this,'marcaVehiculoForm.idTcMarcaVehiculo')" 
				/>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="marcaVehiculoForm.idTcMarcaVehiculo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.marcaVehiculo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<midas:marcaVehiculo size="1" styleClass="cajaTexto w200"
					styleId="marcaVehiculoForm.idTcMarcaVehiculo" 
					propiedad="marcaVehiculoForm.idTcMarcaVehiculo"
					tipoVehiculo="tipoVehiculoForm.idTcTipoVehiculo" 
					onchange="getSubMarcaVehiculos(this,'subMarcaVehiculoForm.idSubTcMarcaVehiculo')" 
				/>
			</td>
			<td style="width:10%"></td>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="submarcaVehiculoForm.idSubTcMarcaVehiculo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.submarcaVehiculo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<midas:submarcaVehiculo size="1" styleClass="cajaTexto w200"
					styleId="subMarcaVehiculoForm.idSubTcMarcaVehiculo" 
					propiedad="subMarcaVehiculoForm.idSubTcMarcaVehiculo"
					marcaVehiculo="marcaVehiculoForm.idTcMarcaVehiculo" 
				/>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveEstilo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveEstilo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<html:text maxlength="8" property="id_claveEstilo"
					styleClass="jQalphaextra jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_idVersionCarga" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<html:text maxlength="5" property="id_idVersionCarga"
					onkeypress="return soloNumeros(this, event, false)"
					styleClass="jQalphaextra jQrestrict jQtoUpper cajaTexto w200" />
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si" 
					property="fechaInicioVigencia" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.fechaInicioVigencia"
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</th>
			<td>			
				<html:text property="fechaInicioVigencia" styleId="fechaInicial" readonly="true"
					styleClass="cajaTexto w200" 
				/>					
			</td>
			<td width="15px">
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" 
						onclick="javascript: mostrarCalendarioDobleLinea()">
					</a>
				</div>
			</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" 
					property="fechaFinVigencia" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.fechaFinVigencia" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text property="fechaFinVigencia" styleId="fechaFinal" readonly="true"
					styleClass="cajaTexto w200" 
				/>	
			</td> 
			<td width="15px">
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="descripcionEstilo" 
					name="estiloVehiculoForm" requerido="si"
					key="catalogos.estilovehiculo.descripcionEstilo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td colspan="4">
				<html:text maxlength="100" property="descripcionEstilo"
					styleClass="jQalphanumeric jQtoUpper cajaTexto" 
				/>
			</td>			
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="numeroAsientos" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.numeroAsientos" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="4" property="numeroAsientos"
					onkeypress="return soloNumeros(this, event, false)"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td></td>
			<td>
				<etiquetas:etiquetaError requerido="si" property="numeroPuertas" 
					name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.numeroPuertas" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="2" property="numeroPuertas"
					styleClass="jQnumeric jQrestrict cajaTexto w200" 
				/>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError name="estiloVehiculoForm" requerido="no"
					property="claveTipoCombustible"
					key="catalogos.estilovehiculo.claveTipoCombustible" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveTipoCombustible" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveTipoCombustible" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="combustibleList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveVersion" 
					name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveVersion" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<html:select property="claveVersion" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveVersion" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="versionList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>			
			<td></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError name="estiloVehiculoForm" requerido="no" 
					property="capacidadToneladas" 
					key="catalogos.estilovehiculo.capacidadToneladas" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="11" property="capacidadToneladas"
					styleClass="jQfloat jQrestrict cajaTexto w200"  onblur="validarDecimal(this, this.value, 8, 2)" 
				/>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError name="estiloVehiculoForm" requerido="no" 
					property="claveAlarmaFabricante" 
					key="catalogos.estilovehiculo.claveAlarmaFabricante" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif" />		  					
			</th>
			<td>
				<html:select property="claveAlarmaFabricante" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveAlarmaFabricante" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="alarmaFabricanteList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError name="estiloVehiculoForm" requerido="no" 
					property="claveUnidadMedida" 
					key="catalogos.estilovehiculo.claveUnidadMedida" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:text maxlength="4" property="claveUnidadMedida"
					styleClass="jQnumeric jQrestrict cajaTexto w200" 
				/>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError name="estiloVehiculoForm" requerido="no" 
					property="claveAireAcondicionado" normalClass="normal"
					key="catalogos.estilovehiculo.claveAireAcondicionado" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveAireAcondicionado" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveAireAcondicionado" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="aireAcondicionadoList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveCilindros" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveCilindros" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveCilindros" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveCilindros" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="numeroCilindrosList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError property="claveTipoInyeccion" 
					requerido="no" name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveTipoInyeccion" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveTipoInyeccion" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveTipoInyeccion" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="inyeccionList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveFrenos" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveFrenos" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<html:select property="claveFrenos" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveFrenos" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="frenosList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError property="claveSistemaElectrico" 
					requerido="no" name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveSistemaElectrico" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveSistemaElectrico" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveSistemaElectrico" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="sistemaElectricoList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveVestidura" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveVestidura" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveVestidura" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveVestidura" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="vestidurasList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveSonido" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveSonido" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<html:select property="claveSonido" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveSonido" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="sonidoList" name="estiloVehiculoForm" label="valor"/>
				</html:select>				
			</td>
			<td></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveQuemaCocos" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveQuemaCocos" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</th>
			<td>
				<html:select property="claveQuemaCocos" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveQuemaCocos" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="quemacocosList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError requerido="no" property="claveBolsasAire" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveBolsasAire" 
					errorClass="error" errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:select property="claveBolsasAire" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveBolsasAire" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="bolsasDeAireList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveTransmision" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveTransmision" 
					errorClass="error" errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:select property="claveTransmision" styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" styleId="claveTransmision" >
						<html:option value="">SELECCIONE...</html:option>
						<html:optionsCollection value="valor"  property="transmisionList" name="estiloVehiculoForm" label="valor"/>
				</html:select>
			</td>
			<td></td>
			<th>
				<etiquetas:etiquetaError property="claveCatalogoFabricante" 
					requerido="no" name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveCatalogoFabricante" 
					errorClass="error" errorImage="/img/information.gif"
				/>
			</th>
			<td>
				<html:text property="claveCatalogoFabricante" maxlength="12"
					styleClass="jQToUpper jQalphanumeric jQrestrict cajaTexto w200" 
				/>
			</td>
			<td></td>
		</tr>		
		<tr>
				<td>
			<etiquetas:etiquetaError requerido="no" property="altoRiesgo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.altoRiesgo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
		</td>
			<td>
				<html:checkbox property="altoRiesgo"/>
			</td>
		<td>
			<etiquetas:etiquetaError requerido="no" property="altaFrecuencia" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.altaFrecuencia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
		</td>
			<td>
				<html:checkbox property="altaFrecuencia"/>
			</td>
		</tr>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.estiloVehiculoForm,'/MidasWeb/catalogos/estilovehiculo/agregar.do', 'contenido','validaGuardarModificarM1();manipulaCalendarioLineas();');"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar"  colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
		</tr>	
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>	
 	</table>
	<html:hidden property="mensaje" styleId="mensaje" name="estiloVehiculoForm"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje" name="estiloVehiculoForm"/>
</midas:formulario>
<div id="errores" style="display: none;"><html:errors/></div>
