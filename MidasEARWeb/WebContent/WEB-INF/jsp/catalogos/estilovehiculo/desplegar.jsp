<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<midas:formulario accion="/catalogos/estilovehiculo/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" /> Estilo Veh&iacute;culo
			</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" normalClass="normal"
					property="id_claveTipoBien" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:comboCatalogo propiedad="id_claveTipoBien" readonly="true"
					styleId="id_claveTipoBien" size="1" styleClass="cajaTexto w200"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'tipoVehiculoForm.idTcTipoVehiculo')"
				/>
			</td>
			<td>
				<etiquetas:etiquetaError requerido="si" normalClass="normal"
					property="tipoVehiculoForm.idTcTipoVehiculo" 
					name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.tipoVehiculo" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<midas:oculto nombreFormulario="estiloVehiculoForm" 
					propiedadFormulario="tipoVehiculoForm.idTcTipoVehiculo" 
					
				/>
				<midas:tipoVehiculo styleId="idTcTipoVehiculo" size="1" 
					propiedad="tipoVehiculoForm.idTcTipoVehiculo" readonly="true"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto w200" 
					onchange="getMarcaVehiculos(this,'idMarcaVehiculo')" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" normalClass="normal"
					property="marcaVehiculoForm.idTcMarcaVehiculo" 
					name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.marcaVehiculo" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<midas:escribeCatalogo size="" styleId="" styleClass="cajaTexto w200" 
					readonly="true" propiedad="marcaVehiculoForm.idTcMarcaVehiculo" 
					clase="mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote"
				/>
			</td>
			
			<td>
				<etiquetas:etiquetaError requerido="si" normalClass="normal"
					property="submarcaVehiculoForm.idSubTcMarcaVehiculo" 
					name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.submarcaVehiculo" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<midas:escribeCatalogo size="" styleId="" styleClass="cajaTexto w200" 
					readonly="true" propiedad="subMarcaVehiculoForm.idSubTcMarcaVehiculo" 
					clase="mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoFacadeRemote"
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" normalClass="normal"
					property="id_claveEstilo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveEstilo" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="8" property="id_claveEstilo"  readonly="true"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError requerido="si" normalClass="normal"
					property="id_idVersionCarga" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="5" property="id_idVersionCarga"  readonly="true"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" name="estiloVehiculoForm"
					property="fechaInicioVigencia" normalClass="normal"
					key="catalogos.estilovehiculo.fechaInicioVigencia" 
					errorClass="error" errorImage="/img/information.gif"
				/>
			</td>
			<td align="left" width="20%">
				<html:text property="fechaInicioVigencia"  readonly="true" styleId="fechaInicial"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>				
			</td>
			<td>
				<etiquetas:etiquetaError property="fechaFinVigencia" 
					name="estiloVehiculoForm" requerido="si"
					key="catalogos.estilovehiculo.fechaFinVigencia" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:text property="fechaFinVigencia"  readonly="true" styleId="fechaFinal"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td> 
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="descripcionEstilo" 
					name="estiloVehiculoForm" requerido="si"
					key="catalogos.estilovehiculo.descripcionEstilo" 
					normalClass="normal" errorClass="error"
					 errorImage="/img/information.gif"
				/>		  					
			</td>
			<td colspan="3">
				<midas:texto caracteres="100" soloLectura="true"
					propiedadFormulario="descripcionEstilo"/>
			</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError property="numeroAsientos" 
					name="estiloVehiculoForm" requerido="si"
					key="catalogos.estilovehiculo.numeroAsientos" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="4" property="numeroAsientos"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError requerido="si" property="numeroPuertas" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.numeroPuertas" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="2" property="numeroPuertas"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="claveTipoCombustible" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveTipoCombustible" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveTipoCombustible"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveVersion" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveVersion" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveVersion"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError property="capacidadToneladas" 
					name="estiloVehiculoForm" requerido="no" 
					key="catalogos.estilovehiculo.capacidadToneladas" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text property="capacidadToneladas"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveAlarmaFabricante" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveAlarmaFabricante" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text property="claveAlarmaFabricante"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="claveUnidadMedida" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveUnidadMedida" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="4" property="claveUnidadMedida"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveAireAcondicionado" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveAireAcondicionado" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="5" property="claveAireAcondicionado"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="claveCilindros" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveCilindros" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveCilindros"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveTipoInyeccion" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveTipoInyeccion"
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:text maxlength="6" property="claveTipoInyeccion"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveFrenos" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveFrenos" 
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveFrenos"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveSistemaElectrico" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveSistemaElectrico" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveSistemaElectrico"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="claveVestidura" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveVestidura" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveVestidura"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveSonido" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveSonido" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveSonido"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="claveQuemaCocos" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveQuemaCocos" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"
				/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveQuemaCocos"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError property="claveBolsasAire" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveBolsasAire" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:text maxlength="6" property="claveBolsasAire"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="claveTransmision" 
					name="estiloVehiculoForm" requerido="no"
					key="catalogos.estilovehiculo.claveTransmision" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"
				/>
			</td>
			<td>
				<html:text maxlength="6" property="claveTransmision"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveCatalogoFabricante" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveCatalogoFabricante" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text maxlength="12" property="claveCatalogoFabricante"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
		</tr>
		<tr>
		<td>
			<etiquetas:etiquetaError requerido="no" property="altoRiesgo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.altoRiesgo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
		</td>
			<td>
				<html:checkbox property="altoRiesgo" disabled="true"/>
			</td>
		<td>
			<etiquetas:etiquetaError requerido="no" property="altaFrecuencia" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.altaFrecuencia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
		</td>
			<td>
				<html:checkbox property="altaFrecuencia" disabled="true"/>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>