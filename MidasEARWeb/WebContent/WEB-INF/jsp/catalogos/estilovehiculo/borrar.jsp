<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/estilovehiculo/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.borrar" /> Estilo Veh&iacute;culo
				<midas:oculto propiedadFormulario="id_claveTipoBien" nombreFormulario="estiloVehiculoForm" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveTipoBien" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</th>
			<td>
				<midas:comboCatalogo propiedad="id_claveTipoBien" readonly="true"
					styleId="id_claveTipoBien" size="1" styleClass="cajaTexto w200"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'tipoVehiculoForm.idTcTipoVehiculo')"
				/>
			</td>
			<td style="width:10%">&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="tipoVehiculoForm.idTcTipoVehiculo" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.tipoVehiculo" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:tipoVehiculo styleId="tipoVehiculoForm.idTcTipoVehiculo" size="1" 
					propiedad="tipoVehiculoForm.idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto w200"
					readonly="true"
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>			
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="marcaVehiculoForm.idTcMarcaVehiculo" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.marcaVehiculo" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:escribeCatalogo size="" styleId="" styleClass="cajaTexto w200" 
					readonly="true" propiedad="marcaVehiculoForm.idTcMarcaVehiculo" 
					clase="mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote"/>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="id_claveEstilo" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_claveEstilo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="8" property="id_claveEstilo"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="id_idVersionCarga" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="5" property="id_idVersionCarga"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr> 		
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" 
					property="fechaInicioVigencia" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.fechaInicioVigencia" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td align="left" width="20%">
				<html:text property="fechaInicioVigencia"  readonly="true"  styleId="fechaInicial"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>		
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="si" property="fechaFinVigencia" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.fechaFinVigencia" 
					errorClass="error" errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text property="fechaFinVigencia"  readonly="true"  styleId="fechaFinal"
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>		
			</td> 
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" 
					property="descripcionEstilo" name="estiloVehiculoForm" 
					key="catalogos.estilovehiculo.descripcionEstilo" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td colspan="4">
				<midas:texto caracteres="100" soloLectura="true"
					propiedadFormulario="descripcionEstilo"/>	
			</td>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="numeroAsientos" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.numeroAsientos" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="4" property="numeroAsientos"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>	
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="si" property="numeroPuertas" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.numeroPuertas" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="2" property="numeroPuertas"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>	
			</td>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveTipoCombustible" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveTipoCombustible" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveTipoCombustible"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>	
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveVersion" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveVersion" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6" property="claveVersion"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="capacidadToneladas" name="estiloVehiculoForm" 
					key="catalogos.estilovehiculo.capacidadToneladas" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text  property="capacidadToneladas"  readonly="true" 
					styleClass="jQalphanumeric jQrestrict jQtoUpper cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveAlarmaFabricante" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveAlarmaFabricante" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text  property="claveAlarmaFabricante"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveUnidadMedida" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveUnidadMedida" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="4"  property="claveUnidadMedida"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveAireAcondicionado" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveAireAcondicionado" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveAireAcondicionado"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveCilindros" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveCilindros" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveCilindros"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveTipoInyeccion" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveTipoInyeccion" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text maxlength="6"  property="claveTipoInyeccion"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveFrenos" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveFrenos" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveFrenos"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveSistemaElectrico" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveSistemaElectrico" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveSistemaElectrico"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>

		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveVestidura" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveVestidura" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveVestidura"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveSonido" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveSonido" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveSonido"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveQuemaCocos" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveQuemaCocos" 
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text maxlength="6"  property="claveQuemaCocos"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveBolsasAire" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveBolsasAire" 
					errorClass="error" errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text maxlength="6"  property="claveBolsasAire"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="no" property="claveTransmision" 
					name="estiloVehiculoForm" normalClass="normal"
					key="catalogos.estilovehiculo.claveTransmision" 					
					errorClass="error" errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text maxlength="6"  property="claveTransmision"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
			<td>
				<etiquetas:etiquetaError requerido="no" 
					property="claveCatalogoFabricante" name="estiloVehiculoForm"
					key="catalogos.estilovehiculo.claveCatalogoFabricante" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td>
				<html:text maxlength="12"  property="claveCatalogoFabricante"  readonly="true" 
					styleClass="cajaTexto w200" 
				/>
			</td>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_borrar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: Confirma('\u00BFRealmente deseas borrar el registro seleccionado?',document.estiloVehiculoForm,'/MidasWeb/catalogos/estilovehiculo/borrar.do', 'contenido','validaBorrarM1()');">
							<midas:mensaje clave="midas.accion.borrar" />
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar" />
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>