<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/catalogos/tipomuro/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="codigoTipoMuro" name="tipoMuroForm"
					key="catalogos.tipomuro.id" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)" caracteres="4" 
				propiedadFormulario="codigoTipoMuro"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionTipoMuro" name="tipoMuroForm"
					key="catalogos.tipomuro.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionTipoMuro"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoMuroForm,'/MidasWeb/catalogos/tipomuro/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="tipoMurosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoMuro"
			claseCss="tablaConResultados" nombreLista="muros"
			urlAccion="/catalogos/tipomuro/listar.do">
			<midas:columna propiedad="codigoTipoMuro" titulo="ID Tipo de Muro"/>
			<midas:columna propiedad="descripcionTipoMuro" titulo="Descripcion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipomuro/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
