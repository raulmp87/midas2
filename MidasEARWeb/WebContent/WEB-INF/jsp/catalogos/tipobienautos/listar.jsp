<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario  accion="/catalogos/tipobienautos/listar">
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Tipo Bien Auto
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipobienautos.id"/>:</th>
			<td><midas:texto caracteres="5" 
				propiedadFormulario="claveTipoBien"
				onkeypress="return soloLetras(this, event, false)" /></td>
			<th><midas:mensaje clave="catalogos.tipobienautos.descripcion"/>:</th>
			<td>
				<html:text property="descripcionTipoBien" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>
			</td>
		</tr> 	
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.tipoBienAutosForm,'/MidasWeb/catalogos/tipobienautos/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listTipoBienAutos"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorTipoBienAutos"
			claseCss="tablaConResultados" nombreLista="listTipoBienAutos"
			urlAccion="/catalogos/tipobienautos/listar.do">
			<midas:columna propiedad="claveTipoBien" titulo="Tipo Bien" />
			<midas:columna propiedad="descripcionTipoBien" titulo="Descripci&oacute;n" />
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipobienautos/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
