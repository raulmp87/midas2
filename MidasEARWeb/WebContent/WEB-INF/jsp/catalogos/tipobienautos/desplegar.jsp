<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/tipobienautos/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" /> Tipo Bien Auto
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipobienautos.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="claveTipoBien"  nombre="tipoBienAutosForm"/></td>
			<th><midas:mensaje clave="catalogos.tipobienautos.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoBien" nombre="tipoBienAutosForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoBienAutosForm,'/MidasWeb/catalogos/tipobienautos/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>