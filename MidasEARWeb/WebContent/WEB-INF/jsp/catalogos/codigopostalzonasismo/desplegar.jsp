<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/codigopostalzonasismo/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="codigoPostalZonaSismoForm" propiedad="descripcionZonaSismo"/>'
						<html:hidden property="nombreColonia" name="codigoPostalZonaSismoForm"/>
					</td>
				</tr>
	 			<tr>
					<th><midas:mensaje clave="catalogos.zonasismo.codigoZonaSismo" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="codigoZonaSismo" nombre="codigoPostalZonaSismoForm"/></td>
					<th colspan="2"></th>								
				</tr> 	
				<tr>
					<th><midas:mensaje clave="catalogos.zonasismo.descripcionZonaSismo" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionZonaSismo" nombre="codigoPostalZonaSismoForm"/></td>
					<th><midas:mensaje clave="catalogos.codigopostalzonasismo.codigoPostal" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="codigoPostal" nombre="codigoPostalZonaSismoForm"/></td>					
				</tr>
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/codigopostalzonasismo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
							</div>
						</div>
					</td>
				</tr>   	  				
		  	</table>
	  	</div>
    </midas:formulario>