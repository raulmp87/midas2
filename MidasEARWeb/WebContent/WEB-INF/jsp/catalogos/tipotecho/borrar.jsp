<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipotecho/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<midas:oculto propiedadFormulario="idtctipotechosistema"/>
			</td>
			<td>
				<html:hidden property="idtctipotecho"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipotecho.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idtctipotecho"  nombre="tipoTechoForm"/></td>
			<th><midas:mensaje clave="catalogos.tipotecho.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripciontipotecho" nombre="tipoTechoForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoTechoForm,'/MidasWeb/catalogos/tipotecho/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.tipoTechoForm,'/MidasWeb/catalogos/tipotecho/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>