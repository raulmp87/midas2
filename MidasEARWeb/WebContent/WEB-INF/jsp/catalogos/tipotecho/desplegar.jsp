<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipotecho/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" /> 
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipotecho.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="idtctipotecho"  nombre="tipoTechoForm"/></td>
			<th><midas:mensaje clave="catalogos.tipotecho.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripciontipotecho" nombre="tipoTechoForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoTechoForm,'/MidasWeb/catalogos/tipotecho/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>