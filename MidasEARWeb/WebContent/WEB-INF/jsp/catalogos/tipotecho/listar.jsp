<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/tipotecho/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipotecho.id"/>:</th>
			<td><midas:texto 
				onkeypress="return soloNumeros(this, event, false)" caracteres="100" 
				propiedadFormulario="idtctipotecho"/></td>
			<th><midas:mensaje clave="catalogos.tipotecho.descripcion"/>:</th>
			<td><midas:texto 
				caracteres="200" 
				propiedadFormulario="descripciontipotecho"/></td>
		</tr> 	
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.tipoTechoForm,'/MidasWeb/catalogos/tipotecho/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="tipoTechoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoTecho"
			claseCss="tablaConResultados" nombreLista="listTipoTecho"
			urlAccion="/catalogos/tipotecho/listar.do">
			<midas:columna propiedad="codigoTipoTecho" titulo="Id tipo techo" />
			<midas:columna propiedad="descripcionTipoTecho" titulo="Descripcion" />
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipotecho/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
