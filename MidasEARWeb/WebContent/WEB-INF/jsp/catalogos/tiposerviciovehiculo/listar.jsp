<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario  accion="/catalogos/tiposerviciovehiculo/listar">
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Tipo Servicio Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tiposerviciovehiculo.idTcTipoVehiculo"/>:</th>
			<td class="fondoCajaTexto">
				<midas:escribeCatalogo size="" styleId="" 
					styleClass="cajaTexto"
					propiedad="idTcTipoVehiculo" 
					clase="mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tiposerviciovehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto">
				<midas:texto caracteres="5" propiedadFormulario="codigoTipoServicioVehiculo" onkeypress="return soloAlfanumericos(this, event, false)" /></td>
			<th><midas:mensaje clave="catalogos.tiposerviciovehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto">
				<html:text property="descripcionTipoServVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>	
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.tipoServicioVehiculoForm,'/MidasWeb/catalogos/tiposerviciovehiculo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listTipoServicioVehiculo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorTipoServicioVehiculo"
			claseCss="tablaConResultados" nombreLista="listTipoServicioVehiculo"
			urlAccion="/catalogos/tiposerviciovehiculo/listarFiltrado.do">
			<midas:columna propiedad="tipoVehiculoDTO.descripcionTipoVehiculo" titulo="Tipo Veh&iacute;culo" />
			<midas:columna propiedad="codigoTipoServicioVehiculo" titulo="C&oacute;digo" />
			<midas:columna propiedad="descripcionTipoServVehiculo" titulo="Descripci&oacute;n" />
			<midas:columna propiedad="acciones" titulo="Acciones" estilo="acciones" />
		</midas:tabla>
	</div>

	<div class="alinearBotonALaDerecha" style="margin-right: 20px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tiposerviciovehiculo/mostrarAgregar.do', 'contenido',null);">
				<midas:mensaje clave="midas.accion.agregar"/>
			</a>
		</div>
	</div>

</midas:formulario>
