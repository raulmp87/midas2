<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/tiposerviciovehiculo/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" /> Tipo Servicio Veh&iacute;culo
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tiposerviciovehiculo.idTcTipoVehiculo"/>:</th>
			<td class="fondoCajaTexto">
			<midas:escribe propiedad="tipoVehiculoDTO.descripcionTipoVehiculo"  nombre="tipoServicioVehiculoForm"/>
			
			
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tiposerviciovehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoTipoServicioVehiculo"  nombre="tipoServicioVehiculoForm"/></td>
			<th><midas:mensaje clave="catalogos.tiposerviciovehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoServVehiculo" nombre="tipoServicioVehiculoForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoServicioVehiculoForm,'/MidasWeb/catalogos/tiposerviciovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>