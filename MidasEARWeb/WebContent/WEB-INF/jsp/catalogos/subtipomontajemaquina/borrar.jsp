<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subtipomontajemaquina/borrar">
	<table id="borar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" />
				<html:hidden property="idSubRamo"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subtipomontajemaquina.tipo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoMontaje"  nombre="subtipoMontajeMaquinaForm"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.subtipomontajemaquina.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigosubtipomontajemaq"  nombre="subtipoMontajeMaquinaForm"/></td>
			<th><midas:mensaje clave="catalogos.subtipomontajemaquina.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionsubtipomontajemaq" nombre="subtipoMontajeMaquinaForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subtipomontajemaquina.claveautorizacion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionClave"  nombre="subtipoMontajeMaquinaForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subtipoMontajeMaquinaForm,'/MidasWeb/catalogos/subtipomontajemaquina/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.subtipoMontajeMaquinaForm,'/MidasWeb/catalogos/subtipomontajemaquina/borrar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>