<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subtipomontajemaquina/agregar">
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<html:hidden property="idtcsubtipomontajemaq"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="idTipoMontaje" name="subtipoMontajeMaquinaForm"
					key="catalogos.subtipomontajemaquina.tipo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>	
			</td>
			<td>
				<midas:tipoMontajeMaquina styleId="subtipoMontaje" size="1" propiedad="idTipoMontaje" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="codigosubtipomontajemaq" name="subtipoMontajeMaquinaForm"
					key="catalogos.subtipomontajemaquina.codigo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)"
				propiedadFormulario="codigosubtipomontajemaq"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionsubtipomontajemaq" name="subtipoMontajeMaquinaForm"
					key="catalogos.subtipomontajemaquina.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionsubtipomontajemaq"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="claveautorizacion" name="subtipoMontajeMaquinaForm"
					key="catalogos.subtipomontajemaquina.claveautorizacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:combo propiedad="claveautorizacion">
					<midas:opcionCombo valor="0">NO REQUERIDA</midas:opcionCombo>
					<midas:opcionCombo valor="1">AREA TECNICA</midas:opcionCombo>
					<midas:opcionCombo valor="2">AREA DE REASEGURO</midas:opcionCombo>
				</midas:combo>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subtipomontajemaquina/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subtipoMontajeMaquinaForm,'/MidasWeb/catalogos/subtipomontajemaquina/midificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
