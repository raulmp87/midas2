<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>

<midas:formulario  accion="/catalogos/subtipomontajemaquina/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.filtrar" />
			</td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="catalogos.subtipomontajemaquina.tipo"/>
			</td>
			<td>
				<midas:tipoMontajeMaquina styleId="subtipoMontaje" size="1" propiedad="idTipoMontaje" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td>
				<midas:mensaje clave="catalogos.subtipomontajemaquina.descripcion"/>	
			</td>
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionsubtipomontajemaq"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<midas:mensaje clave="catalogos.subtipomontajemaquina.codigo"/>					
			</td>
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)"
				propiedadFormulario="codigosubtipomontajemaq"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<midas:mensaje clave="catalogos.subtipomontajemaquina.claveautorizacion"/>		
			</td>
			<td>
				<midas:combo propiedad="claveautorizacion">
					<midas:opcionCombo valor="0">NO REQUERIDA</midas:opcionCombo>
					<midas:opcionCombo valor="1">AREA TECNICA</midas:opcionCombo>
					<midas:opcionCombo valor="2">AREA DE REASEGURO</midas:opcionCombo>
				</midas:combo>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subtipoMontajeMaquinaForm,'/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="SubtiposMontajeMaquinaTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.SubtipoMontajeMaquina"
			claseCss="tablaConResultados" nombreLista="subtiposMontaje"
			urlAccion="/catalogos/subtipomontajemaquina/listar.do">
			<midas:columna propiedad="codigosubtipomontajemaq" titulo="Id de Subtipo de Montaje para Maquina"/>
			<midas:columna propiedad="descripcionsubtipomontajemaq" titulo="Descripcion"/>
			<midas:columna propiedad="claveautorizacion" titulo="Clave de Autorizacion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subtipomontajemaquina/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
