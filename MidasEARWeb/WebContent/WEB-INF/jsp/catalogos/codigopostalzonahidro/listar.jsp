<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario  accion="/catalogos/codigopostalzonahidro/listar">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="codigoPostalZonaHidroForm" property="totalRegistros"/>
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.codigopostalzonahidro.idTcZonaHidro"/>:</th>
			<td><midas:comboCatalogo propiedad="nombreColonia" styleId="nombreColonia"  size="1" styleClass="cajaTexto" nombreCatalogo="tczonahidro" idCatalogo="idTcZonaHidro" descripcionCatalogo="descripcionZonaHidro" /></td>						
			<th><midas:mensaje clave="catalogos.codigopostalzonahidro.codigoPostal"/>:</th>
			<td><midas:texto  propiedadFormulario="codigoPostal"/></td>	
			<input type="text" id="hiddenPatch" style="display: none;"/>		
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.codigoPostalZonaHidroForm,'/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="codigoPostalZonaHidroTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.CodigoPostalZonaHidro"
			claseCss="tablaConResultados" nombreLista="codigoPostalZonaHidros"
			urlAccion="/catalogos/codigopostalzonahidro/listarFiltradoPaginado.do"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="codigoPostal" titulo="C&oacute;digo postal"/>
			<midas:columna propiedad="nombreColonia" titulo="Colonia"/>
			<midas:columna propiedad="descripcionZonaHidro" titulo="Zona Hidro"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />			
		</midas:tabla>		
	</div>
	<midas:mensajeUsuario/>
</midas:formulario>