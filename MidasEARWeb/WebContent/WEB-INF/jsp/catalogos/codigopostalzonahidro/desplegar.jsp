<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/codigopostalzonahidro/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" /> '<midas:escribe nombre="codigoPostalZonaHidroForm" propiedad="descripcionZonaHidro"/>'
					</td>
				</tr>
	 			<tr>
					<th><midas:mensaje clave="catalogos.zonahidro.codigoZonaHidro" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="codigoZonaHidro" nombre="codigoPostalZonaHidroForm"/></td>
					<th colspan="2"></th>								
				</tr> 	
				<tr>
					<th><midas:mensaje clave="catalogos.zonahidro.descripcionZonaHidro" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionZonaHidro" nombre="codigoPostalZonaHidroForm"/></td>
					<th><midas:mensaje clave="catalogos.codigopostalzonahidro.codigoPostal" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="codigoPostal" nombre="codigoPostalZonaHidroForm"/></td>					
				</tr>
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/codigopostalzonahidro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
								<html:hidden property="nombreColonia" name="codigoPostalZonaHidroForm"/>
							</div>
						</div>
					</td>
				</tr>   	  				
		  	</table>
	  	</div>
    </midas:formulario>