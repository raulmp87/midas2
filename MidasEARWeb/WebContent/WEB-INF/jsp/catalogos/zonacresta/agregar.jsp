<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="mx.com.afirme.midas.sistema.Sistema"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


    <midas:formulario accion="/catalogos/zonacresta/agregar">
	  	<table>
	  		<tr>
		  		<td>&nbsp;</td>
		  		<td>
		  			<table>
		  				<tr>
							<td>
								<etiquetas:etiquetaError requerido="si"
									property="nombreArea" name="zonaCrestaForm"
									key="catalogos.zonacresta.nombrearea" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif"/>		  					
							</td>
							<td>
								<midas:texto 
								onkeypress="return soloAlfanumericos(this, event, false)" caracteres="50" 
								propiedadFormulario="nombreArea"/>
							</td>
							<td colspan="2">&nbsp;</td>
							<td>
								<etiquetas:etiquetaError requerido="si"
								property="numeroArea" name="zonaCrestaForm"
								key="catalogos.zonacresta.numeroarea" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />		  					
							</td>
							<td>
								<midas:texto propiedadFormulario="numeroArea"
								onkeypress="return soloNumeros(this, event, false)"/>
							</td>
						</tr> 
						<tr>
							<td>
								<etiquetas:etiquetaError requerido="si"
								property="geoCodigo" name="zonaCrestaForm"
								key="catalogos.zonacresta.geocodigo" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />		  					
							</td>
							<td>
								<midas:texto propiedadFormulario="geoCodigo"
								onkeypress="return soloAlfanumericos(this, event, false)"
								caracteres="50"/>
							</td>
							
					
		  				</tr> 
		  				<tr>
		  					<td>
								<etiquetas:etiquetaError requerido="si"
								property="estado" name="zonaCrestaForm"
								key="catalogo.reaseguradorcorredor.estado" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />
							</td>
							<td>
								<midas:estado styleId="estado" size="1" propiedad="estado"
									pais="PAMEXI" styleClass="cajaTexto"
									onchange="limpiarObjetos('municipio'); getMunicipios(this,'municipio');" />
							</td>
							<td>
								<etiquetas:etiquetaError requerido="si"
									property="idtcmunicipio" name="zonaCrestaForm"
									key="catalogos.zonacresta.municipio" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />	
							</td>
							<td>
								<midas:municipio styleId="municipio" size="1" propiedad="idtcmunicipio"
									estado="estado" styleClass="cajaTexto"/>
							</td>
		  				</tr>
		  				<tr>
		 					<td colspan="2">&nbsp;</td>
		 					<td>
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/listar.do', 'contenido',null);">
									<bean:message key="midas.accion.regresar"/>
								</a>		 					
		 					</td>
		 					<td>		  				
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/agregar.do', 'contenido',null);">
									<bean:message key="midas.accion.guardar"/>
								</a>
							</td>			  				
		  				</tr>
		  			</table>
		  		</td>
		  	</tr>
	  		<tr>
				<midas:mensajeUsuario/>
			</tr>
	  	</table>
    </midas:formulario>

