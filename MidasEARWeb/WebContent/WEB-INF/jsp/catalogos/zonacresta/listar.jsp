<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/zonacresta/listarFiltrado">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonacresta.nombrearea"/>:</th>
			<td><midas:texto  propiedadFormulario="nombreArea" caracteres="50"/></td>
			<th><midas:mensaje clave="catalogos.zonacresta.numeroarea"/>:</th>
			<td><midas:texto  propiedadFormulario="numeroArea"/></td>
			<th><midas:mensaje clave="catalogos.zonacresta.geocodigo"/>:</th>
			<td><midas:texto  propiedadFormulario="geoCodigo" caracteres="50"/></td>
		</tr> 	
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="zonasCrestaTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ZonaCresta"
			claseCss="tablaConResultados" nombreLista="zonasCresta"
			urlAccion="/catalogos/zonacresta/listar.do">
			<midas:columna propiedad="nombrearea" titulo="Nombre de Area" />
			<midas:columna propiedad="numeroarea" titulo="Numero de Area"/>
			<midas:columna propiedad="geocodigo" titulo="Geo Codigo"/>
			<midas:columna propiedad="agregarViejo" titulo="Agregar Zona Vieja"/>
			<midas:columna propiedad="acciones" titulo="" estilo="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
