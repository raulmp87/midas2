<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/zonacresta/mostrarDetalle">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
			</td>
			<td colspan="2"><html:hidden property="idTcZonaCresta" name="zonaCrestaForm"/>&nbsp;</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonacresta.nombrearea"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="nombreArea"  nombre="zonaCrestaForm"/></td>
			<th><midas:mensaje clave="catalogos.zonacresta.numeroarea"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="numeroArea" nombre="zonaCrestaForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonacresta.geocodigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="geoCodigo" nombre="zonaCrestaForm"/></td>
			<th><midas:mensaje clave="catalogos.zonacresta.municipio"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionMunicipio" nombre="zonaCrestaForm"/></td>
		</tr>
		<tr>
			<td><br></td>
		</tr>
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.zonacresta.zonas.asignadas" />
			</td>
		</tr>
		<tr>
			<midas:tabla idTabla="zonasCrestaAsignadasTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ZonaCrestaViejo"
			claseCss="tablaConResultados" nombreLista="zonasAsignadas"
			urlAccion="/catalogos/zonacresta/mostrarDetalle.do">
			<midas:columna propiedad="geocodigoviejo" titulo="Geocodigo"/>
			<midas:columna propiedad="nombreareaviejo" titulo="Nombre de Area" />
			<midas:columna propiedad="numeroareaviejo" titulo="Numero de Area"/>
			<midas:columna propiedad="tipozona" titulo="Tipo"/>
			</midas:tabla>
		</tr>
		
		
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			<!-- 
			</td>
			<td colspan="1"></td>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/viejo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.asignar" /></a>
					</div>
				</div>
			</td>
			 -->
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>