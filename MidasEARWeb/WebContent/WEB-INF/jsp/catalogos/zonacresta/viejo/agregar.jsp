<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="mx.com.afirme.midas.sistema.Sistema"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>


    <midas:formulario accion="/catalogos/zonacresta/viejo/agregar">
	  	<table>
	  		<tr>
		  		<td>&nbsp;</td>
		  		<td>
		  			<table>
		  				<tr>
							<td>
								<etiquetas:etiquetaError requerido="si"
									property="nombreAreaViejo" name="zonaCrestaViejoForm"
									key="catalogos.zonacresta.nombrearea" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif"/>		  					
							</td>
							<td>
								<midas:texto 
								onkeypress="return soloAlfanumericos(this, event, false)" caracteres="50" 
								propiedadFormulario="nombreAreaViejo"/>
							</td>
							<td colspan="2">&nbsp;</td>
							<td>
								<etiquetas:etiquetaError requerido="si"
								property="numeroAreaViejo" name="zonaCrestaViejoForm"
								key="catalogos.zonacresta.numeroarea" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />		  					
							</td>
							<td>
								<midas:texto propiedadFormulario="numeroAreaViejo"
								onkeypress="return soloNumeros(this, event, false)"/>
							</td>
						</tr> 
						<tr>
							<td>
								<etiquetas:etiquetaError requerido="si"
								property="geoCodigoViejo" name="zonaCrestaViejoForm"
								key="catalogos.zonacresta.geocodigo" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />		  					
							</td>
							<td>
								<midas:texto propiedadFormulario="geoCodigoViejo"
								onkeypress="return soloAlfanumericos(this, event, false)"
								caracteres="50"/>
							</td>
							<td>
								<etiquetas:etiquetaError
								property="tipoZona" name="zonaCrestaViejoForm"
								key="catalogos.zonacresta.viejo.tipo" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />		  					
							</td>
							<td><midas:combo propiedad="tipoZona">
									<midas:opcionCombo valor="0">Terremoto</midas:opcionCombo>
									<midas:opcionCombo valor="1">Hidrometeorologico</midas:opcionCombo>
								</midas:combo>
							</td>
					
		  				</tr> 
		  				<tr>
		 					<td colspan="2">&nbsp;</td>
		 					<td>
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/viejo/listar.do', 'contenido',null);">
									<bean:message key="midas.accion.regresar"/>
								</a>		 					
		 					</td>
		 					<td>		  				
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.zonaCrestaViejoForm,'/MidasWeb/catalogos/zonacresta/viejo/agregar.do', 'contenido',null);">
									<bean:message key="midas.accion.guardar"/>
								</a>
							</td>			  				
		  				</tr>
		  			</table>
		  		</td>
	  		</tr>
	  		<tr>
				<midas:mensajeUsuario/>
			</tr>
	  	</table>
    </midas:formulario>

