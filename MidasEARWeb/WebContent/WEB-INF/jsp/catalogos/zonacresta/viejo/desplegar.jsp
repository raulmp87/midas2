<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/zonacresta/viejo/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.zonacresta.viejo.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.zonacresta.viejo.tipo"/>:</th>
			<td>
				<midas:combo propiedad="tipoZona">
					<midas:opcionCombo valor="0">Terremoto</midas:opcionCombo>
					<midas:opcionCombo valor="1">Hidrometeorologico</midas:opcionCombo>
				</midas:combo>
			</td>
			<th><midas:mensaje clave="catalogos.zonacresta.nombrearea"/>:</th>
			<td><midas:texto  propiedadFormulario="nombreAreaViejo"/></td>
			<th><midas:mensaje clave="catalogos.zonacresta.numeroarea"/>:</th>
			<td><midas:texto  propiedadFormulario="numeroAreaViejo"/></td>
			<th><midas:mensaje clave="catalogos.zonacresta.geocodigo"/>:</th>
			<td><midas:texto  propiedadFormulario="geoCodigoViejo"/></td>
		</tr> 	
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaCrestaViejoForm,'/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
		<tr>
			<midas:tabla idTabla="zonasCrestaViejoTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ZonaCrestaViejo"
			claseCss="tablaConResultados" nombreLista="zonasCrestaViejo"
			urlAccion="/catalogos/zonacresta/mostrarDetalle.do">
			<midas:columna propiedad="geocodigoviejo" titulo="Geocodigo"/>
			<midas:columna propiedad="nombreareaviejo" titulo="Nombre de Area" />
			<midas:columna propiedad="numeroareaviejo" titulo="Numero de Area"/>
			<midas:columna propiedad="tipozona" titulo="Tipo"/>
			<midas:columna propiedad="asignar" titulo="Seleccionar"/>
			<midas:columna propiedad="borrar" />
			</midas:tabla>
		</tr>
		
		
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
			<td>
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/viejo/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>