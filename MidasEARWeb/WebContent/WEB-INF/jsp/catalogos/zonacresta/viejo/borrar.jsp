<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
    <html:form action="/catalogos/zonacresta/borrar">
	  	<table>
	  		<tr>
		  		<td>&nbsp;</td>
		  		<td>
		  			<table>
		  				<tr>
		  					<td colspan="2"><html:hidden property="idTcZonaCresta" name="zonaCrestaForm"/>
							</td>
		  					<td>
								<etiquetas:etiquetaError
									property="nombreArea" name="zonaCrestaForm"
									key="catalogos.zonacresta.nombrearea" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />			  					
							</td>
		  					<td><bean:write property="nombreArea" name="zonaCrestaForm"/></td>
		  				</tr> 
		  				<tr>
		  					<td>
								<etiquetas:etiquetaError
									property="numeroArea" name="zonaCrestaForm"
									key="catalogos.zonacresta.numeroarea" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />			  					
		  					</td>
		  					<td><bean:write property="numeroArea" name="zonaCrestaForm"/></td>
		  					<td>
								<etiquetas:etiquetaError
									property="geoCodigo" name="zonaCrestaForm"
									key="catalogos.zonacresta.geocodigo" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />	
		  					</td>
		  					<td><bean:write property="geoCodigo" name="zonaCrestaForm"/></td>
		  				</tr> 		
		  				<tr>
		 					<td colspan="2">&nbsp;</td>
		 					<td>
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/catalogos/zonacresta/listar.do', 'contenido',null);">
									<bean:message key="midas.accion.regresar"/>
								</a>		 					
		 					</td>
		 					<td>
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/catalogos/zonacresta/borrar.do', 'contenido',null);">
									<bean:message key="midas.accion.borrar"/>
								</a>
							</td>
						</tr>  
		  			</table>

		  		</td>
	  		</tr>
	  		<tr>
				<midas:mensajeUsuario/>
			</tr>
	  	</table>
    </html:form>

