<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <html:form action="/catalogos/zonacresta/modificar" method="post">
	  	<table>
	  		<tr>
		  		<td>&nbsp;</td>
		  		<td>
		  			<table>
		  				<tr>
		  					<td colspan="2">&nbsp;</td>
		  					<td>
								<etiquetas:etiquetaError
									property="nombreArea" name="zonaCrestaForm"
									key="catalogos.zonacresta.nombrearea" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />	
		  					</td>
		  					<td><midas:texto 
								onkeypress="return soloAlfanumericos(this, event, false)" caracteres="50" 
								propiedadFormulario="nombreAreaViejo"/>
							</td>
		  				</tr> 
		  				<tr>
		  					<td>
								<etiquetas:etiquetaError
									property="numeroArea" name="zonaCrestaForm"
									key="catalogos.zonacresta.numeroarea" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />	
		  					</td>
		  					<td><html:text property="numeroArea" name="zonaCrestaForm"/></td>
		  					<td>
								<etiquetas:etiquetaError
									property="geoCodigo" name="zonaCrestaForm"
									key="catalogos.zonacresta.geocodigo" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />	
		  					</td>
		  					<td><midas:texto propiedadFormulario="geoCodigoViejo"
								onkeypress="return soloAlfanumericos(this, event, false)"
								caracteres="50"/>
							</td>
		  				</tr>
		  				<tr>
		  					<td colspan="4">
		  						<div id = "detalle" style="height: 100px;">
			  						<table>
			  							<tr>
			  								<th>Geo Codigo Viejo</th>
			  								<th>Nombre &Aacute;rea Viejo</th>
			  								<th>N&uacute;mero &Aacute;rea Viejo</th>
			  								<th>Tipo Zona</th>
			  							</tr>
			  							<tr>
							  				<logic:present name="zonaCrestaForm" property="tcZonaCrestaViejos">
							  					<logic:iterate id="item" name="zonaCrestaForm" property="tcZonaCrestaViejos">
							  						<td><bean:write name="item" property="id.geoCodigoViejo"/></td>
							  						<td><bean:write name="item" property="id.nombreAreaViejo"/></td>
							  						<td><bean:write name="item" property="id.numeroAreaViejo"/></td>
							  						<td><bean:write name="item" property="id.tipoZona"/></td>
							  						<td>
														<a href="javascript: void(0);"
															onclick="javascript: sendRequest(null,'/MidasWeb/zonacrestaviejo/mostrarModificar.do?id=<bean:write name="item" property="id.idTcZonaCrestaViejo" />', 'contenido',null);">
															<bean:message key="midas.accion.modificar"/>
														</a>
							  						</td>
							  						<td>
														<a href="javascript: void(0);"
															onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonacrestaviejo/mostrarBorrar.do?id=<bean:write name="item" property="id.idTcZonaCrestaViejo" />', 'contenido',null);">
															<bean:message key="midas.accion.borrar"/>
														</a>	
							  						</td>
							  						<td>
														<a href="javascript: void(0);"
															onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/zonacrestaviejo/mostrarDetalle.do?id=<bean:write name="item" property="id.idTcZonaCrestaViejo" />', 'contenido',null);">
															<bean:message key="midas.accion.detalle"/>
														</a>								  						
							  						</td>							  						
							  					</logic:iterate>
							  				</logic:present>				  								
			  							</tr>
			  						</table>
		  						</div>
		  					</td>
		  				</tr>			  				
		  				<tr>
		 					<td colspan="2">&nbsp;</td>
		 					<td>
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/zonacresta/listar.do', 'contenido',null);">
									<bean:message key="midas.accion.regresar"/>
								</a>		 					
		 					</td>
		 					<td>		  				
								<a href="javascript: void(0);"
									onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/zonacresta/modificar.do', 'contenido',null);">
									<bean:message key="midas.accion.guardar"/>
								</a>
							</td>			  				
		  				</tr>		  				
		  			</table>
		  		</td>
	  		</tr>
	  		<tr>
				<midas:mensajeUsuario/>
			</tr>
	  	</table>
    </html:form>

