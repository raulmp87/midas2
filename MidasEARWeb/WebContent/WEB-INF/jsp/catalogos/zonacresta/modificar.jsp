<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="mx.com.afirme.midas.sistema.Sistema"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <midas:formulario accion="/catalogos/zonacresta/modificar">
	  	<table id="agregar">
	  		<tr>
		  		<td colspan="4">
		  		<html:hidden property="idTcZonaCresta"/>
		  		</td>
		  	</tr>
			<tr>
				<th>
					<etiquetas:etiquetaError requerido="si"
						property="nombreArea" name="zonaCrestaForm"
						key="catalogos.zonacresta.nombrearea" normalClass="normal"
						errorClass="error" errorImage="/img/information.gif"/>		  					
				</th>
				<td>
					<midas:texto 
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="50" 
					propiedadFormulario="nombreArea"/>
				</td>
				<th>
					<etiquetas:etiquetaError requerido="si"
					property="numeroArea" name="zonaCrestaForm"
					key="catalogos.zonacresta.numeroarea" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />		  					
				</th>
				<td>
					<midas:texto propiedadFormulario="numeroArea"
					onkeypress="return soloNumeros(this, event, false)"/>
				</td>
				
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<td>
								<etiquetas:etiquetaError requerido="si"
								property="geoCodigo" name="zonaCrestaForm"
								key="catalogos.zonacresta.geocodigo" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />		  					
							</td>
							<td colspan="1">
								<midas:texto propiedadFormulario="geoCodigo"
								onkeypress="return soloAlfanumericos(this, event, false)"
								caracteres="50"/>
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
		  					<th>
								<etiquetas:etiquetaError requerido="si"
								property="estado" name="zonaCrestaForm"
								key="catalogo.reaseguradorcorredor.estado" normalClass="normal"
								errorClass="error" errorImage="/img/information.gif" />
							</th>
							<td width="25%">
								<midas:estado styleId="estado" size="1" propiedad="estado"
									pais="PAMEXI" styleClass="cajaTexto"
									onchange="limpiarObjetos('municipio'); getMunicipios(this,'municipio');" />
							</td>
							<th>
								<etiquetas:etiquetaError requerido="si"
									property="idtcmunicipio" name="zonaCrestaForm"
									key="catalogos.zonacresta.municipio" normalClass="normal"
									errorClass="error" errorImage="/img/information.gif" />	
							</th>
							<td>
								<midas:municipio styleId="municipio" size="1" propiedad="idtcmunicipio"
									estado="estado" styleClass="cajaTexto"/>
							</td>
		  				</tr>
		  				<tr>
		 					<td colspan="3">&nbsp;</td>
		 					<td>	
		 						<div id="b_regresar">
									<a href="javascript: void(0);"
										onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/listar.do', 'contenido',null);">
										<bean:message key="midas.accion.regresar"/>
									</a>	
								</div>	
		 						<div id="b_guardar">	  				
									<a href="javascript: void(0);"
										onclick="javascript: sendRequest(document.zonaCrestaForm,'/MidasWeb/catalogos/zonacresta/modificar.do', 'contenido',null);">
										<bean:message key="midas.accion.guardar"/>
									</a>
								</div>
							</td>			  				
		  				</tr>
		  			</table>
		  		</td>
		  	</tr>
	  		<tr>
				<midas:mensajeUsuario/>
			</tr>
	  	</table>
    </midas:formulario>

