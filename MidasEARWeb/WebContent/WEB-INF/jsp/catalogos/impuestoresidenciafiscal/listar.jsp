<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/catalogos/impuestoresidenciafiscal/listar">
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>	
	</table>
	<div id="resultados">
		<midas:tabla idTabla="impuestosResidenciaFiscalTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.ImpuestoResidenciaFiscal"
			claseCss="tablaConResultados" nombreLista="impuestosResidenciaFiscal"
			urlAccion="/catalogos/impuestoresidenciafiscal/listar.do">	
			<midas:columna propiedad="descripcion" titulo="Descripción" />			
			<midas:columna propiedad="porcentaje" titulo="Porcentaje" />			
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
	<midas:oculto nombreFormulario="impuestoResidenciaFiscalForm" propiedadFormulario="idTcImpuestoResidenciaFiscal" />
</midas:formulario>