<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/impuestoresidenciafiscal/borrar">
	<table id="desplegar">		
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="midas.accion.borrar" />
			</td>
		</tr>
		<tr><th><midas:mensaje clave="catalogos.impuestoresidenciafiscal.descripcion" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcion" nombre="impuestoResidenciaFiscalForm"/></td>
			<td witdh="30%"></td>
			<th><midas:mensaje clave="catalogos.impuestoresidenciafiscal.porcentaje" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="porcentaje" nombre="impuestoResidenciaFiscalForm"/>%</td>
		</tr>
		<tr>
			<td class="regresar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
						<html:hidden property="idTcImpuestoResidenciaFiscal" name="impuestoResidenciaFiscalForm"/>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);"
							onclick="javascript: Confirma('<midas:mensaje clave="comun.confirmacionborrar" />', document.impuestoResidenciaFiscalForm,'/MidasWeb/catalogos/impuestoresidenciafiscal/borrar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.borrar" /> </a>
						<html:hidden property="idTcImpuestoResidenciaFiscal" name="impuestoResidenciaFiscalForm" />											   
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>