<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/impuestoresidenciafiscal/modificar">
	<table id="modificar">		
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.modificar" />
			</td>
		</tr>
		<tr>		
			<td>
				<etiquetas:etiquetaError property="descripcion" requerido="si"
					key="catalogos.impuestoresidenciafiscal.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcion" caracteres="50" />
			</td>					
			<td>
				<etiquetas:etiquetaError property="porcentaje" requerido="si"
					key="catalogos.impuestoresidenciafiscal.porcentaje" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<table>
					<tr>
						<td>
							<midas:texto propiedadFormulario="porcentaje" onkeypress="return soloNumeros(this, event, true);" />
						</td>
						<td>%</td>
					</tr>
				</table>
			</td>							
		</tr>
		<tr>
			<td class="campoRequerido" colspan="2">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="2">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.impuestoResidenciaFiscalForm,'/MidasWeb/catalogos/impuestoresidenciafiscal/modificar.do', 'contenido',null);"> <midas:mensaje clave="midas.accion.guardar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<midas:oculto nombreFormulario="impuestoResidenciaFiscalForm" propiedadFormulario="idTcImpuestoResidenciaFiscal" />
</midas:formulario>