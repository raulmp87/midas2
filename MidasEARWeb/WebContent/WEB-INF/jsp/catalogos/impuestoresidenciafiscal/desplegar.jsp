<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/impuestoresidenciafiscal/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="5">
						<midas:mensaje clave="midas.accion.detalle" />
					</td>
				</tr>	 			
	 			<tr><th><midas:mensaje clave="catalogos.impuestoresidenciafiscal.descripcion" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="descripcion" nombre="impuestoResidenciaFiscalForm"/></td>
					<td witdh="30%"></td>
					<th><midas:mensaje clave="catalogos.impuestoresidenciafiscal.porcentaje" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="porcentaje" nombre="impuestoResidenciaFiscalForm"/>%</td>
				</tr> 	 			 	 			 				
				<tr>   	  				
					<td class="regresar" colspan="5">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
								<html:hidden property="idTcImpuestoResidenciaFiscal" name="impuestoResidenciaFiscalForm"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>   	  				
		  	</table>
	  	</div>
    </midas:formulario>