<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/impuestoresidenciafiscal/agregar">
	<table id="agregar">
		<tr>
			<td width="10%"></td>
			<td width="30%"></td>
			<td width="10%"></td>
			<td width="10%"></td>
			<td width="10%"></td>
			<td width="30%"></td>
		</tr>
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.agregar" />				
			</td>			
		</tr>		
		<tr>
			<td>
				<etiquetas:etiquetaError property="descripcion" requerido="si"
					key="catalogos.impuestoresidenciafiscal.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="descripcion" caracteres="50" />
			</td>
			<td></td>		
			<td>
				<etiquetas:etiquetaError property="porcentaje" requerido="si"
					key="catalogos.impuestoresidenciafiscal.porcentaje" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="porcentaje" onkeypress="return soloNumeros(this, event, true);" />%
			</td>	
			<td></td>						
		</tr>							
		<tr>
			<td class="campoRequerido" colspan="5">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
					<a href="javascript: void(0);" onclick="javascript: sendRequest(document.impuestoResidenciaFiscalForm,'/MidasWeb/catalogos/impuestoresidenciafiscal/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>	
</midas:formulario>