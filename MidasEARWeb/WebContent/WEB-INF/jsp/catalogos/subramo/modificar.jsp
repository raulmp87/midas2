<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subramo/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
				<midas:oculto propiedadFormulario="idSubRamo"/>
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="catalogos.subramo.ramo"/>:
				<midas:escribe propiedad="descripcionRamo" nombre="subRamoForm"/>
			</th>
		</tr>
		<tr>
			
			<th><etiquetas:etiquetaError requerido="si" name="subRamoForm" key="catalogos.subramo.codigo" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif" property="codigo"/></th>
			<td><midas:texto propiedadFormulario="codigo" onkeypress="return soloNumeros(this, event, false)"/></td>
		</tr> 
		<tr>
			<th><etiquetas:etiquetaError requerido="si" name="subRamoForm" key="catalogos.subramo.porcentajeMaximoComisionRO" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif" property="porcentajeMaximoComisionRO"/></th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoComisionRO" caracteres="8" onkeypress="return soloNumeros(this, event, false)"/></td>
			
			<th><etiquetas:etiquetaError requerido="si" name="subRamoForm" key="catalogos.subramo.porcentajeMaximoComisionRCI" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif" property="porcentajeMaximoComisionRCI"/></th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoComisionRCI" caracteres="8" onkeypress="return soloNumeros(this, event, false)"/></td>
			
			<th><etiquetas:etiquetaError requerido="si" name="subRamoForm" key="catalogos.subramo.porcentajeMaximoComisionPRR" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif" property="porcentajeMaximoComisionPRR"/></th>
			<td><midas:texto propiedadFormulario="porcentajeMaximoComisionPRR" caracteres="8" onkeypress="return soloNumeros(this, event, false)"/></td>
		</tr>
		<tr>
			<th><etiquetas:etiquetaError property="idRamo" requerido="si" name="subRamoForm" normalClass="normal" errorClass="error" 
				key="catalogos.subramo.idRamo"
					errorImage="/img/information.gif" /></th>
			<td><midas:ramo styleId="idTcRamo" size="1" propiedad="idRamo" styleClass="cajaTexto"	/></td>
			
			<th><etiquetas:etiquetaError requerido="si" name="subRamoForm" key="catalogos.subramo.descripcion" 
				normalClass="normal" errorClass="error" errorImage="/img/information.gif" property="descripcion"/></th>
			<td colspan="3"><midas:texto propiedadFormulario="descripcion" caracteres="200" /></td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subramo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subRamoForm,'/MidasWeb/catalogos/subramo/modificar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
