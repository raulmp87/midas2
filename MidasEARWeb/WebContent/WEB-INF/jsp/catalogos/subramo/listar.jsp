<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>


<midas:formulario accion="/catalogos/subramo/listar">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="subRamoForm" property="totalRegistros"/>
	
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subramo.codigo"/></th>
			<td><midas:texto  caracteres="22" propiedadFormulario="codigo" onkeypress="return soloNumeros(this, event, false)"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subramo.porcentajeMaximoComisionRO"/></th>
			<td><midas:texto caracteres="8" propiedadFormulario="porcentajeMaximoComisionRO" onkeypress="return soloNumeros(this, event, false)"/></td>
			
			<th><midas:mensaje clave="catalogos.subramo.porcentajeMaximoComisionRCI"/></th> 
			<td><midas:texto caracteres="8" propiedadFormulario="porcentajeMaximoComisionRCI" onkeypress="return soloNumeros(this, event, false)"/></td>
			
			<th><midas:mensaje clave="catalogos.subramo.porcentajeMaximoComisionPRR"/></th> 
			<td><midas:texto  caracteres="8" propiedadFormulario="porcentajeMaximoComisionPRR" onkeypress="return soloNumeros(this, event, false)"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.subramo.descripcion"/></th> 
			<td colspan="2"><midas:texto  caracteres="200" propiedadFormulario="descripcion"/></td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subRamoForm,'/MidasWeb/catalogos/subramo/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	
	<div  id="resultados" style="overflow:auto;height:300px;">
	
		<midas:tabla idTabla="subRamosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Subramo"
			claseCss="tablaConResultados" nombreLista="subRamos"
			urlAccion="/catalogos/subramo/listarFiltradoPaginado.do"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="descripcionRamo" titulo="Ramo"/>
			<midas:columna propiedad="codigo" titulo="c&oacute;digo"/>
			<midas:columna propiedad="descripcion" titulo="Descripci&oacute;n"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>

		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/subramo/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
