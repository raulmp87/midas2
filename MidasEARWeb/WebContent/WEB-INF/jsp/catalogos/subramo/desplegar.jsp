<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/subramo/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
			</td>
		</tr> 
		<tr>
			<th colspan="2">
				<midas:mensaje clave="catalogos.subramo.ramo"/>:
				<midas:escribe propiedad="descripcionRamo" nombre="subRamoForm"/>
			</th>
		</tr>
		<tr>
			
			<th><midas:mensaje clave="catalogos.subramo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigo" nombre="subRamoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subramo.porcentajeMaximoComisionRO"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="porcentajeMaximoComisionRO" nombre="subRamoForm"/></td>
			
			<th><midas:mensaje clave="catalogos.subramo.porcentajeMaximoComisionRCI"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="porcentajeMaximoComisionRCI" nombre="subRamoForm"/></td>
			
			<th><midas:mensaje clave="catalogos.subramo.porcentajeMaximoComisionPRR"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="porcentajeMaximoComisionPRR" nombre="subRamoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.subramo.codigoRamo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoRamo" nombre="subRamoForm"/></td>
			
			<th><midas:mensaje clave="catalogos.subramo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcion" nombre="subRamoForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.subRamoForm,'/MidasWeb/catalogos/subramo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>