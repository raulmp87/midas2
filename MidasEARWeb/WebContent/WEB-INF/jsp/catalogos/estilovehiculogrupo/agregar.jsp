<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/estilovehiculogrupo/agregar">
 	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>
	
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.agregar" /> Estilo Veh&iacute;culo Grupo
			</td>
		</tr>
		<tr>	
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="id_claveTipoBien" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:comboCatalogo propiedad="id_claveTipoBien" 
					styleId="id_claveTipoBien" size="1" 
					styleClass="cajaTexto"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'idTcTipoVehiculo')"/>
			</td>
			<td colspan="4" width="66%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="idTcTipoVehiculo" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.tipoVehiculo"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:tipoVehiculo styleId="idTcTipoVehiculo" 
					size="1" propiedad="idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto"
					onchange="getMarcaVehiculos(this,'idTcMarcaVehiculo');" />
			</td>
			<td colspan="4" width="66%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="idTcMarcaVehiculo" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.marcaVehiculo"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:marcaVehiculo styleId="idTcMarcaVehiculo" 
					size="1" propiedad="idTcMarcaVehiculo"
					tipoVehiculo="idTcTipoVehiculo" 
					styleClass="cajaTexto"
					onchange="getVersionCarga(this,'id_claveTipoBien','idTcTipoVehiculo','id_idVersionCarga');"
				/>
			</td>
			<td colspan="4" width="66%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="id_idVersionCarga" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto" 
					tipoBien="id_claveTipoBien" 
					marcaVehiculo="idTcMarcaVehiculo" 
					tipoVehiculo="idTcTipoVehiculo"
					onchange="getEstiloVehiculos(this,'idTcTipoVehiculo','idTcMarcaVehiculo','comboEstiloVehiculo');"
				/>		
			</td>
			<td colspan="4" width="66%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si"
					property="idCompuestoTcVehiculo" name="estiloVehiculoGrupoForm"
					key="catalogos.modelovehiculo.id_estiloVehiculo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  	
			</th>
			<td class="fondoCajaTexto">
				<midas:estiloVehiculo styleId="comboEstiloVehiculo" size="1" 
					propiedad="idCompuestoTcVehiculo" styleClass="cajaTexto"
					tipoVehiculo="idTcTipoVehiculo" 
					marcaVehiculo="idTcMarcaVehiculo" 
					versionCarga="id_idVersionCarga"  
				/>
			</td>
			<td colspan="4" width="66%">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="claveAMIS" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.claveAMIS" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td class="fondoCajaTexto">
				<midas:texto caracteres="8" propiedadFormulario="claveAMIS" 
					onkeypress="return soloAlfanumericos(this, event, false)"/>
			</td>
			<td colspan="4" width="66%">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" property="fechaInicioVigencia" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.fechaInicioVigencia" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</td>
			<td align="left" width="20%">
				<midas:texto propiedadFormulario="fechaInicioVigencia"  
					id="fechaInicial" soloLectura="true"/>				
			</td>
			<td>
				<div id="b_calendario">
					<a href="#" id="mostrarCalendario" 
						onclick="javascript: mostrarCalendarioDobleLinea()"></a>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si" property="fechaFinVigencia" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.fechaFinVigencia" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</td>
			<td>
				<midas:texto propiedadFormulario="fechaFinVigencia"  
					id="fechaFinal" soloLectura="true"/>
			</td> 
			<td width="10%">
				<div id="rangoDeFechas" style="position:absolute;z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="calendarioDer"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="idGrupoDM" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.idGrupoDM" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td class="fondoCajaTexto">
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" 
					caracteres="6" 
					propiedadFormulario="idGrupoDM"/>
			</td>
			<td colspan="4" width="66%"></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="idGrupoRT" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.idGrupoRT" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td class="fondoCajaTexto">
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" 
					caracteres="6" 
					propiedadFormulario="idGrupoRT"/>
			</td>
			<td colspan="4" width="66%"></td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="si" property="idGrupoRC" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.idGrupoRC" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" 
					caracteres="6" 
					propiedadFormulario="idGrupoRC"/>
			</td>
			<td colspan="4" width="66%"></td>
		</tr>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
			<div id="b_guardar" style="margin-right: 4px">
			   <a onclick="javascript: sendRequest(document.estiloVehiculoGrupoForm,'/MidasWeb/catalogos/estilovehiculogrupo/agregar.do', 'contenido','existenErrores(\'listarEstilos()\')');" href="javascript: void(0);"> Guardar</a>
			</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculogrupo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="6">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
	</table>
<div id="errores" style="display: inline;"><html:errors/></div>
</midas:formulario>

