<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/estilovehiculogrupo/listar">
	<table id="desplegar" border="0">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.detalle" /> Estilo Veh&iacute;culo Grupo
				<midas:oculto propiedadFormulario="id_claveEstilo" nombreFormulario="estiloVehiculoGrupoForm" /> 
			</td>
		</tr>
		<tr>	
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="id_claveTipoBien" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.id_claveTipoBien"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:comboCatalogo propiedad="id_claveTipoBien" 
					styleId="id_claveTipoBien" size="1" 
					styleClass="cajaTexto"
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					readonly="true"
					descripcionCatalogo="descripcionTipoBien" 
					onchange="getTipoVehiculos(this,'idTcTipoVehiculo')"/>
			</td>
			<td colspan="4" width="60%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="idTcTipoVehiculo" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.tipoVehiculo"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:tipoVehiculo styleId="idTcTipoVehiculo" 
					size="1" propiedad="idTcTipoVehiculo"
					tipoBien="id_claveTipoBien" styleClass="cajaTexto"
					readonly="true"
					onchange="getMarcaVehiculos(this,'idTcMarcaVehiculo');" />
			</td>
			<td colspan="4" width="60%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="idTcMarcaVehiculo" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.marcaVehiculo"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:marcaVehiculo styleId="idTcMarcaVehiculo" 
					size="1" propiedad="idTcMarcaVehiculo"
					tipoVehiculo="idTcTipoVehiculo" 
					styleClass="cajaTexto"
				    readonly="true"
					onchange="getVersionCarga(this,'id_claveTipoBien','idTcTipoVehiculo','id_idVersionCarga');"
				/>
			</td>
			<td colspan="4" width="60%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError requerido="no" 
					property="id_idVersionCarga" name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculo.id_idVersionCarga"
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td class="fondoCajaTexto">
				<midas:versionCarga styleId="id_idVersionCarga" size="1" 
					propiedad="id_idVersionCarga" styleClass="cajaTexto" 
					tipoBien="id_claveTipoBien" 
					marcaVehiculo="idTcMarcaVehiculo" 
					readonly="true"
					tipoVehiculo="idTcTipoVehiculo"
					onchange="getEstiloVehiculos(this,'idTcTipoVehiculo','idTcMarcaVehiculo','comboEstiloVehiculo');"
				/>		
			</td>
			<td colspan="4" width="60%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="idCompuestoTcVehiculo" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.modelovehiculo.id_estiloVehiculo" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td colspan="2">
				<midas:escribe propiedad="descripcionEstiloVehiculo" 
					nombre="estiloVehiculoGrupoForm"/>
			</td>
			<td colspan="3" >&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="claveAMIS" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.claveAMIS" 
					normalClass="normal" errorClass="error" 
					errorImage="/img/information.gif"/>		  					
			</th>
			<td class="fondoCajaTexto">
				<midas:texto caracteres="8" propiedadFormulario="claveAMIS" 
					deshabilitado="true" />
			</td>
			<td colspan="4" width="60%">&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="fechaInicioVigencia" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.fechaInicioVigencia" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td align="left" width="20%">
				<midas:texto propiedadFormulario="fechaInicioVigencia"  
					id="fechaInicial" soloLectura="true"/>				
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="fechaFinVigencia" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.fechaFinVigencia" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td>
				<midas:texto propiedadFormulario="fechaFinVigencia"  
					id="fechaFinal" soloLectura="true"/>
			</td>
			<td colspan="2" >&nbsp;</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="idGrupoDM" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.idGrupoDM" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td>
				<midas:texto onkeypress="return soloNumeros(this, event, false)" 
					caracteres="6" 
					propiedadFormulario="idGrupoDM" deshabilitado="true" />
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="idGrupoRT" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.idGrupoRT" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td>
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" 
					caracteres="6" propiedadFormulario="idGrupoRT" 
					deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<th>
				<etiquetas:etiquetaError property="idGrupoRC" 
					name="estiloVehiculoGrupoForm"
					key="catalogos.estilovehiculogrupo.idGrupoRC" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>
			</th>
			<td>
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" 
					caracteres="6" 
					propiedadFormulario="idGrupoRC" deshabilitado="true"/>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculogrupo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar" />
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>