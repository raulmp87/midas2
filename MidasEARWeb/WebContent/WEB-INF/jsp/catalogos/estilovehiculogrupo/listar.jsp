<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>

<midas:formulario  accion="/catalogos/estilovehiculogrupo/listar">
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Estilo Veh&iacute;culo Grupo
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="catalogos.estilovehiculogrupo.claveAMIS"/>		  					
			</th>
			<td >
				<midas:texto caracteres="8" propiedadFormulario="claveAMIS" onkeypress="return soloAlfanumericos(this, event, false)"/>
			</td>
			<th>
				<midas:mensaje clave="catalogos.estilovehiculogrupo.idGrupoDM"/>
			</th>
			<td>
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" caracteres="6" 
					propiedadFormulario="idGrupoDM"/>
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="catalogos.estilovehiculogrupo.idGrupoRT"/>		  					
			</th>
			<td>
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" caracteres="6" 
					propiedadFormulario="idGrupoRT"/>
			</td>
			<th>
				<midas:mensaje clave="catalogos.estilovehiculogrupo.idGrupoRC"/>
			</th>
			<td>
				<midas:texto  onkeypress="return soloNumeros(this, event, false)" caracteres="6" 
					propiedadFormulario="idGrupoRC"/>
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.estiloVehiculoGrupoForm,'/MidasWeb/catalogos/estilovehiculogrupo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
<!-- 		<tr> -->
<%-- 			<midas:mensajeUsuario/> --%>
<!-- 		</tr> -->
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listEstiloVehiculoGrupo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorEstiloVehiculoGrupo"
			claseCss="tablaConResultados" nombreLista="listEstiloVehiculoGrupo"
			urlAccion="/catalogos/estilovehiculogrupo/listarFiltrado.do">
			<midas:columna propiedad="id.claveTipoBien" titulo="Clave Tipo Bien" />
			<midas:columna propiedad="id.claveEstilo" titulo="Clave Estilo" />
			<midas:columna propiedad="id.idVersionCarga" titulo="Version Carga" />
			<midas:columna propiedad="claveAMIS" titulo="Clave AMIS" />
			<midas:columna propiedad="idGrupoDM" titulo="Grupo DM" />
			<midas:columna propiedad="idGrupoRT" titulo="Grupo RT" />
			<midas:columna propiedad="idGrupoRC" titulo="Grupo RC" />
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
		</midas:tabla>		
	</div>

	<div class="alinearBotonALaDerecha" style="margin-right: 20px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculogrupo/mostrarAgregar.do', 'contenido','manipulaCalendarioLineas()');">
				<midas:mensaje clave="midas.accion.agregar"/>
			</a>
		</div>
	</div>

</midas:formulario>
