<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipovehiculo/modificar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" /> Tipo Veh&iacute;culo
				<midas:oculto propiedadFormulario="idTcTipoVehiculo"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="tipoBienAutosForm.claveTipoBien" 
					name="tipoVehiculoForm"
					key="catalogos.tipovehiculo.clavetipobien" 
					normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:comboCatalogo propiedad="tipoBienAutosForm.claveTipoBien" 
					styleId="" size="" styleClass="cajaTexto w200" 
					nombreCatalogo="tctipobienautos" idCatalogo="claveTipoBien" 
					descripcionCatalogo="descripcionTipoBien" />
				<html:hidden property="tipoBienAutosForm.claveTipoBien"/>
			</td>
			<td colspan="2" style="width:50%">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="codigoTipoVehiculo" name="tipoVehiculoForm"
					key="catalogos.tipovehiculo.codigo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text property="codigoTipoVehiculo" maxlength="5"
					onkeypress="return soloAlfanumericos(this, event, false)"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionTipoVehiculo" name="tipoVehiculoForm"
					key="catalogos.tipovehiculo.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text property="descripcionTipoVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.tipoVehiculoForm,'/MidasWeb/catalogos/tipovehiculo/modificar.do', 'contenido','validaGuardarModificarM1()');">
							<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipovehiculo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
<div id="errores" style="display: none;"><html:errors/></div>