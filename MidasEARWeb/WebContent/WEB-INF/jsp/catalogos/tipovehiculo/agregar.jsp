<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

  <midas:formulario accion="/catalogos/tipovehiculo/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" /> Tipo Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="tipoBienAutosForm.claveTipoBien" name="tipoVehiculoForm"
					key="catalogos.tipovehiculo.clavetipobien" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:escribeCatalogo size="" styleId="" styleClass="cajaTexto w200"
				propiedad="tipoBienAutosForm.claveTipoBien" 
				clase="mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosFacadeRemote"/>
			</td>
			<td colspan="2" style="width:50%">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="codigoTipoVehiculo" name="tipoVehiculoForm"
					key="catalogos.tipovehiculo.codigo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text property="codigoTipoVehiculo" maxlength="5"
					onkeypress="return soloAlfanumericos(this, event, false)"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" />
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionTipoVehiculo" name="tipoVehiculoForm"
					key="catalogos.tipovehiculo.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<html:text property="descripcionTipoVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto w200" 
				/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_guardar" style="margin-right: 4px">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(document.tipoVehiculoForm,'/MidasWeb/catalogos/tipovehiculo/agregar.do', 'contenido','validaGuardarModificarM1()');">
							<midas:mensaje clave="midas.accion.guardar"/>
						</a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" 
							onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipovehiculo/listar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.regresar"/>
						</a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="4">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
		</tr>
		<tr>
			<td colspan="4">
				<midas:mensajeUsuario/>
			</td>
		</tr>
	</table>
</midas:formulario>
<div id="errores" style="display: none;"><html:errors/></div>