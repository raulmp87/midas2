<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/tipovehiculo/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" /> Tipo Veh&iacute;culo
				<midas:oculto propiedadFormulario="idTcTipoVehiculo"/>
				<midas:oculto propiedadFormulario="tipoBienAutosForm.claveTipoBien"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipovehiculo.clavetipobien"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="tipoBienAutosForm.descripcionTipoBien"  nombre="tipoVehiculoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipovehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoTipoVehiculo"  nombre="tipoVehiculoForm"/></td>
			<th><midas:mensaje clave="catalogos.tipovehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoVehiculo" nombre="tipoVehiculoForm"/></td>
		</tr>
		<tr>
			<td class= "guardar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_borrar" style="margin-right: 4px">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.tipoVehiculoForm,'/MidasWeb/catalogos/tipovehiculo/borrar.do', 'contenido','validaBorrarM1()');"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoVehiculoForm,'/MidasWeb/catalogos/tipovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>