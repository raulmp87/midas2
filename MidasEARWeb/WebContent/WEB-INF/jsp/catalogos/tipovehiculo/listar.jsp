<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario  accion="/catalogos/tipovehiculo/listar">
	<table width="97%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Tipo Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipovehiculo.clavetipobien"/>:</th>
			<td>
				<midas:comboCatalogo propiedad="tipoBienAutosForm.claveTipoBien" 
					styleId="idTipoBien" size="1" idCatalogo="claveTipoBien"
				 	styleClass="cajaTexto" nombreCatalogo="tctipobienautos"  
				 	descripcionCatalogo="descripcionTipoBien" 
				 	onchange="getTipoVehiculos(this,'idTcTipoVehiculo');"/>				
			</td>
		</tr>		
		<tr>
			<th><midas:mensaje clave="catalogos.tipovehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto">
				<midas:texto caracteres="5" 
					propiedadFormulario="codigoTipoVehiculo" 
					onkeypress="return soloAlfanumericos(this, event, false)" />
				</td>
			<th><midas:mensaje clave="catalogos.tipovehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto">
				<html:text property="descripcionTipoVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.tipoVehiculoForm,'/MidasWeb/catalogos/tipovehiculo/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listTipoVehiculo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorTipoVehiculo"
			claseCss="tablaConResultados" nombreLista="listTipoVehiculo"
			urlAccion="/catalogos/tipovehiculo/listarFiltrado.do">
			<%--<midas:columna propiedad="descripcionTipoVehiculo" titulo="Tipo Veh&iacute;culo" />  --%> 
			<midas:columna propiedad="tipoBienAutosDTO.descripcionTipoBien" titulo="Tipo Bien" />
			<midas:columna propiedad="codigoTipoVehiculo" titulo="C&oacute;digo" />
			<midas:columna propiedad="descripcionTipoVehiculo" titulo="Descripci&oacute;n" />
			<midas:columna propiedad="acciones" titulo="Acciones" estilo="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipovehiculo/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
