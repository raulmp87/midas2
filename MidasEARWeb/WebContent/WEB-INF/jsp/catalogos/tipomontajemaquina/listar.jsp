<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/catalogos/tipomontajemaquina/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="codigoTipoMontajeMaq" name="tipoMontajeMaquinaForm"
					key="catalogos.tipomontajemaquina.id" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				onkeypress="return soloNumeros(this, event, false)" caracteres="100" 
				propiedadFormulario="codigoTipoMontajeMaq"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError requerido="si"
					property="descripcionTipoMontajeMaq" name="tipoMontajeMaquinaForm"
					key="catalogos.tipomontajemaquina.descripcion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif"/>		  					
			</td>
			<td>
				<midas:texto 
				caracteres="200" 
				propiedadFormulario="descripcionTipoMontajeMaq"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoMontajeMaquinaForm,'/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>	
		<tr>
			<midas:mensajeUsuario/>
		</tr>	
	</table>
	<div id="resultados">
		<midas:tabla idTabla="tipoMontajeMaquinaTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.TipoMontajeMaquina"
			claseCss="tablaConResultados" nombreLista="montajes"
			urlAccion="/catalogos/tipomontajemaquina/listar.do">
			<midas:columna propiedad="codigotipomontajemaq" titulo="ID Tipo de Montaje de Maquina"/>
			<midas:columna propiedad="descripciontipomontajemaq" titulo="Descripcion"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipomontajemaquina/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
