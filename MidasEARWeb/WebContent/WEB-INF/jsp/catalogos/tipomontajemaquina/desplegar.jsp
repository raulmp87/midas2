<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/tipomontajemaquina/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
				<html:hidden property="idTcTipoMontajeMaq"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.tipomontajemaquina.id"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoTipoMontajeMaq"  nombre="tipoMontajeMaquinaForm"/></td>
			<th><midas:mensaje clave="catalogos.tipomontajemaquina.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoMontajeMaq" nombre="tipoMontajeMaquinaForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoMontajeMaquinaForm,'/MidasWeb/catalogos/tipomontajemaquina/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>