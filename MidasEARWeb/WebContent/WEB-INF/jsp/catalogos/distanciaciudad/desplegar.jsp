<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

    <midas:formulario accion="/catalogos/distanciaciudad/listar">
    	<div id="centrarDesplegar">
		  	<table id="desplegar">
				<tr>
					<td class="titulo" colspan="4">
						<midas:mensaje clave="midas.accion.detalle" />
					</td>
				</tr>
	 			<tr>
	 				<th><midas:mensaje clave="catalogos.distanciaciudad.estadoOrigen" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="estadoOrigenDescripcion" nombre="distanciaCiudadForm"/></td>
	 				<th><midas:mensaje clave="catalogos.distanciaciudad.estadoDestino" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="estadoDestinoDescripcion" nombre="distanciaCiudadForm"/></td>					
	 			</tr> 	 				 			 
	 			<tr>
					<th><midas:mensaje clave="catalogos.distanciaciudad.ciudadOrigen" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="ciudadOrigenDescripcion" nombre="distanciaCiudadForm"/></td>
					<th><midas:mensaje clave="catalogos.distanciaciudad.ciudadDestino" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="ciudadDestinoDescripcion" nombre="distanciaCiudadForm"/></td>					
	 			</tr>
	 			<tr><th><midas:mensaje clave="catalogos.distanciaciudad.distancia" />:</th>
					<td class="fondoCajaTexto"><midas:escribe propiedad="distancia" nombre="distanciaCiudadForm"/></td>
					<td colspan="2"></td>
				</tr> 	 			 	 			 				
				<tr>   	  				
					<td class="regresar" colspan="4">
						<div class="alinearBotonALaDerecha">
							<div id="b_regresar">
								<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
								<html:hidden property="idDistanciaCiudad" name="distanciaCiudadForm"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<midas:mensajeUsuario/>
				</tr>   	  				
		  	</table>
	  	</div>
    </midas:formulario>
