<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/distanciaciudad/modificar">
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="estadoOrigen" requerido="si"
					key="catalogos.distanciaciudad.estadoOrigen" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:estado styleId="estadoOrigen" size="1" propiedad="estadoOrigen"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudadOrigen'); getCiudades(this,'ciudadOrigen');" />
			</td>
			<td>
				<etiquetas:etiquetaError property="estadoDestino" requerido="si"
					key="catalogos.distanciaciudad.estadoDestino" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:estado styleId="estado" size="1" propiedad="estadoDestino"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudadDestino'); getCiudades(this,'ciudadDestino');" />
			</td>					
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="ciudadOrigen" requerido="si"
					key="catalogos.distanciaciudad.ciudadOrigen" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:ciudad styleId="ciudadOrigen" size="1" propiedad="ciudadOrigen"
					estado="estadoOrigen" styleClass="cajaTexto" />
			</td>
			<td>
				<etiquetas:etiquetaError property="ciudadDestino" requerido="si"
					key="catalogos.distanciaciudad.ciudadDestino" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:ciudad styleId="ciudadDestino" size="1" propiedad="ciudadDestino"
					estado="estadoDestino" styleClass="cajaTexto" />
			</td>
		</tr>
		<tr>	
			<td>
				<etiquetas:etiquetaError property="distancia" requerido="si"
					key="catalogos.distanciaciudad.distancia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="distancia"
					onkeypress="return soloNumeros(this, event, false)" /> km.
			</td>
			<td colspan="2"></td>							
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.distanciaCiudadForm,'/MidasWeb/catalogos/distanciaciudad/modificar.do', 'contenido',null);"> <midas:mensaje clave="midas.accion.guardar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<midas:oculto nombreFormulario="distanciaCiudadForm" propiedadFormulario="idDistanciaCiudad" />
</midas:formulario>