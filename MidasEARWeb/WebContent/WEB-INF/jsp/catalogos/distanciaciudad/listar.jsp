<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario  accion="/catalogos/distanciaciudad/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		
		<tr>			
			<th><midas:mensaje clave="catalogos.distanciaciudad.estadoOrigen"/>:</th>
			<td>
				<midas:estado styleId="estadoOrigen" size="1" propiedad="estadoOrigen"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudadOrigen'); getCiudades(this,'ciudadOrigen');" />
			</td>
			<th><midas:mensaje clave="catalogos.distanciaciudad.estadoDestino"/>:</th>			
			<td>
				<midas:estado styleId="estadoDestino" size="1" propiedad="estadoDestino"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudadDestino'); getCiudades(this,'ciudadDestino');" />
			</td>			
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.distanciaciudad.ciudadOrigen"/>:</th>			
			<td>
				<midas:ciudad styleId="ciudadOrigen" size="1" propiedad="ciudadOrigen"
					estado="estadoOrigen" styleClass="cajaTexto" />
			</td>
			<th><midas:mensaje clave="catalogos.distanciaciudad.ciudadDestino"/>:</th>			
			<td>
				<midas:ciudad styleId="ciudadDestino" size="1" propiedad="ciudadDestino"
					estado="estadoDestino" styleClass="cajaTexto" />
			</td>									
		</tr>
		 		
		<tr>
			<td class= "buscar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.distanciaCiudadForm,'/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="distanciaCiudadesTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.DistanciaCiudad"
			claseCss="tablaConResultados" nombreLista="distanciaCiudades"
			urlAccion="/catalogos/distanciaciudad/listar.do">			
			<midas:columna propiedad="estadoOrigenDescripcion" titulo="Estado origen" />			
			<midas:columna propiedad="ciudadOrigenDescripcion" titulo="Ciudad origen" />			
			<midas:columna propiedad="estadoDestinoDescripcion" titulo="Estado destino" />			
			<midas:columna propiedad="ciudadDestinoDescripcion" titulo="Ciudad destino" />
			<midas:columna propiedad="distancia" titulo="Distancia" />
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
	<midas:oculto nombreFormulario="distanciaCiudadForm" propiedadFormulario="idDistanciaCiudad" />
</midas:formulario>
