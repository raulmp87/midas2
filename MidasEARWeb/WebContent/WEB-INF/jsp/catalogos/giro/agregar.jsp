<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/giro/agregar">
	<table id="agregar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.agregar" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="codigoGiro"
					name="giroForm"
					key="catalogos.giro.codigoGiro" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
			</td>
			<td><midas:texto onkeypress="return soloNumeros(this, event, false)" propiedadFormulario="codigoGiro"/></td>
		</tr> 
		<tr>
			<td>
				<etiquetas:etiquetaError 
					requerido="si" 
					property="descripcionGiro" 
					name="giroForm" 
					key="catalogos.giro.descripcionGiro" 
					normalClass="normal" 
					errorClass="error" 
					errorImage="/img/information.gif"/>
				</td>
			<td colspan="2"> <midas:texto  onkeypress="return soloLetras(this, event, false)" caracteres="200" 
				propiedadFormulario="descripcionGiro"/></td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/giro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroForm,'/MidasWeb/catalogos/giro/agregar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.guardar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:escribe propiedad="mensaje" nombre="giroForm"/>
		</tr>
	</table>
</midas:formulario>
