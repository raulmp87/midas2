<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/giro/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.giro.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.giro.idTcGiro"/>:</th>
			<td><midas:texto propiedadFormulario="idTcGiro" caracteres="22" /> </td>
			
			<th><midas:mensaje clave="catalogos.giro.codigoGiro"/>:</th>
			<td><midas:texto propiedadFormulario="codigoGiro" caracteres="22" /> </td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.giro.descripcionGiro"/>:</th>
			<td colspan="2"><midas:texto propiedadFormulario="descripcionGiro" onkeypress="return soloLetras(this, event, false)" caracteres="200" /> </td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroForm,'/MidasWeb/catalogos/giro/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="girosTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Giro"
			claseCss="tablaConResultados" nombreLista="giros"
			urlAccion="/catalogos/giro/listar.do">
			<midas:columna propiedad="idTcGiro" titulo="C&oacute;digo"/>
			<midas:columna propiedad="descripcionGiro" titulo="Descripci&oacute;n"/>
			<midas:columna propiedad="acciones" titulo="Acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/giro/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
