<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/giro/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.detalle" />
				<midas:oculto propiedadFormulario="idTcGiro"/>
			</td>
		</tr> 
		<tr>
			<th> <midas:mensaje clave="catalogos.giro.codigoGiro"/> </th>
			<td class="fondoCajaTexto"> <midas:escribe propiedad="codigoGiro" nombre="giroForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.giro.descripcionGiro"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionGiro" nombre="giroForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="2">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroForm,'/MidasWeb/catalogos/giro/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>