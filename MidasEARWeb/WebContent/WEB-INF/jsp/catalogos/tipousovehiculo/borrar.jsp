<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/tipousovehiculo/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" /> Tipo Uso Veh&iacute;culo
				<midas:oculto propiedadFormulario="idTcTipoUsoVehiculo"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.idTcTipoVehiculo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="tipoVehiculoDTO.descripcionTipoVehiculo"  nombre="tipoUsoVehiculoForm"/></td>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.claveAmisTipoServicio"/>:</th>
			<td class="fondoCajaTexto" id="tipoServicioSapAmis"><midas:escribe propiedad="claveAmisTipoServicio" nombre="tipoUsoVehiculoForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoTipoUsoVehiculo"  nombre="tipoUsoVehiculoForm"/></td>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionTipoUsoVehiculo" nombre="tipoUsoVehiculoForm"/></td>
		</tr>
		<tr>
			<td class= "guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_borrar" style="margin-right: 4px">
						<a href="javascript: void(0);" onclick="javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',document.tipoUsoVehiculoForm,'/MidasWeb/catalogos/tipousovehiculo/borrar.do', 'contenido','validaBorrarM1()');"><midas:mensaje clave="midas.accion.borrar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.tipoUsoVehiculoForm,'/MidasWeb/catalogos/tipousovehiculo/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>