<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<midas:formulario  accion="/catalogos/tipousovehiculo/listar">
	<table width="98%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/> Tipo Uso Veh&iacute;culo
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.idTcTipoVehiculo"/>:</th>
			<td class="fondoCajaTexto">
				<midas:escribeCatalogo size="" styleId="" styleClass="cajaTexto"
				propiedad="idTcTipoVehiculo" 
				clase="mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.codigo"/>:</th>
			<td class="fondoCajaTexto">
				<midas:texto caracteres="5" propiedadFormulario="codigoTipoUsoVehiculo" 
				onkeypress="return soloAlfanumericos(this, event, false)" /></td>
			<th><midas:mensaje clave="catalogos.tipousovehiculo.descripcion"/>:</th>
			<td class="fondoCajaTexto">
				<html:text property="descripcionTipoUsoVehiculo" maxlength="100"
					styleClass="jQalphaextra jQrestrict cajaTexto" 
				/>
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.tipoUsoVehiculoForm,'/MidasWeb/catalogos/tipousovehiculo/listarFiltrado.do', 'contenido', null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	<div id="resultados">
		<midas:tabla idTabla="listTipoUsoVehiculo"
			claseDecoradora="mx.com.afirme.midas.decoradores.DecoradorTipoUsoVehiculo"
			claseCss="tablaConResultados" nombreLista="listTipoUsoVehiculo"
			urlAccion="/catalogos/tipousovehiculo/listarFiltrado.do">
			<midas:columna propiedad="tipoVehiculoDTO.descripcionTipoVehiculo" titulo="Tipo Veh&iacute;culo" />
			<midas:columna propiedad="codigoTipoUsoVehiculo" titulo="C&oacute;digo" />
			<midas:columna propiedad="descripcionTipoUsoVehiculo" titulo="Descripci&oacute;n" />
			<midas:columna propiedad="acciones" titulo="Acciones" estilo="acciones" />
		</midas:tabla>
	</div>

	<div class="alinearBotonALaDerecha" style="margin-right: 20px">
		<div id="b_agregar">
			<a href="javascript: void(0);"
				onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/tipousovehiculo/mostrarAgregar.do', 'contenido', 'actualizacionUnitaria(2)');">
				<midas:mensaje clave="midas.accion.agregar"/>
			</a>
		</div>
	</div>

</midas:formulario>
