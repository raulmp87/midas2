<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/girorc/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="catalogos.girorc.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.girorc.codigoGiroRC"/>:</th>
			<td><midas:texto propiedadFormulario="codigoGiroRC" caracteres="22" /> </td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.girorc.descripcionGiroRC"/>:</th>
			<td colspan="2"><midas:texto propiedadFormulario="descripcionGiroRC" onkeypress="return soloLetras(this, event, false)" caracteres="200" /> </td>
		</tr> 
		<tr>
			<td class= "buscar" colspan="5">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">		
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroRCForm,'/MidasWeb/catalogos/girorc/listarFiltrado.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.filtrar"/></a>
					</div>
				</div>
			</td>      	   		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="girosRCTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.GiroRC"
			claseCss="tablaConResultados" nombreLista="girosRC"
			urlAccion="/catalogos/girorc/listar.do">
			<midas:columna propiedad="idTcGiroRC" titulo="Codigo"/>
			<midas:columna propiedad="codigoGiroRC"/>
			<midas:columna propiedad="descripcionGiroRC"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/girorc/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</midas:formulario>
