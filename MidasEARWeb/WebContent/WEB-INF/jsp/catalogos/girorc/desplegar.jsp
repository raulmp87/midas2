<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/girorc/listar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="catalogos.girorc.detalle" />
				<midas:oculto propiedadFormulario="idTcGiroRC"/>
			</td>
		</tr> 
		<tr>
			<th> <midas:mensaje clave="catalogos.girorc.codigoGiroRC"/> </th>
			<td class="fondoCajaTexto"> <midas:escribe propiedad="codigoGiroRC" nombre="giroRCForm"/></td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.girorc.descripcionGiroRC"/>:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="descripcionGiroRC" nombre="giroRCForm"/></td>
		</tr>
		<tr>
			<td class="regresar" colspan="2">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.giroRCForm,'/MidasWeb/catalogos/girorc/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>