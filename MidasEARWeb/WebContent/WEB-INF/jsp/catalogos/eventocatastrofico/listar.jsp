<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/eventocatastrofico/listar">
	<table width="90%" id="filtros" border="0">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th align="right"><midas:mensaje clave="catalogos.eventocatastrofico.codigo" />:</th>
			<td align="left"><midas:texto caracteres="7" propiedadFormulario="codigoEvento" onkeypress="return soloNumeros(this, event, true);"/> </td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<th align="right"><midas:mensaje clave="catalogos.eventocatastrofico.descripcion" />:</th>
			<td align="left"><midas:texto propiedadFormulario="descripcionEvento" caracteres="200" /> </td>
			<td colspan="2">&nbsp;</td>
		</tr> 
		<tr>
			<td colspan="3">&nbsp;</td>
			<td>
				<midas:boton onclick="buscarEventoCatastrofico()" tipo="buscar" />
			</td>      	   		
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>		
	</table>
	<div id="resultados" style="width: 90%">
		<midas:tabla idTabla="idListaEventosCatastroficos"
			claseDecoradora="mx.com.afirme.midas.decoradores.EventoCatastrofico"
			claseCss="tablaConResultados" nombreLista="listaEventosCatastroficos"
			urlAccion="/catalogos/eventocatastrofico/listar.do">
			<midas:columna propiedad="codigoEvento" titulo="Codigo"/>
			<midas:columna propiedad="descripcionEvento" titulo="Descripcion"/>
			<midas:columna propiedad="acciones" titulo="Acciones" />
		</midas:tabla>
	</div>
		<div id="resultados" style="width: 90%">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/eventocatastrofico/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
			</div>
		</div>
</midas:formulario>
