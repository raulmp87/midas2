<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/eventocatastrofico/mostrarModificar">
	<table id="agregar" border="0">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="midas.accion.modificar" />
				<midas:oculto propiedadFormulario="idTcEventoCatastrofico"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.eventocatastrofico.codigo" /> * </th>
			<td>
				<midas:texto caracteres="7" propiedadFormulario="codigoEvento" onkeypress="return soloNumeros(this, event, true);"/>
			</td>
			<td  colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.eventocatastrofico.descripcion" /> * </th>
			<td colspan="2">
				<midas:texto caracteres="200" propiedadFormulario="descripcionEvento"/>
			</td>
			<td  colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="2">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td  colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
			<td width="10%">
				<midas:boton onclick="regresaListadoEventosCatastroficos()" tipo="regresar"/>
			</td>
			<td>
					<midas:boton onclick="modificarEventoCatastrofico()" tipo="guardar" texto="Modificar"/>
			</td>
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>
