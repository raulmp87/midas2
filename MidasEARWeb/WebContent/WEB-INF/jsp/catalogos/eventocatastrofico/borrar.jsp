<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/eventocatastrofico/mostrarModificar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="midas.accion.borrar" />
				<midas:oculto propiedadFormulario="idTcEventoCatastrofico"/>
			</td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.eventocatastrofico.codigo" /></th>
			<td>
				<midas:texto caracteres="200" propiedadFormulario="codigoEvento" deshabilitado="true"/>
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.eventocatastrofico.descripcion" /></th>
			<td colspan="2">
				<midas:texto caracteres="200" propiedadFormulario="descripcionEvento" deshabilitado="true"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
			<td width="10%">
				<midas:boton onclick="regresaListadoEventosCatastroficos()" tipo="regresar"/>
			</td>
			<td>
					<midas:boton onclick="borrarEventoCatastrofico()" tipo="guardar" texto="Borrar"/>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
</midas:formulario>