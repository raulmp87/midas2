<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"  %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

 <midas:formulario accion="/catalogos/eventocatastrofico/mostrarModificar">
	 <div id="centrarDesplegar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="5">
				<midas:mensaje clave="midas.accion.detalle" />
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.eventocatastrofico.codigo" />:</th>
			<td>
				<midas:texto caracteres="200" propiedadFormulario="codigoEvento" deshabilitado="true"/>
			</td>
			<td  colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.eventocatastrofico.descripcion" />: </th>
			<td colspan="2">
				<midas:texto caracteres="200" propiedadFormulario="descripcionEvento" deshabilitado="true"/>
			</td>
			<td  colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
			<td width="10%">&nbsp;</td>
			<td class="guardar" width="20%">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/eventocatastrofico/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
				</div>
			</td> 
		</tr>
		<tr>
			<midas:mensajeUsuario/>
		</tr>
	</table>
	</div>
</midas:formulario>
