<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/ajustador/modificar">
	<midas:oculto propiedadFormulario="idTcAjustador"/>
	<table id="modificar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.modificar" /> <midas:escribe nombre="ajustadorForm" propiedad="razonSocial"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="nombre" requerido="si"
					key="catalogos.ajustador.nombre" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="nombre" 
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="20"/>
			</td>			
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="rfc" requerido="si" 
					key="catalogos.ajustador.rfc" normalClass="normal" 
					errorClass="error" errorImage="/img/information.gif" /></td>
			<td>
				<midas:texto propiedadFormulario="rfc" 
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="20"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="razonSocial" requerido="si"
					name="ajustadorForm" key="catalogos.ajustador.razonsocial"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="razonSocial" 
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="199"/>
			</td>
		</tr>	
		<tr>
			<td>
				<etiquetas:etiquetaError property="calleYNumero" requerido="si"
					key="catalogos.ajustador.calleynumero" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="calleYNumero" 
					onkeypress="return soloAlfanumericos(this, event, false)" caracteres="100"/>
			</td>
			<td>
				<etiquetas:etiquetaError property="estado" requerido="si"
					key="catalogos.ajustador.estado" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:estado styleId="estado" size="1" propiedad="estado"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudad,colonia,codigoPostal'); getCiudades(this,'ciudad');" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="ciudad" requerido="si"
					key="catalogos.ajustador.ciudad" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:ciudad styleId="ciudad" size="1" propiedad="ciudad"
					estado="estado" styleClass="cajaTexto"
					onchange="getColonias(this,'colonia')" />
			</td>
			<td>
				<etiquetas:etiquetaError property="colonia"
					key="catalogos.ajustador.colonia" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:colonia styleId="colonia" size="1" propiedad="colonia"
					ciudad="ciudad" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="codigoPostal" requerido="si"
					key="catalogos.ajustador.codigopostal" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto id="codigoPostal" propiedadFormulario="codigoPostal" caracteres="6"
					onkeypress="return soloNumeros(this, event, false)"
					onchange="if (this.value !== '')getColoniasPorCP(this.value, 'colonia','ciudad','estado');" />
			</td>
			<td>
				<etiquetas:etiquetaError property="telefono" requerido="si"
					key="catalogos.ajustador.telefono" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="telefono" caracteres="10"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="telefonoCelular" requerido="si"
					key="catalogos.ajustador.telefonocelular" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="telefonoCelular" caracteres="20"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<td>
				<etiquetas:etiquetaError property="radiolocalizador" requerido="si"
					key="catalogos.ajustador.radiolocalizador" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:texto propiedadFormulario="radiolocalizador" caracteres="20"
					onkeypress="return soloAlfanumerico(this, event, false)" />
			</td>
		</tr>
		<tr>
			<td>
				<etiquetas:etiquetaError property="ramo"
					key="catalogos.ajustador.ramo" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:ramo styleId="idTcRamo" size="1" propiedad="ramo" styleClass="cajaTexto" />
			</td>
			<td>
				<etiquetas:etiquetaError property="tipoDeContribuyente" requerido="si"
					key="catalogos.ajustador.tipodecontribuyente"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:comboValorFijo grupoValores="17" propiedad="tipoDeContribuyente" nombre="ajustadorForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;
<%--				<etiquetas:etiquetaError property="tipoDeProveedor" requerido="si"--%>
<%--					key="catalogos.ajustador.tipodeproveedor" normalClass="normal"--%>
<%--					errorClass="error" errorImage="/img/information.gif" />--%>
			</td>
<%--			<td>--%>
<%--				<midas:texto propiedadFormulario="tipoDeProveedor" />--%>
				
<%--			</td>--%>
			<td>
				<etiquetas:etiquetaError property="email"  name="ajustadorForm"
				    requerido="si" key="catalogos.ajustador.email" 
				    normalClass="normal" errorClass="error" 
				    errorImage="/img/information.gif" />
			</td>       
			<td>
			    <midas:texto propiedadFormulario="email"/>
			</td>	
			<td>
				<etiquetas:etiquetaError property="tipoDeProveedor" requerido="no"
					key="catalogos.ajustador.estatus" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</td>
			<td>
				<midas:comboValorFijo grupoValores="4" propiedad="estatus" nombre="ajustadorForm" styleClass="cajaTexto"/>
			</td>
		</tr>
		<tr>
			<td class="campoRequerido" colspan="3">
	 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
			</td> 
			<td class="guardar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/ajustador/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
					</div>
					<div id="b_guardar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/catalogos/ajustador/modificar.do', 'contenido',null);"> <midas:mensaje clave="midas.accion.guardar" /></a>
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>

