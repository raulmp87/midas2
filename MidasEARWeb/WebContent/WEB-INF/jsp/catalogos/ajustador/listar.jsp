<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<midas:formulario  accion="/catalogos/ajustador/listar">
	<table width="90%" id="filtros">
		<tr>
			<td class="titulo" colspan="6">
				<midas:mensaje clave="midas.accion.listar"/>
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.razonsocial"/>:</th>
			<td><midas:texto  propiedadFormulario="razonSocial"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.rfc"/>:</th>
			<td><midas:texto propiedadFormulario="rfc" /></td>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.nombre"/>:</th>
			<td><midas:texto propiedadFormulario="nombre"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.calleynumero"/>:</th>
			<td><midas:texto propiedadFormulario="calleYNumero"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.codigopostal"/>:</th>
			<td><midas:texto propiedadFormulario="codigoPostal"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.estado"/>:</th>
			<td>
				<midas:estado styleId="estado" size="1" propiedad="estado"
					pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudad,colonia,codigoPostal'); getCiudades(this,'ciudad');" />
			</td>
		</tr> 	
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.ciudad"/>:</th>
			<td>
				<midas:ciudad styleId="ciudad" size="1" propiedad="ciudad"
					estado="estado" styleClass="cajaTexto"
					onchange="getColonias(this,'colonia')" />
			</td>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.colonia"/>:</th>
			<td>
				<midas:colonia styleId="colonia" size="1" propiedad="colonia"
					ciudad="ciudad" styleClass="cajaTexto"
					onchange="setCodigoPostal(this.value);"/>
			</td>
			<th><midas:mensaje clave="catalogos.ajustador.filtro.telefono"/>:</th>
			<td><midas:texto propiedadFormulario="telefono"/></td>
		</tr> 		
		<tr>			
			<th>
			    <midas:mensaje clave="catalogos.ajustador.filtro.email"/>:
			</th>       
			<td>
			    <midas:texto propiedadFormulario="email"/>
			</td>
		</tr>
		<tr>
			<td class= "buscar" colspan="6">
				<div class="alinearBotonALaDerecha">
					<div id="b_buscar">
						<a href="javascript: void(0);"
						onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/catalogos/ajustador/listarFiltrado.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.filtrar"/>
						</a>
					</div>
				</div>
			</td>      		
		</tr>		
	</table>
	<div id="resultados">
		<midas:tabla idTabla="ajustadoresTabla"
			claseDecoradora="mx.com.afirme.midas.decoradores.Ajustador"
			claseCss="tablaConResultados" nombreLista="ajustadores"
			urlAccion="/catalogos/ajustador/listar.do">
			<midas:columna propiedad="nombre" estilo="nombre" />
			<midas:columna propiedad="rfc" titulo="RFC" estilo="rfc" />
			<midas:columna propiedad="razonSocial" titulo="Raz&oacute;n Social" estilo="razonSocial" />
			<midas:columna propiedad="acciones" titulo="" estilo="acciones" />
		</midas:tabla>
		<div id="botonAgregar">
			<div class="alinearBotonALaDerecha">
				<div id="b_agregar">
					<a href="javascript: void(0);"
						onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/ajustador/mostrarAgregar.do', 'contenido',null);">
						<midas:mensaje clave="midas.accion.agregar"/>
					</a>
				</div>
				
			</div>
		</div>
		
	</div>
</midas:formulario>
