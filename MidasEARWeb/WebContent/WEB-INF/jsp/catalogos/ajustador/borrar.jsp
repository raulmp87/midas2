<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>

<midas:formulario accion="/catalogos/ajustador/borrar">
	<table id="desplegar">
		<tr>
			<td class="titulo" colspan="4">
				<midas:mensaje clave="midas.accion.borrar" /> '<midas:escribe nombre="ajustadorForm" propiedad="razonSocial" />'
			</td>
		</tr>
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.nombre" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="nombre" nombre="ajustadorForm"/></td>
			<td colspan="2"></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.rfc" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="rfc" nombre="ajustadorForm"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.razonsocial" />:</th>
			<td class="fondoCajaTexto"><midas:escribe nombre="ajustadorForm" propiedad="razonSocial"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.calleynumero" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="calleYNumero" nombre="ajustadorForm"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.estado" />:</th>
			<td><midas:estado styleId="estado" size="1" propiedad="estado"
					pais="PAMEXI" styleClass="cajaTexto" readonly="true" /></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.ciudad" />:</th>
			<td><midas:ciudad styleId="ciudad" size="1" propiedad="ciudad"
					estado="estado" styleClass="cajaTexto" readonly="true"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.colonia" />:</th>
			<td><midas:colonia styleId="colonia" size="1" propiedad="colonia"
					ciudad="ciudad" styleClass="cajaTexto" readonly="true"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.codigopostal" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="codigoPostal" nombre="ajustadorForm"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.telefono" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="telefono" nombre="ajustadorForm"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.telefonocelular" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="telefonoCelular" nombre="ajustadorForm"/></td>
			<th><midas:mensaje clave="catalogos.ajustador.radiolocalizador" />:</th>
			<td class="fondoCajaTexto"><midas:escribe propiedad="radiolocalizador" nombre="ajustadorForm"/></td>
		</tr> 
		<tr>
			<th><midas:mensaje clave="catalogos.ajustador.ramo" />:</th>
			<td class="fondoCajaTexto"><midas:ramo styleId="idTcRamo" size="1" propiedad="ramo" styleClass="cajaTexto" readonly="true" /></td>
			<th><midas:mensaje clave="catalogos.ajustador.tipodecontribuyente" />:</th>
			<td class="fondoCajaTexto"><midas:comboValorFijo grupoValores="17" propiedad="tipoDeContribuyente" nombre="ajustadorForm" styleClass="cajaTexto" readonly="true"/></td>
		</tr>		
		<tr>
<%--			<th><midas:mensaje clave="catalogos.ajustador.tipodeproveedor" />:</th>--%>
<%--			<td class="fondoCajaTexto"><midas:escribe propiedad="tipoDeProveedor" nombre="ajustadorForm"/></td>--%>
			<th>
				<midas:mensaje clave="catalogos.ajustador.email" />:
			</th>    
			<td class="fondoCajaTexto">
				<midas:escribe nombre="ajustadorForm" propiedad="email"/>
			</td>
			<th><midas:mensaje clave="catalogos.ajustador.estatus" />:</th>
			<td>
				<midas:comboValorFijo grupoValores="4" propiedad="estatus" nombre="ajustadorForm" readonly="true" styleClass="cajaTexto"/>
			</td>
		</tr>	
		<tr>
			<td class="regresar" colspan="4">
				<div class="alinearBotonALaDerecha">
					<div id="b_regresar">
						<a href="javascript: void(0);" onclick="javascript: sendRequest(null,'/MidasWeb/catalogos/ajustador/listar.do', 'contenido',null);"><midas:mensaje clave="midas.accion.regresar"/></a>
						<html:hidden property="idTcAjustador" name="ajustadorForm"/>
					</div>
					<div id="b_borrar">
						<a href="javascript: void(0);"
							onclick="javascript: sendRequest(document.ajustadorForm,'/MidasWeb/catalogos/ajustador/borrar.do', 'contenido',null);">
							<midas:mensaje clave="midas.accion.borrar" /> </a>
						<html:hidden property="idTcAjustador" name="ajustadorForm" />
					</div>
				</div>
			</td>
		</tr>
	</table>
</midas:formulario>