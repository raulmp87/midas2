<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<midas:formulario accion="/poliza/listarPolizaFiltrado">
	<midas:oculto propiedadFormulario="totalRegistros"/>
	<midas:oculto propiedadFormulario="numeroPaginaActual"/>
	<midas:oculto propiedadFormulario="paginaInferiorCache"/>
	<midas:oculto propiedadFormulario="paginaSuperiorCache"/>
	<bean:define id="totalReg" name="polizaForm" property="totalRegistros"/>
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar"/> P&oacute;lizas
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros">
		<tr>
			<th>N&uacute;mero de p&oacute;liza:</th>
			<td>
				<midas:texto propiedadFormulario="numeroPoliza" nombreFormulario="polizaForm"
					onkeypress="return soloNumeros(this, event, false)" />
			</td>
			<th>Fecha de creaci&oacute;n(dd/mm/aaaa):</th>
			<td><midas:texto propiedadFormulario="fechaCreacion" nombreFormulario="polizaForm" longitud="10"/>
			</td>
		</tr>
		<tr>
			<th>Nombre del asegurado:</th>
			<td><midas:texto propiedadFormulario="nombreAsegurado"nombreFormulario="polizaForm" /></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<th>
				<midas:boton
					onclick="javascript: sendRequest(document.polizaForm, '/MidasWeb/poliza/listarPolizaFiltrado.do', 'contenido', null);"
					tipo="buscar" />
			</th>
		</tr>
	</table>
	<br />
	<div id="resultados">
		<midas:tabla idTabla="polizas"
			claseDecoradora="mx.com.afirme.midas.decoradores.Poliza"
			claseCss="tablaConResultados" nombreLista="polizas"
			urlAccion="/poliza/listarFiltradoPaginado.do" exportar="false"
			totalRegistros="<%=totalReg.toString()%>">
			<midas:columna propiedad="numeroPoliza" titulo="N&uacute;mero de P&oacute;liza" maxCaracteres="15" />
			<midas:columna propiedad="nombreAsegurado" titulo="Nombre del Asegurado" maxCaracteres="35" />
			<midas:columna propiedad="fechaCreacion" titulo="Fecha de Emis&oacute;n" formato="{0,date,dd/MM/yyyy}" maxCaracteres="15" />
			<midas:columna propiedad="claveEstatus" titulo="Estatus" maxCaracteres="15"/>
			<midas:columna propiedad="acciones"/>
		</midas:tabla>
	</div>
</midas:formulario>