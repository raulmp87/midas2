<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/renovacion/asignarRenovaciones">
	<nested:hidden property="polizasListadas" name="renovacionPolizaForm"/>
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar" />
				P&oacute;lizas Renovables
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros">
		<tr>
			<th> 
				<midas:mensaje clave="poliza.renovacion.numeroPoliza" />
			</th>
			<td>
				<midas:texto propiedadFormulario="numeroPoliza"
					nombreFormulario="renovacionPolizaForm" caracteres="16" longitud="18"
					onfocus="new Mask('####-########-##', 'string').attach(this)" />
			</td>
			<th width="8%">
				<center>
					<midas:mensaje clave="poliza.renovacion.vigencia" />
					<a href="javascript: void(0);" id="mostrarCalendario"
						onclick="mostrarCalendarioDobleLinea()"> <image
							src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /> </a>
				</center>
			</th>
			<th width="8%">
				<etiquetas:etiquetaError property="fechaInicioVigencia"
					requerido="si" key="poliza.renovacion.desde"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaInicial" property="fechaInicioVigencia"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />
			</td>
			<th>
				<etiquetas:etiquetaError property="fechaFinVigencia" requerido="si"
					key="poliza.renovacion.hasta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaFinal" property="fechaFinVigencia"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="poliza.renovacion.nombreAsegurado" />
			</th>
			<td colspan="4">
				<midas:texto propiedadFormulario="nombreAsegurado"
					nombreFormulario="renovacionPolizaForm" />
			</td>
			<td colspan="2">
				<div id="rangoDeFechas" style="position: absolute; z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="fechaFinal"></div>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="poliza.renovacion.producto" />
			</th>
			<td colspan="3">
				<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote" readonly="false" />			
			</td>	
			<th colspan="3">
				<div class="alinearBotonALaDerecha">
					<midas:boton
						onclick="javascript: sendRequest(document.renovacionPolizaForm, '/MidasWeb/renovacion/listarRenovables.do', 'contenido', 'manipulaCalendarioLineas();');"
						tipo="buscar" />				
				</div>
			</th>	
		</tr>
	</table>
	<br/>
	<nested:empty name="renovacionPolizaForm" property="polizasRenovables">
		<div id="resultadosOverFlow">
			<div class="subtituloIzquierdaDiv">Sin registros que mostrar</div>
		</div>
	</nested:empty>	
	<nested:notEmpty name="renovacionPolizaForm" property="polizasRenovables">
		<div id="resultadosOverFlow">
			<table id="t_riesgo" width="100%">
				<tr >
					<th width="5%">
						<input type="checkbox" name="seleccionarTodos" alt="Seleccionar Todos" title="Seleccionar Todos" onclick="seleccionaTodos(this.checked);"/> 
					</th>
					<th width="20%">
						<midas:mensaje clave="poliza.renovacion.numeroPoliza" />
					</th>
					<th width="30%">
						<midas:mensaje clave="poliza.renovacion.nombreAsegurado" />
					</th>
					<th width="20%">
						<midas:mensaje clave="poliza.renovacion.producto" />
					</th>		
					<th width="10%">
						<midas:mensaje clave="poliza.renovacion.finVigencia" />
					</th>	
					<th width="10%">
						D&iacute;as
					</th>							
					<th width="5%">
						<midas:mensaje clave="poliza.renovacion.acciones" />
					</th>														
				</tr>
				<nested:iterate id="nextPolizaId" name="renovacionPolizaForm"
					property="polizasRenovables" indexId="indexVar" scope="request"
					type="mx.com.afirme.midas.poliza.PolizaDTO">
					
					<c:if test="${nextPolizaId.cotizacionDTO.diasPorDevengar < 365}">
						<c:set var="rowClass" value="#F5A9A9"></c:set>
					</c:if>
					<c:if test="${nextPolizaId.cotizacionDTO.diasPorDevengar == 365}">
						<c:set var="rowClass" value="#FFFFFF"></c:set>
					</c:if>
					<tr style="background-color: ${rowClass}">
						<td width="5%">
							<nested:hidden name="nextPolizaId" property="idToPoliza"  styleId='<%="idToPoliza[" + indexVar + "]"%>'/>
							<nested:checkbox name="nextPolizaId" property="claveSeleccion"
								styleId='<%="checkboxSeleccion[" + indexVar + "]"%>'/>
						</td>
						<td width="20%" >
							${nextPolizaId.numeroPolizaFormateada}
						</td>	
						<td width="30%">
							${nextPolizaId.nombreAsegurado}
						</td>	
						<td width="20%">
							${nextPolizaId.nombreProducto}
						</td>	
						<td width="10%">
							<bean:write name="nextPolizaId" property="cotizacionDTO.fechaFinVigencia" format="dd/MM/yyyy"/>
						</td>
						<td width="10%">
							${nextPolizaId.cotizacionDTO.diasPorDevengar}
						</td>		
						<td width="5%">
							&nbsp;
						</td>						
					</tr>
				</nested:iterate>							
			</table>	
		</div>	
	</nested:notEmpty>
	<table id="desplegarDetalle">
		<tr>
			<td width="40%">&nbsp;</td>
			<th width="10%">
				<etiquetas:etiquetaError property="idSuscriptor" requerido="si"
					key="poliza.renovacion.usuario.asignacion" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td width="20%">
				<midas:comboCatalogo idCatalogo="nombreUsuario" styleId="idSuscriptor" propiedad="idSuscriptor" descripcionCatalogo="nombre" nombreCatalogo="rol_suscriptor_cot" size="1"/>
			</td>
			<td width="30%">
				<midas:boton
					onclick="javascript: validarAsigacionDeRenovaciones();"
					tipo="agregar"  texto="Asignar"/>				
			</td>
		</tr>
	</table>	
</midas:formulario>