<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/cotizacion/endoso/modificarCuotas">
	<logic:empty name="renovacionPolizaForm" property="detalleRenovacion" >
		<div class="subtituloIzquierdaDiv">Ocurri&oacute; un error al obtener la bitacora del seguimiento de la poliza</div>
	</logic:empty>		
	<logic:notEmpty name="renovacionPolizaForm" property="detalleRenovacion" >
		<table id="desplegarDetalle">
			<tr>
				<th colspan="2">
					<midas:mensaje clave="poliza.renovacion.numeroPoliza" />: <midas:escribe propiedad="detalleRenovacion.polizaDTO.numeroPolizaFormateada" nombre="renovacionPolizaForm"/>
				</th>
			</tr>
			<tr>
				<th colspan="2">
					<midas:mensaje clave="poliza.renovacion.nombreAsegurado" />:  <midas:escribe propiedad="detalleRenovacion.polizaDTO.nombreAsegurado" nombre="renovacionPolizaForm"/>
				</th>			
			</tr>
			<tr>
				<th colspan="1">
					Inicio Vigencia:  <midas:escribe propiedad="detalleRenovacion.polizaDTO.cotizacionDTO.fechaInicioVigencia" nombre="renovacionPolizaForm" formato="dd/MM/yyy HH:MM:SS"/>
				</th>			
				<th colspan="1">
					Fin Vigencia:  <midas:escribe propiedad="detalleRenovacion.polizaDTO.cotizacionDTO.fechaFinVigencia" nombre="renovacionPolizaForm" formato="dd/MM/yyy HH:MM:SS"/>
				</th>							
			</tr>			
		</table>	
		<logic:empty name="renovacionPolizaForm" property="detalleRenovacion.seguimientoRenovacion" >
			<div class="subtituloIzquierdaDiv">No se encontraron movimientos registrados en la bitacora para esta P&oacute;liza</div>
		</logic:empty>
		<logic:notEmpty name="renovacionPolizaForm" property="detalleRenovacion.seguimientoRenovacion" >
			<table id="t_riesgo" border="0" width="95%">
				<tr>
					<th>Fecha Movimiento</th>
					<th>Tipo Movimiento</th>
					<th>Descripci&oacute;n Movimiento</th>
				</tr>	
				<logic:iterate id="movimiento" name="renovacionPolizaForm" property="detalleRenovacion.seguimientoRenovacion" indexId="index">
					<tr class="bg_t2">
						<td><bean:write name="movimiento" property="fechaMovimiento" format="dd/MM/yyyy HH:mm:ss"/></td>
						<td><bean:write name="movimiento" property="descripcionTipoMovimiento"/></td>
						<td><bean:write name="movimiento" property="descripcionMovimiento" /></td>
					</tr>
				</logic:iterate>
			</table>			
		</logic:notEmpty>
	</logic:notEmpty>
</midas:formulario>