<%@ page isELIgnored="false"%>
<%@ taglib prefix="midas" uri="/WEB-INF/tld/MidasTag.tld"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://www.afirme.com/tags-etiquetas" prefix="etiquetas"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<midas:formulario accion="/renovacion/enviarCorreo">
	<nested:hidden property="polizasListadas" name="renovacionPolizaForm"/>
	<html:hidden property="mensaje" styleId="mensaje"/>
	<html:hidden property="tipoMensaje" styleId="tipoMensaje"/>	
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				<midas:mensaje clave="midas.accion.listar" />
				Seguimiento de Renovaciones
			</td>
		</tr>
	</table>
	<table width="98%" id="filtros">
		<tr>
			<th> 
				<midas:mensaje clave="poliza.renovacion.numeroPoliza" />
			</th>
			<td>
				<midas:texto propiedadFormulario="numeroPoliza"
					nombreFormulario="renovacionPolizaForm" caracteres="16" longitud="16"
					onkeypress="return soloNumeros(this,event,false)" />
			</td>
			<th width="10%">
				<center>
					<midas:mensaje clave="poliza.renovacion.vigencia" />
					<a href="javascript: void(0);" id="mostrarCalendario"
						onclick="mostrarCalendarioDoble()"> <image
							src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /> </a>
				</center>
			</th>
			<th width="8%">
				<etiquetas:etiquetaError property="fechaInicioVigencia"
					requerido="no" key="poliza.renovacion.desde"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaInicial2" property="fechaInicioVigencia"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />			
			</td>
			<th>
				<etiquetas:etiquetaError property="fechaFinVigencia" requerido="no"
					key="poliza.renovacion.hasta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaFinal2" property="fechaFinVigencia"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />
			</td>
		</tr>
				<tr>
			<th>
				<midas:mensaje clave="poliza.renovacion.nombreAsegurado" />
			</th>
			<td >
				<midas:texto propiedadFormulario="nombreAsegurado"
					nombreFormulario="renovacionPolizaForm" />
			</td>
			<th width="10%">
				<center>
					<midas:mensaje clave="poliza.renovacion.asignacion" />
					<a href="javascript: void(0);" id="mostrarCalendario"
						onclick="mostrarCalendarioDobleLinea()"> <image
							src="/MidasWeb/img/icons/ico_calendario.gif" border=0 /> </a>
				</center>			
			</th>
			<th width="8%">
				<etiquetas:etiquetaError property="fechaAsignacionDesde"
					requerido="no" key="poliza.renovacion.desde"
					normalClass="normal" errorClass="error"
					errorImage="/img/information.gif" />			
			</th>
			<td>
				<html:text styleId="fechaInicial" property="fechaAsignacionDesde"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />
			</td>
			<th>
				<etiquetas:etiquetaError property="fechaAsignacionDesde" requerido="no"
					key="poliza.renovacion.hasta" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td>
				<html:text styleId="fechaFinal" property="fechaAsignacionHasta"
					name="renovacionPolizaForm" size="10" onblur="esFechaValida(this);"
					styleClass="cajaTexto" />			
			</td>
		</tr>
		<tr>
			<th>
				<midas:mensaje clave="poliza.renovacion.numeroCotizacion" />
			</th>		
			<td>
				<midas:texto propiedadFormulario="idToCotizacion"
					nombreFormulario="renovacionPolizaForm" />			
			</td>
			<th>
				<midas:mensaje clave="poliza.renovacion.producto" />
			</th>
			<td colspan="2">
				<midas:escribeCatalogo styleId="idToProducto" styleClass="cajaTexto" size="" propiedad="idProducto" clase="mx.com.afirme.midas.producto.ProductoFacadeRemote" readonly="false" />			
			</td>	
			<td colspan="2">
				<div id="rangoDeFechas2" style="position: absolute; z-index: 1;">
					<div id="calendarioIzq2"></div>
					<div id="fechaFinal2"></div>
				</div>			
				<div id="rangoDeFechas" style="position: absolute; z-index: 1;">
					<div id="calendarioIzq"></div>
					<div id="fechaFinal"></div>
				</div>
				<div class="alinearBotonALaDerecha">
					<midas:boton
						onclick="javascript: sendRequest(document.renovacionPolizaForm, '/MidasWeb/renovacion/listarRenovaciones.do', 'contenido', 'manipulaCalendarioLineas();manipulaCalendarioDoble()');"
						tipo="buscar" />				
				</div>								
			</td>						
		</tr>
	</table>
	<br/>
	<nested:empty name="renovacionPolizaForm" property="seguimientoRenovaciones">
		<div id="resultadosOverFlow">
			<div class="subtituloIzquierdaDiv">Sin registros que mostrar</div>
		</div>
	</nested:empty>	
	<nested:notEmpty name="renovacionPolizaForm" property="seguimientoRenovaciones">
		<div id="resultadosOverFlow">
			<table id="t_riesgo" width="100%">
				<tr>
					<th width="3%">
						<input type="checkbox" name="seleccionarTodos" alt="Seleccionar Todos" title="Seleccionar Todos" onclick="seleccionaTodos(this.checked);"/> 
					</th>
					<th width="10%">
						<midas:mensaje clave="poliza.renovacion.numeroPoliza" />

					</th>
					<th width="10%">
						N&uacute;mero Solicitud
					</th>						
					<th width="10%">
						<midas:mensaje clave="poliza.renovacion.numeroCotizacion" />
					</th>					
					<th width="20%">
						<midas:mensaje clave="poliza.renovacion.nombreAsegurado" />
					</th>
					<th width="15%">
						<midas:mensaje clave="poliza.renovacion.producto" />
					</th>	
					<th width="10%">
						<midas:mensaje clave="poliza.renovacion.asignacion" />
					</th>	
					<th width="15%">
						Inicio <midas:mensaje clave="poliza.renovacion.vigencia" />
					</th>		
					<th width="7%">
						<midas:mensaje clave="poliza.renovacion.acciones" />
					</th>														
				</tr>
				<nested:iterate id="nextPolizaId" name="renovacionPolizaForm"
					property="seguimientoRenovaciones" indexId="indexVar" scope="request"
					type="mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO">
					<tr>
						<td width="3%">
							<nested:hidden name="nextPolizaId" property="polizaDTO.idToPoliza"  styleId='<%="idToPoliza[" + indexVar + "]"%>'/>
							<nested:checkbox name="nextPolizaId" property="claveSeleccion"
								styleId='<%="checkboxSeleccion[" + indexVar + "]"%>'/>
						</td>
						<td width="10%">
							${nextPolizaId.polizaDTO.numeroPolizaFormateada}
						</td>
						<td width="10%">
							${nextPolizaId.cotizacionDTO.solicitudDTO.idToSolicitud}
						</td>							
						<td width="10%">
							${nextPolizaId.cotizacionDTO.idToCotizacion}
						</td>							
						<td width="20%">
							${nextPolizaId.polizaDTO.nombreAsegurado}
						</td>	
						<td width="15%">
							${nextPolizaId.polizaDTO.nombreProducto}
						</td>	
						<td width="10%">
							<bean:write name="nextPolizaId" property="fechaCreacion" format="dd/MM/yyyy"/>
						</td>							
						<td width="15%">
							<bean:write name="nextPolizaId" property="polizaDTO.cotizacionDTO.fechaInicioVigencia" format="dd/MM/yyyy"/>
						</td>		
						<td width="7%">
							<a href="javascript: void(0)" onclick="javascript: mostrarBitacoraSeguimiento(${nextPolizaId.polizaDTO.idToPoliza});">
								<img border='0px' alt='Bitacora' title='Bitacora' src='/MidasWeb/img/icons/ico_verdetalle.gif'/>
							</a>	
													
						</td>						
					</tr>
				</nested:iterate>							
			</table>	
		</div>	
	</nested:notEmpty>
	<table id="desplegarDetalle">
		<tr>
			<td width="10%">&nbsp;</td>
			<th width="15%">
				<etiquetas:etiquetaError property="idDestinatarioCorreo" requerido="si"
					key="poliza.renovacion.usuario.destinatario" normalClass="normal"
					errorClass="error" errorImage="/img/information.gif" />
			</th>
			<td width="20%">
				<midas:comboValorFijo grupoValores="51" propiedad="idDestinatarioCorreo" styleId="idDestinatarioCorreo"
					styleClass="cajaTexto" nombre="renovacionPolizaForm" readonly="false" onchange="validaDestinatario(this.value)"/>
			</td>
			<td width="20%">
				<div id="destinatarioAlterno" style="display: none;">
					<midas:texto propiedadFormulario="destinatarioCorreo" id="destinatarioCorreo"
						nombreFormulario="renovacionPolizaForm"  onblur="valEmail(this.value);"/>
				</div>
			<th width="20%">Adjutar Cotizaci&oacute;n?
				<midas:checkBox valorEstablecido="on" propiedadFormulario="adjuntarCotizacion" id="adjuntarCotizacion"></midas:checkBox>	
			<th>
			</td>
			<td width="10%">
				<midas:boton
					onclick="javascript: validarEnvioCorreo();"
					tipo="agregar"  texto="Enviar"/>				
			</td>
		</tr>
	</table>

</midas:formulario>