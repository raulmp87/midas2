<%@ page isELIgnored="false"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>

<midas:formulario accion="/poliza/mostrarImprimirEndoso">
	<table width="98%">
		<tr>
			<td class="titulo" colspan="8">
				Endosos de la p&oacute;liza
			</td>
		</tr>
	</table>
	<br />
	<div id="resultados" style="width: 98%; border: 0px solid red; font-size: 10px;" >
		<display:table name="polizaForm.listaEndosos" sort="page" defaultsort="1" defaultorder="ascending" cellpadding="0" cellspacing="0"
			 requestURI="/poliza/mostrarImprimirEndoso.do?idToPoliza=<bean:write name='polizaForm' property='numeroPoliza' />" id="endosos" 
			 decorator="mx.com.afirme.midas.decoradores.ImpresionEndososPorPoliza" class="tablaConResultados" >
			<midas:columna propiedad="numeroEndoso" titulo="N&uacute;mero de Endoso" maxCaracteres="20" />
			<midas:columna propiedad="motivoEndoso" titulo="Motivo del endoso" />
			<midas:columna propiedad="claveTipoEndoso" titulo="Tipo de endoso" />
			<midas:columna propiedad="fechaCreacion" titulo="Fecha de Emisi�n" formato="{0,date,dd/MM/yyyy}"/>
			<c:choose>
				<c:when test="${polizaForm.accion == 'consultar'}">
					<midas:columna propiedad="accionesConsulta" titulo="Consultar" />
				</c:when>
				<c:otherwise>
					<midas:columna propiedad="acciones" titulo="Imprimir" />
				</c:otherwise>
			</c:choose>
		</display:table>
		
	</div>
</midas:formulario>