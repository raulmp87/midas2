<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Cambio de Password</title>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/form-elements.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/img/favicon.ico"
	rel="shortcut icon" type="image/png" />
<script type="text/javascript">
	var contextPath = '${pageContext.request.contextPath}';
</script>

        <!-- Javascript -->
        <script src="${pageContext.request.contextPath}/js/cobranza/cargoPortal/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/cobranza/pagos/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.backstretch.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/portalLogin.js"></script>

</head>
<body>
	<!-- Top content -->
	<div class="top-content">
		<div class="inner-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text">
						<div class="logo">
							<img alt="Logo"
								src="${pageContext.request.contextPath}/img/Logo_seguros2.jpg">
						</div>
						<div class="description"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 form-box">
						<div class="form-top">
							<div class="form-top-left">
								<h3>Ingresa al sitio</h3>
								<p>Escribe tu nuevo password:</p>
							</div>
							<div class="form-top-right">
								<i class="fa fa-lock"></i>
							</div>
						</div>
						
						<div class="form-bottom">							
							<form role="form" action="changePassword2.action" method="post" class="login-form" id="changePassword">
								<s:hidden name="resetToken" />
								<div class="form-group">
								<label class="sr-only" for="form-newPassword">Nueva Contrase&ntilde;</label>
								<input type="password" maxlength="8" name="newPassword" placeholder="Nuevo Password..." class="form-newPassword form-control" id="form-newPassword">
								</div>
								<div class="form-group">
								<label class="sr-only" for="form-confirmNewPassword">Confirmar Contrase&ntilde;</label>
								<input type="password" maxlength="8" name="confirmNewPassword" placeholder="Confirmar Password..." class="form-confirmNewPassword form-control" id="form-confirmNewPassword">
								</div>
								<br>
								<button type="submit" class="btn">Enviar</button>
							</form>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="mensajeCustom">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">
						<i class="fa fa-bell"></i> Mensaje
					</h4>
				</div>
				<div class="modal-bodyCustom">
					<br>
					<div style="margin-bottom: 0" class="alert alert-warning">
						<p id="textDinamico"></p><!--valor mensaje -->
					</div>
				</div><!-- /.modal-body -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div><!-- /.modal-footer -->
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
</body>
</html>