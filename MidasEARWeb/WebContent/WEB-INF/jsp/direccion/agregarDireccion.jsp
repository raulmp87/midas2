<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/MidasTag.tld" prefix="midas"%>

<link href="<html:rewrite page="/css/midas.css"/>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScriptWindow.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/ajaxScript.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxwindows.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/dhtmlxcommon.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/prototype.js"/>"></script>
<script type="text/javascript" src="<html:rewrite page="/js/validaciones.js"/>"></script>

<midas:formulario accion="/direccion/agregarDireccion">
	<table id="desplegar">
         	<tr>
	           	<td class="titulo" colspan="4">
	           		<midas:mensaje clave="midas.direccion"/>
	           		<midas:oculto propiedadFormulario="idPadre"/>
	           		<midas:oculto propiedadFormulario="idToDireccion"/>
	           		<midas:oculto propiedadFormulario="codigoDireccion"/>
	           		<midas:oculto propiedadFormulario="descripcionPadre"/>
	           	</td>
         	</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.nombreCalle"/>*:</th>
				<td colspan="3"><midas:texto propiedadFormulario="nombreCalle" nombreFormulario="direccionForm" caracteres="100"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.numeroExterior"/>*:</th>
				<td width = "40%"><midas:texto propiedadFormulario="numeroExterior" nombreFormulario="direccionForm" caracteres="10"/></td>
				<th><midas:mensaje clave="midas.direccion.numeroInterior"/>:</th>
				<td width = "40%" style="font-size: 9px;">
				<html:text property="numeroInterior" styleId="numeroInterior" maxlength="10" name="direccionForm" styleClass="cajaTexto"></html:text>
				</td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.entreCalles"/>:</th>
				<td colspan="3"><midas:texto propiedadFormulario="entreCalles" nombreFormulario="direccionForm" caracteres="100"/></td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.idEstado"/>*:</th>
				<td><!--midas:escribe propiedad="idEstado" nombre="direccionForm"/-->
					<midas:estado styleId="estado" size="1" propiedad="idEstado" pais="PAMEXI" styleClass="cajaTexto"
					onchange="limpiarObjetos('ciudad,colonia,codigoPostal'); getCiudades(this,'ciudad');" />
				</td>
				<th><midas:mensaje clave="midas.direccion.idMunicipio"/>*:</th>
				<td>
					<midas:ciudad styleId="ciudad" size="1" propiedad="idMunicipio" estado="idEstado" styleClass="cajaTexto" 
						onchange="getColonias(this,'colonia')" />
				</td>
			</tr>
			<tr>
				<th><midas:mensaje clave="midas.direccion.nombreColonia" />*:</th>
				<td>
					<midas:colonia styleId="colonia" size="1" propiedad="idColonia"
						ciudad="idMunicipio" styleClass="cajaTexto" 
						onchange="setCodigoPostal(this.value);"/>					 
					 
				</td>
				<th><midas:mensaje clave="midas.direccion.codigoPostal"/>*:</th>
				<td>
					<midas:texto id="codigoPostal" propiedadFormulario="codigoPostal" onkeypress="return soloNumeros(this, event, false)"
					onchange="if (this.value !== '')getColoniasPorCP(this.value, 'colonia','ciudad','estado');" caracteres="5"/>
				</td>
			</tr>
		<tr>
			<td colspan="3" class="campoRequerido">
		 			<midas:mensaje clave="catalogos.agregar.asteriscoIndicaCampoRequerido" />
				</td>	
			<td class="guardar">
			<div class="alinearBotonALaDerecha">
				<midas:boton onclick="javascript: guardarDireccion(document.direccionForm);" tipo="guardar"/>
			</div>
			</td>
		</tr>
		</table>
</midas:formulario>