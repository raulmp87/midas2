package mx.com.afirme.midas.danios.reportes.endosocancelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

class PlantillaEndososCancelables extends MidasPlantillaBase{

	private Date fechaCorte;
	private List<EndosoIDTO> listaCancelables;
	
	public PlantillaEndososCancelables(Date fechaCorte){
		this.fechaCorte = fechaCorte;
		inicializarDatosPlantilla();
	}
	
	
	
	private void inicializarDatosPlantilla(){
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.plantillaEndososCancelables.paquete"));
		setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.plantillaEndososCancelables"));
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_XLS );
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		
		super.getListaRegistrosContenido().clear();
		
		super.getParametrosVariablesReporte().put("FECHA_CORTE", fechaCorte);
		super.getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
		if(listaCancelables != null && !listaCancelables.isEmpty()){
			
			for(EndosoIDTO endosoTMP : listaCancelables){
				super.getListaRegistrosContenido().add(endosoTMP);
			}
			
		}
		
		finalizarReporte();
		
		return super.getByteArrayReport();
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public List<EndosoIDTO> getListaCancelables() {
		return listaCancelables;
	}

	public void setListaCancelables(List<EndosoIDTO> listaCancelables) {
		this.listaCancelables = listaCancelables;
	}
}
