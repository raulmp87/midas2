package mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReportePMLHidroSN {

	private ReportePMLHidroFacadeRemote beanRemoto;
	
	public ReportePMLHidroSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReportePMLHidroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	@Deprecated
	public List<ReportePMLHidroDTO> obtieneReportePMLHidro(ReportePMLHidroDTO filtroReporte, String nombreUsuario) {
		try {
			return beanRemoto.obtieneReportePMLHidro(filtroReporte, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Reportes.spDAN_RepPMLHidro");
			sb.append("|");
			sb.append("pFechaCorte" + "=" + filtroReporte.getFechaCorte() + ",");
			sb.append("pTipoCambio" + "=" + filtroReporte.getTipoCambio() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Obtener reporte PML Hidro" + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean calcularReportePMLHidro(ReportePMLHidroDTO filtroReporte, String nombreUsuario){
		return beanRemoto.calcularReportePMLHidro(filtroReporte, nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroIndependientes(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLHidroIndependientes(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoIncisos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLHidroSemiAgrupadoIncisos(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosGenerales(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLHidroSemiAgrupadoDatosGenerales(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLHidroTecnicos(claveTipoReporte,nombreUsuario);
	}
}
