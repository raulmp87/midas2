package mx.com.afirme.midas.danios.reportes;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDTO;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCoberturaCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenRiesgoCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenSeccionCotizacionForm;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.generica.DatoIncisoCotizacionForm;
import mx.com.afirme.midas.danios.reportes.cotizacion.rcfuncionarios.LicitacionForm;
import mx.com.afirme.midas.danios.reportes.poliza.PlantillaPolizaBase;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.lang.NullArgumentException;

public abstract class PlantillaCotizacionBase extends MidasPlantillaBase{
	
	protected BigDecimal idToCotizacion;
	protected CotizacionDTO cotizacionDTO;
	protected ReporteCotizacionBase reporteBase;
	protected IncisoCotizacionDTO incisoCotizacionDTO;
	protected Boolean mostrarLeyendaSANivelCobertura;
	
	public static final String CLAVE_LEYENDA_PIE_PAGINA = "LEYENDA_PIE_PAGINA";
	public static final String MOSTRAR_LEYENDA_PIE_PAGINA = "MOSTRAR_LEYENDA_PIE_PAGINA";
	private boolean mostrarLeyendaPiePagina = false;
	private String leyendaPiePagina = "";
	
	public PlantillaCotizacionBase(BigDecimal idToCotizacion,Map <String,Object> mapaParametrosGenerales,ReporteCotizacionBase reporteBase) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(idToCotizacion, "idToCotizacion");
		validarParametro(mapaParametrosGenerales, "mapaParametrosGenerales");
		validarParametro(reporteBase, "reporteBase");
		
		this.idToCotizacion = idToCotizacion;
		this.parametrosVariablesReporte = new HashMap<String,Object>();
		parametrosVariablesReporte.putAll(mapaParametrosGenerales);
		this.reporteBase = reporteBase;
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	public PlantillaCotizacionBase(CotizacionDTO cotizacionDTO,Map <String,Object> mapaParametrosGenerales,ReporteCotizacionBase reporteBase) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(cotizacionDTO, "cotizacionDTO");
		validarParametro(mapaParametrosGenerales, "mapaParametrosGenerales");
		validarParametro(reporteBase, "reporteBase");
		
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		this.parametrosVariablesReporte = new HashMap<String,Object>();
		parametrosVariablesReporte.putAll(mapaParametrosGenerales);
		this.reporteBase = reporteBase;
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	public PlantillaCotizacionBase(CotizacionDTO cotizacionDTO,IncisoCotizacionDTO incisoCotizacionDTO,Map <String,Object> mapaParametrosGenerales,ReporteCotizacionBase reporteBase) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(cotizacionDTO, "cotizacionDTO");
		validarParametro(mapaParametrosGenerales, "mapaParametrosGenerales");
		validarParametro(reporteBase, "reporteBase");
		validarParametro(incisoCotizacionDTO, "incisoCotizacionDTO");
		
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		this.parametrosVariablesReporte = new HashMap<String,Object>();
		parametrosVariablesReporte.putAll(mapaParametrosGenerales);
		this.reporteBase = reporteBase;
		this.incisoCotizacionDTO = incisoCotizacionDTO;
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	public PlantillaCotizacionBase(CotizacionDTO cotizacionDTO) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(cotizacionDTO, "cotizacionDTO");
		
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	public PlantillaCotizacionBase(BigDecimal idToCotizacion,Map <String,Object> mapaParametrosGenerales) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(idToCotizacion, "idToCotizacion");
		validarParametro(mapaParametrosGenerales, "mapaParametrosGenerales");
		this.idToCotizacion = idToCotizacion;
		this.parametrosVariablesReporte = new HashMap<String,Object>();
		parametrosVariablesReporte.putAll(mapaParametrosGenerales);
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	public PlantillaCotizacionBase(CotizacionDTO cotizacionDTO,Map <String,Object> mapaParametrosGenerales) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(cotizacionDTO, "cotizacionDTO");
		validarParametro(mapaParametrosGenerales, "mapaParametrosGenerales");
		
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		this.parametrosVariablesReporte = new HashMap<String,Object>();
		parametrosVariablesReporte.putAll(mapaParametrosGenerales);
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	public PlantillaCotizacionBase(CotizacionDTO cotizacionDTO,IncisoCotizacionDTO incisoCotizacionDTO) throws NullArgumentException{
		super();
		if(!mostrarLeyendaPiePagina && getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		validarParametro(cotizacionDTO, "cotizacionDTO");
		validarParametro(incisoCotizacionDTO, "incisoCotizacionDTO");
		
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		this.incisoCotizacionDTO = incisoCotizacionDTO;
		setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	protected void procesarDatosPlantilla1erRiesgoLuc(String claveUsuario,boolean esCotizacion) throws SystemException {
		if (this.idToCotizacion != null){
			Double sumaAseguradaAgrupacion1erRiesgo = 0d;
			List<AgrupacionCotDTO> listaAgrupacionCotizacion = PrimerRiesgoLUCDN.getInstancia().listarPorCotizacion(idToCotizacion);
			setListaRegistrosContenido(new ArrayList<Object>());
			String leyendaBienes = esCotizacion ? 
								   UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo.leyendaBienes", 
										   						  getParametrosVariablesReporte().get("TOTAL_INCISOS")) : 
								   UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.primerRiesgo.leyendaBienes", 
										   						  getParametrosVariablesReporte().get("TOTAL_INCISOS"));
								   
			if (leyendaBienes != null)
				getParametrosVariablesReporte().put("LEYENDA_BIENES", leyendaBienes);
			//Si se tiene primer riesgo y/o LUC
			if (listaAgrupacionCotizacion != null && !listaAgrupacionCotizacion.isEmpty()){
				//Buscar si tiene agrupacion con claveTipo = 1
				AgrupacionCotDTO agrupacionPR = null;
				for(AgrupacionCotDTO agrupacion : listaAgrupacionCotizacion){
					if (agrupacion.getClaveTipoAgrupacion().intValue() == 1){
						agrupacionPR = agrupacion;
						sumaAseguradaAgrupacion1erRiesgo = agrupacion.getValorSumaAsegurada();
						break;
					}
				}
				if (agrupacionPR != null){
					Short numero = agrupacionPR.getId().getNumeroAgrupacion();
					CoberturaCotizacionDTO coberturaCot = new  CoberturaCotizacionDTO();
					coberturaCot.setId(new CoberturaCotizacionId());
					coberturaCot.getId().setIdToCotizacion(idToCotizacion);
					coberturaCot.setNumeroAgrupacion(numero);
					coberturaCot.setClaveContrato(Sistema.CONTRATADO);
					coberturaCot.setSeccionCotizacionDTO(new SeccionCotizacionDTO());
					coberturaCot.getSeccionCotizacionDTO().setClaveContrato(Sistema.CONTRATADO);
					List<CoberturaCotizacionDTO> listaCoberturasPR = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCot);
					List<BigDecimal> listaIDSeccionesPR = new ArrayList<BigDecimal>();
					List<SeccionCotizacionDTO> listaSeccionesPrimerRiesgo = new ArrayList<SeccionCotizacionDTO>();
					for(CoberturaCotizacionDTO coberturaCottmp : listaCoberturasPR){
						if (!listaIDSeccionesPR.contains(coberturaCottmp.getId().getIdToSeccion())){
							listaSeccionesPrimerRiesgo.add(coberturaCottmp.getSeccionCotizacionDTO());
							listaIDSeccionesPR.add(coberturaCottmp.getId().getIdToSeccion());
						}
					}
					
					if (listaSeccionesPrimerRiesgo != null && !listaSeccionesPrimerRiesgo.isEmpty()){
						ResumenSeccionCotizacionForm seccionCotizacionForm = new ResumenSeccionCotizacionForm();
						String descripcionSecciones = "";
						
						descripcionSecciones = concatenarNombreSecciones(listaSeccionesPrimerRiesgo);
						
						seccionCotizacionForm.setDescripcionSeccion(descripcionSecciones);
						
						seccionCotizacionForm.setPrimaNeta(Sistema.FORMATO_MONEDA.format(sumaAseguradaAgrupacion1erRiesgo));
						seccionCotizacionForm.setNumeroSeccion(ReporteCotizacionBase.PRIMER_RIESGO_RELATIVO);
						String leyendaSecciones = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo.leyendaSecciones", descripcionSecciones);
						if (leyendaSecciones != null){
							getParametrosVariablesReporte().put("LEYENDA_SECCIONES", leyendaSecciones);
							getParametrosVariablesReporte().put("OCULTA_LEYENDA_1ER_RIESGO", Boolean.FALSE);
							getParametrosVariablesReporte().put("IMPRIME_LEYENDA_1ER_RIESGO", Boolean.TRUE);
						}
//						seccionCotizacionForm.setListaCoberturas(new ArrayList<ResumenCoberturaCotizacionForm>());
						
						//Se deben mostrar todas las coberturas contratadas, con suma asegurada b�sica, que aplicaron a primer riesgo y que adem�s dependen de otras coberturas.
						//(Una cobertura depende de otra si se encuentra al menos un registro en la tabla TrCoberturaRequerida  con el campo IdCoberturaRequerida = id de la cobertura)
//						Map<CoberturaDTO, BigDecimal> coberturasPrimerRiesgo = CoberturaCotizacionDN.getInstancia().obtenerCoberturasPrimerRiesgoPorCotizacion(idToCotizacion);
//						if (coberturasPrimerRiesgo != null && !coberturasPrimerRiesgo.isEmpty()){
//							BigDecimal sumaAseguradaGlobalAntes1erRiesgo = new BigDecimal(0d);
//							BigDecimal factorPrimerRiesgo;
//							for(CoberturaDTO coberturaDTO : coberturasPrimerRiesgo.keySet()){
//								sumaAseguradaGlobalAntes1erRiesgo = sumaAseguradaGlobalAntes1erRiesgo.add(coberturasPrimerRiesgo.get(coberturaDTO));
//							}
//							factorPrimerRiesgo = new BigDecimal(sumaAseguradaAgrupacion1erRiesgo).divide(sumaAseguradaGlobalAntes1erRiesgo,2,RoundingMode.HALF_UP);
//							BigDecimal idToCoberturaIncendio = UtileriasWeb.regresaBigDecimal(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.idToCobertura.IncendioRayo"));
//							for(CoberturaDTO coberturaDTO : coberturasPrimerRiesgo.keySet()){
//								if (coberturaDTO.getIdToCobertura().compareTo(idToCoberturaIncendio) != 0){
//									ResumenCoberturaCotizacionForm resumenCoberturaCOT = new ResumenCoberturaCotizacionForm();
//									//Calcular la suma asegurada por primer riesgo
//									resumenCoberturaCOT.setDescripcionCobertura(coberturaDTO.getNombreComercial());
//									resumenCoberturaCOT.setSumaAsegurada(Sistema.FORMATO_MONEDA.format(coberturasPrimerRiesgo.get(coberturaDTO).multiply(factorPrimerRiesgo)));
//									resumenCoberturaCOT.setDescripcionCoaseguro(ReporteCotizacionBase.PRIMER_RIESGO_RELATIVO);
//									seccionCotizacionForm.getListaCoberturas().add(resumenCoberturaCOT);
//								}
//							}
//						}
						getListaRegistrosContenido().add(seccionCotizacionForm);
					}// fin validacion listaSeccionesPrimerRiesgo != null && !listaSeccionesPrimerRiesgo.isEmpty()
					
					else{
						getParametrosVariablesReporte().put("OCULTA_LEYENDA_1ER_RIESGO", Boolean.TRUE);
						getParametrosVariablesReporte().put("IMPRIME_LEYENDA_1ER_RIESGO", Boolean.FALSE);
					}
				}
				else{
					getParametrosVariablesReporte().put("OCULTA_LEYENDA_1ER_RIESGO", Boolean.TRUE);
					getParametrosVariablesReporte().put("IMPRIME_LEYENDA_1ER_RIESGO", Boolean.FALSE);
				}
				//Fin b�squeda de secciones primer Riesgo
				//B�squeda de secciones LUC
				Map<BigDecimal, SeccionDTO> mapaSecciones = new HashMap<BigDecimal,SeccionDTO>();
				for(AgrupacionCotDTO agrupacionCOTTMP : listaAgrupacionCotizacion){
					if (agrupacionCOTTMP.getClaveTipoAgrupacion().intValue() == 2){
						ResumenSeccionCotizacionForm seccionCotizacionForm = new ResumenSeccionCotizacionForm();
						if (!mapaSecciones.containsKey(agrupacionCOTTMP.getIdToSeccion())){
							SeccionDTO seccionTMP = null;
							try{
								seccionTMP = SeccionDN.getInstancia().getPorId(agrupacionCOTTMP.getIdToSeccion());
							}catch(Exception e){}
							if (seccionTMP != null){
								mapaSecciones.put(agrupacionCOTTMP.getIdToSeccion(), seccionTMP);
								seccionCotizacionForm.setDescripcionSeccion(seccionTMP.getNombreComercial());
							}
							else{
								seccionCotizacionForm.setDescripcionSeccion(ReporteCotizacionBase.SECCION_NO_DISPONIBLE);
							}
						}
						else
							seccionCotizacionForm.setDescripcionSeccion(mapaSecciones.get(agrupacionCOTTMP.getIdToSeccion()).getNombreComercial());
						seccionCotizacionForm.setPrimaNeta(Sistema.FORMATO_MONEDA.format(agrupacionCOTTMP.getValorSumaAsegurada()));
						seccionCotizacionForm.setNumeroSeccion(ReporteCotizacionBase.ABREVIATURAS_LUC);
						getListaRegistrosContenido().add(seccionCotizacionForm);
					}
				}
			}//fin listaAgrupacionCotizacion != null
			
			String nombrePlantillaSubReporte = getPaquetePlantilla()+(esCotizacion?
					UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo.subReporteCoberturas.objetos") : 
						UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.primerRiesgo.subReporteCoberturas.objetos"));
			JasperReport subReporteCoberturas = null;
			subReporteCoberturas = getJasperReport(nombrePlantillaSubReporte);
			if(subReporteCoberturas == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte, null);
			getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
			
			try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}
	}
	
	protected void generarParametrosComunes(CotizacionDTO cotizacionDTO,String claveUsuario){
		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		getParametrosVariablesReporte().put("URL_IMAGEN_FONDO", Sistema.FONDO_PLANTILLA_COTIZACION);
		setParametrosVariablesReporte( new HashMap<String,Object>() );
		String NO_DISPONIBLE = "No disponible";
	    getParametrosVariablesReporte().put("NO_COTIZACION", "COT-" + UtileriasWeb.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toBigInteger().toString(), "0", 8));
		if (cotizacionDTO.getIdMoneda() != null){
			MonedaDTO monedaDTO;
			try {
				monedaDTO = MonedaDN.getInstancia().getPorId(new Short(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
				getParametrosVariablesReporte().put("MONEDA", ((monedaDTO != null)?monedaDTO.getDescripcion() : NO_DISPONIBLE));
			} catch (Exception e) {
				getParametrosVariablesReporte().put("MONEDA", NO_DISPONIBLE);
			}
		}
		Format formatter=null;
		if ( cotizacionDTO.getFechaInicioVigencia() != null){
			formatter = new SimpleDateFormat("dd/MM/yyyy");
			getParametrosVariablesReporte().put("FECHA_INICIO", formatter.format(cotizacionDTO.getFechaInicioVigencia())+" 12 hrs.");
		}
		else
			getParametrosVariablesReporte().put("FECHA_INICIO", NO_DISPONIBLE);
		if ( cotizacionDTO.getFechaFinVigencia() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			getParametrosVariablesReporte().put("FECHA_FIN", formatter.format(cotizacionDTO.getFechaFinVigencia())+" 12 hrs.");
		}
		else
			getParametrosVariablesReporte().put("FECHA_FIN", NO_DISPONIBLE);
		if (cotizacionDTO.getIdToPersonaContratante() != null){
			ClienteDTO contratante = null;
			try{
				if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
					contratante = new ClienteDTO();
					contratante.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
					contratante.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
					contratante = ClienteDN.getInstancia().verDetalleCliente(contratante, cotizacionDTO.getCodigoUsuarioCotizacion());
				}else{
					contratante = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), cotizacionDTO.getCodigoUsuarioCotizacion());
				}						
			}catch(Exception exc){contratante = new ClienteDTO();}
			if (contratante != null){
				getParametrosVariablesReporte().put("NOMBRE_CONTRATANTE", ClienteAction.obtenerNombreCliente(contratante));
				getParametrosVariablesReporte().put("DIRECCION_CONTRATANTE", ClienteAction.obtenerDireccionCliente(contratante));
			}
		}
		String descripcionFormaPago = NO_DISPONIBLE;
		FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
		formaPagoDTO.setIdFormaPago(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()));
		try {
			FormaPagoDN.getInstancia().getPorId(formaPagoDTO);
		} catch (Exception e) {
			if (!UtileriasWeb.esCadenaVacia(formaPagoDTO.getDescripcion()))
				descripcionFormaPago = formaPagoDTO.getDescripcion();
		}
		getParametrosVariablesReporte().put("FORMA_PAGO", descripcionFormaPago);
		int totalIncisos = IncisoCotizacionDN.getInstancia().obtenerCantidadIncisosPorCotizacion(cotizacionDTO.getIdToCotizacion());
	    getParametrosVariablesReporte().put("TOTAL_INCISOS", ""+totalIncisos);
	}
	
	protected String concatenarNombreSecciones(List<SeccionCotizacionDTO> listaSeccionesPrimerRiesgo){
		String descripcionSecciones = "";
		StringBuilder descripcionDeSecciones = new StringBuilder("");
		if(listaSeccionesPrimerRiesgo == null || listaSeccionesPrimerRiesgo.isEmpty())
			return descripcionSecciones;
		if (listaSeccionesPrimerRiesgo.size() == 1)
			descripcionSecciones = listaSeccionesPrimerRiesgo.get(0).getSeccionDTO().getNombreComercial();
		else
			for(int i=0;i<listaSeccionesPrimerRiesgo.size();i++){
				if(i == (listaSeccionesPrimerRiesgo.size()-1)){
					descripcionDeSecciones.delete(descripcionDeSecciones.length() -2, descripcionDeSecciones.length());
					descripcionDeSecciones.append(" y ").append(listaSeccionesPrimerRiesgo.get(i).getSeccionDTO().getNombreComercial());
				}
				else{
					descripcionDeSecciones.append(listaSeccionesPrimerRiesgo.get(i).getSeccionDTO().getNombreComercial() ).append(", ");
				}
				descripcionSecciones = descripcionDeSecciones.toString();
			}
		return descripcionSecciones;
	}
	
	
	public static String obtenerDescripcionDeducible(CoberturaCotizacionDTO coberturaCot){
		String descripcionDeducible = "";
		NumberFormat fNumero = new DecimalFormat("##0");
		if (coberturaCot.getPorcentajeDeducible() == 0d){
			return descripcionDeducible;
		}
		descripcionDeducible += fNumero.format(coberturaCot.getPorcentajeDeducible());
		String descripcionTipoDeducible = null;
		try{
//			descripcionTipoDeducible = UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.GRUPO_CLAVE_TIPO_DEDUCIBLE, cobertura.getClaveTipoDeducible());
			descripcionTipoDeducible = UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.GRUPO_CLAVE_TIPO_DEDUCIBLE, coberturaCot.getClaveTipoDeducible());
			if(descripcionTipoDeducible.equalsIgnoreCase("N/A")){
				descripcionTipoDeducible= " ";
			}
		}catch(Exception e){}
		if (descripcionTipoDeducible != null){
			descripcionDeducible += " "+descripcionTipoDeducible;
		}else{
			descripcionDeducible += " ";
		}
//		if (cobertura.getValorMinimoLimiteDeducible().intValue() >= 1 ){
//			descripcionDeducible += ", Min. " + fNumero.format(cobertura.getValorMinimoLimiteDeducible()) + " ";
//		}else if(cobertura.getValorMaximoLimiteDeducible().intValue() >= 1 ){
//			descripcionDeducible += ", Max. " + fNumero.format(cobertura.getValorMaximoLimiteDeducible()) + " ";
//		}
		boolean consultarDescripcionTipoDeducible = false;
		if(coberturaCot.getValorMinimoLimiteDeducible() != null){
			if (coberturaCot.getValorMinimoLimiteDeducible().intValue() >= 1 ){
				descripcionDeducible += ", Min. " + fNumero.format(coberturaCot.getValorMinimoLimiteDeducible()) + " ";
				consultarDescripcionTipoDeducible = true;
			}else if(coberturaCot.getValorMaximoLimiteDeducible().intValue() >= 1 ){
				descripcionDeducible += ", Max. " + fNumero.format(coberturaCot.getValorMaximoLimiteDeducible()) + " ";
				consultarDescripcionTipoDeducible = true;
			}
		}
		descripcionTipoDeducible = null;
		if (consultarDescripcionTipoDeducible){
			try{
//				descripcionTipoDeducible = UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.GRUPO_CLAVE_TIPO_LIMITE_DEDUCIBLE, cobertura.getClaveTipoLimiteDeducible());
				descripcionTipoDeducible = UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.GRUPO_CLAVE_TIPO_LIMITE_DEDUCIBLE, coberturaCot.getClaveTipoLimiteDeducible());
				if(descripcionTipoDeducible.equalsIgnoreCase("N/A")){
					descripcionTipoDeducible= " ";
				}
			}catch (Exception e){}
		}
		if (descripcionTipoDeducible != null)
			descripcionDeducible += descripcionTipoDeducible;
		return descripcionDeducible;
	}
	
	protected List<TexAdicionalCotDTO> consultarTextosAdicionales() throws ExcepcionDeAccesoADatos, SystemException{
		TexAdicionalCotDTO textoAdicional = new TexAdicionalCotDTO();
		textoAdicional.setCotizacion(cotizacionDTO);
		return TexAdicionalCotDN.getInstancia().listarFiltrado(textoAdicional);
	}
	
	protected List<DocAnexoCotDTO> consultarDocumentosAnexos(boolean modificarOrden) throws ExcepcionDeAccesoADatos, SystemException{
		
		List<DocAnexoCotDTO> listaAnexos = new ArrayList<DocAnexoCotDTO>();
		
		/*
		 * 14/06/2011 JLAB. Se agrega la lista de anexos de reaseguro, la cual, de existir, debe aparecer al principio del listado.
		 */
		List<DocumentoAnexoReaseguroCotizacionDTO> listaAnexosReaseguro = 
				DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtieneAnexosReasCotPorIdCotizacion(cotizacionDTO.getIdToCotizacion());
		
		if(listaAnexosReaseguro != null && !listaAnexosReaseguro.isEmpty()){
		
			for(DocumentoAnexoReaseguroCotizacionDTO docAnexoReaseguro : listaAnexosReaseguro){
				DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
				docAnexoCotDTO.setDescripcionDocumentoAnexo(docAnexoReaseguro.getDescripcionDocumentoAnexo());
				listaAnexos.add(docAnexoCotDTO);
			}
			
		}
		
		DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
		docAnexoCotDTO.setId(new DocAnexoCotId());
		docAnexoCotDTO.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		docAnexoCotDTO.setClaveSeleccion((short)1);
		List<DocAnexoCotDTO> listaAnexosTMP = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexoCotDTO);
		
		if(listaAnexosTMP != null && !listaAnexosTMP.isEmpty()){
			listaAnexos.addAll(listaAnexosTMP);
		}
		
		if (modificarOrden){
			int contador = 1;
			for(DocAnexoCotDTO documento : listaAnexos){
				/**
				 * 31/12/2009 la columna de orden debe aparecer de forma secuencial, ignorar el campo "orden" de cada registro
				 * y cambiarlo por un n�mero secuencial.
				 * Jos� Luis Arellano B�rcenas.
				 */
				documento.setOrden(new BigDecimal(contador));
				contador++;
			}
		}
		return listaAnexos;
	}
	
	/**
	 * M�todo que recupera informaci�n espec�fica del IncisoCotizacionDTO recibido en el constructor.
	 * La informaci�n que recupera es guardada en el mapa de par�metros de la plantilla. Consiste en los siguientes datos:
	 * UBICACION_INCISO, TIPO_CONSTRUCTIVO, NUMERO_PISOS, NUMERO_INCISO, DESCRIPCION_GIRO
	 * @author Jose Luis Arellano
	 */
	protected void poblarParametrosPlantillaDatosGeneralesInciso(){
		final String NO_DISPONIBLE = "No disponible";
		
		poblarParametroDireccionInciso();
			
		String descripcionTipoTecho = NO_DISPONIBLE;
		//Tipo constructivo (tipotecho y tipo muro
		TipoTechoDTO tipoTecho;
		try {
			tipoTecho = DatoIncisoCotizacionDN.getINSTANCIA().obtenerTipoTecho(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
			descripcionTipoTecho = ((tipoTecho != null)?tipoTecho.getDescripcionTipoTecho():NO_DISPONIBLE);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		String descripcionTipoMuro = NO_DISPONIBLE;
		TipoMuroDTO tipoMuro;
		try {
			tipoMuro = DatoIncisoCotizacionDN.getINSTANCIA().obtenerTipoMuro(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
			descripcionTipoMuro = ((tipoMuro != null)?tipoMuro.getDescripcionTipoMuro():NO_DISPONIBLE);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		getParametrosVariablesReporte().put("TIPO_CONSTRUCTIVO", "MUROS DE " + descripcionTipoMuro+" Y TECHOS DE "+descripcionTipoTecho);
		
		String numeroPisos = "1";
		//numero de pisos. Se obtiene de la tabla ToDatoIncisoCot.
		try {
			numeroPisos = DatoIncisoCotizacionDN.getINSTANCIA().obtenerNumeroPisos(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		} catch (SystemException e) {
			e.printStackTrace();
		}
	    getParametrosVariablesReporte().put("NUMERO_PISOS", numeroPisos);
	    
	    String descripcionGiro = NO_DISPONIBLE;
	    //descripcion del giro
		descripcionGiro = incisoCotizacionDTO.getDescripcionGiroAsegurado();
		descripcionGiro = (descripcionGiro!=null)?descripcionGiro:NO_DISPONIBLE;

		getParametrosVariablesReporte().put("DESCRIPCION_GIRO", descripcionGiro);
		poblarParametroNumeroInciso();
	}
	
	protected void poblarParametroNumeroInciso(){
		getParametrosVariablesReporte().put("NUMERO_INCISO",incisoCotizacionDTO.getId().getNumeroInciso().toBigInteger().toString());
	}
	
	protected void poblarParametroDireccionInciso(){
		DireccionDTO direccionDTO=null;
		direccionDTO = incisoCotizacionDTO.getDireccionDTO();
		DireccionForm direccionForm = new DireccionForm();
		try {
			DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
		} catch (Exception e) {}
		getParametrosVariablesReporte().put("UBICACION_INCISO", direccionForm.toString());
	}
	
	/**
	 * M�todo que recorre la lista de secciones contratadas del inciso recibido en el constructor y genera el desgloce de coberturas y riesgos
	 * contratados para cada secci�n. Las secciones son guardadas en la lista de registros de la plantilla.
	 * @throws SystemException 
	 */
	protected void poblarSeccionesPorInciso() throws SystemException{
		//Obtener todas las secciones contratadas del inciso
		List<SeccionCotizacionDTO> listaSeccionesContratadas = getReporteBase().obtenerSeccionesContratadas(incisoCotizacionDTO.getId().getNumeroInciso());
		if(listaRegistrosContenido == null)
			listaRegistrosContenido = new ArrayList<Object>() ;
		for(SeccionCotizacionDTO seccionCot : listaSeccionesContratadas){
			ResumenSeccionCotizacionForm seccionCotizacionForm = new ResumenSeccionCotizacionForm();
			seccionCotizacionForm.setNumeroSeccion(calculaNumeroRomano(seccionCot.getSeccionDTO().getNumeroSecuencia().intValue()));
			seccionCotizacionForm.setDescripcionSeccion(seccionCot.getSeccionDTO().getNombreComercial().toUpperCase());
//			seccionCotizacionForm.setPrimaNeta(String.format("$ %.2f", seccionCot.getValorPrimaNeta()));
			seccionCotizacionForm.setListaCoberturas(new ArrayList<ResumenCoberturaCotizacionForm>());
			List<CoberturaCotizacionDTO> listaCoberturaCot = getReporteBase().obtenerCoberturasContratadas(seccionCot.getId().getIdToSeccion(),incisoCotizacionDTO.getId().getNumeroInciso());
			/**
			 * 19/04/2010. Jos� Luis Arellano. Por petici�n de C�sar Ayma, loscampos de numeroCobertura y numeroSeccion, no deben mostrar ninguna numeraci�n
			 * ya que no concuerdan con las espacificadas en la impresion de la poliza (textos adicionales).
			 * 
			 */
			//int numeroCobertura = 1;
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturaCot){
				ResumenCoberturaCotizacionForm coberturaCotizacionForm = new ResumenCoberturaCotizacionForm();
				//numero de cobertura
				//coberturaCotizacionForm.setNumeroCobertura(""+numeroCobertura+". ");
				coberturaCotizacionForm.setNumeroCobertura("");
				//Descripcion de la cobertura
				CoberturaDTO coberturaTMP = coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO();
				String claveMostrarCobertura = "1";
				if(this instanceof PlantillaPolizaBase && coberturaTMP.getClaveMostrarPolCobertura() != null){
					claveMostrarCobertura = coberturaTMP.getClaveMostrarPolCobertura();
				}
				else if(this instanceof PlantillaCotizacionBase && coberturaTMP.getClaveMostrarCotCobertura() != null){
					claveMostrarCobertura = coberturaTMP.getClaveMostrarCotCobertura();
				}
				if(claveMostrarCobertura.equals("1")){
					coberturaCotizacionForm.setDescripcionCobertura(coberturaTMP.getDescripcion());
					boolean coberturaAmparada = false;
					//Si la suma asegurada de la cobertura es amparada se muestra el texto 'Amparada' (En plantilla viene 'Cubierto'. 
					if (coberturaTMP.getClaveTipoSumaAsegurada().equals("2")){
						coberturaCotizacionForm.setSumaAsegurada(ReporteCotizacionBase.AMPARADA);
						coberturaAmparada = true;
					}
					//Si es b�sica, se muestra la cantidad de suma asegurada.
					else{
						if (coberturaCot.getCoberturaSeccionDTO().getSeccionDTO().getClaveBienSeccion().equals("1") 
								|| Double.valueOf(coberturaCot.getCoberturaSeccionDTO().getSeccionDTO().getClaveBienSeccion()).doubleValue() == 1d){
							if (this.mostrarLeyendaSANivelCobertura != null && mostrarLeyendaSANivelCobertura)
								coberturaCotizacionForm.setSumaAsegurada(Sistema.FORMATO_MONEDA.format( coberturaCot.getValorSumaAsegurada())+"\n"+ReporteCotizacionBase.SA_SEGUN_RELACION_ADJUNTA);
							else
								coberturaCotizacionForm.setSumaAsegurada(Sistema.FORMATO_MONEDA.format( coberturaCot.getValorSumaAsegurada()));
						}else
							coberturaCotizacionForm.setSumaAsegurada(Sistema.FORMATO_MONEDA.format( coberturaCot.getValorSumaAsegurada()));
					}
					//deducible
					coberturaCotizacionForm.setDescripcionDeducible(obtenerDescripcionDeducible(coberturaCot));
					//Columna de coaseguro
					NumberFormat fNumero = new DecimalFormat("##0");
					coberturaCotizacionForm.setDescripcionCoaseguro(coberturaCot.getPorcentajeCoaseguro() == 0d ? "":fNumero.format(coberturaCot.getPorcentajeCoaseguro())+"%");
					coberturaCotizacionForm.setListaRiesgos(new ArrayList<ResumenRiesgoCotizacionForm>());
					/*
					 * 08/06/2010. Jose Luis Arellano. Se agreg� la inyecci�n de los datos del riesgo en el listado de coberturas.
					 */
					poblarDatosRiesgo(coberturaCot, coberturaCotizacionForm.getListaRiesgos());
					
					//Recuperar los riesgos contratados de la cobertura
					List<RiesgoCotizacionDTO> listaRiesgos = getReporteBase().obtenerRiesgosContratados(coberturaCot.getId().getIdToSeccion(), incisoCotizacionDTO.getId().getNumeroInciso(), coberturaCot.getId().getIdToCobertura());
					if (coberturaTMP.getClaveDesglosaRiesgos().equals("1")){
						//int numeroRiesgo = 1;
						if (listaRiesgos != null & !listaRiesgos.isEmpty()){
							for(RiesgoCotizacionDTO riesgoCot : listaRiesgos){
								ResumenRiesgoCotizacionForm riesgoCotizacionForm = new ResumenRiesgoCotizacionForm();
								//n�mero del riesgo
								//riesgoCotizacionForm.setNumeroRiesgo(""+numeroCobertura+"."+numeroRiesgo+" ");
								riesgoCotizacionForm.setNumeroRiesgo("");
								//Descripcion del riesgo
								riesgoCotizacionForm.setDescripcionRiesgo(riesgoCot.getRiesgoCoberturaDTO().getRiesgoDTO().getDescripcion());
								//Suma asegurada del riesgo
								riesgoCotizacionForm.setSumaAsegurada( coberturaAmparada?ReporteCotizacionBase.AMPARADA:Sistema.FORMATO_MONEDA.format(riesgoCot.getValorSumaAsegurada()) );
								//numeroRiesgo++;
								coberturaCotizacionForm.getListaRiesgos().add(riesgoCotizacionForm);
							}
						}
					}
					//numeroCobertura++;
					seccionCotizacionForm.getListaCoberturas().add(coberturaCotizacionForm);
				}
				
			}
			getListaRegistrosContenido().add(seccionCotizacionForm);
		}
	}
	
	/**
	 * M�todo que agrega un bean a la lista de registros de la plantilla, conteniendo la informaci�n de la licitaci�n en la cotizaci�n,
	 * la suma asegurada y las leyendas del deducible y coaseguro.
	 * @throws SystemException 
	 */
	protected void poblarLicitacionPorInciso(boolean cotizacionDeEndoso) throws SystemException{
		LicitacionForm licitacionForm = new LicitacionForm();
		if(cotizacionDeEndoso){
			licitacionForm.setDescripcionLicitacion(cotizacionDTO.getDescripcionLicitacionEndoso());
		}
		else{
			licitacionForm.setDescripcionLicitacion(cotizacionDTO.getDescripcionLicitacion() != null ? cotizacionDTO.getDescripcionLicitacion() : "");
			String leyendaDeducibleCoaseguro = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacion.rcfuncionarios.leyendadeducible");
			licitacionForm.setDeducible(leyendaDeducibleCoaseguro);
			licitacionForm.setCoaseguro(leyendaDeducibleCoaseguro);
			if(cotizacionDTO.getClaveImpresionSumaAsegurada() != null && cotizacionDTO.getClaveImpresionSumaAsegurada().intValue() == 1){
				licitacionForm.setSumaAsegurada(leyendaDeducibleCoaseguro);
			}
			else{
				List<CoberturaCotizacionDTO> listaCoberturasContratadas = getReporteBase().obtenerCoberturasContratadasBasicasPorInciso(incisoCotizacionDTO.getId().getNumeroInciso());
				double sumaAsegurada = 0d;
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas){
					sumaAsegurada += coberturaCot.getValorSumaAsegurada().doubleValue();
				}
				licitacionForm.setSumaAsegurada(Sistema.FORMATO_MONEDA.format(sumaAsegurada));
			}
		}
		if(listaRegistrosContenido == null)
			listaRegistrosContenido = new ArrayList<Object>() ;
		getListaRegistrosContenido().add(licitacionForm);
	}
	
	private void poblarDatosRiesgo(CoberturaCotizacionDTO coberturaCot,List<ResumenRiesgoCotizacionForm> listaDatos) throws SystemException{
		List<ConfiguracionDatoIncisoCotizacionDTO> listaDatosImprimibles = reporteBase.obtenerConfiguracionDatosIncisoImprimibles(
				coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getRamoDTO().getIdTcRamo(), 
				coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
		if(listaDatosImprimibles != null){
			List<RiesgoCotizacionDTO> listaRiesgosContratados = reporteBase.obtenerRiesgosContratados(coberturaCot.getId().getIdToSeccion(), incisoCotizacionDTO.getId().getNumeroInciso(), coberturaCot.getId().getIdToCobertura());
			for(ConfiguracionDatoIncisoCotizacionDTO config : listaDatosImprimibles){
				for(RiesgoCotizacionDTO riesgoCot : listaRiesgosContratados){
					if(riesgoCot.getId().getIdToRiesgo().compareTo(config.getId().getIdToRiesgo()) == 0){
						DatoIncisoCotizacionDTO datoIncisoFiltro = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(idToCotizacion, incisoCotizacionDTO.getId().getNumeroInciso());
						datoIncisoFiltro.getId().setIdTcRamo(config.getId().getIdTcRamo());
						datoIncisoFiltro.getId().setIdTcSubramo(config.getId().getIdTcSubramo());
						datoIncisoFiltro.getId().setClaveDetalle(config.getId().getClaveDetalle());
						datoIncisoFiltro.getId().setIdDato(config.getId().getIdDato());
//						datoIncisoFiltro.getId().setIdToSeccion(coberturaCot.getId().getIdToSeccion());
//						datoIncisoFiltro.getId().setIdToCobertura(coberturaCot.getId().getIdToCobertura());
						datoIncisoFiltro.getId().setIdToRiesgo(config.getId().getIdToRiesgo());
						List<DatoIncisoCotizacionDTO> listaDatosRegistrados = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(datoIncisoFiltro.getId());
						if(listaDatosRegistrados != null){
							for(DatoIncisoCotizacionDTO datoInciso : listaDatosRegistrados){
								String [] datosRiesgo = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoInciso, config);
								if(datosRiesgo != null && datosRiesgo.length >= 2){
									ResumenRiesgoCotizacionForm resumenRiesgoForm = new ResumenRiesgoCotizacionForm();
									resumenRiesgoForm.setDescripcionRiesgo(datosRiesgo[0]+": "+datosRiesgo[1]);
									resumenRiesgoForm.setSumaAsegurada("");
									resumenRiesgoForm.setNumeroRiesgo("");
									listaDatos.add(resumenRiesgoForm);
								}
							}
						}
						break;
					}
				}
			}
		}
	}
	
	protected List<DatoIncisoCotizacionForm> obtenerDatosGeneralesRiesgo() throws SystemException{
		getParametrosVariablesReporte().put("NUMERO_INCISO", incisoCotizacionDTO.getId().getNumeroInciso().toBigInteger().toString());
		//Generar los datos del apartado "BIEN(es) INCISO X de X"
		//Obtener los subincisos del inciso
		List<SubIncisoCotizacionDTO> listaSubIncisos = reporteBase.obtenerSubIncisosPorInciso(incisoCotizacionDTO.getId().getNumeroInciso());
		List<DatoIncisoCotizacionForm> listaDatosIncisoForm = new ArrayList<DatoIncisoCotizacionForm>();
		DatoIncisoCotizacionForm datoInciso = new DatoIncisoCotizacionForm();
		datoInciso.setEtiqueta(ReporteCotizacionBase.UBICACION);
		datoInciso.setValor(DireccionDN.getInstancia().obtenerDescripcionDireccion(incisoCotizacionDTO.getDireccionDTO()));
		listaDatosIncisoForm.add(datoInciso);
		
		if (listaSubIncisos != null && !listaSubIncisos.isEmpty()){
			//Si hay m�s de un subinciso, se muestra la etiqueta "Bien(es):, con la leyenda fija"
			if (listaSubIncisos.size() > 1){
				datoInciso = new DatoIncisoCotizacionForm();
				datoInciso.setEtiqueta(ReporteCotizacionBase.BIENES);
				datoInciso.setValor(ReporteCotizacionBase.SA_SEGUN_RELACION_ADJUNTA);
				listaDatosIncisoForm.add(datoInciso);
				//Agregar la descripci�n del giro
				agregarDescripcionGiroInciso(listaDatosIncisoForm);
			}
			//Si hay un s�lo inciso, se muestran los datos del mismo.
			else{
				//Primero se agrega la descripcion del subinciso, que no es dato de riesgo
				datoInciso = new DatoIncisoCotizacionForm();
				datoInciso.setEtiqueta("Bien Asegurado");
				datoInciso.setValor(listaSubIncisos.get(0).getDescripcionSubInciso());
				listaDatosIncisoForm.add(datoInciso);
				
				DatoIncisoCotizacionId datoIncisoCotId = new DatoIncisoCotizacionId();
				datoIncisoCotId.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
				datoIncisoCotId.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
				if(listaSubIncisos.get(0) != null && listaSubIncisos.get(0).getId() != null )
					datoIncisoCotId.setNumeroSubinciso(listaSubIncisos.get(0).getId().getNumeroSubInciso());
				List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(datoIncisoCotId);
				if (listaDatosInciso != null){
					for(DatoIncisoCotizacionDTO datoIncisoTMP : listaDatosInciso){
						String[] descripcionDatoInciso = null;
						
						ConfiguracionDatoIncisoCotizacionDTO configuracionDato = null;
						ConfiguracionDatoIncisoCotizacionId configuracionDatoId = new ConfiguracionDatoIncisoCotizacionId();
						configuracionDatoId.setIdTcRamo(datoIncisoTMP.getId().getIdTcRamo());
						configuracionDatoId.setIdTcSubramo(datoIncisoTMP.getId().getIdTcSubramo());
						configuracionDatoId.setIdToRiesgo(datoIncisoTMP.getId().getIdToRiesgo());
						configuracionDatoId.setClaveDetalle(datoIncisoTMP.getId().getClaveDetalle());
						configuracionDatoId.setIdDato(datoIncisoTMP.getId().getIdDato());
						
						ConfiguracionDatoIncisoCotizacionSN confDatoIncisoSN = new ConfiguracionDatoIncisoCotizacionSN();
						configuracionDato = confDatoIncisoSN.getPorId(configuracionDatoId);
						if(configuracionDato != null && configuracionDato.getClaveimpresionpoliza().intValue() == 1){
							try{
								descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoTMP,configuracionDato);
							}catch(Exception e){}
							if (descripcionDatoInciso != null){
								datoInciso = new DatoIncisoCotizacionForm();
								datoInciso.setEtiqueta(descripcionDatoInciso[0]);
								datoInciso.setValor(descripcionDatoInciso[1]);
								listaDatosIncisoForm.add(datoInciso);
							}
						}
					}
				}
			}
		}
		//Si no hay subincisos, mostrar s�lo el subgiro, con la etiqueta "Giro"
		else{
			agregarDescripcionGiroInciso(listaDatosIncisoForm);
		}
		return listaDatosIncisoForm;
	}
	
	private void agregarDescripcionGiroInciso(List<DatoIncisoCotizacionForm> listaDatosInciso){
		DatoIncisoCotizacionForm datoInciso = new DatoIncisoCotizacionForm();
		datoInciso.setEtiqueta("Bien o Giro Asegurado");
		datoInciso.setValor("No disponible");

		if(incisoCotizacionDTO.getDescripcionGiroAsegurado() != null) {
			datoInciso.setValor(incisoCotizacionDTO.getDescripcionGiroAsegurado());
		}
		listaDatosInciso.add(datoInciso);
	}
	
	protected Double obtenerSumaAseguradaAgrupacion1erRiesgo(List<AgrupacionCotDTO> listaAgrupacionCotizacion){
		Double sumaAseguradaAgrupacion1erRiesgo = 0d;
		if (listaAgrupacionCotizacion != null)
			for (AgrupacionCotDTO agrupacionTMP : listaAgrupacionCotizacion){
				if (agrupacionTMP.getClaveTipoAgrupacion().intValue() == 1){
					sumaAseguradaAgrupacion1erRiesgo = agrupacionTMP.getValorSumaAsegurada();
					break;
				}
			}
		return sumaAseguradaAgrupacion1erRiesgo;
	}
	
	protected void validarParametro(Object valor,String nombre) throws NullArgumentException{
		if(valor == null)
			throw new NullArgumentException(nombre);
	}
	
	protected void generarLogErrorCompilacionPlantilla(JRException e){
		LogDeMidasWeb.log("Ocurri� un error al compilar la plantilla '"+getPaquetePlantilla()+getNombrePlantilla()+"', Cotizacion: "+idToCotizacion, Level.SEVERE, e);
	}
	
	protected void generarLogErrorCompilacionPlantilla(String nombrePlantilla,JRException e){
		LogDeMidasWeb.log("Ocurri� un error al compilar la plantilla '"+nombrePlantilla+"', Cotizacion: "+idToCotizacion, Level.SEVERE, e);
	}
	
	protected void generarLogPlantillaSinDatosParaMostrar(){
		LogDeMidasWeb.log("No se encontr� informaci�n para la plantilla '"+getPaquetePlantilla()+getNombrePlantilla()+"'. Cotizacion: "+idToCotizacion, Level.WARNING, null);
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}
	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	public ReporteCotizacionBase getReporteBase() {
		return reporteBase;
	}
	public void setReporteBase(ReporteCotizacionBase reporteBase) {
		this.reporteBase = reporteBase;
	}
	public IncisoCotizacionDTO getIncisoCotizacionDTO() {
		return incisoCotizacionDTO;
	}
	public void setIncisoCotizacionDTO(IncisoCotizacionDTO incisoCotizacionDTO) {
		this.incisoCotizacionDTO = incisoCotizacionDTO;
	}
	public Boolean getMostrarLeyendaSANivelCobertura() {
		return mostrarLeyendaSANivelCobertura;
	}
	public void setMostrarLeyendaSANivelCobertura(Boolean mostrarLeyendaSANivelCobertura) {
		this.mostrarLeyendaSANivelCobertura = mostrarLeyendaSANivelCobertura;
	}
	public boolean isMostrarLeyendaPiePagina() {
		return mostrarLeyendaPiePagina;
	}
	public void setMostrarLeyendaPiePagina(boolean mostrarLeyendaPiePagina) {
		if(getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(MOSTRAR_LEYENDA_PIE_PAGINA, new Boolean(mostrarLeyendaPiePagina));
		}
		this.mostrarLeyendaPiePagina = mostrarLeyendaPiePagina;
	}
	public String getLeyendaPiePagina() {
		return leyendaPiePagina;
	}
	public void setLeyendaPiePagina(String leyendaPiePagina) {
		if(getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put(CLAVE_LEYENDA_PIE_PAGINA, leyendaPiePagina);
		}
		this.leyendaPiePagina = leyendaPiePagina;
	}
}