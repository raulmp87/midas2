package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.math.BigDecimal;
import java.util.logging.Level;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;


public class ReporteRR6Generador implements Runnable{
	
		
	private String fechaIni;
	private String fechaFin;
	private BigDecimal tipoCambio;
	private int reporte;
	private String nombreUsuario; 

	public void run() {
		try {
			LogDeMidasWeb.log("Inicia el proceso de ejecucion del reporte "+reporte+"...", Level.INFO, null);
			ReporteRR6DN reporteRR6DN = ReporteRR6DN.getInstancia();
			reporteRR6DN.procesarReporte(fechaIni, fechaFin, tipoCambio, reporte, nombreUsuario);
			LogDeMidasWeb.log("Finaliza el proceso de ejecucion del reporte "+reporte+"...", Level.INFO, null);
		}
		catch (Exception ex){
			LogDeMidasWeb.log("Excepci&oacute;n en Proceso Generador del Reporte RR6: "
					+ ex.getMessage(), Level.ALL, ex);
		}
	}

	
	
	public String getFechaIni() {
		return fechaIni;
	}
	
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	
	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public int getReporte() {
		return reporte;
	}

	public void setReporte(int reporte) {
		this.reporte = reporte;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

}
