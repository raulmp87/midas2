package mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.tecnico;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepml.AtributoEntradaDTO_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.DocumentoPMLBase;
import mx.com.afirme.midas.danios.reportes.reportepml.ParametroSalidaSP_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.ReportePMLTEVDN;
import mx.com.afirme.midas.sistema.creadorxls.MidasHojaXLS;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;

/**
 * @author Jos� Luis Arellano
 */
public class ReportePMLTEVTecnico extends DocumentoPMLBase{

	public ReportePMLTEVTecnico(Integer claveTipoReporte){
		super(claveTipoReporte);
		this.nombreReporte = "TEV_T�cnicos"+obtenerSufijoReporte();
	}
	
	@SuppressWarnings("unchecked")
	public byte[] obtenerReporte(String nombreUsuario) throws Exception{
		MidasHojaXLS hojaPMLHidroTecnicos = generarHojaTecnicos();
		
		hojaPMLHidroTecnicos.iniciarProcesamientoArchivoXLS();
		
		List listaRegistros = ReportePMLTEVDN.getInstancia().obtenerReportePMLTEVTecnicos(claveTipoReporte,nombreUsuario);
		if(listaRegistros != null && !listaRegistros.isEmpty()){
			establecerNumeroRegistro(listaRegistros,false);
			hojaPMLHidroTecnicos.insertarFilasArchivoXLS(listaRegistros);
		}
		return finalizarProcesamientoArchivoXLS();
	}
	
	private MidasHojaXLS generarHojaTecnicos(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_REGISTRO
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
				,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.VALOR_RETENIDO
				,AtributoEntradaDTO_PML.PRIMA
				,AtributoEntradaDTO_PML.CEDIDA
				,AtributoEntradaDTO_PML.RETENIDA
				,AtributoEntradaDTO_PML.MONEDA
				,AtributoEntradaDTO_PML.RSR_T
				,AtributoEntradaDTO_PML.OFI_EMI
				,AtributoEntradaDTO_PML.ZONA_AMIS
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_REGISTRO
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
				,ParametroSalidaSP_PML.VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.VALOR_RETENIDO
				,ParametroSalidaSP_PML.PRIMA
				,ParametroSalidaSP_PML.CEDIDA
				,ParametroSalidaSP_PML.RETENIDA
				,ParametroSalidaSP_PML.MONEDA
				,ParametroSalidaSP_PML.RSR_T
				,ParametroSalidaSP_PML.OFI_EMI
				,ParametroSalidaSP_PML.ZONA_AMIS
		};
		
		MidasHojaXLS hojaPMLHidro = new MidasHojaXLS("Tecnicos",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidro;
	}
}
