package mx.com.afirme.midas.danios.reportes;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasBaseReporte;


public abstract class MidasPlantillaBase extends MidasBaseReporte implements MidasPlantilla{
	private DataSource dataSource;
	protected List<Object> listaRegistrosContenido;
	protected Map<String,Object> parametrosVariablesReporte;
	private String nombrePlantilla;
	private String paquetePlantilla;
	private String tipoReporte;
	private byte[] byteArrayReport;
	
	protected String obtenerDescripcionMes(Date fecha){
		String descripcionMes = "";
		String [] meses = {"ENERO","FEBRERO", "MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","NOVIEMBRE","DICIEMBRE"};
		if(fecha != null){
			Calendar calendarTMP = Calendar.getInstance();
			calendarTMP.setTime(fecha);
			descripcionMes = meses[calendarTMP.get(Calendar.MONTH)];
		}
		return descripcionMes;
	}
	
	protected void finalizarReporte(){
		if(getListaRegistrosContenido() == null || getListaRegistrosContenido().isEmpty()){
			generarLogPlantillaSinDatosParaMostrar();
		}
		try {
			setByteArrayReport( generaReporte(getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			setByteArrayReport( null );
			generarLogErrorCompilacionPlantilla(e);
		}
	}
	protected String calculaNumeroRomano(int numero){
		StringBuilder numeroRomano = new StringBuilder("");
		String []unidades = {"","I","II","III","IV","V","VI","VII","VIII","IX"};
		String []decenas = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
		String []centenas = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
		String numeroCadena = Integer.valueOf(numero).toString();
		for (int i=numeroCadena.length()-1;i>=0;i--){
			if(i == 2)
				numeroRomano.append(centenas[Integer.valueOf(""+numeroCadena.charAt(i)).intValue()]);
			else if (i==1)
				numeroRomano.append(decenas[Integer.valueOf(""+numeroCadena.charAt(i)).intValue()]);
			else if (i==0)
				numeroRomano.append(unidades[Integer.valueOf(""+numeroCadena.charAt(i)).intValue()]);
		}
		return numeroRomano.toString();
	}
	
	protected void generarLogErrorCompilacionPlantilla(JRException e){
		LogDeMidasWeb.log("Ocurri� un error al compilar la plantilla '"+getPaquetePlantilla()+getNombrePlantilla(), Level.SEVERE, e);
	}
	
	protected void generarLogErrorCompilacionPlantilla(String nombrePlantilla,JRException e){
		LogDeMidasWeb.log("Ocurri� un error al compilar la plantilla '"+nombrePlantilla, Level.SEVERE, e);
	}
	
	protected void generarLogPlantillaSinDatosParaMostrar(){
		LogDeMidasWeb.log("No se encontr� informaci�n para la plantilla '"+getPaquetePlantilla()+getNombrePlantilla(), Level.WARNING, null);
	}
	
	public List<Object> getListaRegistrosContenido() {
		return listaRegistrosContenido;
	}
	public void setListaRegistrosContenido(List<Object> listaRegistrosContenido) {
		this.listaRegistrosContenido = listaRegistrosContenido;
	}
	public Map<String, Object> getParametrosVariablesReporte() {
		return parametrosVariablesReporte;
	}
	public void setParametrosVariablesReporte(Map<String, Object> parametrosVariablesReporte) {
		this.parametrosVariablesReporte = parametrosVariablesReporte;
	}
	public String getNombrePlantilla() {
		return nombrePlantilla;
	}
	public void setNombrePlantilla(String nombrePlantilla) {
		this.nombrePlantilla = nombrePlantilla;
	}
	public String getPaquetePlantilla() {
		return paquetePlantilla;
	}
	public void setPaquetePlantilla(String paquetePlantilla) {
		this.paquetePlantilla = paquetePlantilla;
	}
	public String getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
	public byte[] getByteArrayReport() {
		return byteArrayReport;
	}
	public void setByteArrayReport(byte[] byteArrayReport) {
		this.byteArrayReport = byteArrayReport;
	}
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
