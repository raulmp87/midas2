package mx.com.afirme.midas.danios.reportes.cotizacion.generica;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class PL7BienesAseguradosPorInciso extends PlantillaCotizacionBase{
	boolean mostrarCuadriculaTotales;
	public PL7BienesAseguradosPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.bienesAseguradosPorInciso"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionGenerica.paquete"));
	}
	
	public PL7BienesAseguradosPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionGenerica reporteCotizacionGenerica,boolean mostrarCuadriculaTotales) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteCotizacionGenerica);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.bienesAseguradosPorInciso"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionGenerica.paquete"));
		this.mostrarCuadriculaTotales = mostrarCuadriculaTotales;
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		procesarDatosReporte(claveUsuario);
		return getByteArrayReport();
	}

	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			getParametrosVariablesReporte().put("NUMERO_INCISO", incisoCotizacionDTO.getId().getNumeroInciso().toBigInteger().toString());
			List<DatoIncisoCotizacionForm> listaDatosIncisoForm = super.obtenerDatosGeneralesRiesgo();
			
			/*
			 * 01/06/2010. Jose Luis Arellano. Se agreg� validaci�n para utilizar una plantilla diferente cuando los datos de riesgo exceden 
			 * el �rea permitida en la plantilla por default. 
			 */
			if(listaDatosIncisoForm != null && listaDatosIncisoForm.size() > 2){
				super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.bienesAseguradosPorIncisoConSubIncisos"));
			}
			
			super.poblarSeccionesPorInciso();

			if (getListaRegistrosContenido() == null || getListaRegistrosContenido().isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}

			String nombrePlantillaSubReporte = getPaquetePlantilla() + 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.bienesAseguradosPorInciso.subReporteDatoInciso");
			JasperReport subReporteDatosInciso = getJasperReport(nombrePlantillaSubReporte);
			if(subReporteDatosInciso == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte, null);
			getParametrosVariablesReporte().put("SUBREPORTE_DATOS_INCISO", subReporteDatosInciso);
			getParametrosVariablesReporte().put("DATASOURCE_SUBREPORTE_DATOS_INCISO", new JRBeanCollectionDataSource(listaDatosIncisoForm));
			
			Map<String,Object> parametrosReporteCoberturas = new HashMap<String,Object>();
			nombrePlantillaSubReporte = getPaquetePlantilla()+ 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.bienesAseguradosPorInciso.subReporteCoberturas");
			JasperReport subReporteCoberturas = getJasperReport(nombrePlantillaSubReporte);
			if(subReporteCoberturas == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte, null);
			getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
			
			nombrePlantillaSubReporte = getPaquetePlantilla() + 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.bienesAseguradosPorInciso.subReporteRiesgos");
		    JasperReport subReporteRiesgos = getJasperReport(nombrePlantillaSubReporte);
		    if(subReporteRiesgos == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte, null);
		    parametrosReporteCoberturas.put("SUBREPORTE_RIESGOS", subReporteRiesgos);
			getParametrosVariablesReporte().put("PARAMETROS_SUBREPORTE_COBERTURA",parametrosReporteCoberturas);
			
			getParametrosVariablesReporte().put("MOSTRAR_CUADRICULA_TOTALES", new Boolean(mostrarCuadriculaTotales));
			getParametrosVariablesReporte().put("CANTIDAD_REGISTROS", new Integer((getListaRegistrosContenido()!=null?getListaRegistrosContenido().size():0)));
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}//Fin validar cotizacion != null && incisoCot != null
		else	setByteArrayReport( null);
	}
}
