package mx.com.afirme.midas.danios.reportes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;



import mx.com.afirme.midas.danios.reportes.reportercs.RangosDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaDetalleRanDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaRangosRamosDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.CargaMasivaRangosExcel;
import mx.com.afirme.midas.sistema.excel.Propiedad;
import mx.com.afirme.midas.sistema.excel.Validacion;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

public class Ejemplo {

	public static void main(String[] args) {
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		CargaMasivaRangosExcel<RangosDTO> excel = new CargaMasivaRangosExcel<RangosDTO>(RangosDTO.class);
		
		java.util.Date date = new java.util.Date();
		
		System.out.println(date.getMonth());
	
		controlArchivoDTO.setNombreArchivoOriginal("1.xls");
		controlArchivoDTO.setIdToControlArchivo(new BigDecimal(1));
		controlArchivoDTO.setExtension("xls");
		
		excel.setArchivo(controlArchivoDTO);
		
		Validacion ValNumerico = new Validacion(TipoValidacion.NUMERICO);
		Validacion ValNoRequerido = new Validacion(TipoValidacion.NO_REQUERIDO);
		
		
		
		try {
			if(!excel.isValid()) {
				System.out.println("Valido");
			
			}else{

				Propiedad propiedad = new Propiedad(0, "tipoRenglon");
				propiedad.setDescripcion("Calle");
				propiedad.addValidacion(ValNumerico);
				propiedad.addValidacion(ValNoRequerido);
				excel.addPropiedad(propiedad);
				
				Propiedad propiedad2 = new Propiedad(1, "valor");
				propiedad2.setDescripcion("Calle");
				propiedad2.addValidacion(ValNumerico);
				propiedad2.addValidacion(ValNoRequerido);
				excel.addPropiedad(propiedad2);
				 
				
				Propiedad propiedad3 = new Propiedad(2, "valor2");
				propiedad3.setDescripcion("Calle");
				propiedad3.addValidacion(ValNumerico);
				propiedad3.addValidacion(ValNoRequerido);
				excel.addPropiedad(propiedad3);
				
				Propiedad propiedad4 = new Propiedad(3, "valor3");
				propiedad4.setDescripcion("Calle");
				propiedad4.addValidacion(ValNumerico);
				propiedad4.addValidacion(ValNoRequerido);
				excel.addPropiedad(propiedad4);
				
				
				Propiedad propiedad5 = new Propiedad(4, "valor4");
				propiedad5.setDescripcion("Calle");
				propiedad5.addValidacion(ValNumerico);
				propiedad5.addValidacion(ValNoRequerido);
				excel.addPropiedad(propiedad5);
				
				List<RangosDTO> direccionesValidas = excel.getRegistrosValidos();
				List<RangosDTO> direccionesInvalidas = excel.getRegistrosInvalidos();
				
				System.out.println(direccionesValidas.size());
				
				/*for(RangosDTO direccion : direccionesValidas) {
					int index = direccionesValidas.indexOf(direccion);
				}
				for(RangosDTO direccion : direccionesInvalidas) {
					int index = direccionesInvalidas.indexOf(direccion);
					String errores = excel.getRegistrosInvalidosMensaje().get(index);
				}
				*/
				System.out.println("validos "+ direccionesValidas.size());
				ArrayList<CargaMasivaRangosRamosDTO> arrayList = organizar(direccionesInvalidas);
				
				System.out.println("validos : " + arrayList.size());

				/*	CargaMasivaRanDTO cargaMasivaRanDTO = new CargaMasivaRanDTO();

				cargaMasivaRanDTO = CargaMasivaRangosDN.getInstancia()
						.agregar(cargaMasivaRanDTO, direccionesValidas,
								direccionesInvalidas);*/
			} 
		} catch (FileNotFoundException e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		}
	}
	public static ArrayList<CargaMasivaRangosRamosDTO> organizar(List<RangosDTO> direccionesValidas)
	{
		ArrayList<CargaMasivaRangosRamosDTO> coberturasLits = new ArrayList<CargaMasivaRangosRamosDTO>();
		CargaMasivaRangosRamosDTO cobertura =  new CargaMasivaRangosRamosDTO();
		BigDecimal rango_anterior =  new BigDecimal(0); 
		for (RangosDTO rangosDTO : direccionesValidas) {
			
			if(rangosDTO.getTipoRenglon() == null)
				continue;
			
			switch (rangosDTO.getTipoRenglon().intValue()) {
			case 1://Se crea objeto
				System.out.println("cobeeturas");
				rango_anterior =  new BigDecimal(0); 
				cobertura = new CargaMasivaRangosRamosDTO();
				cobertura.setCobertura(rangosDTO.getValor());
				coberturasLits.add(cobertura);
				
				break;
			case 2://Se define tipo Cartera
				System.out.println("tipoCartera");
				//cobertura.setTipoCartera(rangosDTO.getValor());
				break;
			case 3:// se asignan rangos
				System.out.println("valor Rango");
				CargaMasivaDetalleRanDTO cargaMasivaDetalleRanDTO =  new CargaMasivaDetalleRanDTO();
				cargaMasivaDetalleRanDTO.setLim_inf(rango_anterior);
				cargaMasivaDetalleRanDTO.setLim_sup(new BigDecimal(Double.parseDouble(rangosDTO.getValor())));
				//cobertura.addCargaMasivaDetalleRanDTO(cargaMasivaDetalleRanDTO);
				
				rango_anterior = new BigDecimal(Double.parseDouble( rangosDTO.getValor()));
				break;
			default:
				break;
			}
		}
		
		return coberturasLits;
	}
}
