package mx.com.afirme.midas.danios.reportes.cotizacion.transportes;

import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL16_BienesAseguradosPorIncisoModAnual extends PlantillaTransportesBienesAseguradosBase{
	
	public PL16_BienesAseguradosPorIncisoModAnual(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.transportes.bienesPorIncisoAnual"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionTransportes.paquete"));
	}
	
	public PL16_BienesAseguradosPorIncisoModAnual(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionTransportes reporteCotizacionTransportes) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteCotizacionTransportes);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.transportes.bienesPorIncisoAnual"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionTransportes.paquete"));
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		super.procesarDatosGenericosPlantilla(claveUsuario);
		procesarDatosModalidadAnual(claveUsuario);
		return getByteArrayReport();
	}
	
}
