package mx.com.afirme.midas.danios.reportes.reportercs.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;

public class DocumentoDigitalRangosDN {
	private static final DocumentoDigitalRangosDN INSTANCIA = new DocumentoDigitalRangosDN();

	public static DocumentoDigitalRangosDN getInstancia() {
		return DocumentoDigitalRangosDN.INSTANCIA;
	}

	public DocumentoDigitalRangosDTO getPorId(BigDecimal id) throws SystemException {
		DocumentoDigitalRangosSN solicitudSN = new DocumentoDigitalRangosSN();
		return solicitudSN.getPorId(id);
	}

	public List<DocumentoDigitalRangosDTO> listarTodos() throws SystemException {
		DocumentoDigitalRangosSN solicitudSN = new DocumentoDigitalRangosSN();
		return solicitudSN.listarTodos();
	}

	public void agregar(DocumentoDigitalRangosDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalRangosSN solicitudSN = new DocumentoDigitalRangosSN();
		solicitudSN.agregar(documentoDigitalSolicitudDTO);
	}
	
	public void update(DocumentoDigitalRangosDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalRangosSN solicitudSN = new DocumentoDigitalRangosSN();
		solicitudSN.update(documentoDigitalSolicitudDTO);
	}
	
	public void borrar(DocumentoDigitalRangosDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalRangosSN solicitudSN = new DocumentoDigitalRangosSN();
		solicitudSN.borrar(documentoDigitalSolicitudDTO);
	}
	
	public List<DocumentoDigitalRangosDTO> listarDocumentosDigitalesPorCveNegocio(BigDecimal idNegocio) throws SystemException{
		DocumentoDigitalRangosSN documentoDigitalSolicitudSN = new DocumentoDigitalRangosSN();
		return documentoDigitalSolicitudSN.listarDocumentosSolicitud(idNegocio);
	}
	
	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO.getIdToControlArchivo());
	}
}
