package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ReporteMonitoreoSolicitudesDN {

private static final ReporteMonitoreoSolicitudesDN INSTANCIA = new ReporteMonitoreoSolicitudesDN();
	
	public static ReporteMonitoreoSolicitudesDN getInstancia() {
		return INSTANCIA;
	}
	
	
	public List<ReporteMonitoreoSolicitudesDTO> obtieneReporteMonitoreoSolicitudes(
			Date fechaInicio, Date fechaFin, BigDecimal idProducto,	Integer codigoEjecutivo,
			Integer codigoAgente, String codigoUsuarioSolicitud, String nombreUsuario) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		ReporteMonitoreoSolicitudesSN reporteMonitoreoSolicitudesSN = new ReporteMonitoreoSolicitudesSN();
		
		ReporteMonitoreoSolicitudesDTO filtroReporte = new ReporteMonitoreoSolicitudesDTO();
		
		filtroReporte.setFechaInicio(fechaInicio);
		filtroReporte.setFechaFin(fechaFin);
		filtroReporte.setIdProductoSol((idProducto != null?idProducto:new BigDecimal(0)));
		filtroReporte.setCodigoEjecutivoSOL((codigoEjecutivo != null? codigoEjecutivo: 0));
		filtroReporte.setCodigoAgenteSOL((codigoAgente != null? codigoAgente: 0));
		filtroReporte.setCodigoUsuarioSOL(codigoUsuarioSolicitud);
		
		return reporteMonitoreoSolicitudesSN.obtieneReporteMonitoreoSolicitudes(filtroReporte, nombreUsuario);
		
		
	}
	
}
