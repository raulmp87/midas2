package mx.com.afirme.midas.danios.reportes.endoso;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCoberturaCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenRiesgoCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenSeccionCotizacionForm;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public abstract class PlantillaEndosoCotizacionBase extends PlantillaCotizacionBase {
	protected String[] movimientosGeneralesExtra;
	public PlantillaEndosoCotizacionBase(CotizacionDTO cotizacionDTO) {
		super(cotizacionDTO);
	}

	public PlantillaEndosoCotizacionBase(CotizacionDTO cotizacionDTO,Map<String, Object> mapaParametrosPlantilla,ReporteCotizacionBase midasReporteBase) {
		super(cotizacionDTO,mapaParametrosPlantilla,midasReporteBase);
	}
	
	/**
	 * Consulta la lista de movimientos generales y por inciso de la cotizaci�n recibida en el constructor.
	 * @param claveUsuario
	 * @param nombreCompletoPlantillaSubReporteMovtosGenerales. paquete y nombre de la plantilla correspondiente al subreporte de movimientos generales.
	 * @param nombreCompletoPlantillaSubReporteMovtosInciso. paquete y nombre de la plantilla correspondiente al subreporte de movimientos por inciso.
	 * @param nombreCompletoPlantillaSubReporteDescMovtosInciso. paquete y nombre de la plantilla correspondiente al subreporte de descripci�n de movimientos por inciso.
	 * @throws SystemException
	 */
	
	protected void poblarListaMovimientosEndoso(String claveUsuario,String nombreCompletoPlantillaSubReporteMovtosGenerales, 
				String nombreCompletoPlantillaSubReporteMovtosInciso,String nombreCompletoPlantillaSubReporteDescMovtosInciso,boolean agregarMovimientoGeneralExtra,String[]descripcionMovimientosGeneralesExtra) throws SystemException {
		if (this.cotizacionDTO != null) {
			if (getParametrosVariablesReporte() == null) {
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			
			if (reporteBase.getMovimientosCotizacion() == null || reporteBase.getMovimientosCotizacion().isEmpty())
				reporteBase.consultarMovimientosCotizacion(cotizacionDTO, claveUsuario);
			List<MovimientoCotizacionEndosoDTO> movimientosCotizacion = reporteBase.getMovimientosCotizacion();
			
			List<MovimientoCotizacionEndosoDTO> movimientosGenerales = new ArrayList<MovimientoCotizacionEndosoDTO>();
			
			if(agregarMovimientoGeneralExtra){
				if(descripcionMovimientosGeneralesExtra != null){
					for(int i=0;i<descripcionMovimientosGeneralesExtra.length;i++){
						if(descripcionMovimientosGeneralesExtra[i] != null){
							MovimientoCotizacionEndosoDTO movtoExtra= new MovimientoCotizacionEndosoDTO();
							movtoExtra.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
							movtoExtra.setDescripcionMovimiento(descripcionMovimientosGeneralesExtra[i]);
							movimientosGenerales.add(movtoExtra);
						}
					}
				}
			}
			
			Map<BigDecimal,SeccionDTO> mapaSeccionesTMP = new HashMap<BigDecimal, SeccionDTO>();
			EndosoDTO endoso = null;
			StringBuilder descripcion = new StringBuilder();
			List<ResumenSeccionCotizacionForm> listaIncisos = new ArrayList<ResumenSeccionCotizacionForm>();
			String DATOS_RIESGO = "  Datos de Riesgo";
			if (movimientosCotizacion != null && !movimientosCotizacion.isEmpty()){
				for(MovimientoCotizacionEndosoDTO movimiento : movimientosCotizacion){
					if (movimiento.getClaveTipoMovimiento().intValue() == (int)Sistema.TIPO_MOV_MODIFICACION_APP_GRAL){
						
						if (movimiento.getDescripcionMovimiento().indexOf(Sistema.MSG_MOV_TIPO_ENDOSO) != -1) {
							endoso = EndosoDN.getInstancia(Sistema.USUARIO_SISTEMA).buscarPorCotizacion(this.cotizacionDTO.getIdToCotizacion());
							if (endoso != null 
									&& (endoso.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CE
											|| endoso.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_RE)) { 
								descripcion.delete(0,descripcion.length());
								 descripcion.append(Sistema.MSG_MOV_TIPO_ENDOSO).append(" ");
								
								if (endoso.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CE) {
									if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO) {
										descripcion.append(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.tipo.automatica"));
									} else if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_A_PETICION_DEL_ASEGURADO) {
										descripcion.append(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.tipo.manual"));
									}
								} else if (endoso.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_RE) {
									if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO) {
										descripcion.append(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.rehabilitacion.tipo.automatica"));
									} else if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_A_PETICION_DEL_ASEGURADO) {
										descripcion.append(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.rehabilitacion.tipo.manual"));
									}
								}
								movimiento.setDescripcionMovimiento(descripcion.toString());
							}
						}
											
						
						movimientosGenerales.add(movimiento);
					} else if (movimiento.getClaveAgrupacion() == Sistema.NO_AGRUPADO){
						ResumenSeccionCotizacionForm resumenInciso = new ResumenSeccionCotizacionForm();
						resumenInciso.setNumeroSeccion(movimiento.getNumeroInciso().toBigInteger().toString());
						if(!listaIncisos.contains(resumenInciso)){
							listaIncisos.add(resumenInciso);
						} else{
							resumenInciso = listaIncisos.get(listaIncisos.indexOf(resumenInciso));
						}
						if (movimiento.getClaveTipoMovimiento().intValue() == (int)Sistema.TIPO_MOV_MODIFICACION_INCISO
								|| movimiento.getClaveTipoMovimiento().intValue() == (int)Sistema.TIPO_MOV_ALTA_INCISO
									|| movimiento.getClaveTipoMovimiento().intValue() == (int)Sistema.TIPO_MOV_BAJA_INCISO){
							movimiento.setDescripcionSeccion(DATOS_RIESGO);
						}else{
							if (!mapaSeccionesTMP.containsKey(movimiento.getIdToSeccion())){
								SeccionDTO seccionTMP = null;
								try{seccionTMP = SeccionDN.getInstancia().getPorId(movimiento.getIdToSeccion());}catch(Exception e){}
								if (seccionTMP != null)	mapaSeccionesTMP.put(movimiento.getIdToSeccion(), seccionTMP);
							}
							try{
								movimiento.setDescripcionSeccion("  "+mapaSeccionesTMP.get(movimiento.getIdToSeccion()).getNombreComercial());
							}catch(Exception e){movimiento.setDescripcionSeccion(ReporteCotizacionBase.NO_DISPONIBLE);}
						}
						if(resumenInciso.getListaCoberturas() == null)
							resumenInciso.setListaCoberturas(new ArrayList<ResumenCoberturaCotizacionForm>());
						ResumenCoberturaCotizacionForm resumenCobertura = new ResumenCoberturaCotizacionForm();
						resumenCobertura.setDescripcionCobertura(movimiento.getDescripcionSeccion());
						resumenCobertura.setListaRiesgos(new ArrayList<ResumenRiesgoCotizacionForm>());
						if(!resumenInciso.getListaCoberturas().contains(resumenCobertura)){
							resumenInciso.getListaCoberturas().add(resumenCobertura);
						} else{
							resumenCobertura = resumenInciso.getListaCoberturas().get(resumenInciso.getListaCoberturas().indexOf(resumenCobertura));
						}
						ResumenRiesgoCotizacionForm resumenRiesgo = new ResumenRiesgoCotizacionForm();
						resumenRiesgo.setDescripcionRiesgo(" "+movimiento.getDescripcionMovimiento());
						resumenCobertura.getListaRiesgos().add(resumenRiesgo);
						
//						if (!mapaMovimientosPorInciso.get(movimiento.getNumeroInciso()).containsKey(movimiento.getIdToSeccion())){
//							mapaMovimientosPorInciso.get(movimiento.getNumeroInciso()).put(movimiento.getIdToSeccion(), new ArrayList<MovimientoCotizacionEndosoDTO>());
//						}
//						mapaMovimientosPorInciso.get(movimiento.getNumeroInciso()).get(movimiento.getIdToSeccion()).add(movimiento);
//						System.out.println("Movimiento: inciso: "+movimiento.getNumeroInciso()+" ,idSeccion: "+movimiento.getIdToSeccion()+", descripcionseccion: "+movimiento.getDescripcionSeccion());
					}
				}
			}
			
			mapaSeccionesTMP = null;

			Object[] listaIncisosTMP = listaIncisos.toArray();
			for(int i=0;i<listaIncisosTMP.length;i++){
				for(int j=i+1;j<listaIncisosTMP.length;j++){
					if(Integer.valueOf(((ResumenSeccionCotizacionForm)listaIncisosTMP[i]).getNumeroSeccion())>
					Integer.valueOf(((ResumenSeccionCotizacionForm)listaIncisosTMP[j]).getNumeroSeccion()) ) {
						Object tmp = listaIncisosTMP[i];
						listaIncisosTMP[i] = listaIncisosTMP[j];
						listaIncisosTMP[j] = tmp;
					}
				}
			}
			
			setListaRegistrosContenido(new ArrayList<Object>());
			for(int i=0;i<listaIncisosTMP.length;i++){
				getListaRegistrosContenido().add(listaIncisosTMP[i]);
			}
//			for(ResumenSeccionCotizacionForm resumen : listaIncisos)
//				getListaRegistrosContenido().add(resumen);
//			setListaRegistrosContenido(new ArrayList<Object>());
//			for (BigDecimal inciso : mapaMovimientosPorInciso.keySet()){
//				ResumenSeccionCotizacionForm resumenSeccionForm = new ResumenSeccionCotizacionForm();
//				resumenSeccionForm.setNumeroSeccion(inciso.toBigInteger().toString());
//				resumenSeccionForm.setListaCoberturas(new ArrayList<ResumenCoberturaCotizacionForm>());
//				for(BigDecimal idToSeccion : mapaMovimientosPorInciso.get(inciso).keySet()){
//					ResumenCoberturaCotizacionForm coberturaForm = new ResumenCoberturaCotizacionForm();
//					coberturaForm.setListaRiesgos(new ArrayList<ResumenRiesgoCotizacionForm>());
//					String descripcionSeccion = null;
//					for(MovimientoCotizacionEndosoDTO movimiento : mapaMovimientosPorInciso.get(inciso).get(idToSeccion)){
//						ResumenRiesgoCotizacionForm riesgoForm = new ResumenRiesgoCotizacionForm();
//						//descripcionCoaseguro se usar� para mostrar la descripci�n de. movimiento
//						riesgoForm.setDescripcionRiesgo(" "+movimiento.getDescripcionMovimiento());
//						coberturaForm.getListaRiesgos().add(riesgoForm);
//						if (descripcionSeccion == null){
//							descripcionSeccion = movimiento.getDescripcionSeccion();
//						}
//					}
//					//descripsionCobertura se usar� para mostrar el nombre de la secci�n
//					coberturaForm.setDescripcionCobertura(descripcionSeccion);
//					resumenSeccionForm.getListaCoberturas().add(coberturaForm);
//				}
//				listaRegistrosContenido.add(resumenSeccionForm);
//			}
			
			//Subreporte movimientos generales
			JasperReport subReporteMovimientosGenerales = getJasperReport(nombreCompletoPlantillaSubReporteMovtosGenerales);
			if(subReporteMovimientosGenerales == null){
				generarLogErrorCompilacionPlantilla(nombreCompletoPlantillaSubReporteMovtosGenerales,null);
			}
			getParametrosVariablesReporte().put("SUBREPORTE_MOVIMIENTOS_GENERALES",subReporteMovimientosGenerales);
			getParametrosVariablesReporte().put("DATASOURCE_SUBREPORTE_MOVIMIENTOS_GENERALES",new JRBeanCollectionDataSource(movimientosGenerales));
			
			//Subreporte coberturas, usado para mostrar la secci�n de los movimientos por inciso
			Map<String,Object> parametrosReporteMovimientosInciso = new HashMap<String,Object>();

			JasperReport subReporteMovimientosInciso = getJasperReport(nombreCompletoPlantillaSubReporteMovtosInciso);
			if(subReporteMovimientosInciso == null){
				generarLogErrorCompilacionPlantilla(nombreCompletoPlantillaSubReporteMovtosInciso,null);
			}
			getParametrosVariablesReporte().put("SUBREPORTE_MOVIMIENTOS_INCISO", subReporteMovimientosInciso);

			//SubReporte "riesgos", usado para mostrar la descripcion de movimientos por inciso

			JasperReport subReporteDescripcionMovimientos = getJasperReport(nombreCompletoPlantillaSubReporteDescMovtosInciso);
			if(subReporteDescripcionMovimientos == null){
				generarLogErrorCompilacionPlantilla(nombreCompletoPlantillaSubReporteDescMovtosInciso,null);
			}
			parametrosReporteMovimientosInciso.put("SUBREPORTE_DESCRIPCION_MOVTOS", subReporteDescripcionMovimientos);
			
			getParametrosVariablesReporte().put("PARAMETROS_SUBREPORTE_MOVTOS_INCISO",parametrosReporteMovimientosInciso);
			
			if(listaRegistrosContenido == null || listaRegistrosContenido.isEmpty())
				generarLogPlantillaSinDatosParaMostrar();
			
			/**
			 * Se agrega parametro para indicar si se muestran o no los encabezados de movimientos del inciso
			 */
			getParametrosVariablesReporte().put("IMPRIME_LEYENDA_MOVTOS_GRALES", Boolean.valueOf(!(listaRegistrosContenido == null || listaRegistrosContenido.isEmpty())));
			
			try {
				super.setByteArrayReport(generaReporte(Sistema.TIPO_PDF,getPaquetePlantilla() + getNombrePlantilla(),
						getParametrosVariablesReporte(),getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport(null);
				generarLogErrorCompilacionPlantilla(e);
			}
		}// Fin validar cotizacion != null && incisoCot != null
		else
			setByteArrayReport(null);
	}

	public String[] getMovimientosGeneralesExtra() {
		return movimientosGeneralesExtra;
	}
	public void setMovimientosGeneralesExtra(String[] movimientosGeneralesExtra) {
		this.movimientosGeneralesExtra = movimientosGeneralesExtra;
	}
}
