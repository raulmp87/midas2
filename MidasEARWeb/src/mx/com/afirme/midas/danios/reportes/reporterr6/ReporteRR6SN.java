package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteRR6SN {

	private ReporteRR6FacadeRemote beanRemoto;
	public static final String NO_DISPONIBLE = "noDisponible";
	public static final String EXCEPCION_OBTENER_DTO = "excepcionAlObtenerDTO";
		
	
	public ReporteRR6SN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReporteRR6FacadeRemote.class);
			
			
		} catch (Exception e) {
			throw new SystemException(NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
		public List<ReporteRR6DTO> obtenerReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario) {
		try {
			return beanRemoto.obtenerReporte(fechaIni, fechaFinal, tipoCambio, reporte, nombreUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
		
		public void procesarReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario) {
			try {
				beanRemoto.procesarReporte(fechaIni, fechaFinal, tipoCambio, reporte, nombreUsuario);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<ReporteRR6DTO> listarTodos(String fechaCorte) {
			try {
				return beanRemoto.findAll(fechaCorte);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public ReporteRR6DTO getPorId(Integer id) {
			try {
				return beanRemoto.findById(id);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(EXCEPCION_OBTENER_DTO);
			}
		}
		
		public void borrar(ReporteRR6DTO reporteDTO) {
			try {
				beanRemoto.delete(reporteDTO);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<CargaRR6DTO> listarCargas(String fechaCorte) {
			try {
				return beanRemoto.findAllCarga(fechaCorte);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public void actualizarEstatusCarga(int idCarga, int estatusCarga) {
			try {
				beanRemoto.actualizarEstatusCarga(idCarga, estatusCarga);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public int obtenerRegistros(Date fechaIni, Date fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario) {
			try {
				return beanRemoto.obtenerRegistros(fechaIni, fechaFinal, tipoCambio, reporte, nombreUsuario);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<Object[]> obtenerReporteExcel(Date fechaFinal) {
			try {
				return beanRemoto.obtenerReporteExcel(fechaFinal);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<String> obtenerArchivos(Date fechaFinal, int tipoArchivo) {
			try {
				return beanRemoto.obtenerArchivos(fechaFinal, tipoArchivo);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public int obtenerRegistrosSA(Date fechaFinal) {
			try {
				return beanRemoto.obtenerRegistrosSA(fechaFinal);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}			
}
