package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteBasesEmisionForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String fechaInicio;
	private String fechaFin;
	private String idTcRamo;
	private String idTcSubramo;
	private String idTcTipoReaseguro;
	private String nivelAgrupamiento;

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public String getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcSubramo(String idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}

	public String getIdTcSubramo() {
		return idTcSubramo;
	}

	public void setIdTcTipoReaseguro(String idTcTipoReaseguro) {
		this.idTcTipoReaseguro = idTcTipoReaseguro;
	}

	public String getIdTcTipoReaseguro() {
		return idTcTipoReaseguro;
	}

	public void setNivelAgrupamiento(String nivelAgrupamiento) {
		this.nivelAgrupamiento = nivelAgrupamiento;
	}

	public String getNivelAgrupamiento() {
		return nivelAgrupamiento;
	}
	@Override
	public String toString(){
		String cadena = "ReporteBasesEmisionForm: ";
		cadena += "fechaInicio = "+fechaInicio;
		cadena += "fechaFin = "+fechaFin;
		cadena += "idTcRamo = "+idTcRamo;
		cadena += "idTcSubramo = "+idTcSubramo;
		cadena += "idTcTipoReaseguro = "+idTcTipoReaseguro;
		cadena += "nivelAgrupamiento = "+nivelAgrupamiento;
		return cadena;
	}
}