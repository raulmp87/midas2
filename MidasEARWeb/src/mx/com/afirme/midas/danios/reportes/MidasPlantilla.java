package mx.com.afirme.midas.danios.reportes;

import mx.com.afirme.midas.sistema.SystemException;

public interface MidasPlantilla {
	/**
	 * Devuelve el arreglo de bytes generado a partir de la plantilla utilizada y los objetos necesarios para generar el reporte.
	 * @param String claveUsuario. La clave del usuario que solicita el reporte. Es usada para obtener los datos del cliente.
	 * @return JasperPrint. El objetoJasperPrint a partir del cual se genera el archivo f�sico.
	 */
	public byte[] obtenerReporte(String claveUsuario) throws SystemException;
	
}
