package mx.com.afirme.midas.danios.reportes.poliza;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;

public abstract class PlantillaPolizaBase extends PlantillaCotizacionBase{

	public PlantillaPolizaBase(BigDecimal idToCotizacion,Map<String, Object> mapaParametrosGenerales)throws NullArgumentException {
		super(idToCotizacion, mapaParametrosGenerales);
	}

	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO) {
		super(cotizacionDTO);
	}
	
	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO,Map<String,Object> mapaParametros){
		super(cotizacionDTO,mapaParametros);
	}
	
	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO){
		super(cotizacionDTO,incisoCotizacionDTO);
	}
	
	public PlantillaPolizaBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase){
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteBase);
	}
}
