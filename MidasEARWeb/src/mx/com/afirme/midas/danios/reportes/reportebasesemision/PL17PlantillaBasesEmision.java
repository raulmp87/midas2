package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

public class PL17PlantillaBasesEmision extends MidasPlantillaBase{
	private List<ReporteBasesEmisionDTO> listaMovimientoBasesEndosoDTO;
	public PL17PlantillaBasesEmision(List<ReporteBasesEmisionDTO> listaMovimientoBasesEndosoDTO){
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.reporteBasesEmision.paquete"));
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.reporteBasesEmision"));
		this.listaMovimientoBasesEndosoDTO = listaMovimientoBasesEndosoDTO;
		setTipoReporte( Sistema.TIPO_XLS );
	}
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		if(listaMovimientoBasesEndosoDTO == null || listaMovimientoBasesEndosoDTO.isEmpty())
			return null;
		super.setListaRegistrosContenido(new ArrayList<Object>());
		getListaRegistrosContenido().addAll(listaMovimientoBasesEndosoDTO);
		
		setParametrosVariablesReporte(new HashMap<String,Object>());
		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
	    try {
			super.setByteArrayReport( generaReporte(getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			setByteArrayReport( null );
			throw new SystemException("Ocurri� un error al generar el reporte: "+e.getMessage());
		}
		
		return getByteArrayReport();
	}

}
