package mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.independiente;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepml.AtributoEntradaDTO_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.DocumentoPMLBase;
import mx.com.afirme.midas.danios.reportes.reportepml.ParametroSalidaSP_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.ReportePMLHidroDN;
import mx.com.afirme.midas.sistema.creadorxls.MidasHojaXLS;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;

/**
 * @author Jos� Luis Arellano
 */
public class ReportePMLHidroIndependiente extends DocumentoPMLBase{
	
	public ReportePMLHidroIndependiente(Integer claveTipoReporte){
		super(claveTipoReporte);
		this.nombreReporte = "HIDRO_Independientes"+obtenerSufijoReporte();
	}
	
	@SuppressWarnings("unchecked")
	public byte[] obtenerReporte(String nombreUsuario) throws Exception{
		MidasHojaXLS hojaPMLHidroIndependientes = generarHojaIndependientes();
		
		hojaPMLHidroIndependientes.iniciarProcesamientoArchivoXLS();
		
		List listaRegistros = ReportePMLHidroDN.getInstancia().obtenerReportePMLHidroIndependientes(claveTipoReporte,nombreUsuario);
		if(listaRegistros != null && !listaRegistros.isEmpty()){
			establecerNumeroRegistro(listaRegistros,false);
			hojaPMLHidroIndependientes.insertarFilasArchivoXLS(listaRegistros);
		}
		return finalizarProcesamientoArchivoXLS();
	}
	
	private MidasHojaXLS generarHojaIndependientes(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_REGISTRO
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
				,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONVENIO_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.TIPO_PRIMER_RIESGO
				,AtributoEntradaDTO_PML.MONTO_PRIMER_RIESGO	
				,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONSEC_PERIODO_COBERTURA
				,AtributoEntradaDTO_PML.CONVENIO_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.INM_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONVENIO_DEDUCIBLE
				,AtributoEntradaDTO_PML.INM_COASEGURO
				,AtributoEntradaDTO_PML.CONT_COASEGURO
				,AtributoEntradaDTO_PML.CONSEC_COASEGURO
				,AtributoEntradaDTO_PML.CONVENIO_COASEGURO
				,AtributoEntradaDTO_PML.CLAVE_ESTADO
				,AtributoEntradaDTO_PML.CODIGO_POSTAL
				,AtributoEntradaDTO_PML.LONGITUD
				,AtributoEntradaDTO_PML.LATITUD
				,AtributoEntradaDTO_PML.PRIMERA_LINEA_MAR
				,AtributoEntradaDTO_PML.PRIMERA_LINEA_LAGO
				,AtributoEntradaDTO_PML.SOBREELEVACION_DESPLANTE
				,AtributoEntradaDTO_PML.RUGOSIDAD
				,AtributoEntradaDTO_PML.USO_INMUEBLE
				,AtributoEntradaDTO_PML.NUM_PISOS
				,AtributoEntradaDTO_PML.PISO
				,AtributoEntradaDTO_PML.TIPO_CUBIERTA
				,AtributoEntradaDTO_PML.FORMA_CUBIERTA
				,AtributoEntradaDTO_PML.IRRE_PLANTA
				,AtributoEntradaDTO_PML.OBJETOS_CERCA
				,AtributoEntradaDTO_PML.AZOTEA
				,AtributoEntradaDTO_PML.TAMANO_CRISTAL
				,AtributoEntradaDTO_PML.TIPO_VENTANAS
				,AtributoEntradaDTO_PML.TIPO_DOMOS
				,AtributoEntradaDTO_PML.SOPORTE_VENTANA
				,AtributoEntradaDTO_PML.PROCENTAJE_CRISTAL_FACHADAS
				,AtributoEntradaDTO_PML.PORCENTAJE_DOMOS
				,AtributoEntradaDTO_PML.OTROS_FACHADA
				,AtributoEntradaDTO_PML.MUROS_CONTENCION
				,AtributoEntradaDTO_PML.PRIMA
				,AtributoEntradaDTO_PML.CEDIDA
				,AtributoEntradaDTO_PML.RETENIDA
				,AtributoEntradaDTO_PML.MONEDA
				,AtributoEntradaDTO_PML.RSR_T
				,AtributoEntradaDTO_PML.OFI_EMI
				,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.VALOR_RETENIDO
				,AtributoEntradaDTO_PML.INCISO
				,AtributoEntradaDTO_PML.ZONA_AMIS
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_REGISTRO
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
				,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONVENIO_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.TIPO_PRIMER_RIESGO
				,ParametroSalidaSP_PML.MONTO_PRIMER_RIESGO	
				,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONSEC_PERIODO_COBERTURA
				,ParametroSalidaSP_PML.CONVENIO_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.INM_DEDUCIBLE
				,ParametroSalidaSP_PML.CONT_DEDUCIBLE
				,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
				,ParametroSalidaSP_PML.CONVENIO_DEDUCIBLE
				,ParametroSalidaSP_PML.INM_COASEGURO
				,ParametroSalidaSP_PML.CONT_COASEGURO
				,ParametroSalidaSP_PML.CONSEC_COASEGURO
				,ParametroSalidaSP_PML.CONVENIO_COASEGURO
				,ParametroSalidaSP_PML.CLAVE_ESTADO
				,ParametroSalidaSP_PML.CODIGO_POSTAL
				,ParametroSalidaSP_PML.LONGITUD
				,ParametroSalidaSP_PML.LATITUD
				,ParametroSalidaSP_PML.PRIMERA_LINEA_MAR
				,ParametroSalidaSP_PML.PRIMERA_LINEA_LAGO
				,ParametroSalidaSP_PML.SOBREELEVACION_DESPLANTE
				,ParametroSalidaSP_PML.RUGOSIDAD
				,ParametroSalidaSP_PML.USO_INMUEBLE
				,ParametroSalidaSP_PML.NUM_PISOS
				,ParametroSalidaSP_PML.PISO
				,ParametroSalidaSP_PML.TIPO_CUBIERTA
				,ParametroSalidaSP_PML.FORMA_CUBIERTA
				,ParametroSalidaSP_PML.IRRE_PLANTA
				,ParametroSalidaSP_PML.OBJETOS_CERCA
				,ParametroSalidaSP_PML.AZOTEA
				,ParametroSalidaSP_PML.TAMANO_CRISTAL
				,ParametroSalidaSP_PML.TIPO_VENTANAS
				,ParametroSalidaSP_PML.TIPO_DOMOS
				,ParametroSalidaSP_PML.SOPORTE_VENTANA
				,ParametroSalidaSP_PML.PROCENTAJE_CRISTAL_FACHADAS
				,ParametroSalidaSP_PML.PORCENTAJE_DOMOS
				,ParametroSalidaSP_PML.OTROS_FACHADA
				,ParametroSalidaSP_PML.MUROS_CONTENCION
				,ParametroSalidaSP_PML.PRIMA
				,ParametroSalidaSP_PML.CEDIDA
				,ParametroSalidaSP_PML.RETENIDA
				,ParametroSalidaSP_PML.MONEDA
				,ParametroSalidaSP_PML.RSR_T
				,ParametroSalidaSP_PML.OFI_EMI
				,ParametroSalidaSP_PML.VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.VALOR_RETENIDO
				,ParametroSalidaSP_PML.INCISO
				,ParametroSalidaSP_PML.ZONA_AMIS
		};
		
		MidasHojaXLS hojaPMLHidroIndependientes = new MidasHojaXLS("Independientes",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidroIndependientes;
	}
}
