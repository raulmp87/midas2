package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.reportemonitoreosol.ReporteMonitoreoSolicitudesDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PlantillaReporteMonitoreoSolicitudes extends MidasPlantillaBase {
    private List<ReporteMonitoreoSolicitudesDTO> listaMonitoreoSolicitudes;
    public PlantillaReporteMonitoreoSolicitudes(List<ReporteMonitoreoSolicitudesDTO> listaMonitoreoSolicitudes){
	super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.reportemonitoreosolicitudes.paquete"));
	super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.reporteMonitoreoSolicitudes"));
	this.listaMonitoreoSolicitudes = listaMonitoreoSolicitudes;
	setTipoReporte( Sistema.TIPO_XLS );
    	}
    
    public byte[] obtenerReporte(String claveUsuario) throws SystemException {
	if(listaMonitoreoSolicitudes==null || listaMonitoreoSolicitudes.isEmpty()){
	    return null;
	}
	super.setListaRegistrosContenido(new ArrayList<Object>());
	getListaRegistrosContenido().addAll(listaMonitoreoSolicitudes);
	setParametrosVariablesReporte(new HashMap<String,Object>());
	getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
	try {
		super.setByteArrayReport( generaReporte(getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
				getParametrosVariablesReporte(), getListaRegistrosContenido()));
	} catch (JRException e) {
		setByteArrayReport( null );
		throw new SystemException("Ocurri� un error al generar el reporte: "+e.getMessage());
	}
	
	return getByteArrayReport();
	
    }
}
