package mx.com.afirme.midas.danios.reportes.cotizacion;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public abstract class ReporteCotizacionBase extends MidasReporteBase{
	protected BigDecimal idToCotizacion;
	protected CotizacionDTO cotizacionDTO;
	protected List<SeccionCotizacionDTO> listaSeccionesContratadasCotizacion;
	protected Map<Integer,List<SeccionCotizacionDTO>> mapaSeccionesContratadasPorNumeroInciso;
	protected List<CoberturaCotizacionDTO> listaCoberturasContratadasCotizacion;
	protected Map<Integer[],List<CoberturaCotizacionDTO>> mapaCoberturasContratadasPorSeccion;
	protected Map<BigDecimal,List<CoberturaCotizacionDTO>> mapaCoberturasContratadasBasicasPorInciso;
	protected List<RiesgoCotizacionDTO> listaRiesgosContratadosCotizacion;
	protected Map<BigDecimal[],List<RiesgoCotizacionDTO>> mapaRiesgosContratadosPorSeccionIncisoCobertura;
	protected List<SubIncisoCotizacionDTO> listaSubIncisosCotizacion;
	protected Map<BigDecimal[],List<SubIncisoCotizacionDTO>> mapaSubIncisosPorSeccionInciso;
	protected Map<BigDecimal,List<SubIncisoCotizacionDTO>> mapaSubIncisosPorInciso;
	protected List<MovimientoCotizacionEndosoDTO> movimientosCotizacion;
	protected List<IncisoCotizacionDTO> listaIncisos;
	private String numeroCotizacion;
	protected BigDecimal idTcRamo;
	protected BigDecimal idTcSubRamo;
	protected boolean esEndoso;
	
	protected List<ConfiguracionDatoIncisoCotizacionDTO> listaConfiguracionDatosIncisoCotizacion;
	protected Map<BigDecimal[],List<ConfiguracionDatoIncisoCotizacionDTO>> mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo;
	protected List<SubRamoDTO> listaSubRamosContratados;
	
	/**
	 * Declaraci�n de constantes utilizadas en las plantillas.
	 */
	public static final String PRIMER_RIESGO_RELATIVO = "Primer Riesgo Relativo";
	public static final String ABREVIATURAS_LUC = "L.U.C.";
	public static final String SECCION_NO_DISPONIBLE = "Secci�n no disponible";
	public static final String AMPARADA = "Amparada";
	public static final String TITULO_SUBINCISOS_SECCION = "SUBINCISOS DE LA SECCI�N ";
	public static final String SA_SEGUN_RELACION_ADJUNTA = "S/A seg�n relaci�n adjunta";
	public static final String UBICACION = "Ubicaci�n";
	public static final String BIENES = "Bien(es)";
	public static final String NO_DISPONIBLE = "NO DISPONIBLE";
	
	/**
	 * Consulta la informaci�n completa de la cotizaci�n, incluyendo sus secciones, coberturas y riesgos contratados, adem�s de 
	 * sus incisos, subincisos y la lista de configuraciones de datos de riesgo que deben ser mostradas en la impresi�n.
	 * Las listas son guardadas en los atributos del ReporteBase.
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	public void consultarInformacionCotizacion(String nombreUsuario) throws SystemException{
		if (cotizacionDTO == null && idToCotizacion != null)
			cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		listaSeccionesContratadasCotizacion = SeccionCotizacionDN.getInstancia().listarSeccionesContratadasPorCotizacion(idToCotizacion,(short)1,true);
		listaCoberturasContratadasCotizacion = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(idToCotizacion,true);
		listaRiesgosContratadosCotizacion = RiesgoCotizacionDN.getInstancia().listarRiesgosContratadosPorCotizacion(idToCotizacion);
		listaSubIncisosCotizacion = SubIncisoCotizacionDN.getInstancia().listarSubIncisosPorCotizacion(idToCotizacion);
		listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
		listarSubRamosContratados();
		obtenerConfiguracionDatosIncisoImprimibles();
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> obtenerConfiguracionDatosIncisoImprimibles() throws SystemException{
		if(listaConfiguracionDatosIncisoCotizacion == null){
			listaConfiguracionDatosIncisoCotizacion = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			for(SubRamoDTO subRamoDTO : listarSubRamosContratados()){
				ConfiguracionDatoIncisoCotizacionDTO configuracionDatoFiltro = 
					ConfiguracionDatoIncisoCotizacionDN.getInstancia().instanciarConfiguracionDatoIncisoCotizacion((short)0,null,
							subRamoDTO.getRamoDTO().getIdTcRamo(),subRamoDTO.getIdTcSubRamo(),null);
				configuracionDatoFiltro.setClaveimpresionpoliza((short)1);
				List<ConfiguracionDatoIncisoCotizacionDTO> listaConfigDatos = ConfiguracionDatoIncisoCotizacionDN.getInstancia().listarFiltrado(configuracionDatoFiltro);
				if(listaConfigDatos != null){
					listaConfiguracionDatosIncisoCotizacion.addAll(listaConfigDatos);
				}
			}
		}
		return this.listaConfiguracionDatosIncisoCotizacion;
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> obtenerConfiguracionDatosIncisoImprimibles(BigDecimal idTcRamo,BigDecimal idTcSubRamo) throws SystemException{
		if(idTcRamo == null || idTcSubRamo == null)
			return null;
		obtenerConfiguracionDatosIncisoImprimibles();
		if(mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo == null)
			mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo = new HashMap<BigDecimal[], List<ConfiguracionDatoIncisoCotizacionDTO>>();
		BigDecimal []key = {idTcRamo,idTcSubRamo};
		List<ConfiguracionDatoIncisoCotizacionDTO> listaConfDatoPorRamo_SubRamo = mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo.get(key);
		if(listaConfDatoPorRamo_SubRamo == null){
			listaConfDatoPorRamo_SubRamo = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			for(ConfiguracionDatoIncisoCotizacionDTO configDatoTMP : listaConfiguracionDatosIncisoCotizacion){
				if(configDatoTMP.getId().getIdTcRamo().compareTo(idTcRamo) == 0 && configDatoTMP.getId().getIdTcSubramo().compareTo(idTcSubRamo) == 0)
					listaConfDatoPorRamo_SubRamo.add(configDatoTMP);
			}
			mapaConfiguracionDatosIncisoCotPorIdRamoIdSubRamo.put(key, listaConfDatoPorRamo_SubRamo);
		}
		return listaConfDatoPorRamo_SubRamo;
	}
	
	/**
	 * Consulta la lista de incisos de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarIncisosCotizacion() throws SystemException{
		listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
	}
	
	/**
	 * Consulta la lista de coberturas contratadas (claveContrato = 1) de la cotizaci�n.
	 * @throws SystemException
	 */
	public void consultarCoberturasContratadasCotizacion() throws SystemException{
		listaCoberturasContratadasCotizacion = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadasParaReporte(idToCotizacion);
	}
	
	public List<SeccionCotizacionDTO> obtenerSeccionesContratadas(BigDecimal numeroInciso){
		if (this.listaSeccionesContratadasCotizacion != null){
			if (mapaSeccionesContratadasPorNumeroInciso == null){
				mapaSeccionesContratadasPorNumeroInciso = new HashMap<Integer,List<SeccionCotizacionDTO>>();
			}
			Integer keyNumeroInciso = Integer.valueOf(numeroInciso.toString());
			List<SeccionCotizacionDTO> listaSeccionesContratadasPorInciso = mapaSeccionesContratadasPorNumeroInciso.get(keyNumeroInciso);
			if (listaSeccionesContratadasPorInciso == null){
				listaSeccionesContratadasPorInciso = new ArrayList<SeccionCotizacionDTO>();
				for(SeccionCotizacionDTO seccionCot : listaSeccionesContratadasCotizacion){
					if (seccionCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0)
						listaSeccionesContratadasPorInciso.add(seccionCot);
				}
				mapaSeccionesContratadasPorNumeroInciso.put(keyNumeroInciso, listaSeccionesContratadasPorInciso);
			}
			return listaSeccionesContratadasPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas por seccion e inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadas(BigDecimal idToSeccion,BigDecimal numeroInciso){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasPorSeccion == null){
				mapaCoberturasContratadasPorSeccion = new HashMap<Integer[],List<CoberturaCotizacionDTO>>();
			}
			Integer keySeccion = Integer.valueOf(idToSeccion.toString());
			Integer keyNumeroInciso = Integer.valueOf(numeroInciso.toString());
			Integer[] keyCoberturasContratadas = new Integer[2];
			keyCoberturasContratadas[0] = keyNumeroInciso;
			keyCoberturasContratadas[1] = keySeccion;
			List<CoberturaCotizacionDTO> listaCoberturasContratadasPorSeccion = mapaCoberturasContratadasPorSeccion.get(keyCoberturasContratadas);
			if (listaCoberturasContratadasPorSeccion == null){
				listaCoberturasContratadasPorSeccion = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 && coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) ==0)
						listaCoberturasContratadasPorSeccion.add(coberturaCot);
				}
				mapaCoberturasContratadasPorSeccion.put(keyCoberturasContratadas, listaCoberturasContratadasPorSeccion);
			}
			return listaCoberturasContratadasPorSeccion;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la lista de coberturas contratadas B�sicas n�mero de inciso.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasContratadasBasicasPorInciso(BigDecimal numeroInciso){
		if (this.listaCoberturasContratadasCotizacion != null){
			if (mapaCoberturasContratadasBasicasPorInciso == null){
				mapaCoberturasContratadasBasicasPorInciso = new HashMap<BigDecimal,List<CoberturaCotizacionDTO>>();
			}
			List<CoberturaCotizacionDTO> listaCoberturasContratadasBasicasPorInciso = mapaCoberturasContratadasBasicasPorInciso.get(numeroInciso);
			if (listaCoberturasContratadasBasicasPorInciso == null){
				listaCoberturasContratadasBasicasPorInciso = new ArrayList<CoberturaCotizacionDTO>();
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
					if (coberturaCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 && 
							coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA))
						listaCoberturasContratadasBasicasPorInciso.add(coberturaCot);
				}
				mapaCoberturasContratadasBasicasPorInciso.put(numeroInciso, listaCoberturasContratadasBasicasPorInciso);
			}
			return listaCoberturasContratadasBasicasPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Regresa la sumatoria de las sumas aseguradas de las coberturas contratadas 
	 * B�sicas pertenecientes al n�mero de inciso recibido.
	 * @param numeroInciso
	 * @return List<CoberturaCotizacionDTO>
	 */
	public Double obtenerSumatoriaSACoberturasBasicasPorInciso(BigDecimal numeroInciso){
		double sumatoriaSABasicas = 0d;
		List<CoberturaCotizacionDTO> coberturasBasicasInciso = obtenerCoberturasContratadasBasicasPorInciso(numeroInciso);
		if (coberturasBasicasInciso != null)
			for(CoberturaCotizacionDTO coberturaCot : coberturasBasicasInciso)
				sumatoriaSABasicas += coberturaCot.getValorSumaAsegurada().doubleValue();
		return sumatoriaSABasicas;
	}
	
	public List<RiesgoCotizacionDTO> obtenerRiesgosContratados(BigDecimal idToSeccion,BigDecimal numeroInciso,BigDecimal idToCobertura){
		if (this.listaRiesgosContratadosCotizacion != null){
			if (mapaRiesgosContratadosPorSeccionIncisoCobertura == null){
				mapaRiesgosContratadosPorSeccionIncisoCobertura = new HashMap<BigDecimal[],List<RiesgoCotizacionDTO>>();
			}
			BigDecimal[] keySeccionIncisoCobertura = new BigDecimal[3];
			keySeccionIncisoCobertura[0] = idToSeccion;
			keySeccionIncisoCobertura[1] = numeroInciso;
			keySeccionIncisoCobertura[2] = idToCobertura;
			List<RiesgoCotizacionDTO> listaRiesgosContratadosPorSeccionIncisoCobertura = mapaRiesgosContratadosPorSeccionIncisoCobertura.get(keySeccionIncisoCobertura);
			if (listaRiesgosContratadosPorSeccionIncisoCobertura == null){
				listaRiesgosContratadosPorSeccionIncisoCobertura = new ArrayList<RiesgoCotizacionDTO>();
				for(RiesgoCotizacionDTO riesgoCot : listaRiesgosContratadosCotizacion){
					if (riesgoCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 
							&& riesgoCot.getId().getNumeroInciso().compareTo(numeroInciso)==0 
								&& riesgoCot.getId().getIdToCobertura().compareTo(idToCobertura)==0)
						listaRiesgosContratadosPorSeccionIncisoCobertura .add(riesgoCot);
				}
				mapaRiesgosContratadosPorSeccionIncisoCobertura.put(keySeccionIncisoCobertura, listaRiesgosContratadosPorSeccionIncisoCobertura );
			}
			return listaRiesgosContratadosPorSeccionIncisoCobertura ;
		}
		else
			return null;
	}
	
	public List<SubIncisoCotizacionDTO> obtenerSubIncisos(BigDecimal idToSeccion,BigDecimal numeroInciso){
		if (this.listaSubIncisosCotizacion != null){
			if (mapaSubIncisosPorSeccionInciso == null){
				mapaSubIncisosPorSeccionInciso = new HashMap<BigDecimal[],List<SubIncisoCotizacionDTO>>();
			}
			BigDecimal[] keySeccionInciso = new BigDecimal[2];
			keySeccionInciso[0] = idToSeccion;
			keySeccionInciso[1] = numeroInciso;
			List<SubIncisoCotizacionDTO> listaSubIncisosPorSeccionInciso = mapaSubIncisosPorSeccionInciso.get(keySeccionInciso);
			if (listaSubIncisosPorSeccionInciso == null){
				listaSubIncisosPorSeccionInciso = new ArrayList<SubIncisoCotizacionDTO>();
				for(SubIncisoCotizacionDTO subIncisoCot : listaSubIncisosCotizacion){
					if (subIncisoCot.getId().getIdToSeccion().compareTo(idToSeccion) == 0 
							&& subIncisoCot.getId().getNumeroInciso().compareTo(numeroInciso)==0 )
						listaSubIncisosPorSeccionInciso.add(subIncisoCot);
				}
				mapaSubIncisosPorSeccionInciso.put(keySeccionInciso, listaSubIncisosPorSeccionInciso);
			}
			return listaSubIncisosPorSeccionInciso;
		}
		else
			return null;
	}
	
	public List<SubIncisoCotizacionDTO> obtenerSubIncisosPorInciso(BigDecimal numeroInciso){
		if (this.listaSubIncisosCotizacion != null){
			if (mapaSubIncisosPorInciso == null){
				mapaSubIncisosPorInciso = new HashMap<BigDecimal,List<SubIncisoCotizacionDTO>>();
			}
			List<SubIncisoCotizacionDTO> listaSubIncisosPorInciso = mapaSubIncisosPorInciso.get(numeroInciso);
			if (listaSubIncisosPorInciso == null){
				listaSubIncisosPorInciso = new ArrayList<SubIncisoCotizacionDTO>();
				for(SubIncisoCotizacionDTO subIncisoCot : listaSubIncisosCotizacion){
					if (subIncisoCot.getId().getNumeroInciso().compareTo(numeroInciso) == 0 )
						listaSubIncisosPorInciso.add(subIncisoCot);
				}
				mapaSubIncisosPorInciso.put(numeroInciso, listaSubIncisosPorInciso);
			}
			return listaSubIncisosPorInciso;
		}
		else
			return null;
	}
	
	/**
	 * Genera el Mapa con los par�metros que utilizan todas las plantillas. Los parametros espec�ficos son calculados dentro 
	 * de cada plantilla que los requiere.
	 * @param nombreUsuario
	 * @param Boolean aplicaDiasPorDevengar. Indica si se debe distribuir el monto de la prima entre los dias de vigencia
	 * @throws SystemException 
	 */
	protected void poblarParametrosComunes(String nombreUsuario,boolean poblarCuadriculaTotales) throws SystemException{
		//Los siguientes par�metros son comunes para todas las plantillas:
		
		String NO_DISPONIBLE = "--------";
		
		getMapaParametrosGeneralesPlantillas().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		getMapaParametrosGeneralesPlantillas().put("URL_IMAGEN_FONDO", Sistema.FONDO_PLANTILLA_COTIZACION);
		numeroCotizacion = UtileriasWeb.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toBigInteger().toString(), "0", 8);
		getMapaParametrosGeneralesPlantillas().put("NO_COTIZACION", "COT-" + numeroCotizacion);
	    
		if (cotizacionDTO.getIdMoneda() != null){
			MonedaDTO monedaDTO;
			try {
				monedaDTO = MonedaDN.getInstancia().getPorId(new Short(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
				getMapaParametrosGeneralesPlantillas().put("MONEDA", ((monedaDTO != null)?monedaDTO.getDescripcion() : NO_DISPONIBLE));
			} catch (Exception e) {
				getMapaParametrosGeneralesPlantillas().put("MONEDA", NO_DISPONIBLE);
			}
			
		}
		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			getMapaParametrosGeneralesPlantillas().put("FECHA_INICIO", formatter.format(cotizacionDTO.getFechaInicioVigencia()));
			esEndoso = true;
		}else{
			esEndoso = false;
			if(cotizacionDTO.getFechaInicioVigencia() != null){
				getMapaParametrosGeneralesPlantillas().put("FECHA_INICIO", formatter.format(cotizacionDTO.getFechaInicioVigencia()));
			}else{
				getMapaParametrosGeneralesPlantillas().put("FECHA_INICIO", NO_DISPONIBLE);
			}
			
		}
		if ( cotizacionDTO.getFechaFinVigencia() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			getMapaParametrosGeneralesPlantillas().put("FECHA_FIN", formatter.format(cotizacionDTO.getFechaFinVigencia()));
		}
		else
			getMapaParametrosGeneralesPlantillas().put("FECHA_FIN", NO_DISPONIBLE);
		
		if (cotizacionDTO.getIdToPersonaAsegurado() != null){						
			try{
				ClienteDTO asegurado = null;
				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
					asegurado = new ClienteDTO();
					asegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
					asegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, cotizacionDTO.getCodigoUsuarioCotizacion());
				}else{
					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), cotizacionDTO.getCodigoUsuarioCotizacion());
				}					
				if (asegurado != null){					
					String nombreAsegurado = cotizacionDTO.getNombreAsegurado() != null ? 
							  				 cotizacionDTO.getNombreAsegurado() :
							  				 ClienteAction.obtenerNombreCliente(asegurado);
					String direccionAsegurado = ClienteAction.obtenerDireccionCliente(asegurado);
															
				    getMapaParametrosGeneralesPlantillas().put("NOMBRE_CONTRATANTE", nombreAsegurado);
				    getMapaParametrosGeneralesPlantillas().put("DIRECCION_CONTRATANTE", direccionAsegurado);
				}												
			}catch(Exception exc){}										
		}
		
		int totalIncisos = 0;
		if (listaIncisos == null)
			consultarIncisosCotizacion();
		totalIncisos = listaIncisos.size();
		getMapaParametrosGeneralesPlantillas().put("TOTAL_INCISOS", ""+totalIncisos);
		
		///////DATOS PARA LA CUADR�CULA DE TOTALES
		////Datos para el primer rengl�n (totales globales)
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");
		//Sumatoria de las primas netas
		Double sumatoriaPrimaNeta = 0d;
//		for(IncisoCotizacionDTO incisoTMP : listaIncisos){
//			sumatoriaPrimaNeta += incisoTMP.getValorPrimaNeta();
//		}
		if(listaCoberturasContratadasCotizacion == null)
			consultarCoberturasContratadasCotizacion();
		//FIXME cambiar por metodo nuevo que sea especial para endosos
		ResumenCotizacionDN.getInstancia().calcularTotalesResumenCotizacion(cotizacionDTO, listaCoberturasContratadasCotizacion);
//		sumatoriaPrimaNeta = cotizacionDTO.getPrimaNetaAnual();
//		if (aplicaDiasPorDevengar)
			sumatoriaPrimaNeta = cotizacionDTO.getPrimaNetaCotizacion();
		BigDecimal totalPrimaNeta;
		totalPrimaNeta = new BigDecimal(sumatoriaPrimaNeta);
		String subTotalPrimaNetaString = fMonto.format(totalPrimaNeta);
		getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
		
		//Recargo
		FormaPagoIDTO formaPagoIDTO = null;
		try {
			formaPagoIDTO = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
		} catch (NumberFormatException e) {
		} catch (SystemException e) {}
		BigDecimal totalRecargo;
		if(formaPagoIDTO != null){
			getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
			if(poblarCuadriculaTotales){
				totalRecargo = BigDecimal.valueOf(cotizacionDTO.getMontoRecargoPagoFraccionado());
				String totalRecargoString = fMonto.format(totalRecargo);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
				
				BigDecimal totalDerecho = BigDecimal.ZERO;
				String totalDerechoString = NO_DISPONIBLE;
	
				totalDerecho = new BigDecimal(cotizacionDTO.getDerechosPoliza());
				totalDerechoString = fMonto.format(totalDerecho);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);
	
				//03/01/2010. Se debe agregar el IVA como par�metro para las plantillas, se muestra en la cuadr�cula de totales
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
				BigDecimal totalIva = BigDecimal.valueOf(cotizacionDTO.getMontoIVA());
				String totalIvaString = fMonto.format(totalIva);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
				
				BigDecimal totalPrima = BigDecimal.ZERO;
				totalPrima = totalPrimaNeta.add(totalRecargo).add(totalDerecho).add(totalIva);
				String totalPrimaNetaString = fMonto.format(totalPrima);
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
			}
			else{
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", "");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", "");
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA","");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", "");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", "");
			}
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_PRIMA_NETA", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_RECARGO", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_DERECHO", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_IVA", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_TOTAL", " ");
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_PRIMA_NETA", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_RECARGO", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_DERECHO", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_IVA", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_TOTAL", " ");
		}//fin validar formaPagoIDTO != null
		else{
			String error = "Error de comunicaci�n con el sistema Seycos, no se pudo recuperar la forma de pago.<br>Cotizacion: "+
				cotizacionDTO.getIdToCotizacion()+" Forma de pago: "+cotizacionDTO.getIdFormaPago()+" , Moneda: "+cotizacionDTO.getIdMoneda();
			LogDeMidasWeb.log(error, Level.SEVERE, null);
			throw new SystemException(error,40);
		}

		//Nombre y n�mero del agente.
		getMapaParametrosGeneralesPlantillas().put("NOMBRE_AGENTE", cotizacionDTO.getSolicitudDTO().getNombreAgente());
		getMapaParametrosGeneralesPlantillas().put("NUMERO_AGENTE", cotizacionDTO.getSolicitudDTO().getCodigoAgente().toBigInteger().toString());
		
		//Nombre del producto
		getMapaParametrosGeneralesPlantillas().put("NOMBRE_PRODUCTO", "SEGURO "+cotizacionDTO.getSolicitudDTO().getProductoDTO().getNombreComercial().toUpperCase());
	}
	
	/**
	 * Realiza una b�squeda de los movimientos realizados a una cotizaci�n y calcula la prima neta por el endoso, adem�s de los totales de recargo y 
	 * derecho que se deben cobrar por el endoso. Debe ser invocado despu�s de llamar al m�todo "poblarParametrosComunes".
	 * @param cotizacionDTO
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	protected void poblarParametrosCuadriculaTotalesEndoso(CotizacionDTO cotizacionDTO, String nombreUsuario) throws SystemException{
		///////DATOS PARA LA CUADR�CULA DE TOTALES
		////Datos para el primer rengl�n (totales globales)
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");

		EndosoIDTO endosoIDTO = new EndosoIDTO();
		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion())
		.getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		
		Integer tipoEndoso = UtileriasWeb.calculaTipoEndoso(cotizacionDTO);

		endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(
				ultimoEndoso.getId().getIdToPoliza().toString() + "|"
						+ ultimoEndoso.getId().getNumeroEndoso(), cotizacionDTO.getCodigoUsuarioModificacion(),
				cotizacionDTO.getFechaInicioVigencia(), tipoEndoso);
		
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION &&
				endosoIDTO.getCalculo().equals(Sistema.TIPO_CALCULO_SEYCOS)){
			
			String subTotalPrimaNetaString = fMonto.format(endosoIDTO.getPrimaNeta()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
			getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
			
			FormaPagoIDTO formaPagoIDTO = null;
			try {
				formaPagoIDTO = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
			} catch (NumberFormatException e) {
			} catch (SystemException e) {}			
			
			getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
			String totalRecargoString = fMonto.format(endosoIDTO.getRecargoPF()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
			
			String totalDerechoString = fMonto.format(endosoIDTO.getDerechos()*-1D);			
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);
			
			getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");		

			String totalIvaString = fMonto.format(endosoIDTO.getIva()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);	
			
			String totalPrimaNetaString = fMonto.format(endosoIDTO.getPrimaTotal()*-1D);
			getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
			getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);			
			
			
		}else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){

			//cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
			//CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).modificar(cotizacionDTO);								
			ResumenCotizacionDN.getInstancia().setTotalesResumenCambioFormaPagoImpresion(cotizacionDTO);
			if(ultimoEndoso.getIdToCotizacion()!= null){
				String subTotalPrimaNetaString = fMonto.format(0D);
				getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);	
				FormaPagoIDTO formaPagoIDTO = null;
				try {
					formaPagoIDTO = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
				} catch (NumberFormatException e) {
				} catch (SystemException e) {}

				if(formaPagoIDTO != null){
					getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
					getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
					
					String totalRecargoString = fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
							
					String totalDerechoString = fMonto.format(0D);
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);

					getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
					getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
					
					String totalIvaString = fMonto.format(cotizacionDTO.getMontoIVA());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
					
					String totalPrimaNetaString = fMonto.format(cotizacionDTO.getPrimaNetaTotal());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				}
			}			
		}else{
			
			ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO, new ArrayList<SoporteResumen>());
			
			String subTotalPrimaNetaString = fMonto.format(cotizacionDTO.getPrimaNetaCotizacion());
			getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
			getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);			
			
			FormaPagoIDTO formaPagoIDTO = null;
			try {
				formaPagoIDTO = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
			} catch (NumberFormatException e) {
			} catch (SystemException e) {}
			
			if(formaPagoIDTO != null){
				getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
				getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
	
				String totalRecargoString = fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);
						
				String totalDerechoString = fMonto.format(cotizacionDTO.getDerechosPoliza());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);
				
				Double ivaObtenido= CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), "");
				
				if (ivaObtenido != null){
					//03/01/2010. Se debe agregar el IVA como par�metro para las plantillas, se muestra en la cuadr�cula de totales
					getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
					getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
	
					String totalIvaString = fMonto.format(cotizacionDTO.getMontoIVA());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
					
					String totalPrimaNetaString = fMonto.format(cotizacionDTO.getPrimaNetaTotal());
					getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
					getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				
				}//fin validar IVA != null
				else{
					String error = "Error de comunicaci�n con la base de datos, no se pudo obtener un valor para el IVA.";
					LogDeMidasWeb.log(error, Level.SEVERE, null);
					throw new SystemException(error,40);
				}
			}//fin validar formaPagoIDTO != null
					
		}
			
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_PRIMA_NETA", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_RECARGO", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_DERECHO", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_IVA", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_TOTAL", " ");
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");	

		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_PRIMA_NETA", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_RECARGO", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_DERECHO", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_IVA", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_TOTAL", " ");
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");			
	}
	
	//Getters y setters
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public void consultarMovimientosCotizacion(CotizacionDTO cotizacionDTO,String claveUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(claveUsuario);
		MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
		dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		List<MovimientoCotizacionEndosoDTO> listaMovimientosTMP = movimientoCotizacionEndosoDN.listarFiltrado(dto);
		//Consultar las secciones
		Map<BigDecimal,SeccionDTO> mapaSeccionesTMP = new HashMap<BigDecimal, SeccionDTO>();
		for(MovimientoCotizacionEndosoDTO movimiento : listaMovimientosTMP){
			if(movimiento.getIdToSeccion().intValue() != 0)
				if(!mapaSeccionesTMP.containsKey(movimiento.getIdToSeccion()))
					mapaSeccionesTMP.put(movimiento.getIdToSeccion(), SeccionDN.getInstancia().getPorId(movimiento.getIdToSeccion()));
		}
		//separar los movimientos sin seccion de los que si tienen seccion
		this.movimientosCotizacion = new ArrayList<MovimientoCotizacionEndosoDTO>();
		List<MovimientoCotizacionEndosoDTO> listaMovimientosSinSeccion = new ArrayList<MovimientoCotizacionEndosoDTO>();
		List<MovimientoCotizacionEndosoDTO> listaMovimientosSeccion = new ArrayList<MovimientoCotizacionEndosoDTO>();
		for(int i=0;i<listaMovimientosTMP.size();i++){
			if(listaMovimientosTMP.get(i).getIdToSeccion().intValue() != 0)
				listaMovimientosSeccion.add(listaMovimientosTMP.get(i));
			else
				listaMovimientosSinSeccion.add(listaMovimientosTMP.get(i));
		}
		this.movimientosCotizacion.addAll(listaMovimientosSinSeccion);
		//Ordenar todos los movimientos que tienen seccion
		MovimientoCotizacionEndosoDTO []arrayMovimientosSeccion = new MovimientoCotizacionEndosoDTO[listaMovimientosSeccion.size()];
		for(int i=0;i<arrayMovimientosSeccion.length;i++){
			arrayMovimientosSeccion[i]=listaMovimientosSeccion.get(i);
		}
		for(int i=0;i<arrayMovimientosSeccion.length;i++){
			for(int j=i+1;j<arrayMovimientosSeccion.length;j++){
				SeccionDTO seccionI = mapaSeccionesTMP.get(arrayMovimientosSeccion[i].getIdToSeccion());
				SeccionDTO seccionJ = mapaSeccionesTMP.get(arrayMovimientosSeccion[j].getIdToSeccion());
				if(seccionI.getNumeroSecuencia().intValue() > seccionJ.getNumeroSecuencia().intValue()){
					MovimientoCotizacionEndosoDTO movTMP = arrayMovimientosSeccion[i];
					arrayMovimientosSeccion[i]=arrayMovimientosSeccion[j];
					arrayMovimientosSeccion[j]=movTMP;
				}
			}
		}
		for(int i=0;i<arrayMovimientosSeccion.length;i++){
			this.movimientosCotizacion.add(arrayMovimientosSeccion[i]);
		}
	}
	
	/**
	 * Calcula la lista de los diferentes registros SubRamoDTO contratados en la cotizaci�n, utilizando 
	 * la lista de registros CoberturaCotizacionDTO.
	 * @return
	 * @throws SystemException
	 */
	public List<SubRamoDTO> listarSubRamosContratados() throws SystemException{
		if (listaCoberturasContratadasCotizacion == null)
			consultarCoberturasContratadasCotizacion();
		if (listaSubRamosContratados == null){
			listaSubRamosContratados = new ArrayList<SubRamoDTO>();
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadasCotizacion){
				if (!listaSubRamosContratados.contains(coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO()))
					listaSubRamosContratados.add(coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO());
			}
		}
		return listaSubRamosContratados;
	}
	
	protected List<TexAdicionalCotDTO> consultarTextosAdicionales() throws ExcepcionDeAccesoADatos, SystemException{
		TexAdicionalCotDTO textoAdicional = new TexAdicionalCotDTO();
		textoAdicional.setCotizacion(cotizacionDTO);
		return TexAdicionalCotDN.getInstancia().listarFiltrado(textoAdicional);
	}
	
	protected IncisoCotizacionDTO[] ordenarMapaIncisos(Map<BigDecimal,IncisoCotizacionDTO> mapaIncisosModificados){
		IncisoCotizacionDTO[] arrayIncisosModificados = new IncisoCotizacionDTO[mapaIncisosModificados.size()];
		int i = 0;
		for(IncisoCotizacionDTO incisoCot : mapaIncisosModificados.values()){
			arrayIncisosModificados[i] = incisoCot;
			i++;
		}
		
		for(i=0; i< mapaIncisosModificados.size(); i++){
			for(int j=0; j<mapaIncisosModificados.size(); j++){
				int incisoI = arrayIncisosModificados[i].getId().getNumeroInciso().intValue();
				int incisoJ = arrayIncisosModificados[j].getId().getNumeroInciso().intValue();
				if ( incisoI < incisoJ ){
					IncisoCotizacionDTO incisoTMP = arrayIncisosModificados[i];
					arrayIncisosModificados[i] = arrayIncisosModificados[j];
					arrayIncisosModificados[j] = incisoTMP;
				}
			}
		}
		return arrayIncisosModificados;
	}
	
	public List<MovimientoCotizacionEndosoDTO> getMovimientosCotizacion() {
		return movimientosCotizacion;
	}
	public void setMovimientosCotizacion(List<MovimientoCotizacionEndosoDTO> movimientosCotizacion) {
		this.movimientosCotizacion = movimientosCotizacion;
	}

	public List<SeccionCotizacionDTO> getListaSeccionesContratadasCotizacion() {
		return listaSeccionesContratadasCotizacion;
	}
	public void setListaSeccionesContratadasCotizacion(List<SeccionCotizacionDTO> listaSeccionesContratadasCotizacion) {
		this.listaSeccionesContratadasCotizacion = listaSeccionesContratadasCotizacion;
	}
	public List<CoberturaCotizacionDTO> getListaCoberturasContratadasCotizacion() {
		return listaCoberturasContratadasCotizacion;
	}
	public void setListaCoberturasContratadasCotizacion(List<CoberturaCotizacionDTO> listaCoberturasContratadasCotizacion) {
		this.listaCoberturasContratadasCotizacion = listaCoberturasContratadasCotizacion;
	}
	public List<RiesgoCotizacionDTO> getListaRiesgosContratadosCotizacion() {
		return listaRiesgosContratadosCotizacion;
	}
	public void setListaRiesgosContratadosCotizacion(List<RiesgoCotizacionDTO> listaRiesgosContratadosCotizacion) {
		this.listaRiesgosContratadosCotizacion = listaRiesgosContratadosCotizacion;
	}
	public List<SubIncisoCotizacionDTO> getListaSubIncisosCotizacion() {
		return listaSubIncisosCotizacion;
	}
	public void setListaSubIncisosCotizacion(List<SubIncisoCotizacionDTO> listaSubIncisosCotizacion) {
		this.listaSubIncisosCotizacion = listaSubIncisosCotizacion;
	}
	public List<IncisoCotizacionDTO> getListaIncisos() {
		return listaIncisos;
	}
	public void setListaIncisos(List<IncisoCotizacionDTO> listaIncisos) {
		this.listaIncisos = listaIncisos;
	}
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}
	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}
	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}
	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
}
