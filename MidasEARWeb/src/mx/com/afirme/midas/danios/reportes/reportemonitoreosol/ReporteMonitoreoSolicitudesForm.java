package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.usuario.UsuarioDN;
import mx.com.afirme.midas.usuario.UsuarioDTO;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class ReporteMonitoreoSolicitudesForm extends MidasBaseForm {
    private static final long serialVersionUID = 1L;
    private static final SimpleDateFormat formato= new SimpleDateFormat("dd/MM/yyyy");
    
    private Date fechaInicio;
    private Date fechaFinal;
    private BigDecimal idProducto;
    private Integer codigoAgente;
    private String codigoUsuarioSolicitud;
    private String fechaInicioStr;
    private String fechaFinalStr;
    private String codigoAgenteStr;
    private String codigoAgenteStr2;
    private String idProductoStr;
    private String idOficina;
    
    private  List<AgenteDTO> listaEjecutivos;
    private List<ProductoDTO> listaProductos;
    private List<AgenteDTO> listaAgentes;
    private List<UsuarioDTO> listaUsuarios;
    private String sinDatos;
    
    
    public Date getFechaInicio() {
	if(getFechaInicioStr()!=null){
	    try{
		fechaInicio= formato.parse(getFechaInicioStr().trim());
	    }catch (Exception e) {
		// TODO: handle exception
	    }
	}
         return fechaInicio;
    }
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public Date getFechaFinal() {
	if(getFechaFinalStr()!=null){
	    try{
		fechaFinal= formato.parse(getFechaFinalStr().trim());
	    }catch (Exception e) {
		// TODO: handle exception
	    }
	}
        return fechaFinal;
    }
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    
    public String toString(){
	String cadena="";
	if(getFechaInicio()!=null && getFechaFinal()!=null){
	cadena = "tituloReporte = Reporte Monitoreo de Solicitudes ";
	cadena += ", fechaInicio = "+formato.format(getFechaInicio())+" fechaFinal= "+formato.format(getFechaFinal());
	}
	
	return cadena;
    }

    public void setListaProductos(List<ProductoDTO> listaProductos) {
	this.listaProductos = listaProductos;
    }
    public List<ProductoDTO> getListaProductos() {
	return listaProductos;
    }
    public void setListaAgentes(List<AgenteDTO> listaAgentes) {
	this.listaAgentes = listaAgentes;
    }
    public List<AgenteDTO> getListaAgentes() {
	return listaAgentes;
    }
    public void setListaUsuarios(List<UsuarioDTO> listaUsuarios) {
	this.listaUsuarios = listaUsuarios;
    }
    public List<UsuarioDTO> getListaUsuarios() {
	return listaUsuarios;
    }
    public void setIdProductoStr(String idProductoStr) {
	this.idProductoStr = idProductoStr;
    }
    public BigDecimal getIdProducto() {
	 this.idProducto =null;
	if(!UtileriasWeb.esCadenaVacia(getIdProductoStr())){
	    try {
		this.idProducto = UtileriasWeb.regresaBigDecimal(getIdProductoStr().trim());
	    } catch (SystemException e) {  }
	}
	return idProducto;
    }

    public String getIdProductoStr() {
	return idProductoStr;
    }
    public void setIdProducto(BigDecimal idProducto) {
	this.idProducto = idProducto;
    }

    public void setCodigoAgenteStr(String codigoAgenteStr) {
	this.codigoAgenteStr = codigoAgenteStr;
    }
    public String getCodigoAgenteStr() {
	return codigoAgenteStr;
    }


    public void setCodigoAgente(Integer codigoAgente) {
	this.codigoAgente = codigoAgente;
    }
    public Integer getCodigoAgente() {
	 codigoAgente=null;
	if(!UtileriasWeb.esCadenaVacia(codigoAgenteStr)){
	    codigoAgente=Integer.valueOf(codigoAgenteStr.trim());
	}
	return codigoAgente;
    }
    public void setCodigoUsuarioSolicitud(String codigoUsuarioSolicitud) {
	this.codigoUsuarioSolicitud = codigoUsuarioSolicitud;
    }
    public String getCodigoUsuarioSolicitud() {
	return codigoUsuarioSolicitud;
    }
    public void setFechaInicioStr(String fechaInicioStr) {
	this.fechaInicioStr = fechaInicioStr;
    }
    public String getFechaInicioStr() {
	return fechaInicioStr;
    }
    public void setFechaFinalStr(String fechaFinalStr) {
	this.fechaFinalStr = fechaFinalStr;
    }
    public String getFechaFinalStr() {
	return fechaFinalStr;
    }
    
    public ActionErrors validate(ActionMapping mapping,
		HttpServletRequest request) {
	
	ActionErrors errors = super.validate(mapping, request);
	if (errors == null) {
		errors = new ActionErrors();
	}
	mx.com.afirme.midas.interfaz.agente.AgenteDN agenteDN = mx.com.afirme.midas.interfaz.agente.AgenteDN.getInstancia();
  	Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
	if(this.listaEjecutivos==null){
	    List<AgenteDTO> ejecutivos;
	    try {
		ejecutivos = agenteDN.listaEjecutivos(usuario.getNombreUsuario());
		if(ejecutivos != null && !ejecutivos.isEmpty()) {
		    setListaEjecutivos(ejecutivos);
		} else {
		   setListaEjecutivos(new ArrayList<AgenteDTO>());
		}
	    } catch (Throwable e) {
		e.printStackTrace();
		setListaEjecutivos(new ArrayList<AgenteDTO>());
	    }
		
		
	}
        if(this.listaAgentes == null) {
            List<AgenteDTO> agentes;
            try {
        	if(this.idOficina!=null){
                    agentes = agenteDN.listaEjecutivoAgentes(this.idOficina, usuario.getNombreUsuario());
                    if(agentes != null && !agentes.isEmpty()) {
                	this.listaAgentes = agentes;
                    } else {
                	this.listaAgentes = new ArrayList<AgenteDTO>();
                    }
        	}
        	} catch (SystemException e) {
        	    e.printStackTrace();
        	    this.listaAgentes = new ArrayList<AgenteDTO>();
        	}
        		
        }
	
	
	if(this.listaUsuarios == null) {
	       
		List<UsuarioDTO> usuarios;
		
		    usuarios = UsuarioDN.getInstancia().listarTodos();
			if(usuarios != null && !usuarios.isEmpty()) {
				this.listaUsuarios = usuarios;
			} else {
				this.listaUsuarios = new ArrayList<UsuarioDTO>();
			}
		
		
	}
	return errors;
}
    public void setCodigoAgenteStr2(String codigoAgenteStr2) {
	this.codigoAgenteStr2 = codigoAgenteStr2;
    }
    public String getCodigoAgenteStr2() {
	return codigoAgenteStr2;
    }
    public void setListaEjecutivos(List<AgenteDTO> listaEjecutivos) {
	this.listaEjecutivos = listaEjecutivos;
    }
    public List<AgenteDTO> getListaEjecutivos() {
	return listaEjecutivos;
    }
    public void setIdOficina(String idOficina) {
	this.idOficina = idOficina;
    }
    public String getIdOficina() {
	return idOficina;
    }
    public void setSinDatos(String sinDatos) {
	this.sinDatos = sinDatos;
    }
    public String getSinDatos() {
	return sinDatos;
    }

}
