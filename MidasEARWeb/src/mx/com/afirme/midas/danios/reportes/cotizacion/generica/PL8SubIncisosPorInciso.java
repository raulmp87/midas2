package mx.com.afirme.midas.danios.reportes.cotizacion.generica;

import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.PlantillaSubIncisosBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL8SubIncisosPorInciso extends PlantillaSubIncisosBase{

	public PL8SubIncisosPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.subIncisosPorInciso") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionGenerica.paquete") );
	}
	
	public PL8SubIncisosPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase midasReporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,midasReporteBase);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.generica.subIncisosPorInciso") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionGenerica.paquete") );
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		super.poblarParametroNumeroInciso();
		super.poblarSubIncisos(nombreUsuario);
		return getByteArrayReport();
	}
	
}
