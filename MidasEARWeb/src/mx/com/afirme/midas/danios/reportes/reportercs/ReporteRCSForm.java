package mx.com.afirme.midas.danios.reportes.reportercs;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteRCSForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String fechaInicio;
	private String id_ramo;
	private int numeroCortes;
	private String claveNegocio;
	private String nomenclatura;
	private String tipoArchivo;
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}
	
	public String getId_ramo() {
		return id_ramo;
	}

	public void setId_ramo(String id_ramo) {
		this.id_ramo = id_ramo;
	}

	public int getNumeroCortes() {
		return numeroCortes;
	}

	public void setNumeroCortes(int numeroCortes) {
		this.numeroCortes = numeroCortes;
	}
	
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	
	public String getNomenclatura() {
		return nomenclatura;
	}

	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}
	
	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	@Override
	public String toString(){
		String cadena = "ReporteRCSForm: ";
		cadena += "fechaInicio = "+fechaInicio;
		cadena += "idTcRamo = "+id_ramo;
		cadena += "numeroCortes = "+numeroCortes;
		return cadena;
	}
}