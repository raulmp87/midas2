package mx.com.afirme.midas.danios.reportes.reporterr6;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteRR6Form extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String fechaInicio;
	private String fechaFinal;
	private String reporte;
	private String tipoCambio;
	
	
	public String getFechaInicio() {
		return fechaInicio;
	}
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public String getFechaFinal() {
		return fechaFinal;
	}
	
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	
	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Override
	public String toString(){
		String cadena = "ReporteRCSForm: ";
		cadena += "fechaInicio = "+fechaInicio;
		cadena += "fechaFinal = "+fechaFinal;
		return cadena;
	}
}