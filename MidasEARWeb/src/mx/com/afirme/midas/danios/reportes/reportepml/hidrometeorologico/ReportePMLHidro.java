package mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepml.ReportePMLBase;
import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePMLHidro extends ReportePMLBase{
	
	public ReportePMLHidro(Date fechaCorte,Double tipoCambio){
		this.fechaCorte = fechaCorte;
		this.tipoCambio = tipoCambio;
	}

	@Override
	public byte[] obtenerReportePML(TipoReportePML tipoReportePML) {
		if(tipoReportePML != null){
//			if(tipoReportePML == TipoReportePML.)
		}
		return null;
	}
	
	@Deprecated
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		List<ReportePMLHidroDTO> listareportePMLHidro = null;
		try{
			listareportePMLHidro = ReportePMLHidroDN.getInstancia().obtieneReportePMLHidro(fechaCorte, tipoCambio, claveUsuario);
		}catch(SystemException e1){
			throw new SystemException("Ocurri� un error al recuperar la informaci�n para el reporte: "+e1.getCause());
		}
		if(listareportePMLHidro == null)
			throw new SystemException ("Ocurri� un error al recuperar la informaci�n para el reporte. Lista null.");
		if(listareportePMLHidro.isEmpty())
			throw new SystemException ("No se encontraron registros para los datos introducidos.");
		PL18PlantillaPMLHidro plantilla = new PL18PlantillaPMLHidro(listareportePMLHidro);
		byte byteArray[] = null;
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		plantilla.setListaRegistrosContenido(null);
		plantilla = null;
		Runtime.getRuntime().gc();
		return byteArray;
	}
	
}
