package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.util.List;

import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCargaCNSF;
import mx.com.afirme.midas.sistema.SystemException;

public class CargaMasivaReaseguradoresDN {
	private static final CargaMasivaReaseguradoresDN INSTANCIA = new CargaMasivaReaseguradoresDN();

	public static CargaMasivaReaseguradoresDN getInstancia() {
		return CargaMasivaReaseguradoresDN.INSTANCIA;
	}
	
	public void agregar(
			List<ReaseguradorCargaCNSF> direccionesValidas)
			throws  SystemException {

		 new CargaMasivaReaseguradoresSN().agregar(
				direccionesValidas);
	}
	
	
}
