package mx.com.afirme.midas.danios.reportes.reportepml;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReportePMLForm extends MidasBaseForm{
	private static final long serialVersionUID = -5540645615197605069L;
	
	private String fechaCorte;
	private String tipoCambio;
	private String tipoReporte; //variable utilizada para indicar si se trata de : "hidro" para Hidrometeorologico o "terremoto" para "terremoto o erupcion volcanica.
	private String tituloReporte;
	private String claveTipoReporte;//parametro que indica el tipo de reporte a generar: 1.Vigentes, 2.Cancelados, 3.Rehabilitadas

	public String getClaveTipoReporte() {
		return claveTipoReporte;
	}
	public void setClaveTipoReporte(String claveTipoReporte) {
		this.claveTipoReporte = claveTipoReporte;
	}
	public String getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
	public String getTituloReporte() {
		return tituloReporte;
	}
	public void setTituloReporte(String tituloReporte) {
		this.tituloReporte = tituloReporte;
	}
	@Override
	public String toString(){
		String cadena = "tituloReporte = "+this.tituloReporte;
		cadena += ", fechaCorte = "+this.fechaCorte+", tipoCambio = "+this.tipoCambio+", tipoReporte = "+this.tipoReporte;
		return cadena;
	}
}
