package mx.com.afirme.midas.danios.reportes.cotizacion.rcfuncionarios;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL14TextosAdicionales;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL2DocumentosAdicionales;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteCotizacionRCFuncionarios extends ReporteCotizacionBase{

	public ReporteCotizacionRCFuncionarios(CotizacionDTO cotizacionDTO) {
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaPlantillas(new ArrayList<byte[]>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteCotizacionRCFuncionarios(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReporteCotizacionRCFuncionarios(String nombreUsuario) throws SystemException{
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			consultarCoberturasContratadasCotizacion();
			
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(nombreUsuario,true);
			String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
			abreviaturas = "S.E.A.";
			descripcionAbreviaturas = "Seg�n especificaci�n adjunta.";
			leyendaAbreviaturas = "Donde quiera que aparezcan  las siguientes abreviaturas se entender�:";
			getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
			getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
			
			byte[] reporteTMP=null;

			for(int i=0;i<listaIncisos.size();i++){
				IncisoCotizacionDTO incisoTMP =listaIncisos.get(i);
				MidasPlantillaBase plantillaLicitacion = new PL21_LicitacionRCFuncionarios(cotizacionDTO, incisoTMP, getMapaParametrosGeneralesPlantillas(), this);
				try {
					reporteTMP = plantillaLicitacion.obtenerReporte(nombreUsuario);
				} catch (SystemException e1) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
			
			MidasPlantillaBase plantillaDocAnexos = new PL2DocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			plantillaDocAnexos = null;
			MidasPlantillaBase plantillaTextosAdicionales = new PL14TextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			plantillaTextosAdicionales = null;
			
		} catch (SystemException e) {
			throw e;
		}
	}
}
