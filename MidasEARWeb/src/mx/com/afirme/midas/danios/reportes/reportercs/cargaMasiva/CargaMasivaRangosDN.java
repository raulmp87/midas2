package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class CargaMasivaRangosDN {
	private static final CargaMasivaRangosDN INSTANCIA = new CargaMasivaRangosDN();

	public static CargaMasivaRangosDN getInstancia() {
		return CargaMasivaRangosDN.INSTANCIA;
	}

	public CargaMasivaDetalleRanDTO getCargaMasiva()
			throws  SystemException {
		return new CargaMasivaRangosSN().getCargaMasiva();
	}
	public void eliminarCargaMasiva(BigDecimal anio,BigDecimal mes, BigDecimal dia, String cve_negocio)
			throws  SystemException {
		if (anio.intValue() != 0) {
			new CargaMasivaRangosSN().deleteEsquemas(anio,mes, dia,cve_negocio);
		}
	}
	
	public CargaMasivaDetalleRanDTO agregar(
			List<CargaMasivaRangosRamosDTO> direccionesValidas,
			List<CargaMasivaRangosRamosDTO> direccionesInvalidas)
			throws  SystemException {

		return new CargaMasivaRangosSN().agregar(
				direccionesValidas, direccionesInvalidas);
	}
	
	public EsquemasDTO agregar(
			List<EsquemasDTO> direccionesValidas)
			throws  SystemException {

		return new CargaMasivaRangosSN().agregar(
				direccionesValidas);
	}
	
	public CaducidadesDTO agregarCad(
			List<CaducidadesDTO> direccionesValidas)
			throws  SystemException {

		return new CargaMasivaRangosSN().agregarCad(
				direccionesValidas);
	}
}
