package mx.com.afirme.midas.danios.reportes.poliza.transportes;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL14_PolizaTextosAdicionales;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL2_PolizaDocumentosAdicionales;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePolizaTransportes extends ReportePolizaBase{

	public ReportePolizaTransportes(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
		this.cotizacionDTO = polizaDTO.getCotizacionDTO();
		mapaSeccionesContratadasPorNumeroInciso = null;
		setListaPlantillas(new ArrayList<byte[]>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePolizaTransportes(polizaDTO, claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReportePolizaTransportes(PolizaDTO polizaDTO, String nombreUsuario){
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			cotizacionDTO = polizaDTO.getCotizacionDTO();
			idToCotizacion = cotizacionDTO.getIdToCotizacion();
			listaSeccionesContratadasCotizacion = SeccionCotizacionDN.getInstancia().listarSeccionesContratadasPorCotizacion(idToCotizacion,(short)1);
			listaCoberturasContratadasCotizacion = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadasParaReporte(idToCotizacion);
			listaRiesgosContratadosCotizacion = RiesgoCotizacionDN.getInstancia().listarRiesgosContratadosPorCotizacion(idToCotizacion);
			listaSubIncisosCotizacion = SubIncisoCotizacionDN.getInstancia().listarSubIncisosPorCotizacion(idToCotizacion);
			listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
			
			//Poblar los par�metros comunes para todas las plantillas
			this.poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,null);
			String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
			abreviaturas = "S/VT embarque\nL.M. embarque\nMin\nMax\nDSMGVDF\nUMA";
			descripcionAbreviaturas = "Sobre valor total del embarque\nL�mite M�ximo de Suma Asegurada del embarque\ncon M�nimo de\n" +
					"Con m�ximo de\nD�as de salario m�nimo general vigente en el Distrito Federal\nUnidad de Medida y Actualización";
			leyendaAbreviaturas = "Donde quiera que aparezca en este contrato las siguientes abreviaturas se entender�:";
			getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
			getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
			
			byte[] reporteTMP=null;

			int tipoPlantillaBienesAsegurados = 0;
			if (cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("especifica") 
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("espec�fica")
						|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("especifica") != -1 
							|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("espec�fica") != -1){
				tipoPlantillaBienesAsegurados = 1;
			} else if(cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("pronostico") 
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("pron�stico")
						|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("pron�stico") != -1
							|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("pronostico") != -1){
				tipoPlantillaBienesAsegurados = 2;
			} else if(cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("anual") 
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("declaracion") 
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("declaraci�n")
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("anual") != -1
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("declaracion") != -1
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("declaraci�n") != -1){
				tipoPlantillaBienesAsegurados = 3;
			}
			
			for(IncisoCotizacionDTO incisoTMP : listaIncisos){
				
				MidasPlantillaBase plantillaBienesAseguradosPorInciso = null;
				if (tipoPlantillaBienesAsegurados == 1){
					plantillaBienesAseguradosPorInciso = new PL10_PolizaBienesAseguradosPorIncisoModEspecifica(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
				} else if (tipoPlantillaBienesAsegurados == 2){
					plantillaBienesAseguradosPorInciso = new PL15_PolizaBienesAseguradosPorIncisoModPronostico(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
				} else if (tipoPlantillaBienesAsegurados == 3){
					plantillaBienesAseguradosPorInciso = new PL16_PolizaBienesAseguradosPorIncisoModAnual(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
				}
				if(plantillaBienesAseguradosPorInciso != null){
					try {
						reporteTMP = plantillaBienesAseguradosPorInciso.obtenerReporte(nombreUsuario);
					} catch (SystemException e1) {}
					if (reporteTMP !=null){
						getListaPlantillas().add(reporteTMP);
						reporteTMP = null;
					}
				}
				
				MidasPlantillaBase plantillaDedCoasPorInciso = new PL11_PolizaDeduciblesCoasegurosPorInciso(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
				try {
					reporteTMP = plantillaDedCoasPorInciso.obtenerReporte(nombreUsuario);
				} catch (SystemException e1) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
			
			MidasPlantillaBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			plantillaDocAnexos = null;
			MidasPlantillaBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			plantillaTextosAdicionales = null;
			
		} catch (SystemException e) {}
	}
	
}
