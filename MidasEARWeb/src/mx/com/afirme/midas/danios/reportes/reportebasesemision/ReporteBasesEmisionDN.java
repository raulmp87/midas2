package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ReporteBasesEmisionDN {

	private static final ReporteBasesEmisionDN INSTANCIA = new ReporteBasesEmisionDN();
	
	public static ReporteBasesEmisionDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReporteBasesEmisionDTO> obtieneMovimientosEmision(Date fechaInicio, Date fechaFin, Integer idRamo, Integer idSubRamo,
			Integer nivelContratoReaseguro, Integer nivelAgrupamiento, String nombreUsuario)  
			throws SystemException {
		ReporteBasesEmisionSN reporteBasesEmisionSN = new ReporteBasesEmisionSN();
		ReporteBasesEmisionDTO reporteBasesEmisionFiltro = new ReporteBasesEmisionDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setIdSubRamo(idSubRamo);
		reporteBasesEmisionFiltro.setIdNivelContratoReaseguro(nivelContratoReaseguro);
		reporteBasesEmisionFiltro.setNivelAgrupamiento(nivelAgrupamiento);
		
		return reporteBasesEmisionSN.obtieneMovimientosEmision(reporteBasesEmisionFiltro, nombreUsuario);
	}
	
}
