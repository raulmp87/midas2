package mx.com.afirme.midas.danios.reportes.cotizacion.endoso.cancelacion;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.endoso.PL20MovimientosEndoso;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReporteCotizacionEndosoCancelacion extends ReporteCotizacionBase {

	public ReporteCotizacionEndosoCancelacion(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		setListaPlantillas(new ArrayList<byte[]>());
	}

	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteCotizacionEndosoCancelacion(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}

	private void generarReporteCotizacionEndosoCancelacion(String nombreUsuario) throws SystemException {
		setMapaParametrosGeneralesPlantillas(new HashMap<String, Object>());
//		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		byte[] reporteTMP = null;
//		consultarInformacionCotizacion(nombreUsuario);
		poblarParametrosComunes(nombreUsuario,false);
		
		consultarMovimientosCotizacion(cotizacionDTO, nombreUsuario);
		poblarParametrosCuadriculaTotalesEndoso(cotizacionDTO, nombreUsuario);
		PL20MovimientosEndoso plantillaGeneralMovimientos = new PL20MovimientosEndoso(cotizacionDTO, getMapaParametrosGeneralesPlantillas(),this);
		String []movimientosExtra = new String[1];
		movimientosExtra[0] = "Tipo de endoso: "+UtileriasWeb.getDescripcionCatalogoValorFijo(40, cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue());
		if(cotizacionDTO.getClaveMotivoEndoso() != null){
			if(cotizacionDTO.getClaveMotivoEndoso().intValue() >= 1 && cotizacionDTO.getClaveMotivoEndoso().intValue() <= 7){
				String descMotivo = UtileriasWeb.getDescripcionCatalogoValorFijo(42, cotizacionDTO.getClaveMotivoEndoso().intValue());
				movimientosExtra[0] += (UtileriasWeb.esCadenaVacia(descMotivo))?"":" , Motivo: " + descMotivo;
			}
		}
		plantillaGeneralMovimientos.setMovimientosGeneralesExtra(movimientosExtra);
		try {
			reporteTMP = plantillaGeneralMovimientos.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP != null) {
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
	}
}
