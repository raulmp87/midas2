package mx.com.afirme.midas.danios.reportes;

import mx.com.afirme.midas.sistema.SystemException;

public interface MidasReporte {
	/**
	 * Devuelve el arreglo de bytes generado a partir de las plantilla utilizadas generar el reporte completo. El reporte est� copmpuesto por varias plantillas.
	 * @param String claveUsuario. La clave del usuario que solicita el reporte. Es usada para obtener los datos del cliente.
	 * @return JasperPrint. El objetoJasperPrint a partir del cual se genera el archivo f�sico.
	 */
	public byte[] obtenerReporte(String claveUsuario) throws SystemException;
	
}