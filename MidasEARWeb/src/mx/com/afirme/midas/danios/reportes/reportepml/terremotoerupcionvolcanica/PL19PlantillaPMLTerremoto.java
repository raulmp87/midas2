package mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

@Deprecated
public class PL19PlantillaPMLTerremoto extends MidasPlantillaBase{
	private List<ReportePMLTEVDTO> listaRegistrosPMLTerremoto;
	public PL19PlantillaPMLTerremoto(List<ReportePMLTEVDTO> listaRegistrosPMLTerremoto){
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.PML.terremoto.paquete"));
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.PML.terremoto.plantilla"));
		this.listaRegistrosPMLTerremoto = listaRegistrosPMLTerremoto;
		setTipoReporte( Sistema.TIPO_XLS );
	}
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		if(listaRegistrosPMLTerremoto == null || listaRegistrosPMLTerremoto.isEmpty())
			return null;
		super.setListaRegistrosContenido(new ArrayList<Object>());
		getListaRegistrosContenido().addAll(listaRegistrosPMLTerremoto);
		
		setParametrosVariablesReporte(new HashMap<String,Object>());
		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
	    try {
			super.setByteArrayReport( generaReporte(getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			setByteArrayReport( null );
			throw new SystemException("Ocurri� un error al generar el reporte: "+e.getMessage());
		}
		
		return getByteArrayReport();
	}
}
