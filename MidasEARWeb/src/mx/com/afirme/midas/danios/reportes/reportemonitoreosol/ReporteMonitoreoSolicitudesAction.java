package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.usuario.UsuarioDN;
import mx.com.afirme.midas.usuario.UsuarioDTO;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReporteMonitoreoSolicitudesAction extends MidasMappingDispatchAction {
    
	public ActionForward mostrarReporteMonitoreoSolicitudes
	(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
	    ReporteMonitoreoSolicitudesForm reporteMonitoreoSolicitudesForm=(ReporteMonitoreoSolicitudesForm)form;
	    Usuario usuario= (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
	    poblarListasForm(reporteMonitoreoSolicitudesForm,usuario);
	   
	    return mapping.findForward(Sistema.EXITOSO);
	}

	public ActionForward validarDatosFiltros
	(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
	  	   
	    return mapping.findForward(Sistema.EXITOSO);
	}
	
	
	public ActionForward generarReporteMonitoreoSolicitudes(ActionMapping mapping,ActionForm form, 
		HttpServletRequest request,HttpServletResponse response) {
	    String forward = Sistema.EXITOSO;
	    ReporteMonitoreoSolicitudesForm reporteMonitoreoSolicitudesForm=(ReporteMonitoreoSolicitudesForm)form;
	    MidasReporteBase reporteMonitoreo;
	    byte[] reporte = null;
	    StringBuffer nombreArchivo;
	    try{
		reporteMonitoreo = new ReporteMonitoreoSolicitudes(reporteMonitoreoSolicitudesForm);
		if(reporteMonitoreo!=null){
		    reporte= reporteMonitoreo.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
		    if (reporte != null){
			nombreArchivo=new StringBuffer();
			nombreArchivo.append("ReporteMonitoreoSolicitudes_");
			Calendar calendario=Calendar.getInstance();
			if(!UtileriasWeb.esObjetoNulo(reporteMonitoreoSolicitudesForm.getFechaInicio())){
			    calendario.setTime(reporteMonitoreoSolicitudesForm.getFechaInicio());
			    nombreArchivo.append(calendario.get(Calendar.DAY_OF_MONTH)<=9?"0"+calendario.get(Calendar.DAY_OF_MONTH):calendario.get(Calendar.DAY_OF_MONTH));
			    nombreArchivo.append((calendario.get(Calendar.MONTH)+1)<=9?"0"+(calendario.get(Calendar.MONTH)+1):(calendario.get(Calendar.MONTH)+1));
			    nombreArchivo.append(calendario.get(Calendar.YEAR));
			    nombreArchivo.append("_");
			}
			if(!UtileriasWeb.esObjetoNulo(reporteMonitoreoSolicitudesForm.getFechaFinal())){
			    calendario.setTime(reporteMonitoreoSolicitudesForm.getFechaFinal());
			    nombreArchivo.append(calendario.get(Calendar.DAY_OF_MONTH)<=9?"0"+calendario.get(Calendar.DAY_OF_MONTH):calendario.get(Calendar.DAY_OF_MONTH));
			    nombreArchivo.append((calendario.get(Calendar.MONTH)+1)<=9?"0"+(calendario.get(Calendar.MONTH)+1):(calendario.get(Calendar.MONTH)+1));
			    nombreArchivo.append(calendario.get(Calendar.YEAR));
			 }
			this.writeBytes(response, reporte, Sistema.TIPO_XLS, nombreArchivo.toString());
			return null;
		    }else{
			throw new SystemException("Datos de entrada: "+reporteMonitoreoSolicitudesForm.toString(),20);
		    }
		}
		
	    }catch (SystemException e) {
		if(e.getMessage().startsWith("XX")){
		    request.setAttribute("titulo", "    MidasWeb  ");
		    request.setAttribute("leyenda", "No se encontraron registros.");
		}else{
		   request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
		   request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
		}
		forward = "errorImpresion";
	    } catch (IOException e) {
		request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
		request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
		forward = "errorImpresion";
	    }
	    
	    return mapping.findForward(forward);
	}
	
	 
	private void poblarListasForm(ReporteMonitoreoSolicitudesForm reporteMonitoreoSolicitudesForm, Usuario usuario){
		        mx.com.afirme.midas.interfaz.agente.AgenteDN agenteDN = mx.com.afirme.midas.interfaz.agente.AgenteDN.getInstancia();
			List<AgenteDTO> ejecutivos;
			try {
			    ejecutivos = agenteDN.listaEjecutivos(usuario.getNombreUsuario());
			    if(ejecutivos != null && !ejecutivos.isEmpty()) {
				    reporteMonitoreoSolicitudesForm.setListaEjecutivos(ejecutivos);
				} else {
				    reporteMonitoreoSolicitudesForm.setListaEjecutivos(new ArrayList<AgenteDTO>());
				}
			} catch (Throwable e) {
			    e.printStackTrace();
			    reporteMonitoreoSolicitudesForm.setListaEjecutivos(new ArrayList<AgenteDTO>());
			}
			
			reporteMonitoreoSolicitudesForm.setListaAgentes(new ArrayList<AgenteDTO>());
			
			if(reporteMonitoreoSolicitudesForm.getListaUsuarios() == null) {
			       
				List<UsuarioDTO> usuarios;
				
				    usuarios = UsuarioDN.getInstancia().listarTodos();
					if(usuarios != null && !usuarios.isEmpty()) {
					    reporteMonitoreoSolicitudesForm.setListaUsuarios(usuarios);
					} else {
					    reporteMonitoreoSolicitudesForm.setListaUsuarios(new ArrayList<UsuarioDTO>());
					}
				
			}
	}
	
	
	public void cargarAgentesPorEjecutivo(ActionMapping mapping,ActionForm form, 
		HttpServletRequest request,HttpServletResponse response){
	    	List<AgenteDTO> agentes=null;
	    	StringBuffer buffer = new StringBuffer();
	    	buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");

		try {
        	    	if(request.getParameter("idOficina") !=null){
        	    	   try {
        		    agentes = AgenteDN.getInstancia().listaEjecutivoAgentes
        		       (request.getParameter("idOficina").trim(), UtileriasWeb.obtieneNombreUsuario(request));
        		    
        		    if(agentes != null && !agentes.isEmpty()){
        			Iterator<AgenteDTO> iteratorList = agentes.iterator();
        			while (iteratorList.hasNext()) {
        			    	AgenteDTO agente = (AgenteDTO) iteratorList.next();
        				buffer.append("<item>");
        				buffer.append("<id>");
        				buffer.append(agente.getIdTcAgente());
        				buffer.append("</id>");
        				buffer.append("<description><![CDATA[");
        				buffer.append(agente.getNombre());
        				buffer.append("]]></description>");
        				buffer.append("</item>");
        			}
        			
        		    }
        		    } catch (Throwable e) {
        	    	       e.printStackTrace();
        	    	   }
        	    	}
        	    	
        	    	buffer.append("</response>");
        		response.setContentType("text/xml");
        		response.setHeader("Cache-Control", "no-cache");
        		response.setContentLength(buffer.length());
        		response.getWriter().write(buffer.toString());
		} catch (Throwable e){}
	} // End of try/catch
	

}
