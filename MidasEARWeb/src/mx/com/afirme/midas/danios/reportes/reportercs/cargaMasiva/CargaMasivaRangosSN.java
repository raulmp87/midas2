package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;


import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class CargaMasivaRangosSN {
	
	private CargaMasivaDetalleRanFacadeRemote beanRemoto;

	public CargaMasivaRangosSN() throws SystemException {
		try { 
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CargaMasivaDetalleRanFacadeRemote.class);
		} catch (Exception e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void borrar(CargaMasivaDetalleRanDTO cargaMasivaCotDTO)
			throws SystemException {
		try {
			beanRemoto.delete(cargaMasivaCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public void modificar(CargaMasivaDetalleRanDTO cargaMasivaCotDTO)
			throws SystemException {
		try {
			beanRemoto.update(cargaMasivaCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public List<CargaMasivaDetalleRanDTO> listarTodos() throws SystemException {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public List<CargaMasivaRangosRamosDTO> buscarPorPropiedad(String propiedad,
			Object valor) throws SystemException {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CargaMasivaRangosRamosDTO> getPorPropiedad(String name, Object obj)
			throws SystemException {
		try {
			return beanRemoto.findByProperty(name, obj);
		} catch (Exception e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public CargaMasivaDetalleRanDTO agregar(
			List<CargaMasivaRangosRamosDTO> direccionesValidas,
			List<CargaMasivaRangosRamosDTO> direccionesInvalidas) throws SystemException {
		try { 
			return beanRemoto.agregar2(direccionesValidas,
					direccionesInvalidas); 
		} catch (Exception e) { 
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}
	
	public EsquemasDTO agregar(
			List<EsquemasDTO> direccionesValidas) throws SystemException {
		try { 
			return beanRemoto.agregar(direccionesValidas); 
		} catch (Exception e) {   
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		} 
	} 
	
	public CaducidadesDTO agregarCad( 
			List<CaducidadesDTO> direccionesValidas) throws SystemException {
		try {  
			return beanRemoto.agregarCad(direccionesValidas); 
		} catch (Exception e) {   
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		} 
	} 
	
	public int deleteEsquemas(BigDecimal anio,BigDecimal mes, BigDecimal dia,String cve_negocio)
	{
		try {  
			return beanRemoto.deleteEsquemas(anio,mes,dia,cve_negocio);  
		} catch (Exception e) {   
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			try {
				throw new SystemException(e.getClass().getCanonicalName());
			} catch (SystemException e1) {
				LogDeMidasWeb.log("Error", Level.FINEST, e1);
				return 0;
			}
		} 
	}

	public CargaMasivaDetalleRanDTO getCargaMasiva()
			throws SystemException {
		try {
			return null;
		} catch (Exception e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

}
