package mx.com.afirme.midas.danios.reportes.cotizacion.generica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL14TextosAdicionales;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL2DocumentosAdicionales;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteCotizacionGenerica extends ReporteCotizacionBase{
	private CotizacionDTO cotizacionDTO;
	
	public ReporteCotizacionGenerica(CotizacionDTO cotizacionDTO) {
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaPlantillas(new ArrayList<byte[]>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteCotizacionGenerica(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReporteCotizacionGenerica(String nombreUsuario) throws SystemException{
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			consultarInformacionCotizacion(nombreUsuario);
			
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(nombreUsuario,true);
			String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
			abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\n";
			descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\n" +
					"Con m�ximo de:\nD�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\n" +
					"La suma asegurada para esta cobertura es el valor establecido en la cobertura b�sica especificado para la cobertura b�sica, salvo los subl�mites establecidos";
			leyendaAbreviaturas = "Donde quiera que aparezcan  las siguientes abreviaturas se entender�:";
			getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
			getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
			
			byte[] reporteTMP=null;

			for(int i=0;i<listaIncisos.size();i++){
				IncisoCotizacionDTO incisoTMP =listaIncisos.get(i);
				MidasPlantillaBase plantillaBienesAseguradosPorInciso = new PL7BienesAseguradosPorInciso(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this,i==0);
				try {
					reporteTMP = plantillaBienesAseguradosPorInciso.obtenerReporte(nombreUsuario);
				} catch (SystemException e1) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
				
				List<SubIncisoCotizacionDTO> listaSubIncisos = null;
				listaSubIncisos = obtenerSubIncisosPorInciso(incisoTMP.getId().getNumeroInciso());
				if(listaSubIncisos != null && listaSubIncisos.size() > 1){
					MidasPlantillaBase plantillaSubIncisosPorInciso = new PL8SubIncisosPorInciso(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
					try {
						reporteTMP = plantillaSubIncisosPorInciso.obtenerReporte(nombreUsuario);
					} catch (SystemException e1) {}
					if (reporteTMP !=null){
						getListaPlantillas().add(reporteTMP);
						reporteTMP = null;
					}
				}
			}
			
			MidasPlantillaBase plantillaDocAnexos = new PL2DocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			plantillaDocAnexos = null;
			MidasPlantillaBase plantillaTextosAdicionales = new PL14TextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			plantillaTextosAdicionales = null;
			
			/**
			 * 31/12/2009. Por indicaciones de Arturo y Euler, no se deben mostrar los documentos anexos en la cotizaci�n, s�lo en la p�liza.
			 * Jos� Luis Arellano B�rcenas
			 */
//			Agregar los documentos anexos.
//			List<DocAnexoCotDTO> listaDocumentosAnexos = new ArrayList<DocAnexoCotDTO>();
//			DocAnexoCotDTO docAnexo = new DocAnexoCotDTO();
//			docAnexo.setCotizacion(cotizacionDTO);
//			listaDocumentosAnexos = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexo);
//			for(DocAnexoCotDTO documento : listaDocumentosAnexos){
//				try{
//				ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getIdToControlArchivo());
//				super.adjuntarDocumentoAnexo(controlArchivo);
//				}catch(Exception e){}
//			}
		} catch (SystemException e) {
			throw e;
		}
	}
	
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}
	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
}
