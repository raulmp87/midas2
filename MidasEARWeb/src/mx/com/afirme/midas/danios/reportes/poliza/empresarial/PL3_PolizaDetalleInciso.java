package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ImpresionDN;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.poliza.PlantillaPolizaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public class PL3_PolizaDetalleInciso extends PlantillaPolizaBase{

	public PL3_PolizaDetalleInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.coaseguroDeduciblePorInciso") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}
	
	public PL3_PolizaDetalleInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteBase);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.coaseguroDeduciblePorInciso") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}

	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				generarParametrosComunes(cotizacionDTO,claveUsuario);
			}
			super.poblarParametrosPlantillaDatosGeneralesInciso();
			
			super.poblarSeccionesPorInciso();
			//01/12/09. Se debe omitir la plantilla si no hay registros para mostrar.
			if (getListaRegistrosContenido() != null)
				if (getListaRegistrosContenido().isEmpty()){
					setByteArrayReport(null);
					generarLogPlantillaSinDatosParaMostrar();
					return;
				}
					
			Map<String,Object> parametrosReporteCoberturas = new HashMap<String,Object>();
			String nombrePlantillaSubReporte = getPaquetePlantilla()+ 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.unicaUbicacion.subReporteCoberturas");
			JasperReport subReporteCoberturas = ImpresionDN.getInstancia().getJasperReport(nombrePlantillaSubReporte);
			getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
			nombrePlantillaSubReporte = getPaquetePlantilla() + 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.unicaUbicacion.subReporteRiesgos");
		    JasperReport subReporteRiesgos = ImpresionDN.getInstancia().getJasperReport(nombrePlantillaSubReporte);
			parametrosReporteCoberturas.put("SUBREPORTE_RIESGOS", subReporteRiesgos);
			getParametrosVariablesReporte().put("PARAMETROS_SUBREPORTE_COBERTURA",parametrosReporteCoberturas);
		    try {
				setByteArrayReport( ImpresionDN.getInstancia().generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+ getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()) );
			} catch (JRException e) {
				setByteArrayReport( null);
			}
		}
		else	setByteArrayReport( null);
	}
}
