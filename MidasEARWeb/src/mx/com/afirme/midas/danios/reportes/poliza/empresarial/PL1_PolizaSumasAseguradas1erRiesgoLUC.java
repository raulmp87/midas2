package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.danios.reportes.poliza.PlantillaPolizaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL1_PolizaSumasAseguradas1erRiesgoLUC extends PlantillaPolizaBase{

	public PL1_PolizaSumasAseguradas1erRiesgoLUC(BigDecimal idToCotizacion,Map<String,Object> hasHtableParametros) {
		super(idToCotizacion,hasHtableParametros);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.primerRiesgo.objetos") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosPlantilla1erRiesgoLuc(nombreUsuario,false);
		return getByteArrayReport();
	}
}
