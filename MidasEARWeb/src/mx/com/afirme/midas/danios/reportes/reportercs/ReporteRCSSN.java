package mx.com.afirme.midas.danios.reportes.reportercs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteRCSSN {
	
	private static final String ERROR = "Error";
	private static final String SP_DAN_REP_RCS = "MIDAS.PKG_SOLVENCIA_CNSF.spDAN_RepRCS";
	private static final String FECHA_INICIO = "pFechaInicio";
	private static final String FECHA_FIN = "pFechaFin";
	private static final String RAMO = "pIdRamo";
	private static final String NUM_CORTES = "pNumeroCortes";
	
	 
	private ReporteRCSFacadeRemote beanRemoto;
	
	private TipoCambioFacadeRemote tipoCambioService;
	
	public ReporteRCSSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReporteRCSFacadeRemote.class);
			
			tipoCambioService = serviceLocator.getEJB(TipoCambioFacadeRemote.class);
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public List<ReporteRCSDTO> obtieneRCS(
			ReporteRCSDTO movimientoEmision, String cveNegocio) {
		List<ReporteRCSDTO> resultados = new ArrayList<ReporteRCSDTO>();
		Double tipoCambio = 0D; 
		try {
			
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(movimientoEmision.getFechaInicio().getTime(), "MIDAS");
			
			if ("4".equals(cveNegocio))
			{
				if(movimientoEmision.getIdRamo() == 12)
				{
					resultados = beanRemoto.obtieneReportesRCSGenerico(movimientoEmision, tipoCambio, "SEYCOS.PKG_INFO_VIGOR_VIDA.spVida_RepRCS");
				}else{
					resultados = beanRemoto.obtieneRCSVidaLP(movimientoEmision);
				} 
			}
			else{
				resultados = beanRemoto.obtieneReportesRCSGenerico(movimientoEmision, tipoCambio,  SP_DAN_REP_RCS);
			}
			 
			return  resultados;
		} catch (SQLException e) { 
			 
			StringBuilder sb = new StringBuilder();
			sb.append(SP_DAN_REP_RCS);
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			
			return Collections.emptyList();
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ReporteRCSDTO> obtieneRCSVidaPas(
			ReporteRCSDTO movimientoEmision) {
		List<ReporteRCSDTO> resultados = new ArrayList<ReporteRCSDTO>();
		try {
			
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			
			resultados = beanRemoto.obtieneRCSVidaPas(movimientoEmision);
						
			return resultados != null? resultados : new ArrayList<ReporteRCSDTO>();
		} catch (SQLException e) { 
			 
			StringBuilder sb = new StringBuilder();
			sb.append(SP_DAN_REP_RCS);
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return Collections.emptyList();
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ReporteRCSDTO> obtieneDetalleRCS(
			ReporteRCSDTO movimientoEmision) {
		List<ReporteRCSDTO> resultados = new ArrayList<ReporteRCSDTO>();
		Double tipoCambio = 0D;
		try {
			
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(movimientoEmision.getFechaInicio().getTime(), "MIDAS"); 
			if(movimientoEmision.getIdRamo() == 12 || movimientoEmision.getIdRamo() == 13)
			{
				resultados = beanRemoto.obtieneReportesRCSGenerico(movimientoEmision, tipoCambio, "SEYCOS.PKG_INFO_VIGOR_VIDA.spVida_DetRepRCS");
			}
			else{
				resultados = beanRemoto.obtieneReportesRCSGenerico(movimientoEmision, tipoCambio, "MIDAS.PKG_SOLVENCIA_CNSF.spDAN_DetRepRCS");
			}
			return resultados; 
		}  catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			
		}
	}
	
	public List<ReporteRCSDTO> obtieneDetalleVigorLP(
			ReporteRCSDTO movimientoEmision) {
		Double tipoCambio = 0D;
		try {
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(movimientoEmision.getFechaInicio().getTime(), "MIDAS");
						
			return beanRemoto.obtieneReportesRCSGenerico(movimientoEmision,tipoCambio, "SEYCOS.PKG_INFO_VIGOR_VIDA.spVida_RepVigor");
			
		}  catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			
		}
	}
	
	public List<ReporteRCSDTO> obtenerReporteRangos(
			ReporteRCSDTO movimientoEmision) {
		try {
			return beanRemoto.obtieneReportesRCSGenerico(movimientoEmision, 0D,"MIDAS.PKG_SOLVENCIA_CNSF.spObtenerReporteRangos");
		}  catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			
		}
	}
	
	public String obtieneNomArchivo(
			 String nomArch) {
		try {
			 
			return beanRemoto.obtieneNomArchivo(nomArch); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("pkgDAN_Reportes.spDAN_RepBasesEmision");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + nomArch + ",");
			sb.append(FECHA_FIN + "=" + nomArch + ",");
			sb.append(RAMO + "=" + nomArch + ",");
			sb.append(NUM_CORTES + "=" + nomArch + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return null;
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public String obtenerNumRangosDuracion(
			int tipoRango) {
		try {
			 
			return beanRemoto.obtenerNumRangosDuracion(tipoRango); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("pkgDAN_Reportes.spObtenerNumRangosDuracion");
			sb.append("|");
			sb.append("tipoRango = " + tipoRango + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return "0";
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean actualizaRangosSP(
			ReporteRCSDTO movimientoEmision) {
		try {
			
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			
			return beanRemoto.actualizaRangos(movimientoEmision); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("actualizaRangosSP");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return false;
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean eliminaRangosSP(
			ReporteRCSDTO movimientoEmision) {
		try {
			return beanRemoto.eliminaRangos(movimientoEmision); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("actualizaRangosSP");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return false;
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean setRangosDuracionSP(
			Integer duracionPromedio, Integer duracionRemanente) {
		try {
			
			return beanRemoto.setRangosDuracionSP(duracionPromedio, duracionRemanente); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("setRangosDuracionSP");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + duracionPromedio + ",");
			sb.append(FECHA_FIN + "=" + duracionRemanente + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return false;
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean actualizaCalificacionesSP(int dia,
			ReporteRCSDTO movimientoEmision, int anio) {
		try {
			return beanRemoto.actualizaCalificaciones(dia,movimientoEmision, anio); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("actualizaCalificacionesSP");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return false;
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ReporteRCSDTO> obtieneREASRCS(
			ReporteRCSDTO movimientoEmision, int tipoReporte) {
		Double tipoCambio = 0D; 
		try {
			
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(movimientoEmision.getFechaInicio().getTime(), "MIDAS");
			return beanRemoto.obtieneREASRCS(movimientoEmision, tipoReporte, tipoCambio); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("pkgDAN_Reportes.spDAN_RepBasesEmision");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return Collections.emptyList();
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
	public List<ReporteRCSDTO> obtieneValidacionesRCS(
			ReporteRCSDTO movimientoEmision) {
		try {
			
			return beanRemoto.obtieneValidacionesRCS(movimientoEmision); 
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("pkg_cnsf_solvencia.obtieneValidacionesRCS");
			sb.append("|");
			sb.append(FECHA_INICIO + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append(FECHA_FIN + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append(RAMO + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append(NUM_CORTES + "=" + movimientoEmision.getNumeroCortes() + ",");
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			
			return Collections.emptyList();
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ReporteRCSDTO> obtieneClaveRamo(
			String idRamo) {
		try {
			
			return beanRemoto.obtieneClaveRamo(idRamo,"1");    
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("PKG_SOLVENCIA_CNSF.sp_claveRamo");
			sb.append("|");
			sb.append("id_ramo" + "=" + idRamo + ",");
			
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return Collections.emptyList();
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}


	public void actualizaNomSP(String tipoArchivo, String nomenclatura) {
		
		try {
			 
			 beanRemoto.sp_actualizaNom(tipoArchivo,nomenclatura);    
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("PKG_SOLVENCIA_CNSF.actualizaNomSP");
			sb.append("|");
			sb.append("nomenclatura" + "=" + nomenclatura + ",");
			
			
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public int generaInfoRCS(String anio, String mes, String dia, String negocio,String reproceso) {
		int codigoRespuesta = 0;
		try { 
			if ("4".equals(negocio)) { 
				codigoRespuesta = beanRemoto.generaInfoRCSVida(anio, mes, dia,reproceso);
			}
			else{
				codigoRespuesta = beanRemoto.generaInfoRCS(anio, mes, dia, reproceso, negocio);
			}
			return codigoRespuesta;
		} catch (SQLException e) {
			 
			StringBuilder sb = new StringBuilder();
			sb.append("PKG_SOLVENCIA_CNSF.SP_GENERARDATOSSOLVCNSF");
			sb.append("|");
			sb.append("fechaInicio" + "=" + anio + ",");
			LogDeMidasWeb.log(sb.toString(), Level.SEVERE, e);
			return 0;
		} catch (Exception e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			return 0;
		}
	}
	
	public List<SolvenciaLogDTO>  getLogSolvencia(Date fechaCorte, String negocio) {
		try {
			List<String> names;
			if("4".equals(negocio))
			{
				names = Arrays.asList("09", "10");
			} 
			else if ("1".equals(negocio)){
				names = Arrays.asList("01", "02");
			} 
			else{
				names = Arrays.asList("00","99");
			}
			return beanRemoto.findByFechaCorte(fechaCorte,names);   
		} catch (EJBTransactionRolledbackException e) {
			LogDeMidasWeb.log(ERROR, Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Date getFechaUltimoCorteProcesado() {
		Date corte;
		try {
			corte = beanRemoto.getFechaUltimoCorteProcesado(); 
		} catch (Exception e) {
			corte = new Date();
			LogDeMidasWeb.log("Ha ocurrido un error al tratar de obtener la ultima fecha de corte procesada. " + e.getMessage(), Level.WARNING, e);
		}
		return corte;
	}
	
}
