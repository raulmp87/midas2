package mx.com.afirme.midas.danios.reportes.poliza;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.wsCliente.emision.EmisionDN;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfLayer;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public abstract class ReportePolizaBase extends ReporteCotizacionBase{
	protected PolizaDTO polizaDTO;
	protected Boolean contieneRecibo = Boolean.FALSE;
	protected EndosoDTO endosoDTO;
	protected String numeroPoliza;
	public static final String LEYENDA_BASE_LUGAR_Y_FECHA = "En testimonio de lo cual \"La Instituci�n\" firma la presente p�liza en la ciudad de Monterrey, N.L. a ";
	public static final String POLIZA = "P�LIZA";
	public static final String ENDOSO_NO = "Endoso No.";
	protected Short numeroEndoso;
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {

		return obtenerReporte (claveUsuario, true, true);
		
////		Agregar los documentos anexos.
//		List<DocAnexoCotDTO> listaDocumentosAnexos = new ArrayList<DocAnexoCotDTO>();
//		DocAnexoCotDTO docAnexo = new DocAnexoCotDTO();
//		docAnexo.setId(new DocAnexoCotId());
//		docAnexo.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
//		docAnexo.setClaveSeleccion((short)1);
//		listaDocumentosAnexos = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexo);
//		for(DocAnexoCotDTO documento : listaDocumentosAnexos){
//			try{
//			ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getId().getIdToControlArchivo());
//			adjuntarDocumentoAnexo(controlArchivo);
//			}catch(Exception e){}
//		}
////		Agregar los recibos
//		Boolean reciboAlFinal = null;
//		try{
//			reciboAlFinal = adjuntarRecibo(polizaDTO, null, this.numeroEndoso);
//		}catch (SystemException e){
//			//No se pudieron generar los recibos, sin embargo, la poliza s� se puede imprimir
//		}
//		if (reciboAlFinal != null && reciboAlFinal)
//			this.contieneRecibo = reciboAlFinal;
//		if (listaPlantillas == null )
//			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
//		if (listaPlantillas.isEmpty())
//			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
//		
//		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
//		concatenarReportes(outputByteArray);
//		byte byteArrayReporte[] = outputByteArray.toByteArray();
//		ByteArrayOutputStream outputByteArrayReporte = new ByteArrayOutputStream();
//		this.agregarPieDePagina(byteArrayReporte,outputByteArrayReporte,contieneRecibo);
//		return (outputByteArrayReporte.toByteArray());
	}

	public byte[] obtenerReporteEndoso(String claveUsuario) throws SystemException {

//		Agregar los documentos anexos.
		MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
		dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
		dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
		List<MovimientoCotizacionEndosoDTO> movimientos = MovimientoCotizacionEndosoDN.getInstancia(claveUsuario).listarFiltrado(dto);
		for(MovimientoCotizacionEndosoDTO movimiento: movimientos){
			if(movimiento.getIdToControlArchivo().intValue() > 0){
				try{
					ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(movimiento.getIdToControlArchivo());
					if(controlArchivo != null)
						adjuntarDocumentoAnexo(controlArchivo);
				}catch(Exception e){}				
			}
		}
//		Agregar los recibos
		Boolean reciboAlFinal = null;
		try{
			if(endosoDTO != null){
				if(!UtileriasWeb.esCadenaVacia(endosoDTO.getLlaveFiscal())){
					reciboAlFinal = adjuntarRecibo(polizaDTO, this.numeroEndoso);
				}
			}
		}catch (SystemException e){
			//No se pudieron generar los recibos, sin embargo, la poliza s� se puede imprimir
			LogDeMidasWeb.log("No se pudo obtener el recibo, se omite su concetenaci�n y se contin�a con la impresi�n.", Level.SEVERE, e);
		}
		if (reciboAlFinal != null && reciboAlFinal)
			this.contieneRecibo = reciboAlFinal;
		if (listaPlantillas == null )
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		if (listaPlantillas.isEmpty())
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		concatenarReportes(outputByteArray);
		byte byteArrayReporte[] = outputByteArray.toByteArray();
		ByteArrayOutputStream outputByteArrayReporte = new ByteArrayOutputStream();
		this.agregarPieDePagina(byteArrayReporte,outputByteArrayReporte,contieneRecibo);
		return (outputByteArrayReporte.toByteArray());
	}
	
	public byte[] obtenerReporte(String claveUsuario,boolean adjuntarDocumentosAnexos) throws SystemException {
		
		return obtenerReporte (claveUsuario, adjuntarDocumentosAnexos, true);
		
////		Agregar los documentos anexos.
//		if(adjuntarDocumentosAnexos){
//			List<DocAnexoCotDTO> listaDocumentosAnexos = new ArrayList<DocAnexoCotDTO>();
//			DocAnexoCotDTO docAnexo = new DocAnexoCotDTO();
//			docAnexo.setId(new DocAnexoCotId());
//			docAnexo.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
//			docAnexo.setClaveSeleccion((short)1);
//			listaDocumentosAnexos = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexo);
//			for(DocAnexoCotDTO documento : listaDocumentosAnexos){
//				try{
//				ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getId().getIdToControlArchivo());
//				adjuntarDocumentoAnexo(controlArchivo);
//				}catch(Exception e){}
//			}
//		}
////		Agregar los recibos
//		Boolean reciboAlFinal = null;
//		try{
//			reciboAlFinal = adjuntarRecibo(polizaDTO, null, this.numeroEndoso);
//		}catch (SystemException e){
//			//No se pudieron generar los recibos, sin embargo, la poliza s� se puede imprimir
//		}
//		if (reciboAlFinal != null && reciboAlFinal)
//			this.contieneRecibo = reciboAlFinal;
//		if (listaPlantillas == null )
//			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
//		if (listaPlantillas.isEmpty())
//			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
//		
//		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
//		concatenarReportes(outputByteArray);
//		byte byteArrayReporte[] = outputByteArray.toByteArray();
//		ByteArrayOutputStream outputByteArrayReporte = new ByteArrayOutputStream();
//		this.agregarPieDePagina(byteArrayReporte,outputByteArrayReporte,contieneRecibo);
//		return (outputByteArrayReporte.toByteArray());
	}
	
	public byte[] obtenerReporte(String claveUsuario,boolean adjuntarDocumentosAnexos,boolean adjuntarRecibo) throws SystemException {
//		Agregar los documentos anexos.
		if(adjuntarDocumentosAnexos){
			
			//Aqui se agregaran tambien los documentos anexos de reaseguro
			List<DocumentoAnexoReaseguroCotizacionDTO> listaDocumentosAnexosReas = new ArrayList<DocumentoAnexoReaseguroCotizacionDTO>();
			
			listaDocumentosAnexosReas = DocumentoAnexoReaseguroCotizacionDN
				.getInstancia().obtieneAnexosReasCotPorIdCotizacion(cotizacionDTO.getIdToCotizacion());
			
			for(DocumentoAnexoReaseguroCotizacionDTO documento : listaDocumentosAnexosReas){
				try{
				ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getId().getIdToControlArchivo());
				adjuntarDocumentoAnexo(controlArchivo);
				}catch(Exception e){}
			}
			
			List<DocAnexoCotDTO> listaDocumentosAnexos = new ArrayList<DocAnexoCotDTO>();
			DocAnexoCotDTO docAnexo = new DocAnexoCotDTO();
			docAnexo.setId(new DocAnexoCotId());
			docAnexo.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			docAnexo.setClaveSeleccion((short)1);
			listaDocumentosAnexos = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexo);
			for(DocAnexoCotDTO documento : listaDocumentosAnexos){
				try{
				ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getId().getIdToControlArchivo());
				adjuntarDocumentoAnexo(controlArchivo);
				}catch(Exception e){}
			}
			//
		}
//		Agregar los recibos
		Boolean reciboAlFinal = null;
		if(adjuntarRecibo){
			try{
				reciboAlFinal = adjuntarRecibo(polizaDTO, this.numeroEndoso);
			}catch (SystemException e){
				//No se pudieron generar los recibos, sin embargo, la poliza s� se puede imprimir
				generarLogErrorAlGenerarRecibo();
			}
		}
		else
			reciboAlFinal = Boolean.FALSE;
		this.contieneRecibo = (reciboAlFinal != null )? reciboAlFinal : Boolean.FALSE;
		if (listaPlantillas == null )
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		if (listaPlantillas.isEmpty())
			throw new SystemException("Error al generar reporte. No se han obtenido los datos para generar el reporte.",40);
		
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		concatenarReportes(outputByteArray);
		byte byteArrayReporte[] = outputByteArray.toByteArray();
		ByteArrayOutputStream outputByteArrayReporte = new ByteArrayOutputStream();
		this.agregarPieDePagina(byteArrayReporte,outputByteArrayReporte,contieneRecibo);
		return (outputByteArrayReporte.toByteArray());
	}
	
	/**
	 * Genera el Mapa con los par�metros que utilizan todas las plantillas. Los parametros espec�ficos son calculados dentro 
	 * de cada plantilla que los requiere.
	 * @param polizaDTO
	 * @param nombreUsuario
	 * @param listaIncisos
	 * @param Short numeroEndoso.
	 * @throws SystemException 
	 */
	protected void poblarParametrosComunes(PolizaDTO polizaDTO,String nombreUsuario,List<IncisoCotizacionDTO> listaIncisos,Short numeroEndoso) throws SystemException{
		numeroPoliza = "";
		numeroPoliza += UtileriasWeb.llenarIzquierda(polizaDTO.getCodigoProducto(), "0", 2);
		numeroPoliza += UtileriasWeb.llenarIzquierda(polizaDTO.getCodigoTipoPoliza(), "0", 2) + "-";
		numeroPoliza += UtileriasWeb.llenarIzquierda(polizaDTO.getNumeroPoliza().toString(), "0", 6) + "-";
		numeroPoliza += UtileriasWeb.llenarIzquierda(polizaDTO.getNumeroRenovacion().toString(), "0", 2);
		
		getMapaParametrosGeneralesPlantillas().put("NUMERO_POLIZA", numeroPoliza);
		if (numeroEndoso == null){
			numeroEndoso = 0;
			getMapaParametrosGeneralesPlantillas().put("NUMERO_ENDOSO", numeroEndoso.toString());
		}
		else
			getMapaParametrosGeneralesPlantillas().put("NUMERO_ENDOSO", numeroEndoso.toString());
		getMapaParametrosGeneralesPlantillas().put("URL_FIRMA1", Sistema.FIRMA_FUNCIONARIO1);
		getMapaParametrosGeneralesPlantillas().put("URL_FIRMA2", Sistema.FIRMA_FUNCIONARIO2);
		
		if (numeroEndoso != null && numeroEndoso.intValue() == 0)
			getMapaParametrosGeneralesPlantillas().put("LUGAR_Y_FECHA",ReportePolizaBase.LEYENDA_BASE_LUGAR_Y_FECHA + obtenerDescripcionFechaEmision(polizaDTO.getFechaCreacion()));
		else{
			EndosoId endosoId = new EndosoId();
			endosoId.setIdToPoliza(polizaDTO.getIdToPoliza());
			endosoId.setNumeroEndoso(numeroEndoso);
			endosoDTO = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).getPorId(endosoId);
			if(endosoDTO != null && endosoDTO.getFechaCreacion() != null)
				getMapaParametrosGeneralesPlantillas().put("LUGAR_Y_FECHA",ReportePolizaBase.LEYENDA_BASE_LUGAR_Y_FECHA + obtenerDescripcionFechaEmision(endosoDTO.getFechaCreacion()));
			else
				throw new SystemException("No se registr� fecha de creaci�n para el endoso "+numeroEndoso,20);
		}
		
		super.poblarParametrosComunes(nombreUsuario,false);
		if(endosoDTO == null){
			endosoDTO = EndosoDN.getInstancia(nombreUsuario).getPorId(new EndosoId(polizaDTO.getIdToPoliza(),numeroEndoso == null? (short)0:numeroEndoso));
		}
		poblarCuadriculaTotalesEndosoEmitido(endosoDTO, nombreUsuario);
	}

	protected void poblarCuadriculaTotalesEndosoEmitido(EndosoDTO endosoDTO, String nombreUsuario) throws SystemException{
		///////DATOS PARA LA CUADR�CULA DE TOTALES
		////Datos para el primer rengl�n (totales globales)
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");

		FormaPagoIDTO formaPagoIDTO = null;
		try {
			formaPagoIDTO = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).getPorId(Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),Short.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
		} catch (NumberFormatException e) {
		} catch (SystemException e) {}
		
		if(endosoDTO != null){
			if (endosoDTO.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){
				String subTotalPrimaNetaString = fMonto.format(0D);
				getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
				if(formaPagoIDTO != null){
					getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
					getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
				}
				String totalRecargoString = fMonto.format(endosoDTO.getValorRecargoPagoFrac() - endosoDTO.getValorBonifComisionRPF());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);		

				String totalDerechoString = fMonto.format(0D);
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);	

				getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
				
				Double iva = ((endosoDTO.getValorRecargoPagoFrac() - endosoDTO.getValorBonifComisionRPF())* cotizacionDTO.getFactorIVA()/100); 
				String totalIvaString = fMonto.format(iva);
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
				
				String totalPrimaNetaString = fMonto.format(endosoDTO.getValorRecargoPagoFrac() - endosoDTO.getValorBonifComisionRPF() + iva);			
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				
			}else{
				String subTotalPrimaNetaString = fMonto.format(endosoDTO.getValorPrimaNeta() - endosoDTO.getValorBonifComision());
				getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
				if(formaPagoIDTO != null){
					getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
					getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
				}
				String totalRecargoString = fMonto.format(endosoDTO.getValorRecargoPagoFrac() - endosoDTO.getValorBonifComisionRPF());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);		

				String totalDerechoString = fMonto.format(endosoDTO.getValorDerechos());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);	

				getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
				
				String totalIvaString = fMonto.format(endosoDTO.getValorIVA());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
				
				String totalPrimaNetaString = fMonto.format(endosoDTO.getValorPrimaTotal());			
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				
			}
		}
		
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_PRIMA_NETA", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_RECARGO", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_DERECHO", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_IVA", " ");
		getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_TOTAL", " ");
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");	

		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_PRIMA_NETA", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_RECARGO", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_DERECHO", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_IVA", " ");
		getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_TOTAL", " ");
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");			
	}
	
	/**
	 * Calcula la fecha de creaci�n de la p�liza, en formato "dd de MMMMMMM de AAAA"
	 * @return La cadena formada.
	 */
	protected String obtenerDescripcionFechaEmision(Date fechaEmision){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaEmision);
		String []Mes = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String descripcionFecha = "";
		descripcionFecha += calendar.get(Calendar.DATE) + " de ";
		descripcionFecha += Mes[calendar.get(Calendar.MONTH)] + " de ";
		descripcionFecha += (calendar.get(Calendar.YEAR));
		return descripcionFecha;
	}
	
	protected void agregarPieDePagina(byte[] reporte,ByteArrayOutputStream byteOutput,Boolean contieneRecibo){
		try{
            PdfReader reader = new PdfReader(reporte);
            PdfStamper stamper = new PdfStamper(reader, byteOutput);
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
//            Usuario usuarioCot = null;
//            DummyFiller dummy = new DummyFiller();
//            try{
//            	usuarioCot = dummy.obtieneUsuarioPorId(Integer.valueOf(claveUsuario).intValue());
//            }catch(NumberFormatException e){
//            	usuarioCot = dummy.obtieneUsuarioPorNombreUsuario(claveUsuario);
//            }
//            String iniciales = "";
//            if (usuarioCot != null){
//            	try{
//	            	String nombres[] = usuarioCot.getNombre().split(" ");
//	            	for (int j=0;j<nombres.length;j++)
//	            		if (nombres[j] != null & nombres[j].length()>0)
//	            			iniciales += nombres[j].toUpperCase().charAt(0);
//            	}catch(Exception e){}
//            	if (usuarioCot.getApellidoPaterno() != null && usuarioCot.getApellidoPaterno().length()>0)
//            		iniciales += usuarioCot.getApellidoPaterno().toUpperCase().charAt(0);
//            	if (usuarioCot.getApellidoMaterno() != null && usuarioCot.getApellidoMaterno().length()>0)
//            		iniciales += usuarioCot.getApellidoMaterno().toUpperCase().charAt(0);
//            }
            int i=1;
            int limitePaginas;
            if (contieneRecibo)
            	limitePaginas = reader.getNumberOfPages() - 1;
            else
            	limitePaginas = reader.getNumberOfPages();
            while(i <= limitePaginas){
            	PdfContentByte over;
            	over = stamper.getOverContent(i);
                over.saveState();
                over.beginText();
                over.moveText(520, 20);
                over.setFontAndSize(bf, 9);
                over.showText("P�gina "+i+" de "+limitePaginas);
                over.endText();
                
                over.beginText();
                over.moveText(20, 20);
                over.setFontAndSize(bf, 9);
                over.showText(cotizacionDTO.getCodigoUsuarioCotizacion()!=null?cotizacionDTO.getCodigoUsuarioCotizacion():"");
                over.endText();
                i++;
            }
            stamper.close();
        } catch (IOException e) {
                e.printStackTrace();
        } catch (DocumentException e) {
                e.printStackTrace();
        }
	}
	
	public void adjuntarDocumentoAnexo(ControlArchivoDTO archivoAnexo){
		if (archivoAnexo.getNombreArchivoOriginal().toLowerCase().endsWith(".pdf")){
			String uploadFolder;
			if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1)
				uploadFolder = Sistema.UPLOAD_FOLDER;
			else
				uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
			
			String nombreArchivoFisico = ControlArchivoDN.getInstancia().obtenerNombreArchivoFisico(archivoAnexo);
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			if (rotularAnexo(uploadFolder + nombreArchivoFisico,byteArrayOutputStream)){
				byte byteArray[] = byteArrayOutputStream.toByteArray();
				
				if (listaPlantillas == null)
					listaPlantillas = new ArrayList<byte[]>();
				listaPlantillas.add(byteArray);
			}
		}
	}
	
	private boolean rotularAnexo(String archivoOriginal,ByteArrayOutputStream byteOutput){
		try{
            PdfReader reader = new PdfReader(archivoOriginal);
            PdfStamper stamper = new PdfStamper(reader, byteOutput);
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD,BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
            BaseFont bf1 = BaseFont.createFont(BaseFont.HELVETICA,BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
            PdfWriter writer = stamper.getWriter();
            int i=1;
            Rectangle rectangle;
            rectangle = reader.getPageSize(i);
            float height = rectangle.getHeight();
            while(i <= reader.getNumberOfPages()){
            	PdfContentByte over;
            	over = stamper.getOverContent(i);
                over.saveState();
                over.beginText();
                
                over.moveText(340, height-32.25f);
                over.setFontAndSize(bf, 10);
                over.showText(ReportePolizaBase.POLIZA+":");
                over.endText();
                
                over.beginText();
                over.moveText(460, height-32.25f);
                over.setFontAndSize(bf, 10);
                over.showText(mapaParametrosGeneralesPlantillas.get("NUMERO_POLIZA").toString());
                over.endText();
                
                over.beginText();
                over.moveText(340, height-45.25f);
                over.setFontAndSize(bf, 10);
                over.showText(ReportePolizaBase.ENDOSO_NO);
                over.endText();
                
                over.beginText();
                over.moveText(460, height-45.25f);
                over.setFontAndSize(bf, 10);
                over.showText(mapaParametrosGeneralesPlantillas.get("NUMERO_ENDOSO").toString());
                over.endText();
                
                over.beginText();
                over.moveText(340, height-57.25f);
                over.setFontAndSize(bf, 10);
                over.showText("MONEDA");
                over.endText();
                
                over.beginText();
                over.moveText(460, height-57.25f);
                over.setFontAndSize(bf, 10);
                over.showText(mapaParametrosGeneralesPlantillas.get("MONEDA").toString());
                over.endText();
                
                over.moveTo(340, height-63.25f);
    			over.lineTo(560, height-63.25f);
                
    			over.beginText();
                over.moveText(424, height-77.25f);
                over.setFontAndSize(bf, 10);
                over.showText("VIGENCIA");
                over.endText();
                
                over.moveTo(340, height-79.25f);
    			over.lineTo(560, height-79.25f);
    			
    			over.beginText();
                over.moveText(360, height-93.25f);
                over.setFontAndSize(bf, 8);
                over.showText("Desde");
                over.endText();
                
                over.beginText();
                over.moveText(480, height-93.25f);
                over.setFontAndSize(bf, 9);
                over.showText("Hasta");
                over.endText();
                
                over.beginText();
                over.moveText(340, height-106.25f);
                over.setFontAndSize(bf, 9);
                over.showText(mapaParametrosGeneralesPlantillas.get("FECHA_INICIO").toString());
                over.endText();
                
                over.beginText();
                over.moveText(450, height-106.25f);
                over.setFontAndSize(bf, 9);
                over.showText(mapaParametrosGeneralesPlantillas.get("FECHA_FIN").toString());
                over.endText();
                
                over.beginText();
                over.moveText(410, height-106.25f);
                over.setFontAndSize(bf1, 9);
                over.showText("12 Hrs.");
                over.endText();
                
                over.beginText();
                over.moveText(520, height-106.25f);
                over.setFontAndSize(bf1, 9);
                over.showText("12 Hrs.");
                over.endText();
                
                over.roundRectangle(330, height-114.25f, 237, 97,  8);
    			
                /**
                 * 31/12/2009, dado que los documentos anexos s�lo se imprimir�n en la p�liza, no se muestra la leyenda "COTIZACI�N"
                 * Jos� Luis Arellano
                 */
//                over.beginText();
//                over.moveText(180, height-86.25f);
//                over.setFontAndSize(bf, 11);
//                over.showText("COTIZACI�N");
//                over.endText();
                
                over.beginText();
                over.moveText(120, height-106.25f);
                over.setFontAndSize(bf, 11);
                over.showText(mapaParametrosGeneralesPlantillas.get("NOMBRE_PRODUCTO").toString());
                over.endText();
                
                PdfLayer layer = new PdfLayer("Formato impresion", writer);
                
                Image img1 = Image.getInstance(MidasReporteBase.class.getResource(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.logotipoSegurosAfirme")));
                img1.setAbsolutePosition(20, height-66.25f);
                over.beginLayer(layer);
                over.addImage(img1);
                
                over.endLayer();
                over.restoreState();
                over.stroke();
                i++;
            }
            stamper.close();
            return true;
        } catch (IOException e) {
//                e.printStackTrace();
                return false;
        } catch (DocumentException e) {
//                e.printStackTrace();
                return false;
        }
	}
	
	public Boolean adjuntarRecibo(PolizaDTO polizaDTO,Short numeroEndoso) throws SystemException{
		// Cesar Ayma solicita que no se guarden los recibos y tomarlos del servicio
		if (numeroEndoso == null)
			numeroEndoso = new Short((short)0);
		EndosoId endosoId = new EndosoId();
		endosoId.setIdToPoliza(polizaDTO.getIdToPoliza());
		endosoId.setNumeroEndoso(numeroEndoso);
		EndosoDTO endosoDTO = null;

		try {
			
			endosoDTO = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).getPorId(endosoId);
			byte[] recibo = EmisionDN.getInstancia().obtieneFactura(endosoDTO.getLlaveFiscal());
			
			if (recibo != null && recibo.length > 0){
				//Validaci�n del documento
	            try {
	            	PdfReader reader = new PdfReader(recibo);
					PdfStamper stamper = new PdfStamper(reader, new ByteArrayOutputStream());
					stamper.getAcroFields();
					stamper.getMoreInfo();
					stamper.getSignatureAppearance();
					stamper.getWriter();
				} catch (Exception e) {
					LogDeMidasWeb.log("El recibo obtenido no es un documento PDF v�lido, se omite recibo", Level.SEVERE, e);
					return Boolean.FALSE;
				}
				//Si el recibo es un documento v�lido, �ste se agrega a la lista de plantillas para ser concatenado en un s�lo documento
				getListaPlantillas().add(recibo);
				return Boolean.TRUE;
			}
			else
				return Boolean.FALSE;
	} catch (ExcepcionDeAccesoADatos e) {
		
				return Boolean.FALSE;
		}
	
	}
	
	/**
	 * Realiza una b�squeda de los movimientos realizados a una cotizaci�n y calcula la prima neta por el endoso, adem�s de los totales de recargo y 
	 * derecho que se deben cobrar por el endoso. Debe ser invocado despu�s de llamar al m�todo "poblarParametrosComunes".
	 * @param polizaDTO
	 * @param numeroEndoso
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	protected void poblarParametrosCuadriculaTotalesEndosoPoliza(PolizaDTO polizaDTO,Short numeroEndoso, String nombreUsuario) throws SystemException{
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");
		EndosoId id = new EndosoId(polizaDTO.getIdToPoliza(), numeroEndoso);
		EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).getPorId(id);
		String subTotalPrimaNetaString = fMonto.format(endosoDTO.getValorPrimaNeta()- endosoDTO.getValorBonifComision());
		getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
		getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
		//Recargo
		FormaPagoIDTO formaPagoIDTO = null;
		try {
			formaPagoIDTO = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).getPorId(cotizacionDTO.getIdFormaPago().intValue(),cotizacionDTO.getIdMoneda().shortValue());
		} catch (NumberFormatException e) {
		} catch (SystemException e) {}		
		if(formaPagoIDTO != null){
			if (endosoDTO.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO){							
				
				subTotalPrimaNetaString = fMonto.format(0D);
				getMapaParametrosGeneralesPlantillas().remove("SEMI_TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("SEMI_TOTAL_PRIMA_NETA", subTotalPrimaNetaString);
				if(formaPagoIDTO != null){
					getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
					getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
				}
				
				String totalRecargoString = fMonto.format(endosoDTO.getValorRecargoPagoFrac());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);		

				String totalDerechoString = fMonto.format(endosoDTO.getValorDerechos());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);	

				getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getFactorIVA())+"%)");
				
				Double iva = endosoDTO.getValorIVA(); 
				String totalIvaString = fMonto.format(iva);
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);
				
				String totalPrimaNetaString = fMonto.format(endosoDTO.getValorPrimaTotal());			
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);
				
			}else{
				getMapaParametrosGeneralesPlantillas().remove("FORMA_PAGO");
				getMapaParametrosGeneralesPlantillas().put("FORMA_PAGO", formaPagoIDTO.getDescripcion());
				
				String totalRecargoString = fMonto.format(endosoDTO.getValorRecargoPagoFrac() - endosoDTO.getValorBonifComisionRPF());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_RECARGO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_RECARGO", totalRecargoString);

				String totalDerechoString = fMonto.format(endosoDTO.getValorDerechos());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_DERECHO");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_DERECHO", totalDerechoString);	

				getMapaParametrosGeneralesPlantillas().remove("DESCRIPCION_PORCENTAJE_IVA");			
				if(cotizacionDTO.getPorcentajeIva() != null && cotizacionDTO.getPorcentajeIva() > 0D){
					getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", cotizacionDTO.getPorcentajeIva())+"%)");
				}else{
					getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_PORCENTAJE_IVA", "IVA ("+String.format("%.0f", CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), nombreUsuario))+"%)");
				}

				String totalIvaString = fMonto.format(endosoDTO.getValorIVA());
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_IVA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_IVA", totalIvaString);			

				String totalPrimaNetaString = fMonto.format(endosoDTO.getValorPrimaTotal());	
				getMapaParametrosGeneralesPlantillas().remove("TOTAL_PRIMA_NETA");
				getMapaParametrosGeneralesPlantillas().put("TOTAL_PRIMA_NETA", totalPrimaNetaString);	
				
				//Se obtiene el total de meses de la vigencia.
				Calendar calendarInicial = Calendar.getInstance();
				calendarInicial.setTime(cotizacionDTO.getFechaInicioVigencia());
				
				Calendar calendarFinal = Calendar.getInstance();
				calendarFinal.setTime(cotizacionDTO.getFechaFinVigencia());
				
				long millisVigencia = calendarFinal.getTimeInMillis() - calendarInicial.getTimeInMillis();
				Calendar calendarDiferencia = Calendar.getInstance();
				calendarDiferencia.setTimeInMillis(millisVigencia);
				
				int mesesVigencia = calendarDiferencia.get(Calendar.MONTH);
				//Se agregan los meses correspondientes al a�o.
				mesesVigencia += (calendarDiferencia.get(Calendar.YEAR) - 1970)*12;
				//Se agrega un mes extra por los d�as que se pase del mes
				mesesVigencia += 1;
				
				int pagosAnuales = formaPagoIDTO.getNumeroRecibosGenerados();
				double totalPagos = (double)(((double)mesesVigencia ) * ((double)pagosAnuales) / 12f);
				int totalPagosEntero = Double.valueOf(totalPagos).intValue();
				
				if(totalPagos != totalPagosEntero){
					totalPagosEntero = totalPagosEntero+1;
				}
				if (totalPagosEntero == 0d){
					totalPagosEntero = 1;
				}
				@SuppressWarnings("unused")
				BigDecimal sumatoriaPrimaNeta  = new BigDecimal(endosoDTO.getValorPrimaNeta()- endosoDTO.getValorBonifComision() - endosoDTO.getValorBonifComisionRPF());
				@SuppressWarnings("unused")
				BigDecimal totalRecargo = new BigDecimal(endosoDTO.getValorRecargoPagoFrac());
				@SuppressWarnings("unused")
				BigDecimal PORCENTAJE_IVA;
				if(cotizacionDTO.getPorcentajeIva() != null){
					PORCENTAJE_IVA = new BigDecimal(cotizacionDTO.getPorcentajeIva()).divide(new BigDecimal(100d));
				}else{
					PORCENTAJE_IVA = BigDecimal.valueOf(CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), nombreUsuario));
				}
				
				@SuppressWarnings("unused")
				BigDecimal totalIva = new BigDecimal(endosoDTO.getValorIVA());
				@SuppressWarnings("unused")
				BigDecimal totalPrima = new BigDecimal(endosoDTO.getValorPrimaTotal());
				////Datos para el tercer rengl�n (pagos subsecuentes)				
			}
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_PRIMA_NETA", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_RECARGO", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_DERECHO", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_IVA", " ");
			getMapaParametrosGeneralesPlantillas().put("PRIMER_RECIBO_TOTAL", " ");
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");	

			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_PRIMA_NETA", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_RECARGO", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_DERECHO", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_IVA", " ");
			getMapaParametrosGeneralesPlantillas().put("SUBSECUENTES_TOTAL", " ");
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_SUBSECUENTES", " ");						
			
		}//fin validar formaPagoIDTO != null
		else{
			String error = "Error de comunicaci�n con el sistema Seycos, no se pudo recuperar la forma de pago.<br>Cotizacion: "+
				cotizacionDTO.getIdToCotizacion()+" Forma de pago: "+cotizacionDTO.getIdFormaPago()+" , Moneda: "+cotizacionDTO.getIdMoneda();
			LogDeMidasWeb.log(error, Level.SEVERE, null);
			throw new SystemException(error,40);
		}	
		
	}
	protected void generarLogErrorAlGenerarRecibo(){
		try{
			LogDeMidasWeb.log("Ocurri� un error al generar el recibo de la p�lia "+numeroPoliza+"con ID: "+polizaDTO.getIdToPoliza(), Level.SEVERE, null);
		}catch(Exception e){}
	}
	
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}
	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}
	public Boolean getContieneRecibo() {
		return contieneRecibo;
	}
	public void setContieneRecibo(Boolean contieneRecibo) {
		this.contieneRecibo = contieneRecibo;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	
}
