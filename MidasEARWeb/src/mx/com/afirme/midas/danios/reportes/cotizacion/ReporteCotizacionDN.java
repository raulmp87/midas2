package mx.com.afirme.midas.danios.reportes.cotizacion;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;

public class ReporteCotizacionDN {

	private static final ReporteCotizacionDN instancia = new ReporteCotizacionDN();
	
	public static ReporteCotizacionDN getInstancia() {
		return instancia;
	}
	
	private ReporteCotizacionDN(){
	}
	
	public byte[] imprimirCotizacion(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException{
		return new ReporteCotizacionSN().imprimirCotizacion(idToCotizacion, nombreUsuario);
	}
	
	public byte[] imprimirEndoso(BigDecimal idToPoliza,Short numeroEndoso,String nombreUsuario) throws SystemException{
		return new ReporteCotizacionSN().imprimirEndoso(idToPoliza, numeroEndoso, nombreUsuario);
	}
}
