package mx.com.afirme.midas.danios.reportes.poliza.endoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL3_PolizaDetalleInciso;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL4_PolizaDetalleSubincisoPorInciso;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePolizaEndoso extends ReportePolizaBase {
	
	public ReportePolizaEndoso(PolizaDTO polizaDTO,CotizacionDTO cotizacionDTO, Short numeroEndoso) {
		setListaPlantillas(new ArrayList<byte[]>());
		this.polizaDTO = polizaDTO;
		this.idToCotizacion =cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		this.numeroEndoso = numeroEndoso;
	}

	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePolizaEndoso(claveUsuario);
		return super.obtenerReporteEndoso(cotizacionDTO.getCodigoUsuarioEmision());
	}

	private void generarReportePolizaEndoso(String nombreUsuario) throws SystemException {
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		byte[] reporteTMP = null;
		consultarInformacionCotizacion(nombreUsuario);
		
		poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,numeroEndoso);
		consultarMovimientosCotizacion(cotizacionDTO, nombreUsuario);
		poblarParametrosCuadriculaTotalesEndosoPoliza(polizaDTO, numeroEndoso, nombreUsuario);
		PL20PolizaMovimientosEndoso plantillaGeneralMovimientos = new PL20PolizaMovimientosEndoso(cotizacionDTO, getMapaParametrosGeneralesPlantillas(),this);
//		String descripcionTipoMovimiento = "Tipo de endoso: "+UtileriasWeb.getDescripcionCatalogoValorFijo(40, cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue());
		/**
		 * 25/02/2010. Se agreg� la lista de documentos anexos y textos adicionales al apartado de movimientos generales y se eliminaron sus plantillas correspondientes.
		 * Jos� Luis Arellano B�rcenas.
		 */
//		List<String> listaMovimientosExtra = new ArrayList<String>();
//		listaMovimientosExtra.add(descripcionTipoMovimiento);
//		List<DocAnexoCotDTO> listaDocumentos = super.consultarDocumentosAnexos(false);
//		if( listaDocumentos != null)
//			for(DocAnexoCotDTO documento : listaDocumentos)
//				listaMovimientosExtra.add(documento.getDescripcionDocumentoAnexo());
//		List<TexAdicionalCotDTO> listaTextos = super.consultarTextosAdicionales();
//		if(listaTextos != null)
//			for(TexAdicionalCotDTO texto : listaTextos)
//				listaMovimientosExtra.add(texto.getDescripcionTexto());
//		String []movimientosExtra = new String[listaMovimientosExtra.size()];
//		for(int i=0;i<listaMovimientosExtra.size();i++)
//			movimientosExtra[i] = listaMovimientosExtra.get(i);
//		plantillaGeneralMovimientos.setMovimientosGeneralesExtra(movimientosExtra);
		try {
			reporteTMP = plantillaGeneralMovimientos.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP != null) {
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		
		Map<BigDecimal,IncisoCotizacionDTO> mapaIncisosModificados = new HashMap<BigDecimal,IncisoCotizacionDTO>();
		for(MovimientoCotizacionEndosoDTO movimiento : movimientosCotizacion){
			if(movimiento.getNumeroInciso().intValue() != 0){
				if(!mapaIncisosModificados.containsKey(movimiento.getNumeroInciso())){
					for(IncisoCotizacionDTO incisoModificadoTMP : listaIncisos){
						if(incisoModificadoTMP.getId().getNumeroInciso().intValue() == movimiento.getNumeroInciso().intValue()){
							mapaIncisosModificados.put(movimiento.getNumeroInciso(), incisoModificadoTMP);
							break;
						}
					}
				}
			}
		}
		
		IncisoCotizacionDTO[] arrayIncisosModificados = ordenarMapaIncisos(mapaIncisosModificados);
		
		for(int i=0;i<arrayIncisosModificados.length;i++){
			IncisoCotizacionDTO incisoCot = arrayIncisosModificados[i];
			MidasPlantillaBase plantillaDetalleInciso = new PL3_PolizaDetalleInciso(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantillaDetalleInciso.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
			List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
			for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
				MidasPlantillaBase plantilla4 = new PL4_PolizaDetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
				try {
					reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
				} catch (SystemException e) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
		}
		
//		MidasPlantillaBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
//		try {
//			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
//		} catch (SystemException e1) {}
//		if (reporteTMP !=null){
//			getListaPlantillas().add(reporteTMP);
//			reporteTMP = null;
//		}
//		plantillaDocAnexos = null;
//		MidasPlantillaBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
//		try {
//			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
//		} catch (SystemException e1) {}
//		if (reporteTMP !=null){
//			getListaPlantillas().add(reporteTMP);
//			reporteTMP = null;
//		}
		
	}

}
