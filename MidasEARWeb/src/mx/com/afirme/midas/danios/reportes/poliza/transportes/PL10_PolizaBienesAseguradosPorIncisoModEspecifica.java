package mx.com.afirme.midas.danios.reportes.poliza.transportes;

import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.transportes.PlantillaTransportesBienesAseguradosBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL10_PolizaBienesAseguradosPorIncisoModEspecifica extends PlantillaTransportesBienesAseguradosBase{
	
	public PL10_PolizaBienesAseguradosPorIncisoModEspecifica(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.transportes.bienesPorIncisoEspecifica"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.polizaTransportes.paquete"));
	}
	
	public PL10_PolizaBienesAseguradosPorIncisoModEspecifica(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReportePolizaTransportes reporteCotizacionTransportes) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteCotizacionTransportes);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.transportes.bienesPorIncisoEspecifica"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.polizaTransportes.paquete"));
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		super.procesarDatosGenericosPlantilla(claveUsuario);
		super.procesarDatosModalidadEspecifica(claveUsuario);
		return getByteArrayReport();
	}
}
