package mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepml.ReportePMLBase;
import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.sistema.SystemException;

@Deprecated
public class ReportePMLTerremotoErupcion extends ReportePMLBase{
	
	public ReportePMLTerremotoErupcion(Date fechaCorte,Double tipoCambio){
		this.fechaCorte = fechaCorte;
		this.tipoCambio = tipoCambio;
	}

	public byte[] obtenerReporteNuevo(String claveUsuario) throws SystemException {
		List<ReportePMLTEVDTO> listareportePMLTEV = null;
		try{
			listareportePMLTEV = ReportePMLTEVDN.getInstancia().obtieneReportePMLTEV(fechaCorte, tipoCambio, claveUsuario);
		}catch(SystemException e1){
			throw new SystemException("Ocurri� un error al recuperar la informaci�n para el reporte: "+e1.getCause());
		}
		if(listareportePMLTEV == null)
			throw new SystemException ("Ocurri� un error al recuperar la informaci�n para el reporte. Lista null.");
		if(listareportePMLTEV.isEmpty())
			throw new SystemException ("No se encontraron registros para los datos introducidos.");
		PL19PlantillaPMLTerremoto plantilla = new PL19PlantillaPMLTerremoto(listareportePMLTEV);
		byte byteArray[] = null;
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		plantilla.setListaRegistrosContenido(null);
		plantilla = null;
		Runtime.getRuntime().gc();
		return byteArray;
	}
	
	@Deprecated
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		List<ReportePMLTEVDTO> listareportePMLTEV = null;
		try{
			listareportePMLTEV = ReportePMLTEVDN.getInstancia().obtieneReportePMLTEV(fechaCorte, tipoCambio, claveUsuario);
		}catch(SystemException e1){
			throw new SystemException("Ocurri� un error al recuperar la informaci�n para el reporte: "+e1.getCause());
		}
		if(listareportePMLTEV == null)
			throw new SystemException ("Ocurri� un error al recuperar la informaci�n para el reporte. Lista null.");
		if(listareportePMLTEV.isEmpty())
			throw new SystemException ("No se encontraron registros para los datos introducidos.");
		PL19PlantillaPMLTerremoto plantilla = new PL19PlantillaPMLTerremoto(listareportePMLTEV);
		byte byteArray[] = null;
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		plantilla.setListaRegistrosContenido(null);
		plantilla = null;
		Runtime.getRuntime().gc();
		return byteArray;
	}

	@Override
	public byte[] obtenerReportePML(TipoReportePML tipoReportePML) {
		// TODO Auto-generated method stub
		return null;
	}
}
