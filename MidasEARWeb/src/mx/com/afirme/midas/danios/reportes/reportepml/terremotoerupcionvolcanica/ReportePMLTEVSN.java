package mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReportePMLTEVSN {

	private ReportePMLTEVFacadeRemote beanRemoto;
	
	public ReportePMLTEVSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReportePMLTEVFacadeRemote.class);
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al buscar EJB.", Level.SEVERE, e);
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	@Deprecated
	public List<ReportePMLTEVDTO> obtieneReportePMLTEV(
			ReportePMLTEVDTO filtroReporte, String nombreUsuario) {
		try {
			return beanRemoto.obtieneReportePMLTEV(filtroReporte, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Reportes.spDAN_RepPMLTEV");
			sb.append("|");
			sb.append("pFechaCorte" + "=" + filtroReporte.getFechaCorte() + ",");
			sb.append("pTipoCambio" + "=" + filtroReporte.getTipoCambio() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Obtener reporte PML TEV" + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean calcularReportePMLTEV(ReportePMLTEVDTO filtroReporte, String nombreUsuario){
		return beanRemoto.calcularReportePMLTEV(filtroReporte, nombreUsuario);
	}

	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVIndependientesCreditosHipotecarios(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVIndependientesGrandesRiesgos(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVIndependientesRiesgosNormales(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVSemiAgrupadoCreditosHipotecarios(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVSemiAgrupadoGrandesRiesgos(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVSemiAgrupadoRiesgosNormales(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return beanRemoto.obtenerReportePMLTEVTecnicos(claveTipoReporte,nombreUsuario);
	}
}
