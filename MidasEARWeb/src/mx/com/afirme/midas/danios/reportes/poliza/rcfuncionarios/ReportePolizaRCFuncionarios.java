package mx.com.afirme.midas.danios.reportes.poliza.rcfuncionarios;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL14_PolizaTextosAdicionales;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL2_PolizaDocumentosAdicionales;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePolizaRCFuncionarios extends ReportePolizaBase{

	public ReportePolizaRCFuncionarios(PolizaDTO polizaDTO,CotizacionDTO cotizacionDTO,Short numeroEndoso) {
		this.polizaDTO = polizaDTO;
		if(cotizacionDTO != null)
			this.cotizacionDTO = cotizacionDTO;
		else
			this.cotizacionDTO = polizaDTO.getCotizacionDTO();
		this.numeroEndoso = numeroEndoso;
		mapaSeccionesContratadasPorNumeroInciso = null;
		setListaPlantillas(new ArrayList<byte[]>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePolizaRCFuncionarios(polizaDTO, claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReportePolizaRCFuncionarios(PolizaDTO polizaDTO, String nombreUsuario){
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			idToCotizacion = cotizacionDTO.getIdToCotizacion();
			listaCoberturasContratadasCotizacion = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadasParaReporte(idToCotizacion);
			listaSubIncisosCotizacion = SubIncisoCotizacionDN.getInstancia().listarSubIncisosPorCotizacion(idToCotizacion);
			listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
			
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,this.numeroEndoso);
			String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
			abreviaturas = "S.E.A.";
			descripcionAbreviaturas = "Seg�n especificaci�n adjunta.";
			leyendaAbreviaturas = "Donde quiera que aparezca en este contrato las siguientes abreviaturas se entender�:";
			getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
			getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
			
			byte[] reporteTMP=null;

			for(int i=0; i<listaIncisos.size(); i++){
				IncisoCotizacionDTO incisoTMP = listaIncisos.get(i);
				MidasPlantillaBase plantillaLicitacionPorInciso = new PL21_PolizaLicitacionRCFuncionarios(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
				try {
					reporteTMP = plantillaLicitacionPorInciso.obtenerReporte(nombreUsuario);
				} catch (SystemException e1) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
			
			MidasPlantillaBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
			} catch (SystemException e) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			MidasPlantillaBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
			} catch (SystemException e) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
		} catch (SystemException e) {}
	}
}
