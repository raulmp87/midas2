package mx.com.afirme.midas.danios.reportes.poliza.generica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL14_PolizaTextosAdicionales;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL2_PolizaDocumentosAdicionales;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePolizaGenerica extends ReportePolizaBase{
	private BigDecimal idTcRamo;
	private BigDecimal idTcSubRamo;
	
	public ReportePolizaGenerica(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
		this.cotizacionDTO = polizaDTO.getCotizacionDTO();
		mapaSeccionesContratadasPorNumeroInciso = null;
		setListaPlantillas(new ArrayList<byte[]>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePolizaGenerica(polizaDTO, claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReportePolizaGenerica(PolizaDTO polizaDTO, String nombreUsuario){
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			CotizacionDTO cotizacionDTO = polizaDTO.getCotizacionDTO();
			idToCotizacion = cotizacionDTO.getIdToCotizacion();
			listaSeccionesContratadasCotizacion = SeccionCotizacionDN.getInstancia().listarSeccionesContratadasPorCotizacion(cotizacionDTO.getIdToCotizacion(),(short)1);
			listaCoberturasContratadasCotizacion = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadasParaReporte(idToCotizacion);
			listaRiesgosContratadosCotizacion = RiesgoCotizacionDN.getInstancia().listarRiesgosContratadosPorCotizacion(idToCotizacion);
			listaSubIncisosCotizacion = SubIncisoCotizacionDN.getInstancia().listarSubIncisosPorCotizacion(idToCotizacion);
			listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
			
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,null);
			String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
			abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\n";
			descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\n" +
					"Con m�ximo de:\nD�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\n" +
					"La suma asegurada para esta cobertura es el valor establecido en la cobertura b�sica especificado para la cobertura b�sica, salvo los subl�mites establecidos";
			leyendaAbreviaturas = "Donde quiera que aparezcan  las siguientes abreviaturas se entender�:";
			getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
			getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
			getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
			
			byte[] reporteTMP=null;

			for(int i=0; i<listaIncisos.size(); i++){
				IncisoCotizacionDTO incisoTMP = listaIncisos.get(i);
				MidasPlantillaBase plantillaBienesAseguradosPorInciso = new PL7_PolizaBienesAseguradosPorInciso(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this,i==0);
				try {
					reporteTMP = plantillaBienesAseguradosPorInciso.obtenerReporte(nombreUsuario);
				} catch (SystemException e1) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
				
				List<SubIncisoCotizacionDTO> listaSubIncisos = null;
				listaSubIncisos = obtenerSubIncisosPorInciso(incisoTMP.getId().getNumeroInciso());
				if(listaSubIncisos != null && listaSubIncisos.size() > 1){
					MidasPlantillaBase plantillaSubIncisosPorInciso = new PL8_PolizaSubIncisosPorInciso(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
					try {
						reporteTMP = plantillaSubIncisosPorInciso.obtenerReporte(nombreUsuario);
					} catch (SystemException e1) {}
					if (reporteTMP !=null){
						getListaPlantillas().add(reporteTMP);
						reporteTMP = null;
					}
				}
			}
			
			MidasPlantillaBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
			} catch (SystemException e) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			MidasPlantillaBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
			try {
				reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
			} catch (SystemException e) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
		} catch (SystemException e) {}
	}
	
	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
}
