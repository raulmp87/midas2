package mx.com.afirme.midas.danios.reportes.poliza.endoso.cancelacion;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.endoso.PL20PolizaMovimientosEndoso;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReportePolizaEndosoCancelacion extends ReportePolizaBase {
	
	
	public ReportePolizaEndosoCancelacion(PolizaDTO polizaDTO,CotizacionDTO cotizacionDTO, Short numeroEndoso) {
		setListaPlantillas(new ArrayList<byte[]>());
		this.polizaDTO = polizaDTO;
		this.idToCotizacion =cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		this.numeroEndoso = numeroEndoso;
	}

	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePolizaEndosoCancelacion(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioEmision(),false,true);
	}

	private void generarReportePolizaEndosoCancelacion(String nombreUsuario) throws SystemException {
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		byte[] reporteTMP = null;
//		consultarInformacionCotizacion(nombreUsuario);
		
		poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,numeroEndoso);
		consultarMovimientosCotizacion(cotizacionDTO, nombreUsuario);
		poblarParametrosCuadriculaTotalesEndosoPoliza(polizaDTO, numeroEndoso, nombreUsuario);
		PL20PolizaMovimientosEndoso plantillaGeneralMovimientos = new PL20PolizaMovimientosEndoso(cotizacionDTO, getMapaParametrosGeneralesPlantillas(),this);
		String []movimientosExtra = new String[1];
		movimientosExtra[0] = "Tipo de endoso: "+UtileriasWeb.getDescripcionCatalogoValorFijo(40, cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue());
		if(cotizacionDTO.getClaveMotivoEndoso() != null){
			if(cotizacionDTO.getClaveMotivoEndoso().intValue() >= 1 && cotizacionDTO.getClaveMotivoEndoso().intValue() <= 7){
				String descMotivo = UtileriasWeb.getDescripcionCatalogoValorFijo(42, cotizacionDTO.getClaveMotivoEndoso().intValue());
				movimientosExtra[0] += (UtileriasWeb.esCadenaVacia(descMotivo))?"":" , Motivo: " + descMotivo;
			}
		}
		plantillaGeneralMovimientos.setMovimientosGeneralesExtra(movimientosExtra);
		reporteTMP = plantillaGeneralMovimientos.obtenerReporte(nombreUsuario);
		if (reporteTMP != null) {
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
	}
}
