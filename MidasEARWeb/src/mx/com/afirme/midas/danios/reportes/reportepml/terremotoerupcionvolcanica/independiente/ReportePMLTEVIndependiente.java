package mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.independiente;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepml.AtributoEntradaDTO_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.DocumentoPMLBase;
import mx.com.afirme.midas.danios.reportes.reportepml.ParametroSalidaSP_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.ReportePMLTEVDN;
import mx.com.afirme.midas.sistema.creadorxls.MidasHojaXLS;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;

/**
 * @author Jos� Luis Arellano
 */
public class ReportePMLTEVIndependiente extends DocumentoPMLBase{

	public ReportePMLTEVIndependiente(Integer claveTipoReporte){
		super(claveTipoReporte);
		this.nombreReporte = "TEV_Independientes"+obtenerSufijoReporte();
	}
	
	@SuppressWarnings("unchecked")
	public byte[] obtenerReporte(String nombreUsuario) throws Exception{
		MidasHojaXLS hojaPMLTEVCH = generarHojaCreditosHipotecarios();
		MidasHojaXLS hojaPMLTEVGR = generarHojaGrandesRiesgos();
		MidasHojaXLS hojaPMLTEVRN = generarHojaRiesgosNormales();
		
		hojaPMLTEVCH.iniciarProcesamientoArchivoXLS();
		List listaRegistros = ReportePMLTEVDN.getInstancia().obtenerReportePMLTEVIndependientesCreditosHipotecarios(claveTipoReporte,nombreUsuario);
		establecerNumeroRegistro(listaRegistros,false);
		hojaPMLTEVCH.insertarFilasArchivoXLS(listaRegistros);
		
		hojaPMLTEVGR.iniciarProcesamientoArchivoXLS();
		listaRegistros = ReportePMLTEVDN.getInstancia().obtenerReportePMLTEVIndependientesGrandesRiesgos(claveTipoReporte,nombreUsuario);
		establecerNumeroRegistro(listaRegistros,false);
		hojaPMLTEVGR.insertarFilasArchivoXLS(listaRegistros);
		
		hojaPMLTEVRN.iniciarProcesamientoArchivoXLS();
		listaRegistros = ReportePMLTEVDN.getInstancia().obtenerReportePMLTEVIndependientesRiesgosNormales(claveTipoReporte,nombreUsuario);
		establecerNumeroRegistro(listaRegistros,false);
		hojaPMLTEVRN.insertarFilasArchivoXLS(listaRegistros);
		
		return finalizarProcesamientoArchivoXLS();
	}
	
	private MidasHojaXLS generarHojaCreditosHipotecarios(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_REGISTRO
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
				,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.INM_DEDUCIBLE
				,AtributoEntradaDTO_PML.INM_COASEGURO
				,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONT_COASEGURO
				,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONSEC_COASEGURO
				,AtributoEntradaDTO_PML.CLAVE_ESTADO
				,AtributoEntradaDTO_PML.ZONA_SISMICA
				,AtributoEntradaDTO_PML.NUM_PISOS
				,AtributoEntradaDTO_PML.ES_INDUSTRIAL
				,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
				,AtributoEntradaDTO_PML.CODIGO_POSTAL
				,AtributoEntradaDTO_PML.LONGITUD
				,AtributoEntradaDTO_PML.LATITUD
				,AtributoEntradaDTO_PML.EDI_SUELO
				,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
				,AtributoEntradaDTO_PML.EDI_USO
				,AtributoEntradaDTO_PML.EST_COLUMNAS
				,AtributoEntradaDTO_PML.EST_TRABES
				,AtributoEntradaDTO_PML.EST_MUROS
				,AtributoEntradaDTO_PML.EST_CUBIERTA
				,AtributoEntradaDTO_PML.EST_CLAROS
				,AtributoEntradaDTO_PML.EST_MUROS_PRE
				,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
				,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
				,AtributoEntradaDTO_PML.OTR_SOBREPESO
				,AtributoEntradaDTO_PML.OTR_GOLPETEO
				,AtributoEntradaDTO_PML.OTR_ESQUINA
				,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
				,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
				,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
				,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
				,AtributoEntradaDTO_PML.OTR_DA_REPARADO
				,AtributoEntradaDTO_PML.OTR_REFORZADA
				,AtributoEntradaDTO_PML.OTR_FECHA
				,AtributoEntradaDTO_PML.PRIMA
				,AtributoEntradaDTO_PML.CEDIDA
				,AtributoEntradaDTO_PML.RETENIDA
				,AtributoEntradaDTO_PML.MONEDA
				,AtributoEntradaDTO_PML.RSR_T
				,AtributoEntradaDTO_PML.OFI_EMI
				,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.VALOR_RETENIDO
				,AtributoEntradaDTO_PML.INCISO
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_REGISTRO
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
				,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.INM_DEDUCIBLE
				,ParametroSalidaSP_PML.INM_COASEGURO
				,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONT_DEDUCIBLE
				,ParametroSalidaSP_PML.CONT_COASEGURO
				,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
				,ParametroSalidaSP_PML.CONSEC_COASEGURO
				,ParametroSalidaSP_PML.CLAVE_ESTADO
				,ParametroSalidaSP_PML.ZONA_SISMICA
				,ParametroSalidaSP_PML.NUM_PISOS
				,ParametroSalidaSP_PML.ES_INDUSTRIAL
				,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
				,ParametroSalidaSP_PML.CODIGO_POSTAL
				,ParametroSalidaSP_PML.LONGITUD
				,ParametroSalidaSP_PML.LATITUD
				,ParametroSalidaSP_PML.EDI_SUELO
				,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
				,ParametroSalidaSP_PML.EDI_USO
				,ParametroSalidaSP_PML.EST_COLUMNAS
				,ParametroSalidaSP_PML.EST_TRABES
				,ParametroSalidaSP_PML.EST_MUROS
				,ParametroSalidaSP_PML.EST_CUBIERTA
				,ParametroSalidaSP_PML.EST_CLAROS
				,ParametroSalidaSP_PML.EST_MUROS_PRE
				,ParametroSalidaSP_PML.EST_CONTRAVENTEO
				,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
				,ParametroSalidaSP_PML.OTR_SOBREPESO
				,ParametroSalidaSP_PML.OTR_GOLPETEO
				,ParametroSalidaSP_PML.OTR_ESQUINA
				,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
				,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
				,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
				,ParametroSalidaSP_PML.OTR_DA_PREVIOS
				,ParametroSalidaSP_PML.OTR_DA_REPARADO
				,ParametroSalidaSP_PML.OTR_REFORZADA
				,ParametroSalidaSP_PML.OTR_FECHA
				,ParametroSalidaSP_PML.PRIMA
				,ParametroSalidaSP_PML.CEDIDA
				,ParametroSalidaSP_PML.RETENIDA
				,ParametroSalidaSP_PML.MONEDA
				,ParametroSalidaSP_PML.RSR_T
				,ParametroSalidaSP_PML.OFI_EMI
				,ParametroSalidaSP_PML.VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.VALOR_RETENIDO
				,ParametroSalidaSP_PML.INCISO
		};
		
		MidasHojaXLS hojaPMLHidro = new MidasHojaXLS("CreditosHipotecarios",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidro;
	}
	
	private MidasHojaXLS generarHojaGrandesRiesgos(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_REGISTRO
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
				,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.INM_DEDUCIBLE
				,AtributoEntradaDTO_PML.INM_COASEGURO
				,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONT_COASEGURO
				,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONSEC_COASEGURO
				,AtributoEntradaDTO_PML.CLAVE_ESTADO
				,AtributoEntradaDTO_PML.ZONA_SISMICA
				,AtributoEntradaDTO_PML.NUM_PISOS
				,AtributoEntradaDTO_PML.ES_INDUSTRIAL
				,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
				,AtributoEntradaDTO_PML.CODIGO_POSTAL
				,AtributoEntradaDTO_PML.LONGITUD
				,AtributoEntradaDTO_PML.LATITUD
				,AtributoEntradaDTO_PML.EDI_SUELO
				,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
				,AtributoEntradaDTO_PML.EDI_USO
				,AtributoEntradaDTO_PML.EST_COLUMNAS
				,AtributoEntradaDTO_PML.EST_TRABES
				,AtributoEntradaDTO_PML.EST_MUROS
				,AtributoEntradaDTO_PML.EST_CUBIERTA
				,AtributoEntradaDTO_PML.EST_CLAROS
				,AtributoEntradaDTO_PML.EST_MUROS_PRE
				,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
				,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
				,AtributoEntradaDTO_PML.OTR_SOBREPESO
				,AtributoEntradaDTO_PML.OTR_GOLPETEO
				,AtributoEntradaDTO_PML.OTR_ESQUINA
				,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
				,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
				,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
				,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
				,AtributoEntradaDTO_PML.OTR_DA_REPARADO
				,AtributoEntradaDTO_PML.OTR_REFORZADA
				,AtributoEntradaDTO_PML.OTR_FECHA
				,AtributoEntradaDTO_PML.PRIMA
				,AtributoEntradaDTO_PML.CEDIDA
				,AtributoEntradaDTO_PML.RETENIDA
				,AtributoEntradaDTO_PML.MONEDA
				,AtributoEntradaDTO_PML.RSR_T
				,AtributoEntradaDTO_PML.OFI_EMI
				,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.VALOR_RETENIDO
				,AtributoEntradaDTO_PML.INCISO
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_REGISTRO
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
				,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.INM_DEDUCIBLE
				,ParametroSalidaSP_PML.INM_COASEGURO
				,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONT_DEDUCIBLE
				,ParametroSalidaSP_PML.CONT_COASEGURO
				,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
				,ParametroSalidaSP_PML.CONSEC_COASEGURO
				,ParametroSalidaSP_PML.CLAVE_ESTADO
				,ParametroSalidaSP_PML.ZONA_SISMICA
				,ParametroSalidaSP_PML.NUM_PISOS
				,ParametroSalidaSP_PML.ES_INDUSTRIAL
				,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
				,ParametroSalidaSP_PML.CODIGO_POSTAL
				,ParametroSalidaSP_PML.LONGITUD
				,ParametroSalidaSP_PML.LATITUD
				,ParametroSalidaSP_PML.EDI_SUELO
				,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
				,ParametroSalidaSP_PML.EDI_USO
				,ParametroSalidaSP_PML.EST_COLUMNAS
				,ParametroSalidaSP_PML.EST_TRABES
				,ParametroSalidaSP_PML.EST_MUROS
				,ParametroSalidaSP_PML.EST_CUBIERTA
				,ParametroSalidaSP_PML.EST_CLAROS
				,ParametroSalidaSP_PML.EST_MUROS_PRE
				,ParametroSalidaSP_PML.EST_CONTRAVENTEO
				,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
				,ParametroSalidaSP_PML.OTR_SOBREPESO
				,ParametroSalidaSP_PML.OTR_GOLPETEO
				,ParametroSalidaSP_PML.OTR_ESQUINA
				,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
				,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
				,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
				,ParametroSalidaSP_PML.OTR_DA_PREVIOS
				,ParametroSalidaSP_PML.OTR_DA_REPARADO
				,ParametroSalidaSP_PML.OTR_REFORZADA
				,ParametroSalidaSP_PML.OTR_FECHA
				,ParametroSalidaSP_PML.PRIMA
				,ParametroSalidaSP_PML.CEDIDA
				,ParametroSalidaSP_PML.RETENIDA
				,ParametroSalidaSP_PML.MONEDA
				,ParametroSalidaSP_PML.RSR_T
				,ParametroSalidaSP_PML.OFI_EMI
				,ParametroSalidaSP_PML.VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.VALOR_RETENIDO
				,ParametroSalidaSP_PML.INCISO
		};
		
		MidasHojaXLS hojaPMLHidro = new MidasHojaXLS("GrandesRiesgos",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidro;
	}
	
	private MidasHojaXLS generarHojaRiesgosNormales(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_REGISTRO
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
				,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.INM_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.INM_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.INM_DEDUCIBLE
				,AtributoEntradaDTO_PML.INM_COASEGURO
				,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONT_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.CONT_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONT_COASEGURO
				,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONSEC_PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONSEC_COASEGURO
				,AtributoEntradaDTO_PML.CLAVE_ESTADO
				,AtributoEntradaDTO_PML.ZONA_SISMICA
				,AtributoEntradaDTO_PML.NUM_PISOS
				,AtributoEntradaDTO_PML.ES_INDUSTRIAL
				,AtributoEntradaDTO_PML.CLAVE_MUNICIPIO
				,AtributoEntradaDTO_PML.CODIGO_POSTAL
				,AtributoEntradaDTO_PML.LONGITUD
				,AtributoEntradaDTO_PML.LATITUD
				,AtributoEntradaDTO_PML.EDI_SUELO
				,AtributoEntradaDTO_PML.EDI_FECHA_CONSTRUCCION
				,AtributoEntradaDTO_PML.EDI_USO
				,AtributoEntradaDTO_PML.EST_COLUMNAS
				,AtributoEntradaDTO_PML.EST_TRABES
				,AtributoEntradaDTO_PML.EST_MUROS
				,AtributoEntradaDTO_PML.EST_CUBIERTA
				,AtributoEntradaDTO_PML.EST_CLAROS
				,AtributoEntradaDTO_PML.EST_MUROS_PRE
				,AtributoEntradaDTO_PML.EST_CONTRAVENTEO
				,AtributoEntradaDTO_PML.OTR_COLUMNAS_CORTAS
				,AtributoEntradaDTO_PML.OTR_SOBREPESO
				,AtributoEntradaDTO_PML.OTR_GOLPETEO
				,AtributoEntradaDTO_PML.OTR_ESQUINA
				,AtributoEntradaDTO_PML.OTR_IRRE_ELEVACION
				,AtributoEntradaDTO_PML.OTR_IRRE_PLANTA
				,AtributoEntradaDTO_PML.OTR_HUNDIMIENTOS
				,AtributoEntradaDTO_PML.OTR_DA_PREVIOS
				,AtributoEntradaDTO_PML.OTR_DA_REPARADO
				,AtributoEntradaDTO_PML.OTR_REFORZADA
				,AtributoEntradaDTO_PML.OTR_FECHA
				,AtributoEntradaDTO_PML.PRIMA
				,AtributoEntradaDTO_PML.CEDIDA
				,AtributoEntradaDTO_PML.RETENIDA
				,AtributoEntradaDTO_PML.MONEDA
				,AtributoEntradaDTO_PML.RSR_T
				,AtributoEntradaDTO_PML.OFI_EMI
				,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.VALOR_RETENIDO
				,AtributoEntradaDTO_PML.INCISO
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_REGISTRO
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
				,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.INM_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.INM_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.INM_DEDUCIBLE
				,ParametroSalidaSP_PML.INM_COASEGURO
				,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONT_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.CONT_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONT_DEDUCIBLE
				,ParametroSalidaSP_PML.CONT_COASEGURO
				,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONSEC_PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
				,ParametroSalidaSP_PML.CONSEC_COASEGURO
				,ParametroSalidaSP_PML.CLAVE_ESTADO
				,ParametroSalidaSP_PML.ZONA_SISMICA
				,ParametroSalidaSP_PML.NUM_PISOS
				,ParametroSalidaSP_PML.ES_INDUSTRIAL
				,ParametroSalidaSP_PML.CLAVE_MUNICIPIO
				,ParametroSalidaSP_PML.CODIGO_POSTAL
				,ParametroSalidaSP_PML.LONGITUD
				,ParametroSalidaSP_PML.LATITUD
				,ParametroSalidaSP_PML.EDI_SUELO
				,ParametroSalidaSP_PML.EDI_FECHA_CONSTRUCCION
				,ParametroSalidaSP_PML.EDI_USO
				,ParametroSalidaSP_PML.EST_COLUMNAS
				,ParametroSalidaSP_PML.EST_TRABES
				,ParametroSalidaSP_PML.EST_MUROS
				,ParametroSalidaSP_PML.EST_CUBIERTA
				,ParametroSalidaSP_PML.EST_CLAROS
				,ParametroSalidaSP_PML.EST_MUROS_PRE
				,ParametroSalidaSP_PML.EST_CONTRAVENTEO
				,ParametroSalidaSP_PML.OTR_COLUMNAS_CORTAS
				,ParametroSalidaSP_PML.OTR_SOBREPESO
				,ParametroSalidaSP_PML.OTR_GOLPETEO
				,ParametroSalidaSP_PML.OTR_ESQUINA
				,ParametroSalidaSP_PML.OTR_IRRE_ELEVACION
				,ParametroSalidaSP_PML.OTR_IRRE_PLANTA
				,ParametroSalidaSP_PML.OTR_HUNDIMIENTOS
				,ParametroSalidaSP_PML.OTR_DA_PREVIOS
				,ParametroSalidaSP_PML.OTR_DA_REPARADO
				,ParametroSalidaSP_PML.OTR_REFORZADA
				,ParametroSalidaSP_PML.OTR_FECHA
				,ParametroSalidaSP_PML.PRIMA
				,ParametroSalidaSP_PML.CEDIDA
				,ParametroSalidaSP_PML.RETENIDA
				,ParametroSalidaSP_PML.MONEDA
				,ParametroSalidaSP_PML.RSR_T
				,ParametroSalidaSP_PML.OFI_EMI
				,ParametroSalidaSP_PML.VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.VALOR_RETENIDO
				,ParametroSalidaSP_PML.INCISO
		};
		
		MidasHojaXLS hojaPMLHidro = new MidasHojaXLS("RiesgosNormales",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidro;
	}
}
