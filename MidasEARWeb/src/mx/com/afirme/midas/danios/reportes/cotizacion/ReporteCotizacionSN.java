package mx.com.afirme.midas.danios.reportes.cotizacion;

import java.math.BigDecimal;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;


class ReporteCotizacionSN {
	private ReporteCotizacionServiciosRemote beanRemoto;
	
	ReporteCotizacionSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		try{
		beanRemoto = serviceLocator.getEJB(ReporteCotizacionServiciosRemote.class);
		}
		catch(SystemException e){
			LogDeMidasWeb.log("Error al buscar bean remoto: "+ReporteCotizacionServiciosRemote.class, Level.SEVERE, e);
			throw e;
		}
	}
	
	byte[] imprimirCotizacion(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException{
		return beanRemoto.imprimirCotizacion(idToCotizacion, nombreUsuario);
	}
	
	byte[] imprimirEndoso(BigDecimal idToPoliza,Short numeroEndoso,String nombreUsuario) throws SystemException{
		return beanRemoto.imprimirEndoso(idToPoliza, numeroEndoso, nombreUsuario);
	}
}
