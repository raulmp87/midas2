package mx.com.afirme.midas.danios.reportes.poliza.transportes;

import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public class PL11_PolizaDeduciblesCoasegurosPorInciso extends PlantillaCotizacionBase{
	
	public PL11_PolizaDeduciblesCoasegurosPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.transportes.deduciblesCoasegurosPorInciso"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.polizaTransportes.paquete"));
	}
	
	public PL11_PolizaDeduciblesCoasegurosPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReportePolizaTransportes reporteCotizacionTransportes) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteCotizacionTransportes);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.transportes.deduciblesCoasegurosPorInciso"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.polizaTransportes.paquete"));
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		procesarDatosReporte(claveUsuario);
		return getByteArrayReport();
	}

	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			super.poblarParametroNumeroInciso();
			
			super.poblarSeccionesPorInciso();

			if (getListaRegistrosContenido() == null || getListaRegistrosContenido().isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}

			String nombrePlantillaSubReporte;
			
			Map<String,Object> parametrosReporteCoberturas = new HashMap<String,Object>();
			nombrePlantillaSubReporte = getPaquetePlantilla()+ 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.transportes.deduciblesCoasegurosPorInciso.subReporteCoberturas");
			JasperReport subReporteCoberturas = getJasperReport(nombrePlantillaSubReporte);
			if(subReporteCoberturas == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte, null);
			getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
			
			nombrePlantillaSubReporte = getPaquetePlantilla() + 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.transportes.deduciblesCoasegurosPorInciso.subReporteRiesgos");
		    JasperReport subReporteRiesgos = getJasperReport(nombrePlantillaSubReporte);
		    if(subReporteRiesgos == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte, null);
			
		    parametrosReporteCoberturas.put("SUBREPORTE_RIESGOS", subReporteRiesgos);
			getParametrosVariablesReporte().put("PARAMETROS_SUBREPORTE_COBERTURA",parametrosReporteCoberturas);
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}//Fin validar cotizacion != null && incisoCot != null
		else	setByteArrayReport( null);
	}
}
