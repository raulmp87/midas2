package mx.com.afirme.midas.danios.reportes.endosocancelable;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteEndososCancelables extends MidasReporteBase{

	private Date fechaCorte;

	public ReporteEndososCancelables(Date fechaCorte) {
		super();
		this.fechaCorte = fechaCorte;
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		byte byteArray[] = null;
		
		if(fechaCorte == null)
			fechaCorte = new Date();
		
		List<EndosoIDTO> listaEndosoIDTO = EndosoIDN.getInstancia().obtieneListaCancelablesReporte(fechaCorte, claveUsuario);
		
		PlantillaEndososCancelables plantilla = new PlantillaEndososCancelables(fechaCorte);
		plantilla.setListaCancelables(listaEndosoIDTO);
		byteArray = plantilla.obtenerReporte(claveUsuario);
		
		return byteArray;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
}
