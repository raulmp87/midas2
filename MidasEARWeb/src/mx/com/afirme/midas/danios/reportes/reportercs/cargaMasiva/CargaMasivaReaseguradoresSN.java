package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCargaCNSF;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class CargaMasivaReaseguradoresSN {
	
	private CargaMasivaReaseguradoresFacadeRemote beanRemoto;

	public CargaMasivaReaseguradoresSN() throws SystemException {
		try { 
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CargaMasivaReaseguradoresFacadeRemote.class);
		} catch (Exception e) {
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(
			List<ReaseguradorCargaCNSF> direccionesValidas) throws SystemException {
		try { 
			beanRemoto.agregar(direccionesValidas); 
		} catch (Exception e) {   
			LogDeMidasWeb.log("Error", Level.FINEST, e);
			throw new SystemException(e.getClass().getCanonicalName());
		} 
	} 
	
}
