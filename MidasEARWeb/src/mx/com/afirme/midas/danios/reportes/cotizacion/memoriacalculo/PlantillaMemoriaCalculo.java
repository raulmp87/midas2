package mx.com.afirme.midas.danios.reportes.cotizacion.memoriacalculo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.igualacion.IgualacionPrimaNetaForm;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.lang.NullArgumentException;

public class PlantillaMemoriaCalculo extends MidasPlantillaBase{
	
	private BigDecimal idToCotizacion;
	private Integer numeroInciso;
	private CotizacionDTO cotizacionDTO;
	private IncisoCotizacionDTO incisoCotizacionDTO;
	
	public PlantillaMemoriaCalculo(BigDecimal idToCotizacion,Integer numeroInciso)throws NullArgumentException {
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
		inicializarDatosPlantilla();
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporte(claveUsuario);
		return getByteArrayReport();
	}
	
	private void generarReporte(String claveUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		if (this.idToCotizacion != null ){
			
			if(this.numeroInciso == null)
				numeroInciso = -1;
			
			IgualacionPrimaNetaForm igualacionPrimaNetaForm = new IgualacionPrimaNetaForm();
			
			List<SeccionCotizacionDTO> seccionesCombo = new ArrayList<SeccionCotizacionDTO>();
			
			boolean igualacionValida = CotizacionDN.getInstancia(claveUsuario).
					mostrarIgualacionPrimas(idToCotizacion, numeroInciso, seccionesCombo, igualacionPrimaNetaForm);
			
			if(!igualacionValida){
				throw new SystemException(igualacionPrimaNetaForm.getMensajeErrorIgualacionPrima());
			}
			
			//Poblar parametros generales del reporte
			poblarParametrosPlantilla(claveUsuario,igualacionPrimaNetaForm);

			getListaRegistrosContenido().clear();
			
			for(SeccionCotizacionDTO seccion : igualacionPrimaNetaForm.getSecciones()){
//				try {
//					seccion.getSeccionDTO().setNombreComercial(
//							new String(seccion.getSeccionDTO().getNombreComercial().getBytes("UTF-8")));
//				} catch (UnsupportedEncodingException e) {
//					LogDeMidasWeb.log("Error al codificar cadena a UTF-8", Level.WARNING, e);
//				}
				getListaRegistrosContenido().add(seccion);
			}
			
			super.finalizarReporte();
			
		}
	}
	

	private void poblarParametrosPlantilla(String claveUsuario, IgualacionPrimaNetaForm igualacionPrimaNetaForm) throws SystemException{
		if(numeroInciso != -1 && (incisoCotizacionDTO == null || 
				(incisoCotizacionDTO.getId().getIdToCotizacion().compareTo(idToCotizacion) != 0 &&
					incisoCotizacionDTO.getId().getNumeroInciso().intValue() != numeroInciso.intValue()))){
			incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().getPorId(new IncisoCotizacionId(idToCotizacion,new BigDecimal(numeroInciso)));
			
			if(incisoCotizacionDTO == null)
				throw new SystemException("El inciso seleccionado no existe");
			
			cotizacionDTO = incisoCotizacionDTO.getCotizacionDTO();
		}
		else if(incisoCotizacionDTO != null){
			cotizacionDTO = incisoCotizacionDTO.getCotizacionDTO();
		}
		else if(cotizacionDTO == null || cotizacionDTO.getIdToCotizacion().compareTo(idToCotizacion) != 0){//no hay inciso
			cotizacionDTO = CotizacionDN.getInstancia(claveUsuario).getPorId(idToCotizacion);
		}
		
		getParametrosVariablesReporte().put("IDTOCOTIZACION", idToCotizacion);
		
		if(cotizacionDTO.getSolicitudDTO().getNombreAgente() != null){
			getParametrosVariablesReporte().put("NOMBREAGENTE", cotizacionDTO.getSolicitudDTO().getNombreAgente());
		}
		
		if(cotizacionDTO.getNombreAsegurado()!= null){
			getParametrosVariablesReporte().put("NOMBREASEGURADO", cotizacionDTO.getNombreAsegurado());
		}
		
		boolean poblarParametroSinDireccionEspecifica = false;
		
		if(numeroInciso != -1 && incisoCotizacionDTO != null){
			getParametrosVariablesReporte().put("NUMEROINCISO", numeroInciso.toString());
			
			if(incisoCotizacionDTO.getDireccionDTO() != null){
				getParametrosVariablesReporte().put("DIRECCIONINCISO", incisoCotizacionDTO.getDireccionDTO().toString());
			}
			else{
				poblarParametroSinDireccionEspecifica = true;
			}
		}
		else{
			poblarParametroSinDireccionEspecifica = true;
		}
		
		if(poblarParametroSinDireccionEspecifica){
			
			int totalIncisos = IncisoCotizacionDN.getInstancia().obtenerCantidadIncisosPorCotizacion(idToCotizacion);
			
			if(totalIncisos == 1){
				incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion).get(0);
				
				getParametrosVariablesReporte().put("NUMEROINCISO", ""+totalIncisos);
				getParametrosVariablesReporte().put("DIRECCIONINCISO", incisoCotizacionDTO.getDireccionDTO().toString());
			}
			else{
				//Cantidad variable de incisos
				getParametrosVariablesReporte().put("NUMEROINCISO", "Varios");
				getParametrosVariablesReporte().put("DIRECCIONINCISO", totalIncisos+", seg�n listado que obra en poder de la instituci�n.");
			}
			
		}
		
		if(cotizacionDTO.getClaveEstatus().shortValue() >= ConstantesCotizacion.ESTATUS_COT_LIBERADA){
			if(cotizacionDTO.getFechaLiberacion() != null){
				getParametrosVariablesReporte().put("FECHALIBERACION", 
						new SimpleDateFormat("dd/MM/yyyy").format(cotizacionDTO.getFechaLiberacion()));
			}
			getParametrosVariablesReporte().put("MOSTRAR_LEYENDA_COT_NO_LIBERADA", Boolean.FALSE);
		}
		else{
			getParametrosVariablesReporte().put("MOSTRAR_LEYENDA_COT_NO_LIBERADA", Boolean.TRUE);
		}
		
		if(cotizacionDTO.getFechaInicioVigencia() != null)
			getParametrosVariablesReporte().put("FECHA_INICIO_VIGENCIA",new SimpleDateFormat("dd/MM/yyyy").format(cotizacionDTO.getFechaInicioVigencia()));
		
		if(cotizacionDTO.getFechaFinVigencia() != null)
			getParametrosVariablesReporte().put("FECHA_FIN_VIGENCIA",new SimpleDateFormat("dd/MM/yyyy").format(cotizacionDTO.getFechaFinVigencia()));
		
		if (cotizacionDTO.getIdMoneda() != null){
			MonedaDTO monedaDTO;
			try {
				monedaDTO = MonedaDN.getInstancia().getPorId(new Short(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
				getParametrosVariablesReporte().put("MONEDA", ((monedaDTO != null)?monedaDTO.getDescripcion() : "No disponible"));
			} catch (Exception e) {
				getParametrosVariablesReporte().put("MONEDA", "No disponible");
			}
		}
		
		getParametrosVariablesReporte().put("URL_IMAGEN_FONDO", "/img/cotNoAutorizada.png");
		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
		getParametrosVariablesReporte().put("PRIMA_NETA_ANUAL", igualacionPrimaNetaForm.getPrimaNetaAnual());
		getParametrosVariablesReporte().put("PRIMA_NETA_COTIZACION", igualacionPrimaNetaForm.getPrimaNetaCotizacion());
		getParametrosVariablesReporte().put("RECARGO", igualacionPrimaNetaForm.getMontoRecargoPagoFraccionado());
		getParametrosVariablesReporte().put("GASTOS_EXPEDICION", igualacionPrimaNetaForm.getDerechosPoliza());
		getParametrosVariablesReporte().put("LEYENDA_IVA", "IVA a la taza del "+igualacionPrimaNetaForm.getFactorIVA());
		getParametrosVariablesReporte().put("VALOR_IVA", igualacionPrimaNetaForm.getMontoIVA());
		getParametrosVariablesReporte().put("PRIMA_TOTAL", igualacionPrimaNetaForm.getPrimaNetaTotal());
		
		//Se agrega el subreporte de coberturas
		
		String nombrePlantillaSubReporte = getPaquetePlantilla()+ 
			UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.plantillaMemoriaCalculo.subReporteCoberturas");
		JasperReport subReporteCoberturas = getJasperReport(nombrePlantillaSubReporte);
		if(subReporteCoberturas == null)
			generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
		getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
	}
	
	private void inicializarDatosPlantilla(){
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.plantillaMemoriaCalculo.paquete"));
		setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.plantillaMemoriaCalculo"));
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_XLS );
	}
	

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
}
