package mx.com.afirme.midas.danios.reportes.reportercs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaRangosDN;
import mx.com.afirme.midas.danios.reportes.reportercs.documento.DocumentoDigitalRangosDN;
import mx.com.afirme.midas.danios.reportes.reportercs.documento.DocumentoDigitalRangosDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.esquemas.DocumentoDigitalEsquemasDN;
import mx.com.afirme.midas.danios.reportes.reportercs.esquemas.DocumentoDigitalEsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.CargaMasivaEsquemasExcel;
import mx.com.afirme.midas.sistema.excel.CargaMasivaRangosExcel;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.service.catalogos.reaseguradorcnsf.ReaseguradorCnsfService;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReporteRCSAction extends MidasMappingDispatchAction {
	
	private static final String CVENEGOCIO = "claveNegocio";
	private static final String NEGOCIO = "negocio";
	private static final String NOMENCLATURA = "nomenclatura";
	private static final String TITULO = "titulo";
	private static final String ERRORSOLVENCIA = "MidasWeb no se ejecuto el proceso de Solvencia.";
	private static final String LEYENDA = "leyenda";
	private static final String NOTIFICACION = "Notifique al administrador lo siguiente:<br>" ;
	private static final String EXCEPCION = "exc";
	private static final String ERRORIMPRESION = "errorImpresion";
	private static final String DOCUMENTOS = "documentos";
	private static final String ERRORMIDAS = "MidasWeb no termino el proceso correctamente.";
	private static final String SYSTEMEXC = "System Exception: ";
	private static final String LLAVERAMO = "{RAMO}";
	private static final String LLAVEFECHA = "{FECHA}";
	private static final String OKMIDAS = "Midas Web se ha actualizado.";
	private static final String OKMIDAS_RANGOS = "Se han actualizado los rangos.<br>";
	private static final String ERRORMIDAS_FWD = "error";
	
	private ReaseguradorCnsfService reaseguradorCnsfService;
			
	public ActionForward mostrarReporteRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		
		ReporteRCSForm productoForm = (ReporteRCSForm) form;
	
		ReporteRCS reporteBasesEmision = new ReporteRCS(productoForm);
		try {
			request.setAttribute(NEGOCIO, request.getParameter(CVENEGOCIO));
			request.setAttribute(NOMENCLATURA, reporteBasesEmision.obtenerNomenclaturaArch("RCS"));
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward mostrarReporteREASRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		ReporteRCSForm productoForm = (ReporteRCSForm) form;
		ReporteRCS reporteBasesEmision = new ReporteRCS(productoForm);
		try {
			request.setAttribute(NOMENCLATURA, reporteBasesEmision.obtenerNomenclaturaArch("REAS"));
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward mostrarEdicionREASRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		ReporteRCSForm productoForm = (ReporteRCSForm) form;
		ReporteRCS reporteBasesEmision = new ReporteRCS(productoForm);
		try {
			request.setAttribute(NOMENCLATURA, reporteBasesEmision.obtenerNomenclaturaArch("REAS"));
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	
	public ActionForward mostrarCargaREASRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		return mapping.findForward(Sistema.EXITOSO);
	} 
	
	public ActionForward mostrarCargaCadAmis(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		return mapping.findForward(Sistema.EXITOSO);
	} 
		
	public ActionForward mostrarCargaRangosRCS (ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
		try {
			request.setAttribute("durPro", reporteBasesEmision.obtenerNumRangosDuracion(1));
			request.setAttribute("durRem", reporteBasesEmision.obtenerNumRangosDuracion(2));
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward(Sistema.EXITOSO);
	} 
	
	public ActionForward rangosPorDuracion (ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
				 
		try {
			reporteBasesEmision.setRangosDuracionSP();
			
			request.setAttribute("durPro", reporteBasesEmision.obtenerNumRangosDuracion(1));
			request.setAttribute("durRem", reporteBasesEmision.obtenerNumRangosDuracion(2));
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}   
		
		return mapping.findForward(Sistema.EXITOSO);
	} 
	
	public ActionForward mostrarGeneraInfoRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		
		request.setAttribute(CVENEGOCIO, request.getParameter(CVENEGOCIO));
		
		return mapping.findForward(Sistema.EXITOSO);
	} 
	
	public ActionForward iniciarProcesoRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String fechaCorte = request.getParameter("fechaInicio");
		String negocio = request.getParameter(NEGOCIO);
		String reproceso = request.getParameter("reproceso");
		int codigoRespuesta = 0;
		
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		try {
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			codigoRespuesta = reporteBasesEmision.generaInfoRCS(fechaCorte, negocio, reproceso);
			
			if(codigoRespuesta == 999)
			{
				throw new SystemException("Ya esta programado o en ejecucion un corte : " ,20);
			}else if(codigoRespuesta == 998)
			{
				throw new SystemException("No hay Esquemas de Reaseguro registrados para el Corte: " + fechaCorte ,20);
			}
			else if(codigoRespuesta == 995)
			{
				throw new SystemException("No es posible programar el Corte: " + fechaCorte ,20);
			}
			throw new SystemException("El proceso ha sido Programado: " ,20);
		} catch (ParseException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(ERRORMIDAS_FWD);
		
	} 
	
	public ActionForward cargaEsquemasREAS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String idToControlArchivo = request.getParameter("idToControlArchivo");
		try {
			CargaMasivaEsquemasExcel<EsquemasDTO> cargaMasivaEsqExcel = new CargaMasivaEsquemasExcel<EsquemasDTO>(EsquemasDTO.class);
			        						
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			ControlArchivoDTO archivoDTO =  controlArchivoDN.getPorId(new BigDecimal(idToControlArchivo));
			
			String nombreOriginalArchivo = archivoDTO.getNombreArchivoOriginal();
			String extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
							nombreOriginalArchivo.lastIndexOf('.'),
							nombreOriginalArchivo.length());

			
			if(!cargaMasivaEsqExcel.cargaArchivoEsquemas(new BigDecimal(idToControlArchivo),extension))
			{
				if(!".xls".equals(extension)){
					throw new SystemException("[Esquemas]Extension de archivo no Valido.: " + extension ,20);
				}
				else
				{
					throw new SystemException("[Esquemas]Formato no Valido. " ,20);
				}
					
			}else
			{
				throw new SystemException("[Esquemas]El procreso ha sido Finalizado" ,20);
			}
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
			return mapping.findForward(ERRORMIDAS_FWD);
		}
		
	} 
	
	public ActionForward cargaRangos(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String idToControlArchivo = request.getParameter("idToControlArchivo");
		try {
			CargaMasivaRangosExcel<RangosDTO> cargaMasivaRangosExcel = new CargaMasivaRangosExcel<RangosDTO>(RangosDTO.class);
			       						
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			ControlArchivoDTO archivoDTO =  controlArchivoDN.getPorId(new BigDecimal(idToControlArchivo));
			
			String nombreOriginalArchivo = archivoDTO.getNombreArchivoOriginal();
			String extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
							nombreOriginalArchivo.lastIndexOf('.'),
							nombreOriginalArchivo.length());

			if(!cargaMasivaRangosExcel.cargaArchivoRangos(new BigDecimal(idToControlArchivo),extension))
			{
				if(!"xls".equals(extension)){
					throw new SystemException("[Rangos]Extension de archivo no Valido.: " + extension ,20);
				}
				else
				{
					throw new SystemException("[Rangos]Formato no Valido. " ,20);
				}
					
			}else
			{
				throw new SystemException("[Rangos]El procreso ha sido Finalizado" ,20);
			}
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORSOLVENCIA);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
			mapping.findForward(ERRORMIDAS_FWD);
		}
		
		return mapping.findForward(ERRORIMPRESION);
	} 
	
	public ActionForward generarReporteRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String nombreArchivo = "";
		StringBuilder fecha = new StringBuilder();
		
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			ReporteRCS reporteRCS = new ReporteRCS(movimientoEmisionForm);
			byte [] byteArray = reporteRCS.obtenerReporte();
			if ( byteArray != null){
			
				if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
						
						fecha.append(getFechaRCS(movimientoEmisionForm.getFechaInicio()));
						nombreArchivo = movimientoEmisionForm.getNomenclatura().replace(LLAVEFECHA, fecha.toString());
						nombreArchivo = nombreArchivo.replace(LLAVERAMO, reporteRCS.obtenerClaveRamo(movimientoEmisionForm.getId_ramo()));
						
						reporteRCS.sp_actualizaNom("RCS",movimientoEmisionForm.getNomenclatura());
				}
				
				ZipEntry entry = new ZipEntry( nombreArchivo + ".txt");
				entry.setSize( byteArray.length );
				zos.putNextEntry( entry );
				zos.write( byteArray );
				zos.closeEntry();
				
				ZipEntry entryCSV = new ZipEntry( nombreArchivo +".csv");
				entry.setSize( byteArray.length );
				zos.putNextEntry( entryCSV );
				zos.write( byteArray );
				zos.closeEntry();
				if(!"5".equals(movimientoEmisionForm.getClaveNegocio()))
				{
					byte [] byteDetalle = reporteRCS.obtenerDetalleReporte();
					if ( byteDetalle != null && !UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
	
						nombreArchivo = movimientoEmisionForm.getNomenclatura().replace(LLAVEFECHA, fecha.toString());
						nombreArchivo = nombreArchivo.replace(LLAVERAMO, reporteRCS.obtenerClaveRamo(movimientoEmisionForm.getId_ramo()))+"det.csv";
						
						ZipEntry entryDet = new ZipEntry( nombreArchivo );
						entryDet.setSize( byteDetalle.length );
						zos.putNextEntry( entryDet );
						zos.write( byteDetalle);
						zos.closeEntry();
					}
				}
				zos.close();
				this.writeBytes(response, baos.toByteArray(), Sistema.TIPO_ZIP,nombreArchivo+ ".zip");
				baos.close();
			}
			else{
				throw new SystemException(SYSTEMEXC+movimientoEmisionForm.toString(),20);
			}
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteRCS1");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (IOException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage()+ " generarReporteRCS2");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (ParseException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage()+ " generarReporteRCS3");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		return mapping.findForward(forward);
	}
	
	public ZipOutputStream atachFile(ZipOutputStream zos,byte [] byteArray,String nombreArchivo)
	{
		try {
			ZipEntry entry = new ZipEntry( nombreArchivo );
			entry.setSize( byteArray.length );
			zos.putNextEntry( entry );
			zos.write( byteArray );
		} catch (IOException e) {
			LogDeMidasWeb.log("Error al adjuntar archivo.", Level.INFO, e);;
		}
		
		return zos;
	}
	
	public ActionForward generarReporteRCSVida(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String nombreArchivo = "";
		String extArch = "";
		String fecha = "";
		try {
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			byte [] byteArray = reporteBasesEmision.obtenerReporte();
						
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			if ( byteArray != null && !UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
			
				fecha = getFechaRCS(movimientoEmisionForm.getFechaInicio());
				nombreArchivo = movimientoEmisionForm.getNomenclatura().replace(LLAVEFECHA, fecha);
				nombreArchivo = nombreArchivo.replace(LLAVERAMO, reporteBasesEmision.obtenerClaveRamo(movimientoEmisionForm.getId_ramo()))+extArch;
				
				reporteBasesEmision.sp_actualizaNom("RCS",movimientoEmisionForm.getNomenclatura());
				
				zos = atachFile(zos, byteArray, nombreArchivo);
				
				if("13".equals(movimientoEmisionForm.getId_ramo()))
				{
					byte[] breportePas = reporteBasesEmision.obtenerReportePas();
					nombreArchivo = movimientoEmisionForm.getNomenclatura().replace(LLAVEFECHA, fecha);
					nombreArchivo = nombreArchivo.replace(LLAVERAMO, reporteBasesEmision.obtenerClaveRamo(movimientoEmisionForm.getId_ramo()));
					
					zos = atachFile(zos, breportePas, nombreArchivo+"PAS"+extArch);
					
				}
				
				byte []byteDetalle = reporteBasesEmision.obtenerDetalleReporte();
				if ( byteDetalle != null && !UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
					zos = atachFile(zos, byteDetalle, nombreArchivo+"det.csv");
				}
				
				byte []byteVigorLP = reporteBasesEmision.obtenerDetalleVigorLP();
				if ( byteDetalle != null && !UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
					
					ZipEntry entryCSV = new ZipEntry( nombreArchivo+"VigorLP.csv");
					entryCSV.setSize( byteVigorLP.length  );
					zos.putNextEntry( entryCSV );
					zos.write( byteVigorLP );
					
				}
				
				zos.closeEntry();
				zos.close();
				
				this.writeBytes(response, baos.toByteArray(), Sistema.TIPO_ZIP,nombreArchivo+ ".zip");
				baos.close();
			}
			else{
				throw new SystemException(SYSTEMEXC+movimientoEmisionForm.toString(),20);
			}
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteRCSVida1");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (IOException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteRCSVida2");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}catch(Exception e){
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteRCSVida3");
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(forward);
	}
	
	public ActionForward generarDReporteRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String nombreArchivo = "";
		try {
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			byte [] byteArray = reporteBasesEmision.obtenerDetalleReporte();
			if ( byteArray != null){
			
				if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
						
					nombreArchivo = movimientoEmisionForm.getNomenclatura().replace(LLAVEFECHA, 
							getFechaRCS(movimientoEmisionForm.getFechaInicio()));
					nombreArchivo = nombreArchivo.replace(LLAVERAMO, reporteBasesEmision.obtenerClaveRamo(movimientoEmisionForm.getId_ramo()))+".csv";
					
					reporteBasesEmision.sp_actualizaNom("RCS",movimientoEmisionForm.getNomenclatura());
					
				}
				this.writeBytes(response, byteArray, Sistema.TIPO_CSV, nombreArchivo);
				return null;
			}
			else{
				throw new SystemException(SYSTEMEXC+movimientoEmisionForm.toString(),20);
			}
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarDReporteRCS1");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (IOException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarDReporteRCS2");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}catch(Exception e){
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarDReporteRCS3");
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(forward);
	}
	
	public ActionForward eliminarContratos(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		Date corte;
		Date ultimoCorte;
		String forward = Sistema.EXITOSO;		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String cveNegocio = request.getParameter("cve_negocio");
		String fecha = request.getParameter("anioContrato");
		BigDecimal dia = new BigDecimal(fecha.substring(0, 2));
		BigDecimal mes = new BigDecimal(fecha.substring(3,5));
		BigDecimal anio = new BigDecimal(fecha.substring(6,10));
		
		try {
			ReporteRCSSN reporteRCSSN = new ReporteRCSSN();
			
			corte = formatter.parse(fecha);
			
			ultimoCorte = reporteRCSSN.getFechaUltimoCorteProcesado();
			
			if(corte.compareTo(ultimoCorte) != 0) {
				request.setAttribute(TITULO, ERRORMIDAS);
				request.setAttribute(LEYENDA, "No est&aacute; permitido modificar registros pertenecientes a cortes diferentes de " + formatter.format(ultimoCorte));
				return mapping.findForward(forward);			
			}
						
			if(cveNegocio.equals("1")) {
				mes = new BigDecimal(4); 
				dia = new BigDecimal(1);
				cveNegocio = "2";
			} else if(cveNegocio.equals("2")) {
				mes = new BigDecimal(4); 
				dia = new BigDecimal(1);
				cveNegocio = "4";
			}
			
			CargaMasivaRangosDN.getInstancia().eliminarCargaMasiva(anio,mes, dia,cveNegocio);
			
			request.setAttribute(TITULO, OKMIDAS);
			request.setAttribute(LEYENDA, OKMIDAS_RANGOS);
		} catch (ParseException pe) {
			LogDeMidasWeb.log("Error al parsear la fecha " + fecha, Level.WARNING, pe);
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION + "No se logr&oacute; convertir la fecha seleccionada");
			request.setAttribute(EXCEPCION, pe);
			forward = ERRORIMPRESION;
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		
		return mapping.findForward(forward);
	}
	
	public ActionForward actualizaCalificaciones(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		int mes = 0;
		int anio = 0;
		int dia = 0;
		Date corte;
		Date ultimoCorte;
		String forward = Sistema.EXITOSO;
		String fecha = request.getParameter("anioCorte");
		int cveNegocio = Integer.parseInt(request.getParameter("cve_negocio"));
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			ReporteRCSSN reporteRCSSN = new ReporteRCSSN();
			
			corte = formatter.parse(fecha);
			
			ultimoCorte = reporteRCSSN.getFechaUltimoCorteProcesado();
			
			if(corte.compareTo(ultimoCorte) != 0) {
				request.setAttribute(TITULO, ERRORMIDAS);
				request.setAttribute(LEYENDA, "No est&aacute; permitido modificar registros pertenecientes a cortes diferentes de " + formatter.format(ultimoCorte));
				return mapping.findForward(forward);
			}
			
			dia = Integer.parseInt(fecha.substring(0, 2));
			mes = Integer.parseInt(fecha.substring(3, 5));
			anio = Integer.parseInt(fecha.substring(6, 10));
			
			ReporteRCSDN.getInstancia().actualizaCalificacionesSP(dia,mes,anio,cveNegocio);
			
			request.setAttribute(TITULO, OKMIDAS);
			request.setAttribute(LEYENDA, OKMIDAS_RANGOS);
		
		} catch (ParseException pe) {
			LogDeMidasWeb.log("Error al parsear la fecha " + fecha, Level.WARNING, pe);
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION + "No se logr&oacute; convertir la fecha seleccionada");
			request.setAttribute(EXCEPCION, pe);
			forward = ERRORIMPRESION;
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		return mapping.findForward(forward);
	}
	
	
	public ActionForward eliminaRangos(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		try {
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			boolean byteArray = reporteBasesEmision.eliminaRangos();
			if ( !byteArray){
				
				request.setAttribute(TITULO, "MidasWeb no puede actualizar.");
				request.setAttribute(LEYENDA, NOTIFICACION );
				forward = ERRORIMPRESION;
			}
			else{
				
				request.setAttribute(TITULO, OKMIDAS);
				request.setAttribute(LEYENDA, OKMIDAS_RANGOS);
				forward = ERRORIMPRESION;
			}
				
		
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		return mapping.findForward(forward);
	}
	
	public ActionForward actualizaRangos(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		try {
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			boolean byteArray = reporteBasesEmision.actualizaRangos();
			if ( !byteArray){
				
				request.setAttribute(TITULO, "MidasWeb no puede actualizar.");
				request.setAttribute(LEYENDA, NOTIFICACION );
				forward = ERRORIMPRESION;
			}
			else{
				
				request.setAttribute(TITULO, OKMIDAS);
				request.setAttribute(LEYENDA, OKMIDAS_RANGOS);
				forward = ERRORIMPRESION;
			}
				
		
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage());
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		return mapping.findForward(forward);
	}
	
	public ActionForward generarReporteREASRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String nombreArchivo = "";
		String extArch = "";
		try {
			String encabezado = "CONTRATO|ANIO|CER|TIPO_COBERTURA|NIVEL|ORDEN_ENTRADA|RETENCION|CAP_LIMITE|RETENCION_AD|REINSTALACIONES|LLAVE_DIRECTA|LLAVE_RET|CVE_REASEGURADORA_ORIGINAL|CVE_REASEGURADORA|PART_REASEGURADOR|CALIFICACION" +  System.getProperty("line.separator");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			byte [] byteArray = reporteBasesEmision.obtenerReporteREAS(1);
			byte [] byteArrayDet = reporteBasesEmision.obtenerReporteREAS(2);			
			byte [] byteArrayValidaRR6 = reporteBasesEmision.obtenerReporteREAS(3);
			if ( byteArray != null){
			
				if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
						
					nombreArchivo = movimientoEmisionForm.getNomenclatura().replace(LLAVEFECHA, 
								getFechaRCS(movimientoEmisionForm.getFechaInicio()))+extArch;
					reporteBasesEmision.sp_actualizaNom("REAS",movimientoEmisionForm.getNomenclatura());
				}
				 
				ZipEntry entry = new ZipEntry( nombreArchivo + ".txt" );
				entry.setSize( byteArray.length );
				zos.putNextEntry( entry );
				zos.write( byteArray );
				
				zos.flush();
				ZipEntry entryCSV = new ZipEntry( nombreArchivo +".csv");
				entryCSV.setSize( byteArrayDet.length + encabezado.getBytes().length );
				zos.putNextEntry( entryCSV );
				zos.write(encabezado.getBytes());
				zos.write( byteArrayDet );
				zos.flush();
				
				ZipEntry entryRR6CSV = new ZipEntry( nombreArchivo +"rr6.csv");
				entryRR6CSV.setSize( byteArrayValidaRR6.length );
				zos.putNextEntry( entryRR6CSV );
				zos.write( byteArrayValidaRR6 );
				zos.flush();
				
				zos.closeEntry();
				zos.close();
				
				this.writeBytes(response, baos.toByteArray(), Sistema.TIPO_ZIP,nombreArchivo+ ".zip");
				baos.close();
				return null;
			}
			else{
				throw new SystemException(SYSTEMEXC+movimientoEmisionForm.toString(),20);
			}
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteREASRCS1");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (IOException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteREASRCS2");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}catch(Exception e){
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " generarReporteREASRCS3");
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(forward);
	}
		
	public ActionForward consultaValidacionesRCS(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String nombreArchivo = "Validaciones_";
		String fechaInicial = request.getParameter("fechaInicial").toString() + " 23:59:59";
		String claveNegocio = request.getParameter(NEGOCIO).toString();
		movimientoEmisionForm.setFechaInicio(fechaInicial);
		movimientoEmisionForm.setClaveNegocio(claveNegocio);
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			byte [] byteArray = reporteBasesEmision.obtenerValidaciones();
							
			ZipEntry entry = new ZipEntry( nombreArchivo + ".txt" );
			entry.setSize( byteArray.length );
			zos.putNextEntry( entry );
			zos.write( byteArray );
			
			zos.closeEntry();
			zos.close();
			
			this.writeBytes(response, baos.toByteArray(), Sistema.TIPO_ZIP,nombreArchivo+movimientoEmisionForm.getFechaInicio()+ ".zip");
			baos.close();
			
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " consultaValidacionesRCS");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (IOException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " consultaValidacionesRCS2");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		return mapping.findForward(forward);
	}
	
	public ActionForward consultaRangos(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String nombreArchivo = "Rangos_";
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			ReporteRCS reporteBasesEmision = new ReporteRCS(movimientoEmisionForm);
			byte [] byteArray = reporteBasesEmision.obtenerReporteRangos();
							
			ZipEntry entry = new ZipEntry( nombreArchivo + ".csv" );
			entry.setSize( byteArray.length );
			zos.putNextEntry( entry );
			zos.write( byteArray );
			
			zos.closeEntry();
			zos.close();
			
			this.writeBytes(response, baos.toByteArray(), Sistema.TIPO_ZIP,nombreArchivo+movimientoEmisionForm.getFechaInicio()+ ".zip");
			baos.close();
			
		} catch (SystemException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " consultaValidacionesRCS");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		} catch (IOException e) {
			request.setAttribute(TITULO, ERRORMIDAS);
			request.setAttribute(LEYENDA, NOTIFICACION +e.getMessage() + " consultaValidacionesRCS2");
			request.setAttribute(EXCEPCION, e);
			forward = ERRORIMPRESION;
		}
		return mapping.findForward(forward);
	}
	
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		BigDecimal negocio;
		try {
			String claveNegocio = request.getParameter(CVENEGOCIO).toString();
			DocumentoDigitalRangosDN documentoDigitalSolicitudDN = DocumentoDigitalRangosDN.getInstancia();
			
			if("1".equals(claveNegocio))
			{
				negocio = new BigDecimal(1);
			}else{
				negocio = new BigDecimal(2);
			}
			
			request.setAttribute(DOCUMENTOS, documentoDigitalSolicitudDN.listarDocumentosDigitalesPorCveNegocio(negocio));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward consultaProcesoRCS(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ReporteRCSForm movimientoEmisionForm = (ReporteRCSForm)form;
		String claveNegocio = request.getParameter(NEGOCIO).toString();
		String fechaInicial = request.getParameter("fechaInicial").toString() + " 23:59:59";
		try {
			ReporteRCS reporteRCS = new ReporteRCS(movimientoEmisionForm);
			List<SolvenciaLogDTO> documentos;
			documentos = reporteRCS.getLogSolvencia(UtileriasWeb.getFechaFromStringHS(fechaInicial), claveNegocio);
			request.setAttribute(DOCUMENTOS, documentos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			request.setAttribute(EXCEPCION, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ParseException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			request.setAttribute(EXCEPCION, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarDocumentosReas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			List<DocumentoDigitalEsquemasDTO> documentos;
			DocumentoDigitalEsquemasDN documentoDigitalSolicitudDN = DocumentoDigitalEsquemasDN.getInstancia();
			 
			documentos = documentoDigitalSolicitudDN.listarTodos();
			request.setAttribute(DOCUMENTOS, documentos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			request.setAttribute(EXCEPCION, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Elimina un documento de esquemas, por el <code>id</code> de la solicitud y el 
	 * <code>documentoDigitalSolicitudDTO.getIdToControlArchivo()</code>
	 * @param mapping
	 * @param form
	 * @param request 
	 * @param responsesolicitud/borrarDocumento
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
			DocumentoDigitalEsquemasDN documentoDigitalEsquemasDN = DocumentoDigitalEsquemasDN.getInstancia();
			DocumentoDigitalEsquemasDTO documentoDigitalEsquemasDTO = documentoDigitalEsquemasDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			controlArchivoDTO.setIdToControlArchivo(documentoDigitalEsquemasDTO.getIdToControlArchivo());
			ControlArchivoDN controlArchivoD = ControlArchivoDN.getInstancia();
			controlArchivoD.borrar(controlArchivoDTO);
			
			documentoDigitalEsquemasDN.borrar(documentoDigitalEsquemasDTO);
			this.listarDocumentosReas(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward borrarDocumentoRan(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
			
			DocumentoDigitalRangosDN documentoDigitalRangosDN = DocumentoDigitalRangosDN.getInstancia();
			DocumentoDigitalRangosDTO documentoDigitalRangosDTO = documentoDigitalRangosDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			controlArchivoDTO.setIdToControlArchivo(documentoDigitalRangosDTO.getIdToControlArchivo());
			ControlArchivoDN controlArchivoD = ControlArchivoDN.getInstancia();
			controlArchivoD.borrar(controlArchivoDTO);
			
			documentoDigitalRangosDN.borrar(documentoDigitalRangosDTO);
			this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		} catch (ExcepcionDeAccesoADatos e	) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute(EXCEPCION, e);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public String getFechaRCS(String fecha)
	{
		String []data = fecha.split("/");
		StringBuilder fechaB = new StringBuilder();
		for(int i=data.length;i>0;i--)
		{
			fechaB.append(data[i-1]);
		}		
		return fechaB.toString();
	}
	
}