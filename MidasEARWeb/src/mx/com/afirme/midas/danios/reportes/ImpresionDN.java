package mx.com.afirme.midas.danios.reportes;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import mx.com.afirme.midas.sistema.DataSourceLocator;
import mx.com.afirme.midas.sistema.MidasBaseReporte;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ImpresionDN extends MidasBaseReporte {
	private static final ImpresionDN INSTANCIA = new ImpresionDN();
	private Connection connection = null;
	private DataSource dataSource = null;

	public static ImpresionDN getInstancia() {
		return ImpresionDN.INSTANCIA;
	}

	public byte[] imprimirCotizacion(BigDecimal idCotizacion) {
		JasperReport reporte;
		byte[] reporteGenerado = null;
		try {
			//TODO generar logica por tipo de cotizacion
			String paquete = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionEmpresarial.paquete");
			String cotizacion1erRiesgo = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo");
			String subReporte0 = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo.subReporteCoberturas");
			String subReporte1 = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.primerRiesgo.subReporteSeccionesLUC");

			reporte = super.getJasperReport(paquete+cotizacion1erRiesgo);

			dataSource = DataSourceLocator
					.getDataSource(Sistema.MIDAS_DATASOURCE);
			connection = dataSource.getConnection();
			
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("P_IDTOCOTIZACION", idCotizacion);
			parametros.put("P_1ER_RIESGO", super.getJasperReport(paquete+subReporte0));
			parametros.put("P_LUC", super.getJasperReport(paquete+subReporte1));
			
			reporteGenerado = generaReporte(Sistema.TIPO_PDF, reporte,parametros, connection);
			
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();			
		}finally{
			 DbUtils.closeQuietly(connection);
		}
		return reporteGenerado;
	}
	
	public JasperReport getJasperReport(String sourceFileName){
		return super.getJasperReport(sourceFileName);
	}
}
