package mx.com.afirme.midas.danios.reportes.cotizacion.endoso.rehabilitacion;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.endoso.PL20MovimientosEndoso;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReporteCotizacionEndosoRehabilitacion extends ReporteCotizacionBase {

	public ReporteCotizacionEndosoRehabilitacion(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		setListaPlantillas(new ArrayList<byte[]>());
	}

	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		setMapaParametrosGeneralesPlantillas(new HashMap<String, Object>());
//		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		byte[] reporteTMP = null;
//		consultarInformacionCotizacion(nombreUsuario);
		poblarParametrosComunes(claveUsuario,false);
		
		consultarMovimientosCotizacion(cotizacionDTO, claveUsuario);
		poblarParametrosCuadriculaTotalesEndoso(cotizacionDTO, claveUsuario);
		PL20MovimientosEndoso plantillaGeneralMovimientos = new PL20MovimientosEndoso(cotizacionDTO, getMapaParametrosGeneralesPlantillas(),this);
		String []movimientosExtra = new String[1];
		movimientosExtra[0] = "Tipo de endoso: "+UtileriasWeb.getDescripcionCatalogoValorFijo(40, cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue());
//		if(cotizacionDTO.getClaveMotivoEndoso() != null){
//			if(cotizacionDTO.getClaveMotivoEndoso().intValue() >= 1 && cotizacionDTO.getClaveMotivoEndoso().intValue() <= 7){
//				String descMotivo = UtileriasWeb.getDescripcionCatalogoValorFijo(42, cotizacionDTO.getClaveMotivoEndoso().intValue());
//				movimientosExtra[0] += (UtileriasWeb.esCadenaVacia(descMotivo))?"":" , Motivo: " + descMotivo;
//			}
//		}
		plantillaGeneralMovimientos.setMovimientosGeneralesExtra(movimientosExtra);
		try {
			reporteTMP = plantillaGeneralMovimientos.obtenerReporte(claveUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP != null) {
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
}
