package mx.com.afirme.midas.danios.reportes.reportercs;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL17PlantillaRCS extends MidasPlantillaBase{
	private List<ReporteRCSDTO> listaMovimientoBasesEndosoDTO;
	public PL17PlantillaRCS(List<ReporteRCSDTO> listaMovimientoBasesEndosoDTO){
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.reporteRCS.paquete"));
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.reporteRCS"));
		this.listaMovimientoBasesEndosoDTO = listaMovimientoBasesEndosoDTO;
		setTipoReporte( Sistema.TIPO_XLS );
	}
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		if(listaMovimientoBasesEndosoDTO == null || listaMovimientoBasesEndosoDTO.isEmpty())
		{
			return null;
		}
		super.setListaRegistrosContenido(new ArrayList<Object>());
		getListaRegistrosContenido().addAll(listaMovimientoBasesEndosoDTO);
		byte[] bytes;
		try {
				bytes = this.toArrayByte( listaMovimientoBasesEndosoDTO );
			
		} catch (Exception e) {
			bytes = null;
		}	
		return bytes;
	}

private byte[] toArrayByte( List<ReporteRCSDTO> list ) throws Exception {
		
		byte[] bytes = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			String element = "";
			for (ReporteRCSDTO item : list) {

				String lineSeparator = "\r\n";
				if(!item.getRegistro().equals(""))
				{
					element = item.getRegistro()+lineSeparator;
			    	out.writeBytes(element);
				}
			}
			bytes = baos.toByteArray();
		
		return bytes;
	}
}
