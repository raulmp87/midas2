package mx.com.afirme.midas.danios.reportes.cotizacion.transportes;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public abstract class PlantillaTransportesBienesAseguradosBase extends PlantillaCotizacionBase{
	protected static final BigDecimal ID_RIESGO_TRANSITO = new BigDecimal(3120d);
	protected static final BigDecimal ID_RAMO_MARITIMO_TRANSPORTES = new BigDecimal(2d);
	protected static final BigDecimal ID_SUBRAMO_TRANSPORTES_CARGA = new BigDecimal(2d);
	protected static final String NO_DISPONIBLE ="No disponible";
	
	public PlantillaTransportesBienesAseguradosBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
	}
	
	public PlantillaTransportesBienesAseguradosBase(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteCotizacionTransportes) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteCotizacionTransportes);
	}
	
	protected void procesarDatosGenericosPlantilla(String claveUsuario){
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			
			getParametrosVariablesReporte().put("NUMERO_INCISO", incisoCotizacionDTO.getId().getNumeroInciso().toBigInteger().toString());
			
			String descripcionDatoInciso[] = null;
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO;
			//MODALIDAD
			getParametrosVariablesReporte().put("DESCRIPCION_MODALIDAD", cotizacionDTO.getTipoPolizaDTO().getNombreComercial());
			
			//Descripci�n de mercanc�a
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(20d));
			try {
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_MERCANCIA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//Descripci�n de "Empaque o Embalaje"
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(40d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				descripcionDatoInciso = consultarInformacionOtros(datoIncisoCotizacionDTO, descripcionDatoInciso);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_EMPAQUE_EMBALAJE", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//Descripci�n del giro del asegurado
//			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
//			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
//			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
//			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
//			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(10d));
//			try {
//				descripcionDatoInciso = null;
//				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO);
//			} catch (SystemException e1) { }
//			getParametrosVariablesReporte().put("DESCRIPCION_GIRO_ASEGURADO", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//Descripci�n del medio de transporte
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(30d));
			
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_MEDIO_TRANSPORTE", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			String valorMedioTransporte = descripcionDatoInciso[2];
			
			//Descripci�n de veh�culos utilizados
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(90d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				descripcionDatoInciso = consultarInformacionOtros(datoIncisoCotizacionDTO, descripcionDatoInciso);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_VEHICULOS_UTILIZADOS", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//Descripci�n de tipo de vehiculo
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(100d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				descripcionDatoInciso = consultarInformacionOtros(datoIncisoCotizacionDTO, descripcionDatoInciso);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_TIPO_VEHICULO", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//no mostrar estos dato si el dato de riesgo "Medio de transporte" no contiene alguno de estos valores: "MAr�timo","Combinado mar�timo terrestre","Combinado a�reo, terrestre, y mar�timo"
			if(validarImpresionDatos(valorMedioTransporte)){
				//Descripci�n de embarcaci�n clasificada
				datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
				datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
				datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
				datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
				datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(110d));
				try {
					descripcionDatoInciso = null;
					descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				} catch (SystemException e1) { }
				getParametrosVariablesReporte().put("DESCRIPCION_EMBARCACION_CLASIFICADA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
				
				//Descripcion de la antig�edad de la embarcacion
				datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
				datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
				datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
				datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
				datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(120d));
				try {
					descripcionDatoInciso = null;
					descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				} catch (SystemException e1) { }
				getParametrosVariablesReporte().put("DESCRIPCION_ANTIGUEDAD_EMBARCACION", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
				
				//descripcion de Bandeja de conveniencia
				datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
				datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
				datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
				datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
				datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(130d));
				try {
					descripcionDatoInciso = null;
					descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				} catch (SystemException e1) { }
				getParametrosVariablesReporte().put("DESCRIPCION_BANDEJA_CONVENIENCIA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
				
				//descripcion de Transporte sobre cubierta
				datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
				datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
				datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
				datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
				datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(140d));
				try {
					descripcionDatoInciso = null;
					descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
				} catch (SystemException e1) { }
				getParametrosVariablesReporte().put("DESCRIPCION_TRANSPORTE_CUBIERTA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
				
				getParametrosVariablesReporte().put("IMPRIMIR_DATOS_MEDIO_TRANSPORTE", Boolean.TRUE);
			}
			else{
				getParametrosVariablesReporte().put("IMPRIMIR_DATOS_MEDIO_TRANSPORTE", Boolean.FALSE);
			}
			//descripcion "desde u origen"
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(60d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_ORIGEN", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//descripcion "Hasta o destino"
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(70d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_DESTINO", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//descripcion "V�a"
			/*
			 * 02/11/2010 Se remueve este dato por petici�n del usuario.
			 */
//			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
//			datoIncisoCotizacionDTO.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
//			datoIncisoCotizacionDTO.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
//			datoIncisoCotizacionDTO.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
//			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(80d));
//			try {
//				descripcionDatoInciso = null;
//				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
//			} catch (SystemException e1) { }
//			getParametrosVariablesReporte().put("DESCRIPCION_VIA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
		}
	}
	
	private String[] consultarInformacionOtros(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO,String []valores) throws SystemException{
		boolean aplicaConsultaDatoOtro = false;
		BigDecimal valorBusquedaOtro = null;
		BigDecimal datoTipoEmpaque = new BigDecimal(40);
		BigDecimal datoTipoTransporte = new BigDecimal(100);
		BigDecimal datoTipoPropietarioTransporte = new BigDecimal(90);
		BigDecimal datoOtroTipoEmpaque = new BigDecimal(45);
		BigDecimal datoOtroTipoTransporte = new BigDecimal(105);
		BigDecimal datoOtroTipoPropietarioTransporte = new BigDecimal(95);
		if(datoIncisoCotizacionDTO != null && datoIncisoCotizacionDTO.getId() != null &&
				datoIncisoCotizacionDTO.getId().getIdToRiesgo() != null && datoIncisoCotizacionDTO.getId().getIdToRiesgo().compareTo(ID_RIESGO_TRANSITO) == 0 &&
				datoIncisoCotizacionDTO.getId().getIdTcRamo() != null && datoIncisoCotizacionDTO.getId().getIdTcRamo().compareTo(ID_RAMO_MARITIMO_TRANSPORTES) == 0 &&
				datoIncisoCotizacionDTO.getId().getIdTcSubramo() != null && datoIncisoCotizacionDTO.getId().getIdTcSubramo().compareTo(ID_SUBRAMO_TRANSPORTES_CARGA) == 0){
			if(datoIncisoCotizacionDTO.getId().getIdDato() != null && datoIncisoCotizacionDTO.getId().getIdDato().compareTo(datoTipoEmpaque) == 0 &&
					valores[2] != null && valores[2].trim().equals("80")){
				valorBusquedaOtro = datoTipoEmpaque;
				aplicaConsultaDatoOtro = true;
			}
			else if(datoIncisoCotizacionDTO.getId().getIdDato() != null && datoIncisoCotizacionDTO.getId().getIdDato().compareTo(datoTipoTransporte) == 0 &&
					valores[2] != null && valores[2].trim().equals("50")){
				valorBusquedaOtro = datoTipoTransporte;
				aplicaConsultaDatoOtro = true;
			}
			else if(datoIncisoCotizacionDTO.getId().getIdDato() != null && datoIncisoCotizacionDTO.getId().getIdDato().compareTo(datoTipoPropietarioTransporte) == 0 &&
					valores[2] != null && valores[2].trim().equals("30")){
				valorBusquedaOtro = datoTipoPropietarioTransporte;
				aplicaConsultaDatoOtro = true;
			}
		}
		if(aplicaConsultaDatoOtro){
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO2 = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO2.getId().setIdToRiesgo(ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO2.getId().setIdTcRamo(ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO2.getId().setIdTcSubramo(ID_SUBRAMO_TRANSPORTES_CARGA);
			
			if(valorBusquedaOtro.compareTo(datoTipoEmpaque) == 0){
				datoIncisoCotizacionDTO2.getId().setIdDato(datoOtroTipoEmpaque);
			}
			else if(valorBusquedaOtro.compareTo(datoTipoTransporte) == 0){
				datoIncisoCotizacionDTO2.getId().setIdDato(datoOtroTipoTransporte);
			}
			else{
				datoIncisoCotizacionDTO2.getId().setIdDato(datoOtroTipoPropietarioTransporte);
			}
			
			String[] descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO2,null);
			if(descripcionDatoInciso != null && descripcionDatoInciso.length>=2 && !UtileriasWeb.esCadenaVacia(descripcionDatoInciso[1])){
				valores[1] += ": " + descripcionDatoInciso[1];
			}
		}
		
		return valores;
	}
	
	private boolean validarImpresionDatos(String valorMedioTransporte){
		return (valorMedioTransporte.equalsIgnoreCase("30") || //Mar�timo 
				valorMedioTransporte.equalsIgnoreCase("60") || //Combinado maritimo terrestre
				valorMedioTransporte.equalsIgnoreCase("80") ); //Combinado a�reo terrestre y mar�timo
	}

	protected void procesarDatosModalidadEspecifica(String claveUsuario) {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			
			String descripcionDatoInciso[] = null;
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO;
			
			//N�mero de gu�a
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(150d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_NUMERO_GUIA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//N�mero de factura
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL10_BienesAseguradosPorIncisoModEspecifica.ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(160d));
			try {
				descripcionDatoInciso = null;
				descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
			} catch (SystemException e1) { }
			getParametrosVariablesReporte().put("DESCRIPCION_NUMERO_FACTURA", descripcionDatoInciso!=null?descripcionDatoInciso[1]:NO_DISPONIBLE);
			
			//Valor del embarque
			Double valorEmbarque = reporteBase.obtenerSumatoriaSACoberturasBasicasPorInciso(incisoCotizacionDTO.getId().getNumeroInciso());
			getParametrosVariablesReporte().put("DESCRIPCION_VALOR_EMBARQUE", Sistema.FORMATO_MONEDA.format(valorEmbarque));
			
			//Se agrega un objeto para que la plantilla aparezca.
			getListaRegistrosContenido().add(new Object());
			
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}//Fin validar cotizacion != null && incisoCot != null
		else	setByteArrayReport( null);
	}
	
	protected void procesarDatosModalidadPronostico(String claveUsuario) {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			
			String descripcionDatoInciso[] = null;
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO;
			
			//Obtener n�mero de meses en el periodo de declaraci�n
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL15_BienesAseguradosPorIncisoModPronostico.ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(PL15_BienesAseguradosPorIncisoModPronostico.ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL15_BienesAseguradosPorIncisoModPronostico.ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(146d));
			DatoIncisoCotizacionDTO datoIncisoCotTMP = null;
			try {
				datoIncisoCotTMP = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			} catch (SystemException e1) { }
			
			if (datoIncisoCotTMP != null){
				Double numeroMesesEnPeriodo = null;
				try{
					numeroMesesEnPeriodo = Double.valueOf(datoIncisoCotTMP.getValor()).doubleValue();
				} catch(Exception e){}
				if (numeroMesesEnPeriodo != null && numeroMesesEnPeriodo > 0){
					//Estimado anual de embarques
					String estimadoAnualDeEmbarques = null;
					datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL15_BienesAseguradosPorIncisoModPronostico.ID_RIESGO_TRANSITO);
					datoIncisoCotizacionDTO.getId().setIdTcRamo(PL15_BienesAseguradosPorIncisoModPronostico.ID_RAMO_MARITIMO_TRANSPORTES);
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL15_BienesAseguradosPorIncisoModPronostico.ID_SUBRAMO_TRANSPORTES_CARGA);
					datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(142d));
					try {
						descripcionDatoInciso = null;
						descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
					} catch (SystemException e1) { }
					if (descripcionDatoInciso!=null){
						estimadoAnualDeEmbarques = descripcionDatoInciso[1];
						getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", (new DecimalFormat("$#,##0.00")).format(Double.valueOf(estimadoAnualDeEmbarques).doubleValue()));
					} else
						getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", NO_DISPONIBLE);
					
					//Cuota
					datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL15_BienesAseguradosPorIncisoModPronostico.ID_RIESGO_TRANSITO);
					datoIncisoCotizacionDTO.getId().setIdTcRamo(PL15_BienesAseguradosPorIncisoModPronostico.ID_RAMO_MARITIMO_TRANSPORTES);
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL15_BienesAseguradosPorIncisoModPronostico.ID_SUBRAMO_TRANSPORTES_CARGA);
					datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(170d));
					try {
						descripcionDatoInciso = null;
						descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
					} catch (SystemException e1) { }
					if (descripcionDatoInciso!=null){
						BigDecimal cuota = new BigDecimal(descripcionDatoInciso[1]);
						getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", String.format("%.2f", cuota.doubleValue())+" %");
					}
					else
						getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", NO_DISPONIBLE);

					//L�mite m�ximo por embarque
				}//Fin validar numeroMesesEnPeriodo != null
				else{
					getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", NO_DISPONIBLE);
				}
			}//Fin validar DatoIncisoCotTMP != null
			else{
				getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", NO_DISPONIBLE);
				getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", NO_DISPONIBLE);
			}
			/**
			 * 26/01/2010. Jos� Luis Arellano. Por especificaci�n de Mario:
			 * El valor del campo "Prima neta m�nima y de dep�sito" debe ser el mismo que se muestra en "prima neta" en la cuadr�cula de totales.
			 */
			//Prima neta m�nima y de dep�sito
			getParametrosVariablesReporte().put("DESCRIPCION_PRIMA_NETA_MINIMA_DEPOSITO", getParametrosVariablesReporte().get("SEMI_TOTAL_PRIMA_NETA"));
			
			//L�mite m�ximo por embarque
			Double valorEmbarque = reporteBase.obtenerSumatoriaSACoberturasBasicasPorInciso(incisoCotizacionDTO.getId().getNumeroInciso());
			getParametrosVariablesReporte().put("DESCRIPCION_LIMITE_POR_EMBARQUE", Sistema.FORMATO_MONEDA.format(valorEmbarque));
			
			//Se agrega un objeto para que la plantilla aparezca.
			getListaRegistrosContenido().add(new Object());
			
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}//Fin validar cotizacion != null && incisoCot != null
		else	setByteArrayReport( null);
	}
	
	protected void procesarDatosModalidadAnual(String claveUsuario) {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			
			String descripcionDatoInciso[] = null;
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO;
			
			//Obtener n�mero de meses en el periodo de declaraci�n
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL16_BienesAseguradosPorIncisoModAnual.ID_RIESGO_TRANSITO);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(PL16_BienesAseguradosPorIncisoModAnual.ID_RAMO_MARITIMO_TRANSPORTES);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL16_BienesAseguradosPorIncisoModAnual.ID_SUBRAMO_TRANSPORTES_CARGA);
			datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(146d));
			DatoIncisoCotizacionDTO datoIncisoCotTMP = null;
			try {
				datoIncisoCotTMP = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			} catch (SystemException e1) { }
			
			if (datoIncisoCotTMP != null){
				NumberFormat fMonto = new DecimalFormat("0.00%");
				Double numeroMesesEnPeriodo = null;
				try{
					numeroMesesEnPeriodo = Double.valueOf(datoIncisoCotTMP.getValor()).doubleValue();
				} catch(Exception e){}
				if (numeroMesesEnPeriodo != null && numeroMesesEnPeriodo > 0){
					//Estimado anual de embarques
					String estimadoAnualDeEmbarques = null;
					datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL16_BienesAseguradosPorIncisoModAnual.ID_RIESGO_TRANSITO);
					datoIncisoCotizacionDTO.getId().setIdTcRamo(PL16_BienesAseguradosPorIncisoModAnual.ID_RAMO_MARITIMO_TRANSPORTES);
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL16_BienesAseguradosPorIncisoModAnual.ID_SUBRAMO_TRANSPORTES_CARGA);
					datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(142d));
					try {
						descripcionDatoInciso = null;
						descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
					} catch (SystemException e1) { }
					if (descripcionDatoInciso!=null){
						estimadoAnualDeEmbarques = descripcionDatoInciso[1];
						getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", (new DecimalFormat("$#,##0.00")).format(Double.valueOf(estimadoAnualDeEmbarques).doubleValue()));
					} else
						getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", NO_DISPONIBLE);
					//L�mite m�ximo por embarque (no se muestra en esta plantilla, pero se necesita para calcular la cuota)
					//Cuota
					datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL16_BienesAseguradosPorIncisoModAnual.ID_RIESGO_TRANSITO);
					datoIncisoCotizacionDTO.getId().setIdTcRamo(PL16_BienesAseguradosPorIncisoModAnual.ID_RAMO_MARITIMO_TRANSPORTES);
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL16_BienesAseguradosPorIncisoModAnual.ID_SUBRAMO_TRANSPORTES_CARGA);
					datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(170d));
					try {
						descripcionDatoInciso = null;
						descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
					} catch (SystemException e1) { }
					if (descripcionDatoInciso!=null){
						BigDecimal cuota = new BigDecimal(descripcionDatoInciso[1]);
						getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", String.format("%.2f", cuota.doubleValue())+" %");
					}
					else
						getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", NO_DISPONIBLE);
					
					/**
					 * 26/01/2010. Jos� Luis Arellano. Por especificaci�n de Mario:
					 * El valor del campo "Prima neta m�nima y de dep�sito" debe ser el mismo que se muestra en "prima neta" en la cuadr�cula de totales.
					 */
					fMonto = new DecimalFormat("$#,##0.00");
					getParametrosVariablesReporte().put("DESCRIPCION_PRIMA_NETA_MINIMA_DEPOSITO", getParametrosVariablesReporte().get("SEMI_TOTAL_PRIMA_NETA"));
					//Prima neta m�nima por declaraci�n
					datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(),incisoCotizacionDTO.getId().getNumeroInciso());
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(PL16_BienesAseguradosPorIncisoModAnual.ID_RIESGO_TRANSITO);
					datoIncisoCotizacionDTO.getId().setIdTcRamo(PL16_BienesAseguradosPorIncisoModAnual.ID_RAMO_MARITIMO_TRANSPORTES);
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(PL16_BienesAseguradosPorIncisoModAnual.ID_SUBRAMO_TRANSPORTES_CARGA);
					datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(144d));
					try {
						descripcionDatoInciso = null;
						descripcionDatoInciso = ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO,null);
					} catch (SystemException e1) { }
					if (descripcionDatoInciso!=null){
						fMonto = new DecimalFormat("$#,##0.00");
						Double primaMinimaDeclaracion = null;
						try{
							primaMinimaDeclaracion = Double.valueOf(descripcionDatoInciso[1]).doubleValue();
						} catch(Exception e){}
						if (primaMinimaDeclaracion != null)
							getParametrosVariablesReporte().put("DESCRIPCION_PRIMA_NETA_MINIMA_DECLARACION", fMonto.format(primaMinimaDeclaracion));
					} else
						getParametrosVariablesReporte().put("DESCRIPCION_PRIMA_NETA_MINIMA_DECLARACION", NO_DISPONIBLE);
				}//Fin validar numeroMesesEnPeriodo != null
				else{
					getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", NO_DISPONIBLE);
					getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", NO_DISPONIBLE);
					getParametrosVariablesReporte().put("DESCRIPCION_PRIMA_NETA_MINIMA_DECLARACION", NO_DISPONIBLE);
				}
			}//Fin validar DatoIncisoCotTMP != null
			else{
				getParametrosVariablesReporte().put("DESCRIPCION_EMBARQUES_ANUAL", NO_DISPONIBLE);
				getParametrosVariablesReporte().put("DESCRIPCION_CUOTA", NO_DISPONIBLE);
				getParametrosVariablesReporte().put("DESCRIPCION_PRIMA_NETA_MINIMA_DECLARACION", NO_DISPONIBLE);
			}
		
			Double valorEmbarque = reporteBase.obtenerSumatoriaSACoberturasBasicasPorInciso(incisoCotizacionDTO.getId().getNumeroInciso());
			getParametrosVariablesReporte().put("DESCRIPCION_LIMITE_POR_EMBARQUE", Sistema.FORMATO_MONEDA.format(valorEmbarque));
			//Se agrega un objeto para que la plantilla aparezca.
			getListaRegistrosContenido().add(new Object());
			
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}//Fin validar cotizacion != null && incisoCot != null
		else	setByteArrayReport( null);
	}
}
