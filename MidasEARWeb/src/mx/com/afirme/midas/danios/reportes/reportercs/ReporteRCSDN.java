package mx.com.afirme.midas.danios.reportes.reportercs;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;
import mx.com.afirme.midas.sistema.SystemException;
import java.util.Calendar;
public class ReporteRCSDN { 

	private static final ReporteRCSDN INSTANCIA = new ReporteRCSDN();
	
	public static ReporteRCSDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReporteRCSDTO> obtieneRCS(Calendar fechaInicio, Date fechaFin, Integer idRamo,
			Integer numeroCortes, String cveNegocio)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		
		return reporteBasesEmisionSN.obtieneRCS(reporteBasesEmisionFiltro, cveNegocio);
	}
	
	public List<ReporteRCSDTO> obtieneRCSVidaPas(Calendar fechaInicio, Date fechaFin, Integer idRamo,
			Integer numeroCortes)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		
		return reporteBasesEmisionSN.obtieneRCSVidaPas(reporteBasesEmisionFiltro);
	}
	
	public List<ReporteRCSDTO> obtieneDetalleRCS(Calendar fechaInicio, Date fechaFin, Integer idRamo,
			Integer numeroCortes)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		
		return reporteBasesEmisionSN.obtieneDetalleRCS(reporteBasesEmisionFiltro);
	}
	
	public List<ReporteRCSDTO> obtieneDetalleVigorLP(Calendar fechaInicio, Date fechaFin, Integer idRamo,
			Integer numeroCortes)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		
		return reporteBasesEmisionSN.obtieneDetalleVigorLP(reporteBasesEmisionFiltro);
	}
	
	public List<ReporteRCSDTO> obtenerReporteRangos()  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		Calendar cal = Calendar.getInstance();
		reporteBasesEmisionFiltro.setFechaInicio(cal);
		reporteBasesEmisionFiltro.setFechaFin(new Date());
		reporteBasesEmisionFiltro.setIdRamo(0);
		reporteBasesEmisionFiltro.setNumeroCortes(0);
		reporteBasesEmisionFiltro.setCveNegocio(0);
		
		return reporteBasesEmisionSN.obtenerReporteRangos(reporteBasesEmisionFiltro);
	}
	
	public String obtieneNomArchivo(String tipoArchivo)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		
		return reporteBasesEmisionSN.obtieneNomArchivo(tipoArchivo);
	}
	
	public String obtenerNumRangosDuracion(int tipoRango)  
		throws SystemException {
	ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
	
	return reporteBasesEmisionSN.obtenerNumRangosDuracion(tipoRango);
	}
	
	public boolean actualizaRangosSP(Calendar fechaInicio, Date fechaFin, Integer idRamo, Integer cveNegocio,
			Integer numeroCortes)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		reporteBasesEmisionFiltro.setCveNegocio(cveNegocio);
		
		return reporteBasesEmisionSN.actualizaRangosSP(reporteBasesEmisionFiltro);
	}
	
	public boolean eliminaRangosSP(Calendar fechaInicio, Date fechaFin, Integer idRamo, Integer cveNegocio,
			Integer numeroCortes)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		reporteBasesEmisionFiltro.setCveNegocio(cveNegocio);
		
		return reporteBasesEmisionSN.eliminaRangosSP(reporteBasesEmisionFiltro);
	}
	
	public boolean setRangosDuracioSP(Integer duracionPromedio, Integer duracionRemanente)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		
		
		return reporteBasesEmisionSN.setRangosDuracionSP(duracionPromedio, duracionRemanente);
	}
	
	public boolean actualizaCalificacionesSP(int dia,int fechaInicio, int anio, Integer cveNegocio)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setNumeroCortes(fechaInicio);
		reporteBasesEmisionFiltro.setCveNegocio(cveNegocio);
		
		return reporteBasesEmisionSN.actualizaCalificacionesSP(dia,reporteBasesEmisionFiltro,anio);
	}
	
	public List<ReporteRCSDTO> obtieneREASRCS(Calendar fechaInicio, Date fechaFin, Integer idRamo,
			Integer numeroCortes, int tipoReporte)  
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setIdRamo(idRamo);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		 
		return reporteBasesEmisionSN.obtieneREASRCS(reporteBasesEmisionFiltro, tipoReporte);
	}
	
	public List<ReporteRCSDTO> obtieneValidacionesRCS(Calendar fechaInicio, Date fechaFin, Integer cveNegocio,
			Integer numeroCortes)  
			
			throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		ReporteRCSDTO reporteBasesEmisionFiltro = new ReporteRCSDTO();
		reporteBasesEmisionFiltro.setFechaInicio(fechaInicio);
		reporteBasesEmisionFiltro.setFechaFin(fechaFin);
		reporteBasesEmisionFiltro.setCveNegocio(cveNegocio);
		reporteBasesEmisionFiltro.setNumeroCortes(numeroCortes);
		
		return reporteBasesEmisionSN.obtieneValidacionesRCS(reporteBasesEmisionFiltro);
	}
	
	public List<ReporteRCSDTO> obtieneClaveRamo(String idRamo) throws SystemException
	{
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		return reporteBasesEmisionSN.obtieneClaveRamo(idRamo);
	}

	public void actualizaNomSP(String tipoArchivo, String nomenclatura)throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		reporteBasesEmisionSN.actualizaNomSP(tipoArchivo, nomenclatura);
		
	}

	public int generaInfoRCS(String anio, String mes, String dia, String negocio, String reproceso) throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		return reporteBasesEmisionSN.generaInfoRCS(anio, mes, dia, negocio, reproceso);
	}

	public List<SolvenciaLogDTO> getLogSolvencia(Date fechaCorte, String negocio) throws SystemException {
		ReporteRCSSN reporteBasesEmisionSN = new ReporteRCSSN();
		return reporteBasesEmisionSN.getLogSolvencia(fechaCorte, negocio);
	}
	
}