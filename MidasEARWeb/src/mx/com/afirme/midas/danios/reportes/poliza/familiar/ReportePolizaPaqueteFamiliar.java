package mx.com.afirme.midas.danios.reportes.poliza.familiar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL14_PolizaTextosAdicionales;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL1_PolizaSumasAseguradas1erRiesgoLUC;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL2_PolizaDocumentosAdicionales;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL3_PolizaDetalleInciso;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.PL6_PolizaUnicaUbicacion;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReportePolizaPaqueteFamiliar extends ReportePolizaBase{
	private static final String leyendaPiePagina = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.danios.reportes.paqueteFamiliar.leyendaPiePagina");
	
	public ReportePolizaPaqueteFamiliar(BigDecimal idToPoliza) throws SystemException {
		setListaPlantillas(new ArrayList<byte[]>());
		polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		cotizacionDTO = polizaDTO.getCotizacionDTO();
		mapaSeccionesContratadasPorNumeroInciso = null;
	}
	
	public ReportePolizaPaqueteFamiliar(PolizaDTO polizaDTO) {
		setListaPlantillas(new ArrayList<byte[]>());
		this.polizaDTO = polizaDTO;
		this.cotizacionDTO = polizaDTO.getCotizacionDTO();
		mapaSeccionesContratadasPorNumeroInciso = null;
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePaqueteEmpresarial(polizaDTO, claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReportePaqueteEmpresarial(PolizaDTO polizaDTO,String nombreUsuario){
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			CotizacionDTO cotizacionDTO = polizaDTO.getCotizacionDTO();
			listaSeccionesContratadasCotizacion = SeccionCotizacionDN.getInstancia().listarSeccionesContratadasPorCotizacion(cotizacionDTO.getIdToCotizacion(),(short)1);
			listaCoberturasContratadasCotizacion = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadasParaReporte(cotizacionDTO.getIdToCotizacion());
			listaRiesgosContratadosCotizacion = RiesgoCotizacionDN.getInstancia().listarRiesgosContratadosPorCotizacion(cotizacionDTO.getIdToCotizacion());
			listaSubIncisosCotizacion = SubIncisoCotizacionDN.getInstancia().listarSubIncisosPorCotizacion(cotizacionDTO.getIdToCotizacion());
			listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(cotizacionDTO.getIdToCotizacion());
			
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(polizaDTO,nombreUsuario,listaIncisos,null);
			String abreviaturas,descripcionAbreviaturas;
			AgrupacionCotDTO agrupacionCot = PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(), (short)1);
			//plantilla Unica ubicacion
			if (listaIncisos.size() == 1 && agrupacionCot == null){
				abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\n\nLUC\n\nCOBERTURAS\n NORMALES:";
				descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\nCon m�ximo de:\n" +
					"D�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\nLa suma asegurada " +
					"para esta cobertura es el valor establecido en la cobertura b�sica del Bien o Secci�n contratado, salvo los subl�mites establecidos\nL�mite �nico y Combinado  " +
					"(L�mite m�ximo de responsabilidad para la Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o " +
					"todas las ubicaciones aseguradas, durante la vigencia del seguro)\n\nIncendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
				generarReporteUnicaUbicacion(cotizacionDTO, listaIncisos,nombreUsuario);
			}
			else{
				//Si aplic� primer riesgo
				if (agrupacionCot != null){
					abreviaturas = "S.A.\nS/S.A.\nR.C.\nS/VR Eq. Da�.\nS/P\nMIN\nMAX\nDSMGVDFMS\nUMA\nAMPARADO:\nLUC\n\nCOBERTURAS\n NORMALES:";
					descripcionAbreviaturas = "Suma Asegurada\nSobre Suma Asegurada\nResponsabilidad Civil\nSobre Valor de Reposici�n del Equipo Da�ado\nSobre p�rdida\nCon m�nimo de:\n" +
							"Con m�ximo de:\nD�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nComplementa la " +
							"protecci�n del bien especificado en la Secci�n.\nL�mite �nico y Combinado  (L�mite m�ximo de responsabilidad para la " +
							"Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o todas las ubicaciones aseguradas, " +
							"durante la vigencia del seguro)\n\nComprende las coberturas de Incendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				}
				else{
					abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\nLUC\n\nCOBERTURAS\n NORMALES:";
					descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\nCon m�ximo de:\n" +
							"D�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\nLa suma asegurada " +
							"para esta cobertura es el valor establecido en la cobertura b�sica del Bien o Secci�n contratado, salvo los subl�mites establecidos\nL�mite �nico y Combinado  " +
							"(L�mite m�ximo de responsabilidad para la Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o " +
							"todas las ubicaciones aseguradas, durante la vigencia del seguro)\n\nIncendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				}
				getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
				generarReporteMultiplesUbicaciones(cotizacionDTO, listaIncisos, nombreUsuario);
			}
		} catch (SystemException e) {}
	}
	
	private void generarReporteMultiplesUbicaciones(CotizacionDTO cotizacionDTO,List<IncisoCotizacionDTO> listaIncisos,String nombreUsuario){
		byte[] reporteTMP = null;
		PlantillaCotizacionBase plantillaPrimerRiesgo = new PL1_PolizaSumasAseguradas1erRiesgoLUC(cotizacionDTO.getIdToCotizacion(),getMapaParametrosGeneralesPlantillas());
		/**
		 * 28/06/2010. Se agreg� leyenda en el pide de pagina del articulo 36 para poliza paquete familiar (no se invluy� en impresi�n de cotizaci�n)
		 */
		String leyendaPiePagina = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.danios.reportes.paqueteFamiliar.leyendaPiePagina");
		plantillaPrimerRiesgo.setMostrarLeyendaPiePagina(true);
		plantillaPrimerRiesgo.setLeyendaPiePagina(leyendaPiePagina);
		try {
			reporteTMP = plantillaPrimerRiesgo.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		
		PlantillaCotizacionBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		/**
		 * 28/06/2010. Se agreg� leyenda en el pide de pagina del articulo 36 para poliza paquete familiar (no se invluy� en impresi�n de cotizaci�n)
		 */
		plantillaDocAnexos.setMostrarLeyendaPiePagina(true);
		plantillaDocAnexos.setLeyendaPiePagina(leyendaPiePagina);
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaDocAnexos = null;
		
		PlantillaCotizacionBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		/**
		 * 28/06/2010. Se agreg� leyenda en el pide de pagina del articulo 36 para poliza paquete familiar (no se invluy� en impresi�n de cotizaci�n)
		 */
		plantillaTextosAdicionales.setMostrarLeyendaPiePagina(true);
		plantillaTextosAdicionales.setLeyendaPiePagina(leyendaPiePagina);
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaTextosAdicionales = null;
		for(IncisoCotizacionDTO incisoCot : listaIncisos){
			MidasPlantillaBase plantillaDetalleInciso = new PL3_PolizaDetalleInciso(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantillaDetalleInciso.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			/**
			 * 19/01/2010. Jos� Luis Arellano. La impresi�n de paquete familiar no debe mostrar la plantilla de subincisos por inciso 
			 */
			//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
//			List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
//			for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
//				MidasPlantillaBase plantilla4 = new PL4_PolizaDetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
//				try {
//					reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
//				} catch (SystemException e) {}
//				if (reporteTMP !=null){
//					getListaPlantillas().add(reporteTMP);
//					reporteTMP = null;
//				}
//			}
		}
	}
	
	private void generarReporteUnicaUbicacion(CotizacionDTO cotizacionDTO,List<IncisoCotizacionDTO> listaIncisos,String nombreUsuario){
		byte[] reporteTMP = null;
		IncisoCotizacionDTO incisoCot = listaIncisos.get(0);
		PL6_PolizaUnicaUbicacion plantillaUnicaUbicacion = new PL6_PolizaUnicaUbicacion(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
		/**
		 * 28/06/2010. Se agreg� leyenda en el pide de pagina del articulo 36 para poliza paquete familiar (no se invluy� en impresi�n de cotizaci�n)
		 */
		plantillaUnicaUbicacion.setMostrarLeyendaPiePagina(true);
		plantillaUnicaUbicacion.setLeyendaPiePagina(leyendaPiePagina);
		try {
			plantillaUnicaUbicacion.setMostrarLeyendaSANivelCobertura(Boolean.FALSE);
			reporteTMP = plantillaUnicaUbicacion.obtenerReporte(nombreUsuario);
		} catch (SystemException e) {}
		if (reporteTMP !=null)
			getListaPlantillas().add(reporteTMP);
		reporteTMP = null;
		/**
		 * 19/01/2010. Jos� Luis Arellano. La impresi�n de paquete familiar no debe mostrar la plantilla de subincisos por inciso 
		 */
		//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
//		List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
//		for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
//			MidasPlantillaBase plantilla4 = new PL4_PolizaDetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
//			try {
//				reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
//			} catch (SystemException e) {}
//			if (reporteTMP !=null){
//				getListaPlantillas().add(reporteTMP);
//				reporteTMP = null;
//			}
//		}
		PlantillaCotizacionBase plantillaDocAnexos = new PL2_PolizaDocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		/**
		 * 28/06/2010. Se agreg� leyenda en el pide de pagina del articulo 36 para poliza paquete familiar (no se invluy� en impresi�n de cotizaci�n)
		 */
		plantillaDocAnexos.setMostrarLeyendaPiePagina(true);
		plantillaDocAnexos.setLeyendaPiePagina(leyendaPiePagina);
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (SystemException e) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		PlantillaCotizacionBase plantillaTextosAdicionales = new PL14_PolizaTextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		/**
		 * 28/06/2010. Se agreg� leyenda en el pide de pagina del articulo 36 para poliza paquete familiar (no se invluy� en impresi�n de cotizaci�n)
		 */
		plantillaTextosAdicionales.setMostrarLeyendaPiePagina(true);
		plantillaTextosAdicionales.setLeyendaPiePagina(leyendaPiePagina);
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (SystemException e) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
	}
	
}
