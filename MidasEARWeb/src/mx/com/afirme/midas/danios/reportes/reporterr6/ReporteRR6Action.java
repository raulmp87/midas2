package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

@SuppressWarnings("deprecation")
public class ReporteRR6Action extends MidasMappingDispatchAction{
	public static final int BUFFERMAX = 1024;
	public static final int BUFFERMIN = 15;
	public static final String EXITOSO = "exitoso";
	public static final String ERROR = "errorImpresion";
	public static final String USUARIO_ACCESO_MIDAS = "usuarioAccesoMIDAS";
	public static final String RIESGO_CXL = "CXL";
	public static final String RIESGO_IMPREC = "ImpRec";
	public static final String EXTENSION = ".txt";
	public static final int RTRE = 1;
	public static final int RTRC = 2;	
	public static final int RTRF = 3;
	public static final int RTRR = 4;
	public static final int RTRS = 5;
	public static final int RARN = 6;
	public static final int RTRI = 7;
	public static final int CUMF = 8;
	public static final String INICIAR = "0";
	public static final int PROCESAR = 1;
	public static final int REPROCESAR = 2;
	public static final String TIPO_ZIP = "application/zip";
		
	private FileManagerService fileManagerService;
	
	public ActionForward mostrarReporteRR6(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		
		return mapping.findForward(EXITOSO);
	}
	
	/**
	 * Genera informacion de los reportes RR6.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */	
	public ActionForward generarReporteRR6(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		String forward = EXITOSO;
		StringBuffer resultado = new StringBuffer();
		int reporte = Integer.parseInt(request.getParameter("reporte"));
		String fechaIni = request.getParameter("fechaInicio");
		String fechaFin = request.getParameter("fechaFinal");
		BigDecimal tipoCambio = new BigDecimal(request.getParameter("tipoCambio"));
		
				
		String fileName = getNombreArchivo(fechaFin, reporte);
		int registros = 0;
			
		try {
			
			String nombreUsuario = obtieneNombreUsuario(request); 
			List<ReporteRR6DTO> lista = ReporteRR6DN.getInstancia().obtenerReporte(fechaIni, fechaFin, tipoCambio, reporte, nombreUsuario);
			
			if(lista == null)
				throw new SystemException ("Ocurrio un error al recuperar la informacion para el reporte.");
			else if(lista.isEmpty())
			{
				ReporteRR6DTO rr6dto = new ReporteRR6DTO();
				rr6dto.setRegistro("No se encontraron registros para los datos introducidos");
				lista.add(rr6dto);
			}
			
			for(ReporteRR6DTO item:lista){
				if(registros != lista.size()-1){
			        resultado.append(item.getRegistro()+"\r\n");
				}else{
					resultado.append(item.getRegistro());
				}
				registros++;
			}
			byte[] file = resultado.toString().getBytes();
			
			//Si el reporte es diferente al RARN se genera el archivo en formato TXT.
			if(reporte != 6){
				OutputStream output = response.getOutputStream();
				
				response.setContentType("application/text");
				response.setHeader("Content-Disposition","attachment;filename="+fileName);
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(file.length);
				response.setBufferSize(BUFFERMAX * BUFFERMIN);
				output.write(file);
				output.flush();
				output.close();				
			}else{
				//Si el reporte es RARN se genera el archivo en formato ZIP.
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] excelFile = obtenerExcel(fechaFin).toByteArray();
				ZipOutputStream zos = new ZipOutputStream(baos);
				
				ZipEntry entryTXT = new ZipEntry(fileName +".txt");
				entryTXT.setSize(file.length);
				zos.putNextEntry(entryTXT);
				zos.write(file);
				zos.closeEntry();
				
				ZipEntry entryEXCEL = new ZipEntry("SA - EIQ vs RR6" +".xls");
				entryEXCEL.setSize(excelFile.length);
				zos.putNextEntry(entryEXCEL);
				zos.write(excelFile);
				zos.closeEntry();
				
				zos.close();
				this.writeBytes(response, baos.toByteArray(), TIPO_ZIP,fileName+ ".zip");
				baos.close();
				
			}					
			
		
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				forward = ERROR;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				forward = ERROR;
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				forward = ERROR;
			}
		   
	    return mapping.findForward(forward);
	}
	
	
	/**
	 * Procesa informacion de los reportes RR6, Se crea Tread, debido a que 
	 * esos reportes tardan mucho tiempo en ser generados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward procesarReporteRR6(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		Date fechaInicial = null;
		Date fechaFinal = null;
		int reporte = Integer.parseInt(request.getParameter("reporte"));
		
		String fechaIni = request.getParameter("fechaInicio");
		String fechaFin = request.getParameter("fechaFinal");
		BigDecimal tipoCambio = new BigDecimal(request.getParameter("tipoCambio"));
		int accion = Integer.parseInt(request.getParameter("accion"));
		int accionSA = Integer.parseInt(request.getParameter("accionSA"));
		String forward = "success";		
		String fileName = getNombreArchivo(fechaFin, reporte);
		
		try {
			fechaInicial = UtileriasWeb.getFechaFromString(fechaIni);
			fechaFinal = UtileriasWeb.getFechaFromString(fechaFin);
		} catch (ParseException ex) {
			LogDeMidasWeb.log("Excepci&oacute;n obteniendo fecha del Reporte RR6: " + ex.getMessage(), Level.ALL, ex);			
		}
		
		try {
			
			String nombreUsuario = obtieneNombreUsuario(request); 
			int registros = ReporteRR6DN.getInstancia().obtenerRegistros(fechaInicial, fechaFinal, tipoCambio, reporte, nombreUsuario);
			
			if(reporte == RARN){				
				if(accionSA == PROCESAR){
					int registrosSA = ReporteRR6DN.getInstancia().obtenerRegistrosSA(fechaFinal);
					if(registrosSA == 0){					
						try {
							response.getWriter().write("SIN_DATOS");
							return mapping.findForward(forward);
						}catch (IOException e) {	
					  		 throw new SystemException ("Ocurri&oacute; un error al recuperar la informaci&oacute;n del reporte. "+reporte+" con fecha de corte: "+fechaFin+" ... ", e);
					  	}
					}
				}
			}
			
			if(registros == 0){
				// Ejecuta el thread para procesar la informacion del reporte.				
				ejecutarThread(fechaIni, fechaFin, tipoCambio, reporte, nombreUsuario);
				try {
					response.getWriter().write(INICIAR);
					
			  		}catch (IOException e) {	
			  		 throw new SystemException ("Ocurri&oacute; un error al recuperar la informaci&oacute;n del reporte. "+reporte+" con fecha de corte: "+fechaFin+" ... ", e);
			  		}
			}else if(registros > 0){			
			    if(accion == PROCESAR){
					try {
						response.getWriter().write("Ya existe Informaci&oacute;n del Corte: "+ fechaFin+ " para el reporte "+ reporte+"\n");
						
				  		}catch (IOException e) {	
				  		 throw new SystemException ("Ocurri&oacute; un error al recuperar la informaci&oacute;n del reporte. "+reporte+" con fecha de corte: "+fechaFin+" ... ", e);
				  		}
				}else if (accion == REPROCESAR){
					// Ejecuta el thread para reprocesar la informacion del reporte.
					ejecutarThread(fechaIni, fechaFin, tipoCambio, reporte, nombreUsuario);
					forward = EXITOSO;
				}
					
			  }
		   }catch (SystemException e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				mapping.findForward(ERROR);				
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				mapping.findForward(ERROR);
			}
				
		return mapping.findForward(forward);
	}
	
	/**
	 * Lista los reportes cargados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			
			String fechaCorte = request.getParameter("fechaFinal");
			
			List<ReporteRR6DTO> contratos = new ArrayList<ReporteRR6DTO>();
			ReporteRR6DN rporteRR6DN = ReporteRR6DN.getInstancia();
						
			contratos = rporteRR6DN.listarTodos(fechaCorte);
					
			request.setAttribute("documentos", contratos);
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		} catch (ExcepcionDeAccesoADatos e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}	
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Elimina registro por el <code>id</code> del reporte
	 * @param mapping
	 * @param form
	 * @param request
	 * @param responsereporte/borrarDocumento
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			String id = request.getParameter("id");
			
			ReporteRR6DN reporteRR6DN = ReporteRR6DN.getInstancia();
			ReporteRR6DTO reporteDTO = reporteRR6DN.getPorId(Integer.valueOf(id));
			
			reporteRR6DN.borrar(reporteDTO);
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error al borrar el registro ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Error al borrar el registro ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Lista los estatus de los reportes cargados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param responsereporte
	 * @return mapping.findForward(reglaNavegacion).
	 */
	
	public ActionForward listarCargas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			
			String fechaCorte = request.getParameter("fechaFinal");
			
			List<CargaRR6DTO> contratos = new ArrayList<CargaRR6DTO>();
			ReporteRR6DN rporteRR6DN = ReporteRR6DN.getInstancia();
						
			contratos = rporteRR6DN.listarCargas(fechaCorte);
					
			request.setAttribute("cargas", contratos);
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		} catch (ExcepcionDeAccesoADatos e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}	
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Descarga los reportes cargados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward descargarReporte(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = EXITOSO;
		
		try {
			fileManagerService = ServiceLocator.getInstance().getEJB(
					FileManagerService.class);

		} catch (Exception e) {
			LogDeMidasWeb.log("Error en reporte RR6 Action: ",Level.SEVERE, e);
			reglaNavegacion = ERROR;
		}	

		try {

			final String idControlArchivo = request.getParameter("idControlArchivo");
			final ControlArchivoDTO controlArchivoDTO = ControlArchivoDN
					.getInstancia().getPorId(new BigDecimal(idControlArchivo));
			final String fileName = fileManagerService.getFileName(controlArchivoDTO);
			byte[] byteArray = fileManagerService.downloadFile(fileName, idControlArchivo);
			response.setHeader("Content-Disposition", "attachment; filename="
					+ URLEncoder.encode(controlArchivoDTO.getNombreArchivoOriginal(), "ISO-8859-1"));
			response.setHeader("Cache-Control", "no-cache");
			response.setContentType("application/vnd.ms-excel");
			response.setContentLength(byteArray.length);
			response.setBufferSize(BUFFERMAX * BUFFERMIN);
			
			OutputStream output = response.getOutputStream(); 
			output.write(byteArray);
			output.flush();
			output.close();
			
			
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al descargar el archivo.", Level.SEVERE, e);
			reglaNavegacion = ERROR;
		} 
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Actualiza estatus de carga de los reportes cargados - Bloquea o Desbloquea carga.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward actualizarEstatusCarga(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		    String reglaNavegacion = EXITOSO;
		try {
			int id = Integer.valueOf(request.getParameter("id"));
			int estatusCarga = Integer.valueOf(request.getParameter("estatusCarga"));
			
			ReporteRR6DN reporteRR6DN = ReporteRR6DN.getInstancia();
			
			reporteRR6DN.actualizarEstatusCarga(id, estatusCarga);
			return this.listarCargas(mapping, form, request, response);
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error al actualizar el estatus de carga ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede actualizar la carga del.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Error al actualizar el estatus de carga ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede actualizar la carga del reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Ejecuta el Thread encargado de ejecutar el Stored de cada reporte.
	 * @param fechaIni
	 * @param fechaFin
	 * @param tipoCambio
	 * @param reporte
	 * @param nombreUsuario
	 */
	public void ejecutarThread(String fechaIni, String fechaFin, BigDecimal tipoCambio, int reporte, String nombreUsuario) {
		try {
			ReporteRR6Generador reporteRR6 = new ReporteRR6Generador();
			reporteRR6.setFechaIni(fechaIni);
			reporteRR6.setFechaFin(fechaFin);
			reporteRR6.setTipoCambio(tipoCambio);
			reporteRR6.setReporte(reporte);
			reporteRR6.setNombreUsuario(nombreUsuario);
			
			Thread hiloGenerador = new Thread(reporteRR6);
			hiloGenerador.start();	
		}
		catch (Exception ex){
			LogDeMidasWeb.log("Excepci&oacute;n en Proceso Generador del Reporte RR6: "
					+ ex.getMessage(), Level.ALL, ex);
		}
	}
	
	
	/**
	 * Obtiene el nombre del usuario de Midas registrado
	 * 
	 * @param request
	 *            Request del servlet
	 * @return El nombre del usuario de Midas registrado
	 */
	public static String obtieneNombreUsuario(HttpServletRequest request) {

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, USUARIO_ACCESO_MIDAS);

		if (usuario != null) {
			return usuario.getNombreUsuario();
		}

		return "anonimo";
	}
	/**
	 * Obtiene el nombre del archivo a ser generado
	 * 
	 * @param texto
	 *            Estructura vigente para el nombre del archivo
	 * @param fecha
	 *            Fecha de Corte
	 * @param tipoArchivo
	 *            El tipo de archivo a ser generardo
	 * @return El nombre del archivo
	 */
	public String getNombreArchivo(String fecha, int tipoArchivo) {
		String fechaModificada = fecha;
		String dia = fechaModificada.substring(0,2);
		String mes = fechaModificada.substring(3,5);
		String anio = fechaModificada.substring(6,10);
		String fechaCorte = anio+mes+dia;
		String producto = "RR6TRIME";
		String productoRTRI = "RR6TRIMD";
		String nomenclatura="";
				
		if(tipoArchivo == RTRE){
			nomenclatura = producto+"RTRES0094"+fechaCorte;
		}else if(tipoArchivo == RTRC){
			nomenclatura = producto+"RTRCS0094"+fechaCorte;
		}else if(tipoArchivo == RTRF){
			nomenclatura = producto+"RTRFS0094"+fechaCorte;
		}else if(tipoArchivo == RTRR){
			nomenclatura = producto+"RTRRS0094"+fechaCorte;
		}else if(tipoArchivo == RTRS){
			nomenclatura = producto+"RTRSS0094"+fechaCorte;
		}else if(tipoArchivo == RARN){
			nomenclatura = producto+"RARNS0094"+fechaCorte;
		}else if(tipoArchivo == RTRI){
			nomenclatura = productoRTRI+"RTRIS0094"+fechaCorte;
		}else if(tipoArchivo == CUMF){
			nomenclatura = producto+"CUMFS0094"+fechaCorte;
		}
		
		return nomenclatura;
	}
	
	/**
	 * Genera el reporte en formato excel.
	 * @param fechaFin
	 * @return ByteArrayOutputStream
	 */	
	public ByteArrayOutputStream  obtenerExcel(String fechaFin) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String[] tituloCabecera = {"CLAVE RAMO","RAMO","ARCHIVOS (EIQ)","","ARCHIVOS (MIDAS)","","DIFERENCIAS"};
		Date fechaFinal = null;
		List<Object[]> lista = null;
		try {
				fechaFinal = UtileriasWeb.getFechaFromString(fechaFin);					
				lista = ReporteRR6DN.getInstancia().obtenerReporteExcel(fechaFinal);				
			
			} catch (ParseException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n al convertir la fecha del Reporte RR6: " + ex.getMessage(), Level.ALL, ex);			
			} catch (SystemException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n obteniendo fecha del Reporte RR6: " + ex.getMessage(), Level.ALL, ex);			
			}
			
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet hoja = workbook.createSheet("REPORTERR6 - RARN");  
            HSSFRow cabecera = hoja.createRow((short)0);
	        	        
            Cell cell = null;
            HSSFRow hssfRow = null;
           
            setCobinarCeldas(workbook, cabecera, hoja, cell, tituloCabecera);
            
            setSegundaCabecera(workbook, hoja, cell);
        	
        	int rowIdx = 2;
        	int ramo = 0;
        	double SA = 0.0;
        	double SA_RET = 0.0;
	        
        	 for(Object[] reporte : lista){
 	        	hssfRow = hoja.createRow((short)rowIdx++);
 	        	
 	        	for (int celdas = 0; celdas < reporte.length; celdas++) 
   			    {
 	        		ramo = Integer.valueOf(reporte[0].toString());
 	        		cell = hssfRow.createCell(celdas);
 	        		if(celdas == 1){ 	        		
 		        		cell.setCellValue(reporte[celdas].toString());
 	        		}else{
 	        			cell.setCellValue(Double.valueOf(reporte[celdas].toString()));
 	        			if(ramo == 110){
 	        				if(celdas == 2){
 	        				SA += Double.valueOf(reporte[2].toString());
 	        				}else if(celdas == 3){
 	        				SA_RET+= Double.valueOf(reporte[3].toString());
 	        				}
 	        			}
 	        		}
 	        		cell.setCellStyle(getEstiloCeldas(workbook, celdas));
 		        	hoja.autoSizeColumn(celdas, true);
   			    }
 	        }
        	 
        	setTotalCat(workbook, hoja, cell, SA, SA_RET);
	    		
	        workbook.write(outputStream);
	        
		} catch ( Exception ex ) {
			LogDeMidasWeb.log("Excepci&oacute;n al generar platilla de carga del Reporte RR6: "
					+ ex.getMessage(), Level.ALL, ex);
        }finally {
		
		if (outputStream != null) {
			try {
				outputStream.flush();
		        outputStream.close();
				
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo plantilla RR6.", Level.SEVERE, e);				
			}
		}
		
	}
				
		return outputStream;
	}

	/**
	 * Genera el estilo de la cabecera.
	 * @param fechaFin
	 * @return CellStyle
	 */
	public CellStyle getEstiloCabecera(HSSFWorkbook workbook){
		HSSFFont font = workbook.createFont();
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    CellStyle cs = workbook.createCellStyle();
	    
	    cs.setFont(font);
	    cs.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	    cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    cs.setBorderBottom(CellStyle.BORDER_THIN);
	    cs.setBorderTop(CellStyle.BORDER_THIN);
	    cs.setBorderRight(CellStyle.BORDER_THIN);
	    cs.setBorderLeft(CellStyle.BORDER_THIN);
	    cs.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
	    cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    
		return cs;    	
	}
	
	/**
	 * Genera el estilo de las celdas.
	 * @param workbook, celdas
	 * @return CellStyle
	 */
	public CellStyle getEstiloCeldas(HSSFWorkbook workbook, int celdas){
		CellStyle cs = workbook.createCellStyle();
	    
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		
		if(celdas != 0){
			cs.setDataFormat((short)7);
		}
		
	    
		return cs;    	
	}
	
	/**
	 * Conbina las celdas de la cabecera.
	 * @param workbook
	 * @param cabecera
	 * @param hoja
	 * @param cell
	 * @param tituloCabecera
	 */	
	public void setCobinarCeldas(HSSFWorkbook workbook, HSSFRow cabecera, HSSFSheet hoja, Cell cell, String[] tituloCabecera){
		
		for(int i = 0; i < tituloCabecera.length; i++)
	    {
	    	cell = cabecera.createCell(i);
	    	cell.setCellValue(tituloCabecera[i]);
	    	cell.setCellStyle(getEstiloCabecera(workbook));
	    	hoja.autoSizeColumn(i);
	    	switch (i) {
			case 0:
				hoja.addMergedRegion(new CellRangeAddress(0,1,0,0));					
				break;
			case 1:
				hoja.addMergedRegion(new CellRangeAddress(0,1,1,1));					
				break;				
			case 2:
				hoja.addMergedRegion(new CellRangeAddress(0,0,2,3));					
				break;
			case 4:
				hoja.addMergedRegion(new CellRangeAddress(0,0,4,5));					
				break;
			case 6:
				hoja.addMergedRegion(new CellRangeAddress(0,1,6,6));					
				break;
			default:
				break;
			}
	    }	
	}
	
	/**
	 * Crea la segunda cabecera y le da formato.
	 * @param workbook
	 * @param hoja
	 * @param cell
	 */		
	public void setSegundaCabecera(HSSFWorkbook workbook, HSSFSheet hoja, Cell cell){
		
		String[] segundaCabecera = {"","","SUMA ASEGURADA","SA RETENIDA","SUMA ASEGURADA","SA RETENIDA"};
		HSSFRow segundaFila = hoja.createRow((short)1);
		
		for(int i = 0; i < segundaCabecera.length; i++)
	    {
	    	cell = segundaFila.createCell(i);
	    	cell.setCellValue(segundaCabecera[i]);
	    	cell.setCellStyle(getEstiloCabecera(workbook));
	    	hoja.autoSizeColumn(i);
	    }
	
	}
	
	/**
	 * Crea la fila para el total del ramo catastrofico y le da formato.
	 * @param workbook
	 * @param hoja
	 * @param cell
	 */		
	public void setTotalCat(HSSFWorkbook workbook, HSSFSheet hoja, Cell cell, double SA, double SA_RET){
		
		HSSFRow totalCat = hoja.createRow((short)8);
		
		cell = totalCat.createCell(0);
    	cell.setCellValue("TOTAL RAMO 110");
    	cell.setCellStyle(getEstiloCeldas(workbook, 0));
    	hoja.autoSizeColumn(0);
    	
    	cell = totalCat.createCell(1);
    	cell.setCellValue("DIVERSOS");
    	cell.setCellStyle(getEstiloCeldas(workbook, 1));
				
	    cell = totalCat.createCell(2);
	    cell.setCellValue(Double.valueOf(SA));
	    cell.setCellStyle(getEstiloCeldas(workbook, 2));
	    
	    cell = totalCat.createCell(3);
	    cell.setCellValue(Double.valueOf(SA_RET));
	    cell.setCellStyle(getEstiloCeldas(workbook, 3));
	    
	    cell = totalCat.createCell(4);
	    cell.setCellValue(Double.valueOf(SA));
	    cell.setCellStyle(getEstiloCeldas(workbook, 4));
	    
	    cell = totalCat.createCell(5);
	    cell.setCellValue(Double.valueOf(SA_RET));
	    cell.setCellStyle(getEstiloCeldas(workbook, 5));
	    
	    cell = totalCat.createCell(6);
	    cell.setCellValue(Double.valueOf(SA-SA));
	    cell.setCellStyle(getEstiloCeldas(workbook, 6));
	    	
	}
	
	/**
	 * Obtiene los archivos que necesitan ser cargados para el reporte a ser generado
	 * 
	 * @param fecha
	 *            Fecha de Corte
	 * @param tipoArchivo
	 *            El tipo de archivo a ser generardo
	 * @return Los archivos a cargar
	 */
	public ActionForward getArchivosPorCargar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		
		Date fechaFinal = null;
		StringBuffer reporteCarga = new StringBuffer();
		int registros = 0;
		int tipoArchivo = 0;
		String forward = "success";
		String carga = "Archivos de carga para ese reporte:<br>";
		String cargado = "Archivo(s) cargado(s):<br>";
		
		List<String> lista = null;
		try {
			
			tipoArchivo = Integer.parseInt(request.getParameter("reporte"));
			String fechaFin = request.getParameter("fechaFinal");
				if(tipoArchivo != 8){
					fechaFinal = UtileriasWeb.getFechaFromString(fechaFin);
					lista = ReporteRR6DN.getInstancia().obtenerArchivos(fechaFinal, tipoArchivo);
				}else{
					lista = new ArrayList<String>();
				}
				
				
				if(lista == null){
					throw new SystemException ("Ocurrio un error obteniendo los archivos de carga del RR6.");
				}else if(lista.isEmpty()){
					
					response.getWriter().write("<b>"+carga+"</b>"+getTipoContrato(tipoArchivo)+"<br>"+"<b>"+cargado+"</b>"+"<p style=\"color:red;display: inline;\">No hay archivo(s) cargado(s).</p>");				
				}		
			
						
			for(String reporte : lista){
				
					if(registros != lista.size()-1){
						reporteCarga.append(reporte+", ");
					}else{
						reporteCarga.append(reporte);
					}
					registros++;
			}
		
			if(reporteCarga.length()>0){
									
				response.getWriter().write("<b>"+carga+"</b>"+getTipoContrato(tipoArchivo)+"<br>"+"<b>"+cargado+"</b>"+"<p style=\"color:green;display: inline;\">"+reporteCarga+"</p>");			
			
		  }
			
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del RR6...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (IOException e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del RR6...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del RR6...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
		} catch (ParseException e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del RR6...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
		}
		
		return mapping.findForward(forward);
	}
	
	/**
	 * Obtiene el tipo de Contrato
	 * 
	 * @param int
	 *            Numero del reporte
	 * @return El tipo de Contrato
	 */
	public String getTipoContrato(int tipoArchivo) {
		
		String contrato="";
						
		if(tipoArchivo == RTRE){
			contrato = "NO PROPORCIONAL XL.";
		}else if(tipoArchivo == RTRC){
			contrato = "PROPORCIONAL VIDA y NO PROPORCIONAL XL.";
		}else if(tipoArchivo == RTRF){
			contrato = "FACULTATIVOS VIDA.";
		}else if(tipoArchivo == RTRR){
			contrato = "PROPORCIONAL VIDA, NO PROPORCIONAL XL y FACULTATIVOS VIDA.";
		}else if(tipoArchivo == RTRS){
			contrato = "SINIESTROS VIDA ";
		}else if(tipoArchivo == RARN){
			contrato = "PROPORCIONAL VIDA, NO PROPORCIONAL XL y FACULTATIVOS VIDA.";
		}else if(tipoArchivo == RTRI){
			contrato = "Dependencia con los reportes RTRC y RTRF.";
		}else if(tipoArchivo == CUMF){
			contrato = "Este reporte se genera en Ceros.";
		}
		
		
		return contrato;
	}
	
}
