package mx.com.afirme.midas.danios.reportes.endoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL3DetalleInciso;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL4DetalleSubincisoPorInciso;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteCotizacionEndoso extends ReporteCotizacionBase {
	
	public ReporteCotizacionEndoso(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		setListaPlantillas(new ArrayList<byte[]>());
	}

	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteCotizacionEndoso(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}

	private void generarReporteCotizacionEndoso(String nombreUsuario) throws SystemException {
		setMapaParametrosGeneralesPlantillas(new HashMap<String, Object>());
//		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		byte[] reporteTMP = null;
		consultarInformacionCotizacion(nombreUsuario);
		poblarParametrosComunes(nombreUsuario,false);
		
		consultarMovimientosCotizacion(cotizacionDTO, nombreUsuario);
		poblarParametrosCuadriculaTotalesEndoso(cotizacionDTO, nombreUsuario);
		PL20MovimientosEndoso plantillaGeneralMovimientos = new PL20MovimientosEndoso(cotizacionDTO, getMapaParametrosGeneralesPlantillas(),this);

		try {
			reporteTMP = plantillaGeneralMovimientos.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP != null) {
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		
		Map<BigDecimal,IncisoCotizacionDTO> mapaIncisosModificados = new HashMap<BigDecimal,IncisoCotizacionDTO>();
		for(MovimientoCotizacionEndosoDTO movimiento : movimientosCotizacion){
			if(movimiento.getNumeroInciso().intValue() != 0){
				if(!mapaIncisosModificados.containsKey(movimiento.getNumeroInciso())){
					for(IncisoCotizacionDTO incisoModificadoTMP : listaIncisos){
						if(incisoModificadoTMP.getId().getNumeroInciso().intValue() == movimiento.getNumeroInciso().intValue()){
							mapaIncisosModificados.put(movimiento.getNumeroInciso(), incisoModificadoTMP);
							break;
						}
					}
				}
			}
		}
		
		IncisoCotizacionDTO[] arrayIncisosModificados = ordenarMapaIncisos(mapaIncisosModificados);
		
		for(int i=0;i<arrayIncisosModificados.length;i++){
			IncisoCotizacionDTO incisoCot = arrayIncisosModificados[i];
			MidasPlantillaBase plantillaDetalleInciso = new PL3DetalleInciso(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantillaDetalleInciso.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
			List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
			for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
				MidasPlantillaBase plantilla4 = new PL4DetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
				try {
					reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
				} catch (SystemException e) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
		}
	}

}
