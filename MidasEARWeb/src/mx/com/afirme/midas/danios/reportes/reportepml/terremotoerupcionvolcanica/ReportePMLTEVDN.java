package mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePMLTEVDN {

	private static final ReportePMLTEVDN INSTANCIA = new ReportePMLTEVDN();
	
	public static ReportePMLTEVDN getInstancia() {
		return INSTANCIA;
	}
	
	@Deprecated
	public List<ReportePMLTEVDTO> obtieneReportePMLTEV(Date fechaCorte, Double tipoCambio, String nombreUsuario)  
			throws SystemException {
		
		ReportePMLTEVSN reportePMLTEVSN = new ReportePMLTEVSN();
		
		ReportePMLTEVDTO filtroReporte = new ReportePMLTEVDTO();
		
		filtroReporte.setFechaCorte(fechaCorte);
		filtroReporte.setTipoCambio(tipoCambio);
		
		return reportePMLTEVSN.obtieneReportePMLTEV(filtroReporte, nombreUsuario);
	}

	public boolean calcularReportePMLTEV(ReportePMLTEVDTO filtroReporte, String nombreUsuario) throws SystemException{
		return new ReportePMLTEVSN().calcularReportePMLTEV(filtroReporte, nombreUsuario);
	}

	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVIndependientesCreditosHipotecarios(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVIndependientesGrandesRiesgos(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVIndependientesRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVIndependientesRiesgosNormales(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoCreditosHipotecarios(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVSemiAgrupadoCreditosHipotecarios(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoGrandesRiesgos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVSemiAgrupadoGrandesRiesgos(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVSemiAgrupadoRiesgosNormales(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVSemiAgrupadoRiesgosNormales(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLTEVDTO> obtenerReportePMLTEVTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLTEVSN().obtenerReportePMLTEVTecnicos(claveTipoReporte,nombreUsuario);
	}
	
}
