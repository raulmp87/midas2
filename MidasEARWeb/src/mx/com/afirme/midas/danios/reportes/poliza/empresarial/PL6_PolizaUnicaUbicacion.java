package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.poliza.PlantillaPolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public class PL6_PolizaUnicaUbicacion extends PlantillaPolizaBase{

	public PL6_PolizaUnicaUbicacion(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO,incisoCotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.unicaUbicacion"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete"));
	}
	
	public PL6_PolizaUnicaUbicacion(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReportePolizaBase reportePolizaBase) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reportePolizaBase);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.unicaUbicacion"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete"));
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			poblarParametrosPlantillaDatosGeneralesInciso();
			
			poblarSeccionesPorInciso();
			//01/12/09. Se debe omitir la plantilla si no hay registros para mostrar.
			if (getListaRegistrosContenido() == null || getListaRegistrosContenido().isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}
			Map<String,Object> parametrosReporteCoberturas = new HashMap<String,Object>();
			String nombrePlantillaSubReporte = getPaquetePlantilla()+ 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.unicaUbicacion.subReporteCoberturas");
			JasperReport subReporteCoberturas = getJasperReport(nombrePlantillaSubReporte);
			if(subReporteCoberturas == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
			getParametrosVariablesReporte().put("SUBREPORTE_COBERTURAS", subReporteCoberturas);
			nombrePlantillaSubReporte = getPaquetePlantilla() + 
				UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.unicaUbicacion.subReporteRiesgos");
		    JasperReport subReporteRiesgos = getJasperReport(nombrePlantillaSubReporte);
		    if(subReporteRiesgos == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
			parametrosReporteCoberturas.put("SUBREPORTE_RIESGOS", subReporteRiesgos);
			getParametrosVariablesReporte().put("PARAMETROS_SUBREPORTE_COBERTURA",parametrosReporteCoberturas);
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				generarLogErrorCompilacionPlantilla(e);
				setByteArrayReport( null );
			}
		}
		else	setByteArrayReport( null);
	}
}

