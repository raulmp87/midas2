package mx.com.afirme.midas.danios.reportes.poliza.empresarial;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.PlantillaSubIncisosBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL4_PolizaDetalleSubincisoPorInciso extends PlantillaSubIncisosBase{

	public PL4_PolizaDetalleSubincisoPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion) {
		super(cotizacionDTO,incisoCotizacionDTO,idToSeccion);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.subincisosPorInciso") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}
	
	public PL4_PolizaDetalleSubincisoPorInciso(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,BigDecimal idToSeccion,Map<String,Object> mapaParametrosPlantilla,ReporteCotizacionBase reporteBase) {
		super(cotizacionDTO,incisoCotizacionDTO,idToSeccion,mapaParametrosPlantilla,reporteBase);
		setNombrePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.subincisosPorInciso") );
		setPaquetePlantilla( UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.paquete") );
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		super.poblarSubIncisos(nombreUsuario);
		return getByteArrayReport();
	}
}
