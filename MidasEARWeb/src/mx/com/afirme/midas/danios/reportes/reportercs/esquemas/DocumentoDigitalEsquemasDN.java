package mx.com.afirme.midas.danios.reportes.reportercs.esquemas;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;

public class DocumentoDigitalEsquemasDN {
	private static final DocumentoDigitalEsquemasDN INSTANCIA = new DocumentoDigitalEsquemasDN();

	public static DocumentoDigitalEsquemasDN getInstancia() {
		return DocumentoDigitalEsquemasDN.INSTANCIA;
	}

	public DocumentoDigitalEsquemasDTO getPorId(BigDecimal id) throws SystemException {
		DocumentoDigitalEsquemasSN solicitudSN = new DocumentoDigitalEsquemasSN();
		return solicitudSN.getPorId(id);
	}

	public List<DocumentoDigitalEsquemasDTO> listarTodos() throws SystemException {
		DocumentoDigitalEsquemasSN solicitudSN = new DocumentoDigitalEsquemasSN();
		return solicitudSN.listarTodos();
	}

	public void agregar(DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalEsquemasSN solicitudSN = new DocumentoDigitalEsquemasSN();
		solicitudSN.agregar(documentoDigitalSolicitudDTO);
	}
	
	public void update(DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalEsquemasSN solicitudSN = new DocumentoDigitalEsquemasSN();
		solicitudSN.update(documentoDigitalSolicitudDTO);
	}

	public List<DocumentoDigitalEsquemasDTO> listarDocumentosSolicitud(
			BigDecimal idToSolicitud) throws SystemException {
		DocumentoDigitalEsquemasSN solicitudSN = new DocumentoDigitalEsquemasSN();
		return solicitudSN.listarTodos();
	}

	public void borrar(DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalEsquemasSN solicitudSN = new DocumentoDigitalEsquemasSN();
		solicitudSN.borrar(documentoDigitalSolicitudDTO);
	}
	
	public List<DocumentoDigitalEsquemasDTO> listarDocumentosDigitalesPorSolicitud(BigDecimal idToSolicitud) throws SystemException{
		DocumentoDigitalEsquemasSN documentoDigitalSolicitudSN = new DocumentoDigitalEsquemasSN();
		return documentoDigitalSolicitudSN.listarDocumentosSolicitud(idToSolicitud);
	}
	
	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO.getIdToControlArchivo());
	}
}
