package mx.com.afirme.midas.danios.reportes.reportercs;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
  
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReporteRCS extends MidasReporteBase{
	private ReporteRCSForm movimientoEmisionForm;
	private static final String DATEFORMAT = "dd/MM/yyyy";
	private static final String PARSE_EXCEPCION = "ParseException";
	private static final String PARSE_EXCEPCION_MSG = "Ocurrio un error al parsear alguna de las fechas: ";
	private static final String SYSTEM_EXCEPCION = "System Exception";
	private static final String SYSTEM_EXCEPCION_MSG = "Ocurrio un error al recuperar la informacion para el reporte: ";
	private static final String SYSTEM_EXCEPCION_MSG2 = "No se encontraron registros para los datos introducidos.";
	
	public static enum TipoReporte{
		Rangos("10"),VidaCP("12"),VidaLP("13");
	
		private String valor;
		
		TipoReporte(String valor){
			this.valor = valor;
		}
		
		public String getValor(){return valor;}
	};
	
	
	public ReporteRCS(ReporteRCSForm movimientoEmisionForm){
		this.movimientoEmisionForm = movimientoEmisionForm;
	}

	public byte[] obtenerReporte() throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null; 
		try{
			
			listaMovimientos = obtenerRCS(movimientoEmisionForm);
		}catch(ParseException e){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e1);
			
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		if(listaMovimientos == null){
			throw new SystemException (SYSTEM_EXCEPCION_MSG);
		}
		if(listaMovimientos.isEmpty())
		{
			ReporteRCSDTO rcsdto = new ReporteRCSDTO();
			rcsdto.setRegistro(SYSTEM_EXCEPCION_MSG2);
			listaMovimientos.add(rcsdto);
		}
		PL17PlantillaRCS plantilla = new PL17PlantillaRCS(listaMovimientos);
		return plantilla.obtenerReporte("");
	}       
	
	public byte[] obtenerReportePas() throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null; 
		try{
			listaMovimientos = obtenerRCSVidaPas(movimientoEmisionForm);
		}catch(ParseException e){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		if(listaMovimientos == null){
			throw new SystemException (SYSTEM_EXCEPCION_MSG);
		}
		if(listaMovimientos.isEmpty())
		{
			ReporteRCSDTO rcsdto = new ReporteRCSDTO();
			rcsdto.setRegistro(SYSTEM_EXCEPCION_MSG2);
			listaMovimientos.add(rcsdto);
		}
		PL17PlantillaRCS plantilla = new PL17PlantillaRCS(listaMovimientos);
	
		return plantilla.obtenerReporte("");
	}       
	
	public byte[] obtenerDetalleReporte() throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null; 
		byte[] byteArray;
		try{
			listaMovimientos = obtenerDetalleRCS(movimientoEmisionForm);
			
			if(listaMovimientos == null){
				throw new SystemException (SYSTEM_EXCEPCION_MSG);
			}
			if(listaMovimientos.isEmpty())
			{
				ReporteRCSDTO rcsdto = new ReporteRCSDTO();
				rcsdto.setRegistro(SYSTEM_EXCEPCION_MSG2);
				listaMovimientos.add(rcsdto);
			}
			
			byteArray = toArrayByte(listaMovimientos);
			
		}catch(ParseException e){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		} catch (Exception e) {
			LogDeMidasWeb.log("Exception", Level.INFO, e);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e.getCause());
		}
		
		return byteArray;
	}
	
	public byte[] obtenerDetalleVigorLP() throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null; 
		byte[] byteArray;
		try{
			listaMovimientos = obtenerDetalleVigorLP(movimientoEmisionForm);
			
			if(listaMovimientos == null){
				throw new SystemException (SYSTEM_EXCEPCION_MSG);
			}
			if(listaMovimientos.isEmpty())
			{
				ReporteRCSDTO rcsdto = new ReporteRCSDTO();
				rcsdto.setRegistro(SYSTEM_EXCEPCION_MSG2);
				listaMovimientos.add(rcsdto);
			}
		
			byteArray = toArrayByteVigorLP(listaMovimientos);
			
		}catch(ParseException e){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		} catch (Exception e) {
			LogDeMidasWeb.log("Exception", Level.INFO, e);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e.getCause());
		}
		
		return byteArray;
	}
	
	public byte[] obtenerReporteRangos() throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null; 
		byte[] byteArray;
		try{
			listaMovimientos = obtenerReporteRangos(movimientoEmisionForm);
			
			if(listaMovimientos == null){
				throw new SystemException (SYSTEM_EXCEPCION_MSG);
			}
			if(listaMovimientos.isEmpty())
			{
				ReporteRCSDTO rcsdto = new ReporteRCSDTO();
				rcsdto.setRegistro(SYSTEM_EXCEPCION_MSG2);
				listaMovimientos.add(rcsdto);
			}
			
			byteArray = toArrayByte(listaMovimientos);
			
		}catch(ParseException e){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		} catch (Exception e) {
			LogDeMidasWeb.log("Exception", Level.INFO, e);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e.getCause());
		}
		
		return byteArray;
	}
	
	private byte[] toArrayByte( List<ReporteRCSDTO> list) throws IOException {
		
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			
			if(TipoReporte.VidaCP.equals(movimientoEmisionForm.getId_ramo()))
				out.write(encabezadoDetalleRCSVidaCP());
			else if(TipoReporte.VidaLP.equals(movimientoEmisionForm.getId_ramo()))
				out.write(encabezadoDetalleRCSVidaLP());		
			else if(TipoReporte.Rangos.equals(movimientoEmisionForm.getId_ramo()))
				out.write(encabezadoDetalleRangos());
			else
				out.write(encabezadoDetalleRCS());
			for (ReporteRCSDTO item : list) {

			    out.writeBytes(item.getRegistro()+System.getProperty("line.separator"));

			}
			
		return baos.toByteArray();
	}
	
	private byte[] toArrayByteVigorLP( List<ReporteRCSDTO> list) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		
		out.write(encabezadoDetalleVigorLP());
		
		for (ReporteRCSDTO item : list) {

		    out.writeBytes(item.getRegistro()+System.getProperty("line.separator"));

		}
		
	return baos.toByteArray();
}
	
	private byte[] encabezadoDetalleRCSVidaCP() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		
		out.writeBytes(
				"CORTE"+'|'+"ID_COTIZACION"+'|'+"ID_CENTRO_EMIS"+'|'+"NUM_POLIZA"+'|'+"NUM_RENOV_POL"+'|'+
				"ID_LIN_NEGOCIO"+'|'+"ID_INCISO"+'|'+"ID_COBERTURA_BAS"+'|'+"SA_FALLECIMIENTO"+'|'+"SA_PERD_ORGANICAS"+'|'+
				"SA_DBL_INDEM_MTE_ACC"+'|'+"SA_TPL_INDEM_MTE_COL"+'|'+"SA_PAGO_AII"+'|'+"SA_OTROS_BENEF"+'|'+"SA_SOBREVIVENCIA"+'|'+
				"F_NACIMIENTO"+'|'+"EDAD"+'|'+"CVE_SEXO"+'|'+"ID_MONEDA"+'|'+"SA_AMPARADA"+'|'+"F_INI_VIGENCIA"+'|'+"F_FIN_VIGENCIA"+'|'+
				"TIPO_SEG"+'|'+"REASEG_1"+'|'+"REASEG_2"+'|'+"REASEG_3" + System.getProperty("line.separator"));
		
		return baos.toByteArray();
	}
	
	private byte[] encabezadoDetalleRCS() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		
		out.writeBytes(
				"CORTE" + "|" +
				"RAMO_SUBRAMO" + "|" +
				"TIPO_SEGURO_VEHICULO" + "|" +
		        "IDTOPOLIZA" + "|" +
		        "NUM_POLIZA" + "|" +
		        "INICIOVIGENCIA"+ "|" +
		        "FINVIGENCIA" + "|" +
		        "MARCA" + "|" +
		        "MODELO" + "|" +
		        "INCISO_UBICACION" + "|" +
		        "ID_SECCION" + "|" +
		        "ID_COBERTURA" + "|" +
		        "CODIGO_SUBGIRO" + "|" +
		        "COBERTURA_GIRO" + "|" +
		        "MONEDA" + "|" +
		        "CER_1" + "|" +
		        "CER_2" + "|" +
		        "CER_3" + "|" +
		        "NUM_ASEGURADOS" + "|" +
		        "PRIMA_EMI_DIR" + "|" +
		        "PRIMA_EMI_DIR_ANUAL" + "|" +
		        "PRIMA_NO_DEV_ANUAL" + "|" +
		        "DUR_REMANENTE_PROM" + "|" +
		        "LIMITE_MAX_RESP" + "|" +
		        "ID_CNSF_RANGOS_SA" + "|" +
		        "DURACION_PROMEDIO" + "|" +
		        "RETENCION" + "|" +
		        "CUOTA_PARTE" + "|" +
		        "PRIMER_EXCEDENTE" + "|" +
		        "FACULTATIVO" + "|" +
		        "ORIGEN" + System.getProperty("line.separator"));
		
		return baos.toByteArray();
	}
	
	private byte[] encabezadoDetalleRCSVidaLP() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		
		out.writeBytes(
				"CORTE"+'|'+"ID_COTIZACION"+'|'+"ID_CENTRO_EMIS"+'|'+"NUM_POLIZA"+'|'+"NUM_RENOV_POL"+'|'+
				"ID_LIN_NEGOCIO"+'|'+"ID_INCISO"+'|'+"ID_COBERTURA_BAS"+'|'+"SA_FALLECIMIENTO"+'|'+
				"SA_PERD_ORGANICAS"+'|'+
				"SA_DBL_INDEM_MTE_ACC"+'|'+"SA_TPL_INDEM_MTE_COL"+'|'+"SA_PAGO_AII"+'|'+"SA_OTROS_BENEF"+'|'+
				"SA_SOBREVIVENCIA"+'|'+"F_NACIMIENTO"+'|'+"EDAD"+'|'+"CVE_SEXO"+'|'+"PLAZO"+'|'+"ANTIGUEDAD"+'|'+
				"VIGENCIA_RESTANTE"+'|'+"ID_MONEDA"+'|'+"SA_AMPARADA"+'|'+"F_INI_VIGENCIA"+'|'+"F_FIN_VIGENCIA"+'|'+
				"MOD_PLAN"+'|'+"REASEG_1"+'|'+"REASEG_2"+'|'+"REASEG_3"+'|'+"VAL_RESCATE"+'|'+"NUM_PAGOS_REM"+'|'+
				"PRIMA_ANUAL"+'|'+"GT_ADQUISICION"+'|'+"GT_OPERACION"+'|'+"UTILIDAD"+'|'+"EDADCONTRATACION"+'|'+
				"TIPOCADUCIDAD" + System.getProperty("line.separator"));
		
		return baos.toByteArray();
	}	
	
	private byte[] encabezadoDetalleRangos() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		
		out.writeBytes(
				"RAMO"+'|'+"TIPOSEGURO/COBERTURA"+'|'+"LIMITE INF"+'|'+"LIMITE SUP" + System.getProperty("line.separator"));
		
		return baos.toByteArray();
	}	
	
	private byte[] encabezadoDetalleVigorLP() throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		
		out.writeBytes(
				"NUM_POLIZA|EDAD|ANTIGUEDAD|ID_MONEDA|MOD_PLAN|TIPOCADUCIDAD|1|BASICO|TIPOFLUJO|VAL1|VAL2|VAL3|VAL4|VAL5|VAL6|VAL7|VAL8|VAL9|VAL10|VAL11|VAL12|VAL13|VAL14|VAL15|VAL16|VAL17|VAL18|VAL19|VAL20|VAL21|VAL22|VAL23|VAL24|VAL25|VAL26|VAL27|VAL28|VAL29|VAL30|VAL31|VAL32|VAL33|VAL34|VAL35|VAL36|VAL37|VAL38|VAL39|VAL40|VAL41|VAL42|VAL43|VAL44|VAL45|VAL46|VAL47|VAL48|VAL49|VAL50|VAL51|VAL52|VAL53|VAL54|VAL55|VAL56|VAL57|VAL58|VAL59|VAL60|VAL61|VAL62|VAL63|VAL64|VAL65|VAL66|VAL67|VAL68|VAL69|VAL70|VAL71|VAL72|VAL73|VAL74|VAL75|VAL76|VAL77|VAL78|VAL79|VAL80|VAL81|VAL82|VAL83|VAL84|VAL85|VAL86|VAL87|VAL88|VAL89|VAL90|VAL91|VAL92|VAL93|VAL94|VAL95|VAL96|VAL97|VAL98|VAL99|VAL100|0|F_INI_VIGENCIA|F_FIN_VIGENCIA|F_NACIMIENTO|PLAZO_PAGO|PLAZOT|CVE_SEXO|ID_CONTRATANTE|ID_AGENTE|NOM_PRODUCTO|ID_LIN_NEGOCIO|NOM_FORMA_PAGO|ID_GERENCIA" + System.getProperty("line.separator"));
		
		return baos.toByteArray();
	}	
	
	public String obtenerNomenclaturaArch(String tipoArchivo) throws SystemException {
		String nomenclatura = null; 
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		try{
			nomenclatura = reporteBasesEmisionDN.obtieneNomArchivo(tipoArchivo);
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		return nomenclatura;
	}       
	
	public String obtenerNumRangosDuracion(int tipoRango) throws SystemException {
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		try{
			return reporteBasesEmisionDN.obtenerNumRangosDuracion(tipoRango);
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
			
		}
	}  
	
	public boolean actualizaRangos() throws SystemException {
		boolean band = false;
		try{
			band = actualizaRangosSP(movimientoEmisionForm);
		}catch(ParseException e){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		return band;
	}
	
	public boolean eliminaRangos() throws SystemException {
		boolean band = false;
		try{
			band = eliminaRangosSP(movimientoEmisionForm);
		}catch(ParseException e){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		return band;
	}
	
	public boolean setRangosDuracion() throws SystemException {
		try{
			return setRangosDuracionSP();
		}catch(ParseException e){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
	}
	
	public byte[] obtenerReporteREAS(int tipoReporte) throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null;
		try{
			listaMovimientos = obtenerREASRCS(movimientoEmisionForm,tipoReporte);
		}catch(ParseException e){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		if(listaMovimientos == null){
			throw new SystemException (SYSTEM_EXCEPCION_MSG);
		}
		if(listaMovimientos.isEmpty()){
			throw new SystemException (SYSTEM_EXCEPCION_MSG2);
		}
		PL17PlantillaRCS plantilla = new PL17PlantillaRCS(listaMovimientos);

		return plantilla.obtenerReporte("");
	}
		
	public byte[] obtenerValidaciones() throws SystemException {
		List<ReporteRCSDTO> listaMovimientos = null;
		try{
			listaMovimientos = obtenerValidacionesRCS(movimientoEmisionForm);
		}catch(ParseException e){
			LogDeMidasWeb.log(PARSE_EXCEPCION, Level.INFO, e);
			throw new SystemException(PARSE_EXCEPCION_MSG+e.getCause());
		}catch(SystemException e1){
			LogDeMidasWeb.log(SYSTEM_EXCEPCION, Level.INFO, e1);
			throw new SystemException(SYSTEM_EXCEPCION_MSG+e1.getCause());
		}
		if(listaMovimientos == null){
			throw new SystemException (SYSTEM_EXCEPCION_MSG);
		}
		if(listaMovimientos.isEmpty()){
			throw new SystemException (SYSTEM_EXCEPCION_MSG2);
		}
		PL17PlantillaRCS plantilla = new PL17PlantillaRCS(listaMovimientos);
		
		return plantilla.obtenerReporte("");
	}
	
	public String obtenerClaveRamo(String idRamo) {
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		try {
			return reporteBasesEmisionDN.obtieneClaveRamo(idRamo).get(0).getRegistro();
		} catch (SystemException e) {
			LogDeMidasWeb.log("ERROR", Level.INFO, e);
		} 
		return null;
	}
	
	private List<ReporteRCSDTO> obtenerRCS(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer idRamo = 0;

		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getId_ramo())) {
			idRamo = Integer.parseInt(movimientoEmisionForm.getId_ramo());
		}
		
		return reporteBasesEmisionDN.obtieneRCS(cal, fechaFin, idRamo, movimientoEmisionForm.getNumeroCortes(), movimientoEmisionForm.getClaveNegocio());
	}
	
	private List<ReporteRCSDTO> obtenerRCSVidaPas(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer idRamo = 0;
 
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getId_ramo())) {
			idRamo = Integer.parseInt(movimientoEmisionForm.getId_ramo());
		}
		
		return reporteBasesEmisionDN.obtieneRCSVidaPas(cal, fechaFin, idRamo, movimientoEmisionForm.getNumeroCortes());
	} 
	
	private List<ReporteRCSDTO> obtenerDetalleRCS(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer idRamo = 0;

		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getId_ramo())) {
			idRamo = Integer.parseInt(movimientoEmisionForm.getId_ramo());
		}
		
		return reporteBasesEmisionDN.obtieneDetalleRCS(cal, fechaFin, idRamo, movimientoEmisionForm.getNumeroCortes());
	}
	
	private List<ReporteRCSDTO> obtenerDetalleVigorLP(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();

		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		
		return reporteBasesEmisionDN.obtieneDetalleVigorLP(cal, fechaFin, 0, movimientoEmisionForm.getNumeroCortes());
	}
	
	private List<ReporteRCSDTO> obtenerReporteRangos(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		
		return reporteBasesEmisionDN.obtenerReporteRangos();
	}
	
	private boolean actualizaRangosSP(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer idRamo = 0;
		Integer cveNegocio = 0;
		Integer numeroCortes = 0;

		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getId_ramo())) {
			idRamo = Integer.parseInt(movimientoEmisionForm.getId_ramo());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getClaveNegocio())) {
			cveNegocio = Integer.parseInt(movimientoEmisionForm.getClaveNegocio());
		}
		return reporteBasesEmisionDN.actualizaRangosSP(cal, fechaFin, idRamo, cveNegocio, numeroCortes);
	}
	
	private boolean eliminaRangosSP(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer idRamo = 0;
		Integer cveNegocio = 0;
		
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getId_ramo())) {
			idRamo = Integer.parseInt(movimientoEmisionForm.getId_ramo());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getClaveNegocio())) {
			cveNegocio = Integer.parseInt(movimientoEmisionForm.getClaveNegocio());
		}
		return reporteBasesEmisionDN.eliminaRangosSP(cal, fechaFin, idRamo, cveNegocio, 0);
	}
	
	boolean setRangosDuracionSP() throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Integer duracionPromedio = 0;
		Integer duracionRemanente = movimientoEmisionForm.getNumeroCortes();

		
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getNomenclatura())) {
			duracionPromedio = Integer.parseInt(movimientoEmisionForm.getNomenclatura());
		}
		
		return reporteBasesEmisionDN.setRangosDuracioSP(duracionPromedio, duracionRemanente);
	}
	
	private List<ReporteRCSDTO> obtenerREASRCS(ReporteRCSForm movimientoEmisionForm, int tipoReporte) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer idRamo = 0;
 
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		return reporteBasesEmisionDN.obtieneREASRCS(cal, fechaFin, idRamo, movimientoEmisionForm.getNumeroCortes(), tipoReporte);
	}
	
	private List<ReporteRCSDTO> obtenerValidacionesRCS(ReporteRCSForm movimientoEmisionForm) throws ParseException,SystemException{
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		Calendar cal = Calendar.getInstance();
		Date fechaFin = new Date();
		Integer cveNegocio = 0;

		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
			Date today = formatter.parse(movimientoEmisionForm.getFechaInicio());
            cal.setTime(today);
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getClaveNegocio())) {
			cveNegocio = Integer.parseInt(movimientoEmisionForm.getClaveNegocio());
		}
		return reporteBasesEmisionDN.obtieneValidacionesRCS(cal, fechaFin, cveNegocio, movimientoEmisionForm.getNumeroCortes());
	}

	public void sp_actualizaNom(String string, String nomenclatura)throws ParseException,SystemException {
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		reporteBasesEmisionDN.actualizaNomSP(string,nomenclatura);
	}

	public int generaInfoRCS(String fechaCorte, String negocio, String reproceso) throws ParseException,SystemException {
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		String []fechaParts = fechaCorte.split("/");
		return reporteBasesEmisionDN.generaInfoRCS(fechaParts[2],fechaParts[1],fechaParts[0], negocio, reproceso);
	}

	public List<SolvenciaLogDTO> getLogSolvencia(Date fechaCorte, String negocio) throws ParseException,SystemException {
		ReporteRCSDN reporteBasesEmisionDN = ReporteRCSDN.getInstancia();
		return reporteBasesEmisionDN.getLogSolvencia(fechaCorte, negocio);
	}

	public ReporteRCSForm getMovimientoEmisionForm() {
		return movimientoEmisionForm;
	}

	public void setMovimientoEmisionForm(ReporteRCSForm movimientoEmisionForm) {
		this.movimientoEmisionForm = movimientoEmisionForm;
	}
}