package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReporteBasesEmision extends MidasReporteBase{
	ReporteBasesEmisionForm movimientoEmisionForm;
	
	public ReporteBasesEmision(ReporteBasesEmisionForm movimientoEmisionForm){
		this.movimientoEmisionForm = movimientoEmisionForm;
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		List<ReporteBasesEmisionDTO> listaMovimientos = null;
		try{
			listaMovimientos = obtenerMovimientosEmision(movimientoEmisionForm,claveUsuario);
		}catch(ParseException e){
			throw new SystemException("Ocurri� un error al parsear alguna de las fechas: "+e.getCause());
		}catch(SystemException e1){
			throw new SystemException("Ocurri� un error al recuperar la informaci�n para el reporte: "+e1.getCause());
		}
		if(listaMovimientos == null)
			throw new SystemException ("Ocurri� un error al recuperar la informaci�n para el reporte.");
		if(listaMovimientos.isEmpty())
			throw new SystemException ("No se encontraron registros para los datos introducidos.");
		PL17PlantillaBasesEmision plantilla = new PL17PlantillaBasesEmision(listaMovimientos);
		byte byteArray[] = null;
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		plantilla.setListaRegistrosContenido(null);
		plantilla = null;
		Runtime.getRuntime().gc();
		return byteArray;
	}
	
	private List<ReporteBasesEmisionDTO> obtenerMovimientosEmision(ReporteBasesEmisionForm movimientoEmisionForm, String nombreUsuario) throws ParseException,SystemException{
		ReporteBasesEmisionDN reporteBasesEmisionDN = ReporteBasesEmisionDN.getInstancia();
		Date fechaInicio = new Date();
		Date fechaFin = new Date();
		Integer idRamo = 0;
		Integer idSubRamo = 0;
		Integer nivelContratoReaseguro = 0;
		Integer nivelAgrupamiento = 0;

		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())) {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			fechaInicio = (Date)formatter.parse(movimientoEmisionForm.getFechaInicio());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaFin())) {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			fechaFin = (Date)formatter.parse(movimientoEmisionForm.getFechaFin());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getIdTcRamo())) {
			idRamo = Integer.parseInt(movimientoEmisionForm.getIdTcRamo());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getIdTcSubramo())) {
			idSubRamo = Integer.parseInt(movimientoEmisionForm.getIdTcSubramo());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getIdTcTipoReaseguro())) {
			nivelContratoReaseguro = Integer.parseInt(movimientoEmisionForm.getIdTcTipoReaseguro());
		}
		if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getNivelAgrupamiento())) {
			nivelAgrupamiento = Integer.parseInt(movimientoEmisionForm.getNivelAgrupamiento());
		}
		
		return reporteBasesEmisionDN.obtieneMovimientosEmision(fechaInicio, fechaFin, idRamo, idSubRamo, nivelContratoReaseguro, nivelAgrupamiento, nombreUsuario);
	}
}
