package mx.com.afirme.midas.danios.reportes.endoso;

import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL20MovimientosEndoso extends PlantillaEndosoCotizacionBase {

	public PL20MovimientosEndoso(CotizacionDTO cotizacionDTO) {
		super(cotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.cotizacion.endoso.reporte.movimientos"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.danios.reportes.cotizacion.endoso.movimientos.paquete"));
	}

	public PL20MovimientosEndoso(CotizacionDTO cotizacionDTO,Map<String, Object> mapaParametrosPlantilla,ReporteCotizacionBase midasReporteBase) {
		super(cotizacionDTO,mapaParametrosPlantilla,midasReporteBase);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.cotizacion.endoso.reporte.movimientos"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.danios.reportes.cotizacion.endoso.movimientos.paquete"));
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		String nombrePlantillaSubReporteMovtosGenerales = getPaquetePlantilla()
				+ UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.cotizacion.endoso.reporte.subreporte.movimientos.generales");
		String nombrePlantillaMovtosInciso = getPaquetePlantilla() 
				+ UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.endoso.reporte.subreporteMovimientosInciso");
		String nombrePlantillaDescMovtosInciso = getPaquetePlantilla()  
				+ UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.endoso.reporte.subreporteDescripcionMovimientosInciso");
		if(movimientosGeneralesExtra != null)
			super.poblarListaMovimientosEndoso(claveUsuario,nombrePlantillaSubReporteMovtosGenerales,nombrePlantillaMovtosInciso,nombrePlantillaDescMovtosInciso,true,movimientosGeneralesExtra);
		else
			super.poblarListaMovimientosEndoso(claveUsuario,nombrePlantillaSubReporteMovtosGenerales,nombrePlantillaMovtosInciso,nombrePlantillaDescMovtosInciso,false,null);
		return getByteArrayReport();
	}

}
