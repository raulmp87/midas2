package mx.com.afirme.midas.danios.reportes.reportepml;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.sistema.creadorxls.MidasDocumentoXLS;

public abstract class DocumentoPMLBase extends MidasDocumentoXLS{
	protected Integer claveTipoReporte;
	protected String nombreReporte;
	
	public DocumentoPMLBase(Integer claveTipoReporte){
		this.claveTipoReporte = claveTipoReporte;
	}

	@SuppressWarnings("unchecked")
	protected void establecerNumeroRegistro(List listaRegistros,boolean agruparPorPoliza){
		if(listaRegistros != null && !listaRegistros.isEmpty()){
			int contador = 1;
			for(Object registro : listaRegistros){
				boolean aplicarContador = !agruparPorPoliza;
				if(agruparPorPoliza){
					String incisoString = null;
					Integer incisoInt = null;
					if(registro instanceof ReportePMLTEVDTO){
						incisoString = ((ReportePMLTEVDTO)registro).getInciso();
					}
					else if (registro instanceof ReportePMLHidroDTO){
						incisoString = ((ReportePMLHidroDTO)registro).getInciso();
					}
					if(incisoString != null){
						try{
							incisoInt = new Integer(incisoString);
						}catch(Exception e){}
					}
					if(incisoInt != null && incisoInt.intValue() == 0){
						contador = 1;
					}
					else{
						aplicarContador = true;
					}
				}
				if(aplicarContador){
					if(registro instanceof ReportePMLTEVDTO){
						((ReportePMLTEVDTO)registro).setNumeroRegistro(contador);
					}
					else if (registro instanceof ReportePMLHidroDTO){
						((ReportePMLHidroDTO)registro).setNumeroRegistro(contador);
					}
					contador ++;
				}
			}
		}
	}
	
	protected String obtenerSufijoReporte(){
		String sufijo = "";
		if(claveTipoReporte.intValue() == 1)
			sufijo = "_V";
		else if(claveTipoReporte.intValue() == 2)
			sufijo = "_CV";
		else if(claveTipoReporte.intValue() == 3)
			sufijo = "_R";
		return sufijo;
	}
	public abstract byte[] obtenerReporte(String nombreUsuario) throws Exception;
	
	public Integer getClaveTipoReporte() {
		return claveTipoReporte;
	}
	public String getNombreReporte() {
		return nombreReporte;
	}
	public void setNombreReporte(String nombreReporte) {
		this.nombreReporte = nombreReporte;
	}
	public void setClaveTipoReporte(Integer claveTipoReporte) {
		this.claveTipoReporte = claveTipoReporte;
	}
}
