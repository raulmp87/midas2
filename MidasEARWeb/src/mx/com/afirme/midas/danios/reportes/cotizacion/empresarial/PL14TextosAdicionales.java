package mx.com.afirme.midas.danios.reportes.cotizacion.empresarial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

public class PL14TextosAdicionales extends PlantillaCotizacionBase{

	public PL14TextosAdicionales(CotizacionDTO cotizacionDTO) {
		super(cotizacionDTO);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.textosAdicionales"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionEmpresarial.paquete"));
		setParametrosVariablesReporte( null );
	}
	
	public PL14TextosAdicionales(CotizacionDTO cotizacionDTO,Map<String,Object> mapaParametros) {
		super(cotizacionDTO,mapaParametros);
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.reporte.textosAdicionales"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.cotizacionEmpresarial.paquete"));
	}

	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}

			List<TexAdicionalCotDTO> listaTextos = consultarTextosAdicionales();
			
			setListaRegistrosContenido(new ArrayList<Object>());
			
			//Si no hay información para mostrar, no se debe imprimir la plantilla
			if (listaTextos.isEmpty()){
				setByteArrayReport( null );
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}
			else
				getListaRegistrosContenido().addAll(listaTextos);
		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}
		else setByteArrayReport( null );
	}
}
