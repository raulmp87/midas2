package mx.com.afirme.midas.danios.reportes.poliza.rcfuncionarios;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.poliza.PlantillaPolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

public class PL21_PolizaLicitacionRCFuncionarios extends PlantillaPolizaBase{

	private boolean esEndoso;
	public PL21_PolizaLicitacionRCFuncionarios(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		super(cotizacionDTO);
		inicializarDatosPlantilla();
	}
	
	public PL21_PolizaLicitacionRCFuncionarios(CotizacionDTO cotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO,Map<String,Object> mapaParametrosPlantilla,ReportePolizaBase reporteCotizacionGenerica) {
		super(cotizacionDTO,incisoCotizacionDTO,mapaParametrosPlantilla,reporteCotizacionGenerica);
		inicializarDatosPlantilla();
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		procesarDatosReporte(claveUsuario);
		return getByteArrayReport();
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.cotizacionDTO != null && this.incisoCotizacionDTO != null){
			if (getParametrosVariablesReporte() == null){
				super.generarParametrosComunes(cotizacionDTO, claveUsuario);
			}
			poblarParametroNumeroInciso();
			poblarParametroDireccionInciso();
			
			super.poblarLicitacionPorInciso(esEndoso);
			
			if (getListaRegistrosContenido() == null || getListaRegistrosContenido().isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
				return;
			}

		    try {
				super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}//Fin validar cotizacion != null && incisoCot != null
		else	setByteArrayReport( null);
	}

	private void inicializarDatosPlantilla(){
		esEndoso = (cotizacionDTO.getSolicitudDTO() != null &&
				cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada() != null &&
				cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada().compareTo(BigDecimal.ZERO) > 0);
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.poliza.rcfuncionarios.paquete"));
		if (esEndoso)
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.rcfuncionarios.endoso.plantillalicitacion"));
		else
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.reporte.rcfuncionarios.plantillalicitacion"));
	}
}
