package mx.com.afirme.midas.danios.reportes.cotizacion.familiar;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL14TextosAdicionales;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL1SumasAseguradas1erRiesgoLUC;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL2DocumentosAdicionales;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL3DetalleInciso;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL6UnicaUbicacion;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePaqueteFamiliar extends ReporteCotizacionBase{
	
	public ReportePaqueteFamiliar(CotizacionDTO cotizacionDTO) {
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaPlantillas(new ArrayList<byte[]>());
	} 
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReportePaqueteEmpresarial(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReportePaqueteEmpresarial(String nombreUsuario) throws SystemException{
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		try {
			//Recuperar incisos, subincisos, coberturas, etc. de la cotizacion
			consultarInformacionCotizacion(nombreUsuario);
			//Poblar los par�metros comunes para todas las plantillas
			poblarParametrosComunes(nombreUsuario,true);
			String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
			leyendaAbreviaturas = "Donde quiera que aparezca en este contrato las siguientes abreviaturas se entender�:";
			getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
			abreviaturas = null;
			descripcionAbreviaturas = null;
			
			AgrupacionCotDTO agrupacionCot = PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(idToCotizacion, (short)1);
			//Seguro paquete empresarial
			//plantilla Unica ubicacion
			if (listaIncisos.size() == 1 && agrupacionCot == null){
				abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\n\nLUC\n\nCOBERTURAS\n NORMALES:";
				descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\nCon m�ximo de:\n" +
					"D�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\nLa suma asegurada " +
					"para esta cobertura es el valor establecido en la cobertura b�sica del Bien o Secci�n contratado, salvo los subl�mites establecidos\nL�mite �nico y Combinado  " +
					"(L�mite m�ximo de responsabilidad para la Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o " +
					"todas las ubicaciones aseguradas, durante la vigencia del seguro)\n\nIncendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
				generarReporteUnicaUbicacion(nombreUsuario);
			}
			else{
				//Si aplic� primer riesgo
				if (agrupacionCot != null){
					abreviaturas = "S.A.\nS/S.A.\nR.C.\nS/VR Eq. Da�.\nS/P\nMIN\nMAX\nDSMGVDFMS\nUMA\nAMPARADO:\n\n\nCOBERTURAS\n NORMALES:";
					descripcionAbreviaturas = "Suma Asegurada\nSobre Suma Asegurada\nResponsabilidad Civil\nSobre Valor de Reposici�n del Equipo Da�ado\nSobre p�rdida\nCon m�nimo de:\n" +
							"Con m�ximo de:\nD�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nComplementa la " +
							"protecci�n del bien especificado en la Secci�n.\nL�mite �nico y Combinado  (L�mite m�ximo de responsabilidad para la " +
							"Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o todas las ubicaciones aseguradas, " +
							"durante la vigencia del seguro)\n\nComprende las coberturas de Incendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				}
				else{
					abreviaturas = "S/VR Eq. Da�.\nS.A.\nS/S.A.\nMIN\nMAX\nDSMGVDFMS\nUMA\nS/P\nAMPARADO:\nLUC\n\nCOBERTURAS\n NORMALES:";
					descripcionAbreviaturas = "Sobre Valor de Reposici�n del Equipo Da�ado\nSuma Asegurada\nSobre Suma Asegurada\nCon m�nimo de:\nCon m�ximo de:\n" +
							"D�as de Salario M�nimo General Vigente en el Distrito Federal al momento del siniestro\nUnidad de Medida y Actualización\nSobre p�rdida\nLa suma asegurada " +
							"para esta cobertura es el valor establecido en la cobertura b�sica del Bien o Secci�n contratado, salvo los subl�mites establecidos\nL�mite �nico y Combinado  " +
							"(L�mite m�ximo de responsabilidad para la Instituci�n, por uno o todos los siniestros que puedan ocurrir por una o " +
							"todas las ubicaciones aseguradas, durante la vigencia del seguro)\n\nIncendio y/o rayo y extensi�n de cubierta cuando sean  contratadas.";
				}
				getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
				getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
				generarReporteMultiplesUbicaciones(nombreUsuario);
			}
			
//			Agregar los documentos anexos.
			/**
			 * 31/12/2009. Por indicaciones de Arturo y Euler, no se deben mostrar los documentos anexos en la cotizaci�n, s�lo en la p�liza.
			 * Jos� Luis Arellano B�rcenas
			 */
//			List<DocAnexoCotDTO> listaDocumentosAnexos = new ArrayList<DocAnexoCotDTO>();
//			DocAnexoCotDTO docAnexo = new DocAnexoCotDTO();
//			docAnexo.setCotizacion(cotizacionDTO);
//			docAnexo.setClaveSeleccion((short)1);
//			listaDocumentosAnexos = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexo);
//			for(DocAnexoCotDTO documento : listaDocumentosAnexos){
//				try{
//				ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getIdToControlArchivo());
//				super.adjuntarDocumentoAnexo(controlArchivo);
//				}catch(Exception e){}
//			}
		} catch (SystemException e) {
			throw e;
		}
	}
	
	private void generarReporteMultiplesUbicaciones(String nombreUsuario){
		byte[] reporteTMP = null;
		MidasPlantillaBase plantillaPrimerRiesgo = new PL1SumasAseguradas1erRiesgoLUC(cotizacionDTO.getIdToCotizacion(),getMapaParametrosGeneralesPlantillas(),this);
		try {
			reporteTMP = plantillaPrimerRiesgo.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		
		MidasPlantillaBase plantillaDocAnexos = new PL2DocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaDocAnexos = null;
		MidasPlantillaBase plantillaTextosAdicionales = new PL14TextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaTextosAdicionales = null;
		for(IncisoCotizacionDTO incisoCot : listaIncisos){
			MidasPlantillaBase plantillaDetalleInciso = new PL3DetalleInciso(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantillaDetalleInciso.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
			/**
			 * 19/01/2010. Jos� Luis Arellano. La impresi�n de paquete familiar no debe mostrar la plantilla de subincisos por inciso 
			 */
			//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
//			List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
//			for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
//				MidasPlantillaBase plantilla4 = new PL4DetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
//				try {
//					reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
//				} catch (SystemException e) {}
//				if (reporteTMP !=null){
//					getListaPlantillas().add(reporteTMP);
//					reporteTMP = null;
//				}
//			}
		}
		//01/12/09. La plantilla "guia que hacer en caso..." es un documento anexo, no se debe imprimir con la cotizacion.
//		MidasPlantillaBase guiaEnCasoDeSiniestro = new PL5GuiaEnCasoDeSiniestro(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
//		try {
//			reporteTMP = guiaEnCasoDeSiniestro.obtenerReporte(nombreUsuario);
//		} catch (SystemException e) {}
//		if (reporteTMP !=null){
//			getListaPlantillas().add(reporteTMP);
//		}
	}
	
	private void generarReporteUnicaUbicacion(String nombreUsuario){
		byte[] reporteTMP = null;
		IncisoCotizacionDTO incisoCot = listaIncisos.get(0);
		PL6UnicaUbicacion plantillaUnicaUbicacion = new PL6UnicaUbicacion(cotizacionDTO,incisoCot,getMapaParametrosGeneralesPlantillas(),this);
		try {
			plantillaUnicaUbicacion.setMostrarLeyendaSANivelCobertura(Boolean.FALSE);
			reporteTMP = plantillaUnicaUbicacion.obtenerReporte(nombreUsuario);
		} catch (SystemException e) {}
		if (reporteTMP !=null)
			getListaPlantillas().add(reporteTMP);
		reporteTMP = null;
		/**
		 * 19/01/2010. Jos� Luis Arellano. La impresi�n de paquete familiar no debe mostrar la plantilla de subincisos por inciso 
		 */
		//Se debe obtener la lista de secciones de cada inciso para generar el reporte de subincisos
//		List<SeccionCotizacionDTO> listaSeccionCotContratadas = this.obtenerSeccionesContratadas(incisoCot.getId().getNumeroInciso()); //SeccionCotizacionDN.getInstancia().listarSeccionesContratadas(idToCotizacion, incisoCot.getId().getNumeroInciso());
//		for(SeccionCotizacionDTO seccion : listaSeccionCotContratadas){
//			MidasPlantillaBase plantilla4 = new PL4DetalleSubincisoPorInciso(cotizacionDTO,incisoCot,seccion.getId().getIdToSeccion(),getMapaParametrosGeneralesPlantillas(),this);
//			try {
//				reporteTMP = plantilla4.obtenerReporte(nombreUsuario);
//			} catch (SystemException e) {}
//			if (reporteTMP !=null){
//				getListaPlantillas().add(reporteTMP);
//				reporteTMP = null;
//			}
//		}
		MidasPlantillaBase plantillaDocAnexos = new PL2DocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (SystemException e) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		MidasPlantillaBase plantillaTextosAdicionales = new PL14TextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (SystemException e) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		//01/12/09. La plantilla "guia que hacer en caso..." es un documento anexo, no se debe imprimir con la cotizacion.
//		MidasPlantillaBase guiaEnCasoDeSiniestro = new PL5GuiaEnCasoDeSiniestro(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
//		try {
//			reporteTMP = guiaEnCasoDeSiniestro.obtenerReporte(nombreUsuario);
//		} catch (SystemException e) {}
//		if (reporteTMP !=null)
//			getListaPlantillas().add(reporteTMP);
	}
}
