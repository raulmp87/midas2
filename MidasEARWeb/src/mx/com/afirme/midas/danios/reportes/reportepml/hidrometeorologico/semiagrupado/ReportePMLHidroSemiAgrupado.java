package mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.semiagrupado;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepml.AtributoEntradaDTO_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.DocumentoPMLBase;
import mx.com.afirme.midas.danios.reportes.reportepml.ParametroSalidaSP_PML;
import mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.ReportePMLHidroDN;
import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.sistema.creadorxls.MidasHojaXLS;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;

/**
 * @author Jos� Luis Arellano
 */
public class ReportePMLHidroSemiAgrupado extends DocumentoPMLBase{

	public ReportePMLHidroSemiAgrupado(Integer claveTipoReporte){
		super(claveTipoReporte);
		this.nombreReporte = "HIDRO_Colectivos"+obtenerSufijoReporte();
	}
	
	@SuppressWarnings("unchecked")
	public byte[] obtenerReporte(String nombreUsuario) throws Exception{
		MidasHojaXLS hojaIncisos = generarHojaIncisos();
		MidasHojaXLS hojaDatosGenerales = generarHojaDatosGenerales();
		MidasHojaXLS hojaDatosFinancieros = generarHojaDatosFinancieros();

		
		hojaIncisos.iniciarProcesamientoArchivoXLS();
		hojaDatosGenerales.iniciarProcesamientoArchivoXLS();
		hojaDatosFinancieros.iniciarProcesamientoArchivoXLS();
		
		List listaRegistrosHojaIncisos = ReportePMLHidroDN.getInstancia().obtenerReportePMLHidroSemiAgrupadoIncisos(claveTipoReporte,nombreUsuario);
		establecerNumeroRegistro(listaRegistrosHojaIncisos,false);
		establecerCamposVacios(listaRegistrosHojaIncisos);
		hojaIncisos.insertarFilasArchivoXLS(listaRegistrosHojaIncisos);
		listaRegistrosHojaIncisos = null;
		
		List listaRegistrosHojaDatosG = ReportePMLHidroDN.getInstancia().obtenerReportePMLHidroSemiAgrupadoDatosGenerales(claveTipoReporte,nombreUsuario);
		establecerNumeroRegistro(listaRegistrosHojaDatosG,false);
		hojaDatosGenerales.insertarFilasArchivoXLS(listaRegistrosHojaDatosG);
		listaRegistrosHojaDatosG = null;
		
		List listaRegistrosHojaDatosF = ReportePMLHidroDN.getInstancia().obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(claveTipoReporte,nombreUsuario);
		establecerNumeroRegistro(listaRegistrosHojaDatosF,false);
		establecerCoaseguroVacio(listaRegistrosHojaDatosF);
		hojaDatosFinancieros.insertarFilasArchivoXLS(listaRegistrosHojaDatosF);
		listaRegistrosHojaDatosF = null;
		
		return finalizarProcesamientoArchivoXLS();
	}
	
	@SuppressWarnings("unchecked")
	protected void establecerCoaseguroVacio(List listaRegistros){
		if(listaRegistros != null && !listaRegistros.isEmpty()){
			for(Object registro : listaRegistros){
				BigDecimal coaseguro = null;
				if(registro instanceof ReportePMLTEVDTO){
					coaseguro = ((ReportePMLTEVDTO)registro).getCoaseguro();
					if(coaseguro != null && coaseguro.compareTo(BigDecimal.ZERO) == 0){
						((ReportePMLTEVDTO)registro).setCoaseguro(null);
					}
				}
				else if (registro instanceof ReportePMLHidroDTO){
					coaseguro = ((ReportePMLHidroDTO)registro).getCoaseguro();
					if(coaseguro != null && coaseguro.compareTo(BigDecimal.ZERO) == 0){
						((ReportePMLHidroDTO)registro).setCoaseguro(null);
					}
				}
			}
		}
	}
	
	protected void establecerCamposVacios(List<ReportePMLHidroDTO> listaRegistros){
		if(listaRegistros != null && !listaRegistros.isEmpty()){
			for(ReportePMLHidroDTO registro : listaRegistros){
				BigDecimal valor = null;
				//porcentaje retencion
				valor = registro.getPorcentajeRetencion();
				registro.setPorcentajeRetencion(regresaNullSiEsCero(valor));
				
				//monto primer riesgo
				valor = registro.getMontoPrimerRiesgo();
				registro.setMontoPrimerRiesgo(regresaNullSiEsCero(valor));
				
				//consec limite maximo
				valor = registro.getConsecLimiteMaximo();
				registro.setConsecLimiteMaximo(regresaNullSiEsCero(valor));
				
				//convenio limite maximo
				valor = registro.getConvenioLimiteMaximo();
				registro.setConvenioLimiteMaximo(regresaNullSiEsCero(valor));
				
				//consec periodo cobertura
				Integer valorI = registro.getConsecPeriodoCobertura();
				registro.setConsecPeriodoCobertura((valorI != null && valorI.compareTo(new Integer(0)) == 0) ? null : valorI);
				
				//Tipo primer riesgo
				Short valorS = registro.getTipoPrimerRiesgo();
				registro.setTipoPrimerRiesgo((valorS != null && valorS.compareTo(new Short("0")) == 0) ? null : valorS);
			}
		}
	}
	
	private BigDecimal regresaNullSiEsCero(BigDecimal valor){
		return (valor != null && valor.compareTo(BigDecimal.ZERO) == 0) ? null : valor;
	}
	
	private MidasHojaXLS generarHojaIncisos(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_REGISTRO
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
				,AtributoEntradaDTO_PML.INM_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONT_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONSEC_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.CONVENIO_VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.TIPO_PRIMER_RIESGO
				,AtributoEntradaDTO_PML.MONTO_PRIMER_RIESGO
				,AtributoEntradaDTO_PML.CONSEC_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.CONSEC_PERIODO_COBERTURA
				,AtributoEntradaDTO_PML.CONVENIO_LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.INM_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONT_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONSEC_DEDUCIBLE
				,AtributoEntradaDTO_PML.CONVENIO_DEDUCIBLE
				,AtributoEntradaDTO_PML.INM_COASEGURO
				,AtributoEntradaDTO_PML.CONT_COASEGURO
				,AtributoEntradaDTO_PML.CONSEC_COASEGURO
				,AtributoEntradaDTO_PML.CONVENIO_COASEGURO
				,AtributoEntradaDTO_PML.CLAVE_ESTADO
				,AtributoEntradaDTO_PML.CODIGO_POSTAL
				,AtributoEntradaDTO_PML.LONGITUD
				,AtributoEntradaDTO_PML.LATITUD
				,AtributoEntradaDTO_PML.PRIMERA_LINEA_MAR
				,AtributoEntradaDTO_PML.PRIMERA_LINEA_LAGO
				,AtributoEntradaDTO_PML.SOBREELEVACION_DESPLANTE
				,AtributoEntradaDTO_PML.RUGOSIDAD
				,AtributoEntradaDTO_PML.USO_INMUEBLE
				,AtributoEntradaDTO_PML.NUM_PISOS
				,AtributoEntradaDTO_PML.PISO
				,AtributoEntradaDTO_PML.TIPO_CUBIERTA
				,AtributoEntradaDTO_PML.FORMA_CUBIERTA
				,AtributoEntradaDTO_PML.IRRE_PLANTA
				,AtributoEntradaDTO_PML.OBJETOS_CERCA
				,AtributoEntradaDTO_PML.AZOTEA
				,AtributoEntradaDTO_PML.TAMANO_CRISTAL
				,AtributoEntradaDTO_PML.TIPO_VENTANAS
				,AtributoEntradaDTO_PML.TIPO_DOMOS
				,AtributoEntradaDTO_PML.SOPORTE_VENTANA
				,AtributoEntradaDTO_PML.PROCENTAJE_CRISTAL_FACHADAS
				,AtributoEntradaDTO_PML.PORCENTAJE_DOMOS
				,AtributoEntradaDTO_PML.OTROS_FACHADA
				,AtributoEntradaDTO_PML.MUROS_CONTENCION
				,AtributoEntradaDTO_PML.PRIMA
				,AtributoEntradaDTO_PML.CEDIDA
				,AtributoEntradaDTO_PML.RETENIDA
				,AtributoEntradaDTO_PML.MONEDA
				,AtributoEntradaDTO_PML.RSR_T
				,AtributoEntradaDTO_PML.OFI_EMI
				,AtributoEntradaDTO_PML.VALOR_ASEGURABLE
				,AtributoEntradaDTO_PML.VALOR_RETENIDO
				,AtributoEntradaDTO_PML.INCISO
				,AtributoEntradaDTO_PML.ZONA_AMIS
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_REGISTRO
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
				,ParametroSalidaSP_PML.INM_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONT_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONSEC_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.CONVENIO_VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.TIPO_PRIMER_RIESGO
				,ParametroSalidaSP_PML.MONTO_PRIMER_RIESGO	
				,ParametroSalidaSP_PML.CONSEC_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.CONSEC_PERIODO_COBERTURA
				,ParametroSalidaSP_PML.CONVENIO_LIMITE_MAXIMO
				,ParametroSalidaSP_PML.INM_DEDUCIBLE
				,ParametroSalidaSP_PML.CONT_DEDUCIBLE
				,ParametroSalidaSP_PML.CONSEC_DEDUCIBLE
				,ParametroSalidaSP_PML.CONVENIO_DEDUCIBLE
				,ParametroSalidaSP_PML.INM_COASEGURO
				,ParametroSalidaSP_PML.CONT_COASEGURO
				,ParametroSalidaSP_PML.CONSEC_COASEGURO
				,ParametroSalidaSP_PML.CONVENIO_COASEGURO
				,ParametroSalidaSP_PML.CLAVE_ESTADO
				,ParametroSalidaSP_PML.CODIGO_POSTAL
				,ParametroSalidaSP_PML.LONGITUD
				,ParametroSalidaSP_PML.LATITUD
				,ParametroSalidaSP_PML.PRIMERA_LINEA_MAR
				,ParametroSalidaSP_PML.PRIMERA_LINEA_LAGO
				,ParametroSalidaSP_PML.SOBREELEVACION_DESPLANTE
				,ParametroSalidaSP_PML.RUGOSIDAD
				,ParametroSalidaSP_PML.USO_INMUEBLE
				,ParametroSalidaSP_PML.NUM_PISOS
				,ParametroSalidaSP_PML.PISO
				,ParametroSalidaSP_PML.TIPO_CUBIERTA
				,ParametroSalidaSP_PML.FORMA_CUBIERTA
				,ParametroSalidaSP_PML.IRRE_PLANTA
				,ParametroSalidaSP_PML.OBJETOS_CERCA
				,ParametroSalidaSP_PML.AZOTEA
				,ParametroSalidaSP_PML.TAMANO_CRISTAL
				,ParametroSalidaSP_PML.TIPO_VENTANAS
				,ParametroSalidaSP_PML.TIPO_DOMOS
				,ParametroSalidaSP_PML.SOPORTE_VENTANA
				,ParametroSalidaSP_PML.PROCENTAJE_CRISTAL_FACHADAS
				,ParametroSalidaSP_PML.PORCENTAJE_DOMOS
				,ParametroSalidaSP_PML.OTROS_FACHADA
				,ParametroSalidaSP_PML.MUROS_CONTENCION
				,ParametroSalidaSP_PML.PRIMA
				,ParametroSalidaSP_PML.CEDIDA
				,ParametroSalidaSP_PML.RETENIDA
				,ParametroSalidaSP_PML.MONEDA
				,ParametroSalidaSP_PML.RSR_T
				,ParametroSalidaSP_PML.OFI_EMI
				,ParametroSalidaSP_PML.VALOR_ASEGURABLE
				,ParametroSalidaSP_PML.VALOR_RETENIDO
				,ParametroSalidaSP_PML.INCISO
				,ParametroSalidaSP_PML.ZONA_AMIS
		};
		
		MidasHojaXLS hojaPMLHidroIncisos = new MidasHojaXLS("Incisos",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidroIncisos ;
	}
	
	private MidasHojaXLS generarHojaDatosGenerales(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.TIPO_POLIZA
				,AtributoEntradaDTO_PML.FECHA_INICIO
				,AtributoEntradaDTO_PML.FECHA_FIN
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.TIPO_POLIZA
				,ParametroSalidaSP_PML.FECHA_INICIO
				,ParametroSalidaSP_PML.FECHA_FIN
		};
		
		MidasHojaXLS hojaPMLHidro = new MidasHojaXLS("DatosGenerales",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidro;
	}
	
	private MidasHojaXLS generarHojaDatosFinancieros(){
		String[] atributosDTO = {
				AtributoEntradaDTO_PML.NUM_POLIZA
				,AtributoEntradaDTO_PML.NUM_CAPA
				,AtributoEntradaDTO_PML.PORCENTAJE_RETENCION
				,AtributoEntradaDTO_PML.LIMITE_MAXIMO
				,AtributoEntradaDTO_PML.COASEGURO
		};
		
		String[] nombreColumnas = {
				ParametroSalidaSP_PML.NUM_POLIZA
				,ParametroSalidaSP_PML.NUM_CAPA
				,ParametroSalidaSP_PML.PORCENTAJE_RETENCION
				,ParametroSalidaSP_PML.LIMITE_MAXIMO
				,ParametroSalidaSP_PML.COASEGURO
		};
		
		MidasHojaXLS hojaPMLHidro = new MidasHojaXLS("DatosFinancieros",nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
		return hojaPMLHidro;
	}
}
