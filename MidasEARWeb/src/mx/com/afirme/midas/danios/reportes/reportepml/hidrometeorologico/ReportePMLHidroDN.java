package mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePMLHidroDN {

	private static final ReportePMLHidroDN INSTANCIA = new ReportePMLHidroDN();
	
	public static ReportePMLHidroDN getInstancia() {
		return INSTANCIA;
	}
	
	@Deprecated
	public List<ReportePMLHidroDTO> obtieneReportePMLHidro(Date fechaCorte, Double tipoCambio, String nombreUsuario)  
			throws SystemException {
		
		ReportePMLHidroSN reportePMLHidroSN = new ReportePMLHidroSN();
		
		ReportePMLHidroDTO filtroReporte = new ReportePMLHidroDTO();
		
		filtroReporte.setFechaCorte(fechaCorte);
		filtroReporte.setTipoCambio(tipoCambio);
		
		return reportePMLHidroSN.obtieneReportePMLHidro(filtroReporte, nombreUsuario);
	}
	
	public boolean calcularReportePMLHidro(ReportePMLHidroDTO filtroReporte, String nombreUsuario) throws SystemException{
		return new ReportePMLHidroSN().calcularReportePMLHidro(filtroReporte, nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroIndependientes(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLHidroSN().obtenerReportePMLHidroIndependientes(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoIncisos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLHidroSN().obtenerReportePMLHidroSemiAgrupadoIncisos(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosGenerales(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLHidroSN().obtenerReportePMLHidroSemiAgrupadoDatosGenerales(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLHidroSN().obtenerReportePMLHidroSemiAgrupadoDatosFinancieros(claveTipoReporte,nombreUsuario);
	}
	
	public List<ReportePMLHidroDTO> obtenerReportePMLHidroTecnicos(Integer claveTipoReporte,String nombreUsuario)throws Exception{
		return new ReportePMLHidroSN().obtenerReportePMLHidroTecnicos(claveTipoReporte,nombreUsuario);
	}
}
