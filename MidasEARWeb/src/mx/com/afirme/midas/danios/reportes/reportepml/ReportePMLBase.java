package mx.com.afirme.midas.danios.reportes.reportepml;

import java.util.Date;

public abstract class ReportePMLBase{
	protected Date fechaCorte;
	protected Double tipoCambio;
	protected byte tipoReporte;
	
//	private static final short horasDesactualizacionPermitidas = 2;
	
	public static enum TipoReportePML {
		HIDRO_INDEPENDIENTES,
		HIDRO_SEMI_AGRUPADO,
		HIDRO_TECNICOS,
		TEV_INDEPENDIENTES,
		TEV_SEMI_AGRUPADO,
		TEV_TECNICOS};
	
	public static enum PlantillasReportePML {HIDRO_INDEPENDIENTES_,
		HIDRO_SEMI_AGRUPADO_I,HIDRO_SEMI_AGRUPADO_DG,HIDRO_SEMI_AGRUPADO_DF,
		HIDRO_TECNICOS,
		TEV_INDEPENDIENTES_CH,TEV_INDEPENDIENTES_GR,TEV_INDEPENDIENTES_RN,
		TEV_SEMI_AGRUPADO_CH,TEV_SEMI_AGRUPADO_GR,TEV_SEMI_AGRUPADO_RN,
		TEV_TECNICOS};
	
	public abstract byte[] obtenerReportePML(TipoReportePML tipoReportePML);
	
	protected byte[] obtenerReporte(){
		return null;
	}
	
//	public synchronized void calcularReportePMLTEV() throws SystemException{
//		boolean actualizarReporte = true;
//		if(reportePMLTEVActualizado && fechaPMLTEVCalculado != null){
//			Date fechaActual = new Date();
//			double diasDiferencia = UtileriasWeb.obtenerDiasEntreFechas(fechaPMLTEVCalculado, fechaActual);
//			if(diasDiferencia < 1){
//				Calendar calendarPMLCalculado = Calendar.getInstance();
//				calendarPMLCalculado.setTime(fechaPMLTEVCalculado);
//				
//				Calendar calendarActual = Calendar.getInstance();
//				
//				int horasDiferencia = calendarActual.get(Calendar.HOUR_OF_DAY) - calendarPMLCalculado.get(Calendar.HOUR_OF_DAY);
//				
//				if(horasDiferencia <= horasDesactualizacionPermitidas)
//					actualizarReporte = false;
//			}
//		}
//		
//		if(actualizarReporte){
//			
////			ReportePMLTEVDN.getInstancia().calcularReportePMLTEV(filtroReporte, nombreUsuario)
//		}
//	}
	
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public byte getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(byte tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
}

//class CreacionReportePML{
//	private Date fechaCorte;
//	private Double tipoCambio;
//	
//}