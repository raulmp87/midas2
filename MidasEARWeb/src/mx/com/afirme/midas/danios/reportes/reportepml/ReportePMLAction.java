package mx.com.afirme.midas.danios.reportes.reportepml;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.ReportePMLHidroDN;
import mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.independiente.ReportePMLHidroIndependiente;
import mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.semiagrupado.ReportePMLHidroSemiAgrupado;
import mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico.tecnico.ReportePMLHidroTecnico;
import mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.ReportePMLTEVDN;
import mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.independiente.ReportePMLTEVIndependiente;
import mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.semiagrupado.ReportePMLTEVSemiAgrupado;
import mx.com.afirme.midas.danios.reportes.reportepml.terremotoerupcionvolcanica.tecnico.ReportePMLTEVTecnico;
import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.danios.reportes.reportepmltev.ReportePMLTEVDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportePMLAction extends MidasMappingDispatchAction {

	public ActionForward mostrarReportePML(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String tipoReporte = request.getParameter("tipoReporte");
		if(UtileriasWeb.esCadenaVacia(tipoReporte))
			tipoReporte="terremoto";
		ReportePMLForm reportePmlform = (ReportePMLForm)form;
		reportePmlform.setTipoReporte(tipoReporte);
		if(tipoReporte.equalsIgnoreCase("terremoto"))
			reportePmlform.setTituloReporte("Reporte PML Terremoto y Erupci�n volc�nica.");
		else
			reportePmlform.setTituloReporte("Reporte PML Hidrometeorol�gico.");

		if(UtileriasWeb.esCadenaVacia(reportePmlform.getTipoCambio())){
			Double tipoCambioActual = null;
			try{
				Calendar actual = Calendar.getInstance();
				tipoCambioActual = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorMes(actual.get(Calendar.MONTH), actual.get(Calendar.YEAR), (short)Sistema.MONEDA_DOLARES);
			}catch(Exception e){}
			if(tipoCambioActual != null)
				reportePmlform.setTipoCambio(tipoCambioActual.toString());
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	@SuppressWarnings("deprecation")
	public ActionForward generarReportePML(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
//		String forward = Sistema.EXITOSO;
		String claveMensaje = Sistema.EXITO;
		String mensaje = null;
		ReportePMLForm reportePMLForm = (ReportePMLForm)form;
		if(UtileriasWeb.esCadenaVacia(reportePMLForm.getTipoReporte()))
			reportePMLForm.setTipoReporte("terremoto");
		try {
//			MidasReporteBase reportePML;
			Date fechaCorte = new Date();
			String datosFecha[] = null;
			Double tipoCambio = null;
//			byte[] reporte = null;
			boolean error = false;
			if(!UtileriasWeb.esCadenaVacia(reportePMLForm.getFechaCorte()) && !UtileriasWeb.esCadenaVacia(reportePMLForm.getTipoCambio()) &&
					!UtileriasWeb.esCadenaVacia(reportePMLForm.getClaveTipoReporte())){
				try{
					datosFecha = reportePMLForm.getFechaCorte().split("/");
				}catch(Exception e){}
				if(datosFecha != null){
					fechaCorte.setDate(Integer.valueOf(datosFecha[0]));
					fechaCorte.setMonth(Integer.valueOf(datosFecha[1])-1);
					fechaCorte.setYear(Integer.valueOf(datosFecha[2])-1900);
					
					try{
						tipoCambio = Double.valueOf(reportePMLForm.getTipoCambio());
					}catch(NumberFormatException e){}
					if(tipoCambio != null){
						String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
						if(reportePMLForm.getTipoReporte().equalsIgnoreCase("terremoto")){
							ReportePMLTEVDTO filtroReporte = new ReportePMLTEVDTO();
							filtroReporte.setFechaCorte(fechaCorte);
							filtroReporte.setTipoCambio(tipoCambio);
							filtroReporte.setClaveTipoReporte(new Integer(reportePMLForm.getClaveTipoReporte()));
							error = !ReportePMLTEVDN.getInstancia().calcularReportePMLTEV(filtroReporte, nombreUsuario);
//							reportePML = new ReportePMLTerremotoErupcion(fechaCorte,tipoCambio);
						}
						else{
							ReportePMLHidroDTO filtroReporte = new ReportePMLHidroDTO();
							filtroReporte.setFechaCorte(fechaCorte);
							filtroReporte.setTipoCambio(tipoCambio);
							filtroReporte.setClaveTipoReporte(new Integer(reportePMLForm.getClaveTipoReporte()));
							error = !ReportePMLHidroDN.getInstancia().calcularReportePMLHidro(filtroReporte, nombreUsuario);
//							reportePML = new ReportePMLHidro(fechaCorte,tipoCambio);
						}
//						reporte = reportePML.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
					}
					else	error = true;
				}
				else	error = true;
			}
			else	error = true;
			if ( !error /*&& reporte != null*/){
//				String nombreArchivo = "ReportePML_"+reportePMLForm.getTipoReporte()+"_"+datosFecha[0]+"-"+datosFecha[1]+"-"+datosFecha[2];
//				this.writeBytes(response, reporte, Sistema.TIPO_XLS, nombreArchivo);
//				return null;
				mensaje = "El reporte se gener&oacute; correctamente.";
			}
			else{
				
				claveMensaje = Sistema.ERROR;
				mensaje = "Ocurr&oacute; un error al intentar generar el reporte.";
			}
		} catch (Exception e) {
//			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
//			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
//			forward = "errorImpresion";
			LogDeMidasWeb.log("Error al intentar generar el reporte PML", Level.SEVERE, e);
			claveMensaje = Sistema.ERROR;
			mensaje = "Ocurr&oacute; un error al intentar generar el reporte.";
		}finally{
			UtileriasWeb.imprimeMensajeXML(claveMensaje, mensaje, response);
		}
		return null;
	}
	
//	@SuppressWarnings("deprecation")
	public ActionForward obtenerReportePML(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		String tipoReporte = request.getParameter("tipoReporte");
		String claveTipoReporte = request.getParameter("claveTipoReporte");
		byte[] reporte = null;
		boolean error = false;
		try{
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			if(!UtileriasWeb.esCadenaVacia(tipoReporte) && !UtileriasWeb.esCadenaVacia(claveTipoReporte)){
				Integer claveTipoRepInt = new Integer(claveTipoReporte);
				DocumentoPMLBase reportePML = null;
				if(tipoReporte.equals("TEV_Ind")){
					reportePML = new ReportePMLTEVIndependiente(claveTipoRepInt);
				}
				else if(tipoReporte.equals("TEV_SemiAgr")){
					reportePML = new ReportePMLTEVSemiAgrupado(claveTipoRepInt);
				}
				else if(tipoReporte.equals("TEV_Tec")){
					reportePML = new ReportePMLTEVTecnico(claveTipoRepInt);
				}
				else if(tipoReporte.equals("HIDRO_Ind")){
					reportePML = new ReportePMLHidroIndependiente(claveTipoRepInt);
				}
				else if(tipoReporte.equals("HIDRO_SemiAgr")){
					reportePML = new ReportePMLHidroSemiAgrupado(claveTipoRepInt);
				}
				else if(tipoReporte.equals("HIDRO_Tec")){
					reportePML = new ReportePMLHidroTecnico(claveTipoRepInt);
				}
				else{
					//tipo reporte no v�lido
					error = true;
				}
				if ( !error && reportePML != null){
					reporte = reportePML.obtenerReporte(nombreUsuario);
					String nombreArchivo = reportePML.getNombreReporte();
					this.writeBytes(response, reporte, Sistema.TIPO_XLS, nombreArchivo,".xlsx");
					return null;
				}
				else{
					throw new SystemException("Ocurrio un error al generar el reporte.");
				}
			}
			else{
				throw new SystemException("No se recibi� par�metro v�lido.");
			}
		}
		catch(Exception e){
			LogDeMidasWeb.log("Error al generar reporte PML", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = "errorImpresion";
		}
		return mapping.findForward(forward);
	}
}
