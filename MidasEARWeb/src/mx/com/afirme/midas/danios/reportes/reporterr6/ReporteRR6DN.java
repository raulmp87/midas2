package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteRR6DN {

	private static final ReporteRR6DN INSTANCIA = new ReporteRR6DN();
	
	public static ReporteRR6DN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReporteRR6DTO> obtenerReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario)
			throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();
		
		return reporteReaseguroSN.obtenerReporte(fechaIni, fechaFinal, tipoCambio, reporte, nombreUsuario);
	}
	
	public void procesarReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario)
	throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();
		
		reporteReaseguroSN.procesarReporte(fechaIni, fechaFinal, tipoCambio, reporte, nombreUsuario);
}
	
	public List<ReporteRR6DTO> listarTodos(String fechaCorte) throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();
		return reporteReaseguroSN.listarTodos(fechaCorte);
	}
	
	public ReporteRR6DTO getPorId(Integer id) throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();
		return reporteReaseguroSN.getPorId(id);
	}
	
	public void borrar(ReporteRR6DTO reporteDTO) throws SystemException {
		ReporteRR6SN solicitudSN = new ReporteRR6SN();
		solicitudSN.borrar(reporteDTO);
	}
	
	public List<CargaRR6DTO> listarCargas(String fechaCorte) throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();
		return reporteReaseguroSN.listarCargas(fechaCorte);
	}
	
	public void actualizarEstatusCarga(int idCarga, int estatusCarga) throws SystemException {
		ReporteRR6SN solicitudSN = new ReporteRR6SN();
		solicitudSN.actualizarEstatusCarga(idCarga, estatusCarga);
	}
	
	public int obtenerRegistros(Date fechaIni, Date fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario)
	throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();

		return reporteReaseguroSN.obtenerRegistros(fechaIni, fechaFinal, tipoCambio, reporte, nombreUsuario);
	}
	
	public List<Object[]> obtenerReporteExcel(Date fechaFinal)
	throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();

		return reporteReaseguroSN.obtenerReporteExcel(fechaFinal);
	}
	
	public List<String> obtenerArchivos(Date fechaFinal, int tipoArchivo)
	throws SystemException {
	ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();

	return reporteReaseguroSN.obtenerArchivos(fechaFinal, tipoArchivo);
	}
	
	public int obtenerRegistrosSA(Date fechaFinal)
	throws SystemException {
		ReporteRR6SN reporteReaseguroSN = new ReporteRR6SN();

		return reporteReaseguroSN.obtenerRegistrosSA(fechaFinal);
	}
}
