package mx.com.afirme.midas.danios.reportes.reportepml.hidrometeorologico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.reportepmlhidro.ReportePMLHidroDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

public class PL18PlantillaPMLHidro extends MidasPlantillaBase{
	private List<ReportePMLHidroDTO> listaRegistrosPMLHidro;
	public PL18PlantillaPMLHidro(List<ReportePMLHidroDTO> listaRegistrosPMLHidro){
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.PML.hidro.paquete"));
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.danios.reportes.PML.hidro.plantilla"));
		this.listaRegistrosPMLHidro = listaRegistrosPMLHidro;
		setTipoReporte( Sistema.TIPO_XLS );
	}
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		if(listaRegistrosPMLHidro == null || listaRegistrosPMLHidro.isEmpty())
			return null;
		super.setListaRegistrosContenido(new ArrayList<Object>());
		getListaRegistrosContenido().addAll(listaRegistrosPMLHidro);
		
		setParametrosVariablesReporte(new HashMap<String,Object>());
		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
	    try {
			super.setByteArrayReport( generaReporte(getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			setByteArrayReport( null );
			throw new SystemException("Ocurri� un error al generar el reporte: "+e.getMessage());
		}
		
		return getByteArrayReport();
	}
}
