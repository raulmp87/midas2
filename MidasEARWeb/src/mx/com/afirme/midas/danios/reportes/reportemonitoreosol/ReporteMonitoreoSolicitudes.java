package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteMonitoreoSolicitudes extends MidasReporteBase{
    
    ReporteMonitoreoSolicitudesForm reporteMonitoreoSolicitudesForm;
	
	public ReporteMonitoreoSolicitudes(ReporteMonitoreoSolicitudesForm reporteMonitoreoSolicitudesForm){
		this.reporteMonitoreoSolicitudesForm = reporteMonitoreoSolicitudesForm;
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		List<ReporteMonitoreoSolicitudesDTO> listaDeMonitoreoSolicitudes = null;
		try{
		    listaDeMonitoreoSolicitudes=ReporteMonitoreoSolicitudesDN.getInstancia().obtieneReporteMonitoreoSolicitudes
			(reporteMonitoreoSolicitudesForm.getFechaInicio(), 
				reporteMonitoreoSolicitudesForm.getFechaFinal(), 
				reporteMonitoreoSolicitudesForm.getIdProducto(), 
				(UtileriasWeb.esCadenaVacia(reporteMonitoreoSolicitudesForm.getIdOficina()))?0:
				    Integer.valueOf(reporteMonitoreoSolicitudesForm.getIdOficina()), 
				reporteMonitoreoSolicitudesForm.getCodigoAgente(), 
				UtileriasWeb.esCadenaVacia(reporteMonitoreoSolicitudesForm.getCodigoUsuarioSolicitud())?null:
				    reporteMonitoreoSolicitudesForm.getCodigoUsuarioSolicitud(), 
				claveUsuario);
		}catch (Throwable e) {
		    throw new ExcepcionDeAccesoADatos("Ocurri� un error al recuperar la informaci�n para el reporte: "+e.getMessage());
		}
		
		if(listaDeMonitoreoSolicitudes == null){
			throw new ExcepcionDeAccesoADatos ("Ocurri� un error al recuperar la informaci�n para el reporte.La lista no pudo cargarse");
		}
		if(listaDeMonitoreoSolicitudes.isEmpty()){
			throw new SystemException ("XX");
		}
		PlantillaReporteMonitoreoSolicitudes plantilla = new PlantillaReporteMonitoreoSolicitudes(listaDeMonitoreoSolicitudes);
		byte byteArray[] = null;
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		plantilla.setListaRegistrosContenido(null);
		plantilla = null;
		Runtime.getRuntime().gc();
		return byteArray;
	}
	
}
