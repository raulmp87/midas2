package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteMonitoreoSolicitudesSN {

private ReporteMonitoreoSolicitudesFacadeRemote beanRemoto;
	
	public ReporteMonitoreoSolicitudesSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReporteMonitoreoSolicitudesFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public List<ReporteMonitoreoSolicitudesDTO> obtieneReporteMonitoreoSolicitudes(
			ReporteMonitoreoSolicitudesDTO filtroReporte, String nombreUsuario) throws ExcepcionDeAccesoADatos {
		
		try {
			return beanRemoto.obtieneReporteMonitoreoSolicitudes(filtroReporte, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Reportes.spDAN_RepMonitoreoSolicitudes");
			sb.append("|");
			sb.append("pFechaInicio" + "=" + filtroReporte.getFechaInicio() + ",");
			sb.append("pFechaFin" + "=" + filtroReporte.getFechaFin() + ",");
			sb.append("pIdProducto" + "=" + filtroReporte.getIdProductoSol() + ",");
			sb.append("pCodigoEjecutivo" + "=" + filtroReporte.getCodigoEjecutivoSOL() + ",");
			sb.append("pCodigoAgente" + "=" + filtroReporte.getCodigoAgenteSOL() + ",");
			sb.append("pCodigoUsuarioSolicitud" + "=" + filtroReporte.getCodigoUsuarioSOL() + ",");
						
			UtileriasWeb.enviaCorreoExcepcion("Obtener reporte de Monitoreo de Solicitudes" + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
	}
	
}
