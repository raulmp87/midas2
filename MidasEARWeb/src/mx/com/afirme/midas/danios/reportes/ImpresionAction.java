package mx.com.afirme.midas.danios.reportes;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.ReportePaqueteEmpresarial;
import mx.com.afirme.midas.danios.reportes.cotizacion.endoso.cancelacion.ReporteCotizacionEndosoCancelacion;
import mx.com.afirme.midas.danios.reportes.cotizacion.endoso.rehabilitacion.ReporteCotizacionEndosoRehabilitacion;
import mx.com.afirme.midas.danios.reportes.cotizacion.familiar.ReportePaqueteFamiliar;
import mx.com.afirme.midas.danios.reportes.cotizacion.generica.ReporteCotizacionGenerica;
import mx.com.afirme.midas.danios.reportes.cotizacion.memoriacalculo.ReporteMemoraCalculo;
import mx.com.afirme.midas.danios.reportes.cotizacion.rcfuncionarios.ReporteCotizacionRCFuncionarios;
import mx.com.afirme.midas.danios.reportes.cotizacion.transportes.ReporteCotizacionTransportes;
import mx.com.afirme.midas.danios.reportes.endoso.ReporteCotizacionEndoso;
import mx.com.afirme.midas.danios.reportes.endosocancelable.ReporteEndososCancelables;
import mx.com.afirme.midas.danios.reportes.poliza.ReportePolizaBase;
import mx.com.afirme.midas.danios.reportes.poliza.empresarial.ReportePolizaPaqueteEmpresarial;
import mx.com.afirme.midas.danios.reportes.poliza.endoso.ReportePolizaEndoso;
import mx.com.afirme.midas.danios.reportes.poliza.endoso.cancelacion.ReportePolizaEndosoCancelacion;
import mx.com.afirme.midas.danios.reportes.poliza.endoso.rehabilitacion.ReportePolizaEndosoRehabilitacion;
import mx.com.afirme.midas.danios.reportes.poliza.familiar.ReportePolizaPaqueteFamiliar;
import mx.com.afirme.midas.danios.reportes.poliza.generica.ReportePolizaGenerica;
import mx.com.afirme.midas.danios.reportes.poliza.rcfuncionarios.ReportePolizaRCFuncionarios;
import mx.com.afirme.midas.danios.reportes.poliza.transportes.ReportePolizaTransportes;
import mx.com.afirme.midas.danios.reportes.reportebasesemision.ReporteBasesEmision;
import mx.com.afirme.midas.danios.reportes.reportebasesemision.ReporteBasesEmisionForm;
import mx.com.afirme.midas.danios.reportes.reportepml.ReportePMLForm;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ImpresionAction extends MidasMappingDispatchAction {
	public static final int SEGURO_EMPRESARIAL = 1;
	public static final int TRANSPORTES = 2;
	public static final int GENERICA = 3;
	public static final int ENDOSO = 4;
	public static final int SEGURO_FAMILIAR = 5;
	public static final int ENDOSO_CANCELACION = 6;
	public static final int ENDOSO_REHABILITACION=7;
	public static final int RC_FUNCIONARIOS=8;
	public static final int RC_MEDICOS=9;
	public static final int RC_PROFESIONAL_MEDICOS=10;
	
	public ActionForward imprimirCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String id = request.getParameter("id");
		int tipoPlantilla = 0;
		try {
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(id);
			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(idToCotizacion);
			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			byte[] byteArray = null;
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			boolean cotizacionPoliza = true;
			ReporteCotizacionBase reporteCotizacion = null;
			
			switch (tipoPlantilla){
			//Seguro paquete empresarial
				case SEGURO_EMPRESARIAL:
					reporteCotizacion = new ReportePaqueteEmpresarial(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					break;
			//Transportes
				case TRANSPORTES:
					//plantilla "Cotizaci�n Transportes"
					reporteCotizacion = new ReporteCotizacionTransportes(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					break;
			//Generica
				case GENERICA:
					reporteCotizacion = new ReporteCotizacionGenerica(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					break;
			//Endoso
				case ENDOSO:
					reporteCotizacion = new ReporteCotizacionEndoso(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					cotizacionPoliza=false;
					break;
				case SEGURO_FAMILIAR:
					reporteCotizacion = new ReportePaqueteFamiliar(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					break;
				//Endoso de cancelacion:
				case ENDOSO_CANCELACION:
					reporteCotizacion = new ReporteCotizacionEndosoCancelacion(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					cotizacionPoliza=false;
					break;
				//Endoso de rehabilitacion:
				case ENDOSO_REHABILITACION:
					reporteCotizacion = new ReporteCotizacionEndosoRehabilitacion(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					cotizacionPoliza=false;
					break;
				case RC_FUNCIONARIOS:
					reporteCotizacion = new ReporteCotizacionRCFuncionarios(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					cotizacionPoliza=false;
					break;
				case RC_MEDICOS://06/01/2011 RC MEDICOS, Se imprimir� con el mismo formato que RC Funcionarios
					reporteCotizacion = new ReporteCotizacionRCFuncionarios(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					cotizacionPoliza=false;
					break;
				case RC_PROFESIONAL_MEDICOS://07/01/2011 RC MEDICOS, Se imprimir� con el mismo formato que RC Funcionarios
					reporteCotizacion = new ReporteCotizacionRCFuncionarios(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(usuario.getNombreUsuario());
					cotizacionPoliza=false;
					break;
				default:
					String error = "Error al definir el tipo de plantilla: "+tipoPlantilla+" cotizacion "+id;
					LogDeMidasWeb.log(error, Level.SEVERE, null);
					throw new SystemException(error,20);
			}
			
			this.writeBytes(response, byteArray, Sistema.TIPO_PDF, "Cot-"+(cotizacionPoliza?"Pol":"End")+"_"+reporteCotizacion.getNumeroCotizacion());
			return null;
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			if (e.getErrorCode() == 20 || e.getErrorCode() == 40){
				request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>ReporteCotizacion"+id+",tipo de reporte: "+tipoPlantilla+"</h3><h3>"+e.getMessage()+"</h3>");
			}else{
				request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>ReporteCotizacion"+id+" , tipo de reporte: "+tipoPlantilla+"</h3>");
			}
			return mapping.findForward("errorImpresion");
		} catch (Exception e){
			LogDeMidasWeb.log("Error al imprimir cotizacion: "+id, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>ReporteCotizacion"+id+" , tipo de reporte: "+tipoPlantilla+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	public static byte[] imprimirCotizacion(BigDecimal idToCotizacion, String nombreUsuario) throws SystemException, IOException {
		byte[] byteArray = null;
		int tipoPlantilla = 0;
			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			ReporteCotizacionBase reporteCotizacion = null;
			switch (tipoPlantilla){
			//Seguro paquete empresarial
				case SEGURO_EMPRESARIAL:
					reporteCotizacion = new ReportePaqueteEmpresarial(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
			//Transportes
				case TRANSPORTES:
					//plantilla "Cotizaci�n Transportes"
					reporteCotizacion = new ReporteCotizacionTransportes(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
			//Generica
				case GENERICA:
					reporteCotizacion = new ReporteCotizacionGenerica(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
			//Endoso
				case ENDOSO:
					reporteCotizacion = new ReporteCotizacionEndoso(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				case SEGURO_FAMILIAR:
					reporteCotizacion = new ReportePaqueteFamiliar(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				case ENDOSO_CANCELACION:
					reporteCotizacion = new ReporteCotizacionEndosoCancelacion(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				case ENDOSO_REHABILITACION:
					reporteCotizacion = new ReporteCotizacionEndosoCancelacion(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				case RC_FUNCIONARIOS:
					reporteCotizacion = new ReporteCotizacionRCFuncionarios(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				case RC_MEDICOS://06/01/2011 RC MEDICOS, Se imprimir� con el mismo formato que RC Funcionarios
					reporteCotizacion = new ReporteCotizacionRCFuncionarios(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				case RC_PROFESIONAL_MEDICOS://06/01/2011 RC MEDICOS, Se imprimir� con el mismo formato que RC Funcionarios
					reporteCotizacion = new ReporteCotizacionRCFuncionarios(cotizacionDTO);
					byteArray = reporteCotizacion.obtenerReporte(nombreUsuario);
					break;
				default:
					String error = "Error al definir el tipo de plantilla: "+tipoPlantilla+" cotizacion "+idToCotizacion;
					LogDeMidasWeb.log(error, Level.SEVERE, null);
					throw new SystemException(error,20);
			}
		return byteArray;
	}		
	
	public static int calcularTipoPlantillaCotizacion(CotizacionDTO cotizacionDTO){
		int tipo = 0;
		
		try{
			//TODO implementar los otros tipos de endosos "declaracion y rehabilitacion"
			String tipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().toBigInteger().toString();
			if (tipoEndoso != null){
				if (tipoEndoso.equals(Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION+ "")
						|| tipoEndoso.equals(Sistema.SOLICITUD_ENDOSO_DE_DECLARACION+ "")
						|| tipoEndoso.equals(Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO+ ""))
					tipo = ENDOSO;
				else if (tipoEndoso.equals(Sistema.SOLICITUD_ENDOSO_DE_CANCELACION+ "") )
					tipo = ENDOSO_CANCELACION;
				else if (tipoEndoso.equals(Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION+ ""))
					tipo=ENDOSO_REHABILITACION;

			}
		}catch(Exception e){}
		
		List<ParametroGeneralDTO> listaParametro = new ArrayList<ParametroGeneralDTO>();
		String codigoProducto = cotizacionDTO.getSolicitudDTO().getProductoDTO().getCodigo();
		ParametroGeneralDTO parametroGeneral = null;
		try{
			codigoProducto = Integer.valueOf(codigoProducto).toString();
			listaParametro = ParametroGeneralDN.getINSTANCIA().obtenerListaParametroGeneral(BigDecimal.ONE, codigoProducto);
		}catch(Exception e){}
		
		if(!listaParametro.isEmpty()){
			parametroGeneral = listaParametro.get(0);
		}
		
		if (tipo == 0 || (parametroGeneral != null && parametroGeneral.getId().getCodigoParametroGeneral().compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_PRODUCTO) == 0)){
			BigDecimal codigoParametro = parametroGeneral.getId().getCodigoParametroGeneral();
			if (codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_PAQ_EMPRESARIAL) == 0)
				tipo = SEGURO_EMPRESARIAL;
			else if (codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_TRANSPORTES) == 0)
				tipo = TRANSPORTES;
			else if (codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_PAQ_FAMILIAR) == 0)
				tipo = SEGURO_FAMILIAR;
			else if(codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_INCENDIO) == 0)
				tipo = SEGURO_EMPRESARIAL;
			else if (codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_PRODUCTO) == 0){
				String codigoTipoPoliza=null;
				try{
					codigoTipoPoliza = Integer.valueOf(cotizacionDTO.getTipoPolizaDTO().getCodigo()).toString();
					listaParametro = ParametroGeneralDN.getINSTANCIA().obtenerListaParametroGeneral(new BigDecimal(6d), codigoTipoPoliza);
				}catch(Exception e){}
				if(listaParametro != null && !listaParametro.isEmpty()){
					codigoParametro = listaParametro.get(0).getId().getCodigoParametroGeneral();
					BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();
					if(codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_TIPO_POLIZA) == 0 &&
					   (claveTipoEndoso == null || claveTipoEndoso.compareTo(new BigDecimal(Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION)) == 0)){
						tipo = RC_FUNCIONARIOS;
					}
					else if(codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_RCMEDICOS_TIPO_POLIZA) == 0 &&
							   (claveTipoEndoso == null || claveTipoEndoso.compareTo(new BigDecimal(Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION)) == 0)){
						tipo = RC_MEDICOS;
					}
					else if(codigoParametro.compareTo(Sistema.CODIGO_PARAMETRO_GENERAL_IMPRESION_RC_PROFESIONAL_MEDICOS_TIPO_POLIZA) == 0 &&
							   (claveTipoEndoso == null || claveTipoEndoso.compareTo(new BigDecimal(Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION)) == 0)){
						tipo = RC_PROFESIONAL_MEDICOS;
					}
				}
			}
			if(tipo == 0)
				tipo = GENERICA;
		}

		return tipo;
	}
	
	public ActionForward imprimirPoliza(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String id = request.getParameter("id");
		String numeroEndoso = request.getParameter("numeroEndoso");
		int tipoPlantilla = 0;
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(id);
			PolizaDTO polizaDTO;
			CotizacionDTO cotizacionDTO;
			EndosoDTO endosoDTO ;
			Short numeroEndosoShort = null;
			if (numeroEndoso == null){
				polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
				cotizacionDTO = polizaDTO.getCotizacionDTO();
			}
			else{
				if (numeroEndoso.equals("0")){
					polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
					cotizacionDTO = polizaDTO.getCotizacionDTO();
				}
				else{
					numeroEndosoShort = Short.valueOf(numeroEndoso);
					EndosoId endosoId = new EndosoId();
					endosoId.setIdToPoliza(idToPoliza);
					endosoId.setNumeroEndoso(numeroEndosoShort);
					endosoDTO = EndosoDN.getInstancia(usuario.getNombreUsuario()).getPorId(endosoId);
					polizaDTO = endosoDTO.getPolizaDTO();
					cotizacionDTO = CotizacionDN.getInstancia(usuario.getNombreUsuario()).getPorId(endosoDTO.getIdToCotizacion());
				}
			}
			tipoPlantilla = calcularTipoPlantillaCotizacion(cotizacionDTO);
			byte[] byteArray = null;
			ReportePolizaBase reportePoliza;
			switch (tipoPlantilla){
			//Seguro paquete empresarial
				case SEGURO_EMPRESARIAL:
					reportePoliza = new ReportePolizaPaqueteEmpresarial(polizaDTO);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
			//Transportes
				case TRANSPORTES:
					//plantilla "Cotizaci�n Transportes"
					reportePoliza = new ReportePolizaTransportes(polizaDTO);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
			//Generica
				case GENERICA:
					reportePoliza = new ReportePolizaGenerica(polizaDTO);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case ENDOSO:
					reportePoliza =new ReportePolizaEndoso(polizaDTO,cotizacionDTO,numeroEndosoShort);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case SEGURO_FAMILIAR:
					reportePoliza = new ReportePolizaPaqueteFamiliar(polizaDTO);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case ENDOSO_CANCELACION:
					reportePoliza =new ReportePolizaEndosoCancelacion(polizaDTO,cotizacionDTO,numeroEndosoShort);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case ENDOSO_REHABILITACION:
					reportePoliza =new ReportePolizaEndosoRehabilitacion(polizaDTO,cotizacionDTO,numeroEndosoShort);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case RC_FUNCIONARIOS:
					reportePoliza =new ReportePolizaRCFuncionarios(polizaDTO,cotizacionDTO,numeroEndosoShort);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case RC_MEDICOS://06/01/2011 RC Medico se imprime con el mismo formato que RC Funcionarios
					reportePoliza =new ReportePolizaRCFuncionarios(polizaDTO,cotizacionDTO,numeroEndosoShort);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				case RC_PROFESIONAL_MEDICOS://06/01/2011 RC Medico se imprime con el mismo formato que RC Funcionarios
					reportePoliza =new ReportePolizaRCFuncionarios(polizaDTO,cotizacionDTO,numeroEndosoShort);
					byteArray = reportePoliza.obtenerReporte(usuario.getNombreUsuario());
					break;
				default:
					String error = "Error al definir el tipo de plantilla: "+tipoPlantilla+" cotizacion "+id;
					LogDeMidasWeb.log(error, Level.SEVERE, null);
					throw new SystemException(error,20);
			}
//			this.writeBytes(response, byteArray, Sistema.TIPO_PDF, "file");
			
			this.writeBytes(response, byteArray, Sistema.TIPO_PDF, "Poliza"+reportePoliza.getNumeroPoliza());
			return null;
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error al imprimir poliza: "+id, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			if (e.getErrorCode() == 20){
				request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>ReportePoliza"+id+" , tipo de reporte: "+tipoPlantilla+"</h3><h3>"+e.getMessage()+"</h3>");
			} else{
				request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>ReportePoliza"+id+" , tipo de reporte: "+tipoPlantilla+"</h3>");
			}
			return mapping.findForward("errorImpresion");
		} catch (Exception e){
			LogDeMidasWeb.log("Error al imprimir poliza: "+id, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>ReportePoliza"+id+" , tipo de reporte: "+tipoPlantilla+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	public ActionForward imprimirMemoriaCalculoCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		
		String id = request.getParameter("id");
		String numeroInciso = request.getParameter("numeroInciso");
		String tipoReporte = request.getParameter("tipoReporte");
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
						
			Integer numeroIncisoInteger = -1;
			if (numeroInciso != null){
				try{
					numeroIncisoInteger = new Integer(numeroInciso);
				}catch(Exception e){}
			}
			byte[] byteArray = null;
			
			ReporteMemoraCalculo reporteMemoriaCalculo = new ReporteMemoraCalculo(UtileriasWeb.regresaBigDecimal(id),numeroIncisoInteger);
			
			
			if(!UtileriasWeb.esCadenaVacia(tipoReporte) && (
					tipoReporte.equals(Sistema.TIPO_PDF) || tipoReporte.equals(Sistema.TIPO_XLS))){
				reporteMemoriaCalculo.setTipoReporte(tipoReporte);
			}
			else{
				tipoReporte = Sistema.TIPO_XLS;
				reporteMemoriaCalculo.setTipoReporte(tipoReporte);
			}
			
			
			byteArray = reporteMemoriaCalculo.obtenerReporte(usuario.getNombreUsuario());

			this.writeBytes(response, byteArray, tipoReporte, "MemoriaCalculoCot_"+id);
			
			return null;
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al imprimir memoria de c�lculo: "+id, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>Reporte Memoria de c&aacute;lculo. Cotizacion: "+id+" , numeroInciso: "+numeroInciso+"</h3><h3>"+e.getMessage()+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	
	public ActionForward mostrarReporteEndososCancelables(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String fechaCorte = request.getParameter("fechaCorte");
		Date fechaCorteDate = new Date();
		ReportePMLForm formPML = (ReportePMLForm) form;
		try{
			if(!UtileriasWeb.esCadenaVacia(fechaCorte)){
				fechaCorteDate = UtileriasWeb.getFechaFromString(fechaCorte);
			}
		}
		catch(Exception e){
		}
		try {
			formPML.setFechaCorte(UtileriasWeb.getFechaString(fechaCorteDate));
		} catch (Exception e) {
			
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward imprimirEndososCancelables(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String fechaCorte = request.getParameter("fechaCorte");
		Date fechaCorteDate = new Date();
		try{
			fechaCorteDate = UtileriasWeb.getFechaFromString(fechaCorte);
		}
		catch(Exception e){
		}
		try {
			byte[] byteArray = null;
			
			ReporteEndososCancelables reporteCancelables = new ReporteEndososCancelables(fechaCorteDate);
			
			byteArray = reporteCancelables.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));

			this.writeBytes(response, byteArray, Sistema.TIPO_XLS, "RepoteEndososCancelables_"+new SimpleDateFormat("dd/MM/yyyy").format(fechaCorteDate));
			
			return null;
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al generar reporte de endosos cancelables al: "+SimpleDateFormat.getInstance().format(fechaCorteDate), Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>Reporte endosos cancelables al "+
					SimpleDateFormat.getInstance().format(fechaCorteDate)+".</h3><h3>"+e.getCause()+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	public ActionForward generarReporteMovimientoEmision(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		ReporteBasesEmisionForm movimientoEmisionForm = (ReporteBasesEmisionForm)form;
		try {
			ReporteBasesEmision reporteBasesEmision = new ReporteBasesEmision(movimientoEmisionForm);
			byte byteArray[] = reporteBasesEmision.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
			if ( byteArray != null){
				String nombreArchivo = "ReporteBasesEmision_";
				String data[] = null;
				if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaInicio())){
					try{
						data = movimientoEmisionForm.getFechaInicio().split("/");
					}catch(Exception e){}
					if(data != null){
						nombreArchivo = obtenerNombreArchivo(data);
						nombreArchivo += "_";
						data = null;
					}
				}
				if(!UtileriasWeb.esCadenaVacia(movimientoEmisionForm.getFechaFin())){
					try{
						data = movimientoEmisionForm.getFechaFin().split("/");
					}catch(Exception e){}
					if(data != null){
						nombreArchivo = obtenerNombreArchivo(data);
						data = null;
					}
				}
				this.writeBytes(response, byteArray, Sistema.TIPO_XLS, nombreArchivo);
				return null;
			}
			else
				throw new SystemException("Datos de entrada: "+movimientoEmisionForm.toString(),20);
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = "errorImpresion";
		} catch (IOException e) {
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = "errorImpresion";
		}
		return mapping.findForward(forward);
	}

	public String obtenerNombreArchivo(String[] data) {
		return StringUtils.join(data);
	}
}
