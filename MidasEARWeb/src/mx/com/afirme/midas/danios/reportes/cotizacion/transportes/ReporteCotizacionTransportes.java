package mx.com.afirme.midas.danios.reportes.cotizacion.transportes;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.ReporteCotizacionBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL14TextosAdicionales;
import mx.com.afirme.midas.danios.reportes.cotizacion.empresarial.PL2DocumentosAdicionales;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteCotizacionTransportes extends ReporteCotizacionBase{

	public ReporteCotizacionTransportes(CotizacionDTO cotizacionDTO) {
		this.idToCotizacion = cotizacionDTO.getIdToCotizacion();
		this.cotizacionDTO = cotizacionDTO;
		setListaPlantillas(new ArrayList<byte[]>());
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteCotizacionTransportes(claveUsuario);
		return super.obtenerReporte(cotizacionDTO.getCodigoUsuarioCotizacion());
	}
	
	private void generarReporteCotizacionTransportes(String nombreUsuario) throws SystemException{
		setMapaParametrosGeneralesPlantillas(new HashMap<String,Object>());
		consultarInformacionCotizacion(nombreUsuario);
		
		//Poblar los par�metros comunes para todas las plantillas
		poblarParametrosComunes(nombreUsuario,true);
		String abreviaturas,descripcionAbreviaturas,leyendaAbreviaturas;
		abreviaturas = "S/VT embarque\nL.M. embarque\nMin\nMax\nDSMGVDF\nUMA";
		descripcionAbreviaturas = "Sobre valor total del embarque\nL�mite M�ximo de Suma Asegurada del embarque\ncon M�nimo de\n" +
				"Con m�ximo de\nD�as de salario m�nimo general vigente en el Distrito Federal\nUnidad de Medida y Actualización";
		leyendaAbreviaturas = "Donde quiera que aparezca en este contrato las siguientes abreviaturas se entender�:";
		getMapaParametrosGeneralesPlantillas().put("ABREVIATURAS", abreviaturas);
		getMapaParametrosGeneralesPlantillas().put("DESCRIPCION_ABREVIATURAS", descripcionAbreviaturas);
		getMapaParametrosGeneralesPlantillas().put("LEYENDA_ABREVIATURAS", leyendaAbreviaturas);
		
		byte[] reporteTMP=null;

		int tipoPlantillaBienesAsegurados = 0;
		if (cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("especifica") 
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("espec�fica")
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("especifica") != -1 
						|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("espec�fica") != -1){
			tipoPlantillaBienesAsegurados = 1;
		} else if(cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("pronostico") 
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("pron�stico")
					|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("pron�stico") != -1
						|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("pronostico") != -1){
			tipoPlantillaBienesAsegurados = 2;
		} else if(cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("anual") 
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("declaracion") 
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().equalsIgnoreCase("declaraci�n")
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("anual") != -1
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("declaracion") != -1
				|| cotizacionDTO.getTipoPolizaDTO().getNombreComercial().toLowerCase().indexOf("declaraci�n") != -1){
			tipoPlantillaBienesAsegurados = 3;
		}
		for(IncisoCotizacionDTO incisoTMP : listaIncisos){
			
			MidasPlantillaBase plantillaBienesAseguradosPorInciso = null;
			if (tipoPlantillaBienesAsegurados == 1){
				plantillaBienesAseguradosPorInciso = new PL10_BienesAseguradosPorIncisoModEspecifica(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
			} else if (tipoPlantillaBienesAsegurados == 2){
				plantillaBienesAseguradosPorInciso = new PL15_BienesAseguradosPorIncisoModPronostico(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
			} else if (tipoPlantillaBienesAsegurados == 3){
				plantillaBienesAseguradosPorInciso = new PL16_BienesAseguradosPorIncisoModAnual(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
			}
			if(plantillaBienesAseguradosPorInciso != null){
				try {
					reporteTMP = plantillaBienesAseguradosPorInciso.obtenerReporte(nombreUsuario);
				} catch (SystemException e1) {}
				if (reporteTMP !=null){
					getListaPlantillas().add(reporteTMP);
					reporteTMP = null;
				}
			}
			
			MidasPlantillaBase plantillaDedCoasPorInciso = new PL11_DeduciblesCoasegurosPorInciso(cotizacionDTO,incisoTMP,getMapaParametrosGeneralesPlantillas(),this);
			try {
				reporteTMP = plantillaDedCoasPorInciso.obtenerReporte(nombreUsuario);
			} catch (SystemException e1) {}
			if (reporteTMP !=null){
				getListaPlantillas().add(reporteTMP);
				reporteTMP = null;
			}
		}
		
		MidasPlantillaBase plantillaDocAnexos = new PL2DocumentosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		try {
			reporteTMP = plantillaDocAnexos.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaDocAnexos = null;
		MidasPlantillaBase plantillaTextosAdicionales = new PL14TextosAdicionales(cotizacionDTO,getMapaParametrosGeneralesPlantillas());
		try {
			reporteTMP = plantillaTextosAdicionales.obtenerReporte(nombreUsuario);
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
			reporteTMP = null;
		}
		plantillaTextosAdicionales = null;
		
		/**
		 * 31/12/2009. Por indicaciones de Arturo y Euler, no se deben mostrar los documentos anexos en la cotizaci�n, s�lo en la p�liza.
		 * Jos� Luis Arellano B�rcenas
		 */
//			Agregar los documentos anexos.
//			List<DocAnexoCotDTO> listaDocumentosAnexos = new ArrayList<DocAnexoCotDTO>();
//			DocAnexoCotDTO docAnexo = new DocAnexoCotDTO();
//			docAnexo.setCotizacion(cotizacionDTO);
//			listaDocumentosAnexos = DocAnexoCotDN.getInstancia().listarFiltrado(docAnexo);
//			for(DocAnexoCotDTO documento : listaDocumentosAnexos){
//				try{
//				ControlArchivoDTO controlArchivo = ControlArchivoDN.getInstancia().getPorId(documento.getIdToControlArchivo());
//				super.adjuntarDocumentoAnexo(controlArchivo);
//				}catch(Exception e){}
//			}
	}
}
