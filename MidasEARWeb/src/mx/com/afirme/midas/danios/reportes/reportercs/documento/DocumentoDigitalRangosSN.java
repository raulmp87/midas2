package mx.com.afirme.midas.danios.reportes.reportercs.documento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class DocumentoDigitalRangosSN {
	private DocumentoDigitalRangosFacadeRemote beanRemoto;

	public DocumentoDigitalRangosSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoDigitalRangosFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(DocumentoDigitalRangosDTO documentoDigitalSolicitudDTO) {
		try {
			beanRemoto.save(documentoDigitalSolicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DocumentoDigitalRangosDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DocumentoDigitalRangosDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DocumentoDigitalRangosDTO> listarDocumentosSolicitud(
			BigDecimal idCveNegocio) {
		try {
			return beanRemoto.findByProperty("cveNegocio", idCveNegocio.intValue());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DocumentoDigitalRangosDTO documentoDigitalSolicitudDTO) {
		try {
			beanRemoto.delete(documentoDigitalSolicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void update(DocumentoDigitalRangosDTO documentoDigitalSolicitudDTO) {
		try {
			beanRemoto.update(documentoDigitalSolicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
