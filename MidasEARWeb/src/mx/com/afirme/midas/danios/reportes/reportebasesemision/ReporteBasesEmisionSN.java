package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteBasesEmisionSN {

	private ReporteBasesEmisionFacadeRemote beanRemoto;
	
	private TipoCambioFacadeRemote tipoCambioFacade;
	
	public ReporteBasesEmisionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReporteBasesEmisionFacadeRemote.class);
			
			tipoCambioFacade = serviceLocator.getEJB(TipoCambioFacadeRemote.class);
			
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public List<ReporteBasesEmisionDTO> obtieneMovimientosEmision(
			ReporteBasesEmisionDTO movimientoEmision, String nombreUsuario) {
		try {
			
			Calendar fechaFin = Calendar.getInstance();
			fechaFin.setTime(movimientoEmision.getFechaFin());
			fechaFin.add(Calendar.MONTH, 1);
			
			tipoCambioFacade.actualizarTipoCambioMensual(movimientoEmision.getFechaInicio(), fechaFin.getTime(), nombreUsuario);
			
			return beanRemoto.obtieneMovimientosEmision(movimientoEmision, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Reportes.spDAN_RepBasesEmision");
			sb.append("|");
			sb.append("pFechaInicio" + "=" + movimientoEmision.getFechaInicio() + ",");
			sb.append("pFechaFin" + "=" + movimientoEmision.getFechaFin() + ",");
			sb.append("pIdRamo" + "=" + movimientoEmision.getIdRamo() + ",");
			sb.append("pIdSubRamo" + "=" + movimientoEmision.getIdSubRamo() + ",");
			sb.append("pNivelContratoReaseguro" + "=" + movimientoEmision.getIdNivelContratoReaseguro() + ",");
			sb.append("pNivelAgrupamiento" + "=" + movimientoEmision.getNivelAgrupamiento() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Obtener movimientos emision" + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
}
