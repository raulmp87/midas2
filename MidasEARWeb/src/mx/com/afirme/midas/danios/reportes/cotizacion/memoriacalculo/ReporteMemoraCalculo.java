package mx.com.afirme.midas.danios.reportes.cotizacion.memoriacalculo;

import java.math.BigDecimal;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteMemoraCalculo extends MidasReporteBase{

	private BigDecimal idToCotizacion;
	private Integer numeroInciso;
	private String tipoReporte = Sistema.TIPO_XLS;
	
	public ReporteMemoraCalculo(BigDecimal idToCotizacion, Integer numeroInciso) {
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		
		byte byteArray[] = null;
		
		PlantillaMemoriaCalculo plantilla = new PlantillaMemoriaCalculo(idToCotizacion, numeroInciso);
		
		plantilla.setTipoReporte(tipoReporte);
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		
		return byteArray;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
}
