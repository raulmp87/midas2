package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReporteBasesEmisionAction extends MidasMappingDispatchAction {

	public ActionForward mostrarReporteMovimientoEmision(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		return mapping.findForward(Sistema.EXITOSO);
	}

}
