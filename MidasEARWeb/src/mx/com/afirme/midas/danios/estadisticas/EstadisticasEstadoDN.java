package mx.com.afirme.midas.danios.estadisticas;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;


public class EstadisticasEstadoDN {
	private static final EstadisticasEstadoDN INSTANCIA = new EstadisticasEstadoDN();
	private BigDecimal GRUPO_ESTADISTICAS = BigDecimal.valueOf(10D);
	private BigDecimal TRANSICION_SOLICITUD = BigDecimal.valueOf(10D);
	private BigDecimal TRANSICION_ODT = BigDecimal.valueOf(20D);
	private BigDecimal TRANSICION_COTIZACION = BigDecimal.valueOf(30D);
	private BigDecimal TRANSICION_EMISION = BigDecimal.valueOf(40D);
	private BigDecimal TRANSICION_FINAL = BigDecimal.valueOf(50D);
	private String USUARIO_ESTADISTICAS = "SISTEMA";

	public static EstadisticasEstadoDN getInstancia (){
		return EstadisticasEstadoDN.INSTANCIA;
	}
	
	public void agregar(EstadisticasEstadoDTO estadisticasEstadoDTO) throws SystemException{
	    new EstadisticasEstadoSN().agregar(estadisticasEstadoDTO);
	}
	
	public void borrar(EstadisticasEstadoDTO estadisticasEstadoDTO) throws SystemException{
	    new EstadisticasEstadoSN().borrar(estadisticasEstadoDTO);
	}
	
	public EstadisticasEstadoDTO buscarParaActualizarPorRetornoEstatus(BigDecimal idToSolicitud, BigDecimal idToCotizacion) throws SystemException{
	    return  new EstadisticasEstadoSN().buscarParaActualizarPorRetornoEstatus(idToSolicitud,idToCotizacion);
	}
	
	public void modificar(EstadisticasEstadoDTO estadisticasEstadoDTO) throws SystemException{
	    new EstadisticasEstadoSN().modificar(estadisticasEstadoDTO);
	}
	
	public void insertarEstadisticasAutomaticas(BigDecimal idToSolicitud,BigDecimal idToCotizacion, BigDecimal idToPoliza, BigDecimal numeroEndoso)throws SystemException{
		Date fechaEstadisticas = new Date();
		EstadisticasEstadoDTO dto = new EstadisticasEstadoDTO();
		EstadisticasEstadoId id = new EstadisticasEstadoId(GRUPO_ESTADISTICAS, TRANSICION_SOLICITUD, fechaEstadisticas);
		
		dto.setIdBase1(idToSolicitud);
		dto.setIdBase2(BigDecimal.ZERO);
		dto.setIdBase3(BigDecimal.ZERO);
		dto.setIdBase4(BigDecimal.ZERO);
		dto.setValorVariable1(USUARIO_ESTADISTICAS);
		dto.setId(id);
		agregar(dto);
		
		id.setIdSubCodigoEstadistica(TRANSICION_ODT);
		dto.setIdBase2(idToCotizacion);
		dto.setId(id);
		agregar(dto);		
		
		id.setIdSubCodigoEstadistica(TRANSICION_COTIZACION);
		dto.setId(id);
		agregar(dto);			
		
		id.setIdSubCodigoEstadistica(TRANSICION_EMISION);
		dto.setId(id);
		agregar(dto);		

		id.setIdSubCodigoEstadistica(TRANSICION_FINAL);
		dto.setIdBase3(idToPoliza);
		dto.setIdBase4(numeroEndoso);		
		dto.setId(id);
		agregar(dto);			
	}

	public List<EstadisticasEstadoDTO> buscarPorPropiedad(
			String nombrePropiedad, Object valor) throws SystemException {
		return new EstadisticasEstadoSN().buscarPorPropiedad(nombrePropiedad, valor);
	}

	public boolean esTrancisionODT(EstadisticasEstadoDTO estadistica) {
		return estadistica.getId().getIdCodigoEstadistica().shortValue() == GRUPO_ESTADISTICAS.shortValue()
				&& estadistica.getId().getIdSubCodigoEstadistica().shortValue() == TRANSICION_ODT.shortValue();
	}

	public boolean esTrancisionCOT(EstadisticasEstadoDTO estadistica) {
		return estadistica.getId().getIdCodigoEstadistica().shortValue() == GRUPO_ESTADISTICAS.shortValue()
				&& estadistica.getId().getIdSubCodigoEstadistica().shortValue() == TRANSICION_COTIZACION.shortValue();
	}

	public boolean esTrancisionEmision(EstadisticasEstadoDTO estadistica) {
		return estadistica.getId().getIdCodigoEstadistica().shortValue() == GRUPO_ESTADISTICAS.shortValue()
				&& estadistica.getId().getIdSubCodigoEstadistica().shortValue() == TRANSICION_EMISION.shortValue();
	}
}
