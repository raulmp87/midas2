package mx.com.afirme.midas.danios.estadisticas;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class EstadisticasEstadoSN {
    	private EstadisticasEstadoFacadeRemote beanRemoto;

	public EstadisticasEstadoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(EstadisticasEstadoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public void agregar(EstadisticasEstadoDTO estadisticasEstadoDTO){
	    beanRemoto.save(estadisticasEstadoDTO);
	}
	
	public void borrar(EstadisticasEstadoDTO estadisticasEstadoDTO){
	    beanRemoto.delete(estadisticasEstadoDTO);
	}
	
	public EstadisticasEstadoDTO buscarParaActualizarPorRetornoEstatus(BigDecimal idToSolicitud, BigDecimal idToCotizacion){
	    return beanRemoto.buscarParaActualizarPorRetornoEstatus(idToSolicitud, idToCotizacion);
	}
	
	public void modificar(EstadisticasEstadoDTO estadisticasEstadoDTO){
	    beanRemoto.update(estadisticasEstadoDTO);
	}


	public List<EstadisticasEstadoDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}	
}
