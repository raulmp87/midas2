package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import mx.com.afirme.midas.catalogos.clasificacionembarcacion.ClasificacionEmbarcacionDN;
import mx.com.afirme.midas.catalogos.clasificacionembarcacion.ClasificacionEmbarcacionDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDN;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoId;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.mediotransporte.MedioTransporteDN;
import mx.com.afirme.midas.catalogos.mediotransporte.MedioTransporteDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.catalogos.tipoembarcacion.TipoEmbarcacionDN;
import mx.com.afirme.midas.catalogos.tipoembarcacion.TipoEmbarcacionDTO;
import mx.com.afirme.midas.catalogos.tipoempaque.TipoEmpaqueDN;
import mx.com.afirme.midas.catalogos.tipoempaque.TipoEmpaqueDTO;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDN;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.tipoaeronave.TipoAeronaveDN;
import mx.com.afirme.midas.consultas.tipoaeronave.TipoAeronaveDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotId;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotSN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.SeccionSubIncisoSoporteDanosSiniestrosDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.endoso.EndosoSN;
import mx.com.afirme.midas.endoso.inciso.IncisoSoporteDaniosSiniestrosDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaSN;
import mx.com.afirme.midas.poliza.agrupacion.AgrupacionPolizaDTO;
import mx.com.afirme.midas.poliza.agrupacion.AgrupacionPolizaId;
import mx.com.afirme.midas.poliza.agrupacion.AgrupacionPolizaSN;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaDTO;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaId;
import mx.com.afirme.midas.poliza.cobertura.CoberturaPolizaSN;
import mx.com.afirme.midas.poliza.cobertura.CoberturaSoporteDaniosSiniestroDTO;
import mx.com.afirme.midas.poliza.riesgo.subinciso.SubIncisoRiesgoPolizaDTO;
import mx.com.afirme.midas.poliza.riesgo.subinciso.SubIncisoRiesgoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoSN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguro;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.CoberturaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAviacionSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipBarcosSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipEquipoContratistaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipGeneralSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipIncendioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipObraCivilSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCConstructoresSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCFuncionarioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipTransportesSoporteDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

public class SoporteDanosDN {

	private static final SoporteDanosDN INSTANCIA = new SoporteDanosDN();
	
	public static SoporteDanosDN getInstancia() {
		return INSTANCIA;
	}

	/**
	 * Este metodo permite consultar los datos generales de la poliza y los
	 * popula en el objeto de soporte PolizaSoporteDanosDTO
	 * 
	 * @param BigDecimal
	 *            idToPoliza objeto que corresponde al ID de la entidad
	 *            PolizaDTO
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el acceso a BD o la instancia al EJB
	 *             falla
	 * @return PolizaSoporteDanosDTO el objeto actualizado con los datos
	 *         solicitados
	 */
	public PolizaSoporteDanosDTO getDatosGeneralesPoliza(BigDecimal idToPoliza)
			throws ExcepcionDeAccesoADatos, SystemException {
	    	PolizaSoporteDanosDTO polizaSoporteDanosDTO = null;
		PolizaDTO polizaDTO=null;
		if(idToPoliza==null){
		    return null;
	    	}
		//Se consulta los datos de la poliza
		polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		if (polizaDTO!=null) {
		    	polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			this.poblarPolizaSoporteDTO(polizaSoporteDanosDTO, polizaDTO);
		}
		return polizaSoporteDanosDTO;
	}

	/**
	 * Este metodo permite consultar los datos generales de la poliza, asi como
	 * su detalle y los popula en el objeto de soporte PolizaSoporteDanosDTO
	 * 
	 * @param BigDecimal
	 *            idToPoliza objeto que corresponde al ID de la entidad
	 *            PolizaDTO
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el acceso a BD o la instancia al EJB
	 *             falla
	 * @return PolizaSoporteDanosDTO el objeto actualizado con los datos
	 *         solicitados
	 */
	public PolizaSoporteDanosDTO getDetallePoliza(BigDecimal idToPoliza)
			throws ExcepcionDeAccesoADatos, SystemException {
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = null;
		PolizaDTO polizaDTO =null;
		if(idToPoliza==null){
		    return null;
	    	}
		polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		if (polizaDTO != null) {
		    	polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			this.poblarPolizaSoporteDTO(polizaSoporteDanosDTO, polizaDTO);
			this.poblarDetallePolizaSoporteDTO(polizaSoporteDanosDTO, polizaDTO
					.getCotizacionDTO().getIdToCotizacion());
		}

		return polizaSoporteDanosDTO;
	}

	/**
	 * Este metodo permite consultar los datos generales de la poliza, los
	 * popula en el objeto de soporte PolizaSoporteDanosDTO
	 * 
	 * Indispensable que el numero de poliza cumpla el sig formato:
	 * PPTT-NNNNNN-RR
	 * 
	 * @param PolizaSoporteDanosDTO
	 *            polizaSoporteDanosDTO objeto de cual se tomaran los filtros
	 *            para implementar la busqueda en la entidad de PolizaDTO
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el acceso a BD o la instancia al EJB
	 *             falla
	 * @return List<PolizaSoporteDanosDTO> la lista de resultados que
	 *         correspondan a los filtros ingresados
	 */
	public List<PolizaSoporteDanosDTO> buscarPolizasFiltrado(PolizaSoporteDanosDTO polizaSoporteDanosDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		List<PolizaSoporteDanosDTO> listaResultado = new ArrayList<PolizaSoporteDanosDTO>();
		PolizaSN polizaSN = new PolizaSN();
		PolizaDTO polizaDTO = new PolizaDTO();
		List<PolizaDTO> polizas = new ArrayList<PolizaDTO>();
		int band = 0;
		if (polizaSoporteDanosDTO.getIdToPoliza() != null){
			polizaDTO = polizaSN.getPorId(polizaSoporteDanosDTO.getIdToPoliza());
			polizas.add(polizaDTO);
			band=1;
		}
		if (polizaSoporteDanosDTO.getNumeroPoliza() != null) {
			polizas = polizaSN.listarPorNumPolizaFormato(polizaSoporteDanosDTO.getNumeroPoliza());
			band=1;
		}
		if (polizaSoporteDanosDTO.getNombreAsegurado() != null) {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setNombreAsegurado(polizaSoporteDanosDTO
					.getNombreAsegurado());
			polizaDTO.setCotizacionDTO(cotizacionDTO);
			polizas = polizaSN.buscarFiltrado(polizaDTO);
			band=1;
		}
		
		if (polizaSoporteDanosDTO.getIdToPoliza() == null){
			if (polizaSoporteDanosDTO.getNumeroPoliza() == null) {
				if (polizaSoporteDanosDTO.getIdTcTipoNegocio() != null && band == 0) {			
					polizas = polizaSN.buscarFacultativo();
				}
			}
		}
		for (PolizaDTO poliza : polizas) {
			PolizaSoporteDanosDTO polizaSoporteDanosDTO2 = new PolizaSoporteDanosDTO();
			this.poblarPolizaSoporteDTO(polizaSoporteDanosDTO2, poliza);
			listaResultado.add(polizaSoporteDanosDTO2);
		}
		return listaResultado;
	}

	/**
	 * Este metodo permite consultar los datos generales de la poliza, asi como
	 * su detalle y los popula en el objeto de soporte PolizaSoporteDanosDTO
	 * 
	 * Indispensable que el numero de poliza cumpla el sig formato:
	 * PPTT-NNNNNN-RR
	 * 
	 * @param PolizaSoporteDanosDTO
	 *            polizaSoporteDanosDTO objeto de cual se tomaran los filtros
	 *            para implementar la busqueda en la entidad de PolizaDTO
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el acceso a BD o la instancia al EJB
	 *             falla
	 * @return List<PolizaSoporteDanosDTO> la lista de resultados que
	 *         correspondan a los filtros ingresados
	 */
	public List<PolizaSoporteDanosDTO> buscarDetallePolizasFiltrado(
			PolizaSoporteDanosDTO polizaSoporteDanosDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		List<PolizaSoporteDanosDTO> listaResultado = new ArrayList<PolizaSoporteDanosDTO>();
		PolizaSN polizaSN = new PolizaSN();
		PolizaDTO polizaDTO = new PolizaDTO();
		if (polizaSoporteDanosDTO.getNumeroPoliza() != null) {
			this.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza(),polizaDTO);
		}
		if (polizaSoporteDanosDTO.getNombreAsegurado() != null) {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setNombreEmpresaAsegurado(polizaSoporteDanosDTO
					.getNombreAsegurado());
		}
		List<PolizaDTO> polizas = polizaSN.buscarFiltrado(polizaDTO);
		for (PolizaDTO poliza : polizas) {
			PolizaSoporteDanosDTO polizaSoporte = new PolizaSoporteDanosDTO();
			this.poblarPolizaSoporteDTO(polizaSoporte, poliza);
			this.poblarDetallePolizaSoporteDTO(polizaSoporteDanosDTO,
					poliza.getCotizacionDTO().getIdToCotizacion());
			listaResultado.add(polizaSoporte);
		}

		return listaResultado;
	}

	/**
	 * Este metodo permite obtener los datos de un endoso
	 * 
	 * @param BigDecimal
	 *            idToPoliza objeto que corresponde al ID de la entidad
	 *            PolizaDTO
	 * @param BigDecimal
	 *            numeroEndoso objeto que corresponde al numero de endoso
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el acceso a BD o la instancia al EJB
	 *             falla
	 * @return EndosoSoporteDanosDTO el objeto actualizado con los datos
	 *         solicitados
	 */
	public EndosoSoporteDanosDTO getEndosoPorId(BigDecimal idToPoliza,
			BigDecimal numeroEndoso) throws ExcepcionDeAccesoADatos,
			SystemException {
		EndosoSoporteDanosDTO endosoSoporteDanosDTO = new EndosoSoporteDanosDTO();
		EndosoId id = new EndosoId();
		EndosoSN endosoSN = new EndosoSN();
		id.setIdToPoliza(idToPoliza);
		id.setNumeroEndoso(numeroEndoso.shortValue());
		EndosoDTO endoso = endosoSN.getPorId(id);

		if (endoso != null) {
			PolizaDTO polizaDTO = new PolizaDTO();
			PolizaSN polizaSN = new PolizaSN();
			polizaDTO.setIdToPoliza(idToPoliza);
			polizaDTO = polizaSN.buscarFiltrado(polizaDTO).get(0);
			endosoSoporteDanosDTO.setNombreAsegurado(polizaDTO
					.getCotizacionDTO().getNombreEmpresaAsegurado());
			endosoSoporteDanosDTO.setNumeroEndoso(endoso.getId()
					.getNumeroEndoso());
			endosoSoporteDanosDTO.setNumeroPoliza(endoso.getId()
					.getIdToPoliza().toString());
			endosoSoporteDanosDTO.setFechaInicioVigencia(endoso.getFechaInicioVigencia());
			endosoSoporteDanosDTO.setFechaFinVigencia(endoso.getFechaFinVigencia());
			
		}

		return endosoSoporteDanosDTO;
	}
	
	/**
	 * Este metodo permite obtener los datos del �ltimo endoso de una p�liza
	 * 
	 * @param BigDecimal idToPoliza objeto que corresponde al ID de la entidad PolizaDTO
	 * @throws ExcepcionDeAccesoADatos, SystemException cuando el acceso a BD o la instancia al EJB falla
	 * @return EndosoSoporteDanosDTO el objeto actualizado con los datos solicitados
	 */
	public EndosoSoporteDanosDTO getUltimoEndoso(BigDecimal idToPoliza) throws ExcepcionDeAccesoADatos,SystemException {
		EndosoSoporteDanosDTO endosoSoporteDanosDTO = new EndosoSoporteDanosDTO();
		EndosoSN endosoSN = new EndosoSN();
		EndosoDTO endoso = endosoSN.getUltimoEndoso(idToPoliza);

		if (endoso != null) {
			PolizaDTO polizaDTO = new PolizaDTO();
			PolizaSN polizaSN = new PolizaSN();
			polizaDTO.setIdToPoliza(idToPoliza);
			polizaDTO = polizaSN.buscarFiltrado(polizaDTO).get(0);
			endosoSoporteDanosDTO.setNombreAsegurado(polizaDTO.getCotizacionDTO().getNombreEmpresaAsegurado());
			endosoSoporteDanosDTO.setNumeroEndoso(endoso.getId().getNumeroEndoso());
			endosoSoporteDanosDTO.setNumeroPoliza(endoso.getId().getIdToPoliza().toString());
		}

		return endosoSoporteDanosDTO;
	}
	
	/**
	 * Busca la poliza por <code>idToPoliza</code> y retorna un <code>String</code> con el numero de poliza formateado.
	 * @param idToPoliza
	 * @return numeroPoliza formateado
	 * @throws SystemException si al crear una instancia de <code>PolizaSN</code> falla.
	 */
	public String getNumeroPoliza(BigDecimal idToPoliza) throws SystemException{
		String numeroPoliza="";
		PolizaSN polizaSN = new PolizaSN();
		PolizaDTO polizaDTO = polizaSN.getPorId(idToPoliza);
		if(polizaDTO != null){
			numeroPoliza = UtileriasWeb.getNumeroPoliza(polizaDTO);
		}else{
			System.out.println("Error en SoporteDanosDN.getNumeroPoliza ==> Los datos de la poliza " + idToPoliza + " no pudieron ser obtenidos");
		}
		return numeroPoliza;
	} 
	/**
	 * A partir de un numero de Poliza formateado, actualiza los datos en la base de datos.
	 * @param numeroPolizaConFormato
	 * @param polizaDTO
	 */
	private void setNumeroPoliza(String numeroPolizaConFormato,PolizaDTO polizaDTO) {

		String[] numeroDePolizaArray = numeroPolizaConFormato.split("-");
		String codigoProducto = numeroDePolizaArray[0].substring(0, 2);
		String codigoTipoPoliza = numeroDePolizaArray[0].substring(2, 4);
		Integer numeroPoliza = new Integer(numeroDePolizaArray[1]);
		Integer numeroRenovacion = new Integer(numeroDePolizaArray[2]);
		polizaDTO.setCodigoProducto(codigoProducto);
		polizaDTO.setCodigoTipoPoliza(codigoTipoPoliza);
		polizaDTO.setNumeroPoliza(numeroPoliza);
		polizaDTO.setNumeroRenovacion(numeroRenovacion);
	}
        /**
         * Method poblarDetallePolizaSoporteDTO
         * @param polizaSoporteDanosDTO
         * @param IdToCotizacion
         * @throws SystemException cuando ocurre algun error al instanciar los SN, usados.
         */
	private void poblarDetallePolizaSoporteDTO(PolizaSoporteDanosDTO polizaSoporteDanosDTO,
			BigDecimal IdToCotizacion) throws SystemException {
		SubIncisoRiesgoPolizaSN subIncisoRiesgoPolizaSN = new SubIncisoRiesgoPolizaSN();
		SeccionSN seccionSN = new SeccionSN();
		SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN();
		CoberturaSN coberturaSN = new CoberturaSN();
		RiesgoSN riesgoSN = new RiesgoSN();
		CoberturaPolizaSN coberturaPolizaSN = new CoberturaPolizaSN();
		AgrupacionPolizaSN agrupacionPolizaSN = new AgrupacionPolizaSN();
		List<DetallePolizaSoporteDanosSiniestrosDTO> listaDetallesPolizaSiniestros = polizaSoporteDanosDTO
				.getListaDetallesPolizaSiniestros();
		List<SubIncisoRiesgoPolizaDTO> subIncisosRiesgos = subIncisoRiesgoPolizaSN.buscarPorPropiedad
			("id.idToPoliza", polizaSoporteDanosDTO.getIdToPoliza());
		for (SubIncisoRiesgoPolizaDTO subIncisosRiesgo : subIncisosRiesgos) {
			DetallePolizaSoporteDanosSiniestrosDTO detalle = new DetallePolizaSoporteDanosSiniestrosDTO();
			detalle.setNumeroInciso(subIncisosRiesgo.getId().getNumeroInciso());
			detalle.setNumeroSubInciso(subIncisosRiesgo.getId().getNumeroSubInciso());
			detalle.setIdToSeccion(subIncisosRiesgo.getId().getIdToSeccion());
			detalle.setIdToCobertura(subIncisosRiesgo.getId().getIdToCobertura());
			detalle.setIdToRiesgo(subIncisosRiesgo.getId().getIdToRiesgo());
			SeccionDTO seccionDTO = seccionSN.getPorId(subIncisosRiesgo.getId().getIdToSeccion());
			detalle.setDescripcionSeccion(seccionDTO.getDescripcion());
			SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
			id.setIdToCotizacion(IdToCotizacion);
			id.setIdToSeccion(subIncisosRiesgo.getId().getIdToSeccion());
			id.setNumeroInciso(subIncisosRiesgo.getId().getNumeroInciso());
			id.setNumeroSubInciso(subIncisosRiesgo.getId().getNumeroSubInciso());
			SubIncisoCotizacionDTO subIncisoCot = subIncisoCotSN.getPorId(id);
			if (subIncisoCot != null){
			    detalle.setDescripcionSubInciso(subIncisoCot.getDescripcionSubInciso());				
			}
			CoberturaDTO coberturaDTO = coberturaSN.getPorId(subIncisosRiesgo.getId().getIdToCobertura());
			detalle.setDescripcionCobertura(coberturaDTO.getDescripcion());
			RiesgoDTO riesgoDTO = riesgoSN.getPorId(subIncisosRiesgo.getId().getIdToRiesgo());
			detalle.setDescripcionRiesgo(riesgoDTO.getDescripcion());
			detalle.setSumaAsegurada(subIncisosRiesgo.getValorSumaAsegurada());
			detalle.setClaveTipoSumaAsegurada(Integer.valueOf(coberturaDTO.getClaveTipoSumaAsegurada()));
			CoberturaPolizaId id2 = new CoberturaPolizaId();
			id2.setIdToCobertura(subIncisosRiesgo.getId().getIdToCobertura());
			id2.setIdToPoliza(subIncisosRiesgo.getId().getIdToPoliza());
			id2.setIdToSeccion(subIncisosRiesgo.getId().getIdToSeccion());
			id2.setNumeroInciso(subIncisosRiesgo.getId().getNumeroInciso());
			CoberturaPolizaDTO coberturaPolizaDTO = coberturaPolizaSN.getPorId(id2);
			AgrupacionPolizaId id3 = new AgrupacionPolizaId();
			id3.setIdToPoliza(subIncisosRiesgo.getId().getIdToPoliza());
			id3.setNumeroAgrupacion(coberturaPolizaDTO.getNumeroAgrupacion());
			AgrupacionPolizaDTO agrupacionPolizaDTO = agrupacionPolizaSN.getPorId(id3);
			if (agrupacionPolizaDTO == null) {
				detalle.setClaveTipoAgrupacion(0);
			} else {
				detalle.setClaveTipoAgrupacion(agrupacionPolizaDTO.getClaveTipoAgrupacion());
			}
			listaDetallesPolizaSiniestros.add(detalle);
		}

	}
	/**
	 * Method poblarPolizaSoporteDTO
	 * @param polizaSoporteDanosDTO
	 * @param polizaDTO
	 * @throws SystemException si ocurre un error al invocar <code> clienteDN.verDetalleCliente()</code>
	 */
	private void poblarPolizaSoporteDTO(PolizaSoporteDanosDTO polizaSoporteDanosDTO, PolizaDTO polizaDTO)
			throws SystemException {
		polizaSoporteDanosDTO.setIdToPoliza(polizaDTO.getIdToPoliza());
		polizaSoporteDanosDTO.setNumeroPoliza(UtileriasWeb.getNumeroPoliza(polizaDTO));
		polizaSoporteDanosDTO.setFechaEmision(polizaDTO.getFechaCreacion());
		polizaSoporteDanosDTO.setCodigoProducto(Integer.valueOf(polizaDTO.getCodigoProducto()));
		polizaSoporteDanosDTO.setNombreComercialProducto
		(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getNombreComercial());
		polizaSoporteDanosDTO.setNombreAsegurado(polizaDTO.getCotizacionDTO()
			.getNombreAsegurado());
		polizaSoporteDanosDTO.setIdAsegurado(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado());
//		ClienteDN clienteDN = ClienteDN.getInstancia();
		ClienteDTO clienteDTO = null;
		//DESARROLLO CURP
//		if(polizaDTO.getCotizacionDTO().getIdDomicilioAsegurado() != null && polizaDTO.getCotizacionDTO().getIdDomicilioAsegurado().intValue() > 0){
//			clienteDTO = new ClienteDTO();
//			clienteDTO.setIdCliente(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado());
//			clienteDTO.setIdDomicilio(polizaDTO.getCotizacionDTO().getIdDomicilioAsegurado());
//			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, polizaDTO.getCotizacionDTO().getCodigoUsuarioCotizacion());
//		}else{
			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado(), polizaDTO.getCotizacionDTO().getCodigoUsuarioCotizacion());
//		}			
		if(clienteDTO!=null){
		    	polizaSoporteDanosDTO.setCalleAsegurado(clienteDTO.getNombreCalle());
        		polizaSoporteDanosDTO.setCpAsegurado(clienteDTO.getCodigoPostal());
        		polizaSoporteDanosDTO.setIdEstadoAsegurado(clienteDTO.getIdEstado());
        		polizaSoporteDanosDTO.setIdMunicipioAsegurado(clienteDTO.getIdMunicipio());
			polizaSoporteDanosDTO.setIdColoniaAsegurado(clienteDTO.getIdColonia());
			polizaSoporteDanosDTO.setTelefonoAsegurado(clienteDTO.getTelefono());
		}
		
		polizaSoporteDanosDTO.setIdContratante(polizaDTO.getCotizacionDTO().getIdToPersonaContratante());
		//DESARROLLO CURP
//		if(polizaDTO.getCotizacionDTO().getIdDomicilioContratante() != null && polizaDTO.getCotizacionDTO().getIdDomicilioContratante().intValue() > 0){
//			clienteDTO = new ClienteDTO();
//			clienteDTO.setIdCliente(polizaDTO.getCotizacionDTO().getIdToPersonaContratante());
//			clienteDTO.setIdDomicilio(polizaDTO.getCotizacionDTO().getIdDomicilioContratante());
//			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, polizaDTO.getCotizacionDTO().getCodigoUsuarioCotizacion());
//		}else{
			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(polizaDTO.getCotizacionDTO().getIdToPersonaContratante(), polizaDTO.getCotizacionDTO().getCodigoUsuarioCotizacion());
//		}			
		if(clienteDTO!=null){
		    String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	            nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	            nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
	            polizaSoporteDanosDTO.setNombreContratante(nombreCliente);
			polizaSoporteDanosDTO.setCalleContratante(clienteDTO.getNombreCalle());
			polizaSoporteDanosDTO.setCpContratante(clienteDTO.getCodigoPostal());
			polizaSoporteDanosDTO.setIdEstadoContratante(clienteDTO.getIdEstado());
			polizaSoporteDanosDTO.setIdMunicipioContratante(clienteDTO.getIdMunicipio());
			polizaSoporteDanosDTO.setIdColoniaContratante(clienteDTO.getIdColonia());
			polizaSoporteDanosDTO.setTelefonoContratante(clienteDTO.getTelefono());
		}
		
		polizaSoporteDanosDTO.setIdMoneda(polizaDTO.getCotizacionDTO().getIdMoneda());
		MonedaDN monedaDN = MonedaDN.getInstancia();
		polizaSoporteDanosDTO.setDescripcionMoneda(monedaDN.getPorId(polizaDTO.getCotizacionDTO().
			getIdMoneda().shortValue()).getDescripcion());
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		SolicitudDTO solicitud = solicitudDN.getPorId(polizaDTO.getIdToSolicitud());
		polizaSoporteDanosDTO.setNombreOficina(solicitud.getNombreOficinaAgente());

		// TODO datos no disponibles;
		polizaSoporteDanosDTO.setFechaPago(null);
		polizaSoporteDanosDTO.setImportePagado(0d);
		polizaSoporteDanosDTO.setSaldoPendiente(0d);
		polizaSoporteDanosDTO.setSaldoVencido(0d);
		polizaSoporteDanosDTO.setUltimoReciboPagado(null);
		polizaSoporteDanosDTO.setIdFormaPago(polizaDTO.getCotizacionDTO().getIdFormaPago());

		FormaPagoDN formaPagoDN = FormaPagoDN.getInstancia();
		FormaPagoDTO formaPagoDTO = new FormaPagoDTO();

		formaPagoDTO.setIdFormaPago(polizaDTO.getCotizacionDTO().getIdFormaPago().intValue());
		formaPagoDTO = formaPagoDN.getPorId(formaPagoDTO);

		polizaSoporteDanosDTO.setDescripcionFormaPago(formaPagoDTO.getDescripcion());
		polizaSoporteDanosDTO.setFechaInicioVigencia(polizaDTO.getCotizacionDTO().getFechaInicioVigencia());
		polizaSoporteDanosDTO.setFechaFinVigencia(polizaDTO.getCotizacionDTO().getFechaFinVigencia());
		polizaSoporteDanosDTO.setIdTcTipoNegocio(polizaDTO.getCotizacionDTO().getTipoNegocioDTO().getIdTcTipoNegocio());
		polizaSoporteDanosDTO.setCodigoTipoNegocio(polizaDTO.getCotizacionDTO().getTipoNegocioDTO().getCodigoTipoNegocio());

		ParametroGeneralDN parametroGeneralDN = ParametroGeneralDN.getINSTANCIA();
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(BigDecimal.ONE);
		ParametroGeneralDTO parametroGeneralDTO = new ParametroGeneralDTO();
		parametroGeneralDTO.setId(id);
		parametroGeneralDTO.setValor((Integer.valueOf(polizaDTO.getCodigoProducto())).toString());
		List<ParametroGeneralDTO> parametros = parametroGeneralDN.listarFiltrado(parametroGeneralDTO);
		if (parametros.size() > 0) {
			parametroGeneralDTO = parametros.get(0);
			if (parametroGeneralDTO.getId().getCodigoParametroGeneral().intValue() == 10020) {
				polizaSoporteDanosDTO.setProductoTransporte(Boolean.TRUE);
			} else {
				polizaSoporteDanosDTO.setProductoTransporte(Boolean.FALSE);
			}
		} else {
			polizaSoporteDanosDTO.setProductoTransporte(Boolean.FALSE);
		}

		//campo agregado numeroUltimoEndoso
		EndosoDN endosoDN = EndosoDN.getInstancia("");
		EndosoDTO endosoDTO = endosoDN.getUltimoEndoso(polizaDTO.getIdToPoliza());
		if(endosoDTO != null) {
			polizaSoporteDanosDTO.setNumeroUltimoEndoso(endosoDTO.getId().getNumeroEndoso());
		}

		polizaSoporteDanosDTO.setCodigoAgente(polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
		polizaSoporteDanosDTO.setNombreAgente(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNombreAgente());
		polizaSoporteDanosDTO.setIdAsegurado(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado());
	}
	/**
	 * Devuelve un objeto <code>RiesgoSoporteDanosDTO</code> a partir de un objeto <code>RiesgoDTO</code>.
	 * @param idToRiesgo
	 * @return RiesgoSoporteDanosDTO
	 * @throws ExcepcionDeAccesoADatos si ocurre un error al acceder a los datos;al invocar <code> riesgoDN.getPorId()</code>
	 * @throws SystemException si ocurre un error al invocar <code> riesgoDN.getPorId()</code>
	 */
	public RiesgoSoporteDanosDTO getRiesgoSoporte(BigDecimal idToRiesgo)throws ExcepcionDeAccesoADatos,SystemException{		
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		riesgoDTO.setIdToRiesgo(idToRiesgo);
		riesgoDTO = riesgoDN.getPorId(riesgoDTO);
		
		if (riesgoDTO!=null&&riesgoDTO.getIdToRiesgo()!=null){
			riesgoSoporteDanosDTO.setIdToRiesgo(idToRiesgo);
			riesgoSoporteDanosDTO.setDescripcionRiesgo(riesgoDTO.getNombreComercial());
		}else {
			return null;
		}
			
		return riesgoSoporteDanosDTO;
	}
	/**
	 * Devuelve un objeto <code>CoberturaSoporteDanosDTO</code> a partir de los datos de <code>CoberturaDTO</code>.
	 * @param idToCobertura
	 * @return CoberturaSoporteDanosDTO
	 * @throws ExcepcionDeAccesoADatos si ocurre un error al invocar <code> coberturaDN.getPorId</code>
	 * @throws SystemException si ocurre un error al invocar <code> coberturaDN.getPorId</code>
	 */
	public CoberturaSoporteDanosDTO getCoberturaSoporte(BigDecimal idToCobertura)throws ExcepcionDeAccesoADatos,SystemException{
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		
		coberturaDTO = coberturaDN.getPorId(idToCobertura);
		if (coberturaDTO!=null&&coberturaDTO.getIdToCobertura()!=null){
			coberturaSoporteDanosDTO.setIdToCobertura(idToCobertura);
			coberturaSoporteDanosDTO.setClaveTipoSumaAsegurada(new Short(coberturaDTO.getClaveTipoSumaAsegurada()).shortValue());
			coberturaSoporteDanosDTO.setDescripcionCobertura(coberturaDTO.getNombreComercial());
			coberturaSoporteDanosDTO.setDescripcionSubramo(coberturaDTO.getSubRamoDTO().getDescription());
			coberturaSoporteDanosDTO.setDescripcionTipoSumaAsegurada(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_SUMA_ASEGURADA, coberturaDTO.getClaveTipoSumaAsegurada()));
			coberturaSoporteDanosDTO.setIdTcSubRamo(coberturaDTO.getSubRamoDTO().getIdTcSubRamo());
			coberturaSoporteDanosDTO.setIdTcRamo(coberturaDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo());
			coberturaSoporteDanosDTO.setDescripcionRamo(coberturaDTO.getSubRamoDTO().getRamoDTO().getDescripcion());
			coberturaSoporteDanosDTO.setCodigoRamo(coberturaDTO.getSubRamoDTO().getRamoDTO().getCodigo());
			coberturaSoporteDanosDTO.setCodigoSubRamo(coberturaDTO.getSubRamoDTO().getCodigoSubRamo());						
		} else {
			return null;
		}
		return coberturaSoporteDanosDTO;
	}
	/**
	 * Devuelve un objeto <code>SeccionSoporteDanosDTO</code> a partir de un objeto <code>seccionDTO</code>
	 * @param idToSeccion
	 * @return SeccionSoporteDanosDTO
	 * @throws ExcepcionDeAccesoADatos si ocurre un error al invocar <code> seccionDN.getPorId</code>
	 * @throws SystemException si ocurre un error al invocar <code> seccionDN.getPorId</code>
	 */
	public SeccionSoporteDanosDTO getSeccionSoporte(BigDecimal idToSeccion)throws ExcepcionDeAccesoADatos,SystemException{
		SeccionDN seccionDN = SeccionDN.getInstancia();
		SeccionSoporteDanosDTO seccionSoporteDanosDTO = new SeccionSoporteDanosDTO();
		SeccionDTO seccionDTO = new SeccionDTO();
		
		seccionDTO = seccionDN.getPorId(idToSeccion);
		if (seccionDTO!=null&&seccionDTO.getIdToSeccion()!=null){
			seccionSoporteDanosDTO.setIdToSeccion(idToSeccion);
			seccionSoporteDanosDTO.setDescripcionSeccion(seccionDTO.getNombreComercial());
		}else {
			return null;
		}
		
		return seccionSoporteDanosDTO;
	}
	
	
	/**
	 * METODO QUE CARGA LOS DATOS DE UN <code>SlipSoporteDTO</code>, DEPENDIENDO DEL  NUMERO DE
	 * SLIP QUE SE REQUIERA 
	 * @param tipoCumulo
	 * @param idToCotizacion
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @param slipSoporteDTO
	 * @param nombreUsuario
	 * @throws SystemException si fallan los las invocaciones en los metodos de las clases de DN
	 * @author Cesar Morales
	 */
	
	public void getDatosSlip(int tipoCumulo,BigDecimal idToCotizacion,BigDecimal numInciso,BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo,SlipSoporteDTO slipSoporteDTO,String nombreUsuario) throws SystemException{
		if(slipSoporteDTO!=null && idToCotizacion!= null){
			CotizacionDN cotizacionDN = new CotizacionDN();
			CotizacionDTO cotizacionDTO= cotizacionDN.obtenerCotizacionCompleta(idToCotizacion);
			ClienteDTO cliente=null;
			try{
				//DESARROLLO CURP
//				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
//					cliente = new ClienteDTO();
//					cliente.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//					cliente.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
//					cliente = ClienteDN.getInstancia().verDetalleCliente(cliente, cotizacionDTO.getCodigoUsuarioCotizacion());
//				}else{
					cliente = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), cotizacionDTO.getCodigoUsuarioCotizacion());
//				}					
			}catch (Exception e) {}
			slipSoporteDTO.setClienteDTO(cliente);
			slipSoporteDTO.setCotizacionDTO(cotizacionDTO);
//			this.poblarDatosGeneralesSlipSoporteDTO(cotizacionDTO,slipSoporteDTO, nombreUsuario);
    
			switch (slipSoporteDTO.getTipoSlip()) {
				case Sistema.TIPO_SLIP_AVIACION:
					this.getDatosSlipAviacion(slipSoporteDTO.getSlipAviacion(), tipoCumulo, cotizacionDTO, numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_BARCOS:
        			this.getDatosSlipBarcos(slipSoporteDTO.getSlipBarcos(), tipoCumulo, cotizacionDTO, numInciso,idToSeccion,numSubInciso, idTcSubRamo);        		    
        			break;
				case Sistema.TIPO_SLIP_EQUIPO_CONTRATISTA:
					this.getDatosSlipEquipoContratistaSoporteDTO(slipSoporteDTO.getSlipEquipoConstratista(), tipoCumulo, cotizacionDTO,numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_INCENDIO_SOPORTE:
					this.getDatosSlipIncendio(slipSoporteDTO.getSlipIncendioSoporte(),tipoCumulo, cotizacionDTO,numInciso,idToSeccion,numSubInciso,idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_OBRA_CIVIL_SOPORTE:
					this.getDatosSlipObraCivil( slipSoporteDTO.getSlipObraCivilSoporte(), tipoCumulo, cotizacionDTO, numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_CONSTRUCTORES_SOPORTE:
					if (slipSoporteDTO.getSlipRCConstructoresSoporte() == null) {
						slipSoporteDTO.setSlipRCConstructoresSoporte(new ArrayList<SlipRCConstructoresSoporteDTO>());
					}
					this.getDatosSlipRCConstructoresSoporteDTO(slipSoporteDTO.getSlipRCConstructoresSoporte(),tipoCumulo, cotizacionDTO, numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_FUNCIONARIOS_SOPORTE:
					if (slipSoporteDTO.getSlipRCFuncionarioSoporte() == null) {
						slipSoporteDTO.setSlipRCFuncionarioSoporte(new ArrayList<SlipRCFuncionarioSoporteDTO>());
					}
					this.getDatosSlipRCFuncionarioSoporteDTO(slipSoporteDTO.getSlipRCFuncionarioSoporte(),tipoCumulo, cotizacionDTO, numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_TRANSPORTE_SOPORTE:
					this.getDatosSlipTransporte(slipSoporteDTO.getSlipTransportesSoporte(),tipoCumulo, cotizacionDTO, numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
				case Sistema.TIPO_SLIP_GENERAL_SOPORTE:
					this.getDatoSlipGeneralSoporteDTO(slipSoporteDTO.getSlipGeneralSoporte(),tipoCumulo, cotizacionDTO, numInciso, idToSeccion, numSubInciso, idTcSubRamo);
					break;
			}
		}
	}
	
	/**
	 * Carga los datos de <code>slipIncendioSoporteDTO</code>
	 * @param slipIncendioSoporteDTO
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	private void getDatosSlipIncendio(SlipIncendioSoporteDTO slipIncendioSoporteDTO,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo)throws ExcepcionDeAccesoADatos, SystemException {
	    if (slipIncendioSoporteDTO != null){
		    CodigoPostalZonaSismoDN  codigoPostalZonaSismoDN= CodigoPostalZonaSismoDN.getInstancia();
		    CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO = null;
		    DatoIncisoCotizacionDN datoIncisoCotizacionDN= DatoIncisoCotizacionDN.getINSTANCIA();
		    IncisoCotizacionDTO incisoCotizacionDTO=null;
		    
		    for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
		    	if(inciso.getId().getNumeroInciso().equals(numInciso)){
		    		incisoCotizacionDTO=inciso;
		    	}
		    }
		    TipoTechoDTO tipotechoDTO= datoIncisoCotizacionDN.obtenerTipoTecho(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		    
		    TipoMuroDTO tipoMuroDTO= datoIncisoCotizacionDN.obtenerTipoMuro(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		    GiroDTO giroDTO =datoIncisoCotizacionDN.obtenerGiroDTO(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		    
		    slipIncendioSoporteDTO.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
		   
		    CodigoPostalZonaSismoId codigoPostalZonaSismoId= new CodigoPostalZonaSismoId();
		    codigoPostalZonaSismoId.setCodigoPostal(incisoCotizacionDTO.getDireccionDTO().getCodigoPostal());
		    codigoPostalZonaSismoId.setNombreColonia(incisoCotizacionDTO.getDireccionDTO().getNombreColonia());
		    codigoPostalZonaSismoDTO= codigoPostalZonaSismoDN.getPorId(codigoPostalZonaSismoId);
		    slipIncendioSoporteDTO.setZonaTerremoto(codigoPostalZonaSismoDTO!=null?codigoPostalZonaSismoDTO.getDescripcionZonaSismo():null);
		    
		    slipIncendioSoporteDTO.setDireccionRiesgo(getDireccion(incisoCotizacionDTO.getDireccionDTO()));
		   
		    slipIncendioSoporteDTO.setTipoConstruccion(tipotechoDTO.getDescripcionTipoTecho()+ " / "+tipoMuroDTO.getDescripcionTipoMuro());
		    slipIncendioSoporteDTO.setGiroUbicacion(giroDTO.getDescripcionGiro());
		   
		    slipIncendioSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false,idTcSubRamo, cotizacionDTO, numInciso,idToSeccion,numSubInciso));
		    
		    this.cargaSumasAseguradasParaSlipIncendio(slipIncendioSoporteDTO);
	    }
	}
	/**
	 * Carga las sumas aseguradas, dependiendo de la lista de coberturas del slip.
	 * @param slipIncendioSoporteDTO
	 */
	private void cargaSumasAseguradasParaSlipIncendio(SlipIncendioSoporteDTO slipIncendioSoporteDTO){
	    slipIncendioSoporteDTO.setSumaAseguradaTotal(BigDecimal.ZERO);
	    
	    if(slipIncendioSoporteDTO.getListaCoberturas()!=null && !slipIncendioSoporteDTO.getListaCoberturas().isEmpty()){
	    	for(CoberturaSoporteDTO coberturaSoporteDTO : slipIncendioSoporteDTO.getListaCoberturas() ){
	    		if(coberturaSoporteDTO.getClaveTipoSumaAsegurada().intValue() == Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA).intValue()){
	    			slipIncendioSoporteDTO.setSumaAseguradaTotal(slipIncendioSoporteDTO.getSumaAseguradaTotal().add(coberturaSoporteDTO.getSumaAsegurada()));
	    		}
	    	}
	    }
	    
	}
	/**
	 * Carga los datos de un <code>slipBarcosSoporteDTO</code>.
	 * @param slipBarcosSoporteDTO
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	private void getDatosSlipBarcos(SlipBarcosSoporteDTO slipBarcosSoporteDTO,int tipoCumulo,	CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo)throws ExcepcionDeAccesoADatos, SystemException {
		IncisoCotizacionDTO incisoCotizacionDTO=null;
	    DatoIncisoCotizacionDN datoIncisoCotizacionDN = DatoIncisoCotizacionDN.getINSTANCIA();
	    DatoIncisoCotizacionDTO datoIncisoCotizacionDTO= null;
	    TipoEmbarcacionDN tipoEmbarcacionDN=null;
	    TipoEmbarcacionDTO tipoEmbarcacionDTO=null;
	    ClasificacionEmbarcacionDN clasificacionEmbarcacionDN=null;
	    ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO=null;
	   	    
	    for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
	    	if(inciso.getId().getNumeroInciso().equals(numInciso)){
	    		incisoCotizacionDTO=inciso;
	    	}
	    }
	    if(slipBarcosSoporteDTO==null){
	    	slipBarcosSoporteDTO = new SlipBarcosSoporteDTO();
	    }
	    slipBarcosSoporteDTO.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	    
	    //Se busca el valor para aguasNavegacion
	    //primero se busca con el riesgo 3450
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_AGUAS_NAVEGACION);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    //Sino esta se busca con el riesgo 3500
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_AGUAS_NAVEGACION);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
	    	slipBarcosSoporteDTO.setAguasNavegacion(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipBarcosSoporteDTO.setAguasNavegacion(datoIncisoCotizacionDTO.getValor());
	    }
	    //**********************************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_ANIO_CONSTRUCCION);
		
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_ANIO_CONSTRUCCION);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	slipBarcosSoporteDTO.setAnioConstruccion(datoIncisoCotizacionDTO!=null ? Integer.valueOf(datoIncisoCotizacionDTO.getValor()) : 0 );
	    }else{
	    	slipBarcosSoporteDTO.setAnioConstruccion(Integer.valueOf(datoIncisoCotizacionDTO.getValor()));
	    }
	    
	    //********************************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_TIPO_EMBARCACION);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_TIPO_EMBARCACION);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	if(datoIncisoCotizacionDTO!=null){
	    		tipoEmbarcacionDN= TipoEmbarcacionDN.getInstancia();
	    		tipoEmbarcacionDTO = new TipoEmbarcacionDTO();
    		  	tipoEmbarcacionDTO.setIdTcTipoEmbarcacion(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
    		  	tipoEmbarcacionDTO= tipoEmbarcacionDN.getPorId(tipoEmbarcacionDTO);
    		  	slipBarcosSoporteDTO.setTipoEmbarcacion(tipoEmbarcacionDTO!=null?tipoEmbarcacionDTO.getDescripcionTipoEmbarcacion():"");
	    	}
	    }else{
	    	tipoEmbarcacionDN= TipoEmbarcacionDN.getInstancia();
	    	tipoEmbarcacionDTO = new TipoEmbarcacionDTO();
	    	tipoEmbarcacionDTO.setIdTcTipoEmbarcacion(new BigDecimal(datoIncisoCotizacionDTO.getValor()));
	    	tipoEmbarcacionDTO= tipoEmbarcacionDN.getPorId(tipoEmbarcacionDTO);
	    	slipBarcosSoporteDTO.setTipoEmbarcacion(tipoEmbarcacionDTO!=null? tipoEmbarcacionDTO.getDescripcionTipoEmbarcacion():"");
	    }
	    
	    //***************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_MATERIAL_CASCO);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_MATERIAL_CASCO);
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	 slipBarcosSoporteDTO.setMaterialCasco(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():"");
	     }else{
	    	 slipBarcosSoporteDTO.setMaterialCasco(datoIncisoCotizacionDTO.getValor());
	     }
	    //*******************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_USO);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_USO);
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	 slipBarcosSoporteDTO.setUso(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():"");
	     }else{
	    	 slipBarcosSoporteDTO.setUso(datoIncisoCotizacionDTO.getValor());
	     }
	    
	    //*********************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_PUERTO_REGISTRO);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_PUERTO_REGISTRO);
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	 slipBarcosSoporteDTO.setPuertoRegistro(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():"");
	     }else{
	    	 slipBarcosSoporteDTO.setPuertoRegistro(datoIncisoCotizacionDTO.getValor());
	     }
	    //**********************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_CLASIFICACION);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	         datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	         datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	         datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	         datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_CLASIFICACION);
	         datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	         if(datoIncisoCotizacionDTO!=null){
	        	 clasificacionEmbarcacionDN= ClasificacionEmbarcacionDN.getInstancia();
	        	 clasificacionEmbarcacionDTO= new ClasificacionEmbarcacionDTO();
	        	 clasificacionEmbarcacionDTO.setIdTcClasificacionEmbarcacion(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
	        	 clasificacionEmbarcacionDN.getPorId(clasificacionEmbarcacionDTO);
		      
	        	 clasificacionEmbarcacionDTO= clasificacionEmbarcacionDN.getPorId(clasificacionEmbarcacionDTO);
	        	 slipBarcosSoporteDTO.setClasificacion(clasificacionEmbarcacionDTO!=null ? clasificacionEmbarcacionDTO.getDescripcionClasifEmbarcacion() : "" );
	         }
	     }else{
	    	 clasificacionEmbarcacionDN= ClasificacionEmbarcacionDN.getInstancia();
	    	 clasificacionEmbarcacionDTO= new ClasificacionEmbarcacionDTO();
	    	 clasificacionEmbarcacionDTO.setIdTcClasificacionEmbarcacion(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
	    	 clasificacionEmbarcacionDN.getPorId(clasificacionEmbarcacionDTO);
	    	 clasificacionEmbarcacionDTO= clasificacionEmbarcacionDN.getPorId(clasificacionEmbarcacionDTO);
	    	 slipBarcosSoporteDTO.setClasificacion(clasificacionEmbarcacionDTO!=null ? clasificacionEmbarcacionDTO.getDescripcionClasifEmbarcacion() : "");
	     }
	    
	    //*********************************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_BANDERA);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_BANDERA);
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	 if(datoIncisoCotizacionDTO!=null){
	    		 String descripcion= UtileriasWeb.getDescripcionCatalogoValorFijo(80,datoIncisoCotizacionDTO.getValor());
	    		 slipBarcosSoporteDTO.setBandera(descripcion==null?"":descripcion);
	    	 }
	     }else{
	    	 String descripcion= UtileriasWeb.getDescripcionCatalogoValorFijo(80,datoIncisoCotizacionDTO.getValor());
        	 slipBarcosSoporteDTO.setBandera(descripcion==null?"":descripcion);
	     }
	    //*********************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_ESLORA);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	         datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	         datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	         datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	         datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_ESLORA);
	         datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	         slipBarcosSoporteDTO.setEslora(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():"");
	     }else{
	    	 slipBarcosSoporteDTO.setEslora(datoIncisoCotizacionDTO.getValor());
	     }
	    
	    //*******************************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_PUNTAL);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
		     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_PUNTAL);
		     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		     slipBarcosSoporteDTO.setPuntal(datoIncisoCotizacionDTO!=null ? datoIncisoCotizacionDTO.getValor() : "" );
	     }else{
	    	 slipBarcosSoporteDTO.setPuntal(datoIncisoCotizacionDTO.getValor());
	     }
	    
	    ///*************************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_TONELAJE_BRUTO);
	   
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_TONELAJE_BRUTO);
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	 slipBarcosSoporteDTO.setTonelajeBruto(datoIncisoCotizacionDTO!=null ? datoIncisoCotizacionDTO.getValor() : "");
	     }else{
	    	 slipBarcosSoporteDTO.setTonelajeBruto(datoIncisoCotizacionDTO.getValor());
	     }
	    //**********************************************************************/
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	     datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	     datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	     datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_TONELAJE_NETO);
	     datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	     if(datoIncisoCotizacionDTO==null){
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	 datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.BARCO_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	 datoIncisoCotizacionDTO.getId().setIdDato(Sistema.BARCO_ID_DATO_TONELAJE_NETO);
	    	 datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	 slipBarcosSoporteDTO.setTonelajeNeto(datoIncisoCotizacionDTO!=null ? datoIncisoCotizacionDTO.getValor() : "");
	     }else{
	    	 slipBarcosSoporteDTO.setTonelajeNeto(datoIncisoCotizacionDTO.getValor());
	     }
	    
	     //**************************************************************************/
	     slipBarcosSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo,cotizacionDTO, numInciso,idToSeccion, numSubInciso));
	    
	     this.cargaSumasAseguradasSlipBarco(slipBarcosSoporteDTO);
	}
	/**
	 * Carga la suma asegurada dependiendo de la lista de coberturas del <code>slipBarco</code>,
	 * @param slipBarco
	 */
	private void cargaSumasAseguradasSlipBarco(SlipBarcosSoporteDTO slipBarco){
		 BigDecimal sumaAseguradaTotal = BigDecimal.ZERO;
		 BigDecimal sumaTotalValorCasco= BigDecimal.ZERO;
		 BigDecimal sumaTotalValorRC=BigDecimal.ZERO;
		 BigDecimal sumaTotalValorVI= BigDecimal.ZERO;
		 if(slipBarco.getListaCoberturas() != null && !slipBarco.getListaCoberturas().isEmpty()){
			 for(CoberturaSoporteDTO cobertura : slipBarco.getListaCoberturas()){
				 if(cobertura.getClaveTipoSumaAsegurada()==Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
					 if(cobertura.getIdToCobertura().equals(Sistema.BARCO_COBERTURA_VALOR_CASCO)){
						 sumaTotalValorCasco = sumaTotalValorCasco.add(cobertura.getSumaAsegurada());
					 }else if(cobertura.getIdToCobertura().equals(Sistema.BARCO_COBERTURA_VALOR_VI)){
						 sumaTotalValorVI=sumaTotalValorVI.add(cobertura.getSumaAsegurada());
					 }else if(cobertura.getIdToCobertura().equals(Sistema.BARCO_COBERTURA_VALOR_RC)){
						 sumaTotalValorRC=sumaTotalValorRC.add(cobertura.getSumaAsegurada());
					 }
					 sumaAseguradaTotal= sumaAseguradaTotal.add(cobertura.getSumaAsegurada());
				 }
			 }
		 }
		 slipBarco.setValorCasco(sumaTotalValorCasco);
		 slipBarco.setSumaAseguradaCasco(sumaTotalValorCasco);
		 slipBarco.setSumaAseguradaRC(sumaTotalValorRC);
		 slipBarco.setSumaAseguradaVI(sumaTotalValorVI);
		 slipBarco.setSumaAsegurada(sumaAseguradaTotal);
	}
	
	/*----------------- METODOS PARA CARGAR SLIPS DE TRANSPORTES -----------------------*/
	private void getDatosSlipTransporte(SlipTransportesSoporteDTO slipTransporteDTO,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		IncisoCotizacionDTO incisoCotizacionDTO=null;
	    DatoIncisoCotizacionDN datoIncisoCotizacionDN = DatoIncisoCotizacionDN.getINSTANCIA();
	    DatoIncisoCotizacionDTO datoIncisoCotizacionDTO= null;
	    TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
	    TipoEmpaqueDTO tipoEmpaqueDTO= null; 
	    MedioTransporteDN medioTransporteDN= MedioTransporteDN.getInstancia();
	    MedioTransporteDTO medioTransporteDTO=null;
	    
	    for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
	    	if(inciso.getId().getNumeroInciso().equals(numInciso)){
	    		incisoCotizacionDTO=inciso;
	    	}
	    }
	    if(slipTransporteDTO==null){
	    	slipTransporteDTO= new SlipTransportesSoporteDTO();
	    }
	    slipTransporteDTO.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	    
	    //******************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_LUGAR_ORIGEN);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    slipTransporteDTO.setLugarOrigen(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	    //*****************************************************/
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_LUGAR_DESTINO);
	    }else{
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_LUGAR_DESTINO);
	    }
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    slipTransporteDTO.setLugarDestino(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	    //****************************************************/
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_BIENES_ASEGURADOS);
	    }else{
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_BIENES_ASEGURADOS);
	    }
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    slipTransporteDTO.setBienesAsegurados(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	   
	    //****************************************************/
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_TIPO_EMPAQUE);
	    }else{
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_TIPO_EMPAQUE);
	    }
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO!=null){
	    	tipoEmpaqueDTO = new TipoEmpaqueDTO();
	    	tipoEmpaqueDTO.setIdTipoEmpaque(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
	    	tipoEmpaqueDTO= tipoEmpaqueDN.getTipoEmpaquePorId(tipoEmpaqueDTO);
	    	slipTransporteDTO.setTipoEmpaque(tipoEmpaqueDTO!=null ? tipoEmpaqueDTO.getDescripcionTipoEmpaque() : null);
	    }
	    //*************************************************/
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.TRANSPORTE_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_MEDIO_TRANSPORTE);
	    }else{
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.TRANSPORTE_ID_DATO_MEDIO_TRANSPORTE);
	    }
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO!=null){
	    	medioTransporteDTO = new MedioTransporteDTO();
	    	medioTransporteDTO.setIdMedioTransporte(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
	    	medioTransporteDTO= medioTransporteDN.getMedioTransportePorId(medioTransporteDTO);
	    	slipTransporteDTO.setDescripcionMedioTransporte(medioTransporteDTO!=null?medioTransporteDTO.getDescripcionMedioTransporte():null);
	    }
	    //**********************************************/
	    slipTransporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
	}
	/**
	 * Carga los datos de un <code>SlipAviacionSoporteDTO</code>
	 * @param slipAviacionSoporteDTO
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws SystemException si fallan los metodos que son invocados de las clases DN
	 */
	private void getDatosSlipAviacion(SlipAviacionSoporteDTO slipAviacionSoporteDTO ,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
	    IncisoCotizacionDTO incisoCotizacionDTO=null;
	    DatoIncisoCotizacionDN datoIncisoCotizacionDN = DatoIncisoCotizacionDN.getINSTANCIA();
	    DatoIncisoCotizacionDTO datoIncisoCotizacionDTO= null;
	    if(slipAviacionSoporteDTO==null){
	    	slipAviacionSoporteDTO= new SlipAviacionSoporteDTO();
	    }
	    
	    TipoAeronaveDN tipoAeronaveDN= TipoAeronaveDN.getInstancia();
	    TipoAeronaveDTO tipoAeronaveDTO= null;
	    
	    for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
	    	if(inciso.getId().getNumeroInciso().equals(numInciso)){
	    		incisoCotizacionDTO=inciso;
	    	}
	    }
	    slipAviacionSoporteDTO.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	    //***********************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_MARCA);
	    datoIncisoCotizacionDTO = datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	   
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_MARCA);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	slipAviacionSoporteDTO.setMarca( datoIncisoCotizacionDTO !=null ? datoIncisoCotizacionDTO.getValor() : null );
	    }else{
	    	slipAviacionSoporteDTO.setMarca(datoIncisoCotizacionDTO.getValor());
	    }
	    
	    //*******************************************/
	   	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
		datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_MODELO);
		datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_MODELO);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	slipAviacionSoporteDTO.setModelo(datoIncisoCotizacionDTO!=null?
	    			datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setModelo(datoIncisoCotizacionDTO.getValor());
	    }
	    //***************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_TIPOAERONAVE);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	        datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_TIPOAERONAVE);
	        datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
		    if(datoIncisoCotizacionDTO!=null) {
		    	tipoAeronaveDTO= new TipoAeronaveDTO();
		    	tipoAeronaveDTO.setIdTipoAeronave(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
		    	tipoAeronaveDTO= tipoAeronaveDN.getPorId(tipoAeronaveDTO);
		    	slipAviacionSoporteDTO.setTipoAeronave(tipoAeronaveDTO!=null? tipoAeronaveDTO.getIdTipoAeronave().intValue():0);
		    }
	    }else{
	    	tipoAeronaveDTO= new TipoAeronaveDTO();
	    	tipoAeronaveDTO.setIdTipoAeronave(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
	    	tipoAeronaveDTO= tipoAeronaveDN.getPorId(tipoAeronaveDTO);
	    	slipAviacionSoporteDTO.setTipoAeronave(tipoAeronaveDTO!=null? tipoAeronaveDTO.getIdTipoAeronave().intValue():0);
	    }
	   //**************************************************************/ 
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_ANIO);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_ANIO);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
	    	slipAviacionSoporteDTO.setAnio( datoIncisoCotizacionDTO!=null ? Integer.valueOf(datoIncisoCotizacionDTO.getValor()) : 0);
	    }else{
	    	slipAviacionSoporteDTO.setAnio(Integer.valueOf(datoIncisoCotizacionDTO.getValor()));
	    }
	    //************************************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_SERIE);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	        datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_SERIE);
		    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
		    slipAviacionSoporteDTO.setSerie(datoIncisoCotizacionDTO!=null?
		    datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setSerie(datoIncisoCotizacionDTO.getValor());
	    }
	    //********************************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_MATRICULA);
	    
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	        datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_MATRICULA);
		    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
		    slipAviacionSoporteDTO.setMatricula(datoIncisoCotizacionDTO!=null?
		    datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setMatricula(datoIncisoCotizacionDTO.getValor());
	    }
	    //***********************************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_USO);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_USO);
		    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    slipAviacionSoporteDTO.setUso(datoIncisoCotizacionDTO!=null? datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setUso(datoIncisoCotizacionDTO.getValor());
	    }
	    //***********************************************************************/
	    
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_CAPACIDAD);
	   	    
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	        datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_CAPACIDAD);
	        datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
	        slipAviacionSoporteDTO.setCapacidad(datoIncisoCotizacionDTO!=null? datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setCapacidad(datoIncisoCotizacionDTO.getValor());
	    }
	    //**********************************************************************/
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_LIMITES_GEOGRAFICOS);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	        datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_LIMITES_GEOGRAFICOS);
	        datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	        slipAviacionSoporteDTO.setLimitesGeograficos(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setLimitesGeograficos(datoIncisoCotizacionDTO.getValor());
	    }
	    //************************************************************/ 
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
		datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_AEROPUERTOBASE);
		datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
		if(datoIncisoCotizacionDTO==null){
			datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	        datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	        datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_AEROPUERTOBASE);
	        datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
		    
	        slipAviacionSoporteDTO.setAeropuertoBase(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setAeropuertoBase(datoIncisoCotizacionDTO.getValor());
	    }
	    //***********************************************************/
		datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
		datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_TIPO_AEROPUERTO);
		datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO==null){
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    	datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.AVION_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
	    	datoIncisoCotizacionDTO.getId().setIdDato(Sistema.AVION_ID_DATO_TIPO_AEROPUERTO);
	    	datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    	slipAviacionSoporteDTO.setTipoAeropuerto(datoIncisoCotizacionDTO!=null?datoIncisoCotizacionDTO.getValor():null);
	    }else{
	    	slipAviacionSoporteDTO.setTipoAeropuerto(datoIncisoCotizacionDTO.getValor());
	    }
	    /*FIXME Pendiente setear la lista de Informacion de Pilotos
	    slipAviacionSoporteDTO.setListaInformacionPiloto(listaInformacionPiloto);
	    */
	    
	    slipAviacionSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
	    cargaSumasAseguradasSlipAviacion(slipAviacionSoporteDTO);
	}
	
	private void cargaSumasAseguradasSlipAviacion(SlipAviacionSoporteDTO slipAvion){
	    BigDecimal sumaPagosVoluntarioTripulacion = BigDecimal.ZERO;
	    BigDecimal sumaPagosMedicosPasajeros= BigDecimal.ZERO;
	    BigDecimal sumaPagosMedicosTripulacion= BigDecimal.ZERO;
	    
	    if(slipAvion.getListaCoberturas() != null && !slipAvion.getListaCoberturas().isEmpty()){
	    	for(CoberturaSoporteDTO cobertura: slipAvion.getListaCoberturas()){
	    		if(cobertura.getClaveTipoSumaAsegurada() == Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
	    			if(cobertura.getIdToCobertura().equals(Sistema.AVION_COBERTURA_PAGOS_VOLUNTARIOS_TRIPULACION)){
	    				sumaPagosVoluntarioTripulacion= sumaPagosVoluntarioTripulacion.add(cobertura.getSumaAsegurada());
	    			}else if(cobertura.getIdToCobertura().equals(Sistema.AVION_COBERTURA_PAGOS_MEDICOS_PASAJEROS)){
	    				sumaPagosMedicosPasajeros=sumaPagosMedicosPasajeros.add(cobertura.getSumaAsegurada());
	    			}else if(cobertura.getIdToCobertura().equals(Sistema.AVION_COBERTURA_PAGOS_MEDICOS_TRIPULACION)){
	    				sumaPagosMedicosTripulacion=sumaPagosMedicosTripulacion.add(cobertura.getSumaAsegurada());
        		    }
	    		}
	    	}
	    }
	    slipAvion.setPagosVoluntariosTripulacion(sumaPagosVoluntarioTripulacion);
	    slipAvion.setPagosMedicosPasajeros(sumaPagosMedicosPasajeros);
	    slipAvion.setPagosMedicosTripulacion(sumaPagosMedicosTripulacion);
	}
	
	/**
	 * Carga los datos del objeto <code>slipObraCivilSoporteDTO</code>
	 * @param slipObraCivilSoporteDTO
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws SystemException si fallan invocaciones a metodos de las clases del DN
	 */
	private void getDatosSlipObraCivil(SlipObraCivilSoporteDTO slipObraCivilSoporteDTO,int tipoCumulo,	CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		IncisoCotizacionDTO incisoCotizacionDTO=null;
	    DatoIncisoCotizacionDN datoIncisoCotizacionDN = DatoIncisoCotizacionDN.getINSTANCIA();
	    DatoIncisoCotizacionDTO datoIncisoCotizacionDTO= null;
	    TipoObraCivilDN tipoObraCivilDN= TipoObraCivilDN.getInstancia();
	    TipoObraCivilDTO tipoObraCivilDTO=null;
	    if(slipObraCivilSoporteDTO==null){
	    	slipObraCivilSoporteDTO= new SlipObraCivilSoporteDTO();
	    }
	    for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
	    	if(inciso.getId().getNumeroInciso().compareTo(numInciso) == 0){
	    		incisoCotizacionDTO=inciso;
	    	}
	    }
	    
	    slipObraCivilSoporteDTO.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	    slipObraCivilSoporteDTO.setLocalizacionObra(getDireccion(incisoCotizacionDTO.getDireccionDTO()));
	    
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.OBRA_CIVIL_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.OBRA_CIVIL_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.OBRA_CIVIL_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.OBRA_CIVIL_ID_DATO_TIPO_OBRA);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    if(datoIncisoCotizacionDTO!=null){
	    	tipoObraCivilDTO= new TipoObraCivilDTO();
	    	tipoObraCivilDTO.setIdTipoObraCivil(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
	    	tipoObraCivilDTO= tipoObraCivilDN.getTipoObraCivilPorId(tipoObraCivilDTO);
	    	slipObraCivilSoporteDTO.setDescripcionTrabajo(tipoObraCivilDTO!=null?tipoObraCivilDTO.getDescripcionTipoObraCivil():"");
	    }
	    
	   
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.instanciaDatoIncisoCotizacion(cotizacionDTO.getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.OBRA_CIVIL_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
	    datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.OBRA_CIVIL_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.OBRA_CIVIL_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
	    datoIncisoCotizacionDTO.getId().setIdDato(Sistema.OBRA_CIVIL_ID_DATO_CARACTERISTICA_OBRA);
	    datoIncisoCotizacionDTO= datoIncisoCotizacionDN.getPorId(datoIncisoCotizacionDTO);
	    
	    slipObraCivilSoporteDTO.setDescripcionTrabajo(slipObraCivilSoporteDTO.getDescripcionTrabajo()==null?"":slipObraCivilSoporteDTO.getDescripcionTrabajo()+datoIncisoCotizacionDTO==null?" ":datoIncisoCotizacionDTO.getValor());
	    
	    slipObraCivilSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
	    this.cargarSumasAseguradasSlipObraCivil(slipObraCivilSoporteDTO);
	}
	
	/**
	 * Carga la suma asegurada dependiendo la lista de coberturas del <code>slipObraCivil</code>.
	 * @param slipObraCivil
	 */
	private  void cargarSumasAseguradasSlipObraCivil(SlipObraCivilSoporteDTO slipObraCivil){
		BigDecimal sumaAseguradaObra= BigDecimal.ZERO;
		BigDecimal sumaTotalAsegurada= BigDecimal.ZERO;
		for(CoberturaSoporteDTO cobertura: slipObraCivil.getListaCoberturas()){
			if(cobertura.getClaveTipoSumaAsegurada().intValue() == Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA).intValue()){
				sumaTotalAsegurada= sumaTotalAsegurada.add(cobertura.getSumaAsegurada());
				sumaAseguradaObra=sumaAseguradaObra.add(cobertura.getSumaAsegurada());
			}
		}
		slipObraCivil.setSumaAseguradaObra(sumaAseguradaObra);
	    slipObraCivil.setSumaTotalAsegurada(sumaTotalAsegurada);
	}
	
	/**
	 * Carga los datos del objeto <code>slipEquipoContratistaSoporteDTO</code>.
	 * @param slipEquipoContratistaSoporteDTO
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws SystemException si fallan invocaciones a metodos de las clases del DN.
	 */
	private void getDatosSlipEquipoContratistaSoporteDTO(SlipEquipoContratistaSoporteDTO slipEquipoContratistaSoporteDTO,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		SubIncisoCotizacionDTO subIncisoCotizacionDTO=null;
		if(slipEquipoContratistaSoporteDTO!=null){	    
			for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
				if(inciso.getId().getNumeroInciso().equals(numInciso)){
					for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
						if(seccion.getId().getIdToSeccion().equals(idToSeccion) && seccion.getClaveContrato()==Sistema.CONTRATADO){
							for(SubIncisoCotizacionDTO subInciso: seccion.getSubIncisoCotizacionLista()){
								if(subInciso.getId().getNumeroSubInciso().equals(numSubInciso)){
									subIncisoCotizacionDTO= subInciso;                	    
								}
							}
						}
					}
				}
			}
			slipEquipoContratistaSoporteDTO.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
			slipEquipoContratistaSoporteDTO.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			slipEquipoContratistaSoporteDTO.setDescripcionSeccion(subIncisoCotizacionDTO.getSeccionCotizacionDTO().getSeccionDTO().getDescripcion());
			slipEquipoContratistaSoporteDTO.setDescripcionSubInciso(subIncisoCotizacionDTO.getDescripcionSubInciso());
			slipEquipoContratistaSoporteDTO.setUbicacion(getDireccion(subIncisoCotizacionDTO.getSeccionCotizacionDTO().getIncisoCotizacionDTO().getDireccionDTO()));
			slipEquipoContratistaSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
			cargaSumasAsegurasSlipEquipoContratista(slipEquipoContratistaSoporteDTO);
			slipEquipoContratistaSoporteDTO.setListaCoberturasInciso(getCoberturasSlip(Sistema.TIPO_CUMULO_INCISO, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
			cargaSumasAsegurasSlipEquipoContratista(slipEquipoContratistaSoporteDTO);
		}
	}
	/**
	 * Carga sumas aseguradas dependiendo la lista de coberturas del slip <code>slipEquipoContratista</code>
	 * @param slipEquipoContratista
	 */
	private void cargaSumasAsegurasSlipEquipoContratista(SlipEquipoContratistaSoporteDTO slipEquipoContratista){
		String descripcionSubInciso="";
		String[]datosCobertura;
		BigDecimal totalSumaAsegurada=BigDecimal.ZERO;
		BigDecimal sumaAseguradaTemporal=BigDecimal.ZERO;
		if(slipEquipoContratista.getListaCoberturasInciso() != null && !slipEquipoContratista.getListaCoberturasInciso().isEmpty()){
			for(CoberturaSoporteDTO cobertura:slipEquipoContratista.getListaCoberturasInciso()){
				if(cobertura.getSumaAsegurada().compareTo(sumaAseguradaTemporal)==1){
					sumaAseguradaTemporal=cobertura.getSumaAsegurada();
					datosCobertura= cobertura.getDescripcionCobertura().split("-");
					descripcionSubInciso=datosCobertura[2];
					totalSumaAsegurada= totalSumaAsegurada.add(cobertura.getSumaAsegurada());
				}
			}
		}
		slipEquipoContratista.setIncisoMayorValor(descripcionSubInciso+" - "+totalSumaAsegurada.toString());
	}
	/**
	 * Carga la lista de <code>SlipRCConstructoresSoporteDTO</code>
	 * @param slipsRCContructoresSoportesDTOs
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws SystemException si falla al invocar los metodos de las clases del DN.
	 */
	private void getDatosSlipRCConstructoresSoporteDTO(List<SlipRCConstructoresSoporteDTO> slipsRCContructoresSoportesDTOs,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		if(slipsRCContructoresSoportesDTOs==null){
			slipsRCContructoresSoportesDTOs= new ArrayList<SlipRCConstructoresSoporteDTO>();
		}
		if(slipsRCContructoresSoportesDTOs.isEmpty()){
			for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
				slipsRCContructoresSoportesDTOs.add(getDatoSlipRCConstructoresSoporteDTO(tipoCumulo, cotizacionDTO, inciso.getId().getNumeroInciso(), idToSeccion, numSubInciso, idTcSubRamo));
			}
		}
		else{
			for(SlipRCConstructoresSoporteDTO slipRCContructores:slipsRCContructoresSoportesDTOs){
				if(slipRCContructores != null && slipRCContructores.getNumeroInciso() != null){
					for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
						if(slipRCContructores.getNumeroInciso().compareTo(inciso.getId().getNumeroInciso()) == 0){
							poblarDatoSlipRCConstructoresSoporteDTO(slipRCContructores, tipoCumulo, cotizacionDTO, inciso.getId().getNumeroInciso(), idToSeccion, numSubInciso, idTcSubRamo);
//							slipsRCContructoresSoportesDTOs.add(getDatoSlipRCConstructoresSoporteDTO(tipoCumulo, cotizacionDTO, inciso.getId().getNumeroInciso(), idToSeccion, numSubInciso, idTcSubRamo));
						}
					}
				}
			}
		}
	 }
	/**
	 * Crea y llena un objeto <code>SlipRCConstructoresSoporteDTO</code>
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @return SlipRCConstructoresSoporteDTO
	 * @throws SystemException si falla al invocar metodos de las clases de DN
	 */
	private SlipRCConstructoresSoporteDTO getDatoSlipRCConstructoresSoporteDTO(int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso,BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		SlipRCConstructoresSoporteDTO  slipRCConstructoresSoporteDTO= new SlipRCConstructoresSoporteDTO();
		slipRCConstructoresSoporteDTO.setNumeroInciso(numInciso);
		
		slipRCConstructoresSoporteDTO.setDescripcionObra(DatoIncisoCotizacionDN.getINSTANCIA().obtenerDescripcionObraRCConstructores(cotizacionDTO.getIdToCotizacion(), numInciso));
	     //*************************/
		slipRCConstructoresSoporteDTO.setDuracionObra(DatoIncisoCotizacionDN.getINSTANCIA().obtenerDuracionObraRCConstructores(cotizacionDTO.getIdToCotizacion(), numInciso));
		
		slipRCConstructoresSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
		return slipRCConstructoresSoporteDTO;
	}
	
	private void poblarDatoSlipRCConstructoresSoporteDTO(SlipRCConstructoresSoporteDTO slipRCContructores,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso,BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		if(slipRCContructores != null){
			slipRCContructores.setNumeroInciso(numInciso);
			
			slipRCContructores.setDescripcionObra(DatoIncisoCotizacionDN.getINSTANCIA().obtenerDescripcionObraRCConstructores(cotizacionDTO.getIdToCotizacion(), numInciso));
		     //*************************/
			slipRCContructores.setDuracionObra(DatoIncisoCotizacionDN.getINSTANCIA().obtenerDuracionObraRCConstructores(cotizacionDTO.getIdToCotizacion(), numInciso));
			
			slipRCContructores.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
		}
	}
	/**
	 * Carga la lista de <code>SlipRCFuncionarioSoporteDTO</code>
	 * @param slipsRCFuncionarioSoporteDTOs
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws SystemException  si falla al invocar metodos de las clases de DN
	 */
	private void getDatosSlipRCFuncionarioSoporteDTO(List<SlipRCFuncionarioSoporteDTO> slipsRCFuncionarioSoporteDTOs,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		if(slipsRCFuncionarioSoporteDTOs!=null){
			if(slipsRCFuncionarioSoporteDTOs.isEmpty()){
				for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
					slipsRCFuncionarioSoporteDTOs.add(getDatoSlipRCFuncionarioSoporteDTO(tipoCumulo, cotizacionDTO, inciso.getId().getNumeroInciso(), idToSeccion, numSubInciso, idTcSubRamo));
				}
			}
			else{
				for(SlipRCFuncionarioSoporteDTO slipRCFuncionario:slipsRCFuncionarioSoporteDTOs){
					if(slipRCFuncionario != null && slipRCFuncionario.getNumeroInciso() != null){
						for(IncisoCotizacionDTO inciso: cotizacionDTO.getIncisoCotizacionDTOs()){
							if(slipRCFuncionario.getNumeroInciso().compareTo(inciso.getId().getNumeroInciso()) == 0){
								poblarDatoSlipRCFuncionarioSoporteDTO(slipRCFuncionario, tipoCumulo, cotizacionDTO, inciso.getId().getNumeroInciso(), idToSeccion, numSubInciso, idTcSubRamo);
							}
						}
					}
				}
			}
		}
	}
	 /**
	  * Crea y llena un objeto <code>SlipRCFuncionarioSoporteDTO</code>
	  * @param tipoCumulo
	  * @param cotizacionDTO
	  * @param numInciso
	  * @param idToSeccion
	  * @param numSubInciso
	  * @param idTcSubRamo
	  * @return
	  * @throws SystemException
	  */
	private SlipRCFuncionarioSoporteDTO getDatoSlipRCFuncionarioSoporteDTO(int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		SlipRCFuncionarioSoporteDTO  slipRCFuncionarioSoporteDTO= new SlipRCFuncionarioSoporteDTO();
		slipRCFuncionarioSoporteDTO.setNumeroInciso(numInciso);
		slipRCFuncionarioSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
		return slipRCFuncionarioSoporteDTO;
	}
	
	private void poblarDatoSlipRCFuncionarioSoporteDTO(SlipRCFuncionarioSoporteDTO  slipRCFuncionarioSoporteDTO,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		if(slipRCFuncionarioSoporteDTO != null){
			slipRCFuncionarioSoporteDTO.setNumeroInciso(numInciso);
			slipRCFuncionarioSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
		}
	}
	
	/**
	 * Carga el objeto <code>slipGeneralSoporteDTO</code> dependiendo si es por inciso o 
	 * por subinciso,
	 * @param slipGeneralSoporteDTO
	 * @param tipoCumulo
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param idTcSubRamo
	 * @throws SystemException si falla los metodos que son invocados en las clases DN,
	 */
	private void getDatoSlipGeneralSoporteDTO(SlipGeneralSoporteDTO slipGeneralSoporteDTO,int tipoCumulo,CotizacionDTO cotizacionDTO, BigDecimal numInciso, BigDecimal idToSeccion,BigDecimal numSubInciso,BigDecimal idTcSubRamo) throws SystemException{
		if(slipGeneralSoporteDTO!=null){
			if(tipoCumulo==Sistema.TIPO_CUMULO_INCISO ||  tipoCumulo==Sistema.TIPO_CUMULO_SUBINCISO){
				if(tipoCumulo==Sistema.TIPO_CUMULO_INCISO){
					poblarDatosIncisoSlipGeneral(cotizacionDTO, numInciso, idToSeccion, numSubInciso, slipGeneralSoporteDTO);
				}else if(tipoCumulo==Sistema.TIPO_CUMULO_SUBINCISO){
					poblarDatosSubIncisoSlipGeneral	(cotizacionDTO, numInciso, idToSeccion, numSubInciso, slipGeneralSoporteDTO);
				}
				slipGeneralSoporteDTO.setListaCoberturas(getCoberturasSlip(tipoCumulo, false, idTcSubRamo, cotizacionDTO, numInciso, idToSeccion, numSubInciso));
			}
		}
	}
	
	/**
	 * Carga los datos de un slip General por SubInciso
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param slipGeneralSoporteDTO
	 * @throws SystemException si falla los metodos invocados de las clases DN
	 */
	private void poblarDatosSubIncisoSlipGeneral(CotizacionDTO cotizacionDTO, BigDecimal numInciso,BigDecimal idToSeccion, BigDecimal numSubInciso, SlipGeneralSoporteDTO slipGeneralSoporteDTO) throws SystemException{
		SubIncisoCotizacionDTO subIncisoCotizacionDTO=null;
	    StringBuilder datosBien= new StringBuilder();
	    String[]claveValor;
	    DatoIncisoCotizacionDN datoIncisoCotizacionDN= DatoIncisoCotizacionDN.getINSTANCIA();
	    ConfiguracionDatoIncisoCotizacionDN configuracionDatoIncisoCotizacionDN= new ConfiguracionDatoIncisoCotizacionDN();
	    DatoIncisoCotizacionId datoIncisoCotizacionID= new DatoIncisoCotizacionId();
	    
	    for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
	    	if(inciso.getId().getNumeroInciso().equals(numInciso)){
	    		for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
	    			if(seccion.getId().getIdToSeccion().equals(idToSeccion) && seccion.getClaveContrato()==Sistema.CONTRATADO){
	    				for(SubIncisoCotizacionDTO subInciso: seccion.getSubIncisoCotizacionLista()){
	    					if(subInciso.getId().getNumeroSubInciso().equals(numSubInciso)){
	    						subIncisoCotizacionDTO= subInciso;                	    
	    					}
	    				}
	    			}
	    		}
	    	}	  
	    }
	    datoIncisoCotizacionID.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
	    datoIncisoCotizacionID.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionID.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
	    datoIncisoCotizacionID.setNumeroSubinciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
	    datoIncisoCotizacionID.setClaveDetalle((short)2);
	    
	    List<DatoIncisoCotizacionDTO> datosDinamicos= datoIncisoCotizacionDN.listarPorIdFiltrado(datoIncisoCotizacionID);
	    if(datosDinamicos!=null && !datosDinamicos.isEmpty()){
	    	for(DatoIncisoCotizacionDTO dato:datosDinamicos){
	    		claveValor=configuracionDatoIncisoCotizacionDN.obtenerDescripcionDatoInciso(dato,null);
	    		if(claveValor!=null){
	    			datosBien.append(claveValor[0]);
	    			datosBien.append(':');
	    			datosBien.append(claveValor[1]);
	    			datosBien.append('\n');
	    		}
	    	}
	    }
	    if(datosBien.toString().length()>0) {
			slipGeneralSoporteDTO.setDatosBien(datosBien.toString());
		}
	    slipGeneralSoporteDTO.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
	    slipGeneralSoporteDTO.setSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
	    slipGeneralSoporteDTO.setDescripcionSeccion(subIncisoCotizacionDTO.getSeccionCotizacionDTO().getSeccionDTO().getDescripcion());
	    slipGeneralSoporteDTO.setDescripcionSubInciso(subIncisoCotizacionDTO.getDescripcionSubInciso());
	}
	/**
	 *  Carga los datos de un slip General por Inciso
	 * @param cotizacionDTO
	 * @param numInciso
	 * @param idToSeccion
	 * @param numSubInciso
	 * @param slipGeneralSoporteDTO
	 * @throws SystemException si falla los metodos que son invocados de la clases DN.
	 */
	private void poblarDatosIncisoSlipGeneral(CotizacionDTO cotizacionDTO, BigDecimal numInciso,BigDecimal idToSeccion, BigDecimal numSubInciso, SlipGeneralSoporteDTO slipGeneralSoporteDTO) throws SystemException{
		StringBuilder datosBien= new StringBuilder();
	    String[]claveValor;
	    DatoIncisoCotizacionDN datoIncisoCotizacionDN= DatoIncisoCotizacionDN.getINSTANCIA();
	    ConfiguracionDatoIncisoCotizacionDN configuracionDatoIncisoCotizacionDN= new ConfiguracionDatoIncisoCotizacionDN();
	    DatoIncisoCotizacionId datoIncisoCotizacionID= new DatoIncisoCotizacionId();
	    IncisoCotizacionDTO incisoCotizacionDTO=null;
	    
	    for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
	    	if(inciso.getId().getNumeroInciso().compareTo(numInciso) == 0){
	    		incisoCotizacionDTO= inciso;
	    	}
	    }
	    datoIncisoCotizacionID.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
	    datoIncisoCotizacionID.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	    datoIncisoCotizacionID.setClaveDetalle((short)2);
	    
	    List<DatoIncisoCotizacionDTO> datosDinamicos= datoIncisoCotizacionDN.listarPorIdFiltrado(datoIncisoCotizacionID);
	    
	    if(datosDinamicos!=null && !datosDinamicos.isEmpty()){
	    	for(DatoIncisoCotizacionDTO dato:datosDinamicos){
	    		claveValor=configuracionDatoIncisoCotizacionDN.obtenerDescripcionDatoInciso(dato,null);
	    		if(claveValor!=null){
	    			datosBien.append(claveValor[0]);
	    			datosBien.append(':');
	    			datosBien.append(claveValor[1]);
	    			datosBien.append('\n');
	    		}
	    	}
	    }
	    if(datosBien.toString().length()>0) {
			slipGeneralSoporteDTO.setDatosBien(datosBien.toString());
		}
	    slipGeneralSoporteDTO.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	}
	/**
	 * Metodo que a partir de un objeto <code>direccionDTO</code> te devuelve
	 * un <code>String</code> de una direccion ,
	 * @param direccionDTO
	 * @return String direccion formateada,
	 */
	private String getDireccion(DireccionDTO direccionDTO){
	    if(direccionDTO==null){
		return "N-A";
	    }
	    CodigoPostalDN codigoPostalDN= CodigoPostalDN.getInstancia();
	    
	    StringBuilder direccion= new StringBuilder();
	    direccion.append(direccionDTO.getNombreCalle()==null?"":direccionDTO.getNombreCalle());
	    direccion.append(direccionDTO.getNumeroExterior()==null?"":" # "+direccionDTO.getNumeroExterior());
	    direccion.append(direccionDTO.getNumeroInterior()==null?"":" "+direccionDTO.getNumeroInterior());
	    direccion.append(direccionDTO.getNombreDelegacion()==null?"":","+direccionDTO.getNombreDelegacion());
	    if(direccionDTO.getNombreColonia()!=null){
		ColoniaDTO coloniaDTO= codigoPostalDN.getColoniaPorId(direccionDTO.getNombreColonia());
		if(coloniaDTO!=null){
		   direccion.append(","+coloniaDTO.getDescription());
		   direccion.append(",C.P. "+coloniaDTO.getZipCode());
		}else{
		   direccion.append("");
		}
		
	    }else{
		  direccion.append("");
	    }
	    if(direccionDTO.getIdMunicipio()!=null){
        	    MunicipioDTO municipioDTO = codigoPostalDN.getMunicipioPorId
        	    (direccionDTO.getIdMunicipio().toString());
        	    direccion.append(municipioDTO==null?"":","+municipioDTO.getDescription());
	    }
	    if(direccionDTO.getIdEstado()!=null){
        	    EstadoDTO estado = CodigoPostalDN.getInstancia().getEstadoPorId
        	    (direccionDTO.getIdEstado().toString());
        	    direccion.append(estado==null?"":","+estado.getDescription()+".");
	    }
	    
	    return direccion.toString().length()==0?"N-A":direccion.toString();
	    
	}
	
	/**
	 * Devuelve una lista de <code>CoberturaSoporteDTO</code>
	 * para cargar los objetos de los distintos tipos de slip
	 * del objeto <code>SlipSoporteDTO</code>
	 * @param tipoCumulo
	 * @param primerRiesgo
	 * @param idTcSubRamo
	 * @param cotizacionDTO
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param numeroSubInciso
	 * @return
	 * @throws SystemException si ocurre un error al invocar los metodos de las clases de DN,
	 */
	private List<CoberturaSoporteDTO> getCoberturasSlip(int tipoCumulo,boolean primerRiesgo, BigDecimal idTcSubRamo,CotizacionDTO cotizacionDTO, BigDecimal numeroInciso,BigDecimal idToSeccion,BigDecimal numeroSubInciso) throws SystemException {
        List<CoberturaSoporteDTO> coberturasSoporteDTO = new ArrayList<CoberturaSoporteDTO>();

        DetallePrimaCoberturaCotizacionDN detalleCoberturaCotizacionDN = DetallePrimaCoberturaCotizacionDN.getInstancia();
        DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId;
        List<DetallePrimaCoberturaCotizacionDTO> detallesCoberturasCotizacion;
        double factor= UtileriasWeb.getFactorVigencia(cotizacionDTO);
	
        switch (tipoCumulo) {
        	case Sistema.TIPO_CUMULO_POLIZA:
        		for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
        			for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
        				if(seccion.getClaveContrato()== Sistema.CONTRATADO){
        					for(CoberturaCotizacionDTO cobertura:seccion.getCoberturaCotizacionLista()){
        						if(cobertura.getClaveContrato()==Sistema.CONTRATADO &&
        								cobertura.getIdTcSubramo().equals(idTcSubRamo)){
        							if(!CoberturaCotizacionDN.getInstancia().aplicoPrimerRiesgo(cobertura)){
        								coberturasSoporteDTO.add(obtenerCoberturaSoporteDTO(cobertura, factor)); 
        							}
        						}
        					}
        				}
        			}
        		}
        		break;
        	case Sistema.TIPO_CUMULO_INCISO:
        		if(primerRiesgo){
        			for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
        				if(inciso.getId().getNumeroInciso().equals(numeroInciso)){
        					for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
        						if(seccion.getClaveContrato()== Sistema.CONTRATADO){
        							for(CoberturaCotizacionDTO cobertura:seccion.getCoberturaCotizacionLista()){
        								if(cobertura.getClaveContrato()==Sistema.CONTRATADO && cobertura.getIdTcSubramo().equals(idTcSubRamo)){
        									if(CoberturaCotizacionDN.getInstancia().aplicoPrimerRiesgo(cobertura)){
        										coberturasSoporteDTO.add(obtenerCoberturaSoporteDTO(cobertura, factor)); 
        									}
        								}
        							}
        						}
        					}
        				}
        			}
        		}else{
        			for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
        				if(inciso.getId().getNumeroInciso().equals(numeroInciso)){
        					for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
        						if(seccion.getClaveContrato()== Sistema.CONTRATADO){
        							for(CoberturaCotizacionDTO cobertura:seccion.getCoberturaCotizacionLista()){
        								if(cobertura.getClaveContrato()==Sistema.CONTRATADO && cobertura.getIdTcSubramo().equals(idTcSubRamo)){
        									if(!CoberturaCotizacionDN.getInstancia().aplicoPrimerRiesgo(cobertura)){
        										coberturasSoporteDTO.add(obtenerCoberturaSoporteDTO(cobertura, factor)); 
        									}
        								}
        							}
        						}
        					}
        				}
        			}
        		}
        		break;
        	case Sistema.TIPO_CUMULO_SUBINCISO:
        		for(IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()){
        			if(inciso.getId().getNumeroInciso().equals(numeroInciso)){
        				for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
        					if(seccion.getId().getIdToSeccion().equals(idToSeccion) && seccion.getClaveContrato()==Sistema.CONTRATADO){
        						for(SubIncisoCotizacionDTO subInciso: seccion.getSubIncisoCotizacionLista()){
        							if(subInciso.getId().getNumeroSubInciso().equals(numeroSubInciso)){
        								detallePrimaCoberturaCotizacionId= new DetallePrimaCoberturaCotizacionId();
        								detallePrimaCoberturaCotizacionId.setIdToCotizacion(seccion.getId().getIdToCotizacion());
        								detallePrimaCoberturaCotizacionId.setIdToSeccion(seccion.getId().getIdToSeccion());
        								detallePrimaCoberturaCotizacionId.setNumeroInciso(seccion.getId().getNumeroInciso());
        								detallePrimaCoberturaCotizacionId.setNumeroSubInciso(numeroSubInciso);
        								detallesCoberturasCotizacion= detalleCoberturaCotizacionDN.findBySubIncisoCotizacion(detallePrimaCoberturaCotizacionId);
        								if(detallesCoberturasCotizacion!=null && !detallesCoberturasCotizacion.isEmpty()){
        									for(DetallePrimaCoberturaCotizacionDTO detallePrima:detallesCoberturasCotizacion){
        										if(detallePrima.getCoberturaCotizacionDTO().getClaveContrato()==Sistema.CONTRATADO && detallePrima.getCoberturaCotizacionDTO().getIdTcSubramo().equals(idTcSubRamo)){
        											if(!CoberturaCotizacionDN.getInstancia().aplicoPrimerRiesgo(detallePrima.getCoberturaCotizacionDTO())){
        												coberturasSoporteDTO.add(obtenerCoberturaSoporteDTO(detallePrima, subInciso, factor));
        											}
        										}
        									}
        								}
        							}
        						}
        					}
        				}
        			}
        		}
        		break;
        }
        return coberturasSoporteDTO;
	}
	
	private CoberturaSoporteDTO obtenerCoberturaSoporteDTO(CoberturaCotizacionDTO cobertura,double factor){
		CoberturaSoporteDTO coberturaSoporteDTO = new CoberturaSoporteDTO();
		coberturaSoporteDTO.setIdToCotizacion(cobertura.getId().getIdToCotizacion());
		coberturaSoporteDTO.setNumeroInciso(cobertura.getId().getNumeroInciso());
		coberturaSoporteDTO.setIdToSeccion(cobertura.getId().getIdToSeccion());
		coberturaSoporteDTO.setIdToCobertura(cobertura.getId().getIdToCobertura());
		coberturaSoporteDTO.setSumaAsegurada(new BigDecimal(cobertura.getValorSumaAsegurada().toString()));
		coberturaSoporteDTO.setClaveTipoSumaAsegurada(Integer.valueOf(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada()));
		StringBuilder descripcionCobertura= new StringBuilder();
		descripcionCobertura.append(cobertura.getId().getNumeroInciso());
		descripcionCobertura.append("-");
		descripcionCobertura.append(cobertura.getSeccionCotizacionDTO().getSeccionDTO().getNombreComercial());
		descripcionCobertura.append("-");
		descripcionCobertura.append(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
		coberturaSoporteDTO.setDescripcionCobertura(descripcionCobertura.toString());  
		coberturaSoporteDTO.setDeducible(new BigDecimal(cobertura.getValorDeducible().toString()));
		coberturaSoporteDTO.setCoaseguro(new BigDecimal(cobertura.getValorCoaseguro().toString()));
		coberturaSoporteDTO.setCuota(new BigDecimal(cobertura.getValorCuota().toString()));
		coberturaSoporteDTO.setPrima(new BigDecimal(cobertura.getValorPrimaNeta().toString()).multiply(new BigDecimal(factor)));
		try{
			SubRamoDTO subRamoDTO=SubRamoDN.getInstancia().getSubRamoPorId(cobertura.getIdTcSubramo());
			coberturaSoporteDTO.setDescripcionSubRamo(subRamoDTO.getDescripcionSubRamo());
		}catch(Exception e){
			coberturaSoporteDTO.setDescripcionSubRamo("No disponible");
		}
		return coberturaSoporteDTO;
	}
	
	private CoberturaSoporteDTO obtenerCoberturaSoporteDTO(DetallePrimaCoberturaCotizacionDTO detallePrima,SubIncisoCotizacionDTO subInciso,double factor){
		CoberturaSoporteDTO coberturaSoporteDTO = new CoberturaSoporteDTO();
		coberturaSoporteDTO.setIdToCotizacion(detallePrima.getId().getIdToCotizacion());
		coberturaSoporteDTO.setNumeroInciso(detallePrima.getId().getNumeroInciso());
		coberturaSoporteDTO.setIdToSeccion(detallePrima.getId().getIdToSeccion());
		coberturaSoporteDTO.setIdToCobertura(detallePrima.getId().getIdToCobertura());
		coberturaSoporteDTO.setNumeroSubInciso(detallePrima.getId().getNumeroSubInciso());
		StringBuilder descripcionCobertura= new StringBuilder();
		descripcionCobertura.append(detallePrima.getCoberturaCotizacionDTO().getId().getNumeroInciso());
		descripcionCobertura.append("-");
		descripcionCobertura.append(detallePrima.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getNombreComercial());
		descripcionCobertura.append("-");
		descripcionCobertura.append(subInciso.getDescripcionSubInciso());
		descripcionCobertura.append("-");
		descripcionCobertura.append(detallePrima.getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
		coberturaSoporteDTO.setDescripcionCobertura(descripcionCobertura.toString());  
		coberturaSoporteDTO.setCuota(new BigDecimal(detallePrima.getValorCuota()));
		coberturaSoporteDTO.setPrima(new BigDecimal(detallePrima.getValorPrimaNeta().toString()).multiply(new BigDecimal(String.valueOf(factor))));
		coberturaSoporteDTO.setSumaAsegurada(new BigDecimal(detallePrima.getCoberturaCotizacionDTO().getValorSumaAsegurada().toString()));
		coberturaSoporteDTO.setClaveTipoSumaAsegurada(Integer.valueOf(detallePrima.getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada()));
		coberturaSoporteDTO.setDeducible(new BigDecimal(detallePrima.getCoberturaCotizacionDTO().getValorDeducible().toString()));
		coberturaSoporteDTO.setCoaseguro(new BigDecimal(detallePrima.getCoberturaCotizacionDTO().getValorCoaseguro().toString()));
		try{
			SubRamoDTO subRamoDTO=SubRamoDN.getInstancia().getSubRamoPorId(detallePrima.getCoberturaCotizacionDTO().getIdTcSubramo());
			coberturaSoporteDTO.setDescripcionSubRamo(subRamoDTO.getDescripcionSubRamo());
		}catch(Exception e){
			coberturaSoporteDTO.setDescripcionSubRamo("No disponible");
		}
		return coberturaSoporteDTO;
	}
	
	/**
	 * Devuelve un objeto <code>CoberturaSoporteDaniosSiniestroDTO</code> donde 
	 * presenta los datos de una cobertura b�sica de una Poliza
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param numeroSubInciso
	 * @return
	 * @throws SystemException
	 */
	public CoberturaSoporteDaniosSiniestroDTO  obtenerCoberturaBasica(BigDecimal idToPoliza,
		Short numeroEndoso,
		BigDecimal numeroInciso,
		BigDecimal idToSeccion, 
		BigDecimal idToCobertura,
		BigDecimal numeroSubInciso) throws SystemException{
		    CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		    CoberturaCotizacionId coberturaCotizacionId;
		    CoberturaCotizacionDTO coberturaCotizacionDTO;
		    CoberturaSoporteDaniosSiniestroDTO coberturaBasicaSoporteDTO=null;
		    CoberturaDTO coberturaDTO;
		    CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		    BigDecimal idCoberturaBasica = new BigDecimal(0);
		    EndosoDN endosoDN= EndosoDN.getInstancia(null);
		    EndosoDTO endosoDTO;
		    EndosoId endosoId;
		    SubIncisoCotizacionDN subIncisoCotizacionDN;
		    SubIncisoCotizacionDTO subIncisoCotizacionDTO;
		    SubIncisoCotizacionDTOId subIncisoCotizacionDTOId;
		    
		    coberturaDTO= coberturaDN.getPorId(idToCobertura);
		    
		    endosoId= new EndosoId();
			endosoId.setIdToPoliza(idToPoliza);
			endosoId.setNumeroEndoso(numeroEndoso);
			endosoDTO= endosoDN.getPorId(endosoId);
			
			if(endosoDTO!= null){
				if(coberturaDTO != null){
					if(coberturaDTO.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
					     idCoberturaBasica= idToCobertura;
					}else{				 			
						if(Sistema.COBERTURAS_AMPARADAS_RC.contains(idToCobertura)){
							SeccionCotizacionDTOId seccion = new SeccionCotizacionDTOId(endosoDTO.getIdToCotizacion(), numeroInciso, idToSeccion);
							List<CoberturaCotizacionDTO> coberturasBasicasRC = 
								coberturaCotizacionDN.listarCoberturasContratadasEnConjunto(seccion, Sistema.COBERTURAS_BASICAS_RC);
							if(coberturasBasicasRC != null && !coberturasBasicasRC.isEmpty()) {
								idCoberturaBasica = coberturasBasicasRC.get(0).getId().getIdToCobertura();
							}						 
						}else{
							idCoberturaBasica=coberturaDTO.getIdCoberturaSumaAsegurada();
						}
					}						       					
					coberturaCotizacionId= new CoberturaCotizacionId();
					coberturaCotizacionId.setIdToCobertura(idCoberturaBasica);
					coberturaCotizacionId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
					coberturaCotizacionId.setIdToSeccion(idToSeccion);
					coberturaCotizacionId.setNumeroInciso(numeroInciso);
					coberturaCotizacionDTO = coberturaCotizacionDN.obtenerCoberturaContratadaPorId(coberturaCotizacionId);
					if(coberturaCotizacionDTO!=null){
						coberturaBasicaSoporteDTO= new CoberturaSoporteDaniosSiniestroDTO();
						coberturaBasicaSoporteDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
						coberturaBasicaSoporteDTO.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
						coberturaBasicaSoporteDTO.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
						coberturaBasicaSoporteDTO.setNombreComercial(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
						coberturaBasicaSoporteDTO.setSumaAsegurada(new BigDecimal(coberturaCotizacionDTO.getValorSumaAsegurada().toString()));
						coberturaBasicaSoporteDTO.setClaveTipoSumaAsegurada(Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA));
						if(numeroSubInciso!=null && numeroSubInciso.compareTo(BigDecimal.ZERO)>0){
							subIncisoCotizacionDN= SubIncisoCotizacionDN.getInstancia();
							subIncisoCotizacionDTOId= new  SubIncisoCotizacionDTOId();
							subIncisoCotizacionDTOId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
							subIncisoCotizacionDTOId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
							subIncisoCotizacionDTOId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
							subIncisoCotizacionDTOId.setNumeroSubInciso(numeroSubInciso);
							subIncisoCotizacionDTO = subIncisoCotizacionDN.getPorId(subIncisoCotizacionDTOId);
							if(subIncisoCotizacionDTO!=null){
								coberturaBasicaSoporteDTO.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
						    	coberturaBasicaSoporteDTO.setSumaAsegurada(new BigDecimal(subIncisoCotizacionDTO.getValorSumaAsegurada().toString()));
						    }
						}else{
							coberturaBasicaSoporteDTO.setNumeroSubInciso(BigDecimal.ZERO);
					}
				}
			}				    
		}
		return coberturaBasicaSoporteDTO;
	}
	
	/**
	 * Devuelve las coberturas Amparadas y las de sublimites, a partir de una
	 * cobertura b�sica en la lista de <code>CoberturaSoporteDaniosSiniestroDTO</code>
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param numeroEndoso
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CoberturaSoporteDaniosSiniestroDTO> obtenerCoberturasAmparadasSubLimites(
		BigDecimal idToPoliza,
		BigDecimal numeroInciso,
		BigDecimal idToSeccion, 
		BigDecimal idToCobertura,
		Short numeroEndoso) throws ExcepcionDeAccesoADatos, SystemException{
		CoberturaCotizacionDN coberturaCotizacionDN;
	     CoberturaSoporteDaniosSiniestroDTO coberturaSoporteDTO;
	     List<CoberturaCotizacionDTO> coberturasAsociadas;
	    List<CoberturaSoporteDaniosSiniestroDTO> coberturasAsociadasSoporte= new ArrayList<CoberturaSoporteDaniosSiniestroDTO>();
	    EndosoDN endosoDN= EndosoDN.getInstancia(null);
	    EndosoDTO endosoDTO;
	    SeccionCotizacionDTOId seccionCotizacionDTOId;
	    
	    EndosoId endosoId=new EndosoId();
		endosoId.setIdToPoliza(idToPoliza);
		endosoId.setNumeroEndoso(numeroEndoso);
		endosoDTO= endosoDN.getPorId(endosoId);
		if(endosoDTO!= null){
		        coberturaCotizacionDN= CoberturaCotizacionDN.getInstancia();
		    	seccionCotizacionDTOId= new SeccionCotizacionDTOId();
		    	seccionCotizacionDTOId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
		    	seccionCotizacionDTOId.setIdToSeccion(idToSeccion);
		    	seccionCotizacionDTOId.setNumeroInciso(numeroInciso);
			coberturasAsociadas= coberturaCotizacionDN.listarCoberturasAsociadasPorSeccionCotId(seccionCotizacionDTOId, idToCobertura);
			if(coberturasAsociadas!= null && !coberturasAsociadas.isEmpty()){
					for(CoberturaCotizacionDTO coberturaCotizacionDTO:coberturasAsociadas){
					    if(!coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
					      		coberturaSoporteDTO= new CoberturaSoporteDaniosSiniestroDTO();
			        			coberturaSoporteDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
			        			coberturaSoporteDTO.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
			        			coberturaSoporteDTO.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
			        			coberturaSoporteDTO.setNombreComercial(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
			        			coberturaSoporteDTO.setClaveTipoSumaAsegurada(Integer.valueOf(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada()));
			        			coberturaSoporteDTO.setSumaAsegurada(new BigDecimal(coberturaCotizacionDTO.getValorSumaAsegurada().toString()));
			        			coberturasAsociadasSoporte.add(coberturaSoporteDTO);
					    	}
					}
				
			    }
		}
	    
	    
	    return coberturasAsociadasSoporte;
	}

	/**
	 * Devuelve los sig. valores dependiendo de busquedas y validaciones.
        	SD_APLICA_PRIMER_RIESGO = 1;
        	SD_NO_APLICA_PRIMER_RIESGO = 0;
        	SD_NO_SE_ENCONTRO_COBERTURA = -1;
        	SD_NO_SE_ENCONTRO_ENDOSO = -2;
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param numeroEndoso
	 * @return int
	 * @throws SystemException
	 */
	
	public int aplicoPrimerRiesgoCobertura(BigDecimal idToPoliza,
			BigDecimal numeroInciso,BigDecimal idToSeccion,
			BigDecimal idToCobertura,Short numeroEndoso) throws SystemException{
		CoberturaCotizacionDN coberturaCotizacionDN;
		CoberturaCotizacionId coberturaCotizacionId;
		CoberturaCotizacionDTO coberturaCotizacionDTO;
		EndosoDN endosoDN= EndosoDN.getInstancia(null);
		EndosoId endosoId=new EndosoId();
	    endosoId.setIdToPoliza(idToPoliza);
	    endosoId.setNumeroEndoso(numeroEndoso);
	    EndosoDTO endosoDTO= endosoDN.getPorId(endosoId);
			
		if(endosoDTO!= null){	
			coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
			coberturaCotizacionId= new CoberturaCotizacionId();
			coberturaCotizacionId.setIdToCobertura(idToCobertura);
			coberturaCotizacionId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
			coberturaCotizacionId.setIdToSeccion(idToSeccion);
			coberturaCotizacionId.setNumeroInciso(numeroInciso);
			coberturaCotizacionDTO= coberturaCotizacionDN.obtenerCoberturaContratadaPorId(coberturaCotizacionId);
		  		    
		    if(coberturaCotizacionDTO!=null){
		    	return coberturaCotizacionDTO.getNumeroAgrupacion().compareTo((short)0)!=0?Sistema.SD_APLICA_PRIMER_RIESGO:Sistema.SD_NO_APLICA_PRIMER_RIESGO;
		    }else{
		    	return Sistema.SD_NO_SE_ENCONTRO_COBERTURA;
		    }
	    
		}
		return Sistema.SD_NO_SE_ENCONTRO_ENDOSO;
	}
	
	/**
	 * Devuelve  la suma asegurada de una cobertura que aplica LUC o Primer Riesgo,
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param numeroEndoso
	 * @return
	 * @throws SystemException
	 */
	public double obtenerSumaAseguradaCoberturaPrimerRiesgoLUC(BigDecimal idToPoliza,
		BigDecimal numeroInciso,
		BigDecimal idToSeccion, 
		BigDecimal idToCobertura,
		Short numeroEndoso) throws SystemException{
	    double valorSumaAsegurada=0;
	    CoberturaCotizacionDN coberturaCotizacionDN;
	    CoberturaCotizacionId coberturaCotizacionId;
	    CoberturaCotizacionDTO coberturaCotizacionDTO;
	    PrimerRiesgoLUCDN primerRiesgoLUCDN= PrimerRiesgoLUCDN.getInstancia();
            AgrupacionCotId agrupacionCotId;
	    AgrupacionCotDTO agrupacionCotDTO;
	    EndosoDN endosoDN= EndosoDN.getInstancia(null);
	    EndosoId endosoId=new EndosoId();
	    endosoId.setIdToPoliza(idToPoliza);
	    endosoId.setNumeroEndoso(numeroEndoso);
	    EndosoDTO endosoDTO= endosoDN.getPorId(endosoId);
			
		if(endosoDTO != null){	
		    coberturaCotizacionDN=CoberturaCotizacionDN.getInstancia();
		    coberturaCotizacionId= new CoberturaCotizacionId();
		    coberturaCotizacionId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
		    coberturaCotizacionId.setNumeroInciso(numeroInciso);
		    coberturaCotizacionId.setIdToSeccion(idToSeccion);
		    coberturaCotizacionId.setIdToCobertura(idToCobertura);
		    coberturaCotizacionDTO=coberturaCotizacionDN.obtenerCoberturaContratadaPorId(coberturaCotizacionId);
		    if(coberturaCotizacionDTO!=null){
				if( coberturaCotizacionDTO.getNumeroAgrupacion().compareTo((short)0)!=0){
				    agrupacionCotId= new AgrupacionCotId();
				    agrupacionCotId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
				    agrupacionCotId.setNumeroAgrupacion(coberturaCotizacionDTO.getNumeroAgrupacion());
				    agrupacionCotDTO= new AgrupacionCotDTO();
				    agrupacionCotDTO.setId(agrupacionCotId);
				    agrupacionCotDTO= primerRiesgoLUCDN.getPorId(agrupacionCotDTO);
				    
				    if(agrupacionCotDTO!= null){
				    	return agrupacionCotDTO.getValorSumaAsegurada();
				    }
				}
			
		    }
		}
		return valorSumaAsegurada;
	}
	
	/**
	 * Devuelve una listado de coberturas que apliquen primer riesgo o LUC en una poliza
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 * @throws SystemException
	 */
	
	public List<CoberturaSoporteDaniosSiniestroDTO> obtenerCoberturasPrimerRiesgoLUC(BigDecimal idToPoliza,
		Short numeroEndoso, BigDecimal numeroInciso,BigDecimal idToCobertura) throws SystemException{
		CoberturaSoporteDaniosSiniestroDTO coberturaSoportePrimerRiesgoLUC;
		List<CoberturaSoporteDaniosSiniestroDTO> coberturasSoprtePrimerRiesgoLUC= new ArrayList<CoberturaSoporteDaniosSiniestroDTO>();
		List<CoberturaCotizacionDTO> coberturasPrimerRiesgoLUC;
		CoberturaCotizacionDN coberturaCotizacionDN;
		EndosoDN endosoDN= EndosoDN.getInstancia(null);
		EndosoId endosoId=new EndosoId();
		endosoId.setIdToPoliza(idToPoliza);
		endosoId.setNumeroEndoso(numeroEndoso);
		EndosoDTO endosoDTO= endosoDN.getPorId(endosoId);
				
		if(endosoDTO != null){	
			coberturaCotizacionDN= CoberturaCotizacionDN.getInstancia();
			coberturasPrimerRiesgoLUC=coberturaCotizacionDN.
			listarCoberturasContratadasApliquenPrimerRiesgoLUC(endosoDTO.getIdToCotizacion(), numeroInciso, idToCobertura);
			PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia(); 
		    if(coberturasPrimerRiesgoLUC!=null && !coberturasPrimerRiesgoLUC.isEmpty()){
				AgrupacionCotDTO agrupacionCotDTO = null;
				AgrupacionCotId id = null;
				for(CoberturaCotizacionDTO cobertura:coberturasPrimerRiesgoLUC){
						
				    coberturaSoportePrimerRiesgoLUC= new CoberturaSoporteDaniosSiniestroDTO();
				    coberturaSoportePrimerRiesgoLUC.setIdToCobertura(cobertura.getId().getIdToCobertura());
				    coberturaSoportePrimerRiesgoLUC.setClaveTipoSumaAsegurada(Integer.valueOf
					      (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada()));
				    coberturaSoportePrimerRiesgoLUC.setIdToSeccion(cobertura.getId().getIdToSeccion());
				    coberturaSoportePrimerRiesgoLUC.setNumeroInciso(cobertura.getId().getNumeroInciso());
				    coberturaSoportePrimerRiesgoLUC.setSumaAsegurada(new BigDecimal(cobertura.getValorSumaAsegurada().toString()));

				    agrupacionCotDTO = new AgrupacionCotDTO();
					id = new AgrupacionCotId();
					id.setIdToCotizacion(cobertura.getId().getIdToCotizacion());
					id.setNumeroAgrupacion(cobertura.getNumeroAgrupacion());
					agrupacionCotDTO.setId(id);
					agrupacionCotDTO = primerRiesgoLUCDN.getPorId(agrupacionCotDTO);
					if(agrupacionCotDTO != null && agrupacionCotDTO.getValorSumaAsegurada()!= null) {
						coberturaSoportePrimerRiesgoLUC
								.setSumaAseguradaPrimerRiesgoLUC(BigDecimal
										.valueOf(agrupacionCotDTO
												.getValorSumaAsegurada()));
					}
				    coberturasSoprtePrimerRiesgoLUC.add(coberturaSoportePrimerRiesgoLUC);
				}
			}
		}
	   return coberturasSoprtePrimerRiesgoLUC;
	}
	
	/**
	 * Devuelve los incisos en la lista de objetos <code>IncisoSoporteDaniosSiniestrosDTO</code> de una poliza
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 * @throws SystemException
	 */
	
	public List<IncisoSoporteDaniosSiniestrosDTO> obtenerIncisosSoporteDanios(BigDecimal idToPoliza,Short numeroEndoso)
		throws SystemException{
	    
	     List<IncisoSoporteDaniosSiniestrosDTO> incisosSoportes= new ArrayList<IncisoSoporteDaniosSiniestrosDTO>();
	     IncisoSoporteDaniosSiniestrosDTO incisoSoporte;
	     IncisoCotizacionDN incisoCotizacionDN= IncisoCotizacionDN.getInstancia();
	     List<IncisoCotizacionDTO> incisos;
	     EndosoDN endosoDN= EndosoDN.getInstancia(null);
	     EndosoId endosoId= new EndosoId();
	     endosoId.setIdToPoliza(idToPoliza);
	     endosoId.setNumeroEndoso(numeroEndoso);
	     EndosoDTO endosoDTO=endosoDN.getPorId(endosoId);
	       
	     if(endosoDTO!= null){
		
		 if(endosoDTO.getIdToCotizacion()!=null){
		      incisos= incisoCotizacionDN.listarPorCotizacionId(endosoDTO.getIdToCotizacion());
		     if(incisos != null && !incisos.isEmpty()){
			 for(IncisoCotizacionDTO inciso:incisos){
			     incisoSoporte= new IncisoSoporteDaniosSiniestrosDTO();
			     incisoSoporte.setDireccionInciso(this.getDireccion(inciso.getDireccionDTO()));
			     incisoSoporte.setFechaFinVigencia(endosoDTO.getFechaFinVigencia());
			     incisoSoporte.setFechaInicioVigencia(endosoDTO.getFechaInicioVigencia());
			     incisoSoporte.setNumeroInciso(inciso.getId().getNumeroInciso());
			     incisosSoportes.add(incisoSoporte);
			   }
		     }
		     
		     
		 }
		 
		
	     }
	    
	     return incisosSoportes;
	    
	}

	/**
	 * Devuelve las secciones y si tuviese subincisos de una poliza
	 * en una lista de  <code>SeccionSubIncisoSoporteDanosSiniestrosDTO</code>
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @param numeroInciso
	 * @return
	 * @throws SystemException
	 */
	public List<SeccionSubIncisoSoporteDanosSiniestrosDTO> obtenerSeccionSubIncisosSoporteDanios(
			BigDecimal idToPoliza, Short numeroEndoso, BigDecimal numeroInciso)
			throws SystemException {
		EndosoDN endosoDN = EndosoDN.getInstancia(null);
		EndosoId endosoId = new EndosoId();
		endosoId.setIdToPoliza(idToPoliza);
		endosoId.setNumeroEndoso(numeroEndoso);
		EndosoDTO endosoDTO = endosoDN.getPorId(endosoId);
		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
		IncisoCotizacionDTO incisoCotizacionDTO;
		List<SeccionSubIncisoSoporteDanosSiniestrosDTO> seccionesSubIncisos = new ArrayList<SeccionSubIncisoSoporteDanosSiniestrosDTO>();
		SeccionSubIncisoSoporteDanosSiniestrosDTO seccionSubInciso;
		SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
		SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
		SubIncisoCotizacionDTO subIncisocCotizacionDTO;

		if (endosoDTO != null) {
			if (endosoDTO.getIdToCotizacion() != null) {
				incisoCotizacionDTO = new IncisoCotizacionDTO();
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				incisoCotizacionId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
				incisoCotizacionId.setNumeroInciso(numeroInciso);
				incisoCotizacionDTO.setId(incisoCotizacionId);
				incisoCotizacionDTO = incisoCotizacionDN.getPorId(incisoCotizacionDTO);
				if (incisoCotizacionDTO != null) {
					incisoCotizacionDTO.setSeccionCotizacionList(seccionCotizacionDN.listarPorCotizacionNumeroInciso(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso()));
					if (incisoCotizacionDTO.getSeccionCotizacionList() != null && !incisoCotizacionDTO.getSeccionCotizacionList().isEmpty()) {
						for (SeccionCotizacionDTO seccion : incisoCotizacionDTO.getSeccionCotizacionList()) {
							if (seccion.getClaveContrato().equals(Sistema.CONTRATADO)) {
								subIncisocCotizacionDTO = new SubIncisoCotizacionDTO();
								SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId();
								subIncisoCotizacionDTOId.setIdToCotizacion(seccion.getId().getIdToCotizacion());
								subIncisoCotizacionDTOId.setIdToSeccion(seccion.getId().getIdToSeccion());
								subIncisoCotizacionDTOId.setNumeroInciso(seccion.getId().getNumeroInciso());
								subIncisocCotizacionDTO.setId(subIncisoCotizacionDTOId);
								seccion.setSubIncisoCotizacionLista(subIncisoCotizacionDN.listarFiltrado(subIncisocCotizacionDTO));
								if (seccion.getSubIncisoCotizacionLista() != null && !seccion.getSubIncisoCotizacionLista().isEmpty()) {
									SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
									id.setIdToCotizacion(seccion.getId().getIdToCotizacion());
									id.setNumeroInciso(seccion.getId().getNumeroInciso());
									id.setIdToSeccion(seccion.getId().getIdToSeccion());
									id.setNumeroSubInciso(BigDecimal.ZERO);
									DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN = DetallePrimaRiesgoCotizacionDN.getInstancia();
									List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimasRiesgos = detallePrimaRiesgoCotizacionDN .listarPorSubInciso(id);
									if(listaDetallePrimasRiesgos != null && !listaDetallePrimasRiesgos.isEmpty()) {
										seccionSubInciso= new SeccionSubIncisoSoporteDanosSiniestrosDTO();
                                        seccionSubInciso.setIdToSeccion(seccion.getId().getIdToSeccion());
                                        seccionSubInciso.setNombreComercialSeccion(seccion.getSeccionDTO().getNombreComercial());
                                        seccionSubInciso.setNumeroSubInciso(BigDecimal.ZERO);
                                        seccionSubInciso.setDescripcionSubInciso("");
                                        seccionesSubIncisos.add(seccionSubInciso);
									}
									for (SubIncisoCotizacionDTO subInciso : seccion.getSubIncisoCotizacionLista()) {
										seccionSubInciso = new SeccionSubIncisoSoporteDanosSiniestrosDTO();
										seccionSubInciso.setIdToSeccion(subInciso.getId().getIdToSeccion());
										seccionSubInciso.setNombreComercialSeccion(seccion.getSeccionDTO().getNombreComercial());
										seccionSubInciso.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
										seccionSubInciso.setDescripcionSubInciso(subInciso.getDescripcionSubInciso());
										seccionesSubIncisos.add(seccionSubInciso);
									}
								} else {
									seccionSubInciso = new SeccionSubIncisoSoporteDanosSiniestrosDTO();
									seccionSubInciso.setIdToSeccion(seccion.getId().getIdToSeccion());
									seccionSubInciso.setNombreComercialSeccion(seccion.getSeccionDTO().getNombreComercial());
									seccionSubInciso.setNumeroSubInciso(BigDecimal.ZERO);
									seccionSubInciso.setDescripcionSubInciso("");
									seccionesSubIncisos.add(seccionSubInciso);
								}
							}

						}
					}
				}
			}
		}
		return seccionesSubIncisos;
	}

	/**
	 * Obtiene los detalles de una poliza, en la lista de objetos <code>DetallePolizaSoporteDanosSiniestrosDTO</code>
	 * @param seccionSubIncisoSoporte
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param numeroEndoso
	 * @return
	 * @throws SystemException
	 */
	public List<DetallePolizaSoporteDanosSiniestrosDTO> obtenerDetallesPolizasSoporte(
			List<SeccionSubIncisoSoporteDanosSiniestrosDTO> seccionSubIncisoSoporte,
			BigDecimal idToPoliza, BigDecimal numeroInciso, Short numeroEndoso)
			throws SystemException {
		List<DetallePolizaSoporteDanosSiniestrosDTO> detallesPolizasSoportes = new ArrayList<DetallePolizaSoporteDanosSiniestrosDTO>();
		DetallePolizaSoporteDanosSiniestrosDTO detallePolizaSoporteDanosSiniestrosDTO;
		DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN = DetallePrimaRiesgoCotizacionDN.getInstancia();
		List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimasRiesgos;
		EndosoDN endosoDN = EndosoDN.getInstancia(null);
		EndosoId endosoId = new EndosoId();
		endosoId.setIdToPoliza(idToPoliza);
		endosoId.setNumeroEndoso(numeroEndoso);
		EndosoDTO endosoDTO = endosoDN.getPorId(endosoId);
		PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
		AgrupacionCotId agrupacionCotId;
		AgrupacionCotDTO agrupacionCotDTO;
		if (endosoDTO != null) {
			if (seccionSubIncisoSoporte != null && !seccionSubIncisoSoporte.isEmpty()) {
				for (SeccionSubIncisoSoporteDanosSiniestrosDTO seccionSubInciso : seccionSubIncisoSoporte) {
					SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
					id.setIdToCotizacion(endosoDTO.getIdToCotizacion());
					id.setNumeroInciso(numeroInciso);
					id.setIdToSeccion(seccionSubInciso.getIdToSeccion());
					id.setNumeroSubInciso(seccionSubInciso.getNumeroSubInciso());
					listaDetallePrimasRiesgos = detallePrimaRiesgoCotizacionDN.listarPorSubInciso(id);
					if (listaDetallePrimasRiesgos != null && !listaDetallePrimasRiesgos.isEmpty()) {
						for (DetallePrimaRiesgoCotizacionDTO detallePrima : listaDetallePrimasRiesgos) {
							detallePolizaSoporteDanosSiniestrosDTO = new DetallePolizaSoporteDanosSiniestrosDTO();
							detallePolizaSoporteDanosSiniestrosDTO.setIdToSeccion(detallePrima.getId().getIdToSeccion());
							detallePolizaSoporteDanosSiniestrosDTO.setIdToCobertura(detallePrima.getId().getIdToCobertura());
							detallePolizaSoporteDanosSiniestrosDTO.setNumeroInciso(detallePrima.getId().getNumeroInciso());
							detallePolizaSoporteDanosSiniestrosDTO.setNumeroSubInciso(detallePrima.getId().getNumeroSubInciso());
							detallePolizaSoporteDanosSiniestrosDTO.setIdToRiesgo(detallePrima.getId().getIdToRiesgo());

							detallePolizaSoporteDanosSiniestrosDTO.setDescripcionSeccion(detallePrima.getRiesgoCotizacionDTO().getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getNombreComercial());
							detallePolizaSoporteDanosSiniestrosDTO.setDescripcionCobertura(detallePrima.getRiesgoCotizacionDTO().getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
							detallePolizaSoporteDanosSiniestrosDTO.setClaveTipoSumaAsegurada(Integer.valueOf(detallePrima.getRiesgoCotizacionDTO().getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada()));
							detallePolizaSoporteDanosSiniestrosDTO.setDescripcionRiesgo(detallePrima.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getDescripcionRiesgoCobertura());
							agrupacionCotId = new AgrupacionCotId();
							agrupacionCotId.setIdToCotizacion(endosoDTO.getIdToCotizacion());
							agrupacionCotId.setNumeroAgrupacion(detallePrima.getRiesgoCotizacionDTO().getCoberturaCotizacionDTO().getNumeroAgrupacion());
							agrupacionCotDTO = new AgrupacionCotDTO();
							agrupacionCotDTO.setId(agrupacionCotId);
							agrupacionCotDTO = primerRiesgoLUCDN.getPorId(agrupacionCotDTO);
							if (agrupacionCotDTO != null) {
								detallePolizaSoporteDanosSiniestrosDTO.setClaveTipoAgrupacion(agrupacionCotDTO.getClaveTipoAgrupacion());
							}
							detallePolizaSoporteDanosSiniestrosDTO.setPrimaNeta(detallePrima.getValorPrimaNeta());
							if (detallePrima.getId().getNumeroSubInciso().equals(BigDecimal.ZERO)) {
								detallePolizaSoporteDanosSiniestrosDTO.setSumaAsegurada(detallePrima.getValorSumaAsegurada());
							} else {
								SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
								SubIncisoCotizacionDTO subIncisoCotizacionDTO = subIncisoCotizacionDN.getPorId(id);
								detallePolizaSoporteDanosSiniestrosDTO.setSumaAsegurada(subIncisoCotizacionDTO.getValorSumaAsegurada());
							}
							detallePolizaSoporteDanosSiniestrosDTO.setCoaseguro(detallePrima.getRiesgoCotizacionDTO().getValorCoaseguro());
							detallePolizaSoporteDanosSiniestrosDTO.setDeducible(detallePrima.getRiesgoCotizacionDTO().getValorDeducible());
							detallesPolizasSoportes.add(detallePolizaSoporteDanosSiniestrosDTO);
						}
					}

				}

			}
		}
		return detallesPolizasSoportes;
	}

	/**
	 * Dado un objeto de <code>DireccionDTO</code> y los datos de una poliza
	 * se devuelve los datos de su inciso en una lista de objetos <code>IncisoSoporteDaniosSiniestrosDTO</code>
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @param direccionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<IncisoSoporteDaniosSiniestrosDTO> obtenerIncisosPorPolizaDireccion(BigDecimal idToPoliza, 
			Short numeroEndoso,DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		 EndosoDN endosoDN= EndosoDN.getInstancia(null);
		 EndosoId endosoId=new EndosoId();
		 endosoId.setIdToPoliza(idToPoliza);
		 endosoId.setNumeroEndoso(numeroEndoso);
		 EndosoDTO endosoDTO= endosoDN.getPorId(endosoId);  
		 DireccionDN direccionDN= DireccionDN.getInstancia();
		 List<BigDecimal> idsDirecciones;
		 IncisoCotizacionDN incisoCotizacionDN= IncisoCotizacionDN.INSTANCIA;
		 List<IncisoCotizacionDTO> incisosCotizacion;
		 List<IncisoSoporteDaniosSiniestrosDTO> incisosSoportes= new ArrayList<IncisoSoporteDaniosSiniestrosDTO>();
		 IncisoSoporteDaniosSiniestrosDTO incisoSoporte;
		 
		 
		 if(endosoDTO !=null){
			 idsDirecciones= direccionDN.listarIdToDirecciones(direccionDTO);
			if(idsDirecciones!= null && !idsDirecciones.isEmpty()){
				 for(BigDecimal idToDireccion: idsDirecciones){
					 incisosCotizacion= incisoCotizacionDN.
					 obtenerIncisosPorIdCotizacionDireccion(endosoDTO.getIdToCotizacion(), idToDireccion);
					  if(incisosCotizacion!=null && !incisosCotizacion.isEmpty()){
						  for(IncisoCotizacionDTO inciso:incisosCotizacion){
						     incisoSoporte= new IncisoSoporteDaniosSiniestrosDTO();
						     incisoSoporte.setDireccionInciso(this.getDireccion(inciso.getDireccionDTO()));
						     incisoSoporte.setFechaFinVigencia(endosoDTO.getFechaFinVigencia());
						     incisoSoporte.setFechaInicioVigencia(endosoDTO.getFechaInicioVigencia());
						     incisoSoporte.setNumeroInciso(inciso.getId().getNumeroInciso());
						     incisosSoportes.add(incisoSoporte);
						  }
					  }
				 }
			 }
			 
			 
			 
		 }
		
		
		return incisosSoportes;
	}
	/**
	 * Se implementa metodo para envio de correos , para notificaciones cuando se solicita facultativo
	 * a administradores de reaseguro
	 * @param lineaSoporteReaseguro
	 * @throws SystemException
	 */
	public void enviaCorreoNotificacionSolicitudFacultativo(LineaSoporteReaseguro lineaSoporteReaseguro, Usuario usuario) throws SystemException{
	    if(lineaSoporteReaseguro==null){
		return;
	    }
	    List <String> listaAdministradores= new ArrayList<String>();
	    listaAdministradores.add(Sistema.EMAIL_ADMINISTRADOR_REASEGURO);
	    String asunto="MIDAS Notifica: Solicitud de Cotizaci&oacute;n Facultativa COT-"+
	    UtileriasWeb.llenarIzquierda(lineaSoporteReaseguro.getIdCotizacion().toString(), "0", 8);
	    SubRamoDN subRamoDN= SubRamoDN.getInstancia();
	    SubRamoDTO  subRamoDTO= new SubRamoDTO();
	    subRamoDTO.setIdTcSubRamo(lineaSoporteReaseguro.getIdSubramo());
	    subRamoDTO=subRamoDN.getSubRamoPorId(subRamoDTO);
	    
	    if(subRamoDTO==null || subRamoDTO.getDescripcionSubRamo()==null){
		return;
	    }
	    StringBuilder body= new StringBuilder();
	    body.append("Se ha recibido la solicitud de cotizar un facultativo para el cumulo ");
	    body.append(subRamoDTO.getDescripcionSubRamo().toUpperCase());
	    body.append(" por la suma asegurada de ");
	    body.append(lineaSoporteReaseguro.getMontoSumaAsegurada());
	    body.append(" de la Cotizaci&oacute;n COT-");
	    body.append( UtileriasWeb.llenarIzquierda(lineaSoporteReaseguro.getIdCotizacion().toString(), "0", 8)+".");
	    if (!Sistema.NOTIFICA_DANIOS_A_REASEGURO) {
			MailAction.enviaCorreo( listaAdministradores, asunto, body.toString());
		} else {
			MailAction.enviarCorreosPorRoles(Sistema.ROLES_ENVIOS_CORREOS_FACULTATIVO, asunto, body.toString(),usuario);
		}
	}
	
	/**
	 * Se implementa metodo para envio de correos , para notificaciones cuando se solicita facultativo
	 * a administradores de reaseguro
	 * @param lineaSoporteReaseguro
	 * @throws SystemException
	 */
	public void enviaCorreoNotificacionSolicitudFacultativo(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,Usuario usuarioSess) throws SystemException{
	    if(lineaSoporteReaseguroDTO==null){
		return;
	    }
	    List <String> listaAdministradores= new ArrayList<String>();
	    List<Usuario> usuariosPorRol= UsuarioWSDN.getInstancia().obtieneUsuariosSinRolesPorNombreRol(Sistema.ROL_MAIL_REASEGURO, usuarioSess.getNombreUsuario(), usuarioSess.getIdSesionUsuario());
	    if(usuariosPorRol!=null&&!usuariosPorRol.isEmpty() ){
			for(Usuario usuario:usuariosPorRol){
			    if(!UtileriasWeb.esCadenaVacia(usuario.getEmail())){
			    	listaAdministradores.add(usuario.getEmail());
			    }
			}
	    }
	    listaAdministradores.add(Sistema.EMAIL_ADMINISTRADOR_REASEGURO);
	    String asunto="MIDAS Notifica: Solicitud de Cotizaci&oacute;n Facultativa COT-"+
	    UtileriasWeb.llenarIzquierda(lineaSoporteReaseguroDTO.getIdToCotizacion().toString(), "0", 8);
//	    SubRamoDN subRamoDN= SubRamoDN.getInstancia();
	    SubRamoDTO  subRamoDTO= null;;
	    if(lineaSoporteReaseguroDTO.getLineaDTO() != null && lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo() != null) {
			subRamoDTO = lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo();
	    	/*new SubRamoDTO();
	    subRamoDTO.setIdTcSubRamo(lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
	    subRamoDTO=subRamoDN.getSubRamoPorId(subRamoDTO);*/
		}
	    
	    if(subRamoDTO==null || subRamoDTO.getDescripcionSubRamo()==null){
		return;
	    }
	    StringBuilder body= new StringBuilder();
	    body.append("Se ha recibido la solicitud de cotizar un facultativo para el cumulo ");
	    body.append(subRamoDTO.getDescripcionSubRamo().toUpperCase());
	    body.append(" por la suma asegurada de ");
	    body.append(lineaSoporteReaseguroDTO.getMontoSumaAsegurada());
	    body.append(" de la Cotizaci&oacute;n COT-");
	    body.append( UtileriasWeb.llenarIzquierda(lineaSoporteReaseguroDTO.getIdToCotizacion().toString(), "0", 8)+".");
	    if (!Sistema.NOTIFICA_DANIOS_A_REASEGURO) {
			MailAction.enviaCorreo( listaAdministradores, asunto, body.toString());
		} else {
			MailAction.enviarCorreosPorRoles(Sistema.ROLES_ENVIOS_CORREOS_FACULTATIVO, asunto, body.toString(),usuarioSess);
		}
	}
	
	/**
	 * Se implementa metodo para envio de correos , para notificaciones cuando se solicita cancelacion facultativo
	 * a administradores de reaseguro
	 * @param lineaSoporteReaseguro
	 * @throws SystemException
	 */
	public void enviaCorreoCancelacionSolicitudFacultativo(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, Usuario usuarioSess) throws SystemException{
	    if(lineaSoporteReaseguroDTO==null){
		return;
	    }

	    List <String> listaAdministradores= new ArrayList<String>();
	    List<Usuario> usuariosPorRol= UsuarioWSDN.getInstancia().obtieneUsuariosSinRolesPorNombreRol(Sistema.ROL_MAIL_REASEGURO, usuarioSess.getNombreUsuario(), usuarioSess.getIdSesionUsuario());
	    if(usuariosPorRol!=null&&!usuariosPorRol.isEmpty() ){
			for(Usuario usuario:usuariosPorRol){
			    if(!UtileriasWeb.esCadenaVacia(usuario.getEmail())){
			    	listaAdministradores.add(usuario.getEmail());
			    }
			}
	    }
	    listaAdministradores.add(Sistema.EMAIL_ADMINISTRADOR_REASEGURO);
	    String asunto="MIDAS Notifica Cancelaci&oacute;n: Solicitud de Cotizaci&oacute;n Facultativa COT-"+
	    UtileriasWeb.llenarIzquierda(lineaSoporteReaseguroDTO.getIdToCotizacion().toString(), "0", 8);
//	    SubRamoDN subRamoDN= SubRamoDN.getInstancia();
	    SubRamoDTO  subRamoDTO= null;;
	    if(lineaSoporteReaseguroDTO.getLineaDTO() != null && lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo() != null) {
			subRamoDTO = lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo();
	    	/*new SubRamoDTO();
	    subRamoDTO.setIdTcSubRamo(lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getIdTcSubRamo());
	    subRamoDTO=subRamoDN.getSubRamoPorId(subRamoDTO);*/
		}
	    
	    if(subRamoDTO==null || subRamoDTO.getDescripcionSubRamo()==null){
		return;
	    }
	    StringBuilder body= new StringBuilder();
	    body.append("Se ha recibido la solicitud de cancelar un facultativo para el cumulo ");
	    body.append(subRamoDTO.getDescripcionSubRamo().toUpperCase());
	    body.append(" por la suma asegurada de ");
	    body.append(lineaSoporteReaseguroDTO.getMontoSumaAsegurada());
	    body.append(" de la Cotizaci&oacute;n COT-");
	    body.append( UtileriasWeb.llenarIzquierda(lineaSoporteReaseguroDTO.getIdToCotizacion().toString(), "0", 8)+".");
	    if (!Sistema.NOTIFICA_DANIOS_A_REASEGURO) {
			MailAction.enviaCorreo( listaAdministradores, asunto, body.toString());
		} else {
			MailAction.enviarCorreosPorRoles(Sistema.ROLES_ENVIOS_CORREOS_FACULTATIVO, asunto, body.toString(),usuarioSess);
		}
	}
	
	/**
	 * Se implementa metodo para envio de correos , para notificaciones cuando se cancela o rehabilita facultativo
	 * a administradores de reaseguro
	 * @param lineaSoporteReaseguro
	 * @throws SystemException
	 */
	public void enviaCorreoNotificacionEndosoFacultativo(SoporteReaseguroDTO soporteReaseguroPenultimoEndosoDTO,Usuario usuarioSess,short tipoEndoso,String operacion) throws SystemException{
	    if(soporteReaseguroPenultimoEndosoDTO != null && soporteReaseguroPenultimoEndosoDTO.getIdToCotizacion() != null && soporteReaseguroPenultimoEndosoDTO.getIdToSoporteReaseguro() != null){
	    	SoporteReaseguroDN.getInstancia().consultarLineasSoporte(soporteReaseguroPenultimoEndosoDTO);
	    	int cantidadContratos = 0;
	    	String claveCotizacion = "COT-"+UtileriasWeb.llenarIzquierda(soporteReaseguroPenultimoEndosoDTO.getIdToCotizacion().toString(), "0", 8);
	    	List<String[]> datosContratos = new ArrayList<String[]>();
	    	for(LineaSoporteReaseguroDTO lineaTMP : soporteReaseguroPenultimoEndosoDTO.getLineaSoporteReaseguroDTOs()){
	    		if(lineaTMP.getContratoFacultativoDTO() != null){
	    			cantidadContratos ++;
	    			String [] datosContrato = {
	    					"Sub Ramo: " + lineaTMP.getLineaDTO().getSubRamo().getDescripcionSubRamo()+"<br>",
	    					"Por la suma asegurada de : " + lineaTMP.getMontoSumaAsegurada()+"<br>",
	    					"de la cotizacion: "+ claveCotizacion+"<br>"
	    				};
	    			datosContratos.add(datosContrato);
	    		}
	    	}
	    	if (cantidadContratos > 0){
	    		List <String> listaAdministradores= new ArrayList<String>();
			    listaAdministradores.add(Sistema.EMAIL_ADMINISTRADOR_REASEGURO);
			    String asunto="MIDAS Notifica: "+operacion+" de Cotizaci&oacute;n Facultativa "+claveCotizacion;
			    StringBuilder body= new StringBuilder();
			    body.append("Se ha emitido y distribuido un endoso de "+operacion+" para la cotizaci�n: "+claveCotizacion+". Esta Cotizaci�n tiene "+cantidadContratos+" " +
			    		"contrato(s) facultativo(s).<br>");
			    body.append("<hr>");
			    for(String[] datosContrato : datosContratos){
			    	body.append(datosContrato[0]);
				    body.append(datosContrato[1]);
				    body.append(datosContrato[2]);
			    }
			    body.append("<hr>");
			    if (!Sistema.NOTIFICA_DANIOS_A_REASEGURO) {
					MailAction.enviaCorreo( listaAdministradores, asunto, body.toString());
				} else {
					MailAction.enviarCorreosPorRoles(Sistema.ROLES_ENVIOS_CORREOS_FACULTATIVO, asunto, body.toString(),usuarioSess);
				}
	    	}
	    }
	}
	
	/**
	 * Se implementa metodo para envio de correos , para notificaciones cuando se contrata facultativo
	 * a administradores de reaseguro
	 * @param lineaSoporteReaseguro
	 * @throws SystemException
	 */
	public void enviaCorreoNotificacionConfirmacionContratoFacultativo(BigDecimal idToCotizacion,Usuario usuarioSess) throws SystemException{
	    if(idToCotizacion==null){
		return;
	    }
	    List <String> listaAdministradores= new ArrayList<String>();
	    listaAdministradores.add(Sistema.EMAIL_ADMINISTRADOR_REASEGURO);
	    String asunto="MIDAS Notifica: Solicitud para Confirmaci&oacute;n de Contrato Facultativo para emisi&oacute;n de cotizac&oacute;n COT-"+
	    UtileriasWeb.llenarIzquierda(idToCotizacion.toString(), "0", 8);
	    StringBuilder body= new StringBuilder();
	    body.append("Se ha recibido la solicitud para autorizar el contrato facultativo de la cotizaci&oacute;n COT- ");
	    body.append(UtileriasWeb.llenarIzquierda(idToCotizacion.toString(), "0", 8)+".");
	    if (!Sistema.NOTIFICA_DANIOS_A_REASEGURO) {
			MailAction.enviaCorreo(listaAdministradores, asunto, body.toString());
		} else {
			MailAction.enviarCorreosPorRoles(Sistema.ROLES_ENVIOS_CORREOS_FACULTATIVO, asunto, body.toString(),usuarioSess);
		}
	    
	}

	/**
	 * Este metodo permite consultar los datos generales de la poliza y los
	 * popula en el objeto de soporte PolizaSoporteDanosDTO
	 * 
	 * @param BigDecimal
	 *            idToPoliza objeto que corresponde al ID de la entidad
	 *            PolizaDTO
	 * @param fechaSiniestro
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el acceso a BD o la instancia al EJB
	 *             falla
	 * @return PolizaSoporteDanosDTO el objeto actualizado con los datos
	 *         solicitados
	 */
	public PolizaSoporteDanosDTO getDatosGeneralesPoliza(BigDecimal idToPoliza,
			Date fechaSiniestro) throws ExcepcionDeAccesoADatos,
			SystemException {
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = null;
		PolizaDTO polizaDTO = null;
		if (idToPoliza == null || fechaSiniestro == null) {
			return null;
		}
		// Se consulta los datos de la poliza
		polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		if (polizaDTO != null) {
			polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			this.poblarPolizaSoporteDTO(polizaSoporteDanosDTO, polizaDTO, fechaSiniestro);
		}
		return polizaSoporteDanosDTO;
	}

	private void poblarPolizaSoporteDTO(
			PolizaSoporteDanosDTO polizaSoporteDanosDTO, PolizaDTO polizaDTO,
			Date fechaSiniestro) throws SystemException {
		List<EndosoDTO> endosos = EndosoDN.getInstancia("").listarEndososPorPoliza(polizaDTO.getIdToPoliza(), Boolean.TRUE);
		ListIterator<EndosoDTO> listIterator = endosos.listIterator(endosos.size());
		while(listIterator.hasPrevious()) {
			EndosoDTO endosoDTO = listIterator.previous();
			if(endosoDTO.getFechaInicioVigencia().before(fechaSiniestro) || endosoDTO.getFechaInicioVigencia().equals(fechaSiniestro)) {
				polizaSoporteDanosDTO.setNumeroEndosoVigente(endosoDTO.getId().getNumeroEndoso());
				break;
			}
		}
		this.poblarPolizaSoporteDTO(polizaSoporteDanosDTO, polizaDTO);
	}
}