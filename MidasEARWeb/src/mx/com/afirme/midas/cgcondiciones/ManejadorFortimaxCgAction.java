package mx.com.afirme.midas.cgcondiciones;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentoanexo.DocumentoAnexoCgDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ManejadorFortimaxCgAction extends MidasMappingDispatchAction {
	
	private FortimaxV2Service fortimaxV2Service;
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	private String gavetaCondicionesg;
	private String carpetaCondicionesg;
	
	public ManejadorFortimaxCgAction() throws SystemException {
		try {
			gavetaCondicionesg = ""; // Gaveta Fmx Cg
			carpetaCondicionesg = ""; // Ruta/Folder Fmx Autos
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			fortimaxV2Service = serviceLocator.getEJB(FortimaxV2Service.class);
			parametroGeneralFacade = serviceLocator.getEJB(ParametroGeneralFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

    public String armarFortimaxIDFile(String fileName, String idToControlArchivo) {
    	Calendar calendario = Calendar.getInstance();
		calendario.setTime(new Date());
	    StringBuffer fechaForm = new StringBuffer();
	    fechaForm.append(calendario.get(Calendar.YEAR));
	    fechaForm.append((calendario.get(Calendar.MONTH)+1)<=9?"0"+(calendario.get(Calendar.MONTH)+1):(calendario.get(Calendar.MONTH)+1));
	    fechaForm.append(calendario.get(Calendar.DAY_OF_MONTH)<=9?"0"+calendario.get(Calendar.DAY_OF_MONTH):calendario.get(Calendar.DAY_OF_MONTH));
	    String fortimaxIDFile = "23" + fechaForm + idToControlArchivo; //23Autos|24Danios|25Vida|idEnFmx
		return fortimaxIDFile;
	}
    
	public void crearExpedienteFortimax(String fortimaxIDFile) {
		String[] fieldValues = new String[2];
		fieldValues[0]=fortimaxIDFile;
		fieldValues[1]="AUTOS";
		
		String[] strReturns;
		try {
			strReturns = fortimaxV2Service.generateExpedient(getGavetaCondicionesg(), fieldValues);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		if (strReturns[0].equals("false")) {
			throw new RuntimeException("No se pudo crear el expediente en Fortimax para el Documento Anexo.");
		}
	}
	
	public void cargarArchivoFortimax(String fileName, String fortimaxIDFile, TransporteImpresionDTO transporteImpresionDTO){
		try {
			fortimaxV2Service.uploadFile(fileName, transporteImpresionDTO, getGavetaCondicionesg(), new String[]{fortimaxIDFile}, getCarpetaCondicionesg());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	 public void descargarArchivoFortimax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String fortimaxDocName = request.getParameter("fortimaxIDFile");
		CgCondiciones archivoCG = new CgCondiciones();
		archivoCG = buscarDocumentoCondicionesg(fortimaxDocName);

		byte byteArray[] = fortimaxV2Service.downloadFileBP(fortimaxDocName, "D", archivoCG.getFortimaxDocGaveta(), Long.valueOf(archivoCG.getFortimaxDocId()));
		response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(archivoCG.getNombre(), "ISO-8859-1"));
		response.setContentType("application/pdf");
		response.setContentLength(byteArray.length);
		response.setBufferSize(1024 * 15);
		
		OutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();
	}
	
	public byte[] descargaBytesArchivoCgFortimax(String fortimaxDocName){
		try {
			CgCondiciones archivoCG = new CgCondiciones();
			archivoCG = buscarDocumentoCondicionesg(fortimaxDocName);
			byte byteArray[] = fortimaxV2Service.downloadFileBP(fortimaxDocName, "D", archivoCG.getFortimaxDocGaveta(), Long.valueOf(archivoCG.getFortimaxDocId()));
            return byteArray;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public CgCondiciones buscarDocumentoCondicionesg(String fortimaxDocName){
		DocumentoAnexoCgDN controlArchivoDN = DocumentoAnexoCgDN.getInstancia();
		CgCondiciones archivoCG = new CgCondiciones();
		List<CgCondiciones> listado = new ArrayList<CgCondiciones>();
		try {
			listado = controlArchivoDN.listarPorPropiedad("fortimaxDocNombre", fortimaxDocName);
		} catch (ExcepcionDeAccesoADatos e) {
			throw new RuntimeException(e);
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
		archivoCG = (CgCondiciones)listado.get(0);
		return archivoCG;
	}
	
    public FortimaxV2Service getFortimaxV2Service() {
        return this.fortimaxV2Service;
    }
    public void setFortimaxV2Service(FortimaxV2Service fortimaxV2Service) {
        this.fortimaxV2Service = fortimaxV2Service;
    } 
	
    public String getGavetaCondicionesg() {
    	ParametroGeneralId id = new ParametroGeneralId(new BigDecimal(20), new BigDecimal(200080));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}
	public void setGavetaCondicionesg(String gavetaCondicionesg) {
		this.gavetaCondicionesg = gavetaCondicionesg;
	}

	public String getCarpetaCondicionesg() {
		ParametroGeneralId id = new ParametroGeneralId(new BigDecimal(20), new BigDecimal(200140));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}
	public void setCarpetaCondicionesg(String carpetaCondicionesg) {
		this.carpetaCondicionesg = carpetaCondicionesg;
	}
}
