package mx.com.afirme.midas.agente;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;

public class AgenteDN {
	private static final AgenteDN INSTANCIA = new AgenteDN();
	public static final String PROPIEDAD_TIPOPOLIZA = "tipoPoliza";
	public static final String PROPIEDAD_SECCION = "seccion";

	public static AgenteDN getInstancia() {
		return AgenteDN.INSTANCIA;
	}

	public AgenteDTO getPorId(BigDecimal id) throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		return agenteSN.getPorId(id);
	}

	public List<AgenteDTO> listarTodos() throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		return agenteSN.listarTodos();
	}

	public void agregar(AgenteDTO agenteDTO) throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		agenteSN.agregar(agenteDTO);
	}

	public AgenteDTO actualizar(AgenteDTO agenteDTO) throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		return agenteSN.actualizar(agenteDTO);
	}

	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO
				.getIdToControlArchivo());
	}

	public void borrar(AgenteDTO agenteDTO) throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		agenteSN.borrar(agenteDTO);
	}
	
	public boolean validarCedulaAgente(Long idTipoCedula, String propiedad, BigDecimal valor) throws SystemException {
		SeccionDN seccionDN = SeccionDN.getInstancia();
		Long resultado = null;

		if(propiedad.equals(PROPIEDAD_SECCION)) {
			resultado = seccionDN.countCedulasAsociadasPorSeccion(idTipoCedula, valor).longValue();
		} else if(propiedad.equals(PROPIEDAD_TIPOPOLIZA)) {
			resultado = seccionDN.countCedulasAsociadasPorSeccionesEnTipoPoliza(idTipoCedula, valor).longValue();
		}
		return (resultado != null && resultado > 0);
	}
}
