package mx.com.afirme.midas.agente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.endoso.SolicitudEndosoForm;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudForm;

public class AgenteEspecialDN {

	private static final AgenteEspecialDN INSTANCIA = new AgenteEspecialDN();

	public static AgenteEspecialDN getInstancia() {
		return AgenteEspecialDN.INSTANCIA;
	}

	/**
	 * Obtiene el codigo de Agente en los usuarios que tengan el rol de Agente Especial
	 * @param usuario Usuario de Midas
	 * @return Codigo de Agente en el caso de los usuarios que tengan el rol de Agente Especial, null en caso contrario
	 */
	public BigDecimal obtieneCodigoAgenteEspecial(Usuario usuario) {
		BigDecimal idAgente = null;
		
		if(usuario == null) throw new RuntimeException("El usuario es null y no debe serlo");
		
		for (Rol rol : usuario.getRoles()) {
			
			if (rol.getDescripcion().equals(Sistema.ROL_AGENTE_ESPECIAL)) {
				
				String claveIdAgente = "seycos.agentId";
				if (usuario.contieneAtributo(claveIdAgente)){
					AtributoUsuario atributoUsuario = usuario.obtenerAtributo(claveIdAgente);
					if(atributoUsuario.isActivo() && atributoUsuario.getValor() != null) {
						idAgente = new BigDecimal(atributoUsuario.getValor().toString());		
					} else {
						LogDeMidasWeb.log("El atributo del id del Agente Especial no esta activo o esta vacio.", Level.INFO, null);
						idAgente = new BigDecimal(-1); //Se establece asi para indicar que si es un Agente Especial pero hubo algun problema
					}
			
				}else{
					LogDeMidasWeb.log("El Agente especial no contiene el Id de Agente como atributo.", Level.INFO, null);
					idAgente = new BigDecimal(-1); //Se establece asi para indicar que si es un Agente Especial pero hubo algun problema
				}
								
				break;
			}
			
		}
		
		return idAgente;
		
	}
	
	/**
	 * Agrega el filtro del Codigo de Agente en caso de que aplique
	 * @param usuario
	 * @param solicitud Objeto para el filtrado de las solicitudes
	 */
	public void agregaFiltroCodigoAgente(Usuario usuario, SolicitudDTO solicitud) {
		
		BigDecimal codigoAgente = obtieneCodigoAgenteEspecial(usuario);
		
		if (codigoAgente != null) {
			solicitud.setCodigoAgente(codigoAgente);
		}
		
	}
	
	/**
	 * Agrega el filtro del Codigo de Agente en caso de que aplique
	 * @param usuario
	 * @param cotizacion Objeto para el filtrado de las cotizaciones
	 */
	public void agregaFiltroCodigoAgente(Usuario usuario, CotizacionDTO cotizacion) {
		
		if (cotizacion.getSolicitudDTO() == null) {
			SolicitudDTO solicitud = new SolicitudDTO();
			cotizacion.setSolicitudDTO(solicitud);
		}
		
		agregaFiltroCodigoAgente(usuario, cotizacion.getSolicitudDTO());
	}
	
	/**
	 * Agrega el filtro del Codigo de Agente en caso de que aplique
	 * @param usuario
	 * @param poliza Objeto para el filtrado de las polizas
	 */
	public void agregaFiltroCodigoAgente(Usuario usuario, PolizaDTO poliza) {
		
		if (poliza.getCotizacionDTO() == null) {
			CotizacionDTO cotizacion = new CotizacionDTO();
			poliza.setCotizacionDTO(cotizacion);
		}
		
		agregaFiltroCodigoAgente(usuario, poliza.getCotizacionDTO());
		
		
	}
	
	
	/**
	 * En caso de que el usuario sea Agente Especial, valida que la poliza no pertenezca a otro agente
	 * @param usuario Usuario de Midas
	 * @param polizaDTO Poliza a evaluar
	 * @return true en caso de que el usuario sea Agente Especial y la poliza sea de otro agente, false en caso contrario
	 */
	public Boolean esPolizaDeOtroAgente(Usuario usuario, PolizaDTO polizaDTO) {
		
		Boolean respuesta = false;
		
		BigDecimal codigoAgente = obtieneCodigoAgenteEspecial(usuario);
		
		if (codigoAgente != null) {
			if (!polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente().equals(codigoAgente)) {
				respuesta = true;
			}
		}
		
		return respuesta;
	}
	
	
	public Boolean esAgenteEspecial(HttpServletRequest request) {
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		
		BigDecimal codigoAgente = obtieneCodigoAgenteEspecial(usuario);
		
		return codigoAgente != null;
		
	}
	
	
	public List<AgenteDTO> poblarAgenteEspecial(Usuario usuario, SolicitudForm solicitudForm) {
		
		List<AgenteDTO> agentes = obtenerDatosAgenteEspecial(usuario);
		
		if (agentes != null) {
			solicitudForm.setBloqueoDatosAgente(true);
			solicitudForm.setCodigoAgente(agentes.get(0).getIdTcAgente().toString());
			solicitudForm.setIdAgente(agentes.get(0).getIdTcAgente().toString());
			solicitudForm.setPermitirAutoAsignacion(true);
		} else {
			solicitudForm.setBloqueoDatosAgente(false);
		}
		
		return agentes;
	}
	
	public List<AgenteDTO> poblarAgenteEspecial(Usuario usuario, SolicitudEndosoForm solicitudEndosoForm) {
		
		List<AgenteDTO> agentes = obtenerDatosAgenteEspecial(usuario);
		
		if (agentes != null) {
			solicitudEndosoForm.setBloqueoDatosAgente(true);
			solicitudEndosoForm.setCodigoAgente(agentes.get(0).getIdTcAgente().toString());
			solicitudEndosoForm.setIdAgente(agentes.get(0).getIdTcAgente().toString());
			solicitudEndosoForm.setPermitirAutoAsignacion(true);
		} else {
			solicitudEndosoForm.setBloqueoDatosAgente(false);
		}
		
		return agentes;
	}
	
	
	public List<AgenteDTO> poblarAgenteEspecial(Usuario usuario, CotizacionForm cotizacionForm) {
		
		List<AgenteDTO> agentes = obtenerDatosAgenteEspecial(usuario);
		
		if (agentes != null) {
			cotizacionForm.setIdTcAgente(agentes.get(0).getIdTcAgente().toString());
		}		
		return agentes;
	}
	
	
	private List<AgenteDTO> obtenerDatosAgenteEspecial(Usuario usuario) {
		List<AgenteDTO> agentes = null;
		BigDecimal codigoAgente = obtieneCodigoAgenteEspecial(usuario);
		
		if (codigoAgente != null) {
			
			AgenteDTO agenteFiltro = new AgenteDTO();
			agenteFiltro.setIdTcAgente(codigoAgente.intValue());
			
			try {
				agentes = new ArrayList<AgenteDTO>();
				AgenteDTO agente = AgenteDN.getInstancia().verDetalleAgente(agenteFiltro, usuario.getNombreUsuario());
				agentes.add(agente);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			return agentes;
		}
		return null;
	}
	
}
