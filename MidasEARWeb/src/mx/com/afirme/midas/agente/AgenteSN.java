package mx.com.afirme.midas.agente;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Fernando Alonzo
 * @since 01 de Septiembre del 2009
 * 
 */
public class AgenteSN {
	private AgenteFacadeRemote beanRemoto;

	public AgenteSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(AgenteFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
			
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(AgenteDTO agenteDTO) {
		try {
			beanRemoto.save(agenteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AgenteDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AgenteDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AgenteDTO actualizar(AgenteDTO agenteDTO) {
		try {
			return beanRemoto.update(agenteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(AgenteDTO agenteDTO) {
		try {
			beanRemoto.delete(agenteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
