package mx.com.afirme.midas.producto;

import java.util.List;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.exclusion.ExclusionDescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.moneda.MonedaProductoDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

public class ProductoForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idToProducto;
	private String codigo;
	private String version;
	private String descripcion;
	private String nombreComercial;
	private String claveUnidadVigencia = "1";
	private String valorMinimoUnidadVigencia = "1";
	private String valorMaximoUnidadVigencia = "366";
	private String valorDefaultUnidadVigencia = "365";
	private String claveRenovable;
	private String clavePagoInmediato;
	private String diasRetroactividad;
	private String diasDiferimiento;
	private String diasGracia;
	//cambio de nombre claveactivoconfiguracion
	private String claveEstatus = "0";
	private String claveActivo = "0";
	private String descripcionUnidadVigencia;
	//private String idTipoCalculo;
	private String descripcionRegistroCNFS;
	private String descripcionTipoCalculo;
	private String idControlArchivoRegistroCNSF="0";
	private String nombreArchivoRegistroCNSF=null;
	private String idControlArchivoCaratulaPoliza="0";
	private String nombreArchivoCaratulaPoliza=null;
	private String idControlArchivoCondicionesProducto="0";
	private String nombreArchivoCondicionesProducto=null;
	private String idControlArchivoNotaTecnica="0";
	private String nombreArchivoNotaTecnica=null;
	private String idControlArchivoAnalisisCongruencia="0";
	private String nombreArchivoAnalisisCongruencia=null;
	private String idControlArchivoDictamenJuridico="0";
	private String nombreArchivoDictamenJuridico=null;
	private List<TipoPolizaDTO> tiposPoliza;
	private List<DescuentoVarioProductoDTO> descuentosAsociados;
	private List<DescuentoDTO> descuentosPorAsociar;
	private List<RecargoVarioProductoDTO> recargosAsociados;
	private List<RecargoVarioDTO> recargosPorAsociar;
	private List<RamoProductoDTO> ramoProductoAsociados;
	private List<RamoDTO> ramosPorAsociar;
	private List<RamoDTO> ramosAsociados;
	private List<AumentoVarioProductoDTO> aumentosAsociados;
	private List<AumentoVarioDTO> aumentosPorAsociar;
	private List<ExclusionRecargoVarioProductoDTO> excRecargoProductoAsociados;
	private List<ExclusionRecargoVarioProductoDTO> excRecargoProductoNoAsociados;
	private List<ExclusionDescuentoVarioProductoDTO> excDescuentoProductoAsociados;
	private List<ExclusionDescuentoVarioProductoDTO> excDescuentoProductoNoAsociados;
	private List<ExclusionAumentoVarioProductoDTO> excAumentoProductoAsociados;
	private List<ExclusionAumentoVarioProductoDTO> excAumentoProductoNoAsociados;
	private List<MonedaProductoDTO> monedasAsociadas;
	private List<MonedaDTO> monedasPorAsociar;
	private String mostrarInactivos;
	private String descripcionEstatus;
	private String mostrarTipoPolizaInactivas;
	private String claveAjusteVigencia;
	private String claveNegocio;
	private String claveAplicaFlotillas;
	private String claveAplicaAutoexpedible;
	private String diasGraciaSubsecuentes;
	private String fechaInicioVigencia;
	private String soloLectura="false";
	private String fechaCreacion;
	
	public String getIdToProducto() {
		return idToProducto;
	}

	public void setIdToProducto(String idToProducto) {
		this.idToProducto = idToProducto;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getClaveUnidadVigencia() {
		return claveUnidadVigencia;
	}

	public void setClaveUnidadVigencia(String claveUnidadVigencia) {
		this.claveUnidadVigencia = claveUnidadVigencia;
	}

	public String getValorMinimoUnidadVigencia() {
		return valorMinimoUnidadVigencia;
	}

	public void setValorMinimoUnidadVigencia(String valorMinimoUnidadVigencia) {
		this.valorMinimoUnidadVigencia = valorMinimoUnidadVigencia;
	}

	public String getValorMaximoUnidadVigencia() {
		return valorMaximoUnidadVigencia;
	}

	public void setValorMaximoUnidadVigencia(String valorMaximoUnidadVigencia) {
		this.valorMaximoUnidadVigencia = valorMaximoUnidadVigencia;
	}

	public String getClaveRenovable() {
		return claveRenovable;
	}

	public void setClaveRenovable(String claveRenovable) {
		this.claveRenovable = claveRenovable;
	}

	public String getClavePagoInmediato() {
		return clavePagoInmediato;
	}

	public void setClavePagoInmediato(String clavePagoInmediato) {
		this.clavePagoInmediato = clavePagoInmediato;
	}

	public String getDiasRetroactividad() {
		return diasRetroactividad;
	}

	public void setDiasRetroactividad(String diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	public String getDiasDiferimiento() {
		return diasDiferimiento;
	}

	public void setDiasDiferimiento(String diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	public String getDiasGracia() {
		return diasGracia;
	}

	public void setDiasGracia(String diasGracia) {
		this.diasGracia = diasGracia;
	}

	public String getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public String getClaveActivo() {
		return claveActivo;
	}

	public void setClaveActivo(String claveActivo) {
		this.claveActivo = claveActivo;
	}

	public List<TipoPolizaDTO> getTiposPoliza() {
		return tiposPoliza;
	}

	public void setTiposPoliza(List<TipoPolizaDTO> tiposPoliza) {
		this.tiposPoliza = tiposPoliza;
	}

	public void setDescuentosAsociados(
			List<DescuentoVarioProductoDTO> descuentosAsociados) {
		this.descuentosAsociados = descuentosAsociados;
	}

	public List<DescuentoVarioProductoDTO> getDescuentosAsociados() {
		return descuentosAsociados;
	}

	public void setDescuentosPorAsociar(List<DescuentoDTO> descuentosPorAsociar) {
		this.descuentosPorAsociar = descuentosPorAsociar;
	}

	public List<DescuentoDTO> getDescuentosPorAsociar() {
		return descuentosPorAsociar;
	}
	public void setRecargosAsociados(
			List<RecargoVarioProductoDTO> recargosAsociados) {
		this.recargosAsociados = recargosAsociados;
	}

	public List<RecargoVarioProductoDTO> getRecargosAsociados() {
		return recargosAsociados;
	}

	public void setRecargosPorAsociar(List<RecargoVarioDTO> recargosPorAsociar) {
		this.recargosPorAsociar = recargosPorAsociar;
	}

	public List<RecargoVarioDTO> getRecargosPorAsociar() {
		return recargosPorAsociar;
	}

	public List<RamoProductoDTO> getRamoProductoAsociados() {
		return ramoProductoAsociados;
	}

	public void setRamoProductoAsociados(
			List<RamoProductoDTO> ramoProductoAsociados) {
		this.ramoProductoAsociados = ramoProductoAsociados;
	}

	public List<RamoDTO> getRamosPorAsociar() {
		return ramosPorAsociar;
	}

	public void setRamosPorAsociar(List<RamoDTO> ramosPorAsociar) {
		this.ramosPorAsociar = ramosPorAsociar;
	}

	public List<RamoDTO> getRamosAsociados() {
		return ramosAsociados;
	}

	public void setRamosAsociados(List<RamoDTO> ramosAsociados) {
		this.ramosAsociados = ramosAsociados;
	}

	public void setAumentosAsociados(
			List<AumentoVarioProductoDTO> aumentosAsociados) {
		this.aumentosAsociados = aumentosAsociados;
	}

	public List<AumentoVarioProductoDTO> getAumentosAsociados() {
		return aumentosAsociados;
	}

	public void setAumentosPorAsociar(List<AumentoVarioDTO> aumentosPorAsociar) {
		this.aumentosPorAsociar = aumentosPorAsociar;
	}

	public List<AumentoVarioDTO> getAumentosPorAsociar() {
		return aumentosPorAsociar;
	}

	public List<ExclusionRecargoVarioProductoDTO> getExcRecargoProductoAsociados() {
		return excRecargoProductoAsociados;
	}

	public void setExcRecargoProductoAsociados(
			List<ExclusionRecargoVarioProductoDTO> excRecargoProductoAsociados) {
		this.excRecargoProductoAsociados = excRecargoProductoAsociados;
	}

	public List<ExclusionRecargoVarioProductoDTO> getExcRecargoProductoNoAsociados() {
		return excRecargoProductoNoAsociados;
	}

	public void setExcRecargoProductoNoAsociados(
			List<ExclusionRecargoVarioProductoDTO> excRecargoProductoNoAsociados) {
		this.excRecargoProductoNoAsociados = excRecargoProductoNoAsociados;
	}

	public List<ExclusionDescuentoVarioProductoDTO> getExcDescuentoProductoAsociados() {
		return excDescuentoProductoAsociados;
	}

	public void setExcDescuentoProductoAsociados(
			List<ExclusionDescuentoVarioProductoDTO> excDescuentoProductoAsociados) {
		this.excDescuentoProductoAsociados = excDescuentoProductoAsociados;
	}

	public List<ExclusionDescuentoVarioProductoDTO> getExcDescuentoProductoNoAsociados() {
		return excDescuentoProductoNoAsociados;
	}

	public void setExcDescuentoProductoNoAsociados(
			List<ExclusionDescuentoVarioProductoDTO> excDescuentoProductoNoAsociados) {
		this.excDescuentoProductoNoAsociados = excDescuentoProductoNoAsociados;
	}

	public List<ExclusionAumentoVarioProductoDTO> getExcAumentoProductoAsociados() {
		return excAumentoProductoAsociados;
	}

	public void setExcAumentoProductoAsociados(
			List<ExclusionAumentoVarioProductoDTO> excAumentoProductoAsociados) {
		this.excAumentoProductoAsociados = excAumentoProductoAsociados;
	}

	public List<ExclusionAumentoVarioProductoDTO> getExcAumentoProductoNoAsociados() {
		return excAumentoProductoNoAsociados;
	}

	public void setExcAumentoProductoNoAsociados(
			List<ExclusionAumentoVarioProductoDTO> excAumentoProductoNoAsociados) {
		this.excAumentoProductoNoAsociados = excAumentoProductoNoAsociados;
	}

	public void setMonedasAsociadas(List<MonedaProductoDTO> monedasAsociadas) {
		this.monedasAsociadas = monedasAsociadas;
	}

	public List<MonedaProductoDTO> getMonedasAsociadas() {
		return monedasAsociadas;
	}

	public void setMonedasPorAsociar(List<MonedaDTO> monedasPorAsociar) {
		this.monedasPorAsociar = monedasPorAsociar;
	}

	public List<MonedaDTO> getMonedasPorAsociar() {
		return monedasPorAsociar;
	}

	public String getValorDefaultUnidadVigencia() {
		return valorDefaultUnidadVigencia;
	}

	public void setValorDefaultUnidadVigencia(String valorDefaultUnidadVigencia) {
		this.valorDefaultUnidadVigencia = valorDefaultUnidadVigencia;
	}
	public String getDescripcionUnidadVigencia() {
		return descripcionUnidadVigencia;
	}
	public void setDescripcionUnidadVigencia(String descripcionUnidadVigencia) {
		this.descripcionUnidadVigencia = descripcionUnidadVigencia;
	}
	/*
	public String getIdTipoCalculo() {
		return idTipoCalculo;
	}
	public void setIdTipoCalculo(String idTipoCalculo) {
		this.idTipoCalculo = idTipoCalculo;
	}
	*/
	
	public String getDescripcionRegistroCNFS() {
		return descripcionRegistroCNFS;
	}
	public void setDescripcionRegistroCNFS(String descripcionRegistroCNFS) {
		this.descripcionRegistroCNFS = descripcionRegistroCNFS;
	}
	
	public String getDescripcionTipoCalculo() {
		return descripcionTipoCalculo;
	}
	public void setDescripcionTipoCalculo(String descripcionTipoCalculo) {
		this.descripcionTipoCalculo = descripcionTipoCalculo;
	}
	public String getIdControlArchivoRegistroCNSF() {
		return idControlArchivoRegistroCNSF;
	}
	public void setIdControlArchivoRegistroCNSF(String idControlArchivoRegistroCNSF) {
		this.idControlArchivoRegistroCNSF = idControlArchivoRegistroCNSF;
	}
	public String getIdControlArchivoCaratulaPoliza() {
		return idControlArchivoCaratulaPoliza;
	}
	public void setIdControlArchivoCaratulaPoliza(
			String idControlArchivoCaratulaPoliza) {
		this.idControlArchivoCaratulaPoliza = idControlArchivoCaratulaPoliza;
	}
	public String getIdControlArchivoCondicionesProducto() {
		return idControlArchivoCondicionesProducto;
	}
	public void setIdControlArchivoCondicionesProducto(
			String idControlArchivoCondicionesProducto) {
		this.idControlArchivoCondicionesProducto = idControlArchivoCondicionesProducto;
	}
	public String getIdControlArchivoNotaTecnica() {
		return idControlArchivoNotaTecnica;
	}
	public void setIdControlArchivoNotaTecnica(String idControlArchivoNotaTecnica) {
		this.idControlArchivoNotaTecnica = idControlArchivoNotaTecnica;
	}
	public String getIdControlArchivoAnalisisCongruencia() {
		return idControlArchivoAnalisisCongruencia;
	}
	public void setIdControlArchivoAnalisisCongruencia(
			String idControlArchivoAnalisisCongruencia) {
		this.idControlArchivoAnalisisCongruencia = idControlArchivoAnalisisCongruencia;
	}
	public String getIdControlArchivoDictamenJuridico() {
		return idControlArchivoDictamenJuridico;
	}
	public void setIdControlArchivoDictamenJuridico(String idControlArchivoDictamenJuridico) {
		this.idControlArchivoDictamenJuridico = idControlArchivoDictamenJuridico;
	}
	public void setMostrarInactivos(String mostrarInactivos) {
		this.mostrarInactivos = mostrarInactivos;
	}
	public String getMostrarInactivos() {
		return mostrarInactivos;
	}
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	public String getNombreArchivoRegistroCNSF() {
		if (!this.idControlArchivoRegistroCNSF.equals("0") && this.nombreArchivoRegistroCNSF==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoRegistroCNSF));
				nombreArchivoRegistroCNSF = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoRegistroCNSF = "NO DISPONIBLE";
				e.printStackTrace();
			}
		}
		return nombreArchivoRegistroCNSF;
	}
	public void setNombreArchivoRegistroCNSF(String nombreArchivoRegistroCNSF) {
		this.nombreArchivoRegistroCNSF = nombreArchivoRegistroCNSF;
	}
	public String getNombreArchivoCaratulaPoliza() {
		if (!this.idControlArchivoCaratulaPoliza.equals("0") && this.nombreArchivoCaratulaPoliza==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoCaratulaPoliza));
				nombreArchivoCaratulaPoliza = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoCaratulaPoliza = "NO DISPONIBLE";
				e.printStackTrace();
			}
		}
		return nombreArchivoCaratulaPoliza;
	}
	public void setNombreArchivoCaratulaPoliza(String nombreArchivoCaratulaPoliza) {
		this.nombreArchivoCaratulaPoliza = nombreArchivoCaratulaPoliza;
	}
	public String getNombreArchivoCondicionesProducto() {
		if (!this.idControlArchivoCondicionesProducto.equals("0") && this.nombreArchivoCondicionesProducto==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoCondicionesProducto));
				nombreArchivoCondicionesProducto = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoCondicionesProducto = "NO DISPONIBLE";
				e.printStackTrace();
			}
		}
		return nombreArchivoCondicionesProducto;
	}
	public void setNombreArchivoCondicionesProducto(String nombreArchivoCondicionesProducto) {
		this.nombreArchivoCondicionesProducto = nombreArchivoCondicionesProducto;
	}
	public String getNombreArchivoNotaTecnica() {
		if (!this.idControlArchivoNotaTecnica.equals("0") && this.nombreArchivoNotaTecnica==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoNotaTecnica));
				nombreArchivoNotaTecnica = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoNotaTecnica = "NO DISPONIBLE";
				e.printStackTrace();
			}
		}
		return nombreArchivoNotaTecnica;
	}
	public void setNombreArchivoNotaTecnica(String nombreArchivoNotaTecnica) {
		this.nombreArchivoNotaTecnica = nombreArchivoNotaTecnica;
	}
	public String getNombreArchivoAnalisisCongruencia() {
		if (!this.idControlArchivoAnalisisCongruencia.equals("0") && this.nombreArchivoAnalisisCongruencia==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoAnalisisCongruencia));
				nombreArchivoAnalisisCongruencia = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoAnalisisCongruencia = "NO DISPONIBLE";
				e.printStackTrace();
			}
		}
		return nombreArchivoAnalisisCongruencia;
	}
	public void setNombreArchivoAnalisisCongruencia(String nombreArchivoAnalisisCongruencia) {
		this.nombreArchivoAnalisisCongruencia = nombreArchivoAnalisisCongruencia;
	}
	public String getNombreArchivoDictamenJuridico() {
		if (!this.idControlArchivoDictamenJuridico.equals("0") && this.nombreArchivoDictamenJuridico==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoDictamenJuridico));
				nombreArchivoDictamenJuridico = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoDictamenJuridico = "NO DISPONIBLE";
				e.printStackTrace();
			}
		}
		return nombreArchivoDictamenJuridico;
	}
	public void setNombreArchivoDictamenJuridico(String nombreArchivoDictamenJuridico) {
		this.nombreArchivoDictamenJuridico = nombreArchivoDictamenJuridico;
	}

	public void setMostrarTipoPolizaInactivas(String mostrarTipoPolizaInactivas) {
		this.mostrarTipoPolizaInactivas = mostrarTipoPolizaInactivas;
	}

	public String getMostrarTipoPolizaInactivas() {
		return mostrarTipoPolizaInactivas;
	}

	public String getClaveAjusteVigencia() {
		return claveAjusteVigencia;
	}

	public void setClaveAjusteVigencia(String claveAjusteVigencia) {
		this.claveAjusteVigencia = claveAjusteVigencia;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public String getClaveAplicaFlotillas() {
		return claveAplicaFlotillas;
	}

	public void setClaveAplicaFlotillas(String claveAplicaFlotillas) {
		this.claveAplicaFlotillas = claveAplicaFlotillas;
	}

	public String getClaveAplicaAutoexpedible() {
		return claveAplicaAutoexpedible;
	}

	public void setClaveAplicaAutoexpedible(String claveAplicaAutoexpedible) {
		this.claveAplicaAutoexpedible = claveAplicaAutoexpedible;
	}

	public String getDiasGraciaSubsecuentes() {
		return diasGraciaSubsecuentes;
	}

	public void setDiasGraciaSubsecuentes(String diasGraciaSubsecuentes) {
		this.diasGraciaSubsecuentes = diasGraciaSubsecuentes;
	}

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public void setSoloLectura(String soloLectura) {
		this.soloLectura = soloLectura;
	}

	public String getSoloLectura() {
		return soloLectura;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}
}
