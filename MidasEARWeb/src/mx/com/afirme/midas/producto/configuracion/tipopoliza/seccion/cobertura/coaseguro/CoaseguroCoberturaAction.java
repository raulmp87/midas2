package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/*
 * @author Fernando Alonzo
 * @since 21 de agosto de 2009
 */
public class CoaseguroCoberturaAction extends CoberturaAction {

	/**
	 * Method asociarDescuento
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarCoaseguroCobertura(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarDescuentoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarCoaseguros(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		CoaseguroCoberturaDN coaseguroCoberturaDN = CoaseguroCoberturaDN.getInstancia();
		String action = "";
		CoaseguroCoberturaId id = new CoaseguroCoberturaId();
		CoberturaForm coberturaForm = (CoberturaForm) form;
		
		BigDecimal numeroSecuencia = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idCobertura"));
		id.setNumeroSecuencia(numeroSecuencia);
		id.setIdToCobertura(idToCobertura);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				CoaseguroCoberturaDTO coaseguroCoberturaDTO = new CoaseguroCoberturaDTO();
				coaseguroCoberturaDTO.setId(id);
				coaseguroCoberturaDTO.setClaveDefault(Short.parseShort(request.getParameter("claveDefault")));
				coaseguroCoberturaDTO.setValor(Double.parseDouble(request.getParameter("valor")));

				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					action = "insert";
					//Busca si ya existe
					List<CoaseguroCoberturaDTO> list = coaseguroCoberturaDN.listarCoaseguros(idToCobertura);
					for(CoaseguroCoberturaDTO item : list){
						if(item.getValor().equals(coaseguroCoberturaDTO.getValor())){
							response.setContentType("text/xml");
							PrintWriter pw = response.getWriter();
							pw.write("<data><action type=\"duplicateValue\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
							pw.flush();
							pw.close();
							return null;							
						}
					}
					//Numero de secuencia de db
					numeroSecuencia = coberturaDN.nextNumeroSecuenciaCoaseguro(idToCobertura);
					id.setNumeroSecuencia(numeroSecuencia);
					coaseguroCoberturaDTO.setId(id);
					coberturaDN.agregarCoaseguro(coberturaDTO, coaseguroCoberturaDTO);
					mensajeExitoAgregar(coberturaForm, request);
				} else {
					action = "update";
					coberturaDN.actualizarCoaseguro(coberturaDTO, coaseguroCoberturaDTO);
					mensajeExitoModificar(coberturaForm, request);
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				action = "deleted";
				CoaseguroCoberturaDTO coaseguroCoberturaDTO = new CoaseguroCoberturaDTO();
				coaseguroCoberturaDTO.setId(id);
				coberturaDN.eliminarCoaseguro(coberturaDTO, coaseguroCoberturaDTO);				
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (Exception e){
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"duplicateValue\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarCoaseguros(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaDTO = coberturaDN.getPorIdCascada(coberturaDTO);
		
		coberturaForm.setCoaseguros(coberturaDTO.getCoaseguros());

//		String json = "{rows:[";
//		if(coberturaForm.getCoaseguros() != null && coberturaForm.getCoaseguros().size() > 0) {
//			for(CoaseguroCoberturaDTO coaseguro : coberturaForm.getCoaseguros()) {
//				json += "{id:\"" + coaseguro.getId().getNumeroSecuencia() + "\",data:[0,";
//				json += coaseguro.getId().getIdToCobertura()+ ",0,\"";
//				json += coaseguro.getId().getNumeroSecuencia() + "\",";
//				json += coaseguro.getValor() + ",";
//				json += coaseguro.getClaveDefault() + "]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getCoaseguros() != null && coberturaForm.getCoaseguros().size() > 0) {
			for(CoaseguroCoberturaDTO coaseguro : coberturaForm.getCoaseguros()) {
				
				MidasJsonRow row = new MidasJsonRow();
				
				row.setId(coaseguro.getId().getNumeroSecuencia().toString());
				row.setDatos(
						"0",
						coaseguro.getId().getIdToCobertura().toString(),
						"0",
						coaseguro.getId().getNumeroSecuencia().toString(),
						coaseguro.getValor().toString(),
						coaseguro.getClaveDefault().toString()
						
				);
				json.addRow(row);
			}
		}
		
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
