package mx.com.afirme.midas.producto.configuracion.ramo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ramo.RamoDN;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.producto.ProductoAction;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RamoPorProductoAction extends ProductoAction{
	/**
	 * Method mostrarAsociarRamo
	 * 
	 * M�todo que recupera las listas de ramos asociados y no asociados a un producto
	 * y las coloca en la sesi�n del usuario.
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarRamo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoForm productoForm = (ProductoForm) form;
		String idToProducto = request.getParameter("id");
		if (idToProducto != null)
			productoForm.setIdToProducto(idToProducto);
		ProductoDTO productoDTO = new ProductoDTO();
		try {
			productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(productoForm.getIdToProducto()));
			productoForm.setNombreComercial(productoDTO.getNombreComercial());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRamosAsociados
	 * 
	 * M�todo que recupera la lista de ramos asociados a un producto
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRamosAsociados(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String id = request.getParameter("id");
			response.setContentType("text/json");
			ProductoForm productoForm = (ProductoForm) form;
			productoForm.setRamoProductoAsociados(RamoProductoDN.getInstancia().buscarPorPropiedad("id.idtoproducto", UtileriasWeb.regresaBigDecimal(id)));
//			String json = "{\"rows\":[";
//			if(productoForm.getRamoProductoAsociados() != null && productoForm.getRamoProductoAsociados().size() > 0){
//				List<RamoDTO> listaRamos = RamoDN.getInstancia().obtenerRamosObligatoriosPorProducto(((RamoProductoDTO)productoForm.getRamoProductoAsociados().get(0)).getId().getIdtoproducto());
//				for (RamoProductoDTO actual : productoForm.getRamoProductoAsociados()){
//					json += "{\"id\":\"" + actual.getId().getIdtcramo() + "\",\"data\":[\"";
//					json += actual.getRamoDTO().getCodigo() + "\",\"";
//					json += actual.getRamoDTO().getDescripcion() + "\",\"";
//					json += this.contieneRamo(actual, listaRamos) + "\"]},";
//				}
//				json = json.substring(0, json.length() -1) + "]}";
//			}
//			else
//				json += "]}";
			MidasJsonBase json = new MidasJsonBase();
			if(productoForm.getRamoProductoAsociados() != null && productoForm.getRamoProductoAsociados().size() > 0){
				List<RamoDTO> listaRamos = RamoDN.getInstancia().obtenerRamosObligatoriosPorProducto(((RamoProductoDTO)productoForm.getRamoProductoAsociados().get(0)).getId().getIdtoproducto());
				for (RamoProductoDTO actual : productoForm.getRamoProductoAsociados()){
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(actual.getId().getIdtcramo().toString());
					row.setDatos(
							actual.getRamoDTO().getCodigo().toString(),
							actual.getRamoDTO().getDescripcion(),
							this.contieneRamo(actual, listaRamos) + ""
					);
					json.addRow(row);
					
				}
			}
			System.out.println(json);
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method guardarRamoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void guardarRamoAsociado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		ProductoForm productoForm = (ProductoForm) form;
		ProductoDTO productoDTO = new ProductoDTO();
		ProductoDN productoDN = ProductoDN.getInstancia();
		String action = "";
		try {
			poblarDTO(productoForm, productoDTO);
			productoDTO.setIdToProducto(new BigDecimal(request.getParameter("id")));
			if(request.getParameter("!nativeeditor_status").equals("inserted")) {
				RamoProductoId id = new RamoProductoId();
				id.setIdtcramo(new BigDecimal(request.getParameter("gr_id")));
				id.setIdtoproducto(productoDTO.getIdToProducto());
				
				RamoProductoDTO ramoProductoDTO = new RamoProductoDTO();
				ramoProductoDTO.setId(id);
				
				RamoDTO ramo = new RamoDTO();
				ramo.setIdTcRamo(ramoProductoDTO.getId().getIdtcramo());
				ramo = new RamoDN().getRamoPorId(ramo);
				
				ProductoDTO producto = new ProductoDTO();
				producto.setIdToProducto(ramoProductoDTO.getId().getIdtoproducto());
				producto = new ProductoDN().getPorId(producto);
				
				ramoProductoDTO.setProductoDTO(producto);
				ramoProductoDTO.setRamoDTO(ramo);
				
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					productoDN.asociarRamo(ramoProductoDTO);
					action = "insert";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				RamoProductoId id = new RamoProductoId();
				id.setIdtcramo(new BigDecimal(request.getParameter("gr_id")));
				id.setIdtoproducto(productoDTO.getIdToProducto());
				
				RamoProductoDTO ramoProductoDTO = new RamoProductoDTO();
				ramoProductoDTO.setId(id);
				productoDN.desasociarRamo(ramoProductoDTO);
				
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
		} catch (SystemException e) {	e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {	e.printStackTrace();
		} catch (IOException e) {	e.printStackTrace();
		}
	}
	
	/**
	 * Method mostrarRamosPorAsociar
	 * 
	 * M�todo que recupera la lista de ramos no asociados a un producto
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRamosPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id");
		response.setContentType("text/json");
		try {
			productoForm.setRamosPorAsociar(ProductoDN.getInstancia().listarRamosNoAsociados(UtileriasWeb.regresaBigDecimal(id)));
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
//		String json = "{\"rows\":[";
//		if(productoForm.getRamosPorAsociar() != null && productoForm.getRamosPorAsociar().size() >0){
//			for (RamoDTO actual : productoForm.getRamosPorAsociar()){
//				json += "{\"id\":\"" + actual.getIdTcRamo() + "\",\"data\":[\"";
//				json += actual.getCodigo() + "\",\"";
//				json += actual.getDescripcion() + "\",\"false\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else
//			json += "]}";
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getRamosPorAsociar() != null && productoForm.getRamosPorAsociar().size() >0){
			for (RamoDTO actual : productoForm.getRamosPorAsociar()){
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getIdTcRamo().toString());
				row.setDatos(
						actual.getCodigo().toString(),
						actual.getDescripcion(),
						"false"
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method contieneRamo
	 * 		Verifica si el RamoDTO recibido est� dentro de la lista de ramos recibida.
	 * @return true, si el ramo recibido est� contenido en la lista de ramos recibida como par�metro.
	 */
	private boolean contieneRamo(RamoProductoDTO ramo,List<RamoDTO> listaRamosObligatorios){
		boolean result = false;
		for(RamoDTO ramoTMP: listaRamosObligatorios){
			if (ramo.getId().getIdtcramo().equals(ramoTMP.getIdTcRamo())){
				result = true;
				break;
			}
		}
		return result;
	}
}
