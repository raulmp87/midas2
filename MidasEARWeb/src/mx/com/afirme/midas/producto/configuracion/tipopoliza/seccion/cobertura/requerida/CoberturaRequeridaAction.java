package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * @author Fernando Alonzo
 * @since 19 de agosto de 2009
 */
public class CoberturaRequeridaAction extends CoberturaAction {

	/**
	 * Method asociarCoberturasExcluidas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward asociarCoberturasRequeridas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		HttpSession session = request.getSession();
		session.setAttribute("idToSeccion", request.getParameter("idToSeccion"));
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarCoberturasExcluidas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarCoberturasRequeridas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		String action = "";
		CoberturaRequeridaId id = new CoberturaRequeridaId();
		BigDecimal idToCoberturaBase = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCoberturaBase"));
		BigDecimal idToCoberturaRequerida = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		id.setIdToCobertura(idToCoberturaBase);
		id.setIdToCoberturaRequerida(idToCoberturaRequerida);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				CoberturaRequeridaDTO coberturaRequeridaDTO = new CoberturaRequeridaDTO();
				coberturaRequeridaDTO.setId(id);

				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					coberturaDN.asociaCoberturaRequerida(coberturaRequeridaDTO);
					action = "insert";
				} else {
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				CoberturaRequeridaDTO coberturaRequeridaDTO = new CoberturaRequeridaDTO();
				coberturaRequeridaDTO.setId(id);
				coberturaDN.borrarCoberturaRequerida(coberturaRequeridaDTO);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarCoberturasRequeridas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaDTO = coberturaDN.getPorIdCascada(coberturaDTO);
		coberturaForm.setCoberturasRequeridas(coberturaDTO.getCoberturasRequeridas());

		
//		String json = "{rows:[";
//		if(coberturaForm.getCoberturasRequeridas() != null && coberturaForm.getCoberturasRequeridas().size() > 0) {
//			for(CoberturaRequeridaDTO coberturaRequerida : coberturaForm.getCoberturasRequeridas()) {
//				json += "{id:\"" + coberturaRequerida.getId().getIdToCoberturaRequerida() + "\",data:[\"";
//				json += coberturaRequerida.getId().getIdToCobertura() + "\",\"";
//				json += coberturaRequerida.getCoberturaRequeridaDTO().getCodigo() + "\",\"";
//				json += coberturaRequerida.getCoberturaRequeridaDTO().getNombreComercial() + "\",\"";
//				json += coberturaRequerida.getCoberturaRequeridaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getCoberturasRequeridas() != null && coberturaForm.getCoberturasRequeridas().size() > 0) {
			for(CoberturaRequeridaDTO coberturaRequerida : coberturaForm.getCoberturasRequeridas()) {
				CoberturaDTO coberturaDTORequerida = coberturaDN.getPorId(coberturaRequerida.getId().getIdToCoberturaRequerida());
				MidasJsonRow row = new MidasJsonRow();
				row.setId(coberturaRequerida.getId().getIdToCoberturaRequerida().toString());
				row.setDatos(
						coberturaRequerida.getId().getIdToCobertura().toString(),
						coberturaDTORequerida.getCodigo(),
						coberturaDTORequerida.getNombreComercial(),
						coberturaDTORequerida.getDescripcion()
				);
				json.addRow(row);
			}
		}
		
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarCoberturasNoRequeridas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		
		HttpSession session = request.getSession();
		String idToSeccion = session.getAttribute("idToSeccion").toString();
		session.removeAttribute("idToSeccion");
		
		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		coberturaForm.setCoberturasNoRequeridas(coberturaDN.listarCoberturasNoRequeridas(coberturaDTO, UtileriasWeb.regresaBigDecimal(idToSeccion)));

		
		
//		String json = "{rows:[";
//		if(coberturaForm.getCoberturasNoRequeridas() != null && coberturaForm.getCoberturasNoRequeridas().size() > 0) {
//			for(CoberturaDTO cobertura : coberturaForm.getCoberturasNoRequeridas()) {
//				json += "{id:\"" + cobertura.getIdToCobertura() + "\",data:[\"";
//				json += coberturaDTO.getIdToCobertura() + "\",\"";
//				json += cobertura.getCodigo() + "\",\"";
//				json += cobertura.getNombreComercial() + "\",\"";
//				json += cobertura.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getCoberturasNoRequeridas() != null && coberturaForm.getCoberturasNoRequeridas().size() > 0) {
			for(CoberturaDTO cobertura : coberturaForm.getCoberturasNoRequeridas()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(cobertura.getIdToCobertura().toString());
				row.setDatos(
						coberturaDTO.getIdToCobertura().toString(),
						cobertura.getCodigo(),
						cobertura.getNombreComercial(),
						cobertura.getDescripcion()
				);
				json.addRow(row);
			}
		}
		
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
