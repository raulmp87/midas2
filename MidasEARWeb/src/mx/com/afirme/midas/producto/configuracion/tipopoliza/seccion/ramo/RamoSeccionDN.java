package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RamoSeccionDN {
	private static final RamoSeccionDN INSTANCIA = new RamoSeccionDN();

	public static RamoSeccionDN getInstancia() {
		return RamoSeccionDN.INSTANCIA;
	}

	public List<RamoSeccionDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoSeccionSN().listarTodos();
	}

	public void agregar(RamoSeccionDTO ramoSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoSeccionSN().agregar(ramoSeccionDTO);
	}

	public void modificar(RamoSeccionDTO ramoSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoSeccionSN().modificar(ramoSeccionDTO);
	}

	public RamoSeccionDTO getPorId(RamoSeccionDTO RamoSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoSeccionSN().getPorId(RamoSeccionDTO.getId());
	}

	public void borrar(RamoSeccionDTO RamoSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoSeccionSN().borrar(RamoSeccionDTO);
	}
	
	public List<RamoSeccionDTO> buscarPorPropiedad (String propiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {	
		return new RamoSeccionSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<RamoDTO> listarRamosPorAsociar(SeccionDTO SeccionDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		List<RamoSeccionDTO> RamoSeccionDTOList = new RamoSeccionSN().listarRamosPorAsociar(SeccionDTO.getIdToSeccion());
		RamoSN ramoSN = new RamoSN();
		List<RamoDTO> ramoDTOList = new ArrayList<RamoDTO>();
		RamoDTO ramoTMP = new RamoDTO();
		for (RamoSeccionDTO actual : RamoSeccionDTOList){
			ramoTMP.setIdTcRamo(actual.getId().getIdtcramo());
			ramoDTOList.add(ramoSN.getRamoPorId(ramoTMP));
		}
		return ramoDTOList;
	}
}
