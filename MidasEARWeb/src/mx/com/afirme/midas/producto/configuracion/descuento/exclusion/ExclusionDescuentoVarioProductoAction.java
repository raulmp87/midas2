package mx.com.afirme.midas.producto.configuracion.descuento.exclusion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.ProductoAction;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoForm;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ExclusionDescuentoVarioProductoAction extends ProductoAction{
	/**
	 * Method mostrarExclusionDescuento
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionDescuento(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id");
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(BigDecimal.valueOf(Double.valueOf(id)));
		try {
			productoDTO = new ProductoDN().getPorId(productoDTO);
			//this.poblarDTO(productoForm, productoDTO);
			productoForm.setIdToProducto(productoDTO.getIdToProducto().toString());
			//Listas para el filtrado de exlusion de descuentos
			productoForm.setTiposPoliza(TipoPolizaDN.getInstancia().listarVigentesPorIdProducto(productoDTO.getIdToProducto()));
			productoForm.setDescuentosAsociados(new DescuentoVarioProductoSN().listarDescuentoAsociado(productoDTO.getIdToProducto()));
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcDescuentoProductoAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcDescuentoProductoAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		ProductoForm productoForm = (ProductoForm) form;

		String id = request.getParameter("id").toString();
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		ProductoDN productoDN =  ProductoDN.getInstancia();
		productoForm.setDescuentosAsociados(productoDN.getPorIdCascada(productoDTO).getDescuentos());
		productoDTO = productoDN.getPorId(productoDTO);
		productoForm.setExcDescuentoProductoAsociados(productoDN.listarExcDescuentoProductoAsociadas(productoDTO));
//		String json = "{rows:[";
//		if(productoForm.getExcDescuentoProductoAsociados()!= null && productoForm.getExcDescuentoProductoAsociados().size() > 0) {
//			for(ExclusionDescuentoVarioProductoDTO actual : productoForm.getExcDescuentoProductoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id+ "\",data:[";
//				json += actual.getDescuentoDTO().getClaveTipo() + ",\"";
//				json += actual.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += actual.getTipoPolizaDTO().getCodigo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getExcDescuentoProductoAsociados()!= null && productoForm.getExcDescuentoProductoAsociados().size() > 0) {
			for(ExclusionDescuentoVarioProductoDTO actual : productoForm.getExcDescuentoProductoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id);
				row.setDatos(
						actual.getDescuentoDTO().getClaveTipo().toString(),
						actual.getDescuentoDTO().getDescripcion(),
						actual.getTipoPolizaDTO().getCodigo(),
						actual.getTipoPolizaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcDescuentoProductoAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcDescuentoProductoAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoDN productoDN = ProductoDN.getInstancia(); 
		DescuentoDN descuentoDN = new DescuentoDN();
		ExclusionDescuentoVarioProductoId id = new ExclusionDescuentoVarioProductoId();
		TipoPolizaDN polizaDN = new TipoPolizaDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		try {
			id.setIdtodescuentovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtotipopoliza(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtoproducto(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionDescuentoVarioProductoDTO excDescuentoProducto = new ExclusionDescuentoVarioProductoDTO();
			excDescuentoProducto.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				DescuentoDTO descuento = new DescuentoDTO();
				descuento.setIdToDescuentoVario(UtileriasWeb.regresaBigDecimal(ids[0]));
				descuento = descuentoDN.getPorId(descuento);

				TipoPolizaDTO poliza = new TipoPolizaDTO();
				poliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(ids[1]));
				poliza = polizaDN.getPorId(poliza);
				
				ProductoDTO producto = new  ProductoDTO();
				producto.setIdToProducto(UtileriasWeb.regresaBigDecimal(ids[2]));
				producto = productoDN.getPorId(producto);
				
				excDescuentoProducto.setDescuentoDTO(descuento);
				excDescuentoProducto.setProductoDTO(producto);
				excDescuentoProducto.setTipoPolizaDTO(poliza);

				productoDN.ExcluirDescuentoProducto(excDescuentoProducto);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				productoDN.EliminarExclusionDescuentoProducto(excDescuentoProducto);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcDescuentoProductoPorAsociar
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcDescuentoProductoPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id").toString();
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		//Posibles parámetros recibidos para hacer un filtrado por tipoPoliza o Descuento
		String idToTipoPoliza = request.getParameter("idToTipoPoliza");
		String idToDescuento = request.getParameter("idToDescuento");
		if (idToTipoPoliza != null)
			productoForm.setExcDescuentoProductoNoAsociados(ProductoDN.getInstancia().listarExcDescuentoProductoNoAsociadas(productoDTO,UtileriasWeb.regresaBigDecimal(idToTipoPoliza)));
		else if (idToDescuento != null)
			productoForm.setExcDescuentoProductoNoAsociados(ProductoDN.getInstancia().listarExcDescuentoProductoNoAsociadas(UtileriasWeb.regresaBigDecimal(idToDescuento),productoDTO));
		else
			productoForm.setExcDescuentoProductoNoAsociados(ProductoDN.getInstancia().listarExcDescuentoProductoNoAsociadas(productoDTO));
			
//		String json = "{rows:[";
//		if(productoForm.getExcDescuentoProductoNoAsociados() != null && productoForm.getExcDescuentoProductoNoAsociados().size() > 0) {
//			for(ExclusionDescuentoVarioProductoDTO actual : productoForm.getExcDescuentoProductoNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id+ "\",data:[";
//				json += actual.getDescuentoDTO().getClaveTipo() + ",\"";
//				json += actual.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += actual.getTipoPolizaDTO().getCodigo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getExcDescuentoProductoNoAsociados() != null && productoForm.getExcDescuentoProductoNoAsociados().size() > 0) {
			for(ExclusionDescuentoVarioProductoDTO actual : productoForm.getExcDescuentoProductoNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id);
				row.setDatos(
						actual.getDescuentoDTO().getClaveTipo().toString(),
						actual.getDescuentoDTO().getDescripcion(),
						actual.getTipoPolizaDTO().getCodigo(),
						actual.getTipoPolizaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
