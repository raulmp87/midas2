package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoaseguroCoberturaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 21 de Agosto de 2009
	 */

	private CoaseguroCoberturaFacadeRemote beanRemoto;

	public CoaseguroCoberturaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(CoaseguroCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(CoaseguroCoberturaDTO coaseguroCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(coaseguroCoberturaDTO);
		return null;
	}

	public String actualizar(CoaseguroCoberturaDTO coaseguroCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(coaseguroCoberturaDTO);
		return null;
	}

	public String borrar(CoaseguroCoberturaDTO coaseguroCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(coaseguroCoberturaDTO);
		return null;
	}

	public List<CoaseguroCoberturaDTO> listarCoaseguros(
			BigDecimal idToCobertura) {
		return beanRemoto.listarCoaseguros(idToCobertura);
	}

	public List<CoaseguroCoberturaDTO> listarCoasegurosPorCobertura(BigDecimal idToCobertura) {
		return beanRemoto.listarCoasegurosPorCobertura(idToCobertura);
	}
	
	public BigDecimal nextNumeroSecuencia(BigDecimal idToCobertura) {
		return beanRemoto.nextNumeroSecuencia(idToCobertura);
	}	
}
