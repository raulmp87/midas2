package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form.PaquetePolizaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form.RespuestaActualizacionPaqueteForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * 
 * @author jose luis arellano
 * @since 15/04/2011
 */
public class ConfiguracionPaquetesAction extends MidasMappingDispatchAction {

	public ActionForward mostrarPrincipalPaquetes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String idToTipoPolizaString = request.getParameter("id");
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		tipoPolizaForm.setIdToTipoPoliza(idToTipoPolizaString);
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	/**
	 * Consulta los paquetes registrados para un tipo de p�liza y regresa un documento XML para formatear un grid
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward obtenerPaquetesPorTipoPoliza(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String idToTipoPolizaString = request.getParameter("id");
		List<PaquetePolizaDTO> listaPaquetes = new ArrayList<PaquetePolizaDTO>();
		if(!UtileriasWeb.esCadenaVacia(idToTipoPolizaString)){
			BigDecimal idToTipoPoliza;
			try {
				idToTipoPoliza = UtileriasWeb.regresaBigDecimal(idToTipoPolizaString);
				listaPaquetes = PaquetePolizaDN.getInstancia().listarPorTipoPoliza(idToTipoPoliza,true);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		tipoPolizaForm.setIdToTipoPoliza(idToTipoPolizaString);
		tipoPolizaForm.setListaPaquetes(listaPaquetes);
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	/**
	 * Action usado para mostrar el detalle de un paquete. S&oacute;lo consulta los datos del paquete, sin incluir sus coberturas.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward desplegarPaquete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		if(tipoPolizaForm.getPaquetePolizaForm() != null &&
				!UtileriasWeb.esCadenaVacia(tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza())){
			try {
				PaquetePolizaDTO paquetePoliza = PaquetePolizaDN.getInstancia().getPorId(
						UtileriasWeb.regresaBigDecimal(tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza()));
				PaquetePolizaForm paquetePolizaForm = new PaquetePolizaForm();
				
				poblarPaquetePolizaForm(paquetePolizaForm, paquetePoliza);
				tipoPolizaForm.setPaquetePolizaForm(paquetePolizaForm);
				
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error al intentar desplegar informaci�n de paquete.", Level.WARNING, e);
			}
		}
		else{
			if(tipoPolizaForm.getPaquetePolizaForm() != null)
				tipoPolizaForm.setPaquetePolizaForm(new PaquetePolizaForm());
			tipoPolizaForm.getPaquetePolizaForm().setIdToTipoPoliza(tipoPolizaForm.getIdToTipoPoliza());
		}
		if(!UtileriasWeb.esCadenaVacia(tipoPolizaForm.getClaveAccion())){
			if(tipoPolizaForm.getClaveAccion().equalsIgnoreCase("agregarPaquete") ||
					tipoPolizaForm.getClaveAccion().equalsIgnoreCase("editarPaquete")){
				tipoPolizaForm.getPaquetePolizaForm().setSoloLectura(Boolean.FALSE.toString());
			}
			else{
				tipoPolizaForm.getPaquetePolizaForm().setSoloLectura(Boolean.TRUE.toString());
			}
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward agregarPaquete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		if(tipoPolizaForm.getPaquetePolizaForm() != null ){
			try {
				PaquetePolizaDTO paquetePoliza = new PaquetePolizaDTO();
				paquetePoliza.setTipoPolizaDTO(new TipoPolizaDTO());
				tipoPolizaForm.getPaquetePolizaForm().setIdToTipoPoliza(tipoPolizaForm.getIdToTipoPoliza());
				poblarPaquetePolizaDTO(paquetePoliza, tipoPolizaForm.getPaquetePolizaForm());
				
				paquetePoliza = PaquetePolizaDN.getInstancia().agregar(paquetePoliza);
				
				poblarPaquetePolizaForm(tipoPolizaForm.getPaquetePolizaForm(), paquetePoliza);
			} catch (Exception e) {
				LogDeMidasWeb.log("Error al intentar registrar paquete del tipo de p�liza: "+tipoPolizaForm.getIdToTipoPoliza(), Level.WARNING, e);
				tipoPolizaForm.setMensaje("Ocurri� un error al registrar el Paquete. Verifique los datos e intente nuevamente.");
			}
			tipoPolizaForm.getPaquetePolizaForm().setSoloLectura(Boolean.FALSE.toString());
			tipoPolizaForm.setClaveAccion("editarPaquete");
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward modificarPaquete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		if(tipoPolizaForm.getPaquetePolizaForm() != null ){
			try {
				PaquetePolizaDTO paquetePoliza = new PaquetePolizaDTO();
				tipoPolizaForm.getPaquetePolizaForm().setIdToTipoPoliza(tipoPolizaForm.getIdToTipoPoliza());
				poblarPaquetePolizaDTO(paquetePoliza, tipoPolizaForm.getPaquetePolizaForm());
				
				//Se agreg� el estatus debido a que es un campo obligatorio en base de datos.
				paquetePoliza.setEstatus(PaquetePolizaDTO.ESTATUS_CONFIGURANDO);
				
				paquetePoliza = PaquetePolizaDN.getInstancia().modificar(paquetePoliza);
				
				poblarPaquetePolizaForm(tipoPolizaForm.getPaquetePolizaForm(), paquetePoliza);
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error al intentar modificar paquete del tipo de p�liza: "+tipoPolizaForm.getIdToTipoPoliza(), Level.WARNING, e);
			}
			tipoPolizaForm.getPaquetePolizaForm().setSoloLectura(Boolean.FALSE.toString());
			tipoPolizaForm.setClaveAccion("editarPaquete");
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward obtenerCoberturasRegistradas(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		if(tipoPolizaForm.getPaquetePolizaForm() != null && 
				!UtileriasWeb.esCadenaVacia(tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza())){
			try {
				BigDecimal idToPaquetePoliza = UtileriasWeb.regresaBigDecimal(tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza());
				List<CoberturaSeccionPaqueteDTO> listaCoberturasRegistradas = PaquetePolizaDN.getInstancia().obtenerCoberturasRegistradas(idToPaquetePoliza);
				
				tipoPolizaForm.setListaCoberturasPaquete(listaCoberturasRegistradas);
				
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error al consultar las coberturas del paquete "+tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza()+
						",del tipo de p�liza: "+tipoPolizaForm.getIdToTipoPoliza(), Level.WARNING, e);
			}
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward obtenerCoberturasDisponibles(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		if(tipoPolizaForm.getPaquetePolizaForm() != null && 
				!UtileriasWeb.esCadenaVacia(tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza())){
			try {
				BigDecimal idToPaquetePoliza = UtileriasWeb.regresaBigDecimal(tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza());
				List<CoberturaSeccionDTO> listaCoberturasDisponibles = PaquetePolizaDN.getInstancia().obtenerCoberturasDisponibles(idToPaquetePoliza);
				
				tipoPolizaForm.setListaCoberturasDisponibles(listaCoberturasDisponibles);
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error al consultar las coberturas disponibles del paquete "+tipoPolizaForm.getPaquetePolizaForm().getIdToPaquetePoliza()+
						", del tipo de p�liza: "+tipoPolizaForm.getIdToTipoPoliza(), Level.WARNING, e);
			}
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward guardarCoberturaPaquete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		
		String accion = request.getParameter("!nativeeditor_status");
		String idOriginal = request.getParameter("gr_id");
		String idResultado = idOriginal;
		String claveMensaje = "";
		String operacionExitosa = "";
		String mensajeError = "";
		try {
			BigDecimal idToPaquetePoliza = UtileriasWeb.regresaBigDecimal(request.getParameter("idToPaquetePol"));
			String idCoberturaString = request.getParameter("idCoberturaSecPaq");
			BigDecimal idCoberturaSeccionPaq = null;
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
			BigDecimal montoSumaAsegurada = UtileriasWeb.regresaBigDecimal(request.getParameter("montoSumaAsegurada"));
			
			Short claveContrato = null;
			
			if(accion.equals("inserted") || accion.equals("updated")){
				claveContrato = ConstantesCotizacion.CONTRATADO;
			}
			else if (accion.equals("deleted")){
				claveContrato = ConstantesCotizacion.NO_CONTRATADO;
			}
			
			if(!UtileriasWeb.esCadenaVacia(idCoberturaString)){
				idCoberturaSeccionPaq = UtileriasWeb.regresaBigDecimal(idCoberturaString);
			}
			
			ActualizacionCoberturaPaqueteDTO resultadoOperacion = PaquetePolizaDN.getInstancia().
					registrarCoberturaPaquete(idToPaquetePoliza, idCoberturaSeccionPaq, idToSeccion, idToCobertura, montoSumaAsegurada, claveContrato);
			
			if(resultadoOperacion != null){
				//Verificar la clave de respuesta y generar el mensaje de error correspondiente.
				
				claveMensaje = resultadoOperacion.getClaveMensaje();
				operacionExitosa = resultadoOperacion.getOperacionExitosa().toString();
				
				if(!resultadoOperacion.getOperacionExitosa()){
//					extraInfo = "invalid";
					accion="error";
				}
				else{
					idResultado = resultadoOperacion.getCoberturaPaqueteDTO().getIdToCoberturaSeccionPaquete().toString();
					//TODO deducir la informacion adicional para mostrar al usuario.
				}
				
				mensajeError = resultadoOperacion.getMensajeError();
			}
			
		}catch (RuntimeException re){
			operacionExitosa = Boolean.FALSE.toString();
			accion="error";
			claveMensaje = Sistema.ERROR;
		}
		catch (Exception e) {
			operacionExitosa = Boolean.FALSE.toString();
			mensajeError = e.getMessage();
			accion="error";
			claveMensaje = Sistema.ERROR;
		}
		
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		RespuestaActualizacionPaqueteForm respuesta = new RespuestaActualizacionPaqueteForm();
		respuesta.setTipoRespuesta(accion);
		respuesta.setIdOriginal(idOriginal);
		respuesta.setIdResultado(idResultado);
		respuesta.setTipoMensaje(claveMensaje);
		respuesta.setOperacionExitosa(operacionExitosa);
		respuesta.setMensaje(mensajeError);
		tipoPolizaForm.setRespuestaActualizacionPaqueteForm(respuesta);
		
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward liberarPaquetePoliza(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String accion = request.getParameter("!nativeeditor_status");
		String idOriginal = request.getParameter("gr_id");
		String idResultado = idOriginal;
		String claveMensaje = "";
		String operacionExitosa = "";
		String mensajeError = "";
		BigDecimal idToTipoPoliza = null;
		try {
			BigDecimal idToPaquetePoliza = UtileriasWeb.regresaBigDecimal(request.getParameter("idPaquete"));
			
			ActualizacionCoberturaPaqueteDTO resultadoOperacion = PaquetePolizaDN.getInstancia().liberarPaquete(idToPaquetePoliza);
			
			if(resultadoOperacion != null){
				//Verificar la clave de respuesta y generar el mensaje de error correspondiente.
			
				idToTipoPoliza = resultadoOperacion.getPaquetePoliza().getTipoPolizaDTO().getIdToTipoPoliza();
				claveMensaje = resultadoOperacion.getClaveMensaje();
				operacionExitosa = resultadoOperacion.getOperacionExitosa().toString();
				
				if(!resultadoOperacion.getOperacionExitosa()){
					accion="error";
				}
				else{
					idResultado = resultadoOperacion.getCoberturaPaqueteDTO().getIdToCoberturaSeccionPaquete().toString();
				}
				
				mensajeError = resultadoOperacion.getMensajeError();
			}
			
		} catch (Exception e) {
			operacionExitosa = Boolean.FALSE.toString();
			mensajeError = e.getMessage();
			accion="error";
			claveMensaje = Sistema.ERROR;
		}
		
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		RespuestaActualizacionPaqueteForm respuesta = new RespuestaActualizacionPaqueteForm();
		respuesta.setTipoRespuesta(accion);
		respuesta.setIdOriginal(idOriginal);
		respuesta.setIdResultado(idResultado);
		respuesta.setTipoMensaje(claveMensaje);
		respuesta.setOperacionExitosa(operacionExitosa);
		respuesta.setMensaje(mensajeError);
		respuesta.setIdToTipoPoliza(""+idToTipoPoliza);
		tipoPolizaForm.setRespuestaActualizacionPaqueteForm(respuesta);
		
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward eliminarPaquetePoliza(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String accion = request.getParameter("!nativeeditor_status");
		String idOriginal = request.getParameter("gr_id");
		String idResultado = idOriginal;
		String claveMensaje = "";
		String operacionExitosa = "";
		String mensajeError = "";
		BigDecimal idToTipoPoliza = null;
		try {
			BigDecimal idToPaquetePoliza = UtileriasWeb.regresaBigDecimal(request.getParameter("idPaquete"));
			
			ActualizacionCoberturaPaqueteDTO resultadoOperacion = PaquetePolizaDN.getInstancia().eliminarPaquete(idToPaquetePoliza);
			
			if(resultadoOperacion != null){
				//Verificar la clave de respuesta y generar el mensaje de error correspondiente.
				
				idToTipoPoliza = resultadoOperacion.getPaquetePoliza().getTipoPolizaDTO().getIdToTipoPoliza();
				claveMensaje = resultadoOperacion.getClaveMensaje();
				operacionExitosa = resultadoOperacion.getOperacionExitosa().toString();
				
				if(!resultadoOperacion.getOperacionExitosa()){
					accion="error";
				}
				else{
					idResultado = resultadoOperacion.getCoberturaPaqueteDTO().getIdToCoberturaSeccionPaquete().toString();
				}
				
				mensajeError = resultadoOperacion.getMensajeError();
			}
			
		} catch (Exception e) {
			operacionExitosa = Boolean.FALSE.toString();
			mensajeError = e.getMessage();
			accion="error";
			claveMensaje = Sistema.ERROR;
		}
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		RespuestaActualizacionPaqueteForm respuesta = new RespuestaActualizacionPaqueteForm();
		respuesta.setTipoRespuesta(accion);
		respuesta.setIdOriginal(idOriginal);
		respuesta.setIdResultado(idResultado);
		respuesta.setTipoMensaje(claveMensaje);
		respuesta.setOperacionExitosa(operacionExitosa);
		respuesta.setMensaje(mensajeError);
		respuesta.setIdToTipoPoliza(""+idToTipoPoliza);
		tipoPolizaForm.setRespuestaActualizacionPaqueteForm(respuesta);
		
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward definirPaquetePoliza(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String accion = request.getParameter("!nativeeditor_status");
		String idOriginal = request.getParameter("gr_id");
		String idResultado = idOriginal;
		String claveMensaje = "";
		String operacionExitosa = "";
		String mensajeError = "";
		BigDecimal idToTipoPoliza = null;
		try {
			BigDecimal idToPaquetePoliza = UtileriasWeb.regresaBigDecimal(request.getParameter("idPaquete"));
			
			PaquetePolizaDTO paquetePoliza = PaquetePolizaDN.getInstancia().establecerPaqueteDefault(idToPaquetePoliza);
			
			if(paquetePoliza != null){
				//Verificar la clave de respuesta y generar el mensaje de error correspondiente.
				
				idToTipoPoliza = paquetePoliza.getTipoPolizaDTO().getIdToTipoPoliza();
				operacionExitosa = Boolean.TRUE.toString();
				
				idResultado = paquetePoliza.getIdToPaquetePoliza().toString();
			}
			else{
				throw new RuntimeException("No se recibi� respuesta de la operaci�n. Intente nuevamente.");
			}
		} catch (Exception e) {
			operacionExitosa = Boolean.FALSE.toString();
			mensajeError = e.getMessage();
			accion="error";
			claveMensaje = Sistema.ERROR;
		}
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		RespuestaActualizacionPaqueteForm respuesta = new RespuestaActualizacionPaqueteForm();
		respuesta.setTipoRespuesta(accion);
		respuesta.setIdOriginal(idOriginal);
		respuesta.setIdResultado(idResultado);
		respuesta.setTipoMensaje(claveMensaje);
		respuesta.setOperacionExitosa(operacionExitosa);
		respuesta.setMensaje(mensajeError);
		respuesta.setIdToTipoPoliza(""+idToTipoPoliza);
		tipoPolizaForm.setRespuestaActualizacionPaqueteForm(respuesta);
		
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void poblarPaquetePolizaForm(PaquetePolizaForm paquetePolizaForm, PaquetePolizaDTO paquetePolizaDTO){
		BeanFiller beanFiller = new BeanFiller();
		beanFiller.estableceMapeoResultados(PaquetePolizaForm.class, PaquetePolizaDN.ATRIBUTOS_MAPEO_FORM, PaquetePolizaDN.ATRIBUTOS_MAPEO_DTO);
		beanFiller.obtenerResultadoMapeo(paquetePolizaForm, paquetePolizaDTO);
	}
	
	private void poblarPaquetePolizaDTO(PaquetePolizaDTO paquetePolizaDTO,PaquetePolizaForm paquetePolizaForm){
		BeanFiller beanFiller = new BeanFiller();
		beanFiller.estableceMapeoResultados(PaquetePolizaDTO.class, PaquetePolizaDN.ATRIBUTOS_MAPEO_DTO, PaquetePolizaDN.ATRIBUTOS_MAPEO_FORM);
		beanFiller.obtenerResultadoMapeo(paquetePolizaDTO,paquetePolizaForm);
	}
}
