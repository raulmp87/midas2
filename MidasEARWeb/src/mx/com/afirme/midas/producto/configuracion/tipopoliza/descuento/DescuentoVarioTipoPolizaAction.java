package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 14 de agosto de 2009
 */
public class DescuentoVarioTipoPolizaAction extends TipoPolizaAction{
	/**
	 * Method mostrarAsociarDescuento
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarDescuento(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String idToTipoPoliza = request.getParameter("id");
		if (idToTipoPoliza != null)
			tipoPolizaForm.setIdToTipoPoliza(idToTipoPoliza);
		return mapping.findForward(reglaNavegacion);
	}
	
	public void mostrarDescuentosAsociados(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;

		String id = request.getParameter("id");
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();

		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
		tipoPolizaForm.setDescuentoTipoPolizaAsociados(tipoPolizaDN.listarDescuentosAsociados(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getDescuentoTipoPolizaAsociados() != null && tipoPolizaForm.getDescuentoTipoPolizaAsociados().size() > 0) {
//			for(DescuentoVarioTipoPolizaDTO descuento : tipoPolizaForm.getDescuentoTipoPolizaAsociados()) {
//				json += "{id:\"" + descuento.getId().getIdtodescuentovario()+"\",data:[";
//				json += id + ",\"";
//				json += descuento.getDescuentoDTO().getDescripcion() + "\",";
//				json += descuento.getClaveobligatoriedad() + ",";
//				json += descuento.getClavecomercialtecnico() + ",";
//				json += descuento.getClaveaplicareaseguro() + ",\"";
//				json += (descuento.getDescuentoDTO().getClaveTipo().intValue() == 1 ? "%":"$") + "\",";					
//				json += descuento.getValor() + "]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getDescuentoTipoPolizaAsociados() != null && tipoPolizaForm.getDescuentoTipoPolizaAsociados().size() > 0) {
			for(DescuentoVarioTipoPolizaDTO descuento : tipoPolizaForm.getDescuentoTipoPolizaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(descuento.getId().getIdtodescuentovario().toString());
				row.setDatos(
						id,
						descuento.getDescuentoDTO().getDescripcion(),
						descuento.getClaveobligatoriedad().toString(),
						descuento.getClavecomercialtecnico().toString(),
						descuento.getClaveaplicareaseguro().toString(),
						(descuento.getDescuentoDTO().getClaveTipo().intValue() == 1 ? "%":"$"),
						descuento.getValor().toString()
				);
				json.addRow(row);
				
			}
		}		
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	/**
	 * Method guardarDescuentoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarDescuentoAsociado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN = TipoPolizaDN.getInstancia();
		DescuentoDN descuentoDN = DescuentoDN.getInstancia();
		String action = "";
		DescuentoVarioTipoPolizaId id = new DescuentoVarioTipoPolizaId();
		BigDecimal idToDescuentoVario = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToTipoPoliza = UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre"));
		id.setIdtotipopoliza(idToTipoPoliza);
		id.setIdtodescuentovario(idToDescuentoVario);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				DescuentoVarioTipoPolizaDTO descuentoPorTipoPoliza = new DescuentoVarioTipoPolizaDTO();
				descuentoPorTipoPoliza.setId(id);
				descuentoPorTipoPoliza.setClaveaplicareaseguro(UtileriasWeb.regresaShort(request.getParameter("aplicaReaseguro")));
				descuentoPorTipoPoliza.setClavecomercialtecnico(UtileriasWeb.regresaShort(request.getParameter("comercialTecnico")));
				descuentoPorTipoPoliza.setClaveobligatoriedad(UtileriasWeb.regresaShort(request.getParameter("obligatorio").equals("1")? "3" : "0"));
				descuentoPorTipoPoliza.setValor(Double.parseDouble(request.getParameter("valor")));

				DescuentoDTO descuentoDTO = new DescuentoDTO();
				descuentoDTO.setIdToDescuentoVario(descuentoPorTipoPoliza.getId().getIdtodescuentovario());
				descuentoDTO = descuentoDN.getPorId(descuentoDTO);

				TipoPolizaDTO tp = new  TipoPolizaDTO();
				tp.setIdToTipoPoliza(id.getIdtotipopoliza());
				tp = tipoPolizaDN.getPorId(tp);
				descuentoPorTipoPoliza.setTipoPolizaDTO(tp);
				descuentoPorTipoPoliza.setDescuentoDTO(descuentoDTO);
				if(request.getParameter("!nativeeditor_status").equals("inserted")){
					tipoPolizaDN.asociarDescuento(tipoPolizaDTO, descuentoPorTipoPoliza);
					action = "insert";
				}
				else{ 
					tipoPolizaDN.actualizarAsociacion(descuentoPorTipoPoliza);
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				DescuentoVarioTipoPolizaDTO descuentoPorTipoPoliza = new DescuentoVarioTipoPolizaDTO();
				descuentoPorTipoPoliza.setId(id);
				descuentoPorTipoPoliza = new DescuentoVarioTipoPolizaSN().getPorId(id);
				tipoPolizaDN.desasociarDescuento(descuentoPorTipoPoliza);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarDescuentosPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id");
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));

		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();
		tipoPolizaForm.setDescuentosPorAsociar(tipoPolizaDN.listarDescuentoPorAsociar(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getDescuentosPorAsociar() != null && tipoPolizaForm.getDescuentosPorAsociar().size() > 0) {
//			for(DescuentoDTO descuento : tipoPolizaForm.getDescuentosPorAsociar()) {
//				json += "{id:\"" + descuento.getIdToDescuentoVario() + "\",data:[";
//				json += id + ",\""+descuento.getDescripcion() + "\"";
//				json += ",0,1,0,\"" + (descuento.getClaveTipo().intValue() == 1 ? "%":"$") + "\",0]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getDescuentosPorAsociar() != null && tipoPolizaForm.getDescuentosPorAsociar().size() > 0) {
			for(DescuentoDTO descuento : tipoPolizaForm.getDescuentosPorAsociar()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(descuento.getIdToDescuentoVario().toString());
				row.setDatos(
						id,
						descuento.getDescripcion(),
						"0",
						"1",
						"0",
						(descuento.getClaveTipo().intValue() == 1 ? "%":"$"),
						"0"
				);
				json.addRow(row);
				
			}
		}		
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
