package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class RespuestaActualizacionPaqueteForm extends MidasBaseForm{
	private static final long serialVersionUID = 1385601343972900841L;

	private String tipoRespuesta;
	private String idOriginal;
	private String idResultado;
	private String tipoMensaje;
	private String operacionExitosa;
	private String mensaje;
	private String idToTipoPoliza;
	
	public String getTipoRespuesta() {
		return tipoRespuesta;
	}
	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
	public String getIdOriginal() {
		return idOriginal;
	}
	public void setIdOriginal(String idOriginal) {
		this.idOriginal = idOriginal;
	}
	public String getIdResultado() {
		return idResultado;
	}
	public void setIdResultado(String idResultado) {
		this.idResultado = idResultado;
	}
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public String getOperacionExitosa() {
		return operacionExitosa;
	}
	public void setOperacionExitosa(String operacionExitosa) {
		this.operacionExitosa = operacionExitosa;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getIdToTipoPoliza() {
		return idToTipoPoliza;
	}
	public void setIdToTipoPoliza(String idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}
	
	
}
