package mx.com.afirme.midas.producto.configuracion.aumento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AumentoVarioProductoSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private AumentoVarioProductoFacadeRemote beanRemoto;

	public AumentoVarioProductoSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(AumentoVarioProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(AumentoVarioProductoDTO aumentoVarioProductoDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.save(aumentoVarioProductoDTO);
		return null;
	}
	
	public String actualizar(AumentoVarioProductoDTO aumentoVarioProductoDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.update(aumentoVarioProductoDTO);
		return null;
	}
	
	public String borrar(AumentoVarioProductoDTO aumentoVarioProductoDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.delete(aumentoVarioProductoDTO);
		return null;
	}

	public List<AumentoVarioProductoDTO> listarAumentoAsociado(BigDecimal idToProducto) {
		return beanRemoto.findByProperty("id.idtoproducto", idToProducto);
	}
}
