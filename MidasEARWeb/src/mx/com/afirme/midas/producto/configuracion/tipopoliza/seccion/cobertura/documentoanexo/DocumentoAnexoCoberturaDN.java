package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class DocumentoAnexoCoberturaDN {
	private static final DocumentoAnexoCoberturaDN INSTANCIA = new DocumentoAnexoCoberturaDN();

	public static DocumentoAnexoCoberturaDN getInstancia() {
		return DocumentoAnexoCoberturaDN.INSTANCIA;
	}

	public List<DocumentoAnexoCoberturaDTO> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCoberturaSN().listarTodos();
	}

	public void agregar(DocumentoAnexoCoberturaDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		new DocumentoAnexoCoberturaSN().agregar(documentoAnexoproductoDTO);
	}

	public void modificar(DocumentoAnexoCoberturaDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoCoberturaSN().modificar(documentoAnexoproductoDTO);
	}

	public DocumentoAnexoCoberturaDTO getPorId(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCoberturaSN().getPorId(id);
	}

	public void borrar(DocumentoAnexoCoberturaDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoCoberturaSN().borrar(documentoAnexoproductoDTO);
	}
	
	public List<DocumentoAnexoCoberturaDTO> listarPorPropiedad(String propiedad, Object valor) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCoberturaSN().encontrarPorPropiedad(propiedad, valor);
	}

	public List<DocumentoAnexoCoberturaDTO> listarAnexosPorCotizacion(
			BigDecimal idToCotizacion) throws SystemException {
		return new DocumentoAnexoCoberturaSN().listarAnexosPorCotizacion(idToCotizacion);
	}
}
