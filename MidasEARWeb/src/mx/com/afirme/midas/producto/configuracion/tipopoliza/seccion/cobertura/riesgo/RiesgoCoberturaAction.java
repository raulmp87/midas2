package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TreeLoader;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RiesgoCoberturaAction extends CoberturaAction{
	/**
	 * Method mostrarAsociarRiesgo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarRiesgo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(request.getParameter("id"));
		//request.getSession().setAttribute("idToCobertura", request.getParameter("id"));
		
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRiesgosAsociados
	 * 
	 * M�todo que recupera la lista de riesgos asociados a una cobertura
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRiesgosAsociados(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();

		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		coberturaForm.setRiesgoCoberturaAsociados(coberturaDN.listarRiesgosVigentesAsociados(coberturaDTO,UtileriasWeb.regresaBigDecimal(idSeccion)));
		coberturaForm.setIdToSeccion(idSeccion);
//		String json = "{rows:[";
//		if(coberturaForm.getRiesgoCoberturaAsociados() != null && coberturaForm.getRiesgoCoberturaAsociados().size() > 0) {
//			for(RiesgoCoberturaDTO riesgo : coberturaForm.getRiesgoCoberturaAsociados()) {
//				json += "{id:\"" + riesgo.getId().getIdtoriesgo()+"|"+id+"\",data:[";
//				json += idSeccion+",\"";
//				json += riesgo.getRiesgoDTO().getCodigo() + "\",\"";
//				json += riesgo.getRiesgoDTO().getDescripcion() + "\",\"";
//				//json += riesgo.getClaveObligatoriedad() + "\",\"";
//				//json += riesgo.getDescripcionRiesgoCobertura() + "\",\"";
//				json += "/MidasWeb/img/Edit14.gif^Editar Relaci�n^javascript:mostrarAdjuntarDocumentoCNSFCoberturaWindow("+
//					riesgo.getId().getIdtoriesgo()+","+id+","+idSeccion+");^_self\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getRiesgoCoberturaAsociados() != null && coberturaForm.getRiesgoCoberturaAsociados().size() > 0) {
			for(RiesgoCoberturaDTO riesgo : coberturaForm.getRiesgoCoberturaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(riesgo.getId().getIdtoriesgo()+"|"+id);
				row.setDatos(
						idSeccion,
						riesgo.getRiesgoDTO().getCodigo(),
						riesgo.getRiesgoDTO().getNombreComercial(),
						riesgo.getRiesgoDTO().getDescripcion(),
						"/MidasWeb/img/Edit14.gif^Editar Relaci�n^javascript:mostrarEditarRiesgoCoberturaWindow("+
						riesgo.getId().getIdtoriesgo()+","+id+","+idSeccion+");^_self"
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Riesgos asociados: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarRiesgoAsociado
	 * @param mapping, form, request, response
	 * @return ActionForward
	 * @throws SystemException @throws IOException 
	 */
	public ActionForward guardarRiesgoAsociado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaSeccionDN coberturaSeccionDN = CoberturaSeccionDN.getInstancia();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		String action = "";
		RiesgoCoberturaId id = new RiesgoCoberturaId();
		String ids[] = request.getParameter("gr_id").toString().split("\\|");
		
		id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(ids[1]));
		
		id.setIdtoseccion(UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre").toString()));
		id.setIdtoriesgo(UtileriasWeb.regresaBigDecimal(ids[0]));
		
		try {
			RiesgoCoberturaDTO riesgoCobertura = new RiesgoCoberturaDTO();
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				riesgoCobertura.setId(id);
				//riesgoCobertura.setClaveObligatoriedad(UtileriasWeb.regresaShort(request.getParameter("obligatorio")));
				riesgoCobertura.setDescripcionRiesgoCobertura(request.getParameter("descripcionRiesgoCobertura"));

				RiesgoDTO riesgoDTO = new RiesgoDTO();
				riesgoDTO.setIdToRiesgo(riesgoCobertura.getId().getIdtoriesgo());
				riesgoDTO = riesgoDN.getPorId(riesgoDTO);
				
				CoberturaSeccionDTO coberturaSeccion = new CoberturaSeccionDTO();
				CoberturaSeccionDTOId idCS = new CoberturaSeccionDTOId();
				idCS.setIdtocobertura(id.getIdtocobertura());
				idCS.setIdtoseccion(id.getIdtoseccion());
				coberturaSeccion.setId(idCS);
				coberturaSeccion = coberturaSeccionDN.getPorId(coberturaSeccion);

				riesgoCobertura.setRiesgoDTO(riesgoDTO);
				riesgoCobertura.setCoberturaSeccionDTO(coberturaSeccion);
				
				CoberturaDTO cobertura = coberturaSeccion.getCoberturaDTO();
				if(cobertura == null)
					cobertura = CoberturaDN.getInstancia().getPorId(coberturaSeccion.getId().getIdtocobertura());
				riesgoDTO = riesgoCobertura.getRiesgoDTO();
				if (riesgoDTO == null)
					riesgoDTO = RiesgoDN.getInstancia().getPorId(riesgoCobertura.getId().getIdtoriesgo());
				
				if(request.getParameter("!nativeeditor_status").equals("inserted")){
					//Se asigna la clave obligatoriedad 0 - "Obligatorio", este valor se usa temporalmente ya que falta por definir el dafault. 
					riesgoCobertura.setClaveObligatoriedad((short)0);
					//Variable que se hereda del riesgo:
					riesgoCobertura.setDescripcionRiesgoCobertura(riesgoDTO.getDescripcion());
					//Valores que se deben heredar de la cobertura:
					riesgoCobertura.setClaveTipoCoaseguro(UtileriasWeb.regresaShort(cobertura.getClaveTipoCoaseguro()));
					riesgoCobertura.setValorMinimoCoaseguro(cobertura.getValorMinimoCoaseguro());
					riesgoCobertura.setValorMaximoCoaseguro(cobertura.getValorMaximoCoaseguro());
					riesgoCobertura.setClaveTipoLimiteCoaseguro(UtileriasWeb.regresaShort(cobertura.getClaveTipoLimiteCoaseguro()));
					riesgoCobertura.setValorMinimoLimiteCoaseguro(cobertura.getValorMinimoLimiteCoaseguro());
					riesgoCobertura.setValorMaximoLimiteCoaseguro(cobertura.getValorMaximoLimiteCoaseguro());
					riesgoCobertura.setClaveTipoDeducible(UtileriasWeb.regresaShort(cobertura.getClaveTipoDeducible()));
					riesgoCobertura.setValorMinimoLimiteDeducible(cobertura.getValorMinimoLimiteDeducible());
					riesgoCobertura.setValorMaximoLimiteDeducible(cobertura.getValorMaximoLimiteDeducible());
					riesgoCobertura.setClaveTipoSumaAsegurada(UtileriasWeb.regresaShort(cobertura.getClaveTipoSumaAsegurada()));
					riesgoCobertura.setClaveAutorizacion(UtileriasWeb.regresaShort(cobertura.getClaveAutorizacion()));
					riesgoCobertura.setClaveImporteCero(UtileriasWeb.regresaShort(cobertura.getClaveImporteCero()));
					riesgoCobertura.setClaveTipoLimiteDeducible(UtileriasWeb.regresaShort(cobertura.getClaveTipoLimiteDeducible()));
					riesgoCobertura.setValorMinimoDeducible(cobertura.getValorMinimoDeducible());
					riesgoCobertura.setValorMaximoDeducible(cobertura.getValorMaximoDeducible());
					
					coberturaDN.asociarRiesgo(riesgoCobertura);
					action = "insert";
				}
				else{ 
					coberturaDN.actualizarAsociacionRiesgo(riesgoCobertura);
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				riesgoCobertura = new RiesgoCoberturaSN().getPorId(id);
				coberturaDN.desasociarRiesgo(riesgoCobertura);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRiesgosPorAsociar
	 * 
	 * M�todo que recupera la lista de riesgos no asociadas a una cobertura
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRiesgosPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id").toString();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		String idSeccion = request.getParameter("idPadre");
		BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(idSeccion);
		coberturaDTO = coberturaDN.getPorId(coberturaDTO);
		coberturaForm.setRiesgosPorAsociar(coberturaDN.listarRiesgoPorAsociar(coberturaDTO,idToSeccion));
		//String idSeccion = request.getSession().getAttribute("idToSeccion").toString();
		coberturaForm.setIdToSeccion(idSeccion);
//		String json = "{rows:[";
//		if(coberturaForm.getRiesgosPorAsociar() != null && coberturaForm.getRiesgosPorAsociar().size() > 0) {
//			for(RiesgoDTO riesgo : coberturaForm.getRiesgosPorAsociar()) {
//				json += "{id:\"" + riesgo.getIdToRiesgo()+"|"+id+"\",data:[";
//				json += idSeccion +",\"";
//				json += riesgo.getCodigo() + "\",\"";
//				//json += riesgo.getDescripcion() + "\",\"0\",\"\"]},";
//				json += riesgo.getDescripcion() + "\",\"";
//				json += "/MidasWeb/img/Edit14.gif^Editar Relaci�n^javascript:mostrarAdjuntarDocumentoCNSFCoberturaWindow("+riesgo.getIdToRiesgo()+","+id+","+idSeccion+");^_self\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getRiesgosPorAsociar() != null && coberturaForm.getRiesgosPorAsociar().size() > 0) {
			for(RiesgoDTO riesgo : coberturaForm.getRiesgosPorAsociar()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(riesgo.getIdToRiesgo()+"|"+id);
				row.setDatos(
						idSeccion,
						riesgo.getCodigo(),
						riesgo.getNombreComercial(),
						riesgo.getDescripcion(),
						"/MidasWeb/img/Edit14.gif^Editar Relaci�n^javascript:mostrarEditarRiesgoCoberturaWindow("+
						riesgo.getIdToRiesgo()+","+id+","+idSeccion+");^_self"
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Riesgos no asociados: "+json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public ActionForward mostrarEditarRelacionRiesgoCobertura(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoCoberturaForm rcForm = (RiesgoCoberturaForm) form;
		String idToRiesgo = request.getParameter("idToRiesgo");
		String idToCobertura = request.getParameter("idToCobertura");
		String idToSeccion = request.getParameter("idToSeccion");
		rcForm.setIdtoriesgo(idToRiesgo);
		rcForm.setIdtocobertura(idToCobertura);
		rcForm.setIdtoseccion(idToSeccion);
		RiesgoCoberturaDTO rcDTO = new RiesgoCoberturaDTO();
		RiesgoCoberturaId id = new RiesgoCoberturaId();
		try {
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(idToCobertura));
			id.setIdtoriesgo(UtileriasWeb.regresaBigDecimal(idToRiesgo));
			id.setIdtoseccion(UtileriasWeb.regresaBigDecimal(idToSeccion));
			rcDTO.setId(id);
			rcDTO = RiesgoCoberturaDN.getInstancia().getPorId(rcDTO);
			this.poblarForm(rcForm, rcDTO);
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void editarRelacionRiesgoCobertura(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		RiesgoCoberturaForm rcForm = (RiesgoCoberturaForm) form;
		RiesgoCoberturaDTO rcDTO = new RiesgoCoberturaDTO();
		RiesgoCoberturaId id = new RiesgoCoberturaId();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<table id='desplegar'>");
		try {
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(rcForm.getIdtocobertura()));
			id.setIdtoriesgo(UtileriasWeb.regresaBigDecimal(rcForm.getIdtoriesgo()));
			id.setIdtoseccion(UtileriasWeb.regresaBigDecimal(rcForm.getIdtoseccion()));
			rcDTO.setId(id);
			rcDTO = RiesgoCoberturaDN.getInstancia().getPorId(rcDTO);
			this.poblarDTO(rcDTO,rcForm);
			RiesgoCoberturaDN.getInstancia().modificar(rcDTO);
			buffer.append("<tr><td class='titulo'>La relaci�n se actualiz� correctamente.</td></tr>");
		} catch (SystemException e) {
			buffer.append("<tr><td class='titulo'>Ocurri� un error al actualizar la relaci�n.\nVerifique los datos.</td></tr>");
			e.printStackTrace();
		}
		buffer.append("</table>");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method listarPorPradre
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException
	 */
	public void listarPorPradre(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException {

		int numHijos =0;
		String itemId = "";
		if (request.getParameter("id") != null) {
			itemId = request.getParameter("id");
		}

		String id = "";
		if (request.getAttribute("id") != null) {
			id = (String) request.getAttribute("id");
		}

		String contextoMenu = "";
		if (request.getParameter("menu") != null) {
			contextoMenu = request.getParameter("menu");
		}
		TreeLoader tree = new TreeLoader(contextoMenu, mapping, form, request, response);
		//RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();

		try {
			String idSeccion = itemId.substring(itemId.lastIndexOf("(")+1, itemId.lastIndexOf(")"));
			List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(UtileriasWeb.regresaBigDecimal(id), UtileriasWeb.regresaBigDecimal(idSeccion)); 
				//riesgoCoberturaDN.buscarPorPropiedad("coberturaSeccionDTO.id.idtocobertura", BigDecimal.valueOf(Double.valueOf(id)));
			
			StringBuffer buffer = new StringBuffer();
			Iterator<RiesgoCoberturaDTO> iteratorList = riesgos.iterator();
			while (iteratorList.hasNext()) {
				RiesgoCoberturaDTO riesgoCoberturaDTO = iteratorList.next();
				//riesgoCoberturaDTO = riesgoCoberturaDN.getPorId(riesgoCoberturaDTO);
				numHijos =0;
				buffer = tree.escribirItem(buffer, riesgoCoberturaDTO.getRiesgoDTO().getNombreComercial(),
						riesgoCoberturaDTO.getId().getIdtoriesgo().toString(), id+"a"+riesgoCoberturaDTO.getId().getIdtoseccion(), numHijos);
								
			}
			buffer = tree.xmlHeader(buffer, itemId);
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (SystemException e) {
			throw new SystemException(e);
		} catch (ExcepcionDeAccesoADatos e) {
			throw new SystemException(e);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render riesgos List",
					ioException);
		} // End of try/catch
	}
	
	private void poblarForm(RiesgoCoberturaForm rcForm, RiesgoCoberturaDTO rcDTO){
		if (rcForm != null && rcDTO != null){
			if (rcDTO.getId() != null){
				if(rcDTO.getId().getIdtocobertura() != null){
					rcForm.setIdtocobertura(rcDTO.getId().getIdtocobertura().toString());
					try { rcForm.setCoberturaDTO(CoberturaDN.getInstancia().getPorId(rcDTO.getId().getIdtocobertura()));
					} catch (ExcepcionDeAccesoADatos e) { e.printStackTrace();
					} catch (SystemException e) { e.printStackTrace(); }
				}
				if (rcDTO.getId().getIdtoriesgo() != null){
					rcForm.setIdtoriesgo(rcDTO.getId().getIdtoriesgo().toString());
					try { rcForm.setRiesgoDTO(RiesgoDN.getInstancia().getPorId(rcDTO.getId().getIdtoriesgo()));
					} catch (ExcepcionDeAccesoADatos e) { e.printStackTrace();
					} catch (SystemException e) { e.printStackTrace(); }
				}
				if (rcDTO.getId().getIdtoseccion() != null){
					rcForm.setIdtoseccion(rcDTO.getId().getIdtoseccion().toString());
					try { rcForm.setSeccionDTO(SeccionDN.getInstancia().getPorId(rcDTO.getId().getIdtoseccion()));
					} catch (ExcepcionDeAccesoADatos e) { e.printStackTrace();
					} catch (SystemException e) { e.printStackTrace(); }
				}
			}
			if (rcDTO.getDescripcionRiesgoCobertura() != null)
				rcForm.setDescripcionRiesgoCobertura(rcDTO.getDescripcionRiesgoCobertura());
			if (rcDTO.getValorMaximoLimiteCoaseguro() != null)
				rcForm.setValorMaximoLimiteCoaseguro(rcDTO.getValorMaximoLimiteCoaseguro().toString());
			if (rcDTO.getValorMinimoLimiteCoaseguro() != null)
				rcForm.setValorMinimoLimiteCoaseguro(rcDTO.getValorMinimoLimiteCoaseguro().toString());
			if (rcDTO.getClaveTipoLimiteCoaseguro() != null)
				rcForm.setClaveTipoLimiteCoaseguro(rcDTO.getClaveTipoLimiteCoaseguro().toString());
			if (rcDTO.getValorMaximoCoaseguro() != null)
				rcForm.setValorMaximoCoaseguro(rcDTO.getValorMaximoCoaseguro().toString());
			if (rcDTO.getValorMinimoCoaseguro() != null)
				rcForm.setValorMinimoCoaseguro(rcDTO.getValorMinimoCoaseguro().toString());
			if (rcDTO.getClaveObligatoriedad() != null)
				rcForm.setClaveObligatoriedad(rcDTO.getClaveObligatoriedad().toString());
			if (rcDTO.getClaveTipoCoaseguro() != null)
				rcForm.setClaveTipoCoaseguro(rcDTO.getClaveTipoCoaseguro().toString());
			if (rcDTO.getValorMaximoLimiteDeducible() != null)
				rcForm.setValorMaximoLimiteDeducible(rcDTO.getValorMaximoLimiteDeducible().toString());
			if (rcDTO.getValorMinimoLimiteDeducible() != null)
				rcForm.setValorMinimoLimiteDeducible(rcDTO.getValorMinimoLimiteDeducible().toString());
			if (rcDTO.getClaveTipoLimiteDeducible() != null)
				rcForm.setClaveTipoLimiteDeducible(rcDTO.getClaveTipoLimiteDeducible().toString());
			if (rcDTO.getValorMaximoDeducible() != null)
				rcForm.setValorMaximoDeducible(rcDTO.getValorMaximoDeducible().toString());
			if (rcDTO.getValorMinimoDeducible() != null)
				rcForm.setValorMinimoDeducible(rcDTO.getValorMinimoDeducible().toString());
			if (rcDTO.getClaveTipoDeducible() != null)
				rcForm.setClaveTipoDeducible(rcDTO.getClaveTipoDeducible().toString());
			if (rcDTO.getClaveTipoSumaAsegurada() != null)
				rcForm.setClaveTipoSumaAsegurada(rcDTO.getClaveTipoSumaAsegurada().toString());
			//Valores provenientes de un check
			rcForm.setClaveAutorizacion(UtileriasWeb.calculaValorStringDelCheck(rcDTO.getClaveAutorizacion().toString()));
			rcForm.setClaveImporteCero(UtileriasWeb.calculaValorStringDelCheck(rcDTO.getClaveImporteCero().toString()));
		}
	}
	
	private void poblarDTO(RiesgoCoberturaDTO rcDTO,RiesgoCoberturaForm rcForm){
		if (rcForm != null && rcDTO != null){
			if (rcForm.getDescripcionRiesgoCobertura() != null)
				rcDTO.setDescripcionRiesgoCobertura(rcForm.getDescripcionRiesgoCobertura());
			if (rcForm.getValorMaximoLimiteCoaseguro() != null)
				rcDTO.setValorMaximoLimiteCoaseguro(UtileriasWeb.regresaDouble((rcForm.getValorMaximoLimiteCoaseguro())));
			if (rcForm.getValorMinimoLimiteCoaseguro() != null)
				rcDTO.setValorMinimoLimiteCoaseguro(UtileriasWeb.regresaDouble(rcForm.getValorMinimoLimiteCoaseguro()));
			if (rcForm.getClaveTipoLimiteCoaseguro() != null)
				rcDTO.setClaveTipoLimiteCoaseguro(UtileriasWeb.regresaShort(rcForm.getClaveTipoLimiteCoaseguro()));
			if (rcForm.getValorMaximoCoaseguro() != null)
				rcDTO.setValorMaximoCoaseguro(UtileriasWeb.regresaDouble(rcForm.getValorMaximoCoaseguro()));
			if (rcForm.getValorMinimoCoaseguro() != null)
				rcDTO.setValorMinimoCoaseguro(UtileriasWeb.regresaDouble(rcForm.getValorMinimoCoaseguro()));
			if (rcForm.getClaveObligatoriedad() != null)
				rcDTO.setClaveObligatoriedad(UtileriasWeb.regresaShort(rcForm.getClaveObligatoriedad()));
			if (rcForm.getClaveTipoCoaseguro() != null)
				rcDTO.setClaveTipoCoaseguro(UtileriasWeb.regresaShort(rcForm.getClaveTipoCoaseguro()));
			if (rcForm.getValorMaximoLimiteDeducible() != null)
				rcDTO.setValorMaximoLimiteDeducible(UtileriasWeb.regresaDouble(rcForm.getValorMaximoLimiteDeducible()));
			if (rcForm.getValorMinimoLimiteDeducible() != null)
				rcDTO.setValorMinimoLimiteDeducible(UtileriasWeb.regresaDouble(rcForm.getValorMinimoLimiteDeducible()));
			if (rcForm.getClaveTipoLimiteDeducible() != null)
				rcDTO.setClaveTipoLimiteDeducible(UtileriasWeb.regresaShort(rcForm.getClaveTipoLimiteDeducible()));
			if (rcForm.getValorMaximoDeducible() != null)
				rcDTO.setValorMaximoDeducible(UtileriasWeb.regresaDouble(rcForm.getValorMaximoDeducible()));
			if (rcForm.getValorMinimoDeducible() != null)
				rcDTO.setValorMinimoDeducible(UtileriasWeb.regresaDouble(rcForm.getValorMinimoDeducible()));
			if (rcForm.getClaveTipoDeducible() != null)
				rcDTO.setClaveTipoDeducible(UtileriasWeb.regresaShort(rcForm.getClaveTipoDeducible()));
			if (rcForm.getClaveTipoSumaAsegurada() != null)
				rcDTO.setClaveTipoSumaAsegurada(UtileriasWeb.regresaShort(rcForm.getClaveTipoSumaAsegurada()));
			//Valores provenientes de un check
			rcDTO.setClaveAutorizacion(new Short(UtileriasWeb.calculaValorEnteroDelCheck(rcForm.getClaveAutorizacion())));
			rcDTO.setClaveImporteCero(new Short(UtileriasWeb.calculaValorEnteroDelCheck(rcForm.getClaveImporteCero())));
		}
	}
}
