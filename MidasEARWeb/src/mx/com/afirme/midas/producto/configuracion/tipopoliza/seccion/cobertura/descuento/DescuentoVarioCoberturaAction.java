package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * @author Fernando Alonzo
 * @since 17 de agosto de 2009
 */
public class DescuentoVarioCoberturaAction extends CoberturaAction {

	/**
	 * Method asociarDescuento
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward asociarDescuento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarDescuentoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarDescuentoAsociado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		DescuentoDN descuentoDN = DescuentoDN.getInstancia();
		String action = "";
		DescuentoVarioCoberturaId id = new DescuentoVarioCoberturaId();
		BigDecimal idToDescuentoVario = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre"));
		
		id.setIdtodescuentovario(idToDescuentoVario);
		id.setIdtocobertura(idToCobertura);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				DescuentoVarioCoberturaDTO descuentoPorCobertura = new DescuentoVarioCoberturaDTO();
				descuentoPorCobertura.setId(id);
				descuentoPorCobertura.setClaveaplicareaseguro(Short.parseShort(request.getParameter("aplicaReaseguro")));
				descuentoPorCobertura.setClavecomercialtecnico(Short.parseShort(request.getParameter("comercialTecnico")));
				descuentoPorCobertura.setClaveobligatoriedad(Short.parseShort(request.getParameter("obligatorio").equals("1")? "3" : "0"));
				descuentoPorCobertura.setValor(Double.parseDouble(request.getParameter("valor")));

				DescuentoDTO descuentoDTO = new DescuentoDTO();
				descuentoDTO.setIdToDescuentoVario(descuentoPorCobertura.getId().getIdtodescuentovario());
				descuentoDTO = descuentoDN.getPorId(descuentoDTO);

				CoberturaDTO cobertura = new  CoberturaDTO();
				cobertura.setIdToCobertura(id.getIdtocobertura());
				cobertura = coberturaDN.getPorId(cobertura);
				descuentoPorCobertura.setCoberturaDTO(cobertura);
				descuentoPorCobertura.setDescuentoDTO(descuentoDTO);
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					action = "insert";
					coberturaDN.asociarDescuento(coberturaDTO, descuentoPorCobertura);
					mensajeExitoAgregar(coberturaForm, request);
				} else {
					action = "update";
					coberturaDN.actualizarAsociacion(coberturaDTO, descuentoPorCobertura);
					mensajeExitoModificar(coberturaForm, request);
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				action = "deleted";
				DescuentoVarioCoberturaDTO descuentoPorCobertura = new DescuentoVarioCoberturaDTO();
				descuentoPorCobertura.setId(id);
				coberturaDN.desasociarDescuento(coberturaDTO, descuentoPorCobertura);
				mensajeExitoBorrar(coberturaForm, request);
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(coberturaForm, request, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mensajeExcepcion(coberturaForm, request, e);
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarDescuentosAsociados(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		coberturaForm.setDescuentosAsociados(coberturaDN.getPorIdCascada(coberturaDTO).getDescuentos());
//		String json = "{rows:[";
//		if(coberturaForm.getDescuentosAsociados() != null && coberturaForm.getDescuentosAsociados().size() > 0) {
//			for(DescuentoVarioCoberturaDTO descuento : coberturaForm.getDescuentosAsociados()) {
//				json += "{id:\"" + descuento.getId().getIdtodescuentovario() + "\",data:[";
//				json += id + ",\"";
//				json += descuento.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += descuento.getClaveobligatoriedad() + "\",";
//				json += descuento.getClavecomercialtecnico() + ",";
//				json += descuento.getClaveaplicareaseguro() + ",\"";
//				json += (descuento.getDescuentoDTO().getClaveTipo().intValue() == 1 ? "%":"$") + "\",";				
//				json += descuento.getValor() + "]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getDescuentosAsociados() != null && coberturaForm.getDescuentosAsociados().size() > 0) {
			for(DescuentoVarioCoberturaDTO descuento : coberturaForm.getDescuentosAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(descuento.getId().getIdtodescuentovario().toString());
				row.setDatos(
						id,
						descuento.getDescuentoDTO().getDescripcion(),
						descuento.getClaveobligatoriedad().toString(),
						descuento.getClavecomercialtecnico().toString(),
						descuento.getClaveaplicareaseguro().toString(),
						(descuento.getDescuentoDTO().getClaveTipo().intValue() == 1 ? "%":"$"),
						descuento.getValor().toString()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarDescuentosPorAsociar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		
		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		coberturaForm.setDescuentosPorAsociar(coberturaDN.listarDescuentoPorAsociar(coberturaDTO));
//		String json = "{rows:[";
//		if(coberturaForm.getDescuentosPorAsociar() != null && coberturaForm.getDescuentosPorAsociar().size() > 0) {
//			for(DescuentoDTO descuento : coberturaForm.getDescuentosPorAsociar()) {
//				json += "{id:\"" + descuento.getIdToDescuentoVario() + "\",data:[";
//				json += id + ",\""+descuento.getDescripcion() + "\"";
//				json += ",0,1,0,\"" + (descuento.getClaveTipo().intValue() == 1 ? "%":"$") + "\",0]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getDescuentosPorAsociar() != null && coberturaForm.getDescuentosPorAsociar().size() > 0) {
			for(DescuentoDTO descuento : coberturaForm.getDescuentosPorAsociar()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(descuento.getIdToDescuentoVario().toString());
				row.setDatos(
						id,
						descuento.getDescripcion(),
						"0",
						"1",
						"0",
						(descuento.getClaveTipo().intValue() == 1 ? "%":"$"),
						"0"
				);
				json.addRow(row);
				
			}
		}		
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
