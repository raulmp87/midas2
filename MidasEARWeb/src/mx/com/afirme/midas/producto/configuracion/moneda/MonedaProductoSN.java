package mx.com.afirme.midas.producto.configuracion.moneda;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MonedaProductoSN {
	private MonedaProductoFacadeRemote beanRemoto;

	public MonedaProductoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en MonedaProductoSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(MonedaProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<MonedaProductoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(MonedaProductoDTO monedaProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(monedaProductoDTO);
	}

	public void modificar(MonedaProductoDTO monedaProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(monedaProductoDTO);
	}

	public MonedaProductoDTO getPorId(MonedaProductoId id)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(MonedaProductoDTO monedaProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(monedaProductoDTO);
	}

	public List<MonedaProductoDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
}
