package mx.com.afirme.midas.producto.configuracion.aumento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ExclusionAumentoVarioProductoSN {
	
	private ExclusionAumentoVarioProductoFacadeRemote beanRemoto;

	public ExclusionAumentoVarioProductoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionAumentoVarioProductoSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionAumentoVarioProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionAumentoVarioProductoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionAumentoVarioProductoDTO excAumentoPorProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excAumentoPorProductoDTO);
	}

	public void modificar(ExclusionAumentoVarioProductoDTO excAumentoPorProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excAumentoPorProductoDTO);
	}

	public ExclusionAumentoVarioProductoDTO getPorId(ExclusionAumentoVarioProductoId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionAumentoVarioProductoDTO excAumentoPorProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excAumentoPorProductoDTO);
	}

	public List<ExclusionAumentoVarioProductoDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<ExclusionAumentoVarioProductoDTO> buscarVigentesPorIdProducto(BigDecimal idToProducto){
		return beanRemoto.getVigentesPorIdProducto(idToProducto);
	}
}
