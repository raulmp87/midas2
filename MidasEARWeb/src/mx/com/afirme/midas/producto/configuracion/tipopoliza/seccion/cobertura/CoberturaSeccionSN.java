package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class CoberturaSeccionSN {
	private CoberturaSeccionFacadeRemote beanRemoto;

	public CoberturaSeccionSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en CoberturaSeccionSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CoberturaSeccionFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CoberturaSeccionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<CoberturaSeccionDTO> list = beanRemoto.findAll();
		return list;
	}

	public void agregar(CoberturaSeccionDTO coberturaSeccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(coberturaSeccionDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}

	public void modificar(CoberturaSeccionDTO coberturaSeccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(coberturaSeccionDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}

	public CoberturaSeccionDTO getPorId(CoberturaSeccionDTOId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);

	}

	public void borrar(CoberturaSeccionDTO coberturaSeccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(coberturaSeccionDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}

	public List<CoberturaSeccionDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<CoberturaSeccionDTO> listarCoberturasPorAsociar(BigDecimal idToSeccion)  throws SystemException, ExcepcionDeAccesoADatos {
		return beanRemoto.obtenerCoberturasSinAsociar(idToSeccion);
	}
	
	public List<CoberturaSeccionDTO> listarVigentesPorSeccion(BigDecimal idToSeccion)  throws SystemException, ExcepcionDeAccesoADatos {
		return beanRemoto.getVigentesPorSeccion(idToSeccion);
	}

	public Boolean isCobeturaAsociada(BigDecimal idToCobertura) {
		Long count = beanRemoto.countCobeturaSeccion(idToCobertura);
		return count > 0;
	}
}
