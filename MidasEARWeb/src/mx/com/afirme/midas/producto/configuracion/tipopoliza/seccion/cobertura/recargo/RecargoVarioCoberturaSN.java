package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RecargoVarioCoberturaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private RecargoVarioCoberturaFacadeRemote beanRemoto;

	public RecargoVarioCoberturaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(RecargoVarioCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(RecargoVarioCoberturaDTO recargoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(recargoPorCoberturaDTO);
		return null;
	}

	public String actualizar(RecargoVarioCoberturaDTO recargoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(recargoPorCoberturaDTO);
		return null;
	}

	public String borrar(RecargoVarioCoberturaDTO recargoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(recargoPorCoberturaDTO);
		return null;
	}

	public List<RecargoVarioCoberturaDTO> listarRecargoAsociado(
			BigDecimal idToCobertura) {
		return beanRemoto.findByProperty("id.idtocobertura", idToCobertura);
	}

	public List<RecargoVarioCoberturaDTO> listarRecargos(
			BigDecimal idToProducto) {
		return beanRemoto.findAll();
	}

	public List<RecargoVarioCoberturaDTO> buscarRecargoCobertura(BigDecimal idToCobertura,
			BigDecimal idToRecargoVario) {
		return beanRemoto.buscarRecargoCobertura(idToCobertura, idToRecargoVario);
	}
}
