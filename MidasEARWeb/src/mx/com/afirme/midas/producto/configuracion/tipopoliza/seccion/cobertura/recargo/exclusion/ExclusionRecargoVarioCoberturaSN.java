package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ExclusionRecargoVarioCoberturaSN {
	private ExclusionRecargoVarioCoberturaFacadeRemote beanRemoto;

	public ExclusionRecargoVarioCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExcRecargoPorCoberturaSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(ExclusionRecargoVarioCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionRecargoVarioCoberturaDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(
			ExclusionRecargoVarioCoberturaDTO excRecargoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excRecargoPorCoberturaDTO);
	}

	public void modificar(
			ExclusionRecargoVarioCoberturaDTO excRecargoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excRecargoPorCoberturaDTO);
	}

	public ExclusionRecargoVarioCoberturaDTO getPorId(
			ExclusionRecargoVarioCoberturaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionRecargoVarioCoberturaDTO excRecargoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excRecargoPorCoberturaDTO);
	}

	public List<ExclusionRecargoVarioCoberturaDTO> buscarPorPropiedad(
			String nombrePropiedad, Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}

	public List<ExclusionRecargoVarioCoberturaDTO> listarRecargosExcluidos(BigDecimal idToCobertura) {
		return beanRemoto.findByProperty("id.idtocobertura", idToCobertura);
	}
	
	public List<ExclusionRecargoVarioCoberturaDTO> buscarVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.getVigentesPorIdCoberturaIdSeccion(idToCobertura, idToSeccion);
	}
}
