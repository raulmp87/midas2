package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RecargoVarioTipoPolizaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private RecargoVarioTipoPolizaFacadeRemote beanRemoto;

	public RecargoVarioTipoPolizaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(RecargoVarioTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(RecargoVarioTipoPolizaDTO recargoPorTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(recargoPorTipoPolizaDTO);
		return null;
	}

	public String actualizar(RecargoVarioTipoPolizaDTO recargoPorTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(recargoPorTipoPolizaDTO);
		return null;
	}

	public String borrar(RecargoVarioTipoPolizaDTO recargoPorTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(recargoPorTipoPolizaDTO);
		return null;
	}

	public List<RecargoVarioTipoPolizaDTO> listarRecargoAsociado(
			BigDecimal idToTipoPoliza) {
		return beanRemoto.findByProperty("id.idtotipopoliza", idToTipoPoliza);
	}
}
