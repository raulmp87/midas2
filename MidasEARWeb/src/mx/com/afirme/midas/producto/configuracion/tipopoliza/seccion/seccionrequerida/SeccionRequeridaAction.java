package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 20 de agosto de 2009
 */
public class SeccionRequeridaAction extends SeccionAction{
	/**
	 * Method mostrarAsociarSeccion
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarSeccion(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SeccionForm seccionForm = (SeccionForm) form;
		String id = request.getParameter("id");
		seccionForm.setIdToSeccion(id);
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarSeccionesRequeridas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarSeccionesRequeridas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		SeccionForm seccionForm = (SeccionForm) form;
		String id = request.getParameter("id");
		SeccionDTO seccionDTO = new SeccionDTO();
		SeccionDN seccionDN =  SeccionDN.getInstancia();
		seccionDTO.setIdToSeccion(UtileriasWeb.regresaBigDecimal(id));
		//getPorIdCascada devuelve al objeto SeccionDTO ya con su tipoPoliza y las coberturas
		seccionDTO = seccionDN.getPorIdConTipoPoliza(seccionDTO);
		seccionForm.setSeccionesRequeridas(seccionDN.listarSeccionesRequeridas(seccionDTO));
//		String json = "{rows:[";
//		if(seccionForm.getSeccionesRequeridas()!= null && seccionForm.getSeccionesRequeridas().size() > 0) {
//			for(SeccionDTO actual : seccionForm.getSeccionesRequeridas()) {
//				json += "{id:\"" + actual.getIdToSeccion() + "\",data:[";
//				json += id + ",\"";
//				json += actual.getCodigo() + "\",\"";
//				json += actual.getNombreComercial() + "\",\"";
//				json += actual.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(seccionForm.getSeccionesRequeridas()!= null && seccionForm.getSeccionesRequeridas().size() > 0) {
			for(SeccionDTO actual : seccionForm.getSeccionesRequeridas()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getIdToSeccion().toString());
				row.setDatos(
						id,
						actual.getCodigo(),
						actual.getNombreComercial(),
						actual.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Secciones requeridas: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarSeccionRequerida
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarSeccionRequerida(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SeccionDN seccionDN = SeccionDN.getInstancia();
		SeccionRequeridaId id = new SeccionRequeridaId();
		String idSeccionBase = request.getParameter("gr_id");
		try {
			id.setIdToSeccion(UtileriasWeb.regresaBigDecimal(request.getParameter("idSeccionRequerida")));
			id.setIdSeccionRequerida(UtileriasWeb.regresaBigDecimal(idSeccionBase));
			
			SeccionRequeridaDTO seccionRequerida = new SeccionRequeridaDTO();
			seccionRequerida.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				SeccionDTO seccionBase = new SeccionDTO();
				seccionBase.setIdToSeccion(UtileriasWeb.regresaBigDecimal(idSeccionBase));
				seccionBase = seccionDN.getPorId(seccionBase);
				
				SeccionDTO seccionReq = new SeccionDTO();
				seccionReq.setIdToSeccion(id.getIdSeccionRequerida());
				seccionReq = seccionDN.getPorId(seccionReq);
				
				seccionRequerida.setSeccionBaseDTO(seccionBase);
				seccionRequerida.setSeccionRequeridaDTO(seccionReq);
				seccionDN.AsociarSeccionRequerida(seccionRequerida);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				seccionDN.DesasociarSeccionRequerida(seccionRequerida);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarSeccionesPorAsociar
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarSeccionesPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{		
		SeccionForm seccionForm = (SeccionForm) form;
		String id = request.getParameter("id");
		SeccionDTO seccionDTO = new SeccionDTO();
		SeccionDN seccionDN =  SeccionDN.getInstancia();
		seccionDTO.setIdToSeccion(UtileriasWeb.regresaBigDecimal(id));
		//getPorIdCascada devuelve al objeto SeccionDTO ya con su tipoPoliza y las coberturas
		seccionDTO = seccionDN.getPorIdConTipoPoliza(seccionDTO);
		seccionForm.setSeccionesNoRequeridas(seccionDN.listarSeccionesNoRequeridas(seccionDTO));
//		String json = "{rows:[";
//		if(seccionForm.getSeccionesNoRequeridas()!= null && seccionForm.getSeccionesNoRequeridas().size() > 0) {
//			for(SeccionDTO actual : seccionForm.getSeccionesNoRequeridas()) {
//				json += "{id:\"" + actual.getIdToSeccion() + "\",data:[";
//				json += id + ",\"";
//				json += actual.getCodigo() + "\",\"";
//				json += actual.getNombreComercial() + "\",\"";
//				json += actual.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(seccionForm.getSeccionesNoRequeridas()!= null && seccionForm.getSeccionesNoRequeridas().size() > 0) {
			for(SeccionDTO actual : seccionForm.getSeccionesNoRequeridas()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getIdToSeccion().toString());
				row.setDatos(
						id,
						actual.getCodigo(),
						actual.getNombreComercial(),
						actual.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Secciones no requeridas: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
