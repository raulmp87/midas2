package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Jos� Luis Arellano
 * @since 14 de Agosto de 2009
 */
public class ExclusionRecargoVarioTipoPolizaSN {
	private ExclusionRecargoVarioTipoPolizaFacadeRemote beanRemoto;

	public ExclusionRecargoVarioTipoPolizaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionRecargoVarioTipoPolizaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionRecargoVarioTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionRecargoVarioTipoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionRecargoVarioTipoPolizaDTO excRecargoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excRecargoPorTipoPolizaDTO);
	}

	public void modificar(ExclusionRecargoVarioTipoPolizaDTO excRecargoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excRecargoPorTipoPolizaDTO);
	}

	public ExclusionRecargoVarioTipoPolizaDTO getPorId(ExclusionRecargoVarioTipoPolizaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionRecargoVarioTipoPolizaDTO excRecargoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excRecargoPorTipoPolizaDTO);
	}

	public List<ExclusionRecargoVarioTipoPolizaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public ExclusionRecargoVarioTipoPolizaDTO buscarPorIDs(BigDecimal idToTipoPoliza, BigDecimal idToRecargoVario, BigDecimal idToCobertura){
		List<ExclusionRecargoVarioTipoPolizaDTO> lista = beanRemoto.findByIDs(idToTipoPoliza, idToRecargoVario, idToCobertura);
		ExclusionRecargoVarioTipoPolizaDTO registro;
		if (lista == null || lista.isEmpty())
			registro=null;
		else
			registro = lista.get(0); 
		return registro;
	}
	
	public List<ExclusionRecargoVarioTipoPolizaDTO> buscarVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza){
		return beanRemoto.getVigentesPorIdTipoPoliza(idToTipoPoliza);
	}
}
