package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RamoTipoPolizaDN {
	private static final RamoTipoPolizaDN INSTANCIA = new RamoTipoPolizaDN();

	public static RamoTipoPolizaDN getInstancia() {
		return RamoTipoPolizaDN.INSTANCIA;
	}

	public List<RamoTipoPolizaDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoTipoPolizaSN().listarTodos();
	}

	public void agregar(RamoTipoPolizaDTO ramoTipoPolizaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoTipoPolizaSN().agregar(ramoTipoPolizaDTO);
	}

	public void modificar(RamoTipoPolizaDTO ramoTipoPolizaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoTipoPolizaSN().modificar(ramoTipoPolizaDTO);
	}

	public RamoTipoPolizaDTO getPorId(RamoTipoPolizaDTO RamoTipoPolizaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoTipoPolizaSN().getPorId(RamoTipoPolizaDTO.getId());
	}

	public void borrar(RamoTipoPolizaDTO RamoTipoPolizaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoTipoPolizaSN().borrar(RamoTipoPolizaDTO);
	}
	
	public List<RamoTipoPolizaDTO> buscarPorPropiedad (String propiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {	
		return new RamoTipoPolizaSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<RamoDTO> listarRamosPorAsociar(TipoPolizaDTO TipoPolizaDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		List<RamoTipoPolizaDTO> RamoTipoPolizaDTOList = new RamoTipoPolizaSN().listarRamosPorAsociar(TipoPolizaDTO.getIdToTipoPoliza());
		RamoSN ramoSN = new RamoSN();
		List<RamoDTO> ramoDTOList = new ArrayList<RamoDTO>();
		RamoDTO ramoTMP = new RamoDTO();
		for (RamoTipoPolizaDTO actual : RamoTipoPolizaDTOList){
			ramoTMP.setIdTcRamo(actual.getId().getIdtcramo());
			ramoDTOList.add(ramoSN.getRamoPorId(ramoTMP));
		}
		return ramoDTOList;
	}
}
