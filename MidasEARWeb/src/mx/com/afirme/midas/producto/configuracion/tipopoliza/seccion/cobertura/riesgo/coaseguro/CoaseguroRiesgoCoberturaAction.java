package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 21 de Agosto de 2009
 */
public class CoaseguroRiesgoCoberturaAction extends RiesgoAction{
	/**
	 * Method mostrarRegistroCoaseguro
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarRegistroCoaseguro(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoForm riesgoForm = (RiesgoForm) form;
		String id = request.getParameter("id");
		riesgoForm.setIdToRiesgo(id);
		String idPadres = request.getParameter("idPadre");
		String ids[] = idPadres.split("a");
		riesgoForm.setIdToCobertura(ids[0]);
		riesgoForm.setIdToSeccion(ids[1]);
		try {
			CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN = CoaseguroRiesgoCoberturaDN.getInstancia();
			coaseguroRiesgoCoberturaDN.sincronizarCoasegurosRiesgoCobertura(UtileriasWeb.regresaBigDecimal(id), UtileriasWeb.regresaBigDecimal(ids[0]), UtileriasWeb.regresaBigDecimal(ids[1]));
		} catch (SystemException e) {
			e.printStackTrace();
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarCoasegurosRegistrados
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarCoasegurosRegistrados(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		
		String idRiesgo = request.getParameter("id");
		String idPadres = request.getParameter("idPadre");
		String ids[] = idPadres.split("a");
		riesgoForm.setIdToCobertura(ids[0]);
		riesgoForm.setIdToSeccion(ids[1]);
		
		riesgoForm.setCoasegurosRegistrados(riesgoDN.listarCoasegurosPorRiesgoCoberturaSeccion(UtileriasWeb.regresaBigDecimal(idRiesgo), UtileriasWeb.regresaBigDecimal(riesgoForm.getIdToCobertura()), UtileriasWeb.regresaBigDecimal(riesgoForm.getIdToSeccion())));
		
//		String json = "{rows:[";
//		if(riesgoForm.getCoasegurosRegistrados()!= null && riesgoForm.getCoasegurosRegistrados().size() > 0) {
//			for(CoaseguroRiesgoCoberturaDTO actual : riesgoForm.getCoasegurosRegistrados()) {
//				json += "{id:\"" + actual.getId().getNumeroSecuencia()+ "\",data:[";
//				json += actual.getId().getIdToSeccion() + ",";
//				json += actual.getId().getIdToCobertura()+ ",";
//				json += actual.getId().getIdToRiesgo() + ",";
//				json += actual.getId().getNumeroSecuencia() + ",";
//				json += actual.getValor()+",";
//				json += actual.getClaveDefault()+"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(riesgoForm.getCoasegurosRegistrados()!= null && riesgoForm.getCoasegurosRegistrados().size() > 0) {
			for(CoaseguroRiesgoCoberturaDTO actual : riesgoForm.getCoasegurosRegistrados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				
				row.setId(actual.getId().getNumeroSecuencia().toString());
				row.setDatos(
						actual.getId().getIdToSeccion().toString(),
						actual.getId().getIdToCobertura().toString(),
						actual.getId().getIdToRiesgo().toString(),
						actual.getId().getNumeroSecuencia().toString(),
						actual.getValor().toString(),
						actual.getClaveDefault().toString()
				);
				json.addRow(row);
			}
		}
		
		response.setContentType("text/json");
		System.out.println("Coaseguros: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarCoaseguro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException 
	 */
	public ActionForward guardarCoaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN = new CoaseguroRiesgoCoberturaDN();
		String action = "";
		CoaseguroRiesgoCoberturaId id = new CoaseguroRiesgoCoberturaId();
		try {
			id.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(request.getParameter("idRiesgo")));
			id.setIdToCobertura(UtileriasWeb.regresaBigDecimal(request.getParameter("idCobertura")));
			id.setIdToSeccion(UtileriasWeb.regresaBigDecimal(request.getParameter("idSeccion")));
			String numSecuenciaId = request.getParameter("gr_id");
			id.setNumeroSecuencia(UtileriasWeb.regresaBigDecimal(numSecuenciaId));
			CoaseguroRiesgoCoberturaDTO coaseguro = new CoaseguroRiesgoCoberturaDTO();
			coaseguro.setId(id);
			//coaseguro = coaseguroRiesgoCoberturaDN.getPorId(coaseguro);
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				RiesgoCoberturaDTO riesgoCobertura = new RiesgoCoberturaDTO();
				RiesgoCoberturaId riesgoCoberturaId = new RiesgoCoberturaId(id.getIdToSeccion(),id.getIdToCobertura(),id.getIdToRiesgo());
				riesgoCobertura.setId(riesgoCoberturaId);
				riesgoCobertura = new RiesgoCoberturaDN().getPorId(riesgoCobertura);
				coaseguro.setRiesgoCoberturaDTO(riesgoCobertura);
				coaseguro.setValor(Double.valueOf(request.getParameter("valor")).doubleValue());
				coaseguro.setClaveDefault(Short.valueOf(request.getParameter("claveDefault")));
				if (request.getParameter("!nativeeditor_status").equals("inserted")){
					coaseguroRiesgoCoberturaDN.agregar(coaseguro);
					action = "insert";
				}
				else{
					coaseguroRiesgoCoberturaDN.modificar(coaseguro);
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				coaseguroRiesgoCoberturaDN.borrar(coaseguro);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
}
