package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RamoTipoPolizaSN {
	private RamoTipoPolizaFacadeRemote beanRemoto;

	public RamoTipoPolizaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en RamoTipoPolizaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(RamoTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RamoTipoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(RamoTipoPolizaDTO RamoTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(RamoTipoPolizaDTO);
	}

	public void modificar(RamoTipoPolizaDTO RamoTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(RamoTipoPolizaDTO);
	}

	public RamoTipoPolizaDTO getPorId(RamoTipoPolizaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(RamoTipoPolizaDTO RamoTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(RamoTipoPolizaDTO);
	}

	public List<RamoTipoPolizaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<RamoTipoPolizaDTO> listarRamosPorAsociar(BigDecimal idToTipoPoliza)  throws SystemException, ExcepcionDeAccesoADatos {
		return beanRemoto.obtenerRamosSinAsociar(idToTipoPoliza);
	}
}
