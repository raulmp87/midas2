package mx.com.afirme.midas.producto.configuracion.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RamoProductoSN {
	private RamoProductoFacadeRemote beanRemoto;

	public RamoProductoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en RamoProductoSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(RamoProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RamoProductoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(RamoProductoDTO RamoProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(RamoProductoDTO);
	}

	public void modificar(RamoProductoDTO RamoProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(RamoProductoDTO);
	}

	public RamoProductoDTO getPorId(RamoProductoId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(RamoProductoDTO RamoProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(RamoProductoDTO);
	}

	public List<RamoProductoDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<RamoProductoDTO> listarRamosPorAsociar(BigDecimal idToProducto)  throws SystemException, ExcepcionDeAccesoADatos {
		return beanRemoto.obtenerRamosSinAsociar(idToProducto);
	}
}
