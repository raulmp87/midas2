package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RiesgoAction extends MidasMappingDispatchAction {
	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		session.removeAttribute("claveNegocio");
		session.setAttribute("claveNegocio", request.getParameter("claveNegocio"));
		try {
			RiesgoForm riesgoForm= (RiesgoForm)form;
			if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
				riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
			}
			limpiarForm(riesgoForm);
			if (request.getParameter("claveNegocio")!=null){
				if (request.getParameter("claveNegocio").equals("A")){
					RiesgoDN riesgoDN = RiesgoDN.getInstancia();
					List<RiesgoDTO> listRiesgo = riesgoDN.listarVigentesAutos();
					request.setAttribute("listRiesgo", listRiesgo);
				}else{
					this.listarTodos(request);
				}
			}else{
				this.listarTodos(request);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		try {
			poblarDTO(riesgoForm, riesgoDTO);
			//Quita version en busqueda si viene vacio
			if (UtileriasWeb.esCadenaVacia(riesgoForm.getVersion())){
				riesgoDTO.setVersion(null);
			}
			boolean mostrarInactivos = false;
			if(!UtileriasWeb.esCadenaVacia(riesgoForm.getMostrarInactivos())) {
				mostrarInactivos = true;
			}
//			request.setAttribute("listRiesgo", riesgoDN.listarFiltrado(riesgoDTO, mostrarInactivos));
			request.setAttribute("listRiesgo", riesgoDN.getListarFiltrado(riesgoDTO, mostrarInactivos));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
//		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		try {
			poblarDTO(riesgoForm, riesgoDTO);
			riesgoDTO.setClaveEstatus((short) 1); //Nace Activo
			riesgoDN.agregar(riesgoDTO);
			listarTodos(request);
			limpiarForm(riesgoForm);
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			riesgoForm.setMensaje("Verifique la informacion registrada");
			riesgoForm.setTipoMensaje(Sistema.ERROR);
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			riesgoForm.setMensaje("Error al registrar");
			riesgoForm.setTipoMensaje(Sistema.ERROR);
//			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
//		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
//		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		try {
			poblarDTO(riesgoForm, riesgoDTO);
			riesgoDN.modificar(riesgoDTO);
			limpiarForm(riesgoForm);
			this.listarTodos(request);
		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			riesgoForm.setMensaje("Verifique la informacion registrada");
			riesgoForm.setTipoMensaje(Sistema.ERROR);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
//			reglaNavegacion = Sistema.NO_EXITOSO;
			riesgoForm.setMensaje("Error al registrar");
			riesgoForm.setTipoMensaje(Sistema.ERROR);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
	}

	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
//		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		try {
			riesgoDTO.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(riesgoForm.getIdToRiesgo()));
			riesgoDTO = riesgoDN.getPorId(riesgoDTO);
			riesgoDN.borrar(riesgoDTO);
			listarTodos(request);
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
//			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
//		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method borradoLogico
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borradoLogico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
//		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		try {
			riesgoDTO.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(riesgoForm.getIdToRiesgo()));
			riesgoDTO = riesgoDN.getPorId(riesgoDTO);
			riesgoDN.borradoLogico(riesgoDTO);
			listarTodos(request);
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
//			reglaNavegacion = Sistema.NO_DISPONIBLE;/
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
//			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		try {
			poblarDTO(riesgoForm, riesgoDTO);
			riesgoDTO = riesgoDN.getPorId(riesgoDTO);
			this.poblarForm(riesgoDTO, riesgoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		RiesgoForm riesgoForm = (RiesgoForm) form;
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		
		return forward;
	}

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RiesgoForm riesgoForm = (RiesgoForm) form;
		RiesgoDTO riesgoDTO = new RiesgoDTO();
		String id = request.getParameter("id");
		String idPadres = request.getParameter("idPadre");
		if (idPadres != null){
			String ids[] = idPadres.split("a");
			riesgoForm.setIdToCobertura(ids[0]);
			riesgoForm.setIdToSeccion(ids[1]);
		}
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(riesgoForm.getClaveNegocio())) {
			riesgoForm.setClaveNegocio((String)session.getAttribute("claveNegocio"));
		}
		try {
			riesgoDTO.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(id));
			riesgoDTO = riesgoDN.getPorId(riesgoDTO);
			this.poblarForm(riesgoDTO, riesgoForm);
			riesgoForm.setEsAsociadoCobertura(riesgoDN.isCoberturaAsociada(riesgoDTO));
			riesgoForm.setEsAsociadoTarifa(riesgoDN.isTarifaAsociada(riesgoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Recupera la lista de registros RiesgoDTO y la establece como atributo en el request,
	 * bajo el nombre de 'listRiesgo'
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request) throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		//List<RiesgoDTO> listRiesgo = riesgoDN.listarTodos();
		//listar solo los registros activos que no han sido borrados logicamente.
		List<RiesgoDTO> listRiesgo = riesgoDN.listarVigentes();
		request.setAttribute("listRiesgo", listRiesgo);
	}

	/**
	 * Method poblarDTO
	 * 
	 * @param RiesgoForm
	 * @param RiesgoDTO
	 * @throws SystemException 
	 */
	private void poblarDTO(RiesgoForm riesgoForm, RiesgoDTO riesgoDTO) throws SystemException {
		if (!StringUtil.isEmpty(riesgoForm.getIdToRiesgo()))
			riesgoDTO.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(riesgoForm.getIdToRiesgo()));
		//Verificar si el id del subramo es v�lido
		if (!StringUtil.isEmpty(riesgoForm.getIdTcSubRamo())){
			SubRamoDTO subRamoTMP = new SubRamoDTO();
			subRamoTMP.setIdTcSubRamo(UtileriasWeb.regresaBigDecimal(riesgoForm.getIdTcSubRamo()));
			//usar el id para encontrar el registro de l subramo correspondiente
			try{
				subRamoTMP = new SubRamoDN().getSubRamoPorId(subRamoTMP);
			}
			catch(ExcepcionDeAccesoADatos ex){}
			//asignar el objeto subramo al objeto riesgo
			riesgoDTO.setSubRamoDTO(subRamoTMP);
		}
		/*if (!StringUtil.isEmpty(riesgoForm.getNumeroSecuencia()))
			riesgoDTO.setNumeroSecuencia(UtileriasWeb.regresaBigDecimal(riesgoForm.getNumeroSecuencia()));*/
		riesgoDTO.setCodigo(riesgoForm.getCodigo());
		if (!UtileriasWeb.esCadenaVacia(riesgoForm.getVersion()))
			riesgoDTO.setVersion(Integer.valueOf(riesgoForm.getVersion()));
		else
			riesgoDTO.setVersion(new Integer(1));
		riesgoDTO.setDescripcion(riesgoForm.getDescripcion().toUpperCase());
		//riesgoDTO.setDescripcionCorta(riesgoForm.getDescripcionCorta());
		riesgoDTO.setNombreComercial(riesgoForm.getNombreComercial().toUpperCase());
		//riesgoDTO.setClaveTipoSumaAsegurada(riesgoForm.getClaveTipoSumaAsegurada());
		//riesgoDTO.setClaveActivoConfiguracion(riesgoForm.getClaveActivoConfiguracion());
		//riesgoDTO.setClaveActivoProduccion(riesgoForm.getClaveActivoProduccion());
		
		if(!StringUtil.isEmpty(riesgoForm.getClaveAplicaPerdidaTotal())){
			riesgoDTO.setClaveAplicaPerdidaTotal(Short.valueOf("1")); //Aplica Perdida Total
		}else
			riesgoDTO.setClaveAplicaPerdidaTotal(Short.valueOf("0")); //Aplicac Perdida Parcial
		
		if (!UtileriasWeb.esCadenaVacia(riesgoForm.getValorMinimoPrima()))
			riesgoDTO.setValorMinimoPrima(BigDecimal.valueOf(Long.valueOf(riesgoForm.getValorMinimoPrima())));
		else
			riesgoDTO.setValorMinimoPrima(new BigDecimal (0));
		
		Double idRamo;
		if (!StringUtil.isEmpty(riesgoForm.getIdTcRamo())) {
			idRamo = Double.valueOf(riesgoForm.getIdTcRamo());
			riesgoDTO.setIdTcRamo(BigDecimal.valueOf(idRamo));
		}
	}

	/**
	 * Method poblarForm
	 * 
	 * @param RiesgoDTO
	 * @param RiesgoForm
	 */
	private void poblarForm(RiesgoDTO riesgoDTO, RiesgoForm riesgoForm) {
		if (!UtileriasWeb.esCadenaVacia(""+riesgoDTO.getIdToRiesgo()))
			riesgoForm.setIdToRiesgo(riesgoDTO.getIdToRiesgo().toString());
		if (riesgoDTO.getSubRamoDTO()!=null){
			if (!UtileriasWeb.esCadenaVacia(""+riesgoDTO.getSubRamoDTO().getIdTcSubRamo())){
				riesgoForm.setIdTcSubRamo(riesgoDTO.getSubRamoDTO().getIdTcSubRamo().toString());
				riesgoForm.setCodigoSubRamo(riesgoDTO.getSubRamoDTO().getCodigoSubRamo().toBigInteger().toString());
				riesgoForm.setDescripcionSubRamo(riesgoDTO.getSubRamoDTO().getDescripcionSubRamo());
			}
			if (riesgoDTO.getSubRamoDTO().getRamoDTO()!=null && !UtileriasWeb.esCadenaVacia(""+riesgoDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo())){
				riesgoForm.setIdTcRamo(riesgoDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo().toString());
				riesgoForm.setCodigoRamo(riesgoDTO.getSubRamoDTO().getRamoDTO().getCodigo().toBigInteger().toString());
				riesgoForm.setDescripcionRamo(riesgoDTO.getSubRamoDTO().getRamoDTO().getDescripcion());
			}
		}
		/*if (riesgoDTO.getNumeroSecuencia()!=null)
			riesgoForm.setNumeroSecuencia(riesgoDTO.getNumeroSecuencia().toBigInteger().toString());*/
		if (!UtileriasWeb.esCadenaVacia(""+riesgoDTO.getCodigo()))
			riesgoForm.setCodigo(riesgoDTO.getCodigo());
		if (!UtileriasWeb.esCadenaVacia(""+riesgoDTO.getVersion()))
			riesgoForm.setVersion(riesgoDTO.getVersion().toString());
		if (!UtileriasWeb.esCadenaVacia(riesgoDTO.getDescripcion()))
			riesgoForm.setDescripcion(riesgoDTO.getDescripcion());
		/*if (!UtileriasWeb.esCadenaVacia(riesgoDTO.getDescripcionCorta()))
			riesgoForm.setDescripcionCorta(riesgoDTO.getDescripcionCorta());*/
		if (!UtileriasWeb.esCadenaVacia(riesgoDTO.getNombreComercial()))
			riesgoForm.setNombreComercial(riesgoDTO.getNombreComercial());
		/*if (!UtileriasWeb.esCadenaVacia(riesgoDTO.getClaveTipoSumaAsegurada())){
			riesgoForm.setClaveTipoSumaAsegurada(riesgoDTO.getClaveTipoSumaAsegurada());
			riesgoForm.setDescripcionTipoSumaAsegurada(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_SUMA_ASEGURADA, riesgoDTO.getClaveTipoSumaAsegurada()));
		}*/
		/*if (!UtileriasWeb.esCadenaVacia(riesgoDTO.getClaveActivoConfiguracion()))
			riesgoForm.setClaveActivoConfiguracion(riesgoDTO.getClaveActivoConfiguracion());
		if (!UtileriasWeb.esCadenaVacia(riesgoDTO.getClaveActivoProduccion()))
			riesgoForm.setClaveActivoProduccion(riesgoDTO.getClaveActivoProduccion());*/
		
		if (riesgoDTO.getClaveEstatus() != null)
			riesgoForm.setClaveEstatus(riesgoDTO
					.getClaveEstatus().toString());
		
		riesgoForm.setDescripcionEstatus(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.GRUPO_CLAVE_ESTATUS_PRODUCTO, riesgoForm.getClaveEstatus()));
		
		if (riesgoDTO.getValorMinimoPrima() != null) {
			riesgoForm.setValorMinimoPrima(riesgoDTO.getValorMinimoPrima().toString());
		}
		
		if (riesgoDTO.getClaveAplicaPerdidaTotal() != null) {
			riesgoForm.setClaveAplicaPerdidaTotal(UtileriasWeb.calculaValorStringDelCheck(riesgoDTO.getClaveAplicaPerdidaTotal()));
		}
		
		
		
	}
	
	private void limpiarForm(RiesgoForm riesgoForm){
		riesgoForm.setIdToRiesgo("");
		riesgoForm.setIdTcSubRamo("");
		//riesgoForm.setNumeroSecuencia("");
		riesgoForm.setCodigo("");
		riesgoForm.setVersion("");
		riesgoForm.setDescripcion("");
		//riesgoForm.setDescripcionCorta("");
		riesgoForm.setNombreComercial("");
		//riesgoForm.setClaveTipoSumaAsegurada("");
		//riesgoForm.setClaveActivoConfiguracion("");
		//riesgoForm.setClaveActivoProduccion("");
		riesgoForm.setIdTcRamo("");
	}

	/**
	 * Method listarJsonPorPradre
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException
	 * @throws IOException 
	 */
	public void listarJsonPorPradre(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws SystemException, IOException {
		String id = request.getParameter("id");
		String idPadre = request.getParameter("idPadre");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		List<RiesgoCoberturaDTO> riesgos = CoberturaDN.getInstancia().listarRiesgosVigentesAsociados(coberturaDTO , UtileriasWeb.regresaBigDecimal(idPadre));

		MidasJsonBase json = new MidasJsonBase();
		for(RiesgoCoberturaDTO riesgo : riesgos) {
			MidasJsonRow row = new MidasJsonRow();
			row.setId(riesgo.getId().getIdtoriesgo().toString());
			row.setDatos(riesgo.getRiesgoDTO().getCodigo(),
					riesgo.getRiesgoDTO().getNombreComercial(),
					riesgo.getRiesgoDTO().getDescripcion());
			json.addRow(row);
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
