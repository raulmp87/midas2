package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentoanexo;

import java.math.BigDecimal;
import java.util.List;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;

public class DocumentoAnexoCgDN {
	private static final DocumentoAnexoCgDN INSTANCIA = new DocumentoAnexoCgDN();

	public static DocumentoAnexoCgDN getInstancia() {
		return DocumentoAnexoCgDN.INSTANCIA;
	}

	public List<CgCondiciones> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCgSN().listarTodos();
	}

	public void agregar(CgCondiciones cgCondiciones) throws SystemException,ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		new DocumentoAnexoCgSN().agregar(cgCondiciones);
	}

	public void modificar(CgCondiciones cgCondiciones) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoCgSN().modificar(cgCondiciones);
	}

	public CgCondiciones getPorId(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCgSN().getPorId(id);
	}

	public void borrar(CgCondiciones cgCondiciones) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoCgSN().borrar(cgCondiciones);
	}
	
	public List<CgCondiciones> listarPorPropiedad(String propiedad, Object valor) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCgSN().encontrarPorPropiedad(propiedad, valor);
	}
	
	public List<CgCondiciones> listarPorIdSeccion(BigDecimal idToSeccion) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoCgSN().encontrarPorPropiedad("cgCondiciones.id",idToSeccion);
	}
}
