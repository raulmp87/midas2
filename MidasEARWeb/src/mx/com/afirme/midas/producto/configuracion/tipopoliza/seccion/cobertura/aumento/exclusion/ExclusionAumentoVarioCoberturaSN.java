package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 18 de Agosto de 2009
 */
public class ExclusionAumentoVarioCoberturaSN {
	private ExclusionAumentoVarioCoberturaFacadeRemote beanRemoto;

	public ExclusionAumentoVarioCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionAumentoVarioCoberturaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionAumentoVarioCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionAumentoVarioCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionAumentoVarioCoberturaDTO excAumentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excAumentoPorCoberturaDTO);
	}

	public void modificar(ExclusionAumentoVarioCoberturaDTO excAumentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excAumentoPorCoberturaDTO);
	}

	public ExclusionAumentoVarioCoberturaDTO getPorId(ExclusionAumentoVarioCoberturaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionAumentoVarioCoberturaDTO excAumentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excAumentoPorCoberturaDTO);
	}

	public List<ExclusionAumentoVarioCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public ExclusionAumentoVarioCoberturaDTO buscarPorIDs(BigDecimal idToCobertura, BigDecimal idToAumentoVario, BigDecimal idToRiesgo){
		List<ExclusionAumentoVarioCoberturaDTO> lista = beanRemoto.findByIDs(idToCobertura, idToAumentoVario, idToRiesgo);
		ExclusionAumentoVarioCoberturaDTO registro;
		if (lista == null || lista.isEmpty())
			registro=null;
		else
			registro = lista.get(0); 
		return registro;
	}
	
	public List<ExclusionAumentoVarioCoberturaDTO> buscarVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.getVigentesPorIdCoberturaIdSeccion(idToCobertura, idToSeccion);
	}
}
