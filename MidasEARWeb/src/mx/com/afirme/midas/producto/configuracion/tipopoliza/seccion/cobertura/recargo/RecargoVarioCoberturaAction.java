package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Fernando Alonzo
 * @since 17 de agosto de 2009
 */
public class RecargoVarioCoberturaAction extends CoberturaAction {

	/**
	 * Method asociarRecargo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward asociarRecargo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarRecargoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarRecargoAsociado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		RecargoVarioDN descuentoDN = RecargoVarioDN.getInstancia();
		String action = "";
		RecargoVarioCoberturaId id = new RecargoVarioCoberturaId();
		BigDecimal idToRecargoVario = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre"));
		id.setIdtorecargovario(idToRecargoVario);
		id.setIdtocobertura(idToCobertura);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				RecargoVarioCoberturaDTO recargoPorCoberturaDTO = new RecargoVarioCoberturaDTO();
				recargoPorCoberturaDTO.setId(id);
				recargoPorCoberturaDTO.setClaveaplicareaseguro(Short.parseShort(request.getParameter("aplicaReaseguro")));
				recargoPorCoberturaDTO.setClavecomercialtecnico(Short.parseShort(request.getParameter("comercialTecnico")));
				recargoPorCoberturaDTO.setClaveobligatoriedad(Short.parseShort(request.getParameter("obligatorio").equals("1")? "3" : "0"));
				recargoPorCoberturaDTO.setValor(Double.parseDouble(request.getParameter("valor")));

				RecargoVarioDTO recargoDTO = new RecargoVarioDTO();
				recargoDTO.setIdtorecargovario(recargoPorCoberturaDTO.getId().getIdtorecargovario());
				recargoDTO = descuentoDN.getPorId(recargoDTO);

				CoberturaDTO cobertura = new  CoberturaDTO();
				cobertura.setIdToCobertura(id.getIdtocobertura());
				cobertura = coberturaDN.getPorId(cobertura);
				recargoPorCoberturaDTO.setCoberturaDTO(cobertura);
				recargoPorCoberturaDTO.setRecargoVarioDTO(recargoDTO);
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					action = "insert";
					coberturaDN.asociarRecargo(coberturaDTO, recargoPorCoberturaDTO);
					mensajeExitoAgregar(coberturaForm, request);
				} else {
					action = "update";
					coberturaDN.actualizarAsociacion(coberturaDTO, recargoPorCoberturaDTO);
					mensajeExitoModificar(coberturaForm, request);
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				action = "deleted";
				RecargoVarioCoberturaDTO recargoPorCoberturaDTO = new RecargoVarioCoberturaDTO();
				recargoPorCoberturaDTO.setId(id);
				coberturaDN.desasociarRecargo(coberturaDTO, recargoPorCoberturaDTO);
				mensajeExitoBorrar(coberturaForm, request);
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(coberturaForm, request, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mensajeExcepcion(coberturaForm, request, e);
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarRecargosAsociados(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		coberturaForm.setRecargosAsociados(coberturaDN.getPorIdCascada(coberturaDTO).getRecargos());
//		String json = "{rows:[";
//		if(coberturaForm.getRecargosAsociados() != null && coberturaForm.getRecargosAsociados().size() > 0) {
//			for(RecargoVarioCoberturaDTO recargo : coberturaForm.getRecargosAsociados()) {
//				json += "{id:\"" + recargo.getId().getIdtorecargovario() + "\",data:[";
//				json += id + ",\"";
//				json += recargo.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += recargo.getClaveobligatoriedad() + "\",";
//				json += recargo.getClavecomercialtecnico() + ",";
//				json += recargo.getClaveaplicareaseguro() + ",\"";
//				json += ( recargo.getRecargoVarioDTO().getClavetiporecargo().intValue()==1 ? "%":"$" ) + "\",";
//				json += recargo.getValor() + "]},";				
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getRecargosAsociados() != null && coberturaForm.getRecargosAsociados().size() > 0) {
			for(RecargoVarioCoberturaDTO recargo : coberturaForm.getRecargosAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(recargo.getId().getIdtorecargovario().toString());
				row.setDatos(
						id,
						recargo.getRecargoVarioDTO().getDescripcionrecargo(),
						recargo.getClaveobligatoriedad().toString(),
						recargo.getClavecomercialtecnico().toString(),
						recargo.getClaveaplicareaseguro().toString(),
						(recargo.getRecargoVarioDTO().getClavetiporecargo().intValue()==1 ? "%":"$" ),
						recargo.getValor().toString()
				);
				json.addRow(row);
						
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarRecargosPorAsociar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		
		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		coberturaForm.setRecargosPorAsociar(coberturaDN.listarRecargoPorAsociarCobertura(coberturaDTO));
//		String json = "{rows:[";
//		if(coberturaForm.getRecargosPorAsociar() != null && coberturaForm.getRecargosPorAsociar().size() > 0) {
//			for(RecargoVarioDTO recargo : coberturaForm.getRecargosPorAsociar()) {
//				json += "{id:\"" + recargo.getIdtorecargovario() + "\",data:[";
//				json += id + ",\"" + recargo.getDescripcionrecargo() + "\",0,";
//				json += recargo.getClavetiporecargo() + ",";
//				json += "0,\""+(recargo.getClavetiporecargo().intValue()==1 ? "%" : "$" ) + "\",0]},";				
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getRecargosPorAsociar() != null && coberturaForm.getRecargosPorAsociar().size() > 0) {
			for(RecargoVarioDTO recargo : coberturaForm.getRecargosPorAsociar()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(recargo.getIdtorecargovario().toString());
				row.setDatos(
						id,
						recargo.getDescripcionrecargo(),
						"0",
						recargo.getClavetiporecargo().toString(),
						"0",
						(recargo.getClavetiporecargo().intValue()==1 ? "%" : "$" ),
						"0"
				);
				json.addRow(row);
							
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
