package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Fernando Alonzo
 * @since 18 de agosto de 2009
 * @modify Jos� Luis Arellano
 * @since 21 de septiembre de 2009
 */
public class ExclusionRecargoVarioCoberturaAction extends CoberturaAction {

	/**
	 * Method excluirRiesgo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward excluirRecargoCobertura(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		this.poblarForm(coberturaDTO, coberturaForm);
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		try {
			//Estas listas se utilizan para el filtrado de registros de exclusion por Riesgo y por Aumento|Descuento|Recargo
			coberturaForm.setRiesgosAsociados(RiesgoDN.getInstancia().listarRiesgosVigentesPorCoberturaSeccion(UtileriasWeb.regresaBigDecimal(coberturaForm.getIdToCobertura()), UtileriasWeb.regresaBigDecimal(idSeccion)));
			coberturaForm.setRecargosAsociados(new RecargoVarioCoberturaSN().listarRecargoAsociado(UtileriasWeb.regresaBigDecimal(coberturaForm.getIdToCobertura())));
			coberturaForm.setDescripcionSubRamo("");
		} catch (ExcepcionDeAccesoADatos e) {e.printStackTrace();
		} catch (SystemException e) {e.printStackTrace();}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarRiesgoExcluido
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarRecargoCoberturaExcluido(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		RecargoVarioDN recargoDN = RecargoVarioDN.getInstancia();
		ExclusionRecargoVarioCoberturaId id = new ExclusionRecargoVarioCoberturaId();
		RiesgoDN riesgoDN = RiesgoDN.getInstancia();
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		try {
			id.setIdtorecargovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtoriesgo(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionRecargoVarioCoberturaDTO excRecargoCobertura = new ExclusionRecargoVarioCoberturaDTO();
			excRecargoCobertura.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				RecargoVarioDTO Recargo = new RecargoVarioDTO();
				Recargo.setIdtorecargovario(UtileriasWeb.regresaBigDecimal(ids[0]));
				Recargo = recargoDN.getPorId(Recargo);

				CoberturaDTO cobertura = new CoberturaDTO();
				cobertura.setIdToCobertura(UtileriasWeb.regresaBigDecimal(ids[2]));
				cobertura = coberturaDN.getPorId(cobertura);
				
				RiesgoDTO riesgo = new  RiesgoDTO();
				riesgo.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(ids[1]));
				riesgo = riesgoDN.getPorId(riesgo);
				
				excRecargoCobertura.setRecargoVarioDTO(Recargo);
				excRecargoCobertura.setCoberturaDTO(cobertura);
				excRecargoCobertura.setRiesgoDTO(riesgo);
				coberturaDN.ExcluirRecargoCobertura(excRecargoCobertura);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				coberturaDN.EliminarExclusionRecargoCobertura(excRecargoCobertura);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
		
		
	}

	public void mostrarRecargosCoberturaExcluidos(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		String idToSeccion = request.getParameter("idToSeccion");
		
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaForm.setExcRecargoCoberturaAsociados(coberturaDN.listarExcRecargoCoberturaAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
//		String json = "{rows:[";
//		if(coberturaForm.getExcRecargoCoberturaAsociados()!= null && coberturaForm.getExcRecargoCoberturaAsociados().size() > 0) {
//			for(ExclusionRecargoVarioCoberturaDTO recargoExcluido : coberturaForm.getExcRecargoCoberturaAsociados()) {
//				json += "{id:\"" + recargoExcluido.getId().getIdtorecargovario()+"|"+recargoExcluido.getId().getIdtoriesgo() +"|"+id+ "\",data:[";
//				json += recargoExcluido.getRecargoVarioDTO().getClavetiporecargo() + ",\"";
//				json += recargoExcluido.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += recargoExcluido.getRiesgoDTO().getCodigo() + "\",\"";
//				json += recargoExcluido.getRiesgoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getExcRecargoCoberturaAsociados()!= null && coberturaForm.getExcRecargoCoberturaAsociados().size() > 0) {
			for(ExclusionRecargoVarioCoberturaDTO recargoExcluido : coberturaForm.getExcRecargoCoberturaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(recargoExcluido.getId().getIdtorecargovario()+"|"+recargoExcluido.getId().getIdtoriesgo() +"|"+id);
				row.setDatos(
						recargoExcluido.getRecargoVarioDTO().getClavetiporecargo().toString(),
						recargoExcluido.getRecargoVarioDTO().getDescripcionrecargo(),
						recargoExcluido.getRiesgoDTO().getCodigo(),
						recargoExcluido.getRiesgoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Exclusion Recargo Cobertura Excluidos: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarRecargosCoberturaPorExcluir(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		String idToSeccion = request.getParameter("idToSeccion");
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		//Posibles par�metros recibidos para hacer un filtrado por riesgo o Recargo
		String idToRiesgo = request.getParameter("idToRiesgo");
		String idToRecargo = request.getParameter("idToRecargo");
		if (idToRiesgo != null)
			coberturaForm.setExcRecargoCoberturaNoAsociados(coberturaDN.listarExcRecargoCoberturaNoAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion),UtileriasWeb.regresaBigDecimal(idToRiesgo)));
		else if (idToRecargo != null)
			coberturaForm.setExcRecargoCoberturaNoAsociados(coberturaDN.listarExcRecargoCoberturaNoAsociadas(UtileriasWeb.regresaBigDecimal(idToRecargo),coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
		else
			coberturaForm.setExcRecargoCoberturaNoAsociados(coberturaDN.listarExcRecargoCoberturaNoAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));

//		String json = "{rows:[";
//		if(coberturaForm.getExcRecargoCoberturaNoAsociados() != null && coberturaForm.getExcRecargoCoberturaNoAsociados().size() > 0) {
//			for(ExclusionRecargoVarioCoberturaDTO recargoPorExcluir : coberturaForm.getExcRecargoCoberturaNoAsociados()) {
//				json += "{id:\"" + recargoPorExcluir.getId().getIdtorecargovario()+"|"+recargoPorExcluir.getId().getIdtoriesgo() +"|"+id+ "\",data:[";
//				json += recargoPorExcluir.getRecargoVarioDTO().getClavetiporecargo() + ",\"";
//				json += recargoPorExcluir.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += recargoPorExcluir.getRiesgoDTO().getCodigo() + "\",\"";
//				json += recargoPorExcluir.getRiesgoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getExcRecargoCoberturaNoAsociados() != null && coberturaForm.getExcRecargoCoberturaNoAsociados().size() > 0) {
			for(ExclusionRecargoVarioCoberturaDTO recargoPorExcluir : coberturaForm.getExcRecargoCoberturaNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(recargoPorExcluir.getId().getIdtorecargovario()+"|"+recargoPorExcluir.getId().getIdtoriesgo() +"|"+id);
				row.setDatos(
						recargoPorExcluir.getRecargoVarioDTO().getClavetiporecargo().toString(),
						recargoPorExcluir.getRecargoVarioDTO().getDescripcionrecargo(),
						recargoPorExcluir.getRiesgoDTO().getCodigo(),
						recargoPorExcluir.getRiesgoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Exclusion Recargo Cobertura Por Excluir: "+json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
