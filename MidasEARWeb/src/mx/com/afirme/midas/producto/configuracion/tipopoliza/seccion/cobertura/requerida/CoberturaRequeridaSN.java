package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Fernando Alonzo
 * @since 20 de Agosto de 2009
 */
public class CoberturaRequeridaSN {
	private CoberturaRequeridaFacadeRemote beanRemoto;

	public CoberturaRequeridaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en CoberturaRequeridaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CoberturaRequeridaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CoberturaRequeridaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(CoberturaRequeridaDTO coberturaRequeridaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(coberturaRequeridaDTO);
	}

	public void modificar(CoberturaRequeridaDTO coberturaRequeridaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(coberturaRequeridaDTO);
	}

	public CoberturaRequeridaDTO getPorId(CoberturaRequeridaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(CoberturaRequeridaDTO coberturaRequeridaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(coberturaRequeridaDTO);
	}

	public List<CoberturaRequeridaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
}
