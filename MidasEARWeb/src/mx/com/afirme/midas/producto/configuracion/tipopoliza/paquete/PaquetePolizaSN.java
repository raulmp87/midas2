package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author jose luis arellano
 * @since 15/04/2011
 */
class PaquetePolizaSN {
	private PaquetePolizaFacadeRemote beanRemoto;
	
	PaquetePolizaSN() throws SystemException {
		try{
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(PaquetePolizaFacadeRemote.class);
		}
		catch(Exception e){
			LogDeMidasWeb.log("Error al intentar obtener beanRemoto "+PaquetePolizaFacadeRemote.class, Level.SEVERE, e);
			throw new SystemException(e);
		}
	}
	
	public PaquetePolizaDTO agregar(PaquetePolizaDTO paquetePolizaDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.save(paquetePolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public PaquetePolizaDTO modificar(PaquetePolizaDTO paquetePolizaDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.update(paquetePolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<PaquetePolizaDTO> buscarPorPropiedad (String propiedad,Object valor,boolean poblarCoberturas) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findByProperty(propiedad, valor,poblarCoberturas);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public PaquetePolizaDTO getPorId(BigDecimal idToPaquetePoliza) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idToPaquetePoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<CoberturaSeccionDTO> obtenerCoberturasDisponibles(BigDecimal idToPaquetePoliza){
		return beanRemoto.obtenerCoberturasDisponibles(idToPaquetePoliza);
	}
	
	public List<CoberturaSeccionPaqueteDTO> obtenerCoberturasRegistradas(BigDecimal idToPaquetePoliza){
		return beanRemoto.obtenerCoberturasRegistradas(idToPaquetePoliza);
	}
	
	public ActualizacionCoberturaPaqueteDTO registrarCoberturaPaquete(BigDecimal idToPaquetePoliza,BigDecimal idToCoberturaSeccionPaquete,
			BigDecimal idToSeccion,BigDecimal idToCobertura,BigDecimal montoSumaAsegurada,Short claveContrato){
		return beanRemoto.registrarCoberturaPaquete(idToPaquetePoliza, idToCoberturaSeccionPaquete, 
				idToSeccion, idToCobertura, montoSumaAsegurada, claveContrato);
	}

	public List<PaquetePolizaDTO> listarPorTipoPoliza(BigDecimal idToTipoPoliza,boolean poblarCoberturas){
		return beanRemoto.listarPorTipoPoliza(idToTipoPoliza, poblarCoberturas,null);
	}
	
	public ActualizacionCoberturaPaqueteDTO liberarPaquete(BigDecimal idToPaquete){
		return beanRemoto.liberarPaquete(idToPaquete);
	}

    public ActualizacionCoberturaPaqueteDTO eliminarPaquete(BigDecimal idToPaquete){
    	return beanRemoto.eliminarPaquete(idToPaquete);
    }
    
    public PaquetePolizaDTO establecerPaqueteDefault(BigDecimal idToPaquetePoliza){
    	return beanRemoto.establecerPaqueteDefault(idToPaquetePoliza);
    }
}
