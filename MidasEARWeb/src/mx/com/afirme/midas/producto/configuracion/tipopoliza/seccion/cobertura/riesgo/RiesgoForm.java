package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.tarifa.TarifaDTO;

public class RiesgoForm extends MidasBaseForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idToRiesgo;
	private String codigo;
	private String version;
	private String descripcion;
	//private String descripcionCorta="CAMPO_NO_USADO";
	private String nombreComercial;
	//private String claveTipoSumaAsegurada;
	//private String numeroSecuencia;
	//private String claveActivoConfiguracion="1";
	//private String claveActivoProduccion="0";
	//Datos del ramo
	private String idTcRamo;
	private String codigoRamo;
	private String descripcionRamo;
	//Datos del subramo
	private String idTcSubRamo;
	private String codigoSubRamo;
	private String descripcionSubRamo;
	private String idToCobertura;
	private String idToSeccion;
	private List<CoaseguroRiesgoCoberturaDTO> CoasegurosRegistrados;
	private List<DeducibleRiesgoCoberturaDTO> DeduciblesRegistrados;
	private String idConcepto;
	private List<TarifaDTO> tarifas;
	private String descripcionTipoSumaAsegurada;
	private Boolean esAsociadoCobertura;
	private Boolean esAsociadoTarifa;
	private String mostrarInactivos;
	private String claveEstatus;
	private String descripcionEstatus;
	private String valorMinimoPrima;
	private String claveAplicaPerdidaTotal;
	private String claveNegocio;
	
	public String getIdToRiesgo() {
		return idToRiesgo;
	}
	public void setIdToRiesgo(String idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/*public String getDescripcionCorta() {
		return descripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}*/
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	/*public String getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}
	public void setClaveTipoSumaAsegurada(String claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}*/
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	/*public String getNumeroSecuencia() {
		return numeroSecuencia;
	}
	public void setNumeroSecuencia(String numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}*/
	/*public String getClaveActivoConfiguracion() {
		return claveActivoConfiguracion;
	}
	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}
	public String getClaveActivoProduccion() {
		return claveActivoProduccion;
	}
	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/
	public String getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public String getCodigoRamo() {
		return codigoRamo;
	}
	public void setCodigoRamo(String codigoRamo) {
		this.codigoRamo = codigoRamo;
	}
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	public String getCodigoSubRamo() {
		return codigoSubRamo;
	}
	public void setCodigoSubRamo(String codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	
	public String getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(String idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	public String getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public List<CoaseguroRiesgoCoberturaDTO> getCoasegurosRegistrados() {
		return CoasegurosRegistrados;
	}
	public void setCoasegurosRegistrados(
			List<CoaseguroRiesgoCoberturaDTO> coasegurosRegistrados) {
		CoasegurosRegistrados = coasegurosRegistrados;
	}
	public List<DeducibleRiesgoCoberturaDTO> getDeduciblesRegistrados() {
		return DeduciblesRegistrados;
	}
	public void setDeduciblesRegistrados(
			List<DeducibleRiesgoCoberturaDTO> deduciblesRegistrados) {
		DeduciblesRegistrados = deduciblesRegistrados;
	}
	public List<TarifaDTO> getTarifas() {
		return tarifas;
	}
	public void setTarifas(List<TarifaDTO> tarifas) {
		this.tarifas = tarifas;
	}
	public String getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(String idConcepto) {
		this.idConcepto = idConcepto;
	}
	public String getDescripcionTipoSumaAsegurada() {
		return descripcionTipoSumaAsegurada;
	}
	public void setDescripcionTipoSumaAsegurada(String descripcionTipoSumaAsegurada) {
		this.descripcionTipoSumaAsegurada = descripcionTipoSumaAsegurada;
	}
	public void setEsAsociadoCobertura(Boolean esAsociadoCobertura) {
		this.esAsociadoCobertura = esAsociadoCobertura;
	}
	public Boolean getEsAsociadoCobertura() {
		return esAsociadoCobertura;
	}
	public void setEsAsociadoTarifa(Boolean esAsociadoTarifa) {
		this.esAsociadoTarifa = esAsociadoTarifa;
	}
	public Boolean getEsAsociadoTarifa() {
		return esAsociadoTarifa;
	}
	public void setMostrarInactivos(String mostrarInactivos) {
		this.mostrarInactivos = mostrarInactivos;
	}
	public String getMostrarInactivos() {
		return mostrarInactivos;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	public String getValorMinimoPrima() {
		return valorMinimoPrima;
	}
	public void setValorMinimoPrima(String valorMinimoPrima) {
		this.valorMinimoPrima = valorMinimoPrima;
	}
	public String getClaveAplicaPerdidaTotal() {
		return claveAplicaPerdidaTotal;
	}
	public void setClaveAplicaPerdidaTotal(String claveAplicaPerdidaTotal) {
		this.claveAplicaPerdidaTotal = claveAplicaPerdidaTotal;
	}
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
}
