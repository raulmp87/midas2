package mx.com.afirme.midas.producto.configuracion.ramo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoSN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RamoProductoDN {
	private static final RamoProductoDN INSTANCIA = new RamoProductoDN();

	public static RamoProductoDN getInstancia() {
		return RamoProductoDN.INSTANCIA;
	}

	public List<RamoProductoDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoProductoSN().listarTodos();
	}

	public void agregar(RamoProductoDTO ramoProductoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoProductoSN().agregar(ramoProductoDTO);
	}

	public void modificar(RamoProductoDTO ramoProductoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoProductoSN().modificar(ramoProductoDTO);
	}

	public RamoProductoDTO getPorId(RamoProductoDTO RamoProductoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoProductoSN().getPorId(RamoProductoDTO.getId());
	}

	public void borrar(RamoProductoDTO RamoProductoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RamoProductoSN().borrar(RamoProductoDTO);
	}
	
	public List<RamoProductoDTO> buscarPorPropiedad (String propiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {	
		return new RamoProductoSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<RamoDTO> listarRamosPorAsociar(ProductoDTO productoDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		List<RamoProductoDTO> RamoProductoDTOList = new RamoProductoSN().listarRamosPorAsociar(productoDTO.getIdToProducto());
		RamoSN ramoSN = new RamoSN();
		List<RamoDTO> ramoDTOList = new ArrayList<RamoDTO>();
		RamoDTO ramoTMP = new RamoDTO();
		for (RamoProductoDTO actual : RamoProductoDTOList){
			ramoTMP.setIdTcRamo(actual.getId().getIdtcramo());
			ramoDTOList.add(ramoSN.getRamoPorId(ramoTMP));
		}
		return ramoDTOList;
	}
}
