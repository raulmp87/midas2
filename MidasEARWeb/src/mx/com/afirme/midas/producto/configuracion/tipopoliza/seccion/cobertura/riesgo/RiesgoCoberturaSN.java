package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 17 de Agosto de 2009
 */
public class RiesgoCoberturaSN {
	private RiesgoCoberturaFacadeRemote beanRemoto;

	public RiesgoCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en CoberturaRiesgoSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(RiesgoCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RiesgoCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(RiesgoCoberturaDTO riesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(riesgoCoberturaDTO);
	}

	public void modificar(RiesgoCoberturaDTO riesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(riesgoCoberturaDTO);
	}

	public RiesgoCoberturaDTO getPorId(RiesgoCoberturaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(RiesgoCoberturaDTO riesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(riesgoCoberturaDTO);
	}

	public List<RiesgoCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<RiesgoCoberturaDTO> listarRiesgoAsociado(BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.findByCoberturaSeccion(idToCobertura, idToSeccion);
	}
	
	public List<RiesgoCoberturaDTO> listarRiesgoAsociadoPorId(BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.findByCoberturaSeccionId(idToCobertura, idToSeccion);
	}
	
	public List<RiesgoCoberturaDTO> listarRiesgoVigenteAsociado(BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.listarVigentesPorCoberturaSeccion(idToCobertura, idToSeccion);
	}

	public List<BigDecimal> obtenerRiesgosPorTipoPoliza(BigDecimal idToTipoPoliza) {
		return beanRemoto.obtenerRiesgosPorTipoPoliza(idToTipoPoliza);
	}

	public List<BigDecimal> obtenerRiesgosPorSeccionCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion) {
		return beanRemoto.obtenerRiesgosPorSeccionCotizacion(idToCotizacion, numeroInciso, idToSeccion);
	}

	public void borrarARDs(RiesgoCoberturaDTO riesgoCoberturaDTO) {
		beanRemoto.borrarARDs(riesgoCoberturaDTO);
	}
}
