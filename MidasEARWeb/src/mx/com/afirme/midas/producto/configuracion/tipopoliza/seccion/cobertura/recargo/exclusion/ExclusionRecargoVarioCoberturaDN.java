package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;

import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ExclusionRecargoVarioCoberturaDN {
	private static final ExclusionRecargoVarioCoberturaDN INSTANCIA = new ExclusionRecargoVarioCoberturaDN();

	public static ExclusionRecargoVarioCoberturaDN getInstancia() {
		return ExclusionRecargoVarioCoberturaDN.INSTANCIA;
	}

	public List<ExclusionRecargoVarioCoberturaDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		return exclusionRecargoVarioCoberturaSN.listarTodos();
	}

	public void agregar(ExclusionRecargoVarioCoberturaDTO exclusionRecargoVarioCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		exclusionRecargoVarioCoberturaSN.agregar(exclusionRecargoVarioCoberturaDTO);
	}

	public void modificar(ExclusionRecargoVarioCoberturaDTO exclusionRecargoVarioCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		exclusionRecargoVarioCoberturaSN.modificar(exclusionRecargoVarioCoberturaDTO);
	}

	public ExclusionRecargoVarioCoberturaDTO getPorId(ExclusionRecargoVarioCoberturaDTO exclusionRecargoVarioCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		return exclusionRecargoVarioCoberturaSN.getPorId(exclusionRecargoVarioCoberturaDTO.getId());
	}

	public void borrar(ExclusionRecargoVarioCoberturaDTO exclusionRecargoVarioCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		exclusionRecargoVarioCoberturaSN.borrar(exclusionRecargoVarioCoberturaDTO);
	}
	
	public List<ExclusionRecargoVarioCoberturaDTO> buscarPorPropiedad (String propiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {	
		return new ExclusionRecargoVarioCoberturaSN().buscarPorPropiedad(propiedad, valor);
	}

	public void excluirRecargoVarioCobertura(
			CoberturaDTO coberturaDTO,
			ExclusionRecargoVarioCoberturaDTO exclusionRecargoVarioCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		exclusionRecargoVarioCoberturaSN.agregar(exclusionRecargoVarioCoberturaDTO);
	}

	public void eliminarExclusion(
			CoberturaDTO coberturaDTO,
			ExclusionRecargoVarioCoberturaDTO exclusionRecargoVarioCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		exclusionRecargoVarioCoberturaSN.borrar(exclusionRecargoVarioCoberturaDTO);
	}
}
