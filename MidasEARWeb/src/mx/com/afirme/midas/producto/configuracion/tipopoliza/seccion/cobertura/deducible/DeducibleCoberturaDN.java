package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DeducibleCoberturaDN {
	private static final DeducibleCoberturaDN INSTANCIA = new DeducibleCoberturaDN();

	public static DeducibleCoberturaDN getInstancia() {
		return DeducibleCoberturaDN.INSTANCIA;
	}

	public List<DeducibleCoberturaDTO> listarDeducibles(BigDecimal idToCobertura)
			throws SystemException, ExcepcionDeAccesoADatos {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		return deducibleCoberturaSN.listarDeducibles(idToCobertura);
	}

	public void agregar(DeducibleCoberturaDTO deducibleCoberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		deducibleCoberturaSN.guardar(deducibleCoberturaDTO);
	}

	public void modificar(DeducibleCoberturaDTO deducibleCoberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		deducibleCoberturaSN.actualizar(deducibleCoberturaDTO);
	}

	public void borrar(DeducibleCoberturaDTO deducibleCoberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		deducibleCoberturaSN.borrar(deducibleCoberturaDTO);
	}

}
