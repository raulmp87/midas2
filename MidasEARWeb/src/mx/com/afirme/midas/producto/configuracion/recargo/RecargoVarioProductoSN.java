package mx.com.afirme.midas.producto.configuracion.recargo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RecargoVarioProductoSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private RecargoVarioProductoFacadeRemote beanRemoto;

	public RecargoVarioProductoSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(RecargoVarioProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(RecargoVarioProductoDTO recargoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(recargoPorProductoDTO);
		return null;
	}

	public String actualizar(RecargoVarioProductoDTO recargoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(recargoPorProductoDTO);
		return null;
	}

	public String borrar(RecargoVarioProductoDTO recargoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(recargoPorProductoDTO);
		return null;
	}

	public List<RecargoVarioProductoDTO> listarRecargoAsociado(
			BigDecimal idToProducto) {
		return beanRemoto.findByProperty("id.idToProducto", idToProducto);
	}
}
