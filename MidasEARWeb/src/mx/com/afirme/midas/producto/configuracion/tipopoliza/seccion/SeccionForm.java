package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion;

import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SeccionForm extends MidasBaseForm {

	/**
	 */
	private static final long serialVersionUID = 1L;
	private String idToSeccion;
	private String codigo;
	private String version;
	private String descripcion;
	private String nombreComercial;
	private String claveImpresionEncabezado;
	private String claveBienSeccion;
	private String claveImpresionSumaAsegurada;
	private String claveImpresionPrimaNeta;
	private String numeroSecuencia;
	private String claveObligatoriedad;
	/*private String claveActivoConfiguracion="1";
	private String claveActivoProduccion="0";*/
	private String idToTipoPoliza;
	private String nombreComercialTipoPoliza;
	private String claveSubIncisos;
	private String descripcionBienSeccion;
	private String descripcionObligatoriedad;
	private List<CoberturaSeccionDTO> coberturaSeccionAsociadas;
	private List<CoberturaDTO> coberturasPorAsociar;
	private List<CoberturaDTO> coberturasAsociadas;
	
	private List<RamoSeccionDTO> ramoSeccionAsociados;
	private List<RamoDTO> ramosPorAsociar;
	private List<RamoDTO> ramosAsociados;
	
	private List<SeccionDTO> seccionesRequeridas;
	private List<SeccionDTO> seccionesNoRequeridas;
	private String claveLuc;
	private String clavePrimerRiesgo;
	private String claveDependenciaOtrasPr;
	private String claveEdificioContenido;
	private String descripcionEdificioContenido;

	
	private String fechaInicioVigencia;
	private String claveEstatus = "1";
	private String descripcionEstatus;
	private String diasGracia;
	private String diasGraciaSubsecuentes;
	private String diasRetroactividad;
	private String diasDiferimiento;
	private String porcentajeMaximoBonificacion;
	private String porcentajeMaximoComision;
	private String porcentajeMaximoSobrecomision; 
	private TipoVehiculoForm tipoVehiculoForm=new TipoVehiculoForm();
	private String id_claveTipoBien;
    private String descripcionTipoVehiculo;
    private String descripcionTipoBien;
    
    private String fechaRegistro;
    private String numeroRegistro;
    private String tipoValidacionNumSerie;
    private String descripcionTipoValidacionNumSerie;
	
	public String getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getClaveImpresionEncabezado() {
		return claveImpresionEncabezado;
	}

	public void setClaveImpresionEncabezado(String claveImpresionEncabezado) {
		this.claveImpresionEncabezado = claveImpresionEncabezado;
	}

	public String getClaveBienSeccion() {
		return claveBienSeccion;
	}

	public void setClaveBienSeccion(String claveBienSeccion) {
		this.claveBienSeccion = claveBienSeccion;
	}

	public String getClaveImpresionSumaAsegurada() {
		return claveImpresionSumaAsegurada;
	}

	public void setClaveImpresionSumaAsegurada(
			String claveImpresionSumaAsegurada) {
		this.claveImpresionSumaAsegurada = claveImpresionSumaAsegurada;
	}

	public String getClaveImpresionPrimaNeta() {
		return claveImpresionPrimaNeta;
	}

	public void setClaveImpresionPrimaNeta(String claveImpresionPrimaNeta) {
		this.claveImpresionPrimaNeta = claveImpresionPrimaNeta;
	}

	public String getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(String numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public String getClaveObligatoriedad() {
		return claveObligatoriedad;
	}

	public void setClaveObligatoriedad(String claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	/*public String getClaveActivoConfiguracion() {
		return claveActivoConfiguracion;
	}
	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}
	public String getClaveActivoProduccion() {
		return claveActivoProduccion;
	}
	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/

	public String getIdToTipoPoliza() {
		return idToTipoPoliza;
	}

	public void setIdToTipoPoliza(String idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}

	public String getNombreComercialTipoPoliza() {
		return nombreComercialTipoPoliza;
	}

	public void setNombreComercialTipoPoliza(String nombreComercialTipoPoliza) {
		this.nombreComercialTipoPoliza = nombreComercialTipoPoliza;
	}

	public List<CoberturaSeccionDTO> getCoberturaSeccionAsociadas() {
		return coberturaSeccionAsociadas;
	}

	public void setCoberturaSeccionAsociadas(
			List<CoberturaSeccionDTO> coberturaSeccionAsociadas) {
		this.coberturaSeccionAsociadas = coberturaSeccionAsociadas;
	}

	public List<CoberturaDTO> getCoberturasPorAsociar() {
		return coberturasPorAsociar;
	}

	public void setCoberturasPorAsociar(List<CoberturaDTO> coberturasPorAsociar) {
		this.coberturasPorAsociar = coberturasPorAsociar;
	}

	public List<CoberturaDTO> getCoberturasAsociadas() {
		return coberturasAsociadas;
	}

	public void setCoberturasAsociadas(List<CoberturaDTO> coberturasAsociadas) {
		this.coberturasAsociadas = coberturasAsociadas;
	}

	public List<RamoSeccionDTO> getRamoSeccionAsociados() {
		return ramoSeccionAsociados;
	}

	public void setRamoSeccionAsociados(List<RamoSeccionDTO> ramoSeccionAsociados) {
		this.ramoSeccionAsociados = ramoSeccionAsociados;
	}

	public List<RamoDTO> getRamosPorAsociar() {
		return ramosPorAsociar;
	}

	public void setRamosPorAsociar(List<RamoDTO> ramosPorAsociar) {
		this.ramosPorAsociar = ramosPorAsociar;
	}

	public List<RamoDTO> getRamosAsociados() {
		return ramosAsociados;
	}

	public void setRamosAsociados(List<RamoDTO> ramosAsociados) {
		this.ramosAsociados = ramosAsociados;
	}

	public List<SeccionDTO> getSeccionesRequeridas() {
		return seccionesRequeridas;
	}

	public void setSeccionesRequeridas(List<SeccionDTO> seccionesRequeridas) {
		this.seccionesRequeridas = seccionesRequeridas;
	}

	public List<SeccionDTO> getSeccionesNoRequeridas() {
		return seccionesNoRequeridas;
	}

	public void setSeccionesNoRequeridas(List<SeccionDTO> seccionesNoRequeridas) {
		this.seccionesNoRequeridas = seccionesNoRequeridas;
	}
	public String getClaveSubIncisos() {
		return claveSubIncisos;
	}
	public void setClaveSubIncisos(String claveSubIncisos) {
		this.claveSubIncisos = claveSubIncisos;
	}
	public String getDescripcionBienSeccion() {
		return descripcionBienSeccion;
	}
	public void setDescripcionBienSeccion(String descripcionBienSeccion) {
		this.descripcionBienSeccion = descripcionBienSeccion;
	}
	public String getDescripcionObligatoriedad() {
		return descripcionObligatoriedad;
	}
	public void setDescripcionObligatoriedad(String descripcionObligatoriedad) {
		this.descripcionObligatoriedad = descripcionObligatoriedad;
	}

	public String getClaveLuc() {
		return claveLuc;
	}

	public void setClaveLuc(String claveLuc) {
		this.claveLuc = claveLuc;
	}

	public String getClavePrimerRiesgo() {
		return clavePrimerRiesgo;
	}

	public void setClavePrimerRiesgo(String clavePrimerRiesgo) {
		this.clavePrimerRiesgo = clavePrimerRiesgo;
	}

	public String getClaveDependenciaOtrasPr() {
		return claveDependenciaOtrasPr;
	}

	public void setClaveDependenciaOtrasPr(String claveDependenciaOtrasPr) {
		this.claveDependenciaOtrasPr = claveDependenciaOtrasPr;
	}

	public void setClaveEdificioContenido(String claveEdificioContenido) {
		this.claveEdificioContenido = claveEdificioContenido;
	}

	public String getClaveEdificioContenido() {
		return claveEdificioContenido;
	}

	public void setDescripcionEdificioContenido(
			String descripcionEdificioContenido) {
		this.descripcionEdificioContenido = descripcionEdificioContenido;
	}

	public String getDescripcionEdificioContenido() {
		return descripcionEdificioContenido;
	}

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}


	public String getDiasGracia() {
		return diasGracia;
	}

	public void setDiasGracia(String diasGracia) {
		this.diasGracia = diasGracia;
	}

	public String getDiasGraciaSubsecuentes() {
		return diasGraciaSubsecuentes;
	}

	public void setDiasGraciaSubsecuentes(String diasGraciaSubsecuentes) {
		this.diasGraciaSubsecuentes = diasGraciaSubsecuentes;
	}

	public String getDiasRetroactividad() {
		return diasRetroactividad;
	}

	public void setDiasRetroactividad(String diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	public String getDiasDiferimiento() {
		return diasDiferimiento;
	}

	public void setDiasDiferimiento(String diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	public String getPorcentajeMaximoBonificacion() {
		return porcentajeMaximoBonificacion;
	}

	public void setPorcentajeMaximoBonificacion(String porcentajeMaximoBonificacion) {
		this.porcentajeMaximoBonificacion = porcentajeMaximoBonificacion;
	}

	public String getPorcentajeMaximoComision() {
		return porcentajeMaximoComision;
	}

	public void setPorcentajeMaximoComision(String porcentajeMaximoComision) {
		this.porcentajeMaximoComision = porcentajeMaximoComision;
	}

	public String getPorcentajeMaximoSobrecomision() {
		return porcentajeMaximoSobrecomision;
	}

	public void setPorcentajeMaximoSobrecomision(
			String porcentajeMaximoSobrecomision) {
		this.porcentajeMaximoSobrecomision = porcentajeMaximoSobrecomision;
	}

	public TipoVehiculoForm getTipoVehiculoForm() {
		return tipoVehiculoForm;
	}

	public void setTipoVehiculoForm(TipoVehiculoForm tipoVehiculoForm) {
		this.tipoVehiculoForm = tipoVehiculoForm;
	}

	public String getId_claveTipoBien() {
		return id_claveTipoBien;
	}

	public void setId_claveTipoBien(String id_claveTipoBien) {
		this.id_claveTipoBien = id_claveTipoBien;
	}

	public String getDescripcionTipoVehiculo() {
		return descripcionTipoVehiculo;
	}

	public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
		this.descripcionTipoVehiculo = descripcionTipoVehiculo;
	}

	public void setDescripcionTipoBien(String descripcionTipoBien) {
		this.descripcionTipoBien = descripcionTipoBien;
	}

	public String getDescripcionTipoBien() {
		return descripcionTipoBien;
	}

	/**
	 * @return el fechaRegistro
	 */
	public String getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro el fechaRegistro a establecer
	 */
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return el numeroRegistro
	 */
	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	/**
	 * @param numeroRegistro el numeroRegistro a establecer
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public String getTipoValidacionNumSerie() {
		return tipoValidacionNumSerie;
	}

	public void setTipoValidacionNumSerie(String tipoValidacionNumSerie) {
		this.tipoValidacionNumSerie = tipoValidacionNumSerie;
	}

	public String getDescripcionTipoValidacionNumSerie() {
		return descripcionTipoValidacionNumSerie;
	}

	public void setDescripcionTipoValidacionNumSerie(
			String descripcionTipoValidacionNumSerie) {
		this.descripcionTipoValidacionNumSerie = descripcionTipoValidacionNumSerie;
	}
}
