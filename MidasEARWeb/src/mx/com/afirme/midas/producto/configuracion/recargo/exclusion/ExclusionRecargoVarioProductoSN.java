package mx.com.afirme.midas.producto.configuracion.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ExclusionRecargoVarioProductoSN {
	private ExclusionRecargoVarioProductoFacadeRemote beanRemoto;

	public ExclusionRecargoVarioProductoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExcRecargoPorProductoSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(ExclusionRecargoVarioProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionRecargoVarioProductoDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(
			ExclusionRecargoVarioProductoDTO excRecargoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excRecargoPorProductoDTO);
	}

	public void modificar(
			ExclusionRecargoVarioProductoDTO excRecargoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excRecargoPorProductoDTO);
	}

	public ExclusionRecargoVarioProductoDTO getPorId(
			ExclusionRecargoVarioProductoId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionRecargoVarioProductoDTO excRecargoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excRecargoPorProductoDTO);
	}

	public List<ExclusionRecargoVarioProductoDTO> buscarPorPropiedad(
			String nombrePropiedad, Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<ExclusionRecargoVarioProductoDTO> buscarVigentesPorIdProducto(BigDecimal idToProducto){
		return beanRemoto.getVigentesPorIdProducto(idToProducto);
	}
}
