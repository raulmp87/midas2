package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Fernando Alonzo
 * @since 20 de Agosto de 2009
 */
public class ExclusionCoberturaSN {
	private CoberturaExcluidaFacadeRemote beanRemoto;

	public ExclusionCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionCoberturaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CoberturaExcluidaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CoberturaExcluidaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(CoberturaExcluidaDTO coberturaExcluidaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(coberturaExcluidaDTO);
	}

	public void modificar(CoberturaExcluidaDTO coberturaExcluidaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(coberturaExcluidaDTO);
	}

	public CoberturaExcluidaDTO getPorId(CoberturaExcluidaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(CoberturaExcluidaDTO coberturaExcluidaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(coberturaExcluidaDTO);
	}

	public List<CoberturaExcluidaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
}
