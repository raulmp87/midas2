package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoaseguroCoberturaDN {
	private static final CoaseguroCoberturaDN INSTANCIA = new CoaseguroCoberturaDN();

	public static CoaseguroCoberturaDN getInstancia() {
		return CoaseguroCoberturaDN.INSTANCIA;
	}

	public List<CoaseguroCoberturaDTO> listarCoaseguros(BigDecimal idToCobertura)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		return coaseguroCoberturaSN.listarCoaseguros(idToCobertura);
	}

	public void agregar(CoaseguroCoberturaDTO coaseguroCoberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		coaseguroCoberturaSN.guardar(coaseguroCoberturaDTO);
	}

	public void modificar(CoaseguroCoberturaDTO coaseguroCoberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		coaseguroCoberturaSN.actualizar(coaseguroCoberturaDTO);
	}

	public void borrar(CoaseguroCoberturaDTO coaseguroCoberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		coaseguroCoberturaSN.borrar(coaseguroCoberturaDTO);
	}
}
