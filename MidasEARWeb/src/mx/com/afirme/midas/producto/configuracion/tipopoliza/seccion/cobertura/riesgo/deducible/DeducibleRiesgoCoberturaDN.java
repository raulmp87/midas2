package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 21 de Agosto de 2009
 */
public class DeducibleRiesgoCoberturaDN {
	private static final DeducibleRiesgoCoberturaDN INSTANCIA = new DeducibleRiesgoCoberturaDN();

	public static DeducibleRiesgoCoberturaDN getInstancia() {
		return DeducibleRiesgoCoberturaDN.INSTANCIA;
	}

	public List<DeducibleRiesgoCoberturaDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new DeducibleRiesgoCoberturaSN().listarTodos();
	}

	public void agregar(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DeducibleRiesgoCoberturaSN().agregar(deducibleRiesgoCoberturaDTO);
	}

	public void modificar(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DeducibleRiesgoCoberturaSN().modificar(deducibleRiesgoCoberturaDTO);
	}

	public DeducibleRiesgoCoberturaDTO getPorId(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new DeducibleRiesgoCoberturaSN().getPorId(deducibleRiesgoCoberturaDTO.getId());
	}

	public void borrar(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new DeducibleRiesgoCoberturaSN().borrar(deducibleRiesgoCoberturaDTO);
	}

	/**
	 * Sincroniza los deducibles del Riesgo con los de la Cobertura
	 * @param idToRiesgo
	 * @param idToCobertura
	 * @param idToSeccion
	 * @throws SystemException
	 */
	public void sincronizarDeduciblesRiesgoCobertura(BigDecimal idToRiesgo, BigDecimal idToCobertura, BigDecimal idToSeccion) throws SystemException {
		new DeducibleRiesgoCoberturaSN().sincronizarDeduciblesRiesgoCobertura(idToRiesgo, idToCobertura, idToSeccion);
	}

	public List<DeducibleRiesgoCoberturaDTO> listarFiltrado(
			DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws SystemException {
		return new DeducibleRiesgoCoberturaSN().listarFiltrado(deducibleRiesgoCoberturaDTO);
	}
}
