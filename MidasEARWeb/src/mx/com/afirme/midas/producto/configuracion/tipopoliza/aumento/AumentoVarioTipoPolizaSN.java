package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AumentoVarioTipoPolizaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private AumentoVarioTipoPolizaFacadeRemote beanRemoto;

	public AumentoVarioTipoPolizaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(AumentoVarioTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(AumentoVarioTipoPolizaDTO aumentoPorTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(aumentoPorTipoPolizaDTO);
		return null;
	}

	public String actualizar(AumentoVarioTipoPolizaDTO aumentoPorTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(aumentoPorTipoPolizaDTO);
		return null;
	}

	public String borrar(AumentoVarioTipoPolizaDTO aumentoPorTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(aumentoPorTipoPolizaDTO);
		return null;
	}

	public List<AumentoVarioTipoPolizaDTO> listarAumentoAsociado(BigDecimal idToTipoPoliza) {
		return beanRemoto.findByProperty("id.idtotipopoliza", idToTipoPoliza);
	}
}
