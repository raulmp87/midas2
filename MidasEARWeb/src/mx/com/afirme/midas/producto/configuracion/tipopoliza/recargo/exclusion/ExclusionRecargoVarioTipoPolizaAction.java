package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 17 de Agosto de 2009
 */
public class ExclusionRecargoVarioTipoPolizaAction extends TipoPolizaAction{
	/**
	 * Method mostrarExclusionRecargo
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionRecargo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id");
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(BigDecimal.valueOf(Double.valueOf(id)));
		try {
			tipoPolizaDTO = new TipoPolizaDN().getPorId(tipoPolizaDTO);
			tipoPolizaForm.setIdToTipoPoliza(id);
			//Listas para el filtrado de exlusion de descuentos
			tipoPolizaForm.setCoberturas(CoberturaDN.getInstancia().listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza()));
			tipoPolizaForm.setRecargosAsociados(new RecargoVarioTipoPolizaSN().listarRecargoAsociado(tipoPolizaDTO.getIdToTipoPoliza()));
			tipoPolizaForm.setClaveTMPCombo("");
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcRecargoTipoPolizaAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcRecargoTipoPolizaAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id").toString();
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
		tipoPolizaDTO = tipoPolizaDN.getPorId(tipoPolizaDTO);
		tipoPolizaForm.setExcRecargoTipoPolizaAsociados(tipoPolizaDN.listarExcRecargoTipoPolizaAsociadas(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getExcRecargoTipoPolizaAsociados()!= null && tipoPolizaForm.getExcRecargoTipoPolizaAsociados().size() > 0) {
//			for(ExclusionRecargoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcRecargoTipoPolizaAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtocobertura() +"|"+id+ "\",data:[";
//				json += actual.getRecargoVarioDTO().getClavetiporecargo() + ",\"";
//				json += actual.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += actual.getCoberturaDTO().getCodigo() + "\",\"";
//				json += actual.getCoberturaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getExcRecargoTipoPolizaAsociados()!= null && tipoPolizaForm.getExcRecargoTipoPolizaAsociados().size() > 0) {
			for(ExclusionRecargoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcRecargoTipoPolizaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtocobertura() +"|"+id);
				row.setDatos(
						actual.getRecargoVarioDTO().getClavetiporecargo().toString(),
						actual.getRecargoVarioDTO().getDescripcionrecargo(),
						actual.getCoberturaDTO().getCodigo(),
						actual.getCoberturaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Exclusion Recargo TipoPoliza asociados: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcRecargoTipoPolizaAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcRecargoTipoPolizaAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaDN tipoPolizaDN = TipoPolizaDN.getInstancia(); 
		RecargoVarioDN RecargoDN = new RecargoVarioDN();
		ExclusionRecargoVarioTipoPolizaId id = new ExclusionRecargoVarioTipoPolizaId();
		CoberturaDN coberturaDN = new CoberturaDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		
		try {
			id.setIdtorecargovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtotipopoliza(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionRecargoVarioTipoPolizaDTO excRecargoTipoPoliza = new ExclusionRecargoVarioTipoPolizaDTO();
			excRecargoTipoPoliza.setId(id);
			RecargoVarioDTO Recargo = new RecargoVarioDTO();
			Recargo.setIdtorecargovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			Recargo = RecargoDN.getPorId(Recargo);

			CoberturaDTO cobertura = new CoberturaDTO();
			cobertura.setIdToCobertura(UtileriasWeb.regresaBigDecimal(ids[1]));
			cobertura = coberturaDN.getPorId(cobertura);
			
			TipoPolizaDTO tipoPoliza = new  TipoPolizaDTO();
			tipoPoliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(ids[2]));
			tipoPoliza = tipoPolizaDN.getPorId(tipoPoliza);
			
			excRecargoTipoPoliza.setRecargoVarioDTO(Recargo);
			excRecargoTipoPoliza.setCoberturaDTO(cobertura);
			excRecargoTipoPoliza.setTipoPolizaDTO(tipoPoliza);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				tipoPolizaDN.ExcluirRecargoTipoPoliza(excRecargoTipoPoliza);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				tipoPolizaDN.EliminarExclusionRecargoTipoPoliza(excRecargoTipoPoliza);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcRecargoTipoPolizaPorAsociar
	 * 
	 * M�todo que recupera la lista de Recargos no asociados a un tipoPoliza
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcRecargoTipoPolizaPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id").toString();
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));

		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();
		//Posibles par�metros recibidos para hacer un filtrado por tipoPoliza o Descuento
		String idToTipoPoliza = request.getParameter("idToTipoPoliza");
		String idToDescuento = request.getParameter("idToDescuento");
		if (idToTipoPoliza != null)
			tipoPolizaForm.setExcRecargoTipoPolizaNoAsociados(tipoPolizaDN.listarExcRecargoTipoPolizaNoAsociadas(tipoPolizaDTO,UtileriasWeb.regresaBigDecimal(idToTipoPoliza)));
		else if (idToDescuento != null)
			tipoPolizaForm.setExcRecargoTipoPolizaNoAsociados(tipoPolizaDN.listarExcRecargoTipoPolizaNoAsociadas(UtileriasWeb.regresaBigDecimal(idToDescuento),tipoPolizaDTO));
		else
			tipoPolizaForm.setExcRecargoTipoPolizaNoAsociados(tipoPolizaDN.listarExcRecargoTipoPolizaNoAsociadas(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getExcRecargoTipoPolizaNoAsociados() != null && tipoPolizaForm.getExcRecargoTipoPolizaNoAsociados().size() > 0) {
//			for(ExclusionRecargoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcRecargoTipoPolizaNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtocobertura() +"|"+id+ "\",data:[";
//				json += actual.getRecargoVarioDTO().getClavetiporecargo() + ",\"";
//				json += actual.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += actual.getCoberturaDTO().getCodigo() + "\",\"";
//				json += actual.getCoberturaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getExcRecargoTipoPolizaNoAsociados() != null && tipoPolizaForm.getExcRecargoTipoPolizaNoAsociados().size() > 0) {
			for(ExclusionRecargoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcRecargoTipoPolizaNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtocobertura() +"|"+id);
				row.setDatos(
						actual.getRecargoVarioDTO().getClavetiporecargo().toString(),
						actual.getRecargoVarioDTO().getDescripcionrecargo(),
						actual.getCoberturaDTO().getCodigo(),
						actual.getCoberturaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Exclusion Recargo TipoPoliza no asociados: "+json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
