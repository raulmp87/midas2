package mx.com.afirme.midas.producto.configuracion.aumento.exclusion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDN;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.ProductoAction;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoForm;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

public class ExclusionAumentoVarioProductoAction extends ProductoAction{
	/**
	 * Method mostrarExclusionAumento
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionAumento(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id");
		productoForm.setIdToProducto(id);
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(BigDecimal.valueOf(Double.valueOf(id)));
		try {
			productoDTO = new ProductoDN().getPorId(productoDTO);
			//this.poblarDTO(productoForm, productoDTO);
			productoForm.setIdToProducto(productoDTO.getIdToProducto().toString());
			//Listas para el filtrado de exlusion de aumentos
			productoForm.setTiposPoliza(TipoPolizaDN.getInstancia().listarVigentesPorIdProducto(productoDTO.getIdToProducto()));
			productoForm.setAumentosAsociados(new AumentoVarioProductoSN().listarAumentoAsociado(productoDTO.getIdToProducto()));
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcAumentoProductoAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcAumentoProductoAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		ProductoForm productoForm = (ProductoForm) form;

		String id = request.getParameter("id").toString();
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		ProductoDN productoDN =  ProductoDN.getInstancia();
		productoForm.setAumentosAsociados(productoDN.getPorIdCascada(productoDTO).getAumentos());
		productoDTO = productoDN.getPorId(productoDTO);
		productoForm.setExcAumentoProductoAsociados(productoDN.listarExcAumentoProductoAsociadas(productoDTO));
//		String json = "{rows:[";
//		if(productoForm.getExcAumentoProductoAsociados()!= null && productoForm.getExcAumentoProductoAsociados().size() > 0) {
//			for(ExclusionAumentoVarioProductoDTO actual : productoForm.getExcAumentoProductoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id+ "\",data:[";
//				json += actual.getAumentoVarioDTO().getClaveTipoAumento() + ",\"";
//				json += actual.getAumentoVarioDTO().getDescripcionAumento() + "\",\"";
//				json += actual.getTipoPolizaDTO().getCodigo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getExcAumentoProductoAsociados()!= null && productoForm.getExcAumentoProductoAsociados().size() > 0) {
			for(ExclusionAumentoVarioProductoDTO actual : productoForm.getExcAumentoProductoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id);
				row.setDatos(
						actual.getAumentoVarioDTO().getClaveTipoAumento().toString(),
						actual.getAumentoVarioDTO().getDescripcionAumento(),
						actual.getTipoPolizaDTO().getCodigo(),
						actual.getTipoPolizaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcAumentoProductoAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcAumentoProductoAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoDN productoDN = ProductoDN.getInstancia(); 
		AumentoVarioDN aumentoDN = new AumentoVarioDN();
		ExclusionAumentoVarioProductoId id = new ExclusionAumentoVarioProductoId();
		TipoPolizaDN polizaDN = new TipoPolizaDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		try {
			id.setIdtoaumentovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtotipopoliza(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtoproducto(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionAumentoVarioProductoDTO excAumentoProducto = new ExclusionAumentoVarioProductoDTO();
			excAumentoProducto.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				AumentoVarioDTO aumento = new AumentoVarioDTO();
				aumento.setIdAumentoVario(UtileriasWeb.regresaBigDecimal(ids[0]));
				aumento = aumentoDN.getPorId(aumento);

				TipoPolizaDTO poliza = new TipoPolizaDTO();
				poliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(ids[1]));
				poliza = polizaDN.getPorId(poliza);
				
				ProductoDTO producto = new  ProductoDTO();
				producto.setIdToProducto(UtileriasWeb.regresaBigDecimal(ids[2]));
				producto = productoDN.getPorId(producto);
				
				excAumentoProducto.setAumentoVarioDTO(aumento);
				excAumentoProducto.setProductoDTO(producto);
				excAumentoProducto.setTipoPolizaDTO(poliza);

				productoDN.ExcluirAumentoProducto(excAumentoProducto);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				productoDN.EliminarExclusionAumentoProducto(excAumentoProducto);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcAumentoProductoPorAsociar
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcAumentoProductoPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id").toString();
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		//Posibles parámetros recibidos para hacer un filtrado por tipoPoliza o Aumento
		String idToTipoPoliza = request.getParameter("idToTipoPoliza");
		String idToAumento = request.getParameter("idToAumento");
		if (idToTipoPoliza != null)
			productoForm.setExcAumentoProductoNoAsociados(ProductoDN.getInstancia().listarExcAumentoProductoNoAsociadas(productoDTO,UtileriasWeb.regresaBigDecimal(idToTipoPoliza)));
		else if (idToAumento != null)
			productoForm.setExcAumentoProductoNoAsociados(ProductoDN.getInstancia().listarExcAumentoProductoNoAsociadas(UtileriasWeb.regresaBigDecimal(idToAumento),productoDTO));
		else
			productoForm.setExcAumentoProductoNoAsociados(ProductoDN.getInstancia().listarExcAumentoProductoNoAsociadas(productoDTO));
//		String json = "{rows:[";
//		if(productoForm.getExcAumentoProductoNoAsociados() != null && productoForm.getExcAumentoProductoNoAsociados().size() > 0) {
//			for(ExclusionAumentoVarioProductoDTO actual : productoForm.getExcAumentoProductoNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id+ "\",data:[";
//				json += actual.getAumentoVarioDTO().getClaveTipoAumento() + ",\"";
//				json += actual.getAumentoVarioDTO().getDescripcionAumento() + "\",\"";
//				json += actual.getTipoPolizaDTO().getCodigo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getExcAumentoProductoNoAsociados() != null && productoForm.getExcAumentoProductoNoAsociados().size() > 0) {
			for(ExclusionAumentoVarioProductoDTO actual : productoForm.getExcAumentoProductoNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id);
				row.setDatos(
						actual.getAumentoVarioDTO().getClaveTipoAumento().toString(),
						actual.getAumentoVarioDTO().getDescripcionAumento(),
						actual.getTipoPolizaDTO().getCodigo(),
						actual.getTipoPolizaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}		
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
