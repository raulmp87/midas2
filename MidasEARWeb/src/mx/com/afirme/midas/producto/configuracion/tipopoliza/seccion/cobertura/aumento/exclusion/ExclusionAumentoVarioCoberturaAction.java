package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDN;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 18 de Agosto de 2009
 */
public class ExclusionAumentoVarioCoberturaAction extends CoberturaAction{
	/**
	 * Method mostrarExclusionAumento
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionAumento(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		coberturaForm.setIdToCobertura(id);
		String idSeccion = request.getParameter("idToSeccion");
		if (idSeccion == null)
			idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		try {
			//Estas listas se utilizan para el filtrado de registros de exclusion por Riesgo y por Aumento|Descuento|Recargo
			coberturaForm.setRiesgosAsociados(RiesgoDN.getInstancia().listarRiesgosVigentesPorCoberturaSeccion(UtileriasWeb.regresaBigDecimal(coberturaForm.getIdToCobertura()), UtileriasWeb.regresaBigDecimal(idSeccion)));
			coberturaForm.setAumentosAsociados(new AumentoVarioCoberturaSN().listarAumentosAsociado(UtileriasWeb.regresaBigDecimal(id)));
			coberturaForm.setDescripcionSubRamo("");
		} catch (ExcepcionDeAccesoADatos e) {e.printStackTrace();
		} catch (SystemException e) {e.printStackTrace();}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcAumentoCoberturaAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcAumentoCoberturaAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id").toString();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		String idToSeccion = request.getParameter("idToSeccion");
		coberturaForm.setExcAumentoCoberturaAsociados(coberturaDN.listarExcAumentoCoberturaAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
//		String json = "{rows:[";
//		if(coberturaForm.getExcAumentoCoberturaAsociados()!= null && coberturaForm.getExcAumentoCoberturaAsociados().size() > 0) {
//			for(ExclusionAumentoVarioCoberturaDTO actual : coberturaForm.getExcAumentoCoberturaAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id+ "\",data:[";
//				json += actual.getAumentoVarioDTO().getClaveTipoAumento() + ",\"";
//				json += actual.getAumentoVarioDTO().getDescripcionAumento() + "\",\"";
//				json += actual.getRiesgoDTO().getCodigo() + "\",\"";
//				json += actual.getRiesgoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getExcAumentoCoberturaAsociados()!= null && coberturaForm.getExcAumentoCoberturaAsociados().size() > 0) {
			for(ExclusionAumentoVarioCoberturaDTO actual : coberturaForm.getExcAumentoCoberturaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id);
				row.setDatos(
						actual.getAumentoVarioDTO().getClaveTipoAumento().toString(),
						actual.getAumentoVarioDTO().getDescripcionAumento(),
						actual.getRiesgoDTO().getCodigo(),
						actual.getRiesgoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		
		response.setContentType("text/json");
		System.out.println("Exclusion Aumento Cobertura asociados: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcAumentoCoberturaAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcAumentoCoberturaAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaDN coberturaDN = CoberturaDN.getInstancia(); 
		AumentoVarioDN AumentoDN = new AumentoVarioDN();
		ExclusionAumentoVarioCoberturaId id = new ExclusionAumentoVarioCoberturaId();
		RiesgoDN riesgoDN = new RiesgoDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		try {
			id.setIdtoaumentovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtoriesgo(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionAumentoVarioCoberturaDTO excAumentoCobertura = new ExclusionAumentoVarioCoberturaDTO();
			excAumentoCobertura.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				AumentoVarioDTO Aumento = new AumentoVarioDTO();
				Aumento.setIdAumentoVario(UtileriasWeb.regresaBigDecimal(ids[0]));
				Aumento = AumentoDN.getPorId(Aumento);

				CoberturaDTO cobertura = new CoberturaDTO();
				cobertura.setIdToCobertura(UtileriasWeb.regresaBigDecimal(ids[2]));
				cobertura = coberturaDN.getPorId(cobertura);
				
				RiesgoDTO riesgo = new  RiesgoDTO();
				riesgo.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(ids[1]));
				riesgo = riesgoDN.getPorId(riesgo);
				
				excAumentoCobertura.setAumentoVarioDTO(Aumento);
				excAumentoCobertura.setCoberturaDTO(cobertura);
				excAumentoCobertura.setRiesgoDTO(riesgo);
				coberturaDN.ExcluirAumentoCobertura(excAumentoCobertura);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				coberturaDN.EliminarExclusionAumentoCobertura(excAumentoCobertura);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcAumentoCoberturaPorAsociar
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcAumentoCoberturaPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id").toString();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		String idToSeccion = request.getParameter("idToSeccion");
		//Posibles par�metros recibidos para hacer un filtrado por riesgo o Aumento
		String idToRiesgo = request.getParameter("idToRiesgo");
		String idToAumento = request.getParameter("idToAumento");
		if (idToRiesgo != null)
			coberturaForm.setExcAumentoCoberturaNoAsociados(coberturaDN.listarExcAumentoCoberturaNoAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion),UtileriasWeb.regresaBigDecimal(idToRiesgo)));
		else if (idToAumento != null)
			coberturaForm.setExcAumentoCoberturaNoAsociados(coberturaDN.listarExcAumentoCoberturaNoAsociadas(UtileriasWeb.regresaBigDecimal(idToAumento),coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
		else
			coberturaForm.setExcAumentoCoberturaNoAsociados(coberturaDN.listarExcAumentoCoberturaNoAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
//		String json = "{rows:[";
//		if(coberturaForm.getExcAumentoCoberturaNoAsociados() != null && coberturaForm.getExcAumentoCoberturaNoAsociados().size() > 0) {
//			for(ExclusionAumentoVarioCoberturaDTO actual : coberturaForm.getExcAumentoCoberturaNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id+ "\",data:[";
//				json += actual.getAumentoVarioDTO().getClaveTipoAumento() + ",\"";
//				json += actual.getAumentoVarioDTO().getDescripcionAumento() + "\",\"";
//				json += actual.getRiesgoDTO().getCodigo() + "\",\"";
//				json += actual.getRiesgoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getExcAumentoCoberturaNoAsociados() != null && coberturaForm.getExcAumentoCoberturaNoAsociados().size() > 0) {
			for(ExclusionAumentoVarioCoberturaDTO actual : coberturaForm.getExcAumentoCoberturaNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtoaumentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id);
				row.setDatos(
						actual.getAumentoVarioDTO().getClaveTipoAumento().toString(),
						actual.getAumentoVarioDTO().getDescripcionAumento(),
						actual.getRiesgoDTO().getCodigo(),
						actual.getRiesgoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Exclusion Aumento Cobertura no asociados: "+json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
