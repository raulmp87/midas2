package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Jos� Luis Arellano
 * @since 14 de agosto de 2009
 */
public class DescuentoVarioTipoPolizaSN {
	private DescuentoVarioTipoPolizaFacadeRemote beanRemoto;

	public DescuentoVarioTipoPolizaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(DescuentoVarioTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void guardar(DescuentoVarioTipoPolizaDTO descuentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(descuentoPorTipoPolizaDTO);
	}

	public void actualizar(DescuentoVarioTipoPolizaDTO descuentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(descuentoPorTipoPolizaDTO);
	}
	
	public DescuentoVarioTipoPolizaDTO getPorId(DescuentoVarioTipoPolizaId id){
		return beanRemoto.findById(id);
	}

	public void borrar(DescuentoVarioTipoPolizaDTO descuentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(descuentoPorTipoPolizaDTO);
	}

	public List<DescuentoVarioTipoPolizaDTO> listarDescuentoAsociado( BigDecimal idToTipoPoliza) {
		return beanRemoto.findByProperty("id.idtotipopoliza", idToTipoPoliza);
	}

	public List<DescuentoVarioTipoPolizaDTO> listarTodos() {
		return beanRemoto.findAll();
	}
}
