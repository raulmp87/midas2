package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioSN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoSN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion.ExclusionDescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion.ExclusionDescuentoVarioCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion.ExclusionDescuentoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.ExclusionCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoSN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaDN {
	private static final CoberturaDN INSTANCIA = new CoberturaDN();

	public static CoberturaDN getInstancia() {
		return CoberturaDN.INSTANCIA;
	}

	public List<CoberturaDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		return coberturaSN.listarTodos();
	}

	public void agregar(CoberturaDTO coberturaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		coberturaSN.agregar(coberturaDTO);
	}

	public void modificar(CoberturaDTO coberturaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		coberturaSN.modificar(coberturaDTO);
	}

	public CoberturaDTO getPorId(CoberturaDTO coberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		return coberturaSN.getPorId(coberturaDTO.getIdToCobertura());
	}
	
	public CoberturaDTO getPorId(BigDecimal idToCobertura) throws SystemException, ExcepcionDeAccesoADatos {
		return new CoberturaSN().getPorId(idToCobertura);
	}

	public void borrar(CoberturaDTO coberturaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		coberturaSN.borrar(coberturaDTO);
	}

	public List<CoberturaDTO> buscarPorPropiedad(String propiedad, Object valor)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		return coberturaSN.buscarPorPropiedad(propiedad, valor);
	}

	public List<CoberturaDTO> listarFiltrado(CoberturaDTO coberturaDTO, Boolean mostrarInactivos)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		return coberturaSN.listarFiltrado(coberturaDTO, mostrarInactivos);
	}
	
	public List<CoberturaDTO> getListarFiltrado(CoberturaDTO coberturaDTO, Boolean mostrarInactivos)
		throws SystemException, ExcepcionDeAccesoADatos {
			CoberturaSN coberturaSN = new CoberturaSN();
			return coberturaSN.getListarFiltrado(coberturaDTO, mostrarInactivos);
	}
	
	public List<CoberturaDTO>listarVigentesAutos() throws SystemException, ExcepcionDeAccesoADatos {
		return (new CoberturaSN().listarVigentesAutos());
	}

	public CoberturaDTO getPorIdCascada(CoberturaDTO coberturaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaDTO cobertura = this.getPorId(coberturaDTO);
		
		DescuentoVarioCoberturaSN descuentoVarioCoberturaSN = new DescuentoVarioCoberturaSN();
		cobertura.setDescuentos(descuentoVarioCoberturaSN.listarDescuentoAsociado(cobertura.getIdToCobertura()));
		
		AumentoVarioCoberturaSN aumentoVarioCoberturaSN = new AumentoVarioCoberturaSN();
		cobertura.setAumentos(aumentoVarioCoberturaSN.listarAumentosAsociado(cobertura.getIdToCobertura()));

		RecargoVarioCoberturaSN recargoVarioCoberturaSN = new RecargoVarioCoberturaSN();
		cobertura.setRecargos(recargoVarioCoberturaSN.listarRecargoAsociado(cobertura.getIdToCobertura()));

		ExclusionRecargoVarioCoberturaSN exclusionRecargoVarioCoberturaSN = new ExclusionRecargoVarioCoberturaSN();
		cobertura.setExclusionRecargoVarioCoberturaDTOs(exclusionRecargoVarioCoberturaSN.listarRecargosExcluidos(cobertura.getIdToCobertura()));

		ExclusionCoberturaSN exclusionCoberturaSN = new ExclusionCoberturaSN();
		cobertura.setCoberturasExcluidas(exclusionCoberturaSN.buscarPorPropiedad("id.idToCobertura", cobertura.getIdToCobertura()));

		CoberturaRequeridaSN coberturaRequeridaSN = new CoberturaRequeridaSN();
		cobertura.setCoberturasRequeridas(coberturaRequeridaSN.buscarPorPropiedad("id.idToCobertura", cobertura.getIdToCobertura()));

		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		cobertura.setDeducibles(deducibleCoberturaSN.listarDeducibles(cobertura.getIdToCobertura()));
		
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		cobertura.setCoaseguros(coaseguroCoberturaSN.listarCoaseguros(cobertura.getIdToCobertura()));

		return cobertura;

	}
	 
	
	public void borradoLogico(CoberturaDTO CoberturaDTO)throws SystemException, ExcepcionDeAccesoADatos {
		new CoberturaSN().borradoLogico(CoberturaDTO);
	}
	
	public List<CoberturaDTO>listarVigentes() throws SystemException, ExcepcionDeAccesoADatos {
		return (new CoberturaSN().listarVigentes());
	}

	public List<DescuentoDTO> listarDescuentoPorAsociar(
			CoberturaDTO coberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoSN descuentoSN = new DescuentoSN();
		return descuentoSN.listarDescuentosPorAsociarCobertura(coberturaDTO.getIdToCobertura());
	}

	public void asociarDescuento(CoberturaDTO coberturaDTO,
			DescuentoVarioCoberturaDTO descuentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DescuentoVarioCoberturaSN descuentoPorProductoSN = new DescuentoVarioCoberturaSN();
		descuentoPorProductoSN.guardar(descuentoPorCoberturaDTO);
	}

	public void actualizarAsociacion(CoberturaDTO coberturaDTO,
			DescuentoVarioCoberturaDTO descuentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DescuentoVarioCoberturaSN descuentoPorProductoSN = new DescuentoVarioCoberturaSN();
		descuentoPorProductoSN.actualizar(descuentoPorCoberturaDTO);
	}

	public void desasociarDescuento(CoberturaDTO coberturaDTO,
			DescuentoVarioCoberturaDTO descuentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DescuentoVarioCoberturaSN descuentoPorProductoSN = new DescuentoVarioCoberturaSN();
		descuentoPorProductoSN.borrar(descuentoPorCoberturaDTO);
	}

	public List<AumentoVarioDTO> listarAumentoPorAsociar(
			CoberturaDTO coberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.listarAumentosPorAsociarCobertura(coberturaDTO.getIdToCobertura());
	}

	public void asociarAumento(CoberturaDTO coberturaDTO,
			AumentoVarioCoberturaDTO aumentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioCoberturaSN aumentoPorProductoSN = new AumentoVarioCoberturaSN();
		aumentoPorProductoSN.guardar(aumentoPorCoberturaDTO);
	}

	public void actualizarAsociacion(CoberturaDTO coberturaDTO,
			AumentoVarioCoberturaDTO aumentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioCoberturaSN aumentoPorProductoSN = new AumentoVarioCoberturaSN();
		aumentoPorProductoSN.actualizar(aumentoPorCoberturaDTO);
	}

	public void desasociarAumento(CoberturaDTO coberturaDTO,
			AumentoVarioCoberturaDTO aumentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioCoberturaSN aumentoPorProductoSN = new AumentoVarioCoberturaSN();
		aumentoPorProductoSN.borrar(aumentoPorCoberturaDTO);
	}

	public List<AumentoVarioDTO> listarAumentoPorAsociarCobertura(
			CoberturaDTO coberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.listarAumentosPorAsociarCobertura(coberturaDTO.getIdToCobertura());
	}

	public List<RecargoVarioDTO> listarRecargoPorAsociarCobertura(
			CoberturaDTO coberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.listarRecargosPorAsociarCobertura(coberturaDTO.getIdToCobertura());
	}

	public void asociarRecargo(CoberturaDTO coberturaDTO,
			RecargoVarioCoberturaDTO recargoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioCoberturaSN recargoPorProductoSN = new RecargoVarioCoberturaSN();
		recargoPorProductoSN.guardar(recargoPorCoberturaDTO);
	}

	public void actualizarAsociacion(CoberturaDTO coberturaDTO,
			RecargoVarioCoberturaDTO recargoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioCoberturaSN recargoPorProductoSN = new RecargoVarioCoberturaSN();
		recargoPorProductoSN.actualizar(recargoPorCoberturaDTO);
	}

	public void desasociarRecargo(CoberturaDTO coberturaDTO,
			RecargoVarioCoberturaDTO recargoPorCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioCoberturaSN recargoPorProductoSN = new RecargoVarioCoberturaSN();
		recargoPorProductoSN.borrar(recargoPorCoberturaDTO);
	}
	
	/**
	 * M�todo listarRiesgosAsociados. Lista los registros de RiesgoDTO asociados a una Cobertura.
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que est�n asociados los registros RiesgoDTO.
	 * @return List<RiesgoDTO>. La lista de registros RiesgoDTO asociados.
	 */
	public List<RiesgoCoberturaDTO> listarRiesgosAsociados(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException{
		return new RiesgoCoberturaSN().listarRiesgoAsociado(coberturaDTO.getIdToCobertura(),idToSeccion);
	}
	
	/**
	 * M�todo listarRiesgosAsociados. Lista los registros de RiesgoDTO asociados a una Cobertura.
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que est�n asociados los registros RiesgoDTO.
	 * @return List<RiesgoDTO>. La lista de registros RiesgoDTO asociados.
	 */
	public List<RiesgoCoberturaDTO> listarRiesgosVigentesAsociados(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException{
		return new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(),idToSeccion);
	}
	
	/**
	 * M�todo listarRiesgoPorAsociar. Lista los registros de RiesgoDTO no asociados a una Cobertura.
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros RiesgoDTO.
	 * @return List<RiesgoDTO>. La lista de registros RiesgoDTO no asociados.
	 */
	public List<RiesgoDTO> listarRiesgoPorAsociar(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoSN riesgoSN = new RiesgoSN();
		return riesgoSN.listarRiesgosPorAsociarCobertura(coberturaDTO,idToSeccion);
	}
	
	/**
	 * M�todo asociarRiesgo. Relaciona a un riesgo con una cobertura, dando de alta un registro RiesgoCoberturaDTO.
	 * @param RiesgoCoberturaDTO riesgoCoberturaDTO el registro RiesgoCoberturaDTO que se da de alta.
	 */
	public void asociarRiesgo(RiesgoCoberturaDTO riesgoCoberturaDTO)throws SystemException, ExcepcionDeAccesoADatos {
		new RiesgoCoberturaSN().agregar(riesgoCoberturaDTO);
	}
	
	/**
	 * M�todo actualizarAsociacionRiesgo. Actualiza la asociaci�n entre un riesgo con una cobertura.
	 * @param RiesgoCoberturaDTO riesgoCoberturaDTO el registro RiesgoCoberturaDTO que se actualiza.
	 */
	public void actualizarAsociacionRiesgo(RiesgoCoberturaDTO riesgoCoberturaDTO)throws SystemException, ExcepcionDeAccesoADatos {
		new RiesgoCoberturaSN().modificar(riesgoCoberturaDTO);
	}
	
	/**
	 * M�todo desasociarRiesgo. Elimina la relaci�n entre un riesgo y una cobertura, eliminando el registro RiesgoCoberturaDTO.
	 * @param RiesgoCoberturaDTO riesgoCoberturaDTO el registro RiesgoCoberturaDTO que se elimina.
	 */
	public void desasociarRiesgo(RiesgoCoberturaDTO riesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RiesgoCoberturaSN().borrar(riesgoCoberturaDTO);
	}
	
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioCoberturaDTO asociados a una Cobertura
	 * 
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que est�n asociados los registros
	 * @return List<ExcAumentoVarioCoberturaDTO> la lista de ExcAumentoPorCoberturaDTO asociados al registro CoberturaDTO
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> listarExcAumentoCoberturaAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		/*List<ExclusionAumentoVarioCoberturaDTO> lista = new ExclusionAumentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		for(ExclusionAumentoVarioCoberturaDTO actual : lista){
			actual.setCoberturaDTO(coberturaDTO);
			actual.setAumentoVarioDTO(new AumentoVarioSN().getPorId(actual.getId().getIdtoaumentovario()));
			actual.setRiesgoDTO(new RiesgoSN().getPorId(actual.getId().getIdtoriesgo()));
		}
		return lista;*/
		return new ExclusionAumentoVarioCoberturaSN().buscarVigentesPorIdCoberturaIdSeccion(coberturaDTO.getIdToCobertura(), idToSeccion);
	}
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioCoberturaDTO asociados a una Cobertura
	 * 
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que est�n asociados los registros
	 * @return List<ExcDescuentoPorCoberturaDTO> la lista de ExcDescuentoPorCoberturaDTO asociados al registro CoberturaDTO
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> listarExcDescuentoCoberturaAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		/*List<ExclusionDescuentoVarioCoberturaDTO> lista = new ExclusionDescuentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		for(ExclusionDescuentoVarioCoberturaDTO actual : lista){
			actual.setCoberturaDTO(coberturaDTO);
			actual.setDescuentoDTO(new DescuentoSN().getPorId(actual.getId().getIdtodescuentovario()));
			actual.setRiesgoDTO(new RiesgoSN().getPorId(actual.getId().getIdtoriesgo()));
		}
		return lista;*/
		return new ExclusionDescuentoVarioCoberturaSN().buscarVigentesPorIdCoberturaIdSeccion(coberturaDTO.getIdToCobertura(),idToSeccion);
	}
	
	/**
	 * Encuentra los registros de ExclusionRecargoVarioCoberturaDTO asociados a una Cobertura 
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que est�n asociados los registros
	 * @return List<ExcRecargoPorCoberturaDTO> la lista de ExcRecargoPorCoberturaDTO asociados al registro CoberturaDTO
	 */
	public List<ExclusionRecargoVarioCoberturaDTO> listarExcRecargoCoberturaAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		/*List<ExclusionDescuentoVarioCoberturaDTO> lista = new ExclusionDescuentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		for(ExclusionDescuentoVarioCoberturaDTO actual : lista){
			actual.setCoberturaDTO(coberturaDTO);
			actual.setDescuentoDTO(new DescuentoSN().getPorId(actual.getId().getIdtodescuentovario()));
			actual.setRiesgoDTO(new RiesgoSN().getPorId(actual.getId().getIdtoriesgo()));
		}
		return lista;*/
		return new ExclusionRecargoVarioCoberturaSN().buscarVigentesPorIdCoberturaIdSeccion(coberturaDTO.getIdToCobertura(),idToSeccion);
	}
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioCoberturaDTO no asociados a una Cobertura
	 * 
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @return List<ExclusionDescuentoVarioCoberturaDTO> la lista de ExclusionDescuentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> listarExcDescuentoCoberturaNoAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		//obtener los riesgos asociados al tipo de p�liza
		//TODO Se deben listar s�lo los registros cuya secci�n corresponda a la de la cobertura actual
		//List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(), idToSeccion);
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgos.size(),Level.INFO, null);
		//obtener los descuentos asociados a la cobertura
		List<DescuentoVarioCoberturaDTO> descuentos = new DescuentoVarioCoberturaSN().listarDescuentoAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de descuentos asociados ==> " + descuentos.size(),Level.INFO, null);
		
		//obtener los registros de ExclusionDescuentoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionDescuentoVarioCoberturaDTO> listaExcDescuento = listarExcDescuentoCoberturaAsociadas(coberturaDTO, idToSeccion);//new ExclusionDescuentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioCoberturaDTO>(); 

		if (listaExcDescuento.size() < (riesgos.size() * descuentos.size())){
			for(DescuentoVarioCoberturaDTO descuento : descuentos){
				for(RiesgoCoberturaDTO riesgo : riesgos){
					ExclusionDescuentoVarioCoberturaId id = new ExclusionDescuentoVarioCoberturaId();
					id.setIdtocobertura(coberturaDTO.getIdToCobertura());
					id.setIdtodescuentovario(descuento.getId().getIdtodescuentovario());
					id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
					ExclusionDescuentoVarioCoberturaDTO exclusionDescuentoVarioCoberturaDTO = new ExclusionDescuentoVarioCoberturaDTO();
					exclusionDescuentoVarioCoberturaDTO.setId(id);
					//Si el registro no existe, agregarlo a la lista de los no existentes
					if (!listaExcDescuento.contains(exclusionDescuentoVarioCoberturaDTO)){
						exclusionDescuentoVarioCoberturaDTO.setCoberturaDTO(coberturaDTO);
						exclusionDescuentoVarioCoberturaDTO.setDescuentoDTO(descuento.getDescuentoDTO());
						exclusionDescuentoVarioCoberturaDTO.setRiesgoDTO(riesgo.getRiesgoDTO());
						listaResultado.add(exclusionDescuentoVarioCoberturaDTO);
					}
				}
			}
		}
		return listaResultado;
	}
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioCoberturaDTO no asociados a una Cobertura en base al Riesgo cuyo ID se recibe.
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idToRiesgo
	 * @return List<ExclusionDescuentoVarioCoberturaDTO> la lista de ExclusionDescuentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> listarExcDescuentoCoberturaNoAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion,BigDecimal idToRiesgo) throws SystemException, ExcepcionDeAccesoADatos{
		List<RiesgoCoberturaDTO> riesgos = new ArrayList<RiesgoCoberturaDTO>();//new RiesgoCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		RiesgoCoberturaId idRC = new RiesgoCoberturaId();
		idRC.setIdtocobertura(coberturaDTO.getIdToCobertura());
		idRC.setIdtoriesgo(idToRiesgo);
		idRC.setIdtoseccion(idToSeccion);
		riesgos.add(new RiesgoCoberturaSN().getPorId(idRC));
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgos.size(),Level.INFO, null);
		//obtener los descuentos asociados a la cobertura
		List<DescuentoVarioCoberturaDTO> descuentos = new DescuentoVarioCoberturaSN().listarDescuentoAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de descuentos asociados ==> " + descuentos.size(),Level.INFO, null);
		
		//obtener los registros de ExclusionDescuentoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionDescuentoVarioCoberturaDTO> listaExcDescuento = listarExcDescuentoCoberturaAsociadas(coberturaDTO, idToSeccion);//new ExclusionDescuentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioCoberturaDTO>(); 

		for(DescuentoVarioCoberturaDTO descuento : descuentos){
			for(RiesgoCoberturaDTO riesgo : riesgos){
				ExclusionDescuentoVarioCoberturaId id = new ExclusionDescuentoVarioCoberturaId();
				id.setIdtocobertura(coberturaDTO.getIdToCobertura());
				id.setIdtodescuentovario(descuento.getId().getIdtodescuentovario());
				id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
				ExclusionDescuentoVarioCoberturaDTO exclusionDescuentoVarioCoberturaDTO = new ExclusionDescuentoVarioCoberturaDTO();
				exclusionDescuentoVarioCoberturaDTO.setId(id);
				//Si el registro no existe, agregarlo a la lista de los no existentes
				if (!listaExcDescuento.contains(exclusionDescuentoVarioCoberturaDTO)){
					exclusionDescuentoVarioCoberturaDTO.setCoberturaDTO(coberturaDTO);
					exclusionDescuentoVarioCoberturaDTO.setDescuentoDTO(descuento.getDescuentoDTO());
					exclusionDescuentoVarioCoberturaDTO.setRiesgoDTO(riesgo.getRiesgoDTO());
					listaResultado.add(exclusionDescuentoVarioCoberturaDTO);
				}
			}
		}
		return listaResultado;
	}
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioCoberturaDTO no asociados a una Cobertura en base al descuento cuyo ID se recibe
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idToDescuento.
	 * @return List<ExclusionDescuentoVarioCoberturaDTO> la lista de ExclusionDescuentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> listarExcDescuentoCoberturaNoAsociadas(BigDecimal idToDescuento,CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		//obtener los riesgos asociados al tipo de p�liza
		//TODO Se deben listar s�lo los registros cuya secci�n corresponda a la de la cobertura actual
		//List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(), idToSeccion);
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgos.size(),Level.INFO, null);
		//obtener los descuentos asociados a la cobertura
		List<DescuentoVarioCoberturaDTO> descuentos = new DescuentoVarioCoberturaSN().listarDescuentoAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de descuentos asociados ==> " + descuentos.size(),Level.INFO, null);
		
		//obtener los registros de ExclusionDescuentoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionDescuentoVarioCoberturaDTO> listaExcDescuento = this.listarExcDescuentoCoberturaAsociadas(coberturaDTO, idToSeccion);// new ExclusionDescuentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioCoberturaDTO>(); 

		for(DescuentoVarioCoberturaDTO descuento : descuentos){
			if (descuento.getId().getIdtodescuentovario().compareTo(idToDescuento)==0){
				for(RiesgoCoberturaDTO riesgo : riesgos){
					ExclusionDescuentoVarioCoberturaId id = new ExclusionDescuentoVarioCoberturaId();
					id.setIdtocobertura(coberturaDTO.getIdToCobertura());
					id.setIdtodescuentovario(descuento.getId().getIdtodescuentovario());
					id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
					ExclusionDescuentoVarioCoberturaDTO exclusionDescuentoVarioCoberturaDTO = new ExclusionDescuentoVarioCoberturaDTO();
					exclusionDescuentoVarioCoberturaDTO.setId(id);
					//Si el registro no existe, agregarlo a la lista de los no existentes
					if (!listaExcDescuento.contains(exclusionDescuentoVarioCoberturaDTO)){
						exclusionDescuentoVarioCoberturaDTO.setCoberturaDTO(coberturaDTO);
						exclusionDescuentoVarioCoberturaDTO.setDescuentoDTO(descuento.getDescuentoDTO());
						exclusionDescuentoVarioCoberturaDTO.setRiesgoDTO(riesgo.getRiesgoDTO());
						listaResultado.add(exclusionDescuentoVarioCoberturaDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioCoberturaDTO no asociados a una Cobertura
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @return List<ExclusionAumentoVarioCoberturaDTO> la lista de ExclusionAumentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> listarExcAumentoCoberturaNoAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		//obtener los riesgos asociados al tipo de p�liza
		List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(), idToSeccion);//new RiesgoCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgos.size(),Level.INFO, null);
		//obtener los Aumento asociados a la cobertura
		List<AumentoVarioCoberturaDTO> aumentos = new AumentoVarioCoberturaSN().listarAumentosAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de aumentos asociados ==> " + aumentos.size(),Level.INFO, null);
		//obtener los registros de ExclusionAumentoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionAumentoVarioCoberturaDTO> listaExcAumento = listarExcAumentoCoberturaAsociadas(coberturaDTO, idToSeccion);//new ExclusionAumentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionAumentoVarioCoberturaDTO>(); 
		if (listaExcAumento.size()<(riesgos.size()*aumentos.size())){
			ExclusionAumentoVarioCoberturaId id;
			ExclusionAumentoVarioCoberturaDTO excAumentoDTO;
			AumentoVarioSN aumentoSN = new AumentoVarioSN();
			RiesgoSN riesgoSN = new RiesgoSN();
			coberturaDTO = new CoberturaSN().getPorId(coberturaDTO.getIdToCobertura());
			
			for(AumentoVarioCoberturaDTO aumento : aumentos){
				for(RiesgoCoberturaDTO riesgo : riesgos){
					id = new ExclusionAumentoVarioCoberturaId();
					id.setIdtocobertura(coberturaDTO.getIdToCobertura());
					id.setIdtoaumentovario(aumento.getId().getIdtoaumentovario());
					id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
					ExclusionAumentoVarioCoberturaDTO exclusionAumentoVarioCoberturaDTO = new ExclusionAumentoVarioCoberturaDTO();
					exclusionAumentoVarioCoberturaDTO.setId(id);
					//Si el registro no existe, agregarlo a la lista de los no existentes
					//if ((excAumentoDTO = excAumentoSN.buscarPorIDs(id.getIdtocobertura(), id.getIdtoaumentovario(), id.getIdtoriesgo())) == null){
					if (!listaExcAumento.contains(exclusionAumentoVarioCoberturaDTO)){
						excAumentoDTO = new ExclusionAumentoVarioCoberturaDTO();
						excAumentoDTO.setId(id);
						excAumentoDTO.setCoberturaDTO(coberturaDTO);
						excAumentoDTO.setAumentoVarioDTO(aumentoSN.getPorId(aumento.getId().getIdtoaumentovario()));
						excAumentoDTO.setRiesgoDTO(riesgoSN.getPorId(riesgo.getId().getIdtoriesgo()));
						listaResultado.add(excAumentoDTO);
					}
				}
			}
		}
		return listaResultado;
	}
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioCoberturaDTO no asociados a una Cobertura en base al Riesgo cuyo ID se recibe.
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idToRiesgo
	 * @return List<ExclusionAumentoVarioCoberturaDTO> la lista de ExclusionAumentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> listarExcAumentoCoberturaNoAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion,BigDecimal idToRiesgo) throws SystemException, ExcepcionDeAccesoADatos{
		//obtener los riesgos asociados al tipo de p�liza
		List<RiesgoCoberturaDTO> riesgos = new ArrayList<RiesgoCoberturaDTO>();//new RiesgoCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		RiesgoCoberturaId idRC = new RiesgoCoberturaId();
		idRC.setIdtocobertura(coberturaDTO.getIdToCobertura());
		idRC.setIdtoriesgo(idToRiesgo);
		idRC.setIdtoseccion(idToSeccion);
		riesgos.add(new RiesgoCoberturaSN().getPorId(idRC));
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgos.size(),Level.INFO, null);
		//obtener los Aumento asociados a la cobertura
		List<AumentoVarioCoberturaDTO> aumentos = new AumentoVarioCoberturaSN().listarAumentosAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de aumentos asociados ==> " + aumentos.size(),Level.INFO, null);
		//obtener los registros de ExclusionAumentoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionAumentoVarioCoberturaDTO> listaExcAumento = listarExcAumentoCoberturaAsociadas(coberturaDTO, idToSeccion);//new ExclusionAumentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionAumentoVarioCoberturaDTO>(); 
		ExclusionAumentoVarioCoberturaId id;
		ExclusionAumentoVarioCoberturaDTO excAumentoDTO;
		AumentoVarioSN aumentoSN = new AumentoVarioSN();
		RiesgoSN riesgoSN = new RiesgoSN();
		coberturaDTO = new CoberturaSN().getPorId(coberturaDTO.getIdToCobertura());
		
		for(AumentoVarioCoberturaDTO aumento : aumentos){
			for(RiesgoCoberturaDTO riesgo : riesgos){
				id = new ExclusionAumentoVarioCoberturaId();
				id.setIdtocobertura(coberturaDTO.getIdToCobertura());
				id.setIdtoaumentovario(aumento.getId().getIdtoaumentovario());
				id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
				ExclusionAumentoVarioCoberturaDTO exclusionAumentoVarioCoberturaDTO = new ExclusionAumentoVarioCoberturaDTO();
				exclusionAumentoVarioCoberturaDTO.setId(id);
				//Si el registro no existe, agregarlo a la lista de los no existentes
				//if ((excAumentoDTO = excAumentoSN.buscarPorIDs(id.getIdtocobertura(), id.getIdtoaumentovario(), id.getIdtoriesgo())) == null){
				if (!listaExcAumento.contains(exclusionAumentoVarioCoberturaDTO)){
					excAumentoDTO = new ExclusionAumentoVarioCoberturaDTO();
					excAumentoDTO.setId(id);
					excAumentoDTO.setCoberturaDTO(coberturaDTO);
					excAumentoDTO.setAumentoVarioDTO(aumentoSN.getPorId(aumento.getId().getIdtoaumentovario()));
					excAumentoDTO.setRiesgoDTO(riesgoSN.getPorId(riesgo.getId().getIdtoriesgo()));
					listaResultado.add(excAumentoDTO);
				}
			}
		}
		return listaResultado;
	}
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioCoberturaDTO no asociados a una Cobertura en base al aumento cuyo ID se recibe
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idToAumento
	 * @return List<ExclusionAumentoVarioCoberturaDTO> la lista de ExclusionAumentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> listarExcAumentoCoberturaNoAsociadas(BigDecimal idToAumento,CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos{
		//obtener los riesgos asociados al tipo de p�liza
		List<RiesgoCoberturaDTO> riesgos = new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(), idToSeccion);//new RiesgoCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgos.size(),Level.INFO, null);
		//obtener los Aumento asociados a la cobertura
		List<AumentoVarioCoberturaDTO> aumentos = new AumentoVarioCoberturaSN().listarAumentosAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de aumentos asociados ==> " + aumentos.size(),Level.INFO, null);
		//obtener los registros de ExclusionAumentoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionAumentoVarioCoberturaDTO> listaExcAumento = listarExcAumentoCoberturaAsociadas(coberturaDTO, idToSeccion);//new ExclusionAumentoVarioCoberturaSN().buscarPorPropiedad("id.idtocobertura", coberturaDTO.getIdToCobertura());
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionAumentoVarioCoberturaDTO>(); 
		ExclusionAumentoVarioCoberturaId id;
		ExclusionAumentoVarioCoberturaDTO excAumentoDTO;
		AumentoVarioSN aumentoSN = new AumentoVarioSN();
		RiesgoSN riesgoSN = new RiesgoSN();
		coberturaDTO = new CoberturaSN().getPorId(coberturaDTO.getIdToCobertura());
		
		for(AumentoVarioCoberturaDTO aumento : aumentos){
			if (aumento.getId().getIdtoaumentovario().compareTo(idToAumento) == 0){
				for(RiesgoCoberturaDTO riesgo : riesgos){
					id = new ExclusionAumentoVarioCoberturaId();
					id.setIdtocobertura(coberturaDTO.getIdToCobertura());
					id.setIdtoaumentovario(aumento.getId().getIdtoaumentovario());
					id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
					ExclusionAumentoVarioCoberturaDTO exclusionAumentoVarioCoberturaDTO = new ExclusionAumentoVarioCoberturaDTO();
					exclusionAumentoVarioCoberturaDTO.setId(id);
					//Si el registro no existe, agregarlo a la lista de los no existentes
					//if ((excAumentoDTO = excAumentoSN.buscarPorIDs(id.getIdtocobertura(), id.getIdtoaumentovario(), id.getIdtoriesgo())) == null){
					if (!listaExcAumento.contains(exclusionAumentoVarioCoberturaDTO)){
						excAumentoDTO = new ExclusionAumentoVarioCoberturaDTO();
						excAumentoDTO.setId(id);
						excAumentoDTO.setCoberturaDTO(coberturaDTO);
						excAumentoDTO.setAumentoVarioDTO(aumentoSN.getPorId(aumento.getId().getIdtoaumentovario()));
						excAumentoDTO.setRiesgoDTO(riesgoSN.getPorId(riesgo.getId().getIdtoriesgo()));
						listaResultado.add(excAumentoDTO);
					}
				}
			}
		}
		return listaResultado;
	}
	
	public void ExcluirAumentoCobertura(ExclusionAumentoVarioCoberturaDTO excAumentoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ExclusionAumentoVarioCoberturaSN().agregar(excAumentoCoberturaDTO);
	}
	
	public void ExcluirRecargoCobertura(ExclusionRecargoVarioCoberturaDTO excRecargoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ExclusionRecargoVarioCoberturaSN().agregar(excRecargoCoberturaDTO);
	}
	
	public void EliminarExclusionAumentoCobertura(ExclusionAumentoVarioCoberturaDTO excAumentoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ExclusionAumentoVarioCoberturaSN().borrar(excAumentoCoberturaDTO);
	}
	
	public void EliminarExclusionRecargoCobertura(ExclusionRecargoVarioCoberturaDTO excRecargoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ExclusionRecargoVarioCoberturaSN().borrar(excRecargoCoberturaDTO);
	}
	
	public void ExcluirDescuentoCobertura(ExclusionDescuentoVarioCoberturaDTO excDescuentoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ExclusionDescuentoVarioCoberturaSN().agregar(excDescuentoCoberturaDTO);
	}
	
	public void EliminarExclusionDescuentoCobertura(ExclusionDescuentoVarioCoberturaDTO excDescuentoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ExclusionDescuentoVarioCoberturaSN().borrar(excDescuentoCoberturaDTO);
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioCoberturaDTO no asociados a una Cobertura
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idToSeccion
	 * @return List<ExclusionAumentoVarioCoberturaDTO> la lista de ExclusionAumentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionRecargoVarioCoberturaDTO> listarExcRecargoCoberturaNoAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos {
		List<RiesgoCoberturaDTO> riesgosAsociados = RiesgoCoberturaDN.getInstancia().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(), idToSeccion);//buscarPorPropiedad("coberturaSeccionDTO.id.idtocobertura", coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgosAsociados.size(),Level.INFO, null);
		//obtener los Aumento asociados a la cobertura
		List<RecargoVarioCoberturaDTO> recargos = new RecargoVarioCoberturaSN().listarRecargoAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de recargos asociados ==> " + recargos.size(),Level.INFO, null);
	
		//obtener los registros de ExclusionRecargoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionRecargoVarioCoberturaDTO> listaExcRecargo = listarExcRecargoCoberturaAsociadas(coberturaDTO, idToSeccion);
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionRecargoVarioCoberturaDTO>();

		if (listaExcRecargo.size() < (riesgosAsociados.size() * recargos.size())){
			ExclusionRecargoVarioCoberturaId id;
			ExclusionRecargoVarioCoberturaDTO excRecargoDTO;
			coberturaDTO = new CoberturaSN().getPorId(coberturaDTO.getIdToCobertura());
			for(RecargoVarioCoberturaDTO recargo : recargos){
				for(RiesgoCoberturaDTO riesgo : riesgosAsociados){
					id = new ExclusionRecargoVarioCoberturaId();
					id.setIdtocobertura(coberturaDTO.getIdToCobertura());
					id.setIdtorecargovario(recargo.getId().getIdtorecargovario());
					id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
					excRecargoDTO = new ExclusionRecargoVarioCoberturaDTO();
					excRecargoDTO.setId(id);
					//Si el registro no existe, agregarlo a la lista de los no existentes
					if (!listaExcRecargo.contains(excRecargoDTO)){
						excRecargoDTO.setCoberturaDTO(coberturaDTO);
						excRecargoDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
						excRecargoDTO.setRiesgoDTO(riesgo.getRiesgoDTO());
						listaResultado.add(excRecargoDTO);
					}
				}
			}
		}
		return listaResultado;
	}
	
	/**
	 * Encuentra los registros de ExclusionRecargoVarioCoberturaDTO no asociados a una Cobertura en base al riesgo cuyo ID se recibe
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idTo Riesgo @param idToSeccion
	 * @return List<ExclusionAumentoVarioCoberturaDTO> la lista de ExclusionAumentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionRecargoVarioCoberturaDTO> listarExcRecargoCoberturaNoAsociadas(CoberturaDTO coberturaDTO,BigDecimal idToSeccion,BigDecimal idToRiesgo) throws SystemException, ExcepcionDeAccesoADatos {
		//obtener los riesgos asociados al tipo de p�liza
		List<RiesgoCoberturaDTO> riesgosAsociados = new ArrayList<RiesgoCoberturaDTO>();
		RiesgoCoberturaId idRC = new RiesgoCoberturaId();
		idRC.setIdtocobertura(coberturaDTO.getIdToCobertura());
		idRC.setIdtoriesgo(idToRiesgo);
		idRC.setIdtoseccion(idToSeccion);
		riesgosAsociados.add(new RiesgoCoberturaSN().getPorId(idRC));
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgosAsociados.size(),Level.INFO, null);
		//obtener los Aumento asociados a la cobertura
		List<RecargoVarioCoberturaDTO> recargos = new RecargoVarioCoberturaSN().listarRecargoAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de recargos asociados ==> " + recargos.size(),Level.INFO, null);
	
		//obtener los registros de ExclusionRecargoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionRecargoVarioCoberturaDTO> listaExcRecargo = listarExcRecargoCoberturaAsociadas(coberturaDTO, idToSeccion);
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionRecargoVarioCoberturaDTO>();

		ExclusionRecargoVarioCoberturaId id;
		ExclusionRecargoVarioCoberturaDTO excRecargoDTO;
		coberturaDTO = new CoberturaSN().getPorId(coberturaDTO.getIdToCobertura());
		for(RecargoVarioCoberturaDTO recargo : recargos){
			for(RiesgoCoberturaDTO riesgo : riesgosAsociados){
				id = new ExclusionRecargoVarioCoberturaId();
				id.setIdtocobertura(coberturaDTO.getIdToCobertura());
				id.setIdtorecargovario(recargo.getId().getIdtorecargovario());
				id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
				excRecargoDTO = new ExclusionRecargoVarioCoberturaDTO();
				excRecargoDTO.setId(id);
				//Si el registro no existe, agregarlo a la lista de los no existentes
				if (!listaExcRecargo.contains(excRecargoDTO)){
					excRecargoDTO.setCoberturaDTO(coberturaDTO);
					excRecargoDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
					excRecargoDTO.setRiesgoDTO(riesgo.getRiesgoDTO());
					listaResultado.add(excRecargoDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioCoberturaDTO no asociados a una Cobertura en base al Recargo cuyo ID se recibe
	 * @param CoberturaDTO coberturaDTO el registro CoberturaDTO al que no est�n asociados los registros
	 * @param idToSeccion @param idToRecargo
	 * @return List<ExclusionAumentoVarioCoberturaDTO> la lista de ExclusionAumentoVarioCoberturaDTO no asociados al registro coberturaDTO
	 */
	public List<ExclusionRecargoVarioCoberturaDTO> listarExcRecargoCoberturaNoAsociadas(BigDecimal idToRecargo,CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos {
		List<RiesgoCoberturaDTO> riesgosAsociados = RiesgoCoberturaDN.getInstancia().listarRiesgoVigenteAsociado(coberturaDTO.getIdToCobertura(), idToSeccion);//buscarPorPropiedad("coberturaSeccionDTO.id.idtocobertura", coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de riesgos asociados ==> " + riesgosAsociados.size(),Level.INFO, null);
		//obtener los Aumento asociados a la cobertura
		List<RecargoVarioCoberturaDTO> recargos = new RecargoVarioCoberturaSN().listarRecargoAsociado(coberturaDTO.getIdToCobertura());
		LogDeMidasWeb.log("Cobertura id:" + coberturaDTO.getIdToCobertura()+ " Numero de recargos asociados ==> " + recargos.size(),Level.INFO, null);
	
		//obtener los registros de ExclusionRecargoVarioCoberturaDTO asociados a la cobertura
		List<ExclusionRecargoVarioCoberturaDTO> listaExcRecargo = listarExcRecargoCoberturaAsociadas(coberturaDTO, idToSeccion);
		//Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioCoberturaDTO> listaResultado = new ArrayList<ExclusionRecargoVarioCoberturaDTO>();

		ExclusionRecargoVarioCoberturaId id;
		ExclusionRecargoVarioCoberturaDTO excRecargoDTO;
		coberturaDTO = new CoberturaSN().getPorId(coberturaDTO.getIdToCobertura());
		for(RecargoVarioCoberturaDTO recargo : recargos){
			if (recargo.getId().getIdtorecargovario().compareTo(idToRecargo) == 0){
				for(RiesgoCoberturaDTO riesgo : riesgosAsociados){
					id = new ExclusionRecargoVarioCoberturaId();
					id.setIdtocobertura(coberturaDTO.getIdToCobertura());
					id.setIdtorecargovario(recargo.getId().getIdtorecargovario());
					id.setIdtoriesgo(riesgo.getId().getIdtoriesgo());
					excRecargoDTO = new ExclusionRecargoVarioCoberturaDTO();
					excRecargoDTO.setId(id);
					//Si el registro no existe, agregarlo a la lista de los no existentes
					if (!listaExcRecargo.contains(excRecargoDTO)){
						excRecargoDTO.setCoberturaDTO(coberturaDTO);
						excRecargoDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
						excRecargoDTO.setRiesgoDTO(riesgo.getRiesgoDTO());
						listaResultado.add(excRecargoDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}
	
	public List<CoberturaDTO> listarCoberturasPorExcluir(CoberturaDTO coberturaDTO, BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSN coberturaSN = new CoberturaSN();
		return coberturaSN.listarCoberturasPorExcluir(coberturaDTO.getIdToCobertura(), idToSeccion);
	}

	public void excluyeCobertura(CoberturaExcluidaDTO coberturaExcluidaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionCoberturaSN exclusionCoberturaSN = new ExclusionCoberturaSN();
		exclusionCoberturaSN.agregar(coberturaExcluidaDTO);
	}

	public void borrarExclusionCobertura(
			CoberturaExcluidaDTO coberturaExcluidaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ExclusionCoberturaSN exclusionCoberturaSN = new ExclusionCoberturaSN();
		exclusionCoberturaSN.borrar(coberturaExcluidaDTO);
	}

	public List<CoberturaDTO> listarCoberturasNoRequeridas(
			CoberturaDTO coberturaDTO, BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaSN coberturaSN = new CoberturaSN();
		return coberturaSN.listarCoberturasNoExcluidas(coberturaDTO.getIdToCobertura(), idToSeccion);
	}

	public void asociaCoberturaRequerida(
			CoberturaRequeridaDTO coberturaRequeridaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaRequeridaSN coberturaRequeridaSN = new CoberturaRequeridaSN();
		coberturaRequeridaSN.agregar(coberturaRequeridaDTO);
	}

	public void borrarCoberturaRequerida(
			CoberturaRequeridaDTO coberturaRequeridaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaRequeridaSN coberturaRequeridaSN = new CoberturaRequeridaSN();
		coberturaRequeridaSN.borrar(coberturaRequeridaDTO);
	}

	public void agregarDeducible(CoberturaDTO coberturaDTO,
			DeducibleCoberturaDTO deducibleCoberturaDTO) throws SystemException {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		deducibleCoberturaSN.guardar(deducibleCoberturaDTO);
	}

	public void actualizarDeducible(CoberturaDTO coberturaDTO,
			DeducibleCoberturaDTO deducibleCoberturaDTO) throws SystemException {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		deducibleCoberturaSN.actualizar(deducibleCoberturaDTO);
	}

	public void eliminarDeducible(CoberturaDTO coberturaDTO,
			DeducibleCoberturaDTO deducibleCoberturaDTO) throws SystemException {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		deducibleCoberturaSN.borrar(deducibleCoberturaDTO);
	}

	public void agregarCoaseguro(CoberturaDTO coberturaDTO,
			CoaseguroCoberturaDTO coaseguroCoberturaDTO) throws SystemException {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		coaseguroCoberturaSN.guardar(coaseguroCoberturaDTO);
	}

	public void actualizarCoaseguro(CoberturaDTO coberturaDTO,
			CoaseguroCoberturaDTO coaseguroCoberturaDTO) throws SystemException {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		coaseguroCoberturaSN.actualizar(coaseguroCoberturaDTO);
	}

	public void eliminarCoaseguro(CoberturaDTO coberturaDTO,
			CoaseguroCoberturaDTO coaseguroCoberturaDTO) throws SystemException {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		coaseguroCoberturaSN.borrar(coaseguroCoberturaDTO);
	}

	public List<DocumentoAnexoCoberturaDTO> listarDocumentosAnexos(BigDecimal idToCobertura) throws ExcepcionDeAccesoADatos, SystemException{
		return DocumentoAnexoCoberturaDN.getInstancia().listarPorPropiedad("coberturaDTO.idToCobertura", idToCobertura);
	}
	
	public List<CoberturaDTO> listarVigentesPorTipoPoliza(BigDecimal idToTipoPoliza) throws ExcepcionDeAccesoADatos, SystemException{
		return new CoberturaSN().listarVigentesPorTipoPoliza(idToTipoPoliza);
	}

	public Boolean isCobeturaAsociada(BigDecimal idToCobertura) throws SystemException {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		return coberturaSeccionSN.isCobeturaAsociada(idToCobertura);
	}
	
	public BigDecimal nextNumeroSecuenciaCoaseguro(BigDecimal idToCobertura) throws SystemException {
		CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
		return coaseguroCoberturaSN.nextNumeroSecuencia(idToCobertura);
	}	
	
	public BigDecimal nextNumeroSecuenciaDeducible(BigDecimal idToCobertura) throws SystemException {
		DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
		return deducibleCoberturaSN.nextNumeroSecuencia(idToCobertura);
	}		
}
