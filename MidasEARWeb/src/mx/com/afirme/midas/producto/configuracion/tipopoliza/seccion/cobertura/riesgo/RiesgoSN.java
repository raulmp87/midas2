/**
 * 
 */
package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.service.catalogos.riesgo.RiesgoDTOService;

/**
 * @author admin
 * 
 */
public class RiesgoSN {
	private RiesgoFacadeRemote beanRemoto;
	private RiesgoDTOService riesgoDTOService;

	public RiesgoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RiesgoFacadeRemote.class);
			riesgoDTOService = serviceLocator.getEJB(RiesgoDTOService.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RiesgoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try{
			List<RiesgoDTO> list = beanRemoto.findAll();
			return list;
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void agregar(RiesgoDTO riesgoDTO) throws ExcepcionDeAccesoADatos {
		
		try{
			beanRemoto.save(riesgoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(RiesgoDTO riesgoDTO) throws ExcepcionDeAccesoADatos {
		
		try{
			beanRemoto.update(riesgoDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RiesgoDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findById(id);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrar(RiesgoDTO riesgoDTO) throws ExcepcionDeAccesoADatos {
		
		try{
			beanRemoto.delete(riesgoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RiesgoDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}

	public List<RiesgoDTO> listarFiltrado(RiesgoDTO riesgoDTO, boolean mostrarInactivos) throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.listarFiltrado(riesgoDTO, mostrarInactivos);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<RiesgoDTO> getListarFiltrado(RiesgoDTO riesgoDTO, boolean mostrarInactivos) throws ExcepcionDeAccesoADatos {
		try {
			return riesgoDTOService.getListarFiltrado(riesgoDTO, mostrarInactivos);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<RiesgoDTO> listarVigentesAutos() throws ExcepcionDeAccesoADatos {
		try {
			return riesgoDTOService.listarVigentesAutos();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void borradoLogico(RiesgoDTO riesgoDTO) throws ExcepcionDeAccesoADatos{
		
		try{
			beanRemoto.borradoLogico(riesgoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<RiesgoDTO> listarVigentes() throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.listarVigentes();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<RiesgoDTO> listarRiesgosPorAsociarCobertura(CoberturaDTO coberturaDTO,BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarRiesgosPorAsociarCobertura(coberturaDTO,idToSeccion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<RiesgoDTO> listarRiesgosVigentesPorCoberturaSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarRiesgosVigentesPorCoberturaSeccion(idToCobertura, idToSeccion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Boolean isCoberturaAsociada(RiesgoDTO riesgoDTO) {
		try {
			Long count = beanRemoto.countRiesgoCobertura(riesgoDTO.getIdToRiesgo());
			return count > 0;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Boolean isTarifaAsociada(RiesgoDTO riesgoDTO) {
		try {
			Long count = beanRemoto.countRiesgoTarifa(riesgoDTO.getIdToRiesgo());
			return count > 0;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
