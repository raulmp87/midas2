package mx.com.afirme.midas.producto.configuracion.recargo.exclusion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.ProductoAction;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoForm;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ExclusionRecargoVarioProductoAction extends ProductoAction{
	/**
	 * Method mostrarExclusionRecargo
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionRecargo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id");
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(BigDecimal.valueOf(Double.valueOf(id)));
		try {
			productoDTO = new ProductoDN().getPorId(productoDTO);
			//this.poblarDTO(productoForm, productoDTO);
			productoForm.setIdToProducto(productoDTO.getIdToProducto().toString());
			//Listas para el filtrado de exlusion de recargos
			productoForm.setTiposPoliza(TipoPolizaDN.getInstancia().listarVigentesPorIdProducto(productoDTO.getIdToProducto()));
			productoForm.setRecargosAsociados(new RecargoVarioProductoSN().listarRecargoAsociado(productoDTO.getIdToProducto()));
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcRecargoProductoAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcRecargoProductoAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id").toString();
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));
		ProductoDN productoDN =  ProductoDN.getInstancia();
		productoForm.setDescuentosAsociados(productoDN.listarDescuentoAsociado(productoDTO));
		productoDTO = productoDN.getPorId(productoDTO);
		productoForm.setExcRecargoProductoAsociados(productoDN.listarExcRecargoProductoAsociadas(productoDTO));
//		String json = "{rows:[";
//		if(productoForm.getExcRecargoProductoAsociados()!= null && productoForm.getExcRecargoProductoAsociados().size() > 0) {
//			for(ExclusionRecargoVarioProductoDTO actual : productoForm.getExcRecargoProductoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id+ "\",data:[";
//				json += actual.getRecargoVarioDTO().getClavetiporecargo() + ",\"";
//				json += actual.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getCodigo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getExcRecargoProductoAsociados()!= null && productoForm.getExcRecargoProductoAsociados().size() > 0) {
			for(ExclusionRecargoVarioProductoDTO actual : productoForm.getExcRecargoProductoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id);
				row.setDatos(
						actual.getRecargoVarioDTO().getClavetiporecargo().toString(),
						actual.getRecargoVarioDTO().getDescripcionrecargo(),
						actual.getTipoPolizaDTO().getCodigo(),
						actual.getTipoPolizaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcRecargoProductoAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcRecargoProductoAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoDN productoDN = ProductoDN.getInstancia(); 
		RecargoVarioDN recargoDN = new RecargoVarioDN();
		ExclusionRecargoVarioProductoId id = new ExclusionRecargoVarioProductoId();
		TipoPolizaDN polizaDN = new TipoPolizaDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		
		try {
			id.setIdtorecargovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtotipopoliza(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtoproducto(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionRecargoVarioProductoDTO excRecargoProducto = new ExclusionRecargoVarioProductoDTO();
			excRecargoProducto.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				RecargoVarioDTO recargo = new RecargoVarioDTO();
				recargo.setIdtorecargovario(UtileriasWeb.regresaBigDecimal(ids[0]));
				recargo = recargoDN.getPorId(recargo);

				TipoPolizaDTO poliza = new TipoPolizaDTO();
				poliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(ids[1]));
				poliza = polizaDN.getPorId(poliza);
				
				ProductoDTO producto = new  ProductoDTO();
				producto.setIdToProducto(UtileriasWeb.regresaBigDecimal(ids[2]));
				producto = productoDN.getPorId(producto);
				
				excRecargoProducto.setRecargoVarioDTO(recargo);
				excRecargoProducto.setProductoDTO(producto);
				excRecargoProducto.setTipoPolizaDTO(poliza);

				productoDN.ExcluirRecargoProducto(excRecargoProducto);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				productoDN.EliminarExclusionRecargoProducto(excRecargoProducto);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRamosPorAsociar
	 * 
	 * M�todo que recupera la lista de ramos no asociados a un producto
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcRecargoProductoPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		ProductoForm productoForm = (ProductoForm) form;
		String id = request.getParameter("id").toString();
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		//Posibles par�metros recibidos para hacer un filtrado por tipoPoliza o Descuento
		String idToTipoPoliza = request.getParameter("idToTipoPoliza");
		String idToRecargo = request.getParameter("idToRecargo");
		if (idToTipoPoliza != null)
			productoForm.setExcRecargoProductoNoAsociados(ProductoDN.getInstancia().listarExcRecargoProductoNoAsociadas(productoDTO,UtileriasWeb.regresaBigDecimal(idToTipoPoliza)));
		else if (idToRecargo != null)
			productoForm.setExcRecargoProductoNoAsociados(ProductoDN.getInstancia().listarExcRecargoProductoNoAsociadas(UtileriasWeb.regresaBigDecimal(idToRecargo),productoDTO));
		else
			productoForm.setExcRecargoProductoNoAsociados(ProductoDN.getInstancia().listarExcRecargoProductoNoAsociadas(productoDTO));
//		String json = "{rows:[";
//		if(productoForm.getExcRecargoProductoNoAsociados() != null && productoForm.getExcRecargoProductoNoAsociados().size() > 0) {
//			for(ExclusionRecargoVarioProductoDTO actual : productoForm.getExcRecargoProductoNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id+ "\",data:[";
//				json += actual.getRecargoVarioDTO().getClavetiporecargo() + ",\"";
//				json += actual.getRecargoVarioDTO().getDescripcionrecargo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getCodigo() + "\",\"";
//				json += actual.getTipoPolizaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getExcRecargoProductoNoAsociados() != null && productoForm.getExcRecargoProductoNoAsociados().size() > 0) {
			for(ExclusionRecargoVarioProductoDTO actual : productoForm.getExcRecargoProductoNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtorecargovario()+"|"+actual.getId().getIdtotipopoliza() +"|"+id);
				row.setDatos(
						actual.getRecargoVarioDTO().getClavetiporecargo().toString(),
						actual.getRecargoVarioDTO().getDescripcionrecargo(),
						actual.getTipoPolizaDTO().getCodigo(),
						actual.getTipoPolizaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
