package mx.com.afirme.midas.producto.configuracion.tipopoliza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioSN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoSN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoSN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioSN;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion.ExclusionDescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion.ExclusionDescuentoVarioTipoPolizaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion.ExclusionDescuentoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoPolizaDN {
	private static final TipoPolizaDN INSTANCIA = new TipoPolizaDN();

	public static TipoPolizaDN getInstancia() {
		return TipoPolizaDN.INSTANCIA;
	}

	public List<TipoPolizaDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		return tipoPolizaSN.listarTodos();
	}

	public void agregar(TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		tipoPolizaSN.agregar(tipoPolizaDTO);
	}

	public void modificar(TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		tipoPolizaSN.modificar(tipoPolizaDTO);
	}

	public TipoPolizaDTO getPorId(TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		return tipoPolizaSN.getPorId(tipoPolizaDTO.getIdToTipoPoliza());
	}

	public TipoPolizaDTO getPorId(BigDecimal idToTipoPoliza)
			throws SystemException, ExcepcionDeAccesoADatos {
		return new TipoPolizaSN().getPorId(idToTipoPoliza);
	}

	public void borrar(TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		tipoPolizaSN.borrar(tipoPolizaDTO);
	}

	public TipoPolizaDTO getPorIdCascada(TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		return this.poblarPadreHijos(tipoPolizaDTO);
	}

	public TipoPolizaDTO getPorIdHijo(BigDecimal id) throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		return tipoPolizaSN.getPorIdHijo(id);
	}

	private TipoPolizaDTO poblarPadreHijos(TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		TipoPolizaDTO tipoPoliza = tipoPolizaSN.getPorId(tipoPolizaDTO
				.getIdToTipoPoliza());
		ProductoDN productoDN = new ProductoDN();
		tipoPoliza.setProductoDTO(productoDN.getPorIdHijo(tipoPolizaDTO
				.getIdToTipoPoliza()));
		List<SeccionDTO> secciones = SeccionDN.getInstancia()
				.listarVigentesPorIDTipoPoliza(
						tipoPolizaDTO.getIdToTipoPoliza());/*
															 * ).buscarPorPropiedad
															 * (
															 * "tipoPolizaDTO.idToTipoPoliza"
															 * , tipoPolizaDTO
															 * .getIdToTipoPoliza
															 * ());
															 */
		LogDeMidasWeb.log("TipoPolizaDTO id:"
				+ tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de secciones asociadas ==> " + secciones.size(),
				Level.INFO, null);
		tipoPoliza.setSecciones(secciones);

		RecargoVarioTipoPolizaSN recargoPorTipoPolizaSN = new RecargoVarioTipoPolizaSN();
		List<RecargoVarioTipoPolizaDTO> recargos = recargoPorTipoPolizaSN
				.listarRecargoAsociado(tipoPoliza.getIdToTipoPoliza());
		tipoPoliza.setRecargoVarioTipoPolizaDTO(recargos);

		AumentoVarioTipoPolizaSN aumentoVarioTipoPolizaSN = new AumentoVarioTipoPolizaSN();
		List<AumentoVarioTipoPolizaDTO> aumentos = aumentoVarioTipoPolizaSN
				.listarAumentoAsociado(tipoPoliza.getIdToTipoPoliza());
		tipoPoliza.setAumentoVarioTipoPolizaDTOs(aumentos);
		return tipoPoliza;
	}

	public List<TipoPolizaDTO> buscarPorPropiedad(String propiedad, Object valor)
			throws SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		return tipoPolizaSN.buscarPorPropiedad(propiedad, valor);
	}

	public List<TipoPolizaDTO> listarVigentesPorIdProducto(
			BigDecimal idToProducto) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new TipoPolizaSN().listarVigentesPorIdProducto(idToProducto);
	}
	
	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto, Boolean verInactivos, Boolean soloActivos) 
		throws SystemException,	ExcepcionDeAccesoADatos  {
		return new TipoPolizaSN().listarPorIdProducto(idToProducto, verInactivos, soloActivos);
	}

	public void borradoLogico(TipoPolizaDTO TipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new TipoPolizaSN().borradoLogico(TipoPolizaDTO);
	}

	public List<TipoPolizaDTO> listarVigentes() throws SystemException,
			ExcepcionDeAccesoADatos {
		return (new TipoPolizaSN().listarVigentes());
	}

	public TipoPolizaDTO getSeccionesVigentesPorIdCascada(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		if (tipoPolizaDTO != null) {
			/*
			 * TipoPolizaSN tipoPolizaSN = new TipoPolizaSN(); TipoPolizaDTO
			 * tipoPoliza =
			 * tipoPolizaSN.getPorId(tipoPolizaDTO.getIdToTipoPoliza());
			 */
			SeccionSN seccionSN = new SeccionSN();
			List<SeccionDTO> secciones = seccionSN.listarVigentes(tipoPolizaDTO
					.getIdToTipoPoliza());
			LogDeMidasWeb.log("TipoPoliza id:"
					+ tipoPolizaDTO.getIdToTipoPoliza()
					+ ". Numero de secciones VIGENTES asociadas ==> "
					+ secciones.size(), Level.INFO, null);
			tipoPolizaDTO.setSecciones(secciones);
		}
		return tipoPolizaDTO;
	}

	/**
	 * Encuentra los registros de RamoDTO no asociados a un tipoPoliza a trav�s
	 * de los registros RamoTipoPolizaDTO.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro tipoPolizaDTO al que no est�n
	 *            asociados los ramos
	 * 
	 * @return List<RamoDTO> la lista de ramos no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<RamoDTO> listarRamosNoAsociados(TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		if (tipoPolizaDTO.getProductoDTO() == null
				|| tipoPolizaDTO.getProductoDTO().getIdToProducto() == null) {
			ProductoSN productoSN = new ProductoSN();
			tipoPolizaDTO.setProductoDTO(productoSN.getPorIdHijo(tipoPolizaDTO
					.getIdToTipoPoliza()));
		}
		return new RamoSN().obtenerRamosSinAsociarTipoPoliza(tipoPolizaDTO);
	}

	/**
	 * Encuentra los registros de RamoDTO asociados a un tipoPoliza a trav�s de
	 * los registros TipoPolizaDTO.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro tipoPolizaDTO al que est�n asociados
	 *            los ramos
	 * 
	 * @return List<RamoDTO> la lista de ramos asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<RamoDTO> listarRamosAsociadas(TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RamoTipoPolizaSN ramoTipoPolizaSN = new RamoTipoPolizaSN();
		RamoSN ramoSN = new RamoSN();
		// obtener los registros que s� est�n asociados a la seccion
		List<RamoTipoPolizaDTO> ramoTipoPolizaList = ramoTipoPolizaSN
				.buscarPorPropiedad("id.idtotipopoliza", tipoPolizaDTO
						.getIdToTipoPoliza());
		List<RamoDTO> resultList = new ArrayList<RamoDTO>();
		RamoDTO ramoTMP = new RamoDTO();
		// Encontrar y guardar los registros ProductoDTO que coincidan
		for (RamoTipoPolizaDTO ramoTipoPoliza : ramoTipoPolizaList) {
			ramoTMP.setIdTcRamo(ramoTipoPoliza.getId().getIdtcramo());
			resultList.add(ramoSN.getRamoPorId(ramoTMP));
		}
		return resultList;
	}

	public void asociarRamo(RamoTipoPolizaDTO ramoTipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new RamoTipoPolizaSN().agregar(ramoTipoPolizaDTO);
	}

	public void desasociarRamo(RamoTipoPolizaDTO ramoTipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new RamoTipoPolizaSN().borrar(ramoTipoPolizaDTO);
	}

	public List<RecargoVarioDTO> listarRecargosPorAsociar(
			TipoPolizaDTO tipoPolizaDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.listarRecargosPorAsociarTipoPoliza(tipoPolizaDTO
				.getIdToTipoPoliza());
	}

	public void asociarRecargo(TipoPolizaDTO tipoPolizaDTO,
			RecargoVarioTipoPolizaDTO recargoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioTipoPolizaSN recargoVarioTipoPolizaSN = new RecargoVarioTipoPolizaSN();
		recargoVarioTipoPolizaSN.guardar(recargoTipoPolizaDTO);
	}

	public void actualizarAsociacion(TipoPolizaDTO tipoPolizaDTO,
			RecargoVarioTipoPolizaDTO recargoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioTipoPolizaSN recargoVarioTipoPolizaSN = new RecargoVarioTipoPolizaSN();
		recargoVarioTipoPolizaSN.actualizar(recargoTipoPolizaDTO);
	}

	public void desasociarRecargo(TipoPolizaDTO tipoPolizaDTO,
			RecargoVarioTipoPolizaDTO recargoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		RecargoVarioTipoPolizaSN recargoVarioTipoPolizaSN = new RecargoVarioTipoPolizaSN();
		recargoVarioTipoPolizaSN.borrar(recargoTipoPolizaDTO);
	}

	public List<AumentoVarioDTO> listarAumentosPorAsociar(
			TipoPolizaDTO tipoPolizaDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.listarAumentosPorAsociarTipoPoliza(tipoPolizaDTO
				.getIdToTipoPoliza());
	}

	public void asociarAumento(TipoPolizaDTO tipoPolizaDTO,
			AumentoVarioTipoPolizaDTO aumentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioTipoPolizaSN AumentoVarioTipoPolizaSN = new AumentoVarioTipoPolizaSN();
		AumentoVarioTipoPolizaSN.guardar(aumentoTipoPolizaDTO);
	}

	public void actualizarAsociacion(TipoPolizaDTO tipoPolizaDTO,
			AumentoVarioTipoPolizaDTO aumentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioTipoPolizaSN AumentoVarioTipoPolizaSN = new AumentoVarioTipoPolizaSN();
		AumentoVarioTipoPolizaSN.actualizar(aumentoTipoPolizaDTO);
	}

	public void desasociarAumento(TipoPolizaDTO tipoPolizaDTO,
			AumentoVarioTipoPolizaDTO aumentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioTipoPolizaSN AumentoVarioTipoPolizaSN = new AumentoVarioTipoPolizaSN();
		AumentoVarioTipoPolizaSN.borrar(aumentoTipoPolizaDTO);
	}

	/**
	 * M�todo asociarDescuento. Asocia un descuento con un tipo de p�liza.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el tipo de p�liza a asociar.
	 * 
	 * @param DescuentoVarioProductoDTO
	 *            descuentoPorProductoDTO el descuento que se asociar� al tipo
	 *            de p�liza.
	 */
	public void asociarDescuento(TipoPolizaDTO tipoPolizaDTO,
			DescuentoVarioTipoPolizaDTO descuentoPorTipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoVarioTipoPolizaSN descuentoPorTipoPolizaSN = new DescuentoVarioTipoPolizaSN();
		descuentoPorTipoPolizaSN.guardar(descuentoPorTipoPolizaDTO);
	}

	/**
	 * M�todo desasociarDescuento. Elimina la asociaci�n entre descuento y un
	 * tipo de p�liza.
	 * 
	 * @param DescuentoVarioTipoPolizaDTO
	 *            descuentoPorTipoPolizaDTO el descuento que se desasociar� al
	 *            tipo de p�liza.
	 */
	public void desasociarDescuento(
			DescuentoVarioTipoPolizaDTO descuentoPorTipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoVarioTipoPolizaSN descuentoPorTipoPolizaSN = new DescuentoVarioTipoPolizaSN();
		descuentoPorTipoPolizaSN.borrar(descuentoPorTipoPolizaDTO);
	}

	/**
	 * M�todo actualizarAsociaci�n. Actualiza la asociaci�n entre descuento y un
	 * tipo de p�liza.
	 * 
	 * @param DescuentoVarioTipoPolizaDTO
	 *            descuentoPorTipoPolizaDTO la asociasi�n que se actualizar�
	 */
	public void actualizarAsociacion(
			DescuentoVarioTipoPolizaDTO descuentoPorTipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoVarioTipoPolizaSN descuentoPorProductoSN = new DescuentoVarioTipoPolizaSN();
		descuentoPorProductoSN.actualizar(descuentoPorTipoPolizaDTO);
	}

	/**
	 * M�todo listarDescuentosAsociados. Lista los registros de
	 * DescuentoVarioTipoPolizaDTO asociados a un TipoPolizaDTO.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que est�n asociados
	 *            los registros DescuentoVarioTipoPolizaDTO.
	 * 
	 * @return List<DescuentoVarioTipoPolizaDTO> listarDescuentosAsociados. La
	 *         lista de registros DescuentoVarioTipoPolizaDTO asociados.
	 */
	public List<DescuentoVarioTipoPolizaDTO> listarDescuentosAsociados(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException {
		return new DescuentoVarioTipoPolizaSN()
				.listarDescuentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
	}

	/**
	 * M�todo listarDescuentoPorAsociar. Lista los registros de
	 * DescuentoVarioTipoPolizaDTO no asociados a un TipoPolizaDTO.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que no est�n
	 *            asociados los registros DescuentoVarioTipoPolizaDTO.
	 * 
	 * @return List<DescuentoDTO>. La lista de registros DescuentoDTO no
	 *         asociados.
	 */
	public List<DescuentoDTO> listarDescuentoPorAsociar(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		DescuentoSN descuentoSN = new DescuentoSN();
		return descuentoSN.listarDescuentosPorAsociarTipoPoliza(tipoPolizaDTO
				.getIdToTipoPoliza());
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioTipoPolizaDTO asociados a
	 * un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que est�n asociados
	 *            los registros
	 * 
	 * @return List<ExcAumentoPorTipoPolizaDTO> la lista de
	 *         ExcAumentoPorTipoPolizaDTO asociados al registro TipoPolizaDTO
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> listarExcAumentoTipoPolizaAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		/*
		 * List<ExclusionAumentoVarioTipoPolizaDTO> lista = new
		 * ExclusionAumentoVarioTipoPolizaSN
		 * ().buscarPorPropiedad("id.idtotipopoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza()); for
		 * (ExclusionAumentoVarioTipoPolizaDTO actual : lista) {
		 * actual.setTipoPolizaDTO(tipoPolizaDTO); actual.setAumentoVarioDTO(new
		 * AumentoVarioSN().getPorId(actual.getId().getIdtoaumentovario()));
		 * actual.setCoberturaDTO(new
		 * CoberturaSN().getPorId(actual.getId().getIdtocobertura())); } return
		 * lista;
		 */
		return new ExclusionAumentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioTipoPolizaDTO asociados
	 * a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que est�n asociados
	 *            los registros
	 * 
	 * @return List<ExcDescuentoPorTipoPolizaDTO> la lista de
	 *         ExcDescuentoPorTipoPolizaDTO asociados al registro TipoPolizaDTO
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> listarExcDescuentoTipoPolizaAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		/*
		 * List<ExclusionDescuentoVarioTipoPolizaDTO> lista = new
		 * ExclusionDescuentoVarioTipoPolizaSN
		 * ().buscarPorPropiedad("id.idtotipopoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza()); for
		 * (ExclusionDescuentoVarioTipoPolizaDTO actual : lista) {
		 * actual.setTipoPolizaDTO(tipoPolizaDTO); actual.setDescuentoDTO(new
		 * DescuentoSN().getPorId(actual.getId().getIdtodescuentovario()));
		 * actual.setCoberturaDTO(new
		 * CoberturaSN().getPorId(actual.getId().getIdtocobertura())); } return
		 * lista;
		 */
		return new ExclusionDescuentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioTipoPolizaDTO asociados a
	 * un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que est�n asociados
	 *            los registros
	 * 
	 * @return List<ExcRecargoPorTipoPolizaDTO> la lista de
	 *         ExcRecargoPorTipoPolizaDTO asociados al registro TipoPolizaDTO
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> listarExcRecargoTipoPolizaAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		/*
		 * List<ExclusionRecargoVarioTipoPolizaDTO> lista = new
		 * ExclusionRecargoVarioTipoPolizaSN
		 * ().buscarPorPropiedad("id.idtotipopoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza()); for
		 * (ExclusionRecargoVarioTipoPolizaDTO actual : lista) {
		 * actual.setTipoPolizaDTO(tipoPolizaDTO); actual.setRecargoVarioDTO(new
		 * RecargoVarioSN().getPorId(actual.getId().getIdtorecargovario()));
		 * actual.setCoberturaDTO(new
		 * CoberturaSN().getPorId(actual.getId().getIdtocobertura())); } return
		 * lista;
		 */
		return new ExclusionRecargoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que noest�n
	 *            asociados los registros
	 * @return List<ExclusionAumentoVarioTipoPolizaDTO> la lista de
	 *         ExclusionAumentoVarioTipoPolizaDTO no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> listarExcAumentoTipoPolizaNoAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = CoberturaDN.getInstancia()
				.listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los aumentos asociados al tipoPoliza
		List<AumentoVarioTipoPolizaDTO> aumentos = new AumentoVarioTipoPolizaSN()
				.listarAumentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de aumentos asociados ==> " + aumentos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionAumentoVarioTipoPolizaDTO
		// asociadosal producto
		List<ExclusionAumentoVarioTipoPolizaDTO> listaExcAumento = new ExclusionAumentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>();
		if (listaExcAumento.size() < (coberturas.size() * aumentos.size())) {
			for (AumentoVarioTipoPolizaDTO aumento : aumentos) {
				for (CoberturaDTO cobertura : coberturas) {
					ExclusionAumentoVarioTipoPolizaId id = new ExclusionAumentoVarioTipoPolizaId();
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					id.setIdtoaumentovario(aumento.getId()
							.getIdtoaumentovario());
					id.setIdtocobertura(cobertura.getIdToCobertura());

					ExclusionAumentoVarioTipoPolizaDTO exclusionAumentoVarioTipoPolizaDTO = new ExclusionAumentoVarioTipoPolizaDTO();
					exclusionAumentoVarioTipoPolizaDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los
					// noexistentes
					if (!listaExcAumento
							.contains(exclusionAumentoVarioTipoPolizaDTO)) {
						exclusionAumentoVarioTipoPolizaDTO
								.setTipoPolizaDTO(tipoPolizaDTO);
						exclusionAumentoVarioTipoPolizaDTO
								.setAumentoVarioDTO(aumento
										.getAumentoVarioDTO());
						exclusionAumentoVarioTipoPolizaDTO
								.setCoberturaDTO(cobertura);
						listaResultado.add(exclusionAumentoVarioTipoPolizaDTO);
					}
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza que coincidan con la Cobertura cuyo ID se
	 * recibe.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que noest�n
	 *            asociados los registros
	 * @param idToCobertura
	 * @return List<ExclusionAumentoVarioTipoPolizaDTO> la lista de
	 *         ExclusionAumentoVarioTipoPolizaDTO no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> listarExcAumentoTipoPolizaNoAsociadas(
			TipoPolizaDTO tipoPolizaDTO, BigDecimal idToCobertura)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		coberturas.add(CoberturaDN.getInstancia().getPorId(idToCobertura));
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los aumentos asociados al tipoPoliza
		List<AumentoVarioTipoPolizaDTO> aumentos = new AumentoVarioTipoPolizaSN()
				.listarAumentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de aumentos asociados ==> " + aumentos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionAumentoVarioTipoPolizaDTO
		// asociadosal producto
		List<ExclusionAumentoVarioTipoPolizaDTO> listaExcAumento = new ExclusionAumentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>();
		for (AumentoVarioTipoPolizaDTO aumento : aumentos) {
			for (CoberturaDTO cobertura : coberturas) {
				ExclusionAumentoVarioTipoPolizaId id = new ExclusionAumentoVarioTipoPolizaId();
				id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
				id.setIdtoaumentovario(aumento.getId().getIdtoaumentovario());
				id.setIdtocobertura(cobertura.getIdToCobertura());

				ExclusionAumentoVarioTipoPolizaDTO exclusionAumentoVarioTipoPolizaDTO = new ExclusionAumentoVarioTipoPolizaDTO();
				exclusionAumentoVarioTipoPolizaDTO.setId(id);
				// Si el registro no existe, agregarlo a la lista de los
				// noexistentes
				if (!listaExcAumento
						.contains(exclusionAumentoVarioTipoPolizaDTO)) {
					exclusionAumentoVarioTipoPolizaDTO
							.setTipoPolizaDTO(tipoPolizaDTO);
					exclusionAumentoVarioTipoPolizaDTO
							.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
					exclusionAumentoVarioTipoPolizaDTO
							.setCoberturaDTO(cobertura);
					listaResultado.add(exclusionAumentoVarioTipoPolizaDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza que coincidan con el Aumento cuyo ID se recibe.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que noest�n
	 *            asociados los registros
	 * @param idToAumento
	 * @return List<ExclusionAumentoVarioTipoPolizaDTO> la lista de
	 *         ExclusionAumentoVarioTipoPolizaDTO no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> listarExcAumentoTipoPolizaNoAsociadas(
			BigDecimal idToAumento, TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = CoberturaDN.getInstancia()
				.listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los aumentos asociados al tipoPoliza
		List<AumentoVarioTipoPolizaDTO> aumentos = new AumentoVarioTipoPolizaSN()
				.listarAumentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de aumentos asociados ==> " + aumentos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionAumentoVarioTipoPolizaDTO
		// asociadosal producto
		List<ExclusionAumentoVarioTipoPolizaDTO> listaExcAumento = new ExclusionAumentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>();
		for (AumentoVarioTipoPolizaDTO aumento : aumentos) {
			if (aumento.getId().getIdtoaumentovario().compareTo(idToAumento) == 0) {
				for (CoberturaDTO cobertura : coberturas) {
					ExclusionAumentoVarioTipoPolizaId id = new ExclusionAumentoVarioTipoPolizaId();
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					id.setIdtoaumentovario(aumento.getId()
							.getIdtoaumentovario());
					id.setIdtocobertura(cobertura.getIdToCobertura());

					ExclusionAumentoVarioTipoPolizaDTO exclusionAumentoVarioTipoPolizaDTO = new ExclusionAumentoVarioTipoPolizaDTO();
					exclusionAumentoVarioTipoPolizaDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los
					// noexistentes
					if (!listaExcAumento
							.contains(exclusionAumentoVarioTipoPolizaDTO)) {
						exclusionAumentoVarioTipoPolizaDTO
								.setTipoPolizaDTO(tipoPolizaDTO);
						exclusionAumentoVarioTipoPolizaDTO
								.setAumentoVarioDTO(aumento
										.getAumentoVarioDTO());
						exclusionAumentoVarioTipoPolizaDTO
								.setCoberturaDTO(cobertura);
						listaResultado.add(exclusionAumentoVarioTipoPolizaDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que noest�n
	 *            asociados los registros
	 * @return List<ExclusionDescuentoVarioTipoPolizaDTO> la lista de
	 *         ExclusionDescuentoVarioTipoPolizaDTO no asociados al
	 *         registrotipoPolizaDTO
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> listarExcDescuentoTipoPolizaNoAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = CoberturaDN.getInstancia()
				.listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los descuentos asociados al tipoPoliza
		List<DescuentoVarioTipoPolizaDTO> descuentos = new DescuentoVarioTipoPolizaSN()
				.listarDescuentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de descuentos asociados ==> " + descuentos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionDescuentoVarioTipoPolizaDTO
		// asociados al producto
		List<ExclusionDescuentoVarioTipoPolizaDTO> listaExcDescuento = new ExclusionDescuentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioTipoPolizaDTO>();
		if (listaExcDescuento.size() < (coberturas.size() * descuentos.size())) {
			for (DescuentoVarioTipoPolizaDTO descuento : descuentos) {
				for (CoberturaDTO cobertura : coberturas) {
					ExclusionDescuentoVarioTipoPolizaId id = new ExclusionDescuentoVarioTipoPolizaId();
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					id.setIdtodescuentovario(descuento.getId()
							.getIdtodescuentovario());
					id.setIdtocobertura(cobertura.getIdToCobertura());

					ExclusionDescuentoVarioTipoPolizaDTO exclusionDescuentoVarioTipoPolizaDTO = new ExclusionDescuentoVarioTipoPolizaDTO();
					exclusionDescuentoVarioTipoPolizaDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcDescuento
							.contains(exclusionDescuentoVarioTipoPolizaDTO)) {
						exclusionDescuentoVarioTipoPolizaDTO
								.setTipoPolizaDTO(tipoPolizaDTO);
						exclusionDescuentoVarioTipoPolizaDTO
								.setDescuentoDTO(descuento.getDescuentoDTO());
						exclusionDescuentoVarioTipoPolizaDTO
								.setCoberturaDTO(cobertura);
						listaResultado
								.add(exclusionDescuentoVarioTipoPolizaDTO);
					}
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que noest�n
	 *            asociados los registros
	 * @param idToCobertura
	 * @return List<ExclusionDescuentoVarioTipoPolizaDTO> la lista de
	 *         ExclusionDescuentoVarioTipoPolizaDTO no asociados al
	 *         registrotipoPolizaDTO
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> listarExcDescuentoTipoPolizaNoAsociadas(
			TipoPolizaDTO tipoPolizaDTO, BigDecimal idToCobertura)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		coberturas.add(CoberturaDN.getInstancia().getPorId(idToCobertura));
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los descuentos asociados al tipoPoliza
		List<DescuentoVarioTipoPolizaDTO> descuentos = new DescuentoVarioTipoPolizaSN()
				.listarDescuentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de descuentos asociados ==> " + descuentos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionDescuentoVarioTipoPolizaDTO
		// asociados al producto
		List<ExclusionDescuentoVarioTipoPolizaDTO> listaExcDescuento = new ExclusionDescuentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioTipoPolizaDTO>();
		for (DescuentoVarioTipoPolizaDTO descuento : descuentos) {
			for (CoberturaDTO cobertura : coberturas) {
				ExclusionDescuentoVarioTipoPolizaId id = new ExclusionDescuentoVarioTipoPolizaId();
				id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
				id.setIdtodescuentovario(descuento.getId()
						.getIdtodescuentovario());
				id.setIdtocobertura(cobertura.getIdToCobertura());

				ExclusionDescuentoVarioTipoPolizaDTO exclusionDescuentoVarioTipoPolizaDTO = new ExclusionDescuentoVarioTipoPolizaDTO();
				exclusionDescuentoVarioTipoPolizaDTO.setId(id);
				// Si el registro no existe, agregarlo a la lista de los no
				// existentes
				if (!listaExcDescuento
						.contains(exclusionDescuentoVarioTipoPolizaDTO)) {
					exclusionDescuentoVarioTipoPolizaDTO
							.setTipoPolizaDTO(tipoPolizaDTO);
					exclusionDescuentoVarioTipoPolizaDTO
							.setDescuentoDTO(descuento.getDescuentoDTO());
					exclusionDescuentoVarioTipoPolizaDTO
							.setCoberturaDTO(cobertura);
					listaResultado.add(exclusionDescuentoVarioTipoPolizaDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que noest�n
	 *            asociados los registros
	 * @return List<ExclusionDescuentoVarioTipoPolizaDTO> la lista de
	 *         ExclusionDescuentoVarioTipoPolizaDTO no asociados al
	 *         registrotipoPolizaDTO
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> listarExcDescuentoTipoPolizaNoAsociadas(
			BigDecimal idToDescuento, TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = CoberturaDN.getInstancia()
				.listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los descuentos asociados al tipoPoliza
		List<DescuentoVarioTipoPolizaDTO> descuentos = new DescuentoVarioTipoPolizaSN()
				.listarDescuentoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de descuentos asociados ==> " + descuentos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionDescuentoVarioTipoPolizaDTO
		// asociados al producto
		List<ExclusionDescuentoVarioTipoPolizaDTO> listaExcDescuento = new ExclusionDescuentoVarioTipoPolizaSN()
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioTipoPolizaDTO>();
		for (DescuentoVarioTipoPolizaDTO descuento : descuentos) {
			if (descuento.getId().getIdtodescuentovario().compareTo(
					idToDescuento) == 0) {
				for (CoberturaDTO cobertura : coberturas) {
					ExclusionDescuentoVarioTipoPolizaId id = new ExclusionDescuentoVarioTipoPolizaId();
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					id.setIdtodescuentovario(descuento.getId()
							.getIdtodescuentovario());
					id.setIdtocobertura(cobertura.getIdToCobertura());

					ExclusionDescuentoVarioTipoPolizaDTO exclusionDescuentoVarioTipoPolizaDTO = new ExclusionDescuentoVarioTipoPolizaDTO();
					exclusionDescuentoVarioTipoPolizaDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcDescuento
							.contains(exclusionDescuentoVarioTipoPolizaDTO)) {
						exclusionDescuentoVarioTipoPolizaDTO
								.setTipoPolizaDTO(tipoPolizaDTO);
						exclusionDescuentoVarioTipoPolizaDTO
								.setDescuentoDTO(descuento.getDescuentoDTO());
						exclusionDescuentoVarioTipoPolizaDTO
								.setCoberturaDTO(cobertura);
						listaResultado
								.add(exclusionDescuentoVarioTipoPolizaDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioTipoPolizaDTO noasociados
	 * a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que no est�n
	 *            asociados los registros
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> la lista de
	 *         ExclusionRecargoVarioTipoPolizaDTO no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> listarExcRecargoTipoPolizaNoAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>(); for
		 * (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = CoberturaDN.getInstancia()
				.listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los recargos asociados al tipoPoliza
		List<RecargoVarioTipoPolizaDTO> recargos = new RecargoVarioTipoPolizaSN()
				.listarRecargoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de recargos asociados ==> " + recargos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionRecargoVarioTipoPolizaDTO
		// asociadosal producto
		ExclusionRecargoVarioTipoPolizaSN excRecargoSN = new ExclusionRecargoVarioTipoPolizaSN();
		List<ExclusionRecargoVarioTipoPolizaDTO> listaExcRecargo = excRecargoSN
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " ExclusionRecargoVarioTipoPolizaDTO asociados ==> "
				+ listaExcRecargo.size(), Level.INFO, null);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionRecargoVarioTipoPolizaDTO>();
		if (listaExcRecargo.size() < (coberturas.size() * recargos.size())) {
			for (RecargoVarioTipoPolizaDTO recargo : recargos) {
				for (CoberturaDTO cobertura : coberturas) {
					ExclusionRecargoVarioTipoPolizaId id = new ExclusionRecargoVarioTipoPolizaId();
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					id.setIdtorecargovario(recargo.getId()
							.getIdtorecargovario());
					id.setIdtocobertura(cobertura.getIdToCobertura());

					ExclusionRecargoVarioTipoPolizaDTO exclusionRecargoVarioTipoPolizaDTO = new ExclusionRecargoVarioTipoPolizaDTO();
					exclusionRecargoVarioTipoPolizaDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los
					// noexistentes
					if (!listaExcRecargo
							.contains(exclusionRecargoVarioTipoPolizaDTO)) {
						exclusionRecargoVarioTipoPolizaDTO
								.setTipoPolizaDTO(tipoPolizaDTO);
						exclusionRecargoVarioTipoPolizaDTO
								.setRecargoVarioDTO(recargo
										.getRecargoVarioDTO());
						exclusionRecargoVarioTipoPolizaDTO
								.setCoberturaDTO(cobertura);
						listaResultado.add(exclusionRecargoVarioTipoPolizaDTO);
					}
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioTipoPolizaDTO no
	 * asociados a un TipoPoliza que coincidan con la cobertura cuyo ID se
	 * recibe.
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que no est�n
	 *            asociados los registros
	 * @param idToCobertura
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> la lista de
	 *         ExclusionRecargoVarioTipoPolizaDTO no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> listarExcRecargoTipoPolizaNoAsociadas(
			TipoPolizaDTO tipoPolizaDTO, BigDecimal idToCobertura)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		coberturas.add(CoberturaDN.getInstancia().getPorId(idToCobertura));
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los recargos asociados al tipoPoliza
		List<RecargoVarioTipoPolizaDTO> recargos = new RecargoVarioTipoPolizaSN()
				.listarRecargoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de recargos asociados ==> " + recargos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionRecargoVarioTipoPolizaDTO
		// asociadosal producto
		ExclusionRecargoVarioTipoPolizaSN excRecargoSN = new ExclusionRecargoVarioTipoPolizaSN();
		List<ExclusionRecargoVarioTipoPolizaDTO> listaExcRecargo = excRecargoSN
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " ExclusionRecargoVarioTipoPolizaDTO asociados ==> "
				+ listaExcRecargo.size(), Level.INFO, null);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionRecargoVarioTipoPolizaDTO>();
		for (RecargoVarioTipoPolizaDTO recargo : recargos) {
			for (CoberturaDTO cobertura : coberturas) {
				ExclusionRecargoVarioTipoPolizaId id = new ExclusionRecargoVarioTipoPolizaId();
				id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
				id.setIdtorecargovario(recargo.getId().getIdtorecargovario());
				id.setIdtocobertura(cobertura.getIdToCobertura());

				ExclusionRecargoVarioTipoPolizaDTO exclusionRecargoVarioTipoPolizaDTO = new ExclusionRecargoVarioTipoPolizaDTO();
				exclusionRecargoVarioTipoPolizaDTO.setId(id);
				// Si el registro no existe, agregarlo a la lista de los
				// noexistentes
				if (!listaExcRecargo
						.contains(exclusionRecargoVarioTipoPolizaDTO)) {
					exclusionRecargoVarioTipoPolizaDTO
							.setTipoPolizaDTO(tipoPolizaDTO);
					exclusionRecargoVarioTipoPolizaDTO
							.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
					exclusionRecargoVarioTipoPolizaDTO
							.setCoberturaDTO(cobertura);
					listaResultado.add(exclusionRecargoVarioTipoPolizaDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionRecargoVarioTipoPolizaDTO noasociados
	 * a un TipoPoliza
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el registro TipoPolizaDTO al que no est�n
	 *            asociados los registros
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> la lista de
	 *         ExclusionRecargoVarioTipoPolizaDTO no asociados al registro
	 *         tipoPolizaDTO
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> listarExcRecargoTipoPolizaNoAsociadas(
			BigDecimal idToRecargo, TipoPolizaDTO tipoPolizaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener las secciones asociadas al tipo de p�liza
		/*
		 * List<SeccionDTO> secciones = new
		 * SeccionSN().buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",
		 * tipoPolizaDTO.getIdToTipoPoliza());
		 * LogDeMidasWeb.log("Tipo Poliza id:" +
		 * tipoPolizaDTO.getIdToTipoPoliza()+
		 * " Numero de secciones asociadas ==> " + secciones.size(),Level.INFO,
		 * null); // Obtener las coberturas asociadas a cada una de las
		 * secciones SeccionDN seccionDN = SeccionDN.getInstancia();
		 * List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();
		 * 
		 * for (SeccionDTO seccionTMP : secciones) { List<CoberturaDTO>
		 * coberturaListTMP = seccionDN.listarCoberturasAsociadas(seccionTMP);
		 * LogDeMidasWeb.log("Seccion id:" + seccionTMP.getIdToSeccion()+
		 * " Numero de coberturas asociadas ==> "+ coberturaListTMP.size(),
		 * Level.INFO, null); for (CoberturaDTO coberturaTMP : coberturaListTMP)
		 * coberturas.add(coberturaTMP); }
		 */
		List<CoberturaDTO> coberturas = CoberturaDN.getInstancia()
				.listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("Tipo Poliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de coberturas asociadas ==> " + coberturas.size(),
				Level.INFO, null);
		// obtener los recargos asociados al tipoPoliza
		List<RecargoVarioTipoPolizaDTO> recargos = new RecargoVarioTipoPolizaSN()
				.listarRecargoAsociado(tipoPolizaDTO.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " Numero de recargos asociados ==> " + recargos.size(),
				Level.INFO, null);

		// obtener los registros de ExclusionRecargoVarioTipoPolizaDTO
		// asociadosal producto
		ExclusionRecargoVarioTipoPolizaSN excRecargoSN = new ExclusionRecargoVarioTipoPolizaSN();
		List<ExclusionRecargoVarioTipoPolizaDTO> listaExcRecargo = excRecargoSN
				.buscarVigentesPorIdTipoPoliza(tipoPolizaDTO
						.getIdToTipoPoliza());
		LogDeMidasWeb.log("TipoPoliza id:" + tipoPolizaDTO.getIdToTipoPoliza()
				+ " ExclusionRecargoVarioTipoPolizaDTO asociados ==> "
				+ listaExcRecargo.size(), Level.INFO, null);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioTipoPolizaDTO> listaResultado = new ArrayList<ExclusionRecargoVarioTipoPolizaDTO>();
		for (RecargoVarioTipoPolizaDTO recargo : recargos) {
			if (recargo.getId().getIdtorecargovario().compareTo(idToRecargo) == 0) {
				for (CoberturaDTO cobertura : coberturas) {
					ExclusionRecargoVarioTipoPolizaId id = new ExclusionRecargoVarioTipoPolizaId();
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					id.setIdtorecargovario(recargo.getId()
							.getIdtorecargovario());
					id.setIdtocobertura(cobertura.getIdToCobertura());

					ExclusionRecargoVarioTipoPolizaDTO exclusionRecargoVarioTipoPolizaDTO = new ExclusionRecargoVarioTipoPolizaDTO();
					exclusionRecargoVarioTipoPolizaDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los
					// noexistentes
					if (!listaExcRecargo
							.contains(exclusionRecargoVarioTipoPolizaDTO)) {
						exclusionRecargoVarioTipoPolizaDTO
								.setTipoPolizaDTO(tipoPolizaDTO);
						exclusionRecargoVarioTipoPolizaDTO
								.setRecargoVarioDTO(recargo
										.getRecargoVarioDTO());
						exclusionRecargoVarioTipoPolizaDTO
								.setCoberturaDTO(cobertura);
						listaResultado.add(exclusionRecargoVarioTipoPolizaDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}

	public void ExcluirAumentoTipoPoliza(
			ExclusionAumentoVarioTipoPolizaDTO excAumentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionAumentoVarioTipoPolizaSN()
				.agregar(excAumentoTipoPolizaDTO);
	}

	public void EliminarExclusionAumentoTipoPoliza(
			ExclusionAumentoVarioTipoPolizaDTO excAumentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionAumentoVarioTipoPolizaSN().borrar(excAumentoTipoPolizaDTO);
	}

	public void ExcluirDescuentoTipoPoliza(
			ExclusionDescuentoVarioTipoPolizaDTO excDescuentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionDescuentoVarioTipoPolizaSN()
				.agregar(excDescuentoTipoPolizaDTO);
	}

	public void EliminarExclusionDescuentoTipoPoliza(
			ExclusionDescuentoVarioTipoPolizaDTO excDescuentoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionDescuentoVarioTipoPolizaSN()
				.borrar(excDescuentoTipoPolizaDTO);
	}

	public void ExcluirRecargoTipoPoliza(
			ExclusionRecargoVarioTipoPolizaDTO excRecargoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionRecargoVarioTipoPolizaSN()
				.agregar(excRecargoTipoPolizaDTO);
	}

	public void EliminarExclusionRecargoTipoPoliza(
			ExclusionRecargoVarioTipoPolizaDTO excRecargoTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionRecargoVarioTipoPolizaSN().borrar(excRecargoTipoPolizaDTO);
	}

	public List<MonedaTipoPolizaDTO> listarMonedasAsociadas(
			TipoPolizaDTO tipoPolizaDTO) throws SystemException {
		return new MonedaTipoPolizaSN().buscarPorPropiedad("id.idToTipoPoliza",
				tipoPolizaDTO.getIdToTipoPoliza());
	}

	public List<MonedaDTO> listarMonedasNoAsociadas(TipoPolizaDTO tipoPolizaDTO)
			throws SystemException {
		if (tipoPolizaDTO.getProductoDTO() == null
				|| tipoPolizaDTO.getProductoDTO().getIdToProducto() == null) {
			ProductoSN productoSN = new ProductoSN();
			tipoPolizaDTO.setProductoDTO(productoSN.getPorIdHijo(tipoPolizaDTO
					.getIdToTipoPoliza()));
		}
		return new MonedaSN().listarMonedasNoAsociadasTipoPoliza(tipoPolizaDTO
				.getIdToTipoPoliza(), tipoPolizaDTO.getProductoDTO()
				.getIdToProducto());
	}

	public void asociarMoneda(MonedaTipoPolizaDTO monedaTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new MonedaTipoPolizaSN().agregar(monedaTipoPolizaDTO);
	}

	public void actualizarAsociacionMoneda(
			MonedaTipoPolizaDTO monedaTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new MonedaTipoPolizaSN().modificar(monedaTipoPolizaDTO);
	}

	public void desasociarMoneda(MonedaTipoPolizaDTO monedaTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new MonedaTipoPolizaSN().borrar(monedaTipoPolizaDTO);
	}

	public List<DocumentoAnexoTipoPolizaDTO> listarDocumentosAnexos(
			BigDecimal idToTipoPoliza) throws ExcepcionDeAccesoADatos,
			SystemException {
		return DocumentoAnexoTipoPolizaDN.getInstancia().listarPorPropiedad(
				"tipoPolizaDTO.idToTipoPoliza", idToTipoPoliza);
	}

	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto)
			throws SystemException, ExcepcionDeAccesoADatos {
		return new TipoPolizaSN().listarPorIdProducto(idToProducto);
	}
}
