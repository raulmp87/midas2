package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.ramo.RamoDN;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RamoPorTipoPolizaAction extends TipoPolizaAction{
	/**
	 * Method mostrarAsociarRamo
	 * 
	 * M�todo que recupera las listas de ramos asociados y no asociados a un tipoPoliza
	 * y las coloca en la sesi�n del usuario.
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarRamo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN = new TipoPolizaDN();
		String id=request.getParameter("id");
		try {
			//poblarDTO(tipoPolizaForm, tipoPolizaDTO);
			tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
			tipoPolizaDTO=tipoPolizaDN.getPorId(tipoPolizaDTO);
			HttpSession session = request.getSession();
			session.setAttribute("idToTipoPoliza", tipoPolizaDTO.getIdToTipoPoliza());
			tipoPolizaForm.setIdToTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza().toString());
			
			tipoPolizaForm.setNombreComercial(tipoPolizaDTO.getNombreComercial());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRamosAsociados
	 * 
	 * M�todo que recupera la lista de ramos asociados a un tipoPoliza
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRamosAsociados(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/json");
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		try {
			tipoPolizaForm.setRamoTipoPolizaAsociados(RamoTipoPolizaDN.getInstancia().buscarPorPropiedad("id.idtotipopoliza", UtileriasWeb.regresaBigDecimal(""+request.getParameter("id"))));
//			String json = "{\"rows\":[";
//			if(tipoPolizaForm.getRamoTipoPolizaAsociados() != null && tipoPolizaForm.getRamoTipoPolizaAsociados().size() > 0){
//				List<RamoDTO> listaRamos = RamoDN.getInstancia().obtenerRamosObligatoriosPorTipoPoliza(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
//				for (RamoTipoPolizaDTO actual : tipoPolizaForm.getRamoTipoPolizaAsociados()){
//					json += "{\"id\":\"" + actual.getId().getIdtcramo() + "\",\"data\":[\"";
//					json += actual.getRamoDTO().getCodigo() + "\",\"";
//					json += actual.getRamoDTO().getDescripcion() + "\",\"";
//					json += this.contieneRamo(actual, listaRamos) + "\"]},";
//				}
//				json = json.substring(0, json.length() -1) + "]}";
//			}
//			else
//				json += "]}";
			MidasJsonBase json = new MidasJsonBase();
			if(tipoPolizaForm.getRamoTipoPolizaAsociados() != null && tipoPolizaForm.getRamoTipoPolizaAsociados().size() > 0){
				List<RamoDTO> listaRamos = RamoDN.getInstancia().obtenerRamosObligatoriosPorTipoPoliza(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
				for (RamoTipoPolizaDTO actual : tipoPolizaForm.getRamoTipoPolizaAsociados()){
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(actual.getId().getIdtcramo().toString());
					row.setDatos(
							actual.getRamoDTO().getCodigo().toString(),
							actual.getRamoDTO().getDescripcion(),
							this.contieneRamo(actual, listaRamos) + ""
					);
					json.addRow(row);
					
				}
			}
			System.out.println(json);
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method guardarRamoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarRamoAsociado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN = TipoPolizaDN.getInstancia();
		try {
			if(datosValidos(tipoPolizaForm.getRamosPorAsociar())) {
				poblarDTO(tipoPolizaForm, tipoPolizaDTO);
				HttpSession session = request.getSession();
				tipoPolizaDTO.setIdToTipoPoliza(new BigDecimal(session.getAttribute("idToTipoPoliza").toString()));
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					RamoTipoPolizaId id = new RamoTipoPolizaId();
					id.setIdtcramo(new BigDecimal(request.getParameter("gr_id")));
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					
					RamoTipoPolizaDTO ramoTipoPolizaDTO = new RamoTipoPolizaDTO();
					ramoTipoPolizaDTO.setId(id);
					
					RamoDTO ramo = new RamoDTO();
					ramo.setIdTcRamo(ramoTipoPolizaDTO.getId().getIdtcramo());
					ramo = new RamoDN().getRamoPorId(ramo);
					
					TipoPolizaDTO poliza = new TipoPolizaDTO();
					poliza.setIdToTipoPoliza(ramoTipoPolizaDTO.getId().getIdtotipopoliza());
					poliza = new TipoPolizaDN().getPorId(poliza);
					
					ramoTipoPolizaDTO.setTipoPolizaDTO(poliza);
					ramoTipoPolizaDTO.setRamoDTO(ramo);
					
					if(request.getParameter("!nativeeditor_status").equals("inserted")) {
						tipoPolizaDN.asociarRamo(ramoTipoPolizaDTO);
					}
				} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
					RamoTipoPolizaId id = new RamoTipoPolizaId();
					id.setIdtcramo(new BigDecimal(request.getParameter("gr_id")));
					id.setIdtotipopoliza(tipoPolizaDTO.getIdToTipoPoliza());
					
					RamoTipoPolizaDTO ramoTipoPolizaDTO = new RamoTipoPolizaDTO();
					ramoTipoPolizaDTO.setId(id);
					tipoPolizaDN.desasociarRamo(ramoTipoPolizaDTO);
				}
			} else {
				reglaNavegacion = Sistema.DATOS_INVALIDOS;
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRamosPorAsociar
	 * 
	 * M�todo que recupera la lista de ramos no asociados a un producto
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRamosPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/json");
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO ();
		try {
			tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(""+request.getParameter("id")));
			tipoPolizaDTO=TipoPolizaDN.getInstancia().getPorId(tipoPolizaDTO);
			tipoPolizaForm.setRamosPorAsociar(TipoPolizaDN.getInstancia().listarRamosNoAsociados(tipoPolizaDTO));
//			String json = "{\"rows\":[";
//			if(tipoPolizaForm.getRamosPorAsociar() != null && tipoPolizaForm.getRamosPorAsociar().size() >0){
//				for (RamoDTO actual : tipoPolizaForm.getRamosPorAsociar()){
//					json += "{\"id\":\"" + actual.getIdTcRamo() + "\",\"data\":[\"";
//					json += actual.getCodigo() + "\",\"";
//					json += actual.getDescripcion() + "\",\"false\"]},";
//				}
//				json = json.substring(0, json.length() -1) + "]}";
//			} else
//				json += "]}";
			MidasJsonBase json = new MidasJsonBase();
			if(tipoPolizaForm.getRamosPorAsociar() != null && tipoPolizaForm.getRamosPorAsociar().size() >0){
				for (RamoDTO actual : tipoPolizaForm.getRamosPorAsociar()){
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(actual.getIdTcRamo().toString());
					row.setDatos(
							actual.getCodigo().toString(),
							actual.getDescripcion(),
							"false"
					);
					json.addRow(row);
					
				}
			}
			System.out.println(json);
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method datosValidos
	 * 		Verifica que la lista de resgistros Cobertura no asociados a la seccion sea correcta.
	 */
	private Boolean datosValidos(List<RamoDTO> ramosPorAsociar) {
		return Boolean.TRUE;
	}
	
	/**
	 * Method contieneRamo
	 * 		Verifica si el RamoDTO recibido est� dentro de la lista de ramos recibida.
	 * @return true, si el ramo recibido est� contenido en la lista de ramos recibida como par�metro.
	 */
	private boolean contieneRamo(RamoTipoPolizaDTO ramo,List<RamoDTO> listaRamosObligatorios){
		boolean result = false;
		for(RamoDTO ramoTMP: listaRamosObligatorios){
			if (ramo.getId().getIdtcramo().equals(ramoTMP.getIdTcRamo())){
				result = true;
				break;
			}
		}
		return result;
	}
}
