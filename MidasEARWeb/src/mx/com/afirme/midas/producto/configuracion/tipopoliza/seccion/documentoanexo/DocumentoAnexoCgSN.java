package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentoanexo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;
import mx.com.afirme.midas2.domain.condicionesGenerales.DocumentoAnexoCgFacadeRemote;

public class DocumentoAnexoCgSN {
	private DocumentoAnexoCgFacadeRemote beanRemoto;

	public DocumentoAnexoCgSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoAnexoCgFacadeRemote.class);
		} catch (Exception e) { throw new SystemException(Sistema.NO_DISPONIBLE);}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CgCondiciones> listarTodos() throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}

	public void agregar(CgCondiciones ajustadorDTO)throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.save(ajustadorDTO);
		} catch(Exception e){ throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e); }
	}

	public void modificar(CgCondiciones ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.update(ajustadorDTO);
		}catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public CgCondiciones getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findById(id);
		}catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO); }
	}

	public void borrar(CgCondiciones ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.delete(ajustadorDTO);
		}catch(EJBTransactionRolledbackException e){ throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName()); }
	}
	
	public List<CgCondiciones> encontrarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findByProperty(propiedad, valor);
		}catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
}
