package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentoanexo.DocumentoAnexoCgDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida.SeccionRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida.SeccionRequeridaSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;

public class SeccionDN {
	private static final SeccionDN INSTANCIA = new SeccionDN();

	public static SeccionDN getInstancia() {
		return SeccionDN.INSTANCIA;
	}

	public List<SeccionDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		return seccionSN.listarTodos();
	}

	public void agregar(SeccionDTO seccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		seccionSN.agregar(seccionDTO);
	}

	public void modificar(SeccionDTO seccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		seccionSN.modificar(seccionDTO);
	}

	public SeccionDTO getPorIdCascada(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		return this.poblarPadreHijos(seccionDTO);
	}

	public SeccionDTO getPorId(SeccionDTO seccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		return seccionSN.getPorId(seccionDTO.getIdToSeccion());
	}

	public SeccionDTO getPorId(BigDecimal idToSeccion) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new SeccionSN().getPorId(idToSeccion);
	}

	public SeccionDTO getPadrePorId(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		SeccionDTO seccion = seccionSN.getPorId(seccionDTO.getIdToSeccion());
		TipoPolizaDN tipoPolizaDN = new TipoPolizaDN();
		seccion.setTipoPolizaDTO(tipoPolizaDN.getPorIdHijo(seccion
				.getIdToSeccion()));
		return seccion;
	}

	public void borrar(SeccionDTO seccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		seccionSN.borrar(seccionDTO);
	}

	private SeccionDTO poblarPadreHijos(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		SeccionDTO seccion = seccionSN.getPorId(seccionDTO.getIdToSeccion());
//		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
//		seccion.setTipoPolizaDTO(tipoPolizaSN.getPorIdHijo(seccion
//				.getIdToSeccion()));
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		List<CoberturaSeccionDTO> listCoberturaSeccionDTO = coberturaSeccionSN.listarVigentesPorSeccion(seccionDTO.getIdToSeccion());
		//s�lo se deben listar las coberturas vigentes.
				//.buscarPorPropiedad("seccionDTO.idToSeccion", seccionDTO
					//	.getIdToSeccion());
		seccion.setCoberturas(listCoberturaSeccionDTO);
		return seccion;
	}

	public List<SeccionDTO> buscarPorPropiedad(String propiedad, Object valor)
			throws SystemException, ExcepcionDeAccesoADatos {
		SeccionSN seccionSN = new SeccionSN();
		return seccionSN.buscarPorPropiedad(propiedad, valor);
	}

	public void borradoLogico(SeccionDTO SeccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		new SeccionSN().borradoLogico(SeccionDTO);
	}

	public List<SeccionDTO> listarVigentes() throws SystemException,
			ExcepcionDeAccesoADatos {
		return (new SeccionSN().listarVigentes());
	}

	public List<SeccionDTO> listarVigentesPorIDTipoPoliza(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		return (new SeccionSN().listarVigentes(seccionDTO.getTipoPolizaDTO()
				.getIdToTipoPoliza()));
	}

	public List<SeccionDTO> listarVigentesPorIDTipoPoliza(BigDecimal idToTipoPoliza) throws SystemException,ExcepcionDeAccesoADatos {
		return (new SeccionSN().listarVigentes(idToTipoPoliza));
	}
	
	public List<SeccionDTO> listarPorIdTipoPoliza(BigDecimal idToTipoPoliza, Boolean verInactivos, Boolean soloActivos) 
		throws SystemException,ExcepcionDeAccesoADatos {
		return new SeccionSN().listarPorIdTipoPoliza(idToTipoPoliza, verInactivos, soloActivos);
	}

	/**
	 * Encuentra los registros de CoberturaDTO no asociados a una seccion a
	 * trav�s de los registros CoberturaSeccionDTO.
	 * 
	 * @param SeccionDTO seccionDTO el registro seccionDTO al quie no est�n
	 * asociadas las coberturas
	 * 
	 * @return List<CoberturaDTO> la lista de coberturas no asociadas al
	 * registro seccionDTO
	 */
	public List<CoberturaDTO> listarCoberturasNoAsociadas(BigDecimal idToSeccion)
			throws SystemException, ExcepcionDeAccesoADatos {

		return new CoberturaSN()
				.listarCoberturasNoAsociadasSeccion(idToSeccion);
	}

	/**
	 * Encuentra los registros de CoberturaDTO asociados a una seccion a trev�s
	 * de los registros CoberturaSeccionDTO.
	 * 
	 * @param SeccionDTO seccionDTO el registro seccionDTO al quie est�n
	 * asociadas las coberturas
	 * 
	 * @return List<CoberturaDTO> la lista de coberturas asociadas al registro
	 * seccionDTO
	 */
	public List<CoberturaDTO> listarCoberturasAsociadas(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		CoberturaSN coberturaSN = new CoberturaSN();
		// obtener los registros que s� est�n asociados a la seccion
		List<CoberturaSeccionDTO> coberturaSeccionList = coberturaSeccionSN
				.buscarPorPropiedad("seccionDTO.idToSeccion", seccionDTO
						.getIdToSeccion());
		List<CoberturaDTO> resultList = new ArrayList<CoberturaDTO>();
		// Encontrar y guardar los registros CoberturaDTO que coincidan
		for (CoberturaSeccionDTO coberturaSeccion : coberturaSeccionList) {
			resultList.add(coberturaSN.getPorId(coberturaSeccion.getId()
					.getIdtocobertura()));
		}
		return resultList;
	}

	public void asociarCobertura(SeccionDTO seccionDTO,
			CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		new CoberturaSeccionSN().agregar(coberturaSeccionDTO);
	}

	public void actualizarAsociacionCobertura(SeccionDTO seccion,
			CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		new CoberturaSeccionSN().modificar(coberturaSeccionDTO);
	}

	public void desasociarCobertura(SeccionDTO seccionDTO,
			CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
		List<RiesgoCoberturaDTO> riesgos = riesgoCoberturaDN.listarRiesgoAsociado(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());
		for(RiesgoCoberturaDTO riesgo : riesgos) {
			riesgoCoberturaDN.borrarARDs(riesgo);
			/*RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			id.setIdToSeccion(riesgo.getId().getIdtoseccion());
			id.setIdToCobertura(riesgo.getId().getIdtocobertura());
			id.setIdToRiesgo(riesgo.getId().getIdtoriesgo());
			List<RiesgoCotizacionDTO> riesgosCotizacion = riesgoCotizacionDN.listarPorIdFiltrado(id);
			for(RiesgoCotizacionDTO riesgoCotizacion : riesgosCotizacion) {
				riesgoCotizacionDN.borrar(riesgoCotizacion);
			}*/
			riesgoCoberturaDN.borrar(riesgo);
		}
		/*CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
		id.setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarPorIdFiltrado(id);
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			coberturaCotizacionDN.borrar(cobertura);
		}*/
		new CoberturaSeccionSN().borrar(coberturaSeccionDTO);
	}

	/**
	 * Encuentra los registros de RamoDTO no asociados a una seccion a trav�s de
	 * los registros RamoSeccionDTO.
	 * 
	 * @param SeccionDTO seccionDTO el registro seccionDTO al que no est�n
	 * asociados los ramos
	 * 
	 * @return List<RamoDTO> la lista de ramos no asociados al registro
	 * seccionDTO
	 */
	public List<RamoDTO> listarRamosNoAsociados(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {

		return new RamoSN().obtenerRamosSinAsociarSeccion(seccionDTO);
	}

	public void asociarRamo(RamoSeccionDTO coberturaSeccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new RamoSeccionSN().agregar(coberturaSeccionDTO);
	}

	public void desasociarRamo(RamoSeccionDTO ramoSeccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new RamoSeccionSN().borrar(ramoSeccionDTO);
	}

	public List<SeccionDTO> listarSeccionesRequeridas(SeccionDTO seccionDTO)
			throws SystemException {
		return new SeccionSN().listarSeccionesRequeridas(seccionDTO
				.getIdToSeccion());
	}

	public List<SeccionDTO> listarSeccionesNoRequeridas(SeccionDTO seccionDTO)
			throws SystemException {
		return new SeccionSN().listarSeccionesNoRequeridas(seccionDTO
				.getIdToSeccion(), seccionDTO.getTipoPolizaDTO()
				.getIdToTipoPoliza());
	}

	public void AsociarSeccionRequerida(SeccionRequeridaDTO seccionRequeridaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new SeccionRequeridaSN().guardar(seccionRequeridaDTO);
	}

	public void DesasociarSeccionRequerida(
			SeccionRequeridaDTO seccionRequeridaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new SeccionRequeridaSN().borrar(seccionRequeridaDTO);
	}

	public SeccionDTO getPorIdConTipoPoliza(SeccionDTO seccionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		SeccionDTO seccion = new SeccionSN().getPorId(seccionDTO
				.getIdToSeccion());
		seccion.setTipoPolizaDTO(new TipoPolizaSN().getPorIdHijo(seccion
				.getIdToSeccion()));
		return seccion;
	}
	
	public List<ValorCatalogoAgentes> getCedulasAsociadasPorSeccion(BigDecimal idToSeccion) throws SystemException {
		SeccionSN seccionSN = new SeccionSN();
		List<ValorCatalogoAgentes> cedulas = null;
		cedulas = seccionSN.listarCedulasAsociadasPorSeccion(idToSeccion);
		return cedulas;
	}
	
	public List<ValorCatalogoAgentes> getCedulasNoAsociadasPorSeccion(BigDecimal idToSeccion) throws SystemException {
		SeccionSN seccionSN = new SeccionSN();
		List<ValorCatalogoAgentes> cedulas = null;
		cedulas = seccionSN.listarCedulasNoAsociadasPorSeccion(idToSeccion);
		return cedulas;
	}
	
	public Integer insertCedulasAsociadasPorSeccion(BigDecimal idToSeccion, String idsCedulas) throws SystemException {
		SeccionSN seccionSN = new SeccionSN();
		return seccionSN.guardarCedulasAsociadasPorSeccion(idToSeccion, idsCedulas);
	}
	
	public BigDecimal countCedulasAsociadasPorSeccion(Long idTipoCedula, BigDecimal idSeccion) throws SystemException {
		SeccionSN seccionSN = new SeccionSN();
		BigDecimal seccionesCedula = null;
		seccionesCedula = seccionSN.countCedulasAsociadasPorSeccion(idTipoCedula, idSeccion);
		return seccionesCedula;
	}
	
	public BigDecimal countCedulasAsociadasPorSeccionesEnTipoPoliza(Long idTipoCedula, BigDecimal idTipoPoliza) throws SystemException {
		SeccionSN seccionSN = new SeccionSN();
		BigDecimal seccionesCedula = null;
		seccionesCedula = seccionSN.countCedulasAsociadasPorSeccionesEnTipoPoliza(idTipoCedula, idTipoPoliza);
		return seccionesCedula;
	}

	public List<CgCondiciones> listarDocumentosAnexosCg(BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos,SystemException {
		return DocumentoAnexoCgDN.getInstancia().listarPorPropiedad("seccionDTO.idToSeccion", idToSeccion);
	}
	
	public Map<String, String> agregarVersion(BigDecimal idToSeccion) throws Exception {
	SeccionSN seccionSN = new SeccionSN();
    return seccionSN.agregarVersion(idToSeccion);
}
}
