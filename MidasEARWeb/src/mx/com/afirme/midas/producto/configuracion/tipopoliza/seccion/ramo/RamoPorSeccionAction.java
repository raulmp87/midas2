package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ramo.RamoDN;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RamoPorSeccionAction extends SeccionAction{
	/**
	 * Method mostrarAsociarRamo
	 * 
	 * M�todo que recupera las listas de ramos asociados y no asociados a una seccion
	 * y las coloca en la sesi�n del usuario.
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarRamo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SeccionForm seccionForm = (SeccionForm) form;
		String id = request.getParameter("id");
		if (id != null)
			seccionForm.setIdToSeccion(id);
		//RamoSeccionDN ramoSeccionDN = new RamoSeccionDN(); 
		try {
			//poblarDTO(seccionForm, seccionDTO);
			//HttpSession session = request.getSession();
			/*session.setAttribute("ramosAsociados", ramoSeccionDN.buscarPorPropiedad("id.idtoseccion", seccionDTO.getIdToSeccion()));
			session.setAttribute("ramosPorAsociar", seccionDN.listarRamosNoAsociados(seccionDTO));
			session.setAttribute("idToSeccion", seccionDTO.getIdToSeccion());*/
			
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRamosAsociados
	 * 
	 * M�todo que recupera la lista de ramos asociados a una seccion
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRamosAsociados(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/json");
		SeccionForm seccionForm = (SeccionForm) form;
		String id = request.getParameter("id");
		//HttpSession session = request.getSession();
		//seccionForm.setRamoSeccionAsociados((List<RamoSeccionDTO>)session.getAttribute("ramosAsociados"));
		try {seccionForm.setRamoSeccionAsociados(RamoSeccionDN.getInstancia().buscarPorPropiedad("id.idtoseccion", UtileriasWeb.regresaBigDecimal(id) ));
		} catch (ExcepcionDeAccesoADatos e) {e.printStackTrace();
		} catch (SystemException e) {e.printStackTrace();}
//		String json = "{\"rows\":[";
//		if(seccionForm.getRamoSeccionAsociados() != null && seccionForm.getRamoSeccionAsociados().size() > 0){
//			for (RamoSeccionDTO actual : seccionForm.getRamoSeccionAsociados()){
//				json += "{\"id\":\"" + actual.getId().getIdtcramo() + "\",\"data\":[\"";
//				json += actual.getRamoDTO().getCodigo() + "\",\"";
//				json += actual.getRamoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		}
//		else
//			json += "]}";
		MidasJsonBase json = new MidasJsonBase();
		if(seccionForm.getRamoSeccionAsociados() != null && seccionForm.getRamoSeccionAsociados().size() > 0){
			for (RamoSeccionDTO actual : seccionForm.getRamoSeccionAsociados()){
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtcramo().toString());
				row.setDatos(
						actual.getRamoDTO().getCodigo().toString(),
						actual.getRamoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Ramos asociados:"+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarRamoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarRamoAsociado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SeccionForm seccionForm = (SeccionForm) form;
		SeccionDTO seccionDTO = new SeccionDTO();
		SeccionDN seccionDN = SeccionDN.getInstancia();
		try {
			if(datosValidos(seccionForm.getRamosPorAsociar())) {
				poblarDTO(seccionForm, seccionDTO);
				/*HttpSession session = request.getSession();
				seccionDTO.setIdToSeccion(new BigDecimal(session.getAttribute("idToSeccion").toString()));*/
				seccionDTO.setIdToSeccion(new BigDecimal(request.getParameter("id")));
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					RamoSeccionId id = new RamoSeccionId();
					id.setIdtcramo(new BigDecimal(request.getParameter("gr_id")));
					id.setIdtoseccion(seccionDTO.getIdToSeccion());
					
					RamoSeccionDTO ramoSeccionDTO = new RamoSeccionDTO();
					ramoSeccionDTO.setId(id);
					
					RamoDTO ramo = new RamoDTO();
					ramo.setIdTcRamo(ramoSeccionDTO.getId().getIdtcramo());
					ramo = new RamoDN().getRamoPorId(ramo);
					
					SeccionDTO seccion = new SeccionDTO();
					seccion.setIdToSeccion(ramoSeccionDTO.getId().getIdtoseccion());
					seccion = new SeccionDN().getPorId(seccion);
					
					ramoSeccionDTO.setSeccionDTO(seccion);
					ramoSeccionDTO.setRamoDTO(ramo);
					
					if(request.getParameter("!nativeeditor_status").equals("inserted")) {
						seccionDN.asociarRamo(ramoSeccionDTO);
					}
				} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
					RamoSeccionId id = new RamoSeccionId();
					id.setIdtcramo(new BigDecimal(request.getParameter("gr_id")));
					id.setIdtoseccion(seccionDTO.getIdToSeccion());
					
					RamoSeccionDTO ramoSeccionDTO = new RamoSeccionDTO();
					ramoSeccionDTO.setId(id);
					seccionDN.desasociarRamo(ramoSeccionDTO);
				}
			} else {
				reglaNavegacion = Sistema.DATOS_INVALIDOS;
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarRamosPorAsociar
	 * 
	 * M�todo que recupera la lista de ramos no asociados a una seccion
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarRamosPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/json");
		SeccionForm seccionForm = (SeccionForm) form;
		String id = request.getParameter("id");
		//HttpSession session = request.getSession();
		//seccionForm.setRamosPorAsociar((List<RamoDTO>)session.getAttribute("ramosPorAsociar"));
		SeccionDTO seccionDTO = new SeccionDTO();
		try {
			seccionDTO.setIdToSeccion(UtileriasWeb.regresaBigDecimal(id));
			seccionDTO = SeccionDN.getInstancia().getPadrePorId(seccionDTO);
			seccionForm.setRamosPorAsociar(SeccionDN.getInstancia().listarRamosNoAsociados(seccionDTO)) ;
		} catch (ExcepcionDeAccesoADatos e) {e.printStackTrace();
		} catch (SystemException e) {e.printStackTrace();}
//		String json = "{\"rows\":[";
//		if(seccionForm.getRamosPorAsociar() != null && seccionForm.getRamosPorAsociar().size() >0){
//			for (RamoDTO actual : seccionForm.getRamosPorAsociar()){
//				json += "{\"id\":\"" + actual.getIdTcRamo() + "\",\"data\":[\"";
//				json += actual.getCodigo() + "\",\"";
//				json += actual.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else
//			json += "]}";
		MidasJsonBase json = new MidasJsonBase();
		if(seccionForm.getRamosPorAsociar() != null && seccionForm.getRamosPorAsociar().size() >0){
			for (RamoDTO actual : seccionForm.getRamosPorAsociar()){
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getIdTcRamo().toString());
				row.setDatos(
						actual.getCodigo().toString(),
						actual.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Ramos por asociar: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method datosValidos
	 * 		Verifica que la lista de resgistros Cobertura no asociados a la seccion sea correcta.
	 */
	private Boolean datosValidos(List<RamoDTO> ramosPorAsociar) {
		return Boolean.TRUE;
	}
}
