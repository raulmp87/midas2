package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaSeccionDN {
	private static final CoberturaSeccionDN INSTANCIA = new CoberturaSeccionDN();

	public static CoberturaSeccionDN getInstancia() {
		return CoberturaSeccionDN.INSTANCIA;
	}

	public List<CoberturaSeccionDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		return coberturaSeccionSN.listarTodos();
	}

	public void agregar(CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		coberturaSeccionSN.agregar(coberturaSeccionDTO);
	}

	public void modificar(CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		coberturaSeccionSN.modificar(coberturaSeccionDTO);
	}

	public CoberturaSeccionDTO getPorId(CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		return coberturaSeccionSN.getPorId(coberturaSeccionDTO.getId());
	}

	public void borrar(CoberturaSeccionDTO coberturaSeccionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		coberturaSeccionSN.borrar(coberturaSeccionDTO);
	}
	
	public List<CoberturaSeccionDTO> buscarPorPropiedad (String propiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {	
		return new CoberturaSeccionSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<CoberturaSeccionDTO> listarVigentesPorSeccion (BigDecimal idToSeccion) throws SystemException, ExcepcionDeAccesoADatos {	
		return new CoberturaSeccionSN().listarVigentesPorSeccion(idToSeccion);
	}
}
