package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Jos� Luis Arellano
 * @since 14 de Agosto de 2009
 */
public class ExclusionDescuentoVarioTipoPolizaSN {
	private ExclusionDescuentoVarioTipoPolizaFacadeRemote beanRemoto;

	public ExclusionDescuentoVarioTipoPolizaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionDescuentoVarioTipoPolizaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionDescuentoVarioTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionDescuentoVarioTipoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionDescuentoVarioTipoPolizaDTO excDescuentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excDescuentoPorTipoPolizaDTO);
	}

	public void modificar(ExclusionDescuentoVarioTipoPolizaDTO excDescuentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excDescuentoPorTipoPolizaDTO);
	}

	public ExclusionDescuentoVarioTipoPolizaDTO getPorId(ExclusionDescuentoVarioTipoPolizaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionDescuentoVarioTipoPolizaDTO excDescuentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excDescuentoPorTipoPolizaDTO);
	}

	public List<ExclusionDescuentoVarioTipoPolizaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public ExclusionDescuentoVarioTipoPolizaDTO buscarPorIDs(BigDecimal idToTipoPoliza, BigDecimal idToDescuentoVario, BigDecimal idToCobertura){
		List<ExclusionDescuentoVarioTipoPolizaDTO> lista = beanRemoto.findByIDs(idToTipoPoliza, idToDescuentoVario, idToCobertura);
		ExclusionDescuentoVarioTipoPolizaDTO registro;
		if (lista == null || lista.isEmpty())
			registro=null;
		else
			registro = lista.get(0); 
		return registro;
	}
	
	public List<ExclusionDescuentoVarioTipoPolizaDTO> buscarVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza){
		return beanRemoto.getVigentesPorIdTipoPoliza(idToTipoPoliza);
	}
}
