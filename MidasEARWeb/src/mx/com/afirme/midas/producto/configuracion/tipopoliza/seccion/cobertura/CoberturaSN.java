/**
 * 
 */
package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.service.catalogos.cobertura.CoberturaDTOService;

/**
 * @author admin
 * 
 */
public class CoberturaSN {
	private CoberturaFacadeRemote beanRemoto;
	private CoberturaDTOService coberturaDTOService;

	public CoberturaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CoberturaFacadeRemote.class);
			coberturaDTOService = serviceLocator.getEJB(CoberturaDTOService.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<CoberturaDTO> list;
		try {
			list = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return list;

	}

	public void agregar(CoberturaDTO coberturaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(coberturaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(CoberturaDTO coberturaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(coberturaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CoberturaDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrar(CoberturaDTO coberturaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(coberturaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CoberturaDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaDTO> listarFiltrado(CoberturaDTO coberturaDTO, Boolean mostrarInactivos) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(coberturaDTO, mostrarInactivos);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<CoberturaDTO> getListarFiltrado(CoberturaDTO coberturaDTO, Boolean mostrarInactivos) throws ExcepcionDeAccesoADatos {
		try {
			return coberturaDTOService.getListarFiltrado(coberturaDTO, mostrarInactivos);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<CoberturaDTO> listarVigentesAutos() throws ExcepcionDeAccesoADatos {
		try {
			return coberturaDTOService.listarVigentesAutos();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void borradoLogico(CoberturaDTO CoberturaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.borradoLogico(CoberturaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<CoberturaDTO> listarVigentes() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarVigentes();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaDTO> listarCoberturasPorExcluir(BigDecimal idToCobertura, BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarCoberturasPorExcluir(idToCobertura, idToSeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaDTO> listarCoberturasNoExcluidas(
			BigDecimal idToCobertura, BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarCoberturasNoRequeridas(idToCobertura, idToSeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<CoberturaDTO> listarCoberturasNoAsociadasSeccion(BigDecimal idToSeccion){
		return beanRemoto.listarCoberturasPorAsociarSeccion(idToSeccion);
	}
	
	public List<CoberturaDTO> listarVigentesPorTipoPoliza(BigDecimal idToTipoPoliza) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarVigentesPorTipoPoliza(idToTipoPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	

}
