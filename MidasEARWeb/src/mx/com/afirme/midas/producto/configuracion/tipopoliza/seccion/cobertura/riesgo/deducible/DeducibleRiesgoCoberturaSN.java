package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 21 de Agosto de 2009
 */
public class DeducibleRiesgoCoberturaSN {
	private DeducibleRiesgoCoberturaFacadeRemote beanRemoto;

	public DeducibleRiesgoCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en DeducibleRiesgoCoberturaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(DeducibleRiesgoCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<DeducibleRiesgoCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(deducibleRiesgoCoberturaDTO);
	}

	public void modificar(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(deducibleRiesgoCoberturaDTO);
	}

	public DeducibleRiesgoCoberturaDTO getPorId(DeducibleRiesgoCoberturaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(deducibleRiesgoCoberturaDTO);
	}

	public List<DeducibleRiesgoCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<DeducibleRiesgoCoberturaDTO> buscarPorIdRiesgoIdCoberturaIdSeccion(BigDecimal idToRiesgo,BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.findByRiesgoCoberturaSeccion(idToRiesgo, idToCobertura, idToSeccion);
	}

	public void sincronizarDeduciblesRiesgoCobertura(BigDecimal idToRiesgo,
			BigDecimal idToCobertura, BigDecimal idToSeccion) {
		beanRemoto.synchronizeDeduciblesRiesgoCobertura(idToRiesgo, idToCobertura, idToSeccion);
	}

	public List<DeducibleRiesgoCoberturaDTO> listarFiltrado(
			DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO) {
		return beanRemoto.listarFiltrado(deducibleRiesgoCoberturaDTO);
	}
}
