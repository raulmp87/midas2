package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RamoSeccionSN {
	private RamoSeccionFacadeRemote beanRemoto;

	public RamoSeccionSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en RamoSeccionSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(RamoSeccionFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RamoSeccionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(RamoSeccionDTO RamoSeccionDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(RamoSeccionDTO);
	}

	public void modificar(RamoSeccionDTO RamoSeccionDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(RamoSeccionDTO);
	}

	public RamoSeccionDTO getPorId(RamoSeccionId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(RamoSeccionDTO RamoSeccionDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(RamoSeccionDTO);
	}

	public List<RamoSeccionDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<RamoSeccionDTO> listarRamosPorAsociar(BigDecimal idToSeccion)  throws SystemException, ExcepcionDeAccesoADatos {
		return beanRemoto.obtenerRamosSinAsociar(idToSeccion);
	}
}
