/**
 * 
 */
package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion.ExclusionDescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.cargacombo.ComboBean;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;

import org.apache.log4j.Logger;

/**
 * @author Jorge Cano
 * 
 */
public class CoberturaForm extends MidasBaseForm {
	
	private static Logger LOG = Logger.getLogger(CoberturaForm.class);

	private static final long serialVersionUID = 1L;
	private String idToCobertura;
	private String idTcRamo;
	private String idTcSubRamo;
	private String codigo;
	private String version="1";
	private String descripcion;
	private String nombreComercial;
	private String claveTipoSumaAsegurada;
	private String claveTipoCoaseguro;
	private String valorMinimoCoaseguro;
	private String valorMaximoCoaseguro;
	private String claveTipoLimiteCoaseguro;
	private String valorMinimoLimiteCoaseguro;
	private String valorMaximoLimiteCoaseguro;
	private String claveTipoDeducible;
	private String valorMinimoDeducible;
	private String valorMaximoDeducible;
	private String claveTipoLimiteDeducible;
	private String valorMinimoLimiteDeducible;
	private String valorMaximoLimiteDeducible;
	private String claveDesglosaRiesgos;
	private String claveIgualacion;
	private String clavePrimerRiesgo;
	private String claveMostrarPanCobertura;
	private String claveMostrarPanDescripcion;
	private String claveMostrarPanSumaAsegurada;
	private String claveMostrarPanCoaseguro;
	private String claveMostrarPanDeducible;
	private String claveMostrarPanPrimaNeta;
	private String claveMostrarCotCobertura;
	private String claveMostrarCotDescripcion;
	private String claveMostrarCotSumaAsegurada;
	private String claveMostrarCotCoaseguro;
	private String claveMostrarCotDeducible;
	private String claveMostrarCotPrimaNeta;
	private String claveMostrarPolCobertura;
	private String claveMostrarPolDescripcion;
	private String claveMostrarPolSumaAsegurada;
	private String claveMostrarPolCoaseguro;
	private String claveMostrarPolDeducible;
	private String claveMostrarPolPrimaNeta;
	private String numeroSecuencia;
	private String claveAutorizacion;
	private String claveImporteCero;
	/*private String claveActivoConfiguracion="1";
	private String claveActivoProduccion="0";*/
	private String idToSeccion;
	private String nombreComercialSeccion;
	private String TipoSumaAsegurada;
	private String TipoCoaseguro;
	private String TipoDeducible;
	private String TipoLimiteCoaseguro;
	private String TipoLimiteDeducible;
	private String descripcionRamo;
	private String descripcionSubRamo;
	private String claveTipoCalculo;
	
	private String descripcionRegistroCNSF;
	private String idControlArchivoRegistroCNSF="0";
	private String nombreArchivoRegistroCNSF=null;
	private List<DescuentoVarioCoberturaDTO> descuentosAsociados;
	private List<DescuentoDTO> descuentosPorAsociar;
	private List<AumentoVarioCoberturaDTO> aumentosAsociados;
	private List<AumentoVarioDTO> aumentosPorAsociar;
	private List<RecargoVarioCoberturaDTO> recargosAsociados;
	private List<RecargoVarioDTO> recargosPorAsociar;
	private List<RiesgoCoberturaDTO> riesgoCoberturaAsociados;
	private List<RiesgoDTO> riesgosPorAsociar;
	private List<ExclusionDescuentoVarioCoberturaDTO> excDescuentoCoberturaAsociados;
	private List<ExclusionDescuentoVarioCoberturaDTO> excDescuentoCoberturaNoAsociados;
	private List<ExclusionAumentoVarioCoberturaDTO> excAumentoCoberturaAsociados;
	private List<ExclusionAumentoVarioCoberturaDTO> excAumentoCoberturaNoAsociados;
	private List<ExclusionRecargoVarioCoberturaDTO> excRecargoCoberturaAsociados;
	private List<ExclusionRecargoVarioCoberturaDTO> excRecargoCoberturaNoAsociados;
	private List<CoberturaExcluidaDTO> coberturasExcluidas;
	private List<CoberturaDTO> coberturasPorExcluir;
	private List<CoberturaRequeridaDTO> coberturasRequeridas;
	private List<CoberturaDTO> coberturasNoRequeridas;
	private List<DeducibleCoberturaDTO> deducibles;
	private List<CoaseguroCoberturaDTO> coaseguros;
	private List<CoberturaDTO> coberturasSumaAsegurada;
	private String nombreCoberturaSumaAsegurada;
	private Boolean coberturaAsociada;
	private List<RiesgoDTO> riesgosAsociados;
	private String mostrarInactivos;
	private String idCoberturaSumaAsegurada;
	private List<ComboBean> claveTipoCalculoList;
	
	//Nuevos datos para el cotizador Casa
	private String factorMinSumaAsegurada;
	private String factorMaxSumaAsegurada;
	private String factorSumaAsegurada;
	private String leyenda;
	private String editable;
	private String claveTipoValorSumaAsegurada;
	private String minSumaAsegurada;
	private String maxSumaAsegurada;
	
//	private int identificadorCobertura;
	private String claveFuenteSumaAsegurada;
	private String coberturaEsPropia;
	private String valMinPrimaCobertura;
	private String coberturaAplicaDescuentoRecargo;
//	private String fechaInicioVigencia;
	private String claveNegocio;
	private String permiteDeducibleCero;
	
	private String fechaRegistro;
    private String numeroRegistro;
    private String reinstalable;
    private String seguroObligatorio;
    private String factorRecargo;

	public String getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(String idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}

	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	public String getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}

	public void setClaveTipoSumaAsegurada(String claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	public String getClaveTipoCoaseguro() {
		return claveTipoCoaseguro;
	}

	public void setClaveTipoCoaseguro(String claveTipoCoaseguro) {
		this.claveTipoCoaseguro = claveTipoCoaseguro;
	}

	public String getValorMinimoCoaseguro() {
		return valorMinimoCoaseguro;
	}

	public void setValorMinimoCoaseguro(String valorMinimoCoaseguro) {
		this.valorMinimoCoaseguro = valorMinimoCoaseguro;
	}

	public String getValorMaximoCoaseguro() {
		return valorMaximoCoaseguro;
	}

	public void setValorMaximoCoaseguro(String valorMaximoCoaseguro) {
		this.valorMaximoCoaseguro = valorMaximoCoaseguro;
	}

	public String getClaveTipoLimiteCoaseguro() {
		return claveTipoLimiteCoaseguro;
	}

	public void setClaveTipoLimiteCoaseguro(String claveTipoLimiteCoaseguro) {
		this.claveTipoLimiteCoaseguro = claveTipoLimiteCoaseguro;
	}

	public String getValorMinimoLimiteCoaseguro() {
		return valorMinimoLimiteCoaseguro;
	}

	public void setValorMinimoLimiteCoaseguro(String valorMinimoLimiteCoaseguro) {
		this.valorMinimoLimiteCoaseguro = valorMinimoLimiteCoaseguro;
	}

	public String getValorMaximoLimiteCoaseguro() {
		return valorMaximoLimiteCoaseguro;
	}

	public void setValorMaximoLimiteCoaseguro(String valorMaximoLimiteCoaseguro) {
		this.valorMaximoLimiteCoaseguro = valorMaximoLimiteCoaseguro;
	}

	public String getClaveTipoDeducible() {
		return claveTipoDeducible;
	}

	public void setClaveTipoDeducible(String claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	public String getValorMinimoDeducible() {
		return valorMinimoDeducible;
	}

	public void setValorMinimoDeducible(String valorMinimoDeducible) {
		this.valorMinimoDeducible = valorMinimoDeducible;
	}

	public String getValorMaximoDeducible() {
		return valorMaximoDeducible;
	}

	public void setValorMaximoDeducible(String valorMaximoDeducible) {
		this.valorMaximoDeducible = valorMaximoDeducible;
	}

	public String getClaveTipoLimiteDeducible() {
		return claveTipoLimiteDeducible;
	}

	public void setClaveTipoLimiteDeducible(String claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}

	public String getValorMinimoLimiteDeducible() {
		return valorMinimoLimiteDeducible;
	}

	public void setValorMinimoLimiteDeducible(String valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}

	public String getValorMaximoLimiteDeducible() {
		return valorMaximoLimiteDeducible;
	}

	public void setValorMaximoLimiteDeducible(String valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}

	public String getClaveDesglosaRiesgos() {
		return claveDesglosaRiesgos;
	}

	public void setClaveDesglosaRiesgos(String claveDesglosaRiesgos) {
		this.claveDesglosaRiesgos = claveDesglosaRiesgos;
	}
	
	public String getClaveIgualacion() {
		return claveIgualacion;
	}

	public void setClaveIgualacion(String claveIgualacion) {
		this.claveIgualacion = claveIgualacion;
	}
	
	public String getClavePrimerRiesgo() {
		return clavePrimerRiesgo;
	}

	public void setClavePrimerRiesgo(String clavePrimerRiesgo) {
		this.clavePrimerRiesgo = clavePrimerRiesgo;
	}

	public String getClaveMostrarPanCobertura() {
		return claveMostrarPanCobertura;
	}

	public void setClaveMostrarPanCobertura(String claveMostrarPanCobertura) {
		this.claveMostrarPanCobertura = claveMostrarPanCobertura;
	}

	public String getClaveMostrarPanDescripcion() {
		return claveMostrarPanDescripcion;
	}

	public void setClaveMostrarPanDescripcion(String claveMostrarPanDescripcion) {
		this.claveMostrarPanDescripcion = claveMostrarPanDescripcion;
	}

	public String getClaveMostrarPanSumaAsegurada() {
		return claveMostrarPanSumaAsegurada;
	}

	public void setClaveMostrarPanSumaAsegurada(
			String claveMostrarPanSumaAsegurada) {
		this.claveMostrarPanSumaAsegurada = claveMostrarPanSumaAsegurada;
	}

	public String getClaveMostrarPanCoaseguro() {
		return claveMostrarPanCoaseguro;
	}

	public void setClaveMostrarPanCoaseguro(String claveMostrarPanCoaseguro) {
		this.claveMostrarPanCoaseguro = claveMostrarPanCoaseguro;
	}

	public String getClaveMostrarPanDeducible() {
		return claveMostrarPanDeducible;
	}

	public void setClaveMostrarPanDeducible(String claveMostrarPanDeducible) {
		this.claveMostrarPanDeducible = claveMostrarPanDeducible;
	}

	public String getClaveMostrarPanPrimaNeta() {
		return claveMostrarPanPrimaNeta;
	}

	public void setClaveMostrarPanPrimaNeta(String claveMostrarPanPrimaNeta) {
		this.claveMostrarPanPrimaNeta = claveMostrarPanPrimaNeta;
	}

	public String getClaveMostrarCotCobertura() {
		return claveMostrarCotCobertura;
	}

	public void setClaveMostrarCotCobertura(String claveMostrarCotCobertura) {
		this.claveMostrarCotCobertura = claveMostrarCotCobertura;
	}

	public String getClaveMostrarCotDescripcion() {
		return claveMostrarCotDescripcion;
	}

	public void setClaveMostrarCotDescripcion(String claveMostrarCotDescripcion) {
		this.claveMostrarCotDescripcion = claveMostrarCotDescripcion;
	}

	public String getClaveMostrarCotSumaAsegurada() {
		return claveMostrarCotSumaAsegurada;
	}

	public void setClaveMostrarCotSumaAsegurada(
			String claveMostrarCotSumaAsegurada) {
		this.claveMostrarCotSumaAsegurada = claveMostrarCotSumaAsegurada;
	}

	public String getClaveMostrarCotCoaseguro() {
		return claveMostrarCotCoaseguro;
	}

	public void setClaveMostrarCotCoaseguro(String claveMostrarCotCoaseguro) {
		this.claveMostrarCotCoaseguro = claveMostrarCotCoaseguro;
	}

	public String getClaveMostrarCotDeducible() {
		return claveMostrarCotDeducible;
	}

	public void setClaveMostrarCotDeducible(String claveMostrarCotDeducible) {
		this.claveMostrarCotDeducible = claveMostrarCotDeducible;
	}

	public String getClaveMostrarCotPrimaNeta() {
		return claveMostrarCotPrimaNeta;
	}

	public void setClaveMostrarCotPrimaNeta(String claveMostrarCotPrimaNeta) {
		this.claveMostrarCotPrimaNeta = claveMostrarCotPrimaNeta;
	}

	public String getClaveMostrarPolCobertura() {
		return claveMostrarPolCobertura;
	}

	public void setClaveMostrarPolCobertura(String claveMostrarPolCobertura) {
		this.claveMostrarPolCobertura = claveMostrarPolCobertura;
	}

	public String getClaveMostrarPolDescripcion() {
		return claveMostrarPolDescripcion;
	}

	public void setClaveMostrarPolDescripcion(String claveMostrarPolDescripcion) {
		this.claveMostrarPolDescripcion = claveMostrarPolDescripcion;
	}

	public String getClaveMostrarPolSumaAsegurada() {
		return claveMostrarPolSumaAsegurada;
	}

	public void setClaveMostrarPolSumaAsegurada(
			String claveMostrarPolSumaAsegurada) {
		this.claveMostrarPolSumaAsegurada = claveMostrarPolSumaAsegurada;
	}

	public String getClaveMostrarPolCoaseguro() {
		return claveMostrarPolCoaseguro;
	}

	public void setClaveMostrarPolCoaseguro(String claveMostrarPolCoaseguro) {
		this.claveMostrarPolCoaseguro = claveMostrarPolCoaseguro;
	}

	public String getClaveMostrarPolDeducible() {
		return claveMostrarPolDeducible;
	}

	public void setClaveMostrarPolDeducible(String claveMostrarPolDeducible) {
		this.claveMostrarPolDeducible = claveMostrarPolDeducible;
	}

	public String getClaveMostrarPolPrimaNeta() {
		return claveMostrarPolPrimaNeta;
	}

	public void setClaveMostrarPolPrimaNeta(String claveMostrarPolPrimaNeta) {
		this.claveMostrarPolPrimaNeta = claveMostrarPolPrimaNeta;
	}

	public String getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(String numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public String getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(String claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	public String getClaveImporteCero() {
		return claveImporteCero;
	}

	public void setClaveImporteCero(String claveImporteCero) {
		this.claveImporteCero = claveImporteCero;
	}

	/*public String getClaveActivoConfiguracion() {
		return claveActivoConfiguracion;
	}

	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}

	public String getClaveActivoProduccion() {
		return claveActivoProduccion;
	}

	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/

	public String getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public void setNombreComercialSeccion(String nombreComercialSeccion) {
		this.nombreComercialSeccion = nombreComercialSeccion;
	}

	public String getNombreComercialSeccion() {
		return nombreComercialSeccion;
	}

	public void setDescuentosAsociados(List<DescuentoVarioCoberturaDTO> descuentosAsociados) {
		this.descuentosAsociados = descuentosAsociados;
	}

	public List<DescuentoVarioCoberturaDTO> getDescuentosAsociados() {
		return descuentosAsociados;
	}

	public void setDescuentosPorAsociar(List<DescuentoDTO> descuentosPorAsociar) {
		this.descuentosPorAsociar = descuentosPorAsociar;
	}

	public List<DescuentoDTO> getDescuentosPorAsociar() {
		return descuentosPorAsociar;
	}

	public void setAumentosAsociados(List<AumentoVarioCoberturaDTO> aumentosAsociados) {
		this.aumentosAsociados = aumentosAsociados;
	}

	public List<AumentoVarioCoberturaDTO> getAumentosAsociados() {
		return aumentosAsociados;
	}

	public void setAumentosPorAsociar(List<AumentoVarioDTO> aumentosPorAsociar) {
		this.aumentosPorAsociar = aumentosPorAsociar;
	}

	public List<AumentoVarioDTO> getAumentosPorAsociar() {
		return aumentosPorAsociar;
	}

	public void setRecargosAsociados(List<RecargoVarioCoberturaDTO> recargosAsociados) {
		this.recargosAsociados = recargosAsociados;
	}

	public List<RecargoVarioCoberturaDTO> getRecargosAsociados() {
		return recargosAsociados;
	}

	public void setRecargosPorAsociar(List<RecargoVarioDTO> recargosPorAsociar) {
		this.recargosPorAsociar = recargosPorAsociar;
	}

	public List<RecargoVarioDTO> getRecargosPorAsociar() {
		return recargosPorAsociar;
	}

	public List<RiesgoCoberturaDTO> getRiesgoCoberturaAsociados() {
		return riesgoCoberturaAsociados;
	}

	public void setRiesgoCoberturaAsociados(
			List<RiesgoCoberturaDTO> riesgoCoberturaAsociados) {
		this.riesgoCoberturaAsociados = riesgoCoberturaAsociados;
	}

	public List<RiesgoDTO> getRiesgosPorAsociar() {
		return riesgosPorAsociar;
	}

	public void setRiesgosPorAsociar(List<RiesgoDTO> riesgosPorAsociar) {
		this.riesgosPorAsociar = riesgosPorAsociar;
	}

	public List<ExclusionDescuentoVarioCoberturaDTO> getExcDescuentoCoberturaAsociados() {
		return excDescuentoCoberturaAsociados;
	}

	public void setExcDescuentoCoberturaAsociados(
			List<ExclusionDescuentoVarioCoberturaDTO> excDescuentoCoberturaAsociados) {
		this.excDescuentoCoberturaAsociados = excDescuentoCoberturaAsociados;
	}

	public List<ExclusionDescuentoVarioCoberturaDTO> getExcDescuentoCoberturaNoAsociados() {
		return excDescuentoCoberturaNoAsociados;
	}

	public void setExcDescuentoCoberturaNoAsociados(
			List<ExclusionDescuentoVarioCoberturaDTO> excDescuentoCoberturaNoAsociados) {
		this.excDescuentoCoberturaNoAsociados = excDescuentoCoberturaNoAsociados;
	}

	public List<ExclusionAumentoVarioCoberturaDTO> getExcAumentoCoberturaAsociados() {
		return excAumentoCoberturaAsociados;
	}

	public void setExcAumentoCoberturaAsociados(
			List<ExclusionAumentoVarioCoberturaDTO> excAumentoCoberturaAsociados) {
		this.excAumentoCoberturaAsociados = excAumentoCoberturaAsociados;
	}

	public List<ExclusionAumentoVarioCoberturaDTO> getExcAumentoCoberturaNoAsociados() {
		return excAumentoCoberturaNoAsociados;
	}

	public void setExcAumentoCoberturaNoAsociados(
			List<ExclusionAumentoVarioCoberturaDTO> excAumentoCoberturaNoAsociados) {
		this.excAumentoCoberturaNoAsociados = excAumentoCoberturaNoAsociados;
	}

	public void setExcRecargoCoberturaAsociados(
			List<ExclusionRecargoVarioCoberturaDTO> excRecargoCoberturaAsociados) {
		this.excRecargoCoberturaAsociados = excRecargoCoberturaAsociados;
	}

	public List<ExclusionRecargoVarioCoberturaDTO> getExcRecargoCoberturaAsociados() {
		return excRecargoCoberturaAsociados;
	}

	public void setExcRecargoCoberturaNoAsociados(
			List<ExclusionRecargoVarioCoberturaDTO> excRecargoCoberturaNoAsociados) {
		this.excRecargoCoberturaNoAsociados = excRecargoCoberturaNoAsociados;
	}

	public List<ExclusionRecargoVarioCoberturaDTO> getExcRecargoCoberturaNoAsociados() {
		return excRecargoCoberturaNoAsociados;
	}

	public void setCoberturasExcluidas(List<CoberturaExcluidaDTO> coberturasExcluidas) {
		this.coberturasExcluidas = coberturasExcluidas;
	}

	public List<CoberturaExcluidaDTO> getCoberturasExcluidas() {
		return coberturasExcluidas;
	}

	public void setCoberturasPorExcluir(List<CoberturaDTO> coberturasPorExcluir) {
		this.coberturasPorExcluir = coberturasPorExcluir;
	}

	public List<CoberturaDTO> getCoberturasPorExcluir() {
		return coberturasPorExcluir;
	}

	public void setCoberturasRequeridas(List<CoberturaRequeridaDTO> coberturasRequeridas) {
		this.coberturasRequeridas = coberturasRequeridas;
	}

	public List<CoberturaRequeridaDTO> getCoberturasRequeridas() {
		return coberturasRequeridas;
	}

	public void setCoberturasNoRequeridas(List<CoberturaDTO> coberturasNoRequeridas) {
		this.coberturasNoRequeridas = coberturasNoRequeridas;
	}

	public List<CoberturaDTO> getCoberturasNoRequeridas() {
		return coberturasNoRequeridas;
	}

	public void setDeducibles(List<DeducibleCoberturaDTO> deducibles) {
		this.deducibles = deducibles;
	}

	public List<DeducibleCoberturaDTO> getDeducibles() {
		return deducibles;
	}

	public void setCoaseguros(List<CoaseguroCoberturaDTO> coaseguros) {
		this.coaseguros = coaseguros;
	}

	public List<CoaseguroCoberturaDTO> getCoaseguros() {
		return coaseguros;
	}
	public String getTipoSumaAsegurada() {
		return TipoSumaAsegurada;
	}
	public void setTipoSumaAsegurada(String tipoSumaAsegurada) {
		TipoSumaAsegurada = tipoSumaAsegurada;
	}
	public String getTipoCoaseguro() {
		return TipoCoaseguro;
	}
	public void setTipoCoaseguro(String tipoCoaseguro) {
		TipoCoaseguro = tipoCoaseguro;
	}
	public String getTipoDeducible() {
		return TipoDeducible;
	}
	public void setTipoDeducible(String tipoDeducible) {
		TipoDeducible = tipoDeducible;
	}
	public String getTipoLimiteCoaseguro() {
		return TipoLimiteCoaseguro;
	}
	public void setTipoLimiteCoaseguro(String tipoLimiteCoaseguro) {
		TipoLimiteCoaseguro = tipoLimiteCoaseguro;
	}
	public String getTipoLimiteDeducible() {
		return TipoLimiteDeducible;
	}
	public void setTipoLimiteDeducible(String tipoLimiteDeducible) {
		TipoLimiteDeducible = tipoLimiteDeducible;
	}
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public List<CoberturaDTO> getCoberturasSumaAsegurada() {
		if(claveNegocio != null && claveNegocio.equals("A")){
			try {
				coberturasSumaAsegurada=new ArrayList<CoberturaDTO>(1);
				CoberturaDTO cobertura = new CoberturaDTO();
				cobertura.setIdToCobertura(BigDecimal.ZERO);
				cobertura.setDescripcion("NO APLICA");
				coberturasSumaAsegurada.add(cobertura);
				coberturasSumaAsegurada.addAll(CoberturaDN.getInstancia().listarVigentesAutos());
			} catch (ExcepcionDeAccesoADatos e) {
				LOG.error(e);
			} catch (SystemException e) {
				LOG.error(e);
			}
		}else{
			coberturasSumaAsegurada=new ArrayList<CoberturaDTO>(1);
			try {
				coberturasSumaAsegurada = CoberturaDN.getInstancia().buscarPorPropiedad("claveTipoSumaAsegurada", "1");
			} catch (ExcepcionDeAccesoADatos e) {
				LOG.error(e);
			} catch (SystemException e) {
				LOG.error(e);
			}
		}
		return coberturasSumaAsegurada;
	}
	public void setCoberturasSumaAsegurada(List<CoberturaDTO> coberturasSumaAsegurada) {
		this.coberturasSumaAsegurada = coberturasSumaAsegurada;
	}
	public String getNombreCoberturaSumaAsegurada() {
		return nombreCoberturaSumaAsegurada;
	}
	public void setNombreCoberturaSumaAsegurada(String nombreCoberturaSumaAsegurada) {
		this.nombreCoberturaSumaAsegurada = nombreCoberturaSumaAsegurada;
	}

	public List<RiesgoDTO> getRiesgosAsociados() {
		return riesgosAsociados;
	}
	public void setRiesgosAsociados(List<RiesgoDTO> riesgosAsociados) {
		this.riesgosAsociados = riesgosAsociados;
	}

	public void setCoberturaAsociada(Boolean coberturaAsociada) {
		this.coberturaAsociada = coberturaAsociada;
	}

	public Boolean getCoberturaAsociada() {
		return coberturaAsociada;
	}

	public void setMostrarInactivos(String mostrarInactivos) {
		this.mostrarInactivos = mostrarInactivos;
	}

	public String getMostrarInactivos() {
		return mostrarInactivos;
	}
	public String getIdControlArchivoRegistroCNSF() {
		return idControlArchivoRegistroCNSF;
	}
	public void setIdControlArchivoRegistroCNSF(String idControlArchivoRegistroCNSF) {
		this.idControlArchivoRegistroCNSF = idControlArchivoRegistroCNSF;
	}
	public String getDescripcionRegistroCNSF() {
		return descripcionRegistroCNSF;
	}
	public void setDescripcionRegistroCNSF(String descripcionRegistroCNSF) {
		this.descripcionRegistroCNSF = descripcionRegistroCNSF;
	}
	public String getNombreArchivoRegistroCNSF() {
		if (!this.idControlArchivoRegistroCNSF.equals("0") && this.nombreArchivoRegistroCNSF==null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivoRegistroCNSF));
				nombreArchivoRegistroCNSF = controlArchivoDTO.getNombreArchivoOriginal();
			} catch (SystemException e) {
				nombreArchivoRegistroCNSF = "NO DISPONIBLE";
				LOG.error(e);
			}
		}
		return nombreArchivoRegistroCNSF;
	}
	public void setNombreArchivoRegistroCNSF(String nombreArchivoRegistroCNSF) {
		this.nombreArchivoRegistroCNSF = nombreArchivoRegistroCNSF;
	}

	public String getIdCoberturaSumaAsegurada() {
		return idCoberturaSumaAsegurada;
	}

	public void setIdCoberturaSumaAsegurada(String idCoberturaSumaAsegurada) {
		this.idCoberturaSumaAsegurada = idCoberturaSumaAsegurada;
	}

	public String getLeyenda() {
		return leyenda;
	}

	public void setLeyenda(String leyenda) {
		this.leyenda = leyenda;
	}

	public String getClaveTipoValorSumaAsegurada() {
		return claveTipoValorSumaAsegurada;
	}

	public void setClaveTipoValorSumaAsegurada(String claveTipoValorSumaAsegurada) {
		this.claveTipoValorSumaAsegurada = claveTipoValorSumaAsegurada;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getFactorMinSumaAsegurada() {
		return factorMinSumaAsegurada;
	}

	public void setFactorMinSumaAsegurada(String factorMinSumaAsegurada) {
		this.factorMinSumaAsegurada = factorMinSumaAsegurada;
	}

	public String getFactorMaxSumaAsegurada() {
		return factorMaxSumaAsegurada;
	}

	public void setFactorMaxSumaAsegurada(String factorMaxSumaAsegurada) {
		this.factorMaxSumaAsegurada = factorMaxSumaAsegurada;
	}

	public String getFactorSumaAsegurada() {
		return factorSumaAsegurada;
	}

	public void setFactorSumaAsegurada(String factorSumaAsegurada) {
		this.factorSumaAsegurada = factorSumaAsegurada;
	}

	public String getMinSumaAsegurada() {
		return minSumaAsegurada;
	}

	public void setMinSumaAsegurada(String minSumaAsegurada) {
		this.minSumaAsegurada = minSumaAsegurada;
	}

	public String getMaxSumaAsegurada() {
		return maxSumaAsegurada;
	}

	public void setMaxSumaAsegurada(String maxSumaAsegurada) {
		this.maxSumaAsegurada = maxSumaAsegurada;
	}

	public String getCoberturaEsPropia() {
		return coberturaEsPropia;
	}
	
	public void setCoberturaEsPropia(String coberturaEsPropia) {
		this.coberturaEsPropia = coberturaEsPropia;
	}


	public String getValMinPrimaCobertura() {
		return valMinPrimaCobertura;
	}

	public void setValMinPrimaCobertura(String valMinPrimaCobertura) {
		this.valMinPrimaCobertura = valMinPrimaCobertura;
	}

	public String getCoberturaAplicaDescuentoRecargo() {
		return coberturaAplicaDescuentoRecargo;
	}

	public void setCoberturaAplicaDescuentoRecargo(String coberturaAplicaDescuentoRecargo) {
		this.coberturaAplicaDescuentoRecargo = coberturaAplicaDescuentoRecargo;
	}

	public String getClaveFuenteSumaAsegurada() {
		return claveFuenteSumaAsegurada;
	}

	public void setClaveFuenteSumaAsegurada(String claveFuenteSumaAsegurada) {
		this.claveFuenteSumaAsegurada = claveFuenteSumaAsegurada;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public void setPermiteDeducibleCero(String permiteDeducibleCero) {
		this.permiteDeducibleCero = permiteDeducibleCero;
	}

	public String getPermiteDeducibleCero() {
		return permiteDeducibleCero;
	}

	public void setClaveTipoCalculo(String claveTipoCalculo) {
		this.claveTipoCalculo = claveTipoCalculo;
	}

	public String getClaveTipoCalculo() {
		return claveTipoCalculo;
	}

	public void setClaveTipoCalculoList(List<ComboBean> claveTipoCalculoList) {
		this.claveTipoCalculoList = claveTipoCalculoList;
	}

	public List<ComboBean> getClaveTipoCalculoList() {
		try {
			claveTipoCalculoList=new ArrayList<ComboBean>(1);
			List<CatalogoValorFijoDTO> list  = ValorFijoDN.getInstancia().buscarPorPropiedad("id.idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_CLAVE_TIPO_CALCULO);
			for(CatalogoValorFijoDTO item: list){
				String[] valores = item.getDescripcion().split("-");
				ComboBean combo = new ComboBean();
				combo.setId(valores[0].trim());
				combo.setDescripcion(item.getDescripcion());
				claveTipoCalculoList.add(combo);
			}
		} catch (ExcepcionDeAccesoADatos e) {
			LOG.error(e);
		} catch (SystemException e) {
			LOG.error(e);
		}
		return claveTipoCalculoList;
	}
	
	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public String getReinstalable() {
		return reinstalable;
	}

	public void setReinstalable(String reinstalable) {
		this.reinstalable = reinstalable;
	}

	public void setSeguroObligatorio(String seguroObligatorio) {
		this.seguroObligatorio = seguroObligatorio;
	}

	public String getSeguroObligatorio() {
		return seguroObligatorio;
	}

	public String getFactorRecargo() {
		return factorRecargo;
	}

	public void setFactorRecargo(String factorRecargo) {
		this.factorRecargo = factorRecargo;
	}
	
}
