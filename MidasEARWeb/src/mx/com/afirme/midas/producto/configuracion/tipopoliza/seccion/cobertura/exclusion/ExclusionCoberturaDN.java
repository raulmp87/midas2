package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ExclusionCoberturaDN {
	private static final ExclusionCoberturaDN INSTANCIA = new ExclusionCoberturaDN();

	public static ExclusionCoberturaDN getInstancia() {
		return ExclusionCoberturaDN.INSTANCIA;
	}

	public List<CoberturaExcluidaDTO> buscarPorCoberturaBase(BigDecimal idToCoberturaBase)
			throws SystemException, ExcepcionDeAccesoADatos {
		ExclusionCoberturaSN coberturaSN = new ExclusionCoberturaSN();
		return coberturaSN.buscarPorPropiedad("id.idToCobertura", idToCoberturaBase);
	}

}
