package mx.com.afirme.midas.producto.configuracion.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ExclusionDescuentoVarioProductoSN {
	private ExclusionDescuentoVarioProductoFacadeRemote beanRemoto;

	public ExclusionDescuentoVarioProductoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionDescuentoVarioProductoSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionDescuentoVarioProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionDescuentoVarioProductoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionDescuentoVarioProductoDTO excDescuentoPorProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excDescuentoPorProductoDTO);
	}

	public void modificar(ExclusionDescuentoVarioProductoDTO excDescuentoPorProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excDescuentoPorProductoDTO);
	}

	public ExclusionDescuentoVarioProductoDTO getPorId(ExclusionDescuentoVarioProductoId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionDescuentoVarioProductoDTO excDescuentoPorProductoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excDescuentoPorProductoDTO);
	}

	public List<ExclusionDescuentoVarioProductoDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<ExclusionDescuentoVarioProductoDTO> buscarVigentesPorIdProducto(BigDecimal idToProducto){
		return beanRemoto.getVigentesPorIdProducto(idToProducto);
	}
}
