package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDN;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * @author Fernando Alonzo
 * @since 17 de agosto de 2009
 */
public class AumentoVarioCoberturaAction extends CoberturaAction {

	/**
	 * Method asociarAumento
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward asociarAumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarAumentoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarAumentoAsociado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		AumentoVarioDN aumentoVarioDN = AumentoVarioDN.getInstancia();
		String action = "";
		AumentoVarioCoberturaId id = new AumentoVarioCoberturaId();
		BigDecimal idToAumentoVario = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre"));
		id.setIdtoaumentovario(idToAumentoVario);
		id.setIdtocobertura(idToCobertura);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				AumentoVarioCoberturaDTO aumentoPorCobertura = new AumentoVarioCoberturaDTO();
				aumentoPorCobertura.setId(id);
				aumentoPorCobertura.setClaveaplicareaseguro(Short.parseShort(request.getParameter("aplicaReaseguro")));
				aumentoPorCobertura.setClavecomercialtecnico(Short.parseShort(request.getParameter("comercialTecnico")));
				aumentoPorCobertura.setClaveobligatoriedad(Short.parseShort(request.getParameter("obligatorio").equals("1")? "3" : "0"));
				aumentoPorCobertura.setValor(Double.parseDouble(request.getParameter("valor")));

				AumentoVarioDTO aumentoDTO = new AumentoVarioDTO();
				aumentoDTO.setIdAumentoVario(aumentoPorCobertura.getId().getIdtoaumentovario());
				aumentoDTO = aumentoVarioDN.getPorId(aumentoDTO);

				CoberturaDTO cobertura = new  CoberturaDTO();
				cobertura.setIdToCobertura(id.getIdtocobertura());
				cobertura = coberturaDN.getPorId(cobertura);
				aumentoPorCobertura.setCoberturaDTO(cobertura);
				aumentoPorCobertura.setAumentoVarioDTO(aumentoDTO);
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					action = "insert";
					coberturaDN.asociarAumento(coberturaDTO, aumentoPorCobertura);
					mensajeExitoAgregar(coberturaForm, request);
				} else {
					action = "update";
					coberturaDN.actualizarAsociacion(coberturaDTO, aumentoPorCobertura);
					mensajeExitoModificar(coberturaForm, request);
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				action = "deleted";
				AumentoVarioCoberturaDTO aumentoPorCobertura = new AumentoVarioCoberturaDTO();
				aumentoPorCobertura.setId(id);
				coberturaDN.desasociarAumento(coberturaDTO, aumentoPorCobertura);
				mensajeExitoBorrar(coberturaForm, request);
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(coberturaForm, request, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mensajeExcepcion(coberturaForm, request, e);
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarAumentosAsociados(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		coberturaForm.setAumentosAsociados(coberturaDN.getPorIdCascada(coberturaDTO).getAumentos());
		
		
//		String json = "{rows:[";
//		if(coberturaForm.getAumentosAsociados() != null && coberturaForm.getAumentosAsociados().size() > 0) {
//			for(AumentoVarioCoberturaDTO aumento : coberturaForm.getAumentosAsociados()) {
//				json += "{id:\"" + aumento.getId().getIdtoaumentovario() + "\",data:[";
//				json += id + ",\"";
//				json += aumento.getAumentoVarioDTO().getDescripcionAumento() + "\",\"";
//				json += aumento.getClaveobligatoriedad() + "\",";
//				json += aumento.getClavecomercialtecnico() + ",";
//				json += aumento.getClaveaplicareaseguro() + ",\"";
//				json += aumento.getAumentoVarioDTO().getClaveTipoAumento().equals("1")? "%":"$" + "\",";				
//				json += aumento.getValor() + "]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getAumentosAsociados() != null && coberturaForm.getAumentosAsociados().size() > 0) {
			for(AumentoVarioCoberturaDTO aumento : coberturaForm.getAumentosAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				
				row.setId(aumento.getId().getIdtoaumentovario().toString());
				row.setDatos(
						id,
						aumento.getAumentoVarioDTO().getDescripcionAumento(),
						aumento.getClaveobligatoriedad().toString(),
						aumento.getClavecomercialtecnico().toString(),
						aumento.getClaveaplicareaseguro().toString(),
						aumento.getAumentoVarioDTO().getClaveTipoAumento().equals("1")? "%":"$",
						aumento.getValor().toString()		
				);
				json.addRow(row);
			}
		}
		
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarAumentosPorAsociar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		
		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		coberturaForm.setAumentosPorAsociar(coberturaDN.listarAumentoPorAsociarCobertura(coberturaDTO));
		
		
//		String json = "{rows:[";
//		if(coberturaForm.getAumentosPorAsociar() != null && coberturaForm.getAumentosPorAsociar().size() > 0) {
//			for(AumentoVarioDTO aumento : coberturaForm.getAumentosPorAsociar()) {
//				json += "{id:\"" + aumento.getIdAumentoVario() + "\",data:[";
//				json += id + ",\"" +aumento.getDescripcionAumento() + "\"";
//				json += ",0,1,0,\""+(aumento.getClaveTipoAumento().intValue()==1?"%":"$")+"\",0]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getAumentosPorAsociar() != null && coberturaForm.getAumentosPorAsociar().size() > 0) {
			for(AumentoVarioDTO aumento : coberturaForm.getAumentosPorAsociar()) {
				
				MidasJsonRow row = new MidasJsonRow();
				
				row.setId(aumento.getIdAumentoVario().toString());
				row.setDatos(
						id,
						aumento.getDescripcionAumento(),
						"0",
						"1",
						"0",
						(aumento.getClaveTipoAumento().intValue()==1?"%":"$"),
						"0"
				);
				json.addRow(row);
			}
		} 
		
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
