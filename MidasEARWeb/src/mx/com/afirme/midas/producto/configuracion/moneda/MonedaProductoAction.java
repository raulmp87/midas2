package mx.com.afirme.midas.producto.configuracion.moneda;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.producto.ProductoAction;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MonedaProductoAction extends ProductoAction {
	/**
	 * Method asociarMoneda
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward asociarMoneda(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoForm productoForm = (ProductoForm) form;
		String idToProducto = request.getParameter("id");
		if (idToProducto != null)
			productoForm.setIdToProducto(idToProducto);
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarMonedaAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarMonedaAsociada(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoDTO productoDTO = new ProductoDTO();
		ProductoDN productoDN = ProductoDN.getInstancia();
		String action = "";
		MonedaProductoId id = new MonedaProductoId();
		BigDecimal idMoneda = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToProducto = UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre"));
		id.setIdMoneda(idMoneda);
		id.setIdToProducto(idToProducto);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				MonedaProductoDTO monedaProductoDTO = new MonedaProductoDTO();
				monedaProductoDTO.setId(id);

				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					productoDN.asociarMoneda(productoDTO, monedaProductoDTO);
					action = "insert";
				} else {
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				MonedaProductoDTO monedaProductoDTO = new MonedaProductoDTO();
				monedaProductoDTO.setId(id);
				productoDN.desasociarMoneda(productoDTO, monedaProductoDTO);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarMonedasAsociadas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		ProductoForm productoForm = (ProductoForm) form;

		String id = request.getParameter("id");
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		ProductoDN productoDN =  ProductoDN.getInstancia();
		productoForm.setMonedasAsociadas(productoDN.listarMonedasAsociadas(productoDTO.getIdToProducto()));

//		String json = "{rows:[";
//		if(productoForm.getMonedasAsociadas() != null && productoForm.getMonedasAsociadas().size() > 0) {
//			for(MonedaProductoDTO moneda : productoForm.getMonedasAsociadas()) {
//				json += "{id:\"" + moneda.getId().getIdMoneda() + "\",data:[";
//				json += moneda.getId().getIdToProducto() + ",\"";
//				json += productoDN.getMonedaDTO(moneda).getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getMonedasAsociadas() != null && productoForm.getMonedasAsociadas().size() > 0) {
			for(MonedaProductoDTO moneda : productoForm.getMonedasAsociadas()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(moneda.getId().getIdMoneda().toString());
				row.setDatos(
						moneda.getId().getIdToProducto().toString(),
						productoDN.getMonedaDTO(moneda).getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarMonedasPorAsociar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		ProductoForm productoForm = (ProductoForm) form;
		
		String id = request.getParameter("id");
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(id));

		ProductoDN productoDN =  ProductoDN.getInstancia();
		productoForm.setMonedasPorAsociar(productoDN.listarMonedasPorAsociar(productoDTO));

//		String json = "{rows:[";
//		if(productoForm.getMonedasPorAsociar() != null && productoForm.getMonedasPorAsociar().size() > 0) {
//			for(MonedaDTO moneda : productoForm.getMonedasPorAsociar()) {
//				json += "{id:\"" + moneda.getIdTcMoneda() + "\",data:[";
//				json += id + ",\"";
//				json += moneda.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(productoForm.getMonedasPorAsociar() != null && productoForm.getMonedasPorAsociar().size() > 0) {
			for(MonedaDTO moneda : productoForm.getMonedasPorAsociar()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(moneda.getIdTcMoneda().toString());
				row.setDatos(
						id,
						moneda.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
