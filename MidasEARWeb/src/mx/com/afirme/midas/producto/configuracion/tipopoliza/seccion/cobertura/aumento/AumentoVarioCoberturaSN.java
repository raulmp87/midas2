package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AumentoVarioCoberturaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private AumentoVarioCoberturaFacadeRemote beanRemoto;

	public AumentoVarioCoberturaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(AumentoVarioCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(AumentoVarioCoberturaDTO aumentoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(aumentoPorCoberturaDTO);
		return null;
	}

	public String actualizar(AumentoVarioCoberturaDTO aumentoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(aumentoPorCoberturaDTO);
		return null;
	}

	public String borrar(AumentoVarioCoberturaDTO aumentoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(aumentoPorCoberturaDTO);
		return null;
	}

	public List<AumentoVarioCoberturaDTO> listarAumentosAsociado(
			BigDecimal idToCobertura) {
		return beanRemoto.findByProperty("id.idtocobertura", idToCobertura);
	}

	public List<AumentoVarioCoberturaDTO> listarAumentos(
			BigDecimal idToProducto) {
		return beanRemoto.findAll();
	}
}
