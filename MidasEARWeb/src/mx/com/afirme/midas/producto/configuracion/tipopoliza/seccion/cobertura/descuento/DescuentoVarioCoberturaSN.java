package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DescuentoVarioCoberturaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private DescuentoVarioCoberturaFacadeRemote beanRemoto;

	public DescuentoVarioCoberturaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(DescuentoVarioCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(DescuentoVarioCoberturaDTO descuentoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(descuentoPorCoberturaDTO);
		return null;
	}

	public String actualizar(DescuentoVarioCoberturaDTO descuentoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(descuentoPorCoberturaDTO);
		return null;
	}

	public String borrar(DescuentoVarioCoberturaDTO descuentoPorCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(descuentoPorCoberturaDTO);
		return null;
	}

	public List<DescuentoVarioCoberturaDTO> listarDescuentoAsociado(
			BigDecimal idToCobertura) {
		return beanRemoto.findByProperty("id.idtocobertura", idToCobertura);
	}

	public List<DescuentoVarioCoberturaDTO> listarDescuentos(
			BigDecimal idToProducto) {
		return beanRemoto.findAll();
	}
}
