package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Jos� Luis Arellano
 * @since 14 de Agosto de 2009
 */
public class ExclusionAumentoVarioTipoPolizaSN {
	private ExclusionAumentoVarioTipoPolizaFacadeRemote beanRemoto;

	public ExclusionAumentoVarioTipoPolizaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionAumentoVarioTipoPolizaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionAumentoVarioTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionAumentoVarioTipoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionAumentoVarioTipoPolizaDTO excAumentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excAumentoPorTipoPolizaDTO);
	}

	public void modificar(ExclusionAumentoVarioTipoPolizaDTO excAumentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excAumentoPorTipoPolizaDTO);
	}

	public ExclusionAumentoVarioTipoPolizaDTO getPorId(ExclusionAumentoVarioTipoPolizaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionAumentoVarioTipoPolizaDTO excAumentoPorTipoPolizaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excAumentoPorTipoPolizaDTO);
	}

	public List<ExclusionAumentoVarioTipoPolizaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public ExclusionAumentoVarioTipoPolizaDTO buscarPorIDs(BigDecimal idToTipoPoliza, BigDecimal idToAumentoVario, BigDecimal idToCobertura){
		List<ExclusionAumentoVarioTipoPolizaDTO> lista = beanRemoto.findByIDs(idToTipoPoliza, idToAumentoVario, idToCobertura);
		ExclusionAumentoVarioTipoPolizaDTO registro;
		if (lista == null || lista.isEmpty())
			registro=null;
		else
			registro = lista.get(0); 
		return registro;
	}
	
	public List<ExclusionAumentoVarioTipoPolizaDTO> buscarVigentesPorIdTipoPoliza(BigDecimal idToProducto){
		return beanRemoto.getVigentesPorIdTipoPoliza(idToProducto);
	}
}
