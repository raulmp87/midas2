package mx.com.afirme.midas.producto.configuracion.descuento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DescuentoVarioProductoSN {
	/**
	 * @author Fernando Alonzo
	 * @since 4 de Agosto de 2009
	 */

	private DescuentoVarioProductoFacadeRemote beanRemoto;

	public DescuentoVarioProductoSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(DescuentoVarioProductoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(DescuentoVarioProductoDTO descuentoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(descuentoPorProductoDTO);
		return null;
	}

	public String actualizar(DescuentoVarioProductoDTO descuentoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(descuentoPorProductoDTO);
		return null;
	}

	public String borrar(DescuentoVarioProductoDTO descuentoPorProductoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(descuentoPorProductoDTO);
		return null;
	}

	public List<DescuentoVarioProductoDTO> listarDescuentoAsociado(
			BigDecimal idToProducto) {
		return beanRemoto.findByProperty("id.idToProducto", idToProducto);
	}

	public List<DescuentoVarioProductoDTO> listarDescuentos(
			BigDecimal idToProducto) {
		return beanRemoto.findAll();
	}
}
