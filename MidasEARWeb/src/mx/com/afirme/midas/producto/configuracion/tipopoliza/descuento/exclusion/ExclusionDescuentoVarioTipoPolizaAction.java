package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 17 de Agosto de 2009
 */
public class ExclusionDescuentoVarioTipoPolizaAction extends TipoPolizaAction{
	/**
	 * Method mostrarExclusionDescuento
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionDescuento(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id");
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(BigDecimal.valueOf(Double.valueOf(id)));
		try {
			tipoPolizaDTO = new TipoPolizaDN().getPorId(tipoPolizaDTO);
			tipoPolizaForm.setIdToTipoPoliza(id);
			//Listas para el filtrado de exlusion de descuentos
			tipoPolizaForm.setCoberturas(CoberturaDN.getInstancia().listarVigentesPorTipoPoliza(tipoPolizaDTO.getIdToTipoPoliza()));
			tipoPolizaForm.setDescuentoTipoPolizaAsociados(new DescuentoVarioTipoPolizaSN().listarDescuentoAsociado(tipoPolizaDTO.getIdToTipoPoliza()));
			tipoPolizaForm.setClaveTMPCombo("");
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcDescuentoTipoPolizaAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcDescuentoTipoPolizaAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id").toString();
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
		tipoPolizaDTO = tipoPolizaDN.getPorId(tipoPolizaDTO);
		tipoPolizaForm.setExcDescuentoTipoPolizaAsociados(tipoPolizaDN.listarExcDescuentoTipoPolizaAsociadas(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getExcDescuentoTipoPolizaAsociados()!= null && tipoPolizaForm.getExcDescuentoTipoPolizaAsociados().size() > 0) {
//			for(ExclusionDescuentoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcDescuentoTipoPolizaAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtocobertura() +"|"+id+ "\",data:[";
//				json += actual.getDescuentoDTO().getClaveTipo() + ",\"";
//				json += actual.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += actual.getCoberturaDTO().getCodigo() + "\",\"";
//				json += actual.getCoberturaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getExcDescuentoTipoPolizaAsociados()!= null && tipoPolizaForm.getExcDescuentoTipoPolizaAsociados().size() > 0) {
			for(ExclusionDescuentoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcDescuentoTipoPolizaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtocobertura() +"|"+id);
				row.setDatos(
						actual.getDescuentoDTO().getClaveTipo().toString(),
						actual.getDescuentoDTO().getDescripcion(),
						actual.getCoberturaDTO().getCodigo(),
						actual.getCoberturaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Exclusion Descuento TipoPoliza asociados: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcDescuentoTipoPolizaAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcDescuentoTipoPolizaAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaDN tipoPolizaDN = TipoPolizaDN.getInstancia(); 
		DescuentoDN DescuentoDN = new DescuentoDN();
		ExclusionDescuentoVarioTipoPolizaId id = new ExclusionDescuentoVarioTipoPolizaId();
		CoberturaDN coberturaDN = new CoberturaDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		
		try {
			id.setIdtodescuentovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtotipopoliza(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionDescuentoVarioTipoPolizaDTO excDescuentoTipoPoliza = new ExclusionDescuentoVarioTipoPolizaDTO();
			excDescuentoTipoPoliza.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				DescuentoDTO Descuento = new DescuentoDTO();
				Descuento.setIdToDescuentoVario(UtileriasWeb.regresaBigDecimal(ids[0]));
				Descuento = DescuentoDN.getPorId(Descuento);

				CoberturaDTO cobertura = new CoberturaDTO();
				cobertura.setIdToCobertura(UtileriasWeb.regresaBigDecimal(ids[1]));
				cobertura = coberturaDN.getPorId(cobertura);
				
				TipoPolizaDTO tipoPoliza = new  TipoPolizaDTO();
				tipoPoliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(ids[2]));
				tipoPoliza = tipoPolizaDN.getPorId(tipoPoliza);
				
				excDescuentoTipoPoliza.setDescuentoDTO(Descuento);
				excDescuentoTipoPoliza.setCoberturaDTO(cobertura);
				excDescuentoTipoPoliza.setTipoPolizaDTO(tipoPoliza);

				tipoPolizaDN.ExcluirDescuentoTipoPoliza(excDescuentoTipoPoliza);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				tipoPolizaDN.EliminarExclusionDescuentoTipoPoliza(excDescuentoTipoPoliza);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcDescuentoTipoPolizaPorAsociar
	 * 
	 * M�todo que recupera la lista de ramos no asociados a un tipoPoliza
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcDescuentoTipoPolizaPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id").toString();
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));

		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();
		//Posibles par�metros recibidos para hacer un filtrado por tipoPoliza o Descuento
		String idToTipoPoliza = request.getParameter("idToTipoPoliza");
		String idToDescuento = request.getParameter("idToDescuento");
		if (idToTipoPoliza != null)
			tipoPolizaForm.setExcDescuentoTipoPolizaNoAsociados(tipoPolizaDN.listarExcDescuentoTipoPolizaNoAsociadas(tipoPolizaDTO,UtileriasWeb.regresaBigDecimal(idToTipoPoliza)));
		else if (idToDescuento != null)
			tipoPolizaForm.setExcDescuentoTipoPolizaNoAsociados(tipoPolizaDN.listarExcDescuentoTipoPolizaNoAsociadas(UtileriasWeb.regresaBigDecimal(idToDescuento),tipoPolizaDTO));
		else
			tipoPolizaForm.setExcDescuentoTipoPolizaNoAsociados(tipoPolizaDN.listarExcDescuentoTipoPolizaNoAsociadas(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getExcDescuentoTipoPolizaNoAsociados() != null && tipoPolizaForm.getExcDescuentoTipoPolizaNoAsociados().size() > 0) {
//			for(ExclusionDescuentoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcDescuentoTipoPolizaNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtocobertura() +"|"+id+ "\",data:[";
//				json += actual.getDescuentoDTO().getClaveTipo() + ",\"";
//				json += actual.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += actual.getCoberturaDTO().getCodigo() + "\",\"";
//				json += actual.getCoberturaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getExcDescuentoTipoPolizaNoAsociados() != null && tipoPolizaForm.getExcDescuentoTipoPolizaNoAsociados().size() > 0) {
			for(ExclusionDescuentoVarioTipoPolizaDTO actual : tipoPolizaForm.getExcDescuentoTipoPolizaNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtocobertura() +"|"+id);
				row.setDatos(
						actual.getDescuentoDTO().getClaveTipo().toString(),
						actual.getDescuentoDTO().getDescripcion(),
						actual.getCoberturaDTO().getCodigo(),
						actual.getCoberturaDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Exclusion Descuento TipoPoliza no asociados: "+json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}