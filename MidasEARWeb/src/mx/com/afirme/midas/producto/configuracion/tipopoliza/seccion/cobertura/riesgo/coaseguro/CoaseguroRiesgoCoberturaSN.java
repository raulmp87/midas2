package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 21 de Agosto de 2009
 */
public class CoaseguroRiesgoCoberturaSN {
	private CoaseguroRiesgoCoberturaFacadeRemote beanRemoto;

	public CoaseguroRiesgoCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en CoaseguroRiesgoCoberturaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CoaseguroRiesgoCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CoaseguroRiesgoCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(coaseguroRiesgoCoberturaDTO);
	}

	public void modificar(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(coaseguroRiesgoCoberturaDTO);
	}

	public CoaseguroRiesgoCoberturaDTO getPorId(CoaseguroRiesgoCoberturaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(coaseguroRiesgoCoberturaDTO);
	}

	public List<CoaseguroRiesgoCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public List<CoaseguroRiesgoCoberturaDTO> buscarPorIdRiesgoIdCoberturaIdSeccion(BigDecimal idToRiesgo,BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.findByRiesgoCoberturaSeccion(idToRiesgo, idToCobertura, idToSeccion);
	}

	public void sincronizarCoasegurosRiesgoCobertura(BigDecimal idToRiesgo, BigDecimal idToCobertura, BigDecimal idToSeccion) {
		beanRemoto.synchronizeCoasegurosRiesgoCobertura(idToRiesgo, idToCobertura, idToSeccion);
	}

	public List<CoaseguroRiesgoCoberturaDTO> listarFiltrado(
			CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) {
		return beanRemoto.listarFiltrado(coaseguroRiesgoCoberturaDTO);
	}
}
