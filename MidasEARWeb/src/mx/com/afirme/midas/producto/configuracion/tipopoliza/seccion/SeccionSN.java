/**
 * 
 */
package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionSN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

/**
 * @author admin
 * 
 */
public class SeccionSN {
	private SeccionFacadeRemote beanRemoto;

	public SeccionSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en SeccionSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(SeccionFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<SeccionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<SeccionDTO> list = beanRemoto.findAll();
		return list;

	}

	public void agregar(SeccionDTO seccionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(seccionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void modificar(SeccionDTO seccionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(seccionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SeccionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);

	}

	public void borrar(SeccionDTO seccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(seccionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<SeccionDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public void borradoLogico(SeccionDTO SeccionDTO){
		beanRemoto.borradoLogico(SeccionDTO);
	}
	
	public List<SeccionDTO> listarVigentes() throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarVigentes();
	}
	
	public List<SeccionDTO> listarVigentes(BigDecimal idToTipoPoliza) throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarVigentes(idToTipoPoliza);
	}
	
	public List<SeccionDTO> listarPorIdTipoPoliza(BigDecimal idToTipoPoliza, Boolean verInactivos, Boolean soloActivos) throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarPorIdTipoPoliza(idToTipoPoliza, verInactivos, soloActivos);
	}
	
	public List<SeccionDTO> listarSeccionesRequeridas(BigDecimal idToSeccion){
		return beanRemoto.listarSeccionesRequeridas(idToSeccion);
	}
	
	public List<SeccionDTO> listarSeccionesNoRequeridas(BigDecimal idToSeccion,BigDecimal idToTipoPoliza){
		return beanRemoto.listarSeccionesNoRequeridas(idToSeccion, idToTipoPoliza);
	}

	public SeccionDTO getPorIdCascada(BigDecimal idToSeccion) throws SystemException {
		SeccionDTO seccionDTO = this.getPorId(idToSeccion);
		
		CoberturaSeccionSN coberturaSeccionSN = new CoberturaSeccionSN();
		List<CoberturaSeccionDTO> coberturas = coberturaSeccionSN.buscarPorPropiedad("id.idtoseccion", seccionDTO.getIdToSeccion());
		seccionDTO.setCoberturas(coberturas);
		return seccionDTO;
	}
	
	public List<ValorCatalogoAgentes> listarCedulasAsociadasPorSeccion(BigDecimal idToSeccion) {
		return this.beanRemoto.listarCedulasAsociadasPorSeccion(idToSeccion);
	}
	
	public List<ValorCatalogoAgentes> listarCedulasNoAsociadasPorSeccion(BigDecimal idToSeccion) {
		return this.beanRemoto.listarCedulasNoAsociadasPorSeccion(idToSeccion);
	}
	
	public BigDecimal countCedulasAsociadasPorSeccion(Long idTipoCedula, BigDecimal idSeccion) {
		return this.beanRemoto.countCedulasAsociadasPorSeccion(idTipoCedula, idSeccion);
	}
	
	public BigDecimal countCedulasAsociadasPorSeccionesEnTipoPoliza(Long idTipoCedula, BigDecimal idTipoPoliza) {
		return this.beanRemoto.countCedulasAsociadasPorSeccionesEnTipoPoliza(idTipoCedula, idTipoPoliza);
	}
	
	public Integer guardarCedulasAsociadasPorSeccion(BigDecimal idToSeccion, String idsCedulas) {
		return this.beanRemoto.guardarCedulasAsociadasPorSeccion(idToSeccion, idsCedulas);
	}
	
	public Map<String, String> agregarVersion(BigDecimal idToSeccion)throws Exception{
		return beanRemoto.generarNuevaVersion(idToSeccion);
    }		
}
