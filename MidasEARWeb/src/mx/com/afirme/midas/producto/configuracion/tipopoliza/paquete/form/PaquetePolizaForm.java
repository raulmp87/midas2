package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class PaquetePolizaForm extends MidasBaseForm {
	public static final String CLAVE_ACCION_AGREGAR_PAQUETE = "agregarPaquete";
	public static final String CLAVE_ACCION_EDITAR_PAQUETE = "editarPaquete";

	private static final long serialVersionUID = 9070499859178545942L;
	private String idToPaquetePoliza;
	private String idToTipoPoliza;
    private String nombre;
    private String descripcion;
    private String orden;
    private String soloLectura;
    
    public String getSoloLectura() {
		return soloLectura;
	}
	public void setSoloLectura(String soloLectura) {
		this.soloLectura = soloLectura;
	}
	private List<CoberturaSeccionPaqueteDTO> coberturaSeccionPaqueteDTOs = new ArrayList<CoberturaSeccionPaqueteDTO>();
    
	public String getIdToPaquetePoliza() {
		return idToPaquetePoliza;
	}
	public void setIdToPaquetePoliza(String idToPaquetePoliza) {
		this.idToPaquetePoliza = idToPaquetePoliza;
	}
	public String getIdToTipoPoliza() {
		return idToTipoPoliza;
	}
	public void setIdToTipoPoliza(String idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public void setCoberturaSeccionPaqueteDTOs(
			List<CoberturaSeccionPaqueteDTO> coberturaSeccionPaqueteDTOs) {
		this.coberturaSeccionPaqueteDTOs = coberturaSeccionPaqueteDTOs;
	}
	public List<CoberturaSeccionPaqueteDTO> getCoberturaSeccionPaqueteDTOs() {
		return coberturaSeccionPaqueteDTOs;
	}
}
