package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentos.referencias;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.documentos.anexos.DocumentoAnexoSeccion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.documentos.anexos.DocumentoAnexoSeccionService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

@SuppressWarnings("deprecation")
public class DocumentosReferenciasAction extends MidasMappingDispatchAction{
	
	private FileManagerService fileManagerService;

	private FortimaxV2Service fortimaxV2Service;
	private DocumentoAnexoSeccionService documentoAnexoSeccionService;
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	public final int ID_GPO_PARAMETRO_GRAL_GAVETA = 20;
	public final int ID_GPO_PARAMETRO_GRAL_CARPETA = 20;
	public final int CODIGO_PARAMETRO_GRAL_GAVETA = 200120;
	public final int CODIGO_PARAMETRO_GRAL_CARPETA = 200130;
	public final Short CLAVE_OBLIGATORIEDAD = 0;
	public final Long NUMERO_SECUENCIA = 0L;
	
	
    public FortimaxV2Service getFortimaxV2Service() {
        return this.fortimaxV2Service;
    }
    public void setFortimaxV2Service(FortimaxV2Service fortimaxV2Service) {
        this.fortimaxV2Service = fortimaxV2Service;
    }	
    
	public DocumentoAnexoSeccionService getDocumentoAnexoSeccionService() {
		return documentoAnexoSeccionService;
	}
	public void setDocumentoAnexoSeccionService(
			DocumentoAnexoSeccionService documentoAnexoSeccionService) {
		this.documentoAnexoSeccionService = documentoAnexoSeccionService;
	}
	public DocumentosReferenciasAction() throws SystemException {
		try {
			
			fileManagerService = ServiceLocator.getInstance().getEJB(FileManagerService.class);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			fortimaxV2Service = serviceLocator.getEJB(FortimaxV2Service.class);
			parametroGeneralFacade = serviceLocator.getEJB(ParametroGeneralFacadeRemote.class);
			documentoAnexoSeccionService = serviceLocator.getEJB(DocumentoAnexoSeccionService.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}	
	
	public String getGaveta() {
		ParametroGeneralId id = new ParametroGeneralId(new BigDecimal(this.ID_GPO_PARAMETRO_GRAL_GAVETA), new BigDecimal(this.CODIGO_PARAMETRO_GRAL_GAVETA));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}

	public String getCarpeta() {
		ParametroGeneralId id = new ParametroGeneralId(new BigDecimal(this.ID_GPO_PARAMETRO_GRAL_CARPETA),new BigDecimal(this.CODIGO_PARAMETRO_GRAL_CARPETA));
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}
	
	public void crearExpedienteFortimax(String fortimaxIDFile, String nombre) {
		String[] fieldValues = new String[2];
		fieldValues[0]=fortimaxIDFile;
		fieldValues[1]=nombre;
		
		String[] strReturns;
		try {
			strReturns = fortimaxV2Service.generateExpedient(this.getGaveta(), fieldValues);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		if (strReturns[0].equals("false")) {
			throw new RuntimeException("No se pudo crear el expediente en Fortimax para el Documento Anexo.");
		}
	}
	
	public void cargarArchivoFortimax(String fileName, String fortimaxIDFile, TransporteImpresionDTO transporteImpresionDTO){
		try {
			fortimaxV2Service.uploadFile(fileName, transporteImpresionDTO, this.getGaveta(), new String[]{fortimaxIDFile}, this.getCarpeta());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	 public void descargarArchivoFortimax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
			
			OutputStream output = null;

		 	String id = request.getParameter("id");
			DocumentoAnexoSeccion documentoAnexoSeccion = new DocumentoAnexoSeccion();
			documentoAnexoSeccion = documentoAnexoSeccionService.findDocumentoAnexoSeccionById( Long.parseLong(id) );

			final String idControlArchivo = documentoAnexoSeccion.getIdControlArchivo().toString();
			final ControlArchivoDTO controlArchivoDTO = ControlArchivoDN
					.getInstancia().getPorId(new BigDecimal(idControlArchivo));
			final String fileName = fileManagerService.getFileName(controlArchivoDTO);
			final String nodo = controlArchivoDTO.getCodigoExterno();
			byte[] byteArray = fileManagerService.downloadFile(fileName, nodo);
			response.setHeader("Content-Disposition", "attachment; filename="
					+ URLEncoder.encode(controlArchivoDTO.getNombreArchivoOriginal(), "ISO-8859-1"));
			response.setContentType("application/unknown");
			response.setContentLength(byteArray.length);
			response.setBufferSize(1024 * 15);
			
			output = response.getOutputStream(); 
			output.write(byteArray);
			output.flush();
			output.close();
		}

		public DocumentoAnexoSeccion buscarDocumento(String fortimaxDocName) throws SQLException, Exception{
			DocumentoAnexoSeccion documentoAnexoSeccion = new DocumentoAnexoSeccion();
			List<DocumentoAnexoSeccion> listado = new ArrayList<DocumentoAnexoSeccion>();
			listado = documentoAnexoSeccionService.findDocumentoAnexoSeccionByFmxDocName(fortimaxDocName);
			documentoAnexoSeccion = (DocumentoAnexoSeccion)listado.get(0);
			return documentoAnexoSeccion;
		}
		
		public void guardarDocumentoAnexoSeccion(DocumentoAnexoSeccion documentoAnexoSeccion){
			documentoAnexoSeccionService.saveDocumentoAnexoSeccion(documentoAnexoSeccion);
		}	
}
