package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class RiesgoCoberturaForm extends MidasBaseForm{
	private static final long serialVersionUID = -8993031079808594470L;
	private String idtoseccion;
	private String idtocobertura;
	private String idtoriesgo;
	private RiesgoDTO riesgoDTO;
	private CoberturaDTO coberturaDTO;
	private SeccionDTO seccionDTO;
	private String descripcionRiesgoCobertura;
	private String claveObligatoriedad;
	private String claveTipoCoaseguro;
	private String valorMinimoCoaseguro;
	private String valorMaximoCoaseguro;
	private String claveTipoLimiteCoaseguro;
	private String valorMinimoLimiteCoaseguro;
	private String valorMaximoLimiteCoaseguro;
	private String claveTipoDeducible;
	private String valorMinimoDeducible;
	private String valorMaximoDeducible;
	private String claveTipoLimiteDeducible;
	private String valorMinimoLimiteDeducible;
	private String valorMaximoLimiteDeducible;
	private String claveTipoSumaAsegurada;
	private String claveAutorizacion;
	private String claveImporteCero;
	public String getIdtoseccion() {
		return idtoseccion;
	}
	public void setIdtoseccion(String idtoseccion) {
		this.idtoseccion = idtoseccion;
	}
	public String getIdtocobertura() {
		return idtocobertura;
	}
	public void setIdtocobertura(String idtocobertura) {
		this.idtocobertura = idtocobertura;
	}
	public String getIdtoriesgo() {
		return idtoriesgo;
	}
	public void setIdtoriesgo(String idtoriesgo) {
		this.idtoriesgo = idtoriesgo;
	}
	public RiesgoDTO getRiesgoDTO() {
		return riesgoDTO;
	}
	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}
	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}
	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}
	public String getDescripcionRiesgoCobertura() {
		return descripcionRiesgoCobertura;
	}
	public void setDescripcionRiesgoCobertura(String descripcionRiesgoCobertura) {
		this.descripcionRiesgoCobertura = descripcionRiesgoCobertura;
	}
	public String getClaveObligatoriedad() {
		return claveObligatoriedad;
	}
	public void setClaveObligatoriedad(String claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}
	public String getClaveTipoCoaseguro() {
		return claveTipoCoaseguro;
	}
	public void setClaveTipoCoaseguro(String claveTipoCoaseguro) {
		this.claveTipoCoaseguro = claveTipoCoaseguro;
	}
	public String getValorMinimoCoaseguro() {
		return valorMinimoCoaseguro;
	}
	public void setValorMinimoCoaseguro(String valorMinimoCoaseguro) {
		this.valorMinimoCoaseguro = valorMinimoCoaseguro;
	}
	public String getValorMaximoCoaseguro() {
		return valorMaximoCoaseguro;
	}
	public void setValorMaximoCoaseguro(String valorMaximoCoaseguro) {
		this.valorMaximoCoaseguro = valorMaximoCoaseguro;
	}
	public String getClaveTipoLimiteCoaseguro() {
		return claveTipoLimiteCoaseguro;
	}
	public void setClaveTipoLimiteCoaseguro(String claveTipoLimiteCoaseguro) {
		this.claveTipoLimiteCoaseguro = claveTipoLimiteCoaseguro;
	}
	public String getValorMinimoLimiteCoaseguro() {
		return valorMinimoLimiteCoaseguro;
	}
	public void setValorMinimoLimiteCoaseguro(String valorMinimoLimiteCoaseguro) {
		this.valorMinimoLimiteCoaseguro = valorMinimoLimiteCoaseguro;
	}
	public String getValorMaximoLimiteCoaseguro() {
		return valorMaximoLimiteCoaseguro;
	}
	public void setValorMaximoLimiteCoaseguro(String valorMaximoLimiteCoaseguro) {
		this.valorMaximoLimiteCoaseguro = valorMaximoLimiteCoaseguro;
	}
	public String getClaveTipoDeducible() {
		return claveTipoDeducible;
	}
	public void setClaveTipoDeducible(String claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}
	public String getValorMinimoDeducible() {
		return valorMinimoDeducible;
	}
	public void setValorMinimoDeducible(String valorMinimoDeducible) {
		this.valorMinimoDeducible = valorMinimoDeducible;
	}
	public String getValorMaximoDeducible() {
		return valorMaximoDeducible;
	}
	public void setValorMaximoDeducible(String valorMaximoDeducible) {
		this.valorMaximoDeducible = valorMaximoDeducible;
	}
	public String getClaveTipoLimiteDeducible() {
		return claveTipoLimiteDeducible;
	}
	public void setClaveTipoLimiteDeducible(String claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}
	public String getValorMinimoLimiteDeducible() {
		return valorMinimoLimiteDeducible;
	}
	public void setValorMinimoLimiteDeducible(String valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}
	public String getValorMaximoLimiteDeducible() {
		return valorMaximoLimiteDeducible;
	}
	public void setValorMaximoLimiteDeducible(String valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}
	public String getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}
	public void setClaveTipoSumaAsegurada(String claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}
	public String getClaveAutorizacion() {
		return claveAutorizacion;
	}
	public void setClaveAutorizacion(String claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}
	public String getClaveImporteCero() {
		return claveImporteCero;
	}
	public void setClaveImporteCero(String claveImporteCero) {
		this.claveImporteCero = claveImporteCero;
	}
}
