package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Jos� Luis Arellano
 * @since 18 de Agosto de 2009
 */
public class ExclusionDescuentoVarioCoberturaAction extends CoberturaAction{
	/**
	 * Method mostrarExclusionDescuento
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarExclusionDescuento(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		coberturaForm.setIdToCobertura(id);
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		coberturaForm.setDescripcionSubRamo("");
		try {
			//Estas listas se utilizan para el filtrado de registros de exclusion por Riesgo y por Aumento|Descuento|Recargo
			coberturaForm.setRiesgosAsociados(RiesgoDN.getInstancia().listarRiesgosVigentesPorCoberturaSeccion(UtileriasWeb.regresaBigDecimal(coberturaForm.getIdToCobertura()), UtileriasWeb.regresaBigDecimal(idSeccion)));
			coberturaForm.setDescuentosAsociados(new DescuentoVarioCoberturaSN().listarDescuentoAsociado(UtileriasWeb.regresaBigDecimal(id)));
		} catch (ExcepcionDeAccesoADatos e) {e.printStackTrace();
		} catch (SystemException e) {e.printStackTrace();}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcDescuentoCoberturaAsociadas
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarExcDescuentoCoberturaAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id").toString();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		
		String idToSeccion = request.getParameter("idToSeccion");
		
		coberturaForm.setExcDescuentoCoberturaAsociados(coberturaDN.listarExcDescuentoCoberturaAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
//		String json = "{rows:[";
//		if(coberturaForm.getExcDescuentoCoberturaAsociados()!= null && coberturaForm.getExcDescuentoCoberturaAsociados().size() > 0) {
//			for(ExclusionDescuentoVarioCoberturaDTO actual : coberturaForm.getExcDescuentoCoberturaAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id+ "\",data:[";
//				json += actual.getDescuentoDTO().getClaveTipo() + ",\"";
//				json += actual.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += actual.getRiesgoDTO().getCodigo() + "\",\"";
//				json += actual.getRiesgoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getExcDescuentoCoberturaAsociados()!= null && coberturaForm.getExcDescuentoCoberturaAsociados().size() > 0) {
			for(ExclusionDescuentoVarioCoberturaDTO actual : coberturaForm.getExcDescuentoCoberturaAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id);
				row.setDatos(
						actual.getDescuentoDTO().getClaveTipo().toString(),
						actual.getDescuentoDTO().getDescripcion(),
						actual.getRiesgoDTO().getCodigo(),
						actual.getRiesgoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println("Exclusion Descuento Cobertura asociados: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarExcDescuentoCoberturaAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarExcDescuentoCoberturaAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaDN coberturaDN = CoberturaDN.getInstancia(); 
		DescuentoDN DescuentoDN = new DescuentoDN();
		ExclusionDescuentoVarioCoberturaId id = new ExclusionDescuentoVarioCoberturaId();
		RiesgoDN riesgoDN = new RiesgoDN();
		
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		try {
			id.setIdtodescuentovario(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdtoriesgo(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setIdtocobertura(UtileriasWeb.regresaBigDecimal(ids[2]));
			
			ExclusionDescuentoVarioCoberturaDTO excDescuentoCobertura = new ExclusionDescuentoVarioCoberturaDTO();
			excDescuentoCobertura.setId(id);
			if(request.getParameter("!nativeeditor_status") == null || request.getParameter("!nativeeditor_status").equals("inserted")) {
				DescuentoDTO Descuento = new DescuentoDTO();
				Descuento.setIdToDescuentoVario(UtileriasWeb.regresaBigDecimal(ids[0]));
				Descuento = DescuentoDN.getPorId(Descuento);

				CoberturaDTO cobertura = new CoberturaDTO();
				cobertura.setIdToCobertura(UtileriasWeb.regresaBigDecimal(ids[2]));
				cobertura = coberturaDN.getPorId(cobertura);
				
				RiesgoDTO riesgo = new  RiesgoDTO();
				riesgo.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(ids[1]));
				riesgo = riesgoDN.getPorId(riesgo);
				
				excDescuentoCobertura.setDescuentoDTO(Descuento);
				excDescuentoCobertura.setCoberturaDTO(cobertura);
				excDescuentoCobertura.setRiesgoDTO(riesgo);
				coberturaDN.ExcluirDescuentoCobertura(excDescuentoCobertura);
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				coberturaDN.EliminarExclusionDescuentoCobertura(excDescuentoCobertura);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarExcDescuentoCoberturaPorAsociar
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public void mostrarExcDescuentoCoberturaPorAsociar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException,SystemException, ExcepcionDeAccesoADatos{
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id").toString();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		
		String idToSeccion = request.getParameter("idToSeccion");
		//Posibles par�metros recibidos para hacer un filtrado por riesgo o Descuento
		String idToRiesgo = request.getParameter("idToRiesgo");
		String idToDescuento = request.getParameter("idToDescuento");
		if (idToRiesgo != null)
			coberturaForm.setExcDescuentoCoberturaNoAsociados(coberturaDN.listarExcDescuentoCoberturaNoAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion),UtileriasWeb.regresaBigDecimal(idToRiesgo)));
		else if (idToDescuento != null)
			coberturaForm.setExcDescuentoCoberturaNoAsociados(coberturaDN.listarExcDescuentoCoberturaNoAsociadas(UtileriasWeb.regresaBigDecimal(idToDescuento),coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
		else
			coberturaForm.setExcDescuentoCoberturaNoAsociados(coberturaDN.listarExcDescuentoCoberturaNoAsociadas(coberturaDTO,UtileriasWeb.regresaBigDecimal(idToSeccion)));
//		String json = "{rows:[";
//		if(coberturaForm.getExcDescuentoCoberturaNoAsociados() != null && coberturaForm.getExcDescuentoCoberturaNoAsociados().size() > 0) {
//			for(ExclusionDescuentoVarioCoberturaDTO actual : coberturaForm.getExcDescuentoCoberturaNoAsociados()) {
//				json += "{id:\"" + actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id+ "\",data:[";
//				json += actual.getDescuentoDTO().getClaveTipo() + ",\"";
//				json += actual.getDescuentoDTO().getDescripcion() + "\",\"";
//				json += actual.getRiesgoDTO().getCodigo() + "\",\"";
//				json += actual.getRiesgoDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getExcDescuentoCoberturaNoAsociados() != null && coberturaForm.getExcDescuentoCoberturaNoAsociados().size() > 0) {
			for(ExclusionDescuentoVarioCoberturaDTO actual : coberturaForm.getExcDescuentoCoberturaNoAsociados()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(actual.getId().getIdtodescuentovario()+"|"+actual.getId().getIdtoriesgo() +"|"+id);
				row.setDatos(
						actual.getDescuentoDTO().getClaveTipo().toString(),
						actual.getDescuentoDTO().getDescripcion(),
						actual.getRiesgoDTO().getCodigo(),
						actual.getRiesgoDTO().getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println("Exclusion Aumento Cobertura no asociados: "+json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
