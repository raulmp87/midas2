package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/*
 * @author Jos� Luis Arellano
 * @since 18 de Agosto de 2009
 */
public class ExclusionDescuentoVarioCoberturaSN {
	private ExclusionDescuentoVarioCoberturaFacadeRemote beanRemoto;

	public ExclusionDescuentoVarioCoberturaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ExclusionDescuentoVarioCoberturaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ExclusionDescuentoVarioCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ExclusionDescuentoVarioCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(ExclusionDescuentoVarioCoberturaDTO excDescuentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(excDescuentoPorCoberturaDTO);
	}

	public void modificar(ExclusionDescuentoVarioCoberturaDTO excDescuentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(excDescuentoPorCoberturaDTO);
	}

	public ExclusionDescuentoVarioCoberturaDTO getPorId(ExclusionDescuentoVarioCoberturaId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ExclusionDescuentoVarioCoberturaDTO excDescuentoPorCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(excDescuentoPorCoberturaDTO);
	}

	public List<ExclusionDescuentoVarioCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor){
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	
	public ExclusionDescuentoVarioCoberturaDTO buscarPorIDs(BigDecimal idToCobertura, BigDecimal idToDescuentoVario, BigDecimal idToRiesgo){
		List<ExclusionDescuentoVarioCoberturaDTO> lista = beanRemoto.findByIDs(idToCobertura, idToDescuentoVario, idToRiesgo);
		ExclusionDescuentoVarioCoberturaDTO registro;
		if (lista == null || lista.isEmpty())
			registro=null;
		else
			registro = lista.get(0); 
		return registro;
	}
	
	public List<ExclusionDescuentoVarioCoberturaDTO> buscarVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion){
		return beanRemoto.getVigentesPorIdCoberturaIdSeccion(idToCobertura, idToSeccion);
	}
}
