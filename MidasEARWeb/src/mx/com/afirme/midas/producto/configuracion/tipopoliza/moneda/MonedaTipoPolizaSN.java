package mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MonedaTipoPolizaSN {
	private MonedaTipoPolizaFacadeRemote beanRemoto;

	public MonedaTipoPolizaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en MonedaTipoPolizaSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(MonedaTipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<MonedaTipoPolizaDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(MonedaTipoPolizaDTO monedaTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(monedaTipoPolizaDTO);
	}

	public void modificar(MonedaTipoPolizaDTO monedaTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(monedaTipoPolizaDTO);
	}

	public MonedaTipoPolizaDTO getPorId(MonedaTipoPolizaId id)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(MonedaTipoPolizaDTO monedaTipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(monedaTipoPolizaDTO);
	}

	public List<MonedaTipoPolizaDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
}
