package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

import static mx.com.afirme.midas.sistema.UtileriasWeb.mandaMensajeExcepcionRegistrado;
import static mx.com.afirme.midas.sistema.UtileriasWeb.calculaValorStringDelCheck;
import static mx.com.afirme.midas.sistema.UtileriasWeb.calculaValorEnteroDelCheck;
import static mx.com.afirme.midas.sistema.UtileriasWeb.esCadenaVacia;
import static mx.com.afirme.midas.sistema.UtileriasWeb.regresaBigDecimal;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TreeLoader;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.negocio.Negocio;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class CoberturaAction extends MidasMappingDispatchAction {
	
	private static Logger LOG = Logger.getLogger(CoberturaAction.class);
	private final static String CLAVENEGOCIO = "claveNegocio";
	private final static String COBERTURAS = "coberturas";
	private final static SimpleDateFormat formatoFecha= new SimpleDateFormat("dd/MM/yyyy");
	
	
	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {	

		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		session.removeAttribute(CLAVENEGOCIO);
		session.setAttribute(CLAVENEGOCIO, request.getParameter(CLAVENEGOCIO));
		try {
			CoberturaForm coberturaForm = (CoberturaForm) form;
			if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
				coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
			}
			limpiarForm(coberturaForm);
			if (request.getParameter(CLAVENEGOCIO)!=null){
				if (request.getParameter(CLAVENEGOCIO).equals(Negocio.CLAVE_NEGOCIO_AUTOS)){
					CoberturaDN coberturaDN = CoberturaDN.getInstancia();
					List<CoberturaDTO> coberturas = coberturaDN.listarVigentesAutos();
					request.setAttribute(COBERTURAS, coberturas);
				}else{
					this.listarTodos(request);
				}
			}else{
				this.listarTodos(request);
			}
		} catch (SystemException e) {
		reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		
		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		
		try {
			poblarDTO(coberturaForm, coberturaDTO);
			Boolean mostrarInactivos = Boolean.FALSE;
			if(!esCadenaVacia(coberturaForm.getMostrarInactivos())) {
				mostrarInactivos = Boolean.TRUE;
			}
			request.setAttribute(COBERTURAS, coberturaDN.getListarFiltrado(coberturaDTO, mostrarInactivos));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		mx.com.afirme.midas.interfaz.cobertura.CoberturaDN coberturaInterfazDN = 
			mx.com.afirme.midas.interfaz.cobertura.CoberturaDN.getInstancia(usuario.getNombreUsuario());

		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		try {
			agregarDatosUsuarioCreacion(request,coberturaDTO);
			coberturaDN.agregar(coberturaDTO);
			coberturaInterfazDN.agregar(coberturaDTO);
			this.listarTodos(request);
			limpiarForm(coberturaForm);
			request.getSession().removeAttribute(ControlArchivoDTO.IDTOCONTROLARCHIVO);
			
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			coberturaForm.setMensaje("Verifique la informacion registrada");
			coberturaForm.setTipoMensaje(Sistema.ERROR);
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			coberturaForm.setMensaje("El c\u00F3digo introducido ya ha sido usado en otro registro");
			coberturaForm.setTipoMensaje(Sistema.ERROR);
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
	}
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));

		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		try {
			coberturaDTO.setIdToCobertura(regresaBigDecimal(coberturaForm.getIdToCobertura()));
			coberturaDTO = coberturaDN.getPorId(coberturaDTO);
			this.poblarDTO(coberturaForm, coberturaDTO);
			agregarDatosUsuarioModificacion(request, coberturaDTO);
			coberturaDN.modificar(coberturaDTO);
			this.listarTodos(request);
			limpiarForm(coberturaForm);
			String Path=forward.getPath();
			forward.setPath(Path + "?mensaje=guardado&tipoMensaje="+Sistema.EXITO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			coberturaForm.setMensaje("Verifique la informacion registrada");
			coberturaForm.setTipoMensaje(Sistema.ERROR);
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			coberturaForm.setMensaje("El c\u00F3digo introducido ya ha sido usado en otro registro");
			coberturaForm.setTipoMensaje(Sistema.ERROR);
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		try {
			coberturaDN.borrar(coberturaDTO);
			this.listarTodos(request);
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return forward;

	}
	
	/**
	 * Method borradoLogico
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borradoLogico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		try {
			
			coberturaDTO.setIdToCobertura(regresaBigDecimal(coberturaForm.getIdToCobertura()));
			coberturaDTO = coberturaDN.getPorId(coberturaDTO);
			coberturaDN.borradoLogico(coberturaDTO);
			listarTodos(request);
			
			forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
			String path = forward.getPath();
			
			forward.setPath(path
					+ "?id="
					+ coberturaDTO.getIdToCobertura().toString()
					+ "&mensaje=Cobertura Registrada con exito&tipoMensaje="+Sistema.EXITO);
			
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		try {
			coberturaDTO = coberturaDN.getPorId(coberturaDTO);
			this.poblarForm(coberturaDTO, coberturaForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		CoberturaForm coberturaForm = (CoberturaForm) form;
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		coberturaForm.setCoberturaEsPropia("on");
		return forward;
	}

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		String id = request.getParameter("id");
		coberturaDTO.setIdToCobertura(BigDecimal.valueOf(Double.valueOf(id)));
		this.poblarDTO(coberturaForm, coberturaDTO);
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		String idSeccion = request.getParameter("idPadre");
		HttpSession session = request.getSession();
		if (StringUtil.isEmpty(coberturaForm.getClaveNegocio())) {
			coberturaForm.setClaveNegocio((String)session.getAttribute(CLAVENEGOCIO));
		}
		try {
			coberturaDTO = coberturaDN.getPorId(coberturaDTO);
			this.poblarForm(coberturaDTO, coberturaForm);
			if (!StringUtil.isEmpty(idSeccion)) {
				coberturaForm.setIdToSeccion(idSeccion);
				//S�lo se deben listar los riesgos vigentes
				//coberturaForm.setRiesgoCoberturaAsociados(coberturaDN.listarRiesgosAsociados(coberturaDTO, regresaBigDecimal(idSeccion)));
				coberturaForm.setRiesgoCoberturaAsociados(coberturaDN.listarRiesgosVigentesAsociados(coberturaDTO, regresaBigDecimal(idSeccion)));
			}
			if(coberturaDN.isCobeturaAsociada(coberturaDTO.getIdToCobertura())) {
				coberturaForm.setCoberturaAsociada(Boolean.TRUE);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		//listar solo los registros activos que no han sido borrados logicamente.
		List<CoberturaDTO> coberturas = coberturaDN.listarVigentes();
		request.setAttribute(COBERTURAS, coberturas);
	}

	/**
	 * Method poblarDTO
	 * 
	 * @param CoberturaForm
	 * @param CoberturaDTO
	 */
	protected void poblarDTO(CoberturaForm coberturaForm,CoberturaDTO coberturaDTO) {
		Double id;
		if (!StringUtil.isEmpty(coberturaForm.getIdToCobertura())) {
			id = Double.valueOf(coberturaForm.getIdToCobertura());
			coberturaDTO.setIdToCobertura(BigDecimal.valueOf(id));
		}
		if (!StringUtil.isEmpty(coberturaForm.getIdTcSubRamo())
				&& !StringUtil.isEmpty(coberturaForm.getIdTcRamo())) {
			SubRamoDTO subRamoTMP = new SubRamoDTO();
			try{
				subRamoTMP.setIdTcSubRamo(regresaBigDecimal(coberturaForm.getIdTcSubRamo()));
				//usar el id para encontrar el registro de l subramo correspondiente
				subRamoTMP = new SubRamoDN().getSubRamoPorId(subRamoTMP);}
			catch(ExcepcionDeAccesoADatos ex){
				LOG.error(ex.getMessage(), ex);
			} catch (SystemException e) {
				LOG.error(e.getMessage(), e);
			}
			//asignar el objeto subramo al objeto cobertura
			coberturaDTO.setSubRamoDTO(subRamoTMP);
		}

		if (!StringUtil.isEmpty(coberturaForm.getCodigo()))
			coberturaDTO.setCodigo(coberturaForm.getCodigo());

		if (!StringUtil.isEmpty(coberturaForm.getVersion()))
			coberturaDTO.setVersion(Integer.valueOf(coberturaForm.getVersion()));

		if (!StringUtil.isEmpty(coberturaForm.getDescripcion()))
			coberturaDTO.setDescripcion(coberturaForm.getDescripcion().toUpperCase());

		if (!StringUtil.isEmpty(coberturaForm.getNombreComercial()))
			coberturaDTO.setNombreComercial(coberturaForm.getNombreComercial().toUpperCase());

		if (!StringUtil.isEmpty(coberturaForm.getClaveTipoSumaAsegurada())){
			coberturaDTO.setClaveTipoSumaAsegurada(coberturaForm.getClaveTipoSumaAsegurada());
		}

		if (!StringUtil.isEmpty(coberturaForm.getClaveTipoCoaseguro()))
			coberturaDTO.setClaveTipoCoaseguro(coberturaForm.getClaveTipoCoaseguro());

		if (!StringUtil.isEmpty(coberturaForm.getValorMinimoCoaseguro()))
			coberturaDTO.setValorMinimoCoaseguro(Double.valueOf(coberturaForm.getValorMinimoCoaseguro()));

		if (!StringUtil.isEmpty(coberturaForm.getValorMaximoCoaseguro()))
			coberturaDTO.setValorMaximoCoaseguro(Double.valueOf(coberturaForm.getValorMaximoCoaseguro()));

		if (!StringUtil.isEmpty(coberturaForm.getClaveTipoLimiteCoaseguro()))
			coberturaDTO.setClaveTipoLimiteCoaseguro(coberturaForm.getClaveTipoLimiteCoaseguro());

		if (!StringUtil.isEmpty(coberturaForm.getValorMinimoLimiteCoaseguro()))
			coberturaDTO.setValorMinimoLimiteCoaseguro(Double.valueOf((coberturaForm.getValorMinimoLimiteCoaseguro())));

		if (!StringUtil.isEmpty(coberturaForm.getValorMaximoLimiteCoaseguro()))
			coberturaDTO.setValorMaximoLimiteCoaseguro(Double.valueOf(coberturaForm.getValorMaximoLimiteCoaseguro()));

		if (!StringUtil.isEmpty(coberturaForm.getClaveTipoDeducible()))
			coberturaDTO.setClaveTipoDeducible(coberturaForm.getClaveTipoDeducible());

		if (!StringUtil.isEmpty(coberturaForm.getValorMinimoDeducible()))
			coberturaDTO.setValorMinimoDeducible(Double.valueOf(coberturaForm.getValorMinimoDeducible()));

		if (!StringUtil.isEmpty(coberturaForm.getValorMaximoDeducible())) {
			coberturaDTO.setValorMaximoDeducible(Double.valueOf(coberturaForm.getValorMaximoDeducible()));
		}

		if (!StringUtil.isEmpty(coberturaForm.getClaveTipoLimiteDeducible()))
			coberturaDTO.setClaveTipoLimiteDeducible(coberturaForm.getClaveTipoLimiteDeducible());

		if (!StringUtil.isEmpty(coberturaForm.getValorMinimoLimiteDeducible())) {
			coberturaDTO.setValorMinimoLimiteDeducible(Double.valueOf(coberturaForm.getValorMinimoLimiteDeducible()));
		}

		if (!StringUtil.isEmpty(coberturaForm.getValorMaximoLimiteDeducible())) {
			coberturaDTO.setValorMaximoLimiteDeducible(Double.valueOf(coberturaForm.getValorMaximoLimiteDeducible()));
		}

		if (!StringUtil.isEmpty(coberturaForm.getNumeroSecuencia())) {
			try {	coberturaDTO.setNumeroSecuencia(regresaBigDecimal(coberturaForm.getNumeroSecuencia()));
			} catch (SystemException e) { LOG.error(e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getIdControlArchivoRegistroCNSF())){
			try {	coberturaDTO.setIdControlArchivoRegistroCNSF(regresaBigDecimal(coberturaForm.getIdControlArchivoRegistroCNSF()));
			} catch (SystemException e) {	LOG.error(e.getMessage(), e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getDescripcionRegistroCNSF())){
			coberturaDTO.setDescripcionRegistroCNSF(coberturaForm.getDescripcionRegistroCNSF().toUpperCase());
		}else{
			coberturaDTO.setDescripcionRegistroCNSF(coberturaForm.getDescripcionRegistroCNSF());
		}

		if (!esCadenaVacia(coberturaForm.getClaveTipoValorSumaAsegurada())){
			try{
				coberturaDTO.setClaveTipoValorSumaAsegurada(Integer.valueOf(coberturaForm.getClaveTipoValorSumaAsegurada()));
			}catch(Exception e){LOG.error(e.getMessage(), e);}
		}
				
		
		if(coberturaForm.getClaveNegocio() != null && coberturaForm.getClaveNegocio().equals(Negocio.CLAVE_NEGOCIO_AUTOS)){
			if (!esCadenaVacia(coberturaForm.getIdCoberturaSumaAsegurada())){
				try {
					coberturaDTO.setIdCoberturaSumaAsegurada(regresaBigDecimal(coberturaForm.getIdCoberturaSumaAsegurada()));
				} catch (SystemException e) {LOG.error(e.getMessage(), e);}
			}			
		}else{
			if (!esCadenaVacia(coberturaForm.getIdCoberturaSumaAsegurada())){
				try {
					if (!coberturaDTO.getClaveTipoSumaAsegurada().equals("1") || 
							coberturaDTO.getClaveTipoValorSumaAsegurada() != null && coberturaDTO.getClaveTipoValorSumaAsegurada().intValue() == CoberturaDTO.TIPO_VALOR_SA_SUBLIMITE){
						coberturaDTO.setIdCoberturaSumaAsegurada(regresaBigDecimal(coberturaForm.getIdCoberturaSumaAsegurada()));
					}else{
						coberturaDTO.setIdCoberturaSumaAsegurada(BigDecimal.ZERO);
					}
				} catch (SystemException e) {LOG.error(e.getMessage(), e);}
			}
		}
		
		if (!esCadenaVacia(coberturaForm.getFactorMinSumaAsegurada())){
			try {	coberturaDTO.setFactorMinSumaAsegurada(regresaBigDecimal(coberturaForm.getFactorMinSumaAsegurada()));
			} catch (SystemException e) {	LOG.error(e.getMessage(), e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getFactorMaxSumaAsegurada())){
			try {	coberturaDTO.setFactorMaxSumaAsegurada(regresaBigDecimal(coberturaForm.getFactorMaxSumaAsegurada()));
			} catch (SystemException e) {	LOG.error(e.getMessage(), e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getFactorSumaAsegurada())){
			try {	coberturaDTO.setFactorSumaAsegurada(regresaBigDecimal(coberturaForm.getFactorSumaAsegurada()));
			} catch (SystemException e) {	LOG.error(e.getMessage(), e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getMinSumaAsegurada())){
			try {	coberturaDTO.setMinSumaAsegurada(regresaBigDecimal(coberturaForm.getMinSumaAsegurada()));
			} catch (SystemException e) {	LOG.error(e.getMessage(), e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getMaxSumaAsegurada())){
			try {	coberturaDTO.setMaxSumaAsegurada(regresaBigDecimal(coberturaForm.getMaxSumaAsegurada()));
			} catch (SystemException e) {	LOG.error(e.getMessage(), e);}
		}
		
		if (!esCadenaVacia(coberturaForm.getLeyenda())){
			coberturaDTO.setLeyenda(coberturaForm.getLeyenda());
		}
		if (!esCadenaVacia(coberturaForm.getClaveTipoCalculo())){
			coberturaDTO.setClaveTipoCalculo(coberturaForm.getClaveTipoCalculo());
		}		
		
		
		if (!StringUtil.isEmpty(coberturaForm.getCoberturaEsPropia()))
			coberturaDTO.setCoberturaEsPropia(coberturaForm.getCoberturaEsPropia());
		
		if (!StringUtil.isEmpty(coberturaForm.getCoberturaAplicaDescuentoRecargo()))
			coberturaDTO.setCoberturaAplicaDescuentoRecargo(coberturaForm.getCoberturaAplicaDescuentoRecargo());
		
		if (!StringUtil.isEmpty(coberturaForm.getValMinPrimaCobertura()))
			coberturaDTO.setValMinPrimaCobertura(Double.valueOf(coberturaForm.getValMinPrimaCobertura()));
		
		if (!StringUtil.isEmpty(coberturaForm.getClaveFuenteSumaAsegurada()))
			coberturaDTO.setClaveFuenteSumaAsegurada(coberturaForm.getClaveFuenteSumaAsegurada());
		
		Double idRamo;
		if (!StringUtil.isEmpty(coberturaForm.getIdTcRamo())) {
			idRamo = Double.valueOf(coberturaForm.getIdTcRamo());
			coberturaDTO.setIdTcRamo(BigDecimal.valueOf(idRamo));
		}

		//valores provenientes de un check que se deben pasar a entero
		coberturaDTO.setEditable(UtileriasWeb.calculaValorBooleanDelCheck(coberturaForm.getEditable()));
		coberturaDTO.setClaveDesglosaRiesgos(calculaValorEnteroDelCheck(coberturaForm.getClaveDesglosaRiesgos()));
		coberturaDTO.setClaveAutorizacion(calculaValorEnteroDelCheck(coberturaForm.getClaveAutorizacion()));
		coberturaDTO.setClaveMostrarPanCobertura(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPanCobertura()));
		coberturaDTO.setClaveMostrarPanDescripcion(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPanDescripcion()));
		coberturaDTO.setClaveMostrarPanSumaAsegurada(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPanSumaAsegurada()));
		coberturaDTO.setClaveMostrarPanCoaseguro(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPanCoaseguro()));
		coberturaDTO.setClaveMostrarPanDeducible(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPanDeducible()));
		coberturaDTO.setClaveMostrarPanPrimaNeta(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPanPrimaNeta()));
		coberturaDTO.setClaveMostrarCotCobertura(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarCotCobertura()));
		coberturaDTO.setClaveMostrarCotDescripcion(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarCotDescripcion()));
		coberturaDTO.setClaveMostrarCotSumaAsegurada(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarCotSumaAsegurada()));
		coberturaDTO.setClaveMostrarCotCoaseguro(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarCotCoaseguro()));
		coberturaDTO.setClaveMostrarCotDeducible(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarCotDeducible()));
		coberturaDTO.setClaveMostrarCotPrimaNeta(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarCotPrimaNeta()));
		coberturaDTO.setClaveMostrarPolCobertura(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPolCobertura()));
		coberturaDTO.setClaveMostrarPolDescripcion(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPolDescripcion()));
		coberturaDTO.setClaveMostrarPolSumaAsegurada(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPolSumaAsegurada()));
		coberturaDTO.setClaveMostrarPolCoaseguro(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPolCoaseguro()));
		coberturaDTO.setClaveMostrarPolDeducible(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPolDeducible()));
		coberturaDTO.setClaveMostrarPolPrimaNeta(calculaValorEnteroDelCheck(coberturaForm.getClaveMostrarPolPrimaNeta()));
		coberturaDTO.setClaveImporteCero(calculaValorEnteroDelCheck(coberturaForm.getClaveImporteCero()));		
		coberturaDTO.setClaveIgualacion(Short.valueOf(calculaValorEnteroDelCheck(coberturaForm.getClaveIgualacion())));
		coberturaDTO.setClavePrimerRiesgo(Short.valueOf(calculaValorEnteroDelCheck(coberturaForm.getClavePrimerRiesgo())));
		coberturaDTO.setCoberturaEsPropia(calculaValorEnteroDelCheck(coberturaForm.getCoberturaEsPropia()));
		coberturaDTO.setCoberturaAplicaDescuentoRecargo(calculaValorEnteroDelCheck(coberturaForm.getCoberturaAplicaDescuentoRecargo()));
		coberturaDTO.setReinstalable(calculaValorEnteroDelCheck(coberturaForm.getReinstalable()));
		coberturaDTO.setSeguroObligatorio(calculaValorEnteroDelCheck(coberturaForm.getSeguroObligatorio()));
		
		if (!StringUtil.isEmpty(coberturaForm.getNumeroRegistro())){
			coberturaDTO.setNumeroRegistro(coberturaForm.getNumeroRegistro());
		}
		
		try {
			if (!StringUtil.isEmpty(coberturaForm.getNumeroRegistro())){
				coberturaDTO.setFechaRegistro(formatoFecha.parse(coberturaForm.getFechaRegistro()));
			}
		} catch (ParseException e) {
			coberturaDTO.setFechaRegistro(null);
			LOG.error("Error al parsear fecha:" + coberturaForm.getFechaRegistro() , e);
		}

		if (!StringUtil.isEmpty(coberturaForm.getFactorRecargo())){
			coberturaDTO.setFactorRecargo(BigDecimal.valueOf(Integer.valueOf(coberturaForm.getFactorRecargo())));
		}
		
	}

	/**
	 * Method poblarForm
	 * 
	 * @param CoberturaDTO
	 * @param CoberturaForm
	 */
	protected void poblarForm(CoberturaDTO coberturaDTO, CoberturaForm coberturaForm) {
		if (coberturaDTO.getIdToCobertura() != null)
			coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura()
					.toString());

		if (coberturaDTO.getSubRamoDTO() != null
				&& coberturaDTO.getSubRamoDTO().getIdTcSubRamo() != null) {
			coberturaForm.setIdTcSubRamo(coberturaDTO.getSubRamoDTO()
					.getIdTcSubRamo().toString());
			coberturaForm.setIdTcRamo(coberturaDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo().toString());
			coberturaForm.setDescripcionRamo(coberturaDTO.getSubRamoDTO().getRamoDTO().getDescripcion());
			coberturaForm.setDescripcionSubRamo(coberturaDTO.getSubRamoDTO().getDescripcionSubRamo());
		}

		if (!StringUtil.isEmpty(coberturaDTO.getCodigo()))
			coberturaForm.setCodigo(coberturaDTO.getCodigo());

		if (coberturaDTO.getVersion() != null)
			coberturaForm.setVersion(coberturaDTO.getVersion().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getDescripcion()))
			coberturaForm.setDescripcion(coberturaDTO.getDescripcion());

		if (!StringUtil.isEmpty(coberturaDTO.getNombreComercial()))
			coberturaForm.setNombreComercial(coberturaDTO.getNombreComercial());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveTipoSumaAsegurada())){
			coberturaForm.setClaveTipoSumaAsegurada(coberturaDTO.getClaveTipoSumaAsegurada());
			coberturaForm.setTipoSumaAsegurada(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_SUMA_ASEGURADA, coberturaDTO.getClaveTipoSumaAsegurada()));
		}
		
		if (!StringUtil.isEmpty(coberturaDTO.getClaveTipoCoaseguro())){
			coberturaForm.setClaveTipoCoaseguro(coberturaDTO.getClaveTipoCoaseguro());
			coberturaForm.setTipoCoaseguro(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_COASEGURO, coberturaDTO.getClaveTipoCoaseguro()));
		}

		if (coberturaDTO.getValorMinimoCoaseguro() != null)
			coberturaForm.setValorMinimoCoaseguro(coberturaDTO.getValorMinimoCoaseguro().toString());

		if (coberturaDTO.getValorMaximoCoaseguro() != null)
			coberturaForm.setValorMaximoCoaseguro(coberturaDTO.getValorMaximoCoaseguro().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveTipoLimiteCoaseguro())){
			coberturaForm.setClaveTipoLimiteCoaseguro(coberturaDTO.getClaveTipoLimiteCoaseguro());
			coberturaForm.setTipoLimiteCoaseguro(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_LIMITE_COASEGURO, coberturaDTO.getClaveTipoLimiteCoaseguro()));
		}

		if (coberturaDTO.getValorMinimoLimiteCoaseguro() != null)
			coberturaForm.setValorMinimoLimiteCoaseguro(coberturaDTO.getValorMinimoLimiteCoaseguro().toString());

		if (coberturaDTO.getValorMaximoLimiteCoaseguro() != null)
			coberturaForm.setValorMaximoLimiteCoaseguro(coberturaDTO.getValorMaximoLimiteCoaseguro().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveTipoDeducible())){
			coberturaForm.setClaveTipoDeducible(coberturaDTO.getClaveTipoDeducible());
			coberturaForm.setTipoDeducible(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_DEDUCIBLE, coberturaDTO.getClaveTipoDeducible()));
		}

		if (coberturaDTO.getValorMinimoDeducible() != null)
			coberturaForm.setValorMinimoDeducible(coberturaDTO.getValorMinimoDeducible().toString());

		if (coberturaDTO.getValorMaximoDeducible() != null)
			coberturaForm.setValorMaximoDeducible(coberturaDTO.getValorMaximoDeducible().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveTipoLimiteDeducible())){
			coberturaForm.setClaveTipoLimiteDeducible(coberturaDTO.getClaveTipoLimiteDeducible());
			coberturaForm.setTipoLimiteDeducible(UtileriasWeb.getDescripcionCatalogoValorFijo(Sistema.CLAVE_TIPO_LIMITE_DEDUCIBLE, coberturaDTO.getClaveTipoLimiteDeducible()));
		}

		if (coberturaDTO.getValorMinimoLimiteDeducible() != null)
			coberturaForm.setValorMinimoLimiteDeducible(coberturaDTO.getValorMinimoLimiteDeducible().toString());

		if (coberturaDTO.getValorMaximoLimiteDeducible() != null)
			coberturaForm.setValorMaximoLimiteDeducible(coberturaDTO.getValorMaximoLimiteDeducible().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveDesglosaRiesgos()))
			coberturaForm.setClaveDesglosaRiesgos(coberturaDTO.getClaveDesglosaRiesgos());

		if (coberturaDTO.getNumeroSecuencia() != null)
			coberturaForm.setNumeroSecuencia(coberturaDTO.getNumeroSecuencia().toBigInteger().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveAutorizacion()))
			coberturaForm.setClaveAutorizacion(coberturaDTO.getClaveAutorizacion());
		
		if (coberturaDTO.getIdCoberturaSumaAsegurada() != null)
			coberturaForm.setIdCoberturaSumaAsegurada(coberturaDTO.getIdCoberturaSumaAsegurada().toString());
		
		if (coberturaDTO.getIdControlArchivoRegistroCNSF() != null){
			if (coberturaDTO.getIdControlArchivoRegistroCNSF().intValue() != 0){
				coberturaForm.setIdControlArchivoRegistroCNSF(coberturaDTO.getIdControlArchivoRegistroCNSF().toString());
				ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
				try {
					controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(coberturaDTO.getIdControlArchivoRegistroCNSF());
					coberturaForm.setNombreArchivoRegistroCNSF(controlArchivoDTO.getNombreArchivoOriginal());
				} catch (SystemException e) {LOG.error(e.getMessage(), e);}
			}
		}
		
		if (!esCadenaVacia(coberturaDTO.getDescripcionRegistroCNSF())){
			coberturaForm.setDescripcionRegistroCNSF(coberturaDTO.getDescripcionRegistroCNSF());
		}
		
		if (coberturaDTO.getFactorMinSumaAsegurada() != null){
			coberturaForm.setFactorMinSumaAsegurada(coberturaDTO.getFactorMinSumaAsegurada().toString());
		}
		
		if (coberturaDTO.getFactorMaxSumaAsegurada() != null){
			coberturaForm.setFactorMaxSumaAsegurada(coberturaDTO.getFactorMaxSumaAsegurada().toString());
		}
		
		if (coberturaDTO.getFactorSumaAsegurada() != null){
			coberturaForm.setFactorSumaAsegurada(coberturaDTO.getFactorSumaAsegurada().toString());
		}
		
		if (coberturaDTO.getMinSumaAsegurada() != null){
			coberturaForm.setMinSumaAsegurada(coberturaDTO.getMinSumaAsegurada().toString());
		}
		
		if (coberturaDTO.getMaxSumaAsegurada() != null){
			coberturaForm.setMaxSumaAsegurada(coberturaDTO.getMaxSumaAsegurada().toString());
		}
		
		if (!esCadenaVacia(coberturaDTO.getLeyenda())){
			coberturaForm.setLeyenda(coberturaDTO.getLeyenda());
		}
		
		if (coberturaDTO.getClaveTipoValorSumaAsegurada() != null){
			coberturaForm.setClaveTipoValorSumaAsegurada(coberturaDTO.getClaveTipoValorSumaAsegurada().toString());
		}

		if (!StringUtil.isEmpty(coberturaDTO.getCoberturaEsPropia()))
			coberturaForm.setCoberturaEsPropia(coberturaDTO.getCoberturaEsPropia());
		
		if (!StringUtil.isEmpty(coberturaDTO.getCoberturaAplicaDescuentoRecargo()))
			coberturaForm.setCoberturaAplicaDescuentoRecargo(coberturaDTO.getCoberturaAplicaDescuentoRecargo());
		
		if (coberturaDTO.getValMinPrimaCobertura() != null)
			coberturaForm.setValMinPrimaCobertura(coberturaDTO.getValMinPrimaCobertura().toString());

		if (!StringUtil.isEmpty(coberturaDTO.getClaveFuenteSumaAsegurada()))
			coberturaForm.setClaveFuenteSumaAsegurada(coberturaDTO.getClaveFuenteSumaAsegurada());
		
		if (!StringUtil.isEmpty(coberturaDTO.getClaveTipoCalculo()))
			coberturaForm.setClaveTipoCalculo(coberturaDTO.getClaveTipoCalculo());
		

		
		//valores provenientes de un check que se deben pasar a entero
		coberturaForm.setEditable(calculaValorStringDelCheck(coberturaDTO.getEditable()));
		coberturaForm.setClaveDesglosaRiesgos(calculaValorStringDelCheck(coberturaDTO.getClaveDesglosaRiesgos()));
		coberturaForm.setClaveAutorizacion(calculaValorStringDelCheck(coberturaDTO.getClaveAutorizacion()));
		coberturaForm.setClaveMostrarPanCobertura(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPanCobertura()));
		coberturaForm.setClaveMostrarPanDescripcion(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPanDescripcion()));
		coberturaForm.setClaveMostrarPanSumaAsegurada(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPanSumaAsegurada()));
		coberturaForm.setClaveMostrarPanCoaseguro(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPanCoaseguro()));
		coberturaForm.setClaveMostrarPanDeducible(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPanDeducible()));
		coberturaForm.setClaveMostrarPanPrimaNeta(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPanPrimaNeta()));
		coberturaForm.setClaveMostrarCotCobertura(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarCotCobertura()));
		coberturaForm.setClaveMostrarCotDescripcion(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarCotDescripcion()));
		coberturaForm.setClaveMostrarCotSumaAsegurada(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarCotSumaAsegurada()));
		coberturaForm.setClaveMostrarCotCoaseguro(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarCotCoaseguro()));
		coberturaForm.setClaveMostrarCotDeducible(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarCotDeducible()));
		coberturaForm.setClaveMostrarCotPrimaNeta(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarCotPrimaNeta()));
		coberturaForm.setClaveMostrarPolCobertura(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPolCobertura()));
		coberturaForm.setClaveMostrarPolDescripcion(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPolDescripcion()));
		coberturaForm.setClaveMostrarPolSumaAsegurada(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPolSumaAsegurada()));
		coberturaForm.setClaveMostrarPolCoaseguro(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPolCoaseguro()));
		coberturaForm.setClaveMostrarPolDeducible(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPolDeducible()));
		coberturaForm.setClaveMostrarPolPrimaNeta(calculaValorStringDelCheck(coberturaDTO.getClaveMostrarPolPrimaNeta()));
		coberturaForm.setClaveImporteCero(calculaValorStringDelCheck(coberturaDTO.getClaveImporteCero()));		
		coberturaForm.setCoberturaEsPropia(calculaValorStringDelCheck(coberturaDTO.getCoberturaEsPropia()));
		coberturaForm.setCoberturaAplicaDescuentoRecargo(calculaValorStringDelCheck(coberturaDTO.getCoberturaAplicaDescuentoRecargo()));
		
		coberturaForm.setClaveIgualacion(calculaValorStringDelCheck(coberturaDTO.getClaveIgualacion()));
		coberturaForm.setClavePrimerRiesgo(calculaValorStringDelCheck(coberturaDTO.getClavePrimerRiesgo()));

		coberturaForm.setReinstalable(calculaValorStringDelCheck(coberturaDTO.getReinstalable()));
		coberturaForm.setSeguroObligatorio(calculaValorStringDelCheck(coberturaDTO.getSeguroObligatorio()));
		if (!StringUtil.isEmpty(coberturaDTO.getNumeroRegistro())){
			coberturaForm.setNumeroRegistro(coberturaDTO.getNumeroRegistro());
		}
		if (coberturaDTO.getFechaRegistro() != null){
			coberturaForm.setFechaRegistro(formatoFecha.format(coberturaDTO.getFechaRegistro()));
		}

		if (coberturaDTO.getFactorRecargo() != null){
			coberturaForm.setFactorRecargo(coberturaDTO.getFactorRecargo().toString());
		}
		
	}
	
	private void limpiarForm(CoberturaForm form){
		form.setIdTcRamo("");
		form.setIdTcSubRamo("");
		form.setCodigo("");
		form.setVersion("");
		form.setDescripcion("");
		form.setNombreComercial("");
		form.setClaveTipoSumaAsegurada("");
		form.setClaveTipoCoaseguro("");
		form.setValorMinimoCoaseguro("");
		form.setValorMaximoCoaseguro("");
		form.setClaveTipoLimiteCoaseguro("");
		form.setValorMinimoLimiteCoaseguro("");
		form.setValorMaximoLimiteCoaseguro("");
		form.setClaveTipoDeducible("");
		form.setValorMinimoDeducible("");
		form.setValorMaximoDeducible("");
		form.setClaveTipoLimiteDeducible("");
		form.setValorMinimoLimiteDeducible("");
		form.setValorMaximoLimiteDeducible("");
		form.setClaveDesglosaRiesgos("");
		form.setClaveMostrarPanCobertura("");
		form.setClaveMostrarPanDescripcion("");
		form.setClaveMostrarPanSumaAsegurada("");
		form.setClaveMostrarPanCoaseguro("");
		form.setClaveMostrarPanDeducible("");
		form.setClaveMostrarPanPrimaNeta("");
		form.setClaveMostrarCotCobertura("");
		form.setClaveMostrarCotDescripcion("");
		form.setClaveMostrarCotSumaAsegurada("");
		form.setClaveMostrarCotCoaseguro("");
		form.setClaveMostrarCotDeducible("");
		form.setClaveMostrarCotPrimaNeta("");
		form.setClaveMostrarPolCobertura("");
		form.setClaveMostrarPolDescripcion("");
		form.setClaveMostrarPolSumaAsegurada("");
		form.setClaveMostrarPolCoaseguro("");
		form.setClaveMostrarPolDeducible("");
		form.setClaveMostrarPolPrimaNeta("");
		form.setNumeroSecuencia("");
		form.setClaveAutorizacion("");
		form.setClaveImporteCero("");
		form.setCoberturaEsPropia("");
		form.setCoberturaAplicaDescuentoRecargo("");
		form.setClaveFuenteSumaAsegurada("");
		form.setNumeroRegistro("");
		form.setFechaRegistro("");
	}


	/**
	 * Method listarPorPradre
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException
	 */
	public void listarPorPradre(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException {

		int numHijos =0;
		String itemId = "";
		if (request.getParameter("id") != null) {
			itemId = request.getParameter("id");
		}

		String id = "";
		if (request.getAttribute("id") != null) {
			id = (String) request.getAttribute("id");
		}

		String contextoMenu = "";
		if (request.getParameter("menu") != null) {
			contextoMenu = request.getParameter("menu");
		}
		TreeLoader tree = new TreeLoader(contextoMenu, mapping, form, request, response);

		CoberturaSeccionDN coberturaSeccionDN = CoberturaSeccionDN.getInstancia();

		try {
			List<CoberturaSeccionDTO> coberturas = coberturaSeccionDN.listarVigentesPorSeccion(regresaBigDecimal(id));
			
			StringBuffer buffer = new StringBuffer();
			Iterator<CoberturaSeccionDTO> iteratorList = coberturas.iterator();
			while (iteratorList.hasNext()) {
				CoberturaSeccionDTO coberturasSeccionDTO = iteratorList.next();
				numHijos =0;
				
				RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
				List<RiesgoCoberturaDTO> riesgos = riesgoCoberturaDN.listarRiesgoVigenteAsociado(coberturasSeccionDTO.getId().getIdtocobertura(), coberturasSeccionDTO.getId().getIdtoseccion());
				if(riesgos != null && riesgos.size() > 0) {
					numHijos = riesgos.size();
				}
				
				buffer = tree.escribirItem(buffer, coberturasSeccionDTO.getCoberturaDTO().getNombreComercial(),
						coberturasSeccionDTO.getId().getIdtocobertura().toString(), id, numHijos);
								
			}
			buffer = tree.xmlHeader(buffer, itemId);
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (SystemException e) {
			throw new SystemException(e);
		} catch (ExcepcionDeAccesoADatos e) {
			throw new SystemException(e);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render coberturas List",
					ioException);
		} // End of try/catch
	}

	/**
	 * Method mostrarPaginaArchivosAnexos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarPaginaArchivosAnexos(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String idToProducto = request.getParameter("id");
		if (idToProducto != null)
			request.getSession().setAttribute("idToCobertura", request.getParameter("id"));
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);

	}
	

	/**
	 * Method mostrarDocumentosAnexos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void mostrarDocumentosAnexos(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(regresaBigDecimal(id));

		List<DocumentoAnexoCoberturaDTO> listaDocumentos;
		listaDocumentos = CoberturaDN.getInstancia().listarDocumentosAnexos(coberturaDTO.getIdToCobertura());

		ControlArchivoDTO controlArchivoDTO; 
		
//		String json = "{rows:[";
//		if(listaDocumentos != null && listaDocumentos.size() > 0) {
//			for(DocumentoAnexoCoberturaDTO documento : listaDocumentos) {
//				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(documento.getIdToControlArchivo()); 
//				json += "{id:\"" + documento.getIdToDocumentoAnexoCobertura() + "\",data:[";
//				json += coberturaDTO.getIdToCobertura() + ",\"";
//				json += controlArchivoDTO.getNombreArchivoOriginal() + "\",\"";
//				json += documento.getClaveObligatoriedad() + "\",";
//				json += documento.getNumeroSecuencia() + ",\"";
//				if (!documento.getDescripcion().equals("default"))	json += documento.getDescripcion() + "\"]},";
//				else	json += "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(listaDocumentos != null && listaDocumentos.size() > 0) {
			for(DocumentoAnexoCoberturaDTO documento : listaDocumentos) {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(documento.getIdToControlArchivo()); 
				
				MidasJsonRow row = new MidasJsonRow();
				
				row.setId(documento.getIdToDocumentoAnexoCobertura().toString());
				row.setDatos(
						coberturaDTO.getIdToCobertura().toString(),
						controlArchivoDTO.getNombreArchivoOriginal(),
						documento.getClaveObligatoriedad().toString(),
						documento.getNumeroSecuencia().toString(),
						(!documento.getDescripcion().equals("default")?documento.getDescripcion():""),
						"/MidasWeb/img/icons/ico_verdetalle.gif^Descargar^javascript:descargarDocumentoProducto("+
						controlArchivoDTO.getIdToControlArchivo()+");^_self"
				);
				json.addRow(row);
			}
		}
		
		response.setContentType("text/json");
		LOG.info("Documentos Anexos: "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarDocumentoAnexo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarDocumentoAnexo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		String action = "";
		BigDecimal idToControlArchivo = regresaBigDecimal(request.getParameter("gr_id"));
		try {
			DocumentoAnexoCoberturaDTO documentoAnexo = new DocumentoAnexoCoberturaDTO();
			documentoAnexo = DocumentoAnexoCoberturaDN.getInstancia().getPorId(idToControlArchivo);
			CoberturaDTO coberturaDTO = new CoberturaDTO();
			coberturaDTO.setIdToCobertura(regresaBigDecimal(request.getParameter("idToCobertura")));
			coberturaDTO = CoberturaDN.getInstancia().getPorId(coberturaDTO);
			documentoAnexo.setCoberturaDTO(coberturaDTO);
			if(request.getParameter("!nativeeditor_status").equals("updated")) {
				short valor = Short.valueOf(request.getParameter("claveObligatoriedad")).shortValue();
				documentoAnexo.setClaveObligatoriedad(valor);
				String descripcion = request.getParameter("descripcionDocumento");
				descripcion = UtileriasWeb.parseEncodingISO(descripcion);
				documentoAnexo.setDescripcion(descripcion);
				documentoAnexo.setNumeroSecuencia(regresaBigDecimal(request.getParameter("numeroSecuencia")));
				Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
				if (usuario!= null){
					documentoAnexo.setCodigoUsuarioModificacion(usuario.getId().toString());
					documentoAnexo.setNombreUsuarioModificacion(usuario.getNombre());
					documentoAnexo.setFechaModificacion(new Date());
				}
				DocumentoAnexoCoberturaDN.getInstancia().modificar(documentoAnexo);
				action = "update";
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				
				DocumentoAnexoCoberturaDN.getInstancia().borrar(documentoAnexo);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	private void agregarDatosUsuarioCreacion(HttpServletRequest request,CoberturaDTO coberturaDTO){
		if (request != null && coberturaDTO != null){
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			if (usuario!=null){
				if (usuario.getId() != null)
					coberturaDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
			}
			coberturaDTO.setFechaCreacion(new Date());
		}
	}
	
	private void agregarDatosUsuarioModificacion(HttpServletRequest request,CoberturaDTO coberturaDTO){
		if (request != null && coberturaDTO != null){
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			if (usuario!=null){
				if (usuario.getId() != null)
					coberturaDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
			}
			coberturaDTO.setFechaModificacion(new Date());
		}
	}

	/**
	 * Method listarJsonPorPradre
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException
	 * @throws IOException 
	 */
	public void listarJsonPorPradre(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws SystemException, IOException {
		String id = request.getParameter("id");
		List<CoberturaSeccionDTO> coberturas = new CoberturaSeccionSN().listarVigentesPorSeccion(regresaBigDecimal(id));

		MidasJsonBase json = new MidasJsonBase();
		for(CoberturaSeccionDTO coberturaSeccion : coberturas) {
			MidasJsonRow row = new MidasJsonRow();
			row.setId(coberturaSeccion.getId().getIdtocobertura().toString());
			row.setDatos(coberturaSeccion.getCoberturaDTO().getCodigo(),
					coberturaSeccion.getCoberturaDTO().getNombreComercial(),
					coberturaSeccion.getCoberturaDTO().getDescripcion(),
					coberturaSeccion.getClaveObligatoriedad().compareTo(new BigDecimal(0))==0? "No" : "Si");
			json.addRow(row);
		}
		response.setContentType("text/json");
		LOG.info(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}