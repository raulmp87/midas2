/**
 * 
 */
package mx.com.afirme.midas.producto.configuracion.tipopoliza;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class TipoPolizaSN {
	private TipoPolizaFacadeRemote beanRemoto;

	public TipoPolizaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en TipoPolizaSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(TipoPolizaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<TipoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<TipoPolizaDTO> list = beanRemoto.findAll();
		return list;

	}

	public void agregar(TipoPolizaDTO tipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(tipoPolizaDTO);
	}

	public void modificar(TipoPolizaDTO tipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(tipoPolizaDTO);
	}

	public TipoPolizaDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);

	}

	public void borrar(TipoPolizaDTO tipoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(tipoPolizaDTO);
	}

	public List<TipoPolizaDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}

	public TipoPolizaDTO getPorIdHijo(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByChildId(id);
	}
	
	public void borradoLogico(TipoPolizaDTO TipoPolizaDTO){
		beanRemoto.borradoLogico(TipoPolizaDTO);
	}
	
	public List<TipoPolizaDTO> listarVigentes() throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarVigentes();
	}
	
	public List<TipoPolizaDTO> listarVigentesPorIdProducto(BigDecimal idToProducto) throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarVigentesPorIdProducto(idToProducto);
	}
	
	public TipoPolizaDTO encontrarPorCotizacion(BigDecimal idToCotizacion){
		return beanRemoto.findTipoPolizaByCotizacion(idToCotizacion);
	}

	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto) {
		return beanRemoto.listarPorIdProducto(idToProducto);
	}
	
	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto, Boolean verInactivos, Boolean soloActivos) {
		return beanRemoto.listarPorIdProducto(idToProducto, verInactivos, soloActivos);
	}
	
}
