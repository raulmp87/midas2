package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 20 de Agosto de 2009
 */
public class SeccionRequeridaSN {

	private SeccionRequeridaFacadeRemote beanRemoto;

	public SeccionRequeridaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(SeccionRequeridaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void guardar(SeccionRequeridaDTO seccionRequeridaDTO)throws ExcepcionDeAccesoADatos {
		beanRemoto.save(seccionRequeridaDTO);
	}

	public void actualizar(SeccionRequeridaDTO seccionRequeridaDTO)throws ExcepcionDeAccesoADatos {
		beanRemoto.update(seccionRequeridaDTO);
	}

	public void borrar(SeccionRequeridaDTO seccionRequeridaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(seccionRequeridaDTO);
	}

	public List<SeccionRequeridaDTO> listarSeccionesRequeridasAsociadas(BigDecimal idToSeccion) {
		return beanRemoto.findRequiredSecctions(idToSeccion);
	}
}
