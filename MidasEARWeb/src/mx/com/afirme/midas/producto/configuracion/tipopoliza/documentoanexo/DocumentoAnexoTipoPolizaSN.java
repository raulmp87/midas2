package mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoAnexoTipoPolizaSN {
	private DocumentoAnexoTipoPolizaFacadeRemote beanRemoto;

	public DocumentoAnexoTipoPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoAnexoTipoPolizaFacadeRemote.class);
		} catch (Exception e) { throw new SystemException(Sistema.NO_DISPONIBLE);}
		LogDeMidasWeb.log("bean Remoto DocumentoAnexoTipoPoliza instanciado", Level.FINEST, null);
	}

	public List<DocumentoAnexoTipoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}

	public void agregar(DocumentoAnexoTipoPolizaDTO ajustadorDTO)throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.save(ajustadorDTO);
		} catch(Exception e){ throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e); }
	}

	public void modificar(DocumentoAnexoTipoPolizaDTO ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.update(ajustadorDTO);
		}catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public DocumentoAnexoTipoPolizaDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findById(id);
		}catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO); }
	}

	public void borrar(DocumentoAnexoTipoPolizaDTO ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.delete(ajustadorDTO);
		}catch(EJBTransactionRolledbackException e){ throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName()); }
	}
	
	public List<DocumentoAnexoTipoPolizaDTO> encontrarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findByProperty(propiedad, valor);
		}catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
}
