package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CoberturaSeccionPaqueteForm extends MidasBaseForm {

	private static final long serialVersionUID = -7431173778930283092L;
	private String idToCoberturaSeccionPaquete;
    private String idtoPaquetePoliza;
    private String idtoSeccion;
    private String descripcionSeccion;
    private String idToCobertura;
    private String descripcionCobertura;
    private String claveContrato;
    private String valorSumaAsegurada;
	public String getIdToCoberturaSeccionPaquete() {
		return idToCoberturaSeccionPaquete;
	}
	public void setIdToCoberturaSeccionPaquete(String idToCoberturaSeccionPaquete) {
		this.idToCoberturaSeccionPaquete = idToCoberturaSeccionPaquete;
	}
	public String getIdtoPaquetePoliza() {
		return idtoPaquetePoliza;
	}
	public void setIdtoPaquetePoliza(String idtoPaquetePoliza) {
		this.idtoPaquetePoliza = idtoPaquetePoliza;
	}
	public String getIdtoSeccion() {
		return idtoSeccion;
	}
	public void setIdtoSeccion(String idtoSeccion) {
		this.idtoSeccion = idtoSeccion;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(String idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getClaveContrato() {
		return claveContrato;
	}
	public void setClaveContrato(String claveContrato) {
		this.claveContrato = claveContrato;
	}
	public String getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(String valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
}
