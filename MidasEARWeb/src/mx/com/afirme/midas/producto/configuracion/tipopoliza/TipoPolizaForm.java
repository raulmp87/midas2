package mx.com.afirme.midas.producto.configuracion.tipopoliza;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion.ExclusionDescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form.PaquetePolizaForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.form.RespuestaActualizacionPaqueteForm;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class TipoPolizaForm extends MidasBaseForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idToTipoPoliza;
	private String codigo;
	private String version;
	private String descripcion;
	private String nombreComercial;
	private String claveRenovable;
	private String clavePagoInmediato;
	private String diasRetroactividad;
	private String diasDiferimiento;
	private String diasGracia;
	private List<SeccionDTO> secciones;
	private String idToProducto;
	private String nombreComercialProducto;
	private String idTipoCalculoEmision = "1";
	private String idTipoCalculoCancelacion;
	private String descripcionTipoCalculoEmision;
	private String descripcionTipoCalculoCancelacion;
	private String idControlArchivoRegistroCNSF;
	private List<RamoTipoPolizaDTO> ramoTipoPolizaAsociados;
	private List<RamoDTO> ramosPorAsociar;
	private List<RamoDTO> ramosAsociados;
	private List<RecargoVarioTipoPolizaDTO> recargosAsociados;
	private List<RecargoVarioDTO> recargosPorAsociar;
	private List<AumentoVarioTipoPolizaDTO> aumentosAsociados;
	private List<AumentoVarioDTO> aumentosPorAsociar;
	private List<DescuentoVarioTipoPolizaDTO> descuentoTipoPolizaAsociados;
	private List<DescuentoDTO> descuentosPorAsociar;
	private List<ExclusionRecargoVarioTipoPolizaDTO> excRecargoTipoPolizaAsociados;
	private List<ExclusionRecargoVarioTipoPolizaDTO> excRecargoTipoPolizaNoAsociados;
	private List<ExclusionDescuentoVarioTipoPolizaDTO> excDescuentoTipoPolizaAsociados;
	private List<ExclusionDescuentoVarioTipoPolizaDTO> excDescuentoTipoPolizaNoAsociados;
	private List<ExclusionAumentoVarioTipoPolizaDTO> excAumentoTipoPolizaAsociados;
	private List<ExclusionAumentoVarioTipoPolizaDTO> excAumentoTipoPolizaNoAsociados;
	private List<MonedaTipoPolizaDTO> monedasAsociadas;
	private List<MonedaDTO> monedasNoAsociadas;
	private String descripcionRegistroCNSF;
	private List<CoberturaDTO> coberturas;
	private String claveTMPCombo;
	private String claveEstatus;
	private String descripcionEstatus;
	private String claveAccion;
	private PaquetePolizaForm paquetePolizaForm=new PaquetePolizaForm();
	private List<PaquetePolizaDTO> listaPaquetes = new ArrayList<PaquetePolizaDTO>();
	private RespuestaActualizacionPaqueteForm respuestaActualizacionPaqueteForm;
	private List<CoberturaSeccionPaqueteDTO> listaCoberturasPaquete = new ArrayList<CoberturaSeccionPaqueteDTO>();
	private List<CoberturaSeccionDTO> listaCoberturasDisponibles = new ArrayList<CoberturaSeccionDTO>();

	private String claveAplicaFlotillas;
	private String claveAplicaAutoexpedible;
	private String diasGraciaSubsecuentes;
	private String fechaInicioVigencia;
	private String claveProdServ;
	private String validaClaveProd;
	
	
	
	public ActionErrors validate(ActionMapping mapping,HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
//		if(!UtileriasWeb.esCadenaVacia(codigo) && !UtileriasWeb.esCadenaVacia(idToProducto)){
//			List<TipoPolizaDTO> resultado = null;
//			try {
//				resultado = TipoPolizaDN.getInstancia().listarPorIdProducto(UtileriasWeb.regresaBigDecimal(idToProducto), true);
//			} catch (ExcepcionDeAccesoADatos e) {} 
//			catch (SystemException e) {}
//			
//			if(resultado != null && resultado.size() > 0){
//				for(TipoPolizaDTO registro:resultado){
//					if(registro.getCodigo().equals(codigo)){
//						errors.add(
//								"codigo",
//								new ActionMessage(
//										"errors.registro.duplicado",
//										UtileriasWeb
//												.getMensajeRecurso(
//														Sistema.ARCHIVO_RECURSOS,
//														"configuracion.tipopoliza.codigo")));				
//						break;
//					}
//				}
//			}
//			
//		}
		if(!UtileriasWeb.esCadenaVacia(claveAccion) && 
				(claveAccion.equals(PaquetePolizaForm.CLAVE_ACCION_AGREGAR_PAQUETE) ||
				claveAccion.equals(PaquetePolizaForm.CLAVE_ACCION_EDITAR_PAQUETE) )){
			if(UtileriasWeb.esCadenaVacia(paquetePolizaForm.getNombre())){
				errors.add("paquetePolizaForm.nombre", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "configuracion.tipopoliza.paquete.nombre")));
			}
			if(UtileriasWeb.esCadenaVacia(paquetePolizaForm.getOrden())){
				errors.add("paquetePolizaForm.orden", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "configuracion.tipopoliza.paquete.orden")));
			}
			else{
				boolean numeroValido = false;
				try{
					new Integer(paquetePolizaForm.getOrden());
					numeroValido = true;
				}catch(Exception e){
				}
				if(!numeroValido){
					errors.add("paquetePolizaForm.orden", new ActionMessage("errors.integer",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "configuracion.tipopoliza.paquete.orden")));
				}
			}
		}
		else{
			errors = super.validate(mapping, request);
			if (errors == null) {
				errors = new ActionErrors();
			}
		}
		return errors;
	}

	public String getIdToProducto() {
		return idToProducto;
	}

	public void setIdToProducto(String idToProducto) {
		this.idToProducto = idToProducto;
	}

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public String getIdToTipoPoliza() {
		return idToTipoPoliza;
	}

	public void setIdToTipoPoliza(String idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getClaveRenovable() {
		return claveRenovable;
	}

	public void setClaveRenovable(String claveRenovable) {
		this.claveRenovable = claveRenovable;
	}

	public String getClavePagoInmediato() {
		return clavePagoInmediato;
	}

	public void setClavePagoInmediato(String clavePagoInmediato) {
		this.clavePagoInmediato = clavePagoInmediato;
	}

	public String getDiasRetroactividad() {
		return diasRetroactividad;
	}

	public void setDiasRetroactividad(String diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	public String getDiasDiferimiento() {
		return diasDiferimiento;
	}

	public void setDiasDiferimiento(String diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	public String getDiasGracia() {
		return diasGracia;
	}

	public void setDiasGracia(String diasGracia) {
		this.diasGracia = diasGracia;
	}

	/*public String getClaveActivoConfiguracion() {
		return claveActivoConfiguracion;
	}
	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}
	public String getClaveActivoProduccion() {
		return claveActivoProduccion;
	}

	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/
	
	public List<SeccionDTO> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<SeccionDTO> secciones) {
		this.secciones = secciones;
	}

	public List<RamoTipoPolizaDTO> getRamoTipoPolizaAsociados() {
		return ramoTipoPolizaAsociados;
	}

	public void setRamoTipoPolizaAsociados(
			List<RamoTipoPolizaDTO> ramoTipoPolizaAsociados) {
		this.ramoTipoPolizaAsociados = ramoTipoPolizaAsociados;
	}

	public List<RamoDTO> getRamosPorAsociar() {
		return ramosPorAsociar;
	}

	public void setRamosPorAsociar(List<RamoDTO> ramosPorAsociar) {
		this.ramosPorAsociar = ramosPorAsociar;
	}

	public List<RamoDTO> getRamosAsociados() {
		return ramosAsociados;
	}

	public void setRamosAsociados(List<RamoDTO> ramosAsociados) {
		this.ramosAsociados = ramosAsociados;
	}

	public void setRecargosAsociados(
			List<RecargoVarioTipoPolizaDTO> recargosAsociados) {
		this.recargosAsociados = recargosAsociados;
	}

	public List<RecargoVarioTipoPolizaDTO> getRecargosAsociados() {
		return recargosAsociados;
	}

	public void setRecargosPorAsociar(List<RecargoVarioDTO> recargosPorAsociar) {
		this.recargosPorAsociar = recargosPorAsociar;
	}

	public List<RecargoVarioDTO> getRecargosPorAsociar() {
		return recargosPorAsociar;
	}

	public void setAumentosAsociados(
			List<AumentoVarioTipoPolizaDTO> aumentosAsociados) {
		this.aumentosAsociados = aumentosAsociados;
	}

	public List<AumentoVarioTipoPolizaDTO> getAumentosAsociados() {
		return aumentosAsociados;
	}

	public void setAumentosPorAsociar(List<AumentoVarioDTO> aumentosPorAsociar) {
		this.aumentosPorAsociar = aumentosPorAsociar;
	}

	public List<AumentoVarioDTO> getAumentosPorAsociar() {
		return aumentosPorAsociar;
	}

	public List<DescuentoVarioTipoPolizaDTO> getDescuentoTipoPolizaAsociados() {
		return descuentoTipoPolizaAsociados;
	}

	public void setDescuentoTipoPolizaAsociados(
			List<DescuentoVarioTipoPolizaDTO> descuentoTipoPolizaAsociados) {
		this.descuentoTipoPolizaAsociados = descuentoTipoPolizaAsociados;
	}

	public List<DescuentoDTO> getDescuentosPorAsociar() {
		return descuentosPorAsociar;
	}

	public void setDescuentosPorAsociar(List<DescuentoDTO> descuentosPorAsociar) {
		this.descuentosPorAsociar = descuentosPorAsociar;
	}

	public List<ExclusionRecargoVarioTipoPolizaDTO> getExcRecargoTipoPolizaAsociados() {
		return excRecargoTipoPolizaAsociados;
	}

	public void setExcRecargoTipoPolizaAsociados(
			List<ExclusionRecargoVarioTipoPolizaDTO> excRecargoTipoPolizaAsociados) {
		this.excRecargoTipoPolizaAsociados = excRecargoTipoPolizaAsociados;
	}

	public List<ExclusionRecargoVarioTipoPolizaDTO> getExcRecargoTipoPolizaNoAsociados() {
		return excRecargoTipoPolizaNoAsociados;
	}

	public void setExcRecargoTipoPolizaNoAsociados(
			List<ExclusionRecargoVarioTipoPolizaDTO> excRecargoTipoPolizaNoAsociados) {
		this.excRecargoTipoPolizaNoAsociados = excRecargoTipoPolizaNoAsociados;
	}

	public List<ExclusionDescuentoVarioTipoPolizaDTO> getExcDescuentoTipoPolizaAsociados() {
		return excDescuentoTipoPolizaAsociados;
	}

	public void setExcDescuentoTipoPolizaAsociados(
			List<ExclusionDescuentoVarioTipoPolizaDTO> excDescuentoTipoPolizaAsociados) {
		this.excDescuentoTipoPolizaAsociados = excDescuentoTipoPolizaAsociados;
	}

	public List<ExclusionDescuentoVarioTipoPolizaDTO> getExcDescuentoTipoPolizaNoAsociados() {
		return excDescuentoTipoPolizaNoAsociados;
	}

	public void setExcDescuentoTipoPolizaNoAsociados(
			List<ExclusionDescuentoVarioTipoPolizaDTO> excDescuentoTipoPolizaNoAsociados) {
		this.excDescuentoTipoPolizaNoAsociados = excDescuentoTipoPolizaNoAsociados;
	}

	public List<ExclusionAumentoVarioTipoPolizaDTO> getExcAumentoTipoPolizaAsociados() {
		return excAumentoTipoPolizaAsociados;
	}

	public void setExcAumentoTipoPolizaAsociados(
			List<ExclusionAumentoVarioTipoPolizaDTO> excAumentoTipoPolizaAsociados) {
		this.excAumentoTipoPolizaAsociados = excAumentoTipoPolizaAsociados;
	}

	public List<ExclusionAumentoVarioTipoPolizaDTO> getExcAumentoTipoPolizaNoAsociados() {
		return excAumentoTipoPolizaNoAsociados;
	}

	public void setExcAumentoTipoPolizaNoAsociados(
			List<ExclusionAumentoVarioTipoPolizaDTO> excAumentoTipoPolizaNoAsociados) {
		this.excAumentoTipoPolizaNoAsociados = excAumentoTipoPolizaNoAsociados;
	}

	public List<MonedaTipoPolizaDTO> getMonedasAsociadas() {
		return monedasAsociadas;
	}

	public void setMonedasAsociadas(List<MonedaTipoPolizaDTO> monedasAsociadas) {
		this.monedasAsociadas = monedasAsociadas;
	}

	public List<MonedaDTO> getMonedasNoAsociadas() {
		return monedasNoAsociadas;
	}

	public void setMonedasNoAsociadas(List<MonedaDTO> monedasNoAsociadas) {
		this.monedasNoAsociadas = monedasNoAsociadas;
	}

	public String getDescripcionTipoCalculoEmision() {
		return descripcionTipoCalculoEmision;
	}

	public void setDescripcionTipoCalculoEmision(
			String descripcionTipoCalculoEmision) {
		this.descripcionTipoCalculoEmision = descripcionTipoCalculoEmision;
	}

	public String getDescripcionTipoCalculoCancelacion() {
		return descripcionTipoCalculoCancelacion;
	}

	public void setDescripcionTipoCalculoCancelacion(
			String descripcionTipoCalculoCancelacion) {
		this.descripcionTipoCalculoCancelacion = descripcionTipoCalculoCancelacion;
	}

	public String getDescripcionRegistroCNSF() {
		return descripcionRegistroCNSF;
	}

	public void setDescripcionRegistroCNSF(String descripcionRegistroCNSF) {
		this.descripcionRegistroCNSF = descripcionRegistroCNSF;
	}

	public String getIdTipoCalculoEmision() {
		return idTipoCalculoEmision;
	}

	public void setIdTipoCalculoEmision(String idTipoCalculoEmision) {
		this.idTipoCalculoEmision = idTipoCalculoEmision;
	}

	public String getIdTipoCalculoCancelacion() {
		return idTipoCalculoCancelacion;
	}
	public void setIdTipoCalculoCancelacion(String idTipoCalculoCancelacion) {
		this.idTipoCalculoCancelacion = idTipoCalculoCancelacion;
	}
	public String getIdControlArchivoRegistroCNSF() {
		return idControlArchivoRegistroCNSF;
	}
	public void setIdControlArchivoRegistroCNSF(String idControlArchivoRegistroCNSF) {
		this.idControlArchivoRegistroCNSF = idControlArchivoRegistroCNSF;
	}
	public List<CoberturaDTO> getCoberturas() {
		return coberturas;
	}
	public void setCoberturas(List<CoberturaDTO> coberturas) {
		this.coberturas = coberturas;
	}
	public String getClaveTMPCombo() {
		return claveTMPCombo;
	}
	public void setClaveTMPCombo(String claveTMPCombo) {
		this.claveTMPCombo = claveTMPCombo;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	public String getClaveAccion() {
		return claveAccion;
	}
	public PaquetePolizaForm getPaquetePolizaForm() {
		return paquetePolizaForm;
	}
	public void setPaquetePolizaForm(PaquetePolizaForm paquetePolizaForm) {
		this.paquetePolizaForm = paquetePolizaForm;
	}
	public List<PaquetePolizaDTO> getListaPaquetes() {
		return listaPaquetes;
	}
	public void setListaPaquetes(List<PaquetePolizaDTO> listaPaquetes) {
		this.listaPaquetes = listaPaquetes;
	}
	public void setClaveAccion(String claveAccion) {
		this.claveAccion = claveAccion;
	}
	public List<CoberturaSeccionPaqueteDTO> getListaCoberturasPaquete() {
		return listaCoberturasPaquete;
	}
	public void setListaCoberturasPaquete(List<CoberturaSeccionPaqueteDTO> listaCoberturasPaquete) {
		this.listaCoberturasPaquete = listaCoberturasPaquete;
	}
	public List<CoberturaSeccionDTO> getListaCoberturasDisponibles() {
		return listaCoberturasDisponibles;
	}
	public void setListaCoberturasDisponibles(List<CoberturaSeccionDTO> listaCoberturasDisponibles) {
		this.listaCoberturasDisponibles = listaCoberturasDisponibles;
	}
	public RespuestaActualizacionPaqueteForm getRespuestaActualizacionPaqueteForm() {
		return respuestaActualizacionPaqueteForm;
	}
	public void setRespuestaActualizacionPaqueteForm(RespuestaActualizacionPaqueteForm respuestaActualizacionPaqueteForm) {
		this.respuestaActualizacionPaqueteForm = respuestaActualizacionPaqueteForm;
	}

	public String getClaveAplicaFlotillas() {
		return claveAplicaFlotillas;
	}

	public void setClaveAplicaFlotillas(String claveAplicaFlotillas) {
		this.claveAplicaFlotillas = claveAplicaFlotillas;
	}

	public String getClaveAplicaAutoexpedible() {
		return claveAplicaAutoexpedible;
	}

	public void setClaveAplicaAutoexpedible(String claveAplicaAutoexpedible) {
		this.claveAplicaAutoexpedible = claveAplicaAutoexpedible;
	}

	public String getDiasGraciaSubsecuentes() {
		return diasGraciaSubsecuentes;
	}

	public void setDiasGraciaSubsecuentes(String diasGraciaSubsecuentes) {
		this.diasGraciaSubsecuentes = diasGraciaSubsecuentes;
	}
	
	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	public String getClaveProdServ() {
		return claveProdServ;
	}

	public void setClaveProdServ(String claveProdServ) {
		this.claveProdServ = claveProdServ;
	}
	
	public String getValidaClaveProd() {
		return validaClaveProd;
	}

	public void setValidaClaveProd(String validaClaveProd) {
		this.validaClaveProd = validaClaveProd;
	}
	
}
