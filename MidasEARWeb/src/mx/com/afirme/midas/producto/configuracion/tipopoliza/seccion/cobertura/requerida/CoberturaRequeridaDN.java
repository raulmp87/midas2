package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaRequeridaDN {
	private static final CoberturaRequeridaDN INSTANCIA = new CoberturaRequeridaDN();

	public static CoberturaRequeridaDN getInstancia() {
		return CoberturaRequeridaDN.INSTANCIA;
	}

	public List<CoberturaRequeridaDTO> buscarPorCoberturaBase(BigDecimal idToCoberturaBase)
			throws SystemException, ExcepcionDeAccesoADatos {
		CoberturaRequeridaSN coberturaSN = new CoberturaRequeridaSN();
		return coberturaSN.buscarPorPropiedad("id.idToCobertura", idToCoberturaBase);
	}

	public List<CoberturaRequeridaDTO> buscarPorCoberturaRequerida(BigDecimal idToCobertura)throws SystemException, ExcepcionDeAccesoADatos {
		return new CoberturaRequeridaSN().buscarPorPropiedad("id.idToCoberturaRequerida", idToCobertura);
	}
	
	public Boolean esCoberturaRequerida(BigDecimal idToCobertura) throws SystemException, ExcepcionDeAccesoADatos {
		List<CoberturaRequeridaDTO> listaCoberturasBase = new CoberturaRequeridaSN().buscarPorPropiedad("id.idToCoberturaRequerida", idToCobertura);
		if (listaCoberturasBase != null && !listaCoberturasBase.isEmpty())
			return Boolean.TRUE;
		else
			return Boolean.FALSE;
	}
}
