package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasiva;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * @author Fernando Alonzo
 * @since 21 de agosto de 2009
 */
public class DeducibleCoberturaAction extends CoberturaAction {

	/**
	 * Method asociarDescuento
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDeducibleCobertura(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id != null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		String idSeccion = request.getParameter("idPadre");
		coberturaForm.setIdToSeccion(idSeccion);
		//Valida deducible cero para Responsabilidad Civil
		try {
			coberturaForm.setPermiteDeducibleCero("0");
			CoberturaDTO coberturaValida = null;
			CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
			coberturaValida = coberturaDN.getPorId(coberturaDTO.getIdToCobertura());
			if(coberturaValida != null){
				if(coberturaValida.getNombreComercial().equals(GeneraExcelCargaMasiva.NOMBRE_LIMITE_RC_TERCEROS.toUpperCase())){
					coberturaForm.setPermiteDeducibleCero("1");
				}
			}
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {
		}
		
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarDescuentoAsociado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarDeducibles(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		DeducibleCoberturaDN deducibleCoberturaDN = DeducibleCoberturaDN.getInstancia();
		String action = "";
		DeducibleCoberturaId id = new DeducibleCoberturaId();
		BigDecimal numeroSecuencia = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idCobertura"));
		id.setNumeroSecuencia(numeroSecuencia);
		id.setIdToCobertura(idToCobertura);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				DeducibleCoberturaDTO deducibleCoberturaDTO = new DeducibleCoberturaDTO();
				deducibleCoberturaDTO.setId(id);
				deducibleCoberturaDTO.setClaveDefault(Short.parseShort(request.getParameter("claveDefault")));
				deducibleCoberturaDTO.setValor(Double.parseDouble(request.getParameter("valor")));

				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					action = "insert";
					//Busca duplicados
					List<DeducibleCoberturaDTO> list = deducibleCoberturaDN.listarDeducibles(idToCobertura);
					for(DeducibleCoberturaDTO item : list){
						if(item.getValor().equals(deducibleCoberturaDTO.getValor())){
							response.setContentType("text/xml");
							PrintWriter pw = response.getWriter();
							pw.write("<data><action type=\"duplicateValue\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
							pw.flush();
							pw.close();
							return null;
						}
					}
					//Numero de secuencia de db
					numeroSecuencia = coberturaDN.nextNumeroSecuenciaDeducible(idToCobertura);
					id.setNumeroSecuencia(numeroSecuencia);
					deducibleCoberturaDTO.setId(id);
					coberturaDN.agregarDeducible(coberturaDTO, deducibleCoberturaDTO);
					mensajeExitoAgregar(coberturaForm, request);
				} else {
					action = "update";
					coberturaDN.actualizarDeducible(coberturaDTO, deducibleCoberturaDTO);
					mensajeExitoModificar(coberturaForm, request);
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				action = "deleted";
				DeducibleCoberturaDTO deducibleCoberturaDTO = new DeducibleCoberturaDTO();
				deducibleCoberturaDTO.setId(id);
				coberturaDN.eliminarDeducible(coberturaDTO, deducibleCoberturaDTO);
				mensajeExitoBorrar(coberturaForm, request);
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(coberturaForm, request, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mensajeExcepcion(coberturaForm, request, e);
		}catch (Exception e){
				response.setContentType("text/xml");
				PrintWriter pw = response.getWriter();
				pw.write("<data><action type=\"excepcionJava\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
				pw.flush();
				pw.close();
				return null;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarDeducibles(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));
		
		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaDTO = coberturaDN.getPorIdCascada(coberturaDTO);
		
		coberturaForm.setDeducibles(coberturaDTO.getDeducibles());

//		String json = "{rows:[";
//		if(coberturaForm.getDeducibles() != null && coberturaForm.getDeducibles().size() > 0) {
//			for(DeducibleCoberturaDTO deducible : coberturaForm.getDeducibles()) {
//				json += "{id:\"" + deducible.getId().getNumeroSecuencia() + "\",data:[0,";
//				json += deducible.getId().getIdToCobertura()+ ",0,\"";
//				json += deducible.getId().getNumeroSecuencia() + "\",";
//				json += deducible.getValor() + ",";
//				json += deducible.getClaveDefault() + "]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getDeducibles() != null && coberturaForm.getDeducibles().size() > 0) {
			for(DeducibleCoberturaDTO deducible : coberturaForm.getDeducibles()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(deducible.getId().getNumeroSecuencia().toString());
				row.setDatos(
						"0",
						deducible.getId().getIdToCobertura().toString(),
						"0",
						deducible.getId().getNumeroSecuencia().toString(),
						deducible.getValor().toString(),
						deducible.getClaveDefault().toString()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
