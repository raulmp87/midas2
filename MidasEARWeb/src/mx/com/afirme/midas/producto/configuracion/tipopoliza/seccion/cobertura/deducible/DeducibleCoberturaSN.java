package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DeducibleCoberturaSN {
	/**
	 * @author Fernando Alonzo
	 * @since 21 de Agosto de 2009
	 */

	private DeducibleCoberturaFacadeRemote beanRemoto;

	public DeducibleCoberturaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(DeducibleCoberturaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(DeducibleCoberturaDTO deducibleCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(deducibleCoberturaDTO);
		return null;
	}

	public String actualizar(DeducibleCoberturaDTO deducibleCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(deducibleCoberturaDTO);
		return null;
	}

	public String borrar(DeducibleCoberturaDTO deducibleCoberturaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(deducibleCoberturaDTO);
		return null;
	}

	public List<DeducibleCoberturaDTO> listarDeducibles(
			BigDecimal idToCobertura) {
		return beanRemoto.listarDeducibles(idToCobertura);
	}

	public List<DeducibleCoberturaDTO> listarDeduciblesPorCobertura(BigDecimal idToCobertura) {
		return beanRemoto.listarDeduciblesPorCobertura(idToCobertura);
	}
	
	public BigDecimal nextNumeroSecuencia(BigDecimal idToCobertura) {
		return beanRemoto.nextNumeroSecuencia(idToCobertura);
	}		
}
