package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoDN {
	private static final RiesgoDN INSTANCIA = new RiesgoDN();

	public static RiesgoDN getInstancia() {
		return RiesgoDN.INSTANCIA;
	}

	public List<RiesgoDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoSN().listarTodos();
	}

	public void agregar(RiesgoDTO riesgoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RiesgoSN().agregar(riesgoDTO);
	}

	public void modificar(RiesgoDTO riesgoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new RiesgoSN().modificar(riesgoDTO);
	}

	public RiesgoDTO getPorId(RiesgoDTO riesgoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoSN().getPorId(riesgoDTO.getIdToRiesgo());
	}
	
	public RiesgoDTO getPorId(BigDecimal idToRiesgo) throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoSN().getPorId(idToRiesgo);
	}

	public void borrar(RiesgoDTO riesgoDTO) throws SystemException,	ExcepcionDeAccesoADatos {
		new RiesgoSN().borrar(riesgoDTO);
	}

	public List<RiesgoDTO> buscarPorPropiedad(String propiedad, Object valor)
			throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoSN().buscarPorPropiedad(propiedad, valor);
	}

	public List<RiesgoDTO> listarFiltrado(RiesgoDTO riesgoDTO, boolean mostrarInactivos)
		throws SystemException, ExcepcionDeAccesoADatos {
			return new RiesgoSN().listarFiltrado(riesgoDTO, mostrarInactivos);
	}
	
	public List<RiesgoDTO> getListarFiltrado(RiesgoDTO riesgoDTO, boolean mostrarInactivos)
		throws SystemException, ExcepcionDeAccesoADatos {
			RiesgoSN riesgoSN = new RiesgoSN();
			return riesgoSN.getListarFiltrado(riesgoDTO, mostrarInactivos);
	}

	public List<RiesgoDTO>listarVigentesAutos() throws SystemException, ExcepcionDeAccesoADatos {
		return (new RiesgoSN().listarVigentesAutos());
	}
	
	public void borradoLogico(RiesgoDTO riesgoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		new RiesgoSN().borradoLogico(riesgoDTO);
	}
	
	public List<RiesgoDTO>listarVigentes() throws SystemException, ExcepcionDeAccesoADatos {
		return (new RiesgoSN().listarVigentes());
	}
	
	public List<CoaseguroRiesgoCoberturaDTO> listarCoasegurosPorRiesgoCoberturaSeccion(BigDecimal idToRiesgo,BigDecimal IdToCobertura,BigDecimal idToSeccion) throws SystemException{
		return new CoaseguroRiesgoCoberturaSN().buscarPorIdRiesgoIdCoberturaIdSeccion(idToRiesgo, IdToCobertura, idToSeccion);
	}
	
	public List<DeducibleRiesgoCoberturaDTO> listarDeduciblesPorRiesgoCoberturaSeccion(BigDecimal idToRiesgo,BigDecimal IdToCobertura,BigDecimal idToSeccion) throws SystemException{
		return new DeducibleRiesgoCoberturaSN().buscarPorIdRiesgoIdCoberturaIdSeccion(idToRiesgo, IdToCobertura, idToSeccion);
	}
	
	public List<RiesgoDTO> listarRiesgosVigentesPorCoberturaSeccion(BigDecimal IdToCobertura,BigDecimal idToSeccion) throws SystemException{
		return new RiesgoSN().listarRiesgosVigentesPorCoberturaSeccion(IdToCobertura, idToSeccion);
	}

	public Boolean isCoberturaAsociada(RiesgoDTO riesgoDTO) throws SystemException {
		return new RiesgoSN().isCoberturaAsociada(riesgoDTO);
	}

	public Boolean isTarifaAsociada(RiesgoDTO riesgoDTO) throws SystemException {
		return new RiesgoSN().isTarifaAsociada(riesgoDTO);
	}
}
