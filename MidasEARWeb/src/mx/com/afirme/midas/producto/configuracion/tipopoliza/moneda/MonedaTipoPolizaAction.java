package mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MonedaTipoPolizaAction extends TipoPolizaAction {
	/**
	 * Method mostrarAsociarMoneda
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarMoneda(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String idToTipoPoliza = request.getParameter("id");
		if (idToTipoPoliza != null)
			tipoPolizaForm.setIdToTipoPoliza(idToTipoPoliza);
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarMonedasAsociadas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void mostrarMonedasAsociadas(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id");
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
		TipoPolizaDN tipoPolizaDN = TipoPolizaDN.getInstancia();
		tipoPolizaForm.setMonedasAsociadas(tipoPolizaDN.listarMonedasAsociadas(tipoPolizaDTO));
		MonedaDN monedaDN = MonedaDN.getInstancia();
//		String json = "{rows:[";
//		if(tipoPolizaForm.getMonedasAsociadas() != null && tipoPolizaForm.getMonedasAsociadas().size() > 0) {
//			for(MonedaTipoPolizaDTO moneda : tipoPolizaForm.getMonedasAsociadas()) {
//				json += "{id:\"" + moneda.getId().getIdMoneda() + "\",data:[";
//				json += id + ",\"";
//				json += monedaDN.getPorId(Short.valueOf(Short.valueOf(moneda.getId().getIdMoneda().toString()))).getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getMonedasAsociadas() != null && tipoPolizaForm.getMonedasAsociadas().size() > 0) {
			for(MonedaTipoPolizaDTO moneda : tipoPolizaForm.getMonedasAsociadas()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(moneda.getId().getIdMoneda().toString());
				row.setDatos(
						id,
						monedaDN.getPorId(Short.valueOf(Short.valueOf(moneda.getId().getIdMoneda().toString()))).getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarMonedaAsociada
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarMonedaAsociada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoPolizaDN tipoPolizaDN = TipoPolizaDN.getInstancia();

		MonedaTipoPolizaId id = new MonedaTipoPolizaId();
		id.setIdMoneda(UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id")));
		id.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(request.getParameter("idPadre")));
		String action = "";
		try {
			MonedaTipoPolizaDTO monedaPoliza = new MonedaTipoPolizaDTO();
			monedaPoliza.setId(id);
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					tipoPolizaDN.asociarMoneda(monedaPoliza);
					action = "insert";
				} else {
					tipoPolizaDN.actualizarAsociacionMoneda(monedaPoliza);
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				tipoPolizaDN.desasociarMoneda(monedaPoliza);
				action = "delete";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarMonedasPorAsociar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public void mostrarMonedasPorAsociar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		TipoPolizaForm tipoPolizaForm = (TipoPolizaForm) form;
		String id = request.getParameter("id");
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		TipoPolizaDN tipoPolizaDN =  TipoPolizaDN.getInstancia();
		tipoPolizaDTO.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
		tipoPolizaDTO = tipoPolizaDN.getPorId(tipoPolizaDTO);

		tipoPolizaForm.setMonedasNoAsociadas(tipoPolizaDN.listarMonedasNoAsociadas(tipoPolizaDTO));
//		String json = "{rows:[";
//		if(tipoPolizaForm.getMonedasNoAsociadas() != null && tipoPolizaForm.getMonedasNoAsociadas().size() > 0) {
//			for(MonedaDTO moneda : tipoPolizaForm.getMonedasNoAsociadas()) {
//				json += "{id:\"" + moneda.getIdTcMoneda() + "\",data:[";
//				json += id + ",\"";
//				json += moneda.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(tipoPolizaForm.getMonedasNoAsociadas() != null && tipoPolizaForm.getMonedasNoAsociadas().size() > 0) {
			for(MonedaDTO moneda : tipoPolizaForm.getMonedasNoAsociadas()) {
				MidasJsonRow row = new MidasJsonRow();
				row.setId(moneda.getIdTcMoneda().toString());
				row.setDatos(
						id,
						moneda.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
