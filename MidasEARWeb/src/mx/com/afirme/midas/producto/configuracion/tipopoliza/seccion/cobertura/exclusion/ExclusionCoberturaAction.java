package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * @author Fernando Alonzo
 * @since 19 de agosto de 2009
 */
public class ExclusionCoberturaAction extends CoberturaAction {

	/**
	 * Method asociarCoberturasExcluidas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward asociarCoberturasExcluidas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaForm coberturaForm = (CoberturaForm) form;
		String id = request.getParameter("id");
		if (id!= null)
			coberturaForm.setIdToCobertura(id);
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		this.poblarDTO(coberturaForm, coberturaDTO);
		coberturaForm.setIdToCobertura(coberturaDTO.getIdToCobertura().toString());
		HttpSession session = request.getSession();
		session.setAttribute("idToSeccion", request.getParameter("idToSeccion"));
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method guardarCoberturasExcluidas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public ActionForward guardarCoberturasExcluidas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		String action = "";
		CoberturaExcluidaId id = new CoberturaExcluidaId();
		BigDecimal idToCoberturaBase = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCoberturaBase"));
		BigDecimal idToCoberturaExcluida = UtileriasWeb.regresaBigDecimal(request.getParameter("gr_id"));
		id.setIdToCobertura(idToCoberturaBase);
		id.setIdToCoberturaExcluida(idToCoberturaExcluida);
		try {
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {
				CoberturaExcluidaDTO coberturaExcluidaDTO = new CoberturaExcluidaDTO();
				coberturaExcluidaDTO.setId(id);

				if(request.getParameter("!nativeeditor_status").equals("inserted")) {
					coberturaDN.excluyeCobertura(coberturaExcluidaDTO);
					action = "insert";
				} else {
					action = "update";
				}
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				CoberturaExcluidaDTO coberturaExcluidaDTO = new CoberturaExcluidaDTO();
				coberturaExcluidaDTO.setId(id);
				coberturaDN.borrarExclusionCobertura(coberturaExcluidaDTO);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public void mostrarCoberturasExcluidas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;

		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN =  CoberturaDN.getInstancia();
		coberturaDTO = coberturaDN.getPorIdCascada(coberturaDTO);
		coberturaForm.setCoberturasExcluidas(coberturaDTO.getCoberturasExcluidas());

//		String json = "{rows:[";
//		if(coberturaForm.getCoberturasExcluidas() != null && coberturaForm.getCoberturasExcluidas().size() > 0) {
//			for(CoberturaExcluidaDTO coberturaExcluida : coberturaForm.getCoberturasExcluidas()) {
//				json += "{id:\"" + coberturaExcluida.getId().getIdToCoberturaExcluida() + "\",data:[\"";
//				json += coberturaExcluida.getId().getIdToCobertura() + "\",\"";
//				json += coberturaExcluida.getCoberturaExcluidaDTO().getCodigo() + "\",\"";
//				json += coberturaExcluida.getCoberturaExcluidaDTO().getNombreComercial() + "\",\"";
//				json += coberturaExcluida.getCoberturaExcluidaDTO().getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getCoberturasExcluidas() != null && coberturaForm.getCoberturasExcluidas().size() > 0) {
			for(CoberturaExcluidaDTO coberturaExcluida : coberturaForm.getCoberturasExcluidas()) {
				CoberturaDTO coberturaDTOExcluida = coberturaDN.getPorId(coberturaExcluida.getId().getIdToCoberturaExcluida());
				MidasJsonRow row = new MidasJsonRow();
				row.setId(coberturaExcluida.getId().getIdToCoberturaExcluida().toString());
				row.setDatos(
						coberturaExcluida.getId().getIdToCobertura().toString(),
						coberturaDTOExcluida.getCodigo(),
						coberturaDTOExcluida.getNombreComercial(),
						coberturaDTOExcluida.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void mostrarCoberturasPorExcluir(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CoberturaForm coberturaForm = (CoberturaForm) form;
		
		HttpSession session = request.getSession();
		String idToSeccion = session.getAttribute("idToSeccion").toString();
		session.removeAttribute("idToSeccion");
		
		String id = request.getParameter("id");
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(UtileriasWeb.regresaBigDecimal(id));

		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		coberturaForm.setCoberturasPorExcluir(coberturaDN.listarCoberturasPorExcluir(coberturaDTO, UtileriasWeb.regresaBigDecimal(idToSeccion)));

//		String json = "{rows:[";
//		if(coberturaForm.getCoberturasPorExcluir() != null && coberturaForm.getCoberturasPorExcluir().size() > 0) {
//			for(CoberturaDTO cobertura : coberturaForm.getCoberturasPorExcluir()) {
//				json += "{id:\"" + cobertura.getIdToCobertura() + "\",data:[\"";
//				json += coberturaDTO.getIdToCobertura() + "\",\"";
//				json += cobertura.getCodigo() + "\",\"";
//				json += cobertura.getNombreComercial() + "\",\"";
//				json += cobertura.getDescripcion() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(coberturaForm.getCoberturasPorExcluir() != null && coberturaForm.getCoberturasPorExcluir().size() > 0) {
			for(CoberturaDTO cobertura : coberturaForm.getCoberturasPorExcluir()) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(cobertura.getIdToCobertura().toString());
				row.setDatos(
						coberturaDTO.getIdToCobertura().toString(),
						cobertura.getCodigo(),
						cobertura.getNombreComercial(),
						cobertura.getDescripcion()
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
}
