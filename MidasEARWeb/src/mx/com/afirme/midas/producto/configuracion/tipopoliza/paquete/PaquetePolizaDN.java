package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * 
 * @author jose luis arellano
 * @since 15/04/2011
 */
public class PaquetePolizaDN {
	
	public static String[] ATRIBUTOS_MAPEO_DTO = {
		"idToPaquetePoliza",
		"tipoPolizaDTO.idToTipoPoliza",
		"nombre",
		"descripcion",
		"orden",
		"coberturaSeccionPaqueteDTOs"
		};
	
	public static String[] ATRIBUTOS_MAPEO_FORM = {
		"idToPaquetePoliza",
		"idToTipoPoliza",
		"nombre",
		"descripcion",
		"orden",
		"coberturaSeccionPaqueteDTOs"
		};

	private static final PaquetePolizaDN INSTANCIA = new PaquetePolizaDN();

	private PaquetePolizaDN(){
		
	}
	
	public static PaquetePolizaDN getInstancia (){
		return PaquetePolizaDN.INSTANCIA;
	}
	
	public PaquetePolizaDTO agregar(PaquetePolizaDTO paquetePolizaDTO) throws SystemException{
		return new PaquetePolizaSN().agregar(paquetePolizaDTO);
	}
	
	public List<PaquetePolizaDTO> buscarPorPropiedad (String propiedad, Object valor,boolean poblarCoberturas) throws SystemException{
		return new PaquetePolizaSN().buscarPorPropiedad(propiedad, valor,poblarCoberturas);
	}
	
	public List<PaquetePolizaDTO> listarPorTipoPoliza(BigDecimal idToTipoPoliza,boolean poblarCoberturas) throws SystemException{
		return new PaquetePolizaSN().listarPorTipoPoliza(idToTipoPoliza, poblarCoberturas);
	}
	
	public PaquetePolizaDTO modificar (PaquetePolizaDTO paquetePolizaDTO) throws SystemException{
		return new PaquetePolizaSN().modificar(paquetePolizaDTO);
	}
	
	public PaquetePolizaDTO getPorId(BigDecimal idToPaquetePoliza) throws SystemException{
		return new PaquetePolizaSN().getPorId(idToPaquetePoliza);
	}
	
	public List<CoberturaSeccionDTO> obtenerCoberturasDisponibles(BigDecimal idToPaquetePoliza) throws SystemException{
		return new PaquetePolizaSN().obtenerCoberturasDisponibles(idToPaquetePoliza);
	}
	
	public List<CoberturaSeccionPaqueteDTO> obtenerCoberturasRegistradas(BigDecimal idToPaquetePoliza) throws SystemException{
		return new PaquetePolizaSN().obtenerCoberturasRegistradas(idToPaquetePoliza);
	}
	
	public ActualizacionCoberturaPaqueteDTO registrarCoberturaPaquete(BigDecimal idToPaquetePoliza,BigDecimal idToCoberturaSeccionPaquete,
			BigDecimal idToSeccion,BigDecimal idToCobertura,BigDecimal montoSumaAsegurada,Short claveContrato) throws SystemException{
		return new PaquetePolizaSN().registrarCoberturaPaquete(idToPaquetePoliza, idToCoberturaSeccionPaquete, 
				idToSeccion, idToCobertura, montoSumaAsegurada, claveContrato);
	}
	
	public ActualizacionCoberturaPaqueteDTO liberarPaquete(BigDecimal idToPaquete) throws SystemException{
		return new PaquetePolizaSN().liberarPaquete(idToPaquete);
	}
	
	public ActualizacionCoberturaPaqueteDTO eliminarPaquete(BigDecimal idToPaquete) throws SystemException{
		return new PaquetePolizaSN().eliminarPaquete(idToPaquete);
	}
	
	public PaquetePolizaDTO establecerPaqueteDefault(BigDecimal idToPaquetePoliza) throws SystemException{
		return new PaquetePolizaSN().establecerPaqueteDefault(idToPaquetePoliza);
	}
	
//	public List<PaquetePolizaDTO> listarFiltrado(PaquetePolizaDTO paquetePolizaDTO) throws ExcepcionDeAccesoADatos, SystemException{
//		return new PaquetePolizaSN().listarFiltrado(paquetePolizaDTO);
//	}
}
