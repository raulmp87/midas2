package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jos� Luis Arellano
 * @since 21 de Agosto de 2009
 */
public class CoaseguroRiesgoCoberturaDN {
	private static final CoaseguroRiesgoCoberturaDN INSTANCIA = new CoaseguroRiesgoCoberturaDN();

	public static CoaseguroRiesgoCoberturaDN getInstancia() {
		return CoaseguroRiesgoCoberturaDN.INSTANCIA;
	}

	public List<CoaseguroRiesgoCoberturaDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new CoaseguroRiesgoCoberturaSN().listarTodos();
	}

	public void agregar(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new CoaseguroRiesgoCoberturaSN().agregar(coaseguroRiesgoCoberturaDTO);
	}

	public void modificar(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new CoaseguroRiesgoCoberturaSN().modificar(coaseguroRiesgoCoberturaDTO);
	}

	public CoaseguroRiesgoCoberturaDTO getPorId(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new CoaseguroRiesgoCoberturaSN().getPorId(coaseguroRiesgoCoberturaDTO.getId());
	}

	public void borrar(CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new CoaseguroRiesgoCoberturaSN().borrar(coaseguroRiesgoCoberturaDTO);
	}

	public void sincronizarCoasegurosRiesgoCobertura(BigDecimal idToRiesgo,
			BigDecimal idToCobertura, BigDecimal idToSeccion) throws SystemException {
		new CoaseguroRiesgoCoberturaSN().sincronizarCoasegurosRiesgoCobertura(idToRiesgo, idToCobertura, idToSeccion);
	}

	public List<CoaseguroRiesgoCoberturaDTO> listarFiltrado(
			CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO) throws SystemException {
		return new CoaseguroRiesgoCoberturaSN().listarFiltrado(coaseguroRiesgoCoberturaDTO);
	}
}
