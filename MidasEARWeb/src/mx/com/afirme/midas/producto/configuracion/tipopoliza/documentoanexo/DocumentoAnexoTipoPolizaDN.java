package mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo;

import java.math.BigDecimal;
import java.util.List;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class DocumentoAnexoTipoPolizaDN {
	private static final DocumentoAnexoTipoPolizaDN INSTANCIA = new DocumentoAnexoTipoPolizaDN();

	public static DocumentoAnexoTipoPolizaDN getInstancia() {
		return DocumentoAnexoTipoPolizaDN.INSTANCIA;
	}

	public List<DocumentoAnexoTipoPolizaDTO> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoTipoPolizaSN().listarTodos();
	}

	public void agregar(DocumentoAnexoTipoPolizaDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		new DocumentoAnexoTipoPolizaSN().agregar(documentoAnexoproductoDTO);
	}

	public void modificar(DocumentoAnexoTipoPolizaDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoTipoPolizaSN().modificar(documentoAnexoproductoDTO);
	}

	public DocumentoAnexoTipoPolizaDTO getPorId(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		return new DocumentoAnexoTipoPolizaSN().getPorId(id);
	}

	public void borrar(DocumentoAnexoTipoPolizaDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoTipoPolizaSN().borrar(documentoAnexoproductoDTO);
	}
	
	public List<DocumentoAnexoTipoPolizaDTO> listarPorPropiedad(String propiedad, Object valor) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoTipoPolizaSN().encontrarPorPropiedad(propiedad, valor);
	}
	
	public List<DocumentoAnexoTipoPolizaDTO> listarPorIdTipoPoliza(BigDecimal idToTipoPoliza) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoTipoPolizaSN().encontrarPorPropiedad("tipoPolizaDTO.idToTipoPoliza",idToTipoPoliza);
	}
}
