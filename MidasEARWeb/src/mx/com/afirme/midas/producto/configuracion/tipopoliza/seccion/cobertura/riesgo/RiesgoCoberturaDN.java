package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoCoberturaDN {
	private static final RiesgoCoberturaDN INSTANCIA = new RiesgoCoberturaDN();

	public static RiesgoCoberturaDN getInstancia() {
		return RiesgoCoberturaDN.INSTANCIA;
	}

	public List<RiesgoCoberturaDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		return riesgoCoberturaSN.listarTodos();
	}

	public void agregar(RiesgoCoberturaDTO riesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		riesgoCoberturaSN.agregar(riesgoCoberturaDTO);
	}

	public void modificar(RiesgoCoberturaDTO riesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		riesgoCoberturaSN.modificar(riesgoCoberturaDTO);
	}

	public RiesgoCoberturaDTO getPorId(RiesgoCoberturaDTO riesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		return riesgoCoberturaSN.getPorId(riesgoCoberturaDTO.getId());
	}

	public void borrar(RiesgoCoberturaDTO riesgoCoberturaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		riesgoCoberturaSN.borrar(riesgoCoberturaDTO);
	}
	
	public List<RiesgoCoberturaDTO> buscarPorPropiedad (String propiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {	
		return new RiesgoCoberturaSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<RiesgoCoberturaDTO> listarRiesgoAsociado(BigDecimal idToCobertura,BigDecimal idToSeccion)throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoCoberturaSN().listarRiesgoAsociado(idToCobertura, idToSeccion);
	}
	
	public List<RiesgoCoberturaDTO> listarRiesgoAsociadoId(BigDecimal idToCobertura,BigDecimal idToSeccion)throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoCoberturaSN().listarRiesgoAsociado(idToCobertura, idToSeccion);
	}
	
	public List<RiesgoCoberturaDTO> listarRiesgoVigenteAsociado(BigDecimal idToCobertura,BigDecimal idToSeccion)throws SystemException, ExcepcionDeAccesoADatos {
		return new RiesgoCoberturaSN().listarRiesgoVigenteAsociado(idToCobertura, idToSeccion);
	}

	public List<BigDecimal> obtenerRiesgosPorSeccionCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion) throws SystemException {
		return new RiesgoCoberturaSN().obtenerRiesgosPorSeccionCotizacion(idToCotizacion, numeroInciso, idToSeccion);
	}

	public void borrarARDs(RiesgoCoberturaDTO riesgoCoberturaDTO) throws SystemException {
		new RiesgoCoberturaSN().borrarARDs(riesgoCoberturaDTO);
	}
}
