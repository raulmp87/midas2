package mx.com.afirme.midas.producto.documentoanexo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoAnexoProductoSN {
	private DocumentoAnexoProductoFacadeRemote beanRemoto;

	public DocumentoAnexoProductoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoAnexoProductoFacadeRemote.class);
		} catch (Exception e) { throw new SystemException(Sistema.NO_DISPONIBLE);}
		LogDeMidasWeb.log("bean Remoto DocumentoAnexoProducto instanciado", Level.FINEST, null);
	}

	public List<DocumentoAnexoProductoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}

	public void agregar(DocumentoAnexoProductoDTO ajustadorDTO)throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.save(ajustadorDTO);
		} catch(Exception e){ throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e); }
	}

	public void modificar(DocumentoAnexoProductoDTO ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.update(ajustadorDTO);
		}catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public DocumentoAnexoProductoDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findById(id);
		}catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO); }
	}

	public void borrar(DocumentoAnexoProductoDTO ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try{ beanRemoto.delete(ajustadorDTO);
		}catch(EJBTransactionRolledbackException e){ throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName()); }
	}
	
	public List<DocumentoAnexoProductoDTO> encontrarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos {
		try{ return beanRemoto.findByProperty(propiedad, valor);
		}catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
}
