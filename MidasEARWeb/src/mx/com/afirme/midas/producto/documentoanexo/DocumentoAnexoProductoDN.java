package mx.com.afirme.midas.producto.documentoanexo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class DocumentoAnexoProductoDN {
	private static final DocumentoAnexoProductoDN INSTANCIA = new DocumentoAnexoProductoDN();

	public static DocumentoAnexoProductoDN getInstancia() {
		return DocumentoAnexoProductoDN.INSTANCIA;
	}

	public List<DocumentoAnexoProductoDTO> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoProductoSN().listarTodos();
	}

	public void agregar(DocumentoAnexoProductoDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		new DocumentoAnexoProductoSN().agregar(documentoAnexoproductoDTO);
	}

	public void modificar(DocumentoAnexoProductoDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoProductoSN().modificar(documentoAnexoproductoDTO);
	}

	public DocumentoAnexoProductoDTO getPorId(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		return new DocumentoAnexoProductoSN().getPorId(id);
	}

	public void borrar(DocumentoAnexoProductoDTO documentoAnexoproductoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new DocumentoAnexoProductoSN().borrar(documentoAnexoproductoDTO);
	}
	
	public List<DocumentoAnexoProductoDTO> listarPorPropiedad(String propiedad, Object valor) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoProductoSN().encontrarPorPropiedad(propiedad, valor);
	}
	
	public List<DocumentoAnexoProductoDTO> listarPorIdProducto(BigDecimal idToProducto) throws SystemException,ExcepcionDeAccesoADatos {
		return new DocumentoAnexoProductoSN().encontrarPorPropiedad("productoDTO.idToProducto", idToProducto);
	}
}
