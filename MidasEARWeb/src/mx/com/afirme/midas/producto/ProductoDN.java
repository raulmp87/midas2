package mx.com.afirme.midas.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioSN;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoSN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoSN;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioSN;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoId;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.descuento.exclusion.ExclusionDescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.exclusion.ExclusionDescuentoVarioProductoId;
import mx.com.afirme.midas.producto.configuracion.descuento.exclusion.ExclusionDescuentoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.moneda.MonedaProductoDTO;
import mx.com.afirme.midas.producto.configuracion.moneda.MonedaProductoSN;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoSN;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoId;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDN;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class ProductoDN {
	private static final ProductoDN INSTANCIA = new ProductoDN();

	public static ProductoDN getInstancia() {
		return ProductoDN.INSTANCIA;
	}

	public List<ProductoDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();
		return productoSN.listarTodos();
	}

	public void agregar(ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ProductoSN productoSN = new ProductoSN();
		productoSN.agregar(productoDTO);
	}

	public void modificar(ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();
		productoSN.modificar(productoDTO);
	}

	public ProductoDTO encontrarPorSolicitud(BigDecimal idToSolicitud)
			throws SystemException, ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();

		return productoSN.encontrarPorSolicitud(idToSolicitud);
	}

	public ProductoDTO getPorId(ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();

		return productoSN.getPorId(productoDTO.getIdToProducto());
	}

	public ProductoDTO getPorIdCascada(ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();
		ProductoDTO producto = productoSN.getPorId(productoDTO
				.getIdToProducto());
		TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
		List<TipoPolizaDTO> tiposPoliza = tipoPolizaSN
				.listarVigentesPorIdProducto(producto.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + producto.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		DescuentoVarioProductoSN descuentoPorProductoSN = new DescuentoVarioProductoSN();
		List<DescuentoVarioProductoDTO> descuentos = descuentoPorProductoSN
				.listarDescuentoAsociado(productoDTO.getIdToProducto());
		producto.setDescuentos(descuentos);
		producto.setTiposPoliza(tiposPoliza);

		RecargoVarioProductoSN recargoPorProductoSN = new RecargoVarioProductoSN();
		List<RecargoVarioProductoDTO> recargos = recargoPorProductoSN
				.listarRecargoAsociado(productoDTO.getIdToProducto());
		producto.setRecargos(recargos);

		AumentoVarioProductoSN aumentoVarioProductoSN = new AumentoVarioProductoSN();
		List<AumentoVarioProductoDTO> aumentos = aumentoVarioProductoSN
				.listarAumentoAsociado(productoDTO.getIdToProducto());
		producto.setAumentos(aumentos);

		MonedaProductoSN monedaProductoSN = new MonedaProductoSN();
		productoDTO.setMonedas(monedaProductoSN.buscarPorPropiedad(
				"id.idToProducto", productoDTO.getIdToProducto()));

		return producto;
	}

	/**
	 * Regresa la entidad ProductoDTO con la lista List<TipoPolizaDTO>
	 * tiposPoliza poblada
	 * 
	 * @param productoDTO
	 * @return productoDTO.
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public ProductoDTO poblarPolizasVigentes(ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		if (productoDTO != null) {
			/*
			 * ProductoSN productoSN = new ProductoSN(); ProductoDTO producto =
			 * productoSN.getPorId(productoDTO.getIdToProducto());
			 */
			TipoPolizaSN tipoPolizaSN = new TipoPolizaSN();
			/*
			 * S�LO SE DEBEN LISTAR LOS TIPOPOLIZA QUE NO HAYAN SIDO BORRADOS
			 * LOGICAMENTE. List<TipoPolizaDTO> tiposPoliza =
			 * tipoPolizaSN.buscarPorPropiedad("productoDTO.idToProducto",
			 * producto.getIdToProducto());
			 */
			List<TipoPolizaDTO> tiposPoliza = tipoPolizaSN
					.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
			LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
					+ " Numero de polizas VIGENTES asociadas ==> "
					+ tiposPoliza.size(), Level.INFO, null);
			productoDTO.setTiposPoliza(tiposPoliza);
		}
		return productoDTO;
	}

	public void borrar(ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();
		productoSN.borrar(productoDTO);
	}

	public ProductoDTO getPorIdHijo(BigDecimal id) throws SystemException,
			ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();
		return productoSN.getPorIdHijo(id);
	}

	public List<ProductoDTO> listarFiltrados(ProductoDTO productoDTO,
			Boolean mostrarInactivos) throws SystemException,
			ExcepcionDeAccesoADatos {
		ProductoSN productoSN = new ProductoSN();
		return productoSN.listarFiltrados(productoDTO, mostrarInactivos);
	}

	public List<DescuentoVarioProductoDTO> listarDescuentoAsociado(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		DescuentoVarioProductoSN descuentoPorProductoSN = new DescuentoVarioProductoSN();
		return descuentoPorProductoSN.listarDescuentoAsociado(productoDTO
				.getIdToProducto());
	}

	public List<DescuentoDTO> listarDescuentoPorAsociar(ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoSN descuentoSN = new DescuentoSN();
		return descuentoSN.listarDescuentosPorAsociar(productoDTO
				.getIdToProducto());
	}

	public void asociarDescuento(ProductoDTO productoDTO,
			DescuentoVarioProductoDTO descuentoPorProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoVarioProductoSN descuentoPorProductoSN = new DescuentoVarioProductoSN();
		descuentoPorProductoSN.guardar(descuentoPorProductoDTO);
	}

	public void desasociarDescuento(ProductoDTO productoDTO,
			DescuentoVarioProductoDTO descuentoPorProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoVarioProductoSN descuentoPorProductoSN = new DescuentoVarioProductoSN();
		descuentoPorProductoSN.borrar(descuentoPorProductoDTO);
	}

	public void actualizarAsociacion(ProductoDTO productoDTO,
			DescuentoVarioProductoDTO descuentoPorProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoVarioProductoSN descuentoPorProductoSN = new DescuentoVarioProductoSN();
		descuentoPorProductoSN.actualizar(descuentoPorProductoDTO);
	}

	public void borradoLogico(ProductoDTO ProductoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		new ProductoSN().borradoLogico(ProductoDTO);
	}
	
	
	public List<ProductoDTO> listarProductosConfiguracion(HttpServletRequest request) throws SystemException {
		boolean aplicaFiltradoProductos = true;
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		String claveProductosHabilitados = "midas.danios.solicitud.productoshabilitados";
		List<ProductoDTO> list = new ArrayList<ProductoDTO>();
		ProductoDTO productoFiltro = new ProductoDTO();
		
		Boolean verInactivos = (request.getParameter("verInactivos") != null?Boolean.parseBoolean(request.getParameter("verInactivos")):false);
		
		if (usuario != null
				&& usuario.contieneAtributo(claveProductosHabilitados)) {

			AtributoUsuario atributoProductosHabilitados = usuario
					.obtenerAtributo(claveProductosHabilitados);
			if (atributoProductosHabilitados.isActivo()) {

				String[] idToProductoStringArray = ((String) atributoProductosHabilitados
						.getValor()).split(",");
				for (int i = 0; i < idToProductoStringArray.length; i++) {
					if (!UtileriasWeb.esCadenaVacia(idToProductoStringArray[i])) {
						productoFiltro.setIdToProducto(UtileriasWeb
								.regresaBigDecimal(idToProductoStringArray[i]));

						list.addAll(listarFiltrados(productoFiltro, verInactivos));
					}
				}
			} else {
				aplicaFiltradoProductos = false;
			}
		} else {
			aplicaFiltradoProductos = false;
		}
		if (!aplicaFiltradoProductos) {
			
			productoFiltro.setClaveNegocio(request.getParameter("negocio"));
			
			list = listarFiltrados(productoFiltro, verInactivos);
			
		}
		return list;
	}

	public List<ProductoDTO> listarVigentes(Usuario usuario)
			throws SystemException, ExcepcionDeAccesoADatos {
		boolean aplicaFiltradoProductos = true;
		String claveProductosHabilitados = "midas.danios.solicitud.productoshabilitados";
		List<ProductoDTO> list = new ArrayList<ProductoDTO>();
		if (usuario != null
				&& usuario.contieneAtributo(claveProductosHabilitados)) {

			AtributoUsuario atributoProductosHabilitados = usuario
					.obtenerAtributo(claveProductosHabilitados);
			if (atributoProductosHabilitados.isActivo()) {

				String[] idToProductoStringArray = ((String) atributoProductosHabilitados
						.getValor()).split(",");
				ProductoDTO productoTMP = new ProductoDTO();

				for (int i = 0; i < idToProductoStringArray.length; i++) {
					if (!UtileriasWeb.esCadenaVacia(idToProductoStringArray[i])) {
						productoTMP.setIdToProducto(UtileriasWeb
								.regresaBigDecimal(idToProductoStringArray[i]));

						list.addAll(listarFiltrados(productoTMP, false));
					}
				}
			} else {
				aplicaFiltradoProductos = false;
			}
		} else {
			aplicaFiltradoProductos = false;
		}
		if (!aplicaFiltradoProductos) {
			list = new ProductoSN().listarVigentes();	
		}
		return list;
	}

	public List<RecargoVarioDTO> listarRecargosPorAsociar(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.listarRecargosPorAsociar(productoDTO
				.getIdToProducto());
	}

	public void asociarRecargo(ProductoDTO productoDTO,
			RecargoVarioProductoDTO recargoPorProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RecargoVarioProductoSN recargoPorProductoSN = new RecargoVarioProductoSN();
		recargoPorProductoSN.guardar(recargoPorProductoDTO);
	}

	public void actualizarAsociacion(ProductoDTO productoDTO,
			RecargoVarioProductoDTO recargoPorProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RecargoVarioProductoSN recargoPorProductoSN = new RecargoVarioProductoSN();
		recargoPorProductoSN.actualizar(recargoPorProductoDTO);
	}

	public void desasociarRecargo(ProductoDTO productoDTO,
			RecargoVarioProductoDTO recargoPorProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RecargoVarioProductoSN recargoPorProductoSN = new RecargoVarioProductoSN();
		recargoPorProductoSN.borrar(recargoPorProductoDTO);
	}

	/**
	 * Encuentra los registros de RamoDTO no asociados a un producto a trav�s de
	 * los registros RamoProductoDTO.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al quie no est�n asociados
	 *            los ramos
	 * 
	 * @return List<RamoDTO> la lista de ramos no asociados al registro
	 *         productoDTO
	 */
	public List<RamoDTO> listarRamosNoAsociados(ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoSN().obtenerRamosSinAsociarProducto(productoDTO
				.getIdToProducto());
	}

	/**
	 * Encuentra los registros de RamoDTO no asociados a un producto a trav�s de
	 * los registros RamoProductoDTO.
	 * 
	 * @param idToProducto
	 *            el ID del registro productoDTO al quie no est�n asociados los
	 *            ramos
	 * @return List<RamoDTO> la lista de ramos no asociados al registro
	 *         productoDTO
	 */
	public List<RamoDTO> listarRamosNoAsociados(BigDecimal idToProducto)
			throws SystemException, ExcepcionDeAccesoADatos {
		return new RamoSN().obtenerRamosSinAsociarProducto(idToProducto);
	}

	/**
	 * Encuentra los registros de RamoDTO asociados a un producto a trav�s de
	 * los registros RamoProductoDTO.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al quie est�n asociados
	 *            los ramos
	 * 
	 * @return List<RamoDTO> la lista de ramos asociados al registro productoDTO
	 */
	public List<RamoDTO> listarRamosAsociados(ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RamoProductoSN ramoProductoSN = new RamoProductoSN();
		RamoSN ramoSN = new RamoSN();
		// obtener los registros que s� est�n asociados a la seccion
		List<RamoProductoDTO> ramoProductoList = ramoProductoSN
				.buscarPorPropiedad("id.idtoproducto", productoDTO
						.getIdToProducto());
		List<RamoDTO> resultList = new ArrayList<RamoDTO>();
		RamoDTO ramoTMP = new RamoDTO();
		// Encontrar y guardar los registros ProductoDTO que coincidan
		for (RamoProductoDTO ramoProducto : ramoProductoList) {
			ramoTMP.setIdTcRamo(ramoProducto.getId().getIdtcramo());
			resultList.add(ramoSN.getRamoPorId(ramoTMP));
		}
		return resultList;
	}

	public void asociarRamo(RamoProductoDTO ramoProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new RamoProductoSN().agregar(ramoProductoDTO);
	}

	public void desasociarRamo(RamoProductoDTO ramoProductoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		new RamoProductoSN().borrar(ramoProductoDTO);
	}

	/**
	 * Encuentra los registros de ExcRecargoPorProductoDTO asociados a un
	 * producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que est�n asociados los
	 *            registros
	 * 
	 * @return List<ExcRecargoPorProductoDTO> la lista de
	 *         ExcRecargoPorProductoDTO asociados al registro productoDTO
	 */
	public List<ExclusionRecargoVarioProductoDTO> listarExcRecargoProductoAsociadas(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		List<ExclusionRecargoVarioProductoDTO> lista = new ExclusionRecargoVarioProductoSN()
				.buscarVigentesPorIdProducto(productoDTO.getIdToProducto());
		/*
		for (ExclusionRecargoVarioProductoDTO actual : lista) {
			actual.setProductoDTO(productoDTO);
			actual.setRecargoVarioDTO(new RecargoVarioSN().getPorId(actual
					.getId().getIdtorecargovario()));
			actual.setTipoPolizaDTO(new TipoPolizaSN().getPorId(actual.getId()
					.getIdtotipopoliza()));
		}*/
		return lista;
	}

	/**
	 * Encuentra los registros de ExcRecargoPorProductoDTO no asociados a un
	 * producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * 
	 * @return List<ExcRecargoPorProductoDTO> la lista de
	 *         ExcRecargoPorProductoDTO no asociados al registro productoDTO
	 */
	public List<ExclusionRecargoVarioProductoDTO> listarExcRecargoProductoNoAsociadas(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new TipoPolizaSN()
				.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los recargos asociados al producto
		List<RecargoVarioProductoDTO> recargos = new RecargoVarioProductoSN()
				.listarRecargoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de recargos asociadas ==> " + recargos.size(),
				Level.INFO, null);
		// obtener los registros de ExcRecargoPorProductoDTO asociados al
		// producto S�LO LOS VIGENTES
		List<ExclusionRecargoVarioProductoDTO> listaExcRecargo = listarExcRecargoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioProductoDTO> listaResultado = new ArrayList<ExclusionRecargoVarioProductoDTO>();
		if (listaExcRecargo.size() < (tiposPoliza.size() * recargos.size())) {
			for (RecargoVarioProductoDTO recargo : recargos) {
				for (TipoPolizaDTO poliza : tiposPoliza) {
					ExclusionRecargoVarioProductoId id = new ExclusionRecargoVarioProductoId();
					id.setIdtoproducto(productoDTO.getIdToProducto());
					id.setIdtorecargovario(recargo.getId()
							.getIdToRecargoVario());
					id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

					ExclusionRecargoVarioProductoDTO exclusionRecargoVarioProductoDTO = new ExclusionRecargoVarioProductoDTO();
					exclusionRecargoVarioProductoDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcRecargo
							.contains(exclusionRecargoVarioProductoDTO)) {
						exclusionRecargoVarioProductoDTO
								.setProductoDTO(productoDTO);
						exclusionRecargoVarioProductoDTO
								.setRecargoVarioDTO(recargo
										.getRecargoVarioDTO());
						exclusionRecargoVarioProductoDTO
								.setTipoPolizaDTO(poliza);
						listaResultado.add(exclusionRecargoVarioProductoDTO);
					}
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExcRecargoPorProductoDTO no asociados a un
	 * producto que coincidan con el tipoPoliza recibido.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @param idToTipoPoliza
	 *            . El Id del tipo p�liza.
	 * @return List<ExcRecargoPorProductoDTO> la lista de
	 *         ExcRecargoPorProductoDTO no asociados al registro productoDTO
	 */
	public List<ExclusionRecargoVarioProductoDTO> listarExcRecargoProductoNoAsociadas(
			ProductoDTO productoDTO, BigDecimal idToTipoPoliza)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new ArrayList<TipoPolizaDTO>();
		tiposPoliza.add(TipoPolizaDN.getInstancia().getPorId(idToTipoPoliza));
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los recargos asociados al producto
		List<RecargoVarioProductoDTO> recargos = new RecargoVarioProductoSN()
				.listarRecargoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de recargos asociadas ==> " + recargos.size(),
				Level.INFO, null);
		// obtener los registros de ExcRecargoPorProductoDTO asociados al
		// producto S�LO LOS VIGENTES
		List<ExclusionRecargoVarioProductoDTO> listaExcRecargo = listarExcRecargoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioProductoDTO> listaResultado = new ArrayList<ExclusionRecargoVarioProductoDTO>();
		for (RecargoVarioProductoDTO recargo : recargos) {
			for (TipoPolizaDTO poliza : tiposPoliza) {
				ExclusionRecargoVarioProductoId id = new ExclusionRecargoVarioProductoId();
				id.setIdtoproducto(productoDTO.getIdToProducto());
				id.setIdtorecargovario(recargo.getId().getIdToRecargoVario());
				id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

				ExclusionRecargoVarioProductoDTO exclusionRecargoVarioProductoDTO = new ExclusionRecargoVarioProductoDTO();
				exclusionRecargoVarioProductoDTO.setId(id);
				// Si el registro no existe, agregarlo a la lista de los no
				// existentes
				if (!listaExcRecargo.contains(exclusionRecargoVarioProductoDTO)) {
					exclusionRecargoVarioProductoDTO
							.setProductoDTO(productoDTO);
					exclusionRecargoVarioProductoDTO.setRecargoVarioDTO(recargo
							.getRecargoVarioDTO());
					exclusionRecargoVarioProductoDTO.setTipoPolizaDTO(poliza);
					listaResultado.add(exclusionRecargoVarioProductoDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExcRecargoPorProductoDTO no asociados a un
	 * producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * 
	 * @return List<ExcRecargoPorProductoDTO> la lista de
	 *         ExcRecargoPorProductoDTO no asociados al registro productoDTO
	 */
	public List<ExclusionRecargoVarioProductoDTO> listarExcRecargoProductoNoAsociadas(
			BigDecimal idToRecargo, ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new TipoPolizaSN()
				.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los recargos asociados al producto
		List<RecargoVarioProductoDTO> recargos = new RecargoVarioProductoSN()
				.listarRecargoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de recargos asociadas ==> " + recargos.size(),
				Level.INFO, null);
		// obtener los registros de ExcRecargoPorProductoDTO asociados al
		// producto S�LO LOS VIGENTES
		List<ExclusionRecargoVarioProductoDTO> listaExcRecargo = listarExcRecargoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionRecargoVarioProductoDTO> listaResultado = new ArrayList<ExclusionRecargoVarioProductoDTO>();
		for (RecargoVarioProductoDTO recargo : recargos) {
			if (recargo.getId().getIdToRecargoVario().compareTo(idToRecargo) == 0) {
				for (TipoPolizaDTO poliza : tiposPoliza) {
					ExclusionRecargoVarioProductoId id = new ExclusionRecargoVarioProductoId();
					id.setIdtoproducto(productoDTO.getIdToProducto());
					id.setIdtorecargovario(recargo.getId()
							.getIdToRecargoVario());
					id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

					ExclusionRecargoVarioProductoDTO exclusionRecargoVarioProductoDTO = new ExclusionRecargoVarioProductoDTO();
					exclusionRecargoVarioProductoDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcRecargo
							.contains(exclusionRecargoVarioProductoDTO)) {
						exclusionRecargoVarioProductoDTO
								.setProductoDTO(productoDTO);
						exclusionRecargoVarioProductoDTO
								.setRecargoVarioDTO(recargo
										.getRecargoVarioDTO());
						exclusionRecargoVarioProductoDTO
								.setTipoPolizaDTO(poliza);
						listaResultado.add(exclusionRecargoVarioProductoDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}

	public void ExcluirRecargoProducto(
			ExclusionRecargoVarioProductoDTO excRecargoProductoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionRecargoVarioProductoSN().agregar(excRecargoProductoDTO);
	}

	public void EliminarExclusionRecargoProducto(
			ExclusionRecargoVarioProductoDTO excRecargoProductoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionRecargoVarioProductoSN().borrar(excRecargoProductoDTO);
	}

	public List<AumentoVarioDTO> listarAumentoPorAsociar(ProductoDTO productoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.listarAumentosPorAsociar(productoDTO
				.getIdToProducto());
	}

	public void asociarAumento(ProductoDTO productoDTO,
			AumentoVarioProductoDTO aumentoPorProducto)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioProductoSN aumentoVarioProductoSN = new AumentoVarioProductoSN();
		aumentoVarioProductoSN.guardar(aumentoPorProducto);
	}

	public void actualizarAsociacion(ProductoDTO productoDTO,
			AumentoVarioProductoDTO aumentoPorProducto)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioProductoSN aumentoVarioProductoSN = new AumentoVarioProductoSN();
		aumentoVarioProductoSN.actualizar(aumentoPorProducto);
	}

	public void desasociarAumento(ProductoDTO productoDTO,
			AumentoVarioProductoDTO aumentoPorProducto)
			throws ExcepcionDeAccesoADatos, SystemException {
		AumentoVarioProductoSN aumentoVarioProductoSN = new AumentoVarioProductoSN();
		aumentoVarioProductoSN.borrar(aumentoPorProducto);
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioProductoDTO asociados a
	 * un producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que est�n asociados los
	 *            registros
	 * @return List<ExcAumentoPorProductoDTO> la lista de
	 *         ExcAumentoPorProductoDTO asociados al registro productoDTO
	 */
	public List<ExclusionAumentoVarioProductoDTO> listarExcAumentoProductoAsociadas(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new ExclusionAumentoVarioProductoSN()
				.buscarVigentesPorIdProducto(productoDTO.getIdToProducto());
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioProductoDTO no asociados
	 * a un producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @return List<ExclusionAumentoVarioProductoDTO> la lista de
	 *         ExclusionAumentoVarioProductoDTO no asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionAumentoVarioProductoDTO> listarExcAumentoProductoNoAsociadas(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new TipoPolizaSN()
				.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los Aumentos asociados al producto
		List<AumentoVarioProductoDTO> aumentos = new AumentoVarioProductoSN()
				.listarAumentoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de aumentos asociados ==> " + aumentos.size(),
				Level.INFO, null);
		// obtener los registros de ExclusionAumentoVarioProductoDTO asociados
		// al producto S�LO LOS VEGENTES
		List<ExclusionAumentoVarioProductoDTO> listaExcAumento = listarExcAumentoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioProductoDTO> listaResultado = new ArrayList<ExclusionAumentoVarioProductoDTO>();
		if (listaExcAumento.size() < (tiposPoliza.size() * aumentos.size())) {
			for (AumentoVarioProductoDTO aumento : aumentos) {
				for (TipoPolizaDTO poliza : tiposPoliza) {
					ExclusionAumentoVarioProductoId id = new ExclusionAumentoVarioProductoId();
					id.setIdtoproducto(productoDTO.getIdToProducto());
					id.setIdtoaumentovario(aumento.getId()
							.getIdtoaumentovario());
					id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

					ExclusionAumentoVarioProductoDTO exclusionAumentoVarioProductoDTO = new ExclusionAumentoVarioProductoDTO();
					exclusionAumentoVarioProductoDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcAumento
							.contains(exclusionAumentoVarioProductoDTO)) {
						exclusionAumentoVarioProductoDTO
								.setProductoDTO(productoDTO);
						exclusionAumentoVarioProductoDTO
								.setAumentoVarioDTO(aumento
										.getAumentoVarioDTO());
						exclusionAumentoVarioProductoDTO
								.setTipoPolizaDTO(poliza);
						listaResultado.add(exclusionAumentoVarioProductoDTO);
					}
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioProductoDTO no asociados
	 * a un producto que coincidan con el tipoPoliza recibido.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @param idToTipoPoliza
	 *            . El Id del tipo p�liza.
	 * @return List<ExclusionAumentoVarioProductoDTO> la lista de
	 *         ExclusionAumentoVarioProductoDTO no asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionAumentoVarioProductoDTO> listarExcAumentoProductoNoAsociadas(
			ProductoDTO productoDTO, BigDecimal idToTipoPoliza)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new ArrayList<TipoPolizaDTO>();
		tiposPoliza.add(TipoPolizaDN.getInstancia().getPorId(idToTipoPoliza));

		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los Aumentos asociados al producto
		List<AumentoVarioProductoDTO> aumentos = new AumentoVarioProductoSN()
				.listarAumentoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de aumentos asociados ==> " + aumentos.size(),
				Level.INFO, null);
		// obtener los registros de ExclusionAumentoVarioProductoDTO asociados
		// al producto S�LO LOS VIGENTES
		List<ExclusionAumentoVarioProductoDTO> listaExcAumento = listarExcAumentoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioProductoDTO> listaResultado = new ArrayList<ExclusionAumentoVarioProductoDTO>();
		for (AumentoVarioProductoDTO aumento : aumentos) {
			for (TipoPolizaDTO poliza : tiposPoliza) {
				ExclusionAumentoVarioProductoId id = new ExclusionAumentoVarioProductoId();
				id.setIdtoproducto(productoDTO.getIdToProducto());
				id.setIdtoaumentovario(aumento.getId().getIdtoaumentovario());
				id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

				ExclusionAumentoVarioProductoDTO exclusionAumentoVarioProductoDTO = new ExclusionAumentoVarioProductoDTO();
				exclusionAumentoVarioProductoDTO.setId(id);
				// Si el registro no existe, agregarlo a la lista de los no
				// existentes
				if (!listaExcAumento.contains(exclusionAumentoVarioProductoDTO)) {
					exclusionAumentoVarioProductoDTO
							.setProductoDTO(productoDTO);
					exclusionAumentoVarioProductoDTO.setAumentoVarioDTO(aumento
							.getAumentoVarioDTO());
					exclusionAumentoVarioProductoDTO.setTipoPolizaDTO(poliza);
					listaResultado.add(exclusionAumentoVarioProductoDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionAumentoVarioProductoDTO no asociados
	 * a un producto que coincidan con el Aumento recibido.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @param idToAumento
	 *            . El Id del Aumento.
	 * @return List<ExclusionAumentoVarioProductoDTO> la lista de
	 *         ExclusionAumentoVarioProductoDTO no asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionAumentoVarioProductoDTO> listarExcAumentoProductoNoAsociadas(
			BigDecimal idToAumento, ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new TipoPolizaSN()
				.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);

		// obtener los Aumentos asociados al producto
		List<AumentoVarioProductoDTO> aumentos = new AumentoVarioProductoSN()
				.listarAumentoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de aumentos asociados ==> " + aumentos.size(),
				Level.INFO, null);
		// obtener los registros de ExclusionAumentoVarioProductoDTO asociados
		// al producto S�LO LOS VEGENTES
		List<ExclusionAumentoVarioProductoDTO> listaExcAumento = listarExcAumentoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionAumentoVarioProductoDTO> listaResultado = new ArrayList<ExclusionAumentoVarioProductoDTO>();
		for (AumentoVarioProductoDTO aumento : aumentos) {
			if (aumento.getId().getIdtoaumentovario().compareTo(idToAumento) == 0) {
				for (TipoPolizaDTO poliza : tiposPoliza) {
					ExclusionAumentoVarioProductoId id = new ExclusionAumentoVarioProductoId();
					id.setIdtoproducto(productoDTO.getIdToProducto());
					id.setIdtoaumentovario(aumento.getId()
							.getIdtoaumentovario());
					id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

					ExclusionAumentoVarioProductoDTO exclusionAumentoVarioProductoDTO = new ExclusionAumentoVarioProductoDTO();
					exclusionAumentoVarioProductoDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcAumento
							.contains(exclusionAumentoVarioProductoDTO)) {
						exclusionAumentoVarioProductoDTO
								.setProductoDTO(productoDTO);
						exclusionAumentoVarioProductoDTO
								.setAumentoVarioDTO(aumento
										.getAumentoVarioDTO());
						exclusionAumentoVarioProductoDTO
								.setTipoPolizaDTO(poliza);
						listaResultado.add(exclusionAumentoVarioProductoDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}

	public void ExcluirAumentoProducto(
			ExclusionAumentoVarioProductoDTO excAumentoProductoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionAumentoVarioProductoSN().agregar(excAumentoProductoDTO);
	}

	public void EliminarExclusionAumentoProducto(
			ExclusionAumentoVarioProductoDTO excAumentoProductoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionAumentoVarioProductoSN().borrar(excAumentoProductoDTO);
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioProductoDTO asociados a
	 * un producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que est�n asociados los
	 *            registros
	 * @return List<ExclusionDescuentoVarioProductoDTO> la lista de
	 *         ExclusionDescuentoVarioProductoDTO asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionDescuentoVarioProductoDTO> listarExcDescuentoProductoAsociadas(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new ExclusionDescuentoVarioProductoSN()
				.buscarVigentesPorIdProducto(productoDTO.getIdToProducto());
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioProductoDTO no
	 * asociados a un producto
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @return List<ExclusionDescuentoVarioProductoDTO> la lista de
	 *         ExclusionDescuentoVarioProductoDTO no asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionDescuentoVarioProductoDTO> listarExcDescuentoProductoNoAsociadas(
			ProductoDTO productoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new TipoPolizaSN()
				.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los recargos asociados al producto
		List<DescuentoVarioProductoDTO> descuentos = new DescuentoVarioProductoSN()
				.listarDescuentoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de descuentos asociadas ==> " + descuentos.size(),
				Level.INFO, null);
		// obtener los registros de ExclusionDescuentoVarioProductoDTO asociados
		// al producto
		List<ExclusionDescuentoVarioProductoDTO> listaExcDescuento = listarExcDescuentoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioProductoDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioProductoDTO>();
		if (listaExcDescuento.size() < (tiposPoliza.size() * descuentos.size())) {
			for (DescuentoVarioProductoDTO descuento : descuentos) {
				for (TipoPolizaDTO poliza : tiposPoliza) {
					ExclusionDescuentoVarioProductoId id = new ExclusionDescuentoVarioProductoId();
					id.setIdtoproducto(productoDTO.getIdToProducto());
					id.setIdtodescuentovario(descuento.getId()
							.getIdToDescuentoVario());
					id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

					ExclusionDescuentoVarioProductoDTO exclusionDescuentoVarioProductoDTO = new ExclusionDescuentoVarioProductoDTO();
					exclusionDescuentoVarioProductoDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcDescuento
							.contains(exclusionDescuentoVarioProductoDTO)) {
						exclusionDescuentoVarioProductoDTO
								.setProductoDTO(productoDTO);
						exclusionDescuentoVarioProductoDTO
								.setDescuentoDTO(descuento.getDescuentoDTO());
						exclusionDescuentoVarioProductoDTO
								.setTipoPolizaDTO(poliza);
						listaResultado.add(exclusionDescuentoVarioProductoDTO);
					}
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioProductoDTO no
	 * asociados a un producto que coincidan con el tipoPoliza recibido.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @param idToTipoPoliza
	 *            . El Id del tipo p�liza.
	 * @return List<ExclusionDescuentoVarioProductoDTO> la lista de
	 *         ExclusionDescuentoVarioProductoDTO no asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionDescuentoVarioProductoDTO> listarExcDescuentoProductoNoAsociadas(
			ProductoDTO productoDTO, BigDecimal idToTipoPoliza)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new ArrayList<TipoPolizaDTO>();
		tiposPoliza.add(TipoPolizaDN.getInstancia().getPorId(idToTipoPoliza));
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los recargos asociados al producto
		List<DescuentoVarioProductoDTO> descuentos = new DescuentoVarioProductoSN()
				.listarDescuentoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de descuentos asociadas ==> " + descuentos.size(),
				Level.INFO, null);
		// obtener los registros de ExclusionDescuentoVarioProductoDTO asociados
		// al producto
		List<ExclusionDescuentoVarioProductoDTO> listaExcDescuento = listarExcDescuentoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioProductoDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioProductoDTO>();
		for (DescuentoVarioProductoDTO descuento : descuentos) {
			for (TipoPolizaDTO poliza : tiposPoliza) {
				ExclusionDescuentoVarioProductoId id = new ExclusionDescuentoVarioProductoId();
				id.setIdtoproducto(productoDTO.getIdToProducto());
				id.setIdtodescuentovario(descuento.getId()
						.getIdToDescuentoVario());
				id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

				ExclusionDescuentoVarioProductoDTO exclusionDescuentoVarioProductoDTO = new ExclusionDescuentoVarioProductoDTO();
				exclusionDescuentoVarioProductoDTO.setId(id);
				// Si el registro no existe, agregarlo a la lista de los no
				// existentes
				if (!listaExcDescuento
						.contains(exclusionDescuentoVarioProductoDTO)) {
					exclusionDescuentoVarioProductoDTO
							.setProductoDTO(productoDTO);
					exclusionDescuentoVarioProductoDTO
							.setDescuentoDTO(descuento.getDescuentoDTO());
					exclusionDescuentoVarioProductoDTO.setTipoPolizaDTO(poliza);
					listaResultado.add(exclusionDescuentoVarioProductoDTO);
				}
			}
		}
		return listaResultado;
	}

	/**
	 * Encuentra los registros de ExclusionDescuentoVarioProductoDTO no
	 * asociados a un producto que coincidan con el tipoPoliza recibido.
	 * 
	 * @param ProductoDTO
	 *            productoDTO el registro productoDTO al que no est�n asociados
	 *            los registros
	 * @param idToTipoPoliza
	 *            . El Id del tipo p�liza.
	 * @return List<ExclusionDescuentoVarioProductoDTO> la lista de
	 *         ExclusionDescuentoVarioProductoDTO no asociados al registro
	 *         productoDTO
	 */
	public List<ExclusionDescuentoVarioProductoDTO> listarExcDescuentoProductoNoAsociadas(
			BigDecimal idToDescuento, ProductoDTO productoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		// obtener los tipo de poliza asociados al producto
		List<TipoPolizaDTO> tiposPoliza = new TipoPolizaSN()
				.listarVigentesPorIdProducto(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de polizas asociadas ==> " + tiposPoliza.size(),
				Level.INFO, null);
		// obtener los recargos asociados al producto
		List<DescuentoVarioProductoDTO> descuentos = new DescuentoVarioProductoSN()
				.listarDescuentoAsociado(productoDTO.getIdToProducto());
		LogDeMidasWeb.log("Producto id:" + productoDTO.getIdToProducto()
				+ " Numero de descuentos asociadas ==> " + descuentos.size(),
				Level.INFO, null);
		// obtener los registros de ExclusionDescuentoVarioProductoDTO asociados
		// al producto ACTUALIZACI�N: S�LO LOS VIGENTES
		List<ExclusionDescuentoVarioProductoDTO> listaExcDescuento = listarExcDescuentoProductoAsociadas(productoDTO);
		// Instanciar la lista donde se guardar�n los registros no existentes
		List<ExclusionDescuentoVarioProductoDTO> listaResultado = new ArrayList<ExclusionDescuentoVarioProductoDTO>();
		for (DescuentoVarioProductoDTO descuento : descuentos) {
			if (descuento.getId().getIdToDescuentoVario().compareTo(
					idToDescuento) == 0) {
				for (TipoPolizaDTO poliza : tiposPoliza) {
					ExclusionDescuentoVarioProductoId id = new ExclusionDescuentoVarioProductoId();
					id.setIdtoproducto(productoDTO.getIdToProducto());
					id.setIdtodescuentovario(descuento.getId()
							.getIdToDescuentoVario());
					id.setIdtotipopoliza(poliza.getIdToTipoPoliza());

					ExclusionDescuentoVarioProductoDTO exclusionDescuentoVarioProductoDTO = new ExclusionDescuentoVarioProductoDTO();
					exclusionDescuentoVarioProductoDTO.setId(id);
					// Si el registro no existe, agregarlo a la lista de los no
					// existentes
					if (!listaExcDescuento
							.contains(exclusionDescuentoVarioProductoDTO)) {
						exclusionDescuentoVarioProductoDTO
								.setProductoDTO(productoDTO);
						exclusionDescuentoVarioProductoDTO
								.setDescuentoDTO(descuento.getDescuentoDTO());
						exclusionDescuentoVarioProductoDTO
								.setTipoPolizaDTO(poliza);
						listaResultado.add(exclusionDescuentoVarioProductoDTO);
					}
				}
				break;
			}
		}
		return listaResultado;
	}

	public void ExcluirDescuentoProducto(
			ExclusionDescuentoVarioProductoDTO excDescuentoProductoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionDescuentoVarioProductoSN()
				.agregar(excDescuentoProductoDTO);
	}

	public void EliminarExclusionDescuentoProducto(
			ExclusionDescuentoVarioProductoDTO excDescuentoProductoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new ExclusionDescuentoVarioProductoSN().borrar(excDescuentoProductoDTO);
	}

	public MonedaDTO getMonedaDTO(MonedaProductoDTO moneda)
			throws SystemException {
		MonedaSN monedaSN = new MonedaSN();
		return monedaSN.getPorId(moneda.getId().getIdMoneda().shortValue());
	}

	public List<MonedaDTO> listarMonedasPorAsociar(ProductoDTO productoDTO)
			throws SystemException {
		MonedaSN monedaSN = new MonedaSN();
		return monedaSN.listarMonedasPorAsociar(productoDTO.getIdToProducto());
	}

	public void asociarMoneda(ProductoDTO productoDTO,
			MonedaProductoDTO monedaProductoDTO) throws SystemException {
		MonedaProductoSN monedaProductoSN = new MonedaProductoSN();
		monedaProductoSN.agregar(monedaProductoDTO);
	}

	public void desasociarMoneda(ProductoDTO productoDTO,
			MonedaProductoDTO monedaProductoDTO) throws SystemException {
		MonedaProductoSN monedaProductoSN = new MonedaProductoSN();
		monedaProductoSN.borrar(monedaProductoDTO);
	}

	public List<MonedaProductoDTO> listarMonedasAsociadas(
			BigDecimal idToProducto) throws SystemException {
		MonedaProductoSN monedaProductoSN = new MonedaProductoSN();
		return monedaProductoSN.buscarPorPropiedad("id.idToProducto",
				idToProducto);
	}

	public List<DocumentoAnexoProductoDTO> listarDocumentosAnexos(
			BigDecimal idToProducto) throws ExcepcionDeAccesoADatos,
			SystemException {
		return DocumentoAnexoProductoDN.getInstancia().listarPorPropiedad(
				"productoDTO.idToProducto", idToProducto);
	}
	
	public Map<String, String> agregarVersion(BigDecimal idProducto)
		throws Exception {
		ProductoSN productoSN = new ProductoSN();
	 return productoSN.agregarVersion(idProducto);
 }
	
	
}
