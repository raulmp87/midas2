/**
 * 
 */
package mx.com.afirme.midas.producto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ProductoSN {
	private ProductoFacadeRemote beanRemoto;

	public ProductoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ProductoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ProductoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		
		try{
			List<ProductoDTO> ajustadores = beanRemoto.findAll();
			return ajustadores;
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public List<ProductoDTO> listarFiltrados(ProductoDTO ajustadorDTO, Boolean mostrarInactivos)
			throws ExcepcionDeAccesoADatos {
		
		try{
			List<ProductoDTO> ajustadores = beanRemoto.listarFiltrado(ajustadorDTO, mostrarInactivos);
			return ajustadores;
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void agregar(ProductoDTO ajustadorDTO)
			throws ExcepcionDeAccesoADatos {
		
		try{
			beanRemoto.save(ajustadorDTO);
		} catch(Exception e){
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		} 
	}

	public void modificar(ProductoDTO ajustadorDTO)
			throws ExcepcionDeAccesoADatos {
		
		try{
			beanRemoto.update(ajustadorDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ProductoDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findById(id);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrar(ProductoDTO ajustadorDTO) throws ExcepcionDeAccesoADatos {
		
		try{
			beanRemoto.delete(ajustadorDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ProductoDTO getPorIdHijo(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		try{
			return beanRemoto.findByChildId(id);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public void borradoLogico(ProductoDTO ProductoDTO) throws ExcepcionDeAccesoADatos{
		
		try{
			beanRemoto.borradoLogico(ProductoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ProductoDTO> listarVigentes() throws ExcepcionDeAccesoADatos {
		try{
			return beanRemoto.listarVigentes();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ProductoDTO encontrarPorSolicitud(BigDecimal idToSolicitud) throws ExcepcionDeAccesoADatos {
		try{
			return beanRemoto.findProductoBySolicitud(idToSolicitud);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Map<String, String> agregarVersion(BigDecimal idProducto)throws Exception{
		return beanRemoto.generarNuevaVersion(idProducto);
    }	
	
	
}
