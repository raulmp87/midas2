package mx.com.afirme.midas.cliente;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @author vngarcia
 *
 */
public class ClienteForm extends MidasBaseForm{
	private static final long serialVersionUID = 5734773068533272784L;
	private String idCliente;
	private String idDomicilio;
	// Datos Persona
	private String idToPersona;
	private String claveTipoPersona="1";
	private String nombre = "";
	private String apellidoPaterno = "";
	private String apellidoMaterno = "";
	private String fechaNacimiento = "";
	private String codigoRFC = "";
	private String telefono = "";
	private String email = "";
	private String descripcionTipoPersona="";
	private String estadoNacimiento;
	private String codigoCURP;
	private String sexo = "";	
	// Datos Direccion
	private String idToDireccion;
	private String nombreCalle;
	private String nombreColonia;//Campo usado para guardar el id de la colonia
	private String descripcionColonia;//campo usado para guardar el nombre de la colonia (vista para el usuario)
	private String nombreDelegacion;
	private String idMunicipio="";
	private String idEstado;
	private String descripcionEstado;
	private String codigoPostal;
	private String descripcionPadre;
	private String tipoDireccion;
	private String idPadre;
	private String esNuevoRegistro="1";
	private String codigoPersona; //"1"-Persona contratante. "3"-Persona asegurado. Para el caso de Cotizacion
	private String nombreRepresentante;
	private String idEstadoNacimiento;
	private String descripcionEstadoNacimiento;
	private String fechaNacimientoRepresentante = "";

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null)	errors = new ActionErrors();
		if(!this.codigoCURP.trim().equals("") && this.codigoCURP.length() < 18){
			errors.add("codigoCURP", new ActionMessage("errors.minlength",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.curp"),18));
		}else if(!this.codigoCURP.trim().equals("") && this.codigoCURP.length() > 18){
			errors.add("codigoCURP", new ActionMessage("errors.maxlength",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.curp"),18));			
		}
		if(!this.codigoCURP.trim().equals("") && (this.sexo.trim().equals(""))){
			errors.add("sexo", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.sexo")));
		}	
		if(!this.codigoCURP.trim().equals("") && (this.idEstadoNacimiento == null || this.idEstadoNacimiento.equals(""))){
			errors.add("idEstadoNacimiento", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.estadoNacimiento")));
		}			
		//Persona fisica
		if (this.claveTipoPersona.equals(Sistema.CLAVE_TIPO_PERSONA_FISICA)){
			
			if(this.nombre != null){
				if (this.nombre.trim().equals(""))
					errors.add("nombre", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.nombre")));
				//Para persona f�sica, el m�ximo de caracteres es 40 en el nombre
				else if (this.nombre.length() > 40)
					errors.add("nombre", new ActionMessage("errors.maxlength",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.nombre"),"40","40"));
			}
			else	errors.add("nombre", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.nombre")));
			if(this.apellidoMaterno != null){
				if (this.apellidoMaterno.trim().equals(""))
					errors.add("apellidoMaterno", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.apellidoMaterno")));
				//Para persona f�sica, el m�ximo de caracteres es 19 en los apellidos
				else if (this.apellidoMaterno.length() >19)
					errors.add("apellidoMaterno", new ActionMessage("errors.maxlength",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.apellidoMaterno"),"19","19"));
			}
			else	errors.add("apellidoMaterno", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.apellidoMaterno")));
			if(this.apellidoPaterno != null){
				if (this.apellidoPaterno.trim().equals(""))
					errors.add("apellidoPaterno", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.apellidoPaterno")));
				//Para persona f�sica, el m�ximo de caracteres es 19 en los apellidos
				else if (this.apellidoPaterno.length() >19)
					errors.add("apellidoPaterno", new ActionMessage("errors.maxlength",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.apellidoPaterno"),"19","19"));
			}
			else	errors.add("apellidoPaterno", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.apellidoPaterno")));
			if(this.codigoRFC!=null){
        			if(this.codigoRFC.trim().equals("")){
        			    errors.add("codigoRFC", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.rfc")));
        			}else {
        			    if(!UtileriasWeb.validarRfcPersonaFisica(this.codigoRFC)){
        				 errors.add("codigoRFC", new ActionMessage("errors.invalid",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.rfc")));
        			    }
        			}
			}
		}
		//cuando es persona moral, no es necesario introducir apellidos.
		else if (this.claveTipoPersona.equals(Sistema.CLAVE_TIPO_PERSONA_MORAL)){
			if(!this.codigoCURP.trim().equals("") && this.nombreRepresentante.trim().equals("")){
				errors.add("nombreRepresentante", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.nombre")));
			}
			if(!this.codigoCURP.trim().equals("") &&  this.fechaNacimientoRepresentante.trim().equals("")){
				errors.add("fechaNacimientoRepresentante", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.fechaNacimiento")));
			}						
			if(this.nombre != null){
				if (this.nombre.trim().equals(""))
					errors.add("nombre", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.razonSocial")));
				//Para persona moral, el m�ximo de caracteres es 78 en el nombre
				else if (this.nombre.length() >78)
					errors.add("nombre", new ActionMessage("errors.maxlength",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.razonSocial"),"78","78"));
			}
			else	errors.add("nombre", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.razonSocial")));
		
			if(this.codigoRFC!=null){
        			if(this.codigoRFC.trim().equals("")){
        			    errors.add("codigoRFC", new ActionMessage("errors.required",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.rfc")));
        			}else {
        			    if(!UtileriasWeb.validarRfcPersonaMoral(this.codigoRFC)){
        				 errors.add("codigoRFC", new ActionMessage("errors.invalid",UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.persona.rfc")));
        			    }
        			}
			}
		
		}
		if(!errors.isEmpty()) {
			this.setMensaje("Alguno de los datos es incorrecto. Favor de verificar.");
			this.setTipoMensaje(Sistema.INFORMACION);
		}
		return errors;
	}
	public String getIdToPersona() {
		return idToPersona;
	}
	public void setIdToPersona(String idToPersona) {
		this.idToPersona = idToPersona;
	}
	public String getClaveTipoPersona() {
		return claveTipoPersona;
	}
	public void setClaveTipoPersona(String claveTipoPersona) {
		this.claveTipoPersona = claveTipoPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCodigoRFC() {
		return codigoRFC;
	}
	public void setCodigoRFC(String codigoRFC) {
		this.codigoRFC = codigoRFC;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdToDireccion() {
		return idToDireccion;
	}
	public void setIdToDireccion(String idToDireccion) {
		this.idToDireccion = idToDireccion;
	}
	public String getNombreCalle() {
		return nombreCalle;
	}
	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}
	/*public String getNumeroExterior() {
		return numeroExterior;
	}
	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}
	public String getNumeroInterior() {
		return numeroInterior;
	}
	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}
	public String getEntreCalles() {
		return entreCalles;
	}
	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}*/
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	public String getNombreDelegacion() {
		return nombreDelegacion;
	}
	public void setNombreDelegacion(String nombreDelegacion) {
		this.nombreDelegacion = nombreDelegacion;
	}
	public String getIdMunicipio() {
		return idMunicipio;
	}
	public void setIdMunicipio(String idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getDescripcionPadre() {
		return descripcionPadre;
	}
	public void setDescripcionPadre(String descripcionPadre) {
		this.descripcionPadre = descripcionPadre;
	}
	public String getTipoDireccion() {
		return tipoDireccion;
	}
	public void setTipoDireccion(String tipoDireccion) {
		this.tipoDireccion = tipoDireccion;
	}
	public String getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}
	public String getDescripcionTipoPersona() {
		return descripcionTipoPersona;
	}
	public void setDescripcionTipoPersona(String descripcionTipoPersona) {
		this.descripcionTipoPersona = descripcionTipoPersona;
	}
	public String getEsNuevoRegistro() {
		return esNuevoRegistro;
	}
	public void setEsNuevoRegistro(String esNuevoRegistro) {
		this.esNuevoRegistro = esNuevoRegistro;
	}
	public String getCodigoPersona() {
		return codigoPersona;
	}
	public void setCodigoPersona(String codigoPersona) {
		this.codigoPersona = codigoPersona;
	}
	public String getDescripcionColonia() {
		return descripcionColonia;
	}
	public void setDescripcionColonia(String descripcionColonia) {
		this.descripcionColonia = descripcionColonia;
	}
	public String getDescripcionEstado() {
		return descripcionEstado;
	}
	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}
	public String getEstadoNacimiento() {
		return estadoNacimiento;
	}
	public void setEstadoNacimiento(String estadoNacimiento) {
		this.estadoNacimiento = estadoNacimiento;
	}
	public String getCodigoCURP() {
		return codigoCURP;
	}
	public void setCodigoCURP(String codigoCURP) {
		this.codigoCURP = codigoCURP;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getNombreRepresentante() {
		return nombreRepresentante;
	}
	public void setNombreRepresentante(String nombreRepresentante) {
		this.nombreRepresentante = nombreRepresentante;
	}
	public String getIdEstadoNacimiento() {
		return idEstadoNacimiento;
	}
	public void setIdEstadoNacimiento(String idEstadoNacimiento) {
		this.idEstadoNacimiento = idEstadoNacimiento;
	}
	public String getFechaNacimientoRepresentante() {
		return fechaNacimientoRepresentante;
	}
	public void setFechaNacimientoRepresentante(String fechaNacimientoRepresentante) {
		this.fechaNacimientoRepresentante = fechaNacimientoRepresentante;
	}
	public String getDescripcionEstadoNacimiento() {
		return descripcionEstadoNacimiento;
	}
	public void setDescripcionEstadoNacimiento(String descripcionEstadoNacimiento) {
		this.descripcionEstadoNacimiento = descripcionEstadoNacimiento;
	}
	public String getIdDomicilio() {
		return idDomicilio;
	}
	public void setIdDomicilio(String idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
}
