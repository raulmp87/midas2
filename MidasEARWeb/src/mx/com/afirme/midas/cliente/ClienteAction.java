package mx.com.afirme.midas.cliente;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.grid.RespuestaXMLForm;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.curp.ValidaCURPDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.afirme.validation.curp.ValidateCurp;

public class ClienteAction extends MidasMappingDispatchAction {

	/**
	 * Method listarFiltrado
	 * @param mapping, form, request, response
	 * @return ActionForward
	 */
	public void listarFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String tipoPersona = request.getParameter("tipoPersona");
		String nombre,apPaterno,apMaterno,rfc,numeroAsegurado, curp, fechaConstitucion;
		ClienteDTO clienteDTO = new ClienteDTO();
		List<ClienteDTO> listaCliente = null;
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			if (tipoPersona != null){
				if (tipoPersona.equals("fisica")){
					nombre = new String(request.getParameter("nombre") != null? request.getParameter("nombre").getBytes() : "".getBytes(), "UTF-8");
					apPaterno = new String(request.getParameter("apellidoPaterno") != null? request.getParameter("apellidoPaterno").getBytes() : "".getBytes(), "UTF-8");
					apMaterno = new String(request.getParameter("apellidoMaterno") != null? request.getParameter("apellidoMaterno").getBytes() : "".getBytes(), "UTF-8");
					rfc = request.getParameter("rfc");
					curp = new String(request.getParameter("curp") != null? request.getParameter("curp").getBytes() : "".getBytes(), "UTF-8");
					numeroAsegurado = request.getParameter("numeroAsegurado");
					clienteDTO.setNombre(nombre);
					clienteDTO.setApellidoPaterno(apPaterno);
					clienteDTO.setApellidoMaterno(apMaterno);
					clienteDTO.setCodigoRFC(rfc);
					clienteDTO.setClaveTipoPersona((short)1);
					if (numeroAsegurado != null)
						clienteDTO.setIdCliente(UtileriasWeb.regresaBigDecimal(numeroAsegurado));
					if(curp!=null)
						clienteDTO.setCodigoCURP(curp);
					//TODO falta el campo de numero asegurado
					listaCliente = ClienteDN.getInstancia().listarFiltrado(clienteDTO, usuario);
				}
				else{
					nombre = new String(request.getParameter("nombre") != null? request.getParameter("nombre").getBytes() : "".getBytes(), "UTF-8");
					rfc = request.getParameter("rfc");
					fechaConstitucion = new String(request.getParameter("fechaConstitucion") != null? request.getParameter("fechaConstitucion").getBytes() : "".getBytes(), "UTF-8");
					numeroAsegurado = request.getParameter("numeroAsegurado");
					clienteDTO.setNombre(nombre);
					clienteDTO.setCodigoRFC(rfc);
					clienteDTO.setClaveTipoPersona((short)2);
					if (numeroAsegurado != null)
						clienteDTO.setIdCliente(UtileriasWeb.regresaBigDecimal(numeroAsegurado));
					if(fechaConstitucion!=null)
						try {
							clienteDTO.setFechaNacimiento(UtileriasWeb.getFechaFromString(fechaConstitucion));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					listaCliente = ClienteDN.getInstancia().listarFiltrado(clienteDTO, usuario);
				}
			}
			MidasJsonBase json = new MidasJsonBase();
			if (listaCliente != null)
				for(ClienteDTO cliente : listaCliente) {
					MidasJsonRow row = new MidasJsonRow();
					row.setId(cliente.getIdCliente().toString());
					row.setDatos(cliente.getNombre(),
							(cliente.getDireccionCompleta() != null ? cliente.getDireccionCompleta() : ""),
							cliente.getCodigoRFC());
					json.addRow(row);
				}
			response.setContentType("text/json");
			System.out.println("Clientes encontrados: "+json);
			PrintWriter pw;
				pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method mostrarAsociarClienteCotizacion
	 * @param mapping, form, request, response
	 * @return ActionForward
	 */
	public ActionForward mostrarAsociarClienteCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String navegacion = Sistema.EXITOSO;
		String idCotizacion = request.getParameter("idCotizacion");
		String codigoPersona = request.getParameter("codigoPersona");
		String idCliente = request.getParameter("idCliente");
		String cambiaA = request.getParameter("cambiaA");
		ClienteForm clienteForm = (ClienteForm) form;
		clienteForm.setIdPadre(idCotizacion);
		clienteForm.setCodigoPersona(codigoPersona);
		clienteForm.setIdCliente(idCliente);
		
		try{
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(idCotizacion));
			if(idCliente.equals("0")) {
				clienteForm.setClaveTipoPersona(cotizacionDTO.getSolicitudDTO().getClaveTipoPersona().toString());
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoPersona() == 1){
					clienteForm.setNombre(cotizacionDTO.getSolicitudDTO().getNombrePersona());
					clienteForm.setApellidoPaterno(cotizacionDTO.getSolicitudDTO().getApellidoPaterno());
					clienteForm.setApellidoMaterno(cotizacionDTO.getSolicitudDTO().getApellidoMaterno());
				} else {
					if(clienteForm.getNombre()==null || clienteForm.getNombre().equals(""))
						clienteForm.setNombre(cotizacionDTO.getSolicitudDTO().getNombrePersona());
					
				}
				clienteForm.setTelefono(cotizacionDTO.getSolicitudDTO().getTelefonoContacto());
			} else {
				ClienteDTO clienteDTO = null;
				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
					clienteDTO = new ClienteDTO();
					clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
					clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, UtileriasWeb.obtieneNombreUsuario(request));
				}else{
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
				}
				ClienteAction.poblarClienteFormEditable(clienteDTO, clienteForm);
			}
			if(!UtileriasWeb.esCadenaVacia(cambiaA)){
				if(cambiaA.equals("F")){
					clienteForm.setClaveTipoPersona("1");
				}else if(cambiaA.equals("M")){
					clienteForm.setClaveTipoPersona("2");
					clienteForm.setApellidoPaterno(null);
					clienteForm.setApellidoMaterno(null);						
				}
			}		
			
		} catch (SystemException e) {}
		return mapping.findForward(navegacion);
	}

	/**
	 * Method asociarClienteCotizacion
	 * @param mapping, form, request, response
	 * @return ActionForward
	 */
	public ActionForward asociarClienteCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		ActionForward forward;
		forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		ClienteForm clienteForm = (ClienteForm) form;
		ClienteDTO clienteDTO = new ClienteDTO();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		String url = forward.getPath();
		Boolean errorAlGuardar = Boolean.FALSE;
		String cambiaA="";
		try { 
		    
		    	String errorRFC=validaRFC(clienteForm);
		    	if(errorRFC!=null){
		    	    if (clienteForm.getCodigoPersona().equals("1")){
				url += "?idPadre="+clienteForm.getIdPadre()+"&descripcionPadre=cotizacion&tipoDireccion=1";
		    	    } else if (clienteForm.getCodigoPersona().equals("3")){
				url += "?idPadre="+clienteForm.getIdPadre()+"&descripcionPadre=cotizacion&tipoDireccion=3";
		    	    }
		    	       	url += errorRFC;
        		    	forward.setPath(url);
        			return forward;
		    	}		    	
	    	if(clienteForm.getCodigoCURP()!= null && !clienteForm.getCodigoCURP().equals("") ){
		    	ValidateCurp curpVO = new ValidateCurp();
		    	String estadoNacimiento = "ND";
				if (!UtileriasWeb.esCadenaVacia(clienteForm.getIdEstadoNacimiento())){
					try {
						EstadoFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(EstadoFacadeRemote.class);
						List<EstadoDTO> estado = beanRemoto.findByProperty("stateId", clienteForm.getIdEstadoNacimiento());
						if(!estado.isEmpty()) {
							clienteForm.setIdEstadoNacimiento(estado.get(0).getStateId());
							estadoNacimiento = estado.get(0).getStateName();
						}
					} catch (SystemException e) {
					}
				}		    	
				
				curpVO.setBornState(estadoNacimiento);
				curpVO.setCapturedCurp(clienteForm.getCodigoCURP());
				curpVO.setPersonFirstLastName(clienteForm.getApellidoPaterno());
				curpVO.setPersonSecondLastName(clienteForm.getApellidoMaterno());
				if(clienteForm.getSexo()!= null && clienteForm.getSexo().equals("M")){
					curpVO.setPersonGender("MASCULINO");
				}else{
					curpVO.setPersonGender("FEMENINO");
				}									
		    	if(clienteForm.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_FISICA)){
		    		curpVO.setBirthDate(clienteForm.getFechaNacimiento());
					curpVO.setPersonName(clienteForm.getNombre());
					cambiaA ="F";
		    	}else if(clienteForm.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_MORAL)){
		    		curpVO.setBirthDate(clienteForm.getFechaNacimientoRepresentante());
					curpVO.setPersonName(clienteForm.getNombreRepresentante());
					cambiaA = "M";
		    	}
				
		    	curpVO = ValidaCURPDN.getInstancia().valida(curpVO);
				
				if (!curpVO.isValid()) {				
					url = "/cliente/mostrarAsociarClienteCotizacion.do";
					if (clienteForm.getCodigoPersona().equals("1")) {
						url += "?idCotizacion="
								+ clienteForm.getIdPadre()
								+ "&codigoPersona="
								+ clienteForm.getCodigoPersona()
								+ "&idCliente=0&cambiaA="+cambiaA;
					} else if (clienteForm.getCodigoPersona().equals("3")) {
						url += "?idCotizacion="
							+ clienteForm.getIdPadre()
							+ "&codigoPersona="
							+ clienteForm.getCodigoPersona()
							+ "&idCliente=0&cambiaA="+cambiaA;
					}
					clienteForm.setTipoMensaje(Sistema.INFORMACION);
					clienteForm.setMensaje(curpVO.getMessageValidation());
	    	       	url += "&mensaje="+curpVO.getMessageValidation()+"&tipoMensaje="+Sistema.INFORMACION;
    		    	forward.setPath(url);
    			return forward;					
				}	    		
				poblarClienteDTO(clienteForm, clienteDTO);
				clienteDTO = ClienteDN.getInstancia().agregarV2(clienteDTO, usuario.getNombreUsuario());
				
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.getPorId(UtileriasWeb.regresaBigDecimal(clienteForm.getIdPadre()));
				String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	            nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	            nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";

				if (clienteForm.getCodigoPersona() != null){
					// 1 = Contratante ; 3 = asegurado
					if (clienteForm.getCodigoPersona().equals("1")){
						cotizacionDTO.setIdToPersonaContratante(clienteDTO.getIdCliente());
						cotizacionDTO.setNombreContratante(nombreCliente);
						cotizacionDTO.setIdDomicilioContratante(clienteDTO.getIdDomicilio());
					}
					else if (clienteForm.getCodigoPersona().equals("3")){
						cotizacionDTO.setIdToPersonaAsegurado(clienteDTO.getIdCliente());
						cotizacionDTO.setNombreAsegurado(nombreCliente);
						cotizacionDTO.setIdDomicilioAsegurado(clienteDTO.getIdDomicilio());
					}
					CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
				}				
	    	}else{
	    		poblarClienteDTO(clienteForm, clienteDTO);
				BigDecimal idCliente = ClienteDN.getInstancia().agregar(clienteDTO, usuario.getNombreUsuario());
				
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.getPorId(UtileriasWeb.regresaBigDecimal(clienteForm.getIdPadre()));
				String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	            nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	            nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";

				if (clienteForm.getCodigoPersona() != null){
					// 1 = Contratante ; 3 = asegurado
					if (clienteForm.getCodigoPersona().equals("1")){
						cotizacionDTO.setIdToPersonaContratante(idCliente);
						cotizacionDTO.setNombreContratante(nombreCliente);
//						cotizacionDTO.setIdDomicilioContratante(clienteDTO.getIdDomicilio());
					}
					else if (clienteForm.getCodigoPersona().equals("3")){
						cotizacionDTO.setIdToPersonaAsegurado(idCliente);
						cotizacionDTO.setNombreAsegurado(nombreCliente);
//						cotizacionDTO.setIdDomicilioAsegurado(clienteDTO.getIdDomicilio());
					}
					CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
				}
	    		
	    	}		    
		} catch (ExcepcionDeAccesoADatos e) {
			errorAlGuardar = Boolean.TRUE;
		} catch (Exception e) {
			errorAlGuardar = Boolean.TRUE;
		}
		if (clienteForm.getCodigoPersona().equals("1"))
			url += "?idPadre="+clienteForm.getIdPadre()+"&descripcionPadre=cotizacion&tipoDireccion=1";
		else if (clienteForm.getCodigoPersona().equals("3"))
			url += "?idPadre="+clienteForm.getIdPadre()+"&descripcionPadre=cotizacion&tipoDireccion=3";
		if (errorAlGuardar){
			url += "&mensaje=Ocurrio un error al intentar guardar al cliente.&tipoMensaje="+Sistema.ERROR;
		} else{
			url += "&mensaje=Los datos se guardaron correctamente.&tipoMensaje="+Sistema.EXITO;
		}
		forward.setPath(url);
		return forward;
	}
	
	public static void poblarClienteForm(ClienteDTO clienteDTO,ClienteForm clienteForm){
		if (clienteDTO != null && clienteForm != null){
			if(clienteDTO.getIdCliente() != null)
				clienteForm.setIdCliente(clienteDTO.getIdCliente().toString());
			if (clienteDTO.getIdToPersona()!= null)
				clienteForm.setIdToPersona(clienteDTO.getIdToPersona().toString());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombre()))
				clienteForm.setNombre(clienteDTO.getNombre());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getApellidoPaterno()))
				clienteForm.setApellidoPaterno(clienteDTO.getApellidoPaterno());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getApellidoMaterno()))
				clienteForm.setApellidoMaterno(clienteDTO.getApellidoMaterno());
			if(clienteDTO.getClaveTipoPersona() != null){
				clienteForm.setClaveTipoPersona(clienteDTO.getClaveTipoPersona().toString());
				if (clienteForm.getClaveTipoPersona().equals("1")){
					clienteForm.setDescripcionTipoPersona("Persona f�sica");
				}
				else if (clienteForm.getClaveTipoPersona().equals("2")){
					clienteForm.setDescripcionTipoPersona("Persona moral");
				}
			}
			Format formatter=null;
			if ( clienteDTO.getFechaNacimiento() != null){
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				clienteForm.setFechaNacimiento(formatter.format(clienteDTO.getFechaNacimiento()));
			}
			if ( clienteDTO.getFechaNacimientoRepresentante() != null){
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				clienteForm.setFechaNacimientoRepresentante(formatter.format(clienteDTO.getFechaNacimientoRepresentante()));
			}			
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getCodigoRFC()))
				clienteForm.setCodigoRFC(clienteDTO.getCodigoRFC());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getTelefono()))
				clienteForm.setTelefono(clienteDTO.getTelefono());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getEmail()))
				clienteForm.setEmail(clienteDTO.getEmail());
			if (clienteDTO.getIdToDireccion() != null)
				clienteForm.setIdToDireccion(clienteDTO.getIdToDireccion().toString());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreCalle()))
				clienteForm.setNombreCalle(clienteDTO.getNombreCalle());

			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreColonia())){
				clienteForm.setNombreColonia(clienteDTO.getNombreColonia());
			}
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreDelegacion()))
				clienteForm.setNombreDelegacion(clienteDTO.getNombreDelegacion());
			if (clienteDTO.getIdMunicipio() != null)
				clienteForm.setIdMunicipio(clienteDTO.getIdMunicipio().toString());
			if (clienteDTO.getIdEstado() != null){
				clienteForm.setIdEstado(clienteDTO.getIdEstado().toString());
			}
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getDescripcionEstado())){
				clienteForm.setDescripcionEstado(clienteDTO.getDescripcionEstado());
			}
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreDelegacion()))
				clienteForm.setNombreDelegacion(clienteDTO.getNombreDelegacion());
			if (clienteDTO.getCodigoPostal() != null)
				clienteForm.setCodigoPostal(clienteDTO.getCodigoPostal().toString());

			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getCodigoCURP()))
				clienteForm.setCodigoCURP(clienteDTO.getCodigoCURP());
			if (clienteDTO.getIdEstadoNacimiento() != null)
				clienteForm.setEstadoNacimiento(clienteDTO.getIdEstadoNacimiento().toString());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreRepresentante()))
				clienteForm.setNombreRepresentante(clienteDTO.getNombreRepresentante());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getSexo()))
				clienteForm.setSexo(clienteDTO.getSexo());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getEstadoNacimiento()))
				clienteForm.setDescripcionEstadoNacimiento(clienteDTO.getEstadoNacimiento());
			
		}
	}

	public static void poblarClienteFormEditable(ClienteDTO clienteDTO,ClienteForm clienteForm){
		if (clienteDTO != null && clienteForm != null){
			if(clienteDTO.getIdCliente() != null)
				clienteForm.setIdCliente(clienteDTO.getIdCliente().toString());
			if(clienteDTO.getIdDomicilio() != null)
				clienteForm.setIdDomicilio(clienteDTO.getIdDomicilio().toString());			
			if (clienteDTO.getIdToPersona()!= null)
				clienteForm.setIdToPersona(clienteDTO.getIdToPersona().toString());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombre()))
				clienteForm.setNombre(clienteDTO.getNombre());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getApellidoPaterno()))
				clienteForm.setApellidoPaterno(clienteDTO.getApellidoPaterno());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getApellidoMaterno()))
				clienteForm.setApellidoMaterno(clienteDTO.getApellidoMaterno());
			if(clienteDTO.getClaveTipoPersona() != null){
				clienteForm.setClaveTipoPersona(clienteDTO.getClaveTipoPersona().toString());
				if (clienteForm.getClaveTipoPersona().equals("1")){
					clienteForm.setDescripcionTipoPersona("Persona f�sica");
				}
				else if (clienteForm.getClaveTipoPersona().equals("2")){
					clienteForm.setDescripcionTipoPersona("Persona moral");
				}
			}
			Format formatter=null;
			if ( clienteDTO.getFechaNacimiento() != null){
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				clienteForm.setFechaNacimiento(formatter.format(clienteDTO.getFechaNacimiento()));
			}
			if ( clienteDTO.getFechaNacimientoRepresentante() != null){
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				clienteForm.setFechaNacimientoRepresentante(formatter.format(clienteDTO.getFechaNacimientoRepresentante()));
			}					
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getCodigoRFC()))
				clienteForm.setCodigoRFC(clienteDTO.getCodigoRFC());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getTelefono()))
				clienteForm.setTelefono(clienteDTO.getTelefono());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getEmail()))
				clienteForm.setEmail(clienteDTO.getEmail());
			if (clienteDTO.getIdToDireccion() != null)
				clienteForm.setIdToDireccion(clienteDTO.getIdToDireccion().toString());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreCalle()))
				clienteForm.setNombreCalle(clienteDTO.getNombreCalle());

			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getDescripcionEstado())){
				try {
					EstadoFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(EstadoFacadeRemote.class);
					List<EstadoDTO> estado = beanRemoto.findByProperty("stateName", clienteDTO.getDescripcionEstado());
					if(!estado.isEmpty()) {
						clienteForm.setIdEstado(estado.get(0).getStateId());
					}
				} catch (SystemException e) {
				}
			}
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreDelegacion())) {
				try {
					CiudadFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(CiudadFacadeRemote.class);
					if(!UtileriasWeb.esCadenaVacia(clienteForm.getIdEstado())) {
						List<CiudadDTO> ciudad = beanRemoto.findByCityName(UtileriasWeb.regresaBigDecimal(clienteForm.getIdEstado()), clienteDTO.getNombreDelegacion());
						if(!ciudad.isEmpty()) {
							clienteForm.setIdMunicipio(ciudad.get(0).getCityId());
						}
					}
				} catch (SystemException e) {
				}
			}
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreColonia())){
				String nombreColonia = clienteDTO.getNombreColonia();
				try {
					ColoniaFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(ColoniaFacadeRemote.class);
					if(clienteDTO.getCodigoPostal() != null && !UtileriasWeb.esCadenaVacia(clienteForm.getIdMunicipio())) {
						clienteForm.setCodigoPostal(clienteDTO.getCodigoPostal().trim());
						List<ColoniaDTO> colonia = beanRemoto.findByColonyName(UtileriasWeb.regresaBigDecimal(clienteForm.getIdMunicipio()), UtileriasWeb.regresaBigDecimal(clienteForm.getCodigoPostal()), nombreColonia);
						if(!colonia.isEmpty()) {
							clienteForm.setNombreColonia(colonia.get(0).getColonyId());
						}
					}
				} catch (SystemException e) {
				}
			}
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getCodigoCURP()))
				clienteForm.setCodigoCURP(clienteDTO.getCodigoCURP());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getEstadoNacimiento())){
				try {
					EstadoFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(EstadoFacadeRemote.class);
					List<EstadoDTO> estado = beanRemoto.findByProperty("stateName", clienteDTO.getEstadoNacimiento());
					if(!estado.isEmpty()) {
						clienteForm.setIdEstadoNacimiento(estado.get(0).getStateId());
					}
				} catch (SystemException e) {
				}
			}			
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreRepresentante()))
				clienteForm.setNombreRepresentante(clienteDTO.getNombreRepresentante());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getSexo()))
				clienteForm.setSexo(clienteDTO.getSexo());
			if (!UtileriasWeb.esCadenaVacia(clienteDTO.getEstadoNacimiento()))
				clienteForm.setDescripcionEstadoNacimiento(clienteDTO.getEstadoNacimiento());		
		}
	}

	@SuppressWarnings("deprecation")
	public static void poblarClienteDTO(ClienteForm clienteForm,ClienteDTO clienteDTO){
		if (clienteDTO != null && clienteForm != null){
			if(clienteForm.getIdCliente() != null){
				try {
					clienteDTO.setIdCliente(UtileriasWeb.regresaBigDecimal(clienteForm.getIdCliente()));
				} catch (SystemException e) {	}
			}
			if(clienteForm.getIdDomicilio() != null && !clienteForm.getIdDomicilio().equals("")){
				try {
					clienteDTO.setIdDomicilio(UtileriasWeb.regresaBigDecimal(clienteForm.getIdDomicilio()));
				} catch (SystemException e) {	}
			}			
			if (clienteForm.getIdToPersona()!= null){
				try {
					clienteDTO.setIdToPersona(UtileriasWeb.regresaBigDecimal(clienteForm.getIdToPersona()));
				} catch (SystemException e) {}
			}
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getNombre())){
				clienteDTO.setNombre(clienteForm.getNombre());
			}
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getApellidoPaterno()))
				clienteDTO.setApellidoPaterno(clienteForm.getApellidoPaterno());
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getApellidoMaterno()))
				clienteDTO.setApellidoMaterno(clienteForm.getApellidoMaterno());
			// 1 = fisica ; 2 = moral
			if(clienteForm.getClaveTipoPersona() != null){
				clienteDTO.setClaveTipoPersona(UtileriasWeb.regresaShort(clienteForm.getClaveTipoPersona()));
				if (!UtileriasWeb.esCadenaVacia(clienteForm.getCodigoRFC())){
        				if(clienteForm.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_FISICA)){
        				    clienteDTO.setCodigoRFC(clienteForm.getCodigoRFC().trim().toUpperCase());
        				}else if(clienteForm.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_MORAL)){
        				    clienteDTO.setCodigoRFC(clienteForm.getCodigoRFC().trim().toUpperCase());
        				}
				}
			}
			if ( clienteForm.getFechaNacimiento() != null){
				String datos[] = clienteForm.getFechaNacimiento().split("/");
				Date fecha = new Date();
				fecha.setDate(Integer.valueOf(datos[0]));
				fecha.setMonth(Integer.valueOf(datos[1])-1);
				fecha.setYear(Integer.valueOf(datos[2])-1900);
				java.sql.Date sqlFecha = new java.sql.Date(fecha.getTime());
				clienteDTO.setFechaNacimiento(sqlFecha);
			}
			if(!UtileriasWeb.esCadenaVacia(clienteForm.getFechaNacimientoRepresentante())){
				String datos[] = clienteForm.getFechaNacimientoRepresentante().split("/");
				Date fecha = new Date();
				fecha.setDate(Integer.valueOf(datos[0]));
				fecha.setMonth(Integer.valueOf(datos[1])-1);
				fecha.setYear(Integer.valueOf(datos[2])-1900);
				java.sql.Date sqlFecha = new java.sql.Date(fecha.getTime());
				clienteDTO.setFechaNacimientoRepresentante(sqlFecha);
			}
			
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getTelefono()))
				clienteDTO.setTelefono(clienteForm.getTelefono());
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getEmail()))
				clienteDTO.setEmail(clienteForm.getEmail());
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getNombreCalle()))
				clienteDTO.setNombreCalle(clienteForm.getNombreCalle());
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getNombreColonia())){
				String claveColonia = clienteForm.getNombreColonia();
				String descripcionColonia;
				BigDecimal idColonia=null;
				try{
					ColoniaFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(ColoniaFacadeRemote.class);
					ColoniaDTO colonia = beanRemoto.findById(claveColonia);
					descripcionColonia = colonia.getColonyName();
					idColonia = new BigDecimal(colonia.getZipCodeUserId());
				}catch(Exception e){	descripcionColonia = claveColonia;}
				clienteDTO.setNombreColonia(descripcionColonia);
				clienteDTO.setIdColonia(idColonia);
			}
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getNombreDelegacion()))
				clienteDTO.setNombreDelegacion(clienteForm.getNombreDelegacion());
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getIdMunicipio())){
				try {	clienteDTO.setIdMunicipio(UtileriasWeb.regresaBigDecimal(clienteForm.getIdMunicipio()));
				} catch (SystemException e) {}
			}
			if (clienteForm.getIdEstado() != null){
				try {clienteDTO.setIdEstado(UtileriasWeb.regresaBigDecimal(clienteForm.getIdEstado()));
				} catch (SystemException e) {}
			}
			if (clienteForm.getCodigoPostal() != null){
				clienteDTO.setCodigoPostal(clienteForm.getCodigoPostal());
			}
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getCodigoCURP())){
				clienteDTO.setCodigoCURP(clienteForm.getCodigoCURP());
			}
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getIdEstadoNacimiento())){
				try {
					clienteDTO.setIdEstadoNacimiento(UtileriasWeb.regresaBigDecimal(clienteForm.getIdEstadoNacimiento()));
				} catch (SystemException e) {}
			}				
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getNombreRepresentante())){
				clienteDTO.setNombreRepresentante(clienteForm.getNombreRepresentante());
			}	
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getSexo()))
				clienteDTO.setSexo(clienteForm.getSexo());		
		}
	}
	
	/**
	 * Genera un String que contiene el nombre completo del clienteForm recibido.
	 * El m�todo valida si el cliente es una persona f�sica o moral y devuelve la cadena correcta.
	 * @param clienteForm
	 * @return String nombre Completo del cliente.
	 */
	public static String obtenerNombreCliente(ClienteDTO clienteDTO){
		String descripcionCliente = "";
		if (clienteDTO != null){
			if (clienteDTO.getClaveTipoPersona().intValue() == 1){
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombre()))? clienteDTO.getNombre()+" " : "";
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO.getApellidoPaterno()))? clienteDTO.getApellidoPaterno()+" " : "";
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO.getApellidoMaterno()))? clienteDTO.getApellidoMaterno()+" " : "";
			}
			else if (clienteDTO.getClaveTipoPersona().intValue() == 2){
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombre()))? clienteDTO.getNombre()+" " : "";
			}
		}
		return descripcionCliente;
	}
	
	/**
	 * Genera un String que contiene la descripci�n de la direcci�n del cliente recibido.
	 * La descripci�n se genera con los campos nombreCalle, descripcionColonia y descripcionEstado.
	 * @param clienteForm
	 * @return
	 */
	public static String obtenerDireccionCliente(ClienteDTO clienteDTO){
		String descripcionDireccion = "";
		if (clienteDTO != null){
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreCalle()))? clienteDTO.getNombreCalle().toUpperCase()+", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreColonia()))? " COL. "+clienteDTO.getNombreColonia().toUpperCase()+", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO.getNombreDelegacion()))? clienteDTO.getNombreDelegacion().toUpperCase()+", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO.getDescripcionEstado()))? clienteDTO.getDescripcionEstado().toUpperCase()+"" : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO.getCodigoPostal()))? ", C.P. "+clienteDTO.getCodigoPostal().toUpperCase()+" " : "";
		}
		return descripcionDireccion;
	}
	
	
	public String validaRFC(ClienteForm clienteForm){
	    String error=null;
	    if(clienteForm.getClaveTipoPersona() != null){
		
			if (!UtileriasWeb.esCadenaVacia(clienteForm.getCodigoRFC())){
				if(clienteForm.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_FISICA)){
				if(!UtileriasWeb.validarRfcPersonaFisica(clienteForm.getCodigoRFC().toUpperCase())){
				    error= "&mensaje=El RFC para persona fisica no es valido,verifique.&tipoMensaje="+Sistema.ERROR;
				}
					
				}else if(clienteForm.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_MORAL)){
				    if(!UtileriasWeb.validarRfcPersonaMoral(clienteForm.getCodigoRFC().toUpperCase())){
					 error= "&mensaje=El RFC para persona moral no es valido,verifique.&tipoMensaje="+Sistema.ERROR;
				    }
				  
				}
			}
	
	    }
	return error;
	}
	
	public ActionForward listarClientePorCURP(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String tipoPersona = request.getParameter("tipo");
		RespuestaXMLForm respuestaXMLForm = (RespuestaXMLForm) form; 
		String curp = request.getParameter("curp");
		String cotizacion = request.getParameter("cotizacion");
		String codigoCliente = request.getParameter("codigoCliente");
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			if(!UtileriasWeb.esCadenaVacia(tipoPersona) && !UtileriasWeb.esCadenaVacia(curp)){
				respuestaXMLForm.setIdOriginal(cotizacion);
				respuestaXMLForm.setIdResultado(codigoCliente);
				respuestaXMLForm.setResultados(ClienteDN.getInstancia().buscarPorCURP(tipoPersona, curp, usuario));
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}	

	public void buscarCoincidenciasPorCURP(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String tipoPersona = request.getParameter("tipo");
		String curp = request.getParameter("curp");
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		int encontrados = 0;
		try {
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			List<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
			if(!UtileriasWeb.esCadenaVacia(tipoPersona) && !UtileriasWeb.esCadenaVacia(curp)){
				clientes = ClienteDN.getInstancia().buscarPorCURP(tipoPersona, curp, usuario);
				if(clientes != null)
					encontrados = clientes.size();
			}
			buffer.append("<item>");
			
			buffer.append("<id>");
			buffer.append(encontrados);
			buffer.append("</id>");
			if(encontrados == 1){
				buffer.append("<cliente>");
				buffer.append(clientes.get(0).getIdCliente());
				buffer.append("</cliente>");
				buffer.append("<domicilio>");
				buffer.append(clientes.get(0).getIdDomicilio());
				buffer.append("</domicilio>");				
			}else{
				buffer.append("<cliente>");
				buffer.append(0);
				buffer.append("</cliente>");				
			}
			buffer.append("</item>");							
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}
	
	public ActionForward mostrarClientes(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}	
}
