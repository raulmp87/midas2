package mx.com.afirme.midas.cotizacion.primerriesgoluc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.lloyds.LloydsDN;
import mx.com.afirme.midas.catalogos.lloyds.LloydsDTO;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionSN;
import mx.com.afirme.midas.cotizacion.calculo.CalculoLUCDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionSN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.commons.math.MathException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math.analysis.interpolation.UnivariateRealInterpolator;

public class PrimerRiesgoLUCDN {
	private static final PrimerRiesgoLUCDN INSTANCIA = new PrimerRiesgoLUCDN();
	private static final NumberFormat fCuota = Sistema.FORMATO_CUOTA;
	private static final NumberFormat fMonto = Sistema.FORMATO_MONEDA;
	
	public static PrimerRiesgoLUCDN getInstancia() {
		return PrimerRiesgoLUCDN.INSTANCIA;
	}
	public String calcularLUC(BigDecimal idToCotizacion,
			List<SeccionCotizacionDTO> secciones, String nombreUsuario)throws ExcepcionDeAccesoADatos, SystemException{
		StringBuilder mensaje = new StringBuilder("");
		PrimerRiesgoLUCSN primerRiesgoLUCSN = new PrimerRiesgoLUCSN();
		CalculoCotizacionSN calculoCotizacionSN = new CalculoCotizacionSN();
		if (secciones != null && secciones.size() > 0) {
			CoberturaCotizacionSN coberturaCotizacionSN = new  CoberturaCotizacionSN();
			
			for (SeccionCotizacionDTO seccion : secciones) {
				SeccionDTO seccionDTO = SeccionDN.getInstancia().getPorId(seccion.getId().getIdToSeccion());
				if (seccion.getClaveContrato() != null) {
					//Actualizar las secciones que fueron a LUC
					AgrupacionCotDTO agrupacionCotDTO = new AgrupacionCotDTO();
					AgrupacionCotId id = new AgrupacionCotId();
					agrupacionCotDTO = primerRiesgoLUCSN.buscarPorCotizacionSeccion(idToCotizacion, Sistema.TIPO_LUC ,seccion.getId().getIdToSeccion());
					//borrar si existe alguna agrupacion x LUC;
					DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
					RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
					CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();	
					
					List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN.listarCoberturasContratadasPorSeccion(idToCotizacion, seccion.getId().getIdToSeccion());
					
					if (agrupacionCotDTO != null){
						primerRiesgoLUCSN.borrar(agrupacionCotDTO);
						for(CoberturaCotizacionDTO cobertura: coberturas){
							List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN.getInstancia().listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_LUC, Boolean.TRUE);
							for (DescuentoRiesgoCotizacionDTO descuento: descuentos){
								descuento.setValorDescuento(0d);
								descuentoRiesgoCotizacionSN.modificar(descuento);																	
							}	
							List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN.getInstancia().listarRecargosRiesgoPorCoberturaCotizacion(cobertura, Sistema.RECARGO_LUC, Boolean.TRUE);
							for(RecargoRiesgoCotizacionDTO recargo: recargos){
								recargo.setValorRecargo(0d);
								recargoRiesgoCotizacionSN.modificar(recargo);
							}
							calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);	
						}
					}else{
						agrupacionCotDTO = new AgrupacionCotDTO();
					}
					//se calcula el LUC
					CalculoLUCDTO calculoLUCDTO = new CalculoLUCDTO();
					calculoLUCDTO.setIdToCotizacion(idToCotizacion);
					calculoLUCDTO.setIdToSeccion(seccion.getId().getIdToSeccion());
					calculoLUCDTO.setSumaAsegurada(seccion.getValorSumaAsegurada());
					calculoLUCDTO = calculoCotizacionSN.calcularLUC(calculoLUCDTO, nombreUsuario);
					if(calculoLUCDTO == null) {
						mensaje.append("Error al registrar el LUC de la seccion: ").append(seccionDTO.getNombreComercial());
					} else {
						Short numeroAgrupacion = primerRiesgoLUCSN.obtenerNumeroAgrupacion(idToCotizacion);
						id.setNumeroAgrupacion(numeroAgrupacion);
						id.setIdToCotizacion(idToCotizacion);
						agrupacionCotDTO.setClaveTipoAgrupacion(Sistema.TIPO_LUC);
						agrupacionCotDTO.setIdToSeccion(seccion.getId().getIdToSeccion());
						agrupacionCotDTO.setValorCuota(calculoLUCDTO.getCuotaLUC());
						agrupacionCotDTO.setValorPrimaneta(calculoLUCDTO.getPrimaNetaLUC());
						agrupacionCotDTO.setValorSumaAsegurada(seccion.getValorSumaAsegurada());
						agrupacionCotDTO.setId(id);						

						if(this.igualarPrimasPrimerRiesgoLUCV2(coberturas, calculoLUCDTO.getPrimaNetaLUC().doubleValue(), Sistema.DESCUENTO_LUC,
								Sistema.RECARGO_LUC, nombreUsuario, Boolean.TRUE)) {
							primerRiesgoLUCSN.agregar(agrupacionCotDTO);
							for(CoberturaCotizacionDTO cobertura: coberturas){
								cobertura.setNumeroAgrupacion(numeroAgrupacion);
								coberturaCotizacionSN.modificar(cobertura);
							}	
							mensaje.append("<li>Se Registro correctamente el LUC. para la seccion: ").append(seccionDTO.getNombreComercial()).append(" La Prima Neta es: " ).append(fMonto.format(calculoLUCDTO.getPrimaNetaLUC().doubleValue())).append(" , con una Cuota Promedio de: ").append(fCuota.format(calculoLUCDTO.getCuotaLUC().doubleValue())).append("</br>");
						} else {
							mensaje.append("<li>No se ha podido registrar el LUC. para la seccion: ").append(seccionDTO.getNombreComercial()).append("</br>");
						}
					}

					/*Double primaNetaReal = 0D;
					//Actualiza las coberturas que fueron a LUC
					for(CoberturaCotizacionDTO cobertura: coberturas){
						cobertura.setNumeroAgrupacion(numeroAgrupacion);
						coberturaCotizacionSN.modificar(cobertura);
						primaNetaReal +=cobertura.getValorPrimaNeta();
					}					
					
					//Obtener el descuento aplicable
					BigDecimal descuentoAplicable = BigDecimal.ONE.subtract(BigDecimal.valueOf(calculoLUCDTO.getPrimaNetaLUC().doubleValue()).divide(BigDecimal.valueOf(primaNetaReal.doubleValue()), 10, BigDecimal.ROUND_HALF_UP)); 
					
					//Se aplican el descuento a los riesgo
					
					for(CoberturaCotizacionDTO cobertura: coberturas){
						//aplicar Descuentos a los riesgos de las coberturas.
						if(descuentoAplicable.doubleValue()> 0){
							List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN.getInstancia().listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_LUC, Boolean.TRUE);
							for (DescuentoRiesgoCotizacionDTO descuento: descuentos){
								descuento.setValorDescuento(descuentoAplicable.abs().multiply(BigDecimal.valueOf(100)).doubleValue());
								descuentoRiesgoCotizacionSN.modificar(descuento);																	
							}							
						}else{
							List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN.getInstancia().listarRecargosRiesgoPorCoberturaCotizacion(cobertura, Sistema.RECARGO_LUC, Boolean.TRUE);
							for(RecargoRiesgoCotizacionDTO recargo: recargos){
								recargo.setValorRecargo(descuentoAplicable.abs().multiply(BigDecimal.valueOf(100)).doubleValue());
								recargoRiesgoCotizacionSN.modificar(recargo);
							}
						}
						//Recalcular las coberturas Afectadas
						calculoCotizacionDN.calcularARD(cobertura.getId());					
					}*/
				}
			}			
		}
		return mensaje.toString();
	}

//	private Boolean igualarPrimasPrimerRiesgoLUC(
//			List<CoberturaCotizacionDTO> coberturas, Double primaNeta,
//			BigDecimal idToDescuentoEspecial, BigDecimal idToRecargoEspecial,
//			String nombreUsuario, Boolean isLUC) throws SystemException {
//		BigDecimal total = BigDecimal.ZERO;
//		BigDecimal totalARDT = BigDecimal.ZERO;
//		BigDecimal primaModificar = BigDecimal.ZERO;
//		BigDecimal primaFueraIgualacion = BigDecimal.ZERO;
//		BigDecimal factorIgualacion = BigDecimal.ZERO;
//		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
//		for (CoberturaCotizacionDTO cobertura : coberturas) {
//			Double primaARDT = DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId());
//			totalARDT = totalARDT.add(BigDecimal.valueOf(primaARDT));
//
//			Double prima = DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId());
//			total = total.add(BigDecimal.valueOf(prima));
//
//			if ((cobertura.getClaveFacultativo() == null || cobertura.getClaveFacultativo() == 0)
//					&& (isLUC || cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClavePrimerRiesgo() == 1)) {
//				primaModificar = primaModificar.add(BigDecimal.valueOf(primaARDT));
//			}
//		}
//		primaFueraIgualacion = totalARDT.subtract(primaModificar);
//		BigDecimal diferencia = total.subtract(BigDecimal.valueOf(primaNeta));
//		BigDecimal valorRD = diferencia.multiply(BigDecimal.valueOf(100D)).divide(totalARDT.subtract(primaFueraIgualacion), 12, RoundingMode.HALF_UP);
//		if(valorRD.doubleValue() > 0D) {
//			factorIgualacion = BigDecimal.ONE.subtract(valorRD.divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).abs());
//		} else {
//			factorIgualacion = BigDecimal.ONE.add(valorRD.divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).abs());
//		}
//		valorRD = valorRD.abs();
//		if (factorIgualacion.doubleValue() <= 0D) {
//			return Boolean.FALSE;
//		}
//		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
//		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
//		if (factorIgualacion.doubleValue() > 0D) {
//			if (factorIgualacion.doubleValue() - 1D > 0D) {
//				for (CoberturaCotizacionDTO cobertura : coberturas) {
//					if((cobertura.getClaveFacultativo() == null || cobertura.getClaveFacultativo() == 0) 
//							&& (isLUC || cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClavePrimerRiesgo() == 1)) {
//						List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN
//								.listarRecargosRiesgoPorCoberturaCotizacion(
//										cobertura,
//										idToRecargoEspecial,
//										Boolean.FALSE);
//						for (RecargoRiesgoCotizacionDTO recargo : recargos) {
//							recargo.setValorRecargo(valorRD.doubleValue());
//							recargo.setClaveContrato((short) 1);
//							recargoRiesgoCotizacionSN.modificar(recargo);
//						}
//						List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
//								.listarDescuentosRiesgoPorCoberturaCotizacion(
//										cobertura,
//										idToDescuentoEspecial,
//										Boolean.FALSE);
//						for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
//							descuento.setValorDescuento(0D);
//							descuento.setClaveContrato((short) 0);
//							descuentoRiesgoCotizacionSN.modificar(descuento);
//						}
//					}
//				}
//			} else if (factorIgualacion.doubleValue() - 1D < 0D) {
//				for (CoberturaCotizacionDTO cobertura : coberturas) {
//					if((cobertura.getClaveFacultativo() == null || cobertura.getClaveFacultativo() == 0) 
//							&& (isLUC || cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClavePrimerRiesgo() == 1)) {
//						List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
//								.listarDescuentosRiesgoPorCoberturaCotizacion(
//										cobertura,
//										idToDescuentoEspecial,
//										Boolean.FALSE);
//						for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
//							descuento.setValorDescuento(valorRD.doubleValue());
//							descuento.setClaveContrato((short) 1);
//							descuentoRiesgoCotizacionSN.modificar(descuento);
//						}
//						List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN
//								.listarRecargosRiesgoPorCoberturaCotizacion(
//										cobertura,
//										idToRecargoEspecial,
//										Boolean.FALSE);
//						for (RecargoRiesgoCotizacionDTO recargo : recargos) {
//							recargo.setValorRecargo(0D);
//							recargo.setClaveContrato((short) 0);
//							recargoRiesgoCotizacionSN.modificar(recargo);
//						}
//					}
//				}
//			}
//		}
//		for (CoberturaCotizacionDTO cobertura : coberturas) {
//			if ((cobertura.getClaveFacultativo() == null || cobertura.getClaveFacultativo() == 0)
//					&& (isLUC || cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClavePrimerRiesgo() == 1)) {
//				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
//				calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
//			}
//		}
//		return Boolean.TRUE;
//	}
	
	private Boolean igualarPrimasPrimerRiesgoLUCV2(List<CoberturaCotizacionDTO> coberturas, Double primaNeta,
			BigDecimal idToDescuentoEspecial, BigDecimal idToRecargoEspecial,
			String nombreUsuario, Boolean isLUC) throws SystemException {
		
		Map<CoberturaCotizacionId,BigDecimal> mapaPrimaARDTPorCobertura = new HashMap<CoberturaCotizacionId, BigDecimal>();
		Map<CoberturaCotizacionId,BigDecimal> mapaPrimaPorCobertura = new HashMap<CoberturaCotizacionId, BigDecimal>();
		
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal totalARDT = BigDecimal.ZERO;
		BigDecimal primaModificar = BigDecimal.ZERO;
		BigDecimal primaFueraIgualacion = BigDecimal.ZERO;
		BigDecimal primaIgualar = BigDecimal.ZERO;
//		BigDecimal factorIgualacion = BigDecimal.ZERO;
		
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
		
			BigDecimal primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			
			if(primaARDT.compareTo(BigDecimal.ZERO) == 0){
				//Si la prima es cero, se manda recalcular la cobertura debido a que los detalles de prima son inconsistentes.
				CalculoCotizacionDN.getInstancia().calcularCobertura(cobertura, nombreUsuario, true);
				
				primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			}
			
			mapaPrimaARDTPorCobertura.put(cobertura.getId(), primaARDT);
			totalARDT = totalARDT.add(primaARDT);

			BigDecimal prima = new BigDecimal (DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId()) );
			mapaPrimaPorCobertura.put(cobertura.getId(), prima);
			total = total.add(prima);

			if (permitePrimerRiesgoLUC(cobertura,isLUC)) {
				primaModificar = primaModificar.add(primaARDT);
			}
		}
		
		primaFueraIgualacion = total.subtract(primaModificar);
		
		primaIgualar = new BigDecimal(primaNeta).subtract(primaFueraIgualacion);

		if(new BigDecimal(primaNeta).compareTo(primaFueraIgualacion) <= 0){
			return Boolean.FALSE;
		}
		
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			if (permitePrimerRiesgoLUC(cobertura,isLUC) ) {
				
				BigDecimal proporcion = BigDecimal.ZERO;
				BigDecimal primaAQuedar = BigDecimal.ZERO;
				BigDecimal porcentajeNecesario = BigDecimal.ZERO;
				BigDecimal otrosARDs= BigDecimal.ZERO;
				BigDecimal porcentajeAplicable = BigDecimal.ZERO;
				
				//Si la cobertura permite igualacion
				BigDecimal prima = mapaPrimaPorCobertura.get(cobertura.getId());
				if(prima == null){
					prima = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId()));
				}
				
				BigDecimal primaARDT = mapaPrimaARDTPorCobertura.get(cobertura.getId());
				if(primaARDT == null){
					primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
				}
				
				proporcion = prima.divide(primaModificar, 12, RoundingMode.HALF_UP);
				
				primaAQuedar = primaIgualar.multiply(proporcion);
				
				porcentajeNecesario = BigDecimal.ONE.subtract(primaAQuedar.divide(primaARDT, 12, RoundingMode.HALF_UP));
				
				List<RecargoRiesgoCotizacionDTO> listaRecargos = coberturaCotizacionDN.listarRecargosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
				List<DescuentoRiesgoCotizacionDTO> listaDescuentos = coberturaCotizacionDN.listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
				List<AumentoRiesgoCotizacionDTO> listaAumentos = coberturaCotizacionDN.listarAumentosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
				
				List<BigDecimal> listaAumentosAcumulados = new ArrayList<BigDecimal>();
				List<BigDecimal> listaRecargosAcumulados = new ArrayList<BigDecimal>();
				List<BigDecimal> listaDescuentosAcumulados = new ArrayList<BigDecimal>();
				
//				BigDecimal otrosARDS = BigDecimal.ZERO;
				BigDecimal sumatoriaRecargos = BigDecimal.ZERO;
				BigDecimal sumatoriaDescuentos = BigDecimal.ZERO;
				BigDecimal sumatoriaAumentos = BigDecimal.ZERO;
				
				for(RecargoRiesgoCotizacionDTO recargo : listaRecargos){
					if(recargo.getId().getIdToRecargoVario().compareTo(idToRecargoEspecial) != 0 &&
							recargo.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
						//Se excluyen recargos por igualaci�n
						if(!listaRecargosAcumulados.contains(recargo.getId().getIdToRecargoVario())){
							//Cada recargo s�lo se incluye una vez en el acumulado.
							listaRecargosAcumulados.add(recargo.getId().getIdToRecargoVario());
							sumatoriaRecargos = sumatoriaRecargos.add(new BigDecimal(recargo.getValorRecargo()));
						}
					}
				}
				
				for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
					if(descuento.getId().getIdToDescuentoVario().compareTo(idToDescuentoEspecial) != 0 &&
							descuento.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
						//Se excluyen recargos por igualaci�n
						if(!listaDescuentosAcumulados.contains(descuento.getId().getIdToDescuentoVario())){
							//Cada recargo s�lo se incluye una vez en el acumulado.
							listaDescuentosAcumulados.add(descuento.getId().getIdToDescuentoVario());
							sumatoriaDescuentos = sumatoriaDescuentos.add(new BigDecimal(descuento.getValorDescuento()));
						}
					}
				}
				
				for(AumentoRiesgoCotizacionDTO aumento : listaAumentos){
					if(aumento.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
						//No se registran aumentos por igualaci�n, no se valida el tipo de aumento
						if(!listaAumentosAcumulados.contains(aumento.getId().getIdToAumentoVario())){
							//Cada recargo s�lo se incluye una vez en el acumulado.
							listaAumentosAcumulados.add(aumento.getId().getIdToAumentoVario());
							sumatoriaAumentos = sumatoriaAumentos.add(new BigDecimal(aumento.getValorAumento()));
						}
					}
				}
				
				otrosARDs = sumatoriaAumentos.add(sumatoriaRecargos).subtract(sumatoriaDescuentos);
				//Convertir el monto a porcentaje
				
//				otrosARDs = otrosARDs.multiply(new BigDecimal("100")).divide(primaARDT, 12, RoundingMode.HALF_UP);
				
				porcentajeAplicable = otrosARDs.add(porcentajeNecesario.multiply(new BigDecimal("100")));
				
				List<DescuentoRiesgoCotizacionDTO> listaDescuentoRiesgoEspecial = obtenerDescuentoEspecial(listaDescuentos,idToDescuentoEspecial);
				List<RecargoRiesgoCotizacionDTO> listaRecargoRiesgoEspecial = obtenerRecargoEspecial(listaRecargos,idToRecargoEspecial);
				
				if(porcentajeAplicable.compareTo(BigDecimal.ZERO) > 0){
					//se aplica descuento
					for(DescuentoRiesgoCotizacionDTO descuentoRiesgoEspecial : listaDescuentoRiesgoEspecial){
						descuentoRiesgoEspecial.setValorDescuento(porcentajeAplicable.abs().doubleValue());
						descuentoRiesgoEspecial.setClaveContrato((short) 1);
						DescuentoRiesgoCotizacionDN.getInstancia().modificar(descuentoRiesgoEspecial);
					}
					for(RecargoRiesgoCotizacionDTO recargoRiesgoEspecial : listaRecargoRiesgoEspecial){
						recargoRiesgoEspecial.setValorRecargo(0d);
						recargoRiesgoEspecial.setClaveContrato((short) 0);
						RecargoRiesgoCotizacionDN.getInstancia().modificar(recargoRiesgoEspecial);
					}
					
				}
				else if (porcentajeAplicable.compareTo(BigDecimal.ZERO) < 0){
					//se aplica recargo
					for(DescuentoRiesgoCotizacionDTO descuentoRiesgoEspecial : listaDescuentoRiesgoEspecial){
						descuentoRiesgoEspecial .setValorDescuento(0d);
						descuentoRiesgoEspecial .setClaveContrato((short) 0);
						DescuentoRiesgoCotizacionDN.getInstancia().modificar(descuentoRiesgoEspecial );
					}
					for(RecargoRiesgoCotizacionDTO recargoRiesgoEspecial : listaRecargoRiesgoEspecial){
						recargoRiesgoEspecial.setValorRecargo(porcentajeAplicable.abs().doubleValue());
						recargoRiesgoEspecial.setClaveContrato((short) 1);
						RecargoRiesgoCotizacionDN.getInstancia().modificar(recargoRiesgoEspecial);
					}
				}
				
			}
			
		}
		
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			if (permitePrimerRiesgoLUC(cobertura,isLUC)) {
					CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
					calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
			}
		}
		
		
		return Boolean.TRUE;
	}
	
	private boolean permitePrimerRiesgoLUC(CoberturaCotizacionDTO cobertura,boolean isLUC){
		return (cobertura.getClaveFacultativo() == null || cobertura.getClaveFacultativo() == 0)
		&& (isLUC || cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClavePrimerRiesgo() == 1);
	}

	private List<DescuentoRiesgoCotizacionDTO> obtenerDescuentoEspecial(List<DescuentoRiesgoCotizacionDTO> listaDescuentos,BigDecimal idToDescuentoEspecial){
		List<DescuentoRiesgoCotizacionDTO> listaDescuentoRiesgoIgualacion = new ArrayList<DescuentoRiesgoCotizacionDTO>();
		
		for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
			if(descuento.getId().getIdToDescuentoVario().compareTo(idToDescuentoEspecial) == 0){
				listaDescuentoRiesgoIgualacion.add(descuento);
			}
		}
		return listaDescuentoRiesgoIgualacion;
	}
	
	private List<RecargoRiesgoCotizacionDTO> obtenerRecargoEspecial(List<RecargoRiesgoCotizacionDTO> listaRecargos,BigDecimal idToRecargoEspecial){
		List<RecargoRiesgoCotizacionDTO> listaRecargoRiesgoIgualacion = new ArrayList<RecargoRiesgoCotizacionDTO>();
		
		for(RecargoRiesgoCotizacionDTO recargo: listaRecargos){
			if(recargo.getId().getIdToRecargoVario().compareTo(idToRecargoEspecial) == 0){
				listaRecargoRiesgoIgualacion.add(recargo);
			}
		}
		return listaRecargoRiesgoIgualacion;
	}
	
	public String calcularPrimerRiesgo(BigDecimal idToCotizacion,
			List<SeccionCotizacionDTO> secciones, Double sumaAsegurada, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		String mensaje = "";
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		CoberturaCotizacionSN coberturaCotizacionSN = new  CoberturaCotizacionSN();
		
		StringBuilder seccionesCadenas = new StringBuilder("");
		String seccionesCadena = "";
		List<BigDecimal> seccionesSeleccionadas = new ArrayList<BigDecimal>();
		if (sumaAsegurada > 0D) {
			if (secciones != null && secciones.size() > 0) {
				for (SeccionCotizacionDTO seccion : secciones) {
					if (seccion.getClaveContrato() != null) {
						BigDecimal idToseccion = seccion.getSeccionDTO()
						.getIdToSeccion();
							seccionesCadenas.append(idToseccion.intValue());
							seccionesCadenas.append(",");
						seccionesSeleccionadas.add(idToseccion);
					}
				}
					seccionesCadena = seccionesCadenas.substring(0, seccionesCadenas.length() -1);
			}
			if (this.esValidaSumaAseguradaPrimerRiesgo(idToCotizacion,
					sumaAsegurada, seccionesCadena)) {
				
				//borrar si existe alguna agrupacion x primer riesgo;
				PrimerRiesgoLUCSN primerRiesgoLUCSN = new PrimerRiesgoLUCSN();
				AgrupacionCotDTO agrupacionCotDTO = new AgrupacionCotDTO();
							
				agrupacionCotDTO = primerRiesgoLUCSN.buscarPorCotizacion(idToCotizacion, Sistema.TIPO_PRIMER_RIESGO);
				
				//actualizar coberturas del anterior calculo
				DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
				RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
				
				if (agrupacionCotDTO != null && agrupacionCotDTO.getClaveTipoAgrupacion().intValue() == 1){
					//borrar si existe alguna agrupacion x primer riesgo;
					primerRiesgoLUCSN.borrar(agrupacionCotDTO);
					
					List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN.listarCoberturasCalculadasPrimerRiesgoLUC(idToCotizacion, agrupacionCotDTO.getClaveTipoAgrupacion());
					for(CoberturaCotizacionDTO cobertura: coberturas){
						cobertura.setNumeroAgrupacion((short)0);
						coberturaCotizacionSN.modificar(cobertura);
						List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN.getInstancia().listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_PRIMER_RIESGO, Boolean.TRUE);
						List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN.getInstancia().listarRecargosRiesgoPorCoberturaCotizacion(cobertura, Sistema.RECARGO_PRIMER_RIESGO, Boolean.TRUE);
						for (DescuentoRiesgoCotizacionDTO descuento: descuentos){
							descuento.setValorDescuento(0D);
							descuentoRiesgoCotizacionSN.modificar(descuento);							
						}
						for(RecargoRiesgoCotizacionDTO recargo: recargos){
							recargo.setValorRecargo(0D);
							recargoRiesgoCotizacionSN.modificar(recargo);							
						}
						calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);	
					}
				}
				
				
				BigDecimal primaNetaPorCotizacionPrimerRiesgo = incisoCotizacionSN.primaNetaPorCotizacionPrimerRiesgo(idToCotizacion,seccionesCadena);
				BigDecimal sumaAseguradaPorCotizacionPrimerRiesgo = incisoCotizacionSN.sumaAseguradaPrimerRiesgoPorCotizacion(idToCotizacion);
				
				//Paso 1: Calcular cuota promedio.
				BigDecimal cuotaPromedio = primaNetaPorCotizacionPrimerRiesgo.divide(sumaAseguradaPorCotizacionPrimerRiesgo,10,RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(1000D));
				
				//Paso 2: Calcular porcentaje primer riesgo.
				BigDecimal porcentajePrimerRiesgo = BigDecimal.valueOf(sumaAsegurada).divide(sumaAseguradaPorCotizacionPrimerRiesgo,10,RoundingMode.HALF_UP);
				
				//Paso 3: Obtener el factor.
				/*Obtener el Factor. Esto se hace al buscar el rengl�n correspondiente en 
				 * la Tabla de Cotizaci�n de Lloyd's de Londres, usando el Porcentaje de Primer Riesgo 
				 * que se obtuvo en el inciso anterior. En caso de que no se encuentre el valor exacto en la tabla, 
				 * el Factor se debe de obtener extrapolando entre los valores en los que cae el Porcentaje de Primer Riesgo.
				*/
				BigDecimal factor = obtenerFactor(porcentajePrimerRiesgo).divide(BigDecimal.valueOf(100D),10,RoundingMode.HALF_UP);
				
				/*Paso 4:Obtener la Cuota Aplicable Sobre Valores Totales, 
				 * mediante la siguiente f�rmula: 
				 * Cuota Promedio * (1 - Factor / 100).
				*/
				BigDecimal cuotaAplicable = obtenerCuotaAplicable(cuotaPromedio, factor);
				
				
				/*Paso 5: Finalmente, la Prima Neta a Primer Riesgo se obtiene al multiplicar 
				 * los Valores Totales de todas las ubicaciones por la 
				 * Cuota Aplicable Sobre Valores Totales y dividir entre 1,000.
				 */
				BigDecimal primaNetaPrimerRiesgo = obtenerPrimaNetaPrimerRiesgo(sumaAseguradaPorCotizacionPrimerRiesgo,cuotaAplicable);				
				
				
				/* Porcentaje aplicable? Porcentaje Ahorro? */
				
				//BigDecimal porcentajeAhorro = BigDecimal.valueOf(0.05D);
				
				AgrupacionCotId id = new AgrupacionCotId();
				agrupacionCotDTO = new AgrupacionCotDTO();
				
				//id.setIdToCotizacion(idToCotizacion);
				Short numeroAgrupacion = primerRiesgoLUCSN.obtenerNumeroAgrupacion(idToCotizacion);
				id.setNumeroAgrupacion(numeroAgrupacion);
				id.setIdToCotizacion(idToCotizacion);
				agrupacionCotDTO.setClaveTipoAgrupacion((short)1);
				agrupacionCotDTO.setIdToSeccion(BigDecimal.ZERO);
				agrupacionCotDTO.setValorCuota(cuotaAplicable.divide(BigDecimal.valueOf(1000D), 10, RoundingMode.HALF_UP).doubleValue());
				agrupacionCotDTO.setValorPrimaneta(primaNetaPrimerRiesgo.doubleValue());
				agrupacionCotDTO.setValorSumaAsegurada(sumaAsegurada);
				agrupacionCotDTO.setId(id);				

				/*BigDecimal factorAplicable = primaNetaPrimerRiesgo.divide(primaNetaPorCotizacionPrimerRiesgo,10,RoundingMode.HALF_UP);

				//se actualizan las coberturas que fueron a primer riesgo			

				if (seccionesSeleccionadas != null && seccionesSeleccionadas.size() > 0) {
					for (BigDecimal idToSeccion : seccionesSeleccionadas) {
						List<CoberturaCotizacionDTO> coberturasPrimerRiesgo = coberturaCotizacionSN.listarCoberturasContratadasPorSeccion(idToCotizacion, idToSeccion);
						for(CoberturaCotizacionDTO cobertura: coberturasPrimerRiesgo){
							cobertura.setNumeroAgrupacion(numeroAgrupacion);
							coberturaCotizacionSN.modificar(cobertura);
							//aplicar Descuentos a los riesgos de las coberturas.
							if(factorAplicable.doubleValue()> 0){
								List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN.getInstancia().listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_PRIMER_RIESGO, Boolean.TRUE);
								for (DescuentoRiesgoCotizacionDTO descuento: descuentos){
									descuento.setValorDescuento(BigDecimal.ONE.subtract(factorAplicable.abs()).multiply(BigDecimal.valueOf(100D)).doubleValue());
									descuentoRiesgoCotizacionSN.modificar(descuento);
								}
							}else{
								List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN.getInstancia().listarRecargosRiesgoPorCoberturaCotizacion(cobertura, Sistema.RECARGO_PRIMER_RIESGO, Boolean.TRUE);
								for(RecargoRiesgoCotizacionDTO recargo: recargos){
									recargo.setValorRecargo(BigDecimal.ONE.subtract(factorAplicable.abs()).multiply(BigDecimal.valueOf(100D)).doubleValue());
									recargoRiesgoCotizacionSN.modificar(recargo);
								}							
							}
							//Recalcular las coberturas Afectadas
							calculoCotizacionDN.calcularARD(cobertura.getId());								
						}
					}
				}*/
				List<CoberturaCotizacionDTO> coberturasPrimerRiesgo = new ArrayList<CoberturaCotizacionDTO>();
				if (seccionesSeleccionadas != null && seccionesSeleccionadas.size() > 0) {
					for (BigDecimal idToSeccion : seccionesSeleccionadas) {
						coberturasPrimerRiesgo.addAll(coberturaCotizacionSN.listarCoberturasContratadasPorSeccion(idToCotizacion, idToSeccion));
					}
					if(this.igualarPrimasPrimerRiesgoLUCV2(coberturasPrimerRiesgo, primaNetaPrimerRiesgo.doubleValue(), Sistema.DESCUENTO_PRIMER_RIESGO, Sistema.RECARGO_PRIMER_RIESGO, nombreUsuario, Boolean.FALSE)) {
						primerRiesgoLUCSN.agregar(agrupacionCotDTO);
						for(CoberturaCotizacionDTO cobertura : coberturasPrimerRiesgo) {
							cobertura.setNumeroAgrupacion(numeroAgrupacion);
							coberturaCotizacionSN.modificar(cobertura);
						}
						mensaje = "Se Registro correctamente el Primer Riesgo. La Prima Neta es: "+fMonto.format(primaNetaPrimerRiesgo.doubleValue())+ ", con una Cuota Promedio de: "+fCuota.format(cuotaAplicable);
					} else {
						mensaje = "No se ha podido registrar el Primer Riesgo.</br>";
					}
				}
			} else {
				Double sumaAseguradaMaxima = incisoCotizacionSN
				.getSumaAseguradaMayorPorUbicacion(idToCotizacion,
						seccionesCadena.toString());
				mensaje = "La Suma Asegurada debe ser Mayor que:";
				mensaje += fMonto.format((sumaAseguradaMaxima.doubleValue()*0.5D));
			}
		} else {
			mensaje = "La Suma Asegurada debe ser Mayor que " + fMonto.format(0d);
		}

		return mensaje;
	}
	
	private BigDecimal obtenerFactor(BigDecimal porcentajePrimerRiesgo) throws SystemException {
		LloydsDN lloydsDN = LloydsDN.getInstancia();
		List<LloydsDTO> lloydsList = lloydsDN.buscarTodosOrdenadosPor(
		"porcentajePrimerRiesgoAbsoluto");
		double[] porcentajePrimerRiesgoArray = new double[lloydsList.size()];
		double[] porcentajeFactorArray = new double[lloydsList.size()];
		double porcentajeFactor = 0;
		double dPorcentajePrimerRiesgo = porcentajePrimerRiesgo.doubleValue();

		for (int x=0; x < lloydsList.size(); x++) {
			LloydsDTO lloyds = lloydsList.get(x);
			porcentajePrimerRiesgoArray[x] = lloyds.getPorcentajePrimerRiesgoAbsoluto();
			porcentajeFactorArray[x] = lloyds.getPorcentajeFactor();
		}
		double limiteInferior = porcentajePrimerRiesgoArray[0];
		double limiteSuperior = porcentajePrimerRiesgoArray[porcentajePrimerRiesgoArray.length - 1];
		if ((dPorcentajePrimerRiesgo >= limiteInferior) && 
				(dPorcentajePrimerRiesgo <= limiteSuperior)) {
			try {
				UnivariateRealInterpolator interpolator = new SplineInterpolator();
				UnivariateRealFunction function = interpolator.interpolate(porcentajePrimerRiesgoArray, 
						porcentajeFactorArray);
				porcentajeFactor = function.value(porcentajePrimerRiesgo.doubleValue());
			}
			catch (MathException ex) {
				throw new SystemException(ex);
			}
		}
		else if (dPorcentajePrimerRiesgo < limiteInferior) {
			porcentajeFactor = porcentajeFactorArray[0];
		}
		else if (dPorcentajePrimerRiesgo 
				> limiteSuperior) {
			porcentajeFactor = porcentajeFactorArray[porcentajeFactorArray.length -1];
		}

		return new BigDecimal(porcentajeFactor);
	}
	
	/*Obtener la Cuota Aplicable Sobre Valores Totales, 
	 * mediante la siguiente f�rmula: 
	 * Cuota Promedio * (1 - Factor / 100).
	*/
	private BigDecimal obtenerCuotaAplicable(BigDecimal cuotaPromedio, BigDecimal factor) {
		return cuotaPromedio.multiply(
				new BigDecimal(1)
				.subtract(factor));
	}
	
	/*Paso 5: Finalmente, la Prima Neta a Primer Riesgo se obtiene al multiplicar 
	 * los Valores Totales de todas las ubicaciones por la 
	 * Cuota Aplicable Sobre Valores Totales y dividir entre 1,000.
	 */
	private BigDecimal obtenerPrimaNetaPrimerRiesgo(BigDecimal sumaAseguradaPorCotPrimerRiesgo, 
			BigDecimal cuotaAplicable) {
		return sumaAseguradaPorCotPrimerRiesgo.multiply(
				cuotaAplicable)
				.divide(BigDecimal.valueOf(1000D),10,RoundingMode.HALF_UP);
	}
	
	public boolean esValidaSumaAseguradaPrimerRiesgo(BigDecimal idToCotizacion,
			Double sumaAsegurada, String seccionesConComas)
			throws ExcepcionDeAccesoADatos, SystemException {
		boolean esValida = false;

		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		Double sumaAseguradaMaxima = incisoCotizacionSN
				.getSumaAseguradaMayorPorUbicacion(idToCotizacion,
						seccionesConComas);

		if (sumaAsegurada.doubleValue() >= (sumaAseguradaMaxima.doubleValue() * 0.5D)) {
			esValida = true;
		}

		return esValida;
	}

	public AgrupacionCotDTO buscarPorCotizacion(BigDecimal idToCotizacion,Short claveTipoAgrupacion) throws ExcepcionDeAccesoADatos, SystemException {
		return new PrimerRiesgoLUCSN().buscarPorCotizacion(idToCotizacion, claveTipoAgrupacion);
	}
	public List<AgrupacionCotDTO> buscarPorCotizacionAgrupacion(BigDecimal idToCotizacion,Short claveTipoAgrupacion) throws ExcepcionDeAccesoADatos, SystemException {
		SeccionDN seccionDN = SeccionDN.getInstancia();
		SeccionDTO seccionDTO = new SeccionDTO();
		List<AgrupacionCotDTO> agrupaciones = new PrimerRiesgoLUCSN().buscarPorCotizacionAgrupacion(idToCotizacion, claveTipoAgrupacion);
		for(AgrupacionCotDTO dto: agrupaciones){
			seccionDTO = seccionDN.getPorId(dto.getIdToSeccion());
			dto.setDescripcionSeccion(seccionDTO.getNombreComercial());
		}
		return agrupaciones;
	}
	public void borrarAgrupaciones(BigDecimal idToCotizacion) throws SystemException {
		PrimerRiesgoLUCSN primerRiesgoLUCSN = new PrimerRiesgoLUCSN();
		primerRiesgoLUCSN.borrarAgrupaciones(idToCotizacion);
	}
	
	public List<AgrupacionCotDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException {
		return new PrimerRiesgoLUCSN().buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public List<AgrupacionCotDTO> listarPorCotizacion(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos, SystemException {
		return new PrimerRiesgoLUCSN().listarPorCotizacion(idToCotizacion);
	}
	public List<AgrupacionCotDTO> listarFiltrado(AgrupacionCotDTO dto) throws ExcepcionDeAccesoADatos, SystemException {
		return new PrimerRiesgoLUCSN().listarFiltrado(dto);
	}	
	public AgrupacionCotDTO getPorId(AgrupacionCotDTO agrupacionCotDTO) throws ExcepcionDeAccesoADatos, SystemException {
		return new PrimerRiesgoLUCSN().getPorId(agrupacionCotDTO);
	}
	public String borrarAgrupacionLUC(BigDecimal idToCotizacion,BigDecimal numeroAgrupacion,BigDecimal idToSeccion, String nombreUsuario) 
			throws SystemException {
		PrimerRiesgoLUCSN primerRiesgoLUCSN = new PrimerRiesgoLUCSN();
		CoberturaCotizacionSN coberturaCotizacionSN = new  CoberturaCotizacionSN();
		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
		CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
		String mensaje = "";

		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN.listarCoberturasContratadasPorSeccion(idToCotizacion, idToSeccion);
		SeccionDTO seccionDTO = SeccionDN.getInstancia().getPorId(idToSeccion);
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getClaveFacultativo() != null && cobertura.getClaveFacultativo() == 1) {
				return "<li>No se puede eliminar la agrupaci&oacute;n de LUC para la seccion "
						+ seccionDTO.getNombreComercial()
						+ " porque tiene coberturas facultadas.";
			}
		}
		primerRiesgoLUCSN.borrarAgrupacion(idToCotizacion, numeroAgrupacion, idToSeccion);

		for(CoberturaCotizacionDTO cobertura: coberturas){
			cobertura.setNumeroAgrupacion((short)0);
			coberturaCotizacionSN.modificar(cobertura);
			List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN.getInstancia().listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_LUC, Boolean.TRUE);
			for (DescuentoRiesgoCotizacionDTO descuento: descuentos){
				descuento.setValorDescuento(0d);
				descuentoRiesgoCotizacionSN.modificar(descuento);																	
			}	
			List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN.getInstancia().listarRecargosRiesgoPorCoberturaCotizacion(cobertura, Sistema.RECARGO_LUC, Boolean.TRUE);
			for(RecargoRiesgoCotizacionDTO recargo: recargos){
				recargo.setValorRecargo(0d);
				recargoRiesgoCotizacionSN.modificar(recargo);
			}
			calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);	
		}		
		mensaje += "<li>Se Elimin&oacute; exitosamente la agrupaci&oacute;n de LUC. para la seccion: "+seccionDTO.getNombreComercial();
		return mensaje;
	}	
	public String borrarAgrupacionPRR(BigDecimal idToCotizacion,BigDecimal numeroAgrupacion,BigDecimal idToSeccion, String nombreUsuario) 
			throws SystemException {
		PrimerRiesgoLUCSN primerRiesgoLUCSN = new PrimerRiesgoLUCSN();
		CoberturaCotizacionSN coberturaCotizacionSN = new  CoberturaCotizacionSN();
		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
		CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
		String mensaje = "";

		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN.listarCoberturasCalculadasPrimerRiesgoLUC(idToCotizacion,numeroAgrupacion.shortValue());
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getClaveFacultativo() != null && cobertura.getClaveFacultativo() == 1) {
				return "<li>No se puede eliminar la agrupaci&oacute;n de Primer Riesgo porque existen coberturas facultadas.";
			}
		}
		
		primerRiesgoLUCSN.borrarAgrupacion(idToCotizacion, numeroAgrupacion, idToSeccion);

		for(CoberturaCotizacionDTO cobertura: coberturas){
			cobertura.setNumeroAgrupacion((short)0);
			coberturaCotizacionSN.modificar(cobertura);
			List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN.getInstancia().listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_PRIMER_RIESGO, Boolean.TRUE);
			for (DescuentoRiesgoCotizacionDTO descuento: descuentos){
				descuento.setValorDescuento(0d);
				descuentoRiesgoCotizacionSN.modificar(descuento);																	
			}	
			List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN.getInstancia().listarRecargosRiesgoPorCoberturaCotizacion(cobertura, Sistema.RECARGO_PRIMER_RIESGO, Boolean.TRUE);
			for(RecargoRiesgoCotizacionDTO recargo: recargos){
				recargo.setValorRecargo(0d);
				recargoRiesgoCotizacionSN.modificar(recargo);
			}
			calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);	
		}		
		mensaje += "<li>Se Elimin&oacute; exitosamente la agrupaci&oacute;n de Primer Riesgo.";
		return mensaje;
	}

	public List<SeccionDTO> listarSeccionesConAgrupacionPrimerRiesgo(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {
		return new PrimerRiesgoLUCSN()
				.listarSeccionesConAgrupacionPrimerRiesgo(idToCotizacion);
	}	
	
	public List<AgrupacionCotDTO> listarAgrupacionesLUCMalRegistradas(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {
		return new PrimerRiesgoLUCSN().listarAgrupacionesLUCMalRegistradas(idToCotizacion);
	}
	
	public List<AgrupacionCotDTO> listarAgrupacionesExcluyentes(BigDecimal idToCotizacionAgrExistente, BigDecimal idToCotizacionAgrInexistente) 
	throws ExcepcionDeAccesoADatos,SystemException {
		return new PrimerRiesgoLUCSN().listarAgrupacionesExcluyentes(idToCotizacionAgrExistente, idToCotizacionAgrInexistente);
	}
	
	public void agregar(AgrupacionCotDTO agrupacionCotDTO) throws ExcepcionDeAccesoADatos, SystemException {
		new PrimerRiesgoLUCSN().agregar(agrupacionCotDTO);
	}
	
	public void borrar(AgrupacionCotDTO agrupacionCotDTO) throws ExcepcionDeAccesoADatos, SystemException {
		if(agrupacionCotDTO != null)
			new PrimerRiesgoLUCSN().borrar(agrupacionCotDTO);
	}
	
	public AgrupacionCotDTO modificar(AgrupacionCotDTO agrupacionCotDTO) throws ExcepcionDeAccesoADatos, SystemException {
		return new PrimerRiesgoLUCSN().modificar(agrupacionCotDTO);
	}
}