package mx.com.afirme.midas.cotizacion.primerriesgoluc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class PrimerRiesgoLUCForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PrimerRiesgoLUCDTO primerRiesgoLUCDTO = new PrimerRiesgoLUCDTO();
	private List<AgrupacionCotDTO> agrupacionCotDTOs = new ArrayList<AgrupacionCotDTO>();

	@SuppressWarnings("unchecked")
	public PrimerRiesgoLUCForm() {
		this.primerRiesgoLUCDTO.setSeccionesPrimerRiesgo(ListUtils.lazyList(
				new ArrayList(), new Factory() {
					public Object create() {
						return new SeccionCotizacionDTO();
					}
				}));

		this.primerRiesgoLUCDTO.setSeccionesLUC(ListUtils.lazyList(
				new ArrayList(), new Factory() {
					public Object create() {
						return new SeccionCotizacionDTO();
					}
				}));

	}

	public PrimerRiesgoLUCDTO getPrimerRiesgoLUCDTO() {
		return primerRiesgoLUCDTO;
	}

	public void setPrimerRiesgoLUCDTO(PrimerRiesgoLUCDTO primerRiesgoLUCDTO) {
		this.primerRiesgoLUCDTO = primerRiesgoLUCDTO;
	}

	public List<AgrupacionCotDTO> getAgrupacionCotDTOs() {
		return agrupacionCotDTOs;
	}

	public void setAgrupacionCotDTOs(List<AgrupacionCotDTO> agrupacionCotDTOs) {
		this.agrupacionCotDTOs = agrupacionCotDTOs;
	}

}
