package mx.com.afirme.midas.cotizacion.primerriesgoluc;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PrimerRiesgoLUCSN {
	private AgrupacionCotFacadeRemote beanRemoto;

	public PrimerRiesgoLUCSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(AgrupacionCotFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(AgrupacionCotDTO agrupacionCotDTO) {
		try {
			beanRemoto.save(agrupacionCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public Short obtenerNumeroAgrupacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.maxAgrupacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrar(AgrupacionCotDTO agrupacionCotDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(agrupacionCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AgrupacionCotDTO modificar(AgrupacionCotDTO agrupacionCotDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(agrupacionCotDTO);
			return agrupacionCotDTO;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<AgrupacionCotDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}

	}

	public List<AgrupacionCotDTO> listarFiltrado(AgrupacionCotDTO dto)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(dto);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}

	}

	public AgrupacionCotDTO getPorId(AgrupacionCotDTO agrupacionCotDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(agrupacionCotDTO.getId());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AgrupacionCotDTO buscarPorCotizacion(BigDecimal idToCotizacion,
			Short claveTipoAgrupacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarPorCotizacion(idToCotizacion,
					claveTipoAgrupacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<AgrupacionCotDTO> buscarPorCotizacionAgrupacion(
			BigDecimal idToCotizacion, Short claveTipoAgrupacion)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarPorCotizacionAgrupacion(idToCotizacion,
					claveTipoAgrupacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AgrupacionCotDTO buscarPorCotizacionSeccion(
			BigDecimal idToCotizacion, Short claveTipoAgrupacion,
			BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarPorCotizacionSeccion(idToCotizacion,
					claveTipoAgrupacion, idToSeccion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<AgrupacionCotDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrarAgrupaciones(BigDecimal idToCotizacion) {
		try {
			beanRemoto.borrarAgrupaciones(idToCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrarAgrupacion(BigDecimal idToCotizacion,
			BigDecimal numeroAgrupacion, BigDecimal idToSeccion) {
		try {
			beanRemoto.borrarAgrupacion(idToCotizacion, numeroAgrupacion,
					idToSeccion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SeccionDTO> listarSeccionesConAgrupacionPrimerRiesgo(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto
					.listarSeccionesConAgrupacionPrimerRiesgo(idToCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}

	}
	
	public List<AgrupacionCotDTO> listarAgrupacionesLUCMalRegistradas(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarAgrupacionesLUCMalRegistradas(idToCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<AgrupacionCotDTO> listarAgrupacionesExcluyentes(BigDecimal idToCotizacionAgrExistente, BigDecimal idToCotizacionAgrInexistente) 
	throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarAgrupacionesExcluyentes(idToCotizacionAgrExistente, idToCotizacionAgrInexistente);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<AgrupacionCotDTO> listarPorCotizacion(BigDecimal idToCotizacion){
		try {
			return beanRemoto.listarPorCotizacion(idToCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(getClass().toString(),e,Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}
}
