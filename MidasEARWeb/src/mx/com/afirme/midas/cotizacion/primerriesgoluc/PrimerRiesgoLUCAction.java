package mx.com.afirme.midas.cotizacion.primerriesgoluc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PrimerRiesgoLUCAction extends MidasMappingDispatchAction {
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarPrimerRiesgo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarPrimerRiesgo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		PrimerRiesgoLUCForm primerRiesgoLUCForm = (PrimerRiesgoLUCForm) form;
		PrimerRiesgoLUCDTO primerRiesgoLUCDTO = primerRiesgoLUCForm.getPrimerRiesgoLUCDTO();
		String id = request.getParameter("id");
		
		try {
			primerRiesgoLUCDTO.setIdCotizacion(UtileriasWeb.regresaBigDecimal(id));
			SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN
					.getInstancia();
			List<SeccionCotizacionDTO> seccionesPrimerRiesgo = seccionCotizacionDN
					.listarSeccionesPrimerRiesgo(primerRiesgoLUCDTO.getIdCotizacion());	
			
			Double total = Double.valueOf(0D);
			if (seccionesPrimerRiesgo != null && seccionesPrimerRiesgo.size() > 0) {
				for (SeccionCotizacionDTO seccion : seccionesPrimerRiesgo) {
					total += seccion.getValorSumaAsegurada().doubleValue();
				}
			}
			primerRiesgoLUCDTO.setSize(Integer.valueOf(seccionesPrimerRiesgo.size()));
			primerRiesgoLUCDTO.setTotalSecciones(total);
			primerRiesgoLUCDTO.setSeccionesPrimerRiesgo(seccionesPrimerRiesgo);
			
			PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
			AgrupacionCotDTO agrupacion = primerRiesgoLUCDN.buscarPorCotizacion(primerRiesgoLUCDTO.getIdCotizacion(), Sistema.TIPO_PRIMER_RIESGO);
			if(agrupacion != null){
				List<AgrupacionCotDTO> agrupaCotDTOs = new ArrayList<AgrupacionCotDTO>();
				List<SeccionDTO> secciones = primerRiesgoLUCDN.listarSeccionesConAgrupacionPrimerRiesgo(primerRiesgoLUCDTO.getIdCotizacion());
				String seccionesPRR = obtenerSeccionPRR(secciones);	
				agrupacion.setDescripcionSeccion(seccionesPRR);
				agrupaCotDTOs.add(agrupacion);
				primerRiesgoLUCForm.setAgrupacionCotDTOs(agrupaCotDTOs);
			}

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public String obtenerSeccionPRR(List<SeccionDTO> secciones) {
		StringBuilder seccionesPRR = new StringBuilder("");
		for(SeccionDTO seccion:secciones){
			seccionesPRR.append(seccion.getNombreComercial()).append(" ");
		}
		return seccionesPRR.toString();
	}

	/**
	 * Method mostrarLUC
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarLUC(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		PrimerRiesgoLUCForm primerRiesgoLUCForm = (PrimerRiesgoLUCForm) form;
		PrimerRiesgoLUCDTO primerRiesgoLUCDTO = primerRiesgoLUCForm.getPrimerRiesgoLUCDTO();
		String id = request.getParameter("id");
		
		try {
			primerRiesgoLUCDTO.setIdCotizacion(UtileriasWeb.regresaBigDecimal(id));
			SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN
					.getInstancia();
			List<IncisoCotizacionDTO> incisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(primerRiesgoLUCDTO.getIdCotizacion());

			/* Parametro General: Minimo de ubicaciones para LUC (Grupo: 3, Codigo: 30060) */
			ParametroGeneralId idParametro = new ParametroGeneralId();
			idParametro.setIdToGrupoParametroGeneral(BigDecimal.valueOf(3D));
			idParametro.setCodigoParametroGeneral(BigDecimal.valueOf(30060D));
			ParametroGeneralDTO parametroGeneralDTO = ParametroGeneralDN.getINSTANCIA().getPorId(idParametro);

			if(incisos.size() >= Integer.parseInt(parametroGeneralDTO.getValor())) {
				List<SeccionCotizacionDTO> seccionesLUC = seccionCotizacionDN
				.listarSeccionesLUC(primerRiesgoLUCDTO.getIdCotizacion());
				primerRiesgoLUCDTO.setSize(Integer.valueOf(seccionesLUC.size()));
				primerRiesgoLUCDTO.setSeccionesLUC(seccionesLUC);
				
				PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
				List<AgrupacionCotDTO> agrupacion = primerRiesgoLUCDN.buscarPorCotizacionAgrupacion(primerRiesgoLUCDTO.getIdCotizacion(), Sistema.TIPO_LUC);
				if(agrupacion != null){
					primerRiesgoLUCForm.setAgrupacionCotDTOs(agrupacion);
				}				
			}else{
				primerRiesgoLUCDTO.setSize(Integer.valueOf(0));
				primerRiesgoLUCDTO.setSeccionesLUC(new ArrayList<SeccionCotizacionDTO>());				
			}
			
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method calcularPrimerRiesgo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward calcularPrimerRiesgoODT(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String mensaje = "";
		PrimerRiesgoLUCForm primerRiesgoLUCForm = (PrimerRiesgoLUCForm) form;
		PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
		PrimerRiesgoLUCDTO primerRiesgoLUCDTO = primerRiesgoLUCForm
				.getPrimerRiesgoLUCDTO();

		List<SeccionCotizacionDTO> secciones = primerRiesgoLUCDTO
				.getSeccionesPrimerRiesgo();
		try {
			
			if (secciones != null && secciones.size() > 0) {
				mensaje = primerRiesgoLUCDN.calcularPrimerRiesgo(primerRiesgoLUCDTO
						.getIdCotizacion(), secciones, primerRiesgoLUCDTO
						.getSumaAseguradaPrimerRiesgo(), UtileriasWeb.obtieneNombreUsuario(request));

				primerRiesgoLUCForm.setMensaje(mensaje);
				primerRiesgoLUCForm.setTipoMensaje("30");
				StringBuilder seccionesPRR = new StringBuilder("");
				for(SeccionCotizacionDTO seccion:secciones){
					if(seccion.getClaveContrato()!= null){
						seccionesPRR.append(seccion.getSeccionDTO().getNombreComercial()).append("--");
					}
				}
				AgrupacionCotDTO agrupacion = primerRiesgoLUCDN.buscarPorCotizacion(primerRiesgoLUCDTO.getIdCotizacion(), Sistema.TIPO_PRIMER_RIESGO);
				if(agrupacion != null){
					List<AgrupacionCotDTO> agrupaCotDTOs = new ArrayList<AgrupacionCotDTO>();
					agrupacion.setDescripcionSeccion(seccionesPRR.toString());
					agrupaCotDTOs.add(agrupacion);
					primerRiesgoLUCForm.setAgrupacionCotDTOs(agrupaCotDTOs);
				}					
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method calcularLUC
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward calcularLUCODT(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String mensaje = "";
		PrimerRiesgoLUCForm primerRiesgoLUCForm = (PrimerRiesgoLUCForm) form;
		PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
		PrimerRiesgoLUCDTO primerRiesgoLUCDTO = primerRiesgoLUCForm
				.getPrimerRiesgoLUCDTO();

		List<SeccionCotizacionDTO> secciones = primerRiesgoLUCDTO
				.getSeccionesLUC();
		try {
			
			if (secciones != null && secciones.size() > 0) {
				mensaje = primerRiesgoLUCDN.calcularLUC(primerRiesgoLUCDTO
						.getIdCotizacion(), secciones, UtileriasWeb.obtieneNombreUsuario(request));

				primerRiesgoLUCForm.setMensaje(mensaje);
				primerRiesgoLUCForm.setTipoMensaje("30");

				List<AgrupacionCotDTO> agrupacion = primerRiesgoLUCDN.buscarPorCotizacionAgrupacion(primerRiesgoLUCDTO.getIdCotizacion(), Sistema.TIPO_LUC);
				if(agrupacion != null){
					primerRiesgoLUCForm.setAgrupacionCotDTOs(agrupacion);
				}
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			primerRiesgoLUCForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			primerRiesgoLUCForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Method eliminarLUC
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward eliminarLUC(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String mensaje = "";
		String idToCotizacion = request.getParameter("idToCotizacion");
		String numeroAgrupacion = request.getParameter("numeroAgrupacion");
		String idToSeccion = request.getParameter("idToSeccion");
		
		PrimerRiesgoLUCForm primerRiesgoLUCForm = (PrimerRiesgoLUCForm) form;
		PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
		PrimerRiesgoLUCDTO primerRiesgoLUCDTO = primerRiesgoLUCForm
		.getPrimerRiesgoLUCDTO();		
		try {
			mensaje = primerRiesgoLUCDN.borrarAgrupacionLUC(UtileriasWeb.regresaBigDecimal(idToCotizacion),
					UtileriasWeb.regresaBigDecimal(numeroAgrupacion), UtileriasWeb.regresaBigDecimal(idToSeccion), 
					UtileriasWeb.obtieneNombreUsuario(request));
			primerRiesgoLUCForm.setMensaje(mensaje);
			if (mensaje.contains("No se puede eliminar")) {
				primerRiesgoLUCForm.setTipoMensaje(Sistema.INFORMACION);
			} else {
				primerRiesgoLUCForm.setTipoMensaje(Sistema.EXITO);
			}
			
			List<AgrupacionCotDTO> agrupacion = primerRiesgoLUCDN.buscarPorCotizacionAgrupacion(primerRiesgoLUCDTO.getIdCotizacion(), Sistema.TIPO_LUC);
			if(agrupacion != null){
				primerRiesgoLUCForm.setAgrupacionCotDTOs(agrupacion);
			}				
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			primerRiesgoLUCForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			primerRiesgoLUCForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Method eliminarPRR
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward eliminarPRR(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String mensaje = "";
		String idToCotizacion = request.getParameter("idToCotizacion");
		String numeroAgrupacion = request.getParameter("numeroAgrupacion");
		String idToSeccion = request.getParameter("idToSeccion");
		
		PrimerRiesgoLUCForm primerRiesgoLUCForm = (PrimerRiesgoLUCForm) form;
		PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
		PrimerRiesgoLUCDTO primerRiesgoLUCDTO = primerRiesgoLUCForm
		.getPrimerRiesgoLUCDTO();		
		try {
			mensaje = primerRiesgoLUCDN.borrarAgrupacionPRR(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
					UtileriasWeb.regresaBigDecimal(numeroAgrupacion), UtileriasWeb.regresaBigDecimal(idToSeccion), 
					UtileriasWeb.obtieneNombreUsuario(request));
			primerRiesgoLUCForm.setMensaje(mensaje);
			if (mensaje.contains("No se puede eliminar")) {
				primerRiesgoLUCForm.setTipoMensaje(Sistema.INFORMACION);
			} else {
				primerRiesgoLUCForm.setTipoMensaje(Sistema.EXITO);
			}
			AgrupacionCotDTO agrupacion = primerRiesgoLUCDN.buscarPorCotizacion(primerRiesgoLUCDTO.getIdCotizacion(), Sistema.TIPO_PRIMER_RIESGO);
			if(agrupacion != null){
				List<AgrupacionCotDTO> agrupaCotDTOs = new ArrayList<AgrupacionCotDTO>();
				List<SeccionDTO> secciones = primerRiesgoLUCDN.listarSeccionesConAgrupacionPrimerRiesgo(primerRiesgoLUCDTO.getIdCotizacion());
				String seccionesPRR = obtenerSeccionPRR(secciones);	
				agrupacion.setDescripcionSeccion(seccionesPRR);
				agrupaCotDTOs.add(agrupacion);
				primerRiesgoLUCForm.setAgrupacionCotDTOs(agrupaCotDTOs);
			}				
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			primerRiesgoLUCForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			primerRiesgoLUCForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
}
