package mx.com.afirme.midas.cotizacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.direccion.DireccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

/**
 * 
 * @author Jos� Luis Arellano
 * @since 01 Septiembre de 2009
 */
public class CotizacionSN {
	private CotizacionFacadeRemote beanRemoto;
	private String nombreUsuario;

	public CotizacionSN(String nombreUsuario) throws SystemException {
		try {
			this.nombreUsuario = nombreUsuario;
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public CotizacionDTO agregar(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			cotizacionDTO.setNombreUsuarioLog(this.nombreUsuario);
			return beanRemoto.save(cotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			cotizacionDTO.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.delete(cotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			cotizacionDTO.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.update(cotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public CotizacionDTO getPorId(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(cotizacionDTO.getIdToCotizacion());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public CotizacionDTO getPorId(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalFiltrado(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalFiltrado de las OT");
		}
		
	}
	
	public CotizacionDTO generarCotizacionASolicitud(BigDecimal idCotizacion, SolicitudDTO solicitudEndoso, 
			String nombreUsuarioCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.generarCotizacionASolicitud(idCotizacion, solicitudEndoso, this.nombreUsuario, nombreUsuarioCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Obtiene los registros de direccionCobro y TipoPoliza 
	 * correspondientes al registro cotizacion recibido.
	 * @param cotizacionDTO el registro cotizacionDTO.
	 * @return cotizacionDTO el registro cotizacionDTO conteniendo los objetos foraneos.
	 * @throws SystemException 
	 */
	public CotizacionDTO obtenerDatosForaneos(CotizacionDTO cotizacionDTO) throws SystemException{
		DireccionSN direccionSN = new DireccionSN();
		//PersonaSN personaSN = new PersonaSN();
		if (cotizacionDTO != null && cotizacionDTO.getIdToCotizacion()!=null){
			cotizacionDTO.setDireccionCobroDTO(direccionSN.getPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), 2));
			if (cotizacionDTO.getTipoPolizaDTO() == null)
				cotizacionDTO.setTipoPolizaDTO(new TipoPolizaSN().encontrarPorCotizacion(cotizacionDTO.getIdToCotizacion()));
		}
		return cotizacionDTO;
	}

	public List<CotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	
	public CotizacionDTO obtenerCotizacionCompleta(BigDecimal idToCotizacion){
		try {
			return beanRemoto.getCotizacionFull(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CotizacionDTO> listarCotizaciones() {
		try {
			return beanRemoto.listarCotizaciones();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Double getPrimaNetaCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.getPrimaNetaCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<CotizacionDTO> listarCotizacionesFiltrado(CotizacionDTO cotizacionDTO) {
		try {
			return beanRemoto.listarCotizacionesFiltrado(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Long obtenerTotalCotizacionesFiltrado(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalCotizacionesFiltrado(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalCotizacionesFiltrado de las COT");
		}
		
	}

	public PlenoReaseguroDTO obtenerPlenoReaseguro(SubGiroDTO subGiroDTO){
		return beanRemoto.obtenerPlenoReaseguro(subGiroDTO);
	}
}