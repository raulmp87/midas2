package mx.com.afirme.midas.cotizacion.motivorechazocancelacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CotizacionRechazoCancelacionDN {
	private static final CotizacionRechazoCancelacionDN INSTANCIA = new CotizacionRechazoCancelacionDN();

	public static CotizacionRechazoCancelacionDN getInstancia (){
		return CotizacionRechazoCancelacionDN.INSTANCIA;
	}
	
	public void agregar(CotizacionRechazoCancelacionDTO CotizacionRechazoCancelacion) throws ExcepcionDeAccesoADatos, SystemException{
		new CotizacionRechazoCancelacionSN().agregar(CotizacionRechazoCancelacion);
	}
	
	public void borrar (CotizacionRechazoCancelacionDTO CotizacionRechazoCancelacion) throws ExcepcionDeAccesoADatos, SystemException{
		new CotizacionRechazoCancelacionSN().borrar(CotizacionRechazoCancelacion);
	}
	
	public void modificar (CotizacionRechazoCancelacionDTO CotizacionRechazoCancelacion) throws ExcepcionDeAccesoADatos, SystemException{
		new CotizacionRechazoCancelacionSN().modificar(CotizacionRechazoCancelacion);
	}
	
	public CotizacionRechazoCancelacionDTO getPorId(CotizacionRechazoCancelacionId id) throws ExcepcionDeAccesoADatos, SystemException{
		return new CotizacionRechazoCancelacionSN().getPorId(id);
	}
	
	public List<CotizacionRechazoCancelacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new CotizacionRechazoCancelacionSN().listarTodos();
	}
	
	public List<CotizacionRechazoCancelacionDTO> buscarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		return new CotizacionRechazoCancelacionSN().buscarPorPropiedad(propiedad, valor);
	}
}
