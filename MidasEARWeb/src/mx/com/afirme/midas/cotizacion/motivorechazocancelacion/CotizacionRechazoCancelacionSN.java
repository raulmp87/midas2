package mx.com.afirme.midas.cotizacion.motivorechazocancelacion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CotizacionRechazoCancelacionSN {
	private CotizacionRechazoCancelacionFacadeRemote beanRemoto;

	public CotizacionRechazoCancelacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CotizacionRechazoCancelacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto CotizacionRechazoCancelacion instanciado", Level.FINEST, null);
	}
	
	public void agregar(CotizacionRechazoCancelacionDTO CotizacionRechazoCancelacion) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(CotizacionRechazoCancelacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(CotizacionRechazoCancelacionDTO CotizacionRechazoCancelacion) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(CotizacionRechazoCancelacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(CotizacionRechazoCancelacionDTO CotizacionRechazoCancelacion) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(CotizacionRechazoCancelacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<CotizacionRechazoCancelacionDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<CotizacionRechazoCancelacionDTO> buscarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public CotizacionRechazoCancelacionDTO getPorId(CotizacionRechazoCancelacionId id) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
