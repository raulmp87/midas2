package mx.com.afirme.midas.cotizacion.calculo;

import java.math.BigDecimal;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CalculoCotizacionSN {
	private CalculoCotizacionFacadeRemote beanRemoto;

	public CalculoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CalculoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void inicializarCalculoRiesgo(BigDecimal idToCotizacion,String nombreUsuario) {
		try {
			beanRemoto.inicializarCalculoRiesgo(idToCotizacion,nombreUsuario);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CalculoRiesgoDTO calcularRiesgo(CalculoRiesgoDTO calculoRiesgoDTO,
			String nombreUsuario) {
		try {
			return beanRemoto.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CalculoLUCDTO calcularLUC(CalculoLUCDTO calculoLUCDTO,
			String nombreUsuario) {
		try {
			return beanRemoto.calcularLUC(calculoLUCDTO, nombreUsuario);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void calcularRiesgosHidro(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, String nombreUsuario) {
		try {
			beanRemoto.calcularRiesgosHidro(idToCotizacion, numeroInciso,
					nombreUsuario);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CalculoRiesgoDTO calcularRiesgoEmbarque(
			CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario) {
		try {
			return beanRemoto.calcularRiesgoEmbarque(calculoRiesgoDTO,
					nombreUsuario);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	public CalculoRiesgoDTO eliminarRiesgoEmbarque(
			CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario) {
		try {
			return beanRemoto.eliminarRiesgoEmbarque(calculoRiesgoDTO,
					nombreUsuario);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}	
}
