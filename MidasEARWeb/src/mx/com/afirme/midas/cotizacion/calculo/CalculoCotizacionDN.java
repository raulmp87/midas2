package mx.com.afirme.midas.cotizacion.calculo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionSN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaSN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CalculoCotizacionDN {
	private static final CalculoCotizacionDN INSTANCIA = new CalculoCotizacionDN();

	public static CalculoCotizacionDN getInstancia() {
		return CalculoCotizacionDN.INSTANCIA;
	}
	
	/**
	 * Este metodo permite realizar el calculo de una cobertura
	 * al contratar o modificar la Suma Asegurada
	 * * @param coberturaCotizacionDTO
	 *            CoberturaCotizacionDTO objeto con los de la cobertura que 
	 *            se desea contratar o modificar
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 *             cuando el calculo falla
	 * @return CoberturaCotizacionDTO
	 * 				el objeto actualizado con los nuevos calculos            
	 */	
	public CoberturaCotizacionDTO calcularCobertura(
			CoberturaCotizacionDTO coberturaCotizacionDTO, String nombreUsuario, boolean seContratoCobertura)
			throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		//1)	Se modifica la suma asegurada de la cobertura / Se contrata o descontrata una cobertura
		coberturaCotizacionDTO = coberturaCotizacionSN.modificar(coberturaCotizacionDTO);
		
		if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getSeccionDTO().getClaveSubIncisos() != Sistema.PERMITE_SUBINCISOS) {
			//2)	Se actualiza la suma asegurada de todos los riesgos de la cobertura
			//a.	Todos los riesgos tendr�n la misma suma asegurada de la cobertura independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
			RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			
			id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
			id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
			id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
			id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
			
			List<RiesgoCotizacionDTO> riesgosPorCobertura = riesgoCotizacionSN.listarPorIdFiltrado(id);
			if (riesgosPorCobertura.size() > 0){
				for(RiesgoCotizacionDTO riesgo: riesgosPorCobertura){
					riesgo.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
					riesgoCotizacionSN.modificar(riesgo);
				}
			}
			//3)	Si se modific� / contrat� una cobertura con suma asegurada b�sica, 
			//	se modifican las sumas aseguradas amparadas de todos las coberturas amparadas por la b�sica, 
			//	as� como la suma asegurada de todos los riesgos de dichas coberturas amparadas.
			CoberturaSN coberturaSN = new CoberturaSN();
			CoberturaDTO coberturaDTO = coberturaSN.getPorId(coberturaCotizacionDTO.getId().getIdToCobertura());
			if(coberturaDTO.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
				
				SeccionCotizacionDTOId seccionId = new SeccionCotizacionDTOId();
				seccionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
				seccionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
				seccionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
				
				List<CoberturaCotizacionDTO> coberturasPorSeccion = coberturaCotizacionSN.listarPorSeccionCotizacionId(seccionId,Boolean.FALSE);
				
				for(CoberturaCotizacionDTO coberturaPorSeccion: coberturasPorSeccion){
				    
					CoberturaDTO coberturaAsociada = coberturaSN.getPorId(coberturaPorSeccion.getId().getIdToCobertura());
					if(!coberturaAsociada.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
	        				if(coberturaAsociada.getIdCoberturaSumaAsegurada().intValue() == coberturaDTO.getIdToCobertura().intValue()){
	        					coberturaPorSeccion.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
	        					coberturaCotizacionSN.modificar(coberturaPorSeccion);
	        					
	        					List<RiesgoCotizacionDTO> riesgosAsociados = riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(coberturaPorSeccion.getId());
	        					for(RiesgoCotizacionDTO riesgoAsociado: riesgosAsociados){
	        						riesgoAsociado.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
	        						riesgoCotizacionSN.modificar(riesgoAsociado);
	        					}
	        				}
					}
				    
				}		
				
				//4)	Se actualiza la suma asegurada de la secci�n (sumatoria de sumas aseguradas B�SICAS de sus coberturas)
				SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
				
				seccionCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
				seccionCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
				seccionCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
				
				this.actualizarSumaAseguradaSeccion(seccionCotizacionId);
				
			}
			
			//5)	Se recalcula la prima neta de todos los riesgos de la cobertura (y la de la cobertura, secci�n e inciso):
			//	a.	Se obtiene la lista de riesgos contratados de la cobertura ordenados por idToRiesgo
			//	b.	Para cada uno de los riesgos obtenidos:
			//	i.	CalculoCotizacionDN.calcularRiesgo(idCotizacion, numInciso, idSeccion, idCobertura, idRiesgo, 0, sumaAseguradaRiesgo)
			List<RiesgoCotizacionDTO> riesgosContratados = riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(coberturaCotizacionDTO.getId());
			if (riesgosContratados.size() > 0){
				for (RiesgoCotizacionDTO riesgosContratado: riesgosContratados){
				    
					CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
					calculoRiesgoDTO.setIdToCotizacion(riesgosContratado.getId().getIdToCotizacion());
					calculoRiesgoDTO.setNumeroInciso(riesgosContratado.getId().getNumeroInciso());
					calculoRiesgoDTO.setIdToSeccion(riesgosContratado.getId().getIdToSeccion());
					calculoRiesgoDTO.setIdToRiesgo(riesgosContratado.getId().getIdToRiesgo());
					calculoRiesgoDTO.setIdToCobertura(riesgosContratado.getId().getIdToCobertura());
					calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
					calculoRiesgoDTO.setSumaAsegurada(riesgosContratado.getValorSumaAsegurada());

					calculoRiesgoDTO = this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
				    
				}
			}
		} else {
			SubIncisoCotizacionDTO subIncisoCotDTO = new SubIncisoCotizacionDTO();
			subIncisoCotDTO.setId(new SubIncisoCotizacionDTOId());
			subIncisoCotDTO.getId().setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
			subIncisoCotDTO.getId().setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
			subIncisoCotDTO.getId().setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
			
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(coberturaCotizacionDTO.getId().getIdToCobertura());

			List<SubIncisoCotizacionDTO> listaSubIncisos = SubIncisoCotizacionDN.getInstancia().listarFiltrado(subIncisoCotDTO);			
			if(seContratoCobertura){

				//1) se acumula la SA de la Cobertura con la sumatoria de la SA de Subincisos	       
				Double total = 0D;
				for(SubIncisoCotizacionDTO subInciso : listaSubIncisos) {
					total += subInciso.getValorSumaAsegurada();
				}			
				//3)Se actualiza la suma asegurada de la cobertura con la sumatoria de los Subincisos
				coberturaCotizacionDTO.setValorSumaAsegurada(total);
				coberturaCotizacionDTO = coberturaCotizacionSN.modificar(coberturaCotizacionDTO);
				//4) se borra el detalle de primas de cobertura y riesgo
				if(coberturaDTO.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
					CotizacionDN.getInstancia(nombreUsuario).eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
					List<CoberturaCotizacionDTO> coberturasAmparadas = coberturaCotizacionSN.listarCoberturasDeSumaAsegurada(coberturaCotizacionDTO);
					for(CoberturaCotizacionDTO coberturaAmparada: coberturasAmparadas){
						CotizacionDN.getInstancia(nombreUsuario).eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaAmparada);
					}			
				}else{
					CotizacionDN.getInstancia(nombreUsuario).eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
				}
				
				
				//5)Se actualiza la suma asegurada de los riesgos de la cobertura
				RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
				RiesgoCotizacionId idRiesgo = new RiesgoCotizacionId();
				idRiesgo.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
				idRiesgo.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
				idRiesgo.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
				idRiesgo.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
				List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarPorIdFiltrado(idRiesgo);
				for(RiesgoCotizacionDTO riesgo : riesgos) {
					//a.Todos los riesgos tendr�n la misma suma asegurada de la cobertura 
					//independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
					riesgo.setValorSumaAsegurada(total);
					riesgoCotizacionDN.modificar(riesgo);
				}				
				for(SubIncisoCotizacionDTO subInciso : listaSubIncisos){
					//a.Se obtiene la lista de riesgos contratados de todas las coberturas contratadas de la secci�n ordenados por idToRiesgo
					List<RiesgoCotizacionDTO> riesgosCal = riesgoCotizacionDN.listarRiesgoContratadosPorSeccion(coberturaCotizacionDTO.getSeccionCotizacionDTO().getId());
					for(RiesgoCotizacionDTO riesgo : riesgosCal) {
						CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
						calculoRiesgoDTO.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
						calculoRiesgoDTO.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
						calculoRiesgoDTO.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
						calculoRiesgoDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
						calculoRiesgoDTO.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
						calculoRiesgoDTO.setSumaAsegurada(subInciso.getValorSumaAsegurada());
						//b.Para cada uno de los riesgos obtenidos:
						this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
					}
				}				
			
			}else{
				//1) se borra el detalle de primas de cobertura y riesgo
				if(coberturaDTO.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
					CotizacionDN.getInstancia(nombreUsuario).eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
					List<CoberturaCotizacionDTO> coberturasAmparadas = coberturaCotizacionSN.listarCoberturasDeSumaAsegurada(coberturaCotizacionDTO);
					for(CoberturaCotizacionDTO coberturaAmparada: coberturasAmparadas){
						CotizacionDN.getInstancia(nombreUsuario).eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaAmparada);
					}			
				}else{
					CotizacionDN.getInstancia(nombreUsuario).eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);
				}	
				//2)	Se actualiza la suma asegurada de todos los riesgos de la cobertura
				//a.	Todos los riesgos tendr�n la misma suma asegurada de la cobertura independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
				
				RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
				if(coberturaDTO.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
					
					SeccionCotizacionDTOId seccionId = new SeccionCotizacionDTOId();
					seccionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					seccionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					seccionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					
					List<CoberturaCotizacionDTO> coberturasPorSeccion = coberturaCotizacionSN.listarPorSeccionCotizacionId(seccionId,Boolean.FALSE);
					
					for(CoberturaCotizacionDTO coberturaPorSeccion: coberturasPorSeccion){
					    
						CoberturaDTO coberturaAsociada = CoberturaDN.getInstancia().getPorId(coberturaPorSeccion.getId().getIdToCobertura());
						if(!coberturaAsociada.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
		        				if(coberturaAsociada.getIdCoberturaSumaAsegurada().intValue() == coberturaDTO.getIdToCobertura().intValue()){
		        					coberturaPorSeccion.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
		        					coberturaCotizacionSN.modificar(coberturaPorSeccion);
		        					
		        					List<RiesgoCotizacionDTO> riesgosAsociados = riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(coberturaPorSeccion.getId());
		        					for(RiesgoCotizacionDTO riesgoAsociado: riesgosAsociados){
		        						riesgoAsociado.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
		        						riesgoCotizacionSN.modificar(riesgoAsociado);
		        						
		        						CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
		        						calculoRiesgoDTO.setIdToCotizacion(riesgoAsociado.getId().getIdToCotizacion());
		        						calculoRiesgoDTO.setNumeroInciso(riesgoAsociado.getId().getNumeroInciso());
		        						calculoRiesgoDTO.setIdToSeccion(riesgoAsociado.getId().getIdToSeccion());
		        						calculoRiesgoDTO.setIdToRiesgo(riesgoAsociado.getId().getIdToRiesgo());
		        						calculoRiesgoDTO.setIdToCobertura(riesgoAsociado.getId().getIdToCobertura());
		        						calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
		        						calculoRiesgoDTO.setSumaAsegurada(riesgoAsociado.getValorSumaAsegurada());

		        						calculoRiesgoDTO = this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);				        						
		        					}
		        				}
						}
					    
					}		
					
					//4)	Se actualiza la suma asegurada de la secci�n (sumatoria de sumas aseguradas B�SICAS de sus coberturas)
					SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
					
					seccionCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					seccionCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					seccionCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					
					this.actualizarSumaAseguradaSeccion(seccionCotizacionId);				
				
					RiesgoCotizacionId id = new RiesgoCotizacionId();
					
					id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
					
					List<RiesgoCotizacionDTO> riesgosPorCobertura = riesgoCotizacionSN.listarPorIdFiltrado(id);
					if (riesgosPorCobertura.size() > 0){
						for(RiesgoCotizacionDTO riesgo: riesgosPorCobertura){
							riesgo.setValorSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
							riesgoCotizacionSN.modificar(riesgo);		

							CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
    						calculoRiesgoDTO.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
    						calculoRiesgoDTO.setNumeroInciso(riesgo.getId().getNumeroInciso());
    						calculoRiesgoDTO.setIdToSeccion(riesgo.getId().getIdToSeccion());
    						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
    						calculoRiesgoDTO.setIdToCobertura(riesgo.getId().getIdToCobertura());
    						calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
    						calculoRiesgoDTO.setSumaAsegurada(riesgo.getValorSumaAsegurada());

    						calculoRiesgoDTO = this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);							
						}
					}
				} else {
					RiesgoCotizacionId id = new RiesgoCotizacionId();
					id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
					id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
					id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
					id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
					
					List<RiesgoCotizacionDTO> riesgosPorCobertura = riesgoCotizacionSN.listarPorIdFiltrado(id);
					for(RiesgoCotizacionDTO riesgo : riesgosPorCobertura) {
						CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
						calculoRiesgoDTO.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
						calculoRiesgoDTO.setNumeroInciso(riesgo.getId().getNumeroInciso());
						calculoRiesgoDTO.setIdToSeccion(riesgo.getId().getIdToSeccion());
						calculoRiesgoDTO.setIdToCobertura(riesgo.getId().getIdToCobertura());
						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
						calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.ZERO);
						calculoRiesgoDTO.setSumaAsegurada(coberturaCotizacionDTO.getValorSumaAsegurada());
						//b.Para cada uno de los riesgos obtenidos:
						this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
					}
				}
			}			
		}
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(coberturaCotizacionDTO.getId().getIdToCotizacion());
		return coberturaCotizacionSN.getPorId(coberturaCotizacionDTO);
	}
	
	/**
	 * Este metodo permite realizar el calculo de un riesgo
	 * al contratar o modificar la Suma Asegurada
	 * 
	 * @param riesgoCotizacionDTO
	 *            RiesgoCotizacionDTO objeto con los valores del
	 *            riesgo que se desea calcular
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 *             cuando el calculo falla
	 * @return RiesgoCotizacionDTO
	 * 				el objeto actualizado con los nuevos calculos            
	 */		
	public RiesgoCotizacionDTO calcularRiesgo(RiesgoCotizacionDTO riesgoCotizacionDTO, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		//1)	Se modifica la suma asegurada del riesgo / Se contrata o descontrata el riesgo
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		riesgoCotizacionDTO = riesgoCotizacionSN.modificar(riesgoCotizacionDTO);	
		
		//verificar si pertenece a una seccion que maneja subincisos
		if(riesgoCotizacionDTO.getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getSeccionDTO().getClaveSubIncisos() != Sistema.PERMITE_SUBINCISOS) {
			//2)	Si se modific� / contrat� un riesgo con suma asegurada b�sica, 
			//		se modifican las sumas aseguradas amparadas de todos los riesgos amparados por el b�sico.
			RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();		
			RiesgoCoberturaId riesgoCoberturaId = new RiesgoCoberturaId();
		
			riesgoCoberturaId.setIdtocobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			riesgoCoberturaId.setIdtoriesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			riesgoCoberturaId.setIdtoseccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			
			RiesgoCoberturaDTO riesgoCoberturaDTO = riesgoCoberturaSN.getPorId(riesgoCoberturaId);
			if (riesgoCoberturaDTO.getClaveTipoSumaAsegurada().intValue() == Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
				List<RiesgoCoberturaDTO> riesgosPorCobertura = riesgoCoberturaSN.listarRiesgoAsociado(riesgoCoberturaDTO.getId().getIdtocobertura(), riesgoCoberturaDTO.getId().getIdtoseccion());
				for(RiesgoCoberturaDTO riesgoPorCobertura: riesgosPorCobertura){
					if(riesgoPorCobertura.getClaveTipoSumaAsegurada().intValue() != Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
						RiesgoCotizacionId riesgoAsociadoId = new RiesgoCotizacionId();
						riesgoAsociadoId.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
						riesgoAsociadoId.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
						riesgoAsociadoId.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
						riesgoAsociadoId.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
						riesgoAsociadoId.setIdToRiesgo(riesgoPorCobertura.getId().getIdtoriesgo());
						
						RiesgoCotizacionDTO riesgoAsociado = riesgoCotizacionSN.getPorId(riesgoAsociadoId);
						riesgoAsociado.setValorSumaAsegurada(riesgoCotizacionDTO.getValorSumaAsegurada());
						riesgoCotizacionSN.modificar(riesgoAsociado);
					}
				}
				CoberturaCotizacionId id = new CoberturaCotizacionId();
				
				id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
				id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
				id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
				id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());		
				//3)	Se actualiza la suma asegurada de la cobertura (sumatoria de sumas aseguradas B�SICAS de sus riesgos)
				this.actualizarSumaAseguradaCobertura(id);
			}
			
		
			//4)	Si se modific� / contrat� un riesgo b�sico, su cobertura tiene que ser b�sica, 
			//por lo tanto adem�s de modificar la suma asegurada de la cobertura b�sica, 
			//se tendr� que actualizar la suma asegurada de todas coberturas amparadas por esta cobertura y 
			//tambi�n la suma asegurada de cada uno de los riesgos de las coberturas amparadas.
			if (riesgoCoberturaDTO.getClaveTipoSumaAsegurada().intValue() == Integer.valueOf(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)){
				CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
			    CoberturaSN coberturaSN = new CoberturaSN();
				
				SeccionCotizacionDTOId seccionId = new SeccionCotizacionDTOId();
				seccionId.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
				seccionId.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
				seccionId.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
				
				CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
				CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId();
				
				coberturaCotizacionId.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
				coberturaCotizacionId.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
				coberturaCotizacionId.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
				coberturaCotizacionId.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
				coberturaCotizacionDTO.setId(coberturaCotizacionId);
				
				coberturaCotizacionDTO = coberturaCotizacionSN.getPorId(coberturaCotizacionDTO);
				
				List<CoberturaCotizacionDTO> coberturasPorSeccion = coberturaCotizacionSN.listarPorSeccionCotizacionId(seccionId,Boolean.FALSE);
				for(CoberturaCotizacionDTO coberturaPorSeccion: coberturasPorSeccion){
					CoberturaDTO coberturaDTO = coberturaSN.getPorId(coberturaPorSeccion.getId().getIdToCobertura());
									
					if(coberturaDTO.getIdCoberturaSumaAsegurada().intValue() == coberturaCotizacionDTO.getId().getIdToCobertura().intValue()){
						coberturaPorSeccion.setValorSumaAsegurada(riesgoCotizacionDTO.getValorSumaAsegurada());
						coberturaCotizacionSN.modificar(coberturaPorSeccion);
						
						List<RiesgoCotizacionDTO> riesgosAsociados = riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(coberturaPorSeccion.getId());
						for(RiesgoCotizacionDTO riesgoAsociado: riesgosAsociados){
							riesgoAsociado.setValorSumaAsegurada(riesgoCotizacionDTO.getValorSumaAsegurada());
							riesgoCotizacionSN.modificar(riesgoAsociado);
						}
					}
				}
				
				//5)	Se actualiza la suma asegurada de la secci�n (sumatoria de sumas aseguradas B�SICAS de sus coberturas)
				SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
				
				seccionCotizacionId.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
				seccionCotizacionId.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
				seccionCotizacionId.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
				
				this.actualizarSumaAseguradaSeccion(seccionCotizacionId);
			}		
			
			//6)	Se recalcula la prima neta del riesgo (y la de su cobertura, secci�n e inciso):
			//a.	CalculoCotizacionDN.calcularRiesgo(idCotizacion, numInciso, idSeccion, idCobertura, idRiesgo, 0, sumaAseguradaRiesgo)
			CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
			
			calculoRiesgoDTO.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			calculoRiesgoDTO.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
			calculoRiesgoDTO.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			calculoRiesgoDTO.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			calculoRiesgoDTO.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
			calculoRiesgoDTO.setSumaAsegurada(riesgoCotizacionDTO.getValorSumaAsegurada());		
			
			calculoRiesgoDTO = this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
		} else {
			SubIncisoCotizacionDTO subIncisoCotDTO = new SubIncisoCotizacionDTO();
			subIncisoCotDTO.setId(new SubIncisoCotizacionDTOId());
			subIncisoCotDTO.getId().setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			subIncisoCotDTO.getId().setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			subIncisoCotDTO.getId().setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			List<SubIncisoCotizacionDTO> listaSubIncisos = SubIncisoCotizacionDN.getInstancia().listarFiltrado(subIncisoCotDTO);
			for(SubIncisoCotizacionDTO subInciso : listaSubIncisos){
				this.calcularSubInciso(subInciso, nombreUsuario);
			}
		}
		return riesgoCotizacionSN.getPorId(riesgoCotizacionDTO.getId());
	}
	
	public void calcularSubInciso(SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException {
		SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
		List<SubIncisoCotizacionDTO> subIncisos = subIncisoCotizacionDN.listarFiltrado(subIncisoCotizacionDTO);
		//1) se eliminan los detalles de la los subIncisos para que sean calculados
	       
		Double total = 0D;
		for(SubIncisoCotizacionDTO subInciso : subIncisos) {
			total += subInciso.getValorSumaAsegurada();
		}

		//2)Se actualiza la suma asegurada de la secci�n (sumatoria de sumas aseguradas de todos sus subincisos)
		SeccionCotizacionDTO seccionCotizacionDTO = subIncisoCotizacionDTO.getSeccionCotizacionDTO();
		if (seccionCotizacionDTO == null){
			SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
			SeccionCotizacionDTOId id = new SeccionCotizacionDTOId();
			id.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
			seccionCotizacionDTO = seccionCotizacionDN.buscarPorId(id);
		}
		seccionCotizacionDTO.setValorSumaAsegurada(total);
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		seccionCotizacionSN.modificar(seccionCotizacionDTO);

		//3)Se actualiza la suma asegurada de las coberturas de la secci�n
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaCotizacionId idCobertura = new CoberturaCotizacionId();
		idCobertura.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
		idCobertura.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
		idCobertura.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarPorIdFiltrado(idCobertura);
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			//a.Todas las coberturas tendr�n la misma suma asegurada de la secci�n 
			//independientemente del tipo de suma asegurada de la cobertura y de su clave de contrato
			DetallePrimaCoberturaCotizacionDN detallePrimaCoberturaCotizacionDN = DetallePrimaCoberturaCotizacionDN.getInstancia();
			DetallePrimaCoberturaCotizacionId id = new DetallePrimaCoberturaCotizacionId();
			CoberturaCotizacionId coberturaCotId = cobertura.getId();
			id.setIdToCotizacion(coberturaCotId .getIdToCotizacion());
			id.setNumeroInciso(coberturaCotId.getNumeroInciso());
			id.setIdToSeccion(coberturaCotId.getIdToSeccion());
			id.setNumeroSubInciso(BigDecimal.ZERO);
			id.setIdToCobertura(coberturaCotId.getIdToCobertura());
			DetallePrimaCoberturaCotizacionDTO detallePrimaCoberturaCotizacionDTO = detallePrimaCoberturaCotizacionDN.getPorId(id);
			if(detallePrimaCoberturaCotizacionDTO == null) {
				cobertura.setValorSumaAsegurada(seccionCotizacionDTO.getValorSumaAsegurada());
				coberturaCotizacionDN.modificar(cobertura);
			}

			//4)Se actualiza la suma asegurada de los riesgos de cada cobertura de la secci�n
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			RiesgoCotizacionId idRiesgo = new RiesgoCotizacionId();
			idRiesgo.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
			idRiesgo.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
			idRiesgo.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
			idRiesgo.setIdToCobertura(cobertura.getId().getIdToCobertura());
			List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarPorIdFiltrado(idRiesgo);
			for(RiesgoCotizacionDTO riesgo : riesgos) {
				//a.Todos los riesgos tendr�n la misma suma asegurada de la cobertura 
				//independientemente del tipo de suma asegurada del riesgo y de su clave de contrato.
				riesgo.setValorSumaAsegurada(cobertura.getValorSumaAsegurada());
				riesgoCotizacionDN.modificar(riesgo);
				//Se calcula el riesgo solo si la cobertura no contiene detalle de prima con numero de subinciso 0
				if(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)) {
					if (seccionCotizacionDTO.getClaveContrato().shortValue() == 1
							&& cobertura.getClaveContrato().shortValue() == 1
							&& riesgo.getClaveContrato().shortValue() == 1
							&& detallePrimaCoberturaCotizacionDTO == null) {
						CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
						calculoRiesgoDTO.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
						calculoRiesgoDTO.setNumeroInciso(riesgo.getId().getNumeroInciso());
						calculoRiesgoDTO.setIdToSeccion(riesgo.getId().getIdToSeccion());
						calculoRiesgoDTO.setIdToCobertura(riesgo.getId().getIdToCobertura());
						calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
						calculoRiesgoDTO.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
						calculoRiesgoDTO.setSumaAsegurada(subIncisoCotizacionDTO.getValorSumaAsegurada());
						//b.Para cada uno de los riesgos obtenidos:
						this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
					}
				}
			}
		}
		/*RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
		//a.Se obtiene la lista de riesgos contratados de todas las coberturas contratadas de la secci�n ordenados por idToRiesgo
		List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarRiesgoContratadosPorSeccion(seccionCotizacionDTO.getId());
		for(RiesgoCotizacionDTO riesgo : riesgos) {
			CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
			calculoRiesgoDTO.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
			calculoRiesgoDTO.setNumeroInciso(riesgo.getId().getNumeroInciso());
			calculoRiesgoDTO.setIdToSeccion(riesgo.getId().getIdToSeccion());
			calculoRiesgoDTO.setIdToCobertura(riesgo.getId().getIdToCobertura());
			calculoRiesgoDTO.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
			calculoRiesgoDTO.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			calculoRiesgoDTO.setSumaAsegurada(subIncisoCotizacionDTO.getValorSumaAsegurada());
			//b.Para cada uno de los riesgos obtenidos:
			this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
		}*/
	}
	
	/**
	 * Este m�todo debe ser invocado despues de que se modifica la contrataci�n
	 * de descuentos a nivel cobertura.
	 * 
	 * @param CoberturaCotizacionId
	 *            coberturaCotId poblado con todos los atributos.
	 * @throws SystemException
	 * 
	 */
	public void calcularARD(CoberturaCotizacionId coberturaCotId, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
//		3)	Se recalcula la prima neta de todos los riesgos de la cobertura (y la de la cobertura, secci�n e inciso):
//		a.	Se identifica si la secci�n a la cual pertenece la cobertura maneja subincisos
		SeccionDTO seccionDTO = SeccionDN.getInstancia().getPorId(coberturaCotId.getIdToSeccion());

//		b.	Si la secci�n NO maneja subincisos:
//		i.	Se ejecuta el paso 4) de MODIFICACI�N DE LA SUMA ASEGURADA DE UNA COBERTURA o CONTRATO/DESCONTRATO DE UNA COBERTURA
		if (seccionDTO.getClaveSubIncisos() == Sistema.NO_PERMITE_SUBINCISOS){
			
//	4)	Se recalcula la prima neta de todos los riesgos de la cobertura (y la de la cobertura, secci�n e inciso):
//		a.	Se obtiene la lista de riesgos contratados de la cobertura ordenados por idToRiesgo
//		b.	Para cada uno de los riesgos obtenidos:
//			i.	CalculoCotizacionDN.calcularRiesgo(idCotizacion, numInciso, idSeccion, idCobertura, idRiesgo, 0, sumaAseguradaRiesgo)
			List<RiesgoCotizacionDTO> riesgosContratados = riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(coberturaCotId);
			if (riesgosContratados != null && riesgosContratados.size() > 0){
				for (RiesgoCotizacionDTO riesgosContratado: riesgosContratados){
					CalculoRiesgoDTO  calculoRiesgoDTO = new CalculoRiesgoDTO();
					calculoRiesgoDTO.setIdToCotizacion(riesgosContratado.getId().getIdToCotizacion());
					calculoRiesgoDTO.setNumeroInciso(riesgosContratado.getId().getNumeroInciso());
					calculoRiesgoDTO.setIdToSeccion(riesgosContratado.getId().getIdToSeccion());
					calculoRiesgoDTO.setIdToRiesgo(riesgosContratado.getId().getIdToRiesgo());
					calculoRiesgoDTO.setIdToCobertura(riesgosContratado.getId().getIdToCobertura());
					calculoRiesgoDTO.setNumeroSubInciso(BigDecimal.valueOf(0));
					calculoRiesgoDTO.setSumaAsegurada(riesgosContratado.getValorSumaAsegurada());
					calculoRiesgoDTO = this.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
				}
			}
		}
//c.	Si la secci�n S� maneja subincisos:
//	i.	Para cada subInciso de la secci�n:
//	1.	Se ejecuta el paso 5) de MODIFICACI�N DE LA SUMA ASEGURADA DE UN SUBINCISO / AGREGAR UN SUBINCISO, 
//	pero la lista de riesgos contratados estar� filtrada para la cobertura a la cual se le modificaron sus A/R/D.
//	5)	Se recalcula el detalle de prima neta del subinciso para todos los riesgos contratados de todas las coberturas contratadas 
//	(tambi�n se calcula el detalle de prima del subinciso a nivel cobertura, as� como las primas netas del riesgo, cobertura, secci�n e inciso):
//		a.	Se obtiene la lista de riesgos contratados de todas las coberturas contratadas de la secci�n ordenados por idToRiesgo
//		b.	Para cada uno de los riesgos obtenidos:
//		i.	CalculoCotizacionDN.calcularRiesgo(idCotizacion, numInciso, idSeccion, idCobertura, idRiesgo, numeroSubInciso, sumaAseguradaSubInciso)
		else {
			/* Se obtiene detalle de la cobertura */
			DetallePrimaCoberturaCotizacionDN detallePrimaCoberturaCotizacionDN = DetallePrimaCoberturaCotizacionDN.getInstancia();
			DetallePrimaCoberturaCotizacionId id = new DetallePrimaCoberturaCotizacionId();
			id.setIdToCotizacion(coberturaCotId.getIdToCotizacion());
			id.setNumeroInciso(coberturaCotId.getNumeroInciso());
			id.setIdToSeccion(coberturaCotId.getIdToSeccion());
			id.setNumeroSubInciso(BigDecimal.ZERO);
			id.setIdToCobertura(coberturaCotId.getIdToCobertura());
			DetallePrimaCoberturaCotizacionDTO detallePrimaCoberturaCotizacionDTO = detallePrimaCoberturaCotizacionDN.getPorId(id);
			if(detallePrimaCoberturaCotizacionDTO != null) {
				CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
				coberturaCotizacionDTO.setId(coberturaCotId);
				coberturaCotizacionDTO = CoberturaCotizacionDN.getInstancia().getPorId(coberturaCotizacionDTO);
				this.calcularCobertura(coberturaCotizacionDTO, nombreUsuario, false);
			} else {
				SubIncisoCotizacionDTO subIncisoCotDTO = new SubIncisoCotizacionDTO();
				subIncisoCotDTO.setId(new SubIncisoCotizacionDTOId());
				subIncisoCotDTO.getId().setIdToCotizacion(coberturaCotId.getIdToCotizacion());
				subIncisoCotDTO.getId().setIdToSeccion(coberturaCotId.getIdToSeccion());
				subIncisoCotDTO.getId().setNumeroInciso(coberturaCotId.getNumeroInciso());

				List<SubIncisoCotizacionDTO> listaSubIncisos = SubIncisoCotizacionDN.getInstancia().listarFiltrado(subIncisoCotDTO);

				//la lista de riesgos contratados estar� filtrada para la cobertura a la cual se le modificaron sus A/R/D
				//List<RiesgoCotizacionDTO> riesgosContratados = riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(coberturaCotId);
				for(SubIncisoCotizacionDTO subInciso : listaSubIncisos){
					this.calcularSubInciso(subInciso, nombreUsuario);
				}
			}
		}
	}
	
	/**
	 * Este metodo permite realizar el calculo de un riesgo
	 * de acuerdo al el PKGDAN_CALCULOSRIESGOS.SPDAN_CALCULARIESGO 
	 * @param calculoRiesgoDTO
	 *            CalculoRiesgoDTO objeto con los datos para ejecutar
	 *            el SP
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 *             cuando el calculo falla
	 * @return CalculoRiesgoDTO
	 * 				el objeto actualizado con los nuevos calculos            
	 */		
	public CalculoRiesgoDTO calcularRiesgo(CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (calculoRiesgoDTO != null) {
			CalculoCotizacionSN calculoCotizacionSN = new CalculoCotizacionSN();
			calculoCotizacionSN.inicializarCalculoRiesgo(calculoRiesgoDTO.getIdToCotizacion(),nombreUsuario);
			calculoRiesgoDTO = calculoCotizacionSN.calcularRiesgo(calculoRiesgoDTO, nombreUsuario);
		}
		return calculoRiesgoDTO;
	}
	
	/**
	 * Este metodo permite realizar el calculo de un riesgo de acuerdo al el
	 * MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_CALCULARIESGO_EMBARQUE
	 * 
	 * @param calculoRiesgoDTO
	 *            CalculoRiesgoDTO objeto con los datos para ejecutar el SP
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el calculo falla
	 * @return CalculoRiesgoDTO el objeto actualizado con los nuevos calculos
	 */
	public CalculoRiesgoDTO calcularRiesgoEmbarque(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
		if (subIncisoCotizacionDTO != null) {
			CalculoCotizacionSN calculoCotizacionSN = new CalculoCotizacionSN();
			calculoRiesgoDTO.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
			calculoRiesgoDTO.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
			calculoRiesgoDTO.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
			calculoRiesgoDTO.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			calculoRiesgoDTO.setSumaAsegurada(subIncisoCotizacionDTO.getValorSumaAsegurada());

			calculoRiesgoDTO = calculoCotizacionSN.calcularRiesgoEmbarque(
					calculoRiesgoDTO, nombreUsuario);
		}
		return calculoRiesgoDTO;
	}

	/**
	 * Este metodo permite realizar el calculo de un riesgo de acuerdo al el
	 * MIDAS.PKGDAN_CALCULOSRIESGOS.SPDAN_eliminaRIESGO_EMBARQUE
	 * 
	 * @param calculoRiesgoDTO
	 *            CalculoRiesgoDTO objeto con los datos para ejecutar el SP
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException cuando el calculo falla
	 * @return CalculoRiesgoDTO el objeto actualizado con los nuevos calculos
	 */
	public CalculoRiesgoDTO eliminarRiesgoEmbarque(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		CalculoRiesgoDTO calculoRiesgoDTO = new CalculoRiesgoDTO();
		if (subIncisoCotizacionDTO != null) {
			CalculoCotizacionSN calculoCotizacionSN = new CalculoCotizacionSN();
			calculoRiesgoDTO.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
			calculoRiesgoDTO.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
			calculoRiesgoDTO.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
			calculoRiesgoDTO.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			calculoRiesgoDTO.setSumaAsegurada(subIncisoCotizacionDTO.getValorSumaAsegurada());

			calculoRiesgoDTO = calculoCotizacionSN.eliminarRiesgoEmbarque(
					calculoRiesgoDTO, nombreUsuario);
		}
		return calculoRiesgoDTO;
	}	
	/**
	 * Este metodo actualiza la Suma Asuegurada de una cobertura
	 * calculando en base a la sumatoria de la suma asegurada
	 * de sus riesgos basicos
	 * @param id
	 *            CoberturaCotizacionId id de la cobertura
	 *            que se desea actualizar
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 *             cuando el calculo falla         
	 */			
	private void actualizarSumaAseguradaCobertura(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos,
			SystemException {
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		
		coberturaCotizacionDTO.setId(id);

		coberturaCotizacionDTO = coberturaCotizacionSN.getPorId(coberturaCotizacionDTO);

		Double sumatoriaRiesgosBasicos = coberturaCotizacionSN.getSumatoriaRiesgosBasicos(id);
		coberturaCotizacionDTO.setValorSumaAsegurada(sumatoriaRiesgosBasicos);
		coberturaCotizacionSN.modificar(coberturaCotizacionDTO);

	}

	/**
	 * Este metodo actualiza la Suma Asuegurada de una seccion
	 * calculando en base a la sumatoria de la suma asegurada
	 * de sus coberturas basicas
	 * @param id
	 *            SeccionCotizacionDTOId id de la seccion
	 *            que se desea actualizar
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 *             cuando el calculo falla         
	 */			
	private void actualizarSumaAseguradaSeccion(
			SeccionCotizacionDTOId id) throws ExcepcionDeAccesoADatos,
			SystemException {
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		
		Double sumatoriaCoberturasBasicas = seccionCotizacionSN.getSumatoriaCoberturasBasicas(id);
		seccionCotizacionDTO = seccionCotizacionSN.buscarPorId(id);
		seccionCotizacionDTO.setValorSumaAsegurada(sumatoriaCoberturasBasicas);
		
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(id.getIdToCotizacion());
		incisoCotizacionId.setNumeroInciso(id.getNumeroInciso());
		
		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionSN().getPorId(incisoCotizacionId);
		seccionCotizacionDTO.setIncisoCotizacionDTO(incisoCotizacionDTO);
		
		seccionCotizacionSN.modificar(seccionCotizacionDTO);
	}

	public void calcularRiesgosHidro(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, String nombreUsuario) throws SystemException {
		CalculoCotizacionSN calculoCotizacionSN = new CalculoCotizacionSN();
		calculoCotizacionSN.calcularRiesgosHidro(idToCotizacion, numeroInciso, nombreUsuario);
	}

}
