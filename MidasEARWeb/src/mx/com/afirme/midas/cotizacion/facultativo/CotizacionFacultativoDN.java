package mx.com.afirme.midas.cotizacion.facultativo;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroDetalleCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoDetalleCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoDetalleCoberturaCotizacionDN;
import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionId;
import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionId;
import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CotizacionFacultativoDN {
	public static final CotizacionFacultativoDN INSTANCIA = new CotizacionFacultativoDN();

	public static CotizacionFacultativoDN getInstancia (){
		return CotizacionFacultativoDN.INSTANCIA;
	}

	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> obtenerCoberturas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal numeroSubInciso,
			BigDecimal idTcSubRamo) throws ExcepcionDeAccesoADatos, SystemException {
		ReaseguroSubIncisoDetalleCoberturaCotizacionId id = new ReaseguroSubIncisoDetalleCoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		id.setIdToSeccion(idToSeccion);
		id.setNumeroSubInciso(numeroSubInciso);
		id.setIdTcSubRamo(idTcSubRamo);
		
		ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO = new ReaseguroSubIncisoDetalleCoberturaCotizacionDTO();
		reaseguroSubIncisoDetalleCoberturaCotizacionDTO.setId(id);
		ReaseguroSubIncisoDetalleCoberturaCotizacionDN reaseguroSubIncisoDetalleCoberturaCotizacionDN = new ReaseguroSubIncisoDetalleCoberturaCotizacionDN();
		return reaseguroSubIncisoDetalleCoberturaCotizacionDN.listarFiltrado(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	}

	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> obtenerCoberturas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcSubRamo) throws ExcepcionDeAccesoADatos, SystemException {
		ReaseguroIncisoDetalleCoberturaCotizacionId id = new ReaseguroIncisoDetalleCoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		id.setIdTcSubRamo(idTcSubRamo);
		ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO = new ReaseguroIncisoDetalleCoberturaCotizacionDTO();
		reaseguroIncisoDetalleCoberturaCotizacionDTO.setId(id);
		ReaseguroIncisoDetalleCoberturaCotizacionDN reaseguroIncisoDetalleCoberturaCotizacionDN = ReaseguroIncisoDetalleCoberturaCotizacionDN.getInstancia();
		return reaseguroIncisoDetalleCoberturaCotizacionDN.listarFiltrado(reaseguroIncisoDetalleCoberturaCotizacionDTO);
	}

	public List<ReaseguroDetalleCoberturaCotizacionDTO> obtenerCoberturas(
			BigDecimal idToCotizacion, BigDecimal idTcSubRamo) throws ExcepcionDeAccesoADatos, SystemException {
		ReaseguroDetalleCoberturaCotizacionId id = new ReaseguroDetalleCoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdTcSubRamo(idTcSubRamo);
		ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO = new ReaseguroDetalleCoberturaCotizacionDTO();
		reaseguroDetalleCoberturaCotizacionDTO.setId(id);
		ReaseguroDetalleCoberturaCotizacionDN reaseguroDetalleCoberturaCotizacionDN = ReaseguroDetalleCoberturaCotizacionDN.getInstancia();
		return reaseguroDetalleCoberturaCotizacionDN.listarFiltrado(reaseguroDetalleCoberturaCotizacionDTO);
	}

	public List<List<String>> obtenerCoasegurosDeducibles(
			List<CoberturaDTO> coberturas, BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion)
			throws ExcepcionDeAccesoADatos, SystemException {
		Locale mx = new Locale("es", "MX");
		NumberFormat numberFormat = NumberFormat.getInstance();
		Currency currency = Currency.getInstance(mx);
		numberFormat.setCurrency(currency);

		List<String> coaseguros = new ArrayList<String>();
		List<String> deducibles = new ArrayList<String>();
		for(CoberturaDTO cobertura : coberturas) {
			if(cobertura.getClaveDesglosaRiesgos().equals("0")) {
				CoberturaCotizacionId id = new CoberturaCotizacionId();
				id.setIdToCotizacion(idToCotizacion);
				id.setNumeroInciso(numeroInciso);
				id.setIdToSeccion(idToSeccion);
				id.setIdToCobertura(cobertura.getIdToCobertura());
				CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
				coberturaCotizacionDTO.setId(id);

				CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
				coberturaCotizacionDTO = coberturaCotizacionDN .getPorId(coberturaCotizacionDTO);

				if(coberturaCotizacionDTO.getValorCoaseguro() == 0) {
					coaseguros.add("N/A");
				} else {
					coaseguros.add(numberFormat.format(coberturaCotizacionDTO.getValorCoaseguro()) + "%");
				}
				if(coberturaCotizacionDTO.getValorDeducible() == 0) {
					deducibles.add("N/A");
				} else {
					deducibles.add(numberFormat.format(coberturaCotizacionDTO.getValorDeducible()) + "%");
				}
			} else {
				CoaseguroCoberturaSN coaseguroCoberturaSN = new CoaseguroCoberturaSN();
				List<CoaseguroCoberturaDTO> coasegurosCobertura = coaseguroCoberturaSN.listarCoasegurosPorCobertura(cobertura.getIdToCobertura());
				if(!coasegurosCobertura.isEmpty()) {
					CoaseguroCoberturaDTO coaseguroCobertura = coasegurosCobertura.get(0);
					coaseguros.add(numberFormat.format(coaseguroCobertura.getValor()) + "%");
				}
				DeducibleCoberturaSN deducibleCoberturaSN = new DeducibleCoberturaSN();
				List<DeducibleCoberturaDTO> deduciblesCobertura = deducibleCoberturaSN.listarDeduciblesPorCobertura(cobertura.getIdToCobertura());
				if(!deduciblesCobertura.isEmpty()) {
					DeducibleCoberturaDTO deducibleCobertura = deduciblesCobertura.get(0);
					deducibles.add(numberFormat.format(deducibleCobertura.getValor()) + "%");
				}
			}
		}
		List<List<String>> resultado = new ArrayList<List<String>>();
		resultado.add(coaseguros);
		resultado.add(deducibles);
		return resultado;
	}
}
