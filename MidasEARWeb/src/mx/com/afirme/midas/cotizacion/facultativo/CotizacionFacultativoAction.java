package mx.com.afirme.midas.cotizacion.facultativo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionId;
import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Fernando Alonzo
 * @since 16/10/2009
 * 
 */
public class CotizacionFacultativoAction extends MidasMappingDispatchAction {
	/**
	 * Method mostrarCotizacionFacultativo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarCotizacionFacultativo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToSeccion = request.getParameter("idToSeccion");
		String numeroInciso = request.getParameter("numeroInciso");
		String numeroSubInciso = request.getParameter("numeroSubInciso");
		String idTcSubRamo = request.getParameter("idTcSubRamo");
		CotizacionFacultativoForm cotizacionFacultativoForm = (CotizacionFacultativoForm) form;
		try {
			List<CoberturaDTO> coberturas = new ArrayList<CoberturaDTO>();

			CotizacionFacultativoDN cotizacionFacultativoDN = CotizacionFacultativoDN.getInstancia();
			if(numeroSubInciso != null) {
				//Cumulo por subinciso
				List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> coberturasSubInciso = cotizacionFacultativoDN.obtenerCoberturas(UtileriasWeb.regresaBigDecimal(idToCotizacion),
						UtileriasWeb.regresaBigDecimal(numeroInciso),
						UtileriasWeb.regresaBigDecimal(idToSeccion),
						UtileriasWeb.regresaBigDecimal(numeroSubInciso),
						UtileriasWeb.regresaBigDecimal(idTcSubRamo));
				for(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO cobertura : coberturasSubInciso) {
					coberturas.add(cobertura.getCobertura());
				}
				this.poblarFormSubInciso(cotizacionFacultativoForm, coberturasSubInciso, UtileriasWeb.obtieneNombreUsuario(request));
			} else if(numeroInciso != null) {
				//Cumulo por inciso
				List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> coberturasInciso = cotizacionFacultativoDN.obtenerCoberturas(UtileriasWeb.regresaBigDecimal(idToCotizacion),
						UtileriasWeb.regresaBigDecimal(numeroInciso),
						UtileriasWeb.regresaBigDecimal(idTcSubRamo));
				for(ReaseguroIncisoDetalleCoberturaCotizacionDTO cobertura : coberturasInciso) {
					coberturas.add(cobertura.getCobertura());
				}
				this.poblarFormInciso(cotizacionFacultativoForm, coberturasInciso, UtileriasWeb.obtieneNombreUsuario(request));
			} else {
				//Cumulo por tipo poliza
				List<ReaseguroDetalleCoberturaCotizacionDTO> coberturasTipoPoliza = cotizacionFacultativoDN.obtenerCoberturas(UtileriasWeb.regresaBigDecimal(idToCotizacion),
						UtileriasWeb.regresaBigDecimal(idTcSubRamo));
				for(ReaseguroDetalleCoberturaCotizacionDTO cobertura : coberturasTipoPoliza) {
					coberturas.add(cobertura.getCobertura());
				}
				this.poblarFormTipoPoliza(cotizacionFacultativoForm, coberturasTipoPoliza, UtileriasWeb.obtieneNombreUsuario(request));
			}
			numeroInciso = "1";
			idToSeccion = "1";
			List<List<String>> coasegurosDeducibles = cotizacionFacultativoDN.obtenerCoasegurosDeducibles(coberturas, UtileriasWeb.regresaBigDecimal(idToCotizacion),
					UtileriasWeb.regresaBigDecimal(numeroInciso),
					UtileriasWeb.regresaBigDecimal(idToSeccion));
			List<String> coaseguros = coasegurosDeducibles.get(0);
			List<String> deducibles = coasegurosDeducibles.get(1);
			cotizacionFacultativoForm.setCoaseguros(coaseguros.toArray(new String[coaseguros.size()]));
			cotizacionFacultativoForm.setDeducibles(deducibles.toArray(new String[deducibles.size()]));
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
			mensajeExcepcion(cotizacionFacultativoForm, e);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
			mensajeExcepcion(cotizacionFacultativoForm, e);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarFormTipoPoliza(
			CotizacionFacultativoForm cotizacionFacultativoForm,
			List<ReaseguroDetalleCoberturaCotizacionDTO> coberturasTipoPoliza, String nombreUsuario)
	throws ExcepcionDeAccesoADatos, SystemException {
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(coberturasTipoPoliza.get(0).getId().getIdToCotizacion());

		cotizacionFacultativoForm.setCotizacionDTO(cotizacionDTO);
		cotizacionFacultativoForm.setCodigoCotizacion("C-" + String.format("%08d", cotizacionDTO.getIdToCotizacion().intValue()));
		cotizacionFacultativoForm.setCoberturasTipoPoliza(coberturasTipoPoliza);
		cotizacionFacultativoForm.setNombreLinea(coberturasTipoPoliza.get(0).getCobertura().getSubRamoDTO().getDescripcionSubRamo());

		ReaseguroCotizacionDN reaseguroCotizacionDN = ReaseguroCotizacionDN.getInstancia();
		ReaseguroCotizacionId id = new ReaseguroCotizacionId();
		id.setIdToCotizacion(coberturasTipoPoliza.get(0).getId().getIdToCotizacion());
		id.setIdTcSubRamo(coberturasTipoPoliza.get(0).getId().getIdTcSubRamo());
		ReaseguroCotizacionDTO reaseguro = reaseguroCotizacionDN.getPorId(id);

		cotizacionFacultativoForm.setTipoNegocioDTO(reaseguro.getTipoNegocioDTO());
		cotizacionFacultativoForm.setSumaAsegurada(reaseguro.getValorSumaAsegurada());
	}

	private void poblarFormInciso(
			CotizacionFacultativoForm cotizacionFacultativoForm,
			List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> coberturasInciso, String nombreUsuario) 
	throws ExcepcionDeAccesoADatos, SystemException {
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(coberturasInciso.get(0).getId().getIdToCotizacion());

		cotizacionFacultativoForm.setCotizacionDTO(cotizacionDTO);
		cotizacionFacultativoForm.setCodigoCotizacion("C-" + String.format("%08d", cotizacionDTO.getIdToCotizacion().intValue()));
		cotizacionFacultativoForm.setCoberturasInciso(coberturasInciso);
		cotizacionFacultativoForm.setNombreLinea(coberturasInciso.get(0).getCobertura().getSubRamoDTO().getDescripcionSubRamo());
		cotizacionFacultativoForm.setNumeroInciso(Integer.toString(coberturasInciso.get(0).getId().getNumeroInciso().intValue()));

		ReaseguroIncisoCotizacionDN reaseguroIncisoCotizacionDN = ReaseguroIncisoCotizacionDN.getInstancia();
		ReaseguroIncisoCotizacionId id = new ReaseguroIncisoCotizacionId();
		id.setIdToCotizacion(coberturasInciso.get(0).getId().getIdToCotizacion());
		id.setNumeroInciso(coberturasInciso.get(0).getId().getNumeroInciso());
		id.setIdTcSubRamo(coberturasInciso.get(0).getId().getIdTcSubRamo());
		ReaseguroIncisoCotizacionDTO reaseguro = reaseguroIncisoCotizacionDN.getPorId(id);

		cotizacionFacultativoForm.setTipoNegocioDTO(reaseguro.getTipoNegocioDTO());
		cotizacionFacultativoForm.setSumaAsegurada(reaseguro.getValorSumaAsegurada());
	}

	private void poblarFormSubInciso(
			CotizacionFacultativoForm cotizacionFacultativoForm,
			List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> coberturasSubInciso, String nombreUsuario) 
	throws SystemException {
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(coberturasSubInciso.get(0).getId().getIdToCotizacion());

		cotizacionFacultativoForm.setCotizacionDTO(cotizacionDTO);
		cotizacionFacultativoForm.setCodigoCotizacion("C-" + String.format("%08d", cotizacionDTO.getIdToCotizacion().intValue()));
		cotizacionFacultativoForm.setCoberturasSubInciso(coberturasSubInciso);
		cotizacionFacultativoForm.setNombreLinea(coberturasSubInciso.get(0).getCobertura().getSubRamoDTO().getDescripcionSubRamo());
		cotizacionFacultativoForm.setNumeroInciso(Integer.toString(coberturasSubInciso.get(0).getId().getNumeroInciso().intValue()));
		cotizacionFacultativoForm.setNumeroSubInciso(Integer.toString(coberturasSubInciso.get(0).getId().getNumeroSubInciso().intValue()));

		ReaseguroSubIncisoCotizacionDN reaseguroSubIncisoCotizacionDN = ReaseguroSubIncisoCotizacionDN.getInstancia();
		ReaseguroSubIncisoCotizacionId id = new ReaseguroSubIncisoCotizacionId();
		id.setIdToCotizacion(coberturasSubInciso.get(0).getId().getIdToCotizacion());
		id.setNumeroInciso(coberturasSubInciso.get(0).getId().getNumeroInciso());
		id.setIdToSeccion(coberturasSubInciso.get(0).getId().getIdToSeccion());
		id.setNumeroSubinciso(coberturasSubInciso.get(0).getId().getNumeroSubInciso());
		id.setIdTcSubramo(coberturasSubInciso.get(0).getId().getIdTcSubRamo());
		ReaseguroSubIncisoCotizacionDTO reaseguro = reaseguroSubIncisoCotizacionDN.getPorId(id);

		cotizacionFacultativoForm.setTipoNegocioDTO(reaseguro.getTipoNegocioDTO());
		cotizacionFacultativoForm.setSumaAsegurada(reaseguro.getValorSumaAsegurada());
	}

	/**
	 * Method aceptarFacultativo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward aceptarFacultativo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
}