package mx.com.afirme.midas.cotizacion.facultativo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CotizacionFacultativoForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private CotizacionDTO cotizacionDTO;
	private String codigoCotizacion;
	private TipoNegocioDTO tipoNegocioDTO;
	private String nombreLinea;
	private String numeroInciso;
	private String numeroSubInciso;
	private Double sumaAsegurada;
	private List<ReaseguroDetalleCoberturaCotizacionDTO> coberturasTipoPoliza = new ArrayList<ReaseguroDetalleCoberturaCotizacionDTO>();
	private List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> coberturasInciso = new ArrayList<ReaseguroIncisoDetalleCoberturaCotizacionDTO>();
	private List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> coberturasSubInciso = new ArrayList<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO>();
	private String[] coaseguros;
	private String[] deducibles;

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCodigoCotizacion(String codigoCotizacion) {
		this.codigoCotizacion = codigoCotizacion;
	}

	public String getCodigoCotizacion() {
		return codigoCotizacion;
	}

	public void setTipoNegocioDTO(TipoNegocioDTO tipoNegocioDTO) {
		this.tipoNegocioDTO = tipoNegocioDTO;
	}

	public TipoNegocioDTO getTipoNegocioDTO() {
		return tipoNegocioDTO;
	}

	public void setNombreLinea(String nombreLinea) {
		this.nombreLinea = nombreLinea;
	}

	public String getNombreLinea() {
		return nombreLinea;
	}

	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setCoberturasTipoPoliza(List<ReaseguroDetalleCoberturaCotizacionDTO> coberturasTipoPoliza) {
		this.coberturasTipoPoliza = coberturasTipoPoliza;
	}

	public List<ReaseguroDetalleCoberturaCotizacionDTO> getCoberturasTipoPoliza() {
		return coberturasTipoPoliza;
	}

	public void setCoberturasInciso(List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> coberturasInciso) {
		this.coberturasInciso = coberturasInciso;
	}

	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> getCoberturasInciso() {
		return coberturasInciso;
	}

	public void setCoberturasSubInciso(List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> coberturasSubInciso) {
		this.coberturasSubInciso = coberturasSubInciso;
	}

	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> getCoberturasSubInciso() {
		return coberturasSubInciso;
	}

	public void setCoaseguros(String[] coaseguros) {
		this.coaseguros = coaseguros;
	}

	public String[] getCoaseguros() {
		return coaseguros;
	}

	public void setDeducibles(String[] deducibles) {
		this.deducibles = deducibles;
	}

	public String[] getDeducibles() {
		return deducibles;
	}
}
