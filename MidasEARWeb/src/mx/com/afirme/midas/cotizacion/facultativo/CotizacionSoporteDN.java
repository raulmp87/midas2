package mx.com.afirme.midas.cotizacion.facultativo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.SoporteEstructuraCotizacion;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.endoso.SoporteEstructuraCotizacionEndoso;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;
import mx.com.afirme.midas.reaseguro.soporte.validacion.DetalleCobertura;
import mx.com.afirme.midas.reaseguro.soporte.validacion.ValidacionReaseguroFacultativoPorCatalogo;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionEndosoDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class CotizacionSoporteDN {
	private static final Short MODULO_REASEGURO = new Short((short) 2);
	private static final String METODO_CAPTURA = "CotizacionDN.validarReaseguroFacultativo(BigDecimal,HttpServletRequest,CotizacionForm)";
	private static final String objetoOrigenError = "mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguro";
	private static final int ERROR_PREPARA_CARGA_CUMULOS = 1;
	private static final int ERROR_CONSULTA_LINEA_POR_SUBRAMO= 2;
	
	private void registrarErrorInteraccionReaseguro(int clave,String nombreUsuario,Exception e){
		String metodoOrigenError = null;
		String paramMetodoOrigenError = "ninguno";
		String retornoMetodoorigenError = "void";
		String descripcionError = "Error al validar con reaseguro";
		String comentariosAdicionales = "ninguno";
		switch (clave){
			case ERROR_PREPARA_CARGA_CUMULOS:
				metodoOrigenError = "soporteReaseguro.preparaCargaCumulos()";
				descripcionError = "Error al invocar el m�todo preparaCargaCumulos()";
				break;
			case ERROR_CONSULTA_LINEA_POR_SUBRAMO:
				metodoOrigenError = "soporteReaseguro.getTipoDistribucionSubramo(...)";
				descripcionError = "Error al solicitar el tipo de distribucion.";
				break;
		}
		UtileriasWeb.registraLogInteraccionReaseguro(METODO_CAPTURA,MODULO_REASEGURO, objetoOrigenError, metodoOrigenError,nombreUsuario, paramMetodoOrigenError,
				retornoMetodoorigenError, e, descripcionError,comentariosAdicionales, this.toString(), (short) 1);
	}
	
	/**
	 * Metodo para validar los datos de una cotizacion , 
	 * para generar los cumulos (por poliza, inciso y subinciso) segun sea el caso,
	 * @param idToCotizacion
	 * @param request
	 * @param cotizacionForm
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void validarReaseguroFacultativo(BigDecimal idToCotizacion,String nombreUsuario,CotizacionForm cotizacionForm,Map<String,List<LineaSoporteReaseguroDTO>> mapaLineas) throws ExcepcionDeAccesoADatos, SystemException {
		StringBuilder mensajeError= new StringBuilder();
		StringBuilder subRamosNoValidos = new StringBuilder();
		double factor=1;
	   // Constantes para registro de errores.
		String metodoOrigenError;
		String paramMetodoOrigenError;
		String retornoMetodoorigenError;
		@SuppressWarnings("unused")
		String excepcion;
		String descripcionError;
		String comentariosAdicionales;

		boolean mostrarMensajeErrorIncisos = false;
		boolean mostrarMensajeErrorSubRamosNoValidos = false;
		double sumaAseguradaIncendio=0;
		Map<BigDecimal, Double> sumasAseguradasIncendio= new HashMap<BigDecimal, Double>();
		Map<BigDecimal, List<CoberturaCotizacionDTO>> subRamosPrimerRiesgo= new HashMap<BigDecimal, List<CoberturaCotizacionDTO>>();
		SoporteEstructuraCotizacion estructuraCotizacion = null;
		try {
			estructuraCotizacion = new SoporteEstructuraCotizacion(idToCotizacion,nombreUsuario);
		} catch (SystemException e3) {
			LogDeMidasWeb.log("Ocurri� un error al instanciar SoporteEstructuraCotizacion. idToCotizacion="+idToCotizacion+", usuario:"+nombreUsuario, Level.SEVERE, e3);
		}
		if (estructuraCotizacion == null)	return;
		// ArrayList de HashTable con los incisos que no permiten facultativo
//		List<Hashtable<String, Object>> lineasSinFacultativo = new ArrayList<Hashtable<String, Object>>();
		// Obtener los subramos contratados de la cotizacion.
		CotizacionDTO cotizacionDTO = null;
		try {
			estructuraCotizacion.consultarAgrupacionCotizacion();
			estructuraCotizacion.consultarCoberturasContratadasCotizacion();
			estructuraCotizacion.consultarIncisosCotizacion();
			estructuraCotizacion.consultarSubIncisosCotizacion();
			estructuraCotizacion.consultarDetallePrimaCoberturaCotizacion();
			cotizacionDTO = estructuraCotizacion.getCotizacionDTO();
		} catch (SystemException e1) {
			LogDeMidasWeb.log("Ocurri� un error al consultar la informaci�n de la cotizacion en SoporteEstructuraCotizacion. idToCotizacion: "+idToCotizacion, Level.SEVERE, e1);
		}
		Integer ID_MONEDA_COTIZACION = cotizacionDTO.getIdMoneda().intValue();
		factor=UtileriasWeb.getFactorVigencia(cotizacionDTO);
		List<SubRamoDTO> listaSubRamosContratados = null;
		try {
			listaSubRamosContratados = estructuraCotizacion.listarSubRamosContratados();
		} catch (SystemException e2) {}
		if (listaSubRamosContratados == null)	listaSubRamosContratados=new ArrayList<SubRamoDTO>();
		List<IncisoCotizacionDTO> listaIncisos = null;
		try {
			listaIncisos = estructuraCotizacion.listarIncisos();
		} catch (SystemException e1) {}
		SoporteReaseguroCotizacionDN soporteReaseguroCotizacion = null;
	   // Esta l�nea est� arrojando un NullPointerException, por lo cual se agreg� el try - catch para evitar el funcionamiento err�neo
		try {
			soporteReaseguroCotizacion = new SoporteReaseguroCotizacionDN(idToCotizacion);
		} catch (Exception e) {
			UtileriasWeb.registraLogInteraccionReaseguro(METODO_CAPTURA,MODULO_REASEGURO,"new SoporteReaseguro(" + idToCotizacion + ")",
					"Constructor: SoporteReaseguro(" + idToCotizacion+ ")",nombreUsuario,idToCotizacion.toString(),"" + soporteReaseguroCotizacion,
					e,"Error al instanciar un objeto SoporteReaseguro","El constructor que recibe un BigDecimal lanz�; la excepci�n.",this.toString(), (short) 1);
		}
		try {
			soporteReaseguroCotizacion.preparaCargaCumulos();
		} catch (Exception e1) {
			metodoOrigenError = "soporteReaseguro.preparaCargaCumulos()";
			paramMetodoOrigenError = "ninguno";
			retornoMetodoorigenError = "void";
			excepcion = e1.toString();
			descripcionError = "Error al invocar el m�todo preparaCargaCumulos()";
			comentariosAdicionales = "el m�todo preparaCargaCumulos() de la clase SoporteReaseguro lanz� la excepci�n.";
			UtileriasWeb.registraLogInteraccionReaseguro(METODO_CAPTURA,MODULO_REASEGURO, objetoOrigenError, metodoOrigenError,nombreUsuario, paramMetodoOrigenError,
					retornoMetodoorigenError, e1, descripcionError,comentariosAdicionales, this.toString(), (short) 1);
		}
		if (soporteReaseguroCotizacion != null) {
			List<CumuloPoliza> listaCumulos = new ArrayList<CumuloPoliza>();
			// Por cada subramo
			for (SubRamoDTO subRamo : listaSubRamosContratados) {
				LineaDTO lineaDTO=null;
				LogDeMidasWeb.log("CotizacionSoporteDN.validarReaseguroFacultativo: se invocar� SoporteReaseguro.getTipoDistribucionSubramo, para la fechaInicioVigencia: "+cotizacionDTO.getFechaInicioVigencia()+" , idTcSubRamo: "+subRamo.getIdTcSubRamo(), Level.INFO, null);
				try {
					lineaDTO = soporteReaseguroCotizacion.obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), subRamo.getIdTcSubRamo());
				} catch (SystemException e1) {
					UtileriasWeb.registraLogInteraccionReaseguro(METODO_CAPTURA,MODULO_REASEGURO,objetoOrigenError,
							"soporteReaseguro.getTipoDistribucionSubramo("+cotizacionDTO.getFechaInicioVigencia()+ ", "+ subRamo.getIdTcSubRamo()+ "))",nombreUsuario,
							"(cotizacionDTO.getFechaInicioVigencia(),Integer.valueOf(subRamo.getIdTcSubRamo().toBigInteger().toString()))","LineaDTO = " + (lineaDTO!=null?lineaDTO.toString():""),e1,
							"Error al solicitar el tipo de distribuci�n.","el m�todo getTipoDistribuion lanz� la excepci�n.",this.toString(), (short) 1);
				}
				LogDeMidasWeb.log("CotizacionSoporteDN.validarReaseguroFacultativo: termin� la invocaci�n de SoporteReaseguro.getTipoDistribucionSubramo, para la fechaInicioVigencia: "+cotizacionDTO.getFechaInicioVigencia()+" , idTcSubRamo: "+subRamo.getIdTcSubRamo(), Level.INFO, null);
				// Si el tipo de c�mulo es por p�liza:
				if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == Sistema.TIPO_CUMULO_POLIZA) {
					// i. Validar si se requiere facultativo por c�talogo
					ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogoCumuloPoliza(estructuraCotizacion, subRamo, idToCotizacion);//(estructuraCotizacion,tipoCumulo, subRamo,idToCotizacion, null, null, null);

					// ii. Obtener todas las coberturas contratadas de la cotizaci�n correspondientes al subramo en cuesti�n y que no hayan aplicado a primer riesgo.
					List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
					listaCoberturasTMP = estructuraCotizacion.obtenerCoberturasContratadasPorSubRamo(subRamo.getIdTcSubRamo());
					// Que no apliquen primerRiesgo:
					List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
					Double sumaAseguradaLinea = 0d;

					DetalleCobertura detalleCobertura;
					List<DetalleCobertura> listaDetalleCobertura = new ArrayList<DetalleCobertura>();

					Boolean permitirSolicitarFacultativo = validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo();
					Double porcentajePleno = validacionReaseguroPorCatalogo.getPorcentajePleno();
					
					for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
						if (!estructuraCotizacion.aplicoPrimerRiesgo(coberturaCot)) {
							/**
							 * 16/01/2010 Jos� Luis Arellano. Se agreg� validaci�n para no enviar coberturas que aplicaron a LUC.
							 */
							if (!estructuraCotizacion.aplicoLUC(coberturaCot)){
								listaCoberturas.add(coberturaCot);
								// Si es suma asegurada b�sica, se suma
								if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)
										&& !subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)
											&&!subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
								    sumaAseguradaLinea += coberturaCot.getValorSumaAsegurada();
								}
								detalleCobertura = new DetalleCobertura();
								detalleCobertura.setNumeroInciso(coberturaCot.getId().getNumeroInciso().intValue());
								detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
								detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
								detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
								detalleCobertura.setMonedaPrima(ID_MONEDA_COTIZACION);
								detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
								detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta()*factor);
								detalleCobertura.setEsPrimerRiesgo(false);
								listaDetalleCobertura.add(detalleCobertura);
							}
						} else{
						    if(!subRamosPrimerRiesgo.containsKey(subRamo.getIdTcSubRamo())){
						    	subRamosPrimerRiesgo.put(subRamo.getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
						    }
						    subRamosPrimerRiesgo.get(subRamo.getIdTcSubRamo()).add(coberturaCot);
						}
					}
					// Se agreg� esta condici�n para validar que no se envien l�neas sin detalles de coberturas
					if (!listaDetalleCobertura.isEmpty()) {
						if (subRamo.getIdTcSubRamo().equals(Sistema.SUBRAMO_INCENDIO)){
						    sumaAseguradaIncendio= sumaAseguradaLinea;
						}
						if (subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)|| subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
						    sumaAseguradaLinea = sumaAseguradaIncendio;
						}
						CumuloPoliza cumuloPoliza = new CumuloPoliza();
						cumuloPoliza.setIdSubRamo(subRamo.getIdTcSubRamo());
						cumuloPoliza.setIdCotizacion(idToCotizacion);
						cumuloPoliza.setSumaAsegurada(sumaAseguradaLinea);
						cumuloPoliza.setNumeroInciso(null);
						cumuloPoliza.setIdSeccion(null);
						cumuloPoliza.setNumeroSubInciso(null);
						cumuloPoliza.setTipoDistribucion(Sistema.TIPO_CUMULO_POLIZA);
						cumuloPoliza.setLineaDTO(lineaDTO);
						cumuloPoliza.setPorcentajePleno(porcentajePleno);
						cumuloPoliza.setAplicaPrimerRiesgo(false);
						cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
						cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
						cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
						cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
						listaCumulos.add(cumuloPoliza);
							if (!permitirSolicitarFacultativo && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
								if (!mostrarMensajeErrorIncisos){
									mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
									mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
								}
								mensajeError.append("<hr>C�mulo por p&oacute;liza, <br/>SubRamo: "+subRamo.getDescripcionSubRamo()+"<br/>Incisos: ");
								for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
									mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
								mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
							}
						Hashtable<String, Object> linea = new Hashtable<String, Object>();
						linea.put("subRamo", subRamo.getDescripcionSubRamo());
						linea.put("sumaAsegurada", sumaAseguradaLinea);
						linea.put("estatus","Sin permiso para solicitar facultativo");
						List<IncisoCotizacionDTO> incisosPermitenSolicitarFacultativo = validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo();
						linea.put("incisosRequierenFacultativo",incisosPermitenSolicitarFacultativo);
//						lineasSinFacultativo.add(linea); 
					}
				}// Fin c�mulo por poliza
				// Si el c�mulo es por inciso
				else if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == Sistema.TIPO_CUMULO_INCISO) {
					for (IncisoCotizacionDTO inciso : listaIncisos) {
						LogDeMidasWeb.log("CotizacionSoporteDN.validarReaseguroFacultativo: inicia validaci�n por cat�logo para c�mulo por inciso", Level.INFO, null);
						ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(estructuraCotizacion, 2, subRamo, 
								inciso.getId().getNumeroInciso(), null, null);
						LogDeMidasWeb.log("CotizacionSoporteDN.validarReaseguroFacultativo: termina validaci�n por cat�logo para c�mulo por inciso", Level.INFO, null);
						List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
						listaCoberturasTMP = estructuraCotizacion.obtenerCoberturasContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), subRamo.getIdTcSubRamo());
						// Que no apliquen primerRiesgo:
						List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
						double sumaAseguradaLinea = 0;
						DetalleCobertura detalleCobertura;
						List<DetalleCobertura> listaDetalleCobertura = new ArrayList<DetalleCobertura>();
						for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
							try {
								if (!estructuraCotizacion.aplicoPrimerRiesgo(coberturaCot)) {
									/**
									 * 16/01/2010 Jos� Luis Arellano. Se agreg� validaci�n para no enviar coberturas que aplicaron a LUC.
									 */
									if (!estructuraCotizacion.aplicoLUC(coberturaCot)) {
										listaCoberturas.add(coberturaCot);
										if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals("1")
												&& !subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)&&
													!subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
														sumaAseguradaLinea += coberturaCot.getValorSumaAsegurada();
										}
										detalleCobertura = new DetalleCobertura();
										detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
										detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
										detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
										detalleCobertura.setMonedaPrima(ID_MONEDA_COTIZACION);
										detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
										detalleCobertura.setNumeroInciso(Integer.valueOf(inciso.getId().getNumeroInciso().toBigInteger().toString()));
										detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta()*factor);
										detalleCobertura.setEsPrimerRiesgo(false);
										listaDetalleCobertura.add(detalleCobertura);
									}
								} else{
								    if(!subRamosPrimerRiesgo.containsKey(subRamo.getIdTcSubRamo())){
								    	subRamosPrimerRiesgo.put(subRamo.getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
								    }
								    subRamosPrimerRiesgo.get(subRamo.getIdTcSubRamo()).add(coberturaCot);
								}
							} catch (NumberFormatException e) {
								e.printStackTrace();
							}
						}
						// Se agreg� esta condici�n para validar que no se envien l�neas sin detalles de coberturas
						if (!listaDetalleCobertura.isEmpty()) {
						    Double porcentajePleno = validacionReaseguroPorCatalogo.getPorcentajePleno();
							//Si no se encontr� S.A. b�sica, se env�a la suma asegurada por
							//la cual est�n amparadas las coberturas del detalle.
							//Esto ocurre con los subramos de hidrometeorol�gicos y terremoto
						    if(subRamo.getIdTcSubRamo().equals(Sistema.SUBRAMO_INCENDIO)){
					    		if(!sumasAseguradasIncendio.containsKey(inciso.getId().getNumeroInciso())){
					    			sumasAseguradasIncendio.put(inciso.getId().getNumeroInciso(),sumaAseguradaLinea);
					    		}
						    }
						    if (subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)||
						    		subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
						    	if(sumasAseguradasIncendio.containsKey(inciso.getId().getNumeroInciso())){
						    		sumaAseguradaLinea= sumasAseguradasIncendio.get(inciso.getId().getNumeroInciso());
						    	}
						    }
						    CumuloPoliza cumuloPoliza = new CumuloPoliza();
						    cumuloPoliza.setIdSubRamo(subRamo.getIdTcSubRamo());
						    cumuloPoliza.setIdCotizacion(idToCotizacion);
						    cumuloPoliza.setSumaAsegurada(sumaAseguradaLinea);
						    cumuloPoliza.setNumeroInciso(Integer.valueOf(inciso.getId().getNumeroInciso().toBigInteger().toString()));
						    cumuloPoliza.setIdSeccion(null);
						    cumuloPoliza.setNumeroSubInciso(null);
					    	cumuloPoliza.setTipoDistribucion(Sistema.TIPO_CUMULO_INCISO);
					    	cumuloPoliza.setPorcentajePleno(porcentajePleno);
					    	cumuloPoliza.setLineaDTO(lineaDTO);
					    	cumuloPoliza.setAplicaPrimerRiesgo(false);
					    	cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
					    	cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
					    	cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
					    	cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
						    listaCumulos.add(cumuloPoliza);
						}
					}
				}// Fin c�mulo por inciso
				else if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == Sistema.TIPO_CUMULO_SUBINCISO) {
					for (IncisoCotizacionDTO inciso : listaIncisos) {
						List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
						listaCoberturasTMP = estructuraCotizacion.obtenerCoberturasContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), subRamo.getIdTcSubRamo());

						// Que no apliquen primerRiesgo:
						List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
						for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
							if (!estructuraCotizacion.aplicoPrimerRiesgo(coberturaCot)) {
								/**
								 * 16/01/2010 Jos� Luis Arellano. Se agreg� validaci�n para no enviar coberturas con LUC.
								 */
								if (!estructuraCotizacion.aplicoLUC(coberturaCot))
									listaCoberturas.add(coberturaCot);
							} else{
								if(!subRamosPrimerRiesgo.containsKey(subRamo.getIdTcSubRamo())){
									subRamosPrimerRiesgo.put(subRamo.getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
								}
								subRamosPrimerRiesgo.get(subRamo.getIdTcSubRamo()).add(coberturaCot);
							}
						}
						if(listaCoberturas.size()>0){
							List<SubIncisoCotizacionDTO> listaSubIncisos = estructuraCotizacion.obtenerSubIncisos(listaCoberturas.get(0).getId().getIdToSeccion(), inciso.getId().getNumeroInciso());
							DetalleCobertura detalleCobertura;
							List<DetalleCobertura> listaDetalleCobertura = null;
							Double porcentajePleno = 100d;
							for (SubIncisoCotizacionDTO subInciso : listaSubIncisos) {
								ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroFacultativoPorCatalogo = validarReaseguroFacultativoPorCatalogo(
										estructuraCotizacion, 3, subRamo, inciso.getId().getNumeroInciso(), subInciso.getId().getIdToSeccion(), 
										subInciso.getId().getNumeroSubInciso());
								porcentajePleno = validacionReaseguroFacultativoPorCatalogo.getPorcentajePleno();
								Double sumatoriaSumaAsegurada = subInciso.getValorSumaAsegurada();
								listaDetalleCobertura= new ArrayList<DetalleCobertura>();
								for (CoberturaCotizacionDTO coberturaCot : listaCoberturas) {
									DetallePrimaCoberturaCotizacionId detallePrimaId = new DetallePrimaCoberturaCotizacionId();
									detallePrimaId.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
									detallePrimaId.setIdToCotizacion(idToCotizacion);
									detallePrimaId.setNumeroInciso(inciso.getId().getNumeroInciso());
									detallePrimaId.setIdToSeccion(coberturaCot.getId().getIdToSeccion());
									detallePrimaId.setIdToCobertura(coberturaCot.getId().getIdToCobertura());
									DetallePrimaCoberturaCotizacionDTO detalle = null;
									detalle = estructuraCotizacion.obtenerDetallePrimaCoberturaCotizacionPorId(detallePrimaId);
									if (detalle != null) {
										detalleCobertura = new DetalleCobertura();
										detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
										detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
										detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
										detalleCobertura.setMonedaPrima(ID_MONEDA_COTIZACION);
										detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
										detalleCobertura.setNumeroInciso(Integer.valueOf(inciso.getId().getNumeroInciso().toBigInteger().toString()));
										detalleCobertura.setMontoPrimaSuscripcion(detalle.getValorPrimaNeta()*factor);
										detalleCobertura.setEsPrimerRiesgo(false);
										detalleCobertura.setNumeroSubinciso(Integer.valueOf(subInciso.getId().getNumeroSubInciso().toBigInteger().toString()));
										listaDetalleCobertura.add(detalleCobertura);
									}
								}// fin iterar CoberturaCot
								if (!listaDetalleCobertura.isEmpty()) {
									CumuloPoliza cumuloPoliza = new CumuloPoliza();
									cumuloPoliza.setIdSubRamo(subRamo.getIdTcSubRamo());
									cumuloPoliza.setIdCotizacion(idToCotizacion);
									cumuloPoliza.setSumaAsegurada(sumatoriaSumaAsegurada);
									cumuloPoliza.setNumeroInciso(Integer.valueOf(subInciso.getId().getNumeroInciso().toBigInteger().toString()));
									cumuloPoliza.setIdSeccion(subInciso.getId().getIdToSeccion());
									cumuloPoliza.setNumeroSubInciso(Integer.valueOf(subInciso.getId().getNumeroSubInciso().toBigInteger().toString()));
									cumuloPoliza.setTipoDistribucion(Sistema.TIPO_CUMULO_SUBINCISO);
									cumuloPoliza.setLineaDTO(lineaDTO);
									cumuloPoliza.setPorcentajePleno(porcentajePleno);
									cumuloPoliza.setAplicaPrimerRiesgo(false);
									cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
									cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
									cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
									cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
									listaCumulos.add(cumuloPoliza);
								}
							}// fin iterar subincisos
						}// fin if listaCoberturas > 0
					}// fin ciclo incisos
				}// fin c�mulo por subinciso
				else{
					subRamosNoValidos.append(subRamo.getDescripcionSubRamo()).append("<br/>");
				    mostrarMensajeErrorSubRamosNoValidos = true;
				}
			}// fin ciclo subRamo
			List<AgrupacionCotDTO> listaAgrupacionCotizacion = null;
			listaAgrupacionCotizacion = estructuraCotizacion.obtenerListaAgrupacionCotizacion();
			if (listaAgrupacionCotizacion != null && !listaAgrupacionCotizacion.isEmpty()) {
				for(AgrupacionCotDTO agrupacionTMP : listaAgrupacionCotizacion){
					if (agrupacionTMP.getClaveTipoAgrupacion().intValue() == 1){
						Iterator<Entry<BigDecimal, List<CoberturaCotizacionDTO>>> it = subRamosPrimerRiesgo.entrySet().iterator();
						while (it.hasNext()){
							Entry<BigDecimal, List<CoberturaCotizacionDTO>> subRamoLista = (Entry<BigDecimal, List<CoberturaCotizacionDTO>>)it.next();
							SubRamoDTO subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoLista.getKey());
							ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = 
								validarReaseguroFacultativoPorCatalogo(estructuraCotizacion,Sistema.TIPO_CUMULO_POLIZA, 
											subRamoDTO, null, null, null);
							DetalleCobertura  detalleCobertura;
							List<DetalleCobertura>listaDetalleCobertura= new ArrayList<DetalleCobertura>();
							int numeroIncisoCumulo = 0;
							int j = 0;
							for (CoberturaCotizacionDTO coberturaCot : subRamoLista.getValue()) {
								detalleCobertura = new DetalleCobertura();
								detalleCobertura.setNumeroInciso(Integer.valueOf(coberturaCot.getId().getNumeroInciso().toBigInteger().toString()));
								detalleCobertura.setIdSeccion(coberturaCot.getId().getIdToSeccion());
								detalleCobertura.setIdCobertura(coberturaCot.getId().getIdToCobertura());
								detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
								detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta()*factor);
								detalleCobertura.setMonedaPrima(Integer.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
								detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
								detalleCobertura.setEsPrimerRiesgo(true);
								detalleCobertura.setNumeroSubinciso(0);
								listaDetalleCobertura.add(detalleCobertura);
								//12/01/2010 Calculo del numeroInciso del c�mulo
								if(j==0){
									numeroIncisoCumulo = coberturaCot.getId().getNumeroInciso().intValue();
									j=j+1;
								}
								else{
									if(numeroIncisoCumulo != coberturaCot.getId().getNumeroInciso().intValue()){
										numeroIncisoCumulo = 0;
									}
									j=j+1;
								}
							}
							// Obtener el inciso al que pertenece la cobertura
							LineaDTO lineaTMP = soporteReaseguroCotizacion.obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), subRamoLista.getKey());
							CumuloPoliza cumuloPoliza = new CumuloPoliza();
							cumuloPoliza.setIdCotizacion(idToCotizacion);
							cumuloPoliza.setIdSubRamo(subRamoLista.getKey());
							cumuloPoliza.setSumaAsegurada(agrupacionTMP.getValorSumaAsegurada());
							cumuloPoliza.setTipoDistribucion(2);
							cumuloPoliza.setLineaDTO(lineaTMP);
							cumuloPoliza.setAplicaPrimerRiesgo(true);
							cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
							cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
							cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
							cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
							cumuloPoliza.setPorcentajePleno(validacionReaseguroPorCatalogo.getPorcentajePleno());
							cumuloPoliza.setNumeroInciso(numeroIncisoCumulo);
							cumuloPoliza.setNumeroSubInciso(0);
							listaCumulos.add(cumuloPoliza);
							if (!validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo() && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
								if (!mostrarMensajeErrorIncisos){
									mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
									mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
								}
								mensajeError.append("<hr>C�mulo por primer riesgo, <br/>SubRamo: "+subRamoDTO.getDescripcionSubRamo()+"<br/>Incisos: ");
								for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
									mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
								mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
							}
						}
					}//Fin validaci�n agrupacionTMP.getClaveTipoAgrupacion().intValue() == 1
					/**
					 * 16/01/2010. Jos� Luis Arellano.
					 * Para cada agrupaci�n de LUC se env�a un c�mulo.
					 */
					else{
						List<CoberturaCotizacionDTO> listaCoberturasLUC = null;
						listaCoberturasLUC = estructuraCotizacion.obtenerCoberturasContratadasLUCPorNumeroAgrupacion(agrupacionTMP.getId().getNumeroAgrupacion());
						List<DetalleCobertura> listaDetalleCoberturaLuc = new ArrayList<DetalleCobertura>();
						BigDecimal idTcSubRamoCumulo = BigDecimal.ZERO;
						if (listaCoberturasLUC != null && !listaCoberturasLUC.isEmpty()){
							idTcSubRamoCumulo = listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo();
							ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(estructuraCotizacion,Sistema.TIPO_CUMULO_POLIZA, 
									listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO(), null, null, null);
							int numeroIncisoCumuloLUC = 0;
							int j=0;
							for(CoberturaCotizacionDTO coberturaLuc : listaCoberturasLUC){
							  	DetalleCobertura detalleCobertura = new DetalleCobertura();
								detalleCobertura.setNumeroInciso(Integer.valueOf(coberturaLuc.getId().getNumeroInciso().toBigInteger().toString()));
								detalleCobertura.setIdSeccion(agrupacionTMP.getIdToSeccion());
								detalleCobertura.setIdCobertura(coberturaLuc.getId().getIdToCobertura());
								detalleCobertura.setPorcentajeCoaseguro(coberturaLuc.getPorcentajeCoaseguro());
								detalleCobertura.setMontoPrimaSuscripcion(coberturaLuc.getValorPrimaNeta()*factor);
								detalleCobertura.setMonedaPrima(ID_MONEDA_COTIZACION);
								detalleCobertura.setPorcentajeDeducible(coberturaLuc.getPorcentajeDeducible());
								/**
								 * 16/01/2010. Jos� Luis Arellano.
								 * Por especificaci�n de Aram, el campo de esPrimerRiesgo se env�a con valor de false en el caso de los c�mulos por LUC
								 */
								detalleCobertura.setEsPrimerRiesgo(false);
								detalleCobertura.setNumeroSubinciso(0);
								listaDetalleCoberturaLuc.add(detalleCobertura);
								if(j==0){
									numeroIncisoCumuloLUC = coberturaLuc.getId().getNumeroInciso().intValue();
									j=j+1;
								}
								else{
									if(numeroIncisoCumuloLUC != coberturaLuc.getId().getNumeroInciso().intValue()){
										numeroIncisoCumuloLUC = 0;
									}
									j=j+1;
								}
							}
							CumuloPoliza cumuloPoliza = new CumuloPoliza();
							cumuloPoliza.setIdCotizacion(idToCotizacion);
							cumuloPoliza.setIdSubRamo(idTcSubRamoCumulo);
							cumuloPoliza.setSumaAsegurada(agrupacionTMP.getValorSumaAsegurada());
							cumuloPoliza.setNumeroInciso(numeroIncisoCumuloLUC);
							LineaDTO lineaTMP = null;
							try {
								lineaTMP = soporteReaseguroCotizacion.obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), idTcSubRamoCumulo);
							} catch (SystemException e1) {}
							if(lineaTMP != null){
								cumuloPoliza.setTipoDistribucion(lineaTMP.getTipoDistribucion().intValue());
								cumuloPoliza.setLineaDTO(lineaTMP);
								/**
								 * 16/01/2010. Jos� Luis Arellano.
								 * Por especificaci�n de Aram, el campo de aplicaPrimerRiesgo se env�a con valor de false en el caso de los c�mulos por LUC
								 */
								cumuloPoliza.setAplicaPrimerRiesgo(false);
								cumuloPoliza.setDetalleCoberturas(listaDetalleCoberturaLuc);
								cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
								cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
								cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
								cumuloPoliza.setPorcentajePleno(validacionReaseguroPorCatalogo.getPorcentajePleno());
								listaCumulos.add(cumuloPoliza);
								if (!validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo() && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
									if (!mostrarMensajeErrorIncisos){
										mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
										mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
									}
									mensajeError.append("<hr>C�mulo por primer riesgo,<br/>SubRamo: "+
											listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getDescripcionSubRamo()+"<br/>Incisos:");
									for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
										mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
									mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-1));
								}
							}
							else{
								subRamosNoValidos.append(listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getDescripcionSubRamo()).append("<br/>");
							    mostrarMensajeErrorSubRamosNoValidos = true;
							}
							
						}
					}
				}
			}//fin validaci�n listaAgrupacion != null && not Empty
			try {
				soporteReaseguroCotizacion.insertarCumulosLinea(listaCumulos, nombreUsuario);
			} catch (ExcepcionDeLogicaNegocio e) {
			}
			if(mapaLineas != null){
				List<LineaSoporteReaseguroDTO> listaCumuloPoliza = soporteReaseguroCotizacion.getListaLineasSoporte(new BigDecimal(Sistema.TIPO_CUMULO_POLIZA));
				List<LineaSoporteReaseguroDTO> listaCumuloInciso = soporteReaseguroCotizacion.getListaLineasSoporte(new BigDecimal(Sistema.TIPO_CUMULO_INCISO));
				List<LineaSoporteReaseguroDTO> listaCumuloSubInciso = soporteReaseguroCotizacion.getListaLineasSoporte(new BigDecimal(Sistema.TIPO_CUMULO_SUBINCISO));
				mapaLineas.put("listaCumuloPoliza",listaCumuloPoliza);
				mapaLineas.put("listaCumuloInciso",listaCumuloInciso);
				mapaLineas.put("listaCumuloSubInciso",listaCumuloSubInciso);
			}
			String mensajesErrorCumulo = "";
			if (mostrarMensajeErrorSubRamosNoValidos){
				mensajesErrorCumulo += "Los siguientes SubRamos no presentaron tipo de cumulo v�lido : <br/>"+subRamosNoValidos.toString();
				mensajesErrorCumulo += "<hr>";
			}
			mensajesErrorCumulo += mensajeError.toString().length()>0?mensajeError.toString():"";
			cotizacionForm.setMensajeErrorCumulo(mensajesErrorCumulo.length()>0?mensajesErrorCumulo:Sistema.SIN_MENSAJE_PARA_MOSTRAR);
		}// Fin validaci�n nulo
		LogDeMidasWeb.log("Saliendo de CotizacionSoporteDN.validarReaseguroFacultativo", Level.INFO, null);
	}
	/**
	 * Metodo para validar los datos de una cotizacion , 
	 * para generar los cumulos (por poliza, inciso y subinciso) segun sea
	 * el caso,cuando se trata de un endoso
	 * @param idToCotizacion
	 * @param request
	 * @param cotizacionForm
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void validarReaseguroFacultativoEndosoV2(BigDecimal idToCotizacion,String nombreUsuario,CotizacionForm cotizacionForm,Map<String,List<LineaSoporteReaseguroDTO>> mapaListaCumulos,boolean recalcularMovimientosEndoso) throws SystemException {
		CotizacionDTO cotizacionDTO = null;
		EndosoDTO ultimoEndosoDTO = null;
		Double factorVigencia = null;
		CotizacionDN cotizacionDN= CotizacionDN.getInstancia(null);
		double sumaAseguradaIncendio=0;
		Map<BigDecimal, Double> sumasAseguradasIncendio= new HashMap<BigDecimal, Double>();
		boolean mostrarMensajeErrorIncisos = false;
		boolean mostrarMensajeErrorSubRamosNoValidos = false;
		StringBuilder mensajeError= new StringBuilder();
		StringBuilder subRamosNoValidos = new StringBuilder();
		Map<BigDecimal, List<CoberturaCotizacionDTO>> listasCoberturasNoContratadasLUC = new HashMap<BigDecimal, List<CoberturaCotizacionDTO>>();
		Map<BigDecimal, List<CoberturaCotizacionDTO>> subRamosPrimerRiesgo= new HashMap<BigDecimal, List<CoberturaCotizacionDTO>>();
		SoporteEstructuraCotizacionEndoso estructuraCotizacionEndoso = null;
		
		cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
		ultimoEndosoDTO = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		estructuraCotizacionEndoso = new SoporteEstructuraCotizacionEndoso(cotizacionDTO, ultimoEndosoDTO.getIdToCotizacion(),null, nombreUsuario);
		estructuraCotizacionEndoso.consultarInformacionValidacionCotizacion(nombreUsuario);
		factorVigencia = UtileriasWeb.getFactorVigencia(cotizacionDTO);
		
		if(factorVigencia == null){
			throw new SystemException("Factor de Vigencia no pudo ser calculado.");
		}
		Integer monedaCotizacion = estructuraCotizacionEndoso.getCotizacionDTO().getIdMoneda().intValue();
		/*
    	 * COLOCAR EL RECAULCULO DE MOVIMIENTOS
    	 * solo se calculan para los endosos de Modificacion
    	 * agregado por JACC 19/02/10
    	 * 15/03/2011 Se modifica el c�lculo de movimientos.
    	 */
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION && recalcularMovimientosEndoso){
//			MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).calcularDiferenciasDeEndoso(idToCotizacion);
			estructuraCotizacionEndoso.calcularDiferenciasDeEndoso(nombreUsuario);
		}
		
		estructuraCotizacionEndoso.consultarMovimientosEndoso(nombreUsuario);
		
		List<SubRamoDTO> listaSubRamosEndoso = null;
		List<IncisoCotizacionDTO> listaIncisos = null;
		
		listaSubRamosEndoso = estructuraCotizacionEndoso.listarSubRamosEndoso();
		listaIncisos = estructuraCotizacionEndoso.listarIncisosEndoso();
		
		SoporteReaseguroCotizacionEndosoDN soporteReaseguroCotizacionEndoso = null;
		try {
			soporteReaseguroCotizacionEndoso = new SoporteReaseguroCotizacionEndosoDN(ultimoEndosoDTO.getId().getIdToPoliza(),new Integer(ultimoEndosoDTO.getId().getNumeroEndoso()),idToCotizacion);
		} catch (SystemException e) {
			UtileriasWeb.registraLogInteraccionReaseguro(METODO_CAPTURA,MODULO_REASEGURO,"new SoporteReaseguro(" + idToCotizacion + ")",
					"Constructor: SoporteReaseguro(" + idToCotizacion+ ")",nombreUsuario,idToCotizacion.toString(),"" + soporteReaseguroCotizacionEndoso,
					e,"Error al instanciar un objeto SoporteReaseguro","El constructor que recibe un BigDecimal lanz&oacute; la excepci&oacute;n.",this.toString(), (short) 1);
			throw e;
		}
		try {
			soporteReaseguroCotizacionEndoso.preparaCargaCumulos();
		} catch (SystemException e1) {
			registrarErrorInteraccionReaseguro(ERROR_PREPARA_CARGA_CUMULOS, nombreUsuario, e1);
			throw e1;
		}
		List<CoberturaCotizacionDTO> listaCoberturaPrimerRiesgoNoContratadas = new ArrayList<CoberturaCotizacionDTO>();
		List<CumuloPoliza> listaCumulos = new ArrayList<CumuloPoliza>();
		// Por cada subramo
		for (SubRamoDTO subRamo : listaSubRamosEndoso) {
			LineaDTO lineaDTO = null;
			try {
				lineaDTO = soporteReaseguroCotizacionEndoso.obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), subRamo.getIdTcSubRamo());
			} catch (SystemException e1) {
				registrarErrorInteraccionReaseguro(ERROR_CONSULTA_LINEA_POR_SUBRAMO,nombreUsuario,e1);
			}
			// Si el tipo de c�mulo es por p�liza:
			if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == Sistema.TIPO_CUMULO_POLIZA) {
    				// i. Validar si se requiere facultativo por c�talogo
				ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(
						estructuraCotizacionEndoso,lineaDTO.getTipoDistribucion().intValue(),subRamo,null,null,null);
    				// ii. Obtener todas las coberturas contratadas de la cotizaci�n correspondientes al subramo en cuesti�n y que 
    				//no hayan aplicado a primer riesgo.
				List<CoberturaCotizacionDTO> listaCoberturasTMP = null;
				listaCoberturasTMP = estructuraCotizacionEndoso.obtenerCoberturasContratadasPorSubRamo(subRamo.getIdTcSubRamo());
    				// Que no apliquen primerRiesgo:
				List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
				Double sumaAseguradaLinea = 0d;
				DetalleCobertura detalleCobertura;
				List<DetalleCobertura> listaDetalleCobertura = new ArrayList<DetalleCobertura>();
				List<DetalleCobertura> listaDetalleCoberturaNoContratadas = new ArrayList<DetalleCobertura>();
    
				Boolean permitirSolicitarFacultativo = validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo();
				Double porcentajePleno = validacionReaseguroPorCatalogo.getPorcentajePleno();
    
				for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
					//B�squeda de la cobertura en curso en la lista de coberturas contratadas del endoso anterior
					CoberturaCotizacionDTO coberturaUltimoEndoso = estructuraCotizacionEndoso.
										getEstructuraCotizacionEndosoAnterior().obtenerCoberturaContratadaPorId(coberturaCot.getId().getNumeroInciso(),
												coberturaCot.getId().getIdToSeccion(),coberturaCot.getId().getIdToCobertura());
					if(estructuraCotizacionEndoso.aplicoPrimerRiesgo(coberturaCot)){
						if(!subRamosPrimerRiesgo.containsKey(subRamo.getIdTcSubRamo())){
							subRamosPrimerRiesgo.put(subRamo.getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
						}
						subRamosPrimerRiesgo.get(subRamo.getIdTcSubRamo()).add(coberturaCot);
						
						if(coberturaUltimoEndoso!=null){
							boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaUltimoEndoso);
							if(!aplico1erRiesgoCobEndAnt)
								listaCoberturaPrimerRiesgoNoContratadas.add(coberturaUltimoEndoso);
						}
					}//fin aplioPrimerRiesgo
					else if(estructuraCotizacionEndoso.aplicoLUC(coberturaCot)){
						if(coberturaUltimoEndoso!=null){
							boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaUltimoEndoso);
							if(!aplicoLUCCobEndAnt){
								if(listasCoberturasNoContratadasLUC.get(coberturaUltimoEndoso.getId().getIdToSeccion())==null){
									listasCoberturasNoContratadasLUC.put(coberturaUltimoEndoso.getId().getIdToSeccion(),new ArrayList<CoberturaCotizacionDTO>() );
								}
								listasCoberturasNoContratadasLUC.get(coberturaUltimoEndoso.getId().getIdToSeccion()).add(coberturaUltimoEndoso);
							}
						}
					}//fin aplicoLUC
					else{
						listaCoberturas.add(coberturaCot);
						// Si es suma asegurada b�sica, se suma
						CoberturaDTO coberturaTMP = coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO();
						if (coberturaTMP.getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)
								&& !subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)&&
								!subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
							sumaAseguradaLinea += coberturaCot.getValorSumaAsegurada();
						}
						detalleCobertura = new DetalleCobertura(coberturaCot.getId().getNumeroInciso().intValue(),
								coberturaCot.getId().getIdToSeccion(),null,coberturaCot.getId().getIdToCobertura(),false);
						detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
						detalleCobertura.setMonedaPrima(monedaCotizacion);
						detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
						detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta() * factorVigencia);
						listaDetalleCobertura.add(detalleCobertura);
						
						if(coberturaUltimoEndoso!=null){
							boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaUltimoEndoso);
							boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaUltimoEndoso);
							if(!aplico1erRiesgoCobEndAnt && !aplicoLUCCobEndAnt){
								//La cobertura lleva prima no devengada s�lo si no hab�a aplicado a Primer Riesgo o LUC en el endoso anterior
								detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
							}else{        								
								detalleCobertura.setMontoPrimaNoDevengada(0D);
								//se crea nuevo detalle para dar de baja la cobertura que hab�a aplicado Primer Riesgo/LUC
								detalleCobertura = new DetalleCobertura(coberturaUltimoEndoso.getId().getNumeroInciso().intValue(),
										coberturaUltimoEndoso.getId().getIdToSeccion(),null,
										coberturaUltimoEndoso.getId().getIdToCobertura(),aplico1erRiesgoCobEndAnt);
								detalleCobertura.setPorcentajeCoaseguro(coberturaUltimoEndoso.getPorcentajeCoaseguro());
								detalleCobertura.setMonedaPrima(monedaCotizacion);
								detalleCobertura.setPorcentajeDeducible(coberturaUltimoEndoso.getPorcentajeDeducible());
								detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
								detalleCobertura.setMontoPrimaSuscripcion(0D);
								listaDetalleCoberturaNoContratadas.add(detalleCobertura);        								            								
							}
						}else{//La cobertura en el endoso anterior no estaba contratada
							detalleCobertura.setMontoPrimaNoDevengada(0D);
						}
					}
				}
				//Consultar los movimientos de baja de coberturas del subramo en curso
				List<CoberturaCotizacionDTO> listaCoberturasDescontratadas = estructuraCotizacionEndoso.obtenerCoberturasDescontratadasPorSubRamo(subRamo.getIdTcSubRamo());
				for(CoberturaCotizacionDTO coberturaDescontratada : listaCoberturasDescontratadas){
					boolean aplicoLUCCobDescEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaDescontratada);
					boolean aplico1erRiesgoCobDescEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaDescontratada);
					if(aplico1erRiesgoCobDescEndAnt){
					    listaCoberturaPrimerRiesgoNoContratadas.add(coberturaDescontratada);
					}else  if(aplicoLUCCobDescEndAnt){
						//listaCoberturaLUCNoContratadas.add(cobertura);
						if(listasCoberturasNoContratadasLUC.get(coberturaDescontratada.getId().getIdToSeccion())==null){
							listasCoberturasNoContratadasLUC.put(coberturaDescontratada.getId().getIdToSeccion(),new ArrayList<CoberturaCotizacionDTO>() );
						}
						listasCoberturasNoContratadasLUC.get(coberturaDescontratada.getId().getIdToSeccion()).add(coberturaDescontratada);
					}else{
						detalleCobertura = new DetalleCobertura(
								coberturaDescontratada.getId().getNumeroInciso().intValue(),
								coberturaDescontratada.getId().getIdToSeccion(),null,
								coberturaDescontratada.getId().getIdToCobertura(),aplico1erRiesgoCobDescEndAnt);
						detalleCobertura.setPorcentajeCoaseguro(coberturaDescontratada.getPorcentajeCoaseguro());
						detalleCobertura.setMonedaPrima(monedaCotizacion);
						detalleCobertura.setPorcentajeDeducible(coberturaDescontratada.getPorcentajeDeducible());
						detalleCobertura.setMontoPrimaNoDevengada(coberturaDescontratada.getValorPrimaNeta() * factorVigencia);
						detalleCobertura.setMontoPrimaSuscripcion(0D);//cobertura descontratada
						listaDetalleCoberturaNoContratadas.add(detalleCobertura);
					}//fin else (no aplico primer riesgo)
				}
				//Une la lista de coberturas de detalle con las coberturas no contratadas.
				if(!listaDetalleCoberturaNoContratadas.isEmpty()){
					listaDetalleCobertura.addAll(listaDetalleCoberturaNoContratadas);
				}
    				// Se agreg� esta condici�n para validar que no se envien l�neas sin detalles de coberturas
				if (!listaDetalleCobertura.isEmpty()) {
					//Si no se encontr� S.A. b�sica, se env�a la suma asegurada por la cual est�n amparadas las coberturas del detalle.
					//Esto ocurre con los subramos de hidrometeorol�gicos y terremoto  
					if(subRamo.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_INCENDIO) ==0){
						sumaAseguradaIncendio= sumaAseguradaLinea;
					}
					if (subRamo.getIdTcSubRamo().compareTo(Sistema.IDTCSUBRAMO_HIDRO) == 0 || 
							subRamo.getIdTcSubRamo().compareTo(Sistema.IDTCSUBRAMO_TERREMOTO) == 0){
						sumaAseguradaLinea= sumaAseguradaIncendio;
					}
					CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamo.getIdTcSubRamo(),null,null,null,lineaDTO,Sistema.TIPO_CUMULO_POLIZA);
    				cumuloPoliza.setSumaAsegurada(sumaAseguradaLinea);
   					cumuloPoliza.setPorcentajePleno(porcentajePleno);
   					cumuloPoliza.setAplicaPrimerRiesgo(false);
   					cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
   					cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
   					cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
   					cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
   					listaCumulos.add(cumuloPoliza);
					if (!permitirSolicitarFacultativo && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
						if (!mostrarMensajeErrorIncisos){
							mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
							mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
						}
						mensajeError.append("<hr>C�mulo por p&oacute;liza, <br/>SubRamo: "+subRamo.getDescripcionSubRamo()+"<br/>Incisos: ");
						for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
							mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
						mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
					}
				}
			}// Fin c�mulo por poliza
			else if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == Sistema.TIPO_CUMULO_INCISO) {// Si el c�mulo es por inciso
				for (IncisoCotizacionDTO inciso : listaIncisos) {
					ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(
							estructuraCotizacionEndoso,Sistema.TIPO_CUMULO_INCISO,subRamo,inciso.getId().getNumeroInciso(),null,null);
					//Consulta de las coberturas contratadas del subramo e inciso en curso
					List<CoberturaCotizacionDTO> listaCoberturasTMP = estructuraCotizacionEndoso.
										obtenerCoberturasContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), subRamo.getIdTcSubRamo());
					// Que no apliquen primerRiesgo:
					List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
					double sumaAseguradaLinea = 0;
					DetalleCobertura detalleCobertura;
					List<DetalleCobertura> listaDetalleCobertura = new ArrayList<DetalleCobertura>();
					List<DetalleCobertura> listaDetalleCoberturaNoContratadas = new ArrayList<DetalleCobertura>();
					for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
						CoberturaCotizacionDTO coberturaUltimoEndoso = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerCoberturaContratadaPorId(
								coberturaCot.getId().getNumeroInciso(), coberturaCot.getId().getIdToSeccion(), coberturaCot.getId().getIdToCobertura());
						if(estructuraCotizacionEndoso.aplicoPrimerRiesgo(coberturaCot)){
							if(!subRamosPrimerRiesgo.containsKey(subRamo.getIdTcSubRamo())){
								subRamosPrimerRiesgo.put(subRamo.getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
							}
							subRamosPrimerRiesgo.get(subRamo.getIdTcSubRamo()).add(coberturaCot);
							if(coberturaUltimoEndoso != null){
								boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaUltimoEndoso);
								if(!aplico1erRiesgoCobEndAnt)
									listaCoberturaPrimerRiesgoNoContratadas.add(coberturaUltimoEndoso);
							}
						}//fin validacion primer riesgo
						else if(estructuraCotizacionEndoso.aplicoLUC(coberturaCot)){
							if(coberturaUltimoEndoso!=null){
								boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaUltimoEndoso);
								if(!aplicoLUCCobEndAnt){
									if(listasCoberturasNoContratadasLUC.get(coberturaUltimoEndoso.getId().getIdToSeccion())==null){
										listasCoberturasNoContratadasLUC.put(coberturaUltimoEndoso.getId().getIdToSeccion(),new ArrayList<CoberturaCotizacionDTO>() );
									}
									listasCoberturasNoContratadasLUC.get(coberturaUltimoEndoso.getId().getIdToSeccion()).add(coberturaUltimoEndoso);
								}
							}
						}//fin validacion LUC
						else{//no aplico LUC ni primer riesgo en el endoso anterior
							listaCoberturas.add(coberturaCot);
							if (coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)
									&& !subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)&&
										!subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
								sumaAseguradaLinea += coberturaCot.getValorSumaAsegurada();
							}
							detalleCobertura = new DetalleCobertura(inciso.getId().getNumeroInciso().intValue(),coberturaCot.getId().getIdToSeccion(),null,
									coberturaCot.getId().getIdToCobertura(),false);
							detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
							detalleCobertura.setMonedaPrima(Integer.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
							detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
							detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta() * factorVigencia);
							listaDetalleCobertura.add(detalleCobertura);
							
							if(coberturaUltimoEndoso!=null){
								boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaUltimoEndoso);
								boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaUltimoEndoso);
								if(!aplico1erRiesgoCobEndAnt &&!aplicoLUCCobEndAnt){
									//La cobertura lleva prima no devengada s�lo si no hab�a aplicado a Primer Riesgo o LUC en el endoso anterior
									detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
								}else{
									detalleCobertura.setMontoPrimaNoDevengada(0D);
									//se crea nuevo detalle para dar de baja la cobertura que hab�a aplicado Primer Riesgo/LUC
									detalleCobertura = new DetalleCobertura(coberturaUltimoEndoso.getId().getNumeroInciso().intValue(),
											coberturaUltimoEndoso.getId().getIdToSeccion(),null,coberturaUltimoEndoso.getId().getIdToCobertura(),aplico1erRiesgoCobEndAnt); 
    								
    								detalleCobertura.setPorcentajeCoaseguro(coberturaUltimoEndoso.getPorcentajeCoaseguro());
    								detalleCobertura.setMonedaPrima(monedaCotizacion);
    								detalleCobertura.setPorcentajeDeducible(coberturaUltimoEndoso.getPorcentajeDeducible());
    								detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
    								detalleCobertura.setMontoPrimaSuscripcion(0D);
    								
    								listaDetalleCoberturaNoContratadas.add(detalleCobertura);        								            								
								}
							}else{
								detalleCobertura.setMontoPrimaNoDevengada(0D);
							}
						}// fin validacion no aplic� luc ni primer riesgo
					}//fin iteraci�n coberturas del subramo - inciso
					//Obtiene las coberturas no contratadas que est�n contratadas en el �ltimo endoso.
					List<CoberturaCotizacionDTO> listaCoberturasDescontratadas = estructuraCotizacionEndoso.
								obtenerCoberturasDesContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), subRamo.getIdTcSubRamo());
					for(CoberturaCotizacionDTO coberturaDescontratada : listaCoberturasDescontratadas){
						boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaDescontratada);
						boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaDescontratada);
						if(aplico1erRiesgoCobEndAnt){
							listaCoberturaPrimerRiesgoNoContratadas.add(coberturaDescontratada);
						}
						else if(aplicoLUCCobEndAnt){
							if(listasCoberturasNoContratadasLUC.get(coberturaDescontratada.getId().getIdToSeccion())==null){
								listasCoberturasNoContratadasLUC.put(coberturaDescontratada.getId().getIdToSeccion(),new ArrayList<CoberturaCotizacionDTO>() );
							}
							listasCoberturasNoContratadasLUC.get(coberturaDescontratada.getId().getIdToSeccion()).add(coberturaDescontratada);
						}
						else{
							detalleCobertura = new DetalleCobertura(coberturaDescontratada.getId().getNumeroInciso().intValue(),
									coberturaDescontratada.getId().getIdToSeccion(),null,coberturaDescontratada.getId().getIdToCobertura(),false);
							
							detalleCobertura.setPorcentajeCoaseguro(coberturaDescontratada.getPorcentajeCoaseguro());
							detalleCobertura.setMonedaPrima(monedaCotizacion);
							detalleCobertura.setPorcentajeDeducible(coberturaDescontratada.getPorcentajeDeducible());
							detalleCobertura.setMontoPrimaNoDevengada(coberturaDescontratada.getValorPrimaNeta() * factorVigencia);
							detalleCobertura.setMontoPrimaSuscripcion(0D);//cobertura descontratada
							listaDetalleCoberturaNoContratadas.add(detalleCobertura);
						}//fin validacion no es primer riesgo ni luc

					}//fin iteracion coberturas descontratadas del inciso - subramo
					//Une la lista de coberturas de detalle con las coberturas no contratadas.
					if(!listaDetalleCoberturaNoContratadas.isEmpty()){
						listaDetalleCobertura.addAll(listaDetalleCoberturaNoContratadas);
					}
					// Se agreg� esta condici�n para validar que no se envien l�neas sin detalles de coberturas
					if (!listaDetalleCobertura.isEmpty()) {
						Double porcentajePleno = validacionReaseguroPorCatalogo.getPorcentajePleno();
						if(subRamo.getIdTcSubRamo().equals(Sistema.SUBRAMO_INCENDIO)){
							if(!sumasAseguradasIncendio.containsKey(inciso.getId().getNumeroInciso())){
								sumasAseguradasIncendio.put(inciso.getId().getNumeroInciso(), sumaAseguradaLinea);
							}
						}
						if (subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)||
								subRamo.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO)){
							if(sumasAseguradasIncendio.containsKey(inciso.getId().getNumeroInciso())){
								sumaAseguradaLinea=sumasAseguradasIncendio.get(inciso.getId().getNumeroInciso());
							}
						}
						CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamo.getIdTcSubRamo(),inciso.getId().getNumeroInciso().intValue(),null,null,lineaDTO,Sistema.TIPO_CUMULO_INCISO);
						cumuloPoliza.setSumaAsegurada(sumaAseguradaLinea);
    					cumuloPoliza.setPorcentajePleno(porcentajePleno);
    					cumuloPoliza.setAplicaPrimerRiesgo(false);
    					cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
    					cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
    					cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
   						cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
   						listaCumulos.add(cumuloPoliza);
   						
   						Boolean permitirSolicitarFacultativo = validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo();
						if (!permitirSolicitarFacultativo && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
							if (!mostrarMensajeErrorIncisos){
								mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
								mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
							}
							mensajeError.append("<hr>C�mulo por p&oacute;liza, <br/>SubRamo: "+subRamo.getDescripcionSubRamo()+"<br/>Incisos: ");
							for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
								mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
							mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
						}//fin validacion permiteSolicitarFacultativo
					}//fin validacion listaDetallecobertura vac�a
				}//fin iteraci�n incisos
			}// Fin c�mulo por inciso
			else if (lineaDTO != null && lineaDTO.getTipoDistribucion().intValue() == Sistema.TIPO_CUMULO_SUBINCISO) {// si es c�mulo por subinciso
				for (IncisoCotizacionDTO inciso : listaIncisos) {
					//consulta de las coberturas contratadas de la cotizacion-inciso-subramo
					List<CoberturaCotizacionDTO> listaCoberturasTMP = estructuraCotizacionEndoso.
										obtenerCoberturasContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), subRamo.getIdTcSubRamo());
					// Que no apliquen primerRiesgo:
   					List<CoberturaCotizacionDTO> listaCoberturasContratadas = new ArrayList<CoberturaCotizacionDTO>();
   					DetalleCobertura detalleCobertura;
   					List<DetalleCobertura> listaDetalleCobertura = null;
   					List<DetalleCobertura> listaDetalleCoberturaNoContratadas = new ArrayList<DetalleCobertura>();
   					for (CoberturaCotizacionDTO coberturaCot : listaCoberturasTMP) {
   						//Consulta de la cobertura en el endoso anterior
   						CoberturaCotizacionDTO coberturaUltimoEndoso = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().
   												obtenerCoberturaContratadaPorId(coberturaCot.getId().getNumeroInciso(), coberturaCot.getId().getIdToSeccion(), coberturaCot.getId().getIdToCobertura());
   						if(estructuraCotizacionEndoso.aplicoPrimerRiesgo(coberturaCot)){
   							if(!subRamosPrimerRiesgo.containsKey(subRamo.getIdTcSubRamo())){
					    		subRamosPrimerRiesgo.put(subRamo.getIdTcSubRamo(), new ArrayList<CoberturaCotizacionDTO>());
					    	}
					    	subRamosPrimerRiesgo.get(subRamo.getIdTcSubRamo()).add(coberturaCot);
					    	if(coberturaUltimoEndoso!=null){
					    		boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaUltimoEndoso);
					    		if(!aplico1erRiesgoCobEndAnt){
					    			listaCoberturaPrimerRiesgoNoContratadas.add(coberturaUltimoEndoso);
					    		}
					    	}
   						}
   						else if(estructuraCotizacionEndoso.aplicoLUC(coberturaCot)){
							if(coberturaUltimoEndoso != null){
								boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaUltimoEndoso);
								if(!aplicoLUCCobEndAnt){
									if(listasCoberturasNoContratadasLUC.get(coberturaUltimoEndoso.getId().getIdToSeccion())==null){
										listasCoberturasNoContratadasLUC.put(coberturaUltimoEndoso.getId().getIdToSeccion(),new ArrayList<CoberturaCotizacionDTO>() );
									}
									listasCoberturasNoContratadasLUC.get(coberturaUltimoEndoso.getId().getIdToSeccion()).add(coberturaUltimoEndoso);
								}
							}
						}//fin validacion aplicoLUC
						else{
							listaCoberturasContratadas.add(coberturaCot);
						}
   					}//fin ciclo coberturas
   					
   					if(!listaCoberturasContratadas.isEmpty()){
   						//Consulta de los subincisos
   						List<SubIncisoCotizacionDTO> listaSubIncisos = estructuraCotizacionEndoso.obtenerSubIncisos(listaCoberturasContratadas.get(0).getId().getIdToSeccion(), inciso.getId().getNumeroInciso());

    					Double porcentajePleno = 100d;
    					for (SubIncisoCotizacionDTO subInciso : listaSubIncisos) {
    						listaDetalleCobertura = new ArrayList<DetalleCobertura>();
    						ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroFacultativoPorCatalogo = validarReaseguroFacultativoPorCatalogo(
    								estructuraCotizacionEndoso,Sistema.TIPO_CUMULO_SUBINCISO,subRamo,inciso.getId().getNumeroInciso(),subInciso.getId().getIdToSeccion(),subInciso.getId().getNumeroSubInciso());
    						porcentajePleno = validacionReaseguroFacultativoPorCatalogo.getPorcentajePleno();
    						Double sumatoriaSumaAsegurada = subInciso.getValorSumaAsegurada();
    						
    						for (CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas) {
    							DetallePrimaCoberturaCotizacionId detallePrimaId = new DetallePrimaCoberturaCotizacionId(
    									subInciso.getId().getNumeroSubInciso(),idToCotizacion,inciso.getId().getNumeroInciso(),
    									coberturaCot.getId().getIdToSeccion(),coberturaCot.getId().getIdToCobertura());
								DetallePrimaCoberturaCotizacionDTO detalle = null;
								detalle = estructuraCotizacionEndoso.obtenerDetallePrimaCoberturaCotizacionPorId(detallePrimaId);

    							if (detalle != null) {
    								detalleCobertura = new DetalleCobertura(inciso.getId().getNumeroInciso().intValue(),coberturaCot.getId().getIdToSeccion(),
    										subInciso.getId().getNumeroSubInciso().intValue(),coberturaCot.getId().getIdToCobertura(),false);
    								detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
    								detalleCobertura.setMonedaPrima(monedaCotizacion);
    								detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
    								detalleCobertura.setMontoPrimaSuscripcion(detalle.getValorPrimaNeta() * factorVigencia);
    								listaDetalleCobertura.add(detalleCobertura);
    								
    								/*
    								 * Obtenci�n de la Prima Neta del Ultimo Endoso
    								 */
    								CoberturaCotizacionDTO coberturaUltimoEndoso = new CoberturaCotizacionDTO();
    								coberturaUltimoEndoso = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerCoberturaContratadaPorId(
    										coberturaCot.getId().getNumeroInciso(), coberturaCot.getId().getIdToSeccion(), coberturaCot.getId().getIdToCobertura());
    								
    								DetallePrimaCoberturaCotizacionId detallePrimaIdEndosoAnterior = new DetallePrimaCoberturaCotizacionId(
    										subInciso.getId().getNumeroSubInciso(),ultimoEndosoDTO.getIdToCotizacion(),coberturaCot.getId().getNumeroInciso(),
    										coberturaCot.getId().getIdToSeccion(),coberturaCot.getId().getIdToCobertura());
        							DetallePrimaCoberturaCotizacionDTO detalleEndosoAnterior = null;
        							detalleEndosoAnterior = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerDetallePrimaCoberturaCotizacionPorId(detallePrimaIdEndosoAnterior);
        							
    								if(coberturaUltimoEndoso!=null ){
    									boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaUltimoEndoso);
    									boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaUltimoEndoso);
    									//La cobertura lleva prima no devengada s�lo si no hab�a aplicado a Primer Riesgo o LUC en el endoso anterior
        								if(!aplicoLUCCobEndAnt && !aplico1erRiesgoCobEndAnt){
        									if(detalleEndosoAnterior != null){
        										detalleCobertura.setMontoPrimaNoDevengada(detalleEndosoAnterior.getValorPrimaNeta()* factorVigencia);
        									}else
        										detalleCobertura.setMontoPrimaNoDevengada(0d);
        								}else{
        									detalleCobertura.setMontoPrimaNoDevengada(0D); //este detalle es el que se agreg� a la lista listaDetalleCobertura
        									//se crea nuevo detalle para dar de baja la cobertura que hab�a aplicado Primer Riesgo/LUC
        									detalleCobertura = new DetalleCobertura(coberturaUltimoEndoso.getId().getNumeroInciso().intValue(),
        											coberturaUltimoEndoso.getId().getIdToSeccion(), subInciso.getId().getNumeroSubInciso().intValue(),
        											coberturaUltimoEndoso.getId().getIdToCobertura(),aplico1erRiesgoCobEndAnt); 
            								
            								detalleCobertura.setPorcentajeCoaseguro(coberturaUltimoEndoso.getPorcentajeCoaseguro());
            								detalleCobertura.setMonedaPrima(monedaCotizacion);
            								detalleCobertura.setPorcentajeDeducible(coberturaUltimoEndoso.getPorcentajeDeducible());
            								if(detalleEndosoAnterior != null){
            									detalleCobertura.setMontoPrimaNoDevengada(detalleEndosoAnterior.getValorPrimaNeta() * factorVigencia);
            								}else
        										detalleCobertura.setMontoPrimaNoDevengada(0d);
            								detalleCobertura.setMontoPrimaSuscripcion(0D);
            								
            								listaDetalleCoberturaNoContratadas.add(detalleCobertura);        								            								
        								}
        							}else{
        								detalleCobertura.setMontoPrimaNoDevengada(0D);
        							}
    							}//fin validacion detalle != null
    						}// fin iterar CoberturaCot
    						
    						if (!listaDetalleCobertura.isEmpty()) {
								CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamo.getIdTcSubRamo(),subInciso.getId().getNumeroInciso().intValue(),
										subInciso.getId().getNumeroSubInciso().intValue(),subInciso.getId().getIdToSeccion(),lineaDTO,Sistema.TIPO_CUMULO_SUBINCISO);
								cumuloPoliza.setSumaAsegurada(sumatoriaSumaAsegurada);
								cumuloPoliza.setPorcentajePleno(porcentajePleno);
								cumuloPoliza.setAplicaPrimerRiesgo(false);
								cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
								cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
								cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
								cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
								listaCumulos.add(cumuloPoliza);
							}
    						
    					}// fin iterar subincisos
    					
    					//Obtener los subincisos eliminados
    					List<SubIncisoCotizacionDTO> listaSubIncisosEliminados = estructuraCotizacionEndoso.
    							obtenerSubIncisosEliminados(listaCoberturasContratadas.get(0).getId().getIdToSeccion(), inciso.getId().getNumeroInciso());
    					
    					for (SubIncisoCotizacionDTO subInciso : listaSubIncisosEliminados) {
    						
    						listaDetalleCobertura = new ArrayList<DetalleCobertura>();
    						ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroFacultativoPorCatalogo = validarReaseguroFacultativoPorCatalogo(
    								estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior(),Sistema.TIPO_CUMULO_SUBINCISO,subRamo,
    								inciso.getId().getNumeroInciso(),subInciso.getId().getIdToSeccion(),subInciso.getId().getNumeroSubInciso());
    						porcentajePleno = validacionReaseguroFacultativoPorCatalogo.getPorcentajePleno();
    						Double sumatoriaSumaAsegurada = 0D;
    						
    						for (CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas) {
    							DetallePrimaCoberturaCotizacionId detallePrimaId = new DetallePrimaCoberturaCotizacionId(
    									subInciso.getId().getNumeroSubInciso(),subInciso.getId().getIdToCotizacion(),inciso.getId().getNumeroInciso(),
    									coberturaCot.getId().getIdToSeccion(),coberturaCot.getId().getIdToCobertura());
								DetallePrimaCoberturaCotizacionDTO detalle = null;
								detalle = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerDetallePrimaCoberturaCotizacionPorId(detallePrimaId);

    							if (detalle != null) {
    								detalleCobertura = new DetalleCobertura(inciso.getId().getNumeroInciso().intValue(),coberturaCot.getId().getIdToSeccion(),
    										subInciso.getId().getNumeroSubInciso().intValue(),coberturaCot.getId().getIdToCobertura(),false);
    								detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
    								detalleCobertura.setMonedaPrima(monedaCotizacion);
    								detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
    								detalleCobertura.setMontoPrimaSuscripcion(0d);
    								//prima no devengada: 
    								detalleCobertura.setMontoPrimaNoDevengada(detalle.getValorPrimaNeta() * factorVigencia);
    								
    								listaDetalleCobertura.add(detalleCobertura);
    								
    							}
    						}//Fin iteraci�n coberturas contratadas
    						
    						if (!listaDetalleCobertura.isEmpty()) {
								CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamo.getIdTcSubRamo(),subInciso.getId().getNumeroInciso().intValue(),
										subInciso.getId().getNumeroSubInciso().intValue(),subInciso.getId().getIdToSeccion(),lineaDTO,Sistema.TIPO_CUMULO_SUBINCISO);
								cumuloPoliza.setSumaAsegurada(sumatoriaSumaAsegurada);
								cumuloPoliza.setPorcentajePleno(porcentajePleno);
								cumuloPoliza.setAplicaPrimerRiesgo(false);
								cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
								cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
								cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
								cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
								listaCumulos.add(cumuloPoliza);
							}
    						
    					}//fin iterar subincisos eliminados
    					
   					}//fin de if(listaCoberturas.size()>0)
   					
   				//consulta de las coberturas descontratadas de la cotizacion-inciso-subramo
					List<CoberturaCotizacionDTO> listaCoberturasTMPDescontratadasIncisoSubRamo = estructuraCotizacionEndoso.
										obtenerCoberturasDesContratadasPorIncisoSubRamo(inciso.getId().getNumeroInciso(), subRamo.getIdTcSubRamo());
					// Que no apliquen primerRiesgo:
   					List<CoberturaCotizacionDTO> listaCoberturasDescontratadas = new ArrayList<CoberturaCotizacionDTO>();
   					
   					for (CoberturaCotizacionDTO coberturaDescontratada : listaCoberturasTMPDescontratadasIncisoSubRamo) {
   						boolean aplicoLUCCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoLUC(coberturaDescontratada);
						boolean aplico1erRiesgoCobEndAnt = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().aplicoPrimerRiesgo(coberturaDescontratada);
						if(aplico1erRiesgoCobEndAnt){
							listaCoberturaPrimerRiesgoNoContratadas.add(coberturaDescontratada);
						}
						else if(aplicoLUCCobEndAnt){
							if(listasCoberturasNoContratadasLUC.get(coberturaDescontratada.getId().getIdToSeccion())==null){
								listasCoberturasNoContratadasLUC.put(coberturaDescontratada.getId().getIdToSeccion(),new ArrayList<CoberturaCotizacionDTO>() );
							}
							listasCoberturasNoContratadasLUC.get(coberturaDescontratada.getId().getIdToSeccion()).add(coberturaDescontratada);
						}
						else{
							listaCoberturasDescontratadas.add(coberturaDescontratada);
						}//fin validacion no es primer riesgo ni luc
   					}//fin ciclo coberturas
   					
   					// 24/06/2011 Procesa las coberturas no contratadas que est�n contratadas en el �ltimo endoso.
   					if(!listaCoberturasDescontratadas.isEmpty()){
   						//Consulta de los subincisos en el endoso anterior
   						List<SubIncisoCotizacionDTO> listaSubIncisosEndosoAnterior = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().
   									obtenerSubIncisos(listaCoberturasDescontratadas.get(0).getId().getIdToSeccion(), inciso.getId().getNumeroInciso());

    					Double porcentajePleno = 100d;
    					for (SubIncisoCotizacionDTO subIncisoEndosoAnterior : listaSubIncisosEndosoAnterior) {
    						listaDetalleCobertura = new ArrayList<DetalleCobertura>();
    						ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroFacultativoPorCatalogo = validarReaseguroFacultativoPorCatalogo(
    								estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior(),Sistema.TIPO_CUMULO_SUBINCISO,subRamo,inciso.getId().getNumeroInciso(),subIncisoEndosoAnterior.getId().getIdToSeccion(),subIncisoEndosoAnterior.getId().getNumeroSubInciso());
    						porcentajePleno = validacionReaseguroFacultativoPorCatalogo.getPorcentajePleno();
    						Double sumatoriaSumaAsegurada = 0d;
    						
    						for (CoberturaCotizacionDTO coberturaUltimoEndoso : listaCoberturasDescontratadas) {
    							DetallePrimaCoberturaCotizacionId detallePrimaId = new DetallePrimaCoberturaCotizacionId(
    									subIncisoEndosoAnterior.getId().getNumeroSubInciso(),
    									estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().getIdToCotizacion(),inciso.getId().getNumeroInciso(),
    									coberturaUltimoEndoso.getId().getIdToSeccion(),coberturaUltimoEndoso.getId().getIdToCobertura());
								DetallePrimaCoberturaCotizacionDTO detalle = null;
								detalle = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerDetallePrimaCoberturaCotizacionPorId(detallePrimaId);

    							if (detalle != null) {
    								
    								detalleCobertura = new DetalleCobertura(inciso.getId().getNumeroInciso().intValue(),coberturaUltimoEndoso.getId().getIdToSeccion(),
    										subIncisoEndosoAnterior.getId().getNumeroSubInciso().intValue(),coberturaUltimoEndoso.getId().getIdToCobertura(),false);
    								
    								//cobertura descontratada
    								detalleCobertura.setMontoPrimaSuscripcion(0d);
    								
    								detalleCobertura.setPorcentajeCoaseguro(coberturaUltimoEndoso.getPorcentajeCoaseguro());
    								detalleCobertura.setMonedaPrima(Integer.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
    								detalleCobertura.setPorcentajeDeducible(coberturaUltimoEndoso.getPorcentajeDeducible());
    								detalleCobertura.setMontoPrimaNoDevengada(detalle.getValorPrimaNeta() * factorVigencia);
    								listaDetalleCobertura.add(detalleCobertura);
    								
    								
    							}//fin validacion detalle != null
    						}// fin iterar CoberturaCot
    						
    						if (!listaDetalleCobertura.isEmpty()) {
								CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamo.getIdTcSubRamo(),subIncisoEndosoAnterior.getId().getNumeroInciso().intValue(),
										subIncisoEndosoAnterior.getId().getNumeroSubInciso().intValue(),subIncisoEndosoAnterior.getId().getIdToSeccion(),lineaDTO,Sistema.TIPO_CUMULO_SUBINCISO);
								cumuloPoliza.setSumaAsegurada(sumatoriaSumaAsegurada);
								cumuloPoliza.setPorcentajePleno(porcentajePleno);
								cumuloPoliza.setAplicaPrimerRiesgo(false);
								cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
								cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
								cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
								cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
								listaCumulos.add(cumuloPoliza);
							}
    						
    					}// fin iterar subincisos
   					}//fin de !listaCoberturasDescontratadas.isEmpty()
   					
   					/*TODO
					 * Une la lista de coberturas de detalle con las coberturas no contratadas.
					 */
//					if(!listaDetalleCoberturaNoContratadas.isEmpty()){
//						listaDetalleCobertura.addAll(listaDetalleCoberturaNoContratadas);
//					}
//					if (!listaDetalleCobertura.isEmpty()) {
//						CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamo.getIdTcSubRamo(),subInciso.getId().getNumeroInciso().intValue(),
//								subInciso.getId().getNumeroSubInciso().intValue(),subIncisoEndosoAnterior.getId().getIdToSeccion(),lineaDTO,Sistema.TIPO_CUMULO_SUBINCISO);
//						cumuloPoliza.setSumaAsegurada(sumatoriaSumaAsegurada);
//						cumuloPoliza.setPorcentajePleno(porcentajePleno);
//						cumuloPoliza.setAplicaPrimerRiesgo(false);
//						cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
//						cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
//						cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
//						cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
//						listaCumulos.add(cumuloPoliza);
//					}
   					
				}// fin ciclo incisos
			}// fin c�mulo por subinciso
			else{//El objetoLineaDTO obtenido por el soportereaseguro es nulo, no es un subramo v�lido
				subRamosNoValidos.append(subRamo.getDescripcionSubRamo()).append("<br/>");
			    mostrarMensajeErrorSubRamosNoValidos = true;
			}
		}// fin ciclo subRamo
		List<AgrupacionCotDTO> listaAgrupacionCotizacion = null;
		listaAgrupacionCotizacion = estructuraCotizacionEndoso.obtenerListaAgrupacionCotizacion();
		
		if(listaAgrupacionCotizacion!=null && !listaAgrupacionCotizacion.isEmpty()){
			DetalleCobertura detalleCobertura;
			List<DetalleCobertura> listaDetalleCobertura;
			double porcentajePleno = 100;
			SubRamoDTO subRamoInciendio = SubRamoDN.getInstancia().getSubRamoPorId(Sistema.SUBRAMO_INCENDIO);
			for(AgrupacionCotDTO agrupacionCotizacion:listaAgrupacionCotizacion){
				if (agrupacionCotizacion.getClaveTipoAgrupacion()==Sistema.NUM_AGRUPACION_PRIMER_RIESGO) {
					Iterator<Entry<BigDecimal, List<CoberturaCotizacionDTO>>> iteradorSubRamos1erRiesgo = subRamosPrimerRiesgo.entrySet().iterator();
					while (iteradorSubRamos1erRiesgo.hasNext()){
						Entry<BigDecimal, List<CoberturaCotizacionDTO>> subRamoLista = (Entry<BigDecimal, List<CoberturaCotizacionDTO>>)iteradorSubRamos1erRiesgo.next();
						List<CoberturaCotizacionDTO> listaCoberturasEnCurso = subRamoLista.getValue();
						ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo =
							validarReaseguroFacultativoPorCatalogo(estructuraCotizacionEndoso,Sistema.TIPO_CUMULO_POLIZA,
									listaCoberturasEnCurso.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO(),null,null,null);
						listaDetalleCobertura = new ArrayList<DetalleCobertura>();
						int j=0;
						int numeroIncisoCumulo = 0;
						for (CoberturaCotizacionDTO coberturaCot : listaCoberturasEnCurso) {
							detalleCobertura = new DetalleCobertura(coberturaCot.getId().getNumeroInciso().intValue(),coberturaCot.getId().getIdToSeccion(),
									0,coberturaCot.getId().getIdToCobertura(),true);
               				detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
                			detalleCobertura.setMonedaPrima(monedaCotizacion);
               				detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
               				//Obtenci�n de la Prima Neta del Ultimo Endoso
               				CoberturaCotizacionDTO coberturaUltimoEndoso = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerCoberturaContratadaPorId(
               						coberturaCot.getId().getNumeroInciso(), coberturaCot.getId().getIdToSeccion(), coberturaCot.getId().getIdToCobertura());
               				//S�lo hay prima no devengada si la cobertura estaba contratada y ten�a Primer Riesgo en el endoso anterior
            				if(coberturaUltimoEndoso!=null && coberturaUltimoEndoso.getNumeroAgrupacion()!=0){
            					                                					                	               			
               				    detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
               				}else{
               				    detalleCobertura.setMontoPrimaNoDevengada(0D);
               				}
               				
               				detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta() * factorVigencia);
               				listaDetalleCobertura.add(detalleCobertura);
               				try {
            					// Obtener el subgiro del inciso, se utilizar� el ramo Incendio (1)
               					SubGiroDTO subGiroDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubgiro(idToCotizacion,coberturaCot.getId().getNumeroInciso(),BigDecimal.ONE);
               					if (subGiroDTO != null) {
               						PlenoReaseguroDTO plenoReaseguro = cotizacionDN.obtenerPlenoReaseguro(subGiroDTO);
               						if (plenoReaseguro.getPorcentajeMaximo() < porcentajePleno)
               							porcentajePleno = plenoReaseguro.getPorcentajeMaximo();
               					}
               				} catch (SystemException e) {
               					e.printStackTrace();
               				}
               				if(j==0){
               					numeroIncisoCumulo = coberturaCot.getId().getNumeroInciso().intValue();
               					j = j+1;
               				}
               				else{
               					if(numeroIncisoCumulo != coberturaCot.getId().getNumeroInciso().intValue())
               						numeroIncisoCumulo = 0;
               					j = j+1;
               				}
						}//fin iteraci�n coberturas subramo en curso
					/* Coberturas no contratadas que aplicaron a primer riesgo */
						for (CoberturaCotizacionDTO coberturaCot : listaCoberturaPrimerRiesgoNoContratadas) {
							detalleCobertura = new DetalleCobertura(coberturaCot.getId().getNumeroInciso().intValue(),coberturaCot.getId().getIdToSeccion(),
									0,coberturaCot.getId().getIdToCobertura(),true);
	        				detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
	        				detalleCobertura.setMontoPrimaSuscripcion(0D);
	        				detalleCobertura.setMonedaPrima(monedaCotizacion);
	        				detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
	        				//Obtenci�n de la Prima Neta del Ultimo Endoso
	        				CoberturaCotizacionDTO coberturaUltimoEndoso = null;
	        				coberturaUltimoEndoso = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerCoberturaContratadaPorId(
	        						coberturaCot.getId().getNumeroInciso(), coberturaCot.getId().getIdToSeccion(), coberturaCot.getId().getIdToCobertura());
	        				if(coberturaUltimoEndoso!=null){
	        				    detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
	        				}else{
	        				    detalleCobertura.setMontoPrimaNoDevengada(0D);
	        				}
	        				listaDetalleCobertura.add(detalleCobertura);
	        				if(numeroIncisoCumulo != 0 && (numeroIncisoCumulo != coberturaCot.getId().getNumeroInciso().intValue()))
	        					numeroIncisoCumulo = 0;
						}
					// Obtener el inciso al que pertenece la cobertura
						LineaDTO lineaTMP = null;
						try{
							lineaTMP = soporteReaseguroCotizacionEndoso.obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), subRamoLista.getKey());
						}catch(SystemException e){}
						if(lineaTMP != null){
							CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,subRamoLista.getKey(),numeroIncisoCumulo,null,
									null,lineaTMP,Sistema.TIPO_CUMULO_INCISO);
							cumuloPoliza.setSumaAsegurada(agrupacionCotizacion.getValorSumaAsegurada());
		        			cumuloPoliza.setAplicaPrimerRiesgo(true);
		        			cumuloPoliza.setDetalleCoberturas(listaDetalleCobertura);
		        			cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
		        			cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
		        			cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
		        			cumuloPoliza.setPorcentajePleno(validacionReaseguroPorCatalogo.getPorcentajePleno());
		        			listaCumulos.add(cumuloPoliza);			        			
		        			if (!validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo() && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
		        				if (!mostrarMensajeErrorIncisos){
		        					mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
		        					mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
		        				}
		        				mensajeError.append("<hr>C�mulo por primer riesgo, <br/>SubRamo: "+subRamoInciendio.getDescripcionSubRamo()+"<br/>Incisos: ");
		        				for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
		        					mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
		        				mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
		        			}
						}
						else{
							subRamosNoValidos.append(subRamoLista.getKey()).append("<br/>");
						    mostrarMensajeErrorSubRamosNoValidos = true;
						}
					}//fin iteracion subramos primer riesgo
				}//fin validaci�n agrupaci�n primer riesgo
				else{
					//Consulta de las coberturas contratadas de la agrupaci�n en curso.
					List<CoberturaCotizacionDTO> listaCoberturasLUC = estructuraCotizacionEndoso.
								obtenerCoberturasContratadasLUCPorSeccion(agrupacionCotizacion.getIdToSeccion());
					List<DetalleCobertura> listaDetalleCoberturaLuc = new ArrayList<DetalleCobertura>();
    		    	BigDecimal idTcSubRamoCumulo = BigDecimal.ZERO;
    		    	ValidacionReaseguroFacultativoPorCatalogo validacionReaseguroPorCatalogo=null;
    		    	int numeroIncisoCumulo = 0;
    		    	if (listaCoberturasLUC != null && !listaCoberturasLUC.isEmpty()){
    		    		idTcSubRamoCumulo = listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo();
    		    		validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(estructuraCotizacionEndoso,Sistema.TIPO_CUMULO_POLIZA, 
    							listaCoberturasLUC.get(0).getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO(), null, null, null);
    		    		int j=0;
    		    		for (CoberturaCotizacionDTO coberturaCot : listaCoberturasLUC) {
    		    			detalleCobertura = new DetalleCobertura(coberturaCot.getId().getNumeroInciso().intValue(),coberturaCot.getId().getIdToSeccion(),0,
    		    					coberturaCot.getId().getIdToCobertura(),false);
            				
            				detalleCobertura.setPorcentajeCoaseguro(coberturaCot.getPorcentajeCoaseguro());
            				detalleCobertura.setMonedaPrima(Integer.valueOf(cotizacionDTO.getIdMoneda().toBigInteger().toString()));
            				detalleCobertura.setPorcentajeDeducible(coberturaCot.getPorcentajeDeducible());
            				
            				CoberturaCotizacionDTO coberturaUltimoEndoso = estructuraCotizacionEndoso.getEstructuraCotizacionEndosoAnterior().obtenerCoberturaContratadaPorId(
            						coberturaCot.getId().getNumeroInciso(), coberturaCot.getId().getIdToSeccion(), coberturaCot.getId().getIdToCobertura());
            				
            				if(coberturaUltimoEndoso!=null && coberturaUltimoEndoso.getNumeroAgrupacion()!=0){
            					//S�lo hay prima no devengada si la cobertura estaba contratada y ten�a LUC en el endoso anterior 
            				    detalleCobertura.setMontoPrimaNoDevengada(coberturaUltimoEndoso.getValorPrimaNeta() * factorVigencia);
            				}else{
            				    detalleCobertura.setMontoPrimaNoDevengada(0D);
            				}
            				detalleCobertura.setMontoPrimaSuscripcion(coberturaCot.getValorPrimaNeta() * factorVigencia);
            				
            				listaDetalleCoberturaLuc.add(detalleCobertura);
            				if(j == 0){
            					numeroIncisoCumulo = coberturaCot.getId().getNumeroInciso().intValue();
            				}
            				else if(numeroIncisoCumulo != coberturaCot.getId().getNumeroInciso().intValue()){
            					numeroIncisoCumulo =0;
            				}
            				j = j+1;
    		    		}
    		    	}
    		    	if(listasCoberturasNoContratadasLUC.containsKey(agrupacionCotizacion.getIdToSeccion())){
    		    		for(CoberturaCotizacionDTO coberturaLUC:listasCoberturasNoContratadasLUC.get(agrupacionCotizacion.getIdToSeccion())){
    		    			if(idTcSubRamoCumulo.equals(BigDecimal.ZERO)){
    		    				idTcSubRamoCumulo = coberturaLUC.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo();
    		    			}
    		    			if(validacionReaseguroPorCatalogo == null){
    		    				validacionReaseguroPorCatalogo = validarReaseguroFacultativoPorCatalogo(estructuraCotizacionEndoso,Sistema.TIPO_CUMULO_POLIZA, 
    		    						coberturaLUC.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO(), null, null, null);
    		    			}
    		    			detalleCobertura = new DetalleCobertura(coberturaLUC.getId().getNumeroInciso().intValue(),coberturaLUC.getId().getIdToSeccion(),
    		    					0,coberturaLUC.getId().getIdToCobertura(),false);
    		    			detalleCobertura.setPorcentajeCoaseguro(coberturaLUC.getPorcentajeCoaseguro());
    		    			detalleCobertura.setMonedaPrima(monedaCotizacion);
    		    			detalleCobertura.setPorcentajeDeducible(coberturaLUC.getPorcentajeDeducible());
    				// primaNoDevengada = primaUltimoEndoso * factorVigencia
    		    			detalleCobertura.setMontoPrimaNoDevengada(coberturaLUC.getValorPrimaNeta() * factorVigencia);
    		    			detalleCobertura.setMontoPrimaSuscripcion(0D);
    		    			listaDetalleCoberturaLuc.add(detalleCobertura);
    		    			if(numeroIncisoCumulo != 0 && (numeroIncisoCumulo != coberturaLUC.getId().getNumeroInciso().intValue())){
    		    				numeroIncisoCumulo = 0;
    		    			}
    		    		}
    		    	}
    		    	CumuloPoliza cumuloPoliza = new CumuloPoliza(idToCotizacion,idTcSubRamoCumulo,numeroIncisoCumulo,null,null,null,null);
    		    	cumuloPoliza.setSumaAsegurada(agrupacionCotizacion.getValorSumaAsegurada());
    		    	LineaDTO lineaTMP = null;
    		    	try {
    		    		lineaTMP = soporteReaseguroCotizacionEndoso.obtenerLineaPorSubRamo(cotizacionDTO.getFechaInicioVigencia(), idTcSubRamoCumulo);
    		    	} catch (SystemException e1) {}
    		    	if(lineaTMP != null){
        		    	cumuloPoliza.setAplicaPrimerRiesgo(false);
        		    	cumuloPoliza.setDetalleCoberturas(listaDetalleCoberturaLuc);
        		    	cumuloPoliza.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
        		    	cumuloPoliza.setIdMoneda(cotizacionDTO.getIdMoneda());
        		    	cumuloPoliza.setIdToPersonaAsegurado(cotizacionDTO.getIdToPersonaAsegurado());
        		    	cumuloPoliza.setPorcentajePleno(validacionReaseguroPorCatalogo.getPorcentajePleno());
        		    	cumuloPoliza.setLineaDTO(lineaTMP);
        		    	cumuloPoliza.setTipoDistribucion(lineaTMP.getTipoDistribucion().intValue());
        		    	listaCumulos.add(cumuloPoliza);
    		    	}
    		    	else{
						subRamosNoValidos.append(idTcSubRamoCumulo).append("<br/>");
					    mostrarMensajeErrorSubRamosNoValidos = true;
					}
    		    	if (!validacionReaseguroPorCatalogo.getPermitirSolicitarFacultativo() && !validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().isEmpty()){
    		    		if (!mostrarMensajeErrorIncisos){
    		    			mensajeError.append("Los siguientes incisos requieren facultativo, por lo que el c�mulo completo requerir� Soporte Facultativo<br/>");
    		    			mostrarMensajeErrorIncisos = !mostrarMensajeErrorIncisos;
    		    		}
    		    		mensajeError.append("<hr>C�mulo por primer riesgo, <br/>SubRamo: "+subRamoInciendio.getDescripcionSubRamo()+"<br/>Incisos: ");
    		    		for(int i=0;i < validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().size();i++)
    		    			mensajeError.append(" "+validacionReaseguroPorCatalogo.getIncisosRequierenFacultativo().get(i).getId().getNumeroInciso()+", ");
    		    		mensajeError = new StringBuilder(mensajeError.substring(0, mensajeError.length()-2));
    		    	}
				}
			}
		}
		try {
			soporteReaseguroCotizacionEndoso.insertarCumulosLinea(listaCumulos, nombreUsuario);
		} catch (ExcepcionDeLogicaNegocio e) {
		}
		if(mapaListaCumulos != null){
			List<LineaSoporteReaseguroDTO> listaCumuloPoliza = soporteReaseguroCotizacionEndoso.getListaLineasSoporte(BigDecimal.ONE);
			List<LineaSoporteReaseguroDTO> listaCumuloInciso = soporteReaseguroCotizacionEndoso.getListaLineasSoporte(new BigDecimal(2));
			List<LineaSoporteReaseguroDTO> listaCumuloSubInciso = soporteReaseguroCotizacionEndoso.getListaLineasSoporte(new BigDecimal(3));
			mapaListaCumulos.put("listaCumuloPoliza", listaCumuloPoliza);
			mapaListaCumulos.put("listaCumuloInciso",listaCumuloInciso);
			mapaListaCumulos.put("listaCumuloSubInciso",listaCumuloSubInciso);
		}
//			request.getSession().setAttribute("listaCumuloPoliza",listaCumuloPoliza);
//			request.getSession().setAttribute("listaCumuloInciso",listaCumuloInciso);
//			request.getSession().setAttribute("listaCumuloSubInciso",listaCumuloSubInciso);
		String mensajesErrorCumulo = "";
		if (mostrarMensajeErrorSubRamosNoValidos){
			mensajesErrorCumulo += "Los siguientes SubRamos no presentaron tipo de cumulo v�lido : <br/>"+subRamosNoValidos.toString();
			mensajesErrorCumulo += "<hr>";
			mensajesErrorCumulo += mensajeError.toString().length()>0?mensajeError.toString():"";
		}
		
		if(cotizacionForm != null){
			cotizacionForm.setMensajeErrorCumulo(mensajesErrorCumulo.length()>0?mensajesErrorCumulo:Sistema.SIN_MENSAJE_PARA_MOSTRAR);
		}
	}
	
        /**
         * Method recalcularCoberturasFacultativo
         * @param coberturas
         * @param nombreUsuario
         * @throws SystemException
         */
	public void recalcularCoberturasFacultativo(List<CoberturaCotizacionDTO> coberturas, 
		String nombreUsuario)throws SystemException {
    		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
    		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
    		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
    		for (CoberturaCotizacionDTO cobertura : coberturas) {
				List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN.listarRecargosRiesgoPorCoberturaCotizacion(cobertura,	Sistema.RECARGO_REASEGURO_FACULTATIVO, Boolean.TRUE);
				for (RecargoRiesgoCotizacionDTO recargo : recargos) {
					recargo.setValorRecargo(0D);
					recargo.setClaveContrato((short) 0);
					recargoRiesgoCotizacionSN.modificar(recargo);
				}
				List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
				.listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_REASEGURO_FACULTATIVO,
						Boolean.TRUE);
				for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
					descuento.setValorDescuento(0D);
					descuento.setClaveContrato((short) 0);
					descuentoRiesgoCotizacionSN.modificar(descuento);
				}
				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN
				.getInstancia();
				calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
    		}
    	}
	
	/**
	 * Method validarReaseguroFacultativoPorCatalogo().
	 * @param estructuraCotizacion
	 * @param tipoCumulo
	 * @param subRamoDTO
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param numeroSubInciso
	 * @return ValidacionReaseguroFacultativoPorCatalogo
	 * @throws SystemException
	 */
	private ValidacionReaseguroFacultativoPorCatalogo validarReaseguroFacultativoPorCatalogo(SoporteEstructuraCotizacion estructuraCotizacion, int tipoCumulo, SubRamoDTO subRamoDTO, 
		BigDecimal numeroInciso, BigDecimal idToSeccion,BigDecimal numeroSubInciso) throws SystemException {
		boolean requiereFacultativo = false;
		boolean permitirSolicitarFacultativo = true;
		double porcentajePleno = 100;
		short AREA_REASEGURO = (short) 2;
		short CLAVE_INSPECCION_EXCLUIDO = (short) 9;
		BigDecimal idToCotizacion = estructuraCotizacion.getIdToCotizacion();
		BigDecimal INCENDIO = Sistema.SUBRAMO_INCENDIO;
		BigDecimal OBRA_CIVIL_EN_CONSTRUCCION = Sistema.SUBRAMO_OBRA_CIVIL_EN_CONSTRUCCION;
		BigDecimal EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA = Sistema.SUBRAMO_EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA;
		BigDecimal EQUIPO_ELECTRONICO = Sistema.SUBRAMO_EQUIPO_ELECTRONICO;
		BigDecimal MONTAJE_MAQUINA = Sistema.SUBRAMO_MONTAJE_MAQUINA;
		BigDecimal ROTURA_MAQUINARIA = Sistema.SUBRAMO_ROTURA_MAQUINARIA;
		BigDecimal RESPONSABILIDAD_CIVIL_GENERAL = Sistema.SUBRAMO_RESPONSABILIDAD_CIVIL_GENERAL;
		List<IncisoCotizacionDTO> incisosRequierenFacultativo = new ArrayList<IncisoCotizacionDTO>();
		CotizacionDN cotizacionDN= CotizacionDN.getInstancia("");
		DatoIncisoCotizacionDN datoIncisoCotDN = DatoIncisoCotizacionDN.getINSTANCIA();
		// Si el cumulo es por poliza
		if (tipoCumulo == Sistema.TIPO_CUMULO_POLIZA) {
		
			// Obtener incisos en donde las coberturas esten contratadas
			List<IncisoCotizacionDTO> incisos = null;
	//			incisos =  IncisoCotizacionDN.getInstancia().listarIncisosPorCoberturaContratada(idToCotizacion,coberturas, (short) 1);
			incisos =  estructuraCotizacion.obtenerIncisosPorSubRamo(subRamoDTO.getIdTcSubRamo());
			if (incisos == null)	incisos = new ArrayList<IncisoCotizacionDTO>();
			
			for (IncisoCotizacionDTO inciso : incisos) {
				// Si el subramo es incendio
				if (subRamoDTO.getIdTcSubRamo().compareTo(INCENDIO) == 0) {
					SubGiroDTO subGiroDTO;
					try {
						subGiroDTO = estructuraCotizacion.getAdministradorDatosInciso().obtenerSubGiroDTO(
								inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
//							datoIncisoCotDN.obtenerSubgiro(idToCotizacion, inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
						if (subGiroDTO != null) {
							PlenoReaseguroDTO plenoReaseguro = estructuraCotizacion.getAdministradorDatosInciso().obtenerPlenoReaseguro(
									inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
								cotizacionDN.obtenerPlenoReaseguro(subGiroDTO);
							Double porcentajePlenoTemp = plenoReaseguro.getPorcentajeMaximo();
							if (porcentajePlenoTemp < porcentajePleno)
								porcentajePleno = porcentajePlenoTemp;
	
							if (subGiroDTO.getClaveInspeccion() == CLAVE_INSPECCION_EXCLUIDO) {
								incisosRequierenFacultativo.add(inciso);
								porcentajePleno = 0d;
								requiereFacultativo = true;
							}
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo incendio
				// si el subramo corresponde a RESPONSABILIDAD_CIVIL_GENERAL
				else if (subRamoDTO.getIdTcSubRamo().compareTo(RESPONSABILIDAD_CIVIL_GENERAL) == 0) {
					SubGiroRCDTO subGiroRC;
					try {
						subGiroRC = estructuraCotizacion.getAdministradorDatosInciso().obtenerSubGiroRCDTO(
								inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
//							datoIncisoCotDN.obtenerSubGiroRC(idToCotizacion, inciso.getId().getNumeroInciso(), subRamoDTO.getRamoDTO().getIdTcRamo());
						if (subGiroRC.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo Responsabilidad Civil General.
				// si el subramo corresponde a ROTURA_MAQUINARIA
				else if (subRamoDTO.getIdTcSubRamo().compareTo(ROTURA_MAQUINARIA) == 0) {
					SubTipoMaquinariaDTO subTipoMaquinariaDTO;
					try {
						//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
						if (idToSeccion == null && numeroSubInciso == null)
							subTipoMaquinariaDTO = datoIncisoCotDN.obtenerSubTipoMaquinaria(idToCotizacion,inciso.getId().getNumeroInciso(),subRamoDTO.getRamoDTO().getIdTcRamo(),
									estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoMaquinaria());
						else
							subTipoMaquinariaDTO = datoIncisoCotDN.obtenerSubTipoMaquinaria(idToCotizacion,inciso.getId().getNumeroInciso(),subRamoDTO.getRamoDTO().getIdTcRamo(),
									numeroSubInciso,idToSeccion,estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoMaquinaria());
						if (subTipoMaquinariaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo Rotura Maquinaria.
				// si el subramo corresponde a MONTAJE_MAQUINA
				else if (subRamoDTO.getIdTcSubRamo().compareTo(MONTAJE_MAQUINA) == 0) {
					SubtipoMontajeMaquinaDTO subTipoMontajeMaquinaDTO;
					try {
						subTipoMontajeMaquinaDTO = datoIncisoCotDN.obtenerSubTipoMontajeMaquina(idToCotizacion,inciso.getId().getNumeroInciso(),
								subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoMontajeMaquina());
						if (subTipoMontajeMaquinaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subramo Montaje Maquina.
				// si el subramo corresponde a EQUIPO_ELECTRONICO
				else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_ELECTRONICO) == 0) {
					SubtipoEquipoElectronicoDTO subTipoEquipoElectronicoDTO;
					try {
						//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
						if (idToSeccion == null && numeroSubInciso == null)
							subTipoEquipoElectronicoDTO = datoIncisoCotDN.obtenerSubtipoEquipoElectronico(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoElectronico());
						else
							subTipoEquipoElectronicoDTO = datoIncisoCotDN.obtenerSubtipoEquipoElectronico(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo(),numeroSubInciso,idToSeccion,estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoElectronico());
						if (subTipoEquipoElectronicoDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subtipo equipo electronico.
				// si el subramo corresponde a
				// EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA
				else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0) {
					SubtipoEquipoContratistaDTO subTipoEquipoContratistaDTO;
					try {
						//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
						if (idToSeccion == null && numeroSubInciso == null)
							subTipoEquipoContratistaDTO = datoIncisoCotDN.obtenerSubtipoEquipoContratista(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoContratista());
						else
							subTipoEquipoContratistaDTO = datoIncisoCotDN.obtenerSubtipoEquipoContratista(idToCotizacion, inciso.getId().getNumeroInciso(),
									subRamoDTO.getRamoDTO().getIdTcRamo(),numeroSubInciso,idToSeccion,estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoContratista());
						if (subTipoEquipoContratistaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}// Fin subtipo equipo contratista.
				// si el subramo corresponde a OBRA_CIVIL_EN_CONSTRUCCION
				else if (subRamoDTO.getIdTcSubRamo().compareTo(OBRA_CIVIL_EN_CONSTRUCCION) == 0) {
					TipoObraCivilDTO tipoObraCivilDTO;
					try {
						tipoObraCivilDTO = datoIncisoCotDN.obtenerTipoObraCivil(idToCotizacion, inciso.getId().getNumeroInciso(),
								subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoObraCivil());
						if (tipoObraCivilDTO.getClaveAutorizacion() == AREA_REASEGURO) {
							incisosRequierenFacultativo.add(inciso);
							requiereFacultativo = true;
							porcentajePleno = 0d;
						}
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}else if(subRamoDTO.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO) || subRamoDTO.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)){
				    	Double porcentajePlenoTemp = getPorcentajePlenoIncendio(estructuraCotizacion, inciso.getId().getNumeroInciso());
					if (porcentajePlenoTemp < porcentajePleno){
					    porcentajePleno = porcentajePlenoTemp;
					}
				}
			}
			// Si s�lo algunos incisos requieren facultativo por cat�logo, no se permitir� solicitar facultativo
			if (requiereFacultativo == true && incisosRequierenFacultativo.size() != incisos.size()) {
				permitirSolicitarFacultativo = false;
			}
		}// fin tipo cumulo por poliza
		// si el tipoCumulo es por inciso o por subinciso
		else if (tipoCumulo ==Sistema.TIPO_CUMULO_INCISO || tipoCumulo ==Sistema.TIPO_CUMULO_SUBINCISO) {
			// Si el subramo es incendio
			if (subRamoDTO.getIdTcSubRamo().compareTo(INCENDIO) == 0) {
				SubGiroDTO subGiroDTO;
				try {
					subGiroDTO = estructuraCotizacion.getAdministradorDatosInciso().obtenerSubGiroDTO(
							numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
//					subGiroDTO = datoIncisoCotDN.obtenerSubgiro(idToCotizacion, numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo());
					if (subGiroDTO != null) {
						PlenoReaseguroDTO plenoReaseguro = //cotizacionDN.obtenerPlenoReaseguro(subGiroDTO);
							estructuraCotizacion.getAdministradorDatosInciso().obtenerPlenoReaseguro(
								numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
						if(plenoReaseguro!=null)
							porcentajePleno = plenoReaseguro.getPorcentajeMaximo();
						if (subGiroDTO.getClaveInspeccion() == CLAVE_INSPECCION_EXCLUIDO) {
							porcentajePleno = 0d;
							requiereFacultativo = true;
						}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
			// si el subramo corresponde a RESPONSABILIDAD_CIVIL_GENERAL
			else if (subRamoDTO.getIdTcSubRamo().compareTo(RESPONSABILIDAD_CIVIL_GENERAL) == 0) {
				SubGiroRCDTO subGiroRC;
				try {
					subGiroRC = datoIncisoCotDN.obtenerSubGiroRC(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
					if (subGiroRC.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subramo Responsabilidad Civil General.
			// si el subramo corresponde a ROTURA_MAQUINARIA
			else if (subRamoDTO.getIdTcSubRamo().compareTo(ROTURA_MAQUINARIA) == 0) {
				SubTipoMaquinariaDTO subTipoMaquinariaDTO;
				try {
					//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
					if (idToSeccion == null && numeroSubInciso == null)
						subTipoMaquinariaDTO = datoIncisoCotDN.obtenerSubTipoMaquinaria(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo(),
								estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoMaquinaria());
					else
						subTipoMaquinariaDTO = datoIncisoCotDN.obtenerSubTipoMaquinaria(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo(),
								numeroSubInciso,idToSeccion,estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoMaquinaria());
					if (subTipoMaquinariaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subramo Rotura Maquinaria.
			// si el subramo corresponde a MONTAJE_MAQUINA
			else if (subRamoDTO.getIdTcSubRamo().compareTo(MONTAJE_MAQUINA) == 0) {
				SubtipoMontajeMaquinaDTO subTipoMontajeMaquinaDTO;
				try {
					subTipoMontajeMaquinaDTO = datoIncisoCotDN.obtenerSubTipoMontajeMaquina(idToCotizacion,numeroInciso,
							subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoMontajeMaquina());
					if (subTipoMontajeMaquinaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subramo Montaje Maquina.
			// si el subramo corresponde a EQUIPO_ELECTRONICO
			else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_ELECTRONICO) == 0) {
				SubtipoEquipoElectronicoDTO subTipoEquipoElectronicoDTO;
				try {
					//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
					if (idToSeccion == null && numeroSubInciso == null)
						subTipoEquipoElectronicoDTO = datoIncisoCotDN.obtenerSubtipoEquipoElectronico(idToCotizacion,numeroInciso,
								subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoElectronico());
					else
						subTipoEquipoElectronicoDTO = datoIncisoCotDN.obtenerSubtipoEquipoElectronico(idToCotizacion,numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo(),
								numeroSubInciso,idToSeccion,estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoElectronico());
					if (subTipoEquipoElectronicoDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subtipo equipo electronico.
			// si el subramo corresponde a
			// EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA
			else if (subRamoDTO.getIdTcSubRamo().compareTo(EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0) {
				SubtipoEquipoContratistaDTO subTipoEquipoContratistaDTO;
				try {
					//En teoria, esta condici�n no se cumplir� debido a que este caso es a nivel subInciso, en el cual se reciben los datos de idToSeccion y numeroSubInciso
					if (idToSeccion == null && numeroSubInciso == null)
						subTipoEquipoContratistaDTO = datoIncisoCotDN.obtenerSubtipoEquipoContratista(idToCotizacion, numeroInciso,
								subRamoDTO.getRamoDTO().getIdTcRamo(),estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoContratista());
					else
						subTipoEquipoContratistaDTO = datoIncisoCotDN.obtenerSubtipoEquipoContratista(idToCotizacion, numeroInciso,
								subRamoDTO.getRamoDTO().getIdTcRamo(),numeroSubInciso,idToSeccion,estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoEquipoContratista());
					if (subTipoEquipoContratistaDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin subtipo equipo contratista.
			// si el subramo corresponde a OBRA_CIVIL_EN_CONSTRUCCION
			else if (subRamoDTO.getIdTcSubRamo().compareTo(OBRA_CIVIL_EN_CONSTRUCCION) == 0) {
				TipoObraCivilDTO tipoObraCivilDTO;
				try {
					tipoObraCivilDTO = datoIncisoCotDN.obtenerTipoObraCivil(idToCotizacion, numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo(),
							estructuraCotizacion.getAdministradorDatosInciso().getMapaSubTipoObraCivil());
					if (tipoObraCivilDTO!= null && tipoObraCivilDTO.getClaveAutorizacion() == AREA_REASEGURO) {
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}// Fin tipo obra civil.
			else if(subRamoDTO.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_TERREMOTO) || subRamoDTO.getIdTcSubRamo().equals(Sistema.IDTCSUBRAMO_HIDRO)){
			    porcentajePleno= getPorcentajePlenoIncendio(estructuraCotizacion, numeroInciso);
			}
		}// fin tipocumulo por inciso
	
		ValidacionReaseguroFacultativoPorCatalogo validacionPorCatalogo = new ValidacionReaseguroFacultativoPorCatalogo();
		validacionPorCatalogo.setTipoDistribucion(new Integer(tipoCumulo));
		validacionPorCatalogo.setIdTcSubRamo(subRamoDTO.getIdTcSubRamo());
		validacionPorCatalogo.setIdToCotizacion(idToCotizacion);
		validacionPorCatalogo.setNumeroInciso(numeroInciso);
		validacionPorCatalogo.setIdToSeccion(idToSeccion);
		validacionPorCatalogo.setNumeroSubInciso(numeroSubInciso);
		validacionPorCatalogo.setRequiereFacultativo(requiereFacultativo);
		validacionPorCatalogo.setPorcentajePleno(porcentajePleno);
		validacionPorCatalogo.setIncisosRequierenFacultativo(incisosRequierenFacultativo);
		validacionPorCatalogo.setPermitirSolicitarFacultativo(permitirSolicitarFacultativo);
	
		return validacionPorCatalogo;
	}
	/**
	 * Method validarReaseguroFacultativoPorCatalogoCumuloPoliza()
	 * @param estructuraCotizacion
	 * @param subRamoDTO
	 * @param idToCotizacion
	 * @return ValidacionReaseguroFacultativoPorCatalogo
	 * @throws SystemException
	 */
	private ValidacionReaseguroFacultativoPorCatalogo validarReaseguroFacultativoPorCatalogoCumuloPoliza(SoporteEstructuraCotizacion estructuraCotizacion, SubRamoDTO subRamoDTO, 
		BigDecimal idToCotizacion) throws SystemException {
		boolean requiereFacultativo = false;
		boolean permitirSolicitarFacultativo = true;
		double porcentajePleno = 100;
		short AREA_REASEGURO = (short) 2;
		short CLAVE_INSPECCION_EXCLUIDO = (short) 9;
		List<IncisoCotizacionDTO> incisosRequierenFacultativo = new ArrayList<IncisoCotizacionDTO>();
		CotizacionDN cotizacionDN= CotizacionDN.getInstancia("");
		DatoIncisoCotizacionDN datoIncisoCotDN = DatoIncisoCotizacionDN.getINSTANCIA();
			// Obtener incisos en donde las coberturas esten contratadas
		List<IncisoCotizacionDTO> incisos =  estructuraCotizacion.obtenerIncisosPorSubRamo(subRamoDTO.getIdTcSubRamo());
		if (incisos == null)	incisos = new ArrayList<IncisoCotizacionDTO>();
		for (IncisoCotizacionDTO inciso : incisos) {
			try{
				Object subTipoDelSubRamo = datoIncisoCotDN.obtenerDTOSubTipoCorrespondienteAlSubRamo(subRamoDTO, idToCotizacion, inciso.getId().getNumeroInciso());
				if(subTipoDelSubRamo instanceof SubGiroDTO){
					SubGiroDTO subGiroDTO = (SubGiroDTO)subTipoDelSubRamo;
					PlenoReaseguroDTO plenoReaseguro = cotizacionDN.obtenerPlenoReaseguro(subGiroDTO);
					Double porcentajePlenoTemp = plenoReaseguro.getPorcentajeMaximo();
					if (porcentajePlenoTemp < porcentajePleno)
						porcentajePleno = porcentajePlenoTemp;
					if (subGiroDTO.getClaveInspeccion() == CLAVE_INSPECCION_EXCLUIDO) {
						incisosRequierenFacultativo.add(inciso);
						porcentajePleno = 0d;
						requiereFacultativo = true;
					}
				} else{
					java.lang.reflect.Method getClaveAutorizacion = subTipoDelSubRamo.getClass().getMethod("getClaveAutorizacion");
					Object claveAutorizacion = getClaveAutorizacion.invoke(subTipoDelSubRamo);
					if(Integer.valueOf(claveAutorizacion.toString()).shortValue() == AREA_REASEGURO ){
						incisosRequierenFacultativo.add(inciso);
						requiereFacultativo = true;
						porcentajePleno = 0d;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		// Si s�lo algunos incisos requieren facultativo por cat�logo, no se permitir� solicitar facultativo
		if (requiereFacultativo == true && incisosRequierenFacultativo.size() != incisos.size()) {
			permitirSolicitarFacultativo = false;
		}
		ValidacionReaseguroFacultativoPorCatalogo validacionPorCatalogo = new ValidacionReaseguroFacultativoPorCatalogo();
		validacionPorCatalogo.setTipoDistribucion(Sistema.TIPO_CUMULO_POLIZA);
		validacionPorCatalogo.setIdTcSubRamo(subRamoDTO.getIdTcSubRamo());
		validacionPorCatalogo.setIdToCotizacion(idToCotizacion);
		validacionPorCatalogo.setRequiereFacultativo(requiereFacultativo);
		validacionPorCatalogo.setPorcentajePleno(porcentajePleno);
		validacionPorCatalogo.setIncisosRequierenFacultativo(incisosRequierenFacultativo);
		validacionPorCatalogo.setPermitirSolicitarFacultativo(permitirSolicitarFacultativo);
		return validacionPorCatalogo;
	}
    
	/**
	 * Method getPorcentajePlenoIncendio().
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return double porcentaje pleno de Incendio
	 * @throws SystemException
	 */
	
	private double getPorcentajePlenoIncendio(SoporteEstructuraCotizacion estructuraCotizacion,BigDecimal numeroInciso) throws SystemException{
		double pleno = 100d;
//    	SubGiroDTO subGiroDTO;
//    	subGiroDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubgiro(idToCotizacion, numeroInciso,Sistema.SUBRAMO_INCENDIO );
		try{
	    	PlenoReaseguroDTO plenoReaseguro = estructuraCotizacion.getAdministradorDatosInciso().obtenerPlenoReaseguro(numeroInciso, Sistema.SUBRAMO_INCENDIO);
	    	if(plenoReaseguro!=null)
			    pleno = plenoReaseguro.getPorcentajeMaximo();
		}catch(Exception e){
		}
		return pleno;
	}
}