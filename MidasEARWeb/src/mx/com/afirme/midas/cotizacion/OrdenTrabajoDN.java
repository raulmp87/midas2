package mx.com.afirme.midas.cotizacion;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDN;
import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDN;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDTO;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;


public class OrdenTrabajoDN {
	public static final OrdenTrabajoDN INSTANCIA = new OrdenTrabajoDN();
	private static String nombreUsuario;
	
	public static OrdenTrabajoDN getInstancia (String nombreUsuario){
		OrdenTrabajoDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	public List<CotizacionDTO> listarOrdenesDeTrabajo() throws ExcepcionDeAccesoADatos, SystemException{
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		return new CotizacionSN(OrdenTrabajoDN.nombreUsuario).listarFiltrado(cotizacionDTO);
	}
	
	public List<CotizacionDTO> listarOrdenesDeTrabajoFiltrado(CotizacionDTO cotizacionDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException{
		String claveOrdenTrabajoFiltradas = "midas.danios.ordentrabajo.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveOrdenTrabajoFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveOrdenTrabajoFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;			
	
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)
			cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());

		//Se agrega filtro de Codigo de Agente Especial en caso que aplique
		AgenteEspecialDN.getInstancia().agregaFiltroCodigoAgente(usuario, cotizacionDTO);
		
		return new CotizacionSN(OrdenTrabajoDN.nombreUsuario).listarFiltrado(cotizacionDTO);
	}
	
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException {
		String claveOrdenTrabajoFiltradas = "midas.danios.ordentrabajo.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveOrdenTrabajoFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveOrdenTrabajoFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;			
	
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)
			cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());

		//Se agrega filtro de Codigo de Agente Especial en caso que aplique
		AgenteEspecialDN.getInstancia().agregaFiltroCodigoAgente(usuario, cotizacionDTO);
		
		return new CotizacionSN(OrdenTrabajoDN.nombreUsuario).obtenerTotalFiltrado(cotizacionDTO);		
	}
	
	public CotizacionDTO getPorId(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new CotizacionSN(OrdenTrabajoDN.nombreUsuario).getPorId(cotizacionDTO);
	}
	
	public void actualizarCotizacion(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new CotizacionSN(OrdenTrabajoDN.nombreUsuario).modificar(cotizacionDTO);
	}

	public CotizacionDTO getPorIdToSolicitud(BigDecimal idToSolicitud) throws ExcepcionDeAccesoADatos, SystemException{
		List<CotizacionDTO> cotizaciones = new CotizacionSN(OrdenTrabajoDN.nombreUsuario).buscarPorPropiedad("solicitudDTO.idToSolicitud", idToSolicitud);
		return cotizaciones.isEmpty()? null : cotizaciones.get(0);
	}
	
	public CotizacionDTO rechazarCancelarOrdenTrabajo(BigDecimal idToCotizacion,BigDecimal idTcRechazoCancelacion,Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException{
		RechazoCancelacionDTO rechazoCancelacion = RechazoCancelacionDN.getInstancia().getPorId(idTcRechazoCancelacion);
		CotizacionDTO cotizacionDTO = null;
		if(rechazoCancelacion != null){
			cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(idToCotizacion);
			cotizacionDTO = new CotizacionSN(OrdenTrabajoDN.nombreUsuario).getPorId(cotizacionDTO);
			cotizacionDTO = new CotizacionSN(OrdenTrabajoDN.nombreUsuario).obtenerDatosForaneos(cotizacionDTO);
			if (cotizacionDTO != null){
				if (rechazoCancelacion.getClaveTipoMotivo() == 1)
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_ODT_RECHAZADA);
				else
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_ODT_CANCELADA);
				cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				cotizacionDTO.setFechaModificacion(new Date());
				OrdenTrabajoDN.getInstancia(OrdenTrabajoDN.nombreUsuario).actualizarCotizacion(cotizacionDTO);
				CotizacionRechazoCancelacionId cotizacionRechazoId = new CotizacionRechazoCancelacionId();
				cotizacionRechazoId.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				cotizacionRechazoId.setIdTcRechazoCancelacion(rechazoCancelacion.getIdTcRechazoCancelacion());
				
				CotizacionRechazoCancelacionDTO cotizacionRechazo = null;
				List<CotizacionRechazoCancelacionDTO> rechazosExistentes = CotizacionRechazoCancelacionDN.getInstancia().buscarPorPropiedad("id.idToCotizacion", idToCotizacion);
				for(CotizacionRechazoCancelacionDTO rechazo : rechazosExistentes){
					CotizacionRechazoCancelacionDN.getInstancia().borrar(rechazo);
				}
				cotizacionRechazo = new CotizacionRechazoCancelacionDTO();
				cotizacionRechazo.setId(cotizacionRechazoId);
				cotizacionRechazo.setCotizacionDTO(cotizacionDTO);
				cotizacionRechazo.setRechazoCancelacionDTO(rechazoCancelacion);
				CotizacionRechazoCancelacionDN.getInstancia().agregar(cotizacionRechazo);
			}
		}
		return cotizacionDTO;
	}

	public Set<String> terminarOrdenTrabajo (BigDecimal idToCotizacion, Usuario usuario) throws SystemException {
	    	Set<String> errores = null;
		errores = validarOrdenTrabajo(idToCotizacion);
		CotizacionDTO cotizacionDTO =CotizacionDN.getInstancia(usuario.getNombreUsuario()).getPorId(idToCotizacion);
		cotizacionDTO = CotizacionDN.getInstancia(usuario.getNombreUsuario()).obtenerDatosForaneos(cotizacionDTO);
		if (ordenTrabajoValida(errores)) {
			//Cambiar estatus a requerida para retroactividad, diferimiento,
			//vigencia max o minima.
			if (errores.size() > 0) {
			    	if (errores.contains("ordentrabajo.retroactividad") || 
						errores.contains("ordentrabajo.diferimiento")){
			    	    if(cotizacionDTO.getClaveAutRetroacDifer()== null ||
						!Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains
						(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
			    		cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_REQUERIDA);
			    		 cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
			    	    }
			    	}else{
			    	    cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_NO_REQUERIDA);  
			    	    cotizacionDTO.setFechaSolAutRetroacDifer(null);
			    	}
				
				if (errores.contains("ordentrabajo.vigencia.maxima") || 
						errores.contains("ordentrabajo.vigencia.minima")) {
				    if(cotizacionDTO.getClaveAutVigenciaMaxMin()== null||
						!Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains
							(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
					cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_REQUERIDA);
					 cotizacionDTO.setFechaSolAutVigenciaMaxMin(Calendar.getInstance().getTime());
				    }
				}else{
				    cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_NO_REQUERIDA);
				    cotizacionDTO.setFechaSolAutVigenciaMaxMin(null);
				}
				   
				
			}
			cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_ODT_TERMINADA);
			CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
						
		}
		
		return errores;
	}
	
	//De acuerdo al Set generado por validarOrdenTrabajo determina si
	//la orden de trabajo es valida.
	public static boolean ordenTrabajoValida(Set<String> errores) {
		boolean esValida = true;
		int numeroErrores = 0;
		if (errores != null && errores.size() > 0) {
			numeroErrores = errores.contains("ordentrabajo.retroactividad")?++numeroErrores:numeroErrores;
			numeroErrores = errores.contains("ordentrabajo.diferimiento")?++numeroErrores:numeroErrores;
			numeroErrores = errores.contains("ordentrabajo.vigencia.maxima")?++numeroErrores:numeroErrores;
			numeroErrores = errores.contains("ordentrabajo.vigencia.minima")?++numeroErrores:numeroErrores;
			esValida = errores.size() > numeroErrores?false:true;
		}
		return esValida;
	}

	//Validar una orden de trabajo segun reglas de negocio.
	//Regresa una lista con los problemas que se encontraron durante la validacion
	private Set<String> validarOrdenTrabajo(BigDecimal idToCotizacion) throws SystemException {
		CotizacionSN cotizacionDAO = new CotizacionSN(OrdenTrabajoDN.nombreUsuario);
		CotizacionDTO cotizacionDTO = cotizacionDAO.getPorId(idToCotizacion);

		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
		List<IncisoCotizacionDTO> incisos = incisoCotizacionDN.listarPorCotizacionId(idToCotizacion);
		
		
		//Todas las validaciones segun las reglas de negocio.
		Set<String> errores = new HashSet<String>();
		
		Date fechaSolicitud = cotizacionDTO.getSolicitudDTO().getFechaCreacion();
		Date fechaInicioVigencia = cotizacionDTO.getFechaInicioVigencia();
		Date fechaFinVigencia = cotizacionDTO.getFechaFinVigencia();
		if (fechaSolicitud == null) {
			errores.add("ordentrabajo.fechaSolicitud.vacia");
		}
		if (fechaInicioVigencia == null) {
			errores.add("ordentrabajo.fechaInicioVigencia.vacia");
		}
		if (fechaFinVigencia == null) {
			errores.add("ordentrabajo.fechaFinVigencia.vacia");
		}
		if (fechaInicioVigencia != null && fechaSolicitud != null) {
			if (!validarRetroactividad(fechaInicioVigencia, fechaSolicitud)
					&& (cotizacionDTO.getClaveAutRetroacDifer()==null ||
			         (cotizacionDTO.getClaveAutRetroacDifer()!= null && 
				cotizacionDTO.getClaveAutRetroacDifer() != Sistema.AUTORIZADA))) {
				errores.add("ordentrabajo.retroactividad");
			}
			if (!validarDiferimiento(fechaInicioVigencia, fechaSolicitud)
					&& (cotizacionDTO.getClaveAutRetroacDifer()==null ||
						(cotizacionDTO.getClaveAutRetroacDifer() != null && 
						cotizacionDTO.getClaveAutRetroacDifer() != Sistema.AUTORIZADA))) {
				errores.add("ordentrabajo.diferimiento");			
			}
		}
		if (fechaInicioVigencia != null && fechaFinVigencia != null) {
			if (!validarVigenciaMaxima(fechaInicioVigencia, fechaFinVigencia)
				&& (cotizacionDTO.getClaveAutVigenciaMaxMin() ==null||
					 (cotizacionDTO.getClaveAutVigenciaMaxMin() !=null 
						&& cotizacionDTO.getClaveAutVigenciaMaxMin() != Sistema.AUTORIZADA))) {
				errores.add("ordentrabajo.vigencia.maxima");
			}
			if (!validarVigenciaMinima(fechaInicioVigencia, fechaFinVigencia)
					&& (cotizacionDTO.getClaveAutVigenciaMaxMin()==null ||
						(cotizacionDTO.getClaveAutVigenciaMaxMin() != null && 
						cotizacionDTO.getClaveAutVigenciaMaxMin() != Sistema.AUTORIZADA))) {
				errores.add("ordentrabajo.vigencia.minima");
			}
		}
		if (incisos == null || incisos.size() == 0) { //Validar si tiene incisos
			errores.add("ordentrabajo.incisos.vacio");
		}
		else {
			if (!validarIncisosTieneSeccionContratada(incisos)) {
				errores.add("ordentrabajo.seccion.sinContratar");
			}
			else {
				if (!validarSeccionTieneCoberturaContratada(incisos)) {
					errores.add("ordentrabajo.cobertura.sinContratar");
				}
			}
		}
		if (cotizacionDTO.getIdToPersonaContratante() == null) {
			errores.add("ordentrabajo.personaContratante.noExiste");
		}
		if (cotizacionDTO.getIdToPersonaAsegurado() == null) {
			errores.add("ordentrabajo.personaAsegurado.noExiste");
		}

		return errores;
	}
	
	private boolean validarSeccionTieneCoberturaContratada(
			List<IncisoCotizacionDTO> incisos) throws SystemException {
		boolean esValido = false;
		SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		
		incisos:
			for (IncisoCotizacionDTO inciso : incisos) {
				List<SeccionCotizacionDTO> secciones = 
					seccionCotizacionDN.listarPorIncisoId(inciso.getId());
				for (SeccionCotizacionDTO seccion : secciones) {
					List<CoberturaCotizacionDTO> coberturas = 
						coberturaCotizacionDN.listarPorSeccionCotizacionId(seccion.getId(),Boolean.TRUE);
					if (tieneCoberturaContratada(coberturas)) {
						esValido = true;
						break incisos;
					}
				}
			}
		return esValido;
	}
	
	private boolean validarIncisosTieneSeccionContratada(
			List<IncisoCotizacionDTO> incisos) throws SystemException {
		boolean esValido = false;
		SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
		
		for (IncisoCotizacionDTO inciso : incisos) {
			List<SeccionCotizacionDTO> secciones = 
				seccionCotizacionDN.listarPorIncisoId(inciso.getId());
			if (tieneSeccionContratada(secciones)) {
				esValido = true;
				break;
			}
		}
		return esValido;
	}

	private boolean tieneSeccionContratada(List<SeccionCotizacionDTO> secciones) {
		boolean tieneSeccionContrada = false;
		final short CONTRATADA = 1;
		for (SeccionCotizacionDTO seccion: secciones) {
			if (seccion.getClaveContrato() == CONTRATADA) {
				tieneSeccionContrada = true;
				break;
			}
		}
		return tieneSeccionContrada;
	}
	
	private boolean tieneCoberturaContratada(
			List<CoberturaCotizacionDTO> coberturas) {
		boolean tieneCoberturaContrada = false;
		final short CONTRATADA = 1;
		for (CoberturaCotizacionDTO cobertura: coberturas) {
			if (cobertura.getClaveContrato() == CONTRATADA) {
				tieneCoberturaContrada = true;
				break;
			}
		}
		return tieneCoberturaContrada;
	}
	
	public static boolean validarVigenciaMaxima(Date fechaInicioVigencia,
			Date fechaFinVigencia) {
		boolean esValido = true;
		final long diasMaximo = Long.parseLong(UtileriasWeb.getMensajeRecurso(
				Sistema.ARCHIVO_RECURSOS, "vigencia.dias.maximo"));
		long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaFinVigencia);
		if (diasDiferencia > diasMaximo) {
			esValido = false;
		}
		return esValido;
	}
	
	public static boolean validarVigenciaMinima(Date fechaInicioVigencia,
			Date fechaFinVigencia) {
		boolean esValido = true;
		final long diasMinimo = Long.parseLong(UtileriasWeb.getMensajeRecurso(
				Sistema.ARCHIVO_RECURSOS, "vigencia.dias.minimo"));
		long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaFinVigencia);
		if (diasDiferencia < diasMinimo) {
			esValido = false;
		}
		return esValido;
	}
	
	public static boolean validarDiferimiento(Date fechaInicioVigencia,
			Date fechaSolicitud) {
		boolean esValido = true;
		if (fechaInicioVigencia.compareTo(fechaSolicitud) > 0) {
			long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaSolicitud);
			if (diasDiferencia > 15) {
				esValido = false;
			}
		}
		return esValido;
	}

	public static boolean validarFechaRenovacion(Date fechaFinVigenciad) {
		boolean esValido = true;
		Date fechaActual = new Date();
		double diasDiferencia = UtileriasWeb.obtenerDiasEntreFechas(fechaActual, fechaFinVigenciad);
		if (diasDiferencia < 0) {
			esValido = false;
		}
		return esValido;
	}	
	public static boolean validarRetroactividad(Date fechaInicioVigencia, Date fechaSolicitud) {
		boolean esValido = true;
		if (fechaInicioVigencia.compareTo(fechaSolicitud) < 0) {
			long diasDiferencia = diasDiferencia(fechaInicioVigencia, fechaSolicitud);
			if (diasDiferencia > 15) {
				esValido = false;
			}
		}
		return esValido;
	}
	
	private static long diasDiferencia(Date fechaUno, Date fechaDos) {
	    	    
		long ms;
		final long DIAS_MILISEGUNDOS = 24 * 60 * 60 * 1000;
		if (fechaUno.compareTo(fechaDos) > 0) {
			ms = fechaUno.getTime() - fechaDos.getTime();
		}
		else {
			ms = fechaDos.getTime() - fechaUno.getTime();
		}
		return ms / DIAS_MILISEGUNDOS;	
	}
	
	
	
	
	
	
	
}
