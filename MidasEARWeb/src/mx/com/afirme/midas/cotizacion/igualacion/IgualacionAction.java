package mx.com.afirme.midas.cotizacion.igualacion;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Fernando Alonzo
 * @since 13/10/2009
 * 
 */
public class IgualacionAction extends MidasMappingDispatchAction {
	
	/**
	 * Method mostrarIgualaciones
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarIgualaciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Locale idiomaLocal= new Locale("es","MX");
		HttpSession session = request.getSession();
		session.setAttribute("idiomaLocal", idiomaLocal);
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarIgualacionPrimas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarIgualacionPrimas(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Locale idiomaLocal= new Locale("es","MX");
		HttpSession session = request.getSession();
		session.setAttribute("idiomaLocal", idiomaLocal);
		String reglaNavegacion = Sistema.EXITOSO;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		final IgualacionPrimaNetaForm igualacionPrimaNetaForm = (IgualacionPrimaNetaForm) form;
		try {
			String idToCotizacion = request.getParameter("id");						
			
			String numeroInciso = igualacionPrimaNetaForm.getNumeroInciso();
			if(idToCotizacion != null) {
				igualacionPrimaNetaForm.setIdToCotizacion(idToCotizacion);
			} else {
				idToCotizacion = igualacionPrimaNetaForm.getIdToCotizacion();
			}
			
			BigDecimal idToCotizacionBigDecimal = UtileriasWeb.regresaBigDecimal(idToCotizacion);
			Integer numeroIncisoInteger = (numeroInciso != null ? new Integer(numeroInciso) : -1); 
			
			List<SeccionCotizacionDTO> seccionesCombo = new ArrayList<SeccionCotizacionDTO>();
			
			boolean igualacionValida = CotizacionDN.getInstancia(nombreUsuario).
						mostrarIgualacionPrimas(idToCotizacionBigDecimal, numeroIncisoInteger, seccionesCombo, igualacionPrimaNetaForm);

			if (igualacionValida) {
				session = request.getSession();
				session.setAttribute("secciones", seccionesCombo);

			} else {
				session = request.getSession();
				session.setAttribute("secciones", new ArrayList<SeccionCotizacionDTO>());
			}
			
			igualacionPrimaNetaForm.setMostrarOriginales 
				(CotizacionDN.getInstancia(nombreUsuario).esMostrarOriginales(idToCotizacionBigDecimal)); //1 si se muestran, 0 si se ocultan
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			agregarMensaje(e.getMessage(), Sistema.ERROR, igualacionPrimaNetaForm);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		/*
		 * 15/06/2011 JLAB Se agrega flujo alterno para mostrar la pantalla como "solo lectura"
		 */
		String soloLectura= request.getParameter("soloLectura");
		if(!StringUtil.isEmpty(soloLectura) && soloLectura.equals("true")){
			reglaNavegacion = "soloLectura";
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarIgualacionCuotas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarIgualacionCuotas(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Locale idiomaLocal= new Locale("es","MX");
		HttpSession session = request.getSession();
		session.setAttribute("idiomaLocal", idiomaLocal);
		String reglaNavegacion = Sistema.EXITOSO;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		boolean aplicaIgualacionAInciso = false;
		
		try {
			String idToCotizacion = request.getParameter("id");
			IgualacionPrimaNetaForm igualacionPrimaNetaForm = (IgualacionPrimaNetaForm) form;
			if (idToCotizacion != null) {
				igualacionPrimaNetaForm.setIdToCotizacion(idToCotizacion);
			} else {
				idToCotizacion = igualacionPrimaNetaForm.getIdToCotizacion();
			}
			
			BigDecimal idToCotizacionBigDecimal = UtileriasWeb.regresaBigDecimal(idToCotizacion);
			
			StringBuilder mensajeError = new StringBuilder();
			boolean susCuotasSonValidas = CotizacionDN.getInstancia(nombreUsuario).validaTodasLasCoutasMayoresCeros(UtileriasWeb.regresaBigDecimal(idToCotizacion), mensajeError);
			if (susCuotasSonValidas) {
				igualacionPrimaNetaForm.setMensajeErrorIgualacionPrima(Sistema.SIN_MENSAJE_PARA_MOSTRAR);
				// Seccion para mostrar la tabla de resumen

				SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
				igualacionPrimaNetaForm.setTipoIgualacion("1");
				
				List<SeccionCotizacionDTO> seccionesCombo = new ArrayList<SeccionCotizacionDTO>();
				List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
				
				List<SeccionCotizacionDTO> secciones = seccionCotizacionDN.listarSeccionesIgualacionCuotas(
						idToCotizacionBigDecimal, seccionesCombo, coberturas,
						igualacionPrimaNetaForm, aplicaIgualacionAInciso);

				igualacionPrimaNetaForm.setSecciones(secciones);

				// Secciones y coberturas para los combos
				igualacionPrimaNetaForm.setSeccionesCombo(seccionesCombo);
				session = request.getSession();
				session.setAttribute("secciones", seccionesCombo);
				
				igualacionPrimaNetaForm.setCoberturas(coberturas);
			} else {
				igualacionPrimaNetaForm.setMensajeErrorIgualacionPrima(mensajeError.toString());
				igualacionPrimaNetaForm.setCoberturas(new ArrayList<CoberturaCotizacionDTO>());
				session = request.getSession();
				igualacionPrimaNetaForm.setSeccionesCombo(new ArrayList<SeccionCotizacionDTO>());
				igualacionPrimaNetaForm.setSecciones(new ArrayList<SeccionCotizacionDTO>());
				session.setAttribute("secciones", new ArrayList<SeccionCotizacionDTO>());
			}
			
			igualacionPrimaNetaForm.setMostrarOriginales 
			(CotizacionDN.getInstancia(nombreUsuario).esMostrarOriginales(idToCotizacionBigDecimal)); //1 si se muestran, 0 si se ocultan
			
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		/*
		 * 15/06/2011 JLAB Se agrega flujo alterno para mostrar la pantalla como "solo lectura"
		 */
		String soloLectura= request.getParameter("soloLectura");
		if(!StringUtil.isEmpty(soloLectura) && soloLectura.equals("true")){
			reglaNavegacion = "soloLectura";
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method igualarPrimaNeta
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward igualarPrimaNeta(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Locale idiomaLocal= new Locale("es","MX");
		HttpSession session = request.getSession();
		session.setAttribute("idiomaLocal", idiomaLocal);
		String reglaNavegacion = Sistema.EXITOSO;
		IgualacionPrimaNetaForm igualacionPrimaNetaForm = (IgualacionPrimaNetaForm) form;
		String idToCotizacion = igualacionPrimaNetaForm.getIdToCotizacion();
		String idToSeccion1 = igualacionPrimaNetaForm.getIdToSeccion1();
		String idToSeccion2 = igualacionPrimaNetaForm.getIdToSeccion2();
		String idToCobertura = igualacionPrimaNetaForm.getIdToCobertura();
		String tipoIgualacion = igualacionPrimaNetaForm.getTipoIgualacion();
		String primaNeta = igualacionPrimaNetaForm.getPrimaNeta(); 
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		String numeroInciso = igualacionPrimaNetaForm.getNumeroInciso();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		Boolean resultado = Boolean.FALSE;
		try {
			switch(UtileriasWeb.regresaShort(tipoIgualacion)) {
			case 1:
				if(numeroInciso.equals("-1")){
					resultado = cotizacionDN.igualarPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
							UtileriasWeb.regresaDouble(primaNeta), nombreUsuario);					
				}else{
					resultado = cotizacionDN.igualarPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
							UtileriasWeb.regresaDouble(primaNeta), UtileriasWeb.regresaBigDecimal(numeroInciso),nombreUsuario);					
				}
				break;
			case 2:
				if(numeroInciso.equals("-1")){
					resultado = cotizacionDN.igualarPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
							UtileriasWeb.regresaBigDecimal(idToSeccion1), UtileriasWeb.regresaDouble(primaNeta), nombreUsuario);					
				}else{
					resultado = cotizacionDN.igualarPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
							UtileriasWeb.regresaBigDecimal(idToSeccion1), UtileriasWeb.regresaDouble(primaNeta),UtileriasWeb.regresaBigDecimal(numeroInciso), nombreUsuario);					
				}
				break;
			case 3:
				if(numeroInciso.equals("-1")){
					resultado = cotizacionDN.igualarPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
							UtileriasWeb.regresaBigDecimal(idToSeccion2), UtileriasWeb.regresaBigDecimal(idToCobertura), 
							UtileriasWeb.regresaDouble(primaNeta), nombreUsuario);					
				}else{
					resultado = cotizacionDN.igualarPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
							UtileriasWeb.regresaBigDecimal(idToSeccion2), UtileriasWeb.regresaBigDecimal(idToCobertura), 
							UtileriasWeb.regresaDouble(primaNeta),UtileriasWeb.regresaBigDecimal(numeroInciso), nombreUsuario);					
				}

				break;
			}
			this.mostrarIgualacionPrimas(mapping, igualacionPrimaNetaForm, request, response);
			if(!resultado) {
				igualacionPrimaNetaForm.setMensaje("No se puede realizar la igualaci&oacute;n, ya que algunas primas ser�an negativas.");
				igualacionPrimaNetaForm.setTipoMensaje("20");
				reglaNavegacion = Sistema.NO_EXITOSO;
			} else {
				igualacionPrimaNetaForm.setMensaje("");
				igualacionPrimaNetaForm.setTipoMensaje("");
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method eliminaIgualacionPrima
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward eliminaIgualacionPrima(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Locale idiomaLocal= new Locale("es","MX");
		HttpSession session = request.getSession();
		session.setAttribute("idiomaLocal", idiomaLocal);
		String reglaNavegacion = Sistema.EXITOSO;
		IgualacionPrimaNetaForm igualacionPrimaNetaForm = (IgualacionPrimaNetaForm) form;
		String idToCotizacion = igualacionPrimaNetaForm.getIdToCotizacion();
		String idToSeccion1 = igualacionPrimaNetaForm.getIdToSeccion1();
		String idToSeccion2 = igualacionPrimaNetaForm.getIdToSeccion2();
		String idToCobertura = igualacionPrimaNetaForm.getIdToCobertura();
		String tipoIgualacion = igualacionPrimaNetaForm.getTipoIgualacion();
		String numeroInciso = igualacionPrimaNetaForm.getNumeroInciso();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		Boolean resultado = Boolean.FALSE;
		String usuario = UtileriasWeb.obtieneNombreUsuario(request);
		try {
			switch(UtileriasWeb.regresaShort(tipoIgualacion)) {
			case 1:
				if(numeroInciso.equals("-1")){
					resultado = cotizacionDN.eliminaIgualacionPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion),usuario);
				}else{
					resultado = cotizacionDN.eliminaIgualacionPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion),usuario,UtileriasWeb.regresaBigDecimal(numeroInciso));
				}
				break;
			case 2:
				if(numeroInciso.equals("-1")){
					resultado = cotizacionDN.eliminaIgualacionPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(idToSeccion1),usuario);
				}else{
					resultado = cotizacionDN.eliminaIgualacionPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(idToSeccion1),usuario,UtileriasWeb.regresaBigDecimal(numeroInciso));
				}
				break;
			case 3:
				if(numeroInciso.equals("-1")){
					resultado = cotizacionDN.eliminaIgualacionPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(idToSeccion2), UtileriasWeb.regresaBigDecimal(idToCobertura),usuario);
				}else{
					resultado = cotizacionDN.eliminaIgualacionPrimas(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(idToSeccion2), UtileriasWeb.regresaBigDecimal(idToCobertura),usuario,UtileriasWeb.regresaBigDecimal(numeroInciso));
				}
				break;
			}
			this.mostrarIgualacionPrimas(mapping, igualacionPrimaNetaForm, request, response);
			if(!resultado) {
				igualacionPrimaNetaForm.setPrimaNeta("");
				igualacionPrimaNetaForm.setMensaje("Ocurri&oacute; un error al eliminar la Igualaci&oacute;n de Primas.");
				igualacionPrimaNetaForm.setTipoMensaje(Sistema.ERROR);
				reglaNavegacion = Sistema.NO_EXITOSO;
			} else {
				igualacionPrimaNetaForm.setPrimaNeta("");
				igualacionPrimaNetaForm.setMensaje("La Igualaci&oacute;n de Primas fue eliminada correctamente.");
				igualacionPrimaNetaForm.setTipoMensaje(Sistema.EXITO);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	@SuppressWarnings("unchecked")
	public void escribeComboCobertura(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		HttpSession session = request.getSession();
		List<SeccionCotizacionDTO> secciones = (List<SeccionCotizacionDTO>) session.getAttribute("secciones");
		String idToSeccion = request.getParameter("id");
		SeccionCotizacionDTO seccionCotizacionDTO = null;
		for(SeccionCotizacionDTO seccion : secciones) {
			if(seccion.getId().getIdToSeccion().doubleValue() == Double.parseDouble(idToSeccion)) {
				seccionCotizacionDTO = seccion;
				break;
			}
		}
		//List<CoberturaCotizacionDTO> coberturas = seccionCotizacionDTO.getCoberturaCotizacionLista();
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
		for(CoberturaCotizacionDTO cobertura : seccionCotizacionDTO.getCoberturaCotizacionLista()) {
			if(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion().shortValue() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0)) {
				coberturas.add(cobertura);
			}
		}
		try {			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<CoberturaCotizacionDTO> iteratorList = coberturas.iterator();
			while (iteratorList.hasNext()) {
				CoberturaCotizacionDTO dto = iteratorList.next();
				CoberturaDN coberturaDN = CoberturaDN.getInstancia();
				CoberturaDTO coberturaDTO = coberturaDN.getPorId(dto.getId().getIdToCobertura());
				if(coberturaDTO.getClaveIgualacion() == 1) {
					buffer.append("<item>");
					buffer.append("<id>");
					buffer.append(dto.getId().getIdToCobertura());
					buffer.append("</id>");
					buffer.append("<description><![CDATA[");
					buffer.append(dto.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					buffer.append("]]></description>");
					buffer.append("</item>");
				}
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} catch (ExcepcionDeAccesoADatos edaad) {
			throw new SystemException("DB Error - Unable to render select tag",
					edaad);
		}// End of try/catch
	}

	/**
	 * Method igualarCuota
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward igualarCuota(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		Locale idiomaLocal= new Locale("es","MX");
		HttpSession session = request.getSession();
		session.setAttribute("idiomaLocal", idiomaLocal);
		String reglaNavegacion = Sistema.EXITOSO;
		IgualacionPrimaNetaForm igualacionPrimaNetaForm = (IgualacionPrimaNetaForm) form;
		String idToCotizacion = igualacionPrimaNetaForm.getIdToCotizacion();
		String idToSeccion2 = igualacionPrimaNetaForm.getIdToSeccion2();
		String idToCobertura = igualacionPrimaNetaForm.getIdToCobertura();
		String cuota = igualacionPrimaNetaForm.getPrimaNeta(); 
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			cotizacionDN.igualarCuotas(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(idToSeccion2), 
					UtileriasWeb.regresaBigDecimal(idToCobertura), UtileriasWeb.regresaDouble(cuota), 
					UtileriasWeb.obtieneNombreUsuario(request));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
}