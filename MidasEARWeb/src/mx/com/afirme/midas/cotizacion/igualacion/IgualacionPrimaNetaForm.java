package mx.com.afirme.midas.cotizacion.igualacion;

import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class IgualacionPrimaNetaForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String idToCotizacion;
	private String idToSeccion1;
	private String idToSeccion2;
	private List<SeccionCotizacionDTO> secciones;
	private List<IncisoCotizacionDTO> incisos;
	private String numeroInciso;
	private String idToCobertura;
	private List<CoberturaCotizacionDTO> coberturas;
	private String primaNeta;
	private String tipoIgualacion;
	private Double totalCotizacion;
	private Double totalCotizacionOriginal;
	private Boolean aplicoPrimerRiesgo;
	private List<SeccionCotizacionDTO> seccionesCombo;
	private String mensajeErrorIgualacionPrima;
	/* Variables adicionales para resumen de incisos*/
	private String primaNetaAnual;
	private String primaNetaCotizacion;
	private String montoRecargoPagoFraccionado;
	private String derechosPoliza;
	private String factorIVA;
	private String montoIVA;
	private String primaNetaTotal;
	private String mensajeIgualacion;
	private List<SoporteResumen> resumenComisiones;	
	
	//Variables para modificación de recargo por pago fraccionado y derechos
	private String tipoCalculoRPF;
	private Double valorRPFEditable;
	private String valorRPFSoloLectura;
	private Double porcentajeRPFEditable;
	private String porcentajeRPFSoloLectura;
	private String tipoCalculoDerechos;
	private Double valorDerechos;
	private String claveAutorizacionRPF;
	private String claveAutorizacionDerechos;
	private String mostrarEdicionDerechosRPF;
	
	private String soloLectura;
	
	private String mostrarOriginales;

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setSecciones(List<SeccionCotizacionDTO> secciones) {
		this.secciones = secciones;
	}

	public List<SeccionCotizacionDTO> getSecciones() {
		return secciones;
	}

	public void setIdToCobertura(String idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public String getIdToCobertura() {
		return idToCobertura;
	}

	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}

	public String getPrimaNeta() {
		return primaNeta;
	}

	public void setTipoIgualacion(String tipoIgualacion) {
		this.tipoIgualacion = tipoIgualacion;
	}

	public String getTipoIgualacion() {
		return tipoIgualacion;
	}

	public void setTotalCotizacion(Double totalCotizacion) {
		this.totalCotizacion = totalCotizacion;
	}

	public Double getTotalCotizacion() {
		return totalCotizacion;
	}

	public void setTotalCotizacionOriginal(Double totalCotizacionOriginal) {
		this.totalCotizacionOriginal = totalCotizacionOriginal;
	}

	public Double getTotalCotizacionOriginal() {
		return totalCotizacionOriginal;
	}

	public void setCoberturas(List<CoberturaCotizacionDTO> coberturas) {
		this.coberturas = coberturas;
	}

	public List<CoberturaCotizacionDTO> getCoberturas() {
		return coberturas;
	}

	public void setIdToSeccion1(String idToSeccion1) {
		this.idToSeccion1 = idToSeccion1;
	}

	public String getIdToSeccion1() {
		return idToSeccion1;
	}

	public void setIdToSeccion2(String idToSeccion2) {
		this.idToSeccion2 = idToSeccion2;
	}

	public String getIdToSeccion2() {
		return idToSeccion2;
	}

	public void setAplicoPrimerRiesgo(Boolean aplicoPrimerRiesgo) {
		this.aplicoPrimerRiesgo = aplicoPrimerRiesgo;
	}

	public Boolean getAplicoPrimerRiesgo() {
		return aplicoPrimerRiesgo;
	}

	public void setSeccionesCombo(List<SeccionCotizacionDTO> seccionesCombo) {
		this.seccionesCombo = seccionesCombo;
	}

	public List<SeccionCotizacionDTO> getSeccionesCombo() {
		return seccionesCombo;
	}

	public void setMensajeErrorIgualacionPrima(
		String mensajeErrorIgualacionPrima) {
	    this.mensajeErrorIgualacionPrima = mensajeErrorIgualacionPrima;
	}

	public String getMensajeErrorIgualacionPrima() {
	    return mensajeErrorIgualacionPrima;
	}

	public String getPrimaNetaAnual() {
		return primaNetaAnual;
	}

	public void setPrimaNetaAnual(String primaNetaAnual) {
		this.primaNetaAnual = primaNetaAnual;
	}

	public String getPrimaNetaCotizacion() {
		return primaNetaCotizacion;
	}

	public void setPrimaNetaCotizacion(String primaNetaCotizacion) {
		this.primaNetaCotizacion = primaNetaCotizacion;
	}

	public String getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}

	public void setMontoRecargoPagoFraccionado(String montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}

	public String getDerechosPoliza() {
		return derechosPoliza;
	}

	public void setDerechosPoliza(String derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}

	public String getFactorIVA() {
		return factorIVA;
	}

	public void setFactorIVA(String factorIVA) {
		this.factorIVA = factorIVA;
	}

	public String getMontoIVA() {
		return montoIVA;
	}

	public void setMontoIVA(String montoIVA) {
		this.montoIVA = montoIVA;
	}

	public String getPrimaNetaTotal() {
		return primaNetaTotal;
	}

	public void setPrimaNetaTotal(String primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}

	public List<SoporteResumen> getResumenComisiones() {
		return resumenComisiones;
	}

	public void setResumenComisiones(List<SoporteResumen> resumenComisiones) {
		this.resumenComisiones = resumenComisiones;
	}

	/**
	 * @return the incisos
	 */
	public List<IncisoCotizacionDTO> getIncisos() {
		return incisos;
	}

	/**
	 * @param incisos the incisos to set
	 */
	public void setIncisos(List<IncisoCotizacionDTO> incisos) {
		this.incisos = incisos;
	}

	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @return the mensajeIgualacion
	 */
	public String getMensajeIgualacion() {
		return mensajeIgualacion;
	}

	/**
	 * @param mensajeIgualacion the mensajeIgualacion to set
	 */
	public void setMensajeIgualacion(String mensajeIgualacion) {
		this.mensajeIgualacion = mensajeIgualacion;
	}
	
	public String getTipoCalculoRPF() {
		return tipoCalculoRPF;
	}

	public void setTipoCalculoRPF(String tipoCalculoRPF) {
		this.tipoCalculoRPF = tipoCalculoRPF;
	}

	public Double getValorRPFEditable() {
		return valorRPFEditable;
	}

	public void setValorRPFEditable(Double valorRPFEditable) {
		this.valorRPFEditable = valorRPFEditable;
	}
	
	public String getValorRPFSoloLectura() {
		return valorRPFSoloLectura;
	}

	public void setValorRPFSoloLectura(String valorRPFSoloLectura) {
		this.valorRPFSoloLectura = valorRPFSoloLectura;
	}

	public Double getPorcentajeRPFEditable() {
		return porcentajeRPFEditable;
	}

	public void setPorcentajeRPFEditable(Double porcentajeRPFEditable) {
		this.porcentajeRPFEditable = porcentajeRPFEditable;
	}

	public String getPorcentajeRPFSoloLectura() {
		return porcentajeRPFSoloLectura;
	}

	public void setPorcentajeRPFSoloLectura(String porcentajeRPFSoloLectura) {
		this.porcentajeRPFSoloLectura = porcentajeRPFSoloLectura;
	}
	
	public String getTipoCalculoDerechos() {
		return tipoCalculoDerechos;
	}

	public void setTipoCalculoDerechos(String tipoCalculoDerechos) {
		this.tipoCalculoDerechos = tipoCalculoDerechos;
	}

	public Double getValorDerechos() {
		return valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	public String getClaveAutorizacionRPF() {
		return claveAutorizacionRPF;
	}

	public void setClaveAutorizacionRPF(String claveAutorizacionRPF) {
		this.claveAutorizacionRPF = claveAutorizacionRPF;
	}

	public String getClaveAutorizacionDerechos() {
		return claveAutorizacionDerechos;
	}

	public void setClaveAutorizacionDerechos(String claveAutorizacionDerechos) {
		this.claveAutorizacionDerechos = claveAutorizacionDerechos;
	}

	public String getMostrarEdicionDerechosRPF() {
		return mostrarEdicionDerechosRPF;
	}

	public void setMostrarEdicionDerechosRPF(String mostrarEdicionDerechosRPF) {
		this.mostrarEdicionDerechosRPF = mostrarEdicionDerechosRPF;
	}

	public String getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(String soloLectura) {
		this.soloLectura = soloLectura;
	}

	public String getMostrarOriginales() {
		return mostrarOriginales;
	}

	public void setMostrarOriginales(String mostrarOriginales) {
		this.mostrarOriginales = mostrarOriginales;
	}

	/*public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
		SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
		List<SeccionCotizacionDTO> secciones;
		try {
			secciones = seccionCotizacionDN.listarSeccionesContratadas(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			this.secciones = secciones;
			if(secciones != null && !secciones.isEmpty()) {
				this.coberturas = secciones.get(0).getCoberturaCotizacionLista();
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return errors;
	}*/
}
