package mx.com.afirme.midas.cotizacion.resumen;

import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import mx.com.afirme.midas.cotizacion.CotizacionForm;

public class PlantillaCotizacion extends CotizacionForm{

	private static final long serialVersionUID = -3659791655866527403L;

	private String nombreCompletoClienteContratante;
	private String direccionCompletaClienteContratante;
	private String ubicacionInciso;
	private String tipoConstructivo;
	private String numeroPisos;
	private String descripcionGiro;
	private List<ResumenSeccionCotizacionForm> listaPlantillasSecciones;
	
	public String getNombreCompletoClienteContratante() {
		return nombreCompletoClienteContratante;
	}
	public void setNombreCompletoClienteContratante(String nombreCompletoClienteContratante) {
		this.nombreCompletoClienteContratante = nombreCompletoClienteContratante;
	}
	public String getDireccionCompletaClienteContratante() {
		return direccionCompletaClienteContratante;
	}
	public void setDireccionCompletaClienteContratante(String direccionCompletaClienteContratante) {
		this.direccionCompletaClienteContratante = direccionCompletaClienteContratante;
	}
	public String getUbicacionInciso() {
		return ubicacionInciso;
	}
	public void setUbicacionInciso(String ubicacionInciso) {
		this.ubicacionInciso = ubicacionInciso;
	}
	public String getTipoConstructivo() {
		return tipoConstructivo;
	}
	public void setTipoConstructivo(String tipoConstructivo) {
		this.tipoConstructivo = tipoConstructivo;
	}
	public String getNumeroPisos() {
		return numeroPisos;
	}
	public void setNumeroPisos(String numeroPisos) {
		this.numeroPisos = numeroPisos;
	}
	public String getDescripcionGiro() {
		return descripcionGiro;
	}
	public void setDescripcionGiro(String descripcionGiro) {
		this.descripcionGiro = descripcionGiro;
	}
	public List<ResumenSeccionCotizacionForm> getListaPlantillasSecciones() {
		return listaPlantillasSecciones;
	}
	public void setListaPlantillasSecciones(List<ResumenSeccionCotizacionForm> listaPlantillasSecciones) {
		this.listaPlantillasSecciones = listaPlantillasSecciones;
	}
	public JRDataSource getListaSeccionesCotizacionJasper(){
		return new JRBeanCollectionDataSource(listaPlantillasSecciones);
	}
	
}