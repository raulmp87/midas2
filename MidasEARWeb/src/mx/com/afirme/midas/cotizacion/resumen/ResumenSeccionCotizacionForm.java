package mx.com.afirme.midas.cotizacion.resumen;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ResumenSeccionCotizacionForm extends MidasBaseForm{
	private static final long serialVersionUID = -1578903197723324067L;
	private String numeroSeccion;
	private String descripcionSeccion;
	private String primaNeta;
	private List<ResumenCoberturaCotizacionForm> listaCoberturas;
	public String getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}
	public List<ResumenCoberturaCotizacionForm> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<ResumenCoberturaCotizacionForm> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getNumeroSeccion() {
		return numeroSeccion;
	}
	public void setNumeroSeccion(String numeroSeccion) {
		this.numeroSeccion = numeroSeccion;
	}
	public JRDataSource getListaCoberturasCotizacionJasper(){
		return new JRBeanCollectionDataSource(listaCoberturas);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof ResumenSeccionCotizacionForm))	return false;
		ResumenSeccionCotizacionForm castObj = (ResumenSeccionCotizacionForm) obj;
		if(castObj.getNumeroSeccion() == null) return false;
        return (castObj.getNumeroSeccion().equals(this.numeroSeccion));
	}
	
	@Override
	public int hashCode() {
        int result = 17;
        result = 37 * result + ( getNumeroSeccion() == null ? 0 : this.getNumeroSeccion().hashCode() );
        return result;
	}
}
