package mx.com.afirme.midas.cotizacion.resumen;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.cotizacion.CotizacionAction;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.endoso.CotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.validacion.CotizacionReaseguroFacultativoForm;
import mx.com.afirme.midas.danios.reportes.ImpresionAction;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.SeguimientoRenovacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.gestionPendientes.GestorPendientesDanos;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ResumenCotizacionAction extends CotizacionAction{
	private static final NumberFormat fCuota = Sistema.FORMATO_CUOTA;    
	private static final NumberFormat fMonto = Sistema.FORMATO_MONEDA;	
	/**
	 * mostrarResumenCotizacion
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarResumenCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		cotizacionForm.setIdToCotizacion(id);
		cotizacionForm
				.setListaIncisos(new ArrayList<ResumenIncisoCotizacionForm>());
		if (!UtileriasWeb.esCadenaVacia(id)) {
			BigDecimal idToCotizacion;
			try {
				idToCotizacion = UtileriasWeb.regresaBigDecimal(id);
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
						idToCotizacion);

				poblarResumenCotizacionForm(cotizacionDTO, cotizacionForm,
						UtileriasWeb.obtieneNombreUsuario(request));

				cotizacionForm.setBonificacionComision(cotizacionDTO
						.getPorcentajebonifcomision().toString());
				cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus()
						.toString());
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void poblarResumenCotizacionForm(CotizacionDTO cotizacionDTO,CotizacionForm cotizacionForm, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		List<IncisoCotizacionDTO> listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(cotizacionDTO.getIdToCotizacion());
		Double primaNetaAnual = 0D;
		List<CoberturaCotizacionDTO> listaCoberturasTmp = new ArrayList<CoberturaCotizacionDTO>();
		for(IncisoCotizacionDTO incisoCot : listaIncisos){
			ResumenIncisoCotizacionForm resumenIncisoForm = new ResumenIncisoCotizacionForm();
			resumenIncisoForm.setListaSecciones(new ArrayList<ResumenSeccionCotizacionForm>());
					
			List<SeccionCotizacionDTO>  listaSeccionesContratadas = SeccionCotizacionDN.getInstancia().listarPorIncisoId(incisoCot.getId());
			Double primaNetaInciso = 0D;
			for(SeccionCotizacionDTO seccionCot : listaSeccionesContratadas){
				Double primaNetaSeccion = 0D;
				ResumenSeccionCotizacionForm resumenSeccionForm = new ResumenSeccionCotizacionForm();
				resumenSeccionForm.setListaCoberturas(new ArrayList<ResumenCoberturaCotizacionForm>());
				resumenSeccionForm.setDescripcionSeccion(seccionCot.getSeccionDTO().getNombreComercial());
				
				CoberturaCotizacionDTO coberturaCotTMP = new CoberturaCotizacionDTO();
				coberturaCotTMP.setId(new CoberturaCotizacionId());
				coberturaCotTMP.getId().setIdToSeccion(seccionCot.getId().getIdToSeccion());
				coberturaCotTMP.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				coberturaCotTMP.getId().setNumeroInciso(incisoCot.getId().getNumeroInciso());
				coberturaCotTMP.setClaveContrato(Sistema.CONTRATADO);
				SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
				seccionCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
				coberturaCotTMP.setSeccionCotizacionDTO(seccionCotizacionDTO);
				List<CoberturaCotizacionDTO> listaCoberturasContratadas = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotTMP);
				listaCoberturasTmp.addAll(listaCoberturasContratadas);
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas){
					primaNetaAnual += coberturaCot.getValorPrimaNeta();
					primaNetaInciso += coberturaCot.getValorPrimaNeta();		
					primaNetaSeccion += coberturaCot.getValorPrimaNeta();
					ResumenCoberturaCotizacionForm resumenCoberturaForm = new ResumenCoberturaCotizacionForm();
					if(coberturaCot.getValorSumaAsegurada() > 0) {
						resumenCoberturaForm.setCuota(fCuota.format((coberturaCot.getValorPrimaNeta() / coberturaCot.getValorSumaAsegurada()) * 1000D));
					} else {
						resumenCoberturaForm.setCuota(fCuota.format(0D));
					}
					CoberturaDTO coberturaTMP = CoberturaDN.getInstancia().getPorId(coberturaCot.getId().getIdToCobertura());
					resumenCoberturaForm.setDescripcionCobertura(coberturaTMP.getDescripcion());
					resumenCoberturaForm.setPrimaNeta(fMonto.format(coberturaCot.getValorPrimaNeta()));
					resumenCoberturaForm.setSumaAsegurada(fMonto.format(coberturaCot.getValorSumaAsegurada()));
					resumenSeccionForm.getListaCoberturas().add(resumenCoberturaForm);
				}
				resumenSeccionForm.setPrimaNeta(fMonto.format(primaNetaSeccion));
				resumenIncisoForm.getListaSecciones().add(resumenSeccionForm);
			}
			
			DireccionDTO direccionDTO = incisoCot.getDireccionDTO();
			DireccionForm direccionForm = new DireccionForm();
			DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
			
			resumenIncisoForm.setDescripcionInciso(direccionForm.toString());
			resumenIncisoForm.setPrimaNeta(fMonto.format(primaNetaInciso.doubleValue()));
			String descripcionGiroInciso = "No disponible";
			try{
				CoberturaCotizacionDTO coberturaCotTMP = new CoberturaCotizacionDTO();
				coberturaCotTMP.setId(new CoberturaCotizacionId());
				coberturaCotTMP.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				coberturaCotTMP.getId().setNumeroInciso(incisoCot.getId().getNumeroInciso());
				coberturaCotTMP.setClaveContrato((short)1);
				List<CoberturaCotizacionDTO> listaCoberturasContratadas = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotTMP);
				boolean contieneIncendio = false;
				BigDecimal idTcRamo=null;
				CoberturaCotizacionDTO coberturaTMP = null;
				BigDecimal INCENDIO = new BigDecimal(1d);
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas){
					idTcRamo = coberturaCot.getIdTcSubramo();
					coberturaTMP = coberturaCot;
					if (coberturaCot.getIdTcSubramo().compareTo(INCENDIO) == 0){
						contieneIncendio = true;
						break;
					}
				}
				if (contieneIncendio){
					GiroDTO giro = DatoIncisoCotizacionDN.getINSTANCIA().obtenerGiroDTO(cotizacionDTO.getIdToCotizacion(), incisoCot.getId().getNumeroInciso());
					descripcionGiroInciso = giro.getDescripcionGiro();
				}
				else{
					BigDecimal OBRA_CIVIL_EN_CONSTRUCCION = new BigDecimal(10d);
					BigDecimal EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA = new BigDecimal(6d);
					BigDecimal EQUIPO_ELECTRONICO = new BigDecimal(5d);
					BigDecimal MONTAJE_MAQUINA = new BigDecimal(4d);
					BigDecimal ROTURA_MAQUINARIA = new BigDecimal(3d);
					BigDecimal RESPONSABILIDAD_CIVIL_GENERAL = new BigDecimal(2d);
					if (coberturaTMP.getIdTcSubramo().compareTo(RESPONSABILIDAD_CIVIL_GENERAL) == 0){
						SubGiroRCDTO subGiroRC = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubGiroRC(cotizacionDTO.getIdToCotizacion(),incisoCot.getId().getNumeroInciso(),idTcRamo);
						descripcionGiroInciso = subGiroRC.getGiroRC().getDescripcionGiroRC();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(ROTURA_MAQUINARIA) == 0){
						SubTipoMaquinariaDTO subTipoMaquinariaDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubTipoMaquinaria(cotizacionDTO.getIdToCotizacion(),incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoMaquinariaDTO.getTipoMaquinaria().getDescripcionTipoMaquinaria();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(MONTAJE_MAQUINA) == 0){
						SubtipoMontajeMaquinaDTO subTipoMontajeMaquinaDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubTipoMontajeMaquina(cotizacionDTO.getIdToCotizacion(),incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoMontajeMaquinaDTO.getTipoMontajeMaquinaDTO().getDescripciontipomontajemaq();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(EQUIPO_ELECTRONICO) == 0){
						SubtipoEquipoElectronicoDTO subTipoEquipoElectronicoDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubtipoEquipoElectronico(cotizacionDTO.getIdToCotizacion(),incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoEquipoElectronicoDTO.getEquipoElectronicoDTO().getDescripciontipoeqelectro();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0){
						SubtipoEquipoContratistaDTO subTipoEquipoContratistaDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubtipoEquipoContratista(cotizacionDTO.getIdToCotizacion(),incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoEquipoContratistaDTO.getTipoEquipoContratistaDTO().getDescripcionTipoEqContr();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(OBRA_CIVIL_EN_CONSTRUCCION) == 0){
						TipoObraCivilDTO tipoObraCivilDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerTipoObraCivil(cotizacionDTO.getIdToCotizacion(),incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = tipoObraCivilDTO.getDescripcionTipoObraCivil();
					}//Fin tipo obra civil.
				}
			}catch(Exception e){e.printStackTrace();}

			resumenIncisoForm.setDescripcionGiro(descripcionGiroInciso);			
			cotizacionForm.getListaIncisos().add(resumenIncisoForm);
		}
		cotizacionDTO.setPrimaNetaAnual(primaNetaAnual);
		List<SoporteResumen>resumenComisiones = new ArrayList<SoporteResumen>();
		ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,resumenComisiones,listaCoberturasTmp);
		cotizacionForm.setPrimaNetaAnual(fMonto.format(primaNetaAnual));
		cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
		cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
		cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
		cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
		cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
		cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));	
		cotizacionForm.setResumenComisiones(resumenComisiones);
	}
	
	

	/**
	 * Method liberar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */	
	public ActionForward liberar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		CotizacionReaseguroFacultativoForm cotizacionForm = (CotizacionReaseguroFacultativoForm)form;
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String icono = Sistema.ERROR;
		StringBuilder mensaje = new StringBuilder();
		boolean notificarLiberacionAReaseguro = false;
		String[] destinatariosArray = cotizacionForm.getCorreos().split(",");
		try {									
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));//obtenerCotizacionCompleta(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
			CotizacionForm cForm = new CotizacionForm();
			if (destinatariosArray[0].equals("")){
				if(cotizacionDTO.getSolicitudDTO().getEmailContactos() != null) {
					destinatariosArray = (cotizacionDTO.getSolicitudDTO().getEmailContactos()).split(",");
				}
			}else{
				cotizacionDTO.getSolicitudDTO().setEmailContactos(cotizacionForm.getCorreos());
				SolicitudDN.getInstancia().actualizar(cotizacionDTO.getSolicitudDTO());
			}
			
			boolean altaBajaAgrupacionesOK = true;

			//Si se trata de una cotizaci�n de endoso, se valida que no se den de alta/baja agrupaciones (Primer Riesgo/LUC)
			if(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada()!= null && 
			   cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada().intValue() != 0 &&
			   cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud() == Sistema.SOLICITUD_TIPO_ENDOSO){				
				altaBajaAgrupacionesOK = cotizacionDN.validarAltaBajaAgrupaciones(cForm, cotizacionDTO);
				cotizacionForm.setMensaje(cForm.getMensaje());
				cotizacionForm.setTipoMensaje(cForm.getTipoMensaje());	
			}
			// RMP Valida que la direccion del contratante tenga configurado un IVA
			CotizacionDN.getInstancia(null).obtenerPorcentajeIVA(cotizacionDTO.getIdToCotizacion());

			RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN.getInstancia();
			RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN.buscarDetalleRenovacionPoliza(cotizacionDTO.getSolicitudDTO().getIdToPolizaAnterior());
		
			if(altaBajaAgrupacionesOK){									
				//Se ejecuta el validador de Sumas Aseguradas contratadas			
				boolean tieneSumasValidas= cotizacionDN.validadaSumasAseguradas(cForm,cotizacionDTO,true);
				cotizacionForm.setMensaje(cForm.getMensaje());
				cotizacionForm.setTipoMensaje(cForm.getTipoMensaje());
				cotizacionForm.setIdToCotizacion(request.getParameter("id"));
				cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
				if(tieneSumasValidas){
					
					    boolean LUCCorrecto = cotizacionDN.validarCoberturasLUC(cForm, cotizacionDTO);
					    cotizacionForm.setMensaje(cForm.getMensaje());
						cotizacionForm.setTipoMensaje(cForm.getTipoMensaje());										
					    
						if(LUCCorrecto){				
							GestorPendientesDanos gestorPendientesDanos = new GestorPendientesDanos();
							List<String> listaErrores = new ArrayList<String>();
							
							if(gestorPendientesDanos.cotizacionListaParaLiberar(cotizacionDTO, listaErrores, true)){
		
								List<String> destinatarios = new ArrayList<String>();
								Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
								if(renovacionPolizaDTO != null){
									renovacionPolizaDN.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_COT_LIBERADA);
									renovacionPolizaDN.setDetalleNotificacion("Cotizaci�n Liberada por Usuario: " + UtileriasWeb.obtieneNombreUsuario(request));
									renovacionPolizaDN.agregarDetalleSeguimiento(renovacionPolizaDTO.getPolizaDTO());

									if (!destinatariosArray[0].equals("")) {
										for(String correo : destinatariosArray){
											destinatarios.add(correo);
										}										
									
										PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(renovacionPolizaDTO.getPolizaDTO().getIdToPoliza());
										String asunto = UtileriasWeb.getMensajeRecurso(
												Sistema.ARCHIVO_RECURSOS,
												"poliza.renovacion.mail.asunto", renovacionPolizaDTO.getPolizaDTO().getNumeroPolizaFormateada());
										String bodyCorreo = UtileriasWeb.getMensajeRecurso(
												Sistema.ARCHIVO_RECURSOS,
												"poliza.renovacion.mail.body",
												renovacionPolizaDTO.getPolizaDTO().getNumeroPolizaFormateada(),
												cotizacionDTO.getNombreAsegurado()!=null?cotizacionDTO.getNombreAsegurado():cotizacionDTO.getNombreEmpresaAsegurado(), 
												UtileriasWeb.obtenerDiasEntreFechas(
														new Date(), polizaDTO.getCotizacionDTO()
														.getFechaFinVigencia()),
												cotizacionDTO.getSolicitudDTO().getIdToSolicitud().toString());
										asunto += obtenerAsunto(renovacionPolizaDTO);
										
										byte[] byteArray = null;
										try{
											byteArray = ImpresionAction.imprimirCotizacion(cotizacionDTO.getIdToCotizacion(), usuario.getNombreUsuario());
										}catch(Exception e){
											LogDeMidasWeb.log("ERROR AL GENERAR IMPRESI&oacute;N DE COTIZACI&oacute;N: "+cotizacionDTO.getIdToCotizacion()+e.getCause(), Level.SEVERE, null);
										}
										List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
										if (byteArray != null) {
											adjuntos.add(new ByteArrayAttachment("reporteCotizacion"+cotizacionDTO.getIdToCotizacion()+".pdf",TipoArchivo.PDF,byteArray));
										}
										try{
											MailAction.enviaCorreoConTemplate(destinatarios,asunto, bodyCorreo, "Cliente", adjuntos);
											renovacionPolizaDN.setTipoMovimiento(Sistema.TIPO_ENVIO_ALERTAMIENTO);											
											renovacionPolizaDN.setDetalleNotificacion(UtileriasWeb.getMensajeRecurso(
													Sistema.ARCHIVO_RECURSOS,
													"poliza.renovacion.detalle.movimiento.email",
													destinatarios.toString(), "Cliente", cotizacionDTO.getNombreContratante()!=null?cotizacionDTO.getNombreContratante():cotizacionDTO.getNombreEmpresaContratante()));
											renovacionPolizaDN.agregarDetalleSeguimiento(renovacionPolizaDTO.getPolizaDTO());
																						
										}catch (SecurityException e){}
										cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_LIBERADA);
										cotizacionDTO.setFechaLiberacion(new Date());
										cotizacionDTO.setFechaModificacion(new Date());
										CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
										
										notificarLiberacionAReaseguro = true;
										icono = Sistema.EXITO;
										mensaje.append("La cotizaci&oacute;n fue liberada exitosamente.</br> La impresi&oacute;n de la cotizaci&oacute;n fue enviada por correo electr&oacute;nico.");
										
									}else{
										cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_LIBERADA);
										cotizacionDTO.setFechaLiberacion(new Date());
										cotizacionDTO.setFechaModificacion(new Date());
										CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
										
										notificarLiberacionAReaseguro = true;
										icono = Sistema.INFORMACION;
										mensaje.append("La cotizaci&oacute;n fue liberada exitosamente.</br> La impresi&oacute;n de la cotizaci&oacute;n no fue enviada debido a que no se registraron destinatarios.");

									}									
								}else{
									if( !destinatariosArray[0].equals("")){
										for(String correo : destinatariosArray){
											destinatarios.add(correo);
										}
										if(!destinatarios.isEmpty() && CotizacionEndosoDN.getInstancia(usuario.getNombreUsuario()).existenMovimientoDePrimas(cotizacionDTO)){
											byte[] byteArray = null;
											try{
												byteArray = ImpresionAction.imprimirCotizacion(cotizacionDTO.getIdToCotizacion(), usuario.getNombreUsuario());
											}catch(Exception e){
												LogDeMidasWeb.log("ERROR AL GENERAR IMPRESI&oacute;N DE COTIZACI&oacute;N: "+cotizacionDTO.getIdToCotizacion()+e.getCause(), Level.SEVERE, null);
											}
											String nombreAsegurado = "";
											ClienteDTO clienteDTO = null;
											List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
											if (byteArray != null) {
												adjuntos.add(new ByteArrayAttachment("reporteCotizacion"+cotizacionDTO.getIdToCotizacion()+".pdf",TipoArchivo.PDF,byteArray));
											}
											if (cotizacionDTO.getIdToPersonaAsegurado() != null){
												try{

													if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
														clienteDTO = new ClienteDTO();
														clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
														clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
														clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, UtileriasWeb.obtieneNombreUsuario(request));
													}else{
														clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
													}
											
												}catch(Exception e){}
											}
											if (clienteDTO != null) {
												nombreAsegurado = ClienteAction.obtenerNombreCliente(clienteDTO);
											}
											String asunto = "Cotizaci�n Concluida " + cotizacionDTO.getSolicitudDTO().getIdToSolicitud() + ", Producto: " 
													+ cotizacionDTO.getSolicitudDTO().getProductoDTO().getNombreComercial();
											String contenido = "Se notifica que la Cotizaci&oacute;n de la Solicitud " + cotizacionDTO.getSolicitudDTO().getIdToSolicitud() 
													+ " fue terminada. Se anexa cotizaci&oacute;n. Para dudas o aclaraciones favor de comunicarse con su ejecutivo.\n\n\n";
											try{
												MailAction.enviaCorreoConTemplate(destinatarios,asunto, contenido, nombreAsegurado, adjuntos);
											}catch (SecurityException e){}
										}
										cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_LIBERADA);
										cotizacionDTO.setFechaLiberacion(new Date());
										cotizacionDTO.setFechaModificacion(new Date());
										CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
										
										notificarLiberacionAReaseguro = true;
										icono = Sistema.EXITO;
										mensaje.append("La cotizaci&oacute;n fue liberada exitosamente.</br> La impresi&oacute;n de la cotizaci&oacute;n fue enviada por correo electr&oacute;nico.");
									}else{
										cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_LIBERADA);
										cotizacionDTO.setFechaLiberacion(new Date());
										cotizacionDTO.setFechaModificacion(new Date());
										CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
										
										notificarLiberacionAReaseguro = true;
										icono = Sistema.INFORMACION;
										mensaje.append("La cotizaci&oacute;n fue liberada exitosamente.</br> La impresi&oacute;n de la cotizaci&oacute;n no fue enviada debido a que no se registraron destinatarios.");
									}									
								}
							}else{
								icono = Sistema.INFORMACION;
								mensaje.append("La cotizaci&oacute;n NO fue liberada:</br>");
								for (String mensajeError : listaErrores){
									mensaje.append(mensajeError+"</br>");
								}
							}
					
							if(notificarLiberacionAReaseguro){
								/**
								 * 26/03/2010. Se agreg� notificacion a reaseguro cuando se libera la cotizacion.
								 * Jos� Luis Arellano
								 */
								try{
									SoporteReaseguroCotizacionDN soporte = new SoporteReaseguroCotizacionDN(cotizacionDTO.getIdToCotizacion(),false);
									soporte.notificarLiberacion(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso());
								}catch(Exception e){}
							}
							cotizacionForm.setTipoMensaje(icono);
							cotizacionForm.setMensaje(mensaje.toString());
						}
				}
			
			}
			/*cotizacionForm.setListaIncisos(new ArrayList<ResumenIncisoCotizacionForm>());
			poblarResumenCotizacionForm(cotizacionDTO, cotizacionForm, UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionForm.setBonificacionComision(cotizacionDTO.getPorcentajebonifcomision().toString());*/
			cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
		} catch (SystemException e) {
			icono = Sistema.ERROR;
			mensaje= new StringBuilder();
			mensaje.append("Ocurri&oacute; un error al liberar la cotizaci&oacute;n.");
			mensaje.append("<br />" + e.getMessage());
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());
		} catch (ExcepcionDeAccesoADatos e) {
			icono = Sistema.ERROR;
			mensaje= new StringBuilder();
			mensaje.append("Ocurri&oacute; un error al liberar la cotizaci&oacute;n.");
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());
		}
		return mapping.findForward(Sistema.EXITOSO);
		
	}

	public String obtenerAsunto(RenovacionPolizaDTO renovacionPolizaDTO) {
		StringBuilder asunto = new StringBuilder("");
		for(SeguimientoRenovacionDTO seguimiento: renovacionPolizaDTO.getSeguimientoRenovacion()){
			if(seguimiento.getTipoMovimiento().intValue() == Sistema.TIPO_ENVIO_ALERTAMIENTO.intValue()){
				asunto.append(" (Modificaci�n de Cotizaci�n)");
				break;
			}
		}
		return asunto.toString();
	}
	
}
