package mx.com.afirme.midas.cotizacion.resumen;

import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ResumenIncisoCotizacionForm extends MidasBaseForm{

	private static final long serialVersionUID = -6878872877614099440L;
	private String primaNeta;
	private String descripcionGiro;
	private String descripcionInciso;
	private List<ResumenSeccionCotizacionForm> listaSecciones;
	public String getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}
	public String getDescripcionGiro() {
		return descripcionGiro;
	}
	public void setDescripcionGiro(String descripcionGiro) {
		this.descripcionGiro = descripcionGiro;
	}
	public List<ResumenSeccionCotizacionForm> getListaSecciones() {
		return listaSecciones;
	}
	public void setListaSecciones(List<ResumenSeccionCotizacionForm> listaSecciones) {
		this.listaSecciones = listaSecciones;
	}
	public String getDescripcionInciso() {
		return descripcionInciso;
	}
	public void setDescripcionInciso(String descripcionInciso) {
		this.descripcionInciso = descripcionInciso;
	}
	
	public JRDataSource getListaSeccionesJasper(){
		return new JRBeanCollectionDataSource(listaSecciones);
	}
	
}
