package mx.com.afirme.midas.cotizacion.resumen;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ResumenCotizacionDN {
	private static final ResumenCotizacionDN INSTANCIA = new ResumenCotizacionDN();

	public static ResumenCotizacionDN getInstancia() {
		return ResumenCotizacionDN.INSTANCIA;
	}
	public void setTotalesResumenCotizacionEndoso(CotizacionDTO cotizacionDTO) throws SystemException{
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION) {
			EndosoIDTO endosoIDTO = new EndosoIDTO();
			EndosoDTO ultimoEndoso = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion())
			.getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
			
			Integer tipoEndoso = UtileriasWeb.calculaTipoEndoso(cotizacionDTO);
			endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(
					ultimoEndoso.getId().getIdToPoliza().toString() + "|"
							+ ultimoEndoso.getId().getNumeroEndoso(), cotizacionDTO.getCodigoUsuarioModificacion(),
					cotizacionDTO.getFechaInicioVigencia(),tipoEndoso);	
			
			cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());			
			CotizacionDN.getInstancia(null).modificar(cotizacionDTO);

			if(	endosoIDTO.getCalculo()!= null && endosoIDTO.getCalculo().equals(Sistema.TIPO_CALCULO_SEYCOS)){		
				Double ivaObtenido = CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), cotizacionDTO.getCodigoUsuarioModificacion());
				BigDecimal factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
				cotizacionDTO.setDerechosPoliza(endosoIDTO.getDerechos()* -1D);
				cotizacionDTO.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());
				cotizacionDTO.setMontoBonificacionComision(endosoIDTO.getBonificacionComision()* -1D);
				cotizacionDTO.setMontoIVA(endosoIDTO.getIva()* -1D);
				cotizacionDTO.setMontoRecargoPagoFraccionado(endosoIDTO.getRecargoPF()* -1D);			
				cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(endosoIDTO.getBonificacionComisionRPF()* -1D);		
				cotizacionDTO.setPrimaNetaCotizacion(endosoIDTO.getPrimaNeta()* -1D);
				cotizacionDTO.setPrimaNetaTotal(endosoIDTO.getPrimaTotal()* -1D);				
			}else{
				setTotalesResumenCotizacion(cotizacionDTO, new ArrayList<SoporteResumen>());
			}
		}
	}
	public void setTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<SoporteResumen> resumenComisiones) throws SystemException{
		BigDecimal factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		BigDecimal primaNetaCotizacion = BigDecimal.ZERO;
		BigDecimal pctPagoFraccionado = BigDecimal.ZERO;
		BigDecimal derechosPoliza = BigDecimal.ZERO;
		BigDecimal factorIVA = BigDecimal.ZERO;
		BigDecimal montoIVA = BigDecimal.ZERO;
		BigDecimal valorBonifComision = BigDecimal.ZERO;
		BigDecimal valorComision = BigDecimal.ZERO;
		BigDecimal valorComisionRPF = BigDecimal.ZERO;
		Double ivaObtenido=null;
		EndosoDTO ultimoEndoso = null;
		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				ultimoEndoso = EndosoDN.getInstancia(
						cotizacionDTO.getCodigoUsuarioModificacion())
						.getUltimoEndoso(
								cotizacionDTO.getSolicitudDTO()
										.getIdToPolizaEndosada());
				Integer tipoEndoso = UtileriasWeb.calculaTipoEndoso(cotizacionDTO);
				EndosoIDTO endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(
						ultimoEndoso.getId().getIdToPoliza().toString() + "|"
								+ ultimoEndoso.getId().getNumeroEndoso(), cotizacionDTO.getCodigoUsuarioModificacion(),
						cotizacionDTO.getFechaInicioVigencia(),tipoEndoso);
	
				factor = UtileriasWeb.getFactorVigencia(endosoIDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());

				primaNetaCotizacion = BigDecimal.valueOf(CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()));
				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion.doubleValue());
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(endosoIDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
				cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
				CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).modificar(cotizacionDTO);
			}else{
				factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
				MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
				dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

				movimientos = MovimientoCotizacionEndosoDN
						.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).listarFiltrado(dto);

				for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
					if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0) {
						primaNetaCotizacion = primaNetaCotizacion.add(movimiento.getValorDiferenciaPrimaNeta());
					}
				}
				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion.doubleValue());
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
				
			}
		}else{
			if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
					cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual());
				cotizacionDTO.setDiasPorDevengar(365D);
			}else{
				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
			}
			
		}
		List<SoporteResumen> resumenComisionesTmp = new ArrayList<SoporteResumen>();
		//Prima neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion.doubleValue());

		//Obtiene el limite de SA
		List<ParametroGeneralDTO> parametrosLimiteSA = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("id.codigoParametroGeneral",
				BigDecimal.valueOf(30010D));
				
		double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		
		//Obtiene si existe una agrupacion a Primer riesgo
		AgrupacionCotDTO agrupacion = PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),Sistema.TIPO_PRIMER_RIESGO);	

		
		boolean esEndosoCFP = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO;
		boolean esEndosoCancelacion = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION;
				 
		//Obtiene % por forma de Pago	
		CotizacionDTO cotizacionTmp = null;
		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario() == null || cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0 || esEndosoCFP){
			pctPagoFraccionado = BigDecimal.valueOf(CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO));						
			cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_SISTEMA, pctPagoFraccionado.doubleValue(), false, false);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){
			cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getValorRecargoPagoFraccionadoUsuario(), false, false);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
			cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_PORCENTAJE, cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario(), false, false);
		}
		
		pctPagoFraccionado = BigDecimal.valueOf(cotizacionTmp.getPorcentajeRecargoPagoFraccionadoUsuario());				
		cotizacionDTO.setPorcentajePagoFraccionado(pctPagoFraccionado.doubleValue());
		
		//Se obtienen los gastos de expedicion
		if(cotizacionDTO.getValorDerechosUsuario()==null || cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_SISTEMA) == 0 || esEndosoCFP || esEndosoCancelacion){
			derechosPoliza = BigDecimal.valueOf(CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO,false));
			boolean sinAutorizacion = false;
			if(esEndosoCFP || esEndosoCancelacion) {
				sinAutorizacion = true;
			}						
			CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_SISTEMA, derechosPoliza.doubleValue(), false, sinAutorizacion);
		}else{
			derechosPoliza = BigDecimal.valueOf(cotizacionDTO.getValorDerechosUsuario());
		}
		
		cotizacionDTO.setDerechosPoliza(derechosPoliza.doubleValue());
		
		//Se obtiene % de IVA
		ivaObtenido = CotizacionDN.getInstancia(null).obtenerPorcentajeIVA(cotizacionDTO.getIdToCotizacion());
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		
		factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
		cotizacionDTO.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());

		// se guardan las comisiones q se eliminaron en este arreglo
		int numeroEndosoAnterior = 0;
		EndosoDTO endosoDTO = null;
		List<SoporteResumen> resumenComisionesEliminadasTmp = new ArrayList<SoporteResumen>();
		EndosoDTO endosoActualDTO = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
		if (endosoActualDTO != null && endosoActualDTO.getId().getNumeroEndoso().intValue() != 0){
			numeroEndosoAnterior = endosoActualDTO.getId().getNumeroEndoso().intValue() - 1;
			endosoDTO = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPorId(new EndosoId(endosoActualDTO.getId().getIdToPoliza(), (short)numeroEndosoAnterior));
		}else{
			endosoDTO = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		}
		
		

		//se obtiene correctamente el monto de bonificacion de la comision
		List<CoberturaCotizacionDTO> coberturas= new ArrayList<CoberturaCotizacionDTO>();
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			for(MovimientoCotizacionEndosoDTO movimiento: movimientos){
				if(movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() > 0){
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(movimiento.getIdToCotizacion());
					id.setNumeroInciso(movimiento.getNumeroInciso());
					id.setIdToSeccion(movimiento.getIdToSeccion());
					id.setIdToCobertura(movimiento.getIdToCobertura());
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO = coberturaCotizacionDN.getPorId(coberturaCotizacionDTO);
					if(coberturaCotizacionDTO != null){
						coberturaCotizacionDTO.setValorPrimaNeta(movimiento.getValorDiferenciaPrimaNeta().doubleValue());
						coberturas.add(coberturaCotizacionDTO);
					}else{
						CoberturaCotizacionDTO coberturaCotizacionTmp = new CoberturaCotizacionDTO();
						CoberturaCotizacionId idTmp = new CoberturaCotizacionId();
						idTmp.setIdToCotizacion(endosoDTO.getIdToCotizacion());
						idTmp.setNumeroInciso(movimiento.getNumeroInciso());
						idTmp.setIdToSeccion(movimiento.getIdToSeccion());
						idTmp.setIdToCobertura(movimiento.getIdToCobertura());
						coberturaCotizacionTmp.setId(idTmp);
						coberturaCotizacionTmp = coberturaCotizacionDN.getPorId(coberturaCotizacionTmp);						
						
						ComisionCotizacionId idComis = new ComisionCotizacionId();
						idComis.setIdTcSubramo(coberturaCotizacionTmp.getIdTcSubramo());
						idComis.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						
						ComisionCotizacionDTO comision = null;
						SoporteResumen resumenComision = new SoporteResumen();
						Double sumaAseguradaIncendio = 0D;					
						
						if (coberturaCotizacionTmp.getIdTcSubramo().intValue()== 1) {
							limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
							// Se obtiene la SA de Incendio por Inciso
							sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
									.obtenerSACoberturasBasicasIncendioPorCotizacion(
											coberturaCotizacionTmp.getId().getIdToCotizacion());
							if (agrupacion != null) {
								if (coberturaCotizacionTmp.getNumeroAgrupacion().intValue() == agrupacion
										.getId().getNumeroAgrupacion().intValue()) {
									idComis.setTipoPorcentajeComision(Sistema.TIPO_PRR);
								}else{
									idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);
								}
							} else {
								if (sumaAseguradaIncendio < limiteSA) {
									idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);
								} else if (sumaAseguradaIncendio >= limiteSA) {
									idComis.setTipoPorcentajeComision(Sistema.TIPO_RCI);
								}else{
									idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);	
								}
							}
						} else {
							idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);
						}
						comision = ComisionCotizacionDN.getInstancia().getPorId(idComis);		
						//valorComision = valorComision.add(BigDecimal.valueOf(coberturaCotizacionTmp.getValorPrimaNeta()).multiply(comision.getPorcentajeComisionCotizacion()).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
						SubRamoDTO subRamoDTO = new SubRamoDTO();
						subRamoDTO.setIdTcSubRamo(coberturaCotizacionTmp.getIdTcSubramo());
						
						subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoDTO);

						resumenComision.setMontoComision((coberturaCotizacionTmp.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D)*-1);
						resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
						resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
						resumenComision.setMontoComisionCedida((resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D)*-1);
						resumenComision.setId(comision.getId());
						resumenComisionesEliminadasTmp.add(resumenComision);						
						
					}
				}
			}
		}else{
			coberturas = coberturaCotizacionDN.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		}
			
		

		for(CoberturaCotizacionDTO cobertura: coberturas){
			valorBonifComision = valorBonifComision.add(BigDecimal.valueOf(cobertura.getValorPrimaNeta()).multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(cobertura.getIdTcSubramo());
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			ComisionCotizacionDTO comision = null;
			SoporteResumen resumenComision = new SoporteResumen();
			Double sumaAseguradaIncendio = 0D;

			if (cobertura.getIdTcSubramo().intValue()== 1) {
				limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
						.obtenerSACoberturasBasicasIncendioPorCotizacion(
								cobertura.getId().getIdToCotizacion());
				if (agrupacion != null) {
					if (cobertura.getNumeroAgrupacion().intValue() == agrupacion
							.getId().getNumeroAgrupacion().intValue()) {
						id.setTipoPorcentajeComision(Sistema.TIPO_PRR);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id.setTipoPorcentajeComision(Sistema.TIPO_RCI);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);	
					}
				}
			} else {
				id.setTipoPorcentajeComision(Sistema.TIPO_RO);
			}
			comision = ComisionCotizacionDN.getInstancia().getPorId(id);		
			valorComision = valorComision.add(BigDecimal.valueOf(cobertura.getValorPrimaNeta() * factor.doubleValue()).multiply(comision.getPorcentajeComisionCotizacion()).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
			SubRamoDTO subRamoDTO = new SubRamoDTO();
			subRamoDTO.setIdTcSubRamo(cobertura.getIdTcSubramo());
			
			subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoDTO);

			resumenComision.setMontoComision(cobertura.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D);
			resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
			resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			resumenComision.setMontoComisionCedida(resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D);
			resumenComision.setId(comision.getId());
			resumenComisionesTmp.add(resumenComision);
		}
		
		// valorBonifComision = PrimaNeta de la cotizacion * porcentajeBonifComision/100		
		cotizacionDTO.setMontoBonificacionComision(valorComision.multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());		
		
		EndosoDTO endosoEmitido = EndosoDN.getInstancia(null).buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
		boolean esEndosoCE = false;
		boolean esEndosoRE = false;
		if(endosoEmitido != null){
			esEndosoCE = endosoEmitido.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CE;
			esEndosoRE = endosoEmitido.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_RE;
		}
		if(esEndosoCE || esEndosoRE){
			primaNetaCotizacion = new BigDecimal(endosoEmitido.getValorPrimaNeta());
			cotizacionDTO.setMontoBonificacionComision(endosoEmitido.getValorBonifComision());
			cotizacionDTO.setMontoRecargoPagoFraccionado(endosoEmitido.getValorRecargoPagoFrac());
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(endosoEmitido.getValorBonifComisionRPF());
			cotizacionDTO.setDerechosPoliza(endosoEmitido.getValorDerechos());			
		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!=null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
			BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
			double diasPorDevengar = UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			mx.com.afirme.midas.poliza.PolizaDN.getInstancia().setMontosRFPAcumulados(idToPoliza, diasPorDevengar, cotizacionDTO);
			cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() * -1D);
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado() * -1D);
		}else{
			// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
			cotizacionDTO.setMontoRecargoPagoFraccionado(primaNetaCotizacion.multiply(pctPagoFraccionado).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());
					
			valorComisionRPF = valorComision.multiply(pctPagoFraccionado).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
			// valorBonifComRecPagoFrac = valorRecargoPagoFrac * porcentajeBonifComision/100
			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(valorComisionRPF.multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());		
		}
								
		// valorIVA = (primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
		montoIVA = primaNetaCotizacion.add(BigDecimal.valueOf(cotizacionDTO.getMontoRecargoPagoFraccionado())).add(BigDecimal.valueOf(cotizacionDTO.getDerechosPoliza())).subtract(BigDecimal.valueOf(cotizacionDTO.getMontoBonificacionComision())).subtract(BigDecimal.valueOf(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado())).multiply(factorIVA);
		cotizacionDTO.setMontoIVA(montoIVA.doubleValue());

		//Prime neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion.doubleValue() - cotizacionDTO.getMontoBonificacionComision());
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado());
		
		// valorPrimaTotal = primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA		
		cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaCotizacion()
				+ cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()
				+ cotizacionDTO.getDerechosPoliza().doubleValue()
				+ montoIVA.doubleValue());
		//set comisiones 
		resumenComisionesTmp.addAll(resumenComisionesEliminadasTmp);
		this.ordenarAgruparComisiones(resumenComisionesTmp, resumenComisiones);
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
			if(ultimoEndoso.getIdToCotizacion()!= null){
				CotizacionDTO cotizacionUltimoEndoso = CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPorId(ultimoEndoso.getIdToCotizacion());
				cotizacionUltimoEndoso.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
				List<CoberturaCotizacionDTO> coberturasUltimoEndoso = coberturaCotizacionDN.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
				calcularTotalesResumenCotizacion(cotizacionUltimoEndoso, coberturasUltimoEndoso);
				cotizacionDTO.setPrimaNetaCotizacion(0D);
				cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionUltimoEndoso.getMontoRecargoPagoFraccionado());				
				cotizacionDTO.setDerechosPoliza(0D);				
				cotizacionDTO.setMontoIVA(cotizacionDTO.getMontoRecargoPagoFraccionado() * factorIVA.doubleValue());
				cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getMontoRecargoPagoFraccionado() + cotizacionDTO.getMontoIVA());
				
				cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(cotizacionDTO.getMontoRecargoPagoFraccionado());
				CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getMontoRecargoPagoFraccionado(), false, true);
				
				cotizacionDTO.setValorDerechosUsuario(cotizacionDTO.getDerechosPoliza());
				CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_USUARIO, cotizacionDTO.getDerechosPoliza(), false, true);
			}
		}
	}

	public void setTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<SoporteResumen> resumenComisiones, List<CoberturaCotizacionDTO> coberturas) throws SystemException{
		BigDecimal factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		double primaNetaCotizacion = 0D;
		double pctPagoFraccionado = 0D;
		double derechosPoliza = 0D;
		double factorIVA = 0D;
		double montoIVA = 0D;
		double valorBonifComision = 0D;
		double valorComision = 0D;
		double valorComisionRPF = 0D;

		if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
				cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
			primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			cotizacionDTO.setDiasPorDevengar(365D);
		}else{
			cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
			primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual() * factor.doubleValue();
		}
		
		List<SoporteResumen> resumenComisionesTmp = new ArrayList<SoporteResumen>();
		//Prima neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion);

		//Obtiene el limite de SA
		List<ParametroGeneralDTO> parametrosLimiteSA = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("id.codigoParametroGeneral",
				BigDecimal.valueOf(30010D));
				
		double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		
		//Obtiene si existe una agrupacion a Primer riesgo
		AgrupacionCotDTO agrupacion = PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),Sistema.TIPO_PRIMER_RIESGO);	
						
		boolean esPoliza = !(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0);		
		
		if(esPoliza){
			//Obtiene % por forma de Pago		
			CotizacionDTO cotizacionTmp = null;
			if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario() == null || cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0){
				pctPagoFraccionado = CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO);						
				cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_SISTEMA, pctPagoFraccionado, false, false);
			}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){
				cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getValorRecargoPagoFraccionadoUsuario(), false, false);
			}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
				cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_PORCENTAJE, cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario(), false, false);
			}
			
			pctPagoFraccionado = cotizacionTmp.getPorcentajeRecargoPagoFraccionadoUsuario();
			
			//Se obtienen los gastos de expedicion
			if(cotizacionDTO.getValorDerechosUsuario()==null || cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_SISTEMA) == 0){
				derechosPoliza = CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO,true);			
				CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_SISTEMA, derechosPoliza, true, false);
			}else{				
				derechosPoliza = cotizacionDTO.getValorDerechosUsuario();
			}									
		}else{
			pctPagoFraccionado = CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO);
			derechosPoliza = CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO,true);
		}
										
		cotizacionDTO.setDerechosPoliza(derechosPoliza);
		
		//Se obtiene % de IVA
		Double ivaObtenido = null;
		try {
			ivaObtenido = CotizacionDN.getInstancia(null).obtenerPorcentajeIVA(cotizacionDTO.getIdToCotizacion());
		} catch (SystemException e) {
			ivaObtenido = 0.0;
		}
		factorIVA = ivaObtenido / 100D;
		cotizacionDTO.setFactorIVA(factorIVA * 100D);

		//se obtiene correctamente el monto de bonificacion de la comision
		//List<CoberturaCotizacionDTO> coberturas = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());

		for(CoberturaCotizacionDTO cobertura: coberturas){
			valorBonifComision += cobertura.getValorPrimaNeta()* cotizacionDTO.getPorcentajebonifcomision() / 100;
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(cobertura.getIdTcSubramo());
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			ComisionCotizacionDTO comision = null;
			SoporteResumen resumenComision = new SoporteResumen();
			Double sumaAseguradaIncendio = 0D;			

			if (cobertura.getIdTcSubramo().intValue()== 1) {
				limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
						.obtenerSACoberturasBasicasIncendioPorCotizacion(
								cobertura.getId().getIdToCotizacion());				
				if (agrupacion != null) {
					if (cobertura.getNumeroAgrupacion().intValue() == agrupacion
							.getId().getNumeroAgrupacion().intValue()) {
						id.setTipoPorcentajeComision(Sistema.TIPO_PRR);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id.setTipoPorcentajeComision(Sistema.TIPO_RCI);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);	
					}
				}
			} else {
				id.setTipoPorcentajeComision(Sistema.TIPO_RO);
			}
			comision = ComisionCotizacionDN.getInstancia().getPorId(id);		
			valorComision += cobertura.getValorPrimaNeta()* factor.doubleValue() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D;
			SubRamoDTO subRamoDTO = new SubRamoDTO();
			subRamoDTO.setIdTcSubRamo(cobertura.getIdTcSubramo());
			
			subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoDTO);

			resumenComision.setMontoComision(cobertura.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D);
			resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
			resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			resumenComision.setMontoComisionCedida(resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D);
//			resumenComision.setTipoComision(comision.getId().getTipoPorcentajeComision());
//			resumenComision.setSubRamo(cobertura.getIdTcSubramo().intValue());
			resumenComision.setId(comision.getId());
			resumenComisionesTmp.add(resumenComision);
		}
		valorComisionRPF = valorComision * pctPagoFraccionado / 100D;
		// valorBonifComision = PrimaNeta de la cotizacion * porcentajeBonifComision/100		
		cotizacionDTO.setMontoBonificacionComision(valorComision * cotizacionDTO.getPorcentajebonifcomision() / 100D);		
		
		// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
		cotizacionDTO.setMontoRecargoPagoFraccionado(primaNetaCotizacion * pctPagoFraccionado / 100D);
		
		// valorBonifComRecPagoFrac = valorRecargoPagoFrac * porcentajeBonifComision/100
		cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(valorComisionRPF * cotizacionDTO.getPorcentajebonifcomision() / 100D);		

		// valorIVA = (primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
		montoIVA = (primaNetaCotizacion + cotizacionDTO.getMontoRecargoPagoFraccionado() + cotizacionDTO.getDerechosPoliza() - cotizacionDTO.getMontoBonificacionComision() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado()) * factorIVA;
		cotizacionDTO.setMontoIVA(montoIVA);

		//Prime neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion - cotizacionDTO.getMontoBonificacionComision());
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado());
		
		// valorPrimaTotal = primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA		
		cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaCotizacion()
				+ cotizacionDTO.getMontoRecargoPagoFraccionado()
				+ cotizacionDTO.getDerechosPoliza()
				+ montoIVA);
		//set comisiones
		this.ordenarAgruparComisiones(resumenComisionesTmp, resumenComisiones);
	}
	
	public void calcularTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<CoberturaCotizacionDTO> coberturas) throws SystemException{
		BigDecimal factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		double primaNetaCotizacion = 0D;
		double pctPagoFraccionado = 0D;
		double derechosPoliza = 0D;
		double factorIVA = 0D;
		double montoIVA = 0D;
		double valorBonifComision = 0D;
		double valorComision = 0D;
		double valorComisionRPF = 0D;

		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
				&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() > 0
				&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() != Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO) {
			factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			
			MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
			dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			movimientos = MovimientoCotizacionEndosoDN
					.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).listarFiltrado(dto);

			for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
				if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0) {
					primaNetaCotizacion += movimiento.getValorDiferenciaPrimaNeta().doubleValue();
				}
			}			
			cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion);
			primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual() * factor.doubleValue();
			cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
		}else{
			if (cotizacionDTO.getPrimaNetaCotizacion()!= null){
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			}else{
				for(CoberturaCotizacionDTO cobertura:coberturas){
					primaNetaCotizacion +=cobertura.getValorPrimaNeta();
				}
				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion);
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			}
			if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
					cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual();
			}else{
				primaNetaCotizacion = cotizacionDTO.getPrimaNetaAnual() * factor.doubleValue();
			}	
			cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
		}
	
		
		List<SoporteResumen> resumenComisionesTmp = new ArrayList<SoporteResumen>();
		//Prima neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion);

		//Obtiene el limite de SA
		List<ParametroGeneralDTO> parametrosLimiteSA = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("id.codigoParametroGeneral",
				BigDecimal.valueOf(30010D));
				
		double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		
		//Obtiene si existe una agrupacion a Primer riesgo
		AgrupacionCotDTO agrupacion = PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),Sistema.TIPO_PRIMER_RIESGO);	
		
		//Obtiene % por forma de Pago		
		if(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario()==null || cotizacionDTO.getValorRecargoPagoFraccionadoUsuario() == null){		
			pctPagoFraccionado = CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO);						
			CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_SISTEMA, pctPagoFraccionado, false, false);			
		}else{
			pctPagoFraccionado = cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario();
		}
		
		cotizacionDTO.setPorcentajePagoFraccionado(pctPagoFraccionado);
		
		//Se obtienen los gastos de expedicion		
		if(cotizacionDTO.getValorDerechosUsuario()==null){
			derechosPoliza = CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO, false);
			CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_SISTEMA, derechosPoliza, false, false);
		}else{
			derechosPoliza = cotizacionDTO.getValorDerechosUsuario();
		}
										
		cotizacionDTO.setDerechosPoliza(derechosPoliza);
		
		//Se obtiene % de IVA
		Double ivaObtenido= CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), "");
		
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		factorIVA = ivaObtenido/100D;
		cotizacionDTO.setFactorIVA(factorIVA * 100D);

		//se obtiene correctamente el monto de bonificacion de la comision
		
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
			coberturas= new ArrayList<CoberturaCotizacionDTO>();
			for(MovimientoCotizacionEndosoDTO movimiento: movimientos){
				if(movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() > 0){
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(movimiento.getIdToCotizacion());
					id.setNumeroInciso(movimiento.getNumeroInciso());
					id.setIdToSeccion(movimiento.getIdToSeccion());
					id.setIdToCobertura(movimiento.getIdToCobertura());
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO = coberturaCotizacionDN.getPorId(coberturaCotizacionDTO);
					if(coberturaCotizacionDTO != null){
						coberturaCotizacionDTO.setValorPrimaNeta(movimiento.getValorDiferenciaPrimaNeta().doubleValue());
						coberturas.add(coberturaCotizacionDTO);						
					}
				}
			}
		}else{
			coberturas = coberturaCotizacionDN.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		}
		for(CoberturaCotizacionDTO cobertura: coberturas){
			valorBonifComision += cobertura.getValorPrimaNeta()* cotizacionDTO.getPorcentajebonifcomision() / 100;
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(cobertura.getIdTcSubramo());
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

			ComisionCotizacionDTO comision = null;
			SoporteResumen resumenComision = new SoporteResumen();
			Double sumaAseguradaIncendio = 0D;
			
			if (cobertura.getIdTcSubramo().intValue()== 1) {
				limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
						.obtenerSACoberturasBasicasIncendioPorCotizacion(
								cobertura.getId().getIdToCotizacion());					
				if (agrupacion != null) {
					if (cobertura.getNumeroAgrupacion().intValue() == agrupacion
							.getId().getNumeroAgrupacion().intValue()) {
						id.setTipoPorcentajeComision(Sistema.TIPO_PRR);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					}
				} else {
					if (sumaAseguradaIncendio < limiteSA) {
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					} else if (sumaAseguradaIncendio >= limiteSA) {
						id.setTipoPorcentajeComision(Sistema.TIPO_RCI);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);	
					}
				}
			} else {
				id.setTipoPorcentajeComision(Sistema.TIPO_RO);
			}
			comision = ComisionCotizacionDN.getInstancia().getPorId(id);		
			valorComision += cobertura.getValorPrimaNeta()* factor.doubleValue() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D;
			SubRamoDTO subRamoDTO = new SubRamoDTO();
			subRamoDTO.setIdTcSubRamo(cobertura.getIdTcSubramo());
			
			subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoDTO);

			resumenComision.setMontoComision(cobertura.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D);
			resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
			resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			resumenComision.setMontoComisionCedida(resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D);
//			resumenComision.setTipoComision(comision.getId().getTipoPorcentajeComision());
//			resumenComision.setSubRamo(cobertura.getIdTcSubramo().intValue());
			resumenComision.setId(comision.getId());
			resumenComisionesTmp.add(resumenComision);
		}
		valorComisionRPF = valorComision * pctPagoFraccionado / 100D;
		// valorBonifComision = PrimaNeta de la cotizacion * porcentajeBonifComision/100		
		cotizacionDTO.setMontoBonificacionComision(valorComision * cotizacionDTO.getPorcentajebonifcomision() / 100D);		
		
		// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getPrimaNetaCotizacion() * pctPagoFraccionado / 100D);
		
		// valorBonifComRecPagoFrac = valorRecargoPagoFrac * porcentajeBonifComision/100
		cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(valorComisionRPF * cotizacionDTO.getPorcentajebonifcomision() / 100D);		

		// valorIVA = (primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
		montoIVA = (cotizacionDTO.getPrimaNetaCotizacion() + cotizacionDTO.getMontoRecargoPagoFraccionado() + cotizacionDTO.getDerechosPoliza() - cotizacionDTO.getMontoBonificacionComision() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado()) * factorIVA;
		cotizacionDTO.setMontoIVA(montoIVA);

		//Prime neta de la cotizacion
		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion - cotizacionDTO.getMontoBonificacionComision());
		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado());
		// valorPrimaTotal = primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA		
		cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaCotizacion()
				+ cotizacionDTO.getMontoRecargoPagoFraccionado()
				+ cotizacionDTO.getDerechosPoliza()
				+ montoIVA);
		//set comisiones
	}
	
	private void ordenarAgruparComisiones(List<SoporteResumen> resumenComisiones, List<SoporteResumen> resumenComisionesAgrupada){
		Double totalComisionPRR = 0D;
		Double totalComisionRO = 0D;
		Double totalComisionRCI = 0D;
		Double totalComisionCedidaPRR = 0D;
		Double totalComisionCedidaRO = 0D;
		Double totalComisionCedidaRCI = 0D;	
		Double porcentajeComisionPRR = 0D;
		Double porcentajeComisionRO = 0D;
		Double porcentajeComisionRCI = 0D;
		ComisionCotizacionId idPRR = null;
		ComisionCotizacionId idRO = null;
		ComisionCotizacionId idRCI = null;
		String descripcionRamoIncendio= null;
		boolean tieneIncendio = false;
		List<Integer> subRamos = new ArrayList<Integer>();
		for(int i=0; i < resumenComisiones.size(); i++){
			SoporteResumen soporte = resumenComisiones.get(i);
			if(soporte.getId().getIdTcSubramo().intValue() == 1){
				tieneIncendio = true;
				if (idRO == null) {
					idRO = soporte.getId();
				}	
				descripcionRamoIncendio = soporte.getDescripcionRamo();
				if(soporte.getId().getTipoPorcentajeComision().intValue() == Sistema.TIPO_PRR){
					totalComisionPRR = totalComisionPRR + soporte.getMontoComision(); 
					totalComisionCedidaPRR = totalComisionCedidaPRR + soporte.getMontoComisionCedida();
					porcentajeComisionPRR = soporte.getPorcentajeComision();				
				}else if(soporte.getId().getTipoPorcentajeComision().intValue() == Sistema.TIPO_RO){
					totalComisionRO = totalComisionRO + soporte.getMontoComision();				
					totalComisionCedidaRO = totalComisionCedidaRO + soporte.getMontoComisionCedida();
					porcentajeComisionRO = soporte.getPorcentajeComision();				
				}else if(soporte.getId().getTipoPorcentajeComision().intValue() == Sistema.TIPO_RCI){
					totalComisionRCI = totalComisionRCI + soporte.getMontoComision(); 			
					totalComisionCedidaRCI = totalComisionCedidaRCI + soporte.getMontoComisionCedida();
					porcentajeComisionRCI = soporte.getPorcentajeComision();
				
				}
			}else{
				if(subRamos.size()== 0){
					subRamos.add(soporte.getId().getIdTcSubramo().intValue());
				}else{
					if (!subRamos.contains(soporte.getId().getIdTcSubramo().intValue())){
						subRamos.add(soporte.getId().getIdTcSubramo().intValue());
					}
				}
				
			}
		}
	
		if (tieneIncendio){
			SoporteResumen incendioRO = new SoporteResumen();
			incendioRO.setMontoComision(totalComisionRO);
			incendioRO.setMontoComisionCedida(totalComisionCedidaRO);
			incendioRO.setDescripcionRamo(descripcionRamoIncendio +" RO");
			incendioRO.setPorcentajeComision(porcentajeComisionRO);
			idRO.setTipoPorcentajeComision(Sistema.TIPO_RO);
			incendioRO.setId(idRO);
			resumenComisionesAgrupada.add(incendioRO);
			
			SoporteResumen incendioPRR = new SoporteResumen();
			incendioPRR.setMontoComision(totalComisionPRR);
			incendioPRR.setMontoComisionCedida(totalComisionCedidaPRR);
			incendioPRR.setDescripcionRamo(descripcionRamoIncendio +" PRR");
			incendioPRR.setPorcentajeComision(porcentajeComisionPRR);
			idPRR = new ComisionCotizacionId();
			idPRR.setIdTcSubramo(idRO.getIdTcSubramo());
			idPRR.setIdToCotizacion(idRO.getIdToCotizacion());
			idPRR.setTipoPorcentajeComision(Sistema.TIPO_PRR);
			incendioPRR.setId(idPRR);
			resumenComisionesAgrupada.add(incendioPRR);

			SoporteResumen incendioRCI = new SoporteResumen();
			incendioRCI.setMontoComision(totalComisionRCI);
			incendioRCI.setMontoComisionCedida(totalComisionCedidaRCI);
			incendioRCI.setDescripcionRamo(descripcionRamoIncendio +" RCI");
			incendioRCI.setPorcentajeComision(porcentajeComisionRCI);
			idRCI = new ComisionCotizacionId();
			idRCI.setIdTcSubramo(idRO.getIdTcSubramo());
			idRCI.setIdToCotizacion(idRO.getIdToCotizacion());
			idRCI.setTipoPorcentajeComision(Sistema.TIPO_RCI);	
			incendioRCI.setId(idRCI);
			resumenComisionesAgrupada.add(incendioRCI);
			
		}
	
		for(Integer sub: subRamos){
			if(sub.intValue() != 1){
				SoporteResumen soporte = new SoporteResumen();
				Double totalComision = 0D;
				Double totalComisionCedida = 0D;
				String descripcionRamo = null;
				Double porcentajeComision = 0D;
				ComisionCotizacionId idComis = null;
				for(SoporteResumen resumen: resumenComisiones){
					if(sub.intValue() == resumen.getId().getIdTcSubramo().intValue()){
						totalComision = totalComision + resumen.getMontoComision();
						totalComisionCedida = totalComisionCedida + resumen.getMontoComisionCedida();
						descripcionRamo = resumen.getDescripcionRamo();
						porcentajeComision = resumen.getPorcentajeComision();
						idComis = resumen.getId();
					}
				}
				soporte.setDescripcionRamo(descripcionRamo);
				soporte.setMontoComision(totalComision);
				soporte.setMontoComisionCedida(totalComisionCedida);
				soporte.setPorcentajeComision(porcentajeComision);
				soporte.setId(idComis);
				resumenComisionesAgrupada.add(soporte);
				
			}
		}		
	}
	
	public void setTotalesResumenCambioFormaPago(CotizacionDTO cotizacionEndosoActual) throws SystemException{
		//Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del �ltimo recibo pagado o el primero emitido
		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(null).getUltimoEndoso(cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada());
		Integer tipoEndoso = UtileriasWeb.calculaTipoEndoso(cotizacionEndosoActual);
		EndosoIDTO endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(ultimoEndoso.getId().getIdToPoliza().toString() + "|" + 
				                                                            ultimoEndoso.getId().getNumeroEndoso(), 
				                                                            cotizacionEndosoActual.getCodigoUsuarioModificacion(),
				                                                            cotizacionEndosoActual.getFechaInicioVigencia(),
				                                                            tipoEndoso);
		cotizacionEndosoActual.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
		CotizacionDN.getInstancia(cotizacionEndosoActual.getCodigoUsuarioModificacion()).modificar(cotizacionEndosoActual);
		
		//Se inician los c�lculos de los montos a emitir
		BigDecimal idToPoliza = cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada();
		EndosoDN endosoDN = EndosoDN.getInstancia(null);		
		List<EndosoDTO> endosos = endosoDN.listarEndososPorPoliza(idToPoliza, false);
		double diasEndosoActual = UtileriasWeb.obtenerDiasEntreFechas(cotizacionEndosoActual.getFechaInicioVigencia(), 
																	  cotizacionEndosoActual.getFechaFinVigencia());
		
		double primaNetaEndoso = CotizacionDN.getInstancia(null).getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		primaNetaEndoso *= diasEndosoActual/365;
		double recargoPagoFraccionadoEndososAnteriores = 0;
		for(EndosoDTO endoso : endosos){
			//Se obtiene el factor de vigencia
			double diasEndoso = UtileriasWeb.obtenerDiasEntreFechas(endoso.getFechaInicioVigencia(), 
																	endoso.getFechaFinVigencia());			
			double factor = diasEndosoActual/diasEndoso;
									
			factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar m�s RPF de los emitidos				
			
			//Se acumula el recargo por pago fraccionado de los endosos anteriores considerando la bonificaci�n de comisi�n			
			recargoPagoFraccionadoEndososAnteriores += (endoso.getValorRecargoPagoFrac()-endoso.getValorBonifComisionRPF()) * factor;

			//Se termina el proceso cuando se encuentra el �ltimo endoso de CFP emitido.
			if(endoso.getClaveTipoEndoso().compareTo(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0) {
				break;
			}
		}
		
		//Obtiene % por forma de Pago							
		BigDecimal pctPagoFraccionado = BigDecimal.valueOf(CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionEndosoActual));
		
		//Se calcula el monto del recargo por pago fraccionado a emitir 
		double montoRecargoPagoFraccionado = primaNetaEndoso * pctPagoFraccionado.doubleValue() / 100;
		montoRecargoPagoFraccionado -= recargoPagoFraccionadoEndososAnteriores;
								
		//Se obtiene % de IVA
		Double ivaObtenido = CotizacionDN.getInstancia(null).obtenerPorcentajeIVA(cotizacionEndosoActual.getIdToCotizacion());
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		
		BigDecimal factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
		cotizacionEndosoActual.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());
		cotizacionEndosoActual.setPrimaNetaCotizacion(0D);
		cotizacionEndosoActual.setMontoRecargoPagoFraccionado(montoRecargoPagoFraccionado);				
		cotizacionEndosoActual.setDerechosPoliza(0D);				
		cotizacionEndosoActual.setMontoIVA(cotizacionEndosoActual.getMontoRecargoPagoFraccionado() * factorIVA.doubleValue());
		cotizacionEndosoActual.setPrimaNetaTotal(cotizacionEndosoActual.getMontoRecargoPagoFraccionado() + cotizacionEndosoActual.getMontoIVA());																				
	}
	
	public void setTotalesResumenCambioFormaPagoImpresion(CotizacionDTO cotizacionEndosoActual) throws SystemException{
		EndosoDN endosoDN = EndosoDN.getInstancia(null);	
		List<EndosoDTO> endosos = null;
		
		//Se obtienen los endosos de la p�liza en orden descendente
		BigDecimal idToPoliza = cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada();		
		endosos = endosoDN.listarEndososPorPoliza(idToPoliza, false);
		
		//Se busca un endoso emitido con la cotizaci�n dada como par�metro
		EndosoDTO endosoEmitido = endosoDN.buscarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		
		//Si no se ha emitido el endoso
		if(endosoEmitido == null){
			//Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del �ltimo recibo pagado o el primero emitido
			EndosoDTO ultimoEndoso = endosoDN.getUltimoEndoso(cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada());
			Integer tipoEndoso = UtileriasWeb.calculaTipoEndoso(cotizacionEndosoActual);
			EndosoIDTO endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(ultimoEndoso.getId().getIdToPoliza().toString() + "|" + 
					                                                            ultimoEndoso.getId().getNumeroEndoso(), 
					                                                            cotizacionEndosoActual.getCodigoUsuarioModificacion(),
					                                                            cotizacionEndosoActual.getFechaInicioVigencia(),
					                                                            tipoEndoso);
			cotizacionEndosoActual.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
			CotizacionDN.getInstancia(cotizacionEndosoActual.getCodigoUsuarioModificacion()).modificar(cotizacionEndosoActual);								
		}else{//Si el endoso ya fue emitido
			//Se eliminan de la lista el endoso de CFP emitido y todos los endosos posteriores a este.
			//(se deja la lista, tal como si no se hubiera emitido el endoso de CFP)
			for(EndosoDTO endoso : endosos){
				endosos.remove(endoso);
				if(endoso.getIdToCotizacion().compareTo(cotizacionEndosoActual.getIdToCotizacion()) == 0) {
					break;
				}
			}
		}

		double diasEndosoActual = UtileriasWeb.obtenerDiasEntreFechas(cotizacionEndosoActual.getFechaInicioVigencia(), 
																	  cotizacionEndosoActual.getFechaFinVigencia());
		
		double primaNetaEndoso = CotizacionDN.getInstancia(null).getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		primaNetaEndoso *= diasEndosoActual/365;
		double recargoPagoFraccionadoEndososAnteriores = 0;
		for(EndosoDTO endoso : endosos){			
			//Se obtiene el factor de vigencia
			double diasEndoso = UtileriasWeb.obtenerDiasEntreFechas(endoso.getFechaInicioVigencia(), 
																	endoso.getFechaFinVigencia());			
			double factor = diasEndosoActual/diasEndoso;
			
			factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar m�s RPF de los emitidos				
			
			//Se acumula el recargo por pago fraccionado de los endosos anteriores considerando la bonificaci�n de comisi�n			
			recargoPagoFraccionadoEndososAnteriores += (endoso.getValorRecargoPagoFrac()-endoso.getValorBonifComisionRPF()) * factor;

			//Se termina el proceso cuando se encuentra el �ltimo endoso de CFP emitido.
			if(endoso.getClaveTipoEndoso().compareTo(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0) {
				break;
			}
		}
		
		//Obtiene % por forma de Pago					
		BigDecimal pctPagoFraccionado = BigDecimal.valueOf(CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionEndosoActual));
		
		//Se calcula el monto del recargo por pago fraccionado a emitir/emitido 
		double montoRecargoPagoFraccionado = 0;
		if(endosoEmitido == null){//Si no se ha emitido el endoso
			montoRecargoPagoFraccionado = primaNetaEndoso * pctPagoFraccionado.doubleValue() / 100;
		}else{//Si el endoso ya fue emitido
			montoRecargoPagoFraccionado = endosoEmitido.getValorRecargoPagoFrac();
		}
		
		montoRecargoPagoFraccionado -= recargoPagoFraccionadoEndososAnteriores;						
		
		//Se obtiene % de IVA
		Double ivaObtenido = CotizacionDN.getInstancia(null).obtenerPorcentajeIVA(cotizacionEndosoActual.getIdToCotizacion());
		if(ivaObtenido==null){
		    throw new SystemException("No se pudo obtener el valor para el IVA.");
		}
		
		BigDecimal factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
		cotizacionEndosoActual.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());
		cotizacionEndosoActual.setPrimaNetaCotizacion(0D);
		cotizacionEndosoActual.setMontoRecargoPagoFraccionado(montoRecargoPagoFraccionado);				
		cotizacionEndosoActual.setDerechosPoliza(0D);				
		cotizacionEndosoActual.setMontoIVA(cotizacionEndosoActual.getMontoRecargoPagoFraccionado() * factorIVA.doubleValue());
		cotizacionEndosoActual.setPrimaNetaTotal(cotizacionEndosoActual.getMontoRecargoPagoFraccionado() + cotizacionEndosoActual.getMontoIVA());																				
	}
}
