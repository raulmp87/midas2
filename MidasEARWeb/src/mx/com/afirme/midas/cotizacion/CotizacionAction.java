package mx.com.afirme.midas.cotizacion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDN;
import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDN;
import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDTO;
import mx.com.afirme.midas.catalogos.subgiro.SubGiroDN;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.cliente.ClienteForm;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.igualacion.IgualacionPrimaNetaForm;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionForm;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDTO;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDN;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDTO;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotForm;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.validacion.CotizacionReaseguroFacultativoForm;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.persona.PersonaDN;
import mx.com.afirme.midas.persona.PersonaDTO;
import mx.com.afirme.midas.persona.PersonaForm;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.SeguimientoRenovacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.ExclusionCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TreeLoader;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.gestionPendientes.GestorPendientesDanos;
import mx.com.afirme.midas.sistema.gestionPendientes.Pendiente;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudAction;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.lang.StringUtils;

public class CotizacionAction extends MidasMappingDispatchAction {

	/**
	 * Actualiza una cobertura con los datos ingresados o modificados por el
	 * usuario
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public void guardarCoberturas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN
				.getInstancia();
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		String tipoCoaseguroS = request.getParameter("habilitaSumaAsegurada");
		String idToCoberturaCotS = request.getParameter("gr_id");
		String contratadaS = request.getParameter("contratada");
		String sumaAseguradaS = request.getParameter("valorSumaAsegurada");
		Double valorSumaAsegurada = null;

		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();

		coberturaCotizacionDTO.setId(new CoberturaCotizacionId());
		coberturaCotizacionDTO.getId().setIdToCobertura(
				new BigDecimal(idToCoberturaCotS));
		coberturaCotizacionDTO.getId().setIdToCotizacion(
				new BigDecimal(idToCotizacionS));
		coberturaCotizacionDTO.getId().setNumeroInciso(
				new BigDecimal(numeroIncisoS));
		coberturaCotizacionDTO.getId().setIdToSeccion(
				new BigDecimal(idToSeccionS));

		try {
			if (sumaAseguradaS != null) {
				if (tipoCoaseguroS.equals("1")) {
					valorSumaAsegurada = new Double(sumaAseguradaS);
				} else {
					valorSumaAsegurada = new Double(0);

				}
			}
			coberturaCotizacionDTO.setValorSumaAsegurada(valorSumaAsegurada);
			coberturaCotizacionDTO.setClaveContrato(new Short(contratadaS));
			coberturaCotizacionDN.actualizaCoberturaCotizacionContratada(
					coberturaCotizacionDTO, tipoCoaseguroS);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"updated\" sid=\""
				+ request.getParameter("gr_id") + "\" " + "tid=\""
				+ request.getParameter("gr_id") + "\" " + "idToCotizacion=\""
				+ coberturaCotizacionDTO.getId().getIdToCotizacion().intValue()
				+ "\" " + "extra=\""
				+ coberturaCotizacionDTO.getId().getIdToCotizacion().intValue()
				+ "_"
				+ coberturaCotizacionDTO.getId().getNumeroInciso().intValue()
				+ "_"
				+ coberturaCotizacionDTO.getId().getIdToSeccion().intValue()
				+ "\"/></data>");
		pw.flush();
		pw.close();
	}

	/**
	 * Method mostrarCoberturasPorSeccionCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarCoberturasPorSeccionCotizacion(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		String fecha = new String();
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		SeccionDTO seccionDTO = new SeccionDTO();
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionForm cotizacionForm = (CotizacionForm) form;

		SeccionDN seccionDN = SeccionDN.getInstancia();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		if (numeroIncisoS.trim().length() < 1
				|| idToSeccionS.trim().length() < 1) {
			return mapping.findForward(reglaNavegacion);
		}
		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		try {
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
			seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			cotizacionDTO.setIdToCotizacion(idToCotizacion);
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			fecha = dateFormat.format(cotizacionDTO.getFechaCreacion());
			request.setAttribute("tituloCoberturas", seccionDTO
					.getNombreComercial());
			request.setAttribute("idToCotizacion", UtileriasWeb
					.llenarIzquierda(cotizacionDTO.getIdToCotizacion()
							.toString(), "0", 8));
			request.setAttribute("fecha", fecha);
			cotizacionForm.setFechaCreacion(fecha);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * enviarEmision
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward enviarEmision(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String navegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("id");
		try {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(idToCotizacion));
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);

			cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_LISTA_EMITIR);
			cotizacionDN.modificar(cotizacionDTO);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(navegacion);
	}

	/**
	 * guardarCoberturas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward separarInciso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String navegacion = Sistema.EXITOSO;
		String idToCotizacion;
		String idInciso;
		idToCotizacion = request.getParameter("idToCotizacion");
		idInciso = request.getParameter("idInciso");
		if (idToCotizacion != null && idInciso != null) {
			try {
				CotizacionDTO cotizacionDTO = new CotizacionDTO();
				cotizacionDTO.setIdToCotizacion(UtileriasWeb
						.regresaBigDecimal(idToCotizacion));
				/*
				 * IncisoCotizacionDN incisoCotizacionDN =
				 * IncisoCotizacionDN.getInstancia();
				 * incisoCotizacionDN.separarInciso(cotizacionDTO,
				 * UtileriasWeb.regresaBigDecimal(idInciso));
				 */
				BigDecimal idtoCotizacionNueva = CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request))
						.separarInciso(
								UtileriasWeb.regresaBigDecimal(idToCotizacion),
								UtileriasWeb.regresaBigDecimal(idInciso));
				ActionForward forward = new ActionForward(mapping
						.findForward("detalleCotizacion"));
				String url = forward.getPath() + "?id=" + idtoCotizacionNueva;
				forward.setPath(url);
				return forward;
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return mapping.findForward(navegacion);
	}

	/**
	 * A partir de un <code>idToCotizacion</code> obtiene un objeto
	 * <code>CotizacionDTO</code> y crea una copia en la BD.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward copiarCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String navegacion = Sistema.EXITOSO;
		String id = request.getParameter("idToCotizacion");
		if (id != null) {
			CotizacionDTO cotizacion;
			try {
				cotizacion = CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
						UtileriasWeb.regresaBigDecimal(id));
				CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request))
						.copiarCotizacion(cotizacion);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return mapping.findForward(navegacion);
	}

	/**
	 * Para un <code>idToCotizacion</code>, <code>numeroInciso</code> y
	 * <code>idToSeccion</code> devuelve una lista de coberturas, en pantalla,
	 * utilizando ajax
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void mostrarCoberturasPorSeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		List<CoberturaDTO> listaCoberturas = new ArrayList<CoberturaDTO>();
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));

		if (numeroIncisoS.trim().length() < 1
				|| idToSeccionS.trim().length() < 1) {
			return;
		}
		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();

		String json = new String();
		try {
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
			seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);

			coberturas = cotizacionDN
					.getCoberturasPorSeccion(seccionCotizacionDTO);
			if (coberturas != null) {
				for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas) {
					coberturaDTO = new CoberturaDTO();
					coberturaDTO.setIdToCobertura(coberturaCotizacionDTO
							.getId().getIdToCobertura());
					coberturaDTO = coberturaDN.getPorId(coberturaDTO);
					listaCoberturas.add(coberturaDTO);
				}
			}
			json = getJsonTablaCobertura(coberturas, listaCoberturas);

			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	private void poblarAgentes(CotizacionForm cotizacionForm, Usuario usuario)
			throws SystemException {
		mx.com.afirme.midas.interfaz.agente.AgenteDN agenteDN = mx.com.afirme.midas.interfaz.agente.AgenteDN
				.getInstancia();
		List<AgenteDTO> agentes = null;
		try {
			agentes = AgenteEspecialDN.getInstancia().poblarAgenteEspecial(usuario, cotizacionForm);
			if (agentes == null || agentes.size() == 0)
				agentes = agenteDN.getAgentesPorUsuario(usuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (agentes != null && !agentes.isEmpty()) {
			cotizacionForm.setAgentes(agentes);
		} else {
			cotizacionForm.setAgentes(new ArrayList<AgenteDTO>());
		}
	}

	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		ActionForward actionForward = new ActionForward(mapping
				.findForward(Sistema.EXITOSO));
		String idToCotizacion = "";
		BigDecimal idToPoliza = BigDecimal.ZERO;
		if (request.getParameter("id") != null) {
			idToCotizacion = request.getParameter("id");
		} else if (request.getAttribute("id") != null) {
			idToCotizacion = request.getAttribute("id").toString();
		}
		String editaDatoGeneral = request.getParameter("editaDatoGeneral");
		String aceptoRenovacion = request.getParameter("aceptoRenovacion");
		if (!UtileriasWeb.esCadenaVacia(editaDatoGeneral))
			cotizacionForm.setEditaDatoGeneral(editaDatoGeneral);
		String esCotizacion = request.getParameter("esCotizacion");

		String consultaEndoso = request.getParameter("consultaEndoso");
		if (!UtileriasWeb.esCadenaVacia(consultaEndoso)) {
			request.setAttribute("consultaEndoso", consultaEndoso);
		}
		try {
			this.poblarAgentes(cotizacionForm, (Usuario) UtileriasWeb
					.obtenValorSessionScope(request,
							Sistema.USUARIO_ACCESO_MIDAS));
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm, request);
			if (!UtileriasWeb.esCadenaVacia(aceptoRenovacion)
					&& aceptoRenovacion.equals("1")) {
				RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
						.getInstancia();
				RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN
						.buscarDetalleRenovacionPoliza(cotizacionDTO
								.getSolicitudDTO().getIdToPolizaAnterior());
				if (renovacionPolizaDTO != null) {
					renovacionPolizaDN
							.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_ACEPTA_CON_SINIESTRO);
					renovacionPolizaDN
							.setDetalleNotificacion("Cotizaci�n Aceptada con Siniestro por el Usuario: "
									+ UtileriasWeb
											.obtieneNombreUsuario(request));
					renovacionPolizaDN
							.agregarDetalleSeguimiento(renovacionPolizaDTO
									.getPolizaDTO());
				}
			}
			if (!UtileriasWeb.esObjetoNulo(esCotizacion)) {
				cotizacionForm.setEsCotizacion(esCotizacion);
				cotizacionForm.setIdToCotizacionFormateada(UtileriasWeb
						.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
								"midas.cotizacion.prefijo")
						+ UtileriasWeb.llenarIzquierda(idToCotizacion, "0", 8));
				if (cotizacionDTO.getClaveEstatus().equals(
						Sistema.ESTATUS_COT_ASIGNADA)) {
					cotizacionDTO
							.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
					cotizacionSN.modificar(cotizacionDTO);
				}
			} else {
				if (cotizacionDTO.getClaveEstatus().equals(
						Sistema.ESTATUS_ODT_ASIGNADA)) {
					cotizacionDTO
							.setClaveEstatus(Sistema.ESTATUS_ODT_ENPROCESO);
					cotizacionSN.modificar(cotizacionDTO);
				}
				cotizacionForm.setEsCotizacion("0");
				if (cotizacionDTO.getClaveEstatus().equals(
						Sistema.ESTATUS_COT_ASIGNADA)) {
					cotizacionDTO
							.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
					cotizacionSN.modificar(cotizacionDTO);
				}
			}
			request.setAttribute("idToCotizacion", idToCotizacion);

			// Bloque usado para definir si el usuario puede o no visualizar el
			// tag de inisos
			if (cotizacionDTO.getTipoPolizaDTO() != null) {
				cotizacionForm
						.setEditaIncisos((cotizacionDTO.getTipoPolizaDTO()
								.getIdToTipoPoliza() != null) ? "true"
								: "false");
				cotizacionForm
						.setPermiteCambiarPoliza((cotizacionDTO
								.getTipoPolizaDTO().getIdToTipoPoliza() != null) ? "false"
								: "true");
			} else {
				cotizacionForm.setEditaIncisos("false");
				cotizacionForm.setPermiteCambiarPoliza("true");
			}
			// Bloque usado para definir si el usuario puede o no visualizar el
			// tag de primer riesgo/LUC
			int cantidadIncisos = IncisoCotizacionDN.getInstancia()
					.obtenerCantidadIncisosPorCotizacion(
							cotizacionDTO.getIdToCotizacion());
			cotizacionForm.setEditaPrimerRiesgo((cantidadIncisos > 0) ? "true"
					: "false");
			// if (listaIncisos != null)
			// cotizacionForm.setEditaPrimerRiesgo((listaIncisos.size() >
			// 0)?"true":"false");
			// else
			// cotizacionForm.setEditaIncisos("false");

			// Bloque usado para definir qu� tipo de edici�n realizar� el
			// usuario en las comisiones de la cotizacion,
			// Puede editar el porcentaje de la comision y la autorizacion
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);
			for (Rol rol : usuario.getRoles()) {
				if (rol.getDescripcion().equals(
						Sistema.ROL_SUPERVISOR_SUSCRIPTOR)
						|| rol.getDescripcion().equals(
								Sistema.ROL_DIRECTOR_TECNICO)) {
					cotizacionForm.setAutorizacionComision("autorizacion");
					request
							.setAttribute("autorizacionComision",
									"autorizacion");
					request.setAttribute("autorizarTexAdicional",
							"autorizarTexAdicional");
					break;
				}
				// else if
				// (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_COT)){
				// break;
				// }
			}
			cotizacionForm.setEdicionComision("comision");
			request.setAttribute("edicionComision", "comision");

			if (cotizacionDTO.getClaveEstatus().equals(
					Sistema.ESTATUS_COT_EMITIDA)) {
				try {
					List<PolizaDTO> polizas = PolizaDN.getInstancia()
							.buscarPolizasPorPropiedad(
									"cotizacionDTO.idToCotizacion",
									UtileriasWeb
											.regresaBigDecimal(idToCotizacion));
					if (polizas != null && !polizas.isEmpty()) {
						idToPoliza = polizas.get(0).getIdToPoliza();
					}
				} catch (SystemException e1) {
				}
				if (idToPoliza.compareTo(BigDecimal.ZERO) == 1) {
					List<ReciboDTO> recibosPoliza = ReciboDN.getInstancia(
							usuario.getNombreUsuario()).consultaRecibos(
							idToPoliza);
					if (recibosPoliza != null) {
						request.setAttribute("presentaRecibos", "1");
						cotizacionForm.setIdToPoliza(idToPoliza.toString());
					}
				}
			}
			if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
					&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
							.intValue() > 0) {
				request.setAttribute("esEndoso", "true");
				BigDecimal idToPolizaEndosada = cotizacionDTO.getSolicitudDTO()
						.getIdToPolizaEndosada();
				PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(
						idToPolizaEndosada);
				request.setAttribute("numeroPoliza", UtileriasWeb
						.getNumeroPoliza(polizaDTO));
				request.setAttribute("fechaEmision", polizaDTO
						.getFechaCreacion());
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION) {
					cotizacionForm.setEditaDatoGeneral("true");
					cotizacionForm.setDeshabilitaCambioFP("true");
				} else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO) {
					cotizacionForm.setClaveTipoEndoso(cotizacionDTO
							.getSolicitudDTO().getClaveTipoEndoso().toString());
					cotizacionForm.setEditaIncisos("false");
					cotizacionForm.setEditaDatoGeneral("true");
					cotizacionForm.setPermiteCambiarPoliza("false");
					cotizacionForm.setEditaPrimerRiesgo("false");
				}
			} else {
				cotizacionForm.setClaveTipoEndoso("0");
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return actionForward;
	}

	/**
	 * Method dummy
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void dummy(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		// Se obtiene la iformacion de los diversos ramos de la ODT/COT
		// Se obtiene a traves de los ramos asociados al tipo de poliza de la
		// cotizacion

		// BigDecimal cotizacionId=
		// UtileriasWeb.regresaBigDecimal(request.getParameter("cotizacionId"))
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(new BigDecimal("1"));

		try {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			// se obtienen la Informaci�n de Ramo-Inciso
			// cotizacionDN.getDatosRamoInciso(cotizacionDTO);
			// se iteran los objetos obtenidos

			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<datosinciso><![CDATA[");
			buffer
					.append("<table><tr><td class=\"titulo\" colspan=\"4\">Aqui los datos obtenidos</td></tr></table>");
			buffer.append("]]></datosinciso>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	private String abrirItem(String texto, String id) {
		String item = new String();
		item += "<item text=\"";
		item += texto.replaceAll("\"", "&#34;");
		item += "\" id=\"";
		item += id;
		item += "\">\n";
		return item;
	}

	private String cerrarItem() {
		return "</item>\n";
	}

	/**
	 * Method listarOrdenesTrabajoFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarOrdenesTrabajoFiltrado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		try {
			Usuario usuario = getUsuarioMidas(request);
			HttpSession session = request.getSession();
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			if (session.getAttribute("mensaje") != null) {
				cotizacionForm.setMensaje((String) session
						.getAttribute("mensaje"));
				session.removeAttribute("mensaje");
				cotizacionForm.setTipoMensaje((String) session
						.getAttribute("tipoMensaje"));
				session.removeAttribute("tipoMensaje");
			}
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			poblarDTOOrdenTrabajo(cotizacionForm, cotizacionDTO);

			// cotizacionDTO.setClaveEstatus(Short.valueOf(claveEstatus));
			Long totalRegistros = OrdenTrabajoDN.getInstancia(
					usuario.getNombreUsuario()).obtenerTotalFiltrado(
					cotizacionDTO, usuario);

			cotizacionForm.setTotalRegistros(totalRegistros.toString());

			return listarFiltradoPaginado(mapping, cotizacionForm, request,
					response);

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarFiltradoPaginado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		try {
			Usuario usuario = getUsuarioMidas(request);
			if (!listaEnCache(form, request)) {
				CotizacionForm cotizacionForm = (CotizacionForm) form;
				CotizacionDTO cotizacionDTO = new CotizacionDTO();
				poblarDTOOrdenTrabajo(cotizacionForm, cotizacionDTO);

				cotizacionDTO.setPrimerRegistroACargar(cotizacionForm
						.getPrimerRegistroACargar());
				cotizacionDTO.setNumeroMaximoRegistrosACargar(cotizacionForm
						.getNumeroMaximoRegistrosACargar());
				cotizacionForm.setListaPaginada(
						OrdenTrabajoDN.getInstancia(usuario.getNombreUsuario())
								.listarOrdenesDeTrabajoFiltrado(cotizacionDTO,
										usuario), request);

			}

			request.setAttribute("listaOrdenDeTrabajo", obtieneListaAMostrar(
					form, request));

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTOOrdenTrabajo(CotizacionForm cotizacionForm,
			CotizacionDTO cotizacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {

		cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getNombreEmpresaAsegurado()))
			cotizacionDTO.setNombreAsegurado(cotizacionForm
					.getNombreEmpresaAsegurado());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToCotizacion()))
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveEstatus()))
			cotizacionDTO.setClaveEstatus(UtileriasWeb
					.regresaShort(cotizacionForm.getClaveEstatus()));

		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreSolicitante()))
			cotizacionDTO.getSolicitudDTO().setNombrePersona(
					cotizacionForm.getNombreSolicitante());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getApellidoPaternoSolicitante()))
			cotizacionDTO.getSolicitudDTO().setApellidoPaterno(
					cotizacionForm.getApellidoPaternoSolicitante());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getApellidoMaternoSolicitante()))
			cotizacionDTO.getSolicitudDTO().setApellidoMaterno(
					cotizacionForm.getApellidoMaternoSolicitante());
	}

	/**
	 * Method cargar OrdenTrabajo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward cargarArbolOrdenTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String cotizacion = "";
		String inciso = "";
		String seccion = "";
		String cobertura = "";
		String separador = "_";

		StringBuffer buffer = new StringBuffer();
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			SoporteEstructuraCotizacion estructuraCotizacion = new SoporteEstructuraCotizacion(
					UtileriasWeb.regresaBigDecimal(idToCotizacion),
					nombreUsuario);
			estructuraCotizacion.consultarIncisosCotizacion();
			estructuraCotizacion.consultarSeccionesContratadasCotizacion();
			estructuraCotizacion.consultarCoberturasContratadasCotizacion();
			estructuraCotizacion.consultarRiesgosContratadosCotizacion();
			// CotizacionDN cotizacionDN =
			// CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			// IncisoCotizacionDN incisoCotizacionDN =
			// IncisoCotizacionDN.getInstancia();
			// SeccionDN seccionDN = SeccionDN.getInstancia();
			// SeccionCotizacionDN seccionCotizacionDN =
			// SeccionCotizacionDN.getInstancia();
			// CoberturaDN coberturaDN = CoberturaDN.getInstancia();
			// CoberturaCotizacionDN coberturaCotizacionDN =
			// CoberturaCotizacionDN.getInstancia();

			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			// SeccionDTO seccionDTO = new SeccionDTO();
			// CoberturaDTO coberturaDTO = new CoberturaDTO();

			List<IncisoCotizacionDTO> listaIncisos = new ArrayList<IncisoCotizacionDTO>();
			List<SeccionCotizacionDTO> listaSeccion = new ArrayList<SeccionCotizacionDTO>();
			List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();

			listaIncisos = estructuraCotizacion.getListaIncisos(); // incisoCotizacionDN.listarPorCotizacionId(new
																	// BigDecimal(idToCotizacion));
			// cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = estructuraCotizacion.getCotizacionDTO(); // cotizacionDN.getPorId(cotizacionDTO);
			buffer.append("<tree id=\"0\">\n");
			buffer.append(abrirItem("ODT-"
					+ UtileriasWeb.llenarIzquierda(cotizacionDTO
							.getIdToCotizacion().toString(), "0", 8),
					cotizacionDTO.getIdToCotizacion().toString()));
			if (listaIncisos != null) {
				cotizacion = idToCotizacion;
				// Creando Incisos
				for (IncisoCotizacionDTO incisoCotizacionDTO : listaIncisos) {
					if (incisoCotizacionDTO != null
							&& incisoCotizacionDTO.getId() != null) {
						inciso = cotizacion
								+ separador
								+ incisoCotizacionDTO.getId().getNumeroInciso()
										.toString();
						buffer.append(abrirItem(incisoCotizacionDTO.getId()
								.getNumeroInciso().toString(), inciso));

						// Creando Secciones
						listaSeccion = estructuraCotizacion
								.obtenerSeccionesContratadas(incisoCotizacionDTO
										.getId().getNumeroInciso()); // seccionCotizacionDN.listarPorIncisoId(incisoCotizacionDTO.getId());
						if (listaSeccion != null) {
							for (SeccionCotizacionDTO seccionCotizacionDTO : listaSeccion) {
								if (seccionCotizacionDTO != null
										&& seccionCotizacionDTO.getId() != null) {
									// seccionDTO.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
									// seccionDTO =
									// seccionDN.getPorId(seccionDTO);
									seccion = inciso
											+ separador
											+ seccionCotizacionDTO.getId()
													.getIdToSeccion()
													.toString();
									buffer.append(abrirItem(
											seccionCotizacionDTO
													.getSeccionDTO()
													.getNombreComercial(),
											seccion));
									// Creando coberturas
									listaCoberturas = estructuraCotizacion
											.obtenerCoberturasContratadas(
													seccionCotizacionDTO
															.getId()
															.getIdToSeccion(),
													incisoCotizacionDTO.getId()
															.getNumeroInciso()); // coberturaCotizacionDN.listarPorSeccionCotizacionId(seccionCotizacionDTO.getId(),Boolean.TRUE);
									if (listaCoberturas != null) {
										for (CoberturaCotizacionDTO coberturaCotizacionDTO : listaCoberturas) {
											// coberturaDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
											// coberturaDTO =
											// coberturaDN.getPorId(coberturaDTO);
											cobertura = seccion
													+ separador
													+ coberturaCotizacionDTO
															.getId()
															.getIdToCobertura()
															.toString();
											if (coberturaCotizacionDTO
													.getClaveContrato()
													.intValue() == 1) {
												buffer
														.append(abrirItem(
																coberturaCotizacionDTO
																		.getCoberturaSeccionDTO()
																		.getCoberturaDTO()
																		.getNombreComercial(),
																cobertura));
												buffer.append(cerrarItem());
											}
										}
									}

									buffer.append(cerrarItem());
								}
							}
						}
						buffer.append(cerrarItem());
					}// Inciso valido
				}
			}
			buffer.append(cerrarItem());
			buffer.append("</tree>");
			request.setAttribute("xmlArbol", buffer.toString());
			reglaNavegacion = Sistema.EXITOSO;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method cargarArbolCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward cargarArbolCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String idToCotizacion = request.getParameter("idToCotizacion");
		String cotizacion = "";
		String inciso = "";
		String seccion = "";
		String cobertura = "";
		String riesgo = "";
		String separador = "_";

		StringBuffer buffer = new StringBuffer();
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			SoporteEstructuraCotizacion estructuraCotizacion = new SoporteEstructuraCotizacion(
					UtileriasWeb.regresaBigDecimal(idToCotizacion),
					nombreUsuario);
			estructuraCotizacion.consultarIncisosCotizacion();
			estructuraCotizacion.consultarSeccionesContratadasCotizacion();
			estructuraCotizacion.consultarCoberturasContratadasCotizacion();
			estructuraCotizacion.consultarRiesgosContratadosCotizacion();
			// CotizacionDN cotizacionDN =
			// CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			// IncisoCotizacionDN incisoCotizacionDN =
			// IncisoCotizacionDN.getInstancia();
			// SeccionDN seccionDN = SeccionDN.getInstancia();
			// SeccionCotizacionDN seccionCotizacionDN =
			// SeccionCotizacionDN.getInstancia();
			// CoberturaDN coberturaDN = CoberturaDN.getInstancia();
			// CoberturaCotizacionDN coberturaCotizacionDN =
			// CoberturaCotizacionDN.getInstancia();
			// RiesgoCotizacionDN riesgoCotizacionDN =
			// RiesgoCotizacionDN.getInstancia();

			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			// SeccionDTO seccionDTO = new SeccionDTO();
			// CoberturaDTO coberturaDTO = new CoberturaDTO();

			List<IncisoCotizacionDTO> listaIncisos = new ArrayList<IncisoCotizacionDTO>();
			List<SeccionCotizacionDTO> listaSeccion = new ArrayList<SeccionCotizacionDTO>();
			List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
			List<RiesgoCotizacionDTO> listaRiesgos = new ArrayList<RiesgoCotizacionDTO>();

			listaIncisos = estructuraCotizacion.getListaIncisos(); // incisoCotizacionDN.listarPorCotizacionId(new
																	// BigDecimal(idToCotizacion));
			// cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = estructuraCotizacion.getCotizacionDTO(); // cotizacionDN.getPorId(cotizacionDTO);
			String moneda = "()";
			if (cotizacionDTO.getIdMoneda() != null) {
				if (cotizacionDTO.getIdMoneda().equals(
						new BigDecimal(Sistema.MONEDA_PESOS))) {
					moneda = " (MXP)";
				} else if (cotizacionDTO.getIdMoneda().equals(
						new BigDecimal(Sistema.MONEDA_DOLARES))) {
					moneda = " (USD)";
				}
			}

			buffer.append("<tree id=\"0\">\n");
			buffer.append(abrirItem("COT-"
					+ UtileriasWeb.llenarIzquierda(cotizacionDTO
							.getIdToCotizacion().toString(), "0", 8) + moneda,
					cotizacionDTO.getIdToCotizacion().toString()));
			if (listaIncisos != null) {
				cotizacion = idToCotizacion;
				// Creando Incisos
				for (IncisoCotizacionDTO incisoCotizacionDTO : listaIncisos) {
					if (incisoCotizacionDTO != null
							&& incisoCotizacionDTO.getId() != null) {
						inciso = cotizacion
								+ separador
								+ incisoCotizacionDTO.getId().getNumeroInciso()
										.toString();
						buffer.append(abrirItem(incisoCotizacionDTO.getId()
								.getNumeroInciso().toString(), inciso));

						// Creando Secciones
						listaSeccion = estructuraCotizacion
								.obtenerSeccionesContratadas(incisoCotizacionDTO
										.getId().getNumeroInciso()); // seccionCotizacionDN.listarPorIncisoId(incisoCotizacionDTO.getId());
						if (listaSeccion != null) {
							for (SeccionCotizacionDTO seccionCotizacionDTO : listaSeccion) {
								if (seccionCotizacionDTO != null
										&& seccionCotizacionDTO.getId() != null) {
									// seccionDTO.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
									// seccionDTO =
									// seccionDN.getPorId(seccionDTO);
									seccion = inciso
											+ separador
											+ seccionCotizacionDTO.getId()
													.getIdToSeccion()
													.toString();
									buffer.append(abrirItem(
											seccionCotizacionDTO
													.getSeccionDTO()
													.getNombreComercial(),
											seccion));

									listaCoberturas = estructuraCotizacion
											.obtenerCoberturasContratadas(
													seccionCotizacionDTO
															.getId()
															.getIdToSeccion(),
													incisoCotizacionDTO.getId()
															.getNumeroInciso()); // coberturaCotizacionDN.listarPorSeccionCotizacionId(seccionCotizacionDTO.getId());
									if (listaCoberturas != null) {
										for (CoberturaCotizacionDTO coberturaCotizacionDTO : listaCoberturas) {
											// coberturaDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
											// coberturaDTO =
											// coberturaDN.getPorId(coberturaDTO);
											cobertura = seccion
													+ separador
													+ coberturaCotizacionDTO
															.getId()
															.getIdToCobertura()
															.toString();
											if (coberturaCotizacionDTO
													.getClaveContrato()
													.intValue() == 1) {
												// buffer.append(abrirItem(coberturaDTO.getNombreComercial(),
												// cobertura));
												buffer
														.append(abrirItem(
																coberturaCotizacionDTO
																		.getCoberturaSeccionDTO()
																		.getCoberturaDTO()
																		.getNombreComercial(),
																cobertura));
												listaRiesgos = estructuraCotizacion
														.obtenerRiesgosContratados(
																coberturaCotizacionDTO
																		.getId()
																		.getIdToSeccion(),
																coberturaCotizacionDTO
																		.getId()
																		.getNumeroInciso(),
																coberturaCotizacionDTO
																		.getId()
																		.getIdToCobertura()); // riesgoCotizacionDN.listarRiesgoContratadosPorCobertura(coberturaCotizacionDTO.getId());
												if (listaRiesgos != null) {
													for (RiesgoCotizacionDTO riesgoCotizacionDTO : listaRiesgos) {
														if (riesgoCotizacionDTO
																.getId() != null) {
															riesgo = cobertura
																	+ separador
																	+ riesgoCotizacionDTO
																			.getId()
																			.getIdToRiesgo()
																			.toString();
															buffer
																	.append(abrirItem(
																			riesgoCotizacionDTO
																					.getRiesgoCoberturaDTO()
																					.getDescripcionRiesgoCobertura(),
																			riesgo));
															buffer
																	.append(cerrarItem());
														}
													}
												}
												buffer.append(cerrarItem());
											}
										}
									}

									buffer.append(cerrarItem());
								}
							}
						}
						buffer.append(cerrarItem());
					}// Inciso valido
				}
			}
			buffer.append(cerrarItem());
			buffer.append("</tree>");
			request.setAttribute("xmlArbol", buffer.toString());
			reglaNavegacion = Sistema.EXITOSO;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method poblarTreeView Es el metodo que manda llamar el treeview cuando no
	 * tiene la informacion que necesita mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void poblarTreeView(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {

			String itemId = "";
			String idToCotizacion = null;

			if (request.getParameter("id") != null) {
				itemId = request.getParameter("id");
			}

			if (request.getParameter("idToCotizacion") != null) {
				idToCotizacion = request.getParameter("idToCotizacion");
			}

			String contextoMenu = "";
			if (request.getParameter("menu") != null) {
				contextoMenu = request.getParameter("menu");
			}

			TreeLoader tree = new TreeLoader(contextoMenu, mapping, form,
					request, response);

			tree.cargaSiguienteRamaCotizacion(itemId, contextoMenu,
					idToCotizacion);

		} catch (Exception exception) {
			throw new Exception("Error while loading treeview component",
					exception);
		} // End of try/catch

	}

	public ActionForward cargarArbolCotizacionPorNivel(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		int numHijos = 0;
		String itemId = "";
		String nivel = "";
		String contextoMenu = "";
		String idToCotizacion = null;
		String numInciso = null;
		String idSeccion = null;
		String idCobertura = null;
		StringBuffer buffer = new StringBuffer();

		try {

			if (request.getAttribute("id") != null) {
				itemId = request.getAttribute("id").toString();
			}

			if (request.getAttribute("nivel") != null) {
				nivel = request.getAttribute("nivel").toString();
			}

			if (request.getParameter("menu") != null) {
				contextoMenu = request.getParameter("menu");
			}

			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);

			idToCotizacion = itemId.split("_")[0];

			TreeLoader tree = new TreeLoader(contextoMenu, mapping, form,
					request, response);

			// Si se trata del nodo raiz
			if (itemId.equals(idToCotizacion)) {
				// Se le asigna el id de la cotizacion al nodo raiz
				tree.setRoot(idToCotizacion);

				// Se le agrega el numero de la cotizacion a la raiz del arbol
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
						nombreUsuario).getPorId(new BigDecimal(idToCotizacion));
				String moneda = "()";
				if (cotizacionDTO.getIdMoneda() != null) {
					if (cotizacionDTO.getIdMoneda().equals(
							new BigDecimal(Sistema.MONEDA_PESOS))) {
						moneda = " (MXP)";
					} else if (cotizacionDTO.getIdMoneda().equals(
							new BigDecimal(Sistema.MONEDA_DOLARES))) {
						moneda = " (USD)";
					}
				}
				String idToPoliza = request.getParameter("idToPoliza");
				String numeroEndoso = request.getParameter("numeroEndoso");
				if (idToPoliza != null) {
					PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(
							UtileriasWeb.regresaBigDecimal(idToPoliza));
					String numeroPoliza = UtileriasWeb
							.getNumeroPoliza(polizaDTO);
					if (numeroEndoso == null
							|| numeroEndoso.equals("undefined")
							|| numeroEndoso.equals("0")) {
						tree.setDescripcionRoot(numeroPoliza + moneda);
					} else {
						tree.setDescripcionRoot(numeroPoliza + "/"
								+ numeroEndoso + moneda);
					}
				} else {
					if (contextoMenu.equals("estructuraODT")) {
						tree.setDescripcionRoot("ODT-"
								+ UtileriasWeb
										.llenarIzquierda(
												cotizacionDTO
														.getIdToCotizacion()
														.toString(), "0", 8));
					} else {
						tree.setDescripcionRoot("COT-"
								+ UtileriasWeb
										.llenarIzquierda(
												cotizacionDTO
														.getIdToCotizacion()
														.toString(), "0", 8)
								+ moneda);
					}
				}
			}

			if (nivel.equals("inciso")) {
				Long totalSecciones = 0L;
				List<IncisoCotizacionDTO> listaIncisos = IncisoCotizacionDN
						.getInstancia().listarPorCotizacionId(
								new BigDecimal(idToCotizacion));

				for (IncisoCotizacionDTO incisoCotizacionDTO : listaIncisos) {
					numHijos = 0;
					totalSecciones = SeccionCotizacionDN.getInstancia()
							.obtenerTotalSeccionesContratadas(
									new BigDecimal(idToCotizacion),
									incisoCotizacionDTO.getId()
											.getNumeroInciso());

					if (totalSecciones != null) {
						numHijos = totalSecciones.intValue();
					}

					buffer = tree.escribirItemCotizacion(buffer,
							incisoCotizacionDTO.getId().getNumeroInciso()
									.toString(), incisoCotizacionDTO.getId()
									.getNumeroInciso().toString(), itemId,
							numHijos);
				}

			} else if (nivel.equals("seccion")) {
				Long totalCoberturas = 0L;
				numInciso = itemId.split("_")[1];

				List<SeccionCotizacionDTO> listaSeccion = SeccionCotizacionDN
						.getInstancia().listarSeccionesContratadasCotizacion(
								new BigDecimal(idToCotizacion),
								new BigDecimal(numInciso));

				for (SeccionCotizacionDTO seccionCotizacionDTO : listaSeccion) {
					numHijos = 0;

					SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
					seccionCotizacionId.setIdToCotizacion(new BigDecimal(
							idToCotizacion));
					seccionCotizacionId.setIdToSeccion(seccionCotizacionDTO
							.getId().getIdToSeccion());
					seccionCotizacionId.setNumeroInciso(new BigDecimal(
							numInciso));

					totalCoberturas = CoberturaCotizacionDN
							.getInstancia()
							.obtenerTotalCoberturasContratadasPorSeccionCotizacion(
									seccionCotizacionId);

					if (totalCoberturas != null) {
						numHijos = totalCoberturas.intValue();
					}

					buffer = tree.escribirItemCotizacion(buffer,
							seccionCotizacionDTO.getSeccionDTO()
									.getNombreComercial(), seccionCotizacionDTO
									.getId().getIdToSeccion().toString(),
							itemId, numHijos);

				}

			} else if (nivel.equals("cobertura")) {
				Long totalRiesgos = 0L;
				numInciso = itemId.split("_")[1];
				idSeccion = itemId.split("_")[2];

				SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
				seccionCotizacionId.setIdToCotizacion(new BigDecimal(
						idToCotizacion));
				seccionCotizacionId.setIdToSeccion(new BigDecimal(idSeccion));
				seccionCotizacionId.setNumeroInciso(new BigDecimal(numInciso));

				List<CoberturaCotizacionDTO> listaCoberturas = CoberturaCotizacionDN
						.getInstancia()
						.listarCoberturasContratadasPorSeccionCotizacion(
								seccionCotizacionId);

				for (CoberturaCotizacionDTO coberturaCotizacionDTO : listaCoberturas) {
					numHijos = 0;
					if (!contextoMenu.equals("estructuraODT")) {
						CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId();
						coberturaCotizacionId
								.setIdToCobertura(coberturaCotizacionDTO
										.getId().getIdToCobertura());
						coberturaCotizacionId.setIdToCotizacion(new BigDecimal(
								idToCotizacion));
						coberturaCotizacionId.setIdToSeccion(new BigDecimal(
								idSeccion));
						coberturaCotizacionId.setNumeroInciso(new BigDecimal(
								numInciso));

						totalRiesgos = RiesgoCotizacionDN.getInstancia()
								.obtenerTotalRiesgoContratadosPorCobertura(
										coberturaCotizacionId);

						if (totalRiesgos != null) {
							numHijos = totalRiesgos.intValue();
						}
					}
					buffer = tree.escribirItemCotizacion(buffer,
							coberturaCotizacionDTO.getCoberturaSeccionDTO()
									.getCoberturaDTO().getNombreComercial(),
							coberturaCotizacionDTO.getId().getIdToCobertura()
									.toString(), itemId, numHijos);

				}

			} else if (nivel.equals("riesgo")) {

				numInciso = itemId.split("_")[1];
				idSeccion = itemId.split("_")[2];
				idCobertura = itemId.split("_")[3];

				CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId();
				coberturaCotizacionId.setIdToCobertura(new BigDecimal(
						idCobertura));
				coberturaCotizacionId.setIdToCotizacion(new BigDecimal(
						idToCotizacion));
				coberturaCotizacionId.setIdToSeccion(new BigDecimal(idSeccion));
				coberturaCotizacionId
						.setNumeroInciso(new BigDecimal(numInciso));

				List<RiesgoCotizacionDTO> listaRiesgos = RiesgoCotizacionDN
						.getInstancia().listarRiesgoContratadosPorCobertura(
								coberturaCotizacionId);

				for (RiesgoCotizacionDTO riesgoCotizacionDTO : listaRiesgos) {

					buffer = tree.escribirItemCotizacion(buffer,
							riesgoCotizacionDTO.getRiesgoCoberturaDTO()
									.getDescripcionRiesgoCobertura(),
							riesgoCotizacionDTO.getId().getIdToRiesgo()
									.toString(), itemId, numHijos);
				}

			}

			buffer = tree.xmlHeader(buffer, itemId);

			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method listarOrdenesTrabajo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */

	public ActionForward listarOrdenesTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		return listarOrdenesTrabajoFiltrado(mapping, form, request, response);

	}

	/**
	 * Method listarCotizaciones
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarCotizaciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		try {
			// Logear el tipo de cambio
			Double tipoCambio = TipoCambioDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.obtieneTipoCambioPorDia(new Date(),
							(short) Sistema.MONEDA_PESOS);
			System.out.println("Tipo de cambio del dia Moneda Nacional "
					+ tipoCambio);
			// Logear el tipo de cambio

			tipoCambio = TipoCambioDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.obtieneTipoCambioPorDia(new Date(),
							(short) Sistema.MONEDA_DOLARES);
			System.out.println("Tipo de cambio del dia Dolares " + tipoCambio);

		} catch (Exception e) {

		}

		return listarCotizacionesFiltrado(mapping, form, request, response);

	}

	/**
	 * Method listarCotizacionesFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarCotizacionesFiltrado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		try {

			CotizacionForm cotizacionForm = (CotizacionForm) form;
			BigDecimal idToCotizacion = null;
			try {
				idToCotizacion = (BigDecimal) request.getSession()
						.getAttribute("idToCotizacion");
			} catch (Exception e) {
			}

			if (idToCotizacion != null) {
				cotizacionForm.setIdToCotizacion(idToCotizacion.toString());
			}
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);

			request.getSession().removeAttribute("idToCotizacion");
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			poblarDTOCotizacion(cotizacionForm, cotizacionDTO);

			Long totalRegistros = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.obtenerTotalCotizacionesFiltrado(cotizacionDTO, usuario);

			cotizacionForm.setTotalRegistros(totalRegistros.toString());

			return listarCotizacionesFiltradoPaginado(mapping, form, request,
					response);

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarCotizacionesFiltradoPaginado(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		try {
			Usuario usuario = getUsuarioMidas(request);
			if (!listaEnCache(form, request)) {
				CotizacionForm cotizacionForm = (CotizacionForm) form;
				CotizacionDTO cotizacionDTO = new CotizacionDTO();
				poblarDTOCotizacion(cotizacionForm, cotizacionDTO);

				cotizacionDTO.setPrimerRegistroACargar(cotizacionForm
						.getPrimerRegistroACargar());
				cotizacionDTO.setNumeroMaximoRegistrosACargar(cotizacionForm
						.getNumeroMaximoRegistrosACargar());
				cotizacionForm.setListaPaginada(CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request))
						.listarCotizacionFiltrado(cotizacionDTO, usuario),
						request);

			}

			request.setAttribute("listaCotizacion", obtieneListaAMostrar(form,
					request));

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTOCotizacion(CotizacionForm cotizacionForm,
			CotizacionDTO cotizacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreSolicitante()))
			cotizacionDTO.setNombreAsegurado(cotizacionForm
					.getNombreSolicitante());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToCotizacion()))
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveEstatus()))
			cotizacionDTO.setClaveEstatus(UtileriasWeb
					.regresaShort(cotizacionForm.getClaveEstatus()));
	}

	public ActionForward mostrarCancelarOrdenTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		// UsuarioDTO usuarioDTO = new UsuarioDTO();
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String idToCotizacion = request.getParameter("idToCotizacion");
		cotizacionForm.setIdToCotizacion(idToCotizacion);
		try {
			List<CotizacionRechazoCancelacionDTO> listaRechazo = CotizacionRechazoCancelacionDN
					.getInstancia().buscarPorPropiedad("id.idToCotizacion",
							UtileriasWeb.regresaBigDecimal(idToCotizacion));
			if (listaRechazo != null && !listaRechazo.isEmpty()) {
				cotizacionForm
						.setIdMotivoRechazo(((CotizacionRechazoCancelacionDTO) listaRechazo
								.get(0)).getId().getIdTcRechazoCancelacion()
								.toBigInteger().toString());
			}
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarCancelarCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		// UsuarioDTO usuarioDTO = new UsuarioDTO();
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String idToCotizacion = request.getParameter("idToCotizacion");
		cotizacionForm.setIdToCotizacion(idToCotizacion);
		try {
			List<CotizacionRechazoCancelacionDTO> listaRechazo = CotizacionRechazoCancelacionDN
					.getInstancia().buscarPorPropiedad("id.idToCotizacion",
							UtileriasWeb.regresaBigDecimal(idToCotizacion));
			if (listaRechazo != null && !listaRechazo.isEmpty()) {
				cotizacionForm
						.setIdMotivoRechazo(((CotizacionRechazoCancelacionDTO) listaRechazo
								.get(0)).getId().getIdTcRechazoCancelacion()
								.toBigInteger().toString());
			}
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void cancelarOrdenTrabajo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		boolean error = false;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
		try {
			if (!UtileriasWeb
					.esCadenaVacia(cotizacionForm.getIdMotivoRechazo())) {
				Usuario usuario = (Usuario) request.getSession().getAttribute(
						Sistema.USUARIO_ACCESO_MIDAS);
				CotizacionDTO cotizacionDTO = OrdenTrabajoDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request))
						.rechazarCancelarOrdenTrabajo(
								UtileriasWeb.regresaBigDecimal(cotizacionForm
										.getIdToCotizacion()),
								UtileriasWeb.regresaBigDecimal(cotizacionForm
										.getIdMotivoRechazo()), usuario);

				if (cotizacionDTO != null
						&& cotizacionDTO.getSolicitudDTO().getEmailContactos() != null) {
					List<String> destinatarios = new ArrayList<String>();
					String[] listaCorreos = (cotizacionDTO.getSolicitudDTO()
							.getEmailContactos()).split(",");
					for (String correo : listaCorreos) {
						destinatarios.add(correo);
					}
					if (!destinatarios.isEmpty()) {
						RechazoCancelacionDTO rechazoCancelacion = RechazoCancelacionDN
								.getInstancia()
								.getPorId(
										UtileriasWeb
												.regresaBigDecimal(cotizacionForm
														.getIdMotivoRechazo()));
						String asunto = "Orden de Trabajo Rechazada "
								+ cotizacionDTO.getSolicitudDTO()
										.getIdToSolicitud()
								+ ", Producto "
								+ cotizacionDTO.getSolicitudDTO()
										.getProductoDTO().getNombreComercial();
						String contenido = "Hemos verificado su petici&oacute;n de aseguramiento y lamentamos informarle "
								+ "que no es posible efectuar una oferta de seguro por lo siguiente:\n\n\n"
								+ "<center>"
								+ rechazoCancelacion.getDescripcionMotivo()
								+ "</center>\n\n\n"
								+ "Esperamos tener la posibilidad de servirle en otra ocasi&oacute;n."
								+ "<br/><img src='/MidasWeb/img/logo_AS.jpg'/><br/>"
								+ "<h1 style='text-align:right;'>SEGUROS AFIRME, S.A. DE C.V., AFIRME<br/>GRUPO FINANCIERO</h1><br>"
								+ "<p align='right'>Ocampo No. 220 Pte, c.p. 64000, Colonia Centro, Monterrey, N.L.</p>"
								+ "<br/><p align='right'> Tel�fono: (8)3183800, R.F.C. SAF-980202-D99</p>";
						try {
							MailAction.enviaCorreo(destinatarios, asunto,
									contenido,
									new ArrayList<ByteArrayAttachment>());
						} catch (SecurityException e) {
						}
					}
					buffer
							.append("<id>1</id><description><![CDATA[La orden de trabajo se cancel\u00f3 correctamente. La notificaci\u00f3n fue enviada por correo electr\u00f3nico.]]></description>");
				} else {
					buffer
							.append("<id>1</id><description><![CDATA[La orden de trabajo se cancel\u00f3 correctamente. No se envio notificaci&oacute;n debido a que no se registraron destinatarios.]]></description>");
				}
			} else
				buffer
						.append("<id>2</id><description><![CDATA[Seleccione un motivo de la lista.]]></description>");
		} catch (ExcepcionDeAccesoADatos e) {
			error = true;
			e.printStackTrace();
		} catch (SystemException e) {
			error = true;
			e.printStackTrace();
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
		}
		if (error)
			buffer
					.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al cancelar la orden de trabajo.]]></description>");
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cancelarCotizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		boolean error = false;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
		try {
			if (!UtileriasWeb
					.esCadenaVacia(cotizacionForm.getIdMotivoRechazo())) {
				Usuario usuario = (Usuario) request.getSession().getAttribute(
						Sistema.USUARIO_ACCESO_MIDAS);
				CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request))
						.rechazarCancelarCotizacion(
								UtileriasWeb.regresaBigDecimal(cotizacionForm
										.getIdToCotizacion()),
								UtileriasWeb.regresaBigDecimal(cotizacionForm
										.getIdMotivoRechazo()), usuario);
				buffer
						.append("<id>1</id><description><![CDATA[La Cotizaci\u00f3n se cancel\u00f3 correctamente.]]></description>");
			} else
				buffer
						.append("<id>2</id><description><![CDATA[Seleccione un motivo de la lista.]]></description>");
		} catch (ExcepcionDeAccesoADatos e) {
			error = true;
			e.printStackTrace();
		} catch (SystemException e) {
			error = true;
			e.printStackTrace();
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
		}
		if (error)
			buffer
					.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al cancelar la orden de trabajo.]]></description>");
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ActionForward editarOrdenTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		String idToCotizacion = request.getParameter("id");
		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm, request);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward editarCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		String idToCotizacion = request.getParameter("id");
		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm, request);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void autorizarOrdenTrabajo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);
			cotizacionDTO = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
					UtileriasWeb.regresaBigDecimal(cotizacionForm
							.getIdToCotizacion()));
			if (cotizacionForm.getClaveAutRetroacDifer() != null) {
				cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZADA);
				cotizacionDTO.setCodigoUsuarioAutRetroacDifer(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutRetroacDifer(Calendar.getInstance()
						.getTime());
			}
			if (cotizacionForm.getClaveAutVigenciaMaxMin() != null) {
				cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZADA);
				cotizacionDTO.setCodigoUsuarioAutVigenciaMaxMin(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutVigenciaMaxMin(Calendar.getInstance()
						.getTime());
			}
			if (cotizacionForm.getClaveAutorizacionRPF() != null) {
				cotizacionDTO
						.setClaveAutorizacionRecargoPagoFraccionado(Sistema.AUTORIZADA);
				cotizacionDTO
						.setCodigoUsuarioAutorizacionRecargoPagoFraccionado(usuario
								.getNombreUsuario());
				cotizacionDTO
						.setFechaAutorizacionRecargoPagoFraccionado(Calendar
								.getInstance().getTime());
			}
			if (cotizacionForm.getClaveAutorizacionDerechos() != null) {
				cotizacionDTO.setClaveAutorizacionDerechos(Sistema.AUTORIZADA);
				cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutorizacionDerechos(Calendar
						.getInstance().getTime());
			}
			if (cotizacionForm.getClaveAutorizacionDiasGracia() != null) {
				cotizacionDTO
						.setClaveAutorizacionDiasGracia(Sistema.AUTORIZADA);
				cotizacionDTO.setCodigoUsuarioAutorizaDiasGracia(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutorizaDiasGracia(Calendar.getInstance()
						.getTime());
			}

			if (cotizacionForm.getClaveAutorizacionCliente() != null) {

				RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
						.getInstancia();
				RenovacionPolizaDTO renovacion = renovacionPolizaDN
						.buscarRenovacionPolizaPorCotizacion(cotizacionDTO
								.getIdToCotizacion());
				renovacionPolizaDN
						.setDetalleNotificacion(UtileriasWeb
								.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"poliza.renovacion.detalle.movimiento.autorizacion",
										usuario.getNombreUsuario()));
				renovacionPolizaDN
						.setTipoMovimiento(Sistema.TIPO_RENOVACION_ACEPTADA);
				renovacionPolizaDN.agregarDetalleSeguimiento(renovacion
						.getPolizaDTO());
				cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
			}
			cotizacionDTO.setCodigoUsuarioModificacion(usuario
					.getNombreUsuario());
			cotizacionDTO.setFechaModificacion(new Date());

			CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).modificar(
					cotizacionDTO);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

	}

	public void rechazarOrdenTrabajo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);

			cotizacionDTO = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
					UtileriasWeb.regresaBigDecimal(cotizacionForm
							.getIdToCotizacion()));
			if (cotizacionForm.getClaveAutRetroacDifer() != null) {
				cotizacionDTO.setClaveAutRetroacDifer(Sistema.RECHAZADA);
				cotizacionDTO.setCodigoUsuarioAutRetroacDifer(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutRetroacDifer(Calendar.getInstance()
						.getTime());
			}
			if (cotizacionForm.getClaveAutVigenciaMaxMin() != null) {
				cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.RECHAZADA);
				cotizacionDTO.setCodigoUsuarioAutVigenciaMaxMin(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutVigenciaMaxMin(Calendar.getInstance()
						.getTime());
			}
			if (cotizacionForm.getClaveAutorizacionRPF() != null) {
				cotizacionDTO
						.setClaveAutorizacionRecargoPagoFraccionado(Sistema.RECHAZADA);
				cotizacionDTO
						.setCodigoUsuarioAutorizacionRecargoPagoFraccionado(usuario
								.getNombreUsuario());
				cotizacionDTO
						.setFechaAutorizacionRecargoPagoFraccionado(Calendar
								.getInstance().getTime());
			}
			if (cotizacionForm.getClaveAutorizacionDerechos() != null) {
				cotizacionDTO.setClaveAutorizacionDerechos(Sistema.RECHAZADA);
				cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutorizacionDerechos(Calendar
						.getInstance().getTime());
			}
			if (cotizacionForm.getClaveAutorizacionDiasGracia() != null) {
				cotizacionDTO.setClaveAutorizacionDiasGracia(Sistema.RECHAZADA);
				cotizacionDTO.setCodigoUsuarioAutorizaDiasGracia(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaAutorizaDiasGracia(Calendar.getInstance()
						.getTime());
			}

			cotizacionDTO.setCodigoUsuarioModificacion(usuario
					.getNombreUsuario());
			cotizacionDTO.setFechaModificacion(new Date());
			CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).modificar(
					cotizacionDTO);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

	}

	public ActionForward guardarOrdenTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		ActionForward forward;
		BigDecimal idToTipoPolizaAnterior = null;
		BigDecimal idToTipoPolizaNueva = null;
		String parametrosMensaje = null;
		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			if (cotizacionDTO.getTipoPolizaDTO() != null) {
				idToTipoPolizaAnterior = cotizacionDTO.getTipoPolizaDTO()
						.getIdToTipoPoliza();
			}
			Usuario usuario = (Usuario) request.getSession().getAttribute(
					Sistema.USUARIO_ACCESO_MIDAS);
			cotizacionDTO.setCodigoUsuarioModificacion(usuario
					.getNombreUsuario());
			cotizacionDTO.setFechaModificacion(new Date());

			poblarDTO(cotizacionForm, cotizacionDTO);

			// OrdenTrabajoDN.getInstancia().actualizarCotizacion(cotizacionDTO);
			if (cotizacionDTO.getTipoPolizaDTO() != null) {
				idToTipoPolizaNueva = cotizacionDTO.getTipoPolizaDTO()
						.getIdToTipoPoliza();
			}
			// Generar las nuevas comisiones y borrar los incisos
			// correspondientes al tipopoliza anterior.
			CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.cambiarTipoPoliza(cotizacionDTO, idToTipoPolizaAnterior,
							idToTipoPolizaNueva);
			// Se asigna el tipo de cambio actual. En caso de que la Fuente de
			// Datos no este disponible,
			// se interrumpira el proceso
			short moneda = cotizacionDTO.getIdMoneda() == null ? (short) Sistema.MONEDA_PESOS
					: Short.valueOf(cotizacionDTO.getIdMoneda().toString());
			Double tipoCambio = TipoCambioDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.obtieneTipoCambioPorDia(new Date(), moneda);

			// Si hay definido un Tipo de Cambio
			if (tipoCambio != null) {
				cotizacionDTO.setTipoCambio(tipoCambio);
				CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request)).modificar(
						cotizacionDTO);
				// Se manda un mensaje de proceso de exito
				cotizacionForm.setTipoMensaje(Sistema.EXITO);
				cotizacionForm
						.setMensaje("Se registro correctamente la Informaci\u00f3n.");
				parametrosMensaje = "tipoMensaje="
						+ Sistema.EXITO
						+ "&mensaje=Se registro correctamente la Informaci\u00f3n.";
			} else {
				// Si no esta definido ningun Tipo de Cambio (Si la interfaz o
				// la informacion requerida en la BD no estan disponibles)

				// Se notifica al administrador de Midas y Sistema Afirme de la
				// incidencia
				String titulo = UtileriasWeb.getMensajeRecurso(
						Sistema.ARCHIVO_RECURSOS,
						"email.tipocambio.nodisponible.titulo");
				String contenido = UtileriasWeb.getMensajeRecurso(
						Sistema.ARCHIVO_RECURSOS,
						"email.tipocambio.nodisponible.contenido");

				MailAction.enviaCorreoNotificacionAdmins(titulo, contenido);

				// Se manda un mensaje de proceso truncado al usuario
				cotizacionForm.setTipoMensaje(Sistema.ERROR);
				cotizacionForm
						.setMensaje("No se puede continuar con el proceso debido a que el Tipo de Cambio no est� disponible. Favor de consultar a un administrador.");
				parametrosMensaje = "tipoMensaje="
						+ Sistema.ERROR
						+ "&mensaje=No se puede continuar con el proceso debido a que el Tipo de Cambio no est� disponible. Favor de consultar a un administrador.";
			}
			if (cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null) {
				AgenteDN agenteDN = AgenteDN.getInstancia();
				try {
					AgenteDTO agenteDTO = new AgenteDTO();
					agenteDTO.setIdTcAgente(cotizacionDTO.getSolicitudDTO()
							.getCodigoAgente().intValue());
					agenteDTO = agenteDN.verDetalleAgente(agenteDTO, usuario
							.getNombreUsuario());
					if (agenteDTO != null) {
						cotizacionDTO.getSolicitudDTO().setNombreAgente(
								agenteDTO.getNombre());
						if (agenteDTO.getIdOficina() != null) {
							cotizacionDTO.getSolicitudDTO().setIdOficina(
									UtileriasWeb.regresaBigDecimal(agenteDTO
											.getIdGerencia()));
							cotizacionDTO.getSolicitudDTO().setNombreOficina(
									agenteDTO.getNombreGerencia());
							cotizacionDTO.getSolicitudDTO().setCodigoEjecutivo(
									UtileriasWeb.regresaBigDecimal(agenteDTO
											.getIdOficina()));
							cotizacionDTO.getSolicitudDTO().setNombreEjecutivo(
									agenteDTO.getNombreOficina());
							cotizacionDTO.getSolicitudDTO()
									.setNombreOficinaAgente(
											agenteDTO.getNombrePromotoria());
						}
					}
				} catch (Exception e) {
				}

			}

			SolicitudDN.getInstancia().actualizar(
					cotizacionDTO.getSolicitudDTO());
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!UtileriasWeb.esObjetoNulo(cotizacionForm.getEsCotizacion())
				&& cotizacionForm.getEsCotizacion().equals("1")) {
			forward = new ActionForward();
			String path;
			path = "/cotizacion/mostrarDatosGenerales.do?id="
					+ cotizacionForm.getIdToCotizacion() + "&"
					+ parametrosMensaje;
			forward.setPath(path);
			return forward;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarAsignarOrdenTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws ExcepcionDeAccesoADatos,
			SystemException {
		String reglaNavegacion = Sistema.EXITOSO;
		// UsuarioDTO usuarioDTO = new UsuarioDTO();

		CotizacionForm cotizacionForm = (CotizacionForm) form;
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToSolicitud = request.getParameter("idToSolicitud");
		// Si se va a mostrar la forma para generar la OT a partir de la
		// solicitud
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		String claveAsignacionLimitada = "midas.danios.asignacion.ordentrabajo.limitada";
		if (usuario != null
				&& usuario.contieneAtributo(claveAsignacionLimitada)) {
			usuarios.add(usuario);
		} else {
			if (!UtileriasWeb.esCadenaVacia(idToSolicitud)
					&& !idToSolicitud.toLowerCase().startsWith("und")) {

				cotizacionForm.setIdToSolicitud(idToSolicitud);

				if (Integer.parseInt(idToSolicitud) > 0) {
					usuarios = UsuarioWSDN.getInstancia()
							.obtieneUsuariosSinRolesPorNombreRol(
									usuario.getNombreUsuario(),
									usuario.getIdSesionUsuario(),
									Sistema.ROL_SUSCRIPTOR_COT,
									Sistema.ROL_AGENTE_EXTERNO,
									Sistema.ROL_SUSCRIPTOR_EXT);
				} else {
					usuarios = UsuarioWSDN.getInstancia()
							.obtieneUsuariosSinRolesPorNombreRol(
									usuario.getNombreUsuario(),
									usuario.getIdSesionUsuario(),
									Sistema.ROL_EMISOR,
									Sistema.ROL_AGENTE_EXTERNO,
									Sistema.ROL_SUSCRIPTOR_EXT);
				}

			} else { // Se va a mostrar la forma para generar la Cotizacion a
						// partir de la OT
				usuarios = UsuarioWSDN.getInstancia()
						.obtieneUsuariosSinRolesPorNombreRol(
								usuario.getNombreUsuario(),
								usuario.getIdSesionUsuario(),
								Sistema.ROL_SUSCRIPTOR_COT,
								Sistema.ROL_AGENTE_EXTERNO,
								Sistema.ROL_SUSCRIPTOR_EXT);
			}
		}

		if (!UtileriasWeb.esCadenaVacia(idToCotizacion)) {
			cotizacionForm.setIdToCotizacion(idToCotizacion);
			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
					UtileriasWeb.regresaBigDecimal(idToCotizacion));
			cotizacionForm.setClaveTipoEndoso(cotizacionDTO.getSolicitudDTO()
					.getClaveTipoEndoso() != null ? cotizacionDTO
					.getSolicitudDTO().getClaveTipoEndoso().toString() : "0");
		}
		cotizacionForm.setUsuarios(usuarios);

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarAsignarSolicitudCotizacion(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws ExcepcionDeAccesoADatos,
			SystemException {
		String reglaNavegacion = Sistema.EXITOSO;
		// UsuarioDTO usuarioDTO = new UsuarioDTO();

		CotizacionForm cotizacionForm = (CotizacionForm) form;
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String idToPoliza = request.getParameter("idToPoliza");
		String idToSolicitud = request.getParameter("idToSolicitud");
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		// Si se va a mostrar la forma para generar la OT a partir de la
		// solicitud
		if (!UtileriasWeb.esCadenaVacia(idToSolicitud)
				&& !idToSolicitud.toLowerCase().startsWith("und")) {
			cotizacionForm.setIdToSolicitud(idToSolicitud);
			String claveAsignacionLimitada = "midas.danios.asignacion.solicitud.limitada";
			if (usuario != null
					&& usuario.contieneAtributo(claveAsignacionLimitada)) {
				usuarios.add(usuario);
			} else {
				if (Integer.parseInt(idToSolicitud) > 0) {
					usuarios = UsuarioWSDN.getInstancia()
							.obtieneUsuariosSinRolesPorNombreRol(
									usuario.getNombreUsuario(),
									usuario.getIdSesionUsuario(),
									Sistema.ROL_SUSCRIPTOR_OT,
									Sistema.ROL_AGENTE_EXTERNO,
									Sistema.ROL_SUSCRIPTOR_EXT);
				} else {
					usuarios = UsuarioWSDN.getInstancia()
							.obtieneUsuariosSinRolesPorNombreRol(
									usuario.getNombreUsuario(),
									usuario.getIdSesionUsuario(),
									Sistema.ROL_EMISOR,
									Sistema.ROL_AGENTE_EXTERNO,
									Sistema.ROL_SUSCRIPTOR_EXT);
				}
			}
		} else { // Se va a mostrar la forma para generar la Cotizacion a partir
					// de la OT
			usuarios = UsuarioWSDN.getInstancia()
					.obtieneUsuariosSinRolesPorNombreRol(
							usuario.getNombreUsuario(),
							usuario.getIdSesionUsuario(),
							Sistema.ROL_SUSCRIPTOR_COT,
							Sistema.ROL_AGENTE_EXTERNO,
							Sistema.ROL_SUSCRIPTOR_EXT);
		}

		if (!UtileriasWeb.esCadenaVacia(idToPoliza)) {
			cotizacionForm.setIdToPoliza(idToPoliza);
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(
					UtileriasWeb.regresaBigDecimal(idToPoliza));
			CotizacionDTO cotizacionDTO = polizaDTO.getCotizacionDTO();
			cotizacionForm.setClaveTipoEndoso(cotizacionDTO.getSolicitudDTO()
					.getClaveTipoEndoso() != null ? cotizacionDTO
					.getSolicitudDTO().getClaveTipoEndoso().toString() : "0");
			cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion()
					.toString());
		}
		cotizacionForm.setUsuarios(usuarios);

		return mapping.findForward(reglaNavegacion);
	}

	public void asignarSolicitudCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		boolean error = false;

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
		try {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
					.obtieneNombreUsuario(request));
			SolicitudDTO solicitudDTO = SolicitudDN.getInstancia().getPorId(
					UtileriasWeb.regresaBigDecimal(cotizacionForm
							.getIdToSolicitud()));
			if (solicitudDTO.getEsRenovacion().shortValue() == Sistema.ES_RENOVACION) {
				EndosoDTO endosoDTO = EndosoDN.getInstancia(
						usuario.getNombreUsuario()).getUltimoEndoso(
						solicitudDTO.getIdToPolizaAnterior());
				cotizacionDTO = cotizacionDN.generarCotizacionASolicitud(
						endosoDTO.getIdToCotizacion(), solicitudDTO,
						cotizacionForm.getCodigoUsuarioOrdenTrabajo());
			} else {
				cotizacionDTO = cotizacionDN.generarCotizacionASolicitud(
						UtileriasWeb.regresaBigDecimal(cotizacionForm
								.getIdToCotizacion()), solicitudDTO,
						cotizacionForm.getCodigoUsuarioOrdenTrabajo());
			}

			// cotizacionDTO.setCodigoUsuarioEmision(cotizacionForm.getCodigoUsuarioOrdenTrabajo());Actualizar
			// solicitud a 1
			cotizacionDTO.setCodigoUsuarioModificacion(usuario
					.getNombreUsuario());
			cotizacionDTO.setFechaModificacion(new Date());

			Double tipoCambio = TipoCambioDN.getInstancia(
					usuario.getNombreUsuario()).obtieneTipoCambioPorDia(
					new Date(), cotizacionDTO.getIdMoneda().shortValue());
			cotizacionDTO.setTipoCambio(tipoCambio);

			OrdenTrabajoDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.actualizarCotizacion(cotizacionDTO);
			// Se cambia el estatus de la Solicitud a Asignada y se actualiza
			solicitudDTO.setClaveEstatus(Sistema.ESTATUS_SOL_ASIGNADA);
			SolicitudDN.getInstancia().actualizar(solicitudDTO);

			SolicitudDN.getInstancia().registraEstadisticas(
					solicitudDTO.getIdToSolicitud(),
					cotizacionDTO.getIdToCotizacion(), null, null,
					cotizacionForm.getCodigoUsuarioOrdenTrabajo(),
					Sistema.SE_ASIGNA_SOL_A_COT);

			cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion()
					.toString());

			buffer
					.append("<id>1</id><description><![CDATA[La solicitud se asign\u00f3 correctamente. El c\u00f3digo de la cotizaci\u00f3n es: COT-"
							+ UtileriasWeb.llenarIzquierda(cotizacionDTO
									.getIdToCotizacion().toBigInteger()
									.toString(), "0", 8) + ".]]></description>");
			buffer.append("<idToCotizacion>"
					+ cotizacionDTO.getIdToCotizacion() + "</idToCotizacion>");
			request.getSession().removeAttribute("idToCotizacion");
			request.getSession().setAttribute("idToCotizacion",
					cotizacionDTO.getIdToCotizacion());
		} catch (ExcepcionDeAccesoADatos e) {
			error = true;
			e.printStackTrace();
		} catch (SystemException e) {
			error = true;
			e.printStackTrace();
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
		}
		if (error)
			buffer
					.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al asignar la solicitud.]]></description>");
//		try {
//			SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
//			soporteDanosDN
//					.enviaCorreoNotificacionConfirmacionContratoFacultativo(
//							cotizacionDTO.getIdToCotizacion(), usuario);
//		} catch (SystemException e1) {
//			e1.printStackTrace();
//		}

		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void asignarCotizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		boolean error = false;

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			// cotizacionDTO.setCodigoUsuarioEmision(cotizacionForm.getCodigoUsuarioOrdenTrabajo());
			cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_ASIGNADA_EMISION);
			cotizacionDTO.setCodigoUsuarioModificacion(usuario
					.getNombreUsuario());
			cotizacionDTO.setFechaModificacion(new Date());
			cotizacionDTO.setCodigoUsuarioEmision(cotizacionForm
					.getCodigoUsuarioOrdenTrabajo());
			OrdenTrabajoDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.actualizarCotizacion(cotizacionDTO);
			SolicitudDN.getInstancia().registraEstadisticas(
					cotizacionDTO.getSolicitudDTO().getIdToSolicitud(),
					cotizacionDTO.getIdToCotizacion(), null, null,
					cotizacionForm.getCodigoUsuarioOrdenTrabajo(),
					Sistema.SE_ASIGNA_COT_A_EMISION);

			RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
					.getInstancia();
			RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN
					.buscarDetalleRenovacionPoliza(cotizacionDTO
							.getSolicitudDTO().getIdToPolizaAnterior());
			if (renovacionPolizaDTO != null) {
				renovacionPolizaDN
						.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_COT_LISTA_EMISION);
				renovacionPolizaDN
						.setDetalleNotificacion("Cotizaci�n Lista para Emitir asignada a Usuario: "
								+ cotizacionForm.getCodigoUsuarioOrdenTrabajo());
				renovacionPolizaDN
						.agregarDetalleSeguimiento(renovacionPolizaDTO
								.getPolizaDTO());
				renovacionPolizaDTO.getCotizacionDTO().getSolicitudDTO()
						.setClaveEstatus(Sistema.ESTATUS_SOL_TERMINADA);
				SolicitudDN.getInstancia().actualizar(
						renovacionPolizaDTO.getCotizacionDTO()
								.getSolicitudDTO());
			}

			buffer
					.append("<id>1</id><description><![CDATA[La cotizacion se asign\u00f3 correctamente.]]></description>");
		} catch (ExcepcionDeAccesoADatos e) {
			error = true;
			e.printStackTrace();
		} catch (SystemException e) {
			error = true;
			e.printStackTrace();
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
		}
		if (error)
			buffer
					.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al asignar la Cotizaci\u00f3n para Emisi\u00f3n.]]></description>");
//		try {
//			SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
//			soporteDanosDN
//					.enviaCorreoNotificacionConfirmacionContratoFacultativo(
//							cotizacionDTO.getIdToCotizacion(), usuario);
//		} catch (SystemException e1) {
//			e1.printStackTrace();
//		}

		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void asignarOrdenTrabajo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		boolean error = false;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
		try {
			if (UtileriasWeb.esCadenaVacia(cotizacionForm
					.getCodigoUsuarioOrdenTrabajo())) {
				if (!UtileriasWeb.esCadenaVacia(cotizacionForm
						.getIdToSolicitud())) {
					if (!cotizacionForm.getIdToSolicitud().equalsIgnoreCase(
							"undefined")) {
						buffer
								.append("<id>2</id><description><![CDATA[Seleccione un usuario para asignarle la Orden de trabajo.]]></description>");
					} else {
						buffer
								.append("<id>2</id><description><![CDATA[Seleccione un usuario para asignarle la Cotizacion.]]></description>");
					}
				} else {
					buffer
							.append("<id>2</id><description><![CDATA[Seleccione un usuario para asignarle la Cotizacion.]]></description>");
				}
			} else {
				// Si se cuenta con una solicitud se trata de una asignacion de
				// OT
				if (!UtileriasWeb.esCadenaVacia(cotizacionForm
						.getIdToSolicitud())
						& !cotizacionForm.getIdToSolicitud().equalsIgnoreCase(
								"undefined")) {

					// Se asigna el tipo de cambio actual. En caso de que la
					// Fuente de Datos no este disponible,
					// se interrumpira el proceso
					short moneda = cotizacionDTO.getIdMoneda() == null ? (short) Sistema.MONEDA_PESOS
							: Short.valueOf(cotizacionDTO.getIdMoneda()
									.toString());
					Double tipoCambio = TipoCambioDN.getInstancia(
							UtileriasWeb.obtieneNombreUsuario(request))
							.obtieneTipoCambioPorDia(new Date(), moneda);

					// Si hay definido un Tipo de Cambio
					if (tipoCambio != null) {

						SolicitudDN solicitudDN = SolicitudDN.getInstancia();
						SolicitudDTO solicitudDTO = solicitudDN
								.getPorId(UtileriasWeb
										.regresaBigDecimal(cotizacionForm
												.getIdToSolicitud()));
						CotizacionDN cotizacionDN = CotizacionDN
								.getInstancia(UtileriasWeb
										.obtieneNombreUsuario(request));
						List<CotizacionDTO> resultado = cotizacionDN
								.buscarPorPropiedad(
										"solicitudDTO.idToSolicitud",
										solicitudDTO.getIdToSolicitud());
						if (resultado != null && resultado.size() > 0)
							cotizacionDTO = resultado.get(0);
						Usuario usuario = (Usuario) UtileriasWeb
								.obtenValorSessionScope(request,
										Sistema.USUARIO_ACCESO_MIDAS);
						boolean cotizacionExistente = false;
						if (cotizacionDTO.getIdToCotizacion() == null)
							cotizacionDTO = SolicitudAction.crearOrdenTrabajo(
									solicitudDTO, usuario);
						else
							cotizacionExistente = true;

						cotizacionDTO.setPorcentajebonifcomision(0d);
						cotizacionDTO
								.setCodigoUsuarioOrdenTrabajo(cotizacionForm
										.getCodigoUsuarioOrdenTrabajo());
						cotizacionDTO.setTipoCambio(tipoCambio);
						// Se cambia el estatus de la Solicitud a Asignada y se
						// actualiza
						solicitudDTO
								.setClaveEstatus(Sistema.ESTATUS_SOL_ASIGNADA);
						solicitudDN.actualizar(solicitudDTO);

						// Se asigna el estatus de Orden de trabajo asignada
						cotizacionDTO
								.setClaveEstatus(Sistema.ESTATUS_ODT_ASIGNADA);
						if (!cotizacionExistente)
							cotizacionDTO = cotizacionDN.agregar(cotizacionDTO);
						else {
							cotizacionDN.modificar(cotizacionDTO);

							RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
									.getInstancia();
							RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN
									.buscarDetalleRenovacionPoliza(solicitudDTO
											.getIdToPolizaAnterior());
							if (renovacionPolizaDTO != null) {
								renovacionPolizaDN
										.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_ODT_ASIGNADA);
								renovacionPolizaDN
										.setDetalleNotificacion("Order de Trabajo Asignada a Usuario: "
												+ cotizacionForm
														.getCodigoUsuarioOrdenTrabajo());
								renovacionPolizaDN
										.agregarDetalleSeguimiento(renovacionPolizaDTO
												.getPolizaDTO());
							}
						}

						SolicitudDN.getInstancia().registraEstadisticas(
								solicitudDTO.getIdToSolicitud(),
								cotizacionDTO.getIdToCotizacion(), null, null,
								cotizacionForm.getCodigoUsuarioOrdenTrabajo(),
								Sistema.SE_ASIGNA_SOL_A_ODT);
						cotizacionForm.setIdToCotizacion(cotizacionDTO
								.getIdToCotizacion().toString());
						buffer
								.append("<id>1</id><description><![CDATA[La solicitud se asign\u00f3 correctamente. El c\u00f3digo de la orden de trabajo es: ODT-"
										+ UtileriasWeb.llenarIzquierda(
												cotizacionDTO
														.getIdToCotizacion()
														.toBigInteger()
														.toString(), "0", 8)
										+ ".]]></description>");

					} else {
						// Si no esta definido ningun Tipo de Cambio (Si la
						// interfaz o la informacion requerida en la BD no estan
						// disponibles)

						// Se notifica al administrador de Midas y Sistema
						// Afirme de la incidencia
						String titulo = UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"email.tipocambio.nodisponible.titulo");
						String contenido = UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"email.tipocambio.nodisponible.contenido");

						MailAction.enviaCorreoNotificacionAdmins(titulo,
								contenido);

						// Se manda un mensaje de proceso truncado al usuario
						buffer
								.append("<id>2</id><description><![CDATA[No se puede continuar con el proceso debido a que el Tipo de Cambio"
										+ " no est� disponible. Favor de consultar a un administrador.]]></description>");
					}

				} else {
					// De lo contrario se trata de una asignacion de cotizacion

					cotizacionSN = new CotizacionSN(UtileriasWeb
							.obtieneNombreUsuario(request));
					cotizacionDTO.setIdToCotizacion(UtileriasWeb
							.regresaBigDecimal(cotizacionForm
									.getIdToCotizacion()));
					cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
					cotizacionDTO = cotizacionSN
							.obtenerDatosForaneos(cotizacionDTO);
					cotizacionDTO.setCodigoUsuarioCotizacion(cotizacionForm
							.getCodigoUsuarioOrdenTrabajo());

					// Se asigna el estatus de Cotizacion asignada
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_ASIGNADA);

					RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
							.getInstancia();
					RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN
							.buscarDetalleRenovacionPoliza(cotizacionDTO
									.getSolicitudDTO().getIdToPolizaAnterior());
					if (renovacionPolizaDTO != null) {
						renovacionPolizaDN
								.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_ASIGNACION_SUSCRIPTOR);
						renovacionPolizaDN
								.setDetalleNotificacion("Cotizaci�n asignada a Usuario: "
										+ cotizacionForm
												.getCodigoUsuarioOrdenTrabajo());
						renovacionPolizaDN
								.agregarDetalleSeguimiento(renovacionPolizaDTO
										.getPolizaDTO());
					}

					OrdenTrabajoDN.getInstancia(
							UtileriasWeb.obtieneNombreUsuario(request))
							.actualizarCotizacion(cotizacionDTO);
					SolicitudDN.getInstancia().registraEstadisticas(
							cotizacionDTO.getSolicitudDTO().getIdToSolicitud(),
							cotizacionDTO.getIdToCotizacion(), null, null,
							cotizacionForm.getCodigoUsuarioOrdenTrabajo(),
							Sistema.SE_ASIGNA_ODT_A_COT);
					buffer
							.append("<id>1</id><description><![CDATA[La orden de trabajo se asign\u00f3 correctamente.]]></description>");

				}
			}
		} catch (ExcepcionDeAccesoADatos e) {
			error = true;
			e.printStackTrace();
		} catch (SystemException e) {
			error = true;
			e.printStackTrace();
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
		}
		if (error)
			buffer
					.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al asignar la orden de trabajo.]]></description>");
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ActionForward mostrarPersona(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String idPadre = request.getParameter("idPadre");
		String tipoDireccion = request.getParameter("tipoDireccion");
		String descripcionPadre = request.getParameter("descripcionPadre");
		Usuario usuario = getUsuarioMidas(request);
		try {
			if (!UtileriasWeb.esCadenaVacia(descripcionPadre)
					&& !UtileriasWeb.esCadenaVacia(tipoDireccion)
					&& (!UtileriasWeb.esCadenaVacia(idPadre))) {
				if (descripcionPadre.equals("cotizacion")) {
					CotizacionDTO cotizacionDTO = new CotizacionDTO();
					CotizacionSN cotizacionSN = new CotizacionSN(usuario
							.getNombreUsuario());
					cotizacionDTO.setIdToCotizacion(UtileriasWeb
							.regresaBigDecimal(idPadre));
					cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
					cotizacionDTO = cotizacionSN
							.obtenerDatosForaneos(cotizacionDTO);
					/*
					 * cotizacionForm.setDireccionGeneral(new DireccionForm());
					 * cotizacionForm.setPersonaGeneral(new PersonaForm());
					 */
					cotizacionForm.setClienteGeneral(new ClienteForm());
					cotizacionForm.setIdToCotizacion(idPadre);
					ClienteDTO cliente = null;
					if (tipoDireccion.equals("3")){
						try {
							
//							   DESARROLLO CURP
//							   if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){							 
//								cliente = new ClienteDTO();
//								cliente.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//								cliente.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
//								cliente = ClienteDN.getInstancia().verDetalleCliente(cliente, UtileriasWeb.obtieneNombreUsuario(request));
//							}else{
								cliente = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
//						    DESARROLLO CURP								
//							}

						} catch (Exception exc) {
						}
						if (cliente == null)	cliente = new ClienteDTO();
						ClienteAction.poblarClienteForm(cliente, cotizacionForm.getClienteGeneral());
						/*DireccionAction.poblarDireccionForm(cotizacionForm.getDireccionGeneral(), cotizacionDTO.getDireccionAseguradoDTO());
						poblarPersonaForm(cotizacionForm.getPersonaGeneral(), cotizacionDTO.getPersonaAseguradoDTO());*/
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						if (cotizacionDTO.getIdToPersonaAsegurado()!= null && cotizacionDTO.getIdToPersonaAsegurado().doubleValue()>0){
							if(cotizacionDTO.getIdToPersonaContratante() == null || cotizacionDTO.getIdToPersonaContratante().doubleValue() == 0){
								cotizacionDTO.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaAsegurado());
//								 DESARROLLO CURP
//								 cotizacionDTO.setIdDomicilioContratante(cotizacionDTO.getIdDomicilioAsegurado());								 
								cotizacionSN.modificar(cotizacionDTO);
							}
						}						
					} else if (tipoDireccion.equals("1")) {
						try {
//							DESARROLLO CURP							
//							if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
//								cliente = new ClienteDTO();
//								cliente.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
//								cliente.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
//								cliente = ClienteDN.getInstancia().verDetalleCliente(cliente, UtileriasWeb.obtieneNombreUsuario(request));
//							}else{
								cliente = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), UtileriasWeb.obtieneNombreUsuario(request));					
//							}
							
						} catch (Exception exc) {
						}
						if (cliente == null)
							cliente = new ClienteDTO();
						ClienteAction.poblarClienteForm(cliente, cotizacionForm
								.getClienteGeneral());
						/*
						 * DireccionAction.poblarDireccionForm(cotizacionForm.getDireccionGeneral
						 * (), cotizacionDTO.getDireccionContratanteDTO());
						 * poblarPersonaForm
						 * (cotizacionForm.getPersonaGeneral(),cotizacionDTO
						 * .getPersonaContratanteDTO());
						 */
					} else if (tipoDireccion.equals("2")) {
						cotizacionForm.setDireccionCobro(new DireccionForm());
						DireccionAction.poblarDireccionForm(cotizacionForm
								.getDireccionCobro(), cotizacionDTO
								.getDireccionCobroDTO());
					}
					/*
					 * cotizacionForm.getDireccionGeneral().setDescripcionPadre(descripcionPadre
					 * );
					 * cotizacionForm.getDireccionGeneral().setCodigoDireccion
					 * (tipoDireccion);
					 * cotizacionForm.getDireccionGeneral().setIdPadre(idPadre);
					 * cotizacionForm
					 * .getPersonaGeneral().setDescripcionPadre(descripcionPadre
					 * );cotizacionForm.getPersonaGeneral().setCodigoPersona(
					 * tipoDireccion);
					 * cotizacionForm.getPersonaGeneral().setIdPadre(idPadre);
					 */
					cotizacionForm.getClienteGeneral().setDescripcionPadre(
							descripcionPadre);
					// cotizacionForm.getClienteGeneral().setTipoDireccion(tipoDireccion);
					cotizacionForm.getClienteGeneral().setCodigoPersona(
							tipoDireccion);
					cotizacionForm.getClienteGeneral().setIdPadre(idPadre);
					// Se usar� un "0" para identificar que es un nuevo registro
					if (UtileriasWeb.esCadenaVacia(cotizacionForm
							.getClienteGeneral().getIdCliente()))
						cotizacionForm.getClienteGeneral().setIdCliente("0");
					/*
					 * if(UtileriasWeb.esCadenaVacia(cotizacionForm.
					 * getDireccionGeneral().getIdToDireccion()))
					 * cotizacionForm.
					 * getDireccionGeneral().setIdToDireccion("0"); if
					 * (UtileriasWeb
					 * .esCadenaVacia(cotizacionForm.getPersonaGeneral
					 * ().getIdToPersona()))
					 * cotizacionForm.getPersonaGeneral().setIdToPersona("0");
					 */
				}
			}
			if (request.getParameter("origen") != null) {
				request.setAttribute("origen", "COT");
			}
			String mensaje = request.getParameter("mensaje");
			String tipoMensaje = request.getParameter("tipoMensaje");
			if (!UtileriasWeb.esCadenaVacia(mensaje))
				cotizacionForm.setMensaje(mensaje);
			else
				cotizacionForm
						.setMensaje("Los datos se guardaron correctamente.");
			if (!UtileriasWeb.esCadenaVacia(tipoMensaje))
				cotizacionForm.setTipoMensaje(tipoMensaje);
			else
				cotizacionForm.setTipoMensaje(Sistema.EXITO);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarAgregarPersona(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idPadre = request.getParameter("idPadre");
		String codigoPersona = request.getParameter("codigoPersona");
		String idToPersona = request.getParameter("idToPersona");
		String descripcionPadre = request.getParameter("descripcionPadre");
		PersonaForm personaForm = (PersonaForm) form;
		try {
			if (!UtileriasWeb.esCadenaVacia(idToPersona))
				// Si el id de la direccion es diferente de cero, buscar el
				// registro para mostrarlo en la JSP
				if (!idToPersona.equals("0"))
					poblarPersonaForm(
							personaForm,
							PersonaDN
									.getInstancia()
									.getPorId(
											UtileriasWeb
													.regresaBigDecimal(idToPersona)));
			// Verificar a qu� entidad pertenecer� la persona
			if (!UtileriasWeb.esCadenaVacia(descripcionPadre)
					&& !UtileriasWeb.esCadenaVacia(idPadre)
					&& !UtileriasWeb.esCadenaVacia(codigoPersona)) {
				personaForm.setIdPadre(idPadre);
				personaForm.setCodigoPersona(codigoPersona);
				personaForm.setDescripcionPadre(descripcionPadre);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarBuscarPersona(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idPadre = request.getParameter("idPadre");
		String codigo = request.getParameter("codigoPersona");
		ClienteForm clienteForm = (ClienteForm) form;
		// Verificar a qu� entidad pertenecer� la persona
		if (!UtileriasWeb.esCadenaVacia(idPadre)) {
			clienteForm.setIdPadre(idPadre);
			clienteForm.setCodigoPersona(codigo);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void agregarPersona(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		PersonaForm personaForm = (PersonaForm) form;
		PersonaDTO personaDTO = new PersonaDTO();
		PersonaDN personaDN = PersonaDN.getInstancia();
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
		try {
			if (!UtileriasWeb.esCadenaVacia(personaForm.getIdToPersona())) {
				if (!personaForm.getIdToPersona().equals("0")) { // Actualizacion
																	// de
																	// registro
					personaDTO = personaDN.getPorId(UtileriasWeb
							.regresaBigDecimal(personaForm.getIdToPersona()));
					poblarPersonaDTO(personaDTO, personaForm);
					personaDN.modificar(personaDTO);
					buffer
							.append("1</id><description><![CDATA[Los datos se actualizaron correctamente.]]></description>");
				} else { // Nuevo registro
					poblarPersonaDTO(personaDTO, personaForm);
					personaDTO.setIdToPersona(null);
					personaDTO = personaDN.agregar(personaDTO);
					buffer
							.append("1</id><description><![CDATA[Los datos se registraron correctamente.]]></description>");
				}
			}
			// Si se recibe un idPadre, se verifica la descripcion del padre y
			// se registrar� la nueva persona a la entidad correspondiente.
			// La persona ser� la correspondiente al c�digo contenido en el
			// form.
			if (!UtileriasWeb.esCadenaVacia(personaForm.getIdPadre())
					&& !UtileriasWeb.esCadenaVacia(personaForm
							.getDescripcionPadre())) {
				if (personaForm.getDescripcionPadre().equals("cotizacion")) {
					CotizacionDTO cotizacionDTO = new CotizacionDTO();
					cotizacionDTO.setIdToCotizacion(UtileriasWeb
							.regresaBigDecimal(personaForm.getIdPadre()));
					CotizacionSN cotizacionSN = new CotizacionSN(UtileriasWeb
							.obtieneNombreUsuario(request));
					cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
					cotizacionDTO = cotizacionSN
							.obtenerDatosForaneos(cotizacionDTO);
					cotizacionSN.modificar(cotizacionDTO);

					cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
					if (cotizacionDTO.getIdToPersonaAsegurado() != null
							&& cotizacionDTO.getIdToPersonaAsegurado()
									.doubleValue() > 0) {
						if (cotizacionDTO.getIdToPersonaContratante() == null
								|| cotizacionDTO.getIdToPersonaContratante()
										.doubleValue() == 0) {
							cotizacionDTO
									.setIdToPersonaContratante(cotizacionDTO
											.getIdToPersonaAsegurado());
							cotizacionDTO.setNombreContratante(cotizacionDTO
									.getNombreAsegurado());
							cotizacionSN.modificar(cotizacionDTO);
						}
					}
					switch (Integer.valueOf(personaForm.getCodigoPersona())) {
					case 1: // "1"-Persona contratante.
						// TODO se cambi� la tabla de cotizacion
						// cotizacionDTO.setPersonaContratanteDTO(personaDTO);
						break;
					case 3: // "3"-Persona asegurado
						// TODO se cambi� la tabla de cotizacion
						// cotizacionDTO.setPersonaAseguradoDTO(personaDTO);
						break;
					}

				} else if (personaForm.getDescripcionPadre().startsWith(
						"proveedorInspeccion")) {
					// Si es un nuevo proveedor, a�n no se tiene su ID, se usar�
					// la sesi�n para guardar el DTO.
					if (personaForm.getDescripcionPadre().endsWith("Nuevo")) {
						request.getSession().setAttribute("personaProveedor",
								personaDTO);
					} else {
						ProveedorInspeccionDTO proveedorDTO = new ProveedorInspeccionDTO();
						proveedorDTO = ProveedorInspeccionDN.getINSTANCIA()
								.getPorId(
										UtileriasWeb
												.regresaBigDecimal(personaForm
														.getIdPadre()));
						proveedorDTO = ProveedorInspeccionDN.getINSTANCIA()
								.poblarPersonaDTO(proveedorDTO);
						proveedorDTO = ProveedorInspeccionDN.getINSTANCIA()
								.poblarDireccionDTO(proveedorDTO);
						proveedorDTO.setPersonaDTO(personaDTO);
						ProveedorInspeccionDN.getINSTANCIA().modificar(
								proveedorDTO);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			buffer
					.append("2</id><description><![CDATA[Ocurri\u00f3 un error al registrar a la persona.\nVerifique los datos.]]></description>");
		}
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	DESARROLLO CURP	
//	public void asignarCotizacionV2(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
//        StringBuffer buffer = new StringBuffer();
//        String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
//        String idCliente = request.getParameter("idCliente");
//        String idToCotizacion = request.getParameter("idToCotizacion");
//        String codigoCliente = request.getParameter("codigoCliente");
//        String idDomicilio = request.getParameter("idDomicilio");        
//        
//        buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
//        try {
//        	
//            CotizacionDTO cotizacionDTO = new CotizacionDTO();
//            cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario)
//                .getPorId(UtileriasWeb.regresaBigDecimal(idToCotizacion));
//            if (idCliente != null && idDomicilio != null){
//                //Registro existente.
//                if (!idCliente.equals("0") && !idDomicilio.equals("0")){
//
//                    ClienteDN clienteDN = ClienteDN.getInstancia();
//                    ClienteDTO clienteDTO = new ClienteDTO();
//                    
//                    clienteDTO.setIdCliente(UtileriasWeb.regresaBigDecimal(idCliente));
//                    clienteDTO.setIdDomicilio(UtileriasWeb.regresaBigDecimal(idDomicilio));
//                    
//                    clienteDTO = clienteDN.verDetalleCliente(clienteDTO, UtileriasWeb.obtieneNombreUsuario(request));
//                    
//                    String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
//                    nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
//                    nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
//
//                    // 3 = Asegurado ; 1 = Contratante
//                    if (codigoCliente.equals("3")){
//                        cotizacionDTO.setIdToPersonaAsegurado(UtileriasWeb.regresaBigDecimal(idCliente));                                                
//                        cotizacionDTO.setNombreAsegurado(nombreCliente);
//                        cotizacionDTO.setIdDomicilioAsegurado(UtileriasWeb.regresaBigDecimal(idDomicilio));                              
//                    }
//                    else if(codigoCliente.equals("1")){
//                        cotizacionDTO.setIdToPersonaContratante(UtileriasWeb.regresaBigDecimal(idCliente));
//                        cotizacionDTO.setNombreContratante(nombreCliente);
//                        cotizacionDTO.setIdDomicilioContratante(UtileriasWeb.regresaBigDecimal(idDomicilio));
//                    }
//                }
//                //Nuevo Registro
//                else{
//                    ClienteDTO clienteDTO = new ClienteDTO();
//                    CotizacionForm cotizacion = (CotizacionForm) form;
//                    if (cotizacion.getClienteGeneral() != null){
//                        ClienteAction.poblarClienteDTO(cotizacion.getClienteGeneral(), clienteDTO);
//                        BigDecimal idClienteRegistrado = ClienteDN.getInstancia().agregar(clienteDTO, nombreUsuario);
//                        String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
//                        nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
//                        nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
//                        
//                        if (idClienteRegistrado != null){
//                            // 3 = Asegurado ; 1 = Contratante
//                        	if (codigoCliente.equals("3")){
//                                cotizacionDTO.setIdToPersonaAsegurado(idClienteRegistrado);
//                                cotizacionDTO.setNombreAsegurado(nombreCliente);                          
//                            }
//                            else if(codigoCliente.equals("1")){
//                                cotizacionDTO.setIdToPersonaContratante(idClienteRegistrado);
//                                cotizacionDTO.setNombreContratante(nombreCliente);
//                            }
//                        }
//                        else    throw new Exception();    
//                    }
//                }
//                CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
//                buffer.append("1</id><description><![CDATA[La operaci\u00f3n se realiz\u00f3 correctamente.]]></description>");
//            }
//        } catch (SystemException e) {
//            e.printStackTrace();
//        } catch (Exception e){
//            buffer.append("2</id><description><![CDATA[Ocurri\u00f3 un error al registrar al cliente.\nVerifique los datos.]]></description>");
//        }
//        buffer.append("</item></response>");
//        response.setContentType("text/xml");
//        response.setHeader("Cache-Control", "no-cache");
//        response.setContentLength(buffer.length());
//        try {
//            response.getWriter().write(buffer.toString());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

	public void asignarCliente(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		StringBuffer buffer = new StringBuffer();
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		String idCliente = request.getParameter("idCliente");
		String idToCotizacion = request.getParameter("idToCotizacion");
		String codigoCliente = request.getParameter("codigoCliente");
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
		try {

			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(
					UtileriasWeb.regresaBigDecimal(idToCotizacion));
			if (idCliente != null) {
				// Registro existente.
				if (!idCliente.equals("0")) {
//					DESARROLLO CURP
//					ClienteDTO clienteDTO = null;
//					// 3 = Asegurado ; 1 = Contratante
//					if (codigoCliente.equals("3")) {
//						if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
//							clienteDTO = new ClienteDTO();
//							clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//							clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
//							clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, nombreUsuario);
//						}else{
//							clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), nombreUsuario);
//						}											
//					}else if (codigoCliente.equals("1")) {
//						if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
//							clienteDTO = new ClienteDTO();
//							clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
//							clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
//							clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, nombreUsuario);
//						}else{
//							clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), nombreUsuario);
//						}											
//					}
					
					//**************Eliminar cuando se integre el desarrollo de CURP**************
					ClienteDN clienteDN = ClienteDN.getInstancia();
                    ClienteDTO clienteDTO = clienteDN.verDetalleCliente(UtileriasWeb.regresaBigDecimal(idCliente), nombreUsuario);
					//****************************************************************************
					
					String nombreCliente = clienteDTO.getNombre() != null ? clienteDTO
							.getNombre()
							+ " "
							: "";
					nombreCliente += clienteDTO.getApellidoPaterno() != null ? clienteDTO
							.getApellidoPaterno()
							+ " "
							: "";
					nombreCliente += clienteDTO.getApellidoMaterno() != null ? clienteDTO
							.getApellidoMaterno()
							: "";

//					DESARROLLO CURP
//					// 3 = Asegurado ; 1 = Contratante
//					if (codigoCliente.equals("3")) {
//						cotizacionDTO.setIdToPersonaAsegurado(UtileriasWeb
//								.regresaBigDecimal(idCliente));
//						cotizacionDTO.setIdDomicilioAsegurado(clienteDTO.getIdDomicilio());
//						cotizacionDTO.setNombreAsegurado(nombreCliente);
//					} else if (codigoCliente.equals("1")) {
//						cotizacionDTO.setIdToPersonaContratante(UtileriasWeb
//								.regresaBigDecimal(idCliente));
//						cotizacionDTO.setIdDomicilioContratante(clienteDTO.getIdDomicilio());						
//						cotizacionDTO.setNombreContratante(nombreCliente);
//					}
					
					//**************Eliminar cuando se integre el desarrollo de CURP**************
					// 3 = Asegurado ; 1 = Contratante
		            if (codigoCliente.equals("3")){
		            	cotizacionDTO.setIdToPersonaAsegurado(UtileriasWeb.regresaBigDecimal(idCliente));                                                
		                cotizacionDTO.setNombreAsegurado(nombreCliente);
		            }
		            else if(codigoCliente.equals("1")){
		            	cotizacionDTO.setIdToPersonaContratante(UtileriasWeb.regresaBigDecimal(idCliente));
		                cotizacionDTO.setNombreContratante(nombreCliente);
		            }
		          //****************************************************************************
				}
				// Nuevo Registro
				else {
					ClienteDTO clienteDTO = new ClienteDTO();
					CotizacionForm cotizacion = (CotizacionForm) form;
					if (cotizacion.getClienteGeneral() != null) {
						ClienteAction.poblarClienteDTO(cotizacion
								.getClienteGeneral(), clienteDTO);
						BigDecimal idClienteRegistrado = ClienteDN
								.getInstancia().agregar(clienteDTO,
										nombreUsuario);
						String nombreCliente = clienteDTO.getNombre() != null ? clienteDTO
								.getNombre()
								+ " "
								: "";
						nombreCliente += clienteDTO.getApellidoPaterno() != null ? clienteDTO
								.getApellidoPaterno()
								+ " "
								: "";
						nombreCliente += clienteDTO.getApellidoMaterno() != null ? clienteDTO
								.getApellidoMaterno()
								: "";

						if (idClienteRegistrado != null) {
							// 3 = Asegurado ; 1 = Contratante
							if (codigoCliente.equals("3")) {
								cotizacionDTO
										.setIdToPersonaAsegurado(idClienteRegistrado);
								cotizacionDTO.setNombreAsegurado(nombreCliente);
							} else if (codigoCliente.equals("1")) {
								cotizacionDTO
										.setIdToPersonaContratante(idClienteRegistrado);
								cotizacionDTO
										.setNombreContratante(nombreCliente);
							}
						} else
							throw new Exception();
					}
				}
				CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request)).modificar(
						cotizacionDTO);
				buffer
						.append("1</id><description><![CDATA[La operaci\u00f3n se realiz\u00f3 correctamente.]]></description>");
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			buffer
					.append("2</id><description><![CDATA[Ocurri\u00f3 un error al registrar al cliente.\nVerifique los datos.]]></description>");
		}
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method cargarSeccionesPorInciso
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return void
	 */

	public void cargarSeccionesPorInciso(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// Funciona en conjunto con el grid javascript (en la funci�n
		// mostrarSeccionesPorInciso(id){}) para cargar las secciones dado un
		// inciso

		String numeroIncisoStr = request.getParameter("numeroInciso");
		String idToCotizacionStr = request.getParameter("idToCotizacion");
		List<SeccionCotizacionDTO> seccionCotizacionDTOList;
		if (!UtileriasWeb.esCadenaVacia(numeroIncisoStr)
				&& !UtileriasWeb.esCadenaVacia(idToCotizacionStr)) {
			try {
				BigDecimal numeroInciso = UtileriasWeb
						.regresaBigDecimal(numeroIncisoStr);
				BigDecimal idToCotizacion = UtileriasWeb
						.regresaBigDecimal(idToCotizacionStr);

				seccionCotizacionDTOList = SeccionCotizacionDN.getInstancia()
						.listarPorCotizacionNumeroInciso(idToCotizacion,
								numeroInciso);

				String json = getJsonSecciones(seccionCotizacionDTOList);
				response.setContentType("text/json");
				PrintWriter pw;
				pw = response.getWriter();

				pw.write(json);
				pw.flush();
				pw.close();

			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (IOException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
		}

	}

	/**
	 * Method modificarSeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return void
	 */
	public void modificarSeccion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// Funciona en conjunto con el grid javascript (en la funci�n
		// mostrarSeccionesPorInciso(id){}) para actualizar el estatus de
		// obligatoriedad de una secci�n de un inciso
		String claveObligatoriedadStr = request.getParameter("obligatoriedad");
		String claveContratoStr = request.getParameter("contratada");

		if (!UtileriasWeb.esCadenaVacia(claveObligatoriedadStr)) {
			Integer claveObligatoriedad = new Integer(claveObligatoriedadStr);
			if (claveObligatoriedad == 0 || claveObligatoriedad == 1) {
				String id = request.getParameter("idSeccionCot");
				String[] ids = id.split(",");
				BigDecimal idToCotizacion = new BigDecimal(ids[0]);
				BigDecimal numeroInciso = new BigDecimal(ids[1]);
				BigDecimal idToSeccion = new BigDecimal(ids[2]);
				SeccionCotizacionDTOId seccionCotizacionDTOId = new SeccionCotizacionDTOId(
						idToCotizacion, numeroInciso, idToSeccion);
				CotizacionDN cotizacionDN = CotizacionDN
						.getInstancia(UtileriasWeb
								.obtieneNombreUsuario(request));
				SeccionCotizacionDTO seccionCotizacionDTO;
				try {
					seccionCotizacionDTO = cotizacionDN
							.getSeccionCotizacionPorId(seccionCotizacionDTOId);

					Short claveContrato = new Short("0");
					if (claveContratoStr.equals("true"))
						claveContrato = 1;
					IncisoCotizacionDTO incisoCot = new IncisoCotizacionDTO();
					incisoCot.setId(new IncisoCotizacionId());
					incisoCot.getId().setIdToCotizacion(idToCotizacion);
					incisoCot.getId().setNumeroInciso(numeroInciso);
					incisoCot = IncisoCotizacionDN.getInstancia().getPorId(
							incisoCot);
					seccionCotizacionDTO.setIncisoCotizacionDTO(incisoCot);
					seccionCotizacionDTO.setClaveContrato(claveContrato);
					cotizacionDN
							.actualizarSeccionCotizacion(seccionCotizacionDTO);

					if (seccionCotizacionDTO.getClaveContrato() == 0) {
						// SE AGREGA ELIMINACION DE DETALLESPRIMAS COBERTURAS Y
						// DETALLESPRIMASRIESGO
						CotizacionDN
								.getInstancia("")
								.eliminaDetallesPrimasDeCoberturayRiesgoPorSeccion(
										seccionCotizacionDTO);
					}
					// eliminarSubIncisos
					if (seccionCotizacionDTO.getClaveContrato() == Sistema.NO_CONTRATADO) {
						SubIncisoCotizacionDN.getInstancia()
								.borrarSubincisosSeccionCotizacion(
										seccionCotizacionDTO);
						DetallePrimaCoberturaCotizacionDN.getInstancia()
								.eliminarDetallePrimaSeccionCotizacion(
										seccionCotizacionDTO);
					}
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(
							e.getMessage(), request);
				}

			}
		}

	}

	/**
	 * Method mostrarModificarSecciones
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarModificarSecciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// Funciona en conjunto con el grid javascript (en la funci�n
		// mostrarSeccionesPorInciso(id){}) para actualizar el estatus de
		// obligatoriedad de una secci�n de un inciso
		String reglaNavegacion = Sistema.EXITOSO;
		SeccionCotForm seccionCotForm = (SeccionCotForm) form;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		String idToCotizacion = request.getParameter("idToCotizacion");
		if (!UtileriasWeb.esObjetoNulo(idToCotizacion)
				&& !UtileriasWeb.esCadenaVacia(idToCotizacion)) {
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			try {
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				seccionCotForm.setIdToCotizacion(cotizacionDTO
						.getIdToCotizacion().toString());
				seccionCotForm.setFechaCreacion(dateFormat.format(cotizacionDTO
						.getFechaCreacion()));
				request.setAttribute("idToCotizacion", UtileriasWeb
						.llenarIzquierda(idToCotizacion, "0", 8));
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null)
					seccionCotForm.setClaveTipoEndoso(cotizacionDTO
							.getSolicitudDTO().getClaveTipoEndoso().toString());

			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
		} else
			reglaNavegacion = Sistema.NO_EXITOSO;

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarDatosGenerales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDatosGenerales(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;

		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String tipoMensaje = request.getParameter("tipoMensaje");
		String mensaje = request.getParameter("mensaje");

		try {
			if (!UtileriasWeb.esObjetoNulo(tipoMensaje)
					&& !UtileriasWeb.esObjetoNulo(mensaje)) {
				cotizacionForm.setTipoMensaje(tipoMensaje);
				cotizacionForm.setMensaje(mensaje);
			}
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm, request);

			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
			documentoDigitalSolicitudDTOList = DocumentoDigitalSolicitudDN
					.getInstancia().listarDocumentosDigitalesPorSolicitud(
							cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			request.setAttribute("documentosList",
					documentoDigitalSolicitudDTOList);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarDocumentosDigitalesComplementarios
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarDocumentosDigitalesComplementarios(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTOList = new ArrayList<DocumentoDigitalCotizacionDTO>();
		List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
		CotizacionSN cotizacionSN;
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;

		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb
					.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);

			this.poblarForm(cotizacionDTO, cotizacionForm, request);
			cotizacionForm.setIdToCotizacion(idToCotizacion);

			documentoDigitalCotizacionDTOList = DocumentoDigitalCotizacionDN
					.getInstancia().listarDocumentosCotizacionPorCotizacion(
							cotizacionDTO.getIdToCotizacion());
			documentoDigitalSolicitudDTOList = DocumentoDigitalSolicitudDN
					.getInstancia().listarDocumentosDigitalesPorSolicitud(
							cotizacionDTO.getSolicitudDTO().getIdToSolicitud());

		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		request.setAttribute("documentosDigitalesSolicitudList",
				documentoDigitalSolicitudDTOList);
		request.setAttribute("documentosDigitalesCotizacionList",
				documentoDigitalCotizacionDTOList);

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method borrarDocumentoComplementario
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrarDocumentoComplementario(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		ActionForward forward = new ActionForward(mapping
				.findForward(Sistema.EXITOSO));
		String idToControlArchivo = request.getParameter("idControlArchivo");
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idDocumentoDigitalCotizacion = request
				.getParameter("idDocumentoDigitalCotizacion");
		if (idToCotizacion != null
				&& !UtileriasWeb.esCadenaVacia(idToCotizacion)
				&& idToControlArchivo != null
				&& !UtileriasWeb.esCadenaVacia(idToControlArchivo)
				&& idDocumentoDigitalCotizacion != null
				&& !UtileriasWeb.esCadenaVacia(idDocumentoDigitalCotizacion)) {
			DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO = new DocumentoDigitalCotizacionDTO();
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();

			try {
				documentoDigitalCotizacionDTO = DocumentoDigitalCotizacionDN
						.getInstancia().getPorId(
								new BigDecimal(idDocumentoDigitalCotizacion));
				DocumentoDigitalCotizacionDN.getInstancia().borrar(
						documentoDigitalCotizacionDTO);

				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(
						new BigDecimal(idToControlArchivo));
				ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);
				String path;
				path = forward.getPath();
				forward.setPath(path + "?id=" + idToCotizacion);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
				forward = new ActionForward(mapping
						.findForward(Sistema.NO_EXITOSO));
			}
		}

		return forward;
	}

	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarRegistrarInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarRegistrarInspeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		InspeccionForm inspeccionForm = (InspeccionForm) form;
		String idToCotizacion = request.getParameter("idToCotizacion");
		String numeroInciso = request.getParameter("numeroInciso");
		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
//		Usuario usuario = getUsuarioMidas(request);
		// PersonaDTO asegurado = new PersonaDTO();
		AgenteDTO agente = new AgenteDTO();
		ClienteDTO clienteAsegurado = new ClienteDTO();
		try {

			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
					new BigDecimal(idToCotizacion));
			// TODO Se cambi� la table, verificar
			// asegurado = cotizacionDTO.getPersonaAseguradoDTO();

			try {
//				DESARROLLO CURP
//				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
//
//					clienteAsegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//					clienteAsegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
//					clienteAsegurado = ClienteDN.getInstancia().verDetalleCliente(clienteAsegurado, UtileriasWeb.obtieneNombreUsuario(request));
//				}else{
					clienteAsegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
//				}				
			} catch (Exception exc) {
			}

			try {
				Usuario usuario2 = (Usuario) UtileriasWeb
						.obtenValorSessionScope(request,
								Sistema.USUARIO_ACCESO_MIDAS);
				AgenteDN agenteDN = AgenteDN.getInstancia();
				agente = new AgenteDTO();
				agente.setIdTcAgente(Integer.valueOf(cotizacionDTO
						.getSolicitudDTO().getCodigoAgente().toBigInteger()
						.toString()));
				agente = agenteDN.verDetalleAgente(agente, usuario2
						.getNombreUsuario());// getPorId(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdAgente()));
				// agente =
				// AgenteDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getCodigoAgente());
			} catch (Exception e) {
			}

			IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
			incisoCotizacionId.setIdToCotizacion(cotizacionDTO
					.getIdToCotizacion());
			incisoCotizacionId.setNumeroInciso(new BigDecimal(numeroInciso));

			incisoCotizacionDTO.setId(incisoCotizacionId);
			incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().getPorId(
					incisoCotizacionDTO);
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = null;
			List<InspeccionIncisoCotizacionDTO> inspeccionIncisoCotizacionList = InspeccionIncisoCotizacionDN
					.getINSTANCIA().getPorIdIncisoCotizacion(
							incisoCotizacionDTO);
			if (inspeccionIncisoCotizacionList.size() > 0) {
				inspeccionIncisoCotizacionDTO = inspeccionIncisoCotizacionList
						.get(0); // En teor�a solo deber�a regresar 1 registro,
									// aunque el m�todo utilizado en el DN y el
									// SN devuelve una lista
			}

			poblarInspeccionForm(clienteAsegurado, agente, incisoCotizacionDTO
					.getDireccionDTO(), inspeccionForm,
					inspeccionIncisoCotizacionDTO, request);

			List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentosAnexosList = new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>();
			if (!UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO)
					&& !UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO
							.getIdToInspeccionIncisoCotizacion()))
				documentosAnexosList = DocumentoAnexoInspeccionIncisoCotizacionDN
						.getINSTANCIA().listarPorInspeccion(
								inspeccionIncisoCotizacionDTO);

			request.setAttribute("documentosAnexosList", documentosAnexosList);

		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(Sistema.EXITOSO);
	}

	public void poblarInspeccionForm(ClienteDTO asegurado, AgenteDTO agente,
			DireccionDTO direccionInciso, InspeccionForm inspeccionForm,
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO,
			HttpServletRequest request) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		inspeccionForm.setFechaActual(dateFormat.format(new Date()));

		// Datos de asegurado y agente
		inspeccionForm.setNombre(asegurado.getNombre() + " "
				+ asegurado.getApellidoPaterno() + " "
				+ asegurado.getApellidoMaterno());
		inspeccionForm
				.setTipoDePersona((asegurado.getClaveTipoPersona() == 1) ? "Persona f�sica"
						: (asegurado.getClaveTipoPersona() == 2) ? "Persona moral"
								: "");
		inspeccionForm.setTelefono(asegurado.getTelefono());
		if (agente != null)
			inspeccionForm.setAgenteDeSeguros(agente.getNombre());
		else
			inspeccionForm.setAgenteDeSeguros("No registrado");

		// Datos de la Ubicacion
		if (!UtileriasWeb.esObjetoNulo(direccionInciso)) {
			inspeccionForm.setCalleYNumero(direccionInciso.getNombreCalle()
					+ " " + direccionInciso.getNumeroExterior());

			inspeccionForm.setColonia(direccionInciso.getNombreColonia());
			inspeccionForm.setCodigoPostal(direccionInciso.getCodigoPostal()
					.toString());
			CiudadDTO ciudad = CodigoPostalDN.getInstancia().getCiudadPorId(
					obtenerId(direccionInciso.getIdMunicipio().toBigInteger()
							.toString()));
			EstadoDTO estado = CodigoPostalDN.getInstancia().getEstadoPorId(
					obtenerId(direccionInciso.getIdEstado().toBigInteger()
							.toString()));
			inspeccionForm.setCiudad(ciudad.getCityName());
			inspeccionForm.setEstado(estado.getStateName());
		}

		if (!UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO)) {
			if (!UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO
					.getIdToInspeccionIncisoCotizacion()))
				inspeccionForm
						.setIdToInspeccionIncisoCotizacion(inspeccionIncisoCotizacionDTO
								.getIdToInspeccionIncisoCotizacion()
								.toBigInteger().toString());
			else
				inspeccionForm.setIdToInspeccionIncisoCotizacion("-1");

			inspeccionForm.setNumeroDeReporte(inspeccionIncisoCotizacionDTO
					.getNumeroReporte().toBigInteger().toString());
			inspeccionForm
					.setIdProveedorInspeccion(inspeccionIncisoCotizacionDTO
							.getProveedorInspeccion()
							.getIdToProveedorInspeccion().toBigInteger()
							.toString());
			inspeccionForm.setPersonaEntrevistada(inspeccionIncisoCotizacionDTO
					.getNombrePersonaEntrevistada());
			inspeccionForm
					.setFechaDeLaInspeccion(dateFormat
							.format(inspeccionIncisoCotizacionDTO
									.getFechaInspeccion()));
			inspeccionForm.setFechaElaboracionDocumento(dateFormat
					.format(inspeccionIncisoCotizacionDTO
							.getFechaReporteInspeccion()));
			inspeccionForm
					.setResultadoDeLaInspeccion(inspeccionIncisoCotizacionDTO
							.getClaveResultadoInspeccion().toString());
			inspeccionForm
					.setComentariosDeLaInspeccion(inspeccionIncisoCotizacionDTO
							.getComentariosReporte());
		} else {
			inspeccionForm.setIdToInspeccionIncisoCotizacion("-1");
			inspeccionForm
					.setFechaDeLaInspeccion(dateFormat.format(new Date()));
			inspeccionForm.setFechaElaboracionDocumento(dateFormat
					.format(new Date()));
		}

		if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getIdToCotizacion())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getIdToCotizacion()))
			inspeccionForm.setIdToCotizacionFormateada("COT-"
					+ UtileriasWeb.llenarIzquierda(inspeccionForm
							.getIdToCotizacion(), "0", 8));
		else
			inspeccionForm.setIdToCotizacionFormateada("No definida");

	}

	public String obtenerId(String id) {
		return StringUtils.leftPad(id, 5, '0');	
	}

	/**
	 * Method guardarReporteInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarReporteInspeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		InspeccionForm inspeccionForm = (InspeccionForm) form;
		InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = new InspeccionIncisoCotizacionDTO();
		request.setAttribute("documentosAnexosList",
				new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>());
		Usuario usuario = getUsuarioMidas(request);
		this.poblarInspeccionIncisoCotizacionDTO(inspeccionForm,
				inspeccionIncisoCotizacionDTO, request);

		try {
			inspeccionIncisoCotizacionDTO
					.setDocumentoAnexoInspeccionIncisoCotizacionList(null);
			inspeccionIncisoCotizacionDTO = InspeccionIncisoCotizacionDN
					.getINSTANCIA().agregar(inspeccionIncisoCotizacionDTO);

			// //////////////////////////////////////////////////////////////////////
			IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
			try {
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				incisoCotizacionId.setIdToCotizacion(new BigDecimal(
						inspeccionForm.getIdToCotizacion()));
				incisoCotizacionId.setNumeroInciso(new BigDecimal(
						inspeccionForm.getNumeroInciso()));
				incisoCotizacion.setId(incisoCotizacionId);
				incisoCotizacion = IncisoCotizacionDN.getInstancia().getPorId(
						incisoCotizacion);

				Short claveInspeccionParaInciso = Sistema.ESTATUS_INSPECCION_INCISO_REQUERIDO;

				if (inspeccionForm.getResultadoDeLaInspeccion() != null) {
					claveInspeccionParaInciso = Short.parseShort(inspeccionForm
							.getResultadoDeLaInspeccion());
				}

				incisoCotizacion
						.setClaveEstatusInspeccion(claveInspeccionParaInciso);

				IncisoCotizacionDN.getInstancia().modificar(incisoCotizacion);

			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
			// //////////////////////////////////////////////////////////////////////

			// PersonaDTO asegurado =
			// null;//inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getPersonaAseguradoDTO();
			AgenteDTO agente = new AgenteDTO();
			try {
				AgenteDN agenteDN = AgenteDN.getInstancia();
				agente.setIdTcAgente(Integer
						.valueOf(inspeccionIncisoCotizacionDTO
								.getIncisoCotizacion().getCotizacionDTO()
								.getSolicitudDTO().getCodigoAgente()
								.toBigInteger().toString()));
				agente = agenteDN.verDetalleAgente(agente, usuario
						.getNombreUsuario());// getPorId(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdAgente()));
				// agente =
				// AgenteDN.getInstancia().getPorId(inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
			} catch (Exception e) {
			}
			DireccionDTO direccionInciso = inspeccionIncisoCotizacionDTO
					.getIncisoCotizacion().getDireccionDTO();
			ClienteDTO asegurado = new ClienteDTO();
			try {
//				DESARROLLO CURP
//				CotizacionDTO cotizacionDTO = inspeccionIncisoCotizacionDTO
//				.getIncisoCotizacion().getCotizacionDTO();				
//				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
//					asegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//					asegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
//					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, usuario.getNombreUsuario());
//				}else{
//					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), usuario.getNombreUsuario());
//				}
				
				//*************Eliminar cuando se integre desarrollo de CURP************
				asegurado = ClienteDN.getInstancia().verDetalleCliente(inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getIdToCotizacion(), usuario.getNombreUsuario());
				//**********************************************************************
			} catch (Exception exc) {
			}
			this.poblarInspeccionForm(asegurado, agente, direccionInciso,
					inspeccionForm, inspeccionIncisoCotizacionDTO, request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method modificarReporteInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificarReporteInspeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		@SuppressWarnings("unused")
		String reglaNavegacion = Sistema.EXITOSO;

		ActionForward forward;

		InspeccionForm inspeccionForm = (InspeccionForm) form;
		if (!UtileriasWeb.esObjetoNulo(inspeccionForm
				.getIdToInspeccionIncisoCotizacion())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getIdToInspeccionIncisoCotizacion())) {
			try {
				InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = new InspeccionIncisoCotizacionDTO();
				inspeccionIncisoCotizacionDTO = InspeccionIncisoCotizacionDN
						.getINSTANCIA().getPorId(
								new BigDecimal(inspeccionForm
										.getIdToInspeccionIncisoCotizacion()));
				this.poblarInspeccionIncisoCotizacionDTO(inspeccionForm,
						inspeccionIncisoCotizacionDTO, request);

				List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentosAnexosList = DocumentoAnexoInspeccionIncisoCotizacionDN
						.getINSTANCIA().listarPorInspeccion(
								inspeccionIncisoCotizacionDTO);
				if (documentosAnexosList.size() > 0) {
					IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
					incisoCotizacion = inspeccionIncisoCotizacionDTO
							.getIncisoCotizacion();
					incisoCotizacion
							.setClaveEstatusInspeccion(inspeccionIncisoCotizacionDTO
									.getClaveResultadoInspeccion());
					IncisoCotizacionDN.getInstancia().modificar(
							incisoCotizacion);
				}
				inspeccionIncisoCotizacionDTO = InspeccionIncisoCotizacionDN
						.getINSTANCIA()
						.modificar(inspeccionIncisoCotizacionDTO);
				// PersonaDTO asegurado =
				// inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getPersonaAseguradoDTO();
				// AgenteDTO agente =
				// AgenteDN.getInstancia().getPorId(inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
				// DireccionDTO direccionInciso =
				// inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getDireccion();
				// this.poblarInspeccionForm(asegurado, agente, direccionInciso,
				// inspeccionForm, inspeccionIncisoCotizacionDTO, request);
				// request.setAttribute("documentosAnexosList",
				// documentosAnexosList);

			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
		} else
			reglaNavegacion = Sistema.NO_EXITOSO;

		// forward = new ActionForward(mapping.findForward("verCotizacion"));
		forward = new ActionForward();
		/*
		 * String path; path = forward.getPath();
		 */
		// forward.setPath(path + "?id=" + inspeccionForm.getIdToCotizacion());
		forward.setPath("/cotizacion/cotizacion/mostrar.do?id="
				+ inspeccionForm.getIdToCotizacion());
		return forward;
	}

	/**
	 * Method cambiarEstatusInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward cambiarEstatusInspeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("idToCotizacion");
		String numeroInciso = request.getParameter("numeroInciso");
		String claveEstatusInspeccion = request
				.getParameter("claveEstatusInspeccion");
		String claveTipoOrigenInspeccion = request
				.getParameter("claveTipoOrigenInspeccion");
		String claveAutInspeccion = request.getParameter("claveAutInspeccion");
		String claveMensajeInspeccion = request
				.getParameter("claveMensajeInspeccion");

		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(new BigDecimal(idToCotizacion));
		incisoCotizacionId.setNumeroInciso(new BigDecimal(numeroInciso));
		incisoCotizacionDTO.setId(incisoCotizacionId);

		Usuario usuario = getUsuarioMidas(request);
		try {
			incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().getPorId(
					incisoCotizacionDTO);
			incisoCotizacionDTO.setClaveEstatusInspeccion(new Short(
					claveEstatusInspeccion));
			incisoCotizacionDTO.setClaveTipoOrigenInspeccion(new Short(
					claveTipoOrigenInspeccion));
			incisoCotizacionDTO.setClaveAutInspeccion(new Short(
					claveAutInspeccion));
			incisoCotizacionDTO.setClaveMensajeInspeccion(new Short(
					claveMensajeInspeccion));

			incisoCotizacionDTO.setCodigoUsuarioEstInspeccion(usuario.getId()
					.toString());

			IncisoCotizacionDN.getInstancia().modificar(incisoCotizacionDTO);

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarDocumentosAnexosInspeccionIncisoCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarDocumentosAnexosInspeccionIncisoCotizacion(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;

		String idToInspeccionIncisoCotizacion = request.getParameter("id");
		InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion = new InspeccionIncisoCotizacionDTO();
		inspeccionIncisoCotizacion
				.setIdToInspeccionIncisoCotizacion(new BigDecimal(
						idToInspeccionIncisoCotizacion));
		List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentosAnexosList = new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>();
		try {
			documentosAnexosList = DocumentoAnexoInspeccionIncisoCotizacionDN
					.getINSTANCIA().listarPorInspeccion(
							inspeccionIncisoCotizacion);
			request.setAttribute("documentosAnexosList", documentosAnexosList);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarDocumentosAnexosInspeccionIncisoCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrarDocumentoAnexoInspeccionIncisoCotizacion(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		String idToDocAnexoInsp = request.getParameter("idToDocAnexoInsp");
		DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacionDTO = new DocumentoAnexoInspeccionIncisoCotizacionDTO();
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		try {
			documentoAnexoInspeccionIncisoCotizacionDTO
					.setIdToDocumentoAnexoInspeccionIncisoCotizacion(new BigDecimal(
							idToDocAnexoInsp));
			documentoAnexoInspeccionIncisoCotizacionDTO = DocumentoAnexoInspeccionIncisoCotizacionDN
					.getINSTANCIA().getPorId(
							documentoAnexoInspeccionIncisoCotizacionDTO);
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = documentoAnexoInspeccionIncisoCotizacionDTO
					.getInspeccionIncisoCotizacion();
			controlArchivoDTO = documentoAnexoInspeccionIncisoCotizacionDTO
					.getControlArchivo();
			ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);
			DocumentoAnexoInspeccionIncisoCotizacionDN.getINSTANCIA().borrar(
					documentoAnexoInspeccionIncisoCotizacionDTO);
			List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentosAnexosList = DocumentoAnexoInspeccionIncisoCotizacionDN
					.getINSTANCIA().listarPorInspeccion(
							inspeccionIncisoCotizacionDTO);
			request.setAttribute("documentosAnexosList", documentosAnexosList);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	protected void poblarForm(CotizacionDTO cotizacionDTO,
			CotizacionForm cotizacionForm, HttpServletRequest request)
			throws SystemException {
		if (cotizacionDTO.getIdToCotizacion() != null) {
			cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion()
					.toString());
			cotizacionForm.setIdToCotizacionFormateada("COT-"
					+ UtileriasWeb.llenarIzquierda(cotizacionForm
							.getIdToCotizacion(), "0", 8));
			cotizacionForm.setIdOrdenTrabajoFormateada("ODT-"
					+ UtileriasWeb.llenarIzquierda(cotizacionForm
							.getIdToCotizacion(), "0", 8));
		}
		cotizacionForm.setDireccionCobro(new DireccionForm());

		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);

		cotizacionForm.setClienteAsegurado(new ClienteForm());
		if (cotizacionDTO.getIdToPersonaAsegurado() != null) {
			ClienteDTO asegurado = null;
			try {
//				DESARROLLO CURP
//				if (cotizacionDTO.getIdDomicilioAsegurado() != null
//						&& cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0) {
//					asegurado = new ClienteDTO();
//					asegurado
//							.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//					asegurado.setIdDomicilio(cotizacionDTO
//							.getIdDomicilioAsegurado());
//					asegurado = ClienteDN.getInstancia().verDetalleCliente(
//							asegurado, cotizacionDTO.getCodigoUsuarioCotizacion());
//				} else {
					asegurado = ClienteDN.getInstancia().verDetalleCliente(
							cotizacionDTO.getIdToPersonaAsegurado(),
							cotizacionDTO.getCodigoUsuarioCotizacion());
//				}					
			} catch (Exception exc) {
				asegurado = new ClienteDTO();
			}
			if (asegurado != null)
				ClienteAction.poblarClienteForm(asegurado, cotizacionForm
						.getClienteAsegurado());
		}
		cotizacionForm.setClienteContratante(new ClienteForm());
		if (cotizacionDTO.getIdToPersonaContratante() != null) {
			ClienteDTO contratante = null;
			try {
//				DESARROLLO CURP
//				if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
//					contratante = new ClienteDTO();
//					contratante.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
//					contratante.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
//					contratante = ClienteDN.getInstancia().verDetalleCliente(contratante, UtileriasWeb.obtieneNombreUsuario(request));
//				}else{
					contratante = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
//				}
				
			} catch (Exception exc) {
				contratante = new ClienteDTO();
			}
			if (contratante != null)
				ClienteAction.poblarClienteForm(contratante, cotizacionForm
						.getClienteContratante());
		}
		Format formatter = null;
		if (cotizacionDTO.getFechaInicioVigencia() != null) {
			formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaInicioVigencia(formatter
					.format(cotizacionDTO.getFechaInicioVigencia()));
		}
		if (cotizacionDTO.getFechaFinVigencia() != null) {
			if (formatter == null)
				formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaFinVigencia(formatter.format(cotizacionDTO
					.getFechaFinVigencia()));
		}
		if (cotizacionDTO.getFechaCreacion() != null) {
			if (formatter == null)
				formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaCreacion(formatter.format(cotizacionDTO
					.getFechaCreacion()));
		}
		if (cotizacionDTO.getFechaModificacion() != null) {
			if (formatter == null)
				formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaModificacion(formatter.format(cotizacionDTO
					.getFechaModificacion()));
		}
		if (cotizacionDTO.getTipoPolizaDTO() != null) {
			if (cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null) {
				cotizacionForm.setIdToTipoPoliza(cotizacionDTO
						.getTipoPolizaDTO().getIdToTipoPoliza().toBigInteger()
						.toString());
				cotizacionForm.setListaMoneda(new MonedaSN()
						.listarMonedasAsociadasTipoPoliza(cotizacionDTO
								.getTipoPolizaDTO().getIdToTipoPoliza()));
				TipoPolizaDTO tipoPolizaDTO = TipoPolizaDN.getInstancia()
						.getPorId(
								cotizacionDTO.getTipoPolizaDTO()
										.getIdToTipoPoliza());
				cotizacionForm.setTipoDePoliza(tipoPolizaDTO.getDescripcion());
			}
		} else {
			cotizacionForm.setListaMoneda(new ArrayList<MonedaDTO>());
		}
		if (cotizacionDTO.getIdMoneda() != null) {
			cotizacionForm.setIdMoneda(cotizacionDTO.getIdMoneda()
					.toBigInteger().toString());
			Short idMoneda = new Short(cotizacionDTO.getIdMoneda()
					.toBigInteger().toString());
			MonedaDTO monedaDTO = MonedaDN.getInstancia().getPorId(idMoneda);
			cotizacionForm.setMoneda(monedaDTO.getDescripcion());
			List<FormaPagoIDTO> listaFormaPago = null;
			try {
				// 22/12/2009 JLAB, se debe obtener la lista de formas de pago
				// v�lidas para el tipo de moneda de la cotizaci�n.
				listaFormaPago = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN
						.getInstancia(nombreUsuario).listarTodos(idMoneda);
			} catch (Exception e) {
			}
			if (listaFormaPago == null)
				listaFormaPago = new ArrayList<FormaPagoIDTO>();
			cotizacionForm.setListaFormaPago(listaFormaPago);
		}
		if (cotizacionDTO.getIdFormaPago() != null) {
			cotizacionForm.setIdFormaPago(cotizacionDTO.getIdFormaPago()
					.toBigInteger().toString());
			FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
			formaPagoDTO.setIdFormaPago(new Integer(cotizacionDTO
					.getIdFormaPago().intValue()));
			formaPagoDTO = FormaPagoDN.getInstancia().getPorId(formaPagoDTO);
			cotizacionForm.setFormaPago(formaPagoDTO.getDescripcion());
		}
		if (cotizacionDTO.getIdMedioPago() != null) {
			cotizacionForm.setIdMedioPago(cotizacionDTO.getIdMedioPago()
					.toBigInteger().toString());
			MedioPagoDTO medioPagoDTO = new MedioPagoDTO();
			medioPagoDTO.setIdMedioPago(new Integer(cotizacionDTO
					.getIdMedioPago().intValue()));
			medioPagoDTO = MedioPagoDN.getInstancia().getPorId(medioPagoDTO);
			try {
				cotizacionForm.setMedioDePago(medioPagoDTO.getDescripcion());
			} catch (Exception exc) {
				cotizacionForm.setMedioDePago("No disponible");
			}
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionDTO
				.getNombreEmpresaContratante()))
			cotizacionForm.setNombreEmpresaContratante(cotizacionDTO
					.getNombreEmpresaContratante());
		if (!UtileriasWeb.esCadenaVacia(cotizacionDTO
				.getNombreEmpresaAsegurado()))
			cotizacionForm.setNombreEmpresaAsegurado(cotizacionDTO
					.getNombreEmpresaAsegurado());
		if (!UtileriasWeb.esCadenaVacia(cotizacionDTO
				.getCodigoUsuarioOrdenTrabajo()))
			cotizacionForm.setCodigoUsuarioOrdenTrabajo(cotizacionDTO
					.getCodigoUsuarioOrdenTrabajo());
		// Agente de seguros
		if (cotizacionDTO.getSolicitudDTO() != null) {
			if (cotizacionDTO.getSolicitudDTO().getIdToSolicitud() != null)
				cotizacionForm.setIdToSolicitud(cotizacionDTO.getSolicitudDTO()
						.getIdToSolicitud().toString());
			if (cotizacionDTO.getSolicitudDTO().getTelefonoContacto() != null)
				cotizacionForm.setTelefonoContacto(cotizacionDTO
						.getSolicitudDTO().getTelefonoContacto());
			ProductoDTO productoTMP = new ProductoSN()
					.encontrarPorSolicitud(cotizacionDTO.getSolicitudDTO()
							.getIdToSolicitud());
			cotizacionForm.setProducto(productoTMP.getNombreComercial());
			cotizacionForm.setIdToProducto(productoTMP.getIdToProducto()
					.toString());
			cotizacionForm
					.setListaTipoPoliza(new TipoPolizaSN()
							.listarVigentesPorIdProducto(productoTMP
									.getIdToProducto()));
			String nombreSolicitante = "NO DISPONIBLE";
			String apellidoMaterno = cotizacionDTO.getSolicitudDTO()
					.getApellidoMaterno() != null ? cotizacionDTO
					.getSolicitudDTO().getApellidoMaterno() : "";
			if (cotizacionDTO.getSolicitudDTO().getClaveTipoPersona()
					.intValue() == 1)
				nombreSolicitante = cotizacionDTO.getSolicitudDTO()
						.getNombrePersona()
						+ " "
						+ cotizacionDTO.getSolicitudDTO().getApellidoPaterno()
						+ " " + apellidoMaterno;
			else
				nombreSolicitante = cotizacionDTO.getSolicitudDTO()
						.getNombrePersona();
			cotizacionForm.setNombreSolicitante(nombreSolicitante);

			if (cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null) {
				try {
					AgenteDN agenteDN = AgenteDN.getInstancia();
					AgenteDTO agenteDTO = new AgenteDTO();
					agenteDTO.setIdTcAgente(Integer.valueOf(cotizacionDTO
							.getSolicitudDTO().getCodigoAgente().toBigInteger()
							.toString()));
					agenteDTO = agenteDN.verDetalleAgente(agenteDTO,
							nombreUsuario);
					cotizacionForm.setNombreAgente(agenteDTO.getNombre());
					cotizacionForm.setIdTcAgente(agenteDTO.getIdTcAgente()
							.toString());
					cotizacionForm.setOficina(agenteDTO.getNumeroOficina());
				} catch (Exception exc) {
					cotizacionForm.setNombreAgente("NO DISPONIBLE");
					cotizacionForm.setOficina("NO DISPONIBLE");
				}
			}
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionDTO
				.getCodigoUsuarioCreacion()))
			cotizacionForm.setCodigoUsuarioCreacion(cotizacionDTO
					.getCodigoUsuarioCreacion());
		if (!UtileriasWeb.esCadenaVacia(cotizacionDTO
				.getCodigoUsuarioModificacion()))
			cotizacionForm.setCodigoUsuarioModificacion(cotizacionDTO
					.getCodigoUsuarioModificacion());
		if (cotizacionDTO.getClaveEstatus() != null)
			cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus()
					.toString());
		// cotizacionForm.setListaFormaPago(FormaPagoDN.getInstancia().listarTodos());
		cotizacionForm.setListaMedioPago(MedioPagoDN.getInstancia()
				.listarTodos());

		if (cotizacionDTO.getClaveAutRetroacDifer() != null) {
			cotizacionForm.setClaveAutRetroacDifer(cotizacionDTO
					.getClaveAutRetroacDifer().toString());

			if (cotizacionForm.getClaveAutRetroacDifer().equals(
					String.valueOf(Sistema.AUTORIZACION_REQUERIDA))
					|| cotizacionForm.getClaveAutRetroacDifer().equals(
							String.valueOf(Sistema.RECHAZADA)))
				cotizacionForm.setMostrarChkAutRetroDifer("1");
			else
				cotizacionForm.setMostrarChkAutRetroDifer("0");
		}
		if (cotizacionDTO.getClaveEstatus().shortValue() == Sistema.ESTATUS_COT_PENDIENTE_AUTORIZACION) {
			cotizacionForm.setMostrarChkAutCliente("1");
		} else {
			cotizacionForm.setMostrarChkAutCliente("0");
		}
		if (cotizacionDTO.getClaveAutVigenciaMaxMin() != null) {
			cotizacionForm.setClaveAutVigenciaMaxMin(cotizacionDTO
					.getClaveAutVigenciaMaxMin().toString());

			if (cotizacionForm.getClaveAutVigenciaMaxMin().equals(
					String.valueOf(Sistema.AUTORIZACION_REQUERIDA))
					|| cotizacionForm.getClaveAutVigenciaMaxMin().equals(
							String.valueOf(Sistema.RECHAZADA)))
				cotizacionForm.setMostrarChkAutVigMaxMin("1");
			else
				cotizacionForm.setMostrarChkAutVigMaxMin("0");
		}

		if (cotizacionDTO.getAclaraciones() != null) {
			cotizacionForm.setAclaraciones(cotizacionDTO.getAclaraciones());
		}
		if (cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada() != null) {
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(
					cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
			cotizacionForm.setNumeroPolizaFormateada(UtileriasWeb
					.getNumeroPoliza(polizaDTO));
			EndosoDTO ultimoEndoso = EndosoDN.getInstancia(
					polizaDTO.getCodigoUsuarioCreacion()).getUltimoEndoso(
					polizaDTO.getIdToPoliza());
			CotizacionDTO cotizacionAnterior = CotizacionDN.getInstancia(
					polizaDTO.getCodigoUsuarioCreacion()).getPorId(
					ultimoEndoso.getIdToCotizacion());
			cotizacionForm.setIdFormaPagoAnterior(cotizacionAnterior
					.getIdFormaPago().toString());
		}

		if (cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() != null
				&& (cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado()
						.compareTo(Sistema.AUTORIZACION_REQUERIDA) == 0 || cotizacionDTO
						.getClaveAutorizacionRecargoPagoFraccionado()
						.compareTo(Sistema.RECHAZADA) == 0))
			cotizacionForm.setMostrarChkAutRPF("1");
		else
			cotizacionForm.setMostrarChkAutRPF("0");

		if (cotizacionDTO.getClaveAutorizacionDerechos() != null
				&& (cotizacionDTO.getClaveAutorizacionDerechos().compareTo(
						Sistema.AUTORIZACION_REQUERIDA) == 0 || cotizacionDTO
						.getClaveAutorizacionDerechos().compareTo(
								Sistema.RECHAZADA) == 0))
			cotizacionForm.setMostrarChkAutDerechos("1");
		else
			cotizacionForm.setMostrarChkAutDerechos("0");

		if (cotizacionDTO.getClaveAutorizacionDiasGracia() != null
				&& (cotizacionDTO.getClaveAutorizacionDiasGracia().compareTo(
						Sistema.AUTORIZACION_REQUERIDA) == 0 || cotizacionDTO
						.getClaveAutorizacionDiasGracia().compareTo(
								Sistema.RECHAZADA) == 0)) {
			cotizacionForm.setMostrarChkAutDiasGracia("1");
		} else {
			cotizacionForm.setMostrarChkAutDiasGracia("0");
		}

		if (cotizacionDTO.getDiasGracia() != null) {
			cotizacionForm.setDiasGracia(cotizacionDTO.getDiasGracia()
					.toString());
		}

		if (cotizacionDTO.getSolicitudDTO() != null
				&& cotizacionDTO.getSolicitudDTO().getEsRenovacion() != null) {
			cotizacionForm.setEsRenovacion(cotizacionDTO.getSolicitudDTO()
					.getEsRenovacion().toString());
		}
	}

	@SuppressWarnings( { "deprecation" })
	protected void poblarDTO(CotizacionForm cotizacionForm,
			CotizacionDTO cotizacionDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToCotizacion()))
			cotizacionDTO.setIdToCotizacion(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
		// direcciones
		DireccionDN direccionDN = DireccionDN.getInstancia();

		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToDireccionCobro()))
			cotizacionDTO.setDireccionCobroDTO(direccionDN
					.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm
							.getIdToDireccionCobro())));
		direccionDN = null;
		// personas		
//		ClienteDN clienteDN = ClienteDN.getInstancia();
		ClienteDTO clienteDTO = null;
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getIdToPersonaAsegurado())) {
			//DESARROLLO CURP
//			if (cotizacionDTO.getIdDomicilioAsegurado() != null
//					&& cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0) {
//				clienteDTO = new ClienteDTO();
//				clienteDTO
//						.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//				clienteDTO.setIdDomicilio(cotizacionDTO
//						.getIdDomicilioAsegurado());
//				clienteDTO = ClienteDN.getInstancia().verDetalleCliente(
//						clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
//			} else {
				clienteDTO = ClienteDN.getInstancia().verDetalleCliente(
						cotizacionDTO.getIdToPersonaAsegurado(),
						cotizacionDTO.getCodigoUsuarioCotizacion());
//			}			
			if (clienteDTO != null) {
				String nombreCliente = clienteDTO.getNombre() != null ? clienteDTO
						.getNombre()
						+ " "
						: "";
				nombreCliente += clienteDTO.getApellidoPaterno() != null ? clienteDTO
						.getApellidoPaterno()
						+ " "
						: "";
				nombreCliente += clienteDTO.getApellidoMaterno() != null ? clienteDTO
						.getApellidoMaterno()
						: "";
				cotizacionDTO.setNombreAsegurado(nombreCliente);
			}
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getIdToPersonaContratante())) {
//			DESARROLLO CURP
//			if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
//				clienteDTO = new ClienteDTO();
//				clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
//				clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
//				clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
//			}else{
				clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), cotizacionDTO.getCodigoUsuarioCotizacion());
//			}				
			if (clienteDTO != null) {
				String nombreCliente = clienteDTO.getNombre() != null ? clienteDTO
						.getNombre()
						+ " "
						: "";
				nombreCliente += clienteDTO.getApellidoPaterno() != null ? clienteDTO
						.getApellidoPaterno()
						+ " "
						: "";
				nombreCliente += clienteDTO.getApellidoMaterno() != null ? clienteDTO
						.getApellidoMaterno()
						: "";
				cotizacionDTO.setNombreContratante(nombreCliente);
			}
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToTipoPoliza())) {
			TipoPolizaDTO poliza = new TipoPolizaDTO();
			poliza.setIdToTipoPoliza(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdToTipoPoliza()));
			cotizacionDTO.setTipoPolizaDTO(TipoPolizaDN.getInstancia()
					.getPorId(poliza));
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToSolicitud()))
			cotizacionDTO.setSolicitudDTO(SolicitudDN.getInstancia().getPorId(
					UtileriasWeb.regresaBigDecimal(cotizacionForm
							.getIdToSolicitud())));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMoneda()))
			cotizacionDTO.setIdMoneda(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdMoneda()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdFormaPago()))
			cotizacionDTO.setIdFormaPago(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdFormaPago()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMedioPago()))
			cotizacionDTO.setIdMedioPago(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdMedioPago()));
		if (!UtileriasWeb
				.esCadenaVacia(cotizacionForm.getFechaInicioVigencia())) {
			String datos[] = cotizacionForm.getFechaInicioVigencia().split("/");
			Date fecha = new Date();
			fecha.setDate(Integer.valueOf(datos[0]));
			fecha.setMonth(Integer.valueOf(datos[1]) - 1);
			fecha.setYear(Integer.valueOf(datos[2]) - 1900);
			cotizacionDTO.setFechaInicioVigencia(fecha);
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaFinVigencia())) {
			String datos[] = cotizacionForm.getFechaFinVigencia().split("/");
			Date fecha = new Date();
			fecha.setDate(Integer.valueOf(datos[0]));
			fecha.setMonth(Integer.valueOf(datos[1]) - 1);
			fecha.setYear(Integer.valueOf(datos[2]) - 1900);
			cotizacionDTO.setFechaFinVigencia(fecha);

		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getNombreEmpresaContratante()))
			cotizacionDTO.setNombreEmpresaContratante(cotizacionForm
					.getNombreEmpresaContratante());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getNombreEmpresaAsegurado()))
			cotizacionDTO.setNombreEmpresaAsegurado(cotizacionForm
					.getNombreEmpresaAsegurado());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getCodigoUsuarioOrdenTrabajo()))
			cotizacionDTO.setCodigoUsuarioOrdenTrabajo(cotizacionForm
					.getCodigoUsuarioOrdenTrabajo());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getCodigoUsuarioCotizacion()))
			cotizacionDTO.setCodigoUsuarioCotizacion(cotizacionForm
					.getCodigoUsuarioCotizacion());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaCreacion()))
			cotizacionDTO.setFechaCreacion(new Date(cotizacionForm
					.getFechaCreacion()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getCodigoUsuarioCreacion()))
			cotizacionDTO.setCodigoUsuarioCreacion(cotizacionForm
					.getCodigoUsuarioCreacion());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaModificacion()))
			cotizacionDTO.setFechaModificacion(new Date(cotizacionForm
					.getFechaModificacion()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getCodigoUsuarioModificacion()))
			cotizacionDTO.setCodigoUsuarioModificacion(cotizacionForm
					.getCodigoUsuarioModificacion());
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveEstatus()))
			cotizacionDTO.setClaveEstatus(Short.valueOf(cotizacionForm
					.getClaveEstatus()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getClaveAutRetroacDifer()))
			cotizacionDTO.setClaveAutRetroacDifer(Short.valueOf(cotizacionForm
					.getClaveAutRetroacDifer()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm
				.getClaveAutVigenciaMaxMin()))
			cotizacionDTO.setClaveAutVigenciaMaxMin(Short
					.valueOf(cotizacionForm.getClaveAutVigenciaMaxMin()));
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdTcAgente())) {
			SolicitudDTO solicitudDTO = SolicitudDN.getInstancia().getPorId(
					cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			solicitudDTO.setCodigoAgente(UtileriasWeb
					.regresaBigDecimal(cotizacionForm.getIdTcAgente()));
			cotizacionDTO.setSolicitudDTO(solicitudDTO);
		}
	}

	public static void poblarPersonaForm(PersonaForm personaForm,
			PersonaDTO personaDTO) {
		if (personaForm != null && personaDTO != null) {
			if (personaDTO.getIdToPersona() != null)
				personaForm.setIdToPersona(personaDTO.getIdToPersona()
						.toString());
			if (personaDTO.getClaveTipoPersona() != null) {
				personaForm.setClaveTipoPersona(""
						+ personaDTO.getClaveTipoPersona());
				if (personaForm.getClaveTipoPersona().equals("1"))
					personaForm.setDescripcionTipoPersona("Persona f�sica");
				else if (personaForm.getClaveTipoPersona().equals("2"))
					personaForm.setDescripcionTipoPersona("Persona moral");
			}
			if (personaDTO.getNombre() != null)
				personaForm.setNombre(personaDTO.getNombre());
			if (personaDTO.getApellidoPaterno() != null)
				personaForm.setApellidoPaterno(personaDTO.getApellidoPaterno());
			if (personaDTO.getApellidoMaterno() != null)
				personaForm.setApellidoMaterno(personaDTO.getApellidoMaterno());
			if (personaDTO.getFechaNacimiento() != null) {
				Format formatter = null;
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				personaForm.setFechaNacimiento(formatter.format(personaDTO
						.getFechaNacimiento()));
			}
			if (personaDTO.getCodigoRFC() != null)
				personaForm.setCodigoRFC(personaDTO.getCodigoRFC());
			if (!UtileriasWeb.esCadenaVacia(personaDTO.getEmail()))
				personaForm.setEmail(personaDTO.getEmail());
			if (!UtileriasWeb.esCadenaVacia(personaDTO.getTelefono()))
				personaForm.setTelefono(personaDTO.getTelefono());
		}
	}

	@SuppressWarnings("deprecation")
	public static void poblarPersonaDTO(PersonaDTO personaDTO,
			PersonaForm personaForm) throws SystemException {
		if (personaForm != null && personaDTO != null) {
			if (!UtileriasWeb.esCadenaVacia(personaForm.getIdToPersona()))
				personaDTO.setIdToPersona(UtileriasWeb
						.regresaBigDecimal(personaForm.getIdToPersona()));
			if (!UtileriasWeb.esCadenaVacia(personaForm.getClaveTipoPersona()))
				personaDTO.setClaveTipoPersona(Short.valueOf(personaForm
						.getClaveTipoPersona()));
			if (!UtileriasWeb.esCadenaVacia(personaForm.getNombre()))
				personaDTO.setNombre(personaForm.getNombre());
			if (!UtileriasWeb.esCadenaVacia(personaForm.getApellidoPaterno()))
				personaDTO.setApellidoPaterno(personaForm.getApellidoPaterno());
			if (!UtileriasWeb.esCadenaVacia(personaForm.getApellidoMaterno()))
				personaDTO.setApellidoMaterno(personaForm.getApellidoMaterno());
			if (!UtileriasWeb.esCadenaVacia(personaForm.getFechaNacimiento())) {
				String datos[] = personaForm.getFechaNacimiento().split("/");
				Date fecha = new Date();
				fecha.setDate(Integer.valueOf(datos[0]));
				fecha.setMonth(Integer.valueOf(datos[1]) - 1);
				fecha.setYear(Integer.valueOf(datos[2]) - 1900);
				personaDTO.setFechaNacimiento(fecha);
			}
			if (!UtileriasWeb.esCadenaVacia(personaForm.getCodigoRFC()))
				personaDTO.setCodigoRFC(personaForm.getCodigoRFC());
			if (!UtileriasWeb.esCadenaVacia(personaForm.getEmail()))
				personaDTO.setEmail(personaForm.getEmail());
			if (!UtileriasWeb.esCadenaVacia(personaForm.getTelefono()))
				personaDTO.setTelefono(personaForm.getTelefono());
		}
	}

	/**
	 * Method getJsonSecciones
	 * 
	 * @param seccionCotizacionDTOList
	 *            (List<SeccionCotizacionDTO>)
	 * @return String
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	private String getJsonSecciones(
			List<SeccionCotizacionDTO> seccionCotizacionDTOList)
			throws ExcepcionDeAccesoADatos, SystemException {

		MidasJsonBase json = new MidasJsonBase();

		if (seccionCotizacionDTOList != null
				&& seccionCotizacionDTOList.size() > 0) {
			for (SeccionCotizacionDTO seccionCotizacionDTO : seccionCotizacionDTOList) {
				MidasJsonRow row = new MidasJsonRow();
				SeccionDN seccionDN = SeccionDN.getInstancia();
				SeccionDTO seccionDTO = new SeccionDTO();
				seccionDTO.setIdToSeccion(seccionCotizacionDTO.getId()
						.getIdToSeccion());
				seccionDTO = seccionDN.getPorId(seccionDTO);
				row.setId(seccionCotizacionDTO.getId().getIdToCotizacion()
						+ "," + seccionCotizacionDTO.getId().getNumeroInciso()
						+ "," + seccionCotizacionDTO.getId().getIdToSeccion());
				row
						.setDatos(
								seccionDTO.getNombreComercial(),
								seccionCotizacionDTO.getClaveContrato()
										.toString(),// este campo muestra el
													// estatus del checkbox, si
													// es 0 el estado es
													// no-seleccionado, si es
													// mayor a 1 el estado es
													// seleccionado
								seccionCotizacionDTO.getClaveObligatoriedad()
										.toString(), // este campo est� oculto,
														// se utiliza en la
														// funcion JS function
														// deshabilitarChecksSeccionesGrid()
														// para deshabilitar los
														// checkbox con
														// claveObligatoriedad
														// mayor a 1
								(seccionCotizacionDTO.getClaveContrato()
										.shortValue() == Sistema.CONTRATADO) ? seccionCotizacionDTO
										.getValorSumaAsegurada().toString()
										: "0",
								(seccionCotizacionDTO.getClaveContrato()
										.shortValue() == Sistema.CONTRATADO) ? seccionCotizacionDTO
										.getValorPrimaNeta().toString()
										: "0",
								"0",
								seccionDTO.getClaveBienSeccion().equals("2") ? "2"
										: "0");
				json.addRow(row);
			}
		}
		return json.toString();
	}

	private String getJsonTablaCobertura(
			List<CoberturaCotizacionDTO> coberturas,
			List<CoberturaDTO> listaCoberturas) throws SystemException {
		int tipoCoaseguro = 0;
		String listaCoberturaExcluidas = new String();
		String listaCoberturaRequeridas = new String();
		/*
		 * Restricciones La suma asegurada solo es editable si: 1.- La cobertura
		 * NO desglosa Riesgos 2.- La seccion ala que pertenece la cobertura no
		 * acepta subincisos
		 */
		try {
			ExclusionCoberturaDN exclusionCoberturaDN = ExclusionCoberturaDN
					.getInstancia();
			CoberturaRequeridaDN coberturaRequeridaDN = CoberturaRequeridaDN
					.getInstancia();
			List<CoberturaExcluidaDTO> excluidas = new ArrayList<CoberturaExcluidaDTO>();
			List<CoberturaRequeridaDTO> requeridas = new ArrayList<CoberturaRequeridaDTO>();
			CoberturaDTO coberturaDTO = new CoberturaDTO();
			// String json = "{rows:[";
			// int index=0;
			// if (coberturas!=null && listaCoberturas.size()>0){
			// for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas)
			// {
			// listaCoberturaExcluidas = "";
			// listaCoberturaRequeridas = "";
			// coberturaDTO = (CoberturaDTO)listaCoberturas.get(index);
			// tipoCoaseguro =
			// Integer.parseInt(coberturaDTO.getClaveTipoCoaseguro());
			//					
			// requeridas =
			// coberturaRequeridaDN.buscarPorCoberturaBase(coberturaDTO);
			// if (requeridas!=null){
			// for (CoberturaRequeridaDTO coberturaRequeridaDTO : requeridas) {
			// listaCoberturaRequeridas +=
			// coberturaRequeridaDTO.getId().getIdToCoberturaRequerida().toString()
			// + "_";
			// }
			// if (requeridas.size()>0)
			// listaCoberturaRequeridas = listaCoberturaRequeridas.substring(0,
			// listaCoberturaRequeridas.length() -1);
			// }
			//					
			// excluidas =
			// exclusionCoberturaDN.buscarPorCoberturaBase(coberturaDTO);
			// if (excluidas!=null){
			// for (CoberturaExcluidaDTO coberturaExcluidaDTO : excluidas) {
			// listaCoberturaExcluidas +=
			// coberturaExcluidaDTO.getCoberturaExcluidaDTO().getIdToCobertura().toString()
			// + "_";
			// }
			// if (excluidas.size()>0)
			// listaCoberturaExcluidas = listaCoberturaExcluidas.substring(0,
			// listaCoberturaExcluidas.length() -1);
			// }
			//					
			// json += "{id:" +
			// coberturaCotizacionDTO.getId().getIdToCobertura().toString() +
			// ",data:[\"";
			// json +=
			// coberturaCotizacionDTO.getId().getIdToCotizacion().toString() +
			// "\",\"";
			// json +=
			// coberturaCotizacionDTO.getId().getNumeroInciso().toString() +
			// "\",\"";
			// json += coberturaCotizacionDTO.getId().getIdToSeccion() +
			// "\",\"";
			// json +=
			// coberturaCotizacionDTO.getClaveObligatoriedad().toString() +
			// "\",\"";
			// if (
			// (coberturaDTO.getClaveDesglosaRiesgos().toString().equalsIgnoreCase("0")
			// ||
			// coberturaDTO.getClaveDesglosaRiesgos().toString().equalsIgnoreCase("n"))
			// || tipoCoaseguro==0)
			// json += "1" + "\",\"";
			// else
			// json += "0" + "\",\"";
			//					
			// json += listaCoberturaRequeridas + "\",\"";
			// json += listaCoberturaExcluidas + "\",\"";
			// json += coberturaDTO.getNombreComercial() + "\",\"";
			// json += coberturaCotizacionDTO.getClaveContrato().toString() +
			// "\",\"";
			//	
			// //Tipo coaseguro
			// switch(tipoCoaseguro){
			// case 1:
			// json += coberturaCotizacionDTO.getValorSumaAsegurada().toString()
			// + "\"";
			// break;
			// case 2:
			// json += "Amparada" + "\"";
			// break;
			// case 3:
			// json += "Sublimite" + "\"";
			// break;
			// }
			// //Tipo de coaseguro
			//					
			// json += "]},";
			// index++;
			// }
			// json = json.substring(0, json.length() -1) + "]}";
			// }else {
			// json += "]}";
			// }

			MidasJsonBase json = new MidasJsonBase();
			int index = 0;
			if (coberturas != null && listaCoberturas.size() > 0) {
				for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas) {
					listaCoberturaExcluidas = "";
					listaCoberturaRequeridas = "";
					coberturaDTO = (CoberturaDTO) listaCoberturas.get(index);
					tipoCoaseguro = Integer.parseInt(coberturaDTO
							.getClaveTipoCoaseguro());
					String tipoCoaseguroDesc = null;
					String desglosaRiesgoDesc = "0";

					requeridas = coberturaRequeridaDN
							.buscarPorCoberturaBase(coberturaDTO
									.getIdToCobertura());
					if (requeridas != null) {
						listaCoberturaRequeridas =  obtenerListaCoberturaRequeridas(requeridas);
					}

					excluidas = exclusionCoberturaDN
							.buscarPorCoberturaBase(coberturaDTO
									.getIdToCobertura());
					if (excluidas != null) {
						listaCoberturaExcluidas = obtenerListaCoberturaExcluidas(excluidas);
					}

					// Tipo coaseguro
					switch (tipoCoaseguro) {
					case 1:
						tipoCoaseguroDesc = coberturaCotizacionDTO
								.getValorSumaAsegurada().toString();
						break;
					case 2:
						tipoCoaseguroDesc = "Amparada";
						break;
					case 3:
						tipoCoaseguroDesc = coberturaCotizacionDTO
								.getValorSumaAsegurada().toString();
						break;
					}

					if ((coberturaDTO.getClaveDesglosaRiesgos().toString()
							.equalsIgnoreCase("0") || coberturaDTO
							.getClaveDesglosaRiesgos().toString()
							.equalsIgnoreCase("n"))
							|| tipoCoaseguro == 0) {
						desglosaRiesgoDesc = "1";
					}

					MidasJsonRow row = new MidasJsonRow();
					row.setId(coberturaCotizacionDTO.getId().getIdToCobertura()
							.toString());
					row.setDatos(coberturaCotizacionDTO.getId()
							.getIdToCotizacion().toString(),
							coberturaCotizacionDTO.getId().getNumeroInciso()
									.toString(), coberturaCotizacionDTO.getId()
									.getIdToSeccion().toString(),
							coberturaCotizacionDTO.getClaveObligatoriedad()
									.toString(), desglosaRiesgoDesc,
							listaCoberturaRequeridas, listaCoberturaExcluidas,
							coberturaDTO.getNombreComercial(),
							coberturaCotizacionDTO.getClaveContrato()
									.toString(), tipoCoaseguroDesc

					);
					json.addRow(row);

					index++;
				}
			}

			return json.toString();
		} catch (SystemException e) {
			throw e;
		}
	}

	public String obtenerListaCoberturaExcluidas(
			List<CoberturaExcluidaDTO> excluidas) {
		StringBuilder listaCoberturaExcluidas = new StringBuilder("");
		for (CoberturaExcluidaDTO coberturaExcluidaDTO : excluidas) {
			listaCoberturaExcluidas.append(coberturaExcluidaDTO
					.getCoberturaExcluidaDTO()
					.getIdToCobertura().toString()
					).append("_");
		}
		if (excluidas.size() > 0)
			return listaCoberturaExcluidas
					.substring(0, listaCoberturaExcluidas
							.length() - 1);	
		
		return listaCoberturaExcluidas.toString();
	}

	public String obtenerListaCoberturaRequeridas(
			List<CoberturaRequeridaDTO> requeridas) {
		StringBuilder listaCoberturaRequeridas = new StringBuilder("");
		for (CoberturaRequeridaDTO coberturaRequeridaDTO : requeridas) {
			listaCoberturaRequeridas.append(coberturaRequeridaDTO
					.getId().getIdToCoberturaRequerida()
					.toString()).append("_");
		}
		if (requeridas.size() > 0)
			return listaCoberturaRequeridas
					.substring(0, listaCoberturaRequeridas
							.length() - 1);
		
		return listaCoberturaRequeridas.toString();
	}

	public void poblarInspeccionIncisoCotizacionDTO(
			InspeccionForm inspeccionForm,
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO,
			HttpServletRequest request) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); // DateFormat.getDateInstance();

		ProveedorInspeccionDTO proveedorInspeccionDTO = new ProveedorInspeccionDTO();

		if (!UtileriasWeb.esObjetoNulo(inspeccionForm
				.getIdToInspeccionIncisoCotizacion())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getIdToInspeccionIncisoCotizacion())
				&& !inspeccionForm.getIdToInspeccionIncisoCotizacion().equals(
						"-1"))
			inspeccionIncisoCotizacionDTO
					.setIdToInspeccionIncisoCotizacion(new BigDecimal(
							inspeccionForm.getIdToInspeccionIncisoCotizacion()));

		if (!UtileriasWeb.esCadenaVacia(inspeccionForm.getNumeroInciso())
				&& !UtileriasWeb
						.esCadenaVacia(inspeccionForm.getNumeroInciso())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getIdToCotizacion())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getIdToCotizacion())) {
			IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
			try {
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				incisoCotizacionId.setIdToCotizacion(new BigDecimal(
						inspeccionForm.getIdToCotizacion()));
				incisoCotizacionId.setNumeroInciso(new BigDecimal(
						inspeccionForm.getNumeroInciso()));
				incisoCotizacion.setId(incisoCotizacionId);
				incisoCotizacion = IncisoCotizacionDN.getInstancia().getPorId(
						incisoCotizacion);
				inspeccionIncisoCotizacionDTO
						.setIncisoCotizacion(incisoCotizacion);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
		}

		if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getNumeroDeReporte())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getNumeroDeReporte()))
			inspeccionIncisoCotizacionDTO.setNumeroReporte(new BigDecimal(
					inspeccionForm.getNumeroDeReporte()));

		if (!UtileriasWeb.esObjetoNulo(inspeccionForm
				.getIdProveedorInspeccion())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getIdProveedorInspeccion())) {
			proveedorInspeccionDTO.setIdToProveedorInspeccion(new BigDecimal(
					inspeccionForm.getIdProveedorInspeccion())); // pendiente de
																	// traer por
																	// ID
			try {
				proveedorInspeccionDTO = ProveedorInspeccionDN.getINSTANCIA()
						.getPorId(proveedorInspeccionDTO);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
			inspeccionIncisoCotizacionDTO
					.setProveedorInspeccion(proveedorInspeccionDTO);
		}

		inspeccionIncisoCotizacionDTO
				.setNombrePersonaEntrevistada(inspeccionForm
						.getPersonaEntrevistada());

		try {
			inspeccionIncisoCotizacionDTO.setFechaInspeccion(dateFormat
					.parse(inspeccionForm.getFechaDeLaInspeccion()));
			inspeccionIncisoCotizacionDTO.setFechaReporteInspeccion(dateFormat
					.parse(inspeccionForm.getFechaElaboracionDocumento()));
		} catch (ParseException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		if (!UtileriasWeb.esObjetoNulo(inspeccionForm
				.getResultadoDeLaInspeccion())
				&& !UtileriasWeb.esCadenaVacia(inspeccionForm
						.getResultadoDeLaInspeccion()))
			inspeccionIncisoCotizacionDTO
					.setClaveResultadoInspeccion(new Short(inspeccionForm
							.getResultadoDeLaInspeccion()));

		inspeccionIncisoCotizacionDTO.setComentariosReporte(inspeccionForm
				.getComentariosDeLaInspeccion());
	}

	/**
	 * cargarDocAnexo cargarDocAnexo carga los documentos anexos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward cargarDocAnexo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * cargarDocAnexosReaseguro cargarDocAnexosReaseguro carga los documentos
	 * anexos de reaseguro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward cargarDocAnexosReaseguro(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;

		String idToCotizacion = request.getParameter("id");
		if (idToCotizacion != null)
			request.getSession().setAttribute("idToCotizacion", idToCotizacion);

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * muestra los documentos anexos de reaseguro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void mostrarDocAnexosReaseguro(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String id = request.getParameter("id");

		String respuesta;
		try {
			respuesta = DocumentoAnexoReaseguroCotizacionDN.getInstancia()
					.obtieneContenidoAnexosReaseguroCotizacion(
							UtileriasWeb.regresaBigDecimal(id));

			response.setContentType("text/json");
			System.out.println("Documentos Anexos Reaseguro: " + respuesta);
			PrintWriter pw = response.getWriter();
			pw.write(respuesta);
			pw.flush();
			pw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method guardarDocumentoAnexoReaseguro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException
	 * @throws IOException
	 */
	public ActionForward guardarDocumentoAnexoReaseguro(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws SystemException, IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		String action = "";
		BigDecimal idToControlArchivo = UtileriasWeb.regresaBigDecimal(request
				.getParameter("gr_id"));
		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCotizacion"));
		try {
			DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId(
					idToCotizacion, idToControlArchivo);
			DocumentoAnexoReaseguroCotizacionDTO documentoAnexo = DocumentoAnexoReaseguroCotizacionDN
					.getInstancia().obtienePorId(id);

			if (request.getParameter("!nativeeditor_status").equals("updated")) {

				documentoAnexo.setDescripcionDocumentoAnexo(request
						.getParameter("descripcionDocumento"));
				documentoAnexo.setNumeroSecuencia(UtileriasWeb
						.regresaBigDecimal(request
								.getParameter("numeroSecuencia")));
				Usuario usuario = (Usuario) UtileriasWeb
						.obtenValorSessionScope(request,
								Sistema.USUARIO_ACCESO_MIDAS);
				if (usuario != null) {
					documentoAnexo.setCodigoUsuarioModificacion(usuario.getId()
							.toString());
					documentoAnexo.setNombreUsuarioModificacion(usuario
							.getNombreUsuario());
					documentoAnexo.setFechaModificacion(new Date());
				}
				DocumentoAnexoReaseguroCotizacionDN.getInstancia().actualizar(
						documentoAnexo);
				action = "update";
			} else if (request.getParameter("!nativeeditor_status").equals(
					"deleted")) {
				/*
				 * ControlArchivoDTO controlArchivoDTO = new
				 * ControlArchivoDTO();
				 * controlArchivoDTO.setIdToControlArchivo(documentoAnexo
				 * .getIdToControlArchivo());
				 */

				DocumentoAnexoReaseguroCotizacionDN.getInstancia().borrar(
						documentoAnexo);
				// ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\""
					+ request.getParameter("gr_id") + "\" tid=\""
					+ request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * guardarOrdenDocAnexos guardar el orden de los documentos anexos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void guardarOrdenDocAnexos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String orden = request.getParameter("orden");
		String idtocotizacion = request.getParameter("idToCotizacion");
		String idToControlArchivo = new String();
		DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
		DocAnexoCotDN docAnexoCotDN = DocAnexoCotDN.getInstancia();
		StringTokenizer stringTokenizer = new StringTokenizer(orden, ",");

		int posicion = 1;
		try {
			while (stringTokenizer.hasMoreTokens()) {
				docAnexoCotDTO = new DocAnexoCotDTO();
				idToControlArchivo = stringTokenizer.nextToken();
				// docAnexoCotDTO.setIdToControlArchivo(new
				// BigDecimal(idToControlArchivo));
				docAnexoCotDTO.setId(new DocAnexoCotId());
				docAnexoCotDTO.getId().setIdToControlArchivo(
						UtileriasWeb.regresaBigDecimal(idToControlArchivo));
				docAnexoCotDTO.getId().setIdToCotizacion(
						UtileriasWeb.regresaBigDecimal(idtocotizacion));
				docAnexoCotDTO = docAnexoCotDN.getPorId(docAnexoCotDTO);
				if (docAnexoCotDTO != null) {
					docAnexoCotDTO.setOrden(new BigDecimal(posicion));
					docAnexoCotDN.modificar(docAnexoCotDTO);
					posicion++;
					response.setContentType("text/xml");
					PrintWriter pw;
					pw = response.getWriter();
					pw.write("<data><action type=\"update\" sid=\""
							+ idToControlArchivo + "\" tid=\""
							+ idToControlArchivo + "\" /></data>");
					pw.flush();
					pw.close();
				}
			}
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * modificarDocAnexo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void modificarDocAnexo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String idToControlArchivo = request.getParameter("gr_id");
		String claveSeleccion = request.getParameter("claveSeleccion");
		String idtocotizacion = request.getParameter("idToCotizacion");
		DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
		DocAnexoCotDN docAnexoCotDN = DocAnexoCotDN.getInstancia();

		// docAnexoCotDTO.setIdToControlArchivo(new
		// BigDecimal(idToControlArchivo));
		try {
			docAnexoCotDTO.setId(new DocAnexoCotId());
			docAnexoCotDTO.getId().setIdToControlArchivo(
					UtileriasWeb.regresaBigDecimal(idToControlArchivo));
			docAnexoCotDTO.getId().setIdToCotizacion(
					UtileriasWeb.regresaBigDecimal(idtocotizacion));
			docAnexoCotDTO = docAnexoCotDN.getPorId(docAnexoCotDTO);
			if (docAnexoCotDTO != null) {
				docAnexoCotDTO.setClaveSeleccion(new Short(claveSeleccion));
				docAnexoCotDN.modificar(docAnexoCotDTO);
				response.setContentType("text/xml");
				PrintWriter pw;
				pw = response.getWriter();
				pw.write("<data><action type=\"update\" sid=\""
						+ idToControlArchivo + "\" tid=\"" + idToControlArchivo
						+ "\" /></data>");
				pw.flush();
				pw.close();
			}
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * cargarGridAnexos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void cargarGridAnexos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		DocAnexoCotDN docAnexoCotDN = DocAnexoCotDN.getInstancia();

		List<DocAnexoCotDTO> anexos = new ArrayList<DocAnexoCotDTO>();
		String idToCotizacion = request.getParameter("idToCotizacion");
		String json = "";

		try {
			// cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));

			anexos = docAnexoCotDN
					.listarDocumentosAnexosPorCotizacion(UtileriasWeb
							.regresaBigDecimal(idToCotizacion));

			json = getJsonDocAnexos(anexos);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	private String getJsonDocAnexos(List<DocAnexoCotDTO> anexos)
			throws SystemException {
		MidasJsonBase json = new MidasJsonBase();
		if (anexos != null && anexos.size() > 0) {
			for (DocAnexoCotDTO docAnexoCotDTO : anexos) {
				String claveTipoDocAnexo = null;
				if (docAnexoCotDTO.getClaveTipo().equals(
						Sistema.CLAVE_DOC_ANEXO_PRODUCTO)) {
					claveTipoDocAnexo = Sistema.DOC_PRODUCTO;
				} else if (docAnexoCotDTO.getClaveTipo().equals(
						Sistema.CLAVE_DOC_ANEXO_TIPO_POLIZA)) {
					claveTipoDocAnexo = Sistema.DOC_TIPO_POLIZA;
				} else if (docAnexoCotDTO.getClaveTipo().equals(
						Sistema.CLAVE_DOC_ANEXO_COBERTURA)) {
					claveTipoDocAnexo = Sistema.DOC_COBERTURA;
				} else {
					claveTipoDocAnexo = Sistema.DOC_INVALIDO;
				}

				MidasJsonRow row = new MidasJsonRow();
				row.setId(docAnexoCotDTO.getId().getIdToControlArchivo()
						.toString());
				row.setDatos(docAnexoCotDTO.getId().getIdToCotizacion()
						.toString(), docAnexoCotDTO.getClaveObligatoriedad()
						.toString(), docAnexoCotDTO.getClaveSeleccion()
						.toString(), claveTipoDocAnexo, docAnexoCotDTO
						.getDescripcionDocumentoAnexo(), MidasJsonRow
						.generarLineaImagenDataGrid(
								"/MidasWeb/img/btn_guardar.jpg", "Descargar",
								"descargarAnexo("
										+ docAnexoCotDTO.getId()
												.getIdToControlArchivo()
												.intValue() + ");", null));
				json.addRow(row);
			}
		}
		return json.toString();
	}

	private StringBuffer enviaMensajeUsuarioTexAdicional(String id,
			String descripcion) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<id>" + id + "</id><description><![CDATA[" + descripcion
				+ "]]></description>");
		buffer.append("</item>");
		buffer.append("</response>");
		return buffer;
	}

	/**
	 * cargarTexAdicionalPorAutorizar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward cargarTexAdicionalPorAutorizar(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		// TODO falta cargar la cotizacion
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		request.setAttribute("usuario", usuario.getNombreUsuario());
		request.setAttribute("fecha", UtileriasWeb.getFechaString(new Date()));
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * cambiarEstatusTextosAdicionales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void cambiarEstatusTextosAdicionales(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String seleccionado = request.getParameter("seleccionado");
		String estatus = request.getParameter("estatus");
		String codigoUsuarioAutorizacion = UtileriasWeb
				.obtieneNombreUsuario(request);
		String id = request.getParameter("gr_id");
		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
		texAdicionalCotDTO.setIdToTexAdicionalCot(new BigDecimal(id));

		// TODO cambiar el codigo del usuario que autoriza el texto adicional
		// por el que esta en sesion
		try {
			texAdicionalCotDTO = texAdicionalCotDN.getPorId(texAdicionalCotDTO);
			texAdicionalCotDTO
					.setCodigoUsuarioAutorizacion(codigoUsuarioAutorizacion);
			texAdicionalCotDTO.setClaveAutorizacion(new Short(estatus));
			texAdicionalCotDTO.setFechaModificacion(new Date());
			if (seleccionado.equalsIgnoreCase("1")) {
				texAdicionalCotDN.modificar(texAdicionalCotDTO);
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"delete\" sid=\""
					+ request.getParameter("gr_id") + "\" tid=\""
					+ request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * cargarTextosAdicionalesPorAutorizar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void cargarTextosAdicionalesPorAutorizar(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
		List<TexAdicionalCotDTO> textosAdicionales = new ArrayList<TexAdicionalCotDTO>();

		String idToCotizacion = request.getParameter("idToCotizacion");
		String json = new String();
		try {
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			texAdicionalCotDTO.setCotizacion(cotizacionDTO);
			textosAdicionales = texAdicionalCotDN
					.listarFiltradoPorAutorizar(texAdicionalCotDTO);

			json = getJsonTexAdicionalPorAutorizar(textosAdicionales);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * agregarTextoAdicional
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void agregarTextoAdicional(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		Double idToTexAdicionalCot = new Double(0.0);

		String claveUsuarioModifico = "cveN";
		String nombreUsuarioModifico = UtileriasWeb
				.obtieneNombreUsuario(request);
		String idToTexAdicionalCotS = request.getParameter("gr_id");
		String descripcionTexto = request.getParameter("textAdicional");
		descripcionTexto = descripcionTexto.replace("&amp;", "&");
		String idToCotizacion = request.getParameter("idToCotizacion");
		String posicion = request.getParameter("posicion");

		try {
			idToTexAdicionalCot = new Double(idToTexAdicionalCotS);
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			// Si el registro existe se actualiza sino se crea uno nuevo
			if (idToTexAdicionalCot.floatValue() > 0) {
				texAdicionalCotDTO.setIdToTexAdicionalCot(new BigDecimal(
						idToTexAdicionalCotS));
				texAdicionalCotDTO = texAdicionalCotDN
						.getPorId(texAdicionalCotDTO);
			} else {
				texAdicionalCotDTO.setNumeroSecuencia(new BigDecimal(posicion));
				texAdicionalCotDTO
						.setCodigoUsuarioCreacion(claveUsuarioModifico);
				texAdicionalCotDTO
						.setNombreUsuarioCreacion(nombreUsuarioModifico);
			}

			texAdicionalCotDTO.setDescripcionTexto(descripcionTexto);
			texAdicionalCotDTO.setClaveAutorizacion(new Short("1"));
			// TODO: el usuario esta nulo validar con andres
			texAdicionalCotDTO
					.setCodigoUsuarioModificacion(claveUsuarioModifico);
			texAdicionalCotDTO
					.setNombreUsuarioModificacion(nombreUsuarioModifico);
			texAdicionalCotDTO.setCodigoUsuarioAutorizacion("");
			texAdicionalCotDTO.setFechaCreacion(new Date());
			texAdicionalCotDTO.setCotizacion(cotizacionDTO);

			if (idToTexAdicionalCot.floatValue() > 0) {
				texAdicionalCotDTO = texAdicionalCotDN
						.modificar(texAdicionalCotDTO);
			} else {
				texAdicionalCotDTO = texAdicionalCotDN
						.agregar(texAdicionalCotDTO);
			}
			StringBuffer buffer = new StringBuffer();
			buffer.append(enviaMensajeUsuarioTexAdicional(texAdicionalCotDTO
					.getIdToTexAdicionalCot().toString(),
					"El texto adicional se agreg\u00f3 correctamente."));
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (SystemException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * cargarTexAdicional
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward cargarTexAdicional(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		// UsuarioDTO usuarioDTO = getUsuarioMidas(request);
		// TODO: el usuario esta nulo validar con andres

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		request.setAttribute("usuario", usuario.getNombre());
		request.setAttribute("fecha", UtileriasWeb.getFechaString(new Date()));
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void borrarTexAdicionalCot(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String idToTexAdicionalCot = request
				.getParameter("idToTexAdicionalCot");
		double idF = new Double(idToTexAdicionalCot);

		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
		if (idF > 0)
			texAdicionalCotDTO.setIdToTexAdicionalCot(new BigDecimal(
					idToTexAdicionalCot));

		try {
			if (idF > 0) {
				texAdicionalCotDTO = texAdicionalCotDN
						.getPorId(texAdicionalCotDTO);
				texAdicionalCotDN.borrar(texAdicionalCotDTO);
			}
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<item>");
			if (idF > 0) {
				buffer
						.append("<id>1</id><description><![CDATA[El texto adicional se elimin\u00f3 correctamente.]]></description>");
			} else {
				buffer
						.append("<id>0</id><description><![CDATA[No se pudo eliminar el elemento seleccionado.]]></description>");
			}
			buffer.append("</item>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * guardarOrdenTexAdicionales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void guardarOrdenTexAdicionales(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String orden = request.getParameter("orden");
		String idToTexAdicionalCot = new String();
		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
		StringTokenizer stringTokenizer = new StringTokenizer(orden, ",");
		Double id = new Double("0");

		int posicion = 1;
		try {
			while (stringTokenizer.hasMoreTokens()) {
				texAdicionalCotDTO = new TexAdicionalCotDTO();
				idToTexAdicionalCot = stringTokenizer.nextToken();
				id = new Double(idToTexAdicionalCot);
				if (id.floatValue() <= 0)
					continue;
				texAdicionalCotDTO.setIdToTexAdicionalCot(new BigDecimal(
						idToTexAdicionalCot));
				texAdicionalCotDTO = texAdicionalCotDN
						.getPorId(texAdicionalCotDTO);
				texAdicionalCotDTO.setNumeroSecuencia(new BigDecimal(posicion));
				texAdicionalCotDN.modificar(texAdicionalCotDTO);
				posicion++;
			}
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * cargarTextosAdicionales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void cargarTextosAdicionales(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
		List<TexAdicionalCotDTO> textosAdicionales = new ArrayList<TexAdicionalCotDTO>();

		String idToCotizacion = request.getParameter("idToCotizacion");
		String json = new String();
		try {
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			texAdicionalCotDTO.setCotizacion(cotizacionDTO);
			textosAdicionales = texAdicionalCotDN
					.listarFiltrado(texAdicionalCotDTO);

			json = getJsonTexAdicional(textosAdicionales);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

	}

	private String getJsonTexAdicional(
			List<TexAdicionalCotDTO> textosAdicionales) throws SystemException {
		// String json = "{rows:[";
		// if (textosAdicionales!=null && textosAdicionales.size()>0){
		// for (TexAdicionalCotDTO texAdicionalCotDTO : textosAdicionales) {
		// json += "{id:" +
		// texAdicionalCotDTO.getIdToTexAdicionalCot().toString() + ",data:[\"";
		// json += texAdicionalCotDTO.getDescripcionTexto() + "\",\"";
		// json += texAdicionalCotDTO.getNombreUsuarioModificacion() + "\",\"";
		// if (texAdicionalCotDTO.getCodigoUsuarioAutorizacion()==null){
		// json += "" + "\",\"";
		// }else{
		// json += texAdicionalCotDTO.getNombreUsuarioModificacion() + "\",\"";
		// }
		// switch (texAdicionalCotDTO.getClaveAutorizacion().intValue()) {
		// case 1:
		// json += "/MidasWeb/img/b_pendiente.gif^Pendiente" + "\",\"";
		// break;
		// case 7:
		// json += "/MidasWeb/img/b_autorizar.gif^Autorizado" + "\",\"";
		// break;
		// case 8:
		// json += "/MidasWeb/img/b_rechazar.gif^Rechazado" + "\",\"";
		// break;
		// }
		// json +=
		// UtileriasWeb.getFechaString(texAdicionalCotDTO.getFechaCreacion())+
		// "\",";
		// json +=
		// "\"/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestTexAdicionalCot(1,"+texAdicionalCotDTO.getIdToTexAdicionalCot()+");^_self\""+",";
		// json += "]},";
		// }
		// json = json.substring(0, json.length() -1) + "]}";
		// }else {
		// json += "]}";
		// }

		MidasJsonBase json = new MidasJsonBase();
		if (textosAdicionales != null && textosAdicionales.size() > 0) {
			for (TexAdicionalCotDTO texAdicionalCotDTO : textosAdicionales) {

				String iconoClaveAutorizacion = null;
				String nombreUsuarioAutorizacion = "";

				if (texAdicionalCotDTO.getCodigoUsuarioAutorizacion() != null) {
					nombreUsuarioAutorizacion = texAdicionalCotDTO
							.getCodigoUsuarioAutorizacion();
				}

				switch (texAdicionalCotDTO.getClaveAutorizacion().intValue()) {
				case 1:
					iconoClaveAutorizacion = "/MidasWeb/img/b_pendiente.gif^Pendiente";
					break;
				case 7:
					iconoClaveAutorizacion = "/MidasWeb/img/b_autorizar.gif^Autorizado";
					break;
				case 8:
					iconoClaveAutorizacion = "/MidasWeb/img/b_rechazar.gif^Rechazado";
					break;
				}

				MidasJsonRow row = new MidasJsonRow();
				row.setId(texAdicionalCotDTO.getIdToTexAdicionalCot()
						.toString());
				row.setDatos(texAdicionalCotDTO.getDescripcionTexto(),
						texAdicionalCotDTO.getNombreUsuarioModificacion(),
						nombreUsuarioAutorizacion, iconoClaveAutorizacion,
						UtileriasWeb.getFechaString(texAdicionalCotDTO
								.getFechaCreacion()),
						"/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestTexAdicionalCot(1,"
								+ texAdicionalCotDTO.getIdToTexAdicionalCot()
								+ ");^_self");
				json.addRow(row);

			}
		}

		return json.toString();
	}

	private String getJsonTexAdicionalPorAutorizar(
			List<TexAdicionalCotDTO> textosAdicionales) throws SystemException {
		// String json = "{rows:[";
		// if (textosAdicionales!=null && textosAdicionales.size()>0){
		// for (TexAdicionalCotDTO texAdicionalCotDTO : textosAdicionales) {
		// json += "{id:" +
		// texAdicionalCotDTO.getIdToTexAdicionalCot().toString() + ",data:[\"";
		// json += "0" + "\",\"";
		// if (texAdicionalCotDTO.getClaveAutorizacion().shortValue()==1)
		// json += "/MidasWeb/img/b_pendiente.gif^Pendiente" + "\",\"";
		// else
		// json += "/MidasWeb/img/b_rechazar.gif^Rechazado" + "\",\"";
		//				
		// json += texAdicionalCotDTO.getDescripcionTexto() + "\",\"";
		// json += texAdicionalCotDTO.getNombreUsuarioCreacion() + "\",\"";
		// if (texAdicionalCotDTO.getNombreUsuarioModificacion()==null){
		// json += "" + "\",\"";
		// }else{
		// json += texAdicionalCotDTO.getNombreUsuarioModificacion() + "\",\"";
		// }
		// json +=
		// UtileriasWeb.getFechaString(texAdicionalCotDTO.getFechaCreacion())+
		// "\",\"";
		// json += "0" + "\"";
		// json += "]},";
		// }
		// json = json.substring(0, json.length() -1) + "]}";
		// }else {
		// json += "]}";
		// }

		MidasJsonBase json = new MidasJsonBase();
		if (textosAdicionales != null && textosAdicionales.size() > 0) {
			for (TexAdicionalCotDTO texAdicionalCotDTO : textosAdicionales) {

				String iconoClaveAutorizacion = "/MidasWeb/img/b_rechazar.gif^Rechazado";

				if (texAdicionalCotDTO.getClaveAutorizacion().shortValue() == 1) {
					iconoClaveAutorizacion = "/MidasWeb/img/b_pendiente.gif^Pendiente";
				}

				MidasJsonRow row = new MidasJsonRow();
				row.setId(texAdicionalCotDTO.getIdToTexAdicionalCot()
						.toString());
				row.setDatos("0", iconoClaveAutorizacion, texAdicionalCotDTO
						.getDescripcionTexto(), texAdicionalCotDTO
						.getNombreUsuarioCreacion(), texAdicionalCotDTO
						.getNombreUsuarioModificacion(), UtileriasWeb
						.getFechaString(texAdicionalCotDTO.getFechaCreacion()),
						"0");
				json.addRow(row);

			}
		}

		return json.toString();
	}

	/**
	 * Method mostrarValidar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException
	 */
	public ActionForward mostrarValidar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
				UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
				UtileriasWeb.regresaBigDecimal(id));
		CotizacionReaseguroFacultativoForm cotizacionReaseguroFacultativoForm = (CotizacionReaseguroFacultativoForm) form;
		cotizacionReaseguroFacultativoForm.setIdToCotizacion(id);
		cotizacionReaseguroFacultativoForm.setClaveEstatus(cotizacionDTO
				.getClaveEstatus().toString());
		cotizacionReaseguroFacultativoForm.setCorreos(cotizacionDTO
				.getSolicitudDTO().getEmailContactos());
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
				&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
						.intValue() > 0) {
			request.setAttribute("esEndoso", "true");
		}
		return mapping.findForward(Sistema.EXITOSO);
	}

	/**
	 * Method mostrarAutorizacionesPendientesPorCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAutorizacionesPendientesPorCotizacion(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		cotizacionForm.setIdToCotizacion(idToCotizacion);
		if (!UtileriasWeb.esCadenaVacia(idToCotizacion)) {
			try {
				CotizacionDTO cotizacion = CotizacionDN.getInstancia(
						UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
						UtileriasWeb.regresaBigDecimal(idToCotizacion));
				if (cotizacion != null) {
					List<Pendiente> listaPendientes = new GestorPendientesDanos()
							.obtenerPendientesPorCotizacion(cotizacion);
					if (listaPendientes != null && listaPendientes.size() > 0) {
						cotizacionForm.setContienePendientes("true");
						cotizacionForm.setListaPendientes(listaPendientes);
					} else {
						cotizacionForm.setContienePendientes("false");
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return mapping.findForward(Sistema.EXITOSO);
	}

	/**
	 * Method mostrarValidarInspeccionAutorizar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarValidarInspeccionAutorizar(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String idToCotizacion = request.getParameter("idToCotizacion");
		request.setAttribute("idToCotizacion", idToCotizacion);
		return mapping.findForward(Sistema.EXITOSO);
	}

	/**
	 * Method mostrarValidarInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarValidarInspeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String idToCotizacion = request.getParameter("idToCotizacion");
		try {
			int consecutivoListaIncisos = 0;

			// Constantes de INCISOCOTIZACION
			final Short NO_REQUERIDA = 0; // campo claveEstatusInspeccion
			final Short REQUERIDA = 1; // campo claveEstatusInspeccion
			final Short APROBADO = 7; // campo claveEstatusInspeccion
			final Short RECHAZADO = 8; // campo claveEstatusInspeccion

			final Short CATALOGO = 1;
			@SuppressWarnings("unused")
			final Short VOLUNTARIA = 2; // campo claveTipoOrigenInspeccion

			final Short NO_HAY_MENSAJE = 0; // campo claveMensajeInspeccion
			final Short CONSULTAR_SUPERVISOR = 2; // campo
													// claveMensajeInspeccion
			final Short VALIDAR_MADERA_MAS_50_PORCIENTO = 3; // campo
																// claveMensajeInspeccion
			final Short INCISO_EXCLUIDO = 9; // campo claveMensajeInspeccion

			// Constantes de SUBGIRO
			final Short EXCLUIDO = 9; // campo claveInspeccion
			final Short INSPECCION = 1; // // campo claveInspeccion, = REQUERIDA
			final Short INSPECCION_SUJETA_A_CONSULTA = 2; // campo
															// claveInspeccion
			final Short INSPECCION_SI_VT_MAYOR_QUE_1MDP = 3; // campo
																// claveInspeccion,
																// VALIDADO POR
																// MARIO
																// GONZALEZ CON
																// JAVIER AYMA
																// EL 5-OCT-2009
			final Short INSPECCION_SI_CONTENIDOS_MAYOR_50_PORCIENTO_MADERA = 4; // campo
																				// claveInspeccion

			final BigDecimal INCENDIO = new BigDecimal("1"); // VALIDADO POR
																// MARIO
																// GONZALEZ CON
																// JAVIER AYMA
																// EL 5-OCT-2009
			final BigDecimal SUBGIRO = new BigDecimal("20");

			BigDecimal idSeccion = BigDecimal.ZERO;
			BigDecimal idCobertura = BigDecimal.ZERO;
			BigDecimal idRiesgo = BigDecimal.ZERO;
			BigDecimal idSubInciso = BigDecimal.ZERO;
			BigDecimal idRamo = INCENDIO;
			BigDecimal idSubRamo = BigDecimal.ZERO;
			Short claveDetalle = new Short("0");
			BigDecimal idDato = SUBGIRO;

			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);
			for (Rol rol : usuario.getRoles()) {
				if (rol.getDescripcion().equals(
						Sistema.ROL_SUPERVISOR_SUSCRIPTOR)
						|| rol.getDescripcion().equals(
								Sistema.ROL_DIRECTOR_TECNICO)) {
					request.setAttribute("autorizaInspecciones", Boolean.TRUE);
					break;
				}
			}

			SoporteEstructuraCotizacion estructuraCotizacion = new SoporteEstructuraCotizacion(
					new BigDecimal(idToCotizacion), UtileriasWeb
							.obtieneNombreUsuario(request));
			estructuraCotizacion.consultarCoberturasContratadasCotizacion();
			List<IncisoCotizacionDTO> incisoCotizacionList = estructuraCotizacion
					.listarIncisos();
			// IncisoCotizacionDN.getInstancia().getPorPropiedad("id.idToCotizacion",
			// new BigDecimal(idToCotizacion));

			Map<BigDecimal, SubGiroDTO> mapaSubGirosCache = new HashMap<BigDecimal, SubGiroDTO>();
			ParametroGeneralDTO parametroGeneralDTO = null;
			for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList) {
				boolean modificarInciso = false;
				// if
				// ((incisoCotizacionDTO.getClaveEstatusInspeccion().equals(NO_REQUERIDA)
				// ||
				// incisoCotizacionDTO.getClaveEstatusInspeccion().equals(REQUERIDA))
				// &&
				// !incisoCotizacionDTO.getClaveTipoOrigenInspeccion().equals(VOLUNTARIA)){
				if (!incisoCotizacionDTO.getClaveEstatusInspeccion().equals(
						APROBADO)
						&& !incisoCotizacionDTO.getClaveEstatusInspeccion()
								.equals(RECHAZADO)) {
					DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
					datoIncisoCotizacionDTO = this
							.poblarDatoIncisoCotizacionDTO(incisoCotizacionDTO
									.getId().getIdToCotizacion(),
									incisoCotizacionDTO.getId()
											.getNumeroInciso(), idSeccion,
									idCobertura, idRiesgo, idSubInciso, idRamo,
									idSubRamo, claveDetalle, idDato);
					datoIncisoCotizacionDTO = DatoIncisoCotizacionDN
							.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
					if (!UtileriasWeb.esObjetoNulo(datoIncisoCotizacionDTO)) {
						BigDecimal idSubGiro = new BigDecimal(
								datoIncisoCotizacionDTO.getValor());
						SubGiroDTO subGiroDTO = mapaSubGirosCache
								.get(idSubGiro);
						if (subGiroDTO == null) {
							subGiroDTO = SubGiroDN.getInstancia()
									.getSubGiroPorId(idSubGiro);
							mapaSubGirosCache.put(idSubGiro, subGiroDTO);
						}

						Short claveInspeccion = subGiroDTO.getClaveInspeccion();

						if (claveInspeccion.equals(EXCLUIDO)) {
							if (incisoCotizacionDTO.getClaveEstatusInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveEstatusInspeccion()
											.equals(NO_REQUERIDA)) {
								incisoCotizacionDTO
										.setClaveEstatusInspeccion(NO_REQUERIDA);
								modificarInciso = true;
							}
							if (incisoCotizacionDTO.getClaveMensajeInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveMensajeInspeccion()
											.equals(INCISO_EXCLUIDO)) {
								incisoCotizacionDTO
										.setClaveMensajeInspeccion(INCISO_EXCLUIDO);
								modificarInciso = true;
							}
						} else if (claveInspeccion.equals(INSPECCION)) {
							if (incisoCotizacionDTO.getClaveEstatusInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveEstatusInspeccion()
											.equals(REQUERIDA)) {
								incisoCotizacionDTO
										.setClaveEstatusInspeccion(REQUERIDA);
								modificarInciso = true;
							}
							if (incisoCotizacionDTO.getClaveMensajeInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveMensajeInspeccion()
											.equals(NO_HAY_MENSAJE)) {
								incisoCotizacionDTO
										.setClaveMensajeInspeccion(NO_HAY_MENSAJE);
								modificarInciso = true;
							}
						} else if (claveInspeccion
								.equals(INSPECCION_SUJETA_A_CONSULTA)) {
							if (incisoCotizacionDTO.getClaveEstatusInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveEstatusInspeccion()
											.equals(NO_REQUERIDA)) {
								incisoCotizacionDTO
										.setClaveEstatusInspeccion(NO_REQUERIDA);
								modificarInciso = true;
							}
							if (incisoCotizacionDTO.getClaveMensajeInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveMensajeInspeccion()
											.equals(CONSULTAR_SUPERVISOR)) {
								incisoCotizacionDTO
										.setClaveMensajeInspeccion(CONSULTAR_SUPERVISOR);
								modificarInciso = true;
							}
						} else if (claveInspeccion
								.equals(INSPECCION_SI_VT_MAYOR_QUE_1MDP)) {
							if (parametroGeneralDTO == null) {
								ParametroGeneralDN parametroGeneralDN = ParametroGeneralDN
										.getINSTANCIA();
								ParametroGeneralId id = new ParametroGeneralId();
								id.setIdToGrupoParametroGeneral(BigDecimal
										.valueOf(4));
								id.setCodigoParametroGeneral(BigDecimal
										.valueOf(40010));
								parametroGeneralDTO = parametroGeneralDN
										.getPorId(id);
							}

							Double limiteSA = Double.valueOf(1000000);
							if (parametroGeneralDTO != null)// TODO: Si no se
															// encuentra el
															// par�metro debe
															// enviar mensaje de
															// error. Por ahora
															// se toma el valor
															// de 1000000 como
															// default.
								limiteSA = Double.valueOf(parametroGeneralDTO
										.getValor());

							Double sumaAsegurada = estructuraCotizacion
									.obtenerSumatoriaSACoberturasBasicasPorInciso(incisoCotizacionDTO
											.getId().getNumeroInciso());
							// IncisoCotizacionDN.getInstancia().calcularSumaAsegurada(incisoCotizacionDTO);

							Double tipoCambio = Double.valueOf(1);
							if (incisoCotizacionDTO.getCotizacionDTO()
									.getIdMoneda().intValue() == Sistema.MONEDA_DOLARES) { // Si
																							// la
																							// moneda
																							// es
																							// D�LARES
								tipoCambio = incisoCotizacionDTO
										.getCotizacionDTO().getTipoCambio();
							}

							Double sumaAseguradadDolares = limiteSA
									/ tipoCambio;
							if (sumaAsegurada.doubleValue() > sumaAseguradadDolares
									.doubleValue()) {
								if (incisoCotizacionDTO
										.getClaveEstatusInspeccion() == null
										|| !incisoCotizacionDTO
												.getClaveEstatusInspeccion()
												.equals(REQUERIDA)) {
									incisoCotizacionDTO
											.setClaveEstatusInspeccion(REQUERIDA);
									modificarInciso = true;
								}
							} else {
								if (incisoCotizacionDTO
										.getClaveEstatusInspeccion() == null
										|| !incisoCotizacionDTO
												.getClaveEstatusInspeccion()
												.equals(NO_REQUERIDA)) {
									incisoCotizacionDTO
											.setClaveEstatusInspeccion(NO_REQUERIDA);
									modificarInciso = true;
								}
							}
						}
						if (claveInspeccion
								.equals(INSPECCION_SI_CONTENIDOS_MAYOR_50_PORCIENTO_MADERA)) {
							if (incisoCotizacionDTO.getClaveEstatusInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveEstatusInspeccion()
											.equals(NO_REQUERIDA)) {
								incisoCotizacionDTO
										.setClaveEstatusInspeccion(NO_REQUERIDA);
								modificarInciso = true;
							}
							if (incisoCotizacionDTO.getClaveMensajeInspeccion() == null
									|| !incisoCotizacionDTO
											.getClaveMensajeInspeccion()
											.equals(
													VALIDAR_MADERA_MAS_50_PORCIENTO)) {
								incisoCotizacionDTO
										.setClaveMensajeInspeccion(VALIDAR_MADERA_MAS_50_PORCIENTO);
								modificarInciso = true;
							}
						}

					} else {
						if (incisoCotizacionDTO.getClaveEstatusInspeccion() == null
								|| !incisoCotizacionDTO
										.getClaveEstatusInspeccion().equals(
												NO_REQUERIDA)) {
							incisoCotizacionDTO
									.setClaveEstatusInspeccion(NO_REQUERIDA);
							modificarInciso = true;
						}
					}

					if (incisoCotizacionDTO.getClaveTipoOrigenInspeccion() == null
							|| !incisoCotizacionDTO
									.getClaveTipoOrigenInspeccion().equals(
											CATALOGO)) {
						incisoCotizacionDTO
								.setClaveTipoOrigenInspeccion(CATALOGO);
						modificarInciso = true;
					}
					if (modificarInciso)
						incisoCotizacionDTO = IncisoCotizacionDN.getInstancia()
								.modificar(incisoCotizacionDTO);
				}
			}

			Map<String, CiudadDTO> mapaCiudades = new HashMap<String, CiudadDTO>();
			Map<String, EstadoDTO> mapaEstados = new HashMap<String, EstadoDTO>();
			StringBuilder ubicacion = new StringBuilder("");
			for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList) {
				ubicacion.delete(0,ubicacion.length());
				if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
						.getDireccionDTO())) {
					ubicacion.append(incisoCotizacionDTO.getDireccionDTO()
							.getNombreCalle()).append(
							 " ").append(
							 incisoCotizacionDTO.getDireccionDTO()
									.getNumeroExterior());
					String ciudad = "";
					CiudadDTO ciudadDTO = null;
					String idCiudad = incisoCotizacionDTO.getDireccionDTO()
							.getIdMunicipio().toBigInteger().toString();
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
							.getDireccionDTO().getIdMunicipio())
							&& !UtileriasWeb
									.esObjetoNulo((ciudadDTO = mapaCiudades
											.containsKey(idCiudad) ? mapaCiudades
											.get(idCiudad)
											: CodigoPostalDN.getInstancia()
													.getCiudadPorId(idCiudad)))) {
						if (!mapaCiudades.containsKey(idCiudad)) {
							mapaCiudades.put(idCiudad, ciudadDTO);
						}
						ciudad = ciudadDTO.getCityName();
					}

					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
							.getDireccionDTO().getIdMunicipio())
							&& !UtileriasWeb
									.esObjetoNulo(CodigoPostalDN.getInstancia()
											.getCiudadPorId(
													incisoCotizacionDTO
															.getDireccionDTO()
															.getIdMunicipio()
															.toBigInteger()
															.toString())))
						ciudad = CodigoPostalDN.getInstancia().getCiudadPorId(
								incisoCotizacionDTO.getDireccionDTO()
										.getIdMunicipio().toBigInteger()
										.toString()).getCityName();

					String estado = "";
					String pais = "";

					EstadoDTO estadoDTO = null;
					String idEstado = incisoCotizacionDTO.getDireccionDTO()
							.getIdEstado().toBigInteger().toString();
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
							.getDireccionDTO().getIdEstado())
							&& !UtileriasWeb
									.esObjetoNulo((estadoDTO = mapaEstados
											.containsKey(idEstado) ? mapaEstados
											.get(idEstado)
											: CodigoPostalDN.getInstancia()
													.getEstadoPorId(idEstado)))) {
						if (!mapaEstados.containsKey(idEstado)) {
							mapaEstados.put(idEstado, estadoDTO);
						}
						estado = estadoDTO.getStateName();
						PaisDTO paisDTO = new PaisDTO();
						if (!UtileriasWeb.esObjetoNulo(CodigoPostalDN
								.getInstancia().getEstadoPorId(
										incisoCotizacionDTO.getDireccionDTO()
												.getIdEstado().toBigInteger()
												.toString()).getCountryId())) {
							paisDTO.setCountryId(CodigoPostalDN.getInstancia()
									.getEstadoPorId(
											incisoCotizacionDTO
													.getDireccionDTO()
													.getIdEstado()
													.toBigInteger().toString())
									.getCountryId());
							pais = PaisTipoDestinoTransporteDN.getInstancia()
									.getPaisPorId(paisDTO).getCountryName();
						}
					}

					ubicacion.append(", ").append(ciudad).append(", ").append(estado
							).append(", ").append(pais);

				} else {
					ubicacion.append("No definida");
					incisoCotizacionDTO.setDireccionDTO(new DireccionDTO());
				}

				incisoCotizacionDTO.getDireccionDTO().setEntreCalles(ubicacion.toString()); // Solamente
																					// es
																					// usado
																					// en
																					// tablaValidarInspeccion.jsp
																					// para
																					// desplegar
																					// la
																					// ubicaci�n
																					// del
																					// inciso,
																					// solamente
																					// transporta
																					// la
																					// informaci�n,
																					// no
																					// se
																					// realiza
																					// operaci�n
																					// alguna
				consecutivoListaIncisos++;
				incisoCotizacionDTO.setCodigoUsuarioEstInspeccion(String
						.valueOf(consecutivoListaIncisos)); // Solamente es
															// usado en
															// tablaValidarInspeccion.jsp
															// para desplegar
															// n�meros
															// consecutivos para
															// los incisos,
															// solamente
															// transporta la
															// informaci�n, no
															// se realiza
															// operaci�n alguna

				switch (incisoCotizacionDTO.getClaveEstatusInspeccion()) {
				case 0:
					incisoCotizacionDTO
							.getDireccionDTO()
							.setNombreColonia(
									UtileriasWeb
											.getMensajeRecurso(
													Sistema.ARCHIVO_RECURSOS,
													"midas.cotizacion.inspeccion.validar.estatus.noRequiereInspeccion"));
					break;
				case 1:
					incisoCotizacionDTO
							.getDireccionDTO()
							.setNombreColonia(
									UtileriasWeb
											.getMensajeRecurso(
													Sistema.ARCHIVO_RECURSOS,
													"midas.cotizacion.inspeccion.validar.estatus.requiereInspeccion"));
					if (incisoCotizacionDTO.getClaveAutInspeccion()
							.shortValue() == 1) {
						incisoCotizacionDTO.getDireccionDTO().setNombreColonia(
								"Autorizaci&oacute;n Solicitada");
					} else if (incisoCotizacionDTO.getClaveAutInspeccion()
							.shortValue() == 7) {
						incisoCotizacionDTO.getDireccionDTO().setNombreColonia(
								"Aprobado sin inspecci&oacute;n");
					} else if (incisoCotizacionDTO.getClaveAutInspeccion()
							.shortValue() == 8) {
						incisoCotizacionDTO.getDireccionDTO().setNombreColonia(
								"Rechazado sin inspecci&oacute;n");
					}
					break;
				case 7:
					incisoCotizacionDTO
							.getDireccionDTO()
							.setNombreColonia(
									UtileriasWeb
											.getMensajeRecurso(
													Sistema.ARCHIVO_RECURSOS,
													"midas.cotizacion.inspeccion.validar.estatus.inspeccionAprobada"));
					break;
				case 8:
					incisoCotizacionDTO
							.getDireccionDTO()
							.setNombreColonia(
									UtileriasWeb
											.getMensajeRecurso(
													Sistema.ARCHIVO_RECURSOS,
													"midas.cotizacion.inspeccion.validar.estatus.inspeccionRechazada"));
					break;
				}
			}

			request.setAttribute("incisoCotizacionList", incisoCotizacionList);
			request.setAttribute("idToCotizacion", idToCotizacion);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(Sistema.EXITOSO);
	}

	private DatoIncisoCotizacionDTO poblarDatoIncisoCotizacionDTO(
			BigDecimal idCotizacion, BigDecimal idInciso, BigDecimal idSeccion,
			BigDecimal idCobertura, BigDecimal idRiesgo,
			BigDecimal idSubInciso, BigDecimal idRamo, BigDecimal idSubRamo,
			Short claveDetalle, BigDecimal idDato) {
		DatoIncisoCotizacionId datoIncisoCotizacionId = new DatoIncisoCotizacionId();
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
		datoIncisoCotizacionId.setIdToCotizacion(idCotizacion);
		datoIncisoCotizacionId.setNumeroInciso(idInciso);
		datoIncisoCotizacionId.setIdToSeccion(idSeccion);
		datoIncisoCotizacionId.setIdToCobertura(idCobertura);
		datoIncisoCotizacionId.setIdToRiesgo(idRiesgo);
		datoIncisoCotizacionId.setNumeroSubinciso(idSubInciso);
		datoIncisoCotizacionId.setIdTcRamo(idRamo);
		datoIncisoCotizacionId.setIdTcSubramo(idSubRamo);
		datoIncisoCotizacionId.setClaveDetalle(claveDetalle);
		datoIncisoCotizacionId.setIdDato(idDato);

		datoIncisoCotizacionDTO.setId(datoIncisoCotizacionId);

		return datoIncisoCotizacionDTO;
	}

	/**
	 * Method validarInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward validarInspeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		return mapping.findForward(Sistema.EXITOSO);
	}

	/**
	 * Method solicitarInspeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	// public ActionForward solicitarInspeccion(ActionMapping mapping,
	// ActionForm form,HttpServletRequest request, HttpServletResponse response)
	// {
	// String reglaNavegacion = Sistema.EXITOSO;
	// String idToCotizacion = request.getParameter("idToCotizacion");
	// String numeroInciso = request.getParameter("numeroInciso");
	//		
	// IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
	// IncisoCotizacionId id = new IncisoCotizacionId();
	// id.setIdToCotizacion(new BigDecimal(idToCotizacion));
	// id.setNumeroInciso(new BigDecimal(numeroInciso));
	// incisoCotizacionDTO.setId(id);
	// try {
	// incisoCotizacionDTO =
	// IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacionDTO);
	// incisoCotizacionDTO.setClaveEstatusInspeccion(new Short("1")); // Clave
	// REQUERIDA = 1
	// incisoCotizacionDTO.setClaveTipoOrigenInspeccion(new Short("2")); //
	// Clave VOLUNTARIA = 2
	// IncisoCotizacionDN.getInstancia().modificar(incisoCotizacionDTO);
	// } catch (SystemException e) {
	// UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
	// reglaNavegacion = Sistema.NO_DISPONIBLE;
	// }
	//		
	// return mapping.findForward(reglaNavegacion);
	// }

	/**
	 * Method cargarAutorizacionesPendientes
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void cargarAutorizacionesPendientes(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String idToCotizacion = request.getParameter("idToCotizacion");
		try {
			List<IncisoCotizacionDTO> incisoCotizacionList = IncisoCotizacionDN
					.getInstancia()
					.listarAutorizacionesPendientesYRechazadasPorCotizacion(
							new BigDecimal(idToCotizacion));
			String json = getJsonAutorizacionesPendientes(incisoCotizacionList);

			response.setContentType("text/json");
			PrintWriter pw;
			pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	/**
	 * Method mostrarAutorizacionesPendientes
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAutorizacionesPendientes(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("idToCotizacion");
		InspeccionForm inspeccionForm = (InspeccionForm) form;
		inspeccionForm.setIdToCotizacion(idToCotizacion);

		return mapping.findForward(reglaNavegacion);
	}

	public String getJsonAutorizacionesPendientes(
			List<IncisoCotizacionDTO> incisoCotizacionList)
			throws ExcepcionDeAccesoADatos, SystemException {
		// String json = "{rows:[";
		// int i = 0;
		// if (incisoCotizacionList != null){
		// for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList)
		// {
		// i++;
		// String ubicacion = "";
		// if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccion())){
		// ubicacion = incisoCotizacionDTO.getDireccion().getNombreCalle() + " "
		// +
		// incisoCotizacionDTO.getDireccion().getNumeroExterior().toBigInteger().toString();
		// String ciudad = "";
		// if
		// (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccion().getIdMunicipio())
		// &&
		// !UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getCiudadPorId(incisoCotizacionDTO.getDireccion().getIdMunicipio().toBigInteger().toString())))
		// ciudad =
		// CodigoPostalDN.getInstancia().getCiudadPorId(incisoCotizacionDTO.getDireccion().getIdMunicipio().toBigInteger().toString()).getCityName();
		//					
		// String estado = "";
		// String pais = "";
		// if
		// (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccion().getIdEstado())
		// &&
		// !UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccion().getIdEstado().toBigInteger().toString()))){
		// estado =
		// CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccion().getIdEstado().toBigInteger().toString()).getStateName();
		//					
		// PaisDTO paisDTO = new PaisDTO();
		// if
		// (!UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccion().getIdEstado().toBigInteger().toString()).getCountryId())){
		// paisDTO.setCountryId(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccion().getIdEstado().toBigInteger().toString()).getCountryId());
		// pais =
		// PaisTipoDestinoTransporteDN.getInstancia().getPaisPorId(paisDTO).getCountryName();
		// }
		// }
		//					
		// ubicacion = ubicacion + ", " + ciudad + ", " + estado + ", " + pais;
		//					
		// }else
		// ubicacion = "No definida";
		//				
		// String mensajeRechazada =
		// UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
		// "midas.cotizacion.inspeccion.validar.mensaje.solicitudOmisionInspeccionRechazada");
		//				
		// json += "{id:\"" + incisoCotizacionDTO.getId().getIdToCotizacion() +
		// "_" + incisoCotizacionDTO.getId().getNumeroInciso() + "\",data:[";
		// json += "\"" + i + "\",";
		// json += "\"" + ubicacion + "\",";
		// if (incisoCotizacionDTO.getClaveAutInspeccion().intValue() == 8)
		// json +=
		// "\"/MidasWeb/img/b_rechazar.gif^"+mensajeRechazada+"^javascript: void(0);^_self"+
		// "\",";
		// else
		// json += "\"/MidasWeb/img/blank.png^^javascript: void(0);^_self"+
		// "\",";
		// json += "\"0\"";
		// json += "]},";
		// }
		// json = json.substring(0, json.length() -1);
		// }
		// json += "]}";

		MidasJsonBase json = new MidasJsonBase();
		int i = 0;
		if (incisoCotizacionList != null) {
			StringBuilder ubicacion = new StringBuilder("");
			for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList) {
				i++;
				ubicacion.delete(0, ubicacion.length());
				if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
						.getDireccionDTO())) {
					ubicacion.append(incisoCotizacionDTO.getDireccionDTO()
							.getNombreCalle()).append(
							 " ").append(
									  incisoCotizacionDTO.getDireccionDTO()
									.getNumeroExterior());
					String ciudad = "";
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
							.getDireccionDTO().getIdMunicipio())
							&& !UtileriasWeb
									.esObjetoNulo(CodigoPostalDN.getInstancia()
											.getCiudadPorId(
													incisoCotizacionDTO
															.getDireccionDTO()
															.getIdMunicipio()
															.toBigInteger()
															.toString())))
						ciudad = CodigoPostalDN.getInstancia().getCiudadPorId(
								incisoCotizacionDTO.getDireccionDTO()
										.getIdMunicipio().toBigInteger()
										.toString()).getCityName();

					String estado = "";
					String pais = "";
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO
							.getDireccionDTO().getIdEstado())
							&& !UtileriasWeb
									.esObjetoNulo(CodigoPostalDN.getInstancia()
											.getEstadoPorId(
													incisoCotizacionDTO
															.getDireccionDTO()
															.getIdEstado()
															.toBigInteger()
															.toString()))) {
						estado = CodigoPostalDN.getInstancia().getEstadoPorId(
								incisoCotizacionDTO.getDireccionDTO()
										.getIdEstado().toBigInteger()
										.toString()).getStateName();

						PaisDTO paisDTO = new PaisDTO();
						if (!UtileriasWeb.esObjetoNulo(CodigoPostalDN
								.getInstancia().getEstadoPorId(
										incisoCotizacionDTO.getDireccionDTO()
												.getIdEstado().toBigInteger()
												.toString()).getCountryId())) {
							paisDTO.setCountryId(CodigoPostalDN.getInstancia()
									.getEstadoPorId(
											incisoCotizacionDTO
													.getDireccionDTO()
													.getIdEstado()
													.toBigInteger().toString())
									.getCountryId());
							pais = PaisTipoDestinoTransporteDN.getInstancia()
									.getPaisPorId(paisDTO).getCountryName();
						}
					}

					ubicacion.append(", ").append(ciudad).append(", ").append(estado
							).append(", ").append(pais);

				} else
					ubicacion.append("No definida");

				String mensajeRechazada = UtileriasWeb
						.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"midas.cotizacion.inspeccion.validar.mensaje.solicitudOmisionInspeccionRechazada");

				String iconoClaveAutInspeccion = "/MidasWeb/img/blank.png^^javascript: void(0);^_self";

				if (incisoCotizacionDTO.getClaveAutInspeccion().intValue() == 8) {
					iconoClaveAutInspeccion = "/MidasWeb/img/b_rechazar.gif^"
							+ mensajeRechazada + "^javascript: void(0);^_self";
				}

				MidasJsonRow row = new MidasJsonRow();
				row.setId(incisoCotizacionDTO.getId().getIdToCotizacion() + "_"
						+ incisoCotizacionDTO.getId().getNumeroInciso());
				row.setDatos(i + "", ubicacion.toString(), iconoClaveAutInspeccion, "0");
				json.addRow(row);

			}
		}

		System.out.println("\n\n JSON: " + json);

		return json.toString();
	}

	/**
	 * Method actualizarAutorizacionesPendientes
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void actualizarAutorizacionesPendientes(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// Constantes de INCISOCOTIZACION
		final Short AUTORIZADA = 7; // campo claveAutInspeccion
		final Short RECHAZADA = 8; // campo claveAutInspeccion
		String ids = request.getParameter("ids");
		String nuevoEstatus = request.getParameter("nuevoEstatus");

		String array[] = ids.split(",");
		String idToCotizacion;
		String numeroInciso;
		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
		for (String idsCompuestos : array) {
			String arrayIds[] = idsCompuestos.split("_");
			idToCotizacion = arrayIds[0];
			numeroInciso = arrayIds[1];

			if (!UtileriasWeb.esObjetoNulo(idToCotizacion)
					&& !UtileriasWeb.esCadenaVacia(idToCotizacion)
					&& !UtileriasWeb.esObjetoNulo(numeroInciso)
					&& !UtileriasWeb.esCadenaVacia(numeroInciso)) {
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				incisoCotizacionId.setIdToCotizacion(new BigDecimal(
						idToCotizacion));
				incisoCotizacionId
						.setNumeroInciso(new BigDecimal(numeroInciso));
				incisoCotizacionDTO.setId(incisoCotizacionId);
				try {
					incisoCotizacionDTO = IncisoCotizacionDN.getInstancia()
							.getPorId(incisoCotizacionDTO);
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO)) {
						if (!UtileriasWeb.esObjetoNulo(nuevoEstatus)
								&& !UtileriasWeb.esCadenaVacia(nuevoEstatus)
								&& (nuevoEstatus.equals("autorizar") || nuevoEstatus
										.equals("rechazar"))) {
							Short nuevoEstatusShort = new Short("8"); // por
																		// default
																		// rechazada
							if (nuevoEstatus.equals("autorizar"))
								nuevoEstatusShort = AUTORIZADA;
							if (nuevoEstatus.equals("rechazar"))
								nuevoEstatusShort = RECHAZADA;

							incisoCotizacionDTO
									.setClaveAutInspeccion(nuevoEstatusShort);
							IncisoCotizacionDN.getInstancia().modificar(
									incisoCotizacionDTO);
						}
					}

				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(
							e.getMessage(), request);
				}
			}

		}

	}

	/**
	 * Method terminarOrdenTrabajo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward terminarOrdenTrabajo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		Set<String> errores;
		String tipoMensaje;
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			SoporteEstructuraCotizacion estructuraCotizacion = new SoporteEstructuraCotizacion(
					UtileriasWeb.regresaBigDecimal(idToCotizacion),
					nombreUsuario);
			// CotizacionDTO cotizacionDTO =
			// cotizacionDN.obtenerCotizacionCompleta(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			// Se ejecuta el validador de Sumas Aseguradas contratadas
			// Consulta las coberturas contratadas, y busca las coberturas
			// b�sicas que tengan SA en cero, por cada una genera un mensaje de
			// error
			boolean tieneSumasValidas = estructuraCotizacion
					.validadaSumasAseguradas(cotizacionForm, false);
			// cotizacionDN.validadaSumasAseguradas(cotizacionForm,cotizacionDTO,false);

			if (tieneSumasValidas) {
				// Validadores normales
				BigDecimal idToCot = UtileriasWeb
						.regresaBigDecimal(idToCotizacion);
				errores = estructuraCotizacion.terminarOrdenTrabajo(idToCot,
						usuario);
				// ordenTrabajoDN.terminarOrdenTrabajo(idToCot, usuario);
				tipoMensaje = OrdenTrabajoDN.ordenTrabajoValida(errores) ? Sistema.EXITO
						: Sistema.ERROR;
				// Generar mensajes de informacion para la presentacion
				Map<String, String> mensajes = generarMensajes(errores, request);
				if (generarMensajeTerminarOrdenTrabajo(mensajes, tipoMensaje,
						cotizacionForm, request)) {
					CotizacionDTO cotizacionAnexoDTO = new CotizacionDTO();
					cotizacionAnexoDTO.setIdToCotizacion(idToCot);
					RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
							.getInstancia();
					RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN
							.buscarDetalleRenovacionPoliza(estructuraCotizacion
									.getCotizacionDTO().getSolicitudDTO()
									.getIdToPolizaAnterior());
					if (estructuraCotizacion.getCotizacionDTO()
							.getSolicitudDTO().getClaveTipoEndoso() == null
							&& renovacionPolizaDTO == null)// Solo se copian
															// cuando no sea
															// endoso
						cotizacionDN.copiarAnexos(cotizacionAnexoDTO, usuario);
				}
			}
			HttpSession session = request.getSession();
			session.removeAttribute("mensaje");
			session.setAttribute("mensaje", cotizacionForm.getMensaje());
			session.removeAttribute("tipoMensaje");
			session
					.setAttribute("tipoMensaje", cotizacionForm
							.getTipoMensaje());
			Runtime.getRuntime().gc();
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method generarMensajeTerminarOrdenTrabajo
	 * 
	 * @param mensajes
	 * @param tipoMensaje
	 * @param cotizacionForm
	 * @param request
	 * @return boolean
	 */
	private boolean generarMensajeTerminarOrdenTrabajo(
			Map<String, String> mensajes, String tipoMensaje,
			CotizacionForm cotizacionForm, HttpServletRequest request) {
		boolean resultado = true;
		String mensajeInicial = null;
		MessageResources messageResources = getResources(request);
		if (tipoMensaje == Sistema.EXITO) {
			if (mensajes == null || mensajes.size() == 0) {
				mensajeInicial = messageResources
						.getMessage("ordentrabajo.mensaje.exito");
			} else {
				mensajeInicial = messageResources
						.getMessage("ordentrabajo.mensaje.exitoDos");
			}
		} else {
			mensajeInicial = messageResources
					.getMessage("ordentrabajo.mensaje.error");
			resultado = false;
		}
		agregarMensajes(mensajes, tipoMensaje, cotizacionForm, mensajeInicial);
		return resultado;
	}

	/**
	 * Method generarMensajes
	 * 
	 * @param errores
	 * @param request
	 * @return Map<String, String>
	 */
	private Map<String, String> generarMensajes(Set<String> errores,
			HttpServletRequest request) {
		MessageResources messageResources = getResources(request);
		Map<String, String> mensajes = new HashMap<String, String>();
		for (String errorKey : errores) {
			mensajes.put(errorKey, messageResources.getMessage(errorKey));
		}
		return mensajes;
	}

	/**
	 * Dado un <code>idToCotizacion</code> se cambia el estatus a COTIZACION EN
	 * PROCESO siempre que se ejecute este metodo, y se notifica a reaseguro,
	 * para que cancele el facultativo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void cambiarAEstatusEnProceso(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String idToCotizacion = request.getParameter("idToCotizacion");
		if (idToCotizacion != null) {
			try {
				if (idToCotizacion != null) {
					CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
							null).getPorId(
							UtileriasWeb.regresaBigDecimal(idToCotizacion));
					try {
						/**
						 * 12/05/2010 Jose Luis Arellano. Se modific� la
						 * instanciaci�n del objeto "soporte", usando el
						 * constructor que no genera un nuevo soporte en caso de
						 * no exista ning�n soporte. Esto 06/04/2010 Jose Luis
						 * Arellano. Se modific� el m�todo
						 * "cancelarProcesoFacultativo", integrandolo a la clase
						 * SoporteReaseguroCotizacionDN. El m�todo hace la
						 * llamada a "notificarCotizacionEnProceso", por lo cual
						 * no debe ser invocado desde aqu�. Se agreg�
						 * notificaci�n a reaseguro cuando la cotizacion regresa
						 * a "en proceso" 26/03/2010 Jose Luis Arellano
						 */
						try {
							SoporteReaseguroCotizacionDN soporte = new SoporteReaseguroCotizacionDN(
									cotizacionDTO.getIdToCotizacion(), false);
							soporte.cancelarProcesoFacultativo(cotizacionDTO
									.getSolicitudDTO().getClaveTipoEndoso());
						} catch (ExcepcionDeLogicaNegocio e) {
							LogDeMidasWeb
									.log(
											"Ocurrio un error al cancelar el proceso facultativo del soporte correspondiente a la cotizacion: "
													+ cotizacionDTO
															.getIdToCotizacion(),
											Level.WARNING, e);
						} catch (SystemException e) {
							LogDeMidasWeb
									.log(
											"Ocurrio un error al cancelar el proceso facultativo del soporte correspondiente a la cotizacion: "
													+ cotizacionDTO
															.getIdToCotizacion(),
											Level.WARNING, e);
						}
						if (cotizacionDTO != null) {
							cotizacionDTO
									.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
							cotizacionDTO.setFechaLiberacion(null);
							cotizacionDTO.setFechaModificacion(new Date());
							CotizacionDN.getInstancia(
									UtileriasWeb.obtieneNombreUsuario(request))
									.modificar(cotizacionDTO);
							UtileriasWeb
									.imprimeMensajeXML(
											Sistema.EXITO,
											"La cotizacion se encuentra de nuevo en Proceso",
											response);
						}
					} catch (SystemException e) {
						e.printStackTrace();
						UtileriasWeb
								.imprimeMensajeXML(
										Sistema.ERROR,
										"No se pudo cambiar el estatus, debido a errores de sistema,consulte al personal de soporte",
										response);
						return;
					} catch (ExcepcionDeAccesoADatos e) {
						e.printStackTrace();
						UtileriasWeb
								.imprimeMensajeXML(
										Sistema.ERROR,
										"No se pudo cambiar el estatus, debido a errores de acceso a datos,consulte al personal de soporte",
										response);
						return;
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
				UtileriasWeb
						.imprimeMensajeXML(
								Sistema.ERROR,
								"No se pudo cambiar el estatus, debido a errores de sistema,consulte al personal de soporte",
								response);
			}
		}
	}

	/**
	 * Busca los recibos de una poliza una vez que ya fue emitida
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listarRecibosPoliza(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			String idToPoliza = (String) request.getParameter("idPoliza");
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			if (idToPoliza != null) {
				PolizaDN polizaDN = PolizaDN.getInstancia();
				PolizaDTO polizaDTO = polizaDN.getPorId(UtileriasWeb
						.regresaBigDecimal(idToPoliza));

				EndosoDN endosoDN = EndosoDN.getInstancia(nombreUsuario);
				List<EndosoDTO> endosos = endosoDN.listarEndososPorPoliza(
						polizaDTO.getIdToPoliza(), Boolean.TRUE);
				polizaDTO.setEndosoDTOs(endosos);
				for (EndosoDTO endosoDTO : polizaDTO.getEndosoDTOs()) {
					ReciboDN reciboDN = ReciboDN.getInstancia(nombreUsuario);
					List<ReciboDTO> recibos = reciboDN.consultaRecibos(
							endosoDTO.getId().getIdToPoliza(), endosoDTO
									.getId().getNumeroEndoso());
					endosoDTO.setRecibos(recibos);

					CotizacionDN cotizacionDN = CotizacionDN
							.getInstancia(nombreUsuario);
					CotizacionDTO cotizacionDTO = cotizacionDN
							.getPorId(endosoDTO.getIdToCotizacion());
					endosoDTO.setCotizacionDTO(cotizacionDTO);

					MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN
							.getInstancia(UtileriasWeb
									.obtieneNombreUsuario(request));
					if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
							&& cotizacionDTO.getSolicitudDTO()
									.getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION) {
						List<MovimientoCotizacionEndosoDTO> movimientosInciso = movimientoCotizacionEndosoDN
								.obtenerMovimientosPorCotizacion(cotizacionDTO
										.getIdToCotizacion());
						endosoDTO.setMovimientosInciso(movimientosInciso);
						MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
						dto
								.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
						dto
								.setIdToCotizacion(cotizacionDTO
										.getIdToCotizacion());
						List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN
								.listarFiltrado(dto);
						endosoDTO.setMovimientosGenerales(movimientosGenerales);
					}
				}
				cotizacionForm.setPolizaDTO(polizaDTO);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Muestra el contenido del Tab de Aclaraciones en OT
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarAclaraciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		try {
			String idToCotizacion = request.getParameter("idToCotizacion");
			if (idToCotizacion != null && idToCotizacion != "") {
				CotizacionDN cotizacionDN = CotizacionDN
						.getInstancia(UtileriasWeb
								.obtieneNombreUsuario(request));
				CotizacionDTO cotizacionDTO = cotizacionDN
						.getPorId(UtileriasWeb
								.regresaBigDecimal(idToCotizacion));
				cotizacionForm.setIdToCotizacion(idToCotizacion);
				cotizacionForm.setAclaraciones(cotizacionDTO.getAclaraciones());
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Guarda las aclaraciones
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward guardarAclaraciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		try {
			String idToCotizacion = cotizacionForm.getIdToCotizacion();
			String aclaraciones = cotizacionForm.getAclaraciones();

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
					.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb
					.regresaBigDecimal(idToCotizacion));
			cotizacionDTO.setAclaraciones(aclaraciones);
			cotizacionDN.modificar(cotizacionDTO);

			cotizacionForm
					.setMensaje("La aclaraci�n se ha guardado exitosamente");
			cotizacionForm.setTipoMensaje(Sistema.EXITO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			cotizacionForm
					.setMensaje("Ocurri� un error al guardar la aclaraci�n");
			cotizacionForm.setTipoMensaje(Sistema.ERROR);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			cotizacionForm
					.setMensaje("Ocurri� un error al guardar la aclaraci�n");
			cotizacionForm.setTipoMensaje(Sistema.ERROR);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Guarda las aclaraciones en COT
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward guardarAclaracionesCotizacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTOList = new ArrayList<DocumentoDigitalCotizacionDTO>();
		List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		try {
			String idToCotizacion = cotizacionForm.getIdToCotizacion();
			String aclaraciones = cotizacionForm.getAclaraciones();

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
					.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb
					.regresaBigDecimal(idToCotizacion));
			cotizacionDTO.setAclaraciones(aclaraciones);
			cotizacionDN.modificar(cotizacionDTO);

			documentoDigitalCotizacionDTOList = DocumentoDigitalCotizacionDN
					.getInstancia().listarDocumentosCotizacionPorCotizacion(
							cotizacionDTO.getIdToCotizacion());
			documentoDigitalSolicitudDTOList = DocumentoDigitalSolicitudDN
					.getInstancia().listarDocumentosDigitalesPorSolicitud(
							cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			request.setAttribute("documentosDigitalesSolicitudList",
					documentoDigitalSolicitudDTOList);
			request.setAttribute("documentosDigitalesCotizacionList",
					documentoDigitalCotizacionDTOList);

			cotizacionForm
					.setMensaje("La aclaraci�n se ha guardado exitosamente");
			cotizacionForm.setTipoMensaje(Sistema.EXITO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			cotizacionForm
					.setMensaje("Ocurri� un error al guardar la aclaraci�n");
			cotizacionForm.setTipoMensaje(Sistema.ERROR);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			cotizacionForm
					.setMensaje("Ocurri� un error al guardar la aclaraci�n");
			cotizacionForm.setTipoMensaje(Sistema.ERROR);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Modifica los derechos y recargo por pago fraccionado de una cotizaci�n.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void modificarRPFDerechos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		IgualacionPrimaNetaForm igualacionForm = (IgualacionPrimaNetaForm) form;

		BigDecimal idToCotizacion = new BigDecimal(igualacionForm
				.getIdToCotizacion());

		Short tipoCalculoDerechos = Short.valueOf(igualacionForm
				.getTipoCalculoDerechos());
		Double valorDerechos = igualacionForm.getValorDerechos();

		Short tipoCalculoRPF = Short
				.valueOf(igualacionForm.getTipoCalculoRPF());
		Double valorRecargoPagoFraccionado = 0D;

		if (tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0)
			valorRecargoPagoFraccionado = igualacionForm.getValorRPFEditable();
		else if (tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0)
			valorRecargoPagoFraccionado = igualacionForm
					.getPorcentajeRPFEditable();

		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));

		CotizacionDTO cotizacionDTO = null;
		List<SoporteResumen> resumenComisiones = new ArrayList<SoporteResumen>();
		try {
			cotizacionDTO = cotizacionDN.modificarRecargoPagoFraccionado(
					idToCotizacion, tipoCalculoRPF,
					valorRecargoPagoFraccionado, true, false);
			cotizacionDTO = cotizacionDN.modificarDerechos(idToCotizacion,
					tipoCalculoDerechos, valorDerechos, true, false);
			cotizacionDTO.setPrimaNetaAnual(cotizacionDN
					.getPrimaNetaCotizacion(idToCotizacion));
			ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(
					cotizacionDTO, resumenComisiones);
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}

		NumberFormat fMonto = new DecimalFormat("$#,##0.00");
		NumberFormat fPorcentaje = new DecimalFormat("##0.00##%");
		NumberFormat fPorcentajeSinDecimales = new DecimalFormat("##0%");
		NumberFormat fNumero = new DecimalFormat("0.00########");

		igualacionForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO
				.getPrimaNetaAnual()));
		igualacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO
				.getPrimaNetaCotizacion()));
		igualacionForm.setMontoRecargoPagoFraccionado(fMonto
				.format(cotizacionDTO.getMontoRecargoPagoFraccionado()));
		igualacionForm.setFactorIVA(fPorcentajeSinDecimales
				.format(cotizacionDTO.getFactorIVA() / 100));
		igualacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA()));
		igualacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO
				.getDerechosPoliza()));
		igualacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO
				.getPrimaNetaTotal()));
		igualacionForm.setResumenComisiones(resumenComisiones);
		igualacionForm.setClaveAutorizacionRPF(cotizacionDTO
				.getClaveAutorizacionRecargoPagoFraccionado().toString());
		igualacionForm.setClaveAutorizacionDerechos(cotizacionDTO
				.getClaveAutorizacionDerechos().toString());

		valorDerechos = cotizacionDTO.getValorDerechosUsuario();
		valorRecargoPagoFraccionado = cotizacionDTO
				.getValorRecargoPagoFraccionadoUsuario();

		String valorRPFEditable = " ", porcentajeRPFSoloLectura = " ", valorRPFSoloLectura = " ", porcentajeRPFEditable = " ", textoDerechos = " ";

		if (tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0) {
			valorRPFEditable = fNumero.format(cotizacionDTO
					.getValorRecargoPagoFraccionadoUsuario());
			porcentajeRPFSoloLectura = fPorcentaje.format(cotizacionDTO
					.getPorcentajeRecargoPagoFraccionadoUsuario() / 100);
		} else if (tipoCalculoRPF
				.compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0) {
			valorRPFSoloLectura = fMonto.format(cotizacionDTO
					.getValorRecargoPagoFraccionadoUsuario());
			porcentajeRPFEditable = fNumero.format(cotizacionDTO
					.getPorcentajeRecargoPagoFraccionadoUsuario());
		}

		if (tipoCalculoDerechos.compareTo(Sistema.CLAVE_DERECHOS_USUARIO) == 0)
			textoDerechos = fNumero.format(valorDerechos);

		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<valorRPFEditable>" + valorRPFEditable
				+ "</valorRPFEditable>");
		buffer.append("<porcentajeRPFSoloLectura>" + porcentajeRPFSoloLectura
				+ "</porcentajeRPFSoloLectura>");
		buffer.append("<valorRPFSoloLectura>" + valorRPFSoloLectura
				+ "</valorRPFSoloLectura>");
		buffer.append("<porcentajeRPFEditable>" + porcentajeRPFEditable
				+ "</porcentajeRPFEditable>");
		buffer.append("<rpf>" + fMonto.format(valorRecargoPagoFraccionado)
				+ "</rpf>");
		buffer.append("<rpfResumen>"
				+ igualacionForm.getMontoRecargoPagoFraccionado()
				+ "</rpfResumen>");
		buffer.append("<textoDerechos>" + textoDerechos + "</textoDerechos>");
		buffer.append("<derechos>" + fMonto.format(valorDerechos)
				+ "</derechos>");
		buffer.append("<iva>" + igualacionForm.getMontoIVA() + "</iva>");
		buffer.append("<primaTotal>" + igualacionForm.getPrimaNetaTotal()
				+ "</primaTotal>");
		buffer.append("<claveAutRPF>"
				+ igualacionForm.getClaveAutorizacionRPF() + "</claveAutRPF>");
		buffer.append("<claveAutDerechos>"
				+ igualacionForm.getClaveAutorizacionDerechos()
				+ "</claveAutDerechos>");
		buffer.append("</item>");
		buffer.append("</response>");

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());

		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Method mostrarDatosLicitacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDatosLicitacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		CotizacionForm datosLicitacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		ActionForward actionForward = new ActionForward(mapping
				.findForward(Sistema.EXITOSO));

		String idToCotizacion = "";
		if (request.getParameter("id") != null) {
			idToCotizacion = request.getParameter("id");
		}

		try {
			cotizacionDTO = cotizacionDN
					.getPorId(new BigDecimal(idToCotizacion));
			datosLicitacionForm.setIdToCotizacion(idToCotizacion);

			if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
					&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
							.intValue() > 0) {
				datosLicitacionForm.setTextoLicitacion(cotizacionDTO
						.getDescripcionLicitacionEndoso());
				datosLicitacionForm.setMostrarRadioBttnClaveImpresionSA("0");
			} else {
				datosLicitacionForm.setTextoLicitacion(cotizacionDTO
						.getDescripcionLicitacion());

				if (cotizacionDTO.getClaveImpresionSumaAsegurada() != null)
					datosLicitacionForm
							.setClaveImpresionSumaAsegurada(cotizacionDTO
									.getClaveImpresionSumaAsegurada()
									.toString());
				else
					datosLicitacionForm
							.setClaveImpresionSumaAsegurada(Sistema.CLAVE_IMPRESION_SUMA_ASEGURADA_CANTIDAD
									.toString());

				datosLicitacionForm.setMostrarRadioBttnClaveImpresionSA("1");
			}

		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return actionForward;
	}

	/**
	 * Method guardarDatosLicitacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarDatosLicitacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		CotizacionForm datosLicitacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
				.obtieneNombreUsuario(request));
		ActionForward actionForward = new ActionForward(mapping
				.findForward(Sistema.EXITOSO));
		String mensaje = UtileriasWeb.getMensajeRecurso(
				Sistema.ARCHIVO_RECURSOS,
				"midas.cotizacion.licitacion.guardar.exito");
		String tipoMensaje = Sistema.EXITO;
		try {
			cotizacionDTO = cotizacionDN.getPorId(new BigDecimal(
					datosLicitacionForm.getIdToCotizacion()));

			if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
					&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()
							.intValue() > 0) {
				cotizacionDTO
						.setDescripcionLicitacionEndoso(datosLicitacionForm
								.getTextoLicitacion());
				datosLicitacionForm.setMostrarRadioBttnClaveImpresionSA("0");
			} else {
				cotizacionDTO.setDescripcionLicitacion(datosLicitacionForm
						.getTextoLicitacion());

				if (datosLicitacionForm.getClaveImpresionSumaAsegurada() != null)
					cotizacionDTO.setClaveImpresionSumaAsegurada(Short
							.valueOf(datosLicitacionForm
									.getClaveImpresionSumaAsegurada()));
				else
					cotizacionDTO
							.setClaveImpresionSumaAsegurada(Sistema.CLAVE_IMPRESION_SUMA_ASEGURADA_CANTIDAD);

				datosLicitacionForm.setMostrarRadioBttnClaveImpresionSA("1");
			}

			cotizacionDN.modificar(cotizacionDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
					"midas.cotizacion.licitacion.guardar.error");
			tipoMensaje = Sistema.ERROR;
		} catch (SystemException e) {
			e.printStackTrace();
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
					"midas.cotizacion.licitacion.guardar.error");
			tipoMensaje = Sistema.ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
					"midas.cotizacion.licitacion.guardar.error");
			tipoMensaje = Sistema.ERROR;
		}

		datosLicitacionForm.setMensaje(mensaje);
		datosLicitacionForm.setTipoMensaje(tipoMensaje);

		return actionForward;
	}
}