package mx.com.afirme.midas.cotizacion.diasgracia;

import mx.com.afirme.midas.sistema.SystemException;

public class DiasGraciaDN {
	private final static DiasGraciaDN INSTANCIA = new DiasGraciaDN();
	
	private DiasGraciaDN(){
		
	}
	
	public static DiasGraciaDN getInstancia(){
		return INSTANCIA;
	}
	
	public Integer obtenerDiasGraciaDefault() throws SystemException{
		return new DiasGraciaSN().obtenerDiasGraciaDefault();
	}
}
