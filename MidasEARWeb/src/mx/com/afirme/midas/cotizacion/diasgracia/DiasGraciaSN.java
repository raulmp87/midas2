package mx.com.afirme.midas.cotizacion.diasgracia;

import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class DiasGraciaSN {

	private DiasGraciaServicio beanRemoto;
	
	DiasGraciaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DiasGraciaServicio.class);
		} catch (Exception e) {
			throw new SystemException(e);
		}
	}
	
	public Integer obtenerDiasGraciaDefault(){
		return beanRemoto.obtenerDiasGraciaDefault();
	}
}
