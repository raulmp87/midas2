package mx.com.afirme.midas.cotizacion.cobertura;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.ExclusionCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CoberturaCotizacionAction extends MidasMappingDispatchAction {

	private static final NumberFormat fCuota = new DecimalFormat("##0.0000");
	private static final NumberFormat fMonto = new DecimalFormat("##0.00");

	/**
	 * Method mostrarCoberturasPorSeccion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarCoberturasPorSeccion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		String fecha = new String();
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		SeccionDTO seccionDTO = new SeccionDTO();
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionForm cotizacionForm = (CotizacionForm) form;

		SeccionDN seccionDN = SeccionDN.getInstancia();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		if (numeroIncisoS.trim().length() < 1
				|| idToSeccionS.trim().length() < 1) {
			return mapping.findForward(reglaNavegacion);
		}
		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		try {
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
			seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			cotizacionDTO.setIdToCotizacion(idToCotizacion);
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			fecha = dateFormat.format(cotizacionDTO.getFechaCreacion());
			request.setAttribute("tituloCoberturas", seccionDTO
					.getNombreComercial());
			request.setAttribute("idToCotizacionS", UtileriasWeb
					.llenarIzquierda(cotizacionDTO.getIdToCotizacion()
							.toString(), "0", 8));
			request.setAttribute("fecha", fecha);
			cotizacionForm.setFechaCreacion(fecha);		
			request.setAttribute("idToCotizacion", idToCotizacion);
			request.setAttribute("numeroInciso", numeroInciso);
			request.setAttribute("idToSeccion", idToSeccion);			

			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null)
				cotizacionForm.setClaveTipoEndoso(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().toString());
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			for (Rol rol : usuario.getRoles()) {
				if (rol.getDescripcion().equals(Sistema.ROL_SUPERVISOR_SUSCRIPTOR) || rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_TECNICO)){
					request.setAttribute("autorizaCoberturas", Boolean.TRUE);
					break;
				}
			}
			
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarCoberturasPorSeccionAutorizar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarCoberturasPorSeccionAutorizar(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		String fecha = new String();
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		SeccionDTO seccionDTO = new SeccionDTO();
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionForm cotizacionForm = (CotizacionForm) form;

		SeccionDN seccionDN = SeccionDN.getInstancia();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		if (numeroIncisoS.trim().length() < 1
				|| idToSeccionS.trim().length() < 1) {
			return mapping.findForward(reglaNavegacion);
		}
		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		try {
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
			seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			cotizacionDTO.setIdToCotizacion(idToCotizacion);
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			fecha = dateFormat.format(cotizacionDTO.getFechaCreacion());
			request.setAttribute("tituloCoberturas", seccionDTO
					.getNombreComercial());
			request.setAttribute("idToCotizacion", UtileriasWeb
					.llenarIzquierda(cotizacionDTO.getIdToCotizacion()
							.toString(), "0", 8));
			request.setAttribute("fecha", fecha);
			cotizacionForm.setFechaCreacion(fecha);		

			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null)
				cotizacionForm.setClaveTipoEndoso(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().toString());
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarCoberturas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void mostrarCoberturas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		List<CoberturaDTO> listaCoberturas = new ArrayList<CoberturaDTO>();
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));

		if (numeroIncisoS.trim().length() < 1
				|| idToSeccionS.trim().length() < 1) {
			return;
		}
		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();

		String json = new String();
		try {
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
			seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);

			coberturas = cotizacionDN
					.getCoberturasPorSeccion(seccionCotizacionDTO);
			if (coberturas != null) {
				for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas) {
					coberturaDTO = new CoberturaDTO();
					coberturaDTO.setIdToCobertura(coberturaCotizacionDTO
							.getId().getIdToCobertura());
					coberturaDTO = coberturaDN.getPorId(coberturaDTO);
					listaCoberturas.add(coberturaDTO);
				}
			}
			json = getJsonTablaCobertura(coberturas, listaCoberturas);

			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
	}

	private String getJsonTablaCobertura(
			List<CoberturaCotizacionDTO> coberturas,
			List<CoberturaDTO> listaCoberturas) throws SystemException {
		String listaCoberturaExcluidas = new String();
		String listaCoberturaRequeridas = new String();
		String iconoCoaseguro = new String();
		String iconoDeducible = new String();
		Double totalPrimas = Double.valueOf("0");
		String desglosaRiesgo = new String();
		String sumaAsegurada = new String();
		String sendRequest = "";

		/*
		 * Restricciones La suma asegurada solo es editable si: 1.- La cobertura
		 * NO desglosa Riesgos 2.- La seccion ala que pertenece la cobertura no
		 * acepta subincisos
		 */
		try {
			ExclusionCoberturaDN exclusionCoberturaDN = ExclusionCoberturaDN
					.getInstancia();
			CoberturaRequeridaDN coberturaRequeridaDN = CoberturaRequeridaDN
					.getInstancia();
			List<CoberturaExcluidaDTO> excluidas = new ArrayList<CoberturaExcluidaDTO>();
			List<CoberturaRequeridaDTO> requeridas = new ArrayList<CoberturaRequeridaDTO>();
			CoberturaDTO coberturaDTO = new CoberturaDTO();

			MidasJsonBase json = new MidasJsonBase();
			int index = 0;
			if (coberturas != null && listaCoberturas.size() > 0) {
				for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas) {
					MidasJsonRow row = new MidasJsonRow();

					listaCoberturaExcluidas = "";
					listaCoberturaRequeridas = "";
					coberturaDTO = (CoberturaDTO) listaCoberturas.get(index);

					requeridas = coberturaRequeridaDN
							.buscarPorCoberturaBase(coberturaDTO.getIdToCobertura());

					listaCoberturaRequeridas = this
							.getCoberturasRequeridasCadena(requeridas);

					excluidas = exclusionCoberturaDN
							.buscarPorCoberturaBase(coberturaDTO.getIdToCobertura());

					listaCoberturaExcluidas = this
							.getCoberturasExcluidasCadena(excluidas);

					if ((coberturaDTO.getClaveDesglosaRiesgos().toString()
							.equalsIgnoreCase("0") || coberturaDTO
							.getClaveDesglosaRiesgos().toString()
							.equalsIgnoreCase("n")))
						desglosaRiesgo = "0";
					else
						desglosaRiesgo = "1";

					// Suma Asegurada
					switch (Integer.valueOf(
							coberturaDTO.getClaveTipoSumaAsegurada())
							.intValue()) {
					case 1:
						sumaAsegurada = fMonto.format(coberturaCotizacionDTO
								.getValorSumaAsegurada());
						break;
					case 2:
						sumaAsegurada = "Amparada";
						break;
					case 3:
						sumaAsegurada = fMonto.format(coberturaCotizacionDTO
							.getValorSumaAsegurada()==null?new Double("0"):coberturaCotizacionDTO
								.getValorSumaAsegurada());
						break;
					}

					// semaforo de coaseguro icono
					String altCoaseguro = "";
					String altDeducible = "";
					switch (coberturaCotizacionDTO.getClaveAutCoaseguro()) {
					case Sistema.AUTORIZACION_NO_REQUERIDA:
						iconoCoaseguro = Sistema.ICONO_BLANCO;
						break;
					case Sistema.AUTORIZACION_REQUERIDA:
						iconoCoaseguro = Sistema.ICONO_AMARILLO;
						altCoaseguro = "Requiere Autorizaci&oacute;n";
						break;
					case Sistema.AUTORIZADA:
						iconoCoaseguro = Sistema.ICONO_VERDE;
						altCoaseguro = "Coaseguro Autorizado";
						break;
					case Sistema.RECHAZADA:
						altCoaseguro = "Coaseguro Rechazado";
						iconoCoaseguro = Sistema.ICONO_ROJO;
						break;

					}
					// semaforo de deducible icono
					switch (coberturaCotizacionDTO.getClaveAutDeducible()) {
					case Sistema.AUTORIZACION_NO_REQUERIDA:
						iconoDeducible = Sistema.ICONO_BLANCO;
						break;
					case Sistema.AUTORIZACION_REQUERIDA:
						iconoDeducible = Sistema.ICONO_AMARILLO;
						altDeducible = "Requiere Autorizaci&oacute;n";
						break;
					case Sistema.AUTORIZADA:
						iconoDeducible = Sistema.ICONO_VERDE;
						altDeducible = "Deducible Autorizado";
						break;
					case Sistema.RECHAZADA:
						iconoDeducible = Sistema.ICONO_ROJO;
						altDeducible = "Deducible Rechazado";
						break;

					}

					Double valorCoaseguro = 0D;
					Double valorDeducible = 0D;
					if(coberturaCotizacionDTO.getClaveContrato().shortValue() != 1) {
						CoaseguroCoberturaDN coaseguroCoberturaDN = CoaseguroCoberturaDN.getInstancia();
						List<CoaseguroCoberturaDTO> coaseguros = coaseguroCoberturaDN.listarCoaseguros(coberturaCotizacionDTO.getId().getIdToCobertura());
						for(CoaseguroCoberturaDTO coaseguro : coaseguros) {
							if(coaseguro.getClaveDefault().shortValue() == 1) {
								valorCoaseguro = coaseguro.getValor();
							}
						}
						DeducibleCoberturaDN deducibleCoberturaDN = DeducibleCoberturaDN.getInstancia();
						List<DeducibleCoberturaDTO> deducibles = deducibleCoberturaDN.listarDeducibles(coberturaCotizacionDTO.getId().getIdToCobertura());
						for(DeducibleCoberturaDTO deducible : deducibles) {
							if(deducible.getClaveDefault().shortValue() == 1) {
								valorDeducible = deducible.getValor();
							}
						}
					}

					totalPrimas = totalPrimas
							+ coberturaCotizacionDTO.getValorPrimaNeta();

		

					row.setId(coberturaCotizacionDTO.getId().getIdToSeccion()
							.toString()
							+ "_"
							+ coberturaCotizacionDTO.getId().getIdToCobertura()
									.toString());
					row.setDatos(
							coberturaCotizacionDTO.getId().getIdToCotizacion()
									.toString(),
							coberturaCotizacionDTO.getId().getNumeroInciso()
									.toString(),
							coberturaCotizacionDTO.getId().getIdToSeccion()
									.toString(),
							coberturaCotizacionDTO.getId().getIdToCobertura()
									.toString(),
							coberturaCotizacionDTO.getClaveObligatoriedad()
									.toString(),
							coberturaDTO.getClaveTipoSumaAsegurada(),
							listaCoberturaRequeridas,
							listaCoberturaExcluidas,
							coberturaDTO.getNombreComercial(),
							coberturaCotizacionDTO.getClaveContrato()
									.toString(),
							sumaAsegurada,// suma asegurada
							fCuota.format(coberturaCotizacionDTO
									.getValorCuota() * 1000D),// cuota
							fMonto.format(coberturaCotizacionDTO
									.getValorPrimaNeta()),// prima neta
							"0",// comision
							desglosaRiesgo.equals("1") ? "Segun Riesgo"
									: valorCoaseguro > 0 ? valorCoaseguro.toString()
											: coberturaCotizacionDTO
													.getPorcentajeCoaseguro()
													.toString(),// coaseguro
							MidasJsonRow
									.generarLineaImagenDataGrid(iconoCoaseguro,
											altCoaseguro, "void(0)", null),// Semaforo
							// de
							// autorizacion
							// coaseguro
							desglosaRiesgo.equals("1") ? "Segun Riesgo"
									: valorDeducible > 0 ? valorDeducible.toString()
											: coberturaCotizacionDTO
													.getPorcentajeDeducible()
													.toString(),// Deducible
							MidasJsonRow
									.generarLineaImagenDataGrid(iconoDeducible,
											altDeducible, "void(0)", null),// Semaforo
							// de
							// autorizacion
							// deducible
							desglosaRiesgo.equals("1") ? "Segun Riesgo" : (coberturaCotizacionDTO.getClaveTipoDeducible()!= null?coberturaCotizacionDTO.getClaveTipoDeducible().toString():""),
							desglosaRiesgo.equals("1") ? "Segun Riesgo" : (coberturaCotizacionDTO.getValorMinimoLimiteDeducible()!=null?coberturaCotizacionDTO.getValorMinimoLimiteDeducible().toString():""),
							desglosaRiesgo.equals("1") ? "Segun Riesgo" : (coberturaCotizacionDTO.getValorMaximoLimiteDeducible()!=null?coberturaCotizacionDTO.getValorMaximoLimiteDeducible().toString():""),
							desglosaRiesgo.equals("1") ? "Segun Riesgo" : (coberturaCotizacionDTO.getClaveTipoLimiteDeducible()!=null?coberturaCotizacionDTO.getClaveTipoLimiteDeducible().toString():""),
							(coberturaCotizacionDTO.getClaveContrato()
									.intValue() == 1 ) ? MidasJsonRow
									.generarLineaImagenDataGrid(
											Sistema.ICONO_DETALLE,
											"Aumentos/Recargos/Descuentos",
											obtenerSendRequest(coberturaCotizacionDTO), null) : "0",
							desglosaRiesgo,
							coberturaCotizacionDTO
								.getSeccionCotizacionDTO().getSeccionDTO()
								.getClaveBienSeccion().equals("1")? "1" : "0",
							coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion().toString());
					json.addRow(row);
					index++;
				}
			}
			return json.toString();
		} catch (SystemException e) {
			throw e;
		}
	}

	public String obtenerSendRequest(CoberturaCotizacionDTO coberturaCotizacionDTO) {
		StringBuilder sendRequest = new StringBuilder("");
		sendRequest.append("sendRequest(null,'/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion=");
			sendRequest.append(coberturaCotizacionDTO.getId()
					.getIdToCotizacion().toString());
			sendRequest.append("&numeroInciso=").append(coberturaCotizacionDTO.getId().getNumeroInciso()
							.toString());
			sendRequest.append("&idToSeccion=").append(coberturaCotizacionDTO.getId().getIdToSeccion()
							.toString());
			sendRequest.append("&idToCobertura=").append(coberturaCotizacionDTO.getId().getIdToCobertura()
							.toString());
			sendRequest.append("','configuracion_detalle',null);");
			return sendRequest.toString();
	}

	public void getValoresCoaseguroDeducible(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws SystemException, IOException {

		String id = request.getParameter("tipo");
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request
				.getParameter("id"));
		StringBuffer buffer = new StringBuffer();

		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");

		if (id.equals("C")) {// Coaseguro
			CoaseguroCoberturaDN coaseguroCoberturaDN = CoaseguroCoberturaDN
					.getInstancia();
			List<CoaseguroCoberturaDTO> coaseguros = coaseguroCoberturaDN
					.listarCoaseguros(idToCobertura);
			for (CoaseguroCoberturaDTO coaseguro : coaseguros) {
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(coaseguro.getValor().toString());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(coaseguro.getValor().toString() + "%");
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
		} else if (id.equals("D")) {// Deducibles
			DeducibleCoberturaDN deducibleCoberturaDN = DeducibleCoberturaDN
					.getInstancia();
			List<DeducibleCoberturaDTO> deducibles = deducibleCoberturaDN
					.listarDeducibles(idToCobertura);
			for (DeducibleCoberturaDTO deducible : deducibles) {
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(deducible.getValor().toString());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(deducible.getValor().toString() + "%");
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
		}else if (id.equals("TD")){ //Tipo Deducible
			List<CatalogoValorFijoDTO> tiposDeducible = ValorFijoDN.getInstancia().buscarPorPropiedad("id.idGrupoValores", Integer.valueOf(Sistema.GRUPO_CLAVE_TIPO_DEDUCIBLE));
			for(CatalogoValorFijoDTO catalogo: tiposDeducible){
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(catalogo.getId().getIdDato());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(catalogo.getDescripcion());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
		}else if (id.equals("LD")){//Tipo Limite Deducible
			List<CatalogoValorFijoDTO> tiposLimiteDeducible = ValorFijoDN.getInstancia().buscarPorPropiedad("id.idGrupoValores", Integer.valueOf(Sistema.GRUPO_CLAVE_TIPO_LIMITE_DEDUCIBLE));
			for(CatalogoValorFijoDTO catalogo: tiposLimiteDeducible){
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(catalogo.getId().getIdDato());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(catalogo.getDescripcion());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}			
		}
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		response.getWriter().write(buffer.toString());
	}
	
	@SuppressWarnings("unused")
	private boolean laSumaSubLimiteEsMenorSumaBasica(Double sumaSubLimite, Double sumaBasica){
	    if(sumaSubLimite<sumaBasica){
		return true;
	    }else{
		return false;
	    }
	    
	}
	
	public void guardarCobertura(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {
		
		String claveContrato =request.getParameter("claveContrato");
		String sumaAsegurada = request.getParameter("sumaAsegurada");
		String coaseguro = request.getParameter("coaseguro");
		String deducible = request.getParameter("deducible");
		String tipoDeducible = request.getParameter("tipoDeducible");
		String limiteMinimo = request.getParameter("minimo");
		String limiteMaximo = request.getParameter("maximo");
		String tipoLimite = request.getParameter("tipoLimiteDeducible");
		String gr_id = request.getParameter("gr_id");
		CoberturaCotizacionId id = new CoberturaCotizacionId();

		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
		id.setIdToCotizacion(idToCotizacion);
		BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
		id.setNumeroInciso(numeroInciso);
		BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
		id.setIdToSeccion(idToSeccion);
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
		id.setIdToCobertura(idToCobertura);
		
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		coberturaCotizacionDTO.setId(id);
		
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();	
		coberturaCotizacionDTO = coberturaCotizacionDN.getPorId(coberturaCotizacionDTO);

		if (claveContrato.equals("0")) {
		    //Se agrega esta condicional para que actualize a 0, cuando se descontrato una cobertura de sublimite
	    	if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().
	    		getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_SUBLIMITE)){
	    	    coberturaCotizacionDTO.setValorSumaAsegurada(new Double("0"));
	    	}
		  	coberturaCotizacionDTO.setClaveContrato((short) 0);
		  	coberturaCotizacionDN.modificar(coberturaCotizacionDTO);
			coberturaCotizacionDTO = CalculoCotizacionDN.getInstancia().calcularCobertura(coberturaCotizacionDTO, 
					UtileriasWeb.obtieneNombreUsuario(request), false);
			//SE AGREGA ELIMINACION DE DETALLESPRIMAS COBERTURAS Y DETALLESPRIMASRIESGO
			CotizacionDN.getInstancia("").eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(coberturaCotizacionDTO);	    	
		}else{
			boolean esContratada = false;
			Double sumaAseguradaAnterior = coberturaCotizacionDTO.getValorSumaAsegurada();
			SeccionDTO seccionDTO = SeccionDN.getInstancia().getPorId(coberturaCotizacionDTO.getId().getIdToSeccion());
			if(seccionDTO.getClaveSubIncisos() == Sistema.PERMITE_SUBINCISOS && (sumaAsegurada.startsWith("A") || Double.parseDouble(sumaAsegurada) == 0)){
				esContratada = true;
			}
			if(!coaseguro.startsWith("S")){
			  	if (coberturaCotizacionDTO.getPorcentajeCoaseguro().doubleValue() != Double.parseDouble(coaseguro)){
					coberturaCotizacionDTO.setPorcentajeCoaseguro(Double.parseDouble(request.getParameter("coaseguro")));
					if(coberturaCotizacionDTO.getPorcentajeCoaseguro().doubleValue() != 0D) {
						coberturaCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZACION_REQUERIDA);
						CoaseguroCoberturaDN coaseguroCoberturaDN = CoaseguroCoberturaDN.getInstancia();
						List<CoaseguroCoberturaDTO> coaseguros = coaseguroCoberturaDN.listarCoaseguros(idToCobertura);
						for(CoaseguroCoberturaDTO coaseguroDTO : coaseguros) {
							if(coaseguroDTO.getValor().doubleValue() == coberturaCotizacionDTO.getPorcentajeCoaseguro().doubleValue()) {
								coberturaCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZACION_NO_REQUERIDA);
								break;
							}
						}
					} else {
						coberturaCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZACION_NO_REQUERIDA);
					}	  		
			  	}
				
			}
			if (!deducible.startsWith("S")){
			  	if (coberturaCotizacionDTO.getPorcentajeDeducible().doubleValue() != Double.parseDouble(deducible)){
					coberturaCotizacionDTO.setPorcentajeDeducible(Double.parseDouble(request.getParameter("deducible")));
					
					if(coberturaCotizacionDTO.getPorcentajeDeducible().doubleValue() != 0D) {
						coberturaCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZACION_REQUERIDA);
						DeducibleCoberturaDN deducibleCoberturaDN = DeducibleCoberturaDN.getInstancia();
						List<DeducibleCoberturaDTO> deducibles = deducibleCoberturaDN.listarDeducibles(idToCobertura);
						for(DeducibleCoberturaDTO deducibleDTO : deducibles) {
							if(deducibleDTO.getValor().doubleValue() == coberturaCotizacionDTO.getPorcentajeDeducible()) {
								coberturaCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZACION_NO_REQUERIDA);
								break;
							}
						}
					} else {
						coberturaCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZACION_NO_REQUERIDA);
					}
			  	}
			}
		  	
			if (sumaAsegurada.startsWith("A")) {
				coberturaCotizacionDTO
						.setValorSumaAsegurada(coberturaCotizacionDTO
								.getValorSumaAsegurada());
			} else {
				coberturaCotizacionDTO.setValorSumaAsegurada(Double
						.parseDouble(sumaAsegurada));
			}  
			
			if (tipoDeducible != null && !tipoDeducible.equals("") && !tipoDeducible.equalsIgnoreCase("Segun Riesgo")){
				if(coberturaCotizacionDTO.getClaveTipoDeducible().intValue() != Integer.parseInt(tipoDeducible))
					coberturaCotizacionDTO.setClaveTipoDeducible(Short.valueOf(tipoDeducible));
			}
			if(limiteMinimo != null && !limiteMinimo.equals("") && !limiteMinimo.equalsIgnoreCase("Segun Riesgo")){
				if(coberturaCotizacionDTO.getValorMinimoLimiteDeducible().doubleValue() != Double.valueOf(limiteMinimo))
					coberturaCotizacionDTO.setValorMinimoLimiteDeducible(Double.valueOf(limiteMinimo));
			}
			if (limiteMaximo != null && !limiteMaximo.equals("") && !limiteMaximo.equalsIgnoreCase("Segun Riesgo")){
				if(coberturaCotizacionDTO.getValorMaximoLimiteDeducible().doubleValue() != Double.valueOf(limiteMaximo))
					coberturaCotizacionDTO.setValorMaximoLimiteDeducible(Double.valueOf(limiteMaximo));
			}
			if (tipoLimite != null && !tipoLimite.equals("") && !tipoLimite.equalsIgnoreCase("Segun Riesgo")){
				if(coberturaCotizacionDTO.getClaveTipoLimiteDeducible().intValue() != Integer.parseInt(tipoLimite))
					coberturaCotizacionDTO.setClaveTipoLimiteDeducible(Short.valueOf(tipoLimite));
			}

			RiesgoCotizacionId idRiesgo = new RiesgoCotizacionId();
			idRiesgo.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
			idRiesgo.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
			idRiesgo.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
			idRiesgo.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
			RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
			riesgoCotizacionDTO.setId(idRiesgo);
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarFiltrado(riesgoCotizacionDTO);

			for(RiesgoCotizacionDTO riesgo : riesgos) {
				riesgo.setPorcentajeDeducible(coberturaCotizacionDTO.getPorcentajeDeducible());
				riesgo.setClaveTipoDeducible(coberturaCotizacionDTO.getClaveTipoDeducible());
				riesgo.setValorMinimoLimiteDeducible(coberturaCotizacionDTO.getValorMinimoLimiteDeducible());
				riesgo.setValorMaximoLimiteDeducible(coberturaCotizacionDTO.getValorMaximoLimiteDeducible());
				riesgo.setClaveTipoLimiteDeducible(coberturaCotizacionDTO.getClaveTipoLimiteDeducible());
				riesgo.setClaveAutDeducible(Sistema.AUTORIZACION_NO_REQUERIDA);
			}
			coberturaCotizacionDTO.setRiesgoCotizacionLista(riesgos);

			Boolean esCoberturaContratada = coberturaCotizacionDTO.getClaveContrato().shortValue() == 0? true : false;
			coberturaCotizacionDTO.setClaveContrato((short) 1);
			coberturaCotizacionDN.modificar(coberturaCotizacionDTO);

			if(esCoberturaContratada || sumaAseguradaAnterior.doubleValue() != coberturaCotizacionDTO.getValorSumaAsegurada().doubleValue())
				coberturaCotizacionDTO = CalculoCotizacionDN.getInstancia().calcularCobertura(coberturaCotizacionDTO, 
						UtileriasWeb.obtieneNombreUsuario(request),esContratada);			
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"updated\" sid=\"" + gr_id
				+ "\" tid=\"" + gr_id + "\" ></action></data>");
		pw.flush();
		pw.close();
	}

	private String getCoberturasRequeridasCadena(
			List<CoberturaRequeridaDTO> coberturas) {
		StringBuilder cadenaCoberturas = new StringBuilder("");
		String cadena = "";
		if (coberturas != null) {
			for (CoberturaRequeridaDTO coberturaRequeridaDTO : coberturas) {
				cadenaCoberturas.append(coberturaRequeridaDTO.getId()
						.getIdToCoberturaRequerida().toString()
						).append("_");
			}
			cadena = cadenaCoberturas.toString();
			if (coberturas.size() > 0)
				cadena = cadena.substring(0, cadena.length() - 1);
		}
		return cadena;
	}

	private String getCoberturasExcluidasCadena(
			List<CoberturaExcluidaDTO> coberturas) {
		StringBuilder cadenaCoberturas = new StringBuilder("");
		String cadena = "";
		if (coberturas != null) {
			for (CoberturaExcluidaDTO coberturaExcluidaDTO : coberturas) {
				cadenaCoberturas.append(coberturaExcluidaDTO.getId()
						.getIdToCoberturaExcluida().toString()
						).append("_");
			}
			cadena = cadenaCoberturas.toString();
			if (coberturas.size() > 0)
				cadena = cadena.substring(0, cadena.length() - 1);
		}
		return cadena;
	}

	/**
	 * Method mostrarARD
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarARD(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		String idToCoberturaS = request.getParameter("idToCobertura");
		String claveTipoEndosoS = request.getParameter("tipoEndoso");
		String fecha = new String();

		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		BigDecimal idToCobertura = new BigDecimal(idToCoberturaS);
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		
		try {
			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(
				idToCotizacion);
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(
				idToCobertura);
			List<AumentoRiesgoCotizacionDTO> aumentos = CoberturaCotizacionDN
				.getInstancia().getAumentosPorCobertura(idToCotizacion,
					numeroInciso, idToSeccion, idToCobertura, nombreUsuario);
			List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN
				.getInstancia().getDescuentosPorCobertura(idToCotizacion,
					numeroInciso, idToSeccion, idToCobertura, nombreUsuario);
			List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN
				.getInstancia().getRecargosPorCobertura(idToCotizacion,
					numeroInciso, idToSeccion, idToCobertura, nombreUsuario);
			fecha = dateFormat.format(cotizacionDTO.getFechaCreacion());
			request.setAttribute("tituloCoberturas", coberturaDTO
					.getNombreComercial());
			request.setAttribute("idToCotizacionCadena", UtileriasWeb
					.llenarIzquierda(idToCotizacion.toString(), "0", 8));
			request.setAttribute("fecha", fecha);
			request.setAttribute("aumentos", aumentos);
			request.setAttribute("descuentos", descuentos);
			request.setAttribute("recargos", recargos);
			request.setAttribute("idToCotizacion", idToCotizacion);
			request.setAttribute("numeroInciso", numeroInciso);
			request.setAttribute("idToSeccion", idToSeccion);
			request.setAttribute("idToCobertura", idToCobertura);
			request.setAttribute("aumentosSize", aumentos.size());
			request.setAttribute("descuentosSize", descuentos.size());
			request.setAttribute("claveTipoEndoso", claveTipoEndosoS == null ? "":claveTipoEndosoS);
			request.setAttribute("recargosSize", recargos.size());

		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void guardarARD(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {
		BigDecimal idToCotizacion = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCotizacion")));
		BigDecimal numeroInciso = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("numeroInciso")));
		BigDecimal idToSeccion = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToSeccion")));
		BigDecimal idToCobertura = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCobertura")));
		String tipo = request.getParameter("tipo");
		Double valor = UtileriasWeb
				.regresaDouble(request.getParameter("valor"));
		int contrato = Integer.valueOf(request.getParameter("contrato"));
		String idARD = request.getParameter("idARD");
		String claveAutorizacion = request.getParameter("claveAutorizacion");

		CoberturaCotizacionDN.getInstancia()
				.guardarARD(idToCotizacion, numeroInciso, idToSeccion,
						idToCobertura, tipo, valor, contrato, idARD, claveAutorizacion, UtileriasWeb.obtieneNombreUsuario(request));
	}

	public void guardarAutorizarARD(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {
		BigDecimal idToCotizacion = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCotizacion")));
		BigDecimal numeroInciso = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("numeroInciso")));
		BigDecimal idToSeccion = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToSeccion")));
		BigDecimal idToCobertura = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCobertura")));
		String tipo = request.getParameter("tipo");
		String idARD = request.getParameter("idARD");

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);

		CoberturaCotizacionDN.getInstancia().guardarAutorizarARD(
				idToCotizacion, numeroInciso, idToSeccion, idToCobertura, idARD, tipo,
				usuario.getNombreUsuario());
	}

	public void guardarRechazarARD(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {
		BigDecimal idToCotizacion = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCotizacion")));
		BigDecimal numeroInciso = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("numeroInciso")));
		BigDecimal idToSeccion = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToSeccion")));
		BigDecimal idToCobertura = (UtileriasWeb.regresaBigDecimal(request
				.getParameter("idToCobertura")));
		String tipo = request.getParameter("tipo");
		String idARD = request.getParameter("idARD");

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);

		CoberturaCotizacionDN.getInstancia().guardarRechazarARD(idToCotizacion,
				numeroInciso, idToSeccion, idToCobertura, idARD, tipo,
				usuario.getNombreUsuario());
	}

	/**
	 * Method listarAutorizacionRiesgos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException
	 */
	public void listarAutorizacionCoberturas(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToSeccion = request.getParameter("idToSeccion");
		String numeroInciso = request.getParameter("numeroInciso");
		@SuppressWarnings("unused")
		String sumaAsegurada = "";
		try {

			SeccionCotizacionDTO  seccionCotizacionDTO = new SeccionCotizacionDTO();
			
			List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
			seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
			seccionCotizacionDTO.getId().setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			seccionCotizacionDTO.getId().setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso));
			seccionCotizacionDTO.getId().setIdToSeccion(UtileriasWeb.regresaBigDecimal(idToSeccion));

			coberturas = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request))
					.getCoberturasPorSeccion(seccionCotizacionDTO);		

			MidasJsonBase json = new MidasJsonBase();
			StringBuilder sendRequest = new StringBuilder("");
			for (CoberturaCotizacionDTO cobertura : coberturas) {
				ComisionCotizacionDN comisionCotizacionDN = ComisionCotizacionDN
						.getInstancia();
				ComisionCotizacionId idComision = new ComisionCotizacionId();
				idComision.setIdToCotizacion(cobertura.getId()
						.getIdToCotizacion());
				idComision.setIdTcSubramo(cobertura.getIdTcSubramo());
				idComision.setTipoPorcentajeComision((short) 1);
				ComisionCotizacionDTO comision = comisionCotizacionDN
						.getPorId(idComision);

				CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
						.getPorId(cobertura.getId().getIdToCobertura());

				String requeridas = this
						.getCoberturasRequeridasCadena(CoberturaRequeridaDN
								.getInstancia().buscarPorCoberturaBase(coberturaDTO.getIdToCobertura()));
				String excluidas = this
						.getCoberturasExcluidasCadena(ExclusionCoberturaDN
								.getInstancia().buscarPorCoberturaBase(coberturaDTO.getIdToCobertura()));

				// Suma Asegurada
				switch (Integer.valueOf(
						coberturaDTO.getClaveTipoSumaAsegurada()).intValue()) {
				case 1:
					sumaAsegurada = cobertura.getValorSumaAsegurada()
							.toString();
					break;
				case 2:
					sumaAsegurada = "Amparada";
					break;
				case 3:
					sumaAsegurada = cobertura.getValorSumaAsegurada()
					.toString();
					break;
				}
				sendRequest.delete(0, sendRequest.length());
				sendRequest.append("sendRequest(null,'/MidasWeb/cotizacion/cobertura/mostrarAutorizarARD.do?idToCotizacion=");
				sendRequest.append(cobertura.getId().getIdToCotizacion().toString());
				sendRequest.append("&numeroInciso=").append(
						cobertura.getId().getNumeroInciso().toString());
				sendRequest.append("&idToSeccion=").append(
						cobertura.getId().getIdToSeccion().toString());
				sendRequest.append("&idToCobertura=").append(
						cobertura.getId().getIdToCobertura().toString());
				sendRequest.append("','configuracion_detalle',null);");
				MidasJsonRow row = new MidasJsonRow();
				row.setId(cobertura.getId().getIdToCobertura() + "_"
						+ cobertura.getId().getIdToCobertura());
				row.setDatos(
						cobertura.getId().getIdToCotizacion().toString(),
						cobertura.getId().getNumeroInciso().toString(),
						cobertura.getId().getIdToSeccion().toString(),
						cobertura.getId().getIdToCobertura().toString(),
						cobertura.getClaveObligatoriedad().toString(),
						coberturaDTO.getClaveTipoSumaAsegurada(),
						requeridas,
						excluidas,
						coberturaDTO.getNombreComercial(),
						cobertura.getClaveContrato().toString(),
						fMonto.format(cobertura.getValorSumaAsegurada()),
						fCuota.format(cobertura.getValorCuota() * 1000D),
						fMonto.format(cobertura.getValorPrimaNeta()),
						comision.getValorComisionCotizacion().toString(), 
						cobertura.getPorcentajeCoaseguro().toString(),
						cobertura.getClaveAutCoaseguro().toString(), cobertura
								.getPorcentajeDeducible().toString(), cobertura
								.getClaveAutDeducible().toString(),
						MidasJsonRow.generarLineaImagenDataGrid(
								Sistema.ICONO_DETALLE,
								"Aumentos/Recargos/Descuentos", sendRequest.toString(),
								null), coberturaDTO.getClaveDesglosaRiesgos(),
						cobertura.getSeccionCotizacionDTO().getSeccionDTO()
								.getClaveSubIncisos().toString());
				json.addRow(row);
			}
			response.setContentType("text/json");
			System.out.println(json);
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Method guardarCoberturaAutorizado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException
	 * @throws SystemException
	 */
	public void guardarCoberturaAutorizado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, SystemException {
		String gr_id = request.getParameter("gr_id");
		String status = request.getParameter("!nativeeditor_status");
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		String action = "";
		String extra = "";
		if (status.equals("updated")) {
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			id.setIdToCotizacion(idToCotizacion);
			BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
			id.setNumeroInciso(numeroInciso);
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			id.setIdToSeccion(idToSeccion);
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
			id.setIdToCobertura(idToCobertura);

			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			coberturaCotizacionDTO.setId(id);

			CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
			CoberturaCotizacionDTO cobertura = coberturaCotizacionDN.getPorId(coberturaCotizacionDTO);

			String coaseguroAutorizado = request.getParameter("coaseguroAutorizado");
			String deducibleAutorizado = request.getParameter("deducibleAutorizado");
			if (coaseguroAutorizado != null && coaseguroAutorizado.equals("1")) {
				action = "coaseguroActualizado";
				cobertura.setClaveAutCoaseguro(Sistema.AUTORIZADA);
				cobertura.setCodigoUsuarioAutCoaseguro(nombreUsuario);
			}
			if (deducibleAutorizado != null && deducibleAutorizado.equals("1")) {
				if (action.equals("")) {
					action = "deducibleActualizado";
				} else {
					extra = "deducibleActualizado";
				}
				cobertura.setClaveAutDeducible(Sistema.AUTORIZADA);
				cobertura.setCodigoUsuarioAutDeducible(nombreUsuario);
			}
			coberturaCotizacionDN.modificar(cobertura);
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + gr_id
				+ "\" tid=\"" + gr_id + "\" extra=\"" + extra
				+ "\"  ></action></data>");
		pw.flush();
		pw.close();
	}

	/**
	 * Method guardarCoberturaRechazado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException
	 * @throws SystemException
	 */
	public void guardarCoberturaRechazado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, SystemException {
		String gr_id = request.getParameter("gr_id");
		String status = request.getParameter("!nativeeditor_status");
		String action = "";
		String extra = "";
		if (status.equals("updated")) {
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request
					.getParameter("idToCotizacion"));
			id.setIdToCotizacion(idToCotizacion);
			BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request
					.getParameter("numeroInciso"));
			id.setNumeroInciso(numeroInciso);
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request
					.getParameter("idToSeccion"));
			id.setIdToSeccion(idToSeccion);
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request
					.getParameter("idToCobertura"));
			id.setIdToCobertura(idToCobertura);

			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			coberturaCotizacionDTO.setId(id);
			CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN
					.getInstancia();
			CoberturaCotizacionDTO cobertura = coberturaCotizacionDN
					.getPorId(coberturaCotizacionDTO);

			String coaseguroAutorizado = request
					.getParameter("coaseguroAutorizado");
			String deducibleAutorizado = request
					.getParameter("deducibleAutorizado");
			if (coaseguroAutorizado != null && coaseguroAutorizado.equals("1")) {
				action = "coaseguroActualizado";
				cobertura.setClaveAutCoaseguro(Sistema.RECHAZADA);
			}
			if (deducibleAutorizado != null && deducibleAutorizado.equals("1")) {
				if (action.equals("")) {
					action = "deducibleActualizado";
				} else {
					extra = "deducibleActualizado";
				}
				cobertura.setClaveAutDeducible(Sistema.RECHAZADA);
			}
			coberturaCotizacionDN.modificar(cobertura);
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + gr_id
				+ "\" tid=\"" + gr_id + "\" extra=\"" + extra
				+ "\"  ></action></data>");
		pw.flush();
		pw.close();
	}
}
