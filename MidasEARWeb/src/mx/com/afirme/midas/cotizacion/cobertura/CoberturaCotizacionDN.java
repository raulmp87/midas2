package mx.com.afirme.midas.cotizacion.cobertura;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionSN;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaCotizacionDN {
	public static final CoberturaCotizacionDN INSTANCIA = new CoberturaCotizacionDN();

	public static CoberturaCotizacionDN getInstancia() {
		return CoberturaCotizacionDN.INSTANCIA;
	}

	/**
	 * Method agregar()
	 * 
	 * @param coberturaCotizacionDTO
	 * @return CoberturaCotizacionDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public CoberturaCotizacionDTO agregar(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.agregar(coberturaCotizacionDTO);
	}

	/**
	 * Method modificar()
	 * 
	 * @param coberturaCotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public CoberturaCotizacionDTO modificar(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.modificar(coberturaCotizacionDTO);
	}

	/**
	 * getPorId()
	 * 
	 * @param coberturaCotizacionDTO
	 * @return CoberturaCotizacionDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public CoberturaCotizacionDTO getPorId(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new CoberturaCotizacionSN().getPorId(coberturaCotizacionDTO);
	}

	/**
	 * Method listarPorSeccionCotizacionId().
	 * 
	 * @param seccionCotizacionDTOId
	 * @param listarSoloContratadas
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionId(
			SeccionCotizacionDTOId seccionCotizacionDTOId,
			Boolean listarSoloContratadas) throws ExcepcionDeAccesoADatos,
			SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.listarPorSeccionCotizacionId(
				seccionCotizacionDTOId, listarSoloContratadas);
	}

	/**
	 * Method actualizaCoberturaCotizacionContratada()
	 * 
	 * @param cob
	 * @param tipoCoaseguroS
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void actualizaCoberturaCotizacionContratada(
			CoberturaCotizacionDTO cob, String tipoCoaseguroS)
			throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();

		coberturaCotizacionDTO = coberturaCotizacionSN.getPorId(cob);
		coberturaCotizacionDTO.setValorSumaAsegurada(cob
				.getValorSumaAsegurada());
		coberturaCotizacionDTO.setClaveContrato(cob.getClaveContrato());
		coberturaCotizacionDTO = coberturaCotizacionSN
				.modificar(coberturaCotizacionDTO);
		if (tipoCoaseguroS.equals("1")) {
			// TODO Verificar con jorge la regla de modificacion de sumas
			// aseguradas
			listaCoberturas = null;// coberturaCotizacionSN.listarCoberturasDeSumaAsegurada(coberturaCotizacionDTO);
			if (listaCoberturas != null) {
				for (CoberturaCotizacionDTO cobertura : listaCoberturas) {
					cobertura.setValorSumaAsegurada(coberturaCotizacionDTO
							.getValorSumaAsegurada());
					coberturaCotizacionSN.modificar(cobertura);
				}
			}
		}

	}

	/**
	 * Method listarCoberturasContratadasPorSeccion()
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarCoberturasContratadasPorSeccion(idToCotizacion,
						idToSeccion);
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getIdToCobertura() == null) {
				CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
						.getPorId(cobertura.getId().getIdToCobertura());
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return coberturas;
	}

	/**
	 * Method listarRecargosRiesgoPorCoberturaCotizacion().
	 * 
	 * @param coberturaCotizacionDTO
	 * @param idRecargoEspecial
	 * @param contratado
	 * @return
	 * @throws SystemException
	 */
	public List<RecargoRiesgoCotizacionDTO> listarRecargosRiesgoPorCoberturaCotizacion(CoberturaCotizacionDTO coberturaCotizacionDTO,
			BigDecimal idRecargoEspecial, Boolean contratado)throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.listarRecargosRiesgoPorCoberturaCotizacion(
						coberturaCotizacionDTO.getId(), idRecargoEspecial,contratado);
	}
	
	/**
	 * 
	 * @param coberturaCotizacionDTO
	 * @param idRecargoEspecial
	 * @param contratado
	 * @return
	 * @throws SystemException
	 */
	public List<AumentoRiesgoCotizacionDTO> listarAumentosRiesgoPorCoberturaCotizacion(CoberturaCotizacionDTO coberturaCotizacionDTO,
			BigDecimal idRecargoEspecial, Boolean contratado)throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.listarAumentosRiesgoPorCoberturaCotizacion(
						coberturaCotizacionDTO.getId(), idRecargoEspecial,contratado);
	}

	/**
	 * Busca los descuentos de los riesgos de una cobertura .
	 * 
	 * @param coberturaCotizacionDTO
	 * @param idDescuentoEspecial
	 * @param contratado
	 * @return List<DescuentoRiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionDTO coberturaCotizacionDTO,
			BigDecimal idDescuentoEspecial, Boolean contratado)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN
				.listarDescuentosRiesgoPorCoberturaCotizacion(
						coberturaCotizacionDTO.getId(), idDescuentoEspecial,
						contratado);
	}

	/**
	 * De una lista de secciones se calculan sus primaNeta y PrimaNetaOriginal,
	 * para cada una de ellas; apartir de la sumatoria de sus coberturas.
	 * 
	 * @param secciones
	 * @throws SystemException
	 */
	public static void calcularPrimasOriginalesSeccion(
			List<SeccionCotizacionDTO> secciones,
			boolean aplicaIgualacionAInciso) throws SystemException {
		for (SeccionCotizacionDTO seccion : secciones) {
			BigDecimal primaNetaSeccion = BigDecimal.ZERO;
			BigDecimal primaNetaOriginalSeccion = BigDecimal.ZERO;

			calcularPrimasOriginalesCobertura(seccion
					.getCoberturaCotizacionLista(), aplicaIgualacionAInciso);
			for (CoberturaCotizacionDTO cobertura : seccion
					.getCoberturaCotizacionLista()) {
				if (cobertura.getValorPrimaNeta() != null)
					primaNetaSeccion = primaNetaSeccion.add(BigDecimal
							.valueOf(cobertura.getValorPrimaNeta()));
				if (cobertura.getValorPrimaNetaOriginal() != null)
					primaNetaOriginalSeccion = primaNetaOriginalSeccion
							.add(BigDecimal.valueOf(cobertura
									.getValorPrimaNetaOriginal()));
			}
			seccion.setValorPrimaNeta(primaNetaSeccion.doubleValue());
			seccion.setValorPrimaNetaOriginal(primaNetaOriginalSeccion
					.doubleValue());
		}
	}

	/**
	 * De una lista de coberturas se calcula, el valor de la cuota, valorCuota
	 * original y prima neta original.
	 * 
	 * @param coberturas
	 * @throws SystemException
	 */

	public static void calcularPrimasOriginalesCobertura(
			List<CoberturaCotizacionDTO> coberturas,
			boolean aplicaIgualacionAInciso) throws SystemException {
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			BigDecimal primaNeta = BigDecimal.valueOf(cobertura
					.getValorPrimaNeta());
			BigDecimal sumaAsegurada = BigDecimal.valueOf(cobertura
					.getValorSumaAsegurada());
			if (sumaAsegurada.doubleValue() > 0d) {
				BigDecimal valorCuota = primaNeta.divide(sumaAsegurada, 10,
						RoundingMode.HALF_UP);
				cobertura.setValorCuota(valorCuota.doubleValue());
				BigDecimal primaNetaOriginal = BigDecimal.ZERO;
				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
						.getClaveIgualacion() == 1) {
					List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN
							.getInstancia()
							.listarRecargosRiesgoPorCoberturaCotizacion(
									cobertura, Sistema.RECARGO_IGUALACION,
									Boolean.TRUE);
					if (!recargos.isEmpty()) {
						RecargoRiesgoCotizacionDTO recargoDTO = recargos.get(0);
						if (recargoDTO.getClaveContrato().shortValue() == 1) {
							BigDecimal recargo = BigDecimal.valueOf(recargoDTO
									.getValorRecargo());
							if (!aplicaIgualacionAInciso)
								cobertura.getId().setNumeroInciso(null);
							Double primaARDT = DetallePrimaCoberturaCotizacionDN
									.getInstancia()
									.sumPrimaNetaARDTCoberturaCotizacion(
											cobertura.getId());
							if (primaARDT != null) {
								primaNetaOriginal = primaNeta
										.subtract(BigDecimal.valueOf(primaARDT)
												.multiply(recargo).divide(
														BigDecimal
																.valueOf(100D),
														12,
														RoundingMode.HALF_UP));
							} else {
								primaNetaOriginal = primaNeta;
							}
							/*
							 * recargo =
							 * recargo.divide(BigDecimal.valueOf(100D), 4,
							 * RoundingMode.HALF_UP).add(BigDecimal.ONE);
							 * primaNetaOriginal = primaNeta.divide(recargo, 4,
							 * RoundingMode.HALF_UP);
							 */
						}
					} else {
						List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN
								.getInstancia()
								.listarDescuentosRiesgoPorCoberturaCotizacion(
										cobertura,
										Sistema.DESCUENTO_IGUALACION,
										Boolean.TRUE);
						if (!descuentos.isEmpty()) {
							DescuentoRiesgoCotizacionDTO descuentoDTO = descuentos
									.get(0);
							if (descuentoDTO.getClaveContrato().shortValue() == 1) {
								BigDecimal descuento = BigDecimal
										.valueOf(descuentoDTO
												.getValorDescuento());
								if (!aplicaIgualacionAInciso)
									cobertura.getId().setNumeroInciso(null);
								Double primaARDT = DetallePrimaCoberturaCotizacionDN
										.getInstancia()
										.sumPrimaNetaARDTCoberturaCotizacion(
												cobertura.getId());
								if (primaARDT != null) {
									primaNetaOriginal = primaNeta
											.add(BigDecimal
													.valueOf(primaARDT)
													.multiply(descuento)
													.divide(
															BigDecimal
																	.valueOf(100D),
															12,
															RoundingMode.HALF_UP));
								} else {
									primaNetaOriginal = primaNeta;
								}
								/*
								 * descuento =
								 * BigDecimal.ONE.subtract(descuento.divide(
								 * BigDecimal.valueOf(100D), 4,
								 * RoundingMode.HALF_UP)); primaNetaOriginal =
								 * primaNeta.divide(descuento, 4,
								 * RoundingMode.HALF_UP);
								 */
							}
						} else {
							primaNetaOriginal = primaNeta;
						}
					}
				} else {
					primaNetaOriginal = primaNeta;
				}
				BigDecimal cuotaOriginal = primaNetaOriginal.divide(
						sumaAsegurada, 10, RoundingMode.HALF_UP);
				cobertura.setValorCuotaOriginal(cuotaOriginal.doubleValue());
				cobertura.setValorPrimaNetaOriginal(primaNetaOriginal
						.doubleValue());
			}
		}
	}

	/**
	 * Busca todas las coberturas contratadas de una cotizacion.
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion) throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarCoberturasContratadas(idToCotizacion);
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getIdToCobertura() == null) {
				CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
						.getPorId(cobertura.getId().getIdToCobertura());
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return coberturas;
	}

	/**
	 * Busca todas las coberturas contratadas de una cotizacion.
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		coberturaCotizacionDTO.setId(id);
		coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);

		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarFiltrado(coberturaCotizacionDTO);

		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getIdToCobertura() == null) {
				CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
						.getPorId(cobertura.getId().getIdToCobertura());
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return coberturas;
	}

	/**
	 * Busca todas las coberturas contratadas de una cotizacion, y determina si
	 * se aplica merge o no ,en la persistencia.
	 * 
	 * @param idToCotizacion
	 * @param aplicarMerge
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion, boolean aplicarMerge)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarCoberturasContratadas(idToCotizacion, aplicarMerge);
		return coberturas;
	}

	/**
	 * Busca todas las coberturas contratadas de una cotizacion, para presentar
	 * un reporte.
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasParaReporte(
			BigDecimal idToCotizacion) throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarCoberturasContratadas(idToCotizacion);
		Map<BigDecimal, CoberturaDTO> mapaCoberturas = new HashMap<BigDecimal, CoberturaDTO>();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO() == null) {
				if (mapaCoberturas.containsKey(cobertura.getId()
						.getIdToCobertura())) {
					CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
					coberturaSeccionDTO.setCoberturaDTO(mapaCoberturas
							.get(cobertura.getId().getIdToCobertura()));
					cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
				} else {
					CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
							.getPorId(cobertura.getId().getIdToCobertura());
					CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
					coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
					cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
					mapaCoberturas.put(cobertura.getId().getIdToCobertura(),
							coberturaDTO);
				}
			}
		}
		return coberturas;
	}

	/**
	 * Busca todas las coberturas contratadas a partir de un id de cotizacion,
	 * un numero de inciso y un id de seccion.
	 * 
	 * @param id
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCobertura(
			CoberturaCotizacionId id) throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarCoberturasContratadasPorSeccionCobertura(id);
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getIdToCobertura() == null) {
				CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
						.getPorId(cobertura.getId().getIdToCobertura());
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return coberturas;
	}

	/**
	 * Verifica si la cobertura, aplic� o no primer riesgo.
	 * 
	 * @param coberturaCotizacionDTO
	 * @return Boolean true o false
	 * @throws SystemException
	 */
	public Boolean aplicoPrimerRiesgo(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		AgrupacionCotId id = new AgrupacionCotId();
		id
				.setIdToCotizacion(coberturaCotizacionDTO.getId()
						.getIdToCotizacion());
		id.setNumeroAgrupacion(coberturaCotizacionDTO.getNumeroAgrupacion());
		AgrupacionCotDTO agrupacion = coberturaCotizacionSN
				.obtenerAgrupacionCotizacion(id);
		if (agrupacion == null) {
			return Boolean.FALSE;
		}
		return agrupacion.getClaveTipoAgrupacion().shortValue() == 1 ? Boolean.TRUE
				: Boolean.FALSE;
	}

	/**
	 * Verifica si la cobertura, aplic� o no LUC.
	 * 
	 * @param coberturaCotizacionDTO
	 * @return Boolean true o false
	 * @throws SystemException
	 */
	public Boolean aplicoLUC(CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		AgrupacionCotId id = new AgrupacionCotId();
		id
				.setIdToCotizacion(coberturaCotizacionDTO.getId()
						.getIdToCotizacion());
		id.setNumeroAgrupacion(coberturaCotizacionDTO.getNumeroAgrupacion());
		AgrupacionCotDTO agrupacion = coberturaCotizacionSN
				.obtenerAgrupacionCotizacion(id);
		if (agrupacion == null) {
			return Boolean.FALSE;
		}
		return agrupacion.getClaveTipoAgrupacion().shortValue() != 1 ? Boolean.TRUE
				: Boolean.FALSE;
	}

	/**
	 * Lista los registros de CoberturaCotizacionDTO usando como filtro los
	 * atributos del objeto CoberturaCotizacionId recibido. Los atributos usados
	 * son: idToCotizacion, idToCobertura, idToSeccion y numeroInciso.
	 * 
	 * @param CoberturaCotizacionId
	 *            id
	 * @return List<CoberturaCotizacionDTO> lista de entidades encontrada
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException
	 */
	public List<CoberturaCotizacionDTO> listarPorIdFiltrado(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos,
			SystemException {
		return new CoberturaCotizacionSN().listarPorIdFiltrado(id);
	}

	/**
	 * Lista los registros de CoberturaCotizacionDTO usando como filtro los
	 * atributos del objeto CoberturaCotizacionDTO.
	 * 
	 * @param coberturaCotizacionDTO
	 * @return List<CoberturaCotizacionDTO> lista de entidades encontrada
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarFiltrado(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new CoberturaCotizacionSN()
				.listarFiltrado(coberturaCotizacionDTO);
	}

	/**
	 * Lista los registros de AumentoRiesgoCotizacionDTO por cotizacion este
	 * metodo regresa todos los Aumentos asociados a una cotizacion y agrega el
	 * campo clave tipo que indica a que entidad pertenece P=Producto T=Tipo
	 * Poliza C=Cobertura
	 * 
	 * @param BigDecimal
	 *            IdToCotizacion
	 * @param BigDecimal
	 *            numeroInciso
	 * @param BigDecimal
	 *            idToSeccion
	 * @param BigDecimal
	 *            idToCobertura
	 * @return List<AumentoRiesgoCotizacionDTO> lista de entidades encontrada
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException
	 */
	public List<AumentoRiesgoCotizacionDTO> getAumentosPorCobertura(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			String nombreUsuario) throws ExcepcionDeAccesoADatos,
			SystemException {

		CotizacionSN cotizacionSN = new CotizacionSN(nombreUsuario);
		List<AumentoRiesgoCotizacionDTO> aumentos = new ArrayList<AumentoRiesgoCotizacionDTO>();
		CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(idToCotizacion);

		if (cotizacionDTO != null) {
			AumentoRiesgoCotizacionSN aumentoRiesgoCotizacionSN = new AumentoRiesgoCotizacionSN();

			RiesgoCotizacionId id = new RiesgoCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setIdToCobertura(idToCobertura);

			List<AumentoRiesgoCotizacionDTO> aumentosPorCoberturaCotizacion = aumentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);

			if (aumentosPorCoberturaCotizacion != null
					&& aumentosPorCoberturaCotizacion.size() > 0) {

				AumentoRiesgoCotizacionDTO aumentoPorCoberturaCotizacion = aumentosPorCoberturaCotizacion
						.get(0);
				id.setIdToRiesgo(aumentoPorCoberturaCotizacion.getId()
						.getIdToRiesgo());

				aumentos = aumentoRiesgoCotizacionSN.findByRiesgoCotizacion(id);

			}

		}
		return aumentos;
	}

	/**
	 * Lista los registros de DescuentoRiesgoCotizacionDTO por cotizacion este
	 * metodo regresa todos los Descuentos asociados a una cotizacion y agrega
	 * el campo clave tipo que indica a que entidad pertenece P=Producto T=Tipo
	 * Poliza C=Cobertura
	 * 
	 * @param BigDecimal
	 *            IdToCotizacion
	 * @param BigDecimal
	 *            numeroInciso
	 * @param BigDecimal
	 *            idToSeccion
	 * @param BigDecimal
	 *            idToCobertura
	 * 
	 * @return List<DescuentoRiesgoCotizacionDTO> lista de entidades encontrada
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException
	 */
	public List<DescuentoRiesgoCotizacionDTO> getDescuentosPorCobertura(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			String nombreUsuario) throws ExcepcionDeAccesoADatos,
			SystemException {

		CotizacionSN cotizacionSN = new CotizacionSN(nombreUsuario);
		List<DescuentoRiesgoCotizacionDTO> descuentos = new ArrayList<DescuentoRiesgoCotizacionDTO>();
		CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(idToCotizacion);

		if (cotizacionDTO != null) {
			DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();

			RiesgoCotizacionId id = new RiesgoCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setIdToCobertura(idToCobertura);

			List<DescuentoRiesgoCotizacionDTO> descuentosPorCoberturaCotizacion = descuentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);

			if (descuentosPorCoberturaCotizacion != null
					&& descuentosPorCoberturaCotizacion.size() > 0) {

				DescuentoRiesgoCotizacionDTO descuentoPorCoberturaCotizacion = descuentosPorCoberturaCotizacion
						.get(0);
				id.setIdToRiesgo(descuentoPorCoberturaCotizacion.getId()
						.getIdToRiesgo());

				descuentos = descuentoRiesgoCotizacionSN
						.findByRiesgoCotizacion(id);

			}

		}
		return descuentos;
	}

	/**
	 * Lista los registros de RecargoRiesgoCotizacionDTO por cotizacion este
	 * metodo regresa todos los Recargos asociados a una cotizacion y agrega el
	 * campo clave tipo que indica a que entidad pertenece P=Producto T=Tipo
	 * Poliza C=Cobertura
	 * 
	 * @param BigDecimal
	 *            IdToCotizacion
	 * @param BigDecimal
	 *            numeroInciso
	 * @param BigDecimal
	 *            idToSeccion
	 * @param BigDecimal
	 *            idToCobertura
	 * 
	 * @return List<RecargoRiesgoCotizacionDTO> lista de entidades encontrada
	 * @throws ExcepcionDeAccesoADatos
	 *             , SystemException
	 */
	public List<RecargoRiesgoCotizacionDTO> getRecargosPorCobertura(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			String nombreUsuario) throws ExcepcionDeAccesoADatos,
			SystemException {

		CotizacionSN cotizacionSN = new CotizacionSN(nombreUsuario);
		List<RecargoRiesgoCotizacionDTO> recargos = new ArrayList<RecargoRiesgoCotizacionDTO>();
		CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(idToCotizacion);

		if (cotizacionDTO != null) {
			RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();

			RiesgoCotizacionId id = new RiesgoCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setIdToCobertura(idToCobertura);

			List<RecargoRiesgoCotizacionDTO> recargosPorCoberturaCotizacion = recargoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);

			if (recargosPorCoberturaCotizacion != null
					&& recargosPorCoberturaCotizacion.size() > 0) {

				RecargoRiesgoCotizacionDTO recargoPorCoberturaCotizacion = recargosPorCoberturaCotizacion
						.get(0);
				id.setIdToRiesgo(recargoPorCoberturaCotizacion.getId()
						.getIdToRiesgo());

				recargos = recargoRiesgoCotizacionSN.findByRiesgoCotizacion(id);

			}

		}
		return recargos;

	}

	/**
	 * Mehtod guardarAutorizarARD().
	 * 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param tipo
	 * @param usuario
	 * @param usuario
	 * @throws SystemException
	 */
	public void guardarAutorizarARD(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion,
			BigDecimal idToCobertura, String idARD, String tipo, String usuario)
			throws SystemException {
		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);

		if (tipo.equals("A")) {
			AumentoRiesgoCotizacionSN aumentoRiesgoCotizacionSN = new AumentoRiesgoCotizacionSN();
			List<AumentoRiesgoCotizacionDTO> aumentosPorCoberturaCotizacion = aumentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			for (AumentoRiesgoCotizacionDTO aumentoPorCoberturaCotizacion : aumentosPorCoberturaCotizacion) {
				if (idARD != null
						&& Double.parseDouble(idARD) == aumentoPorCoberturaCotizacion
								.getId().getIdToAumentoVario().doubleValue()) {
					aumentoPorCoberturaCotizacion
							.setClaveAutorizacion(Sistema.AUTORIZADA);
					aumentoPorCoberturaCotizacion
							.setCodigoUsuarioAutorizacion(usuario);
					aumentoRiesgoCotizacionSN
							.modificar(aumentoPorCoberturaCotizacion);
					break;
				}
			}
		} else if (tipo.equals("D")) {
			DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
			List<DescuentoRiesgoCotizacionDTO> descuentosPorCoberturaCotizacion = descuentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			for (DescuentoRiesgoCotizacionDTO descuentoPorCoberturaCotizacion : descuentosPorCoberturaCotizacion) {
				if (idARD != null
						&& Double.parseDouble(idARD) == descuentoPorCoberturaCotizacion
								.getId().getIdToDescuentoVario().doubleValue()) {
					descuentoPorCoberturaCotizacion
							.setClaveAutorizacion(Sistema.AUTORIZADA);
					descuentoPorCoberturaCotizacion
							.setCodigoUsuarioAutorizacion(usuario);
					descuentoRiesgoCotizacionSN
							.modificar(descuentoPorCoberturaCotizacion);
					break;
				}
			}
		} else if (tipo.equals("R")) {
			RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
			List<RecargoRiesgoCotizacionDTO> recargosPorCoberturaCotizacion = recargoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			for (RecargoRiesgoCotizacionDTO recargoPorCoberturaCotizacion : recargosPorCoberturaCotizacion) {
				if (idARD != null
						&& Double.parseDouble(idARD) == recargoPorCoberturaCotizacion
								.getId().getIdToRecargoVario().doubleValue()) {
					recargoPorCoberturaCotizacion
							.setClaveAutorizacion(Sistema.AUTORIZADA);
					recargoPorCoberturaCotizacion
							.setCodigoUsuarioAutorizacion(usuario);
					recargoRiesgoCotizacionSN
							.modificar(recargoPorCoberturaCotizacion);
					break;
				}
			}
		}
	}

	/**
	 * Method guardarRechazarARD().
	 * 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param tipo
	 * @param usuario
	 * @param usuario
	 * @throws SystemException
	 */
	public void guardarRechazarARD(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion,
			BigDecimal idToCobertura, String idARD, String tipo, String usuario)
			throws SystemException {
		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);

		if (tipo.equals("A")) {
			AumentoRiesgoCotizacionSN aumentoRiesgoCotizacionSN = new AumentoRiesgoCotizacionSN();

			List<AumentoRiesgoCotizacionDTO> aumentosPorCoberturaCotizacion = aumentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			for (AumentoRiesgoCotizacionDTO aumentoPorCoberturaCotizacion : aumentosPorCoberturaCotizacion) {
				if (idARD != null
						&& Double.parseDouble(idARD) == aumentoPorCoberturaCotizacion
								.getId().getIdToAumentoVario().doubleValue()) {
					aumentoPorCoberturaCotizacion
							.setClaveAutorizacion(Sistema.RECHAZADA);
					aumentoPorCoberturaCotizacion
							.setCodigoUsuarioAutorizacion(usuario);
					aumentoRiesgoCotizacionSN
							.modificar(aumentoPorCoberturaCotizacion);
					break;
				}
			}
		} else if (tipo.equals("D")) {
			DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
			List<DescuentoRiesgoCotizacionDTO> descuentosPorCoberturaCotizacion = descuentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			for (DescuentoRiesgoCotizacionDTO descuentoPorCoberturaCotizacion : descuentosPorCoberturaCotizacion) {
				if (idARD != null
						&& Double.parseDouble(idARD) == descuentoPorCoberturaCotizacion
								.getId().getIdToDescuentoVario().doubleValue()) {
					descuentoPorCoberturaCotizacion
							.setClaveAutorizacion(Sistema.RECHAZADA);
					descuentoPorCoberturaCotizacion
							.setCodigoUsuarioAutorizacion(usuario);
					descuentoRiesgoCotizacionSN
							.modificar(descuentoPorCoberturaCotizacion);
					break;
				}
			}
		} else if (tipo.equals("R")) {
			RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
			List<RecargoRiesgoCotizacionDTO> recargosPorCoberturaCotizacion = recargoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			for (RecargoRiesgoCotizacionDTO recargoPorCoberturaCotizacion : recargosPorCoberturaCotizacion) {
				if (idARD != null
						&& Double.parseDouble(idARD) == recargoPorCoberturaCotizacion
								.getId().getIdToRecargoVario().doubleValue()) {
					recargoPorCoberturaCotizacion
							.setClaveAutorizacion(Sistema.RECHAZADA);
					recargoPorCoberturaCotizacion
							.setCodigoUsuarioAutorizacion(usuario);
					recargoRiesgoCotizacionSN
							.modificar(recargoPorCoberturaCotizacion);
					break;
				}
			}
		}
	}

	/**
	 * Mehtod guardarARD().
	 * 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param tipo
	 * @param valor
	 * @param contrato
	 * @param nombreUsuario
	 * @param nombreUsuario
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	public void guardarARD(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal idToCobertura, String tipo,
			Double valor, int contrato, String idARD, String claveAutorizacion,
			String nombreUsuario) throws SystemException {
		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);

		// 1) Se seleccionan/deseleccionan Aumentos, Recargos y Descuentos de
		// una cobertura espec�fica
		// (Se registran los mismos Aumentos, Recargos y Descuentos para todos
		// los riesgos de la cobertura).
		// Listar los registros descuentoRiesgoCot usando idToCotizacion,
		// idSeccion, idCobertura y numeroinciso
		if (tipo.equals("A")) {
			AumentoRiesgoCotizacionSN aumentoRiesgoCotizacionSN = new AumentoRiesgoCotizacionSN();
			List<AumentoRiesgoCotizacionDTO> aumentosPorCoberturaCotizacion = aumentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			if (aumentosPorCoberturaCotizacion != null
					&& aumentosPorCoberturaCotizacion.size() > 0)
				for (AumentoRiesgoCotizacionDTO aumentoRiesgoCot : aumentosPorCoberturaCotizacion) {
					if (idARD != null
							&& Double.parseDouble(idARD) == aumentoRiesgoCot
									.getId().getIdToAumentoVario()
									.doubleValue()) {
						aumentoRiesgoCot.setValorAumento(valor);
						aumentoRiesgoCot.setClaveContrato((short) contrato);
						if (claveAutorizacion != null) {
							if (claveAutorizacion.equals("0"))
								aumentoRiesgoCot
										.setClaveAutorizacion((short) 0);
							else if (claveAutorizacion.equals("1"))
								aumentoRiesgoCot
										.setClaveAutorizacion((short) 1);
						}
						AumentoRiesgoCotizacionDN.getInstancia().modificar(
								aumentoRiesgoCot);
						break;
					}
				}
		} else if (tipo.equals("D")) {
			DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
			List<DescuentoRiesgoCotizacionDTO> descuentosPorCoberturaCotizacion = descuentoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			if (descuentosPorCoberturaCotizacion != null
					&& descuentosPorCoberturaCotizacion.size() > 0) {
				for (DescuentoRiesgoCotizacionDTO descuentoRiesgoCot : descuentosPorCoberturaCotizacion) {
					if (idARD != null
							&& Double.parseDouble(idARD) == descuentoRiesgoCot
									.getId().getIdToDescuentoVario()
									.doubleValue()) {
						descuentoRiesgoCot.setValorDescuento(valor);
						descuentoRiesgoCot.setClaveContrato((short) contrato);
						if (claveAutorizacion != null) {
							if (claveAutorizacion.equals("0"))
								descuentoRiesgoCot
										.setClaveAutorizacion((short) 0);
							else if (claveAutorizacion.equals("1"))
								descuentoRiesgoCot
										.setClaveAutorizacion((short) 1);
						}
						DescuentoRiesgoCotizacionDN.getInstancia().modificar(
								descuentoRiesgoCot);
						break;
					}
				}
			}
		} else if (tipo.equals("R")) {
			RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
			List<RecargoRiesgoCotizacionDTO> recargosPorCoberturaCotizacion = recargoRiesgoCotizacionSN
					.findByCoberturaCotizacion(id);
			if (recargosPorCoberturaCotizacion != null
					&& recargosPorCoberturaCotizacion.size() > 0) {
				for (RecargoRiesgoCotizacionDTO recargoRiesgoCot : recargosPorCoberturaCotizacion) {
					if (idARD != null
							&& Double.parseDouble(idARD) == recargoRiesgoCot
									.getId().getIdToRecargoVario()
									.doubleValue()) {
						recargoRiesgoCot.setValorRecargo(valor);
						recargoRiesgoCot.setClaveContrato((short) contrato);
						if (claveAutorizacion != null) {
							if (claveAutorizacion.equals("0"))
								recargoRiesgoCot
										.setClaveAutorizacion((short) 0);
							else if (claveAutorizacion.equals("1"))
								recargoRiesgoCot
										.setClaveAutorizacion((short) 1);
						}
						RecargoRiesgoCotizacionDN.getInstancia().modificar(
								recargoRiesgoCot);
						break;
					}
				}
			}
		}
		// Los pasos siguientes se ejecutan en el siguiente m�todo, que es
		// com�n para los tres casos (aumento, descuento y recargo)
		CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN
				.getInstancia();
		CoberturaCotizacionId coberturaCotId = new CoberturaCotizacionId();
		coberturaCotId.setIdToCobertura(idToCobertura);
		coberturaCotId.setIdToCotizacion(idToCotizacion);
		coberturaCotId.setIdToSeccion(idToSeccion);
		coberturaCotId.setNumeroInciso(numeroInciso);
		calculoCotizacionDN.calcularARD(coberturaCotId, nombreUsuario);
	}

	/**
	 * Method borrar().
	 * 
	 * @param coberturaCotizacionDTO
	 * @throws SystemException
	 */
	public void borrar(CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		coberturaCotizacionSN.borrar(coberturaCotizacionDTO);
	}

	/**
	 * Method listarCoberturasDistintas().
	 * 
	 * @param idToCotizacion
	 * @return List<BigDecimal> lista de id's de coberturas
	 * @throws SystemException
	 */
	public List<BigDecimal> listarCoberturasDistintas(
			BigDecimal idToCotizacion, boolean soloContratadas)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.listarCoberturasDistintas(idToCotizacion,
				soloContratadas);
	}

	/**
	 * Method listarCoberturasCalculadasPrimerRiesgoLUC().
	 * 
	 * @param idToCotizacion
	 * @param numeroAgrupacion
	 * @return List<CoberturaCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasCalculadasPrimerRiesgoLUC(
			BigDecimal idToCotizacion, Short numeroAgrupacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new CoberturaCotizacionSN()
				.listarCoberturasCalculadasPrimerRiesgoLUC(idToCotizacion,
						numeroAgrupacion);
	}

	/**
	 * METODO QUE OBTIENE UNA COBERTURA QUE ESTE CONTRATADA Y QUE SU SECCION
	 * TAMBIEN LO ESTE
	 * 
	 * @param id
	 * @return CoberturaCotizacionDTO
	 * @throws SystemException
	 */
	public CoberturaCotizacionDTO obtenerCoberturaContratadaPorId(
			CoberturaCotizacionId id) throws SystemException {
		return new CoberturaCotizacionSN().obtenerCoberturaContratadaPorId(id);
	}

	/**
	 * Method listarCoberturasContratadasApliquenPrimerRiesgoLUC().
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasApliquenPrimerRiesgoLUC(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToCobertura) throws ExcepcionDeAccesoADatos,
			SystemException {
		return new CoberturaCotizacionSN()
				.listarCoberturasContratadasApliquenPrimerRiesgoLUC(
						idToCotizacion, numeroInciso, idToCobertura);
	}

	/**
	 * Method listarCoberturasAsociadasPorSeccionCotId().
	 * 
	 * @param seciCotizacionDTOId
	 * @param idToCoberturaBasica
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasAsociadasPorSeccionCotId(
			SeccionCotizacionDTOId seciCotizacionDTOId,
			BigDecimal idToCoberturaBasica) throws ExcepcionDeAccesoADatos,
			SystemException {
		return new CoberturaCotizacionSN()
				.listarCoberturasAsociadasPorSeccionCotId(seciCotizacionDTOId,
						idToCoberturaBasica);
	}

	/**
	 * Mehtod obtenerCoberturasPrimerRiesgoPorCotizacion().
	 * 
	 * @param idToCotizacion
	 * @return Map<CoberturaDTO,BigDecimal>
	 * @throws SystemException
	 */
	public Map<CoberturaDTO, BigDecimal> obtenerCoberturasPrimerRiesgoPorCotizacion(
			BigDecimal idToCotizacion) throws SystemException {
		Map<BigDecimal, BigDecimal> mapaIdCoberturasPrimerRiesgo = new CoberturaCotizacionSN()
				.obtenerIdCoberturasPrimerRiesgo(idToCotizacion);
		Map<BigDecimal, CoberturaDTO> mapaCoberturas = new HashMap<BigDecimal, CoberturaDTO>();
		Map<CoberturaDTO, BigDecimal> mapaCoberturasPrimerRiesgo = new HashMap<CoberturaDTO, BigDecimal>();
		for (BigDecimal idToCobertura : mapaIdCoberturasPrimerRiesgo.keySet()) {
			if (!mapaCoberturas.containsKey(idToCobertura)) {
				CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(
						idToCobertura);
				mapaCoberturas.put(idToCobertura, cobertura);
			}
			CoberturaDTO coberturaTMP = mapaCoberturas.get(idToCobertura);
			mapaCoberturasPrimerRiesgo.put(coberturaTMP,
					mapaIdCoberturasPrimerRiesgo.get(idToCobertura));
		}
		return mapaCoberturasPrimerRiesgo;
	}

	/**
	 * Mehtod listarPorSeccionCotizacionIdSinContratar().
	 * 
	 * @param id
	 * @return List<CoberturaCotizacionDTO> lista de coberturas
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionIdSinContratar(
			SeccionCotizacionDTOId id) throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN
				.listarPorSeccionCotizacionIdSinContratar(id);
	}

	/**
	 * Method obtenerSACoberturasBasicasIncendioPorInciso()
	 * 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return Double suma Asegurada
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public Double obtenerSACoberturasBasicasIncendioPorCotizacion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {
		return new CoberturaCotizacionSN()
				.obtenerSACoberturasBasicasIncendioPorCotizacion(idToCotizacion);
	}

	/**
	 * Obtiene un listado de coberturas contratadas en base a una seccion de
	 * cotizacion
	 * 
	 * @param seccionCotizacionDTOId
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new CoberturaCotizacionSN()
				.listarCoberturasContratadasPorSeccionCotizacion(seccionCotizacionDTOId);
	}

	/**
	 * Obtiene el total de coberturas contratadas por seccion
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @return
	 * @throws SystemException
	 */
	public Long obtenerTotalCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new CoberturaCotizacionSN()
				.obtenerTotalCoberturasContratadasPorSeccionCotizacion(seccionCotizacionDTOId);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasBasicasContratadas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso)
			throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionSN
				.listarCoberturasBasicasContratadas(idToCotizacion,
						numeroInciso);
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO() == null
					|| cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getIdToCobertura() == null) {
				CoberturaDTO coberturaDTO = CoberturaDN.getInstancia()
						.getPorId(cobertura.getId().getIdToCobertura());
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return coberturas;
	}

	public List<SubRamoDTO> listarSubRamosEndoso(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.listarSubRamosEndoso(idToCotizacion);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasEnConjunto(
			SeccionCotizacionDTOId seccion, List<BigDecimal> idsCoberturas)
			throws ExcepcionDeAccesoADatos, SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		return coberturaCotizacionSN.listarCoberturasContratadasEnConjunto(
				seccion, idsCoberturas);
	}
}