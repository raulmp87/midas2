package mx.com.afirme.midas.cotizacion.cobertura;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Eric Villafana
 * @since 10 Septiembre de 2009
 */
public class CoberturaCotizacionSN {
	private CoberturaCotizacionFacadeRemote beanRemoto;

	public CoberturaCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CoberturaCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public CoberturaCotizacionDTO agregar(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(coberturaCotizacionDTO);
			return coberturaCotizacionDTO;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(coberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CoberturaCotizacionDTO modificar(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(coberturaCotizacionDTO);
			return coberturaCotizacionDTO;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CoberturaCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public CoberturaCotizacionDTO getPorId(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(coberturaCotizacionDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Lista las entidades CoberturaCotizacionDTO relacionadas con la seccion
	 * cuyo id se recibe.
	 * 
	 * @param BigDecimal
	 *            coberturaCotizacionId
	 * @return List<IncisoCotizacionDTO> all CoberturaCotizacionDTO entities
	 */
	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionId(
			SeccionCotizacionDTOId seccionCotizacionDTO,
			Boolean listarSoloContratadas) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarPorSeccionCotizacionId(
					seccionCotizacionDTO, listarSoloContratadas);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void guardar(CoberturaCotizacionDTO coberturaCotizacionDTO) {
		try {
			beanRemoto.save(coberturaCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Lista las entidades CoberturaCotizacionDTO relacionadas con la suma
	 * asegurada
	 * 
	 * @param BigDecimal
	 *            coberturaCotizacionId
	 * @return List<IncisoCotizacionDTO> all CoberturaCotizacionDTO entities
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasDeSumaAsegurada(
			CoberturaCotizacionDTO coberturaCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto
					.listarCoberturasDeSumaAsegurada(coberturaCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> buscarPorPropiedad(
			String nombrePropiedad, Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion) {
		return beanRemoto.listarCoberturasContratadasPorSeccion(idToCotizacion,
				idToSeccion);
	}

	public List<RecargoRiesgoCotizacionDTO> listarRecargosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idRecargoEspecial,Boolean contratado) {
		return beanRemoto.listarRecargosRiesgoPorCoberturaCotizacion(id,
				idRecargoEspecial, contratado);
	}

	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idDescuentoEspecial,Boolean contratado) {
		return beanRemoto.listarDescuentosRiesgoPorCoberturaCotizacion(id,
				idDescuentoEspecial, contratado);
	}
	
	public List<AumentoRiesgoCotizacionDTO> listarAumentosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idRecargoEspecial,Boolean contratado) {
		return beanRemoto.listarAumentosRiesgoPorCoberturaCotizacion(id,idRecargoEspecial, contratado);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion) {
		return beanRemoto.listarCoberturasContratadas(idToCotizacion);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion, boolean aplicarMerge) {
		return beanRemoto.listarCoberturasContratadas(idToCotizacion,
				aplicarMerge);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCobertura(
			CoberturaCotizacionId id) {
		return beanRemoto.listarCoberturasContratadasPorSeccionCobertura(id);
	}

	public List<CoberturaCotizacionDTO> listarCoberturasCalculadasPrimerRiesgoLUC(
			BigDecimal idToCotizacion, Short numeroAgrupacion)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarCoberturasCalculadasPrimerRiesgoLUC(
					idToCotizacion, numeroAgrupacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public AgrupacionCotDTO obtenerAgrupacionCotizacion(AgrupacionCotId id) {
		try {
			return beanRemoto.getAgrupacionCotizacion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarPorIdFiltrado(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarPorIdFiltrado(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarFiltrado(
			CoberturaCotizacionDTO entity) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(entity);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Double getSumatoriaRiesgosBasicos(CoberturaCotizacionId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.getSumatoriaRiesgosBasicos(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Double obtenerSACoberturasBasicasIncendioPorCotizacion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto
					.obtenerSACoberturasBasicasIncendioPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<BigDecimal> listarCoberturasDistintas(
			BigDecimal idToCotizacion, boolean soloContratadas) {
		try {
			return beanRemoto.listarCoberturasDistintas(idToCotizacion,
					soloContratadas);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public CoberturaCotizacionDTO obtenerCoberturaContratadaPorId(
			CoberturaCotizacionId id) {
		try {
			return beanRemoto.obtenerCoberturaContratadaPorId(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasApliquenPrimerRiesgoLUC(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToCobertura) {
		try {
			return beanRemoto
					.listarCoberturasContratadasApliquenPrimerRiesgoLUC(
							idToCotizacion, numeroInciso, idToCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarCoberturasAsociadasPorSeccionCotId(
			SeccionCotizacionDTOId seccionID, BigDecimal idToCoberturaBasica) {
		try {
			return beanRemoto.listarCoberturasAsociadasPorSeccionCotizacionId(
					seccionID, idToCoberturaBasica);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Map<BigDecimal, BigDecimal> obtenerIdCoberturasPrimerRiesgo(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.obtenerCoberturasPrimerRiesgo(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionIdSinContratar(
			SeccionCotizacionDTOId id) {
		try {
			return beanRemoto.listarPorSeccionCotizacionIdSinContratar(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarCoberturasPrimerRiesgoSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion) {
		try {
			return beanRemoto.listarCoberturasPrimerRiesgoSeccion(
					idToCotizacion, idToSeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId) {
		try {
			return beanRemoto
					.listarCoberturasContratadasPorSeccionCotizacion(seccionCotizacionDTOId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Long obtenerTotalCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId) {
		try {
			return beanRemoto
					.obtenerTotalCoberturasContratadasPorSeccionCotizacion(seccionCotizacionDTOId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarCoberturasBasicasContratadas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		return beanRemoto.listarCoberturasBasicasContratadas(idToCotizacion,
				numeroInciso);
	}

	public List<SubRamoDTO> listarSubRamosEndoso(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarSubRamosEndoso(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasEnConjunto(
			SeccionCotizacionDTOId seccion, List<BigDecimal> idsCoberturas) {
		try {
			return beanRemoto.listarCoberturasContratadasEnConjunto(seccion,
					idsCoberturas);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}