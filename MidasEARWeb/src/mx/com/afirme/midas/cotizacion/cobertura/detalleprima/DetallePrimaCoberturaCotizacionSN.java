package mx.com.afirme.midas.cotizacion.cobertura.detalleprima;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetallePrimaCoberturaCotizacionSN {
	private DetallePrimaCoberturaCotizacionFacadeRemote beanRemoto;

	public DetallePrimaCoberturaCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(DetallePrimaCoberturaCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log(
				"bean Remoto DetallePrimaCoberturaCotizacion instanciado",
				Level.FINEST, null);
	}

	public void agregar(
			DetallePrimaCoberturaCotizacionDTO recargoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(recargoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(
			DetallePrimaCoberturaCotizacionDTO recargoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(recargoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DetallePrimaCoberturaCotizacionDTO modificar(
			DetallePrimaCoberturaCotizacionDTO recargoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(recargoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DetallePrimaCoberturaCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public DetallePrimaCoberturaCotizacionDTO getPorId(
			DetallePrimaCoberturaCotizacionId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * findByCoberturaCotizacion. Encuentra la lista de entidades
	 * DetallePrimaCoberturaCotizacionDTO que tengan los atributos recibidos en
	 * el objeto CoberturaCotizacionId. Los atributos usados para la b�squeda
	 * son los siguientes: idToCotizacion, numeroInciso, idToSeccion,
	 * idToCobertura.
	 * 
	 * @param CoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findByCoberturaCotizacion(
			CoberturaCotizacionId coberturaCotId) {
		try {
			return beanRemoto.findByCoberturaCotizacion(coberturaCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DetallePrimaCoberturaCotizacionDTO> buscarPorPropiedad(
			String propertyName, Object value) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	
	/**
	 * findBySubIncisoCotizacion. Encuentra la lista de entidades
	 * DetallePrimaCoberturaCotizacionDTO que tengan los atributos recibidos en
	 * el objeto DetallePrimaCoberturaCotizacionId. Los atributos usados para la b�squeda
	 * son los siguientes: idToCotizacion, numeroInciso, idToSeccion,
	 * numSubInciso.
	 * 
	 * @param DetallePrimaCoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findBySubIncisoCotizacion(
		DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId) {
		try {
			return beanRemoto.findBySubIncisoCotizacion(detallePrimaCoberturaCotizacionId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	

	public Double sumPrimaNetaARDTCoberturaCotizacion(CoberturaCotizacionId id) {
		try {
			return beanRemoto.sumPrimaNetaARDTCoberturaCotizacion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Double sumPrimaNetaCoberturaCotizacion(CoberturaCotizacionId id) {
		try {
			return beanRemoto.sumPrimaNetaCoberturaCotizacion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void eliminarDetallePrimaCoberturaCotizacion(CoberturaCotizacionId id) {
		try {
			beanRemoto.deleteDetallePrimaCoberturaCotizacion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void borrarFiltrado(DetallePrimaCoberturaCotizacionId id) {
		try {
			beanRemoto.borrarFiltrado(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DetallePrimaCoberturaCotizacionDTO> listarPorCotizacion(BigDecimal idToCotizacion){
		try {
			return beanRemoto.listarPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(this.getClass().toString(),e,Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
