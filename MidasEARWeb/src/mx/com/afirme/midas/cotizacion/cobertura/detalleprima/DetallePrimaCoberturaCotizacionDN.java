package mx.com.afirme.midas.cotizacion.cobertura.detalleprima;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetallePrimaCoberturaCotizacionDN {
	private static final DetallePrimaCoberturaCotizacionDN INSTANCIA = new DetallePrimaCoberturaCotizacionDN();

	public static DetallePrimaCoberturaCotizacionDN getInstancia (){
		return DetallePrimaCoberturaCotizacionDN.INSTANCIA;
	}
	
	public void agregar(DetallePrimaCoberturaCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DetallePrimaCoberturaCotizacionSN().agregar(recargoRiesgoCotizacionDTO);
	}
	
	public void borrar (DetallePrimaCoberturaCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DetallePrimaCoberturaCotizacionSN().borrar(recargoRiesgoCotizacionDTO);
	}
	
	public DetallePrimaCoberturaCotizacionDTO modificar (DetallePrimaCoberturaCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaCoberturaCotizacionSN().modificar(recargoRiesgoCotizacionDTO);
	}
	
	public DetallePrimaCoberturaCotizacionDTO getPorId(DetallePrimaCoberturaCotizacionId recargoRiesgoCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaCoberturaCotizacionSN().getPorId(recargoRiesgoCotizacionId);
	}
	
	public List<DetallePrimaCoberturaCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaCoberturaCotizacionSN().listarTodos();
	}
	
	/**
	 * 	findByCoberturaCotizacion. Encuentra la lista de entidades DetallePrimaCoberturaCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto CoberturaCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura.
	 * @param CoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findByCoberturaCotizacion(CoberturaCotizacionId coberturaCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaCoberturaCotizacionSN().findByCoberturaCotizacion(coberturaCotId);
	}
	
	/**
	 * 	findBySubIncisoCotizacion. Encuentra la lista de entidades DetallePrimaCoberturaCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto CoberturaCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, numeroSubInciso.
	 * @param DetallePrimaCoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findBySubIncisoCotizacion(DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaCoberturaCotizacionSN().findBySubIncisoCotizacion(detallePrimaCoberturaCotizacionId);
	}


	public Double sumPrimaNetaARDTCoberturaCotizacion(CoberturaCotizacionId id) throws SystemException {
		return new DetallePrimaCoberturaCotizacionSN().sumPrimaNetaARDTCoberturaCotizacion(id);
	}

	public Double sumPrimaNetaCoberturaCotizacion(CoberturaCotizacionId id) throws SystemException {
		return new DetallePrimaCoberturaCotizacionSN().sumPrimaNetaCoberturaCotizacion(id);
	}

	public void eliminarDetallePrimaCoberturaCotizacion(
			CoberturaCotizacionDTO coberturaCotizacionDTO) throws SystemException {
		new DetallePrimaCoberturaCotizacionSN().eliminarDetallePrimaCoberturaCotizacion(coberturaCotizacionDTO.getId());
		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
		id.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
		id.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
		id.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
		new DetallePrimaRiesgoCotizacionSN().eliminarDetallePrimaRiesgoCotizacion(id);
	}

	public void eliminarDetallePrimaSeccionCotizacion(
			SeccionCotizacionDTO seccionCotizacionDTO) throws SystemException {
		CoberturaCotizacionId idCobertura = new CoberturaCotizacionId();
		idCobertura.setIdToCotizacion(seccionCotizacionDTO.getId().getIdToCotizacion());
		idCobertura.setNumeroInciso(seccionCotizacionDTO.getId().getNumeroInciso());
		idCobertura.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
		new DetallePrimaCoberturaCotizacionSN().eliminarDetallePrimaCoberturaCotizacion(idCobertura);
		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(idCobertura.getIdToCotizacion());
		id.setNumeroInciso(idCobertura.getNumeroInciso());
		id.setIdToSeccion(idCobertura.getIdToSeccion());
		new DetallePrimaRiesgoCotizacionSN().eliminarDetallePrimaRiesgoCotizacion(id);
	}

	
	public void borrarFiltrado(DetallePrimaCoberturaCotizacionId id) throws SystemException{
	    new DetallePrimaCoberturaCotizacionSN().borrarFiltrado(id);
	}

	
	public List<DetallePrimaCoberturaCotizacionDTO> listarPorCotizacion(BigDecimal idToCotizacion) throws SystemException{
		return new DetallePrimaCoberturaCotizacionSN().listarPorCotizacion(idToCotizacion);
	}

}
