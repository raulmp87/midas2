package mx.com.afirme.midas.cotizacion.subinciso;

import mx.com.afirme.midas.sistema.MidasDynaBaseForm;

public class SubIncisoForm extends MidasDynaBaseForm {

	private static final long serialVersionUID = 1L;
	private String idToCotizacion;
	private String numeroInciso;
	private String numeroSubInciso;
	private String idToSeccion;
	private String[] datos;
	private String fechaCreacion;
	private String subInciso;
	private String sumaAsegurada;
	private String seccionNombreComercial;
	
	private String origen;
	private String tipoEndoso;
	

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getIdToSeccion() {
		return idToSeccion;
	}

	public void setDatos(String[] datos) {
		this.datos = datos;
	}

	public String[] getDatos() {
		return datos;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setSubInciso(String subInciso) {
		this.subInciso = subInciso;
	}

	public String getSubInciso() {
		return subInciso;
	}

	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public String getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSeccionNombreComercial(String seccionNombreComercial) {
		this.seccionNombreComercial = seccionNombreComercial;
	}

	public String getSeccionNombreComercial() {
		return seccionNombreComercial;
	}
}