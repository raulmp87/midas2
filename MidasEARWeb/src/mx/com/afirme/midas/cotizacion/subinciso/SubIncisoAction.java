package mx.com.afirme.midas.cotizacion.subinciso;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionUtil;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SubIncisoAction extends MidasMappingDispatchAction {

	/**
	 * Method mostrarDatosInciso
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public void mostrarDatosSubInciso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			String modificar = request.getParameter("modificar");
			Boolean soloLectura = request.getParameter("soloLectura").equals("1")? true : false;
			String disabled = soloLectura? "disabled" : "";
			
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));

			//List<ConfiguracionDatoIncisoCotizacionDTO> datosSubInciso = cotizacionDN.getDatosRiesgoSubInciso(idToCotizacion, numeroInciso, idToSeccion);
			// se iteran los objetos obtenidos
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<datossubinciso><![CDATA[");

			List<ConfiguracionDatoIncisoCotizacionDTO> configuracion = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			List<BigDecimal> riesgos = cotizacionDN.listarRiesgosPorSeccionCotizacion(idToCotizacion, numeroInciso, idToSeccion);
			int total = 0;
			
			Map<String,String> mapaIDsCascadeoHijos = new HashMap<String,String>();
			for(BigDecimal idToRiesgo : riesgos) {
				RiesgoDN riesgoDN = RiesgoDN.getInstancia();
				List<ConfiguracionDatoIncisoCotizacionDTO> datosSubInciso = cotizacionDN.getDatosRiesgoSubInciso(idToRiesgo);
				configuracion.addAll(datosSubInciso);
				if(!datosSubInciso.isEmpty()) {
					buffer.append("<div class=\"subtituloIzquierdaDiv\">Riesgo: " + riesgoDN.getPorId(idToRiesgo).getDescripcion() + "</div>");
				}
				buffer.append("<table id= \"desplegarDetalle\">");
				boolean trAbierto = false;
				for(int i = 0; i < datosSubInciso.size(); i++) {
					if(i % 2 == 0 && trAbierto == true) {
						buffer.append("</tr>");
						trAbierto = false;
					}
					if(i % 2 == 0 && trAbierto == false) {
						buffer.append("<tr>");
						trAbierto = true;
					}
					ConfiguracionDatoIncisoCotizacionDTO dato = datosSubInciso.get(i);

					String value = "";
					if(modificar.equals("1")) {
						BigDecimal numeroSubInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroSubInciso"));
						DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
						id.setIdToCotizacion(idToCotizacion);
						id.setNumeroInciso(numeroInciso);
						id.setIdDato(dato.getId().getIdDato());
						id.setClaveDetalle(dato.getId().getClaveDetalle());
						id.setIdTcRamo(dato.getId().getIdTcRamo());
						id.setIdTcSubramo(dato.getId().getIdTcSubramo());
						id.setIdToCobertura(BigDecimal.ZERO);
						id.setIdToRiesgo(dato.getId().getIdToRiesgo());
						id.setIdToSeccion(idToSeccion);
						id.setNumeroSubinciso(numeroSubInciso);
						
						DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
						DatoIncisoCotizacionDTO valor = datoIncisoCotizacionSN.getPorId(id);
						if(valor != null) {
							value = valor.getValor();
						}
					}
					
					buffer.append("<th class='normal' width='20%'>" + dato.getDescripcionEtiqueta() + ": </th>");
					Map atributos = null;
					String name = "datos[" + (total++) + "]";
					String id = "id" + total;
					String onblur = "";
					switch (dato.getClaveTipoValidacion()){
					case 1:	//cuota al millar, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 4)";
						break;
					case 2:	//porcentaje, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 2)";
						break;
					case 3:	//importe. validar 16 digitos con 2 decimales 
						onblur="validarDecimal(this.form."+id+", this.value, 16, 2)";
						break;
					case 4:	//factor, valodar 16 d�gitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 16, 4)";
						break;
					case 5:	//DSMGVDF, validar entero de 8 d�gitos
						onblur="validarDecimal(this.form."+id+", this.value, 8, 0)";
						break;
					}
					switch(dato.getClaveTipoControl()) {
					case 1:
						atributos = new HashMap();
//						atributos.put("class", "cajaTexto");
//						atributos.put("size", "");
//						atributos.put("id", name);
//						atributos.put("name", name);
//						buffer.append("<td>" + IncisoCotizacionUtil.obtenerSelectCatalogo(atributos, dato.getDescripcionClaseRemota(), soloLectura, value) + "</td>");
						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectCatalogo(name,dato.getDescripcionClaseRemota(), soloLectura, value,mapaIDsCascadeoHijos) + "</td>");
						break;
					case 2:
						atributos = new HashMap();
						atributos.put("class", "cajaTexto");
						atributos.put("size", "");
						atributos.put("id", name);
						atributos.put("name",name);
						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectValorFijo(atributos, dato.getIdGrupo(), soloLectura, value) + "</td>");
						break;
					case 3:
						buffer.append("<td width='25%'><input type=\"text\" class=\"cajaTexto\" style=\"text-transform: none;\" value='" + value + "' name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'/></td>");
						break;
					case 4:
						buffer.append("<td width='25%'><textarea class=\"cajaTexto\" style=\"text-transform: none;\" name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'>" + value + "</textarea></td>");
						break;
					}
				}
				if(trAbierto == true) {
					buffer.append("</tr>");
				}
				buffer.append("</table>");
			}
			buffer.append("]]></datossubinciso>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());

			HttpSession session = request.getSession();
			session.removeAttribute("configuracion");
			session.setAttribute("configuracion", configuracion);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method listarSubIncisosPorSeccion
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarSubIncisosPorSeccion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		String idSeccionCot = request.getParameter("idSeccionCot");
		BigDecimal idToCotizacion;
	    BigDecimal numeroInciso;
	    BigDecimal idToSeccion;
	    SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		subIncisoForm.setTipoEndoso(request.getParameter("claveTipoEndoso"));
		subIncisoForm.setOrigen(request.getParameter("origen"));
	    
	    List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = new ArrayList<SubIncisoCotizacionDTO>();
		if (!UtileriasWeb.esCadenaVacia(idSeccionCot)){
			String[] ids = idSeccionCot.split(",");
			idToCotizacion = new BigDecimal(ids[0]);
		    numeroInciso = new BigDecimal(ids[1]);
		    idToSeccion = new BigDecimal(ids[2]);
		    
		    subIncisoForm.setIdToSeccion(idToSeccion.toBigInteger().toString());
		    subIncisoForm.setNumeroInciso(numeroInciso.toBigInteger().toString());
		    
		    CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(idToCotizacion,numeroInciso,idToSeccion,null);
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
			subIncisoCotizacionDTO.setId(subIncisoCotizacionDTOId);
			//IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			try {
				subIncisoCotizacionDTOLista = cotizacionDN.listarFiltrado(subIncisoCotizacionDTO);
				/*List<SubIncisoDTO> subIncisoDTOList = new ArrayList<SubIncisoDTO>();
				for (SubIncisoCotizacionDTO subIncisoCotizacion : subIncisoCotizacionDTOLista) {
					SubIncisoDTO subIncisoDTO = incisoCotizacionDN.getSubIncisoPorId(subIncisoCotizacion.getId().getNumeroSubInciso());
					if (subIncisoDTO != null){
						String id = subIncisoCotizacion.getId().getIdToCotizacion() + "," + subIncisoCotizacion.getId().getNumeroInciso() + "," + subIncisoCotizacion.getId().getIdToSeccion() + "," + subIncisoCotizacion.getId().getNumeroSubInciso();
						subIncisoDTO.setNoSerie(id); //habilitado temporal para pasar el ID al decorador
						subIncisoDTOList.add(subIncisoDTO);
					}
				}*/
				request.setAttribute("subIncisoDTOList", subIncisoCotizacionDTOLista);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		
		request.setAttribute("subIncisoCotizacionDTOLista", subIncisoCotizacionDTOLista);
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarAgregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		subIncisoForm.setTipoEndoso(request.getParameter("claveTipoEndoso"));
		subIncisoForm.setOrigen(request.getParameter("origen"));
		BigDecimal idToCotizacion = new BigDecimal(subIncisoForm.get("idToCotizacion").toString());
		BigDecimal numeroInciso = new BigDecimal(subIncisoForm.get("numeroInciso").toString());
		BigDecimal idToSeccion = new BigDecimal(subIncisoForm.get("idToSeccion").toString());
		try {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

			subIncisoForm.set("fechaCreacion", simpleDateFormat.format(cotizacionDTO.getFechaCreacion()));
			subIncisoForm.set("idToCotizacion", idToCotizacion.toBigInteger().toString());
			subIncisoForm.set("idToSeccion", idToSeccion.toBigInteger().toString());
			subIncisoForm.set("numeroInciso", numeroInciso.toBigInteger().toString());
			
			SeccionDN seccionDN = SeccionDN.getInstancia();
			SeccionDTO seccionDTO = new SeccionDTO();
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			subIncisoForm.set("seccionNombreComercial", seccionDTO.getNombreComercial());

			request.setAttribute("idToCotizacion", idToCotizacion.intValue());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method agregar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		ActionForward forward = null;
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;

		BigDecimal idToCotizacion = new BigDecimal(subIncisoForm.get("idToCotizacion").toString());
		BigDecimal numeroInciso = new BigDecimal(subIncisoForm.get("numeroInciso").toString());
		BigDecimal idToSeccion = new BigDecimal(subIncisoForm.get("idToSeccion").toString());
		try {
			SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
			BigDecimal numeroSubInciso = subIncisoCotizacionDN.obtenerNumeroSubIncisoSiguiente(idToCotizacion, numeroInciso, idToSeccion);

			SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
			subIncisoCotizacionDTO.setClaveAutReaseguro((short)0);
			subIncisoCotizacionDTO.setDescripcionSubInciso(subIncisoForm.get("subInciso").toString());
			subIncisoCotizacionDTO.setValorSumaAsegurada(Double.parseDouble(subIncisoForm.get("sumaAsegurada").toString()));
			SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setNumeroSubInciso(numeroSubInciso);
			subIncisoCotizacionDTO.setId(id);

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			subIncisoCotizacionDTO = cotizacionDN.agregarSubIncisoCotizacionDTO(subIncisoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));

			HttpSession session = request.getSession();
			List<ConfiguracionDatoIncisoCotizacionDTO> datosSubInciso = (List<ConfiguracionDatoIncisoCotizacionDTO>) session.getAttribute("configuracion");
			String[] datos = (String[])subIncisoForm.get("datos");
			for(int i = 0; i < datos.length; i++) {
				if(datos[i] != null) {
					ConfiguracionDatoIncisoCotizacionDTO configuracion = datosSubInciso.get(i);
					
					DatoIncisoCotizacionId idSubInciso = new DatoIncisoCotizacionId();
					idSubInciso.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
					idSubInciso.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
					idSubInciso.setIdDato(configuracion.getId().getIdDato());
					idSubInciso.setClaveDetalle(configuracion.getId().getClaveDetalle());
					idSubInciso.setIdTcRamo(configuracion.getId().getIdTcRamo());
					idSubInciso.setIdTcSubramo(configuracion.getId().getIdTcSubramo());
					idSubInciso.setIdToCobertura(BigDecimal.ZERO);
					idSubInciso.setIdToRiesgo(configuracion.getId().getIdToRiesgo());
					idSubInciso.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
					idSubInciso.setNumeroSubinciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());

					DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
					datoIncisoCotizacionDTO.setId(idSubInciso);
					datoIncisoCotizacionDTO.setValor(datos[i]);

					DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
					datoIncisoCotizacionSN.agregar(datoIncisoCotizacionDTO);
				}
			}
			CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
			calculoCotizacionDN.calcularSubInciso(subIncisoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));

			forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
			String path = forward.getPath();
			forward.setPath(path+ "?idToCotizacion="+ subIncisoCotizacionDTO.getId().getIdToCotizacion().toBigInteger().toString());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		if (forward == null) {
			return mapping.findForward(reglaNavegacion);
		} else {
			return forward;
		}
	}

	/**
	 * Method mostrarDetalle
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("idSubInciso");
		String ids[]= id.split(",");
		BigDecimal idToCotizacion = new BigDecimal(ids[0]);
		BigDecimal numeroInciso = new BigDecimal(ids[1]);
		BigDecimal idToSeccion = new BigDecimal(ids[2]);
		BigDecimal numeroSubInciso = new BigDecimal(ids[3]);
		
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(idToCotizacion,numeroInciso,idToSeccion,numeroSubInciso);
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		subIncisoForm.setTipoEndoso(request.getParameter("claveTipoEndoso"));
		subIncisoForm.setOrigen(request.getParameter("origen"));
		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
		try {
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = incisoCotizacionDN.getSubIncisoCotizacionPorId(subIncisoCotizacionDTOId);
			//SubIncisoDTO subIncisoDTO = incisoCotizacionDN.getSubIncisoPorId(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			//this.poblarForm(subIncisoCotizacionDTO, subIncisoDTO, subIncisoForm);
			
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

			subIncisoForm.set("fechaCreacion", simpleDateFormat.format(cotizacionDTO.getFechaCreacion()));
			subIncisoForm.set("idToCotizacion", idToCotizacion.toBigInteger().toString());
			subIncisoForm.set("idToSeccion", idToSeccion.toBigInteger().toString());
			subIncisoForm.set("numeroInciso", numeroInciso.toBigInteger().toString());
			subIncisoForm.set("numeroSubInciso", subIncisoCotizacionDTO.getId().getNumeroSubInciso().toString());
			subIncisoForm.set("subInciso", subIncisoCotizacionDTO.getDescripcionSubInciso());
			subIncisoForm.set("sumaAsegurada", subIncisoCotizacionDTO.getValorSumaAsegurada().toString());
			
			SeccionDN seccionDN = SeccionDN.getInstancia();
			SeccionDTO seccionDTO = new SeccionDTO();
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			subIncisoForm.set("seccionNombreComercial", seccionDTO.getNombreComercial());

			request.setAttribute("idToCotizacion", idToCotizacion.intValue());
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);		
	}
	
	/**
	 * Method mostrarModificar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("idSubInciso");
		String ids[]= id.split(",");
		BigDecimal idToCotizacion = new BigDecimal(ids[0]);
		BigDecimal numeroInciso = new BigDecimal(ids[1]);
		BigDecimal idToSeccion = new BigDecimal(ids[2]);
		BigDecimal numeroSubInciso = new BigDecimal(ids[3]);
		
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(idToCotizacion,numeroInciso,idToSeccion,numeroSubInciso);
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		subIncisoForm.setTipoEndoso(request.getParameter("claveTipoEndoso"));
		subIncisoForm.setOrigen(request.getParameter("origen"));
		
		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
		try {
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = incisoCotizacionDN.getSubIncisoCotizacionPorId(subIncisoCotizacionDTOId);
			//SubIncisoDTO subIncisoDTO = incisoCotizacionDN.getSubIncisoPorId(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			//this.poblarForm(subIncisoCotizacionDTO, subIncisoDTO, subIncisoForm);

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

			subIncisoForm.set("fechaCreacion", simpleDateFormat.format(cotizacionDTO.getFechaCreacion()));
			subIncisoForm.set("idToCotizacion", idToCotizacion.toBigInteger().toString());
			subIncisoForm.set("idToSeccion", idToSeccion.toBigInteger().toString());
			subIncisoForm.set("numeroInciso", numeroInciso.toBigInteger().toString());
			subIncisoForm.set("numeroSubInciso", subIncisoCotizacionDTO.getId().getNumeroSubInciso().toString());
			subIncisoForm.set("subInciso", subIncisoCotizacionDTO.getDescripcionSubInciso());
			subIncisoForm.set("sumaAsegurada", subIncisoCotizacionDTO.getValorSumaAsegurada().toString());
			
			SeccionDN seccionDN = SeccionDN.getInstancia();
			SeccionDTO seccionDTO = new SeccionDTO();
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			subIncisoForm.set("seccionNombreComercial", seccionDTO.getNombreComercial());

			request.setAttribute("idToCotizacion", idToCotizacion.intValue());
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);	
	}
	
	/**
	 * Method mostrarBorrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarBorrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("idSubInciso");
		String ids[]= id.split(",");
		BigDecimal idToCotizacion = new BigDecimal(ids[0]);
		BigDecimal numeroInciso = new BigDecimal(ids[1]);
		BigDecimal idToSeccion = new BigDecimal(ids[2]);
		BigDecimal numeroSubInciso = new BigDecimal(ids[3]);
		
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(idToCotizacion,numeroInciso,idToSeccion,numeroSubInciso);
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		subIncisoForm.setTipoEndoso(request.getParameter("claveTipoEndoso"));
		subIncisoForm.setOrigen(request.getParameter("origen"));
		
		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
		try {
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = incisoCotizacionDN.getSubIncisoCotizacionPorId(subIncisoCotizacionDTOId);
			//SubIncisoDTO subIncisoDTO = incisoCotizacionDN.getSubIncisoPorId(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
			//this.poblarForm(subIncisoCotizacionDTO, subIncisoDTO, subIncisoForm);
			
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

			subIncisoForm.set("fechaCreacion", simpleDateFormat.format(cotizacionDTO.getFechaCreacion()));
			subIncisoForm.set("idToCotizacion", idToCotizacion.toBigInteger().toString());
			subIncisoForm.set("idToSeccion", idToSeccion.toBigInteger().toString());
			subIncisoForm.set("numeroInciso", numeroInciso.toBigInteger().toString());
			subIncisoForm.set("numeroSubInciso", subIncisoCotizacionDTO.getId().getNumeroSubInciso().toString());
			subIncisoForm.set("subInciso", subIncisoCotizacionDTO.getDescripcionSubInciso());
			subIncisoForm.set("sumaAsegurada", subIncisoCotizacionDTO.getValorSumaAsegurada().toString());
			
			SeccionDN seccionDN = SeccionDN.getInstancia();
			SeccionDTO seccionDTO = new SeccionDTO();
			seccionDTO.setIdToSeccion(idToSeccion);
			seccionDTO = seccionDN.getPorId(seccionDTO);
			subIncisoForm.set("seccionNombreComercial", seccionDTO.getNombreComercial());

			request.setAttribute("idToCotizacion", idToCotizacion.intValue());
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);		
	}
	
	/**
	 * Method borrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		ActionForward forward = null;
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;

		BigDecimal idToCotizacion = new BigDecimal(subIncisoForm.get("idToCotizacion").toString());
		BigDecimal numeroInciso = new BigDecimal(subIncisoForm.get("numeroInciso").toString());
		BigDecimal idToSeccion = new BigDecimal(subIncisoForm.get("idToSeccion").toString());
		BigDecimal numeroSubInciso = new BigDecimal(subIncisoForm.get("numeroSubInciso").toString());
		//this.poblarDTOs(subIncisoForm,subIncisoCotizacionDTO,subIncisoDTO);
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
			SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setNumeroSubInciso(numeroSubInciso);
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = subIncisoCotizacionDN.getPorId(id);
			subIncisoCotizacionDN.borrar(subIncisoCotizacionDTO,UtileriasWeb.obtieneNombreUsuario(request));
			//SE AGREGA ELIMINACION DE DETALLESPRIMAS COBERTURAS Y DETALLESPRIMASRIESGO
			cotizacionDN.eliminaDetallesPrimasDeCoberturayRiesgoPorSubInciso(subIncisoCotizacionDTO);

			HttpSession session = request.getSession();
			session.removeAttribute("configuracion");

			forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
			String path = forward.getPath();
			forward.setPath(path+ "?idToCotizacion="+ subIncisoCotizacionDTO.getId().getIdToCotizacion().toBigInteger().toString());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		if (forward == null) {
			return mapping.findForward(reglaNavegacion);
		} else {
			return forward;
		}
	}
	
	/**
	 * Method modificar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){

		String reglaNavegacion = Sistema.EXITOSO;
		ActionForward forward = null;
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;

		BigDecimal idToCotizacion = new BigDecimal(subIncisoForm.get("idToCotizacion").toString());
		BigDecimal numeroInciso = new BigDecimal(subIncisoForm.get("numeroInciso").toString());
		BigDecimal idToSeccion = new BigDecimal(subIncisoForm.get("idToSeccion").toString());
		BigDecimal numeroSubInciso = new BigDecimal(subIncisoForm.get("numeroSubInciso").toString());
		try {
			SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
			//BigDecimal numeroSubInciso = subIncisoCotizacionDN.obtenerNumeroSubIncisoSiguiente(idToCotizacion, numeroInciso, idToSeccion);

			/*SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
			subIncisoCotizacionDTO.setClaveAutReaseguro((short)0);
			*/
			SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setNumeroSubInciso(numeroSubInciso);
			//subIncisoCotizacionDTO.setId(id);
			
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = subIncisoCotizacionDN.getPorId(id);
			subIncisoCotizacionDTO.setDescripcionSubInciso(subIncisoForm.get("subInciso").toString());
			subIncisoCotizacionDTO.setValorSumaAsegurada(Double.parseDouble(subIncisoForm.get("sumaAsegurada").toString()));

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			subIncisoCotizacionDTO = cotizacionDN.modificarSubIncisoCotizacionDTO(subIncisoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));

			HttpSession session = request.getSession();
			List<ConfiguracionDatoIncisoCotizacionDTO> datosSubInciso = (List<ConfiguracionDatoIncisoCotizacionDTO>) session.getAttribute("configuracion");
			String[] datos = (String[])subIncisoForm.get("datos");
			for(int i = 0; i < datos.length; i++) {
				if(datos[i] != null) {
					ConfiguracionDatoIncisoCotizacionDTO configuracion = datosSubInciso.get(i);
					
					DatoIncisoCotizacionId idSubInciso = new DatoIncisoCotizacionId();
					idSubInciso.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
					idSubInciso.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
					idSubInciso.setIdDato(configuracion.getId().getIdDato());
					idSubInciso.setClaveDetalle(configuracion.getId().getClaveDetalle());
					idSubInciso.setIdTcRamo(configuracion.getId().getIdTcRamo());
					idSubInciso.setIdTcSubramo(configuracion.getId().getIdTcSubramo());
					idSubInciso.setIdToCobertura(BigDecimal.ZERO);
					idSubInciso.setIdToRiesgo(configuracion.getId().getIdToRiesgo());
					idSubInciso.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
					idSubInciso.setNumeroSubinciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());

					DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
					datoIncisoCotizacionDTO.setId(idSubInciso);
					datoIncisoCotizacionDTO.setValor(datos[i]);

					DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
					datoIncisoCotizacionSN.modificar(datoIncisoCotizacionDTO);
				}
			}

			forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
			String path = forward.getPath();
			forward.setPath(path+ "?idToCotizacion="+ subIncisoCotizacionDTO.getId().getIdToCotizacion().toBigInteger().toString());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		if (forward == null) {
			return mapping.findForward(reglaNavegacion);
		} else {
			return forward;
		}
	}
	
	/*public void poblarForm(SubIncisoCotizacionDTO subIncisoCotizacionDTO, SubIncisoDTO subIncisoDTO, SubIncisoForm subIncisoForm) throws SystemException{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		//Falta definir de donde obtener los datos
		if (subIncisoDTO.getAnioFabricacion() != null)
			subIncisoForm.setAnioDeFabricacion(subIncisoDTO.getAnioFabricacion().toString());
		if (subIncisoCotizacionDTO.getId().getIdToCotizacion() != null)
			subIncisoForm.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion().toString());
		if (subIncisoCotizacionDTO.getId().getIdToSeccion() != null)
			subIncisoForm.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion().toString());
		if (subIncisoCotizacionDTO.getId().getNumeroInciso() != null)
		subIncisoForm.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso().toBigInteger().toString());
		
		subIncisoForm.setMarca(subIncisoDTO.getMarca());
		subIncisoForm.setModelo(subIncisoDTO.getModelo());
		subIncisoForm.setNumeroDeSerie(subIncisoDTO.getNoSerie());
		if (subIncisoDTO.getSumaAsegurada() != null)
			subIncisoForm.setSumaAsegurada(subIncisoDTO.getSumaAsegurada().toString());
		if (subIncisoDTO.getTipoEquipo() != null)
			subIncisoForm.setTipoDeEquipo(subIncisoDTO.getTipoEquipo().toString());
		subIncisoForm.setSubInciso(subIncisoDTO.getDescripcionSubInciso());
		if (subIncisoCotizacionDTO.getId().getNumeroSubInciso() != null)
			subIncisoForm.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso().toBigInteger().toString());
		
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia();
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
		cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
		if (cotizacionDTO.getFechaCreacion() != null)
			subIncisoForm.setFechaCreacion(dateFormat.format(cotizacionDTO.getFechaCreacion()));
		
		SeccionDN seccionDN = SeccionDN.getInstancia();
		SeccionDTO seccionDTO = new SeccionDTO();
		seccionDTO.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
		seccionDTO = seccionDN.getPorId(seccionDTO);
		subIncisoForm.setSeccionNombreComercial(seccionDTO.getNombreComercial());
	}
	
	public void poblarDTOs(SubIncisoForm subIncisoForm, SubIncisoCotizacionDTO subIncisoCotizacionDTO, SubIncisoDTO subIncisoDTO){
		
		// Filling SubIncisoCotizacionDTO
		subIncisoCotizacionDTO.setClaveAutReaseguro(new Short("0")); // Dato Pendiente de corroborar de d&oacute;nde debe ser importado/definido
		subIncisoCotizacionDTO.setCodigoUsuarioAutReaseguro(null); // Dato Pendiente de corroborar de d&oacute;nde debe ser importado/definido
		if (!UtileriasWeb.esCadenaVacia(subIncisoForm.getSumaAsegurada()) && !UtileriasWeb.esObjetoNulo(subIncisoForm.getSumaAsegurada()))
			subIncisoCotizacionDTO.setValorSumaAsegurada(new Double(subIncisoForm.getSumaAsegurada())); // Dato Pendiente de corroborar de d&oacute;nde debe ser importado/definido
		else
			subIncisoCotizacionDTO.setValorSumaAsegurada(new Double("0"));
		
		SeccionCotizacionDTOId seccionCotizacionDTOId = new SeccionCotizacionDTOId();
		seccionCotizacionDTOId.setIdToCotizacion(new BigDecimal(subIncisoForm.getIdToCotizacion()));
		seccionCotizacionDTOId.setIdToSeccion(new BigDecimal(subIncisoForm.getIdToSeccion()));
		seccionCotizacionDTOId.setNumeroInciso(new BigDecimal(subIncisoForm.getNumeroInciso()));
		SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
		seccionCotizacionDTO.setId(seccionCotizacionDTOId);
		
		subIncisoCotizacionDTO.setSeccionCotizacionDTO(seccionCotizacionDTO);
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId();
		
		if (!UtileriasWeb.esCadenaVacia(subIncisoForm.getIdToCotizacion()))
			subIncisoCotizacionDTOId.setIdToCotizacion(new BigDecimal(subIncisoForm.getIdToCotizacion()));
		else
			subIncisoCotizacionDTOId.setIdToCotizacion(null);
		
		if (!UtileriasWeb.esCadenaVacia(subIncisoForm.getNumeroInciso()))
			subIncisoCotizacionDTOId.setNumeroInciso(new BigDecimal(subIncisoForm.getNumeroInciso()));
		else
			subIncisoCotizacionDTOId.setNumeroInciso(null);
		
		if (!UtileriasWeb.esCadenaVacia(subIncisoForm.getIdToSeccion()))
			subIncisoCotizacionDTOId.setIdToSeccion(new BigDecimal(subIncisoForm.getIdToSeccion()));
		else
			subIncisoCotizacionDTOId.setIdToSeccion(null);
		
		if (!UtileriasWeb.esCadenaVacia(subIncisoForm.getNumeroSubInciso()))
			subIncisoCotizacionDTOId.setNumeroSubInciso(new BigDecimal(subIncisoForm.getNumeroSubInciso()));
		else
			subIncisoCotizacionDTOId.setNumeroSubInciso(null);
		
		subIncisoCotizacionDTO.setId(subIncisoCotizacionDTOId);
		
		// Filling SubIncisoDTO
		
		if (subIncisoForm.getAnioDeFabricacion() != null && subIncisoForm.getAnioDeFabricacion().length() > 0)
			subIncisoDTO.setAnioFabricacion(new Short(subIncisoForm.getAnioDeFabricacion()));
		
		subIncisoDTO.setDescripcionSubInciso(subIncisoForm.getSubInciso());
		subIncisoDTO.setMarca(subIncisoForm.getMarca());
		subIncisoDTO.setModelo(subIncisoForm.getModelo());
		subIncisoDTO.setNoSerie(subIncisoForm.getNumeroDeSerie());
		subIncisoDTO.setDescripcionSubInciso(subIncisoForm.getSubInciso());
		
		if (subIncisoForm.getSumaAsegurada()!= null && subIncisoForm.getSumaAsegurada().length() > 0)
			subIncisoDTO.setSumaAsegurada(new Double(subIncisoForm.getSumaAsegurada()));
		
		if (subIncisoForm.getTipoDeEquipo() != null && subIncisoForm.getTipoDeEquipo().length() > 0)
			subIncisoDTO.setTipoEquipo(new Short(subIncisoForm.getTipoDeEquipo()));
		
		subIncisoCotizacionDTO.setDescripcionSubInciso(subIncisoForm.getSubInciso());
		
	}*/
	
}