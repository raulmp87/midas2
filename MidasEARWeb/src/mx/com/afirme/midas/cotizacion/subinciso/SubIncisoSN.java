package mx.com.afirme.midas.cotizacion.subinciso;

import java.math.BigDecimal;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class SubIncisoSN {
	private SubIncisoFacadeRemote beanRemoto;
	
	public SubIncisoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubIncisoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public SubIncisoDTO agregar(SubIncisoDTO entity){
		 return beanRemoto.save(entity);
	}
	
	public SubIncisoDTO getPorId(BigDecimal id){
		return beanRemoto.findById(id);
	}
	
	public void borrar(SubIncisoDTO entity){
		beanRemoto.delete(entity);
	}
	
	public SubIncisoDTO modificar(SubIncisoDTO entity){
		return beanRemoto.update(entity);
	}
	
}
