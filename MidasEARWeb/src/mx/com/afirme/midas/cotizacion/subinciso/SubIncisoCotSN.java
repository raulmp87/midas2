package mx.com.afirme.midas.cotizacion.subinciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubIncisoCotSN {
	private SubIncisoCotizacionFacadeRemote beanRemoto;

	public SubIncisoCotSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(SubIncisoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public SubIncisoCotizacionDTO agregar(SubIncisoCotizacionDTO entity) {
		return beanRemoto.save(entity);
	}

	public SubIncisoCotizacionDTO modificar(SubIncisoCotizacionDTO entity) {
		return beanRemoto.update(entity);
	}

	public void borrar(SubIncisoCotizacionDTO entity) {
		beanRemoto.delete(entity);
	}

	public List<SubIncisoCotizacionDTO> listarFiltrado(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {

		return beanRemoto.listarFiltrado(subIncisoCotizacionDTO);
	}
	
	public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO subIncisoCotizacionDTO,boolean aplicarMerge)throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(subIncisoCotizacionDTO,aplicarMerge);
	}

	public List<SubIncisoCotizacionDTO> buscarPorPropiedad(String propertyName,
			Object value) throws ExcepcionDeAccesoADatos {

		return beanRemoto.findByProperty(propertyName, value);
	}

	public SubIncisoCotizacionDTO getPorId(
			SubIncisoCotizacionDTOId subIncisoCotizacionDTOId)
			throws ExcepcionDeAccesoADatos {

		return beanRemoto.findById(subIncisoCotizacionDTOId);
	}

	public BigDecimal obtenerNumeroIncisoSiguiente(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion) {
		try {
			return beanRemoto.maxSubIncisos(idToCotizacion, numeroInciso,
					idToSeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public BigDecimal primaNetaSubInciso(SubIncisoCotizacionDTOId id) {
		try {
			return beanRemoto.primaNetaSubInciso(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrarSubincisosSeccionCotizacion(SeccionCotizacionDTOId id) {
		try {
			beanRemoto.borrarSubincisosSeccionCotizacion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SubIncisoCotizacionDTO> listarSubIncisosPorCotizacion(BigDecimal idToCotizacion){
		try {
			return beanRemoto.listarSubIncisosPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(this.getClass().toString(),e,Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
