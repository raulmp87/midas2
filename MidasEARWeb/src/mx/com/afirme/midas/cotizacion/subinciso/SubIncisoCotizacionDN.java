package mx.com.afirme.midas.cotizacion.subinciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubIncisoCotizacionDN {
	private static final SubIncisoCotizacionDN INSTANCIA = new SubIncisoCotizacionDN();

	public static SubIncisoCotizacionDN getInstancia (){
		return SubIncisoCotizacionDN.INSTANCIA;
	}
	
	public SubIncisoCotizacionDTO agregar(SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		subIncisoCotizacionDTO = new SubIncisoCotSN().agregar(subIncisoCotizacionDTO);
		return subIncisoCotizacionDTO;
	}
	
	public void borrar (SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		new SubIncisoCotSN().borrar(subIncisoCotizacionDTO);
		List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = new ArrayList<SubIncisoCotizacionDTO>();
		SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId(
				subIncisoCotizacionDTO.getId().getIdToCotizacion(),
				subIncisoCotizacionDTO.getId().getNumeroInciso(),
				subIncisoCotizacionDTO.getId().getIdToSeccion(), null);
		SubIncisoCotizacionDTO subIncisoDTO= new SubIncisoCotizacionDTO();
		subIncisoDTO.setId(id);
		try{
			subIncisoCotizacionDTOLista = CotizacionDN.getInstancia(nombreUsuario).listarFiltrado(subIncisoDTO);
			for(SubIncisoCotizacionDTO subIncisoTmp: subIncisoCotizacionDTOLista){
				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
				calculoCotizacionDN.calcularSubInciso(subIncisoTmp, nombreUsuario);				
			}
		} catch (SystemException e) {
			throw e;
		}
	}
	
	
	public List<SubIncisoCotizacionDTO> borrarSubIncisoCotizacionCE (SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		new SubIncisoCotSN().borrar(subIncisoCotizacionDTO);
		List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = new ArrayList<SubIncisoCotizacionDTO>();
		SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId(
				subIncisoCotizacionDTO.getId().getIdToCotizacion(),
				subIncisoCotizacionDTO.getId().getNumeroInciso(),
				subIncisoCotizacionDTO.getId().getIdToSeccion(), null);
		SubIncisoCotizacionDTO subIncisoDTO= new SubIncisoCotizacionDTO();
		subIncisoDTO.setId(id);
		
		subIncisoCotizacionDTOLista = CotizacionDN.getInstancia(nombreUsuario).listarFiltrado(subIncisoDTO);
		return subIncisoCotizacionDTOLista;
		
	}
	
	
	public SubIncisoCotizacionDTO modificar (SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario) 
			throws ExcepcionDeAccesoADatos, SystemException{
		subIncisoCotizacionDTO = new SubIncisoCotSN().modificar(subIncisoCotizacionDTO);
		CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
		calculoCotizacionDN.calcularSubInciso(subIncisoCotizacionDTO, nombreUsuario);
		return subIncisoCotizacionDTO;
	}
	
	public SubIncisoCotizacionDTO modificarSinCalcular (SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario) 
		throws ExcepcionDeAccesoADatos, SystemException{
		subIncisoCotizacionDTO = new SubIncisoCotSN().modificar(subIncisoCotizacionDTO);
		return subIncisoCotizacionDTO;
	}
	
	public SubIncisoCotizacionDTO getPorId(SubIncisoCotizacionDTOId id) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubIncisoCotSN().getPorId(id);
	}
	
	public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO SubIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubIncisoCotSN().listarFiltrado(SubIncisoCotizacionDTO);
	}

	public BigDecimal obtenerNumeroSubIncisoSiguiente(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion) throws SystemException {
		SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN();
		return subIncisoCotSN.obtenerNumeroIncisoSiguiente(idToCotizacion, numeroInciso, idToSeccion).add(BigDecimal.ONE);
	}
	
	public List<SubIncisoCotizacionDTO> listarSubIncisosPorCotizacion(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubIncisoCotSN().listarSubIncisosPorCotizacion(idToCotizacion);
	}
	
	public void borrarSubincisosSeccionCotizacion(SeccionCotizacionDTO seccionCotizacionDTO) throws SystemException {
		new SubIncisoCotSN().borrarSubincisosSeccionCotizacion(seccionCotizacionDTO.getId());
	}
	
	
}
