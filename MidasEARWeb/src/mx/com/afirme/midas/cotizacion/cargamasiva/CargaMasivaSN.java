package mx.com.afirme.midas.cotizacion.cargamasiva;

import java.util.logging.Level;


import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

public class CargaMasivaSN {
	private CargaMasivaService beanRemoto;

	public CargaMasivaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			setBeanRemoto(serviceLocator.getEJB(CargaMasivaService.class));
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void setBeanRemoto(CargaMasivaService beanRemoto) {
		this.beanRemoto = beanRemoto;
	}

	public CargaMasivaService getBeanRemoto() {
		return beanRemoto;
	}


}
