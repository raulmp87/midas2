package mx.com.afirme.midas.cotizacion.cargamasiva;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;


public class CargaMasivaDN {
	private static final CargaMasivaDN INSTANCIA = new CargaMasivaDN();

	public static CargaMasivaDN getInstancia() {
		return CargaMasivaDN.INSTANCIA;
	}
	
	public CargaMasivaService getCargaMasivaService() throws SystemException{
		return new CargaMasivaSN().getBeanRemoto();
	}
}