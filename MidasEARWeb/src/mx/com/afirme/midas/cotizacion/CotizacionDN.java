package mx.com.afirme.midas.cotizacion;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDN;
import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDTO;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionDN;
import mx.com.afirme.midas.consultas.gastoexpedicion.GastoExpedicionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO.ArchivosPendientes;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionSN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionSN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDN;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionSN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.facultativo.CotizacionSoporteDN;
import mx.com.afirme.midas.cotizacion.igualacion.IgualacionPrimaNetaForm;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDN;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDTO;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionId;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCSN;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotSN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.ImpresionAction;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoSN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoDN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaId;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDN;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.vault.VaultAction;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudSN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudSN;


public class CotizacionDN extends CotizacionSoporteDN {
	private static final CotizacionDN INSTANCIA = new CotizacionDN();
	private static String nombreUsuario;

	public static CotizacionDN getInstancia(String nombreUsuario) {
		CotizacionDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	/**
	 * 
	 * @param numeroInciso
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<SeccionCotizacionDTO> getSeccionesPorInciso(int numeroInciso)
	throws ExcepcionDeAccesoADatos, SystemException {
		SeccionCotSN seccionCotSN = new SeccionCotSN();

		return seccionCotSN.buscarPorNumeroInciso(numeroInciso);

	}
	/**
	 * 
	 * @param subIncisoCotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO subIncisoCotizacionDTO)
	throws ExcepcionDeAccesoADatos, SystemException {
		SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN();

		return subIncisoCotSN.listarFiltrado(subIncisoCotizacionDTO);
	}

	public SeccionCotizacionDTO getSeccionCotizacionPorId(SeccionCotizacionDTOId seccionCotizacionDTOId)
	throws SystemException {
		SeccionCotSN seccionCotSN = new SeccionCotSN();
		return seccionCotSN.buscarPorId(seccionCotizacionDTOId);
	}

	public SeccionCotizacionDTO actualizarSeccionCotizacion(
			SeccionCotizacionDTO seccionCotizacionDTO) throws SystemException {
		SeccionCotSN seccionCotSN = new SeccionCotSN();

		return seccionCotSN.actualizarSeccionCotizacion(seccionCotizacionDTO);
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public CotizacionDTO getPorId(CotizacionDTO cotizacionDTO)
	throws SystemException {
		CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);

		return cotizacionSN.getPorId(cotizacionDTO);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public CotizacionDTO getPorId(BigDecimal idToCotizacion)
	throws SystemException {
		return new CotizacionSN(CotizacionDN.nombreUsuario)
		.getPorId(idToCotizacion);
	}
	/**
	 * 
	 * @param subIncisoCotizacionDTO
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public SubIncisoCotizacionDTO agregarSubIncisoCotizacionDTO(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario)
	throws SystemException {
		SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN
		.getInstancia();
		return subIncisoCotizacionDN.agregar(subIncisoCotizacionDTO, nombreUsuario);
	}
	/**
	 * 
	 * @param subIncisoCotizacionDTO
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public SubIncisoCotizacionDTO modificarSubIncisoCotizacionDTO(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO, String nombreUsuario)
	throws SystemException {
		SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN
		.getInstancia();
		return subIncisoCotizacionDN.modificar(subIncisoCotizacionDTO, nombreUsuario);
	}

	/**
	 * 
	 * @param subIncisoCotizacionDTO
	 * @throws SystemException
	 */
	public void borrarSubIncisoCotizacion(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO)
	throws SystemException {
		SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN();
		subIncisoCotSN.borrar(subIncisoCotizacionDTO);
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @param idToTipoPolizaAnterior
	 * @param idToTipoPolizaNueva
	 */
	public void cambiarTipoPoliza(CotizacionDTO cotizacionDTO,
			BigDecimal idToTipoPolizaAnterior, BigDecimal idToTipoPolizaNueva) {
		boolean generarComisiones = false;
		boolean borrarIncisos = false;
		if (idToTipoPolizaAnterior == null) {
			generarComisiones = true;
			borrarIncisos = true;
		} else if (idToTipoPolizaNueva.compareTo(idToTipoPolizaAnterior) != 0) {
			generarComisiones = true;
			borrarIncisos = true;
		}

		if (borrarIncisos) {
			try {
				List<IncisoCotizacionDTO> listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(cotizacionDTO.getIdToCotizacion());
				for (IncisoCotizacionDTO insicoCotizacion : listaIncisos) {
					try {
						IncisoCotizacionDN.getInstancia().borrar(insicoCotizacion, CotizacionDN.nombreUsuario);
					} catch (Exception e) {
					}
				}
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}

		if (generarComisiones) {
			List<ComisionCotizacionDTO> listaComision = null;
			try {
				listaComision = ComisionCotizacionDN.getInstancia().listarPorCotizacion(cotizacionDTO.getIdToCotizacion());
			} catch (ExcepcionDeAccesoADatos e2) {
			} catch (SystemException e2) {
			}
			for (ComisionCotizacionDTO comision : listaComision) {
				try {
					ComisionCotizacionDN.getInstancia().borrar(comision);
				} catch (ExcepcionDeAccesoADatos e) {
				} catch (SystemException e) {
				}
			}

			if (cotizacionDTO.getTipoPolizaDTO() != null) {
				List<CoberturaDTO> coberturas = null;
				try {
					coberturas = CoberturaDN.getInstancia().listarVigentesPorTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
				} catch (ExcepcionDeAccesoADatos e1) {
				} catch (SystemException e1) {
				}
				if (coberturas == null) {
					return;
				}
				List<SubRamoDTO> listaSubRamosCotizacion = new ArrayList<SubRamoDTO>();
				for (CoberturaDTO cobertura : coberturas) {
					if (!listaSubRamosCotizacion.contains(cobertura
							.getSubRamoDTO())) {
						listaSubRamosCotizacion.add(cobertura.getSubRamoDTO());
					}
				}
				for (SubRamoDTO subRamoDTO : listaSubRamosCotizacion) {
					// Si es incendio, se generan tres registros para comisi�n
					// if (subRamoDTO.getCodigoSubRamo().compareTo(new
					// BigDecimal(1))==0){
					if (subRamoDTO.getIdTcSubRamo()
							.compareTo(new BigDecimal(1)) == 0
							|| subRamoDTO.getCodigoSubRamo().compareTo(
									new BigDecimal(101)) == 0) {
						for (int i = 1; i <= 3; i++) {
							ComisionCotizacionId id = new ComisionCotizacionId();
							id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
							id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
							id.setTipoPorcentajeComision((short) i);

							ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
							comisionCotizacionDTO.setId(id);
							comisionCotizacionDTO.setCotizacionDTO(cotizacionDTO);
							comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
							comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
							comisionCotizacionDTO.setClaveAutComision((short) 0);
							if (i == 1) {
								comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO
										.getPorcentajeMaximoComisionRO()));
								comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO
										.getPorcentajeMaximoComisionRO()));
							} else if (i == 2) {
								comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO
										.getPorcentajeMaximoComisionRCI()));
								comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO
										.getPorcentajeMaximoComisionRCI()));
							} else {
								comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO
										.getPorcentajeMaximoComisionPRR()));
								comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO
										.getPorcentajeMaximoComisionPRR()));
							}
							try {
								new ComisionCotizacionSN().agregar(comisionCotizacionDTO);
							} catch (SystemException e) {
							}
						}
					}
					// Si no es incendio, s�lo se genera un registro de
					// comisiones usando el campo porcentajeMaximoComisionRO
					else {
						ComisionCotizacionId id = new ComisionCotizacionId();
						id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
						id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						id.setTipoPorcentajeComision((short) 1);

						ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
						comisionCotizacionDTO.setId(id);
						comisionCotizacionDTO.setCotizacionDTO(cotizacionDTO);
						comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
						comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
						comisionCotizacionDTO.setClaveAutComision((short) 0);
						comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO
								.getPorcentajeMaximoComisionRO()));
						comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO
								.getPorcentajeMaximoComisionRO()));

						try {
							new ComisionCotizacionSN()
							.agregar(comisionCotizacionDTO);
						} catch (SystemException e) {
						} catch (Exception e) {
						}
					}
				}
			}
		}
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CotizacionDTO> listarCotizacionFiltrado(
			CotizacionDTO cotizacionDTO, Usuario usuario) throws ExcepcionDeAccesoADatos,
			SystemException {
		String claveCotizacionesFiltradas = "midas.danios.cotizacion.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveCotizacionesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveCotizacionesFiltradas);
			if(!atributoUsuario.isActivo()) {
				aplicaFiltrado = false;
			}					
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado) {
			cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		}
		
		return new CotizacionSN(CotizacionDN.nombreUsuario).listarCotizacionesFiltrado(cotizacionDTO);
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public Long obtenerTotalCotizacionesFiltrado(CotizacionDTO cotizacionDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException {
		String claveCotizacionesFiltradas = "midas.danios.cotizacion.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveCotizacionesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveCotizacionesFiltradas);
			if(!atributoUsuario.isActivo()) {
				aplicaFiltrado = false;
			}					
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado) {
			cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		}
		
		return new CotizacionSN(CotizacionDN.nombreUsuario).obtenerTotalCotizacionesFiltrado(cotizacionDTO);
	}

	/**
	 * Lista todas las cotizaciones que no tenga nestatus de orden de trabajo.
	 * Los estatus para las cotizaciones son: 10:COT En Proceso, 13:COT
	 * Liberada, 14:COT Lista para emitir, 15: COT Asignada, 16:COT Emitida,
	 * 18:COT Rechazada y 19:COT Cancelada
	 * 
	 * @return List<CotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CotizacionDTO> listarCotizaciones()
			throws ExcepcionDeAccesoADatos, SystemException {
		List<CotizacionDTO> listaResultado = new ArrayList<CotizacionDTO>();
		listaResultado = new CotizacionSN(CotizacionDN.nombreUsuario).listarCotizaciones();
		return listaResultado;
	}
	/**
	 * 
	 * @param ramoTipoPolizaDTO
	 * @return
	 * @throws SystemException
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRamoInciso(
			RamoTipoPolizaDTO ramoTipoPolizaDTO) throws SystemException {
		ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
		return configuracionDatoIncisoCotizacionSN.getDatosRamoInciso(ramoTipoPolizaDTO.getId().getIdtcramo());
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<RamoTipoPolizaDTO> listarRamosCotizacion(
			CotizacionDTO cotizacionDTO) throws SystemException {
		RamoTipoPolizaSN ramoTipoPolizaSN = new RamoTipoPolizaSN();
		return ramoTipoPolizaSN.buscarPorPropiedad("id.idtotipopoliza",	cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarIncisos(CotizacionDTO cotizacionDTO)
	throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.buscarPorIdCotizacion(cotizacionDTO.getIdToCotizacion());
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarIncisos(BigDecimal idToCotizacion)
	throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.listarPorCotizacionId(idToCotizacion);
	}

	/**
	 * Obtiene los registros de direccionCobro y TipoPoliza correspondientes al
	 * registro cotizacion recibido.
	 * 
	 * @param cotizacionDTO
	 *            el registro cotizacionDTO.
	 * @return cotizacionDTO el registro cotizacionDTO conteniendo los objetos
	 *         foraneos.
	 * @throws SystemException
	 */
	public CotizacionDTO obtenerDatosForaneos(CotizacionDTO cotizacionDTO)
	throws SystemException {
		return new CotizacionSN(CotizacionDN.nombreUsuario)
		.obtenerDatosForaneos(cotizacionDTO);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToInciso
	 * @return
	 * @throws SystemException
	 */
	public BigDecimal separarInciso(BigDecimal idToCotizacion,
			BigDecimal idToInciso) throws SystemException {
		CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);
		CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(idToCotizacion);

		CotizacionDTO cotizacionNuevaDTO = cotizacionSN.getPorId(idToCotizacion);
		cotizacionNuevaDTO.setIdToCotizacion(null);
		SolicitudSN solicitudSN = new SolicitudSN();
		SolicitudDTO solicitudNuevaDTO = solicitudSN.getPorId(cotizacionDTO
				.getSolicitudDTO().getIdToSolicitud());
		solicitudNuevaDTO.setIdToSolicitud(null);
		solicitudNuevaDTO = solicitudSN.agregar(solicitudNuevaDTO);

		cotizacionNuevaDTO.setSolicitudDTO(solicitudNuevaDTO);
		cotizacionNuevaDTO = cotizacionSN.agregar(cotizacionNuevaDTO);

		// Copiar los documentos de la solicitud
		DocumentoDigitalSolicitudSN documentoDigitalSolicitudSN = new DocumentoDigitalSolicitudSN();
		List<DocumentoDigitalSolicitudDTO> documentosSolicitud = documentoDigitalSolicitudSN
		.listarDocumentosSolicitud(cotizacionDTO.getSolicitudDTO()
				.getIdToSolicitud());
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		for (DocumentoDigitalSolicitudDTO documento : documentosSolicitud) {
			documento.setIdToDocumentoDigitalSolicitud(null);
			documento.setSolicitudDTO(solicitudNuevaDTO);

			ControlArchivoDTO controlArchivoNuevo = controlArchivoSN
			.getPorId(documento.getIdToControlArchivo());
			controlArchivoNuevo.setIdToControlArchivo(null);
			controlArchivoNuevo = controlArchivoSN.agregar(controlArchivoNuevo);
			try {
				VaultAction.copyFile(controlArchivoSN.getPorId(documento
						.getIdToControlArchivo()), controlArchivoNuevo);
			} catch (SystemException e) {
				controlArchivoSN.borrar(controlArchivoNuevo);
				throw e;
			}
			documento.setIdToControlArchivo(controlArchivoNuevo
					.getIdToControlArchivo());
		}
		solicitudNuevaDTO.setDocumentoDigitalSolicitudDTOs(documentosSolicitud);
		solicitudNuevaDTO = solicitudSN.actualizar(solicitudNuevaDTO);

		// Copiar los documentos digitales de la cotizacion
		DocumentoDigitalCotizacionSN documentoDigitalCotizacionSN = new DocumentoDigitalCotizacionSN();
		List<DocumentoDigitalCotizacionDTO> documentosCotizacion = documentoDigitalCotizacionSN
		.listarDocumentosCotizacion(cotizacionDTO.getIdToCotizacion());
		for (DocumentoDigitalCotizacionDTO documento : documentosCotizacion) {
			documento.setIdDocumentoDigitalCotizacion(null);
			documento.setCotizacionDTO(cotizacionNuevaDTO);

			ControlArchivoDTO controlArchivoNuevo = controlArchivoSN
			.getPorId(documento.getIdControlArchivo());
			controlArchivoNuevo.setIdToControlArchivo(null);
			controlArchivoNuevo = controlArchivoSN.agregar(controlArchivoNuevo);
			try {
				VaultAction.copyFile(controlArchivoSN.getPorId(documento.getIdControlArchivo()), controlArchivoNuevo);
			} catch (SystemException e) {
				controlArchivoSN.borrar(controlArchivoNuevo);
				throw e;
			}
			documento.setIdControlArchivo(controlArchivoNuevo
					.getIdToControlArchivo());
		}
		cotizacionNuevaDTO
		.setDocumentoDigitalCotizacionDTOs(documentosCotizacion);
		cotizacionSN.modificar(cotizacionNuevaDTO);

		// Copiar las comisiones de la cotizacion
		ComisionCotizacionSN comisionCotizacionSN = new ComisionCotizacionSN();
		List<ComisionCotizacionDTO> comisiones = comisionCotizacionSN
		.buscarPorPropiedad("cotizacionDTO.idToCotizacion",
				cotizacionDTO.getIdToCotizacion());
		for (ComisionCotizacionDTO comision : comisiones) {
			comision.getId().setIdToCotizacion(	cotizacionNuevaDTO.getIdToCotizacion());
			comision.setCotizacionDTO(cotizacionNuevaDTO);
			if (comision.getClaveAutComision().intValue() != 0) {
				comision.setClaveAutComision((short) 1);
			}
			ComisionCotizacionDN.getInstancia().agregar(comision);
		}
		// Copiar el inciso cuyo ID se recibi�
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(idToCotizacion);
		incisoCotizacionId.setNumeroInciso(idToInciso);
		IncisoCotizacionDTO incisoACopiar = incisoCotizacionSN.getPorId(incisoCotizacionId);
		if (incisoACopiar != null) {
			incisoACopiar.getId().setIdToCotizacion(
					cotizacionNuevaDTO.getIdToCotizacion());
			incisoACopiar.setCotizacionDTO(cotizacionNuevaDTO);
			if (incisoACopiar.getClaveEstatusInspeccion().intValue() != 0) {
				incisoACopiar.setClaveEstatusInspeccion((short) 1);
			}
			if (incisoACopiar.getClaveAutInspeccion().intValue() != 0) {
				incisoACopiar.setClaveAutInspeccion((short) 1);
			}
			IncisoCotizacionDTO incisoOriginal = incisoCotizacionSN.getPorId(incisoCotizacionId);

			// Se copian los datos del inciso original al inciso nuevo
			IncisoCotizacionDN.getInstancia().copiar(incisoACopiar,incisoOriginal, CotizacionDN.nombreUsuario);

			// Se borra el inciso Original de la cotizacion
			IncisoCotizacionDN.getInstancia().borrar(incisoOriginal,CotizacionDN.nombreUsuario);
		}

		// Copiar los textos adicionales de la cotizacion
		List<TexAdicionalCotDTO> texAdicionalesCot = TexAdicionalCotDN
		.getInstancia().buscarPorPropiedad("cotizacion.idToCotizacion",
				cotizacionDTO.getIdToCotizacion());
		for (TexAdicionalCotDTO texAdicional : texAdicionalesCot) {
			texAdicional.setIdToTexAdicionalCot(null);
			texAdicional.setCotizacion(cotizacionNuevaDTO);
			if (texAdicional.getClaveAutorizacion().intValue() != 0) {
				texAdicional.setClaveAutorizacion((short) 1);
			}
			TexAdicionalCotDN.getInstancia().agregar(texAdicional);
		}

		// Copiar los registros de AgrupacionCotizacion
		try {
			PrimerRiesgoLUCSN agrupacionCotSN = new PrimerRiesgoLUCSN();
			List<AgrupacionCotDTO> listaAgrupacion = agrupacionCotSN
			.buscarPorPropiedad("id.idToCotizacion", cotizacionDTO
					.getIdToCotizacion());
			if (listaAgrupacion != null) {
				for (AgrupacionCotDTO agrupacion : listaAgrupacion) {
					agrupacion.getId().setIdToCotizacion(
							cotizacionNuevaDTO.getIdToCotizacion());
					agrupacionCotSN.agregar(agrupacion);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cotizacionNuevaDTO.getIdToCotizacion();
	}
	/**
	 * 
	 * @param seccionCotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<CoberturaCotizacionDTO> getCoberturasPorSeccion(SeccionCotizacionDTO seccionCotizacionDTO) throws SystemException {
		CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
		CoberturaCotizacionDTO coberturaTMP = new CoberturaCotizacionDTO();
		coberturaTMP.setId(new CoberturaCotizacionId());
		coberturaTMP.getId().setIdToCotizacion(seccionCotizacionDTO.getId().getIdToCotizacion());
		coberturaTMP.getId().setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
		coberturaTMP.getId().setNumeroInciso(seccionCotizacionDTO.getId().getNumeroInciso());
		return coberturaCotizacionSN.listarFiltrado(coberturaTMP);//buscarPorPropiedad("seccionCotizacionDTO",seccionCotizacionDTO);
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @throws SystemException
	 */
	public void copiarCotizacion(CotizacionDTO cotizacionDTO)
	throws SystemException {
		CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);
		CotizacionDTO cotizacionNuevaDTO = cotizacionSN.getPorId(cotizacionDTO);
		cotizacionNuevaDTO.setIdToCotizacion(null);

		SolicitudSN solicitudSN = new SolicitudSN();
		SolicitudDTO solicitudNuevaDTO = solicitudSN.getPorId(cotizacionDTO
				.getSolicitudDTO().getIdToSolicitud());
		solicitudNuevaDTO.setIdToSolicitud(null);
		solicitudNuevaDTO = solicitudSN.agregar(solicitudNuevaDTO);

		cotizacionNuevaDTO.setSolicitudDTO(solicitudNuevaDTO);
		cotizacionNuevaDTO = cotizacionSN.agregar(cotizacionNuevaDTO);

		DocumentoDigitalSolicitudSN documentoDigitalSolicitudSN = new DocumentoDigitalSolicitudSN();
		List<DocumentoDigitalSolicitudDTO> documentosSolicitud = documentoDigitalSolicitudSN
		.listarDocumentosSolicitud(cotizacionDTO.getSolicitudDTO()
				.getIdToSolicitud());
		for (DocumentoDigitalSolicitudDTO documento : documentosSolicitud) {
			documento.setIdToDocumentoDigitalSolicitud(null);
			documento.setSolicitudDTO(solicitudNuevaDTO);

			ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
			ControlArchivoDTO controlArchivoNuevo = controlArchivoSN
			.getPorId(documento.getIdToControlArchivo());
			controlArchivoNuevo.setIdToControlArchivo(null);
			controlArchivoNuevo = controlArchivoSN.agregar(controlArchivoNuevo);
			try {
				VaultAction.copyFile(controlArchivoSN.getPorId(documento
						.getIdToControlArchivo()), controlArchivoNuevo);
			} catch (SystemException e) {
				controlArchivoSN.borrar(controlArchivoNuevo);
				throw e;
			}
			documento.setIdToControlArchivo(controlArchivoNuevo
					.getIdToControlArchivo());
		}
		solicitudNuevaDTO.setDocumentoDigitalSolicitudDTOs(documentosSolicitud);
		solicitudNuevaDTO = solicitudSN.actualizar(solicitudNuevaDTO);

		DocumentoDigitalCotizacionSN documentoDigitalCotizacionSN = new DocumentoDigitalCotizacionSN();
		List<DocumentoDigitalCotizacionDTO> documentosCotizacion = documentoDigitalCotizacionSN
		.listarDocumentosCotizacion(cotizacionDTO.getIdToCotizacion());
		for (DocumentoDigitalCotizacionDTO documento : documentosCotizacion) {
			documento.setIdDocumentoDigitalCotizacion(null);
			documento.setCotizacionDTO(cotizacionNuevaDTO);

			ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
			ControlArchivoDTO controlArchivoNuevo = controlArchivoSN.getPorId(documento.getIdControlArchivo());
			controlArchivoNuevo.setIdToControlArchivo(null);
			controlArchivoNuevo = controlArchivoSN.agregar(controlArchivoNuevo);
			try {
				VaultAction.copyFile(controlArchivoSN.getPorId(documento.getIdControlArchivo()), controlArchivoNuevo);
			} catch (SystemException e) {
				controlArchivoSN.borrar(controlArchivoNuevo);
				throw e;
			}
			documento.setIdControlArchivo(controlArchivoNuevo
					.getIdToControlArchivo());
		}

		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		List<IncisoCotizacionDTO> incisos = incisoCotizacionSN.buscarPorIdCotizacion(cotizacionDTO.getIdToCotizacion());
		for (IncisoCotizacionDTO inciso : incisos) {
			inciso.getId().setIdToCotizacion(
					cotizacionNuevaDTO.getIdToCotizacion());
			inciso.setCotizacionDTO(cotizacionNuevaDTO);

			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN
			.getInstancia();
			incisoCotizacionDN.guardar(inciso, CotizacionDN.nombreUsuario);
		}
		cotizacionNuevaDTO.setDocumentoDigitalCotizacionDTOs(documentosCotizacion);
		cotizacionSN.modificar(cotizacionNuevaDTO);

		ComisionCotizacionSN comisionCotizacionSN = new ComisionCotizacionSN();
		List<ComisionCotizacionDTO> comisiones = comisionCotizacionSN.buscarPorPropiedad("cotizacionDTO.idToCotizacion",	cotizacionDTO.getIdToCotizacion());
		for (ComisionCotizacionDTO comision : comisiones) {
			comision.getId().setIdToCotizacion(cotizacionNuevaDTO.getIdToCotizacion());
			comision.setCotizacionDTO(cotizacionNuevaDTO);
			ComisionCotizacionDN.getInstancia().agregar(comision);
		}

		// Copiar los textos adicionales de la cotizacion
		List<TexAdicionalCotDTO> texAdicionalesCot = TexAdicionalCotDN.getInstancia()
		.buscarPorPropiedad("cotizacion.idToCotizacion",cotizacionDTO.getIdToCotizacion());
		for (TexAdicionalCotDTO texAdicional : texAdicionalesCot) {
			texAdicional.setIdToTexAdicionalCot(null);
			texAdicional.setCotizacion(cotizacionNuevaDTO);
			if (texAdicional.getClaveAutorizacion().intValue() != 0) {
				texAdicional.setClaveAutorizacion((short) 1);
			}
			TexAdicionalCotDN.getInstancia().agregar(texAdicional);
		}

		// Copiar los registros de AgrupacionCotizacion
		try {
			PrimerRiesgoLUCSN agrupacionCotSN = new PrimerRiesgoLUCSN();
			List<AgrupacionCotDTO> listaAgrupacion = agrupacionCotSN
			.buscarPorPropiedad("id.idToCotizacion", cotizacionDTO.getIdToCotizacion());
			if (listaAgrupacion != null) {
				for (AgrupacionCotDTO agrupacion : listaAgrupacion) {
					agrupacion.getId().setIdToCotizacion(cotizacionNuevaDTO.getIdToCotizacion());
					agrupacionCotSN.agregar(agrupacion);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public CotizacionDTO agregar(CotizacionDTO cotizacionDTO)
	throws ExcepcionDeAccesoADatos, SystemException {
		return new CotizacionSN(CotizacionDN.nombreUsuario)
		.agregar(cotizacionDTO);
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void modificar(CotizacionDTO cotizacionDTO)
	throws ExcepcionDeAccesoADatos, SystemException {
		new CotizacionSN(CotizacionDN.nombreUsuario).modificar(cotizacionDTO);
	}
	
	private Boolean igualarPrimas(List<CoberturaCotizacionDTO> coberturas,
			Double primaNeta, String nombreUsuario) throws SystemException {
		return igualarPrimasV2 (coberturas, primaNeta, nombreUsuario, false);
	}
	
	/**
	 * 
	 * @param coberturas
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private Boolean igualarPrimas(List<CoberturaCotizacionDTO> coberturas,
			Double primaNeta, String nombreUsuario, boolean forzarIgualacion) throws SystemException {
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal totalARDT = BigDecimal.ZERO;
		BigDecimal primaModificar = BigDecimal.ZERO;
		BigDecimal primaFueraIgualacion = BigDecimal.ZERO;
		BigDecimal factorIgualacion = BigDecimal.ZERO;
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- primaNeta = " + primaNeta,Level.INFO, null);
		//____________________________________________________________________
		
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			
			//____________________________________________________________________
			LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- idtoCotizacion = " + cobertura.getId().getIdToCotizacion(),Level.INFO, null);
			//____________________________________________________________________
			
			//____________________________________________________________________
			LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- numeroInciso = " + cobertura.getId().getNumeroInciso(),Level.INFO, null);
			//____________________________________________________________________
			
			//____________________________________________________________________
			LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- idToSeccion = " + cobertura.getId().getIdToSeccion(),Level.INFO, null);
			//____________________________________________________________________
			
			//____________________________________________________________________
			LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- idToCobertura = " + cobertura.getId().getIdToCobertura(),Level.INFO, null);
			//____________________________________________________________________
			
			Double primaARDT = DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId());
			totalARDT = totalARDT.add(BigDecimal.valueOf(primaARDT));

			Double prima = DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId());
			total = total.add(BigDecimal.valueOf(prima));

			if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0))
				|| forzarIgualacion == true) {
				primaModificar = primaModificar.add(BigDecimal.valueOf(primaARDT));
			}
		}
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- total = " + total,Level.INFO, null);
		//____________________________________________________________________
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- totalARDT = " + totalARDT,Level.INFO, null);
		//____________________________________________________________________
		
		primaFueraIgualacion = totalARDT.subtract(primaModificar);
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- primaFueraIgualacion = " + primaFueraIgualacion,Level.INFO, null);
		//____________________________________________________________________
		
		BigDecimal diferencia = total.subtract(BigDecimal.valueOf(primaNeta));
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- diferencia = " + diferencia,Level.INFO, null);
		//____________________________________________________________________
		
		
		BigDecimal valorRD = diferencia.multiply(BigDecimal.valueOf(100D)).divide(totalARDT.subtract(primaFueraIgualacion), 12, RoundingMode.HALF_UP);
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- valorRD = " + valorRD,Level.INFO, null);
		//____________________________________________________________________
		
		
		if(valorRD.doubleValue() > 0D) {
			factorIgualacion = BigDecimal.ONE.subtract(valorRD.divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).abs());
		} else {
			factorIgualacion = BigDecimal.ONE.add(valorRD.divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).abs());
		}
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- factorIgualacion = " + factorIgualacion,Level.INFO, null);
		//____________________________________________________________________
				
		
		valorRD = valorRD.abs();
		if (factorIgualacion.doubleValue() <= 0D && forzarIgualacion == false) {
			return Boolean.FALSE;
		}
		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
		if (factorIgualacion.doubleValue() > 0D) {
			if (factorIgualacion.doubleValue() - 1D > 0D) {
				for (CoberturaCotizacionDTO cobertura : coberturas) {
					if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
							&& (cobertura.getClaveFacultativo() == null
							|| cobertura.getClaveFacultativo() == 0))
						|| forzarIgualacion == true) {
						List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN
								.listarRecargosRiesgoPorCoberturaCotizacion(
										cobertura,
										Sistema.RECARGO_IGUALACION,
										Boolean.FALSE);
						for (RecargoRiesgoCotizacionDTO recargo : recargos) {
							recargo.setValorRecargo(valorRD.doubleValue());
							recargo.setClaveContrato((short) 1);
							recargoRiesgoCotizacionSN.modificar(recargo);
						}
						List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
								.listarDescuentosRiesgoPorCoberturaCotizacion(
										cobertura,
										Sistema.DESCUENTO_IGUALACION,
										Boolean.FALSE);
						for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
							descuento.setValorDescuento(0D);
							descuento.setClaveContrato((short) 0);
							descuentoRiesgoCotizacionSN.modificar(descuento);
						}
					}
				}
			} else if (factorIgualacion.doubleValue() - 1D < 0D) {
				for (CoberturaCotizacionDTO cobertura : coberturas) {
					if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
							&& (cobertura.getClaveFacultativo() == null
							|| cobertura.getClaveFacultativo() == 0))
						|| forzarIgualacion == true) {
						List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
								.listarDescuentosRiesgoPorCoberturaCotizacion(
										cobertura,
										Sistema.DESCUENTO_IGUALACION,
										Boolean.FALSE);
						for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
							descuento.setValorDescuento(valorRD.doubleValue());
							descuento.setClaveContrato((short) 1);
							descuentoRiesgoCotizacionSN.modificar(descuento);

						}
						List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN
								.listarRecargosRiesgoPorCoberturaCotizacion(
										cobertura,
										Sistema.RECARGO_IGUALACION,
										Boolean.FALSE);
						for (RecargoRiesgoCotizacionDTO recargo : recargos) {
							recargo.setValorRecargo(0D);
							recargo.setClaveContrato((short) 0);
							recargoRiesgoCotizacionSN.modificar(recargo);
						}
					}
				}
			}
		}
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0))
				|| forzarIgualacion == true) {
				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
				calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
			}
		}
		return Boolean.TRUE;
	}
	
	private Boolean igualarPrimasV2(List<CoberturaCotizacionDTO> coberturas,
			Double primaNeta, String nombreUsuario, boolean forzarIgualacion) throws SystemException {
		
		Map<CoberturaCotizacionId,BigDecimal> mapaPrimaARDTPorCobertura = new HashMap<CoberturaCotizacionId, BigDecimal>();
		Map<CoberturaCotizacionId,BigDecimal> mapaPrimaPorCobertura = new HashMap<CoberturaCotizacionId, BigDecimal>();
		
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal totalARDT = BigDecimal.ZERO;
		BigDecimal primaModificar = BigDecimal.ZERO;
		BigDecimal primaFueraIgualacion = BigDecimal.ZERO;
		BigDecimal primaIgualar = BigDecimal.ZERO;
//		BigDecimal factorIgualacion = BigDecimal.ZERO;
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- primaNeta = " + primaNeta,Level.INFO, null);
		//____________________________________________________________________
		
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			
			BigDecimal primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			
			if(primaARDT.compareTo(BigDecimal.ZERO) == 0){
				//Si la prima es cero, se manda recalcular la cobertura debido a que los detalles de prima son inconsistentes.
				CalculoCotizacionDN.getInstancia().calcularCobertura(cobertura, nombreUsuario, true);
				
				primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			}
			
			mapaPrimaARDTPorCobertura.put(cobertura.getId(), primaARDT);
			
			totalARDT = totalARDT.add(primaARDT);

			BigDecimal prima = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId()));
			mapaPrimaPorCobertura.put(cobertura.getId(), prima);
			total = total.add(prima);

			if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0))
				|| forzarIgualacion == true) {
				primaModificar = primaModificar.add(prima);
			}
		}
		
		primaFueraIgualacion = total.subtract(primaModificar);
		
		primaIgualar = new BigDecimal(primaNeta).subtract(primaFueraIgualacion);

		if(new BigDecimal(primaNeta).compareTo(primaFueraIgualacion) <= 0){
			return Boolean.FALSE;
		}
		
		if(primaModificar.compareTo(BigDecimal.ZERO) == 0){
			return Boolean.TRUE;
		}
		
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0))
				|| forzarIgualacion == true) {
				
				BigDecimal proporcion = BigDecimal.ZERO;
				BigDecimal primaAQuedar = BigDecimal.ZERO;
				BigDecimal porcentajeNecesario = BigDecimal.ZERO;
				BigDecimal otrosARDs= BigDecimal.ZERO;
				BigDecimal porcentajeAplicable = BigDecimal.ZERO;
				
				//Si la cobertura permite igualacion
				BigDecimal prima = mapaPrimaPorCobertura.get(cobertura.getId());
				if(prima == null){
					prima = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId()));
				}
				
				BigDecimal primaARDT = mapaPrimaARDTPorCobertura.get(cobertura.getId());
				if(primaARDT == null){
					primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
				}
				
				proporcion = prima.divide(primaModificar, 12, RoundingMode.HALF_UP);
				
				primaAQuedar = primaIgualar.multiply(proporcion);
				
				porcentajeNecesario = BigDecimal.ONE.subtract(primaAQuedar.divide(primaARDT, 12, RoundingMode.HALF_UP));
				
				List<RecargoRiesgoCotizacionDTO> listaRecargos = coberturaCotizacionDN.listarRecargosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
				List<DescuentoRiesgoCotizacionDTO> listaDescuentos = coberturaCotizacionDN.listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
				List<AumentoRiesgoCotizacionDTO> listaAumentos = coberturaCotizacionDN.listarAumentosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
				
				List<BigDecimal> listaAumentosAcumulados = new ArrayList<BigDecimal>();
				List<BigDecimal> listaRecargosAcumulados = new ArrayList<BigDecimal>();
				List<BigDecimal> listaDescuentosAcumulados = new ArrayList<BigDecimal>();
				
//				BigDecimal otrosARDS = BigDecimal.ZERO;
				BigDecimal sumatoriaRecargos = BigDecimal.ZERO;
				BigDecimal sumatoriaDescuentos = BigDecimal.ZERO;
				BigDecimal sumatoriaAumentos = BigDecimal.ZERO;
				
				for(RecargoRiesgoCotizacionDTO recargo : listaRecargos){
					if(recargo.getId().getIdToRecargoVario().compareTo(Sistema.RECARGO_IGUALACION) != 0 &&
							recargo.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
						//Se excluyen recargos por igualaci�n
						
						if(!listaRecargosAcumulados.contains(recargo.getId().getIdToRecargoVario())){
							//Cada recargo s�lo se incluye una vez en el acumulado.
							listaRecargosAcumulados.add(recargo.getId().getIdToRecargoVario());
							sumatoriaRecargos = sumatoriaRecargos.add(new BigDecimal(recargo.getValorRecargo()));
						}
						
					}
				}
				
				for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
					if(descuento.getId().getIdToDescuentoVario().compareTo(Sistema.DESCUENTO_IGUALACION) != 0 &&
							descuento.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
						//Se excluyen recargos por igualaci�n
						
						if(!listaDescuentosAcumulados.contains(descuento.getId().getIdToDescuentoVario())){
							//Cada recargo s�lo se incluye una vez en el acumulado.
							listaDescuentosAcumulados.add(descuento.getId().getIdToDescuentoVario());
							sumatoriaDescuentos = sumatoriaDescuentos.add(new BigDecimal(descuento.getValorDescuento()));
						}
					}
				}
				
				for(AumentoRiesgoCotizacionDTO aumento : listaAumentos){
					if(aumento.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
						//No se registran aumentos por igualaci�n, no se valida el tipo de aumento
						
						if(!listaAumentosAcumulados.contains(aumento.getId().getIdToAumentoVario())){
							//Cada recargo s�lo se incluye una vez en el acumulado.
							listaAumentosAcumulados.add(aumento.getId().getIdToAumentoVario());
							sumatoriaAumentos = sumatoriaAumentos.add(new BigDecimal(aumento.getValorAumento()));
						}
					}
				}
				
				otrosARDs = sumatoriaAumentos.add(sumatoriaRecargos).subtract(sumatoriaDescuentos);
				//Convertir el monto a porcentaje
				
//				otrosARDs = otrosARDs.multiply(new BigDecimal("100")).divide(primaARDT, 12, RoundingMode.HALF_UP);
				
				porcentajeAplicable = otrosARDs.add(porcentajeNecesario.multiply(new BigDecimal("100")));
				
				List<DescuentoRiesgoCotizacionDTO> listaDescuentoRiesgoIgualacion = obtenerDescuentoEspecial(listaDescuentos,Sistema.RECARGO_IGUALACION);
				List<RecargoRiesgoCotizacionDTO> listaRecargoRiesgoIgualacion = obtenerRecargoEspecial(listaRecargos,Sistema.RECARGO_IGUALACION);
				
				if(porcentajeAplicable.compareTo(BigDecimal.ZERO) > 0){
					//se aplica descuento
					for(DescuentoRiesgoCotizacionDTO descuentoRiesgoIgualacion : listaDescuentoRiesgoIgualacion){
						descuentoRiesgoIgualacion.setValorDescuento(porcentajeAplicable.abs().doubleValue());
						descuentoRiesgoIgualacion.setClaveContrato((short) 1);
						DescuentoRiesgoCotizacionDN.getInstancia().modificar(descuentoRiesgoIgualacion);
					}
					for(RecargoRiesgoCotizacionDTO recargoRiesgoIgualacion : listaRecargoRiesgoIgualacion){
						recargoRiesgoIgualacion.setValorRecargo(0d);
						recargoRiesgoIgualacion.setClaveContrato((short) 0);
						RecargoRiesgoCotizacionDN.getInstancia().modificar(recargoRiesgoIgualacion);
					}
					
				}
				else if (porcentajeAplicable.compareTo(BigDecimal.ZERO) < 0){
					//se aplica recargo
					for(DescuentoRiesgoCotizacionDTO descuentoRiesgoIgualacion : listaDescuentoRiesgoIgualacion){
						descuentoRiesgoIgualacion.setValorDescuento(0d);
						descuentoRiesgoIgualacion.setClaveContrato((short) 0);
						DescuentoRiesgoCotizacionDN.getInstancia().modificar(descuentoRiesgoIgualacion);
					}
					for(RecargoRiesgoCotizacionDTO recargoRiesgoIgualacion : listaRecargoRiesgoIgualacion){
						recargoRiesgoIgualacion.setValorRecargo(porcentajeAplicable.abs().doubleValue());
						recargoRiesgoIgualacion.setClaveContrato((short) 1);
						RecargoRiesgoCotizacionDN.getInstancia().modificar(recargoRiesgoIgualacion);
					}
				}
				
			}
			
		}
		
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0))
				|| forzarIgualacion == true) {
					CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
					calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
			}
		}
		
		return Boolean.TRUE;
	}
	
	private List<DescuentoRiesgoCotizacionDTO> obtenerDescuentoEspecial(List<DescuentoRiesgoCotizacionDTO> listaDescuentos,BigDecimal idToDescuentoEspecial){
		List<DescuentoRiesgoCotizacionDTO> listaDescuentoRiesgoIgualacion = new ArrayList<DescuentoRiesgoCotizacionDTO>();
		
		for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
			if(descuento.getId().getIdToDescuentoVario().compareTo(idToDescuentoEspecial) == 0){
				listaDescuentoRiesgoIgualacion.add(descuento);
			}
		}
		return listaDescuentoRiesgoIgualacion;
	}
	
	private List<RecargoRiesgoCotizacionDTO> obtenerRecargoEspecial(List<RecargoRiesgoCotizacionDTO> listaRecargos,BigDecimal idToRecargoEspecial){
		List<RecargoRiesgoCotizacionDTO> listaRecargoRiesgoIgualacion = new ArrayList<RecargoRiesgoCotizacionDTO>();
		
		for(RecargoRiesgoCotizacionDTO recargo: listaRecargos){
			if(recargo.getId().getIdToRecargoVario().compareTo(idToRecargoEspecial) == 0){
				listaRecargoRiesgoIgualacion.add(recargo);
			}
		}
		return listaRecargoRiesgoIgualacion;
	}
	
	/**
	 * 
	 * @param idToCotizacion
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarPrimas(BigDecimal idToCotizacion, Double primaNeta,
			String nombreUsuario) throws SystemException {
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadas(idToCotizacion);
		this.recalcularCoberturas(coberturas, nombreUsuario);
		return this.igualarPrimas(coberturas, primaNeta, nombreUsuario);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarPrimas(BigDecimal idToCotizacion, Double primaNeta, BigDecimal numeroInciso,
			String nombreUsuario) throws SystemException {
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadas(idToCotizacion, numeroInciso);
		this.recalcularCoberturas(coberturas, nombreUsuario);
		return this.igualarPrimas(coberturas, primaNeta, nombreUsuario);
	}	
	/**
	 * 
	 * @param idToCotizacion
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean eliminaIgualacionPrimas(BigDecimal idToCotizacion,
			String nombreUsuario) throws SystemException {
		Boolean exitoso = Boolean.TRUE;
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadas(idToCotizacion);
		try {
			this.recalcularCoberturas(coberturas, nombreUsuario);
		} catch (SystemException e) {
			e.printStackTrace();
			exitoso = Boolean.FALSE;
		}
		return exitoso;
	}	

	/**
	 * 
	 * @param idToCotizacion
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean eliminaIgualacionPrimas(BigDecimal idToCotizacion,
			String nombreUsuario,BigDecimal numeroInciso) throws SystemException {
		Boolean exitoso = Boolean.TRUE;
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		coberturaCotizacionDTO.setId(id);
		coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);		
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarFiltrado(coberturaCotizacionDTO);
		try {
			this.recalcularCoberturas(coberturas, nombreUsuario);
		} catch (SystemException e) {
			e.printStackTrace();
			exitoso = Boolean.FALSE;
		}
		return exitoso;
	}		
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean eliminaIgualacionPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, String nombreUsuario)
			throws SystemException {
		Boolean exitoso = Boolean.TRUE;
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccion(idToCotizacion, idToSeccion);
		try {
			this.recalcularCoberturas(coberturas, nombreUsuario);
		} catch (SystemException e) {
			e.printStackTrace();
			exitoso = Boolean.FALSE;
		}
		return exitoso;
	}

	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean eliminaIgualacionPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, String nombreUsuario, BigDecimal numeroInciso)
			throws SystemException {
		Boolean exitoso = Boolean.TRUE;
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setNumeroInciso(numeroInciso);
		coberturaCotizacionDTO.setId(id);
		coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);			
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarFiltrado(coberturaCotizacionDTO);
		try {
			this.recalcularCoberturas(coberturas, nombreUsuario);
		} catch (SystemException e) {
			e.printStackTrace();
			exitoso = Boolean.FALSE;
		}
		return exitoso;
	}	
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean eliminaIgualacionPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			String nombreUsuario) throws SystemException {
		Boolean exitoso = Boolean.TRUE;
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);

		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccionCobertura(id);
		try {
			this.recalcularCoberturas(coberturas, nombreUsuario);
		} catch (SystemException e) {
			e.printStackTrace();
			exitoso = Boolean.FALSE;
		}
		return exitoso;
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean eliminaIgualacionPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			String nombreUsuario, BigDecimal numeroInciso) throws SystemException {
		Boolean exitoso = Boolean.TRUE;
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);
		id.setNumeroInciso(numeroInciso);
		coberturaCotizacionDTO.setId(id);
		coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);			
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarFiltrado(coberturaCotizacionDTO);
		try {
			this.recalcularCoberturas(coberturas, nombreUsuario);
		} catch (SystemException e) {
			e.printStackTrace();
			exitoso = Boolean.FALSE;
		}
		return exitoso;
	}	
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, Double primaNeta, String nombreUsuario)
			throws SystemException {
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccion(idToCotizacion, idToSeccion);
		this.recalcularCoberturas(coberturas, nombreUsuario);
		return this.igualarPrimas(coberturas, primaNeta, nombreUsuario);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, Double primaNeta, BigDecimal numeroInciso, String nombreUsuario)
			throws SystemException {
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setNumeroInciso(numeroInciso);
		coberturaCotizacionDTO.setId(id);
		coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarFiltrado(coberturaCotizacionDTO);
		this.recalcularCoberturas(coberturas, nombreUsuario);
		return this.igualarPrimas(coberturas, primaNeta, nombreUsuario);
	}	
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, BigDecimal idToCobertura, Double primaNeta,
			String nombreUsuario) throws SystemException {
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);

		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccionCobertura(id);
		this.recalcularCoberturas(coberturas, nombreUsuario);
		return this.igualarPrimas(coberturas, primaNeta, nombreUsuario);
	}
	
	
	public Boolean igualarPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, BigDecimal idToCobertura, Double primaNeta, BigDecimal numeroInciso,
			String nombreUsuario) throws SystemException {
		return igualarPrimas(idToCotizacion,
				idToSeccion, idToCobertura, primaNeta, numeroInciso,
				nombreUsuario, false);
	}
	
	
	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param primaNeta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarPrimas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, BigDecimal idToCobertura, Double primaNeta, BigDecimal numeroInciso,
			String nombreUsuario, boolean forzarIgualacion) throws SystemException {
		CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setNumeroInciso(numeroInciso);
		id.setIdToCobertura(idToCobertura);
		coberturaCotizacionDTO.setId(id);
		coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarFiltrado(coberturaCotizacionDTO);
		this.recalcularCoberturas(coberturas, nombreUsuario, forzarIgualacion);
		return this.igualarPrimasV2(coberturas, primaNeta, nombreUsuario, forzarIgualacion);
	}
	
	
	private void recalcularCoberturas(List<CoberturaCotizacionDTO> coberturas,
			String nombreUsuario) throws SystemException {
		 	recalcularCoberturas(coberturas, nombreUsuario, false);
	}
	
	
	
	/**
	 * 
	 * @param coberturas
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	private void recalcularCoberturas(List<CoberturaCotizacionDTO> coberturas,
			String nombreUsuario, boolean forzarIgualacion) throws SystemException {
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if ((cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0))
				|| forzarIgualacion == true) {
				List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN.listarRecargosRiesgoPorCoberturaCotizacion(cobertura,Sistema.RECARGO_IGUALACION, Boolean.TRUE);
				for (RecargoRiesgoCotizacionDTO recargo : recargos) {
					recargo.setValorRecargo(0D);
					recargo.setClaveContrato((short) 0);
					recargoRiesgoCotizacionSN.modificar(recargo);
				}
				List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN.listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, Sistema.DESCUENTO_IGUALACION,Boolean.TRUE);
				for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
					descuento.setValorDescuento(0D);
					descuento.setClaveContrato((short) 0);
					descuentoRiesgoCotizacionSN.modificar(descuento);
				}
				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
				calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
			}
		}
	}

	/**
	 * 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param cuota
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Boolean igualarCuotas(BigDecimal idToCotizacion,
			BigDecimal idToSeccion, BigDecimal idToCobertura, Double cuota,
			String nombreUsuario) throws SystemException {
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);

		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccionCobertura(id);
		this.recalcularCoberturas(coberturas, nombreUsuario);
		BigDecimal factorIgualacion = BigDecimal.ZERO;
		BigDecimal cuotaOriginal = BigDecimal.ZERO;
		BigDecimal primaNeta = BigDecimal.ZERO;
		BigDecimal sumaAsegurada = BigDecimal.ZERO;
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			CoberturaCotizacionDTO coberturaTMP = coberturaCotizacionDN.getPorId(cobertura);
			if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion().shortValue() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0)) {
				primaNeta = primaNeta.add(BigDecimal.valueOf(coberturaTMP.getValorPrimaNeta()));
				sumaAsegurada = sumaAsegurada.add(BigDecimal.valueOf(coberturaTMP.getValorSumaAsegurada()));
			}
		}
		cuotaOriginal = primaNeta.divide(sumaAsegurada, 10, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(1000D));
		RecargoRiesgoCotizacionSN recargoRiesgoCotizacionSN = new RecargoRiesgoCotizacionSN();
		DescuentoRiesgoCotizacionSN descuentoRiesgoCotizacionSN = new DescuentoRiesgoCotizacionSN();
		factorIgualacion = BigDecimal.valueOf(cuota).divide(cuotaOriginal, 10, RoundingMode.HALF_UP);
		if (factorIgualacion.doubleValue() - 1D > 0D) {
			for (CoberturaCotizacionDTO cobertura : coberturas) {
				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
						&& (cobertura.getClaveFacultativo() == null
						|| cobertura.getClaveFacultativo() == 0)) {
					List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN
							.listarRecargosRiesgoPorCoberturaCotizacion(
									cobertura,
									Sistema.RECARGO_IGUALACION,
									Boolean.FALSE);
					for (RecargoRiesgoCotizacionDTO recargo : recargos) {
						recargo.setValorRecargo(BigDecimal.ONE.subtract(factorIgualacion).multiply(BigDecimal.valueOf(100D)).abs().doubleValue());
						recargo.setClaveContrato((short) 1);
						recargoRiesgoCotizacionSN.modificar(recargo);
					}
					List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
							.listarDescuentosRiesgoPorCoberturaCotizacion(
									cobertura,
									Sistema.DESCUENTO_IGUALACION,
									Boolean.FALSE);
					for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
						descuento.setValorDescuento(0D);
						descuento.setClaveContrato((short) 0);
						descuentoRiesgoCotizacionSN.modificar(descuento);
					}
				}
			}
		} else if (factorIgualacion.doubleValue() - 1D < 0D) {
			for (CoberturaCotizacionDTO cobertura : coberturas) {
				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
						&& (cobertura.getClaveFacultativo() == null
						|| cobertura.getClaveFacultativo() == 0)) {
					List<DescuentoRiesgoCotizacionDTO> descuentos = coberturaCotizacionDN
							.listarDescuentosRiesgoPorCoberturaCotizacion(
									cobertura,
									Sistema.DESCUENTO_IGUALACION,
									Boolean.FALSE);
					for (DescuentoRiesgoCotizacionDTO descuento : descuentos) {
						descuento.setValorDescuento(BigDecimal.ONE.subtract(factorIgualacion).multiply(BigDecimal.valueOf(100D)).abs().doubleValue());
						descuento.setClaveContrato((short) 1);
						descuentoRiesgoCotizacionSN.modificar(descuento);
					}
					List<RecargoRiesgoCotizacionDTO> recargos = coberturaCotizacionDN
							.listarRecargosRiesgoPorCoberturaCotizacion(cobertura,
									Sistema.RECARGO_IGUALACION, Boolean.FALSE);
					for (RecargoRiesgoCotizacionDTO recargo : recargos) {
						recargo.setValorRecargo(0D);
						recargo.setClaveContrato((short) 0);
						recargoRiesgoCotizacionSN.modificar(recargo);
					}
				}
			}
		}
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion() == 1
					&& (cobertura.getClaveFacultativo() == null
					|| cobertura.getClaveFacultativo() == 0)) {
				CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
				calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * 
	 * @param riesgoCotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgos(
			RiesgoCotizacionDTO riesgoCotizacionDTO) throws SystemException {
		RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN
		.getInstancia();
		List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN
		.listarFiltrado(riesgoCotizacionDTO);
		for (RiesgoCotizacionDTO riesgo : riesgos) {
			CoaseguroRiesgoCoberturaId idCoaseguro = new CoaseguroRiesgoCoberturaId();
			idCoaseguro.setIdToCobertura(riesgo.getRiesgoCoberturaDTO().getId().getIdtocobertura());
			idCoaseguro.setIdToSeccion(riesgo.getRiesgoCoberturaDTO().getId().getIdtoseccion());
			idCoaseguro.setIdToRiesgo(riesgo.getRiesgoCoberturaDTO().getId().getIdtoriesgo());
			CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO = new CoaseguroRiesgoCoberturaDTO();
			coaseguroRiesgoCoberturaDTO.setId(idCoaseguro);

			CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN = CoaseguroRiesgoCoberturaDN.getInstancia();
			List<CoaseguroRiesgoCoberturaDTO> coaseguros = coaseguroRiesgoCoberturaDN.listarFiltrado(coaseguroRiesgoCoberturaDTO);
			riesgo.getRiesgoCoberturaDTO().setCoaseguros(coaseguros);

			DeducibleRiesgoCoberturaId idDeducible = new DeducibleRiesgoCoberturaId();
			idDeducible.setIdToCobertura(riesgo.getRiesgoCoberturaDTO().getId().getIdtocobertura());
			idDeducible.setIdToSeccion(riesgo.getRiesgoCoberturaDTO().getId().getIdtoseccion());
			idDeducible.setIdToRiesgo(riesgo.getRiesgoCoberturaDTO().getId().getIdtoriesgo());
			DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO = new DeducibleRiesgoCoberturaDTO();
			deducibleRiesgoCoberturaDTO.setId(idDeducible);

			DeducibleRiesgoCoberturaDN deducibleRiesgoCoberturaDN = DeducibleRiesgoCoberturaDN.getInstancia();
			List<DeducibleRiesgoCoberturaDTO> deducibles = deducibleRiesgoCoberturaDN.listarFiltrado(deducibleRiesgoCoberturaDTO);
			riesgo.getRiesgoCoberturaDTO().setDeducibles(deducibles);
		}
		return riesgos;
	}
	/**
	 * 
	 * @param riesgoCotizacionDTO
	 * @throws SystemException
	 */
	public void modificarRiesgo(RiesgoCotizacionDTO riesgoCotizacionDTO)throws SystemException {
		RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
		List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.modificar(riesgoCotizacionDTO);

		riesgoCotizacionDTO.getCoberturaCotizacionDTO().setValorPrimaNeta(calcularPrimaNetaCobertura(riesgos));
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN
		.getInstancia();
		coberturaCotizacionDN.modificar(riesgoCotizacionDTO
				.getCoberturaCotizacionDTO());
	}
	/**
	 * 
	 * @param riesgos
	 * @return
	 */
	private Double calcularPrimaNetaCobertura(List<RiesgoCotizacionDTO> riesgos) {
		BigDecimal primaNeta = BigDecimal.ZERO;
		for (RiesgoCotizacionDTO riesgo : riesgos) {
			primaNeta = primaNeta.add(BigDecimal.valueOf(riesgo
					.getValorPrimaNeta()));
		}
		return primaNeta.doubleValue();
	}
	/**
	 * 
	 * @param propiedad
	 * @param valor
	 * @return
	 * @throws SystemException
	 */
	public List<CotizacionDTO> buscarPorPropiedad(String propiedad, Object valor)
	throws SystemException {
		return new CotizacionSN(CotizacionDN.nombreUsuario).buscarPorPropiedad(propiedad, valor);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param idTcRechazoCancelacion
	 * @param usuario
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void rechazarCancelarCotizacion(BigDecimal idToCotizacion,
			BigDecimal idTcRechazoCancelacion, Usuario usuario)
	throws ExcepcionDeAccesoADatos, SystemException {
		RechazoCancelacionDTO rechazoCancelacion = RechazoCancelacionDN
		.getInstancia().getPorId(idTcRechazoCancelacion);
		if (rechazoCancelacion != null) {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(idToCotizacion);
			cotizacionDTO = new CotizacionSN(CotizacionDN.nombreUsuario)
			.getPorId(cotizacionDTO);
			cotizacionDTO = new CotizacionSN(CotizacionDN.nombreUsuario)
			.obtenerDatosForaneos(cotizacionDTO);
			if (cotizacionDTO != null) {
				if (rechazoCancelacion.getClaveTipoMotivo() == 1) {
					cotizacionDTO
					.setClaveEstatus(Sistema.ESTATUS_ODT_RECHAZADA);
				} else {
					cotizacionDTO
					.setClaveEstatus(Sistema.ESTATUS_ODT_CANCELADA);
				}
				cotizacionDTO.setCodigoUsuarioModificacion(usuario
						.getNombreUsuario());
				cotizacionDTO.setFechaModificacion(new Date());
				OrdenTrabajoDN.getInstancia(CotizacionDN.nombreUsuario)
				.actualizarCotizacion(cotizacionDTO);
				CotizacionRechazoCancelacionId cotizacionRechazoId = new CotizacionRechazoCancelacionId();
				cotizacionRechazoId.setIdToCotizacion(cotizacionDTO
						.getIdToCotizacion());
				cotizacionRechazoId
				.setIdTcRechazoCancelacion(rechazoCancelacion
						.getIdTcRechazoCancelacion());

				CotizacionRechazoCancelacionDTO cotizacionRechazo = null;
				List<CotizacionRechazoCancelacionDTO> rechazosExistentes = CotizacionRechazoCancelacionDN
				.getInstancia().buscarPorPropiedad("id.idToCotizacion",idToCotizacion);
				for (CotizacionRechazoCancelacionDTO rechazo : rechazosExistentes) {
					CotizacionRechazoCancelacionDN.getInstancia().borrar(
							rechazo);
				}
				cotizacionRechazo = new CotizacionRechazoCancelacionDTO();
				cotizacionRechazo.setId(cotizacionRechazoId);
				cotizacionRechazo.setCotizacionDTO(cotizacionDTO);
				cotizacionRechazo.setRechazoCancelacionDTO(rechazoCancelacion);
				CotizacionRechazoCancelacionDN.getInstancia().agregar(
						cotizacionRechazo);
			}
		}
	}
	/**
	 * 
	 * @param subGiroDTO
	 * @return
	 */
	public PlenoReaseguroDTO obtenerPlenoReaseguro(SubGiroDTO subGiroDTO) {
		PlenoReaseguroDTO plenoReaseguro = null;
		try {
			plenoReaseguro = new CotizacionSN(nombreUsuario).obtenerPlenoReaseguro(subGiroDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return plenoReaseguro;
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(
			CotizacionDTO cotizacionDTO) throws SystemException {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		List<BigDecimal> riesgos = riesgoCoberturaSN
		.obtenerRiesgosPorTipoPoliza(cotizacionDTO.getTipoPolizaDTO()
				.getIdToTipoPoliza());
		List<ConfiguracionDatoIncisoCotizacionDTO> datos = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
		for (BigDecimal idToRiesgo : riesgos) {
			ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
			datos.addAll(configuracionDatoIncisoCotizacionSN
					.getDatosRiesgoInciso(idToRiesgo));
		}
		return datos;
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<BigDecimal> listarRiesgosCotizacion(CotizacionDTO cotizacionDTO)
	throws SystemException {
		RiesgoCoberturaSN riesgoCoberturaSN = new RiesgoCoberturaSN();
		return riesgoCoberturaSN.obtenerRiesgosPorTipoPoliza(cotizacionDTO
				.getTipoPolizaDTO().getIdToTipoPoliza());
	}
	/**
	 * 
	 * @param idToRiesgo
	 * @return
	 * @throws SystemException
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToRiesgo) throws SystemException {
		ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
		return configuracionDatoIncisoCotizacionSN.getDatosRiesgoInciso(idToRiesgo);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @return
	 * @throws SystemException
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoSubInciso(BigDecimal idToCotizacion, 
			BigDecimal numeroInciso,BigDecimal idToSeccion) throws SystemException {
		RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
		List<BigDecimal> riesgos = riesgoCoberturaDN.obtenerRiesgosPorSeccionCotizacion(idToCotizacion,
				numeroInciso, idToSeccion);
		List<ConfiguracionDatoIncisoCotizacionDTO> datos = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
		for (BigDecimal idToRiesgo : riesgos) {
			ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
			datos.addAll(configuracionDatoIncisoCotizacionSN
					.getDatosRiesgoSubInciso(idToRiesgo));
		}
		return datos;
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @return
	 * @throws SystemException
	 */
	public List<BigDecimal> listarRiesgosPorSeccionCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion) throws SystemException {
		RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
		return riesgoCoberturaDN.obtenerRiesgosPorSeccionCotizacion(
				idToCotizacion, numeroInciso, idToSeccion);
	}
	/**
	 * 
	 * @param idToRiesgo
	 * @return
	 * @throws SystemException
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoSubInciso(
			BigDecimal idToRiesgo) throws SystemException {
		ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
		return configuracionDatoIncisoCotizacionSN
		.getDatosRiesgoSubInciso(idToRiesgo);
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public Set<String> validarCotizacion(BigDecimal idToCotizacion, Usuario usuario)
	throws SystemException {
		Set<String> errores = new HashSet<String>();

		
		CotizacionDN cotizacionDN = CotizacionDN.INSTANCIA;
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
		int tipoRetroActividadDiferimiento=validaRetroActividadDiferimiento(cotizacionDTO);
		int tipoVigenciaMinimaMaxima=validaVigenciaMinimaMaxima(cotizacionDTO);
		
		
		if (cotizacionDTO.getSolicitudDTO().getEsRenovacion().intValue() == Sistema.ES_RENOVACION) {
			int tipoRetroDiferimientoRenovacion = validaRetroActividadDiferimientoRenovacion(cotizacionDTO);
			if (cotizacionDTO.getClaveAutRetroacDifer() != null
					&& cotizacionDTO.getClaveAutRetroacDifer().equals(Sistema.AUTORIZACION_REQUERIDA)) {
				if (tipoRetroDiferimientoRenovacion == 1) {
					errores.add("cotizacion.renovacion.retroactividad");
				}
			}
		}
		//Asegurado
//		ClienteDTO clienteDTO = null;
//		if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
//			clienteDTO = new ClienteDTO();
//			clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
//			clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
//			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, usuario.getNombreUsuario());
//		}else{
//			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), usuario.getNombreUsuario());					
//		}
//		if(clienteDTO != null){
//			if(!ValidaCURPDN.getInstancia().esValido(clienteDTO))
//				errores.add("cotizacion.asegurado.curp.invalido");
//		}
			
		//Contratante
//		if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
//			clienteDTO = new ClienteDTO();
//			clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
//			clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
//			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, usuario.getNombreUsuario());
//		}else{
//			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), usuario.getNombreUsuario());					
//		}
//		if(clienteDTO != null){
//			if(!ValidaCURPDN.getInstancia().esValido(clienteDTO))
//				errores.add("cotizacion.contratante.curp.invalido");
//		}
		
		if (cotizacionDTO.getDiasGracia() == null) {
			errores.add("cotizacion.diasGracia.vacia");
		}
		else{
			if(cotizacionDTO.getClaveAutorizacionDiasGracia() != null && (
					cotizacionDTO.getClaveAutorizacionDiasGracia().shortValue() == Sistema.AUTORIZACION_REQUERIDA ||
					cotizacionDTO.getClaveAutorizacionDiasGracia().shortValue() == Sistema.RECHAZADA )){
				errores.add("cotizacion.diasGracia.autorizacion");
			}
		}

		if (cotizacionDTO.getFolioPolizaAsociada() != null&& !cotizacionDTO.getFolioPolizaAsociada().equals("")) {
			// Verificar que la poliza asociada exista
			PolizaDN polizaDN = PolizaDN.getInstancia();
			if (!polizaDN.existePoliza(cotizacionDTO.getFolioPolizaAsociada(),usuario)) {
				errores.add("cotizacion.folioPolizaAsociada.noExiste");
			}
		}

		if (cotizacionDTO.getTipoNegocioDTO() == null) {
			errores.add("cotizacion.tipoNegocio.vacia");
		}
		
		if(cotizacionDTO.getClaveAutRetroacDifer() != null && 
			cotizacionDTO.getClaveAutRetroacDifer().equals(Sistema.AUTORIZACION_REQUERIDA)){
		    if (tipoRetroActividadDiferimiento==1){
			errores.add("cotizacion.retroactividad");
		    }else if(tipoRetroActividadDiferimiento==2){
			errores.add("cotizacion.diferimiento");
		    }
		}

		if(cotizacionDTO.getClaveAutVigenciaMaxMin() != null && 
			cotizacionDTO.getClaveAutVigenciaMaxMin().equals(Sistema.AUTORIZACION_REQUERIDA)){
		    if (tipoVigenciaMinimaMaxima==1){
			errores.add("cotizacion.vigencia.minima");
		    }else if(tipoVigenciaMinimaMaxima==2){
			errores.add("cotizacion.vigencia.maxima");
		    }
		    
		}
		
		if(cotizacionDTO.getClaveAutRetroacDifer() != null && 
				cotizacionDTO.getClaveAutRetroacDifer().shortValue() == Sistema.RECHAZADA){
			errores.add("cotizacion.retroactividad.rechazada");
		}

		if(cotizacionDTO.getClaveAutVigenciaMaxMin() != null && 
				cotizacionDTO.getClaveAutVigenciaMaxMin().shortValue() == Sistema.RECHAZADA){
			errores.add("cotizacion.vigencia.rechazada");
		}	
		
		if (cotizacionDTO.getIdMedioPago() == null) {
			errores.add("cotizacion.medioPago.vacia");
		}

		if (!validarDocumentosComplementar(cotizacionDTO)) {
			errores.add("cotizacion.documentosOperacionesIlicitas.vacia");
		}

		/**
		 * 15/12/2009 Se agreg� validaci�n de reaseguro facultativo para permitir la emisi�n de la cotizaci�n.
		 * 26/03/2010 Se modific� la invocaci�n a reaseguro.
		 * JACC
		 * 19/04/2010 Se agrega validacion:  2)  No pedir permiso a reaseguro para liberar ni emitir
		 * 29/04/2010 JLAB. Se modifica la validaci�n, los casos en los que no se pide permiso para liberar ni emitir es s�lo en los endosos de cancelaci�n y rehabilitaci�n
		 * 20/08/2010 JLAB. Se modifica la validaci�n, se agrega par�metro "tipoEndoso" para hacer la validaci�n del lado de reaseguro.
		 */
		SoporteReaseguroCotizacionDN soporteReaseguroCotizacion = new SoporteReaseguroCotizacionDN(idToCotizacion,false);
		List<String> motivoError = new ArrayList<String>();
		if( !soporteReaseguroCotizacion.autorizadoEmision(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso(),motivoError)){
			for(String error: motivoError) {
				if(error != null) {
					errores.add(error);
				}
			}
		}   	

//		JEAS Desarrollo de No emitir en periodo determinado. 26 Marzo 2010
		if(UtileriasWeb.validaFechaIncluidaEnIntervalo(UtileriasWeb.fijaFecha(Sistema.FECHANOEMITEINICIAL),UtileriasWeb.fijaFecha(Sistema.FECHANOEMITEFINAL),new Date())){
			errores.add("cotizacion.emitir.sinAutorizacionDelSistema.PorFecha");
		}
		
		
		if(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() != null && 
				cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().compareTo(Sistema.AUTORIZACION_REQUERIDA) == 0){
			errores.add("cotizacion.rpf.sinAutorizar");
		}
		
		if(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() != null && 
				cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().compareTo(Sistema.RECHAZADA) == 0){
			errores.add("cotizacion.rpf.rechazado");
		}
		
		if(cotizacionDTO.getClaveAutorizacionDerechos() != null && 
				cotizacionDTO.getClaveAutorizacionDerechos().compareTo(Sistema.AUTORIZACION_REQUERIDA) == 0){
			errores.add("cotizacion.derechos.sinAutorizar");
		}
		
		if(cotizacionDTO.getClaveAutorizacionDerechos() != null && 
				cotizacionDTO.getClaveAutorizacionDerechos().compareTo(Sistema.RECHAZADA) == 0){
			errores.add("cotizacion.derechos.rechazados");
		}
		
		// RMP Valida que el agente de la solicitud a la que pertenece la cotizaci�n
		// est� activo, y tenga cedula vigente
		final SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		final Integer idAgente = cotizacionDTO.getSolicitudDTO().getCodigoAgente().intValue();
		errores.addAll(solicitudDN.validarAgente(idAgente, nombreUsuario));				
		return errores;
	}
	
	/**
	 * 
	 * @param cotizacionDTO
	 * @throws SystemException
	 */
	private  int validaRetroActividadDiferimiento(CotizacionDTO cotizacionDTO) throws SystemException {
	    	Date fechaSolicitud = cotizacionDTO.getSolicitudDTO().getFechaCreacion();
		Date fechaInicioVigencia = cotizacionDTO.getFechaInicioVigencia();
		int tipo=0;
		if (fechaInicioVigencia != null && fechaSolicitud != null) {
		    //Retroactividad
		    if(!OrdenTrabajoDN.validarRetroactividad(fechaInicioVigencia, fechaSolicitud)){
			 if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
				    !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
        			 tipo=1;
        			 cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_REQUERIDA); 
        			 cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
        			 
        		    }
		    }
		    
		    //Diferimiento
		    if(!OrdenTrabajoDN.validarDiferimiento(fechaInicioVigencia, fechaSolicitud)){
			 if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
				 !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
       			 	tipo=2;
       			 	cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_REQUERIDA);
       			 	cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
       			 }
		    }
		    
		    if(tipo==0 ){
			if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
				 !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
			    cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_NO_REQUERIDA); 
			    cotizacionDTO.setFechaSolAutRetroacDifer(null);
			    
			    }
		   }
		   modificar(cotizacionDTO);
		}
		return tipo;
		
	}
	private  int validaRetroActividadDiferimientoRenovacion(CotizacionDTO cotizacionDTO) throws SystemException {
		int tipo=0;
		RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN.getInstancia();
		RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN.buscarDetalleRenovacionPoliza(cotizacionDTO.getSolicitudDTO().getIdToPolizaAnterior());
		if(renovacionPolizaDTO != null){
			if(!OrdenTrabajoDN.validarFechaRenovacion(renovacionPolizaDTO.getPolizaDTO().getCotizacionDTO().getFechaFinVigencia())){
				 if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
						    !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
		        			 tipo=1;
		        			 cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_REQUERIDA); 
		        			 cotizacionDTO.setFechaSolAutRetroacDifer(Calendar.getInstance().getTime());
				 }
		        			 		
			}
		    if(tipo==0 ){
				if(cotizacionDTO.getClaveAutRetroacDifer()==null || 
					 !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutRetroacDifer().toString())){
				    cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZACION_NO_REQUERIDA); 
				    cotizacionDTO.setFechaSolAutRetroacDifer(null);
				    
				 }
			 }
			 modificar(cotizacionDTO);			
		}
		return tipo;
	}
	
	private  int validaVigenciaMinimaMaxima(CotizacionDTO cotizacionDTO) throws SystemException {
	    	Date fechaInicioVigencia = cotizacionDTO.getFechaInicioVigencia();
		Date fechaFinVigencia = cotizacionDTO.getFechaFinVigencia();
		int tipo=0;
		
		if (fechaInicioVigencia != null && fechaFinVigencia != null) {
		     //Vigencia Minima
		    if(!OrdenTrabajoDN.validarVigenciaMinima(fechaInicioVigencia, fechaFinVigencia)){
			 if(cotizacionDTO.getClaveAutVigenciaMaxMin()==null || 
				    !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
        			 tipo=1;
        			 cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_REQUERIDA); 
        			 cotizacionDTO.setFechaSolAutVigenciaMaxMin(Calendar.getInstance().getTime());
			    }
		    }
		    //Vigencia Maxima
		    if(!OrdenTrabajoDN.validarVigenciaMaxima(fechaInicioVigencia, fechaFinVigencia)){
			 if(cotizacionDTO.getClaveAutVigenciaMaxMin()==null || 
				 !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
       			 	tipo=2;
       			 	cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_REQUERIDA);
       			 	cotizacionDTO.setFechaSolAutVigenciaMaxMin(Calendar.getInstance().getTime());
			   }
		    }
		    if(tipo==0){
			if(cotizacionDTO.getClaveAutVigenciaMaxMin()==null || 
				 !Sistema.CLAVES_AUTORIZADA_RECHAZADA.contains(cotizacionDTO.getClaveAutVigenciaMaxMin().toString())){
			    cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZACION_NO_REQUERIDA); 
			    cotizacionDTO.setFechaSolAutVigenciaMaxMin(null);
			}
		    }
		  modificar(cotizacionDTO);
		}
		return tipo;
	}

	
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	private boolean validarDocumentosComplementar(CotizacionDTO cotizacionDTO)
	throws SystemException {
		boolean esValido = true;
		DocPrevencionOperIlicitasDN docPrevencionDN = DocPrevencionOperIlicitasDN
		.getInstancia();
		List<DocPrevencionOperIlicitasDTO> documentos = docPrevencionDN
		.buscarPorPropiedad("idToCotizacion", cotizacionDTO.getIdToCotizacion());
		if (cotizacionDTO.getClaveAutoEmisionDocOperIlicitas() != null) {
			if (cotizacionDTO.getClaveAutoEmisionDocOperIlicitas() != Sistema.AUTORIZADA) {
				if (documentos == null || documentos.size() == 0) {
					esValido = false;
				}
			}

		}
		return esValido;
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public Double obtenerSumaAseguradaCotizacion(CotizacionDTO cotizacionDTO)
	throws ExcepcionDeAccesoADatos, SystemException {

		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN
		.getInstancia();
		List<IncisoCotizacionDTO> incisos = incisoCotizacionDN
		.listarPorCotizacionId(cotizacionDTO.getIdToCotizacion());
		SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
		Double sumaAsegurada = 0D;
		for (IncisoCotizacionDTO inciso : incisos) {
			List<SeccionCotizacionDTO> secciones = seccionCotizacionDN
			.listarSeccionesContratadas(inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso());
			for (SeccionCotizacionDTO seccion : secciones) {
				sumaAsegurada += seccion.getValorSumaAsegurada();

			}
		}
		return sumaAsegurada;
	}

	/*----------------   METODOS DE VALIDACION PARA SUMAS ASEGURADAS  ------------------*/

	/**
	 * Metodo que sirve para validar las sumas aseguradas de una cotizacion u orden de trabajo
	 * @param cotizacionForm
	 * @param cotizacionDTO
	 * @param esCotizacion : sirve para poder trabajar cuando se termina una orden o se libera una cotizacion.
	 * Valores esperados:
	 * <code>true</code>  cuando se trata de liberar una Cotizacion normal o de Endoso y 
	 * <code>false</code> cuando se trata de terminar una Orden de Trabajo
	 * @author CMM
	 */
	public boolean validadaSumasAseguradas(CotizacionForm cotizacionForm,CotizacionDTO cotizacionDTO, boolean esCotizacion) throws ExcepcionDeAccesoADatos, SystemException{
		String encabezadoError="La Orden de Trabajo NO fue terminada debido a que ";
		if(esCotizacion){
			encabezadoError="La cotizacion NO fue liberada debido a que ";
		}
		String icono = Sistema.ERROR;
		StringBuilder mensaje = new StringBuilder();
		StringBuilder mensajeError= new StringBuilder();
		List<String>mensajesCoberturas=new ArrayList<String>();
		//Se ejecuta el validador de Sumas Aseguradas contratadas
		validadorSumasAseguradas(cotizacionDTO,mensajeError,mensajesCoberturas);

		if(mensajeError!=null && mensajeError.toString().length()>0){
			//Excepciones relacionadas si en la Orden no se contrato ninguna 
			//seccion, del inciso o incisos que se presentaron del producto, asi como tambien
			//que no presente ninguna cobertura contratada de la seccion o secciones contratadas, del inciso o incisos
			icono = Sistema.INFORMACION;
			mensaje.append(encabezadoError);
			mensaje.append(mensajeError);
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());
			return false;
		}else{
			if(!mensajesCoberturas.isEmpty()){
				//Existieron sumas aseguradas en 0 de las coberturas basicas contratadas; despliega mensaje
				icono = Sistema.INFORMACION;
				mensaje.append(encabezadoError);
				mensaje.append("las siguientes coberturas tienen </br> sumas aseguradas en CEROS:<br> ");

				for(String mensajeCobertura:mensajesCoberturas){
					mensaje.append(mensajeCobertura);
				}
				cotizacionForm.setTipoMensaje(icono);
				cotizacionForm.setMensaje(mensaje.toString());
				return false;
			}
		}
		return true;
	}


	/**
	 * validadorSumasAseguradas 
	 * @param cotizacionDTO
	 * @param mensajeError
	 * @param coberturasEnCero
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 * @author CMM
	 */
	private void validadorSumasAseguradas(CotizacionDTO cotizacionDTO,StringBuilder mensajeError, List<String> coberturasEnCero)throws ExcepcionDeAccesoADatos, SystemException {
		String coberturaEnCero;
		try {
			List<CoberturaCotizacionDTO> listaCoberturasContratadas = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
			if(listaCoberturasContratadas != null && !listaCoberturasContratadas.isEmpty()){
				for(CoberturaCotizacionDTO cobertura : listaCoberturasContratadas){
					if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoSumaAsegurada().equals(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)) {
						coberturaEnCero = verificaSumaAseguradaPorCobertura(cobertura);
						if (coberturaEnCero != null) {
							coberturasEnCero.add(coberturaEnCero);
						}
					}
				}
			}else{
				mensajeError.append("NO tiene coberturas contratadas en ningun Inciso");
			}
//			// Primero se valida que en los incisos por lo menos uno tenga seccion contratada, y esta a su vez por lo menos una cobertura contratada
//			if (hayIncisosConSeccionCoberturaContradas(cotizacionDTO.getIncisoCotizacionDTOs())) {
//
//				for (IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()) {
//					for (SeccionCotizacionDTO seccion : inciso.getSeccionCotizacionList()) {
//						if (seccion.getClaveContrato() == Sistema.CONTRATADO) {
//							for (CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()) {
//								if (cobertura.getClaveContrato() == Sistema.CONTRATADO && cobertura.getCoberturaSeccionDTO().getCoberturaDTO().
//										getClaveTipoSumaAsegurada().equalsIgnoreCase(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)) {
//									coberturaEnCero = verificaSumaAseguradaPorCobertura(cobertura);
//									if (coberturaEnCero != null) {
//										coberturasEnCero.add(coberturaEnCero);
//									}
//								}
//							}
//						}
//					}
//				}
//			} else {
//				mensajeError.append("NO tiene secciones contratadas en ningun Inciso");
//			}
		} catch (IllegalArgumentException e) {
			mensajeError.append(e.getMessage());
		}
	}
	/**
	 * 
	 * @param incisos
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean hayIncisosConSeccionCoberturaContradas(List<IncisoCotizacionDTO> incisos) {
		if (incisos == null || incisos.isEmpty()) {
			throw new IllegalArgumentException("NO existen Incisos.");
		}
		for (IncisoCotizacionDTO inciso : incisos) {
			if (tieneSeccionesContratadas(inciso)) {
				for (SeccionCotizacionDTO seccion : inciso.getSeccionCotizacionList()) {
				    if(seccion.getClaveContrato()==Sistema.CONTRATADO){
					if (tieneCoberturasContratadas(seccion)) {
						return true;
					}
				    }
				}
			}
		}
		return false;
	}
	/**
	 * 
	 * @param inciso
	 * @return
	 */
	private boolean tieneSeccionesContratadas(IncisoCotizacionDTO inciso) {
		if (inciso.getSeccionCotizacionList() == null|| inciso.getSeccionCotizacionList().isEmpty()) {
			throw new IllegalArgumentException("NO existen secciones para el Inciso:"+ inciso.getId().getNumeroInciso() + ".");
		}
		for (SeccionCotizacionDTO seccion : inciso.getSeccionCotizacionList()) {
			if (seccion.getClaveContrato() == Sistema.CONTRATADO) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 
	 * @param seccion
	 * @return
	 */
	private boolean tieneCoberturasContratadas(SeccionCotizacionDTO seccion) {
		if (seccion.getCoberturaCotizacionLista() == null || seccion.getCoberturaCotizacionLista().isEmpty()) {
			throw new IllegalArgumentException("No existen coberturas para la seccion:"+ seccion.getSeccionDTO().getDescripcion()
					+ " del Inciso :"+ seccion.getId().getNumeroInciso() + ".");
		}
		for (CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()) {
			if (cobertura.getClaveContrato() == Sistema.CONTRATADO) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 
	 * @param cobertura
	 * @return
	 */
	private String verificaSumaAseguradaPorCobertura(CoberturaCotizacionDTO cobertura) {
		if (cobertura.getValorSumaAsegurada().equals(new Double(0d))) {
			StringBuilder mensaje = new StringBuilder();
			mensaje.append("*No. Inciso: "	+ cobertura.getId().getNumeroInciso());
			mensaje.append(" Seccion: "	+ cobertura.getSeccionCotizacionDTO().getSeccionDTO().getDescripcion());
			mensaje.append(" Cobertura: "+ cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion());
			mensaje.append("</br>");
			return mensaje.toString();
		} else {
			return null;
		}
	}
	/*---------------------------------------------------------------------------*/
	/**
	 * 
	 * @param cotizacionDTO
	 * @param usuario
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void copiarAnexos(CotizacionDTO cotizacionDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException {
		cotizacionDTO = this.getPorId(cotizacionDTO);
		BigDecimal idToProducto = cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto();
		DocumentoAnexoProductoDN documentoAnexoProductoDN = DocumentoAnexoProductoDN.getInstancia();
		List<DocumentoAnexoProductoDTO> listaAnexosProducto = documentoAnexoProductoDN.listarPorIdProducto(idToProducto);

		BigDecimal idToTipoPoliza = cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza();
		DocumentoAnexoTipoPolizaDN documentoAnexoTipoPolizaDN = DocumentoAnexoTipoPolizaDN.getInstancia();
		List<DocumentoAnexoTipoPolizaDTO> listaAnexosTipoPoliza = documentoAnexoTipoPolizaDN.listarPorIdTipoPoliza(idToTipoPoliza);

		List<DocumentoAnexoCoberturaDTO> listaAnexosCobertura = DocumentoAnexoCoberturaDN.getInstancia().listarAnexosPorCotizacion(cotizacionDTO.getIdToCotizacion());

		Date fechaActual = new Date();
		BigDecimal secuencia = new BigDecimal(1d);
		for (DocumentoAnexoProductoDTO documento : listaAnexosProducto) {
			DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
			docAnexoCotDTO.setCotizacionDTO(cotizacionDTO);
			docAnexoCotDTO.setId(new DocAnexoCotId());//(documento.getIdToControlArchivo());
			docAnexoCotDTO.getId().setIdToControlArchivo(documento.getIdToControlArchivo());
			docAnexoCotDTO.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			docAnexoCotDTO.setFechaCreacion(fechaActual);
			docAnexoCotDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
			docAnexoCotDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
			docAnexoCotDTO.setFechaModificacion(fechaActual);
			docAnexoCotDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
			docAnexoCotDTO.setNombreUsuarioModificacion(usuario.getNombreUsuario());
			docAnexoCotDTO.setDescripcionDocumentoAnexo(documento.getDescripcion());
			docAnexoCotDTO.setClaveObligatoriedad(documento.getClaveObligatoriedad());
			docAnexoCotDTO.setIdToCobertura(BigDecimal.ZERO);
			if(documento.getClaveObligatoriedad().intValue() == Sistema.DOCUMENTO_ANEXO_OBLIGATORIO.intValue()){
			    docAnexoCotDTO.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SELECCIONADO);
			}else{
			    docAnexoCotDTO.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SIN_SELECCIONAR);
			}
//			docAnexoCotDTO.setOrden(documento.getNumeroSecuencia());
			docAnexoCotDTO.setOrden(secuencia);
			secuencia = secuencia.add(BigDecimal.ONE);
			docAnexoCotDTO.setClaveTipo(Sistema.CLAVE_DOC_ANEXO_PRODUCTO);
			try {
				DocAnexoCotDN docAnexoCotDN = DocAnexoCotDN.getInstancia();
				docAnexoCotDN.agregar(docAnexoCotDTO);
			} catch (Exception e) {
				LogDeMidasWeb.log("ERROR AL GUARDAR DOCUMENTO_ANEXO_COTIZACION DE NIVEL PRODUCTO.", Level.SEVERE, e);
			}
		}
		for (DocumentoAnexoTipoPolizaDTO documento : listaAnexosTipoPoliza) {
			DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
			docAnexoCotDTO.setCotizacionDTO(cotizacionDTO);
			docAnexoCotDTO.setId(new DocAnexoCotId());//(documento.getIdToControlArchivo());
			docAnexoCotDTO.getId().setIdToControlArchivo(documento.getIdToControlArchivo());
			docAnexoCotDTO.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			docAnexoCotDTO.setFechaCreacion(fechaActual);
			docAnexoCotDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
			docAnexoCotDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
			docAnexoCotDTO.setFechaModificacion(fechaActual);
			docAnexoCotDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
			docAnexoCotDTO.setNombreUsuarioModificacion(usuario.getNombreUsuario());
			docAnexoCotDTO.setDescripcionDocumentoAnexo(documento.getDescripcion());
			docAnexoCotDTO.setClaveObligatoriedad(documento.getClaveObligatoriedad());
			docAnexoCotDTO.setIdToCobertura(BigDecimal.ZERO);
			if(documento.getClaveObligatoriedad().intValue() ==Sistema.DOCUMENTO_ANEXO_OBLIGATORIO.intValue()){
			    docAnexoCotDTO.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SELECCIONADO);
			}else{
			    docAnexoCotDTO.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SIN_SELECCIONAR);
			}
//			docAnexoCotDTO.setOrden(documento.getNumeroSecuencia());
			docAnexoCotDTO.setOrden(secuencia);
			secuencia = secuencia.add(BigDecimal.ONE);
			docAnexoCotDTO.setClaveTipo(Sistema.CLAVE_DOC_ANEXO_TIPO_POLIZA);
			try {
				DocAnexoCotDN.getInstancia().agregar(docAnexoCotDTO);
			} catch (Exception e) {
				LogDeMidasWeb.log("ERROR AL GUARDAR DOCUMENTO_ANEXO_COTIZACION DE NIVEL TIPO_POLIZA.", Level.SEVERE, e);
			}
		}
		for (DocumentoAnexoCoberturaDTO documento : listaAnexosCobertura) {
		    DocAnexoCotDTO docAnexoCotDTO = new DocAnexoCotDTO();
			docAnexoCotDTO.setCotizacionDTO(cotizacionDTO);
			docAnexoCotDTO.setId(new DocAnexoCotId());
			docAnexoCotDTO.getId().setIdToControlArchivo(documento.getIdToControlArchivo());
			docAnexoCotDTO.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			docAnexoCotDTO.setFechaCreacion(fechaActual);
			docAnexoCotDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
			docAnexoCotDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
			docAnexoCotDTO.setFechaModificacion(fechaActual);
			docAnexoCotDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
			docAnexoCotDTO.setNombreUsuarioModificacion(usuario.getNombreUsuario());
			docAnexoCotDTO.setDescripcionDocumentoAnexo(documento.getDescripcion());
			/**
			 * 07/01/2010
			 * No se respetar� la clave de obligatoriedad a nivel cobertura, debido a que actualmente no se sabe si est� contratada, queda pendiente
			 * Implementar la l�gica correcta.
			 * Jose Luis Arellano B�rcenas.
			 */
			if(documento.getCoberturaDTO().getIdToCobertura() != null){
				docAnexoCotDTO.setIdToCobertura(documento.getCoberturaDTO().getIdToCobertura());
			}else{
				docAnexoCotDTO.setIdToCobertura(BigDecimal.ZERO);
			}
			
			docAnexoCotDTO.setClaveObligatoriedad(documento.getClaveObligatoriedad());
//			docAnexoCotDTO.setClaveObligatoriedad((short)0);
//			if(documento.getClaveObligatoriedad()==Sistema.DOCUMENTO_ANEXO_OBLIGATORIO){
//			    docAnexoCotDTO.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SELECCIONADO);
//			}else{
			    docAnexoCotDTO.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SIN_SELECCIONAR);
//			}
//			docAnexoCotDTO.setOrden(documento.getNumeroSecuencia());
			docAnexoCotDTO.setOrden(secuencia);
			secuencia = secuencia.add(BigDecimal.ONE);
			docAnexoCotDTO.setClaveTipo(Sistema.CLAVE_DOC_ANEXO_COBERTURA);

			try {
				DocAnexoCotDN.getInstancia().agregar(docAnexoCotDTO);
			} catch (Exception e) {
				LogDeMidasWeb.log("ERROR AL GUARDAR DOCUMENTO_ANEXO_COTIZACION DE NIVEL COBERTURA.", Level.SEVERE, e);
			}
		}
	}
	/**
	 * 
	 * @param coberturasSinSA
	 * @param idToCotizacion
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public Boolean validarSumasAseguradasDeCoberturasContratadas(
			List<CoberturaCotizacionDTO> coberturasSinSA,
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {
		Boolean sonValidas = Boolean.TRUE;
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadas(idToCotizacion);
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			if (cobertura.getValorSumaAsegurada() <= 0) {
				CoberturaDTO coberturaDTO = coberturaDN.getPorId(cobertura
						.getId().getIdToCobertura());
				CoberturaSeccionDTO coberturaSeccionDTO = cobertura.getCoberturaSeccionDTO();
				if (coberturaSeccionDTO == null) {
					coberturaSeccionDTO = new CoberturaSeccionDTO();
				}
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
				coberturasSinSA.add(cobertura);
				sonValidas = Boolean.FALSE;
			}
		}
		return sonValidas;
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<BigDecimal> obtenerRiesgosContratadosCotizacion(CotizacionDTO cotizacionDTO) throws SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN
		.obtenerRiesgosContratadosCotizacion(cotizacionDTO
				.getIdToCotizacion());
	}

	/**
	 * Genera una nueva cotizacion a partir de una solicitud de endoso
	 * 
	 * @param idCotizacion
	 *            Id de la cotizacion asociada al ultimo endoso de la poliza
	 * @param solicitudEndoso
	 *            Solicitud del endoso
	 * @param nombreUsuarioCotizacion
	 *            Codigo del usuario al que se asignara la cotizacion
	 * @return La nueva cotizacion con la solicitud asociada
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public CotizacionDTO generarCotizacionASolicitud(BigDecimal idCotizacion,
			SolicitudDTO solicitudEndoso, String nombreUsuarioCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		CotizacionDTO cotizacionNueva = new CotizacionSN(CotizacionDN.nombreUsuario).generarCotizacionASolicitud(idCotizacion, solicitudEndoso, nombreUsuarioCotizacion);
		try{
		if (cotizacionNueva.getArchivosPendientes() != null){
			for(ArchivosPendientes pendiente : cotizacionNueva.getArchivosPendientes()) {
				VaultAction.copyFile(pendiente.getArchivoOriginal(), pendiente.getArchivoNuevo());
			}
		}
		}catch(Exception e){}
		return cotizacionNueva;
	}
	/**
	 * 
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public CotizacionDTO obtenerCotizacionCompleta(BigDecimal idToCotizacion)throws SystemException {
		CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);
		return cotizacionSN.obtenerCotizacionCompleta(idToCotizacion);
	}
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<SubRamoDTO> listarSubRamosContratadosPorCotizacion(CotizacionDTO cotizacionDTO) throws SystemException{
		List<CoberturaCotizacionDTO> listaCoberturaCotContratadas = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		List<SubRamoDTO> listaSubRamosContratados = new ArrayList<SubRamoDTO>();
		if (listaCoberturaCotContratadas != null) {
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturaCotContratadas){
				if (!listaSubRamosContratados.contains(coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO())) {
					listaSubRamosContratados.add(coberturaCot.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO());
				}
			}
		}
		return listaSubRamosContratados;
	}
	
	/**
	 * 
	 * @param cotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public List<SubRamoDTO> listarSubRamosEndoso(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		return coberturaCotizacionDN.listarSubRamosEndoso(cotizacionDTO.getIdToCotizacion());		
	}
	
	/**
	 * 	
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public Double getPrimaNetaCotizacion(BigDecimal idToCotizacion)
			throws SystemException {
		CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);
		return cotizacionSN.getPrimaNetaCotizacion(idToCotizacion);
	}
	
	//**---------------   METODOS DE SOPORTE PARA FACULTATIVO ----------------------**/
	@Override
	public void validarReaseguroFacultativo(BigDecimal idToCotizacion,String nombreUsuario,CotizacionForm cotizacionForm,Map<String,List<LineaSoporteReaseguroDTO>> mapaLineas) throws ExcepcionDeAccesoADatos, SystemException {
	    super.validarReaseguroFacultativo(idToCotizacion, nombreUsuario, cotizacionForm,mapaLineas);
	}
	
	public void validarReaseguroFacultativoEndoso(BigDecimal idToCotizacion,String nombreUsuario,CotizacionForm cotizacionForm,Map<String,List<LineaSoporteReaseguroDTO>> mapaListaCumulos) throws SystemException {
	    super.validarReaseguroFacultativoEndosoV2(idToCotizacion, nombreUsuario,cotizacionForm,mapaListaCumulos,true);
	}
	
	 public Boolean igualarPrimasFacultadas(List<CoberturaCotizacionDTO> coberturas,
			Double primaNeta, String nombreUsuario) throws SystemException {
		this.recalcularCoberturasFacultativo(coberturas, nombreUsuario);
		
		Map<CoberturaCotizacionId,BigDecimal> mapaPrimaARDTPorCobertura = new HashMap<CoberturaCotizacionId, BigDecimal>();
		Map<CoberturaCotizacionId,BigDecimal> mapaPrimaPorCobertura = new HashMap<CoberturaCotizacionId, BigDecimal>();
		
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal totalARDT = BigDecimal.ZERO;
		BigDecimal primaModificar = BigDecimal.ZERO;
		BigDecimal primaFueraIgualacion = BigDecimal.ZERO;
		BigDecimal primaIgualar = BigDecimal.ZERO;
//				BigDecimal factorIgualacion = BigDecimal.ZERO;
		
		//____________________________________________________________________
		LogDeMidasWeb.log(" AAC IGUALAR PRIMAS -- primaNeta = " + primaNeta,Level.INFO, null);
		//____________________________________________________________________
		
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			
			BigDecimal primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			
			if(primaARDT.compareTo(BigDecimal.ZERO) == 0){
				//Si la prima es cero, se manda recalcular la cobertura debido a que los detalles de prima son inconsistentes.
				CalculoCotizacionDN.getInstancia().calcularCobertura(cobertura, nombreUsuario, true);
				
				primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			}
			
			mapaPrimaARDTPorCobertura.put(cobertura.getId(), primaARDT);
			
			totalARDT = totalARDT.add(primaARDT);

			BigDecimal prima = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId()));
			mapaPrimaPorCobertura.put(cobertura.getId(), prima);
			total = total.add(prima);
			
			primaModificar = primaModificar.add(prima);
		}
		
		primaFueraIgualacion = total.subtract(primaModificar);
		
		primaIgualar = new BigDecimal(primaNeta).subtract(primaFueraIgualacion);

		//En la igualacion por facultativo se permite cualquier monto.
//				if(new BigDecimal(primaNeta).compareTo(primaFueraIgualacion) <= 0){
//					return Boolean.FALSE;
//				}
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			BigDecimal proporcion = BigDecimal.ZERO;
			BigDecimal primaAQuedar = BigDecimal.ZERO;
			BigDecimal porcentajeNecesario = BigDecimal.ZERO;
			BigDecimal otrosARDs= BigDecimal.ZERO;
			BigDecimal porcentajeAplicable = BigDecimal.ZERO;
			
			//Si la cobertura permite igualacion
			BigDecimal prima = mapaPrimaPorCobertura.get(cobertura.getId());
			if(prima == null){
				prima = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaCoberturaCotizacion(cobertura.getId()));
			}
			
			BigDecimal primaARDT = mapaPrimaARDTPorCobertura.get(cobertura.getId());
			if(primaARDT == null){
				primaARDT = new BigDecimal(DetallePrimaCoberturaCotizacionDN.getInstancia().sumPrimaNetaARDTCoberturaCotizacion(cobertura.getId()));
			}
			
			proporcion = prima.divide(primaModificar, 12, RoundingMode.HALF_UP);
			
			primaAQuedar = primaIgualar.multiply(proporcion);
			
			porcentajeNecesario = BigDecimal.ONE.subtract(primaAQuedar.divide(primaARDT, 12, RoundingMode.HALF_UP));
			
			List<RecargoRiesgoCotizacionDTO> listaRecargos = coberturaCotizacionDN.listarRecargosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
			List<DescuentoRiesgoCotizacionDTO> listaDescuentos = coberturaCotizacionDN.listarDescuentosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
			List<AumentoRiesgoCotizacionDTO> listaAumentos = coberturaCotizacionDN.listarAumentosRiesgoPorCoberturaCotizacion(cobertura, null, Boolean.FALSE);
			
			List<BigDecimal> listaAumentosAcumulados = new ArrayList<BigDecimal>();
			List<BigDecimal> listaRecargosAcumulados = new ArrayList<BigDecimal>();
			List<BigDecimal> listaDescuentosAcumulados = new ArrayList<BigDecimal>();
			
//						BigDecimal otrosARDS = BigDecimal.ZERO;
			BigDecimal sumatoriaRecargos = BigDecimal.ZERO;
			BigDecimal sumatoriaDescuentos = BigDecimal.ZERO;
			BigDecimal sumatoriaAumentos = BigDecimal.ZERO;
			
			for(RecargoRiesgoCotizacionDTO recargo : listaRecargos){
				if(recargo.getId().getIdToRecargoVario().compareTo(Sistema.RECARGO_REASEGURO_FACULTATIVO) != 0 &&
						recargo.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
					//Se excluyen recargos por igualaci�n
					
					if(!listaRecargosAcumulados.contains(recargo.getId().getIdToRecargoVario())){
						//Cada recargo s�lo se incluye una vez en el acumulado.
						listaRecargosAcumulados.add(recargo.getId().getIdToRecargoVario());
						sumatoriaRecargos = sumatoriaRecargos.add(new BigDecimal(recargo.getValorRecargo()));
					}
					
				}
			}
			
			for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
				if(descuento.getId().getIdToDescuentoVario().compareTo(Sistema.DESCUENTO_IGUALACION) != 0 &&
						descuento.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
					//Se excluyen recargos por igualaci�n
					
					if(!listaDescuentosAcumulados.contains(descuento.getId().getIdToDescuentoVario())){
						//Cada recargo s�lo se incluye una vez en el acumulado.
						listaDescuentosAcumulados.add(descuento.getId().getIdToDescuentoVario());
						sumatoriaDescuentos = sumatoriaDescuentos.add(new BigDecimal(descuento.getValorDescuento()));
					}
				}
			}
			
			for(AumentoRiesgoCotizacionDTO aumento : listaAumentos){
				if(aumento.getClaveContrato().compareTo(Sistema.CONTRATADO) == 0){
					//No se registran aumentos por igualaci�n, no se valida el tipo de aumento
					
					if(!listaAumentosAcumulados.contains(aumento.getId().getIdToAumentoVario())){
						//Cada recargo s�lo se incluye una vez en el acumulado.
						listaAumentosAcumulados.add(aumento.getId().getIdToAumentoVario());
						sumatoriaAumentos = sumatoriaAumentos.add(new BigDecimal(aumento.getValorAumento()));
					}
				}
			}
			
			otrosARDs = sumatoriaAumentos.add(sumatoriaRecargos).subtract(sumatoriaDescuentos);
			
			porcentajeAplicable = otrosARDs.add(porcentajeNecesario.multiply(new BigDecimal("100")));
			
			List<DescuentoRiesgoCotizacionDTO> listaDescuentoRiesgoIgualacion = obtenerDescuentoEspecial(listaDescuentos, Sistema.DESCUENTO_REASEGURO_FACULTATIVO);
			List<RecargoRiesgoCotizacionDTO> listaRecargoRiesgoIgualacion = obtenerRecargoEspecial(listaRecargos, Sistema.RECARGO_REASEGURO_FACULTATIVO);
			
			if(porcentajeAplicable.compareTo(BigDecimal.ZERO) > 0){
				//se aplica descuento
				for(DescuentoRiesgoCotizacionDTO descuentoRiesgoIgualacion : listaDescuentoRiesgoIgualacion){
					descuentoRiesgoIgualacion.setValorDescuento(porcentajeAplicable.abs().doubleValue());
					descuentoRiesgoIgualacion.setClaveContrato((short) 1);
					DescuentoRiesgoCotizacionDN.getInstancia().modificar(descuentoRiesgoIgualacion);
				}
				for(RecargoRiesgoCotizacionDTO recargoRiesgoIgualacion : listaRecargoRiesgoIgualacion){
					recargoRiesgoIgualacion.setValorRecargo(0d);
					recargoRiesgoIgualacion.setClaveContrato((short) 0);
					RecargoRiesgoCotizacionDN.getInstancia().modificar(recargoRiesgoIgualacion);
				}
				
			}
			else if (porcentajeAplicable.compareTo(BigDecimal.ZERO) < 0){
				//se aplica recargo
				for(DescuentoRiesgoCotizacionDTO descuentoRiesgoIgualacion : listaDescuentoRiesgoIgualacion){
					descuentoRiesgoIgualacion.setValorDescuento(0d);
					descuentoRiesgoIgualacion.setClaveContrato((short) 0);
					DescuentoRiesgoCotizacionDN.getInstancia().modificar(descuentoRiesgoIgualacion);
				}
				for(RecargoRiesgoCotizacionDTO recargoRiesgoIgualacion : listaRecargoRiesgoIgualacion){
					recargoRiesgoIgualacion.setValorRecargo(porcentajeAplicable.abs().doubleValue());
					recargoRiesgoIgualacion.setClaveContrato((short) 1);
					RecargoRiesgoCotizacionDN.getInstancia().modificar(recargoRiesgoIgualacion);
				}
			}
			
		}
		
		for(CoberturaCotizacionDTO cobertura : coberturas) {
			CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
			calculoCotizacionDN.calcularARD(cobertura.getId(), nombreUsuario);
		}
		
		
		return Boolean.TRUE;
	 }
	 
	@Override
	public void recalcularCoberturasFacultativo(List<CoberturaCotizacionDTO> coberturas, String nombreUsuario)
		throws SystemException {
	     super.recalcularCoberturasFacultativo(coberturas, nombreUsuario);
	 }
	 
//	 public Boolean autorizacionEmitirReaseguroFacultativo(CotizacionDTO cotizacionDTO){
//	    return super.autorizacionEmitirReaseguroFacultativo(cotizacionDTO);
//	 }
	
	 /*
	  * 
	  *	METODOS AGREGADOS PARA LIMPIAR LAS TABLAS DE TODETPRIMACOBERTURACOT Y TODETPRIMACONERTURARIESGO , YA
	  *	SEA CUANDO SE ELIMINA UN INCISO, CUANDO SE DESCONTRATA UNA SECCION, SE ELIMINA UN SUBINCISO,
	  *	SE DESCONTRATA UNA COBERTURA O BIEN SE DESCONTRATA UN RIESGO
	  */

	/**
	 * Method eliminaDetallesPrimasDeCoberturayRiesgoPorInciso()
	 * @param incisoCotizacionDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	 public void eliminaDetallesPrimasDeCoberturayRiesgoPorInciso(IncisoCotizacionDTO incisoCotizacionDTO) 
	 throws ExcepcionDeAccesoADatos, SystemException{
	       DetallePrimaCoberturaCotizacionDN detallePrimaCoberturaCotizacionDN=DetallePrimaCoberturaCotizacionDN.getInstancia();
	       DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId= new DetallePrimaCoberturaCotizacionId();
	       detallePrimaCoberturaCotizacionId.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaCoberturaCotizacionId.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaCoberturaCotizacionDN.borrarFiltrado(detallePrimaCoberturaCotizacionId);
	      
	       DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN= DetallePrimaRiesgoCotizacionDN.getInstancia();
	       DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId= new DetallePrimaRiesgoCotizacionId();
	       detallePrimaRiesgoCotizacionId.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaRiesgoCotizacionId.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaRiesgoCotizacionDN.borrarFiltrado(detallePrimaRiesgoCotizacionId);
	 }
	 
	 /**
	  * Method eliminaDetallesPrimasDeCoberturayRiesgoPorSeccion()
	  * @param seccionCotizacionDTO
	  * @throws ExcepcionDeAccesoADatos
	  * @throws SystemException
	  */
	 public void eliminaDetallesPrimasDeCoberturayRiesgoPorSeccion(SeccionCotizacionDTO seccionCotizacionDTO) 
	 throws ExcepcionDeAccesoADatos, SystemException{
	       DetallePrimaCoberturaCotizacionDN detallePrimaCoberturaCotizacionDN=DetallePrimaCoberturaCotizacionDN.getInstancia();
	       DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId= new DetallePrimaCoberturaCotizacionId();
	       detallePrimaCoberturaCotizacionId.setIdToCotizacion(seccionCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaCoberturaCotizacionId.setNumeroInciso(seccionCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaCoberturaCotizacionId.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaCoberturaCotizacionDN.borrarFiltrado(detallePrimaCoberturaCotizacionId);
	      
	       DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN= DetallePrimaRiesgoCotizacionDN.getInstancia();
	       DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId= new DetallePrimaRiesgoCotizacionId();
	       detallePrimaRiesgoCotizacionId.setIdToCotizacion(seccionCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaRiesgoCotizacionId.setNumeroInciso(seccionCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaRiesgoCotizacionId.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaRiesgoCotizacionDN.borrarFiltrado(detallePrimaRiesgoCotizacionId);
	 }
	 /**
	  * Method eliminaDetallesPrimasDeCoberturayRiesgoPorSubInciso().
	  * @param subIncisoCotizacionDTO
	  * @throws ExcepcionDeAccesoADatos
	  * @throws SystemException
	  */
	 public void eliminaDetallesPrimasDeCoberturayRiesgoPorSubInciso(SubIncisoCotizacionDTO subIncisoCotizacionDTO) 
	 throws ExcepcionDeAccesoADatos, SystemException{
	       DetallePrimaCoberturaCotizacionDN detallePrimaCoberturaCotizacionDN=DetallePrimaCoberturaCotizacionDN.getInstancia();
	       DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId= new DetallePrimaCoberturaCotizacionId();
	       detallePrimaCoberturaCotizacionId.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaCoberturaCotizacionId.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaCoberturaCotizacionId.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaCoberturaCotizacionId.setNumeroSubInciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
	       detallePrimaCoberturaCotizacionDN.borrarFiltrado(detallePrimaCoberturaCotizacionId);
	       
	       DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN= DetallePrimaRiesgoCotizacionDN.getInstancia();
	       DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId= new DetallePrimaRiesgoCotizacionId();
	       detallePrimaRiesgoCotizacionId.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaRiesgoCotizacionId.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaRiesgoCotizacionId.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaRiesgoCotizacionId.setNumeroSubInciso( subIncisoCotizacionDTO.getId().getNumeroSubInciso());
	       detallePrimaRiesgoCotizacionDN.borrarFiltrado(detallePrimaRiesgoCotizacionId);
	 }
	 
	 
	 /**
	  * Method eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura().
	  * @param coberturaCotizacionDTO
	  * @throws ExcepcionDeAccesoADatos
	  * @throws SystemException
	  */
	 public void eliminaDetallesPrimasDeCoberturayRiesgoPorCobertura(CoberturaCotizacionDTO coberturaCotizacionDTO) 
	 throws ExcepcionDeAccesoADatos, SystemException{
	       DetallePrimaCoberturaCotizacionDN detallePrimaCoberturaCotizacionDN=DetallePrimaCoberturaCotizacionDN.getInstancia();
	       DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId= new DetallePrimaCoberturaCotizacionId();
	       detallePrimaCoberturaCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaCoberturaCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaCoberturaCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaCoberturaCotizacionId.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
	       detallePrimaCoberturaCotizacionDN.borrarFiltrado(detallePrimaCoberturaCotizacionId);
	       
	       DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN= DetallePrimaRiesgoCotizacionDN.getInstancia();
	       DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId= new DetallePrimaRiesgoCotizacionId();
	       detallePrimaRiesgoCotizacionId.setIdToCotizacion(coberturaCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaRiesgoCotizacionId.setNumeroInciso(coberturaCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaRiesgoCotizacionId.setIdToSeccion(coberturaCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaRiesgoCotizacionId.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
	       detallePrimaRiesgoCotizacionDN.borrarFiltrado(detallePrimaRiesgoCotizacionId);
	 }
	 /**
	  * Method eliminaDetallesPrimasDeRiesgo().
	  * @param riesgoCotizacionDTO
	  * @throws ExcepcionDeAccesoADatos
	  * @throws SystemException
	  */
	 public void eliminaDetallesPrimasDeRiesgo(RiesgoCotizacionDTO riesgoCotizacionDTO) 
	 throws ExcepcionDeAccesoADatos, SystemException{
	              
	       DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN= DetallePrimaRiesgoCotizacionDN.getInstancia();
	       DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId= new DetallePrimaRiesgoCotizacionId();
	       detallePrimaRiesgoCotizacionId.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
	       detallePrimaRiesgoCotizacionId.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());
	       detallePrimaRiesgoCotizacionId.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
	       detallePrimaRiesgoCotizacionId.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
	       detallePrimaRiesgoCotizacionId.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
	       detallePrimaRiesgoCotizacionDN.borrarFiltrado(detallePrimaRiesgoCotizacionId);
	 }
	
	 
	 /**
		 * Valida que todas las coberturas que aplicaron a LUC tengan almacenado correctamente el n�mero de agrupaci�n correspondiente.
		 * @param cotizacionForm
		 * @param cotizacionDTO
		 */
		public boolean validarCoberturasLUC(CotizacionForm cotizacionForm,CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
			
			List<AgrupacionCotDTO> listaAgrupacionesLUCMalRegistradas = 
				PrimerRiesgoLUCDN.getInstancia().listarAgrupacionesLUCMalRegistradas(cotizacionDTO.getIdToCotizacion());
			
			StringBuilder mensaje =  new StringBuilder("");
			if(listaAgrupacionesLUCMalRegistradas != null && !listaAgrupacionesLUCMalRegistradas.isEmpty()){
				mensaje.append("El LUC fue registrado de forma inadecuada para las siguientes secciones. ").append("Por favor, apl&iacute;calo nuevamente.</br>").append("<ul>");
				for(AgrupacionCotDTO agrupacion : listaAgrupacionesLUCMalRegistradas){
					SeccionDTO seccion = SeccionDN.getInstancia().getPorId(agrupacion.getIdToSeccion());										
					mensaje.append("<li> ").append(seccion.getNombreComercial());										
				}
				mensaje.append("</ul>");
				String icono = Sistema.INFORMACION;				
				cotizacionForm.setTipoMensaje(icono);
				cotizacionForm.setMensaje(mensaje.toString());
				return false;
			}
			return true;
		} 		
		
		/**
		 * Valida que no se de alta/baja de LUC/Primer Riesgo en cotizaciones de endoso.
		 * @param cotizacionForm
		 * @param cotizacionDTO
		 */
		public boolean validarAltaBajaAgrupaciones(CotizacionForm cotizacionForm,CotizacionDTO cotizacionEndosoActual) 
		throws ExcepcionDeAccesoADatos, SystemException{						
			BigDecimal idToPoliza = cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada();
			EndosoDTO endosoAnterior = EndosoDN.getInstancia(null).getUltimoEndoso(idToPoliza);			
			
			List<AgrupacionCotDTO> listaAgrupacionesAlta = 
				PrimerRiesgoLUCDN.getInstancia().listarAgrupacionesExcluyentes(cotizacionEndosoActual.getIdToCotizacion(), 
																			   endosoAnterior.getIdToCotizacion());
			
			List<AgrupacionCotDTO> listaAgrupacionesBaja = 
				PrimerRiesgoLUCDN.getInstancia().listarAgrupacionesExcluyentes(endosoAnterior.getIdToCotizacion(), 
						          											   cotizacionEndosoActual.getIdToCotizacion());
			
			StringBuilder mensaje = new StringBuilder("");
			if(listaAgrupacionesAlta != null && !listaAgrupacionesAlta.isEmpty() || 
			   listaAgrupacionesBaja != null && !listaAgrupacionesBaja.isEmpty()){
				mensaje.append("Los siguientes movimientos son inv�lidos en el endoso: ").append("<ul>");
				for(AgrupacionCotDTO agrupacion : listaAgrupacionesAlta){					
					if (agrupacion.getClaveTipoAgrupacion() == Sistema.NUM_AGRUPACION_PRIMER_RIESGO){
						mensaje.append("<li> Alta de Primer Riesgo");	
					}else{
						SeccionDTO seccion = SeccionDN.getInstancia().getPorId(agrupacion.getIdToSeccion());
						mensaje.append("<li> Alta de LUC para ").append(seccion.getNombreComercial());						
					} 
				}
				
				for(AgrupacionCotDTO agrupacion : listaAgrupacionesBaja){					
					if (agrupacion.getClaveTipoAgrupacion() == Sistema.NUM_AGRUPACION_PRIMER_RIESGO){
						mensaje.append("<li> Baja de Primer Riesgo");	
					}else{
						SeccionDTO seccion = SeccionDN.getInstancia().getPorId(agrupacion.getIdToSeccion());
						mensaje.append("<li> Baja de LUC para ").append(seccion.getNombreComercial());						
					} 
				}
				mensaje.append("</ul>");
				String icono = Sistema.INFORMACION;				
				cotizacionForm.setTipoMensaje(icono);
				cotizacionForm.setMensaje(mensaje.toString());
				return false;
			}
			return true;
		}	
		
		//Calcula la prima neta de una cotizaci�n
		public Double calcularPrimaNetaCotizacion(CotizacionDTO cotizacionDTO)throws SystemException{
			Double primaNetaCotizacion = 0D;
			BigDecimal factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
			if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
					cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
				primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion());							
				cotizacionDTO.setDiasPorDevengar(365D);
			}else{
				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
				primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()) * factor.doubleValue();
			}
			
			return primaNetaCotizacion;
		}
		
		public Double calcularPrimaNetaCotizacionEndoso(CotizacionDTO cotizacionEndosoActual)throws SystemException{
			BigDecimal idToPoliza = cotizacionEndosoActual.getSolicitudDTO().getIdToPolizaEndosada();
			
			EndosoDTO endosoAnterior;
			EndosoDTO endosoActual = EndosoDN.getInstancia(null).buscarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
			
			if (endosoActual != null) {
				endosoAnterior = new CancelacionEndosoSN().obtieneEndosoAnterior(endosoActual.getId());
			} else {
				endosoAnterior = EndosoDN.getInstancia(null).getUltimoEndoso(idToPoliza);
			}
			
			BigDecimal factor = UtileriasWeb.getFactorVigencia(cotizacionEndosoActual.getFechaInicioVigencia(), cotizacionEndosoActual.getFechaFinVigencia());
			
			Double primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion());	
			Double primaNetaCotizacionBase = this.getPrimaNetaCotizacion(endosoAnterior.getIdToCotizacion());
									
			primaNetaCotizacion = (primaNetaCotizacion - primaNetaCotizacionBase) * factor.doubleValue();
						
			double diasPorDevengar = UtileriasWeb.obtenerDiasEntreFechas(cotizacionEndosoActual.getFechaInicioVigencia(), cotizacionEndosoActual.getFechaFinVigencia());			
							
			EndosoDTO endosoEmitido = EndosoDN.getInstancia(cotizacionEndosoActual.getCodigoUsuarioModificacion())
			.buscarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
			
			if(cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
				primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D ) * -1D;
			}else if(cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){
				primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D );
			}else if(cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				primaNetaCotizacion = this.getPrimaNetaCotizacion(cotizacionEndosoActual.getIdToCotizacion()) * (diasPorDevengar / 365D );	
			} else if (endosoEmitido!=null && (endosoEmitido.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CE
					|| endosoEmitido.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_RE)
					&& primaNetaCotizacion < 0) {				
				primaNetaCotizacion = primaNetaCotizacion * -1D;
			}
			
			return primaNetaCotizacion;
		}
		
		//Calcula los derechos de una cotizaci�n 
		public Double calcularDerechosCotizacion(CotizacionDTO cotizacionDTO, boolean calculoPoliza)
		throws SystemException {
			BigDecimal primaNetaCotizacion = BigDecimal.ZERO;			
			Double derechos = 0D;
			
			if (!calculoPoliza && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0) {
				primaNetaCotizacion = BigDecimal.valueOf(calcularPrimaNetaCotizacionEndoso(cotizacionDTO));
			} else {
				primaNetaCotizacion = BigDecimal.valueOf(calcularPrimaNetaCotizacion(cotizacionDTO));
			}
			
			GastoExpedicionDTO gasto = GastoExpedicionDN.getInstancia().getGastoExpedicion(cotizacionDTO
					.getIdMoneda(), primaNetaCotizacion.abs());	
			if(gasto != null){
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
					
					EndosoDTO endosoValidacion = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion())
					.buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
				
					if (endosoValidacion != null && endosoValidacion.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CE) {					
						derechos = gasto.getGastoExpedicionEndosoAumento() * -1D;					
					}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){						
						derechos = gasto.getGastoExpedicionPoliza();		
					}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
						/*EndosoDTO endoso = EndosoDN
								.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion())
								.getPorId(new EndosoId(cotizacionDTO
												.getSolicitudDTO()
												.getIdToPolizaEndosada(), (short) 0));

						if(cotizacionDTO.getFechaInicioVigencia().compareTo(endoso.getFechaInicioVigencia())== 0){
							derechos = gasto.getGastoExpedicionPoliza() * -1D;								
						}	
						*/
						//Se regresan los derechos de cada endosos, s�lo si el endoso en cuesti�n no tiene recibos pagados
						//Se toman encuenta todos los endosos de la p�liza, a partir del �ltimo endoso de CFP emitido.
						EndosoDN endosoDN = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion());
						BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
						List<EndosoDTO> endosos = endosoDN.listarEndososPorPoliza(idToPoliza, false);
						for(EndosoDTO endoso : endosos){
							if(!endosoDN.primerReciboPagado(endoso)) {
								derechos += endoso.getValorDerechos() * -1D;
							}
							
							if(endoso.getClaveTipoEndoso().compareTo(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0) {
								break;
							}
						}						
					}else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO) {
						if(!PolizaDN.getInstancia().existeReciboPagado(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada())) {
							;
						}
							derechos = gasto.getGastoExpedicionPoliza();
					}else if(primaNetaCotizacion.doubleValue() > 0){//Si es endoso de modificaci�n con prima positiva = Endoso de Aumento
						derechos = gasto.getGastoExpedicionEndosoAumento();
					}	
				}else{
					derechos = gasto.getGastoExpedicionPoliza();
				}
			}
			
			return derechos;
		}
		
		//Obtiene el porcentaje de recargo por pago fraccionado de una cotizaci�n de acuerdo a la moneda y la forma de pago.
		public Double obtenerPorcentajeRecargoPagoFraccionadoCotizacion(CotizacionDTO cotizacionDTO)
		throws SystemException {
			FormaPagoIDTO formaPago = FormaPagoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPorId(cotizacionDTO.getIdFormaPago().intValue(),cotizacionDTO.getIdMoneda().shortValue());
			if(formaPago != null) {
				return formaPago.getPorcentajeRecargoPagoFraccionado().doubleValue();
			} else {
				return null;
			}
		}
		
		//Calcula el porcentaje de recargo por pago fraccionado de una cotizaci�n a partir de un monto espec�fico de RPF y de la prima neta de la cotizaci�n.  
		public Double calcularPorcentajeRPF(CotizacionDTO cotizacionDTO, boolean calculoPoliza)throws SystemException{
			Double primaNetaCotizacion = 0D;
			if (!calculoPoliza && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0) {
				primaNetaCotizacion = calcularPrimaNetaCotizacionEndoso(cotizacionDTO);
			} else {
				primaNetaCotizacion = calcularPrimaNetaCotizacion(cotizacionDTO);
			}
			
			if(primaNetaCotizacion==0) {
				return -1D;
			}
			
			Double valorRPF = cotizacionDTO.getValorRecargoPagoFraccionadoUsuario();
			return valorRPF/primaNetaCotizacion * 100;
		}
		
		//Calcula el valor del recargo por pago fraccionado de una cotizaci�n a partir de un porcentaje espec�fico de RPF y de la prima neta de la cotizaci�n.
		public Double calcularValorRPF(CotizacionDTO cotizacionDTO, boolean calculoPoliza)throws SystemException{
			Double primaNetaCotizacion = 0D;
			if (!calculoPoliza && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0) {
				primaNetaCotizacion = calcularPrimaNetaCotizacionEndoso(cotizacionDTO);
			} else {
				primaNetaCotizacion = calcularPrimaNetaCotizacion(cotizacionDTO);
			}
			
			Double porcentajeRPF = cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario();
			return primaNetaCotizacion * porcentajeRPF / 100;
		}

		//Modifica los derechos y regresa la cotizaci�n modificada
		public CotizacionDTO modificarDerechos(BigDecimal idToCotizacion, Short claveDerechosUsuario, Double valorDerechos, boolean calculoPoliza, boolean sinAutorizacion)
		throws ExcepcionDeAccesoADatos, SystemException {
			CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);
			CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(idToCotizacion);
						
			cotizacionDTO.setClaveDerechosUsuario(claveDerechosUsuario);						
			
			if(claveDerechosUsuario.compareTo(Sistema.CLAVE_DERECHOS_SISTEMA) == 0){
				cotizacionDTO.setValorDerechosUsuario(CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO,calculoPoliza));
				cotizacionDTO.setClaveAutorizacionDerechos(Sistema.AUTORIZACION_NO_REQUERIDA);
				cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(null);
				cotizacionDTO.setFechaAutorizacionDerechos(null);
			}else{			
				
				if(cotizacionDTO.getValorDerechosUsuario() == null) {
					cotizacionDTO.setValorDerechosUsuario(0D);
				}
				
				DecimalFormat df10dec = new DecimalFormat("#.##########");				
				
				double valorDerechosAnterior = Double.valueOf(df10dec.format(cotizacionDTO.getValorDerechosUsuario()));							
				double valorDerechosActual = Double.valueOf(df10dec.format(valorDerechos));				
				
				if(valorDerechosAnterior != valorDerechosActual){
					cotizacionDTO.setValorDerechosUsuario(valorDerechos);
					cotizacionDTO.setClaveAutorizacionDerechos(Sistema.AUTORIZACION_REQUERIDA);
					cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(null);
					cotizacionDTO.setFechaAutorizacionDerechos(null);
				}	
			}	
			
			if(sinAutorizacion){
				cotizacionDTO.setClaveAutorizacionDerechos(Sistema.AUTORIZACION_NO_REQUERIDA);
				cotizacionDTO.setCodigoUsuarioAutorizacionDerechos(null);
				cotizacionDTO.setFechaAutorizacionDerechos(null);
			}
			
			cotizacionSN.modificar(cotizacionDTO);			
			return cotizacionSN.getPorId(idToCotizacion);
		}

		//Modifica el recargo por pago fraccionado y regresa la cotizaci�n modificada		
		public CotizacionDTO modificarRecargoPagoFraccionado(BigDecimal idToCotizacion, Short claveRecargoPagoFraccionadoUsuario, Double valorRecargoPagoFraccionado, boolean calculoPoliza, boolean sinAutorizacion)
		throws ExcepcionDeAccesoADatos, SystemException {
			CotizacionSN cotizacionSN = new CotizacionSN(CotizacionDN.nombreUsuario);
			CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(idToCotizacion);
						
			cotizacionDTO.setClaveRecargoPagoFraccionadoUsuario(claveRecargoPagoFraccionadoUsuario);	
			
			double valorRPFAnterior = cotizacionDTO.getValorRecargoPagoFraccionadoUsuario() == null ? 0 : cotizacionDTO.getValorRecargoPagoFraccionadoUsuario().doubleValue();
			double porcentajeRPFAnterior = cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario() == null ? 0 : cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario().doubleValue();
			
			if(claveRecargoPagoFraccionadoUsuario.compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0){
				cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(this.obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO));
				cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(this.calcularValorRPF(cotizacionDTO, calculoPoliza));
			}else if(claveRecargoPagoFraccionadoUsuario.compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){
				cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(valorRecargoPagoFraccionado);
				cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(this.calcularPorcentajeRPF(cotizacionDTO, calculoPoliza));				
			}else if(claveRecargoPagoFraccionadoUsuario.compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
				cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(valorRecargoPagoFraccionado);
				cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(this.calcularValorRPF(cotizacionDTO, calculoPoliza));
			}
										
			if(claveRecargoPagoFraccionadoUsuario.compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0 || sinAutorizacion){
				cotizacionDTO.setClaveAutorizacionRecargoPagoFraccionado(Sistema.AUTORIZACION_NO_REQUERIDA);
				cotizacionDTO.setCodigoUsuarioAutorizacionRecargoPagoFraccionado(null);
				cotizacionDTO.setFechaAutorizacionRecargoPagoFraccionado(null);
			}else{		
				DecimalFormat df10dec = new DecimalFormat("#.##########");
				DecimalFormat df4dec = new DecimalFormat("#.####");
				
				valorRPFAnterior = Double.valueOf(df10dec.format(valorRPFAnterior));
				porcentajeRPFAnterior = Double.valueOf(df4dec.format(porcentajeRPFAnterior));			
				double valorRPFActual = Double.valueOf(df10dec.format(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario().doubleValue()));
				double porcentajeRPFActual = Double.valueOf(df4dec.format(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario().doubleValue()));
				
				if(valorRPFActual != valorRPFAnterior || porcentajeRPFActual != porcentajeRPFAnterior){					
					cotizacionDTO.setClaveAutorizacionRecargoPagoFraccionado(Sistema.AUTORIZACION_REQUERIDA);
					cotizacionDTO.setCodigoUsuarioAutorizacionRecargoPagoFraccionado(null);
					cotizacionDTO.setFechaAutorizacionRecargoPagoFraccionado(null);
				}				
			}			
			cotizacionSN.modificar(cotizacionDTO);					
			return cotizacionSN.getPorId(idToCotizacion);
		}
		
		public Double obtenerPorcentajeIVA(BigDecimal idToCotizacion)throws SystemException{
			//Se obtiene % de IVA
			//List<ParametroGeneralDTO> parametroIva = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("id.codigoParametroGeneral",BigDecimal.valueOf(20020D));
			Double porcentajeIva = CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(idToCotizacion, "");
			
			if(porcentajeIva==null){
			    throw new SystemException("No se pudo obtener el porcentaje de IVA.");
			}
			
			return porcentajeIva;			
		}

	public ByteArrayAttachment obtenerCotizacionEnPDF(BigDecimal idToCotizacion,
			String nombreArchivo) {
		byte[] cotizacionArr = null;
		ByteArrayAttachment att = null;
		try {
			cotizacionArr = ImpresionAction.imprimirCotizacion(
					idToCotizacion, nombreUsuario);
		} catch (SystemException e) {} catch (IOException e) {}
		if(cotizacionArr != null){
			 att = new ByteArrayAttachment(nombreArchivo,
					TipoArchivo.PDF, cotizacionArr);		
		}
		return att;
	}
	
	public boolean mostrarIgualacionPrimas(BigDecimal idToCotizacion, Integer numeroInciso,
			List<SeccionCotizacionDTO> seccionesCombo,
			IgualacionPrimaNetaForm igualacionPrimaNetaForm) throws SystemException{
		
		boolean aplicaIgualacionAInciso = false;
		boolean igualacionValida = true;
		
		igualacionPrimaNetaForm.setIdToCotizacion(""+idToCotizacion);
		
		if(numeroInciso != null && numeroInciso.intValue() != -1) {
			igualacionPrimaNetaForm.setNumeroInciso(numeroInciso.toString());
			igualacionPrimaNetaForm.setMensajeIgualacion(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "cotizacion.mensajeIgualacion.inciso")+numeroInciso);
			aplicaIgualacionAInciso = true;
		}else{
			igualacionPrimaNetaForm.setNumeroInciso("-1");
			igualacionPrimaNetaForm.setMensajeIgualacion(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "cotizacion.mensajeIgualacion.cotizacion"));
		}
		
		StringBuilder mensajeError = new StringBuilder();
		boolean susCuotasSonValidas = validaTodasLasCoutasMayoresCeros(idToCotizacion, mensajeError);
		
		BigDecimal totalPNO = BigDecimal.ZERO;
		BigDecimal totalPN = BigDecimal.ZERO;
		List<CoberturaCotizacionDTO> coberturasComision = new ArrayList<CoberturaCotizacionDTO>();
		
		if (susCuotasSonValidas) {
			igualacionPrimaNetaForm.setMensajeErrorIgualacionPrima(Sistema.SIN_MENSAJE_PARA_MOSTRAR);
			
			List<IncisoCotizacionDTO> incisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
			if(incisos!= null && incisos.size()>1) {
				igualacionPrimaNetaForm.setIncisos(incisos);
			}
							
			// Seccion para mostrar la tabla de resumen
			SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
			List<SeccionCotizacionDTO> secciones = null;
			if(numeroInciso != null && numeroInciso.intValue() != -1){
				secciones = seccionCotizacionDN.
						listarSeccionesContratadasIgualacion(idToCotizacion, new BigDecimal(numeroInciso));
			}else{
				secciones = seccionCotizacionDN.listarSeccionesContratadas(idToCotizacion);
			}
						
			if (UtileriasWeb.esCadenaVacia(igualacionPrimaNetaForm.getTipoIgualacion())) {
				igualacionPrimaNetaForm.setTipoIgualacion("1");
			}
			CoberturaCotizacionDN.calcularPrimasOriginalesSeccion(secciones,aplicaIgualacionAInciso);

			if(seccionesCombo != null){
				for (SeccionCotizacionDTO seccion : secciones) {
					totalPN = totalPN.add(BigDecimal.valueOf(seccion.getValorPrimaNeta()));
					igualacionPrimaNetaForm.setTotalCotizacion(totalPN.doubleValue());
					totalPNO = totalPNO.add(BigDecimal.valueOf(seccion.getValorPrimaNetaOriginal()));
					igualacionPrimaNetaForm.setTotalCotizacionOriginal(totalPNO.doubleValue());
					seccionesCombo.add(seccion);
				}
				igualacionPrimaNetaForm.setSecciones(secciones);

				// Secciones y coberturas para los combos
				if (seccionesCombo != null && !seccionesCombo.isEmpty()) {
					for (SeccionCotizacionDTO seccion : secciones) {
						coberturasComision.addAll(seccion.getCoberturaCotizacionLista());
					}
				}
				igualacionPrimaNetaForm.setSeccionesCombo(seccionesCombo);
//				session = request.getSession();
//				session.setAttribute("secciones", seccionesCombo);
				
				for (SeccionCotizacionDTO seccion : seccionesCombo) {
					if (igualacionPrimaNetaForm.getIdToSeccion2() != null
							&& seccion.getId().getIdToSeccion().intValue() == Integer.parseInt(igualacionPrimaNetaForm.getIdToSeccion2())) {
						List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
						for (CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()) {
							if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion().shortValue() == 1
									&& (cobertura.getClaveFacultativo() == null
									|| cobertura.getClaveFacultativo() == 0)) {
								coberturas.add(cobertura);
							}
						}
						igualacionPrimaNetaForm.setCoberturas(coberturas);
						break;
					}
				}
			}
			
			if (igualacionPrimaNetaForm.getCoberturas() == null || igualacionPrimaNetaForm.getCoberturas().isEmpty()) {
				List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
				for (CoberturaCotizacionDTO cobertura : seccionesCombo.get(0).getCoberturaCotizacionLista()) {
					if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion().shortValue() == 1
							&& (cobertura.getClaveFacultativo() == null || cobertura.getClaveFacultativo() == 0)) {
						coberturas.add(cobertura);
					}
				}
				igualacionPrimaNetaForm.setCoberturas(coberturas);
			}
		} else {
			igualacionPrimaNetaForm.setMensajeErrorIgualacionPrima(mensajeError.toString());
			igualacionPrimaNetaForm.setCoberturas(new ArrayList<CoberturaCotizacionDTO>());
//			session = request.getSession();
			igualacionPrimaNetaForm.setSeccionesCombo(new ArrayList<SeccionCotizacionDTO>());
			igualacionPrimaNetaForm.setSecciones(new ArrayList<SeccionCotizacionDTO>());
//			session.setAttribute("secciones", new ArrayList<SeccionCotizacionDTO>());
			igualacionValida = false;
		}
		
		List<SoporteResumen>resumenComisiones = new ArrayList<SoporteResumen>();
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(idToCotizacion);
		cotizacionDTO = getPorId(cotizacionDTO.getIdToCotizacion());
		cotizacionDTO.setPrimaNetaAnual(totalPN.doubleValue());
		ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,resumenComisiones,coberturasComision);
		//ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,resumenComisiones);
		igualacionPrimaNetaForm.setPrimaNetaAnual(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getPrimaNetaAnual()));
		igualacionPrimaNetaForm.setPrimaNetaCotizacion(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
		igualacionPrimaNetaForm.setMontoRecargoPagoFraccionado(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
		igualacionPrimaNetaForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
		igualacionPrimaNetaForm.setMontoIVA(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getMontoIVA().doubleValue()));
		igualacionPrimaNetaForm.setDerechosPoliza(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
		igualacionPrimaNetaForm.setPrimaNetaTotal(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));	
		igualacionPrimaNetaForm.setResumenComisiones(resumenComisiones);
		
		// RMP Si el iva no est� configurado hay que avisarle al usuario
		if(cotizacionDTO.getFactorIVA() == 0.0) {
			igualacionPrimaNetaForm.setMensaje(UtileriasWeb.getMensajeRecurso("midas.cotizacion.noiva"));
			igualacionPrimaNetaForm.setTipoMensaje(Sistema.ERROR);
		}
		
		//Llenar campos de modificaci�n de recargo por pago fraccionado y derechos
		
		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario() == null) {
			cotizacionDTO.setClaveRecargoPagoFraccionadoUsuario(Sistema.CLAVE_RPF_SISTEMA);
		}
		
		igualacionPrimaNetaForm.setTipoCalculoRPF(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().toString() );
					
		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0){
			igualacionPrimaNetaForm.setValorRPFEditable(null);
			igualacionPrimaNetaForm.setPorcentajeRPFSoloLectura(null);				
			igualacionPrimaNetaForm.setPorcentajeRPFEditable(null);
			igualacionPrimaNetaForm.setValorRPFSoloLectura(null);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){
			igualacionPrimaNetaForm.setValorRPFEditable(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario());
			igualacionPrimaNetaForm.setPorcentajeRPFSoloLectura(new DecimalFormat("##0.00%").format(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario()/100));				
			igualacionPrimaNetaForm.setPorcentajeRPFEditable(null);
			igualacionPrimaNetaForm.setValorRPFSoloLectura(null);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
			igualacionPrimaNetaForm.setValorRPFEditable(null);
			igualacionPrimaNetaForm.setPorcentajeRPFSoloLectura(null);				
			igualacionPrimaNetaForm.setPorcentajeRPFEditable(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario());
			igualacionPrimaNetaForm.setValorRPFSoloLectura(Sistema.FORMATO_MONEDA.format(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario()));
		}
								
		if(cotizacionDTO.getClaveDerechosUsuario() == null) {
			cotizacionDTO.setClaveDerechosUsuario(Sistema.CLAVE_DERECHOS_SISTEMA);
		}
		
		igualacionPrimaNetaForm.setTipoCalculoDerechos(cotizacionDTO.getClaveDerechosUsuario().toString());
		
		if(cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_SISTEMA) == 0){			
			igualacionPrimaNetaForm.setValorDerechos(null);
		}else if(cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_USUARIO) == 0){
			igualacionPrimaNetaForm.setValorDerechos(cotizacionDTO.getValorDerechosUsuario());
		}
		
		if(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() == null) {
			cotizacionDTO.setClaveAutorizacionRecargoPagoFraccionado(Sistema.AUTORIZACION_NO_REQUERIDA);
		}						
		igualacionPrimaNetaForm.setClaveAutorizacionRPF(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().toString());
		
		if(cotizacionDTO.getClaveAutorizacionDerechos() == null) {
			cotizacionDTO.setClaveAutorizacionDerechos(Sistema.AUTORIZACION_NO_REQUERIDA);
		}			
		igualacionPrimaNetaForm.setClaveAutorizacionDerechos(cotizacionDTO.getClaveAutorizacionDerechos().toString());
		
		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0) {
			igualacionPrimaNetaForm.setMostrarEdicionDerechosRPF("0");
		} else {
			igualacionPrimaNetaForm.setMostrarEdicionDerechosRPF("1");
		}
		
		return igualacionValida;
	}
	
	public boolean validaTodasLasCoutasMayoresCeros(BigDecimal idToCotizacion,StringBuilder mensajeError) throws SystemException {
	    if(idToCotizacion != null) {
	    	List<CoberturaCotizacionDTO> coberturas = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(idToCotizacion);
	    	for(CoberturaCotizacionDTO cobertura : coberturas) {
	    		if (cobertura.getValorPrimaNeta() <= 0 && cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveImporteCero().equals("0")) {
					mensajeError.append("*Inciso ");
					mensajeError.append(cobertura.getId().getNumeroInciso());
					mensajeError.append("-");
					mensajeError.append(cobertura.getSeccionCotizacionDTO().getSeccionDTO().getNombreComercial());
					mensajeError.append("-");
					mensajeError.append(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					mensajeError.append("<br>");
				}
	    	}
	    }
	    if (mensajeError.toString().length() > 0) {
			return false;
		}
	    return true;
	}
	
	
	public String esMostrarOriginales(BigDecimal idToCotizacion) {
		
		String respuesta = "1";
		
		EndosoDTO endoso = null;
		
		try {
			endoso = EndosoDN.getInstancia(Sistema.USUARIO_SISTEMA).buscarPorCotizacion(idToCotizacion);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		if (endoso != null) {
			respuesta = "0";
		}
		
		return respuesta;
		
	}
	
	
}