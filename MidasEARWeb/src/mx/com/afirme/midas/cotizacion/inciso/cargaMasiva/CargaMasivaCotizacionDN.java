package mx.com.afirme.midas.cotizacion.inciso.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaDetalleCotDTO;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

public class CargaMasivaCotizacionDN {
	private static final CargaMasivaCotizacionDN INSTANCIA = new CargaMasivaCotizacionDN();

	public static CargaMasivaCotizacionDN getInstancia() {
		return CargaMasivaCotizacionDN.INSTANCIA;
	}

	public boolean cargaMasivaValida(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		boolean esValido = true;
		CargaMasivaCotDTO cargaMasivaCotDTO = this
				.getCargaMasiva(idToCotizacion);
		if (cargaMasivaCotDTO != null) {
			for (CargaMasivaDetalleCotDTO detalle : cargaMasivaCotDTO
					.getCargaMasivaDetalleCotDTO()) {
				if (detalle.getClaveEstatus().equals(Sistema.INCORRECTO)) {
					esValido = false;
					break;
				}
			}
		}
		return esValido;
	}

	public boolean existeCargaMasiva(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		boolean existeCarga = false;
		CargaMasivaCotDTO cargaMasivaCotDTO = this
				.getCargaMasiva(idToCotizacion);
		if (cargaMasivaCotDTO != null) {
			existeCarga = true;
		}
		return existeCarga;
	}

	public CargaMasivaCotDTO getCargaMasiva(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new CargaMasivaCotizacionSN().getCargaMasiva(idToCotizacion);
	}

	public void actualizaDetalle(CargaMasivaDetalleCotDTO detalle)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (detalle.getId() != null) {
			new CargaMasivaCotizacionSN().actualizaDetalle(detalle);
		}
	}

	public void eliminarCargaMasiva(CargaMasivaCotDTO carga)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (carga != null) {
			new CargaMasivaCotizacionSN().borrar(carga);
		}
	}

	public String getJsonDetalleCargaMasiva(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		CargaMasivaCotDTO cargaMasivaCotDTO = new CargaMasivaCotizacionSN()
				.getCargaMasiva(idToCotizacion);
		MidasJsonBase json = new MidasJsonBase();
		String icono = new String();
		String alt = new String();
		for (CargaMasivaDetalleCotDTO detalle : cargaMasivaCotDTO
				.getCargaMasivaDetalleCotDTO()) {
			MidasJsonRow row = new MidasJsonRow();
			row.setId(detalle.getId().getIdToCotizacion().toString() + "_"
					+ detalle.getId().getNumeroInciso());
			if (detalle.getClaveEstatus().equals(Sistema.CORRECTO)) {
				icono = Sistema.ICONO_VERDE;
				alt = "Inciso Correcto";
			} else {
				icono = Sistema.ICONO_ROJO;
				alt = "Inciso Incorrecto";
			}
			row
					.setDatos(
							MidasJsonRow.generarLineaImagenDataGrid(icono, alt,
									"void(0)", null),
							detalle.getId().getNumeroInciso().toString(),
							detalle.getNombreCalle() == null ? " "
									: detalle.getNombreCalle(),
							detalle.getNumeroExterior() == null ? " " : detalle
									.getNumeroExterior(),
							detalle.getNumeroInterior() == null ? " " : detalle
									.getNumeroInterior(),
							detalle.getCodigoPostal() == null ? " " : detalle
									.getCodigoPostal(),
							detalle.getNombreEstadoOriginal() == null ? " "
									: detalle.getNombreEstadoOriginal()
											+ "<br>"
											+ detalle.getNombreEstadoValido() == null ? 
											" "
											: detalle.getNombreEstadoOriginal(),
							detalle.getNombreMunicipioOriginal() + "<br>"
									+ detalle.getNombreMunicipioValido(),
							detalle.getNombreColoniaOriginal() == null ? " "
									: detalle.getNombreColoniaOriginal(),
							detalle.getNombreColoniaValida() != null
									&& !detalle.getNombreColoniaValida()
											.equals("") ? detalle
									.getNombreColoniaValida() : " ",
							detalle.getMensajeError() == null ? " "
									: detalle.getMensajeError(), detalle
									.getIdColonia() == null ? " " : detalle
									.getIdColonia(), detalle.getId()
									.getIdToCotizacion().toString());
			json.addRow(row);
		}
		return json.toString();
	}

	public CargaMasivaCotDTO agregar(CargaMasivaCotDTO cargaMasivaCotDTO,
			List<DireccionDTO> direccionesValidas,
			List<DireccionDTO> direccionesInvalidas)
			throws ExcepcionDeAccesoADatos, SystemException {

		return new CargaMasivaCotizacionSN().agregar(cargaMasivaCotDTO,
				direccionesValidas, direccionesInvalidas);
	}

	public void procesarCargaMasiva(BigDecimal idToCotizacion,
			String nombreUsuario) throws ExcepcionDeAccesoADatos,
			SystemException {
		CargaMasivaCotizacionSN cargaMasivaCotizacionSN = new CargaMasivaCotizacionSN();
		CargaMasivaCotDTO cargaMasivaCotDTO = cargaMasivaCotizacionSN
				.getCargaMasiva(idToCotizacion);
		if (cargaMasivaCotDTO != null
				&& cargaMasivaCotDTO.getCargaMasivaDetalleCotDTO() != null) {
			for (CargaMasivaDetalleCotDTO detalle : cargaMasivaCotDTO
					.getCargaMasivaDetalleCotDTO()) {
				DireccionDTO direccion = cargaMasivaCotizacionSN
						.poblarDireccion(detalle);
				direccion = DireccionDN.getInstancia().agregar(direccion);
				BigDecimal numeroInciso = IncisoCotizacionDN.getInstancia()
						.copiarInciso(cargaMasivaCotDTO.getIdToCotizacion(),
								cargaMasivaCotDTO.getNumeroIncisoBase(),
								direccion.getIdToDireccion());
				List<CoberturaCotizacionDTO> coberturas = CoberturaCotizacionDN
						.getInstancia().listarCoberturasBasicasContratadas(
								detalle.getId().getIdToCotizacion(),
								numeroInciso);
				String[] split = detalle.getSumaAseguradaSecRgo().split("-");
				int splitIndex = 0;
				for (CoberturaCotizacionDTO cobertura : coberturas) {
					String codigoSeccion = split[splitIndex++];
					String codigoCobertura = split[splitIndex++];
					String sumaAsegurada = split[splitIndex++];
					if (cobertura.getCoberturaSeccionDTO().getSeccionDTO()
							.getCodigo().equals(codigoSeccion)
							&& cobertura.getCoberturaSeccionDTO()
									.getCoberturaDTO().getCodigo().equals(
											codigoCobertura)) {
						cobertura.setValorSumaAsegurada(Double
								.parseDouble(sumaAsegurada));
						CoberturaCotizacionDN.getInstancia().modificar(
								cobertura);
						CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN
								.getInstancia();
						calculoCotizacionDN.calcularCobertura(cobertura,
								nombreUsuario, false);
					}
				}
			}
			// Eliminar la Carga procesada.
			this.eliminarCargaMasiva(cargaMasivaCotDTO);
		}
	}
}
