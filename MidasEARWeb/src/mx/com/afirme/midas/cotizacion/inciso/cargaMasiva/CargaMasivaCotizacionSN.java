package mx.com.afirme.midas.cotizacion.inciso.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaDetalleCotDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class CargaMasivaCotizacionSN {
	private CargaMasivaCotFacadeRemote beanRemoto;

	public CargaMasivaCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CargaMasivaCotFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public DireccionDTO poblarDireccion(CargaMasivaDetalleCotDTO detalle)
			throws SystemException {
		try {
			return beanRemoto.poblarDireccion(detalle);
		} catch (EJBTransactionRolledbackException e) {
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public void borrar(CargaMasivaCotDTO cargaMasivaCotDTO)
			throws SystemException {
		try {
			beanRemoto.delete(cargaMasivaCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public void modificar(CargaMasivaCotDTO cargaMasivaCotDTO)
			throws SystemException {
		try {
			beanRemoto.update(cargaMasivaCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public List<CargaMasivaCotDTO> listarTodos() throws SystemException {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public CargaMasivaCotDTO getPorId(BigDecimal id) throws SystemException {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CargaMasivaCotDTO> buscarPorPropiedad(String propiedad,
			Object valor) throws SystemException {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CargaMasivaCotDTO> getPorPropiedad(String name, Object obj)
			throws SystemException {
		try {
			return beanRemoto.findByProperty(name, obj);
		} catch (Exception e) {
			throw new SystemException(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public CargaMasivaCotDTO agregar(CargaMasivaCotDTO cargaMasivaCotDTO,
			List<DireccionDTO> direccionesValidas,
			List<DireccionDTO> direccionesInvalidas) throws SystemException {
		try {
			return beanRemoto.agregar(cargaMasivaCotDTO, direccionesValidas,
					direccionesInvalidas);
		} catch (Exception e) {
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public CargaMasivaCotDTO getCargaMasiva(BigDecimal idToCotizacion)
			throws SystemException {
		try {
			return beanRemoto.getCargaMasiva(idToCotizacion);
		} catch (Exception e) {
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}

	public void actualizaDetalle(CargaMasivaDetalleCotDTO detalle)
			throws SystemException {
		try {
			beanRemoto.actualizaDetalle(detalle);
		} catch (EJBTransactionRolledbackException e) {
			throw new SystemException(e.getClass().getCanonicalName());
		}
	}
}
