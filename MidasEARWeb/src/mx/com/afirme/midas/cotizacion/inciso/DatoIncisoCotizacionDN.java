package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubTipoEquipoElectronicoDN;
import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.GiroDN;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subgiro.SubGiroDN;
import mx.com.afirme.midas.catalogos.subgirorc.SubGiroRCDN;
import mx.com.afirme.midas.catalogos.subtipoequipocontratista.SubtipoEquipoContratistaDN;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDN;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDN;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDN;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDN;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDN;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class DatoIncisoCotizacionDN {
	private static final DatoIncisoCotizacionDN INSTANCIA = new DatoIncisoCotizacionDN();
	
	public static DatoIncisoCotizacionDN getINSTANCIA() {
		return INSTANCIA;
	}

	public DatoIncisoCotizacionDTO getPorId(DatoIncisoCotizacionDTO datoIncisoCotizacion) throws SystemException{
		DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
		return datoIncisoCotizacionSN.getPorId(datoIncisoCotizacion.getId());
	}
	
	/**
	 * Filtra los registros de DatoIncisoCotizacionDTO en base a los atributos recibidos en el objeto DatoIncisoCotizacionId.
	 * Los atributos enviados en la consulta son: idToCotizacion, numeroInciso, idToSeccion, idToCobertura, idToRiesgo,
	 * numeroSubinciso, idTcRamo, idTcSubramo, claveDetalle, idDato. Los atributos que se reciben con valor de null son ignorados.
	 * @param DatoIncisoCotizacionId idDatoInciso
	 */
	public List<DatoIncisoCotizacionDTO> listarPorIdFiltrado(DatoIncisoCotizacionId idDatoInciso) throws SystemException{
		return new DatoIncisoCotizacionSN().listarPorIdFiltrado(idDatoInciso);
	}
	
	/**
	 * Obtiene el objeto TipoTecho correspondiente a un inciso, utilizando la configuraci�n adecuada para la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return TipoTechoDTO. Entidad tipoTechoDTO correspondiente al inciso, en base al valor del registro DatoInciso encontrado. Si no se encuentra el 
	 * registro, devuelve null.
	 */
	public TipoTechoDTO obtenerTipoTecho(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException{
		DatoIncisoCotizacionDTO datoInciso = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(30d));

		TipoTechoDTO tipoTechoDTO = null;
		try {
			datoInciso = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoInciso);
			if (datoInciso == null){
				throw new SystemException("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"30");
			}
			TipoTechoDTO tipoTechoTMP = new TipoTechoDTO();
			tipoTechoTMP.setIdTipoTecho(UtileriasWeb.regresaBigDecimal(datoInciso.getValor()));
			tipoTechoDTO = TipoTechoDN.getInstancia().getTipoTechoPorIdSistema(tipoTechoTMP);
		} catch (Exception e) {}
		if (tipoTechoDTO == null){
			throw new SystemException("No se encontr� TipoTechoDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
		}
		return tipoTechoDTO;
	}
	
	/**
	 * Obtiene el objeto TipoMuro correspondiente a un inciso, utilizando la configuraci�n adecuada para la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return TipoMuroDTO. Entidad tipoMuroDTO correspondiente al inciso, en base al valor del registro DatoInciso encontrado. Si no se encuentra el 
	 * registro, devuelve null.
	 */
	public TipoMuroDTO obtenerTipoMuro(BigDecimal idToCotizacion,BigDecimal numeroInciso)throws SystemException{
		DatoIncisoCotizacionDTO datoInciso = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(40d));
		
		TipoMuroDTO tipoMuroDTO = null;
		try {
			datoInciso = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoInciso);
			if (datoInciso == null){
				throw new SystemException("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"40");
			}
			TipoMuroDTO tipoMuroTMP = new TipoMuroDTO();
			tipoMuroTMP.setIdTipoMuro(UtileriasWeb.regresaBigDecimal(datoInciso.getValor()));
			tipoMuroDTO = TipoMuroDN.getInstancia().getTipoMuroPorId(tipoMuroTMP);
		} catch (Exception e) {}
		if (tipoMuroDTO == null){
			throw new SystemException("No se encontr� TipoMuroDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
		}
		return tipoMuroDTO;
	}
	
	/**
	 * Obtiene el registro de la tabla DatoIncisoCotizacion que contiene el n�mero de pisos del inciso cuyos atributos se reciben.
	 * El n�mero de pisos se recupera del campo "valor" de la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return String numero de pisos del inciso, si no se encuentra el registro, regresa "No disponible";
	 */
	public String obtenerNumeroPisos(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException{
		//numero de pisos. Se obtiene de la tabla ToDatoIncisoCot.
		DatoIncisoCotizacionDTO datoInciso = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(50d));
		
		String numeroPisos = "No disponible";
		try {
			datoInciso = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoInciso);
			if (datoInciso == null){
				throw new SystemException("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"50");
			}
			numeroPisos = datoInciso.getValor();
		} catch (Exception e) {}
		return numeroPisos;
	}
	
	/**
	 * Regresa el GiroDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return GiroDTO. El giro del inciso, null en caso de no encontrar el registro.
	 */
	public GiroDTO obtenerGiroDTO(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException{
//		 Se obtiene de la tabla ToDatoIncisoCot. La columna valor contendr� el id del giro. Con ese id se puede buscar en el cat�logo 
//		 TcGiro la descripci�n correspondiente. 
		DatoIncisoCotizacionDTO datoInciso = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(10d));
		
		GiroDTO giroDTO = null;
		try {
			datoInciso = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoInciso);
			if (datoInciso == null){
				throw new SystemException("No se encontr� DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"10");
			}
			GiroDTO giroTMP = new GiroDTO();
			giroTMP.setIdTcGiro(UtileriasWeb.regresaBigDecimal(datoInciso.getValor()));
			giroDTO = GiroDN.getInstancia().getGiroPorId(giroTMP);
		} catch (Exception e) {}
		if (giroDTO == null){
			throw new SystemException("No se encontr� giroDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
		}
		return giroDTO;
	}
	
	/**
	 * Regresa la descripci�n del Giro correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return GiroDTO. El giro del inciso, null en caso de no encontrar el registro.
	 */
	public String obtenerDescripcionGiro(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException{
//		 Se obtiene de la tabla ToDatoIncisoCot. La columna valor contendr� el id del giro. Con ese id se puede buscar en el cat�logo 
//		 TcGiro la descripci�n correspondiente. 
		DatoIncisoCotizacionDTO datoInciso = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(1d));
		datoInciso.getId().setIdDato(new BigDecimal(25d));
		
		try {
			datoInciso = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoInciso);
			if (datoInciso == null){
				throw new SystemException("No se encontr� Descripci�n DatoInciso para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+1+", idDato: "+"25");
			}
			else
				return datoInciso.getValor();
		} catch (Exception e) {}
		return null;
	}
	
	/**
	 * Regresa el SubGiroDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubGiroDTO. El sub giro del inciso, null en caso de no encontrar el registro.
	 */
	public SubGiroDTO obtenerSubgiro(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo) throws SystemException{
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		//Para obtener el subgiro, se utiliza el idDato = 20
		datoIncisoCotizacionDTO.getId().setIdDato(new BigDecimal(20d));
		SubGiroDTO subGiro = null;
		try {
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			if (datoIncisoCotizacionDTO == null){
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: "+"20");
			}
			SubGiroDTO subGiroTMP = new SubGiroDTO();
			subGiroTMP.setIdTcSubGiro(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
			subGiro = SubGiroDN.getInstancia().getSubGiroPorId(subGiroTMP);
			if (subGiro == null){
				throw new SystemException("No se encontr� subGiroDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
			}
		} catch (SystemException e) {	e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		return subGiro;
	}
	
	/**
	 * Regresa el SubGiroRCDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubGiroRCDTO. El SubGiroRCDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubGiroRCDTO obtenerSubGiroRC(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo) throws SystemException{
		//Para obtener el subgiroRC, se utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		SubGiroRCDTO subGiroRC = null;
		try {
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			if (datoIncisoCotizacionDTO == null){
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: "+"20");
			}
			SubGiroRCDTO subGiroRCTMP = new SubGiroRCDTO();
			subGiroRCTMP.setIdTcSubGiroRC(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
			subGiroRC = SubGiroRCDN.getInstancia().getSubGiroRCPorId(subGiroRCTMP);
			if (subGiroRC == null){
				throw new SystemException("No se encontr� subGiroRC para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
			}
		} catch (SystemException e) {
			e.printStackTrace();}
		return subGiroRC;
	}

	/**
	 * Regresa el SubtipoMontajeMaquinaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoMontajeMaquinaDTO. El SubtipoMontajeMaquinaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoMontajeMaquinaDTO obtenerSubTipoMontajeMaquina (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo,Map<BigDecimal,SubtipoMontajeMaquinaDTO> mapaSubTipoMontajeMaq) throws SystemException{
		//Para obtener el subTipoMontajeM�quina, se utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = null;
		SubtipoMontajeMaquinaDTO result = null;
		try {
			for(int i=0;i<4;i++){
				datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
				datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
				datoIncisoCotizacionDTO.getId().setIdDato(idDato);
				if (i==0){
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2540));
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(17));
				} else if (i==1){
					datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2540));
				} else if (i==2){
					datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(17));
				}
				datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
				if (datoIncisoCotizacionDTO != null)
					break;
			}
			if (datoIncisoCotizacionDTO == null){
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20, idToRiesgo: 2540, idTcSubRamo: 17");
			}
			BigDecimal idtcsubtipomontajemaq = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
			if(mapaSubTipoMontajeMaq != null){
				result = mapaSubTipoMontajeMaq.get(idtcsubtipomontajemaq);
			}
			if(result == null)
				result = SubtipoMontajeMaquinaDN.getInstancia().getSubtipoMontajeMaquinaPorId(idtcsubtipomontajemaq);
			if(mapaSubTipoMontajeMaq != null && result != null){
				mapaSubTipoMontajeMaq.put(idtcsubtipomontajemaq, result);
			}
			if (result == null){
				throw new SystemException("No se encontr� SubtipoMontajeMaquinaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso);
			}
		} catch (SystemException e) {
			e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoElectronicoDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoElectronicoDTO. El SubtipoEquipoElectronicoDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoElectronicoDTO obtenerSubtipoEquipoElectronico (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo,Map<BigDecimal,SubtipoEquipoElectronicoDTO> mapaSubTipoEquipoElectronico) throws SystemException{
		//SUBTIPO_EQUIPO_ELECTRONICO utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.SUBRAMO_EQUIPO_ELECTRONICO);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2240));
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(null);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(null);
		SubtipoEquipoElectronicoDTO result = null;
		try {
			List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(datoIncisoCotizacionDTO.getId());
			if (listaDatosInciso != null && !listaDatosInciso.isEmpty()){
				int i=0;
				do{
					datoIncisoCotizacionDTO = listaDatosInciso.get(i);
					i++;
				}while(datoIncisoCotizacionDTO.getId().getIdToSeccion().compareTo(BigDecimal.ZERO) == 0 && datoIncisoCotizacionDTO.getId().getNumeroSubinciso().compareTo(BigDecimal.ZERO) == 0 && i<listaDatosInciso.size());
				if (datoIncisoCotizacionDTO != null){
					BigDecimal idSubTipo = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
					if(mapaSubTipoEquipoElectronico != null){
						result = mapaSubTipoEquipoElectronico.get(idSubTipo);
					}
					if(result == null)
						result = SubTipoEquipoElectronicoDN.getInstancia().getPorId(idSubTipo);
					if(mapaSubTipoEquipoElectronico != null && result != null)
						mapaSubTipoEquipoElectronico.put(idSubTipo, result);
					if (result == null){
						String error  = "No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo+
								"valorDatoInciso(idTcSubTipoEquipoElectronico): "+datoIncisoCotizacionDTO.getValor();
						LogDeMidasWeb.log(error, Level.SEVERE, null);
						throw new SystemException(error);
					}
				}
				else{
					throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
							+"idTcRamo: "+idTcRamo+", idDato: 20");
				}
			}
			else{
				String error = "No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
					+"idTcRamo: "+idTcRamo+", idDato: 20, idToRiesgo: 2240, idTcSubRamo: "+Sistema.SUBRAMO_EQUIPO_ELECTRONICO;
				LogDeMidasWeb.log(error, Level.SEVERE, null);
				throw new SystemException(error);
			}
		}
		catch(SystemException e){
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoElectronicoDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoElectronicoDTO. El SubtipoEquipoElectronicoDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoElectronicoDTO obtenerSubtipoEquipoElectronico (BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion,Map<BigDecimal,SubtipoEquipoElectronicoDTO> mapaSubTipoEquipoElectronico) throws SystemException{
		//SUBTIPO_EQUIPO_ELECTRONICO utiliza el idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(numeroSubInciso);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(13));
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2240));
		SubtipoEquipoElectronicoDTO result = null;
		try {
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			if (datoIncisoCotizacionDTO != null){
				BigDecimal idSubTipo = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
				if(mapaSubTipoEquipoElectronico != null){
					result = mapaSubTipoEquipoElectronico.get(idSubTipo);
				}
				if(result == null)
					result = SubTipoEquipoElectronicoDN.getInstancia().getPorId(idSubTipo);
				if(mapaSubTipoEquipoElectronico != null && result != null)
					mapaSubTipoEquipoElectronico.put(idSubTipo, result);
				if (result == null){
					throw new SystemException("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
							", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
				}
			}
			else{
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
		}
		catch(SystemException e){
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoContratistaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoContratistaDTO. El SubtipoEquipoContratistaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoContratistaDTO obtenerSubtipoEquipoContratista (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo,Map<BigDecimal, SubtipoEquipoContratistaDTO> mapaSubTipoEquipoContratista) throws SystemException{
		// para SUBTIPO_EQUIPO_CONTRATISTA se utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(14));
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2650));
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(null);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(null);
		SubtipoEquipoContratistaDTO result = null;
		try {
			List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(datoIncisoCotizacionDTO.getId());
			if (listaDatosInciso != null && !listaDatosInciso.isEmpty()){
				int i=0;
				do{
					datoIncisoCotizacionDTO = listaDatosInciso.get(i);
					i++;
				}while(datoIncisoCotizacionDTO.getId().getIdToSeccion() != null && datoIncisoCotizacionDTO.getId().getNumeroSubinciso() != null && i<listaDatosInciso.size());
				if (datoIncisoCotizacionDTO != null){
					BigDecimal idSubTipo = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
					if(mapaSubTipoEquipoContratista != null)
						result = mapaSubTipoEquipoContratista.get(idSubTipo);
					if(result == null)
						result = SubtipoEquipoContratistaDN.getInstancia().getSubtipoEqContrPorId(idSubTipo);
					if(mapaSubTipoEquipoContratista != null && result != null)
						mapaSubTipoEquipoContratista.put(idSubTipo, result);
					if (result == null){
						throw new SystemException("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
								", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
					}
				}
				else{
					throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
							+"idTcRamo: "+idTcRamo+", idDato: 20");
				}
			}
			else{
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
		} catch (SystemException e) {
			e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		if (result == null){
			throw new SystemException("No se encontr� SubtipoEquipoContratistaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
		}
		return result;
	}
	
	/**
	 * Regresa el SubtipoEquipoContratistaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoContratistaDTO. El SubtipoEquipoContratistaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoContratistaDTO obtenerSubtipoEquipoContratista (BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion,Map<BigDecimal, SubtipoEquipoContratistaDTO> mapaSubTipoEquipoContratista) throws SystemException{
		// para SUBTIPO_EQUIPO_CONTRATISTA se utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(14));
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2650));
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(numeroSubInciso);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		SubtipoEquipoContratistaDTO result = null;
		try {
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			if (datoIncisoCotizacionDTO != null){
				BigDecimal idSubTipo = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
				if(mapaSubTipoEquipoContratista != null)
					result = mapaSubTipoEquipoContratista.get(idSubTipo);
				if(result == null)
					result = SubtipoEquipoContratistaDN.getInstancia().getSubtipoEqContrPorId(idSubTipo);
				if(mapaSubTipoEquipoContratista != null && result != null)
					mapaSubTipoEquipoContratista.put(idSubTipo, result);
				if (result == null){
					throw new SystemException("No se encontr� SubTipoEquipoElectronicoDTO para la cotizacion: "+idToCotizacion+
							", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
				}
			}
			else{
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
		} catch (SystemException e) {
			e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		if (result == null){
			throw new SystemException("No se encontr� SubtipoEquipoContratistaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
		}
		return result;
	}
	
	/**
	 * Regresa el TipoObraCivilDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return TipoObraCivilDTO. El TipoObraCivilDTO del inciso, null en caso de no encontrar el registro.
	 */
	public TipoObraCivilDTO obtenerTipoObraCivil (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo,Map<BigDecimal,TipoObraCivilDTO> mapaSubTipoObraCivil)throws SystemException{
		//TIPOOBRA CIVIL utiliza idDato = 10
		BigDecimal idDato = new BigDecimal(10d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(16));
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2710));
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		TipoObraCivilDTO result = null;
		try {
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			if (datoIncisoCotizacionDTO == null){
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 50");
			}
			BigDecimal id = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
			if(mapaSubTipoObraCivil != null)
				result = mapaSubTipoObraCivil.get(id);
			if(result == null)
				result = TipoObraCivilDN.getInstancia().getTipoObraCivilPorId(id);
			if(mapaSubTipoObraCivil != null && result != null)
				mapaSubTipoObraCivil.put(id, result);
			if (result == null){
				throw new SystemException("No se encontr� TipoObraCivilDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
			}
		} catch (SystemException e) {
			e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	/**
	 * Regresa el SubTipoMaquinariaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubTipoMaquinariaDTO. El SubTipoMaquinariaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubTipoMaquinariaDTO obtenerSubTipoMaquinaria(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo,Map<BigDecimal,SubTipoMaquinariaDTO> mapaSubTipos) throws SystemException{
		//SUBTIPO_MAQUINARIA utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.SUBRAMO_ROTURA_MAQUINARIA);
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2090));
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(null);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(null);
		SubTipoMaquinariaDTO result = null;
		try {
			List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(datoIncisoCotizacionDTO.getId());
			if (listaDatosInciso != null && !listaDatosInciso.isEmpty()){
				int i=0;
				do{
					datoIncisoCotizacionDTO = listaDatosInciso.get(i);
					i++;
				}while(datoIncisoCotizacionDTO.getId().getIdToSeccion().compareTo(BigDecimal.ZERO) == 0 && datoIncisoCotizacionDTO.getId().getNumeroSubinciso().compareTo(BigDecimal.ZERO) == 0 && i<listaDatosInciso.size());
				if (datoIncisoCotizacionDTO != null){
					BigDecimal idSubTipoMaquinaria = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
					if(mapaSubTipos != null){
						result = mapaSubTipos.get(idSubTipoMaquinaria);
					}
					if(result == null)
						result = SubTipoMaquinariaDN.getInstancia().getPorId(idSubTipoMaquinaria);
					if(mapaSubTipos != null && result != null)
						mapaSubTipos.put(idSubTipoMaquinaria, result);
					else if (result == null){
						String error = "No se encontr� SubTipoMaquinariaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo
										+", idTcSubRamo: "+Sistema.SUBRAMO_ROTURA_MAQUINARIA+", valor (idTcSubTipoMaquinaria):"+datoIncisoCotizacionDTO.getValor();
						LogDeMidasWeb.log(error, Level.SEVERE, null);
						throw new SystemException(error);
					}
				}
				else{
					String error = "No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
					+"idTcRamo: "+idTcRamo+", idDato: 20, con idToSeccion != 0 y numeroSubInciso != 0";
					LogDeMidasWeb.log(error, Level.SEVERE, null);
					throw new SystemException(error);
				}
			}
			else{
				String error = "No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
				+"idTcRamo: "+idTcRamo+", idDato: 20";
				LogDeMidasWeb.log(error, Level.SEVERE, null);
				throw new SystemException(error);
			}
		} catch (SystemException e) {
			e.printStackTrace();
			throw e;
		}
		catch(Exception e){e.printStackTrace();}
		return result;
	}
	
	/**
	 * Regresa el SubTipoMaquinariaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubTipoMaquinariaDTO. El SubTipoMaquinariaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubTipoMaquinariaDTO obtenerSubTipoMaquinaria(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion,Map<BigDecimal,SubTipoMaquinariaDTO> mapaSubTipos) throws SystemException{
		//SUBTIPO_MAQUINARIA utiliza idDato = 20
		BigDecimal idDato = new BigDecimal(20d);
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = instanciaDatoIncisoCotizacion(idToCotizacion,numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(idTcRamo);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(new BigDecimal(12));
		datoIncisoCotizacionDTO.getId().setIdDato(idDato);
		datoIncisoCotizacionDTO.getId().setClaveDetalle((short)2);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(new BigDecimal(2090));
		datoIncisoCotizacionDTO.getId().setNumeroSubinciso(numeroSubInciso);
		datoIncisoCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		SubTipoMaquinariaDTO result = null;
		try {
			datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			if (datoIncisoCotizacionDTO != null){
				BigDecimal idSubTipoMaquinaria = UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor());
				if(mapaSubTipos != null){
					result = mapaSubTipos.get(idSubTipoMaquinaria);
				}
				if(result == null)
					result = SubTipoMaquinariaDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(datoIncisoCotizacionDTO.getValor()));
				if(mapaSubTipos != null && result != null)
					mapaSubTipos.put(idSubTipoMaquinaria, result);
				if (result == null){
					throw new SystemException("No se encontr� SubTipoMaquinariaDTO para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo:"+idTcRamo);
				}
			}
			else{
				throw new SystemException("No se encontr� DatoIncisoCotizacion para la cotizacion: "+idToCotizacion+", inciso: "+numeroInciso
						+"idTcRamo: "+idTcRamo+", idDato: 20");
			}
		} catch (SystemException e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	/**
	 * Regresa el subtipo correspondiente al SubRamoDTO recibido. los Subtipos considerados son los siguientes: subGiro, subGiroRC, 
	 * SubTipoMaquinaria, SubTipoMontajeMaquina, SubTipoEquipoElectronico, SubTipoEquipoContratista y tipoObraCivil 
	 * @return Object el objeto de los datos de riesgo correspondiente al subramo recibido de la cotizaci�n e inciso recibidos. null en caso de error
	 * @throws SystemException. En caso de no encontrar el datoIncisoCotizacion, o de no encontrar la entidad a la que hace referencia el DatoIncisoCotizacion 
	 */
	public Object obtenerDTOSubTipoCorrespondienteAlSubRamo(SubRamoDTO subRamoDTO,BigDecimal idToCotizacion, BigDecimal numeroInciso) throws SystemException{
		Object result = null;
		if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_INCENDIO) == 0)
			result = obtenerSubgiro(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_RESPONSABILIDAD_CIVIL_GENERAL) == 0)
			result = obtenerSubGiroRC(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo());
		else if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_ROTURA_MAQUINARIA) == 0) 
			result = obtenerSubTipoMaquinaria(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo(),null);
		else if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_MONTAJE_MAQUINA) == 0)
			result = obtenerSubTipoMontajeMaquina(idToCotizacion,numeroInciso,subRamoDTO.getRamoDTO().getIdTcRamo(),null);
		else if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_EQUIPO_ELECTRONICO) == 0)
			result = obtenerSubtipoEquipoElectronico(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo(),null);
		else if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0)
			result = obtenerSubtipoEquipoContratista(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo(),null);
		else if (subRamoDTO.getIdTcSubRamo().compareTo(Sistema.SUBRAMO_OBRA_CIVIL_EN_CONSTRUCCION) == 0)
			result = obtenerTipoObraCivil(idToCotizacion, numeroInciso, subRamoDTO.getRamoDTO().getIdTcRamo(),null);
		return result;
	}
	
	public DatoIncisoCotizacionDTO instanciaDatoIncisoCotizacion(){
		DatoIncisoCotizacionDTO datoInciso;
		datoInciso = new DatoIncisoCotizacionDTO();
		datoInciso.setId(new DatoIncisoCotizacionId());
		datoInciso.getId().setIdToCotizacion(new BigDecimal(0d));
		datoInciso.getId().setNumeroInciso(new BigDecimal(0d));
		datoInciso.getId().setIdTcRamo(new BigDecimal(0d));
		datoInciso.getId().setIdDato(new BigDecimal(0d));
		datoInciso.getId().setIdToSeccion(new BigDecimal(0d));
		datoInciso.getId().setIdToCobertura(new BigDecimal(0d));
		datoInciso.getId().setNumeroSubinciso(new BigDecimal(0d));
		datoInciso.getId().setIdTcSubramo(new BigDecimal(0d));
		datoInciso.getId().setClaveDetalle(new Short((short)0));
		datoInciso.getId().setIdToRiesgo(new BigDecimal(0d));
		return datoInciso;
	}
	
	public DatoIncisoCotizacionDTO instanciaDatoIncisoCotizacion(BigDecimal idToCotizacion,BigDecimal numeroInciso){
		DatoIncisoCotizacionDTO datoInciso;
		datoInciso = new DatoIncisoCotizacionDTO();
		datoInciso.setId(new DatoIncisoCotizacionId());
		datoInciso.getId().setIdToCotizacion(idToCotizacion);
		datoInciso.getId().setNumeroInciso(numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(0d));
		datoInciso.getId().setIdDato(new BigDecimal(0d));
		datoInciso.getId().setIdToSeccion(new BigDecimal(0d));
		datoInciso.getId().setIdToCobertura(new BigDecimal(0d));
		datoInciso.getId().setNumeroSubinciso(new BigDecimal(0d));
		datoInciso.getId().setIdTcSubramo(new BigDecimal(0d));
		datoInciso.getId().setClaveDetalle(new Short((short)0));
		datoInciso.getId().setIdToRiesgo(new BigDecimal(0d));
		return datoInciso;
	}
	
	/**
	 * Filtra los registros de DatoIncisoCotizacionDTO en base al idToCotizacion recibido.
	 * @param BigDecimal idToCotizacion
	 * @return List<DatoIncisoCotizacionDTO>. lista de registros DatoIncisoCotizacion de la cotizacion recibida.
	 * @throws SystemException 
	 */
	public List<DatoIncisoCotizacionDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion) throws SystemException {
		return new DatoIncisoCotizacionSN().listarPorIdToCotizacion(idToCotizacion);
	}
	
	/**
	 * consulta la descripci�n de la obra para una cotizacion del subramo RC Constructores
	 */
	public String obtenerDescripcionObraRCConstructores(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException{
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO= null;
		String descripcion = null;
		datoIncisoCotizacionDTO= instanciaDatoIncisoCotizacion(idToCotizacion, numeroInciso);
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
		datoIncisoCotizacionDTO.getId().setIdDato(Sistema.RC_CONSTRUCTORES_ID_DATO_DESCRIPCION_OBRA);
		datoIncisoCotizacionDTO= getPorId(datoIncisoCotizacionDTO);
		if(datoIncisoCotizacionDTO==null){
			datoIncisoCotizacionDTO= instanciaDatoIncisoCotizacion(idToCotizacion, numeroInciso);
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
			datoIncisoCotizacionDTO.getId().setIdDato(Sistema.RC_CONSTRUCTORES_ID_DATO_DESCRIPCION_OBRA);
			datoIncisoCotizacionDTO= getPorId(datoIncisoCotizacionDTO);
			descripcion = datoIncisoCotizacionDTO==null ? "No disponible" : datoIncisoCotizacionDTO.getValor();
		}else{
			descripcion = datoIncisoCotizacionDTO.getValor();
		}
		return descripcion;
	}
	
	/**
	 * Consulta la duracion de la obra de una cotizacion de subramo RC Constructores
	 * @throws SystemException 
	 */
	public String obtenerDuracionObraRCConstructores(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException{
		DatoIncisoCotizacionDTO datoIncisoCotizacionDTO= instanciaDatoIncisoCotizacion(idToCotizacion, numeroInciso);
		String descripcion = null;
		datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION[Sistema.POSICION_IDTORIESGO]);
		datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCRAMO]);
		datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION[Sistema.POSICION_IDTCSUBRAMO]);
		datoIncisoCotizacionDTO.getId().setIdDato(Sistema.RC_CONSTRUCTORES_ID_DATO_DURACION_OBRA);
		datoIncisoCotizacionDTO= getPorId(datoIncisoCotizacionDTO);
		if(datoIncisoCotizacionDTO==null){
			datoIncisoCotizacionDTO= instanciaDatoIncisoCotizacion(idToCotizacion, numeroInciso);
			datoIncisoCotizacionDTO.getId().setIdToRiesgo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTORIESGO]);
			datoIncisoCotizacionDTO.getId().setIdTcRamo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCRAMO]);
			datoIncisoCotizacionDTO.getId().setIdTcSubramo(Sistema.RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA[Sistema.POSICION_IDTCSUBRAMO]);
			datoIncisoCotizacionDTO.getId().setIdDato(Sistema.RC_CONSTRUCTORES_ID_DATO_DURACION_OBRA);
			datoIncisoCotizacionDTO= getPorId(datoIncisoCotizacionDTO);
			descripcion = (datoIncisoCotizacionDTO==null?"No disponible":datoIncisoCotizacionDTO.getValor());
		}else{
			descripcion = (datoIncisoCotizacionDTO.getValor());
		}
		return descripcion;
	}

	public void agregar(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO) throws SystemException {
		new DatoIncisoCotizacionSN().agregar(datoIncisoCotizacionDTO);
	}
	
	public void modificar(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO) throws SystemException {
		new DatoIncisoCotizacionSN().modificar(datoIncisoCotizacionDTO);
	}
}
