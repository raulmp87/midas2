package mx.com.afirme.midas.cotizacion.inciso.datoinciso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDN;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.catalogos.subgiro.SubGiroDN;
import mx.com.afirme.midas.catalogos.subgirorc.SubGiroRCDN;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

/**
 * 
 * @author jose luis arellano
 */
public class AdministradorDatoIncisoCotizacion {
	private BigDecimal idToCotizacion;
	private List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion;
	
	private Map<BigDecimal[],List<DatoIncisoCotizacionDTO>> mapaDatosIncisoCotizacion = new HashMap<BigDecimal[], List<DatoIncisoCotizacionDTO>>();
	private Map<BigDecimal[],DatoIncisoCotizacionDTO> mapaDatosIncisoCotizacionPorIncisoRamoIdDato = new HashMap<BigDecimal[], DatoIncisoCotizacionDTO>();
	
	private static Map<BigDecimal[],ConfiguracionDatoIncisoCotizacionDTO> mapaConfiguracionDatoInciso = new HashMap<BigDecimal[], ConfiguracionDatoIncisoCotizacionDTO>();
	
	//Mapa usado para almacenar valores de otras tablas, consultadas por el campo "claseRemota" y el valor del datoInciso
	private static Map<String,Map<BigDecimal,String>> mapaAnidadoValoresBeanRemoto = new HashMap<String, Map<BigDecimal,String>>();
	
	//Mapa usado para almacenar valores de la tabla CatalogoValorFijo, consultadas por el campo "claseRemota" y el valor del datoInciso
	private Map<BigDecimal[],String> mapaValoresCatalogoValorFijo = new HashMap<BigDecimal[], String>();
	
	//Mapa usado para almacenar los subgiros
	private Map<BigDecimal,SubGiroDTO> mapaSubGiros = new HashMap<BigDecimal, SubGiroDTO>();
	
	//Mapa usado para almacenar los subgiroRC
	private Map<BigDecimal,SubGiroRCDTO> mapaSubGiroRC = new HashMap<BigDecimal, SubGiroRCDTO>();
	
	//Mapa usado para almacenar los registros de PlenoReaseguroDTO
	private Map<BigDecimal,PlenoReaseguroDTO> mapaPlenoReaseguro= new HashMap<BigDecimal, PlenoReaseguroDTO>();
	
	//Mapa usado para almacenar los SubTipoMontajeMaquina
	private Map<BigDecimal,SubtipoMontajeMaquinaDTO> mapaSubTipoMontajeMaquina = new HashMap<BigDecimal, SubtipoMontajeMaquinaDTO>();
	
	//Mapa usado para almacenar los SubTipoEquipoElectronico
	private Map<BigDecimal,SubtipoEquipoElectronicoDTO> mapaSubTipoEquipoElectronico = new HashMap<BigDecimal, SubtipoEquipoElectronicoDTO>();
	
	//Mapa usado para almacenar los SubtipoEquipoContratistaDTO
	private Map<BigDecimal,SubtipoEquipoContratistaDTO> mapaSubTipoEquipoContratista= new HashMap<BigDecimal, SubtipoEquipoContratistaDTO>();
	
	//Mapa usado para almacenar los SubTipoObraCivilDTO
	private Map<BigDecimal,TipoObraCivilDTO> mapaSubTipoObraCivil =new HashMap<BigDecimal, TipoObraCivilDTO>();

	private Map<BigDecimal,SubTipoMaquinariaDTO> mapaSubTipoMaquinaria = new HashMap<BigDecimal, SubTipoMaquinariaDTO>();

	public AdministradorDatoIncisoCotizacion(BigDecimal idToCotizacion){
		this.idToCotizacion = idToCotizacion;
	}
	
	public void consultarDatosIncisoCotizacion() throws SystemException{
		if(listaDatosIncisoCotizacion == null){
			listaDatosIncisoCotizacion = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdToCotizacion(idToCotizacion);
		}
	}
	
	public List<DatoIncisoCotizacionDTO> obtenerDatoIncisoCotPorNumeroInciso(BigDecimal numeroInciso) throws SystemException{
		consultarDatosIncisoCotizacion();
		BigDecimal []key = new BigDecimal[1];
		key[0] = numeroInciso;
		List<DatoIncisoCotizacionDTO> listaEncontrada = mapaDatosIncisoCotizacion.get(key);
		if(listaEncontrada == null){
			listaEncontrada = new ArrayList<DatoIncisoCotizacionDTO>();
			for(DatoIncisoCotizacionDTO datoTMP : listaDatosIncisoCotizacion){
				if(datoTMP.getId().getNumeroInciso().compareTo(numeroInciso) == 0){
					listaEncontrada.add(datoTMP);
				}
			}
			mapaDatosIncisoCotizacion.put(key, listaEncontrada);
		}
		return listaEncontrada;
	}
	
	public List<DatoIncisoCotizacionDTO> obtenerDatoIncisoCotPorIncisoSubInciso(BigDecimal numeroInciso,BigDecimal numeroSubInciso) throws SystemException{
		consultarDatosIncisoCotizacion();
		BigDecimal []key = new BigDecimal[2];
		key[0] = numeroInciso;
		key[1] = numeroSubInciso;
		List<DatoIncisoCotizacionDTO> listaEncontrada = mapaDatosIncisoCotizacion.get(key);
		if(listaEncontrada == null){
			listaEncontrada = new ArrayList<DatoIncisoCotizacionDTO>();
			
			List<DatoIncisoCotizacionDTO> listaEncontradaPorInciso = busquedaRapidaPorInciso(numeroInciso);
			List<DatoIncisoCotizacionDTO> listaUniversoPorBuscar = null;
			
			if(listaEncontradaPorInciso != null){
				listaUniversoPorBuscar = listaEncontradaPorInciso;
			}
			else{
				listaUniversoPorBuscar = listaDatosIncisoCotizacion;
			}
			for(DatoIncisoCotizacionDTO datoTMP : listaUniversoPorBuscar){
				if(datoTMP.getId().getNumeroInciso().compareTo(numeroInciso) == 0 &&
						datoTMP.getId().getNumeroSubinciso().compareTo(numeroSubInciso) == 0){
					listaEncontrada.add(datoTMP);
				}
			}
			mapaDatosIncisoCotizacion.put(key, listaEncontrada);
		}
		return listaEncontrada;
	}
	
	public DatoIncisoCotizacionDTO obtenerDatoIncisoCotPorIncisoRamoIdDato(BigDecimal numeroInciso,BigDecimal idTcRamo,BigDecimal idDato) throws SystemException{
		consultarDatosIncisoCotizacion();
		BigDecimal []key = new BigDecimal[3];
		key[0] = numeroInciso;
		key[1] = idTcRamo;
		key[2] = idDato;
		DatoIncisoCotizacionDTO datoEncontrado = mapaDatosIncisoCotizacionPorIncisoRamoIdDato.get(key);
		if(datoEncontrado == null){
			
			List<DatoIncisoCotizacionDTO> listaPorInciso = busquedaRapidaPorInciso(numeroInciso);
			List<DatoIncisoCotizacionDTO> listaUniversoPorBuscar = listaPorInciso!= null ? listaPorInciso: listaDatosIncisoCotizacion;
			
			for(DatoIncisoCotizacionDTO datoTMP : listaUniversoPorBuscar){
				if(datoTMP.getId().getNumeroInciso().compareTo(numeroInciso) == 0 &&
						datoTMP.getId().getIdTcRamo().compareTo(idTcRamo) == 0 &&
						datoTMP.getId().getIdDato().compareTo(idDato) == 0){
					datoEncontrado = datoTMP;
					break;
				}
			}
			if(datoEncontrado != null)
				mapaDatosIncisoCotizacionPorIncisoRamoIdDato.put(key, datoEncontrado);
		}
		return datoEncontrado;
	}
	
	public SubGiroDTO obtenerSubGiroDTO(BigDecimal numeroInciso,BigDecimal idTcRamo) throws SystemException{
		//Busqueda del datoIncisoCot
		//Para obtener el subgiro, se utiliza el idDato = 20
		DatoIncisoCotizacionDTO datoSubGiro = obtenerDatoIncisoCotPorIncisoRamoIdDato(numeroInciso, idTcRamo, new BigDecimal("20"));
		if(datoSubGiro == null){
			throw new SystemException("No se encontr� datoIncisoCot para definir el subGiro. idToCotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo: "+idTcRamo);
		}
		
		BigDecimal idTcSubGiro = UtileriasWeb.regresaBigDecimal(datoSubGiro.getValor());
		SubGiroDTO subGiro = mapaSubGiros.get(idTcSubGiro);
		
		if(subGiro == null){
			subGiro = SubGiroDN.getInstancia().getSubGiroPorId(idTcSubGiro);
			if(subGiro != null){
				mapaSubGiros.put(idTcSubGiro, subGiro);
			}
		}
		return subGiro;
	}
	
	public SubGiroRCDTO obtenerSubGiroRCDTO(BigDecimal numeroInciso,BigDecimal idTcRamo) throws SystemException{
		//Busqueda del datoIncisoCot
		//Para obtener el subgiroRC, se utiliza el idDato = 20
		DatoIncisoCotizacionDTO datoSubGiro = obtenerDatoIncisoCotPorIncisoRamoIdDato(numeroInciso, idTcRamo, new BigDecimal("20"));
		if(datoSubGiro == null){
			throw new SystemException("No se encontr� datoIncisoCot para definir el subGiroRC. idToCotizacion: "+idToCotizacion+", inciso: "+numeroInciso+", idTcRamo: "+idTcRamo);
		}
		
		BigDecimal idTcSubGiro = UtileriasWeb.regresaBigDecimal(datoSubGiro.getValor());
		SubGiroRCDTO subGiro = mapaSubGiroRC.get(idTcSubGiro);
		
		if(subGiro == null){
			subGiro = SubGiroRCDN.getInstancia().getSubGiroRCPorId(idTcSubGiro);
			if(subGiro != null){
				mapaSubGiroRC.put(idTcSubGiro, subGiro);
			}
		}
		return subGiro;
	}
	
	public PlenoReaseguroDTO obtenerPlenoReaseguro(BigDecimal numeroInciso,BigDecimal idTcRamo) throws SystemException{
		SubGiroDTO subGiro = obtenerSubGiroDTO(numeroInciso, idTcRamo);
		if(subGiro == null){
			throw new SystemException("No se encontr� subgiro para los datos: idToCotizacion: "+idToCotizacion+", idTcRamo: "+idTcRamo);
		}
		BigDecimal idPlenoReaseguro = subGiro.getIdGrupoPlenos();
		PlenoReaseguroDTO plenoEncontrado = mapaPlenoReaseguro.get(idPlenoReaseguro);
		if(plenoEncontrado == null){
			plenoEncontrado = PlenoReaseguroDN.getInstancia().getPorId(idPlenoReaseguro);
			if(plenoEncontrado != null){
				mapaPlenoReaseguro.put(idPlenoReaseguro, plenoEncontrado);
			}
		}
		return plenoEncontrado;
	}
	
	
	public String[] obtenerDescripcionDatoInciso(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO,ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO) throws SystemException{
		ConfiguracionDatoIncisoCotizacionDTO configuracionDato = null;
		if(configuracionDatoIncisoCotizacionDTO == null || configuracionDatoIncisoCotizacionDTO.getDescripcionEtiqueta() == null || configuracionDatoIncisoCotizacionDTO.getDescripcionEtiqueta().trim().equals("")){
			BigDecimal[] keyConsultaConfigDato = new BigDecimal[5];
			keyConsultaConfigDato[0]=datoIncisoCotizacionDTO.getId().getIdTcRamo();
			keyConsultaConfigDato[1]=datoIncisoCotizacionDTO.getId().getIdTcSubramo();
			keyConsultaConfigDato[2]=datoIncisoCotizacionDTO.getId().getIdToRiesgo();
			keyConsultaConfigDato[3]=new BigDecimal(datoIncisoCotizacionDTO.getId().getClaveDetalle().toString());
			keyConsultaConfigDato[4]=datoIncisoCotizacionDTO.getId().getIdDato();
			
			configuracionDato = mapaConfiguracionDatoInciso.get(keyConsultaConfigDato);
			
			if(configuracionDato == null){
				
				//Consulta del registro ConfiguracionDatoInciso
				ConfiguracionDatoIncisoCotizacionId configuracionDatoId = new ConfiguracionDatoIncisoCotizacionId();
				configuracionDatoId.setIdTcRamo(datoIncisoCotizacionDTO.getId().getIdTcRamo());
				configuracionDatoId.setIdTcSubramo(datoIncisoCotizacionDTO.getId().getIdTcSubramo());
				configuracionDatoId.setIdToRiesgo(datoIncisoCotizacionDTO.getId().getIdToRiesgo());
				configuracionDatoId.setClaveDetalle(datoIncisoCotizacionDTO.getId().getClaveDetalle());
				configuracionDatoId.setIdDato(datoIncisoCotizacionDTO.getId().getIdDato());
				
				ConfiguracionDatoIncisoCotizacionSN confDatoIncisoSN = new ConfiguracionDatoIncisoCotizacionSN();
				configuracionDato = confDatoIncisoSN.getPorId(configuracionDatoId);
				
				if(configuracionDato == null)
					throw new SystemException("No se ecnontro� la configuraci�n para el dato: \nidTcRamo: "+datoIncisoCotizacionDTO.getId().getIdTcRamo()+"\n"+
							"idTcSubRamo: "+datoIncisoCotizacionDTO.getId().getIdTcSubramo()+"\nidToRiesgo: "+datoIncisoCotizacionDTO.getId().getIdToRiesgo()+"\n"+
							"claveDetalle: "+datoIncisoCotizacionDTO.getId().getClaveDetalle()+"\nidDato: "+datoIncisoCotizacionDTO.getId().getIdDato());
				
				mapaConfiguracionDatoInciso.put(keyConsultaConfigDato, configuracionDato);
				
			}
		} else{
			configuracionDato = configuracionDatoIncisoCotizacionDTO;
		}
		
		if (configuracionDato == null){
			throw new SystemException("No se ecnontro� la configuraci�n para el dato: \nidTcRamo: "+datoIncisoCotizacionDTO.getId().getIdTcRamo()+"\n"+
					"idTcSubRamo: "+datoIncisoCotizacionDTO.getId().getIdTcSubramo()+"\nidToRiesgo: "+datoIncisoCotizacionDTO.getId().getIdToRiesgo()+"\n"+
					"claveDetalle: "+datoIncisoCotizacionDTO.getId().getClaveDetalle()+"\nidDato: "+datoIncisoCotizacionDTO.getId().getIdDato());
		}
		
		String result[] = new String[3];
		result[0]=configuracionDato.getDescripcionEtiqueta();
		
		result[1]=datoIncisoCotizacionDTO.getValor();
		result[2]=datoIncisoCotizacionDTO.getValor();
		
		switch(configuracionDato.getClaveTipoControl()) {
			case 1:
				result[1] = obtenerValorBeanRemoto(configuracionDato.getDescripcionClaseRemota(), UtileriasWeb.regresaBigDecimal(result[1]));
				break;
			case 2:
				result[1] = obtenerValorCatalogoValorFijo(configuracionDato.getDescripcionClaseRemota(),
						UtileriasWeb.regresaBigDecimal(result[1]), configuracionDato.getIdGrupo());
				break;
			default:{
				switch(configuracionDato.getCodigoFormato().intValue()){
					case 1:
						try{
							result[1] = new DecimalFormat("#,##0").format(Double.valueOf(result[1]));
						}catch(Exception e){
						}
				}
				break;
			}
		}
		return result;
	}

	private List<DatoIncisoCotizacionDTO> busquedaRapidaPorInciso(BigDecimal numeroInciso){
		BigDecimal[] keyBusquedaPorInciso = new BigDecimal[1];
		keyBusquedaPorInciso[0] = numeroInciso;
		
		List<DatoIncisoCotizacionDTO> listaEncontradaPorInciso = mapaDatosIncisoCotizacion.get(keyBusquedaPorInciso);
		return listaEncontradaPorInciso;
	}
	
	@SuppressWarnings("unchecked")
	private String obtenerValorCatalogoValorFijo(String beanRemoto,BigDecimal idDato,BigDecimal idGrupoValores) throws SystemException{
		BigDecimal[] key = new BigDecimal[2];
		key[0] = idDato;
		key[1] = idGrupoValores;
		String valorCatalogoValorFijo = mapaValoresCatalogoValorFijo.get(key);
		
		if(valorCatalogoValorFijo == null){
			CatalogoValorFijoId catalogoFijoId = new CatalogoValorFijoId();
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			MidasInterfaceBase beanBase;
			try{
				catalogoFijoId.setIdDato(idDato.intValue());
				catalogoFijoId.setIdGrupoValores(idGrupoValores.intValue());
				beanBase = serviceLocator.getEJB(beanRemoto);
				valorCatalogoValorFijo = beanBase.findById(catalogoFijoId).getDescription();
			}catch(NullPointerException e){}
			catch(NumberFormatException e){}
			catch (ClassNotFoundException e) {}
			
			if(valorCatalogoValorFijo != null)
				mapaValoresCatalogoValorFijo.put(key,valorCatalogoValorFijo);
		}
		return valorCatalogoValorFijo;
	}
	
	@SuppressWarnings("unchecked")
	private String obtenerValorBeanRemoto(String beanRemoto,BigDecimal valor) throws SystemException{
		Map<BigDecimal,String> mapaValoresBeanRemoto = mapaAnidadoValoresBeanRemoto.get(beanRemoto);
		if(mapaValoresBeanRemoto == null){
			mapaValoresBeanRemoto = new HashMap<BigDecimal, String>();
			mapaAnidadoValoresBeanRemoto.put(beanRemoto, mapaValoresBeanRemoto);
		}
		String resultado = mapaValoresBeanRemoto.get(valor);
		if(resultado == null){
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			MidasInterfaceBase beanBase;
			try{
				beanBase = serviceLocator.getEJB(beanRemoto);
				resultado = beanBase.findById(valor).getDescription();
			}catch(NullPointerException e){
			}catch (ClassNotFoundException e) {
			}
			if(resultado != null){
				mapaValoresBeanRemoto.put(valor, resultado);
			}
		}
		return resultado;
	}
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public List<DatoIncisoCotizacionDTO> getListaDatosIncisoCotizacion() {
		return listaDatosIncisoCotizacion;
	}

	public void setListaDatosIncisoCotizacion(List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion) {
		this.listaDatosIncisoCotizacion = listaDatosIncisoCotizacion;
	}
	public Map<BigDecimal, SubTipoMaquinariaDTO> getMapaSubTipoMaquinaria() {
		return mapaSubTipoMaquinaria;
	}

	public void setMapaSubTipoMaquinaria(Map<BigDecimal, SubTipoMaquinariaDTO> mapaSubTipoMaquinaria) {
		this.mapaSubTipoMaquinaria = mapaSubTipoMaquinaria;
	}
	
	public Map<BigDecimal, SubtipoMontajeMaquinaDTO> getMapaSubTipoMontajeMaquina() {
		return mapaSubTipoMontajeMaquina;
	}

	public void setMapaSubTipoMontajeMaquina(Map<BigDecimal, SubtipoMontajeMaquinaDTO> mapaSubTipoMontajeMaquina) {
		this.mapaSubTipoMontajeMaquina = mapaSubTipoMontajeMaquina;
	}
	
	public Map<BigDecimal, SubtipoEquipoElectronicoDTO> getMapaSubTipoEquipoElectronico() {
		return mapaSubTipoEquipoElectronico;
	}

	public void setMapaSubTipoEquipoElectronico(Map<BigDecimal, SubtipoEquipoElectronicoDTO> mapaSubTipoEquipoElectronico) {
		this.mapaSubTipoEquipoElectronico = mapaSubTipoEquipoElectronico;
	}
	
	public Map<BigDecimal, SubtipoEquipoContratistaDTO> getMapaSubTipoEquipoContratista() {
		return mapaSubTipoEquipoContratista;
	}

	public void setMapaSubTipoEquipoContratista(Map<BigDecimal, SubtipoEquipoContratistaDTO> mapaSubTipoEquipoContratista) {
		this.mapaSubTipoEquipoContratista = mapaSubTipoEquipoContratista;
	}
	
	public Map<BigDecimal, TipoObraCivilDTO> getMapaSubTipoObraCivil() {
		return mapaSubTipoObraCivil;
	}

	public void setMapaSubTipoObraCivil(Map<BigDecimal, TipoObraCivilDTO> mapaSubTipoObraCivil) {
		this.mapaSubTipoObraCivil = mapaSubTipoObraCivil;
	}
}
