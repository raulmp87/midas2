package mx.com.afirme.midas.cotizacion.inciso;

import java.util.Date;

import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.sistema.MidasDynaBaseForm;

public class IncisoCotizacionForm extends MidasDynaBaseForm {

	private static final long serialVersionUID = 1L;
	private Date fechaOrdenTrabajo;
	private DireccionForm direccionGeneral;
	private String idCotizacion;
	private String[] datos;
	private String numeroInciso;
	private String numeroCopias;
	private String claveTipoEndoso;
	private String descripcionGiroAsegurado;
	private String cargaMasivaValida = "false";
	private String cargaMasivaExistente = "false";

	public void setFechaOrdenTrabajo(Date fechaOrdenTrabajo) {
		this.fechaOrdenTrabajo = fechaOrdenTrabajo;
	}

	public Date getFechaOrdenTrabajo() {
		return fechaOrdenTrabajo;
	}

	public void setDireccionGeneral(DireccionForm direccionGeneral) {
		this.direccionGeneral = direccionGeneral;
	}

	public DireccionForm getDireccionGeneral() {
		return direccionGeneral;
	}

	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public String getIdCotizacion() {
		return idCotizacion;
	}

	public void setDatos(String[] datos) {
		this.datos = datos;
	}

	public String[] getDatos() {
		return datos;
	}

	public void set(String name, int index, Object value) {
		super.set(name, index, value);
	}

	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroCopias(String numeroCopias) {
		this.numeroCopias = numeroCopias;
	}

	public String getNumeroCopias() {
		return numeroCopias;
	}

	public String getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(String claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public void setDescripcionGiroAsegurado(String descripcionGiroAsegurado) {
		this.descripcionGiroAsegurado = descripcionGiroAsegurado;
	}

	public String getDescripcionGiroAsegurado() {
		return descripcionGiroAsegurado;
	}

	public String getCargaMasivaValida() {
		return cargaMasivaValida;
	}

	public void setCargaMasivaValida(String cargaMasivaValida) {
		this.cargaMasivaValida = cargaMasivaValida;
	}

	public String getCargaMasivaExistente() {
		return cargaMasivaExistente;
	}

	public void setCargaMasivaExistente(String cargaMasivaExistente) {
		this.cargaMasivaExistente = cargaMasivaExistente;
	}
}
