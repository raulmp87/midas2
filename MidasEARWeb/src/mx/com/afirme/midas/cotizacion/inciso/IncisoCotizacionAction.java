package mx.com.afirme.midas.cotizacion.inciso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.cargaMasiva.CargaMasivaCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaDetalleCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.cargamasiva.CargaMasivaDetalleCotId;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.CargaMasivaIncisosExcel;
import mx.com.afirme.midas.sistema.excel.Dependencia;
import mx.com.afirme.midas.sistema.excel.Propiedad;
import mx.com.afirme.midas.sistema.excel.Validacion;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * MyEclipse Struts Creation date: 09-01-2009
 * 
 */
public class IncisoCotizacionAction extends MidasMappingDispatchAction {
	
	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm)form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			String id = request.getParameter("id");
			String origen = request.getParameter("origen");
			
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(id));
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			List<IncisoCotizacionDTO> incisos = cotizacionDN.listarIncisos(cotizacionDTO);
			request.setAttribute("incisos", incisos);
			HttpSession session = request.getSession();
			session.removeAttribute("idCotizacion");
			session.setAttribute("idCotizacion", new Integer(cotizacionDTO.getIdToCotizacion().intValue()));
			session.removeAttribute("fechaCreacion");
			session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());
			
			session.removeAttribute("origen");
			if(!UtileriasWeb.esCadenaVacia(origen)){
				session.setAttribute("origen", origen);
			}
			if(CargaMasivaCotizacionDN.getInstancia().existeCargaMasiva(cotizacionDTO.getIdToCotizacion()))
				incisoCotizacionForm.setCargaMasivaExistente("true");
			this.poblarForm(cotizacionDTO, incisoCotizacionForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarDatosInciso
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public void mostrarDatosInciso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// Se obtiene la iformacion de los diversos ramos de la ODT/COT
		// Se obtiene a traves de los ramos asociados al tipo de poliza de la
		// cotizacion
		try {
			String modificar = request.getParameter("modificar");
			Boolean soloLectura = request.getParameter("soloLectura").equals("1")? true : false;
			String disabled = soloLectura? "disabled" : "";
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("cotizacionId")));

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			// se obtienen la Informaci�n de Ramo-Inciso
			/*List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso = cotizacionDN.getDatosRamoInciso(cotizacionDTO);
			datosInciso.addAll(cotizacionDN.getDatosRiesgoInciso(cotizacionDTO));*/

			// se iteran los objetos obtenidos
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<datosinciso><![CDATA[");

			List<ConfiguracionDatoIncisoCotizacionDTO> configuracion = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			List<RamoTipoPolizaDTO> ramos = cotizacionDN.listarRamosCotizacion(cotizacionDTO);
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();

			int total = 0;
			Map<String,String> mapaIDsCascadeoHijos = new HashMap<String,String>();
			for(RamoTipoPolizaDTO ramo : ramos) {
				boolean trAbierto = false;
				datosInciso = cotizacionDN.getDatosRamoInciso(ramo);
				configuracion.addAll(datosInciso);
				if(!datosInciso.isEmpty()) {
					buffer.append("<div class=\"subtituloIzquierdaDiv\">Ramo: " + ramo.getRamoDTO().getDescripcion() + "</div>");
				}
				buffer.append("<table id= \"desplegarDetalle\">");
				for(int i = 0; i < datosInciso.size(); i++) {
//					if(i % 2 == 0 && trAbierto == true) {
//						buffer.append("</tr>");
//						trAbierto = false;
//					}
//					if(i % 2 == 0 && trAbierto == false) {
//						buffer.append("<tr>");
//						trAbierto = true;
//					}
					buffer.append("<tr>");
					trAbierto = true;
					
					ConfiguracionDatoIncisoCotizacionDTO dato = datosInciso.get(i);

					String value = "";
					if(modificar.equals("1")) {
						DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
						id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));
						id.setIdDato(dato.getId().getIdDato());
						id.setClaveDetalle(dato.getId().getClaveDetalle());
						id.setIdTcRamo(dato.getId().getIdTcRamo());
						id.setIdTcSubramo(dato.getId().getIdTcSubramo());
						id.setIdToCobertura(BigDecimal.ZERO);
						id.setIdToRiesgo(dato.getId().getIdToRiesgo());
						id.setIdToSeccion(BigDecimal.ZERO);
						id.setNumeroSubinciso(BigDecimal.ZERO);
						
						DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
						DatoIncisoCotizacionDTO valor = datoIncisoCotizacionSN.getPorId(id);
						if(valor != null) {
							value = valor.getValor();
						}
					}
					
					//buffer.append("<th class='normal' width='10%'>" + dato.getDescripcionEtiqueta() + ": </th>");
					Map atributos = null;
					String name = "datos[" + (total++) + "]";
					String id = "id" + total;
					String onblur = "";
					switch (dato.getClaveTipoValidacion()){
					case 1:	//cuota al millar, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 4)";
						break;
					case 2:	//porcentaje, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 2)";
						break;
					case 3:	//importe. validar 16 digitos con 2 decimales 
						onblur="validarDecimal(this.form."+id+", this.value, 16, 2)";
						break;
					case 4:	//factor, valodar 16 d�gitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 16, 4)";
						break;
					case 5:	//DSMGVDF, validar entero de 8 d�gitos
						onblur="validarDecimal(this.form."+id+", this.value, 8, 0)";
						break;
					}
					switch(dato.getClaveTipoControl()) {
					case 1:
						/**
						 * Hard Code de valores default para aviones
						 * en el giro y subgiro, la solucion es temporal
						 * y se requiere una solucion que contemple este
						 * escenario para todos los productos de midas.
						 * @author JORGEKNO
						 */
						if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getCodigo().equalsIgnoreCase("03") && !modificar.equals("1")){
							if(dato.getId().getIdDato().doubleValue() == 10D){
								value = "1603";
							}else if(dato.getId().getIdDato().doubleValue() == 20D){
								value = "1604";
							}
						}							
						buffer.append("<td colspan='2'><table width='100%'><tr>");
						buffer.append("<th style='font-size: 9px;' class='normal' width='100%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("</tr><tr>");
						buffer.append("<td width='100%'>" + IncisoCotizacionUtil.obtenerSelectCatalogo(name,dato.getDescripcionClaseRemota(), soloLectura, value,mapaIDsCascadeoHijos) + "</td>");
						buffer.append("</tr></table></td>");
						break;
					case 2:
						atributos = new HashMap();
						atributos.put("class", "cajaTexto");
						atributos.put("size", "");
						atributos.put("id", name);
						atributos.put("name",name);
						buffer.append("<td colspan='2'><table width='100%'><tr>");
						buffer.append("<th style='font-size: 9px;' width='100%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("</tr><tr>");
						buffer.append("<td width='100%'>" + IncisoCotizacionUtil.obtenerSelectValorFijo(atributos, dato.getIdGrupo(), soloLectura, value) + "</td>");
						buffer.append("</tr></table></td>");
						break;
					case 3:
						buffer.append("<th class='normal' width='25%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("<td width='75%'><input type=\"text\" class=\"cajaTexto\" style=\"text-transform: none;\" value='" + value + "' name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'/></td>");
						break;
					case 4:
						buffer.append("<th class='normal' width='25%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("<td width='75%'><textarea class=\"cajaTexto\" style=\"text-transform: none;\" name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'>" + value + "</textarea></td>");
						break;
					}
					buffer.append("</tr>");
					trAbierto = false;
				}
				if(trAbierto == true) {
					buffer.append("</tr>");
				}
				buffer.append("</table>");
			}
			List<BigDecimal> riesgos = new ArrayList<BigDecimal>();
			// FIX JEAS
			if(!modificar.equals("1") && !soloLectura) {
				riesgos = cotizacionDN.listarRiesgosCotizacion(cotizacionDTO);
			} else {
				riesgos = cotizacionDN.obtenerRiesgosContratadosCotizacion(cotizacionDTO);
			}
			for(BigDecimal idToRiesgo : riesgos) {
				boolean trAbierto = false;
				RiesgoDN riesgoDN = RiesgoDN.getInstancia();
				datosInciso = cotizacionDN.getDatosRiesgoInciso(idToRiesgo);
				configuracion.addAll(datosInciso);
				if(!datosInciso.isEmpty()) {
					buffer.append("<div class=\"subtituloIzquierdaDiv\">Riesgo: " + riesgoDN.getPorId(idToRiesgo).getDescripcion() + "</div>");
				}
				buffer.append("<table id= \"desplegarDetalle\">");
				for(int i = 0; i < datosInciso.size(); i++) {
//					if(i % 2 == 0 && trAbierto == true) {
//						buffer.append("</tr>");
//						trAbierto = false;
//					}
//					if(i % 2 == 0 && trAbierto == false) {
//						buffer.append("<tr>");
//						trAbierto = true;
//					}
					buffer.append("<tr>");
					trAbierto = true;
					
					ConfiguracionDatoIncisoCotizacionDTO dato = datosInciso.get(i);

					String value = "";
					if(modificar.equals("1")) {
						DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
						id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));
						id.setIdDato(dato.getId().getIdDato());
						id.setClaveDetalle(dato.getId().getClaveDetalle());
						id.setIdTcRamo(dato.getId().getIdTcRamo());
						id.setIdTcSubramo(dato.getId().getIdTcSubramo());
						id.setIdToCobertura(BigDecimal.ZERO);
						id.setIdToRiesgo(dato.getId().getIdToRiesgo());
						id.setIdToSeccion(BigDecimal.ZERO);
						id.setNumeroSubinciso(BigDecimal.ZERO);
						
						DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
						DatoIncisoCotizacionDTO valor = datoIncisoCotizacionSN.getPorId(id);
						if(valor != null) {
							value = valor.getValor();
						}
					}
					
					//buffer.append("<th class='normal' width='20%'>" + dato.getDescripcionEtiqueta() + ": </th>");
					Map atributos = null;
					String name = "datos[" + (total++) + "]";
					String id = "id" + total;
					String onblur = "";
					switch (dato.getClaveTipoValidacion()){
					case 1:	//cuota al millar, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 4)";
						break;
					case 2:	//porcentaje, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 2)";
						break;
					case 3:	//importe. validar 16 digitos con 2 decimales 
						onblur="validarDecimal(this.form."+id+", this.value, 16, 2)";
						break;
					case 4:	//factor, valodar 16 d�gitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 16, 4)";
						break;
					case 5:	//DSMGVDF, validar entero de 8 d�gitos
						onblur="validarDecimal(this.form."+id+", this.value, 8, 0)";
						break;
					}
					switch(dato.getClaveTipoControl()) {
					case 1:
//						atributos = new HashMap();
//						atributos.put("class", "cajaTexto");
//						atributos.put("size", "");
//						atributos.put("id", name);
//						atributos.put("name", name);
//						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectCatalogo(atributos, dato.getDescripcionClaseRemota(), soloLectura, value) + "</td>");
						buffer.append("<td colspan='2'><table width='100%'><tr>");
						buffer.append("<th style='font-size: 9px;' class='normal' width='100%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("</tr><tr>");
						buffer.append("<td width='100%'>" + IncisoCotizacionUtil.obtenerSelectCatalogo(name,dato.getDescripcionClaseRemota(), soloLectura, value,mapaIDsCascadeoHijos) + "</td>");
						buffer.append("</tr></table></td>");
						break;
					case 2:
						atributos = new HashMap();
						atributos.put("class", "cajaTexto");
						atributos.put("size", "");
						atributos.put("id", name);
						atributos.put("name",name);
						buffer.append("<td colspan='2'><table width='100%'><tr>");
						buffer.append("<th style='font-size: 9px;' class='normal' width='100%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("</tr><tr>");
						buffer.append("<td width='100%'>" + IncisoCotizacionUtil.obtenerSelectValorFijo(atributos, dato.getIdGrupo(), soloLectura, value) + "</td>");
						buffer.append("</tr></table></td>");
						break;
					case 3:
						buffer.append("<th class='normal' width='25%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("<td width='75%'><input type=\"text\" class=\"cajaTexto\" style=\"text-transform: none;\" value='" + value + "' name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'/></td>");
						break;
					case 4:
						buffer.append("<th class='normal' width='25%'>" + dato.getDescripcionEtiqueta() + ": </th>");
						buffer.append("<td width='75%'><textarea class=\"cajaTexto\" style=\"text-transform: none;\" name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'>" + value + "</textarea></td>");
						break;
					}
					buffer.append("</tr>");
					trAbierto = false;
				}
				if(trAbierto == true) {
					buffer.append("</tr>");
				}
				buffer.append("</table>");
			}

			buffer.append("]]></datosinciso>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
			
			HttpSession session = request.getSession();
			session.removeAttribute("configuracion");
			session.setAttribute("configuracion", configuracion);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	private void poblarForm(CotizacionDTO cotizacionDTO,
			IncisoCotizacionForm incisoCotizacionForm) {
		if(cotizacionDTO.getIdToCotizacion() != null) {
			incisoCotizacionForm.setIdCotizacion(cotizacionDTO.getIdToCotizacion().toString());
		}
		if(cotizacionDTO.getFechaCreacion() != null) {
			incisoCotizacionForm.setFechaOrdenTrabajo(cotizacionDTO.getFechaCreacion());
		}
	}

	public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm)form;
		incisoCotizacionForm.setDireccionGeneral(new DireccionForm());
		HttpSession session = request.getSession();
		Date fechaCreacion = (Date)session.getAttribute("fechaCreacion");
		incisoCotizacionForm.setFechaOrdenTrabajo(fechaCreacion);
		incisoCotizacionForm.setIdCotizacion(session.getAttribute("idCotizacion").toString());
		incisoCotizacionForm.set("idCotizacion", session.getAttribute("idCotizacion").toString());

		try{
			BigDecimal numeroInciso = IncisoCotizacionDN.getInstancia().obtenerNumeroIncisoSiguiente(UtileriasWeb.regresaBigDecimal(incisoCotizacionForm.getIdCotizacion()));
			if(numeroInciso.compareTo(BigDecimal.ONE) == 0) {
				CotizacionDN  cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(incisoCotizacionForm.getIdCotizacion()));
				if (cotizacionDTO.getIdToPersonaAsegurado() != null && cotizacionDTO.getIdToPersonaAsegurado().doubleValue() > 0){
					ClienteDTO asegurado = null;
					try {
						if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
							asegurado = new ClienteDTO();
							asegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
							asegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
							asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, UtileriasWeb.obtieneNombreUsuario(request));
						}else{
							asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
						}						
					} catch (Exception exc) {
					}
					if (asegurado != null){
						ServiceLocator serviceLocator = ServiceLocator.getInstance();
						ColoniaFacadeRemote beanRemoto = serviceLocator.getEJB(ColoniaFacadeRemote.class);
						List<ColoniaDTO> colonias = beanRemoto.findByProperty("zipCode", asegurado.getCodigoPostal().trim());
						if(!colonias.isEmpty()) {
							DireccionDTO direccionDTO = new DireccionDTO();

							CiudadFacadeRemote ciudadRemoto = serviceLocator.getEJB(CiudadFacadeRemote.class);
							CiudadDTO ciudad = ciudadRemoto.findById(colonias.iterator().next().getCityId().trim());
							if(ciudad != null) {
								direccionDTO.setIdMunicipio(UtileriasWeb.regresaBigDecimal(ciudad.getCityId().trim()));

								EstadoFacadeRemote estadoRemoto = serviceLocator.getEJB(EstadoFacadeRemote.class);
								EstadoDTO estado = estadoRemoto.findById(ciudad.getStateId().trim());
								if(estado != null) {
									direccionDTO.setIdEstado(UtileriasWeb.regresaBigDecimal(estado.getStateId()));
								}
							}
							direccionDTO.setNombreCalle(asegurado.getNombreCalle());
							direccionDTO.setNumeroExterior(asegurado.getNumeroExterior() != null? asegurado.getNumeroExterior().toBigInteger().toString() : "0");
							direccionDTO.setNumeroInterior(asegurado.getNumeroInterior());
							for(ColoniaDTO colonia : colonias) {
								if(colonia.getColonyName().contains(asegurado.getNombreColonia().trim())) {
									direccionDTO.setNombreColonia(colonia.getColonyId());
									break;
								}
							}
							direccionDTO.setCodigoPostal(UtileriasWeb.regresaBigDecimal(asegurado.getCodigoPostal().trim()));
							direccionDTO.setEntreCalles(asegurado.getEntreCalles());
							
							direccionDTO = DireccionDN.getInstancia().agregar(direccionDTO);
							
							DireccionForm direccionForm = new DireccionForm();
							DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
							incisoCotizacionForm.set("direccionGeneral", direccionForm);
						}
					}
				}
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	@SuppressWarnings("unchecked")
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
			String idCotizacion = (String)incisoCotizacionForm.get("idCotizacion");
			DireccionForm direccionForm = (DireccionForm)incisoCotizacionForm.get("direccionGeneral");
			String descripcionGiroAsegurado = (String)incisoCotizacionForm.get("descripcionGiroAsegurado");
			
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idCotizacion));
			cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(cotizacionDTO);
			BigDecimal idToDireccion = UtileriasWeb.regresaBigDecimal(direccionForm.getIdToDireccion());
			HttpSession session = request.getSession();
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso = (List<ConfiguracionDatoIncisoCotizacionDTO>) session.getAttribute("configuracion");
			String[] datos = (String[])incisoCotizacionForm.get("datos");
			String[] temp = new String[datos.length + 1];
			temp[datos.length] = descripcionGiroAsegurado;
			System.arraycopy(datos, 0, temp, 0, datos.length);
			datos = temp;

			Map<String, String> mensaje = incisoCotizacionDN.agregarInciso(cotizacionDTO, idToDireccion, datosInciso, datos, UtileriasWeb.obtieneNombreUsuario(request));
			incisoCotizacionForm.set("mensaje", mensaje.get("mensaje"));
			incisoCotizacionForm.set("tipoMensaje", mensaje.get("tipoMensaje"));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarBorrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
			IncisoCotizacionId id = new IncisoCotizacionId();
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
			id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));

			IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
			incisoCotizacionDTO.setId(id);

			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			incisoCotizacionDTO = incisoCotizacionDN.getPorId(incisoCotizacionDTO);

			this.poblarForm(incisoCotizacionDTO, incisoCotizacionForm);
			HttpSession session = request.getSession();
			Date fechaCreacion = (Date)session.getAttribute("fechaCreacion");
			incisoCotizacionForm.setFechaOrdenTrabajo(fechaCreacion);
		} catch (SystemException e) {
			//mensajeExcepcion((IncisoCotizacionForm)form, e);
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
			String idCotizacion = (String)incisoCotizacionForm.get("idCotizacion");
			String numeroInciso = (String)incisoCotizacionForm.get("numeroInciso");

			IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
			incisoCotizacionId.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idCotizacion));
			incisoCotizacionId.setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso.toString()));
			IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
			incisoCotizacionDTO.setId(incisoCotizacionId);
			
			incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacionDTO);

			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			Map<String, String> mensaje = incisoCotizacionDN.borrarInciso(incisoCotizacionDTO);

			DireccionDTO direccion = incisoCotizacionDTO.getDireccionDTO();
			DireccionDN.getInstancia().borrar(direccion);

			
			incisoCotizacionForm.set("mensaje", mensaje.get("mensaje"));
			incisoCotizacionForm.set("tipoMensaje", mensaje.get("tipoMensaje"));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
			IncisoCotizacionId id = new IncisoCotizacionId();
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
			id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));

			IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
			incisoCotizacionDTO.setId(id);

			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			incisoCotizacionDTO = incisoCotizacionDN.getPorId(incisoCotizacionDTO);

			this.poblarForm(incisoCotizacionDTO, incisoCotizacionForm);
			HttpSession session = request.getSession();
			Date fechaCreacion = (Date)session.getAttribute("fechaCreacion");
			incisoCotizacionForm.setFechaOrdenTrabajo(fechaCreacion);
		} catch (SystemException e) {
			//mensajeExcepcion((IncisoCotizacionForm)form, e);
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarForm(IncisoCotizacionDTO incisoCotizacionDTO,
			IncisoCotizacionForm incisoCotizacionForm) throws ExcepcionDeAccesoADatos, SystemException {

		DireccionDTO direccionDTO = incisoCotizacionDTO.getDireccionDTO();
		DireccionForm direccionForm = new DireccionForm();
		DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
		incisoCotizacionForm.set("direccionGeneral", direccionForm);

		if(incisoCotizacionDTO.getId().getIdToCotizacion() != null) {
			incisoCotizacionForm.set("idCotizacion", incisoCotizacionDTO.getId().getIdToCotizacion().toString());
		}
		if(incisoCotizacionDTO.getId().getNumeroInciso() != null) {
			incisoCotizacionForm.set("numeroInciso", incisoCotizacionDTO.getId().getNumeroInciso().toString());
		}
		if(incisoCotizacionDTO.getDescripcionGiroAsegurado() != null) {
			incisoCotizacionForm.set("descripcionGiroAsegurado", incisoCotizacionDTO.getDescripcionGiroAsegurado());
		}
	}

	@SuppressWarnings("unchecked")
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
		try {
			String idCotizacion = (String)incisoCotizacionForm.get("idCotizacion");
			String numeroInciso = (String)incisoCotizacionForm.get("numeroInciso");
			String descripcionGiroAsegurado = (String)incisoCotizacionForm.get("descripcionGiroAsegurado");

			IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
			IncisoCotizacionId idInciso = new IncisoCotizacionId();
			idInciso.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idCotizacion));
			idInciso.setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso));
			incisoCotizacionDTO.setId(idInciso);
			incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacionDTO);
			incisoCotizacionDTO.setDescripcionGiroAsegurado(descripcionGiroAsegurado);
			IncisoCotizacionDN.getInstancia().modificar(incisoCotizacionDTO);

			HttpSession session = request.getSession();
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso = (List<ConfiguracionDatoIncisoCotizacionDTO>) session.getAttribute("configuracion");
			String[] datos = (String[])incisoCotizacionForm.get("datos");
			for(int i = 0; i < datos.length; i++) {
				if(datos[i] != null) {
					ConfiguracionDatoIncisoCotizacionDTO configuracion = datosInciso.get(i);
					
					DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
					id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idCotizacion));
					id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso));
					id.setIdDato(configuracion.getId().getIdDato());
					id.setClaveDetalle(configuracion.getId().getClaveDetalle());
					id.setIdTcRamo(configuracion.getId().getIdTcRamo());
					id.setIdTcSubramo(configuracion.getId().getIdTcSubramo());
					id.setIdToCobertura(BigDecimal.ZERO);
					id.setIdToRiesgo(configuracion.getId().getIdToRiesgo());
					id.setIdToSeccion(BigDecimal.ZERO);
					id.setNumeroSubinciso(BigDecimal.ZERO);

					DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
					datoIncisoCotizacionDTO.setId(id);
					datoIncisoCotizacionDTO.setValor(datos[i]);

					DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
					datoIncisoCotizacionSN.modificar(datoIncisoCotizacionDTO);
				}
			}
			incisoCotizacionForm.set("mensaje", "La informaci&oacute;n se registr&oacute; exitosamente.");
			incisoCotizacionForm.set("tipoMensaje", "30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			incisoCotizacionForm.set("mensaje", "Error al modificar Inciso");
			incisoCotizacionForm.set("tipoMensaje", "10");
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			incisoCotizacionForm.set("mensaje", "Error al modificar Inciso");
			incisoCotizacionForm.set("tipoMensaje", "10");
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
			IncisoCotizacionId id = new IncisoCotizacionId();
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
			id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));

			IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
			incisoCotizacionDTO.setId(id);

			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			incisoCotizacionDTO = incisoCotizacionDN.getPorId(incisoCotizacionDTO);

			this.poblarForm(incisoCotizacionDTO, incisoCotizacionForm);
			HttpSession session = request.getSession();
			Date fechaCreacion = (Date)session.getAttribute("fechaCreacion");
			incisoCotizacionForm.setFechaOrdenTrabajo(fechaCreacion);
		} catch (SystemException e) {
			//mensajeExcepcion((IncisoCotizacionForm)form, e);
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarUbicacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm)form;
			String idDireccion = request.getParameter("idDireccion");
			DireccionDN direccionDN = DireccionDN.getInstancia();
			DireccionDTO direccionDTO = direccionDN.getPorId(UtileriasWeb.regresaBigDecimal(idDireccion));
			DireccionForm direccionForm = new DireccionForm();
			DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
			incisoCotizacionForm.setDireccionGeneral(direccionForm);
			incisoCotizacionForm.set("direccionGeneral", direccionForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method copiar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward copiar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm)form;
			String numeroCopias = (String) incisoCotizacionForm.get("numeroCopias");
			if(!UtileriasWeb.esCadenaVacia(numeroCopias)) {
				Integer numeroCopiasInt = Integer.parseInt(numeroCopias);

				String maximoCopias = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.copiasMaximas");
				if (UtileriasWeb.esCadenaVacia(maximoCopias))
					maximoCopias = ""+Sistema.MAXIMO_COPIAS_INCISO;
				int maximoCopiasInt = Integer.valueOf(maximoCopias).intValue();
				
				BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal((String) incisoCotizacionForm.get("idCotizacion"));
				BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal((String) incisoCotizacionForm.get("numeroInciso"));
				
				if (numeroCopiasInt < maximoCopiasInt){
					
					Map<String, String> mensaje = IncisoCotizacionDN.getInstancia().copiarInciso(idToCotizacion, numeroInciso, numeroCopiasInt);
					
//					IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
//					IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
//					IncisoCotizacionId id = new IncisoCotizacionId();
//					id.setIdToCotizacion(idToCotizacion);
//					id.setNumeroInciso(numeroInciso);
//					incisoCotizacionDTO.setId(id);
//					
//					Map<String, String> mensaje = incisoCotizacionDN.copiarInciso(cotizacionDTO, incisoCotizacionDTO, numeroCopiasInt);
					incisoCotizacionForm.set("mensaje", mensaje.get("mensaje"));
					incisoCotizacionForm.set("tipoMensaje", mensaje.get("tipoMensaje"));
				}
				else{
					incisoCotizacionForm.set("mensaje", "La cantidad m&aacute;xima de copias es "+maximoCopias+".");
					incisoCotizacionForm.set("tipoMensaje", "10");
				}
				String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
				
				CotizacionDTO cotizacionDTO = new CotizacionDTO();
				cotizacionDTO.setIdToCotizacion(idToCotizacion);
				CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				List<IncisoCotizacionDTO> incisos = CotizacionDN.getInstancia(nombreUsuario).listarIncisos(idToCotizacion);
				request.setAttribute("incisos", incisos);
				HttpSession session = request.getSession();
				session.removeAttribute("idCotizacion");
				session.setAttribute("idCotizacion", new Integer(idToCotizacion.intValue()));
				session.removeAttribute("fechaCreacion");
				session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());
				this.poblarForm(cotizacionDTO, incisoCotizacionForm);
			} else {
				reglaNavegacion = Sistema.ALTERNO;
				incisoCotizacionForm.setIdCotizacion(request.getParameter("idCotizacion"));
				incisoCotizacionForm.setNumeroInciso(request.getParameter("numeroInciso"));
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void calcularRiesgosHidro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String numeroInciso = request.getParameter("numeroInciso");		
		try {
			CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
			calculoCotizacionDN.calcularRiesgosHidro(UtileriasWeb.regresaBigDecimal(idToCotizacion), 
					UtileriasWeb.regresaBigDecimal(numeroInciso), UtileriasWeb.obtieneNombreUsuario(request));

			List<CoberturaCotizacionDTO> coberturas = CoberturaCotizacionDN.getInstancia().listarCoberturasBasicasContratadas(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(numeroInciso));
			for(CoberturaCotizacionDTO cobertura : coberturas) {
				if(cobertura.getCoberturaSeccionDTO().getSeccionDTO().getClaveSubIncisos().shortValue() == Sistema.PERMITE_SUBINCISOS) {
					calculoCotizacionDN.calcularARD(cobertura.getId(), UtileriasWeb.obtieneNombreUsuario(request));
				} else {
					calculoCotizacionDN.calcularCobertura(cobertura, UtileriasWeb.obtieneNombreUsuario(request), false);
				}
			}
		} catch (Exception e) {
			/*
			 * Esta excepcion se le elimina por que asi esta definido el SP
			 * pcursorCalculaRiesgo OUT curGeneral, -- (OUT) Cursor de Respuesta (Siempre NULL)
			 */
			System.out.println("Se ejecuto calcularRiesgosHidro para inciso:"+numeroInciso + " cotizacion:"+idToCotizacion);
		}
	}

	public ActionForward cargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToControlArchivo = request.getParameter("idToControlArchivo");
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
		String mensaje = "";
		if(!UtileriasWeb.esCadenaVacia(idToControlArchivo)) {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			try {
				ControlArchivoDTO controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idToControlArchivo));
				CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(idToCotizacion));
				
				CargaMasivaIncisosExcel<DireccionDTO> excel = new CargaMasivaIncisosExcel<DireccionDTO>(DireccionDTO.class);
				excel.setIdToCotizacion(idToCotizacion);
				excel.setArchivo(controlArchivoDTO);
				if(excel.isValid()) {
					Propiedad propiedad = new Propiedad(0, "nombreCalle");
					propiedad.setDescripcion("Calle");
					Validacion validacion = new Validacion(TipoValidacion.ALFANUMERICO);
					propiedad.addValidacion(validacion);
					validacion = new Validacion(TipoValidacion.LONGITUD_MAXIMA, "100");
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);

					propiedad = new Propiedad(1, "numeroExterior");
					propiedad.setDescripcion("Numero Exterior");
					validacion = new Validacion(TipoValidacion.ALFANUMERICO);
					propiedad.addValidacion(validacion);
					validacion = new Validacion(TipoValidacion.LONGITUD_MAXIMA, "10");
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);

					propiedad = new Propiedad(2, "numeroInterior");
					propiedad.setDescripcion("Numero Interior");
					validacion = new Validacion(TipoValidacion.ALFANUMERICO);
					propiedad.addValidacion(validacion);
					validacion = new Validacion(TipoValidacion.LONGITUD_MAXIMA, "10");
					propiedad.addValidacion(validacion);
					validacion = new Validacion(TipoValidacion.NO_REQUERIDO);
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);

					propiedad = new Propiedad(3, "idEstado");
					propiedad.setDescripcion("Estado");
					validacion = new Validacion(TipoValidacion.EXISTENCIA);
					Dependencia dependencia = new Dependencia();
					dependencia.setClaseRemota("mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote");
					dependencia.setMetodo("findByProperty");
					dependencia.addParametro("stateName");
					dependencia.addTipoPametros(String.class, Object.class);
					dependencia.setMapeadoPor("getStateId");
					validacion.addDependencia(dependencia);
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);

					propiedad = new Propiedad(5, "codigoPostal");
					propiedad.setDescripcion("Codigo Postal");
					validacion = new Validacion(TipoValidacion.NUMERICO);
					propiedad.addValidacion(validacion);
					validacion = new Validacion(TipoValidacion.LONGITUD_MAXIMA, "99999");
					propiedad.addValidacion(validacion);
					validacion = new Validacion(TipoValidacion.EXISTENCIA);
					dependencia = new Dependencia();
					dependencia.setClaseRemota("mx.com.afirme.midas.interfaz.codigopostalcolonia.CodigoPostalColoniaFacadeRemote");
					dependencia.setMetodo("findByZipCode");
					dependencia.setMapeadoPor("getCodigoPostal");
					validacion.addDependencia(dependencia);
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);
					
					propiedad = new Propiedad(4, "idMunicipio");
					propiedad.setDescripcion("Ciudad");
					validacion = new Validacion(TipoValidacion.EXISTENCIA);
					dependencia = new Dependencia();
					dependencia.setClaseRemota("mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote");
					dependencia.setMetodo("findByCityName");
					dependencia.addParametroFromT("getIdEstado");
					dependencia.setMapeadoPor("getCityId");
					validacion.addDependencia(dependencia);
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);

					propiedad = new Propiedad(6, "nombreColonia");
					propiedad.setDescripcion("Colonia");
					validacion = new Validacion(TipoValidacion.EXISTENCIA);
					dependencia = new Dependencia();
					dependencia.setClaseRemota("mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote");
					dependencia.setMetodo("findByColonyName");
					dependencia.addParametroFromT("getIdMunicipio");
					dependencia.addParametroFromT("getCodigoPostal");
					dependencia.setMapeadoPor("getColonyId");
					validacion.addDependencia(dependencia);
					propiedad.addValidacion(validacion);
					excel.addPropiedad(propiedad);
					excel.getRegistrosInvalidos();
					
					//Paso 2 Se insertan los detalles de la carga masiva					
					List<DireccionDTO> direccionesValidas = excel.getRegistrosValidos();
					List<DireccionDTO> direccionesInvalidas = excel.getRegistrosInvalidos();
					
					for(DireccionDTO direccion : direccionesValidas) {
						int index = direccionesValidas.indexOf(direccion);
						String sumas = excel.getSumasAseguradas().get(index);
						direccion.setSumasAseguradas(sumas);
						direccion.setMensajeError("");
					}
					for(DireccionDTO direccion : direccionesInvalidas) {
						int index = direccionesInvalidas.indexOf(direccion);
						String sumas = excel.getSumasAseguradasInvalidas().get(index);
						direccion.setSumasAseguradas(sumas);
						String errores = excel.getRegistrosInvalidosMensaje().get(index);
						direccion.setMensajeError(errores);
					}
					//Paso 1 Se inserta la cabecera de la carga Masiva
					CargaMasivaCotDTO cargaMasivaCotDTO = new CargaMasivaCotDTO(
							UtileriasWeb.regresaBigDecimal(excel
									.getIdToCotizacion()),
							UtileriasWeb.regresaBigDecimal(excel
									.getNumeroInciso()), excel
									.getClavesSeccionRiesgo());

					cargaMasivaCotDTO = CargaMasivaCotizacionDN.getInstancia()
							.agregar(cargaMasivaCotDTO, direccionesValidas,
									direccionesInvalidas);

					if(!excel.getRegistrosInvalidosMensaje().isEmpty()) {
						mensaje += "Se realiz&oacute; la carga del archivo. <br>Se econtraron los siguientes errores:<br>";
						StringBuilder mensajeError = new StringBuilder(mensaje);
						for(String error : excel.getRegistrosInvalidosMensaje()) {
							mensajeError.append(error).append("<br>");
						}
						mensaje=mensajeError.toString();
					}
					if(mensaje == null || mensaje.length()<=1){
						mensaje += "Ocurri&oacute; un error al procesar el archivo, intente nuevamente";
						incisoCotizacionForm.set("mensaje", mensaje);
						incisoCotizacionForm.set("tipoMensaje", "10");
					}else{
						incisoCotizacionForm.set("mensaje", mensaje);
						incisoCotizacionForm.set("tipoMensaje", "30");						
					}
				} else {
					incisoCotizacionForm.set("mensaje", "El archivo no es valido.");
					incisoCotizacionForm.set("tipoMensaje", "10");
				}

				List<IncisoCotizacionDTO> incisos = cotizacionDN.listarIncisos(cotizacionDTO);
				request.setAttribute("incisos", incisos);
				HttpSession session = request.getSession();
				session.removeAttribute("idCotizacion");
				session.setAttribute("idCotizacion", new Integer(cotizacionDTO.getIdToCotizacion().intValue()));
				session.removeAttribute("fechaCreacion");
				session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());
				String origen = request.getParameter("origen");
				session.removeAttribute("origen");
				if(!UtileriasWeb.esCadenaVacia(origen)){
					session.setAttribute("origen", origen);
				}
				if(CargaMasivaCotizacionDN.getInstancia().existeCargaMasiva(cotizacionDTO.getIdToCotizacion()))
					incisoCotizacionForm.setCargaMasivaExistente("true");				
				this.poblarForm(cotizacionDTO, incisoCotizacionForm);
			} catch (SystemException e) {
				mensaje += "Ocurri&oacute; un error al procesar el archivo, intente nuevamente";
				incisoCotizacionForm.set("mensaje", mensaje);
				incisoCotizacionForm.set("tipoMensaje", "10");
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (FileNotFoundException e) {
				mensaje += "Ocurri&oacute; un error al procesar el archivo, Error en el Archivo";
				incisoCotizacionForm.set("mensaje", mensaje);
				incisoCotizacionForm.set("tipoMensaje", "10");				
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (IOException e) {
				mensaje += "Ocurri&oacute; un error al procesar el archivo, Error al leer el archivo";
				incisoCotizacionForm.set("mensaje", mensaje);
				incisoCotizacionForm.set("tipoMensaje", "10");								
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward procesarCargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("idToCotizacion");
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);		
		
		if(!UtileriasWeb.esCadenaVacia(idToCotizacion)) {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			List<IncisoCotizacionDTO> incisos = new ArrayList<IncisoCotizacionDTO>();
			try {
				cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(idToCotizacion));
				CargaMasivaCotizacionDN.getInstancia().procesarCargaMasiva(cotizacionDTO.getIdToCotizacion(), nombreUsuario);
				incisos = cotizacionDN.listarIncisos(cotizacionDTO);
				incisoCotizacionForm.set("mensaje", "Se proceso correctamente la carga masiva.");
				incisoCotizacionForm.set("tipoMensaje", "30");				
			} catch (SystemException e) {
				incisoCotizacionForm.set("mensaje", "Ocurri&oacute; un error al procesar la carga masiva, por favor intertarlo nuevamente.");
				incisoCotizacionForm.set("tipoMensaje", "10");				
			}
			
			request.setAttribute("incisos", incisos);
			HttpSession session = request.getSession();
			session.removeAttribute("idCotizacion");
			session.setAttribute("idCotizacion", new Integer(cotizacionDTO.getIdToCotizacion().intValue()));
			session.removeAttribute("fechaCreacion");
			session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());
			String origen = request.getParameter("origen");
			session.removeAttribute("origen");
			if(!UtileriasWeb.esCadenaVacia(origen)){
				session.setAttribute("origen", origen);
			}
			this.poblarForm(cotizacionDTO, incisoCotizacionForm);			
			
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void getCargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String idToCotizacion = request.getParameter("idToCotizacion");
		if (!UtileriasWeb.esCadenaVacia(idToCotizacion)) {
			try {
				String json = new String();
				json = CargaMasivaCotizacionDN.getInstancia()
						.getJsonDetalleCargaMasiva(
								UtileriasWeb.regresaBigDecimal(idToCotizacion));
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (IOException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
		}
	}

	public void generarTemplateCargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String numeroInciso = request.getParameter("numeroInciso");
		HSSFWorkbook workbook = CargaMasivaIncisosExcel.generarTemplateCargaMasiva(UtileriasWeb.regresaBigDecimal(idToCotizacion), UtileriasWeb.regresaBigDecimal(numeroInciso));
		
		String userAgent = request.getHeader("User-Agent");
		if (userAgent.indexOf("MSIE") != -1) { // IE
			response.setHeader("Content-Disposition", "attachment; filename="
					+ URLEncoder.encode("Template" + idToCotizacion + "-" + numeroInciso + ".xls", "ISO-8859-1"));
		} else { // Mozilla, Firefox...
			response.setHeader("Content-Disposition", "attachment; filename="
					+ URLEncoder.encode("Template" + idToCotizacion + "-" + numeroInciso + ".xls", "ISO-8859-1"));
		}
		OutputStream outputStream = response.getOutputStream();
		String contentType = "application/unknown";
		response.setContentType(contentType);
		response.setBufferSize(1024 * 15);
		workbook.write(outputStream);
		outputStream.flush();
		outputStream.close();
	}
	
	public ActionForward mostrarCargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm)form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			String id = request.getParameter("id");
			String origen = request.getParameter("origen");
			
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(id));
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO.getIdToCotizacion());
			HttpSession session = request.getSession();
			session.removeAttribute("idCotizacion");
			session.setAttribute("idCotizacion", new Integer(cotizacionDTO.getIdToCotizacion().intValue()));
			session.removeAttribute("fechaCreacion");
			session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());	
			
			session.removeAttribute("origen");
			if(!UtileriasWeb.esCadenaVacia(origen)){
				session.setAttribute("origen", origen);
			}
			if(CargaMasivaCotizacionDN.getInstancia().cargaMasivaValida(cotizacionDTO.getIdToCotizacion()))
				incisoCotizacionForm.setCargaMasivaValida("true");
			this.poblarForm(cotizacionDTO, incisoCotizacionForm);			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void guardarDetalleCargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {
		String gr_id = request.getParameter("gr_id");
		String idToCotizacionStr = request.getParameter("idToCotizacion");
		String numeroIncisoStr = request.getParameter("numeroInciso");
		String nombreCalle = request.getParameter("nombreCalle");
		String numeroExterior = request.getParameter("numeroExterior");
		String numeroInterior = request.getParameter("numeroInterior");
		String codigoPostal = request.getParameter("codigoPostal");
		String nombreColoniaValida = request.getParameter("nombreColoniaValida");
		String nombreEstado = request.getParameter("nombreEstado");
		String nombreMunicipio = request.getParameter("nombreMunicipio");
		String action = "update";
		try{
			if(!UtileriasWeb.esCadenaVacia(idToCotizacionStr)){
				BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(idToCotizacionStr);
				BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(numeroIncisoStr);
				if(idToCotizacion != null && numeroInciso !=null){
					CargaMasivaDetalleCotId id = new CargaMasivaDetalleCotId(idToCotizacion, numeroInciso);
					CargaMasivaDetalleCotDTO detalle = new CargaMasivaDetalleCotDTO();
					detalle.setId(id);
					detalle.setNombreCalle(nombreCalle);
					detalle.setNumeroExterior(numeroExterior);
					detalle.setNumeroInterior(numeroInterior);
					detalle.setCodigoPostal(codigoPostal);
					detalle.setIdColonia(nombreColoniaValida);
					detalle.setNombreEstadoValido(nombreEstado);
					String muniTmp[] = nombreMunicipio.split("<br>");
					detalle.setNombreMunicipioValido(muniTmp.length > 1? muniTmp[1]: muniTmp[0]);
					CargaMasivaCotizacionDN.getInstancia().actualizaDetalle(detalle);
				}
				HttpSession session = request.getSession();
				String origen = request.getParameter("origen");
				session.removeAttribute("idCotizacion");
				session.setAttribute("idCotizacion", new Integer(idToCotizacion.intValue()));
				session.removeAttribute("origen");
				if(!UtileriasWeb.esCadenaVacia(origen)){
					session.setAttribute("origen", origen);
				}			
			}
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}				
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + gr_id + "\" tid=\"" + gr_id + "\" /></data>");
		pw.flush();
		pw.close();				 
	}
	
	public ActionForward eliminarCargaMasiva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {
		String idToCotizacionStr = request.getParameter("idToCotizacion");
		String reglaNavegacion = Sistema.EXITOSO;
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
		try{
			if(!UtileriasWeb.esCadenaVacia(idToCotizacionStr)){
				BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(idToCotizacionStr);
				CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);			
				CargaMasivaCotDTO cargaMasiva = CargaMasivaCotizacionDN.getInstancia().getCargaMasiva(idToCotizacion);
				if(cargaMasiva != null){
					CargaMasivaCotizacionDN.getInstancia().eliminarCargaMasiva(cargaMasiva);
				}
				incisoCotizacionForm.set("mensaje", "La informaci&oacute;n se registr&oacute; exitosamente.");
				incisoCotizacionForm.set("tipoMensaje", "30");			
				List<IncisoCotizacionDTO> incisos = cotizacionDN.listarIncisos(cotizacionDTO);
				request.setAttribute("incisos", incisos);
				HttpSession session = request.getSession();
				session.removeAttribute("idCotizacion");
				session.setAttribute("idCotizacion", new Integer(cotizacionDTO.getIdToCotizacion().intValue()));
				session.removeAttribute("fechaCreacion");
				session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());
				String origen = request.getParameter("origen");
				session.removeAttribute("origen");
				if(!UtileriasWeb.esCadenaVacia(origen)){
					session.setAttribute("origen", origen);
				}
				this.poblarForm(cotizacionDTO, incisoCotizacionForm);							
			}
		} catch (SystemException e) {
			incisoCotizacionForm.set("mensaje", "Ocurri&oacute; un error al eliminar la carga masiva, por favor intertarlo nuevamente.");
			incisoCotizacionForm.set("tipoMensaje", "10");				
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
			incisoCotizacionForm.set("mensaje", "Ocurri&oacute; un error al eliminar la carga masiva, por favor intertarlo nuevamente.");
			incisoCotizacionForm.set("tipoMensaje", "10");				
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
			
		return mapping.findForward(reglaNavegacion);
	}
}