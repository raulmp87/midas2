package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.igualacion.IgualacionPrimaNetaForm;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotSN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SeccionCotizacionDN {
	public static final SeccionCotizacionDN INSTANCIA = new SeccionCotizacionDN();

	public static SeccionCotizacionDN getInstancia() {
		return SeccionCotizacionDN.INSTANCIA;
	}
	/**
	 * Method listarPorIncisoId().
	 * @param idToInciso
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<SeccionCotizacionDTO> listarPorIncisoId(IncisoCotizacionId idToInciso) throws ExcepcionDeAccesoADatos,SystemException {
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		return seccionCotizacionSN.listarPorIncisoCotizacionId(idToInciso);
	}
	/**
	 * Method listarSeccionesPrimerRiesgo().
	 * @param idToCotizacion
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<SeccionCotizacionDTO> listarSeccionesPrimerRiesgo(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,SystemException {
		List<SeccionCotizacionDTO> secciones = new SeccionCotizacionSN().listarSeccionesPrimerRiesgo(idToCotizacion);
		return secciones;
	}
	/**
	 * Method  listarSeccionesLUC().
	 * @param idToCotizacion
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<SeccionCotizacionDTO> listarSeccionesLUC(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,SystemException {
		return new SeccionCotizacionSN().listarSeccionesLUC(idToCotizacion);
	}
	/**
	 * Method listarSeccionesContratadas().
	 * @param idToCotizacion
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException  
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadas(BigDecimal idToCotizacion) throws SystemException, ExcepcionDeAccesoADatos {
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		List<SeccionCotizacionDTO> secciones = seccionCotizacionSN.listarSeccionesContratadas(idToCotizacion);
		SeccionSN seccionSN = new SeccionSN();
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for(SeccionCotizacionDTO seccion : secciones) {
			seccion.setSeccionDTO(seccionSN.getPorId(seccion.getId().getIdToSeccion()));
			List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccion(idToCotizacion, seccion.getId().getIdToSeccion());
			seccion.setCoberturaCotizacionSinAgrupar(coberturas);

			List<CoberturaCotizacionDTO> coberturasAgrupadas = seccion.getCoberturaCotizacionLista();
			for(CoberturaCotizacionDTO cobertura : coberturasAgrupadas) {
				CoberturaDN coberturaDN = CoberturaDN.getInstancia();
				CoberturaDTO coberturaDTO = coberturaDN.getPorId(cobertura.getId().getIdToCobertura());
				
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return secciones;
	}
	
	/**
	 * Metodo usado para consultar el desglose de secciones y coberturas contratadas, incluyendo la información 
	 * de primas y cuotas.
	 * @param idToCotizacion
	 * @param seccionesCombo
	 * @param coberturas
	 * @param igualacionPrimaNetaForm
	 * @param aplicaIgualacionAInciso
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<SeccionCotizacionDTO> listarSeccionesIgualacionCuotas(BigDecimal idToCotizacion,
			List<SeccionCotizacionDTO> seccionesCombo,
			List<CoberturaCotizacionDTO> coberturas,
			IgualacionPrimaNetaForm igualacionPrimaNetaForm,boolean aplicaIgualacionAInciso) throws ExcepcionDeAccesoADatos, SystemException{
		
		if(igualacionPrimaNetaForm == null){
			igualacionPrimaNetaForm = new IgualacionPrimaNetaForm();
		}
		
		List<SeccionCotizacionDTO> secciones = listarSeccionesContratadas(idToCotizacion);
		
		CoberturaCotizacionDN.calcularPrimasOriginalesSeccion(secciones,aplicaIgualacionAInciso);

		BigDecimal totalPN = BigDecimal.ZERO;
		BigDecimal totalPNO = BigDecimal.ZERO;
		
		for (SeccionCotizacionDTO seccion : secciones) {
			totalPN = totalPN.add(BigDecimal.valueOf(seccion.getValorPrimaNeta()));
			igualacionPrimaNetaForm.setTotalCotizacion(totalPN.doubleValue());
			totalPNO = totalPNO.add(BigDecimal.valueOf(seccion.getValorPrimaNetaOriginal()));
			igualacionPrimaNetaForm.setTotalCotizacionOriginal(totalPNO.doubleValue());
			
			if(seccionesCombo != null){
				seccionesCombo.add(seccion);
			}
		}
		
		if (seccionesCombo != null && !seccionesCombo.isEmpty() && coberturas != null) {
			for (CoberturaCotizacionDTO cobertura : seccionesCombo.get(0).getCoberturaCotizacionLista()) {
				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveIgualacion().shortValue() == 1
						&& (cobertura.getClaveFacultativo() == null
						|| cobertura.getClaveFacultativo() == 0)) {
					coberturas.add(cobertura);
				}
			}
		}
		
		return secciones;
	}
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasIgualacion(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos {
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		List<SeccionCotizacionDTO> secciones = seccionCotizacionSN.listarSeccionesContratadasIgualacion(idToCotizacion,numeroInciso);
		SeccionSN seccionSN = new SeccionSN();
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for(SeccionCotizacionDTO seccion : secciones) {
			seccion.setSeccionDTO(seccionSN.getPorId(seccion.getId().getIdToSeccion()));
			List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccion(idToCotizacion, seccion.getId().getIdToSeccion());
			seccion.setCoberturaCotizacionSinAgrupar(coberturas);

			List<CoberturaCotizacionDTO> coberturasAgrupadas = seccion.getCoberturaCotizacionLista();
			for(CoberturaCotizacionDTO cobertura : coberturasAgrupadas) {
				CoberturaDN coberturaDN = CoberturaDN.getInstancia();
				CoberturaDTO coberturaDTO = coberturaDN.getPorId(cobertura.getId().getIdToCobertura());
				
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}
		}
		return secciones;
	}	
	/**
	 * Method listarSeccionesContratadasPorCotizacion().
	 * @param idToCotizacion
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion) throws SystemException, ExcepcionDeAccesoADatos {
		List<SeccionCotizacionDTO> secciones = new SeccionCotizacionSN().listarSeccionesContratadas(idToCotizacion);
		return secciones;
	}
	/**
	 * Method listarSeccionesContratadasPorCotizacion().
	 * @param idToCotizacion
	 * @param claveContrato
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato) throws SystemException, ExcepcionDeAccesoADatos {
		List<SeccionCotizacionDTO> secciones = new SeccionCotizacionSN().listarSeccionesContratadasPorCotizacion(idToCotizacion, claveContrato);
		return secciones;
	}
	/**
	 * Method listarSeccionesContratadasPorCotizacion().
	 * @param idToCotizacion
	 * @param claveContrato
	 * @param aplicarMerge
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato,boolean aplicarMerge) throws SystemException, ExcepcionDeAccesoADatos {
		List<SeccionCotizacionDTO> secciones = new SeccionCotizacionSN().listarSeccionesContratadasPorCotizacion(idToCotizacion, claveContrato,aplicarMerge);
		return secciones;
	}
	/**
	 * Method listarSeccionesContratadas().
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos {
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		List<SeccionCotizacionDTO> secciones = seccionCotizacionSN.listarSeccionesContratadas(idToCotizacion,numeroInciso);
		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
		for(SeccionCotizacionDTO seccion : secciones) {
			List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionDN.listarCoberturasContratadasPorSeccion(idToCotizacion, seccion.getId().getIdToSeccion());
			seccion.setCoberturaCotizacionSinAgrupar(coberturas);

			/*@SuppressWarnings("unused")
			List<CoberturaCotizacionDTO> coberturasAgrupadas = seccion.getCoberturaCotizacionLista();
			for(CoberturaCotizacionDTO cobertura : coberturasAgrupadas) {
				CoberturaDN coberturaDN = CoberturaDN.getInstancia();
				CoberturaDTO coberturaDTO = coberturaDN.getPorId(cobertura.getId().getIdToCobertura());
				
				CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
				coberturaSeccionDTO.setCoberturaDTO(coberturaDTO);
				cobertura.setCoberturaSeccionDTO(coberturaSeccionDTO);
			}*/
		}
		return secciones;
	}
	/**
	 * Method  listarSeccionesContratadasSinCoberturaSinAgrupar().
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadasSinCoberturaSinAgrupar(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos {
		SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
		List<SeccionCotizacionDTO> secciones = seccionCotizacionSN.listarSeccionesContratadas(idToCotizacion,numeroInciso);
		return secciones;
	}
	/**
	 * Method buscarPorId().
	 * @param id
	 * @return SeccionCotizacionDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public SeccionCotizacionDTO buscarPorId(SeccionCotizacionDTOId id) throws ExcepcionDeAccesoADatos,SystemException {
		return new SeccionCotizacionSN().buscarPorId(id);
	}
	/**
	 * Method listarPorCotizacionNumeroInciso()
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return List<SeccionCotizacionDTO> lista de entidades
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarPorCotizacionNumeroInciso(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos {
		return new SeccionCotSN().listarPorCotizacionNumeroInciso(idToCotizacion, numeroInciso);
	}
	
	/**
	 * Lista todas las secciones contratadas para una cotizacion (Sin establecer setCoberturaCotizacionSinAgrupar)
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<SeccionCotizacionDTO> listarSeccionesContratadasCotizacion(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos {
		return new SeccionCotizacionSN().listarSeccionesContratadas(idToCotizacion,numeroInciso);
	}
	
	/**
	 * Obtiene la cantidad total de secciones por numero de inciso
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public Long obtenerTotalSeccionesContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos {
		return new SeccionCotSN().obtenerTotalSeccionesContratadas(idToCotizacion, numeroInciso);
	}
	
	public SeccionCotizacionDTO modificar (SeccionCotizacionDTO seccionCotizacionDTO) throws SystemException {
		return new SeccionCotizacionSN().modificar(seccionCotizacionDTO);
	}
	
}
