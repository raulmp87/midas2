package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;


public class ConfiguracionDatoIncisoCotizacionDN {
	private static final ConfiguracionDatoIncisoCotizacionDN INSTANCIA = new ConfiguracionDatoIncisoCotizacionDN();

	public static ConfiguracionDatoIncisoCotizacionDN getInstancia (){
		return ConfiguracionDatoIncisoCotizacionDN.INSTANCIA;
	}
	
	/**
	 * Obtiene la etiqueta y valor real de un datoIncisoCotizacionDTO.
	 * @param datoIncisoCotizacionDTO
	 * @return String[] arreglo de String que contiene en la posici�n 0 la etiqueta, en la posici�n [1] el valor y en la posici�n [2] el valor registrado en datoIncisoCot.
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	public String[] obtenerDescripcionDatoInciso(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO,ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO) throws SystemException{
		ConfiguracionDatoIncisoCotizacionDTO configuracionDato = null;
		if(configuracionDatoIncisoCotizacionDTO == null || configuracionDatoIncisoCotizacionDTO.getDescripcionEtiqueta() == null || configuracionDatoIncisoCotizacionDTO.getDescripcionEtiqueta().trim().equals("")){
			ConfiguracionDatoIncisoCotizacionId configuracionDatoId = new ConfiguracionDatoIncisoCotizacionId();
			configuracionDatoId.setIdTcRamo(datoIncisoCotizacionDTO.getId().getIdTcRamo());
			configuracionDatoId.setIdTcSubramo(datoIncisoCotizacionDTO.getId().getIdTcSubramo());
			configuracionDatoId.setIdToRiesgo(datoIncisoCotizacionDTO.getId().getIdToRiesgo());
			configuracionDatoId.setClaveDetalle(datoIncisoCotizacionDTO.getId().getClaveDetalle());
			configuracionDatoId.setIdDato(datoIncisoCotizacionDTO.getId().getIdDato());
			
			ConfiguracionDatoIncisoCotizacionSN confDatoIncisoSN = new ConfiguracionDatoIncisoCotizacionSN();
			configuracionDato = confDatoIncisoSN.getPorId(configuracionDatoId);
		} else{
			configuracionDato = configuracionDatoIncisoCotizacionDTO;
		}
		
		if (configuracionDato == null){
			throw new SystemException("No se ecnontr&oacute; la configuraci&oacute;n para el dato: \nidTcRamo: "+datoIncisoCotizacionDTO.getId().getIdTcRamo()+"\n"+
					"idTcSubRamo: "+datoIncisoCotizacionDTO.getId().getIdTcSubramo()+"\nidToRiesgo: "+datoIncisoCotizacionDTO.getId().getIdToRiesgo()+"\n"+
					"claveDetalle: "+datoIncisoCotizacionDTO.getId().getClaveDetalle()+"\nidDato: "+datoIncisoCotizacionDTO.getId().getIdDato());
		}
		String result[] = new String[3];
		result[0]=configuracionDato.getDescripcionEtiqueta();
		//Si el dato no trae valor, se hace la consulta a la tabla.
		if (datoIncisoCotizacionDTO.getValor()==null){
			DatoIncisoCotizacionDTO datoIncisoCotTMP = null;
			datoIncisoCotTMP = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
			//Si no se encuentra, se lanza la excepcion
			if (datoIncisoCotTMP == null)
				throw new SystemException("No se encontr&oacute; DATOINCISOCOTIZACION con los datos: \nidTcRamo: "+datoIncisoCotizacionDTO.getId().getIdTcRamo()+"\n"+
					"idTcSubRamo: "+datoIncisoCotizacionDTO.getId().getIdTcSubramo()+"\nidToRiesgo: "+datoIncisoCotizacionDTO.getId().getIdToRiesgo()+"\n"+
					"claveDetalle: "+datoIncisoCotizacionDTO.getId().getClaveDetalle()+"\nidDato: "+datoIncisoCotizacionDTO.getId().getIdDato());
			else
				datoIncisoCotizacionDTO = datoIncisoCotTMP;
		}
		result[1]=datoIncisoCotizacionDTO.getValor();
		result[2]=datoIncisoCotizacionDTO.getValor();
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		MidasInterfaceBase beanBase;
		switch(configuracionDato.getClaveTipoControl()) {
			case 1:
				try{
					beanBase = serviceLocator.getEJB(configuracionDato.getDescripcionClaseRemota());
					result[1] = beanBase.findById(UtileriasWeb.regresaBigDecimal(result[1])).getDescription();
				}catch(NullPointerException e){result[1] = "No disponible";}
				catch (ClassNotFoundException e) {	result[1] = "No disponible";}
				break;
			case 2:
				CatalogoValorFijoId catalogoFijoId = new CatalogoValorFijoId();
				try{
					catalogoFijoId.setIdDato(Integer.valueOf(result[1]).intValue());
					catalogoFijoId.setIdGrupoValores(Integer.valueOf(configuracionDato.getIdGrupo().toString()).intValue());
					beanBase = serviceLocator.getEJB(configuracionDato.getDescripcionClaseRemota());
					result[1] = beanBase.findById(catalogoFijoId).getDescription();
				}catch(NullPointerException e){result[1] = "No disponible";}
				catch(NumberFormatException e){result[1] = "No disponible";}
				catch (ClassNotFoundException e) {	result[1] = "No disponible";}
				break;
			default:{
				switch(configuracionDato.getCodigoFormato().intValue()){
					case 1:
						try{
							result[1] = new DecimalFormat("#,##0").format(Double.valueOf(result[1]));
						}catch(Exception e){
						}
				}
				break;
			}
		}
		return result;
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoImpresionPoliza(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo) throws SystemException {
		
		return new ConfiguracionDatoIncisoCotizacionSN().getDatosRiesgoImpresionPoliza(idTcRamo, idTcSubRamo);
		
	}
	
	
	public void actualizaEstatusRiesgoImpresionPoliza(ConfiguracionDatoIncisoCotizacionId id, String nuevoEstatus) throws SystemException {
		Short nuevoStatus = 0;
		ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
		ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacion = configuracionDatoIncisoCotizacionSN.getPorId(id);
		
		if (nuevoEstatus.trim().equals("true")) {
			nuevoStatus = 1;
		}
		
		configuracionDatoIncisoCotizacion.setClaveimpresionpoliza(nuevoStatus);
		configuracionDatoIncisoCotizacionSN.modificar(configuracionDatoIncisoCotizacion);
		
	}
	
	
	public void actualizaCodigoFormatoImpresionPoliza(ConfiguracionDatoIncisoCotizacionId id, String codigoFormato) throws SystemException {
		
		ConfiguracionDatoIncisoCotizacionSN configuracionDatoIncisoCotizacionSN = new ConfiguracionDatoIncisoCotizacionSN();
		ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacion = configuracionDatoIncisoCotizacionSN.getPorId(id);
		
		configuracionDatoIncisoCotizacion.setCodigoFormato(Short.valueOf(codigoFormato.trim()));
		configuracionDatoIncisoCotizacionSN.modificar(configuracionDatoIncisoCotizacion);
		
	}
	
	
	public ConfiguracionDatoIncisoCotizacionDTO instanciarConfiguracionDatoIncisoCotizacion(Short claveDetalle,BigDecimal idDato,BigDecimal idTcRamo,BigDecimal idTcSubRamo,BigDecimal idToRiesgo){
		ConfiguracionDatoIncisoCotizacionDTO configuracionDato = new ConfiguracionDatoIncisoCotizacionDTO(new ConfiguracionDatoIncisoCotizacionId(),null,null,null,null,null);
		configuracionDato.getId().setClaveDetalle(claveDetalle);
		configuracionDato.getId().setIdDato(idDato);
		configuracionDato.getId().setIdTcRamo(idTcRamo);
		configuracionDato.getId().setIdTcSubramo(idTcSubRamo);
		configuracionDato.getId().setIdToRiesgo(idToRiesgo);
		return configuracionDato;
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> listarFiltrado(ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO) throws SystemException{
		return new ConfiguracionDatoIncisoCotizacionSN().listarFiltrado(configuracionDatoIncisoCotizacionDTO);
	}
}

