package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SeccionCotizacionSN {
	private SeccionCotizacionFacadeRemote beanRemoto;

	public SeccionCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(SeccionCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	/**
	 * Lista las entidades IncisoSeccionDTO relacionadas con la cotizacion cuyo
	 * id se recibe.
	 * 
	 * @param BigDecimal
	 *            idToInciso
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<SeccionCotizacionDTO> listarPorIncisoCotizacionId(
			IncisoCotizacionId idToInciso) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarPorIncisoCotizacionId(idToInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void guardar(SeccionCotizacionDTO seccionCotizacionDTO) {
		try {
			beanRemoto.save(seccionCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrar(SeccionCotizacionDTO seccionCotizacionDTO) {
		try {
			beanRemoto.delete(seccionCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Lista las secciones de la cotizacion que pueden ser configuradas para
	 * primer riesgo
	 * 
	 * @param BigDecimal
	 *            idToInciso
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<SeccionCotizacionDTO> listarSeccionesPrimerRiesgo(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarSeccionesPrimerRiesgo(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Lista las secciones de la cotizacion que pueden ser configuradas para LUC
	 * 
	 * @param BigDecimal
	 *            idToInciso
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<SeccionCotizacionDTO> listarSeccionesLUC(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarSeccionesLUC(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<SeccionCotizacionDTO> listarSeccionesContratadas(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarSeccionesContratadas(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarSeccionesContratadasPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso) {
		try {
			return beanRemoto.listarSeccionesContratadas(idToCotizacion,numeroInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SeccionCotizacionDTO buscarPorId(SeccionCotizacionDTOId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Double getSumatoriaCoberturasBasicas(SeccionCotizacionDTOId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.getSumatoriaCoberturasBasicas(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SeccionCotizacionDTO modificar(SeccionCotizacionDTO seccionCotizacionDTO) {
		try {
			return beanRemoto.update(seccionCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato) {
		try {
			return beanRemoto.listarSeccionesContratadasPorCotizacion(idToCotizacion, claveContrato);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato,boolean aplicarMerge) {
		try {
			return beanRemoto.listarSeccionesContratadasPorCotizacion(idToCotizacion, claveContrato,aplicarMerge);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public List<SeccionCotizacionDTO> listarSeccionesContratadasIgualacion(BigDecimal idToCotizacion,BigDecimal numeroInciso) {
		try {
			return beanRemoto.listarSeccionesContratadasIgualacion(idToCotizacion,numeroInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
}
