package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IncisoCotizacionSN {
	private IncisoCotizacionFacadeRemote beanRemoto;

	public IncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(IncisoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public IncisoCotizacionDTO agregar(IncisoCotizacionDTO incisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(incisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(IncisoCotizacionDTO incisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(incisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public IncisoCotizacionDTO modificar(IncisoCotizacionDTO incisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(incisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<IncisoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public IncisoCotizacionDTO getPorId(IncisoCotizacionId incisoCotizacionid) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(incisoCotizacionid);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Lista las entidades IncisoCotizacionDTO relacionadas con la cotizacion cuyo id se recibe.
	 * @param BigDecimal idToCotizacion
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<IncisoCotizacionDTO> listarPorCotizacionId(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByCotizacionId(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<IncisoCotizacionDTO> buscarPorIdCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.findByProperty("id.idToCotizacion", idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IncisoCotizacionDTO> getPorPropiedad(String name,Object obj) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(name, obj);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public IncisoCotizacionDTO guardar(IncisoCotizacionDTO incisoCotizacionDTO) {
		try {
			return beanRemoto.save(incisoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public BigDecimal obtenerNumeroIncisoSiguiente(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.maxIncisos(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IncisoCotizacionDTO> listarAutorizacionesPendientesYRechazadasPorCotizacion(BigDecimal idToCotizacion){
		
		return beanRemoto.listarAutorizacionesPendientesYRechazadasPorCotizacion(idToCotizacion);
	}

	/**
	 * Regresa la suma asegurada maxima por ubicacion de acuerdo a las secciones
	 * que van a primer riesgo
	 * 
	 * @param BigDecimal
	 *            idToCotizacion
	 * @param String
	 *            seccionesConComas
	 * @return Double la suma asegurada maxima por ubicacion
	 */
	public Double getSumaAseguradaMayorPorUbicacion(BigDecimal idToCotizacion,
			String seccionesConComas) {
		try {
			return beanRemoto.getSumaAseguradaMayorPorUbicacion(idToCotizacion,
					seccionesConComas);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Regresa la prima neta acumulada por cotizacion que van a primer riesgo
	 * 
	 * @param BigDecimal
	 *            idToCotizacion
	 * @param String
	 *            seccionesConComas
	 * @return BigDecimal prima neta acumulada por cotizacion
	 */
	public BigDecimal primaNetaPorCotizacionPrimerRiesgo(BigDecimal idToCotizacion,
			String seccionesConComas) {
		try {
			return beanRemoto.primaNetaPorCotizacionPrimerRiesgo(
					idToCotizacion, seccionesConComas);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	/**
	 * Regresa la suma asegurada acumulada por cotizacion
	 * 
	 * @param BigDecimal
	 *            idToCotizacion
	 * @return BigDecimal prima neta acumulada por cotizacion
	 */
	public BigDecimal sumaAseguradaPrimerRiesgoPorCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.sumaAseguradaPrimerRiesgoPorCotizacion(
					idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Lista los incisos de una cotizacion que tengan las coberturas recibidas con la claveContrato recibido.
	 * @param BigDecimal idToCotizacion @param List<CoberturaCotizacionDTO> Lista de coberturas con la claveContrato recibida
	 * @param claveContrato
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas pro el query
	 */
	public List<IncisoCotizacionDTO> listarIncisosPorCoberturaContratada(BigDecimal idToCotizacion,List<CoberturaCotizacionDTO> coberturas,Short claveContrato) {
		try {
			return beanRemoto.listarIncisosPorCoberturaContratada(idToCotizacion, coberturas, claveContrato);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Map<String, String> agregarInciso(CotizacionDTO cotizacionDTO,BigDecimal idToDireccion,BigDecimal numeroInciso,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso,String[] datos,
				List<RecargoVarioDTO> recargosEspeciales,List<DescuentoDTO> descuentosEspeciales) {
		try {
			return beanRemoto.agregarInciso(cotizacionDTO, idToDireccion, numeroInciso, datosInciso, datos,recargosEspeciales,descuentosEspeciales);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Map<String, String> borrarInciso(
			IncisoCotizacionDTO incisoCotizacionDTO) {
		try {
			return beanRemoto.borrarInciso(incisoCotizacionDTO);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Boolean copiarInciso(IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoOriginal, CotizacionDTO cotizacionDTO,
			DireccionDTO direccionNueva,List<DatoIncisoCotizacionDTO> listaDatosInciso) {
		try {
			return beanRemoto.copiarInciso(incisoNuevo, incisoOriginal, cotizacionDTO,direccionNueva,listaDatosInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Map<String, String> separarInciso(CotizacionDTO cotizacionDTO,
			BigDecimal numeroInciso) {
		try {
			return beanRemoto.separarInciso(cotizacionDTO, numeroInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IncisoCotizacionDTO> obtenerIncisosPorIdCotizacionDireccion(BigDecimal idToCotizacion,BigDecimal idToDireccion) {
		try {
			return beanRemoto.obtenerIncisosPorIdCotizacionDireccion(idToCotizacion,idToDireccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public int obtenerCantidadIncisosPorCotizacion(BigDecimal idToCotizacion){
		try {
			return beanRemoto.contarIncisosPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public BigDecimal copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToDireccion) {
		try {
			return beanRemoto.copiarInciso(idToCotizacion, numeroInciso, idToDireccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Lista los incisos de una cotizacion que tengan al menos una cobertura contratada con el subramo especificado. 
	 * @param idToCotizacion, idTcSubRamo
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */
	List<IncisoCotizacionDTO> listarIncisosPorSubRamo(BigDecimal idToCotizacion, BigDecimal idTcSubRamo){
		try {
			return beanRemoto.listarIncisosPorSubRamo(idToCotizacion, idTcSubRamo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}
	
	/**
	 * Lista los incisos dados de baja en una cotizacion de endoso. 
	 * @param idToCotizacionEndoso
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */	
	public List<IncisoCotizacionDTO> listarBajaIncisos(BigDecimal idToCotizacionEndosoActual, BigDecimal idToCotizacionEndosoAnterior){
		try {
			return beanRemoto.listarBajaIncisos(idToCotizacionEndosoActual, idToCotizacionEndosoAnterior);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}
}
