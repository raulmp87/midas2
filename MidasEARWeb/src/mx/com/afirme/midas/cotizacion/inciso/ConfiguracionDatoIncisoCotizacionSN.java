package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConfiguracionDatoIncisoCotizacionSN {
	private ConfiguracionDatoIncisoCotizacionFacadeRemote beanRemoto;

	public ConfiguracionDatoIncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(ConfiguracionDatoIncisoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(
			ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(configuracionDatoIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(
			ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(configuracionDatoIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(
			ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(configuracionDatoIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public ConfiguracionDatoIncisoCotizacionDTO getPorId(
			ConfiguracionDatoIncisoCotizacionId configuracionDatoIncisoCotizacionid)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(configuracionDatoIncisoCotizacionid);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> buscarPorPropiedad(
			String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> getPorPropiedad(
			String name, Object obj) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(name, obj);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void guardar(
			ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO) {
		try {
			beanRemoto.save(configuracionDatoIncisoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRamoInciso(
			BigDecimal idTcRamo) {
		try {
			return beanRemoto.getDatosRamoInciso(idTcRamo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToRiesgo) {
		try {
			return beanRemoto.getDatosRiesgoInciso(idToRiesgo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoSubInciso(BigDecimal idToRiesgo) {
		try {
			return beanRemoto.getDatosRiesgoSubInciso(idToRiesgo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoImpresionPoliza(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo) {
		try {
			return beanRemoto.getDatosRiesgoImpresionPoliza(idTcRamo, idTcSubRamo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> listarFiltrado(ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO) {
		try {
			return beanRemoto.listarFiltrado(configuracionDatoIncisoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
