package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DatoIncisoCotizacionSN {
	private DatoIncisoCotizacionFacadeRemote beanRemoto;

	public DatoIncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(DatoIncisoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(datoIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(datoIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(datoIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DatoIncisoCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public DatoIncisoCotizacionDTO getPorId(
			DatoIncisoCotizacionId datoIncisoCotizacionid)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(datoIncisoCotizacionid);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DatoIncisoCotizacionDTO> buscarPorPropiedad(String propiedad,
			Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DatoIncisoCotizacionDTO> getPorPropiedad(String name, Object obj)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(name, obj);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void guardar(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO) {
		try {
			beanRemoto.save(datoIncisoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrarTodos(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		try {
			beanRemoto.deleteAll(idToCotizacion, numeroInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Filtra los registros de DatoIncisoCotizacionDTO en base a los atributos recibidos en el objeto DatoIncisoCotizacionId.
	 * Los atributos enviados en la consulta son: idToCotizacion, numeroInciso, idToSeccion, idToCobertura, idToRiesgo,
	 * numeroSubinciso, idTcRamo, idTcSubramo, claveDetalle, idDato. Los atributos que se reciben con valor de null son ignorados.
	 * @param DatoIncisoCotizacionId idDatoInciso
	 */
	public List<DatoIncisoCotizacionDTO> listarPorIdFiltrado(DatoIncisoCotizacionId idDatoInciso) {
		try {
			return beanRemoto.listarPorIdFiltrado(idDatoInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Filtra los registros de DatoIncisoCotizacionDTO en base al idToCotizacion recibido.
	 * @param BigDecimal idToCotizacion
	 * @return List<DatoIncisoCotizacionDTO>. lista de registros DatoIncisoCotizacion de la cotizacion recibida.
	 */
	public List<DatoIncisoCotizacionDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.findByProperty("id.idToCotizacion", idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
