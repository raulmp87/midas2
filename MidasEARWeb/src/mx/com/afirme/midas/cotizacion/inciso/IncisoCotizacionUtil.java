package mx.com.afirme.midas.cotizacion.inciso;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;

import org.apache.commons.beanutils.BeanUtils;

public class IncisoCotizacionUtil {

	@SuppressWarnings("unchecked")
	public static String obtenerSelectCatalogo(Map atributos, String claseRemota, boolean esSoloLectura, String valorSeleccionado) throws SystemException {
		try {
			List optionList = null;
			if (claseRemota != null && !UtileriasWeb.esCadenaVacia(claseRemota)) {
				ServiceLocator serviceLocator = ServiceLocator.getInstance();
				MidasInterfaceBase beanBase = serviceLocator
						.getEJB(claseRemota);
				optionList = beanBase.findAll();
				if (optionList == null) {
					optionList = new ArrayList();
				}
			} else {
				optionList = new ArrayList();
			}
			return getSelectString(atributos, optionList, esSoloLectura, "id", "description", valorSeleccionado);
		} catch (ClassNotFoundException e) {
			throw new SystemException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static String obtenerSelectCatalogoAutos(Map atributos, String claseRemota, boolean esSoloLectura, String valorSeleccionado, BigDecimal idToSeccion) throws SystemException {
		try {
			List optionList = null;
			if (claseRemota != null && !UtileriasWeb.esCadenaVacia(claseRemota)) {
				ServiceLocator serviceLocator = ServiceLocator.getInstance();
				MidasInterfaceBaseAuto beanBase = serviceLocator
						.getEJB(claseRemota);
				optionList = beanBase.findAll(idToSeccion);
				if (optionList == null) {
					optionList = new ArrayList();
				}
			} else {
				optionList = new ArrayList();
			}
			return getSelectString(atributos, optionList, esSoloLectura, "id", "description", valorSeleccionado);
		} catch (ClassNotFoundException e) {
			throw new SystemException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static String obtenerSelectCatalogo(String name, String claseRemota, boolean esSoloLectura, String valorSeleccionado,Map<String,String> mapaIDsCascadeoHijos) throws SystemException {
		try {
			List optionList = new ArrayList();
			Map attributes = new HashMap();
			attributes.put("class", "cajaTexto");
			attributes.put("size", "");
			attributes.put("id", name);
			attributes.put("name", name);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			MidasInterfaceBase beanBase = serviceLocator.getEJB(claseRemota);
			if (!UtileriasWeb.esCadenaVacia(claseRemota)) {
				//Settear un id para las listas que son hijas, cuyos datos provienen de un cascadeo
				if (claseRemota.indexOf(".SubGiroFacade") != -1){
					String idHijo = mapaIDsCascadeoHijos.get("SubGiro");
					if (idHijo == null){
						idHijo = "idTcSubGiro_0";
						mapaIDsCascadeoHijos.put("SubGiro", "idTcSubGiro_1");
					}
					else{
						int contador = Integer.valueOf(idHijo.substring(idHijo.length()-1)) + 1;
						mapaIDsCascadeoHijos.remove("SubGiro");
						mapaIDsCascadeoHijos.put("SubGiro", "idTcSubGiro_" + contador);
					}
					attributes.remove("id");
					attributes.put("id", idHijo);
					if (!UtileriasWeb.esCadenaVacia(valorSeleccionado))
						optionList = beanBase.listRelated(UtileriasWeb.regresaBigDecimal(valorSeleccionado));
				}
				else if (claseRemota.indexOf(".SubtipoEquipoElectronico") != -1){
					String idHijo = mapaIDsCascadeoHijos.get("SubtipoEquipoElectronico");
					if (idHijo == null){
						idHijo = "idTcSubTipoEquipoElectronico_0";
						mapaIDsCascadeoHijos.put("SubtipoEquipoElectronico", "idTcSubTipoEquipoElectronico_1");
					}
					else{
						int contador = Integer.valueOf(idHijo.substring(idHijo.length()-1)) + 1;
						mapaIDsCascadeoHijos.remove("SubtipoEquipoElectronico");
						mapaIDsCascadeoHijos.put("SubtipoEquipoElectronico", "idTcSubTipoEquipoElectronico_" + contador);
					}
					attributes.remove("id");
					attributes.put("id", idHijo);
					if (!UtileriasWeb.esCadenaVacia(valorSeleccionado))
						optionList = beanBase.listRelated(UtileriasWeb.regresaBigDecimal(valorSeleccionado));
				}
				else if (claseRemota.indexOf(".SubtipoEquipoContratista") != -1){
					String idHijo = mapaIDsCascadeoHijos.get("SubtipoEquipoContratista");
					if (idHijo == null){
						idHijo = "idTcSubtipoEquipoContratista_0";
						mapaIDsCascadeoHijos.put("SubtipoEquipoContratista", "idTcSubtipoEquipoContratista_1");
					}
					else{
						int contador = Integer.valueOf(idHijo.substring(idHijo.length()-1)) + 1;
						mapaIDsCascadeoHijos.remove("SubtipoEquipoContratista");
						mapaIDsCascadeoHijos.put("SubtipoEquipoContratista", "idTcSubtipoEquipoContratista_" + contador);
					}
					attributes.remove("id");
					attributes.put("id", idHijo);
					if (!UtileriasWeb.esCadenaVacia(valorSeleccionado))
						optionList = beanBase.listRelated(UtileriasWeb.regresaBigDecimal(valorSeleccionado));
				}
				else if (claseRemota.indexOf(".SubtipoRecipientePresion") != -1){
					String idHijo = mapaIDsCascadeoHijos.get("SubtipoRecipientePresion");
					if (idHijo == null){
						idHijo = "idTcSubTipoRecipientePresion_0";
						mapaIDsCascadeoHijos.put("SubtipoRecipientePresion", "idTcSubTipoRecipientePresion_1");
					}
					else{
						int contador = Integer.valueOf(idHijo.substring(idHijo.length()-1)) + 1;
						mapaIDsCascadeoHijos.remove("SubtipoRecipientePresion");
						mapaIDsCascadeoHijos.put("SubtipoRecipientePresion", "idTcSubTipoRecipientePresion_" + contador);
					}
					attributes.remove("id");
					attributes.put("id", idHijo);
					if (!UtileriasWeb.esCadenaVacia(valorSeleccionado))
						optionList = beanBase.listRelated(UtileriasWeb.regresaBigDecimal(valorSeleccionado));
				}
				else if (claseRemota.indexOf(".SubGiroRCFacadeRemote") != -1){
					String idHijo = mapaIDsCascadeoHijos.get("SubGiroRC");
					if (idHijo == null){
						idHijo = "idTcSubGiroRC_0";
						mapaIDsCascadeoHijos.put("SubGiroRC", "idTcSubGiroRC_1");
					}
					else{
						int contador = Integer.valueOf(idHijo.substring(idHijo.length()-1)) + 1;
						mapaIDsCascadeoHijos.remove("SubGiroRC");
						mapaIDsCascadeoHijos.put("SubGiroRC", "idTcSubGiroRC_" + contador);
					}
					attributes.remove("id");
					attributes.put("id", idHijo);
					if (!UtileriasWeb.esCadenaVacia(valorSeleccionado))
						optionList = beanBase.listRelated(UtileriasWeb.regresaBigDecimal(valorSeleccionado));
				}else if (claseRemota.indexOf(".SubTipoMaquinariaFacadeRemote") != -1){
					String idHijo = mapaIDsCascadeoHijos.get("SubTipoMaquinaria");
					if (idHijo == null){
						idHijo = "idTcSubTipoMaquinaria_0";
						mapaIDsCascadeoHijos.put("SubTipoMaquinaria", "idTcSubTipoMaquinaria_1");
					}
					else{
						int contador = Integer.valueOf(idHijo.substring(idHijo.length()-1)) + 1;
						mapaIDsCascadeoHijos.remove("SubTipoMaquinaria");
						mapaIDsCascadeoHijos.put("SubTipoMaquinaria", "idTcSubTipoMaquinaria_" + contador);
					}
					attributes.remove("id");
					attributes.put("id", idHijo);
					if (!UtileriasWeb.esCadenaVacia(valorSeleccionado))
						optionList = beanBase.listRelated(UtileriasWeb.regresaBigDecimal(valorSeleccionado));					
				}
				//Para las listas que no provienen de cascadeo, obtener la lista completa de registros
				else{
					optionList = beanBase.findAll();
					if (optionList == null)		optionList = new ArrayList();
					//Para las listas que son padres para cascadear, establecer el evento onchange correspondiente
					if (claseRemota.indexOf(".GiroFacadeRemote")!=-1){
						String idHijo = mapaIDsCascadeoHijos.get("SubGiro");
						if (idHijo == null) idHijo = "idTcSubGiro_0";
						attributes.remove("onchange");
						attributes.put("onchange","getSubGiros(this,'"+idHijo+"');");
					}
					else if (claseRemota.indexOf(".TipoRecipientePresion")!=-1){
						String idHijo = mapaIDsCascadeoHijos.get("SubtipoRecipientePresion");
						if (idHijo == null) idHijo = "idTcSubTipoRecipientePresion_0";
						attributes.remove("onchange");
						attributes.put("onchange","getSubTiposRecipientePresion(this,'"+idHijo+"');");
					}
					else if (claseRemota.indexOf(".EquipoElectronico")!=-1){
						String idHijo = mapaIDsCascadeoHijos.get("SubtipoEquipoElectronico");
						if (idHijo == null) idHijo = "idTcSubTipoEquipoElectronico_0";
						attributes.remove("onchange");
						attributes.put("onchange","getSubTiposEquipoElectronico(this,'"+idHijo+"');");
					}
					else if (claseRemota.indexOf(".TipoEquipoContratista")!=-1){
						String idHijo = mapaIDsCascadeoHijos.get("SubtipoEquipoContratista");
						if (idHijo == null) idHijo = "idTcSubtipoEquipoContratista_0";
						attributes.remove("onchange");
						attributes.put("onchange","getSubTiposEquipoContratista(this,'"+idHijo+"');");
					}
					else if (claseRemota.indexOf(".GiroRCFacadeRemote")!=-1){
						String idHijo = mapaIDsCascadeoHijos.get("SubGiroRC");
						if (idHijo == null) idHijo = "idTcSubGiroRC_0";
						attributes.remove("onchange");
						attributes.put("onchange","getSubGirosRC(this,'"+idHijo+"');");
					}
					else if (claseRemota.indexOf(".TipoMaquinariaFacadeRemote")!=-1){
						String idHijo = mapaIDsCascadeoHijos.get("SubTipoMaquinaria");
						if (idHijo == null) idHijo = "idTcSubTipoMaquinaria_0";
						attributes.remove("onchange");
						attributes.put("onchange","getSubTiposMaquinaria(this,'"+idHijo+"');");
					}					
				}
			}
			return getSelectString(attributes, optionList, esSoloLectura, "id", "description", valorSeleccionado);
		} catch (ClassNotFoundException e) {
			throw new SystemException(e);
		}
	}

	private static void renderAttribute(StringBuffer buffer, String name,
			String value) {
		buffer.append(" ");
		buffer.append(name);
		buffer.append("=\"");
		buffer.append(value);
		buffer.append("\"");
	}
	
	@SuppressWarnings("unchecked")
	public static String obtenerSelectValorFijo(Map atributos, BigDecimal idGrupo, boolean esSoloLectura, String valorSeleccionado) throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		CatalogoValorFijoFacadeRemote beanCatalogoValorFijo;
		try {
			beanCatalogoValorFijo = serviceLocator.getEJB(CatalogoValorFijoFacadeRemote.class);
			List<CatalogoValorFijoDTO> listaCatalogosFijos = beanCatalogoValorFijo.findByProperty("id.idGrupoValores", idGrupo);
			return getSelectString(atributos, listaCatalogosFijos, esSoloLectura, "id.idDato", "descripcion", valorSeleccionado);
		} catch (SystemException e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	private static String getSelectString(Map atributos, List optionList, boolean esSoloLectura, String id, String description, String selectedValue) throws SystemException {
		StringBuffer buffer = new StringBuffer();
		Iterator attributeKeyIterator = atributos.keySet().iterator();
		Iterator optionIterator = optionList.iterator();
		try {
			if (esSoloLectura)
				buffer.append("<select disabled=\"disabled\"");
			else
				buffer.append("<select");
			while (attributeKeyIterator.hasNext()) {
				String name = (String) attributeKeyIterator.next();
				String value = (String) atributos.get(name);
				if (value != null) {
					renderAttribute(buffer, name, value);
				} // End of if
			} // End of while()
			buffer.append(">\n");
			if (optionList.isEmpty()) {
				buffer.append("<option value=\"\"");
			} else {
				buffer.append("<option value=\"\"");
			}
			/*
			 * if (TagUtil.noneSelected(optionList, selected)) {
			 * buffer.append(" selected=\"selected\""); }
			 */// End of if/else
			buffer.append("> Seleccione ... </option>");

			while (optionIterator.hasNext()) {
				CacheableDTO instance = (CacheableDTO) optionIterator.next();
				String codeValue = BeanUtils.getProperty(instance, id);
				String descriptionValue = BeanUtils.getProperty(instance,
						description);
				buffer.append("<option value=\"");
				buffer.append(codeValue);
				buffer.append("\"");
				if (codeValue.equals(selectedValue)) {
					buffer.append(" selected=\"selected\"");
				}
				// / End of if()
				buffer.append(">");
				buffer.append(descriptionValue);
				buffer.append("</option>");
			} // End of while()
			buffer.append("</select>");
		} catch (IllegalAccessException iaException) {
			throw new SystemException(iaException);
		} catch (InvocationTargetException itException) {
			throw new SystemException(itException);
		} catch (NoSuchMethodException nsmException) {
			throw new SystemException(nsmException);
		}
		return buffer.toString();
	}
}
