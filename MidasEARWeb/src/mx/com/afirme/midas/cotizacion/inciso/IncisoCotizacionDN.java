package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionSN;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionSN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionSN;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotSN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoSN;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IncisoCotizacionDN {
	public static final IncisoCotizacionDN INSTANCIA = new IncisoCotizacionDN();
	//Identificador del Subramo Incendio.
	@SuppressWarnings("unused")
	private final BigDecimal ID_SUBRAMO_INCENDIO = new BigDecimal(1);

	public static IncisoCotizacionDN getInstancia (){
		return IncisoCotizacionDN.INSTANCIA;
	}
	
	/**
	 * Method listarPorCotizacionId().
	 * @param idToCotizacion
	 * @return List<IncisoCotizacionDTO> lista de entidades
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarPorCotizacionId(BigDecimal idToCotizacion) 
			throws ExcepcionDeAccesoADatos,SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		//return incisoCotizacionSN.listarPorCotizacionId(idToCotizacion);
		return incisoCotizacionSN.getPorPropiedad("id.idToCotizacion", idToCotizacion);
	}
	/**
	 * Method guardar().
	 * @param incisoCotizacionDTO
	 * @param nombreUsuario
	 * @return IncisoCotizacionDTO
	 * @throws SystemException
	 */
	public IncisoCotizacionDTO guardar(IncisoCotizacionDTO incisoCotizacionDTO, String nombreUsuario) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		incisoCotizacionDTO = incisoCotizacionSN.guardar(incisoCotizacionDTO);
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
		CotizacionSN cotizacionSN = new CotizacionSN(nombreUsuario);
		cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
		incisoCotizacionDTO.setCotizacionDTO(cotizacionDTO);

		List<SeccionDTO> secciones = SeccionDN.getInstancia().listarVigentesPorIDTipoPoliza(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
			//seccionSN.buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza", incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
		for(SeccionDTO seccionDTO : secciones) {
			SeccionCotizacionDTOId id = new SeccionCotizacionDTOId();
			id.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToSeccion(seccionDTO.getIdToSeccion());
			id.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());

			SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
			seccionCotizacionDTO.setId(id);
			seccionCotizacionDTO.setIncisoCotizacionDTO(incisoCotizacionDTO);
			seccionCotizacionDTO.setSeccionDTO(seccionDTO);
			seccionCotizacionDTO.setValorPrimaNeta(0D);
			seccionCotizacionDTO.setValorSumaAsegurada(0D);
			//seccionCotizacionDTO.setClaveObligatoriedad((short)0);
			//seccionCotizacionDTO.setClaveContrato((short)0);
			seccionCotizacionDTO.setClaveObligatoriedad(new Short(seccionDTO.getClaveObligatoriedad()));
			if (seccionCotizacionDTO.getClaveObligatoriedad().intValue() == 2 || seccionCotizacionDTO.getClaveObligatoriedad().intValue() == 3)
				seccionCotizacionDTO.setClaveContrato((short)1);
			else
				seccionCotizacionDTO.setClaveContrato((short)0);

			SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
			seccionCotizacionSN.guardar(seccionCotizacionDTO);

			//SeccionDTO seccion = seccionSN.getPorIdCascada(seccionCotizacionDTO.getId().getIdToSeccion());
			List<CoberturaSeccionDTO> listaCoberturaSeccion = CoberturaSeccionDN.getInstancia().listarVigentesPorSeccion(seccionDTO.getIdToSeccion()); 
			for(CoberturaSeccionDTO coberturaSeccionDTO : listaCoberturaSeccion) {
				CoberturaCotizacionId idCoberturaCotizacion = new CoberturaCotizacionId();
				idCoberturaCotizacion.setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
				idCoberturaCotizacion.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
				idCoberturaCotizacion.setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
				idCoberturaCotizacion.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
				
				CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
				coberturaCotizacionDTO.setId(idCoberturaCotizacion);
				coberturaCotizacionDTO.setSeccionCotizacionDTO(seccionCotizacionDTO);
				coberturaCotizacionDTO.setCoberturaSeccionDTO(coberturaSeccionDTO);
				coberturaCotizacionDTO.setIdTcSubramo(coberturaSeccionDTO.getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
				//coberturaCotizacionDTO.setIdTcSubramo(UtileriasWeb.regresaBigDecimal("0"));
				coberturaCotizacionDTO.setValorPrimaNeta(0D);
				coberturaCotizacionDTO.setValorSumaAsegurada(0D);
				coberturaCotizacionDTO.setValorCoaseguro(0D);
				coberturaCotizacionDTO.setValorDeducible(0D);
				//coberturaCotizacionDTO.setClaveAutReaseguro((short)0);
				//coberturaCotizacionDTO.setClaveObligatoriedad((short)0);
				coberturaCotizacionDTO.setClaveObligatoriedad(new Short(coberturaSeccionDTO.getClaveObligatoriedad().toBigInteger().toString()));
				//coberturaCotizacionDTO.setClaveContrato((short)0);
				//La clave de contrataci�n se debe establecer en base a la clave obligatoriedad de la cobertura, previamente establecida a coberturaCotizacion
				if (coberturaCotizacionDTO.getClaveObligatoriedad().intValue() == 2 || coberturaCotizacionDTO.getClaveObligatoriedad().intValue() == 3)
					coberturaCotizacionDTO.setClaveContrato((short)1);
				else
					coberturaCotizacionDTO.setClaveContrato((short)0);
				//Nuevos campos
				coberturaCotizacionDTO.setValorCuota(0D);
				coberturaCotizacionDTO.setPorcentajeCoaseguro(0D);
				coberturaCotizacionDTO.setClaveAutCoaseguro((short)0);
				coberturaCotizacionDTO.setPorcentajeDeducible(0D);
				coberturaCotizacionDTO.setClaveAutDeducible((short)0);
				coberturaCotizacionDTO.setNumeroAgrupacion((short)0);

				CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
				coberturaCotizacionSN.guardar(coberturaCotizacionDTO);

				RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
				//Se deben tomar en cuenta s�lo los registros vigentes, que no han sido borrados logicamente.
				//buscarPorPropiedad("coberturaSeccionDTO.id", coberturaSeccionDTO.getId());
				List<RiesgoCoberturaDTO> riesgosCoberturas = riesgoCoberturaDN.listarRiesgoVigenteAsociado(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());
				for(RiesgoCoberturaDTO riesgo : riesgosCoberturas) {
					RiesgoCotizacionId idRiesgoCotizacion = new RiesgoCotizacionId();
					idRiesgoCotizacion.setIdToCobertura(riesgo.getId().getIdtocobertura());
					idRiesgoCotizacion.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
					idRiesgoCotizacion.setIdToRiesgo(riesgo.getId().getIdtoriesgo());
					idRiesgoCotizacion.setIdToSeccion(riesgo.getId().getIdtoseccion());
					idRiesgoCotizacion.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());

					RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
					riesgoCotizacionDTO.setId(idRiesgoCotizacion);
					riesgoCotizacionDTO.setRiesgoCoberturaDTO(riesgo);
					riesgoCotizacionDTO.setCoberturaCotizacionDTO(coberturaCotizacionDTO);
					//riesgoCotizacionDTO.setIdTcSubramo(UtileriasWeb.regresaBigDecimal("0"));
					riesgoCotizacionDTO.setIdTcSubramo(coberturaSeccionDTO.getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
					riesgoCotizacionDTO.setValorPrimaNeta(0D);
					riesgoCotizacionDTO.setValorSumaAsegurada(0D);
					riesgoCotizacionDTO.setValorCoaseguro(0D);
					riesgoCotizacionDTO.setValorDeducible(0D);
					riesgoCotizacionDTO.setClaveAutCoaseguro((short)0);
					riesgoCotizacionDTO.setClaveAutDeducible((short)0);
					riesgoCotizacionDTO.setClaveObligatoriedad(riesgo.getClaveObligatoriedad());
					if (riesgo.getClaveObligatoriedad().intValue() == 0)
						riesgoCotizacionDTO.setClaveContrato((short)1);
					else
						riesgoCotizacionDTO.setClaveContrato((short)0);
					riesgoCotizacionDTO.setValorAumento(0D);
					riesgoCotizacionDTO.setValorDescuento(0D);
					riesgoCotizacionDTO.setValorRecargo(0D);
					riesgoCotizacionDTO.setPorcentajeCoaseguro(0D);
					riesgoCotizacionDTO.setPorcentajeDeducible(0D);
					riesgoCotizacionDTO.setValorCuotaARDT(0D);
					riesgoCotizacionDTO.setValorCuotaB(0d);
					riesgoCotizacionDTO.setValorPrimaNetaARDT(0d);
					riesgoCotizacionDTO.setValorPrimaNetaB(0d);

					
					riesgoCotizacionSN.guardar(riesgoCotizacionDTO);

					List<DescuentoVarioCoberturaDTO> descuentosCobertura = riesgoCotizacionSN
							.getDescuentosCobertura(riesgoCotizacionDTO.getId().getIdToCobertura(), riesgoCotizacionDTO.getId().getIdToRiesgo());
					List<DescuentoVarioTipoPolizaDTO> descuentosTipoPoliza = riesgoCotizacionSN
							.getDescuentosTipoPoliza(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza(), riesgo.getId().getIdtocobertura());
					List<DescuentoVarioProductoDTO> descuentosProducto = riesgoCotizacionSN
							.getDescuentosProducto(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getIdToProducto(), incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
					RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
					riesgoCotizacionDTO.setDescuentoRiesgoCotizacionDTOs(riesgoCotizacionDN.getDescuentosRiesgoCotizacion(riesgoCotizacionDTO, descuentosCobertura, descuentosTipoPoliza, descuentosProducto));

					List<AumentoVarioCoberturaDTO> aumentosCobertura = riesgoCotizacionSN
							.getAumentosCobertura(riesgoCotizacionDTO.getId().getIdToCobertura(), riesgoCotizacionDTO.getId().getIdToRiesgo());
					List<AumentoVarioTipoPolizaDTO> aumentosTipoPoliza = riesgoCotizacionSN
							.getAumentosTipoPoliza(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza(), riesgo.getId().getIdtocobertura());
					List<AumentoVarioProductoDTO> aumentosProducto = riesgoCotizacionSN
							.getAumentosProducto(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getIdToProducto(), incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
					riesgoCotizacionDTO.setAumentoRiesgoCotizacionDTOs(riesgoCotizacionDN.getAumentosRiesgoCotizacion(riesgoCotizacionDTO, aumentosCobertura, aumentosTipoPoliza, aumentosProducto));

					List<RecargoVarioCoberturaDTO> recargosCobertura = riesgoCotizacionSN
							.getRecargosCobertura(riesgoCotizacionDTO.getId().getIdToCobertura(), riesgoCotizacionDTO.getId().getIdToRiesgo());
					List<RecargoVarioTipoPolizaDTO> recargosTipoPoliza = riesgoCotizacionSN
							.getRecargosTipoPoliza(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza(), riesgo.getId().getIdtocobertura());
					List<RecargoVarioProductoDTO> recargosProducto = riesgoCotizacionSN
							.getRecargosProducto(incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getIdToProducto(), incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
					riesgoCotizacionDTO.setRecargoRiesgoCotizacionDTOs(riesgoCotizacionDN.getRecargosRiesgoCotizacion(riesgoCotizacionDTO, recargosCobertura, recargosTipoPoliza, recargosProducto));

					riesgoCotizacionSN.modificar(riesgoCotizacionDTO);
/*					
					//Recargos y Descuentos
					RiesgoCotizacionId idRiesgo = new RiesgoCotizacionId();
					idRiesgo.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
					idRiesgo.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
					List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarPorIdFiltrado(idRiesgo);
					
					RecargoVarioDN recargoVarioDN = RecargoVarioDN.getInstancia();
					List<RecargoVarioDTO> recargos = recargoVarioDN.listarRecargosEspeciales();
					
					DescuentoDN descuentoDN = DescuentoDN.getInstancia();
					List<DescuentoDTO> descuentos = descuentoDN.listarDescuentosEspeciales();
					
					for(RiesgoCotizacionDTO riesgoCotizacion : riesgos) {
						for(RecargoVarioDTO recargo : recargos) {
							RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
							RecargoRiesgoCotizacionId idRecargo = new RecargoRiesgoCotizacionId();
							idRecargo.setIdToCotizacion(riesgoCotizacion.getId().getIdToCotizacion());
							idRecargo.setNumeroInciso(riesgoCotizacion.getId().getNumeroInciso());
							idRecargo.setIdToSeccion(riesgoCotizacion.getId().getIdToSeccion());
							idRecargo.setIdToCobertura(riesgoCotizacion.getId().getIdToCobertura());
							idRecargo.setIdToRiesgo(riesgoCotizacion.getId().getIdToRiesgo());
							idRecargo.setIdToRecargoVario(recargo.getIdtorecargovario());
							recargoRiesgoCotizacionDTO.setId(idRecargo);
							recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
							recargoRiesgoCotizacionDTO.setValorRecargo(0D);
							recargoRiesgoCotizacionDTO.setClaveObligatoriedad((short)0);
							recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
							recargoRiesgoCotizacionDTO.setClaveComercialTecnico(Sistema.TIPO_TECNICO);
							recargoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_COBERTURA);
							recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo);
							recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacion);
							
							try {
								RecargoRiesgoCotizacionDN recargoRiesgoCotizacionDN = RecargoRiesgoCotizacionDN.getInstancia();
								recargoRiesgoCotizacionDN.agregar(recargoRiesgoCotizacionDTO);
							} catch(Exception e) {
							}
						}
						for(DescuentoDTO descuento : descuentos) {
							DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
							DescuentoRiesgoCotizacionId idDescuento = new DescuentoRiesgoCotizacionId();
							idDescuento.setIdToCotizacion(riesgoCotizacion.getId().getIdToCotizacion());
							idDescuento.setNumeroInciso(riesgoCotizacion.getId().getNumeroInciso());
							idDescuento.setIdToSeccion(riesgoCotizacion.getId().getIdToSeccion());
							idDescuento.setIdToCobertura(riesgoCotizacion.getId().getIdToCobertura());
							idDescuento.setIdToRiesgo(riesgoCotizacion.getId().getIdToRiesgo());
							idDescuento.setIdToDescuentoVario(descuento.getIdToDescuentoVario());
							descuentoRiesgoCotizacionDTO.setId(idDescuento);
							descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
							descuentoRiesgoCotizacionDTO.setValorDescuento(0D);
							descuentoRiesgoCotizacionDTO.setClaveObligatoriedad((short)0);
							descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
							descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(Sistema.TIPO_TECNICO);
							descuentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_COBERTURA);
							descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento);
							descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacion);

							try {
								DescuentoRiesgoCotizacionDN descuentoRiesgoCotizacionDN = DescuentoRiesgoCotizacionDN.getInstancia();
								descuentoRiesgoCotizacionDN.agregar(descuentoRiesgoCotizacionDTO);
							} catch(Exception e) {
							}
						}
					}
*/					
				}
			}
		}
		riesgoCotizacionSN.insertarARD(incisoCotizacionDTO.getId().getIdToCotizacion(),
				incisoCotizacionDTO.getId().getNumeroInciso(),
				cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto(),
				cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
		
		return incisoCotizacionDTO;
	}
	
	/**
	 * copiar. Guarda un registro de inciso, pero con los datos contenidos en otro inciso, recibido como par�metro.
	 * @param IncisoCotizacionDTO incisoNuevo. El registro de inciso a guardar.
	 * @param IncisoCotizacionDTO incisoOriginal. El inciso del cual se copiar�n los datos.
	 * @return IncisoCotizacionDTO el nuevo inciso guardado.
	 * @throws SystemException
	 */
	public IncisoCotizacionDTO copiar(IncisoCotizacionDTO incisoNuevo, IncisoCotizacionDTO incisoOriginal, String nombreUsuario) throws SystemException {
		DireccionDTO direccionDTO = incisoOriginal.getDireccionDTO();
		direccionDTO.setIdToDireccion(null);
		DireccionDN direccionDN = DireccionDN.getInstancia();
		direccionDTO = direccionDN.agregar(direccionDTO);
		incisoNuevo.setDireccionDTO(direccionDTO);
		
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		incisoNuevo = incisoCotizacionSN.guardar(incisoNuevo);

		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
		CotizacionSN cotizacionSN = new CotizacionSN(nombreUsuario);
		cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
		incisoNuevo.setCotizacionDTO(cotizacionDTO);

		//Copiar los registros de DatoIncisoCotizacion
		DatoIncisoCotizacionId idDatoInciso = new DatoIncisoCotizacionId();
		idDatoInciso.setIdToCotizacion(incisoOriginal.getId().getIdToCotizacion());
		idDatoInciso.setNumeroInciso(incisoOriginal.getId().getNumeroInciso());
		List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(idDatoInciso);
		DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
		for(DatoIncisoCotizacionDTO datoInciso : listaDatosInciso){
			datoInciso.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
			datoInciso.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
			try{	datoIncisoCotizacionSN.agregar(datoInciso);
			}catch (Exception e){e.printStackTrace();}
		}
		
		SeccionSN seccionSN = new SeccionSN();
		List<SeccionDTO> secciones = seccionSN.buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza", incisoNuevo.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
		for(SeccionDTO seccionDTO : secciones) {
			//Copiar los registros SeccionCotizacion
			SeccionCotizacionDTOId idOriginal = new SeccionCotizacionDTOId();
			idOriginal.setIdToCotizacion(incisoOriginal.getId().getIdToCotizacion());
			idOriginal.setIdToSeccion(seccionDTO.getIdToSeccion());
			idOriginal.setNumeroInciso(incisoOriginal.getId().getNumeroInciso());

			SeccionCotizacionDTO seccionCotizacionOriginal = null;
			seccionCotizacionOriginal = SeccionCotizacionDN.getInstancia().buscarPorId(idOriginal);
			if(seccionCotizacionOriginal != null){
				//Se crea una nueva SeccionCotizacion con los datos del nuevo inciso en la llave.
				SeccionCotizacionDTOId idNuevo = new SeccionCotizacionDTOId();
				idNuevo.setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
				idNuevo.setIdToSeccion(seccionDTO.getIdToSeccion());
				idNuevo.setNumeroInciso(incisoNuevo.getId().getNumeroInciso());

				SeccionCotizacionDTO seccionCotizacionNuevo = new SeccionCotizacionDTO();
				seccionCotizacionNuevo.setId(idNuevo);
				seccionCotizacionNuevo.setIncisoCotizacionDTO(incisoNuevo);
				seccionCotizacionNuevo.setSeccionDTO(seccionDTO);
				//Los datos de la SeccionCotizacion obtenida del incisoOriginal se pasan al nuevo seccionCotizacion
				seccionCotizacionNuevo.setValorPrimaNeta(seccionCotizacionOriginal.getValorPrimaNeta());
				seccionCotizacionNuevo.setValorPrimaNetaOriginal(seccionCotizacionOriginal.getValorPrimaNetaOriginal());
				seccionCotizacionNuevo.setValorSumaAsegurada(seccionCotizacionOriginal.getValorSumaAsegurada());
				seccionCotizacionNuevo.setClaveObligatoriedad(seccionCotizacionOriginal.getClaveObligatoriedad());
				seccionCotizacionNuevo.setClaveContrato(seccionCotizacionOriginal.getClaveContrato());
				
				//Se guarda el nuevo registro de SeccionCotizacion correspondiente al nuevo inciso
				try{	new SeccionCotizacionSN().guardar(seccionCotizacionNuevo);
				}catch(Exception e){e.printStackTrace();}
				
				//Copiarlos subincisos
				SubIncisoCotizacionDTOId subIncisoCotizacionId = new SubIncisoCotizacionDTOId();
				subIncisoCotizacionId.setIdToCotizacion(incisoOriginal.getId().getIdToCotizacion());
				subIncisoCotizacionId.setNumeroInciso(incisoOriginal.getId().getNumeroInciso());
				subIncisoCotizacionId.setIdToSeccion(seccionCotizacionOriginal.getId().getIdToSeccion());
				SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
				subIncisoCotizacionDTO.setId(subIncisoCotizacionId);
				List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = null;
				subIncisoCotizacionDTOLista = new SubIncisoCotSN().listarFiltrado(subIncisoCotizacionDTO);
				SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN();
				if (subIncisoCotizacionDTOLista != null)
					for (SubIncisoCotizacionDTO subIncisoCotizacion : subIncisoCotizacionDTOLista) {
						subIncisoCotizacion.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
						try{	subIncisoCotSN.agregar(subIncisoCotizacion);
						}catch (Exception e){e.printStackTrace();}
					}

				SeccionDTO seccion = seccionSN.getPorIdCascada(seccionCotizacionOriginal.getId().getIdToSeccion());
				for(CoberturaSeccionDTO coberturaSeccionDTO : seccion.getCoberturas()) {
					//Se recuperan los registros CoberturaCotizacion del inciso Original
					CoberturaCotizacionId idCoberturaCotizacionOriginal = new CoberturaCotizacionId();
					idCoberturaCotizacionOriginal.setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
					idCoberturaCotizacionOriginal.setIdToCotizacion(incisoOriginal.getId().getIdToCotizacion());
					idCoberturaCotizacionOriginal.setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
					idCoberturaCotizacionOriginal.setNumeroInciso(incisoOriginal.getId().getNumeroInciso());
					
					CoberturaCotizacionDTO coberturaCotizacionOriginal = new CoberturaCotizacionDTO();
					coberturaCotizacionOriginal.setId(idCoberturaCotizacionOriginal);
					coberturaCotizacionOriginal = CoberturaCotizacionDN.getInstancia().getPorId(coberturaCotizacionOriginal);
					
					if (coberturaCotizacionOriginal != null){
						//Se crea un nuevo registro de CoberturaCotizacion para asignarlo al nuevo inciso
						CoberturaCotizacionId idCoberturaCotizacionNuevo = new CoberturaCotizacionId();
						idCoberturaCotizacionNuevo.setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
						idCoberturaCotizacionNuevo.setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
						idCoberturaCotizacionNuevo.setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
						idCoberturaCotizacionNuevo.setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
						
						CoberturaCotizacionDTO coberturaCotizacionNueva= new CoberturaCotizacionDTO();
						coberturaCotizacionNueva.setId(idCoberturaCotizacionNuevo);
						coberturaCotizacionNueva.setSeccionCotizacionDTO(seccionCotizacionNuevo);
						coberturaCotizacionNueva.setCoberturaSeccionDTO(coberturaSeccionDTO);
						
						//Al nuevo registro se le establecen los valores del registro perteneciente al inciso original
						coberturaCotizacionNueva.setIdTcSubramo(coberturaCotizacionOriginal.getIdTcSubramo());
						coberturaCotizacionNueva.setValorPrimaNeta(coberturaCotizacionOriginal.getValorPrimaNeta());
						coberturaCotizacionNueva.setValorPrimaNetaOriginal(coberturaCotizacionOriginal.getValorPrimaNetaOriginal());
						coberturaCotizacionNueva.setValorSumaAsegurada(coberturaCotizacionOriginal.getValorSumaAsegurada());
						coberturaCotizacionNueva.setValorCoaseguro(coberturaCotizacionOriginal.getValorCoaseguro());
						coberturaCotizacionNueva.setValorDeducible(coberturaCotizacionOriginal.getValorDeducible());
						coberturaCotizacionNueva.setClaveObligatoriedad(coberturaCotizacionOriginal.getClaveObligatoriedad());
						coberturaCotizacionNueva.setValorCuota(coberturaCotizacionOriginal.getValorCuota());
						coberturaCotizacionNueva.setValorCuotaOriginal(coberturaCotizacionOriginal.getValorCuotaOriginal());
						coberturaCotizacionNueva.setPorcentajeCoaseguro(coberturaCotizacionOriginal.getPorcentajeCoaseguro());
						coberturaCotizacionNueva.setClaveContrato(coberturaCotizacionOriginal.getClaveContrato());
						if (coberturaCotizacionOriginal.getClaveAutCoaseguro().intValue() != 0)
							coberturaCotizacionNueva.setClaveAutCoaseguro((short)1);
						else
							coberturaCotizacionNueva.setClaveAutCoaseguro((short)0);
						coberturaCotizacionNueva.setPorcentajeDeducible(coberturaCotizacionOriginal.getPorcentajeDeducible());
						if (coberturaCotizacionOriginal.getClaveAutCoaseguro().intValue() != 0)
							coberturaCotizacionNueva.setClaveAutDeducible((short)1);
						else
							coberturaCotizacionNueva.setClaveAutDeducible((short)0);
						/*if (coberturaCotizacionOriginal.getClaveAutReaseguro().intValue() != 0)
							coberturaCotizacionNueva.setClaveAutReaseguro((short)1);
						else
							coberturaCotizacionNueva.setClaveAutReaseguro((short)0);*/
						coberturaCotizacionNueva.setNumeroAgrupacion(coberturaCotizacionOriginal.getNumeroAgrupacion());
	
						try{
							new CoberturaCotizacionSN().guardar(coberturaCotizacionNueva);
						}catch(Exception e){e.printStackTrace();}
						
						//Copiar los registros de DetallePrimaCoberturaCotizacion
						
	
						//Por cada cobertura se obtienen los riesgos
						RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
						List<RiesgoCoberturaDTO> riesgosCoberturas = riesgoCoberturaDN.listarRiesgoVigenteAsociado(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());//buscarPorPropiedad("coberturaSeccionDTO.id", coberturaSeccionDTO.getId());
						for(RiesgoCoberturaDTO riesgo : riesgosCoberturas) {
							//Se recupera el registro RiesgoCotizacionDTO del inciso original
							RiesgoCotizacionId idRiesgoCotizacionOriginal = new RiesgoCotizacionId();
							idRiesgoCotizacionOriginal.setIdToCobertura(riesgo.getId().getIdtocobertura());
							idRiesgoCotizacionOriginal.setIdToCotizacion(incisoOriginal.getId().getIdToCotizacion());
							idRiesgoCotizacionOriginal.setIdToRiesgo(riesgo.getId().getIdtoriesgo());
							idRiesgoCotizacionOriginal.setIdToSeccion(riesgo.getId().getIdtoseccion());
							idRiesgoCotizacionOriginal.setNumeroInciso(incisoOriginal.getId().getNumeroInciso());
							RiesgoCotizacionDTO riesgoCotizacionOriginal = new RiesgoCotizacionDTO();
							riesgoCotizacionOriginal.setId(idRiesgoCotizacionOriginal);
							riesgoCotizacionOriginal = new RiesgoCotizacionSN().getPorId(idRiesgoCotizacionOriginal);
							
							//Se crea el nuevo registro RiesgoCotizacionDTO para el nuevo inciso
							/*RiesgoCotizacionId idRiesgoCotizacionNuevo = new RiesgoCotizacionId();
							idRiesgoCotizacionNuevo.setIdToCobertura(riesgo.getId().getIdtocobertura());
							idRiesgoCotizacionNuevo.setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
							idRiesgoCotizacionNuevo.setIdToRiesgo(riesgo.getId().getIdtoriesgo());
							idRiesgoCotizacionNuevo.setIdToSeccion(riesgo.getId().getIdtoseccion());
							idRiesgoCotizacionNuevo.setNumeroInciso(incisoNuevo.getId().getNumeroInciso());*/
							
							RiesgoCotizacionDTO riesgoCotizacionNuevo= new RiesgoCotizacionDTO();
							//riesgoCotizacionNuevo.setId(idRiesgoCotizacionNuevo);
							riesgoCotizacionNuevo = new RiesgoCotizacionSN().getPorId(idRiesgoCotizacionOriginal);
							if (riesgoCotizacionNuevo != null){
								riesgoCotizacionNuevo.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
								riesgoCotizacionNuevo.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
								//Al nuevo registro se le establecen los valores del registro original
								riesgoCotizacionNuevo.setRiesgoCoberturaDTO(riesgo);
								riesgoCotizacionNuevo.setCoberturaCotizacionDTO(coberturaCotizacionNueva);
								riesgoCotizacionNuevo.setIdTcSubramo(riesgoCotizacionOriginal.getIdTcSubramo());
								riesgoCotizacionNuevo.setValorSumaAsegurada(riesgoCotizacionOriginal.getValorSumaAsegurada());
								riesgoCotizacionNuevo.setValorCoaseguro(riesgoCotizacionOriginal.getValorCoaseguro());
								riesgoCotizacionNuevo.setValorDeducible(riesgoCotizacionOriginal.getValorDeducible());
								if (riesgoCotizacionOriginal.getClaveAutCoaseguro().intValue() != 0)
									riesgoCotizacionNuevo.setClaveAutCoaseguro((short)1);
								else
									riesgoCotizacionNuevo.setClaveAutCoaseguro((short)0);
								if (riesgoCotizacionOriginal.getClaveAutDeducible().intValue() != 0)
									riesgoCotizacionNuevo.setClaveAutDeducible((short)1);
								else
									riesgoCotizacionNuevo.setClaveAutDeducible((short)0);
								 
								riesgoCotizacionNuevo.setClaveObligatoriedad((short)0);
								riesgoCotizacionNuevo.setClaveContrato(riesgoCotizacionOriginal.getClaveContrato());
								// TODO riesgoCotizacionNuevo.setClaveContrato((short)0);
								riesgoCotizacionNuevo.setValorAumento(riesgoCotizacionOriginal.getValorAumento());
								riesgoCotizacionNuevo.setValorDescuento(riesgoCotizacionOriginal.getValorDescuento());
								riesgoCotizacionNuevo.setValorRecargo(riesgoCotizacionOriginal.getValorRecargo());
								riesgoCotizacionNuevo.setPorcentajeCoaseguro(riesgoCotizacionOriginal.getPorcentajeCoaseguro());
								riesgoCotizacionNuevo.setPorcentajeDeducible(riesgoCotizacionOriginal.getPorcentajeDeducible());
								riesgoCotizacionNuevo.setValorCuotaB(riesgoCotizacionOriginal.getValorCuotaB());
								riesgoCotizacionNuevo.setValorCuotaARDT(riesgoCotizacionOriginal.getValorCuotaARDT());
								riesgoCotizacionNuevo.setValorPrimaNeta(riesgoCotizacionOriginal.getValorPrimaNeta());
								riesgoCotizacionNuevo.setValorPrimaNetaARDT(riesgoCotizacionOriginal.getValorPrimaNetaARDT());
								riesgoCotizacionNuevo.setValorPrimaNetaB(riesgoCotizacionOriginal.getValorPrimaNetaB());
		
								//Se guarda el nuevo registro de RiesgoCotizacion, correspondiente al nuevo inciso.
								try{
									new RiesgoCotizacionSN().guardar(riesgoCotizacionNuevo);
								}catch(Exception e){e.printStackTrace();}
			
								//Recuperar los aumentos del riesgoCotizacionOriginal
								List<AumentoRiesgoCotizacionDTO> aumentosRiesgoCotizacionOriginal = null;
								try{
									aumentosRiesgoCotizacionOriginal = AumentoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacionOriginal);
								}catch(Exception e){e.printStackTrace();}
								//A cada aumento del riesgoCotizacion Original, se le establecen los id�s del nuevo inciso
								if (aumentosRiesgoCotizacionOriginal != null){
									for(AumentoRiesgoCotizacionDTO aumento : aumentosRiesgoCotizacionOriginal){
										aumento.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
										aumento.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
										//TODO aumento.setClaveContrato((short)0);
										if (aumento.getClaveAutorizacion() != 0)
											aumento.setClaveAutorizacion((short)1);
										try{
											AumentoRiesgoCotizacionDN.getInstancia().agregar(aumento);
										}catch(Exception e){e.printStackTrace();}
									}
								}
								
								//Recuperar los descuentos del riesgoCotizacionOriginal
								List<DescuentoRiesgoCotizacionDTO> descuentosRiesgoCotizacionOriginal = null;
								try{
									descuentosRiesgoCotizacionOriginal = DescuentoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacionOriginal);
								}catch(Exception e){e.printStackTrace();}
								//A cada aumento del riesgoCotizacion Original, se le establecen los id�s del nuevo inciso
								if (descuentosRiesgoCotizacionOriginal != null){
									for(DescuentoRiesgoCotizacionDTO descuento : descuentosRiesgoCotizacionOriginal){
										descuento.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
										descuento.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
										//TODO descuento.setClaveContrato((short)0);
										if (descuento.getClaveAutorizacion() != 0)
											descuento.setClaveAutorizacion((short)1);
										try{
											DescuentoRiesgoCotizacionDN.getInstancia().agregar(descuento);
										}catch(Exception e){e.printStackTrace();}
									}
								}
								
								//Recuperar los recargos del riesgoCotizacionOriginal
								List<RecargoRiesgoCotizacionDTO> recargosRiesgoCotizacionOriginal = null;
								try{
									recargosRiesgoCotizacionOriginal = RecargoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacionOriginal);
								}catch(Exception e){e.printStackTrace();}
								//A cada aumento del riesgoCotizacion Original, se le establecen los id�s del nuevo inciso
								if (recargosRiesgoCotizacionOriginal != null){
									for(RecargoRiesgoCotizacionDTO recargo: recargosRiesgoCotizacionOriginal){
										recargo.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
										recargo.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
										//recargo.setClaveContrato((short)0);
										if (recargo.getClaveAutorizacion() != 0)
											recargo.setClaveAutorizacion((short)1);
										try{
											RecargoRiesgoCotizacionDN.getInstancia().agregar(recargo);
										}catch(Exception e){e.printStackTrace();}
									}
								}
								
								//Copiar los registros de DetallePrimaRiesgoCotizacion
								List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgo = DetallePrimaRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacionOriginal);
								for(DetallePrimaRiesgoCotizacionDTO detallePrimaRiesgo : listaDetallePrimaRiesgo){
										detallePrimaRiesgo.getId().setIdToCotizacion(incisoNuevo.getId().getIdToCotizacion());
										detallePrimaRiesgo.getId().setNumeroInciso(incisoNuevo.getId().getNumeroInciso());
										try{
											DetallePrimaRiesgoCotizacionDN.getInstancia().agregar(detallePrimaRiesgo); 
										}catch(Exception e){e.printStackTrace();}
								}
							}//Fin validacion RiesgoCotizacion Nuevo != null
							
						}//Fin iterar riesgosCotizacion
					}//Fin validar coberturaCotizacionOriginal != null
				}//Fin iterar CoberturasCotuzacion
			}//Fin condicion seccionCotizacionNueva != null
		}//Fin iterar SeccionCotizacion
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(incisoNuevo.getId().getIdToCotizacion());
		return incisoNuevo;
	}
	/*
	 * copiarDetallesPrima. Copia los registros de DetallePrimaCoberturaCotizacionDTO y DetallePrimaRiesgoCotizacionDTO
	 * del incisoCotizacionOriginal al incisoCotizacionNuevo. 
	 * @param incisoCotizacionNuevo
	 * @param incisoCotizacionOriginal
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	/*public void copiarDetallesPrima(IncisoCotizacionDTO incisoCotizacionNuevo,IncisoCotizacionDTO incisoCotizacionOriginal) throws ExcepcionDeAccesoADatos, SystemException{
		if (incisoCotizacionOriginal.getCotizacionDTO() == null){
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(incisoCotizacionOriginal.getId().getIdToCotizacion());
			cotizacionDTO = new CotizacionSN().getPorId(cotizacionDTO);
			incisoCotizacionOriginal.setCotizacionDTO(cotizacionDTO);
		}
		if(incisoCotizacionNuevo.getCotizacionDTO() == null){
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(incisoCotizacionNuevo.getId().getIdToCotizacion());
			cotizacionDTO = new CotizacionSN().getPorId(cotizacionDTO);
			incisoCotizacionNuevo.setCotizacionDTO(cotizacionDTO);
		}

		SeccionSN seccionSN = new SeccionSN();
		List<SeccionDTO> secciones = seccionSN.buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza", incisoCotizacionOriginal.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
		for(SeccionDTO seccionDTO : secciones) {
			SeccionCotizacionDTOId id = new SeccionCotizacionDTOId();
			id.setIdToCotizacion(incisoCotizacionOriginal.getId().getIdToCotizacion());
			id.setIdToSeccion(seccionDTO.getIdToSeccion());
			id.setNumeroInciso(incisoCotizacionOriginal.getId().getNumeroInciso());

			SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
			seccionCotizacionDTO.setId(id);

			SeccionDTO seccion = seccionSN.getPorIdCascada(seccionCotizacionDTO.getId().getIdToSeccion());
			for(CoberturaSeccionDTO coberturaSeccionDTO : seccion.getCoberturas()) {
				CoberturaCotizacionId idCoberturaCotizacion = new CoberturaCotizacionId();
				idCoberturaCotizacion.setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
				idCoberturaCotizacion.setIdToCotizacion(incisoCotizacionOriginal.getId().getIdToCotizacion());
				idCoberturaCotizacion.setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
				idCoberturaCotizacion.setNumeroInciso(incisoCotizacionOriginal.getId().getNumeroInciso());
				
				CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
				coberturaCotizacionDTO.setId(idCoberturaCotizacion);
				
				RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
				List<RiesgoCoberturaDTO> riesgosCoberturas = riesgoCoberturaDN.buscarPorPropiedad("coberturaSeccionDTO.id", coberturaSeccionDTO.getId());
				for(RiesgoCoberturaDTO riesgo : riesgosCoberturas) {
					RiesgoCotizacionId idRiesgoCotizacion = new RiesgoCotizacionId();
					idRiesgoCotizacion.setIdToCobertura(riesgo.getId().getIdtocobertura());
					idRiesgoCotizacion.setIdToCotizacion(incisoCotizacionOriginal.getId().getIdToCotizacion());
					idRiesgoCotizacion.setIdToRiesgo(riesgo.getId().getIdtoriesgo());
					idRiesgoCotizacion.setIdToSeccion(riesgo.getId().getIdtoseccion());
					idRiesgoCotizacion.setNumeroInciso(incisoCotizacionOriginal.getId().getNumeroInciso());

					RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
					riesgoCotizacionDTO.setId(idRiesgoCotizacion);
					
					//Copiar los registros de DetallePrimaRiesgoCotizacion
					List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgo = DetallePrimaRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacion);
					for(DetallePrimaRiesgoCotizacionDTO detallePrimaRiesgo : listaDetallePrimaRiesgo){
						try{
							detallePrimaRiesgo.getId().setIdToCotizacion(incisoCotizacionNuevo.getId().getIdToCotizacion());
							detallePrimaRiesgo.getId().setNumeroInciso(incisoCotizacionNuevo.getId().getNumeroInciso());
							DetallePrimaRiesgoCotizacionDN.getInstancia().agregar(detallePrimaRiesgo); 
						}catch(Exception e){}
					}
				}
				//Copiar los registros de DetallePrimaRiesgoCotizacion
				List<DetallePrimaCoberturaCotizacionDTO> listaDetalleCoberturaRiesgo = DetallePrimaCoberturaCotizacionDN.getInstancia().findByCoberturaCotizacion(idCoberturaCotizacion);
				for(DetallePrimaCoberturaCotizacionDTO detallePrimaCobertura : listaDetalleCoberturaRiesgo){
					try{
						detallePrimaCobertura.getId().setIdToCotizacion(incisoCotizacionNuevo.getId().getIdToCotizacion());
						detallePrimaCobertura.getId().setNumeroInciso(incisoCotizacionNuevo.getId().getNumeroInciso());
						DetallePrimaCoberturaCotizacionDN.getInstancia().agregar(detallePrimaCobertura);
					}
					catch(Exception e){}
				}
			}
		}
	}*/

	/**
	 * Method modificar().
	 * @param incisoCotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public IncisoCotizacionDTO modificar(IncisoCotizacionDTO incisoCotizacionDTO) throws SystemException{
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.modificar(incisoCotizacionDTO);
	}
	/**
	 * Method getPorId(). Busca los datos de un Inciso de  la Cotizacion por su id
	 * @param incisoCotizacionDTO
	 * @return IncisoCotizacionDTO
	 * @throws SystemException
	 */
	public IncisoCotizacionDTO getPorId(IncisoCotizacionDTO incisoCotizacionDTO) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.getPorId(incisoCotizacionDTO.getId());
	}
	
	/**
	 * Method getPorId(). Busca los datos de un Inciso de  la Cotizacion por su id
	 * @param incisoCotizacionDTO
	 * @return IncisoCotizacionDTO
	 * @throws SystemException
	 */
	public IncisoCotizacionDTO getPorId(IncisoCotizacionId incisoCotizacionId) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.getPorId(incisoCotizacionId);
	}
	
	/**
	 * Method getSubIncisoCotizacionPorId().
	 * @param subIncisoCotizacionDTOId
	 * @return SubIncisoCotizacionDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public SubIncisoCotizacionDTO getSubIncisoCotizacionPorId(SubIncisoCotizacionDTOId subIncisoCotizacionDTOId)throws ExcepcionDeAccesoADatos,SystemException {
		SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN();
		return subIncisoCotSN.getPorId(subIncisoCotizacionDTOId);
	}
	/**
	 * Method getSubIncisoPorId().
	 * @param id
	 * @return SubIncisoDTO
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public SubIncisoDTO getSubIncisoPorId(BigDecimal id)throws ExcepcionDeAccesoADatos,SystemException {
		SubIncisoSN subIncisoSN = new SubIncisoSN();
		return subIncisoSN.getPorId(id);
	}
	/**
	 * Method obtenerNumeroIncisoSiguiente().
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public BigDecimal obtenerNumeroIncisoSiguiente(BigDecimal idToCotizacion) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.obtenerNumeroIncisoSiguiente(idToCotizacion).add(BigDecimal.ONE);
	}
	/**
	 * Method  borrar().
	 * @param incisoCotizacionDTO
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	public void borrar(IncisoCotizacionDTO incisoCotizacionDTO, String nombreUsuario) throws SystemException {
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
		CotizacionSN cotizacionSN = new CotizacionSN(nombreUsuario);
		cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
		incisoCotizacionDTO.setCotizacionDTO(cotizacionDTO);

		SeccionSN seccionSN = new SeccionSN();
		List<SeccionDTO> secciones = seccionSN.buscarPorPropiedad("tipoPolizaDTO.idToTipoPoliza", incisoCotizacionDTO.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza());
		for(SeccionDTO seccionDTO : secciones) {
			SeccionCotizacionDTOId id = new SeccionCotizacionDTOId();
			id.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToSeccion(seccionDTO.getIdToSeccion());
			id.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());

			SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();

			seccionCotizacionDTO = SeccionCotizacionDN.getInstancia().buscarPorId(id);
			if (seccionCotizacionDTO != null){
				SeccionDTO seccion = seccionSN.getPorIdCascada(seccionCotizacionDTO.getId().getIdToSeccion());
				for(CoberturaSeccionDTO coberturaSeccionDTO : seccion.getCoberturas()) {
					CoberturaCotizacionId idCoberturaCotizacion = new CoberturaCotizacionId();
					idCoberturaCotizacion.setIdToCobertura(coberturaSeccionDTO.getId().getIdtocobertura());
					idCoberturaCotizacion.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
					idCoberturaCotizacion.setIdToSeccion(coberturaSeccionDTO.getId().getIdtoseccion());
					idCoberturaCotizacion.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
					
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					coberturaCotizacionDTO.setId(idCoberturaCotizacion);
					
					RiesgoCoberturaDN riesgoCoberturaDN = RiesgoCoberturaDN.getInstancia();
					List<RiesgoCoberturaDTO> riesgosCoberturas = riesgoCoberturaDN.listarRiesgoVigenteAsociado(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());//buscarPorPropiedad("coberturaSeccionDTO.id", coberturaSeccionDTO.getId());
					for(RiesgoCoberturaDTO riesgo : riesgosCoberturas) {
						RiesgoCotizacionId idRiesgoCotizacion = new RiesgoCotizacionId();
						idRiesgoCotizacion.setIdToCobertura(riesgo.getId().getIdtocobertura());
						idRiesgoCotizacion.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
						idRiesgoCotizacion.setIdToRiesgo(riesgo.getId().getIdtoriesgo());
						idRiesgoCotizacion.setIdToSeccion(riesgo.getId().getIdtoseccion());
						idRiesgoCotizacion.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
	
						RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
						riesgoCotizacionDTO.setId(idRiesgoCotizacion);
						
						//Borrar los aumentos del riesgo Cotizacion
						List<AumentoRiesgoCotizacionDTO> listaAumentosRiesgoCotizacion = AumentoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacion);
						for(AumentoRiesgoCotizacionDTO aumentoRiesgoCot : listaAumentosRiesgoCotizacion){
							try{	AumentoRiesgoCotizacionDN.getInstancia().borrar(aumentoRiesgoCot);}
							catch(Exception e){}
						}
						//Borrar los descuentos del riesgo Cotizacion
						List<DescuentoRiesgoCotizacionDTO> listaDescuentosRiesgoCotizacion = DescuentoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacion);
						for(DescuentoRiesgoCotizacionDTO descuentoRiesgoCot : listaDescuentosRiesgoCotizacion){
							try{	DescuentoRiesgoCotizacionDN.getInstancia().borrar(descuentoRiesgoCot);}
							catch(Exception e){}
						}
						//Borrar los recargos del riesgo Cotizacion
						List<RecargoRiesgoCotizacionDTO> listaRecargosRiesgoCotizacion = RecargoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacion);
						for(RecargoRiesgoCotizacionDTO recargoRiesgoCot : listaRecargosRiesgoCotizacion){
							try{	RecargoRiesgoCotizacionDN.getInstancia().borrar(recargoRiesgoCot);}
							catch(Exception e){}
						}
						//Borrar los registros de DetallePrimaRiesgoCotizacion
						List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgo = DetallePrimaRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(idRiesgoCotizacion);
						for(DetallePrimaRiesgoCotizacionDTO detallePrimaRiesgo : listaDetallePrimaRiesgo){
							try{ DetallePrimaRiesgoCotizacionDN.getInstancia().borrar(detallePrimaRiesgo); }
							catch(Exception e){}
						}
						//Borrar el RiesgoCotizacion
						RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
						try{	riesgoCotizacionSN.borrar(riesgoCotizacionDTO);
						}catch(Exception exc){}
					}
					//Borrar los registros de DetallePrimaCoberturaCotizacion
					List<DetallePrimaCoberturaCotizacionDTO> listaDetalleCoberturaRiesgo = DetallePrimaCoberturaCotizacionDN.getInstancia().findByCoberturaCotizacion(idCoberturaCotizacion);
					for(DetallePrimaCoberturaCotizacionDTO detallePrimaCobertura : listaDetalleCoberturaRiesgo){
						try{ DetallePrimaCoberturaCotizacionDN.getInstancia().borrar(detallePrimaCobertura); }
						catch(Exception e){}
					}
					
					CoberturaCotizacionSN coberturaCotizacionSN = new CoberturaCotizacionSN();
					try{coberturaCotizacionSN.borrar(coberturaCotizacionDTO);
					}catch(Exception e){}
					//Borrar los registros de DatoIncisoCotizacion
					DatoIncisoCotizacionId idDatoInciso = new DatoIncisoCotizacionId();
					idDatoInciso.setIdToCotizacion(incisoCotizacionDTO.getCotizacionDTO().getIdToCotizacion());
					idDatoInciso.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
					List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(idDatoInciso);
					DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
					for(DatoIncisoCotizacionDTO datoInciso : listaDatosInciso){
						try{	datoIncisoCotizacionSN.borrar(datoInciso);
						}catch (Exception e){}
					}
				}
				//Borrar los subincisos
				SubIncisoCotizacionDTOId subIncisoCotizacionId = new SubIncisoCotizacionDTOId();
				subIncisoCotizacionId.setIdToCotizacion(incisoCotizacionDTO.getId().getIdToCotizacion());
				subIncisoCotizacionId.setNumeroInciso(incisoCotizacionDTO.getId().getNumeroInciso());
				subIncisoCotizacionId.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
				SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
				subIncisoCotizacionDTO.setId(subIncisoCotizacionId);
				
				List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = null;
				subIncisoCotizacionDTOLista = new SubIncisoCotSN().listarFiltrado(subIncisoCotizacionDTO);
				SubIncisoCotSN subIncisoCotSN = new SubIncisoCotSN(); 
				if (subIncisoCotizacionDTOLista != null)
					for (SubIncisoCotizacionDTO subIncisoCotizacion : subIncisoCotizacionDTOLista) {
						try{
							subIncisoCotSN.borrar(subIncisoCotizacion);
						}catch (Exception e){}
					}
				
				SeccionCotizacionSN seccionCotizacionSN = new SeccionCotizacionSN();
				seccionCotizacionSN.borrar(seccionCotizacionDTO);
			}
		}
		//Borrar los DatoIncisoCotizacion
		DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
		datoIncisoCotizacionSN.borrarTodos(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
		//Borra las agrupaciones de la cotizacion
		PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
		primerRiesgoLUCDN.borrarAgrupaciones(incisoCotizacionDTO.getId().getIdToCotizacion());
		//Borrar el inciso
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		incisoCotizacionSN.borrar(incisoCotizacionDTO);
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(incisoCotizacionDTO.getId().getIdToCotizacion());
	}
	/**
	 * Method getPorPropiedad().
	 * @param propiedad
	 * @param object
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> getPorPropiedad(String propiedad, Object object) throws SystemException{
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		return incisoCotizacionSN.getPorPropiedad(propiedad, object);
	}
	/**
	 * Method calcularSumaAsegurada().
	 * @param incisoCotizacionDTO
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public Double calcularSumaAsegurada(IncisoCotizacionDTO incisoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		Double sumaAsegurada = new Double("0");
		List<SeccionCotizacionDTO>  seccionCotizacionDTOList = SeccionCotizacionDN.getInstancia().listarPorIncisoId(incisoCotizacionDTO.getId());
		for (SeccionCotizacionDTO seccionCotizacionDTO : seccionCotizacionDTOList) {
			if (seccionCotizacionDTO.getClaveContrato() == 1){
				sumaAsegurada = sumaAsegurada + seccionCotizacionDTO.getValorSumaAsegurada();
			}
		}
		
		return sumaAsegurada;
		
	}
	/**
	 * Method listarAutorizacionesPendientesYRechazadasPorCotizacion().
	 * @param idToCotizacion
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarAutorizacionesPendientesYRechazadasPorCotizacion(BigDecimal idToCotizacion) throws SystemException{
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		
		return incisoCotizacionSN.listarAutorizacionesPendientesYRechazadasPorCotizacion(idToCotizacion);
	}
	
	/**
	 * Lista los incisos de una cotizacion que tengan las coberturas recibidas con la claveContrato recibido.
	 * @param BigDecimal idToCotizacion @param List<CoberturaCotizacionDTO> Lista de coberturas con la claveContrato recibida
	 * @param claveContrato
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas pro el query
	 * @throws SystemException 
	 */
	public List<IncisoCotizacionDTO> listarIncisosPorCoberturaContratada(BigDecimal idToCotizacion,List<CoberturaCotizacionDTO> coberturas,Short claveContrato) throws SystemException {
		return new IncisoCotizacionSN().listarIncisosPorCoberturaContratada(idToCotizacion, coberturas, claveContrato);
	}
	
	/**
	 * Devuelve la cantidad de incisos que han sido registrados para la cotizaci�n cuyo ID se recibe.
	 * @param idToCotizacion. El id de la cotizaci�n.
	 * @return int cantidad de incisos registrados para la cotizaci�n recibida.
	 */
	public int obtenerCantidadIncisosPorCotizacion(BigDecimal idToCotizacion){
		int totalIncisos = 0;
		try{
			totalIncisos = new IncisoCotizacionSN().obtenerCantidadIncisosPorCotizacion(idToCotizacion);
		}catch(Exception e){}
		return totalIncisos;
	}
	/**
	 * Method agregarInciso().
	 * @param cotizacionDTO
	 * @param idToDireccion
	 * @param datosInciso
	 * @param datos
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	public Map<String, String> agregarInciso(CotizacionDTO cotizacionDTO,BigDecimal idToDireccion,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso,String[] datos, String nombreUsuario) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		BigDecimal numeroInciso = IncisoCotizacionDN.getInstancia().obtenerNumeroIncisoSiguiente(cotizacionDTO.getIdToCotizacion());
		List<SeccionDTO> listaSeccionesTipoPoliza = SeccionDN.getInstancia().listarVigentesPorIDTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
		

		for(SeccionDTO seccionDTO : listaSeccionesTipoPoliza){
			List<CoberturaSeccionDTO> listaCoberturaSeccion = CoberturaSeccionDN.getInstancia().listarVigentesPorSeccion(seccionDTO.getIdToSeccion());
			if (listaCoberturaSeccion == null)
				listaCoberturaSeccion = new ArrayList<CoberturaSeccionDTO>();
			for(CoberturaSeccionDTO coberturaSeccionDTO : listaCoberturaSeccion){
				List<RiesgoCoberturaDTO> listaRiesgosCobertura = RiesgoCoberturaDN.getInstancia().listarRiesgoVigenteAsociado(coberturaSeccionDTO.getId().getIdtocobertura(), coberturaSeccionDTO.getId().getIdtoseccion());
				if(listaRiesgosCobertura == null)
					listaRiesgosCobertura = new ArrayList<RiesgoCoberturaDTO>();
				coberturaSeccionDTO.setRiesgos( listaRiesgosCobertura );
			}
			seccionDTO.setCoberturas(listaCoberturaSeccion);
		}
		cotizacionDTO.getTipoPolizaDTO().setSecciones(listaSeccionesTipoPoliza);
		Map<String, String> resultado = incisoCotizacionSN.agregarInciso(cotizacionDTO, idToDireccion, numeroInciso, datosInciso, datos,null,null);

		CalculoCotizacionDN calculoCotizacionDN = new CalculoCotizacionDN();
		calculoCotizacionDN.calcularRiesgosHidro(cotizacionDTO.getIdToCotizacion(), numeroInciso, nombreUsuario);
		//se actualizan documentos anexos de coberturas
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(cotizacionDTO.getIdToCotizacion());	

		return resultado;
	}
	/**
	 * Method obtenerAumentosPorCobertura().
	 * @param mapaAumentosPorCobertura
	 * @param idToCobertura
	 * @param idToRiesgo
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<AumentoVarioCoberturaDTO> obtenerAumentosPorCobertura(Map<String,List<AumentoVarioCoberturaDTO>> mapaAumentosPorCobertura,BigDecimal idToCobertura,BigDecimal idToRiesgo){
		String id = idToCobertura.toBigInteger().toString()+"_"+idToRiesgo.toBigInteger().toString();
		List<AumentoVarioCoberturaDTO> listaAumentosCobertura = mapaAumentosPorCobertura.get(id);
		if (listaAumentosCobertura == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaAumentosCobertura = riesgoCotSN.getAumentosCobertura(idToCobertura, idToRiesgo);
				if (listaAumentosCobertura == null)
					listaAumentosCobertura = new ArrayList<AumentoVarioCoberturaDTO>();
				mapaAumentosPorCobertura.put(id, listaAumentosCobertura);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaAumentosCobertura;
	}
	/**
	 * Method obtenerDescuentosPorCobertura().
	 * @param mapaDescuentosPorCobertura
	 * @param idToCobertura
	 * @param idToRiesgo
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<DescuentoVarioCoberturaDTO> obtenerDescuentosPorCobertura(Map<String,List<DescuentoVarioCoberturaDTO>> mapaDescuentosPorCobertura,BigDecimal idToCobertura,BigDecimal idToRiesgo){
		String id = idToCobertura.toBigInteger().toString()+"_"+idToRiesgo.toBigInteger().toString();
		List<DescuentoVarioCoberturaDTO> listaDescuentosCobertura = mapaDescuentosPorCobertura.get(id);
		if (listaDescuentosCobertura == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaDescuentosCobertura = riesgoCotSN.getDescuentosCobertura(idToCobertura, idToRiesgo);
				if (listaDescuentosCobertura == null)
					listaDescuentosCobertura = new ArrayList<DescuentoVarioCoberturaDTO>();
				mapaDescuentosPorCobertura.put(id, listaDescuentosCobertura);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaDescuentosCobertura;
	}
	/**
	 * Method obtenerRecargosPorCobertura().
	 * @param mapaRecargosPorCobertura
	 * @param idToCobertura
	 * @param idToRiesgo
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<RecargoVarioCoberturaDTO> obtenerRecargosPorCobertura(Map<String,List<RecargoVarioCoberturaDTO>> mapaRecargosPorCobertura,BigDecimal idToCobertura,BigDecimal idToRiesgo){
		String id = idToCobertura.toBigInteger().toString()+"_"+idToRiesgo.toBigInteger().toString();
		List<RecargoVarioCoberturaDTO> listaRecargosCobertura = mapaRecargosPorCobertura.get(id);
		if (listaRecargosCobertura == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaRecargosCobertura = riesgoCotSN.getRecargosCobertura(idToCobertura, idToRiesgo);
				if (listaRecargosCobertura == null)
					listaRecargosCobertura = new ArrayList<RecargoVarioCoberturaDTO>();
				mapaRecargosPorCobertura.put(id, listaRecargosCobertura);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaRecargosCobertura;
	}
	/**
	 * Method obtenerAumentosPorTipoPoliza()
	 * @param mapaAumentosPorTipoPoliza
	 * @param idToTipoPoliza
	 * @param idToCobertura
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<AumentoVarioTipoPolizaDTO> obtenerAumentosPorTipoPoliza(Map<String,List<AumentoVarioTipoPolizaDTO>> mapaAumentosPorTipoPoliza,
			BigDecimal idToTipoPoliza,BigDecimal idToCobertura){
		String id = idToTipoPoliza.toBigInteger().toString()+"_"+idToCobertura.toBigInteger().toString();
		List<AumentoVarioTipoPolizaDTO> listaAumentosTipoPoliza = mapaAumentosPorTipoPoliza.get(id);
		if (listaAumentosTipoPoliza == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaAumentosTipoPoliza = riesgoCotSN.getAumentosTipoPoliza(idToTipoPoliza, idToCobertura);
				if (listaAumentosTipoPoliza == null)
					listaAumentosTipoPoliza = new ArrayList<AumentoVarioTipoPolizaDTO>();
				mapaAumentosPorTipoPoliza.put(id, listaAumentosTipoPoliza);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaAumentosTipoPoliza;
	}
	/**
	 * Method obtenerDescuentosPorTipoPoliza().
	 * @param mapaDescuentosPorTipoPoliza
	 * @param idToTipoPoliza
	 * @param idToCobertura
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<DescuentoVarioTipoPolizaDTO> obtenerDescuentosPorTipoPoliza(Map<String,List<DescuentoVarioTipoPolizaDTO>> mapaDescuentosPorTipoPoliza,
			BigDecimal idToTipoPoliza,BigDecimal idToCobertura){
		String id = idToTipoPoliza.toBigInteger().toString()+"_"+idToCobertura.toBigInteger().toString();
		List<DescuentoVarioTipoPolizaDTO> listaDescuentosTipoPoliza = mapaDescuentosPorTipoPoliza.get(id);
		if (listaDescuentosTipoPoliza == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaDescuentosTipoPoliza = riesgoCotSN.getDescuentosTipoPoliza(idToTipoPoliza, idToCobertura);
				if (listaDescuentosTipoPoliza == null)
					listaDescuentosTipoPoliza = new ArrayList<DescuentoVarioTipoPolizaDTO>();
				mapaDescuentosPorTipoPoliza.put(id, listaDescuentosTipoPoliza);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaDescuentosTipoPoliza;
	}
	/**
	 * Method obtenerRecargosPorTipoPoliza()
	 * @param mapaRecargosPorTipoPoliza
	 * @param idToTipoPoliza
	 * @param idToCobertura
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<RecargoVarioTipoPolizaDTO> obtenerRecargosPorTipoPoliza(Map<String,List<RecargoVarioTipoPolizaDTO>> mapaRecargosPorTipoPoliza,
			BigDecimal idToTipoPoliza,BigDecimal idToCobertura){
		String id = idToTipoPoliza.toBigInteger().toString()+"_"+idToCobertura.toBigInteger().toString();
		List<RecargoVarioTipoPolizaDTO> listaRecargosTipoPoliza = mapaRecargosPorTipoPoliza.get(id);
		if (listaRecargosTipoPoliza == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaRecargosTipoPoliza = riesgoCotSN.getRecargosTipoPoliza(idToTipoPoliza, idToCobertura);
				if (listaRecargosTipoPoliza == null)
					listaRecargosTipoPoliza = new ArrayList<RecargoVarioTipoPolizaDTO>();
				mapaRecargosPorTipoPoliza.put(id, listaRecargosTipoPoliza);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaRecargosTipoPoliza;
	}
	/**
	 * Method obtenerAumentosPorProducto()
	 * @param mapaAumentosPorProducto
	 * @param idToProducto
	 * @param idToTipoPoliza
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<AumentoVarioProductoDTO> obtenerAumentosPorProducto(Map<String,List<AumentoVarioProductoDTO>> mapaAumentosPorProducto,
			BigDecimal idToProducto,BigDecimal idToTipoPoliza){
		String id = idToProducto.toBigInteger().toString()+"_"+idToTipoPoliza.toBigInteger().toString();
		List<AumentoVarioProductoDTO> listaAumentosProducto = mapaAumentosPorProducto.get(id);
		if (listaAumentosProducto == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaAumentosProducto = riesgoCotSN.getAumentosProducto(idToProducto, idToTipoPoliza);
				if (listaAumentosProducto == null)
					listaAumentosProducto = new ArrayList<AumentoVarioProductoDTO>();
				mapaAumentosPorProducto.put(id, listaAumentosProducto);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaAumentosProducto;
	}
	/**
	 * Method obtenerDescuentosPorProducto()
	 * @param mapaDescuentosPorProducto
	 * @param idToProducto
	 * @param idToTipoPoliza
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<DescuentoVarioProductoDTO> obtenerDescuentosPorProducto(Map<String,List<DescuentoVarioProductoDTO>> mapaDescuentosPorProducto,
			BigDecimal idToProducto,BigDecimal idToTipoPoliza){
		String id = idToProducto.toBigInteger().toString()+"_"+idToTipoPoliza.toBigInteger().toString();
		List<DescuentoVarioProductoDTO> listaDescuentosProducto = mapaDescuentosPorProducto.get(id);
		if (listaDescuentosProducto == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaDescuentosProducto = riesgoCotSN.getDescuentosProducto(idToProducto, idToTipoPoliza);
				if (listaDescuentosProducto == null)
					listaDescuentosProducto = new ArrayList<DescuentoVarioProductoDTO>();
				mapaDescuentosPorProducto.put(id, listaDescuentosProducto);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaDescuentosProducto;
	}
	/**
	 * Method obtenerRecargosPorProducto()
	 * @param mapaRecargosPorProducto
	 * @param idToProducto
	 * @param idToTipoPoliza
	 * @return
	 */
	
	@SuppressWarnings("unused")
	private List<RecargoVarioProductoDTO> obtenerRecargosPorProducto(Map<String,List<RecargoVarioProductoDTO>> mapaRecargosPorProducto,
			BigDecimal idToProducto,BigDecimal idToTipoPoliza){
		String id = idToProducto.toBigInteger().toString()+"_"+idToTipoPoliza.toBigInteger().toString();
		List<RecargoVarioProductoDTO> listaRecargosProducto = mapaRecargosPorProducto.get(id);
		if (listaRecargosProducto == null){
			RiesgoCotizacionSN riesgoCotSN;
			try {
				riesgoCotSN = new RiesgoCotizacionSN();
				listaRecargosProducto = riesgoCotSN.getRecargosProducto(idToProducto, idToTipoPoliza);
				if (listaRecargosProducto == null)
					listaRecargosProducto = new ArrayList<RecargoVarioProductoDTO>();
				mapaRecargosPorProducto.put(id, listaRecargosProducto);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return listaRecargosProducto;
	}
	/**
	 * Method borrar()
	 * @param incisoCotizacionDTO
	 * @return
	 * @throws SystemException
	 */
	public Map<String, String> borrarInciso(IncisoCotizacionDTO incisoCotizacionDTO) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		Map<String, String> mensaje = incisoCotizacionSN.borrarInciso(incisoCotizacionDTO);
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(incisoCotizacionDTO.getId().getIdToCotizacion());
		return mensaje;
	}
	/**
	 * Cuando un inciso no requiere facultativo, 
	 * se le quita(separa) ese inciso a la cotizacion para que pase la validaci�n.
	 * esa funcionalidad genera una nueva cotizacion (copia) 
	 * y le agrega el inciso que se separ�.
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param numeroCopias
	 * @return
	 * @throws SystemException
	 */
	public Map<String, String> copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Integer numeroCopias) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		Map<String, String> mensaje = new HashMap<String, String>();
		int numeroCopiasValidas = 0;
		IncisoCotizacionId incisoIdOriginal = new IncisoCotizacionId();
		incisoIdOriginal.setIdToCotizacion(idToCotizacion);
		incisoIdOriginal.setNumeroInciso(numeroInciso);
		IncisoCotizacionDTO incisoOriginal = incisoCotizacionSN.getPorId(incisoIdOriginal);
		CotizacionDTO cotizacionDTO = incisoOriginal.getCotizacionDTO();
		DireccionDTO direccionOriginal = incisoOriginal.getDireccionDTO();
		
		List<SubIncisoCotizacionDTO> listaSubIncisoCotizacion = SubIncisoCotizacionDN.getInstancia().listarSubIncisosPorCotizacion(idToCotizacion);
		Map<String,List<SubIncisoCotizacionDTO>> mapaSubIncisosCotizacion = new HashMap<String,List<SubIncisoCotizacionDTO>>();
		
		//Poblar SeccionCotizacionList
		List<SeccionCotizacionDTO> listaSeccionCotizacion = SeccionCotizacionDN.getInstancia().listarPorCotizacionNumeroInciso(idToCotizacion, numeroInciso);
		if (listaSeccionCotizacion == null)
			listaSeccionCotizacion = new ArrayList<SeccionCotizacionDTO>();
		for(SeccionCotizacionDTO seccionCot : listaSeccionCotizacion){
			List<SubIncisoCotizacionDTO> listaSubIncisoCot = obtenerSubIncisosPorSeccion(mapaSubIncisosCotizacion, listaSubIncisoCotizacion,numeroInciso,seccionCot.getId().getIdToSeccion());
			//Se recuperan los registros de coberturaCotizacion relacionados con la seccionCotizacion
			List<CoberturaCotizacionDTO> listaCoberturaCotizacion = CoberturaCotizacionDN.getInstancia().listarPorSeccionCotizacionIdSinContratar(seccionCot.getId());
			for(CoberturaCotizacionDTO coberturaCot : listaCoberturaCotizacion){
				List<DetallePrimaCoberturaCotizacionDTO> listaDetallePrimacoberturaCot = DetallePrimaCoberturaCotizacionDN.getInstancia().findByCoberturaCotizacion(coberturaCot.getId());
				if (listaDetallePrimacoberturaCot == null)	listaDetallePrimacoberturaCot = new ArrayList<DetallePrimaCoberturaCotizacionDTO>();
				coberturaCot.setListaDetallePrimacoberturaCotizacion(listaDetallePrimacoberturaCot);
				
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCobertura(coberturaCot.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToCotizacion(idToCotizacion);
				riesgoCotizacionId.setIdToSeccion(coberturaCot.getId().getIdToSeccion());
				riesgoCotizacionId.setNumeroInciso(coberturaCot.getId().getNumeroInciso());
				List<RiesgoCotizacionDTO> listaRiesgoCotizacion = RiesgoCotizacionDN.getInstancia().listarPorIdFiltrado(riesgoCotizacionId);
				if (listaRiesgoCotizacion == null)	listaRiesgoCotizacion = new ArrayList<RiesgoCotizacionDTO>();
				
				for(RiesgoCotizacionDTO riesgoCot : listaRiesgoCotizacion){
					List<AumentoRiesgoCotizacionDTO> listaAumentosRiesgoCot = AumentoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(riesgoCot.getId());
					if (listaAumentosRiesgoCot == null)		listaAumentosRiesgoCot = new ArrayList<AumentoRiesgoCotizacionDTO>();
					riesgoCot.setAumentoRiesgoCotizacionDTOs(listaAumentosRiesgoCot);
					
					List<DescuentoRiesgoCotizacionDTO> listaDescuentosRiesgoCot = DescuentoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(riesgoCot.getId());
					if (listaAumentosRiesgoCot == null)		listaAumentosRiesgoCot = new ArrayList<AumentoRiesgoCotizacionDTO>();
					riesgoCot.setDescuentoRiesgoCotizacionDTOs(listaDescuentosRiesgoCot);
					
					List<RecargoRiesgoCotizacionDTO> listaRecargosRiesgoCot = RecargoRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(riesgoCot.getId());
					if (listaRecargosRiesgoCot == null)		listaRecargosRiesgoCot = new ArrayList<RecargoRiesgoCotizacionDTO>();
					riesgoCot.setRecargoRiesgoCotizacionDTOs(listaRecargosRiesgoCot);
					
					List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgoCot = DetallePrimaRiesgoCotizacionDN.getInstancia().findByRiesgoCotizacion(riesgoCot.getId());
					if (listaDetallePrimaRiesgoCot == null)	listaDetallePrimaRiesgoCot = new ArrayList<DetallePrimaRiesgoCotizacionDTO>();
					riesgoCot.setDetallePrimaRiesgoCotizacionList(listaDetallePrimaRiesgoCot);
				}
				
				coberturaCot.setRiesgoCotizacionLista(listaRiesgoCotizacion);
				
				
			}
			
			seccionCot.setCoberturaCotizacionLista(listaCoberturaCotizacion);
			seccionCot.setSubIncisoCotizacionLista(listaSubIncisoCot);
		}
		incisoOriginal.setSeccionCotizacionList(listaSeccionCotizacion);
		
		DatoIncisoCotizacionId idDatoInciso = new DatoIncisoCotizacionId();
		idDatoInciso.setIdToCotizacion(idToCotizacion);
		idDatoInciso.setNumeroInciso(numeroInciso);
		List<DatoIncisoCotizacionDTO> listaDatosInciso = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(idDatoInciso);
		BigDecimal numeroIncisoNuevo = obtenerNumeroIncisoSiguiente(idToCotizacion);
		if (incisoOriginal != null){
			for(int i=0; i < numeroCopias.intValue(); i++){
				DireccionDTO direccionNueva = new DireccionDTO();
				direccionNueva.setCodigoPostal(direccionOriginal.getCodigoPostal());
				direccionNueva.setEntreCalles(direccionOriginal.getEntreCalles());
				direccionNueva.setIdEstado(direccionOriginal.getIdEstado());
				direccionNueva.setIdMunicipio(direccionOriginal.getIdMunicipio());
				direccionNueva.setIdToDireccion(null);
				direccionNueva.setNombreCalle(direccionOriginal.getNombreCalle());
				direccionNueva.setNombreColonia(direccionOriginal.getNombreColonia());
				direccionNueva.setNombreDelegacion(direccionOriginal.getNombreDelegacion());
				direccionNueva.setNumeroExterior(direccionOriginal.getNumeroExterior());
				direccionNueva.setNumeroInterior(direccionOriginal.getNumeroInterior());
				direccionNueva = DireccionDN.getInstancia().agregar(direccionNueva);
				
				IncisoCotizacionDTO incisoNuevo = new IncisoCotizacionDTO();
				incisoNuevo.setClaveAutInspeccion(incisoOriginal.getClaveAutInspeccion());
				incisoNuevo.setClaveEstatusInspeccion(incisoOriginal.getClaveEstatusInspeccion());
				incisoNuevo.setClaveMensajeInspeccion(incisoOriginal.getClaveMensajeInspeccion());
				incisoNuevo.setClaveTipoOrigenInspeccion(incisoOriginal.getClaveTipoOrigenInspeccion());
				incisoNuevo.setCodigoUsuarioEstInspeccion(incisoOriginal.getCodigoUsuarioEstInspeccion());
				incisoNuevo.setCotizacionDTO(cotizacionDTO);
//				incisoNuevo.setDireccion(direccionNueva);
//				incisoNuevo.setIdToDireccionInciso(direccionNueva.getIdToDireccion());
				incisoNuevo.setDescripcionGiroAsegurado(incisoOriginal.getDescripcionGiroAsegurado());
				incisoNuevo.setValorPrimaNeta(incisoOriginal.getValorPrimaNeta());
				incisoNuevo.setId(new IncisoCotizacionId());
				incisoNuevo.getId().setIdToCotizacion(idToCotizacion);
				
				incisoNuevo.getId().setNumeroInciso(numeroIncisoNuevo);
				
				try{
					if (incisoCotizacionSN.copiarInciso(incisoNuevo,incisoOriginal,cotizacionDTO,direccionNueva,listaDatosInciso)){
						numeroCopiasValidas++;
						numeroIncisoNuevo = numeroIncisoNuevo.add(BigDecimal.ONE);
					}
					else{
						mensaje.put("tipoMensaje", "10");//Error
						mensaje.put("mensaje", "Error al realizar la copia n&uacute;mero "+(i+1)+". Se realizaron "+i+" copias");
						break;
					}
				}
				catch(Exception e){
					mensaje.put("tipoMensaje", "10");//Error
					mensaje.put("mensaje", "Error al realizar la copia n&uacute;mero "+(i+1)+". Se realizaron "+i+" copias");
					break;
				}
			}
			if (numeroCopiasValidas == numeroCopias){
				mensaje.put("tipoMensaje", "30");//Exito
				mensaje.put("mensaje", "Se realizaron "+numeroCopiasValidas+" copias del inciso exitosamente.");
			}
		}
		else{
			mensaje.put("tipoMensaje", "10");//Error
			mensaje.put("mensaje", "Error al intentar copiar el inciso n&uacute;mero "+(numeroInciso.toBigInteger().toString()));
		}
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		return mensaje;
	}
	/**
	 * Method separarInciso()
	 * @param cotizacionDTO
	 * @param numeroInciso
	 * @return
	 * @throws SystemException
	 */
	public Map<String, String> separarInciso(CotizacionDTO cotizacionDTO,
			BigDecimal numeroInciso) throws SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		Map<String, String> mensaje = incisoCotizacionSN.separarInciso(cotizacionDTO, numeroInciso);
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
		return mensaje;
	}
	/**
	 * Method obtenerSubIncisosPorSeccion()
	 * @param mapaSubIncisosCotizacion
	 * @param listaSubIncisoCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @return
	 */
	private List<SubIncisoCotizacionDTO> obtenerSubIncisosPorSeccion(Map<String,List<SubIncisoCotizacionDTO>> mapaSubIncisosCotizacion,
			List<SubIncisoCotizacionDTO> listaSubIncisoCotizacion,BigDecimal numeroInciso,BigDecimal idToSeccion){
		String key = ""+numeroInciso.intValue()+"_"+idToSeccion.intValue();
		List<SubIncisoCotizacionDTO> listaSubIncisoCotizacionPorSeccion = mapaSubIncisosCotizacion.get(key);
		if (listaSubIncisoCotizacionPorSeccion == null){
			listaSubIncisoCotizacionPorSeccion = new ArrayList<SubIncisoCotizacionDTO>();
			for(SubIncisoCotizacionDTO subIncisoCot : listaSubIncisoCotizacion){
				if(subIncisoCot.getId().getIdToSeccion().doubleValue() == idToSeccion.doubleValue() && subIncisoCot.getId().getNumeroInciso().doubleValue() == numeroInciso.doubleValue()){
					listaSubIncisoCotizacionPorSeccion.add(subIncisoCot);
				}
			}
		}
		mapaSubIncisosCotizacion.put(key, listaSubIncisoCotizacionPorSeccion);
		return listaSubIncisoCotizacionPorSeccion;
	}
	
	/**
	 * Method obtenerIncisosPorIdCotizacionDireccion()
	 * @param idToCotizacion
	 * @param idToDireccion
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> obtenerIncisosPorIdCotizacionDireccion(BigDecimal idToCotizacion,BigDecimal idToDireccion) throws SystemException {
		return new IncisoCotizacionSN().obtenerIncisosPorIdCotizacionDireccion(idToCotizacion, idToDireccion);
	}

	public BigDecimal copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToDireccion) throws SystemException {
		BigDecimal inciso = new IncisoCotizacionSN().copiarInciso(idToCotizacion, numeroInciso, idToDireccion);
		DocAnexoCotDN.getInstancia().actualizaAnexosDeCoberturasContratadas(idToCotizacion);
		return inciso;
	}
	
	/**
	 * Lista los incisos de una cotizacion que tengan al menos una cobertura contratada con el subramo especificado. 
	 * @param idToCotizacion, idTcSubRamo
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */
	public List<IncisoCotizacionDTO> listarIncisosPorSubRamo(BigDecimal idToCotizacion, BigDecimal idTcSubRamo)throws SystemException {
		return new IncisoCotizacionSN().listarIncisosPorSubRamo(idToCotizacion, idTcSubRamo);
	}
	
	/**
	 * Lista los incisos dados de baja en una cotizacion de endoso. 
	 * @param idToCotizacionEndoso
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */	
	public List<IncisoCotizacionDTO> listarBajaIncisos(BigDecimal idToCotizacionEndosoActual, BigDecimal idToCotizacionEndosoAnterior) 
			throws ExcepcionDeAccesoADatos,SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();		
		return incisoCotizacionSN.listarBajaIncisos(idToCotizacionEndosoActual, idToCotizacionEndosoAnterior);
	}
	
	
	/**
	 * Lista los incisos contratados en la cotizaci�n de un endoso, as� como los dados de baja en la misma.
	 * @param idToCotizacionEndosoActual
	 * @param idToCotizacionEndosoAnterior
	 * @return List<IncisoCotizacionDTO> lista de entidades
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarIncisosEndoso(BigDecimal idToCotizacionEndosoActual, BigDecimal idToCotizacionEndosoAnterior) 
			throws ExcepcionDeAccesoADatos,SystemException {
		IncisoCotizacionSN incisoCotizacionSN = new IncisoCotizacionSN();
		List<IncisoCotizacionDTO> listaIncisosEndoso, listaBajaIncisos;
		listaIncisosEndoso = incisoCotizacionSN.getPorPropiedad("id.idToCotizacion", idToCotizacionEndosoActual);
		listaBajaIncisos = this.listarBajaIncisos(idToCotizacionEndosoActual, idToCotizacionEndosoAnterior);
		listaIncisosEndoso.addAll(listaBajaIncisos);
		return listaIncisosEndoso;
	}

	public void agregar (IncisoCotizacionDTO incisoCotizacionDTO) throws SystemException {
		new IncisoCotizacionSN().guardar(incisoCotizacionDTO);
	}

}
