package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class DocAnexoCotDN {
	public static final DocAnexoCotDN INSTANCIA = new DocAnexoCotDN();

	public static DocAnexoCotDN getInstancia (){
		return DocAnexoCotDN.INSTANCIA;
	}
	
	public DocAnexoCotDTO agregar(DocAnexoCotDTO docAnexoCotDTO)throws ExcepcionDeAccesoADatos, SystemException{
		DocAnexoCotSN docAnexoCotSN = new DocAnexoCotSN();
		return docAnexoCotSN.agregar(docAnexoCotDTO);
	}
	
	public DocAnexoCotDTO modificar(DocAnexoCotDTO docAnexoCotDTO)throws ExcepcionDeAccesoADatos, SystemException{
		DocAnexoCotSN docAnexoCotSN = new DocAnexoCotSN();
		return docAnexoCotSN.modificar(docAnexoCotDTO);
	}
	
	public DocAnexoCotDTO getPorId(DocAnexoCotDTO docAnexoCotDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new DocAnexoCotSN().getPorId(docAnexoCotDTO.getId());
	}
	
	public DocAnexoCotDTO getPorId(DocAnexoCotId docAnexoCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DocAnexoCotSN().getPorId(docAnexoCotId);
	}

	public List<DocAnexoCotDTO> listarFiltrado(DocAnexoCotDTO docAnexoCotDTO) throws ExcepcionDeAccesoADatos,SystemException {
		DocAnexoCotSN docAnexoCotSN = new DocAnexoCotSN();
		return docAnexoCotSN.listarfiltrado(docAnexoCotDTO);
	}
	
	public List<DocAnexoCotDTO> listarDocumentosAnexosPorCotizacion(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,SystemException {
		return new DocAnexoCotSN().buscarPorPropiedad("id.idToCotizacion", idToCotizacion);
	}
	
	public void actualizaAnexosDeCoberturasContratadas(BigDecimal idToCotizacion) throws SystemException{
		//Obtener todas las distintas coberturas contratadas
		List<BigDecimal> coberturasContratadas = CoberturaCotizacionDN.getInstancia().listarCoberturasDistintas(idToCotizacion, true);
		//Limpiar todos los anexos a nivel cobertura
		List<DocAnexoCotDTO>  documentosAnexos= this.listarDocumentosAnexosPorCotizacion(idToCotizacion);
		for(DocAnexoCotDTO documentoAnexo: documentosAnexos){
			if(documentoAnexo.getIdToCobertura().intValue() != 0){
				if (!documentoAnexo.getClaveSeleccion().equals(Sistema.DOCUMENTO_ANEXO_COTIZACION_SIN_SELECCIONAR)
						&& documentoAnexo.getClaveObligatoriedad().equals(Sistema.DOCUMENTO_ANEXO_OBLIGATORIO))
					this.inicializaAnexoDeCobertura(documentoAnexo);
			}
		}
		//Seleccionar los anexos de las distintas coberturas contratadas 		
		for(BigDecimal idToCobertura: coberturasContratadas){
			for(DocAnexoCotDTO documentoAnexo: documentosAnexos){
				if(documentoAnexo.getIdToCobertura().intValue() ==
					idToCobertura.intValue()){
					if (!documentoAnexo.getClaveSeleccion().equals(Sistema.DOCUMENTO_ANEXO_COTIZACION_SELECCIONADO)
							&& documentoAnexo.getClaveObligatoriedad().equals(Sistema.DOCUMENTO_ANEXO_OBLIGATORIO))
						this.seleccionaAnexoDeCobertura(documentoAnexo);
				}
			}
		}
	}
	
	public void inicializaAnexoDeCobertura(DocAnexoCotDTO documentoAnexo) throws ExcepcionDeAccesoADatos, SystemException{
		documentoAnexo.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SIN_SELECCIONAR);
		this.modificar(documentoAnexo);
	}
	public void seleccionaAnexoDeCobertura(DocAnexoCotDTO documentoAnexo) throws ExcepcionDeAccesoADatos, SystemException{
		documentoAnexo.setClaveSeleccion(Sistema.DOCUMENTO_ANEXO_COTIZACION_SELECCIONADO);
		this.modificar(documentoAnexo);
	}	
}
