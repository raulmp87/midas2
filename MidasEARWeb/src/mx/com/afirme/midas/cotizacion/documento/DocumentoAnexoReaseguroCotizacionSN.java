package mx.com.afirme.midas.cotizacion.documento;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoAnexoReaseguroCotizacionSN {

	private DocumentoAnexoReaseguroCotizacionFacadeRemote beanRemoto;
	
	public DocumentoAnexoReaseguroCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(DocumentoAnexoReaseguroCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> findAll() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByProperty(String propertyName,
			final Object value) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public DocumentoAnexoReaseguroCotizacionDTO findById(DocumentoAnexoReaseguroCotizacionId id) 
		throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DocumentoAnexoReaseguroCotizacionDTO update(
			DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacion)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(documentoAnexoReaseguroCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void delete(DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacion)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(documentoAnexoReaseguroCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void save(
			DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacion)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(documentoAnexoReaseguroCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
}
