package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;


public class DocPrevencionOperIlicitasDN {
	private static final DocPrevencionOperIlicitasDN INSTANCIA = new DocPrevencionOperIlicitasDN();

	private DocPrevencionOperIlicitasDN() {
	}
	
	public static DocPrevencionOperIlicitasDN getInstancia() {
		return DocPrevencionOperIlicitasDN.INSTANCIA;
	}
	
	public List<DocPrevencionOperIlicitasDTO> buscarPorPropiedad(String propiedad, Object valor) throws SystemException {
		return new DocPrevencionOperIlicitasSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<DocPrevencionOperIlicitasDTO> buscarTodos() throws SystemException {
		return new DocPrevencionOperIlicitasSN().buscarTodos();
	}
	
	public DocPrevencionOperIlicitasDTO buscarPorId(BigDecimal idToControlArchivo) throws SystemException {
		return new DocPrevencionOperIlicitasSN().buscarPorId(idToControlArchivo);
	}
	
	public void guardar(DocPrevencionOperIlicitasDTO docPrevencionOperIlicitasDTO) throws SystemException{
		new DocPrevencionOperIlicitasSN().guardar(docPrevencionOperIlicitasDTO);
	}
	
	public DocPrevencionOperIlicitasDTO actualizar(DocPrevencionOperIlicitasDTO docPrevencionOperIlicitasDTO) throws SystemException {
		return new DocPrevencionOperIlicitasSN().actualizar(docPrevencionOperIlicitasDTO);
	}
}
