package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author Fernando Alonzo
 * @since 01 de Septiembre del 2009
 * 
 */
public class DocumentoDigitalCotizacionSN {
	private DocumentoDigitalCotizacionFacadeRemote beanRemoto;

	public DocumentoDigitalCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoDigitalCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO) {
		try {
			beanRemoto.save(documentoDigitalCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DocumentoDigitalCotizacionDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DocumentoDigitalCotizacionDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DocumentoDigitalCotizacionDTO> listarDocumentosCotizacion(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.findByProperty("cotizacionDTO.idToCotizacion", idToCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO) {
		try {
			beanRemoto.delete(documentoDigitalCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
