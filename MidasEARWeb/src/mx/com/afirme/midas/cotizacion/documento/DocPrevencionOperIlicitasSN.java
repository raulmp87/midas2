package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocPrevencionOperIlicitasSN {
	private DocPrevencionOperIlicitasFacadeRemote beanRemoto;

	public DocPrevencionOperIlicitasSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocPrevencionOperIlicitasFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<DocPrevencionOperIlicitasDTO> buscarPorPropiedad(String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<DocPrevencionOperIlicitasDTO> buscarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public DocPrevencionOperIlicitasDTO buscarPorId(BigDecimal idToControlArchivo) {
		try {
			return beanRemoto.findById(idToControlArchivo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void guardar(DocPrevencionOperIlicitasDTO docPrevencionOperIlicitasDTO) {
		try {
			beanRemoto.save(docPrevencionOperIlicitasDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public DocPrevencionOperIlicitasDTO actualizar(DocPrevencionOperIlicitasDTO docPrevencionOperIlicitasDTO) {
		try {
			return beanRemoto.update(docPrevencionOperIlicitasDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
