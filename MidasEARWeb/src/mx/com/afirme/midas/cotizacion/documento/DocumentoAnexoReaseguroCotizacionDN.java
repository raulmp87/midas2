package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;


public class DocumentoAnexoReaseguroCotizacionDN {

	private static final DocumentoAnexoReaseguroCotizacionDN INSTANCIA = new DocumentoAnexoReaseguroCotizacionDN();

	public static DocumentoAnexoReaseguroCotizacionDN getInstancia() {
		return DocumentoAnexoReaseguroCotizacionDN.INSTANCIA;
	}
	
	
	public String obtieneContenidoAnexosReaseguroCotizacion(BigDecimal idToCotizacion) 
		throws ExcepcionDeAccesoADatos,	SystemException {
		
		List<DocumentoAnexoReaseguroCotizacionDTO> listaDocumentos = 
			obtieneAnexosReasCotPorIdCotizacion(idToCotizacion);

		ControlArchivoDTO controlArchivoDTO; 

		MidasJsonBase json = new MidasJsonBase();
		if(listaDocumentos != null && listaDocumentos.size() > 0) {
			for(DocumentoAnexoReaseguroCotizacionDTO documento : listaDocumentos) {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(documento.getId().getIdToControlArchivo()); 
				if(controlArchivoDTO != null){
					MidasJsonRow row = new MidasJsonRow();
					row.setId(controlArchivoDTO.getIdToControlArchivo().toString());
					row.setDatos(
							documento.getId().getIdToCotizacion().toString(),
							controlArchivoDTO.getNombreArchivoOriginal(),
							documento.getNumeroSecuencia().toString(),
							(!documento.getDescripcionDocumentoAnexo().equals("default")
									?documento.getDescripcionDocumentoAnexo():"")
					);
					json.addRow(row);
				}
			}
		}
		
		return json.toString();
		
	}
	
	public DocumentoAnexoReaseguroCotizacionDTO obtienePorId (DocumentoAnexoReaseguroCotizacionId id) 
		throws ExcepcionDeAccesoADatos,	SystemException {
			return new DocumentoAnexoReaseguroCotizacionSN().findById(id);
	}
	
	public void agregar (DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacion) 
	throws ExcepcionDeAccesoADatos,	SystemException {
		new DocumentoAnexoReaseguroCotizacionSN().save(documentoAnexoReaseguroCotizacion);
	}
	
	public DocumentoAnexoReaseguroCotizacionDTO actualizar (DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacion) 
		throws ExcepcionDeAccesoADatos,	SystemException {
		return new DocumentoAnexoReaseguroCotizacionSN().update(documentoAnexoReaseguroCotizacion);
	}
	
	public void borrar (DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacion) 
		throws ExcepcionDeAccesoADatos,	SystemException {
			new DocumentoAnexoReaseguroCotizacionSN().delete(documentoAnexoReaseguroCotizacion);
	}
	
	public List<DocumentoAnexoReaseguroCotizacionDTO> obtieneAnexosReasCotPorIdCotizacion(BigDecimal idToCotizacion) 
		throws ExcepcionDeAccesoADatos,	SystemException {
			return new DocumentoAnexoReaseguroCotizacionSN()
				.findByProperty("id.idToCotizacion", idToCotizacion);
	}
		
	
}
