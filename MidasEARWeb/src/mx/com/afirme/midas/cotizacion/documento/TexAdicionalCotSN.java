package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * 
 * @author Eric Villafana
 * @since 10 Septiembre de 2009
 */
public class TexAdicionalCotSN {
	private TexAdicionalCotFacadeRemote beanRemoto;

	public TexAdicionalCotSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TexAdicionalCotFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public TexAdicionalCotDTO getPorId(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(texAdicionalCotDTO.getIdToTexAdicionalCot());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TexAdicionalCotDTO agregar(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(texAdicionalCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(texAdicionalCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public TexAdicionalCotDTO modificar(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(texAdicionalCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TexAdicionalCotDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	
	/**
	 * Lista las entidades TexAdicionalCotDTO relacionadas 
	 * @param TexAdicionalCotDTO texAdicionalCotDTO
	 * @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public List<TexAdicionalCotDTO> listarfiltrado(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(texAdicionalCotDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
	
	/**
	 * Lista las entidades TexAdicionalCotDTO relacionadas 
	 * @param TexAdicionalCotDTO texAdicionalCotDTO
	 * @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public List<TexAdicionalCotDTO> listarfiltradoPorAutorizar(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltradoPorAutorizar(texAdicionalCotDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Lista las entidades TexAdicionalCotDTO relacionadas 
	 * @param TexAdicionalCotDTO texAdicionalCotDTO
	 * @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public boolean cambiarEstatusTextosAdicionales(String ids, TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.cambiarEstatusTextosAdicionales(ids,texAdicionalCotDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TexAdicionalCotDTO> buscarPorClaveAutorizacionPorCotizacion(BigDecimal idToCotizacion, Short claveAutorizacion){
		try {
			return beanRemoto.buscarPorClaveAutorizacionPorCotizacion(idToCotizacion,claveAutorizacion);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}