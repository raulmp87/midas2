package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class TexAdicionalCotDN {
	public static final TexAdicionalCotDN INSTANCIA = new TexAdicionalCotDN();

	public static TexAdicionalCotDN getInstancia (){
		return TexAdicionalCotDN.INSTANCIA;
	}
	
	public TexAdicionalCotDTO agregar(TexAdicionalCotDTO texAdicionalCotDTO)throws ExcepcionDeAccesoADatos, SystemException{
		TexAdicionalCotSN texAdicionalCotSN = new TexAdicionalCotSN();
		return texAdicionalCotSN.agregar(texAdicionalCotDTO);
	}
	
	public TexAdicionalCotDTO modificar(TexAdicionalCotDTO texAdicionalCotDTO)throws ExcepcionDeAccesoADatos, SystemException{
		TexAdicionalCotSN texAdicionalCotSN = new TexAdicionalCotSN();
		return texAdicionalCotSN.modificar(texAdicionalCotDTO);
	}
	
	public TexAdicionalCotDTO getPorId(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TexAdicionalCotSN().getPorId(texAdicionalCotDTO);
	}
	
	public void borrar(TexAdicionalCotDTO texAdicionalCotDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TexAdicionalCotSN texAdicionalCotSN = new TexAdicionalCotSN();
		texAdicionalCotSN.borrar(texAdicionalCotDTO);
	}

	public List<TexAdicionalCotDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos,SystemException {
		return new TexAdicionalCotSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<TexAdicionalCotDTO> listarFiltrado(TexAdicionalCotDTO texAdicionalCotDTO) 
			throws ExcepcionDeAccesoADatos,SystemException {
		TexAdicionalCotSN texAdicionalCotSN = new TexAdicionalCotSN();
		return texAdicionalCotSN.listarfiltrado(texAdicionalCotDTO);
	}
	
	public List<TexAdicionalCotDTO> listarFiltradoPorAutorizar(TexAdicionalCotDTO texAdicionalCotDTO) 
		throws ExcepcionDeAccesoADatos,SystemException {
		TexAdicionalCotSN texAdicionalCotSN = new TexAdicionalCotSN();
	return texAdicionalCotSN.listarfiltradoPorAutorizar(texAdicionalCotDTO);
	}
	
	public boolean cambiarEstatusTextosAdicionales(String ids, TexAdicionalCotDTO texAdicionalCotDTO) 
			throws ExcepcionDeAccesoADatos,SystemException {
		TexAdicionalCotSN texAdicionalCotSN = new TexAdicionalCotSN();
		return texAdicionalCotSN.cambiarEstatusTextosAdicionales(ids,texAdicionalCotDTO);
	}
	
	public List<TexAdicionalCotDTO> buscarPorClaveAutorizacionPorCotizacion(BigDecimal idToCotizacion, Short claveAutorizacion)throws ExcepcionDeAccesoADatos,SystemException {
		return new TexAdicionalCotSN().buscarPorClaveAutorizacionPorCotizacion(idToCotizacion,claveAutorizacion);
	}
}
