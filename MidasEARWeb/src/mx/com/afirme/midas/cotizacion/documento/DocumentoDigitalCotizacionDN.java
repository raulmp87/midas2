package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;

public class DocumentoDigitalCotizacionDN {
	private static final DocumentoDigitalCotizacionDN INSTANCIA = new DocumentoDigitalCotizacionDN();

	public static DocumentoDigitalCotizacionDN getInstancia() {
		return DocumentoDigitalCotizacionDN.INSTANCIA;
	}

	public DocumentoDigitalCotizacionDTO getPorId(BigDecimal id) throws SystemException {
		DocumentoDigitalCotizacionSN cotizacionSN = new DocumentoDigitalCotizacionSN();
		return cotizacionSN.getPorId(id);
	}

	public List<DocumentoDigitalCotizacionDTO> listarTodos() throws SystemException {
		DocumentoDigitalCotizacionSN cotizacionSN = new DocumentoDigitalCotizacionSN();
		return cotizacionSN.listarTodos();
	}

	public void agregar(DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO) throws SystemException {
		DocumentoDigitalCotizacionSN cotizacionSN = new DocumentoDigitalCotizacionSN();
		cotizacionSN.agregar(documentoDigitalCotizacionDTO);
	}

	public List<DocumentoDigitalCotizacionDTO> listarDocumentosCotizacionPorCotizacion(
			BigDecimal idToCotizacion) throws SystemException {
		DocumentoDigitalCotizacionSN cotizacionSN = new DocumentoDigitalCotizacionSN();
		List<DocumentoDigitalCotizacionDTO> lista = cotizacionSN.listarDocumentosCotizacion(idToCotizacion);
		for (DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO : lista) {
			if (documentoDigitalCotizacionDTO.getControlArchivo() == null)
				documentoDigitalCotizacionDTO.setControlArchivo(ControlArchivoDN.getInstancia().getPorId(documentoDigitalCotizacionDTO.getIdControlArchivo()));
		}
		
		return lista;
	}

	public void borrar(DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO) throws SystemException {
		DocumentoDigitalCotizacionSN cotizacionSN = new DocumentoDigitalCotizacionSN();
		cotizacionSN.borrar(documentoDigitalCotizacionDTO);
	}
}
