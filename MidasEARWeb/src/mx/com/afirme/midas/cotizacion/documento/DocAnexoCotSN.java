package mx.com.afirme.midas.cotizacion.documento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * 
 * @author Eric Villafana
 * @since 10 Septiembre de 2009
 */
public class DocAnexoCotSN {
	private DocAnexoCotFacadeRemote beanRemoto;

	public DocAnexoCotSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocAnexoCotFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public DocAnexoCotDTO getPorId(DocAnexoCotId docAnexoCotId) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(docAnexoCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public DocAnexoCotDTO agregar(DocAnexoCotDTO docAnexoCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(docAnexoCotDTO);
			return docAnexoCotDTO;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DocAnexoCotDTO docAnexoCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(docAnexoCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DocAnexoCotDTO modificar(DocAnexoCotDTO docAnexoCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(docAnexoCotDTO);
			return docAnexoCotDTO;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	
	/**
	 * Lista las entidades DocAnexoCotDTO 
	 * @param DocAnexoCotDTO docAnexoCotDTO
	 * @return List<TexAdicionalCotDTO> all DocAnexoCotDTO entities
	 */
	public List<DocAnexoCotDTO> listarfiltrado(DocAnexoCotDTO docAnexoCotDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(docAnexoCotDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
	
	public List<DocAnexoCotDTO> buscarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}