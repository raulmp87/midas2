package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;

import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.persona.PersonaForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ProveedorInspeccionForm extends MidasBaseForm{
	private static final long serialVersionUID = 1077261251805151260L;
	private String idToProveedorInspeccion;
	private String idToDireccion;
	private String idToPersona;
	private DireccionForm direccion;
	private PersonaForm persona;
	//Variable usada para definir la acci�n que se realizar� en desplegar.jsp de proveedorInspeccion
	private String accion="detalle";
	public String getIdToProveedorInspeccion() {
		return idToProveedorInspeccion;
	}
	public void setIdToProveedorInspeccion(String idToProveedorInspeccion) {
		this.idToProveedorInspeccion = idToProveedorInspeccion;
	}
	public DireccionForm getDireccion() {
		return direccion;
	}
	public void setDireccion(DireccionForm direccion) {
		this.direccion = direccion;
	}
	public PersonaForm getPersona() {
		return persona;
	}
	public void setPersona(PersonaForm persona) {
		this.persona = persona;
	}
	public String getIdToDireccion() {
		return idToDireccion;
	}
	public void setIdToDireccion(String idToDireccion) {
		this.idToDireccion = idToDireccion;
	}
	public String getIdToPersona() {
		return idToPersona;
	}
	public void setIdToPersona(String idToPersona) {
		this.idToPersona = idToPersona;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
}
