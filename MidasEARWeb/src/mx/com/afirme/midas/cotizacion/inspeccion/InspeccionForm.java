package mx.com.afirme.midas.cotizacion.inspeccion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class InspeccionForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4295743526435545319L;
	
	private String idToInspeccionIncisoCotizacion;
	private String nombre;
	private String tipoDePersona;
	private String telefono;
	private String agenteDeSeguros;
	private String calleYNumero;
	private String colonia;
	private String codigoPostal;
	private String ciudad;
	private String estado;
	private String numeroDeReporte;
	private String idProveedorInspeccion;
	private String personaEntrevistada;
	private String fechaDeLaInspeccion;
	private String fechaElaboracionDocumento;
	private String resultadoDeLaInspeccion;
	private String comentariosDeLaInspeccion;
	private String idToCotizacion;
	private String idToCotizacionFormateada;
	private String numeroInciso;
	private String fechaActual;
	
	// Getters & Setters
	
	public String getIdToInspeccionIncisoCotizacion() {
		return idToInspeccionIncisoCotizacion;
	}
	
	public void setIdToInspeccionIncisoCotizacion(String idToInspeccionIncisoCotizacion) {
		this.idToInspeccionIncisoCotizacion = idToInspeccionIncisoCotizacion;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoDePersona() {
		return tipoDePersona;
	}
	public void setTipoDePersona(String tipoDePersona) {
		this.tipoDePersona = tipoDePersona;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getAgenteDeSeguros() {
		return agenteDeSeguros;
	}
	public void setAgenteDeSeguros(String agenteDeSeguros) {
		this.agenteDeSeguros = agenteDeSeguros;
	}
	public String getCalleYNumero() {
		return calleYNumero;
	}
	public void setCalleYNumero(String calleYNumero) {
		this.calleYNumero = calleYNumero;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNumeroDeReporte() {
		return numeroDeReporte;
	}
	public void setNumeroDeReporte(String numeroDeReporte) {
		this.numeroDeReporte = numeroDeReporte;
	}
	public String getIdProveedorInspeccion() {
		return idProveedorInspeccion;
	}
	public void setIdProveedorInspeccion(String idProveedorInspeccion) {
		this.idProveedorInspeccion = idProveedorInspeccion;
	}
	public String getPersonaEntrevistada() {
		return personaEntrevistada;
	}
	public void setPersonaEntrevistada(String personaEntrevistada) {
		this.personaEntrevistada = personaEntrevistada;
	}
	public String getFechaDeLaInspeccion() {
		return fechaDeLaInspeccion;
	}
	public void setFechaDeLaInspeccion(String fechaDeLaInspeccion) {
		this.fechaDeLaInspeccion = fechaDeLaInspeccion;
	}
	public String getFechaElaboracionDocumento() {
		return fechaElaboracionDocumento;
	}
	public void setFechaElaboracionDocumento(String fechaElaboracionDocumento) {
		this.fechaElaboracionDocumento = fechaElaboracionDocumento;
	}
	public String getResultadoDeLaInspeccion() {
		return resultadoDeLaInspeccion;
	}
	public void setResultadoDeLaInspeccion(String resultadoDeLaInspeccion) {
		this.resultadoDeLaInspeccion = resultadoDeLaInspeccion;
	}
	public String getComentariosDeLaInspeccion() {
		return comentariosDeLaInspeccion;
	}
	public void setComentariosDeLaInspeccion(String comentariosDeLaInspeccion) {
		this.comentariosDeLaInspeccion = comentariosDeLaInspeccion;
	}
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getIdToCotizacionFormateada() {
		return idToCotizacionFormateada;
	}

	public void setIdToCotizacionFormateada(String idToCotizacionFormateada) {
		this.idToCotizacionFormateada = idToCotizacionFormateada;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	
}
