package mx.com.afirme.midas.cotizacion.inspeccion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Rodrigo M�rquez Castillo
 * @since  30 de Septiembre de 2009
 */
public class InspeccionIncisoCotizacionSN {
	private InspeccionIncisoCotizacionFacadeRemote beanRemoto;
	
	public InspeccionIncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(InspeccionIncisoCotizacionFacadeRemote.class);
		} catch (SystemException e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public InspeccionIncisoCotizacionDTO agregar(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion) throws ExcepcionDeAccesoADatos {
		InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = new InspeccionIncisoCotizacionDTO();
		try{
			if (UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacion.getIdToInspeccionIncisoCotizacion()))
					inspeccionIncisoCotizacionDTO = beanRemoto.save(inspeccionIncisoCotizacion);
			else
				inspeccionIncisoCotizacionDTO = beanRemoto.update(inspeccionIncisoCotizacion);
			
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return inspeccionIncisoCotizacionDTO;
	}
	
	public InspeccionIncisoCotizacionDTO modificar(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion) throws ExcepcionDeAccesoADatos {
		try{
			return beanRemoto.update(inspeccionIncisoCotizacion);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion) throws ExcepcionDeAccesoADatos {
		try{
			beanRemoto.delete(inspeccionIncisoCotizacion);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public InspeccionIncisoCotizacionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try{
			return beanRemoto.findById(id);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<InspeccionIncisoCotizacionDTO> getPorIdIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion){
		
		return beanRemoto.findByProperty("incisoCotizacion", incisoCotizacion);
	}

}