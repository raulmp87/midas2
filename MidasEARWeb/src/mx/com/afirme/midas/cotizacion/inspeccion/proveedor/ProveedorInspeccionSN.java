package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ProveedorInspeccionSN {
	private ProveedorInspeccionFacadeRemote beanRemoto;
	
	public ProveedorInspeccionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ProveedorInspeccionFacadeRemote.class);
		} catch (SystemException e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public ProveedorInspeccionDTO agregar(ProveedorInspeccionDTO proveedorInspeccionDTO) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		
		return beanRemoto.save(proveedorInspeccionDTO);
	}
	
	public void borrar(ProveedorInspeccionDTO proveedorInspeccionDTO) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		
		beanRemoto.delete(proveedorInspeccionDTO);
	}

	public void modificar (ProveedorInspeccionDTO proveedorInspeccionDTO) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		beanRemoto.update(proveedorInspeccionDTO);
	}
	
	public ProveedorInspeccionDTO getPorId(ProveedorInspeccionDTO proveedorInspeccionDTO) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		return beanRemoto.findById(proveedorInspeccionDTO.getIdToProveedorInspeccion());
	}
	
	public ProveedorInspeccionDTO getPorId(BigDecimal idToProveedorInspeccion) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		return beanRemoto.findById(idToProveedorInspeccion);
	}
	
	public List<ProveedorInspeccionDTO> listarTodos(){
		
		return beanRemoto.findAll();
	}
	
}
