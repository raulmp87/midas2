package mx.com.afirme.midas.cotizacion.inspeccion.parametros;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ParametroGeneralDN {
	
	private static final ParametroGeneralDN INSTANCIA = new ParametroGeneralDN();

	public static ParametroGeneralDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public List<ParametroGeneralDTO> getPorPropiedad(String propiedad, Object object) throws SystemException{
		ParametroGeneralSN parametroGeneralSN = new ParametroGeneralSN();
		return parametroGeneralSN.getPorPropiedad(propiedad, object);
	}
	
	public List<ParametroGeneralDTO> listarFiltrado(ParametroGeneralDTO parametroGeneralDTO) throws SystemException{
		return new ParametroGeneralSN().listarFiltrado(parametroGeneralDTO);
	}
	
	public ParametroGeneralDTO getPorId(ParametroGeneralId id) throws SystemException{
		return new ParametroGeneralSN().getPorId(id);
	}
	
	public List<ParametroGeneralDTO> obtenerListaParametroGeneral(BigDecimal idToGrupoParametroGeneral,String valor) throws SystemException{
		ParametroGeneralDTO parametroGeneral = new ParametroGeneralDTO();
		parametroGeneral.setId(new ParametroGeneralId());
		parametroGeneral.getId().setIdToGrupoParametroGeneral(idToGrupoParametroGeneral);
		parametroGeneral.setValor(valor);
		return listarFiltrado(parametroGeneral);
	}
}