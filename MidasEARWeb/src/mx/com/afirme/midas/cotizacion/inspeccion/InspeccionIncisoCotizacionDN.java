package mx.com.afirme.midas.cotizacion.inspeccion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class InspeccionIncisoCotizacionDN {
	private static final InspeccionIncisoCotizacionDN INSTANCIA = new InspeccionIncisoCotizacionDN();

	public static InspeccionIncisoCotizacionDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public InspeccionIncisoCotizacionDTO agregar(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos{
			InspeccionIncisoCotizacionSN inspeccionIncisoCotizacionSN = new InspeccionIncisoCotizacionSN();
		
		return inspeccionIncisoCotizacionSN.agregar(inspeccionIncisoCotizacionDTO);
	}
	
	public InspeccionIncisoCotizacionDTO modificar(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos{
		InspeccionIncisoCotizacionSN inspeccionIncisoCotizacionSN = new InspeccionIncisoCotizacionSN();
	
	return inspeccionIncisoCotizacionSN.modificar(inspeccionIncisoCotizacionDTO);
}
	
	public InspeccionIncisoCotizacionDTO getPorId(BigDecimal id) throws SystemException{
		
		InspeccionIncisoCotizacionSN inspeccionIncisoCotizacionSN = new InspeccionIncisoCotizacionSN();
		return inspeccionIncisoCotizacionSN.getPorId(id);		
	}
	
	public  List<InspeccionIncisoCotizacionDTO> getPorIdIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) throws SystemException{
		InspeccionIncisoCotizacionSN inspeccionIncisoCotizacionSN = new InspeccionIncisoCotizacionSN();
		
		return inspeccionIncisoCotizacionSN.getPorIdIncisoCotizacion(incisoCotizacion);		
	}

}