package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.direccion.DireccionSN;
import mx.com.afirme.midas.persona.PersonaSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ProveedorInspeccionDN {
	private static final ProveedorInspeccionDN INSTANCIA = new ProveedorInspeccionDN();

	public static ProveedorInspeccionDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public ProveedorInspeccionDTO agregar(ProveedorInspeccionDTO proveedorInspeccionDTO) throws SystemException, ExcepcionDeAccesoADatos{
		ProveedorInspeccionSN proveedorInspeccionSN = new ProveedorInspeccionSN();
		
		return proveedorInspeccionSN.agregar(proveedorInspeccionDTO);
	}
	
	public void modificar(ProveedorInspeccionDTO proveedorInspeccionDTO) throws SystemException, ExcepcionDeAccesoADatos{
		new ProveedorInspeccionSN().modificar(proveedorInspeccionDTO);
	}
	
	public void borrar(ProveedorInspeccionDTO proveedorInspeccionDTO) throws SystemException, ExcepcionDeAccesoADatos{
		ProveedorInspeccionSN proveedorInspeccionSN = new ProveedorInspeccionSN();
		
		proveedorInspeccionSN.borrar(proveedorInspeccionDTO);
	}

	public List<ProveedorInspeccionDTO> listarTodos() throws SystemException{
		ProveedorInspeccionSN proveedorInspeccionSN = new ProveedorInspeccionSN();
		
		return proveedorInspeccionSN.listarTodos();
	}
	
	public ProveedorInspeccionDTO getPorId(ProveedorInspeccionDTO proveedorInspeccionDTO) throws SystemException{
		ProveedorInspeccionSN proveedorInspeccionSN = new ProveedorInspeccionSN();
		return proveedorInspeccionSN.getPorId(proveedorInspeccionDTO);
	}
	
	public ProveedorInspeccionDTO getPorId(BigDecimal idToProveedorInspeccion) throws SystemException{
		return new ProveedorInspeccionSN().getPorId(idToProveedorInspeccion);
	}
	
	public ProveedorInspeccionDTO poblarPersonaDTO (ProveedorInspeccionDTO proveedorInspeccionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		if (proveedorInspeccionDTO != null){
			if (proveedorInspeccionDTO.getIdToProveedorInspeccion() != null){
				if (proveedorInspeccionDTO.getPersonaDTO() == null)
				proveedorInspeccionDTO.setPersonaDTO(new PersonaSN().getPorIdProveedorInspeccion(proveedorInspeccionDTO.getIdToProveedorInspeccion()));
			}
		}
		return proveedorInspeccionDTO;
	}
	
	public ProveedorInspeccionDTO poblarDireccionDTO (ProveedorInspeccionDTO proveedorInspeccionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		if (proveedorInspeccionDTO != null){
			if (proveedorInspeccionDTO.getIdToProveedorInspeccion() != null){
				if (proveedorInspeccionDTO.getDireccionDTO() == null)
					proveedorInspeccionDTO.setDireccionDTO(new DireccionSN().getPorIdProveedorInspeccion(proveedorInspeccionDTO.getIdToProveedorInspeccion()));
			}
		}
		return proveedorInspeccionDTO;
	}
}