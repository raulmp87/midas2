package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.CotizacionAction;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.persona.PersonaDN;
import mx.com.afirme.midas.persona.PersonaDTO;
import mx.com.afirme.midas.persona.PersonaForm;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProveedorInspeccionAction extends MidasMappingDispatchAction {

	
	/**
	 * Method listar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			request.setAttribute("listaProveedoresInspeccion", ProveedorInspeccionDN.getINSTANCIA().listarTodos());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProveedorInspeccionForm proveedorForm = (ProveedorInspeccionForm) form;
		ProveedorInspeccionDTO proveedorDTO = new ProveedorInspeccionDTO();
		PersonaDTO personaSession = (PersonaDTO) request.getSession().getAttribute("personaProveedor");
		DireccionDTO direccionSession = (DireccionDTO) request.getSession().getAttribute("direccionProveedor");
		try {
			if (personaSession == null || direccionSession == null){
				proveedorForm.setMensaje("El registro no fu� guardado. Debe introducir los datos de la persona y su direcci&oacute;n.");
			}
			else{
				proveedorDTO.setPersonaDTO(personaSession);
				proveedorDTO.setDireccionDTO(direccionSession);
				ProveedorInspeccionDN.getINSTANCIA().agregar(proveedorDTO);
				request.getSession().removeAttribute("personaProveedor");
				request.getSession().removeAttribute("direccionProveedor");
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);	
	}
	
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProveedorInspeccionForm proveedorForm = (ProveedorInspeccionForm) form;
		String id = request.getParameter("id");
		if (id != null)
			proveedorForm.setIdToProveedorInspeccion(id);
		ProveedorInspeccionDTO proveedorDTO = new ProveedorInspeccionDTO();
		try {
			proveedorDTO.setIdToProveedorInspeccion(UtileriasWeb.regresaBigDecimal(proveedorForm.getIdToProveedorInspeccion()));
			proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarDireccionDTO(proveedorDTO);
			proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
			ProveedorInspeccionDN.getINSTANCIA().borrar(proveedorDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);	
	}
	
	public ActionForward mostrarProveedorInspeccion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProveedorInspeccionForm proveedorForm = (ProveedorInspeccionForm) form;
		String idPadre = request.getParameter("idPadre");
		String accion = request.getParameter("accion");
		try {
			if ((!UtileriasWeb.esCadenaVacia(idPadre))){
				ProveedorInspeccionDTO proveedorDTO = new ProveedorInspeccionDTO();
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().getPorId(UtileriasWeb.regresaBigDecimal(idPadre));
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarDireccionDTO(proveedorDTO);
				this.poblarForm(proveedorDTO, proveedorForm);
				proveedorForm.getPersona().setDescripcionPadre("proveedorInspeccion");
				proveedorForm.getDireccion().setDescripcionPadre("proveedorInspeccion");
			}
			else{
				//Se usar� un "0" para identificar que es un nuevo registro
				PersonaDTO personaSession = (PersonaDTO) request.getSession().getAttribute("personaProveedor");
				DireccionDTO direccionSession = (DireccionDTO) request.getSession().getAttribute("direccionProveedor");
				if (personaSession == null)
					proveedorForm.setPersona((proveedorForm.getPersona() == null) ? new PersonaForm():proveedorForm.getPersona() );
				else{
					proveedorForm.setPersona(new PersonaForm());
					CotizacionAction.poblarPersonaForm(proveedorForm.getPersona(), personaSession);
				}
				if (direccionSession == null)
					proveedorForm.setDireccion((proveedorForm.getDireccion() == null) ? new DireccionForm():proveedorForm.getDireccion() );
				else{
					proveedorForm.setDireccion(new DireccionForm());
					DireccionAction.poblarDireccionForm(proveedorForm.getDireccion(), direccionSession);
				}
				if (UtileriasWeb.esCadenaVacia(proveedorForm.getDireccion().getIdToDireccion()))
					proveedorForm.getDireccion().setIdToDireccion("0");
				if (UtileriasWeb.esCadenaVacia(proveedorForm.getPersona().getIdToPersona())){
					proveedorForm.getPersona().setDescripcionTipoPersona("");
					proveedorForm.getPersona().setIdToPersona("0");
				}
				proveedorForm.getPersona().setDescripcionPadre("proveedorInspeccionNuevo");
				proveedorForm.getDireccion().setDescripcionPadre("proveedorInspeccionNuevo");
			}
			if (accion != null){
				proveedorForm.setAccion(accion);
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	
	
	private void poblarForm(ProveedorInspeccionDTO proveedorDTO, ProveedorInspeccionForm proveedorForm) throws SystemException{
		if (proveedorDTO.getIdToProveedorInspeccion() != null){
			proveedorForm.setIdToProveedorInspeccion(proveedorDTO.getIdToProveedorInspeccion().toString());
		}
		//Direccion
		proveedorForm.setDireccion(new DireccionForm());
		DireccionAction.poblarDireccionForm(proveedorForm.getDireccion(),proveedorDTO.getDireccionDTO());
		//Persona
		proveedorForm.setPersona(new PersonaForm());
		CotizacionAction.poblarPersonaForm(proveedorForm.getPersona(),proveedorDTO.getPersonaDTO());
	}
	
	@SuppressWarnings("unused")
	private void poblarDTO(ProveedorInspeccionForm proveedorForm, ProveedorInspeccionDTO proveedorDTO) throws SystemException{
			if ( !UtileriasWeb.esCadenaVacia(proveedorForm.getIdToProveedorInspeccion()))
				proveedorDTO.setIdToProveedorInspeccion(UtileriasWeb.regresaBigDecimal(proveedorForm.getIdToProveedorInspeccion()));
			//direccion
			if ( !UtileriasWeb.esCadenaVacia(proveedorForm.getIdToDireccion()))
				proveedorDTO.setDireccionDTO(DireccionDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(proveedorForm.getIdToDireccion())));
			//persona
			if ( !UtileriasWeb.esCadenaVacia(proveedorForm.getIdToPersona()))
				proveedorDTO.setPersonaDTO(PersonaDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(proveedorForm.getIdToPersona())));
		}
}