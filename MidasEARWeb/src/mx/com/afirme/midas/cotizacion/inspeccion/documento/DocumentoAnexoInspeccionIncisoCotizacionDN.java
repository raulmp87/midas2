package mx.com.afirme.midas.cotizacion.inspeccion.documento;

import java.util.List;

import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoAnexoInspeccionIncisoCotizacionDN {
	private static final DocumentoAnexoInspeccionIncisoCotizacionDN INSTANCIA = new DocumentoAnexoInspeccionIncisoCotizacionDN();

	public static DocumentoAnexoInspeccionIncisoCotizacionDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public DocumentoAnexoInspeccionIncisoCotizacionDTO agregar(DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacion) throws SystemException, ExcepcionDeAccesoADatos{
		DocumentoAnexoInspeccionIncisoCotizacionSN documentoAnexoInspeccionIncisoCotizacionSN = new DocumentoAnexoInspeccionIncisoCotizacionSN();
		
		return documentoAnexoInspeccionIncisoCotizacionSN.agregar(documentoAnexoInspeccionIncisoCotizacion);
	}
	
	public void borrar(DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacion) throws SystemException, ExcepcionDeAccesoADatos{
		DocumentoAnexoInspeccionIncisoCotizacionSN documentoAnexoInspeccionIncisoCotizacionSN = new DocumentoAnexoInspeccionIncisoCotizacionSN();
		
		documentoAnexoInspeccionIncisoCotizacionSN.borrar(documentoAnexoInspeccionIncisoCotizacion);
	}

	public List<DocumentoAnexoInspeccionIncisoCotizacionDTO> listarPorInspeccion(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion) throws SystemException{
		DocumentoAnexoInspeccionIncisoCotizacionSN documentoAnexoInspeccionIncisoCotizacionSN = new DocumentoAnexoInspeccionIncisoCotizacionSN();
		
		return documentoAnexoInspeccionIncisoCotizacionSN.listarPorInspeccion(inspeccionIncisoCotizacion);
	}
	
	public DocumentoAnexoInspeccionIncisoCotizacionDTO getPorId(DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacionDTO) throws SystemException{
		DocumentoAnexoInspeccionIncisoCotizacionSN documentoAnexoInspeccionIncisoCotizacionSN = new DocumentoAnexoInspeccionIncisoCotizacionSN();
		return documentoAnexoInspeccionIncisoCotizacionSN.getPorId(documentoAnexoInspeccionIncisoCotizacionDTO);
	}
}