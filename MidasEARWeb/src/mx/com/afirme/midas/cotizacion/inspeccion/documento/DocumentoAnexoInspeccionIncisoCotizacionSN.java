package mx.com.afirme.midas.cotizacion.inspeccion.documento;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoAnexoInspeccionIncisoCotizacionSN {
	private DocumentoAnexoInspeccionIncisoCotizacionFacadeRemote beanRemoto;
	
	public DocumentoAnexoInspeccionIncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoAnexoInspeccionIncisoCotizacionFacadeRemote.class);
		} catch (SystemException e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public DocumentoAnexoInspeccionIncisoCotizacionDTO agregar(DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacion) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		
		return beanRemoto.save(documentoAnexoInspeccionIncisoCotizacion);
	}
	
	public void borrar(DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacion) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		
		beanRemoto.delete(documentoAnexoInspeccionIncisoCotizacion);
	}
	
	public DocumentoAnexoInspeccionIncisoCotizacionDTO getPorId(DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		
		return beanRemoto.findById(documentoAnexoInspeccionIncisoCotizacionDTO.getIdToDocumentoAnexoInspeccionIncisoCotizacion());
	}
	
	/**
	 * 
	 * Recupera todos los DocumentoAnexoInspeccionIncisoCotizacionDTO que pertenezcan a una misma inspeccionIncisoCotizacionDTO
	 * @param InspeccionIncisoCotizacionDTO
	 * @return List<DocumentoAnexoInspeccionIncisoCotizacionDTO>, la lista de documentos pertenecientes a la inspeccionIncisoCotizacionDTO.
	 * @throws SystemException, ExcepcionDeAccesoADatos
	 */
	public List<DocumentoAnexoInspeccionIncisoCotizacionDTO> listarPorInspeccion(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos, ExcepcionDeAccesoADatos {
		
		return beanRemoto.findByProperty("inspeccionIncisoCotizacion.idToInspeccionIncisoCotizacion", inspeccionIncisoCotizacionDTO.getIdToInspeccionIncisoCotizacion());
	}

}
