package mx.com.afirme.midas.cotizacion.inspeccion.parametros;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import java.util.List;

public class ParametroGeneralSN {
	
	private ParametroGeneralFacadeRemote beanRemoto;
	
	public ParametroGeneralSN() throws SystemException{
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ParametroGeneralFacadeRemote.class);
		} catch (SystemException e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ParametroGeneralDTO> getPorPropiedad(String propiedad, Object object){
		return beanRemoto.findByProperty(propiedad, object);
	}
	
	public List<ParametroGeneralDTO> listarFiltrado(ParametroGeneralDTO parametroGeneralDTO){
		return beanRemoto.listarFiltrado(parametroGeneralDTO);
	}
	
	public ParametroGeneralDTO getPorId(ParametroGeneralId id){
		return beanRemoto.findById(id);
	}
	
}