package mx.com.afirme.midas.cotizacion.complementar;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class ComplementarCotizacionForm extends MidasBaseForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idToCotizacion;
	private String diasGracia;
	private String folioPolizaAsociada;
	private String idTcTipoNegocio;
	private String idMedioPago;
	private String generaComision;
	private Short claveAutoEmisionDocOperIlicitas;
	private Short aseguradoTipoPersona;
	/* 1:Identificaci�n del Asegurado */
	private String identificacionAseguradoArchivo;
	private String identificacionAseguradoIdArchivo;
	/* 2:RFC del Adegurado */
	private String rfcAseguradoArchivo;
	private String rfcAseguradoIdArchivo;
	/* 3:Comprobante de Domicilio del Asegurado */
	private String comprobanteDomicilioAseguradoArchivo;
	private String comprobanteDomicilioAseguradoIdArchivo;
	/* 4:Acta Constitutiva de la Empresa */
	private String actaConstitutivaEmpresaArchivo;
	private String actaConstitutivaEmpresaIdArchivo;
	/* 5:Poder del Representante Legal de la Empresa */
	private String poderRepresentanteEmpresaArchivo;
	private String poderRepresentanteEmpresaIdArchivo;
	/* 6: Identificaci�n del Representante Legal de la Empresa */
	private String identificacionRepresentanteEmpresaArchivo;
	private String identificacionRepresentanteEmpresaIdArchivo;
	/* 7:Comprobante de Domicilio de la Empresa */
	private String comprobanteDomicilioEmpresaArchivo;
	private String comprobanteDomicilioEmpresaIdArchivo;
	/* 8: C�dula Fiscal de la Empresa */
	private String cedulaFiscalEmpresaArchivo;
	private String cedulaFiscalEmpresaIdArchivo;	
	
	private String diasGraciaDefault;
	/*Tuve que agregar este campo para poder saber 
	cuando es Guardar o Emitir y poder hacer validaciones en el validator 
	con el validwhen ya que no puedo extender de ValidatorActionForm ya que 
	debemos de heredar de MidasBaseForm los valores seran:
	1: guardar
	2: emitir */
	private String tipoDeAccion; 
	
	//variables para notificar a reaseguro la
	//emision de una poliza.
	private String idToPoliza;
	private String noEndoso;
	private String fechaCreacionPoliza;
	private String origen;
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null)
			errors = new ActionErrors();

		if (this.folioPolizaAsociada != null
				&& !this.folioPolizaAsociada.equals("")) {
			if (this.folioPolizaAsociada.length() < 14) {
				errors.add("folioPolizaAsociada", new ActionMessage(
						"errors.mask", UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"cotizacion.complementar.polizaAsociada"),
						"PPTT-NNNNNN-RR"));
			}

		}
		return errors;
	}
	
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getDiasGracia() {
		return diasGracia;
	}
	public void setDiasGracia(String diasGracia) {
		this.diasGracia = diasGracia;
	}
	public String getFolioPolizaAsociada() {
		return folioPolizaAsociada;
	}
	public void setFolioPolizaAsociada(String folioPolizaAsociada) {
		this.folioPolizaAsociada = folioPolizaAsociada;
	}
	public String getIdTcTipoNegocio() {
		return idTcTipoNegocio;
	}
	public void setIdTcTipoNegocio(String idTcTipoNegocio) {
		this.idTcTipoNegocio = idTcTipoNegocio;
	}
	public String getIdMedioPago() {
		return idMedioPago;
	}
	public void setIdMedioPago(String idMedioPago) {
		this.idMedioPago = idMedioPago;
	}
	public String getGeneraComision() {
		return generaComision;
	}
	public void setGeneraComision(String generaComision) {
		this.generaComision = generaComision;
	}
	public void setAseguradoTipoPersona(Short aseguradoTipoPersona) {
		this.aseguradoTipoPersona = aseguradoTipoPersona;
	}
	public Short getAseguradoTipoPersona() {
		return aseguradoTipoPersona;
	}
	public void setClaveAutoEmisionDocOperIlicitas(
			Short claveAutoEmisionDocOperIlicitas) {
		this.claveAutoEmisionDocOperIlicitas = claveAutoEmisionDocOperIlicitas;
	}
	public Short getClaveAutoEmisionDocOperIlicitas() {
		return claveAutoEmisionDocOperIlicitas;
	}
	public void setTipoDeAccion(String tipoDeAccion) {
		this.tipoDeAccion = tipoDeAccion;
	}
	public String getTipoDeAccion() {
		return tipoDeAccion;
	}
	public String getIdentificacionAseguradoArchivo() {
		return identificacionAseguradoArchivo;
	}
	public void setIdentificacionAseguradoArchivo(
			String identificacionAseguradoArchivo) {
		this.identificacionAseguradoArchivo = identificacionAseguradoArchivo;
	}
	public String getIdentificacionAseguradoIdArchivo() {
		return identificacionAseguradoIdArchivo;
	}
	public void setIdentificacionAseguradoIdArchivo(
			String identificacionAseguradoIdArchivo) {
		this.identificacionAseguradoIdArchivo = identificacionAseguradoIdArchivo;
	}
	public String getRfcAseguradoArchivo() {
		return rfcAseguradoArchivo;
	}
	public void setRfcAseguradoArchivo(String rfcAseguradoArchivo) {
		this.rfcAseguradoArchivo = rfcAseguradoArchivo;
	}
	public String getRfcAseguradoIdArchivo() {
		return rfcAseguradoIdArchivo;
	}
	public void setRfcAseguradoIdArchivo(String rfcAseguradoIdArchivo) {
		this.rfcAseguradoIdArchivo = rfcAseguradoIdArchivo;
	}
	public String getComprobanteDomicilioAseguradoArchivo() {
		return comprobanteDomicilioAseguradoArchivo;
	}
	public void setComprobanteDomicilioAseguradoArchivo(
			String comprobanteDomicilioAseguradoArchivo) {
		this.comprobanteDomicilioAseguradoArchivo = comprobanteDomicilioAseguradoArchivo;
	}
	public void setComprobanteDomicilioAseguradoIdArchivo(
			String comprobanteDomicilioAseguradoIdArchivo) {
		this.comprobanteDomicilioAseguradoIdArchivo = comprobanteDomicilioAseguradoIdArchivo;
	}
	public String getComprobanteDomicilioAseguradoIdArchivo() {
		return comprobanteDomicilioAseguradoIdArchivo;
	}
	public String getActaConstitutivaEmpresaArchivo() {
		return actaConstitutivaEmpresaArchivo;
	}
	public void setActaConstitutivaEmpresaArchivo(
			String actaConstitutivaEmpresaArchivo) {
		this.actaConstitutivaEmpresaArchivo = actaConstitutivaEmpresaArchivo;
	}
	public String getActaConstitutivaEmpresaIdArchivo() {
		return actaConstitutivaEmpresaIdArchivo;
	}
	public void setActaConstitutivaEmpresaIdArchivo(
			String actaConstitutivaEmpresaIdArchivo) {
		this.actaConstitutivaEmpresaIdArchivo = actaConstitutivaEmpresaIdArchivo;
	}
	public String getPoderRepresentanteEmpresaArchivo() {
		return poderRepresentanteEmpresaArchivo;
	}
	public void setPoderRepresentanteEmpresaArchivo(
			String poderRepresentanteEmpresaArchivo) {
		this.poderRepresentanteEmpresaArchivo = poderRepresentanteEmpresaArchivo;
	}
	public String getPoderRepresentanteEmpresaIdArchivo() {
		return poderRepresentanteEmpresaIdArchivo;
	}
	public void setPoderRepresentanteEmpresaIdArchivo(
			String poderRepresentanteEmpresaIdArchivo) {
		this.poderRepresentanteEmpresaIdArchivo = poderRepresentanteEmpresaIdArchivo;
	}
	public String getIdentificacionRepresentanteEmpresaArchivo() {
		return identificacionRepresentanteEmpresaArchivo;
	}
	public void setIdentificacionRepresentanteEmpresaArchivo(
			String identificacionRepresentanteEmpresaArchivo) {
		this.identificacionRepresentanteEmpresaArchivo = identificacionRepresentanteEmpresaArchivo;
	}
	public String getIdentificacionRepresentanteEmpresaIdArchivo() {
		return identificacionRepresentanteEmpresaIdArchivo;
	}
	public void setIdentificacionRepresentanteEmpresaIdArchivo(
			String identificacionRepresentanteEmpresaIdArchivo) {
		this.identificacionRepresentanteEmpresaIdArchivo = identificacionRepresentanteEmpresaIdArchivo;
	}
	public String getComprobanteDomicilioEmpresaArchivo() {
		return comprobanteDomicilioEmpresaArchivo;
	}
	public void setComprobanteDomicilioEmpresaArchivo(
			String comprobanteDomicilioEmpresaArchivo) {
		this.comprobanteDomicilioEmpresaArchivo = comprobanteDomicilioEmpresaArchivo;
	}
	public String getComprobanteDomicilioEmpresaIdArchivo() {
		return comprobanteDomicilioEmpresaIdArchivo;
	}
	public void setComprobanteDomicilioEmpresaIdArchivo(
			String comprobanteDomicilioEmpresaIdArchivo) {
		this.comprobanteDomicilioEmpresaIdArchivo = comprobanteDomicilioEmpresaIdArchivo;
	}
	public String getCedulaFiscalEmpresaArchivo() {
		return cedulaFiscalEmpresaArchivo;
	}
	public void setCedulaFiscalEmpresaArchivo(String cedulaFiscalEmpresaArchivo) {
		this.cedulaFiscalEmpresaArchivo = cedulaFiscalEmpresaArchivo;
	}
	public String getCedulaFiscalEmpresaIdArchivo() {
		return cedulaFiscalEmpresaIdArchivo;
	}
	public void setCedulaFiscalEmpresaIdArchivo(String cedulaFiscalEmpresaIdArchivo) {
		this.cedulaFiscalEmpresaIdArchivo = cedulaFiscalEmpresaIdArchivo;
	}
	public void setDiasGraciaDefault(String diasGraciaDefault) {
		this.diasGraciaDefault = diasGraciaDefault;
	}
	public String getDiasGraciaDefault() {
		return diasGraciaDefault;
	}
	public String getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(String idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public String getNoEndoso() {
		return noEndoso;
	}
	public void setNoEndoso(String noEndoso) {
		this.noEndoso = noEndoso;
	}
	public String getFechaCreacionPoliza() {
		return fechaCreacionPoliza;
	}
	public void setFechaCreacionPoliza(String fechaCreacionPoliza) {
		this.fechaCreacionPoliza = fechaCreacionPoliza;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getOrigen() {
		return origen;
	}
}
