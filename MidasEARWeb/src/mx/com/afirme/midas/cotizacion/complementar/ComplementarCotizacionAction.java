package mx.com.afirme.midas.cotizacion.complementar;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDN;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.diasgracia.DiasGraciaDN;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDN;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.cancelacion.CancelacionPolizaDN;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionEndosoDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.tareas.endoso.TareaRehabilitacionEndoso;
import mx.com.afirme.midas.solicitud.SolicitudDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

public class ComplementarCotizacionAction extends MidasMappingDispatchAction {
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaDeNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("id");
		String origen = request.getParameter("origen");
		ComplementarCotizacionForm complementarForm = (ComplementarCotizacionForm) form;
		complementarForm.setIdToCotizacion(idToCotizacion);
		complementarForm.setOrigen(origen);
		try {
			inicializarMostrar(complementarForm, request);
		}
		catch (SystemException e) {
			reglaDeNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(complementarForm, e);
		}
		return mapping.findForward(reglaDeNavegacion);
	}
	
	public ActionForward guardar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward = mapping.findForward(Sistema.EXITOSO);
		ComplementarCotizacionForm complementarForm = (ComplementarCotizacionForm) form;
		String folioPolizaAsociada = complementarForm.getFolioPolizaAsociada();
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, 
				Sistema.USUARIO_ACCESO_MIDAS);
				
		try {
			//System.out.println(UsuarioWSDN.getInstancia().getUserAccessId());
			
			PolizaDN polizaDN = PolizaDN.getInstancia();
			if (!StringUtil.isEmpty(folioPolizaAsociada)
					&& ! polizaDN.existePoliza(folioPolizaAsociada,usuario)) {
				forward = mapping.getInputForward();
				ActionMessages actionMessages = new ActionMessages();
				actionMessages.add("folioPolizaAsociada", 
						new ActionMessage("cotizacion.folioPolizaAsociada.noExiste"));
				MessageResources messageResources = getResources(request);
				saveErrors(request, actionMessages);
				String mensaje = messageResources.getMessage("cotizacion.folioPolizaAsociada.noExiste");
				agregarMensaje(mensaje, Sistema.ERROR, complementarForm);
			}
			else {

				CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());
				BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(complementarForm.getIdToCotizacion());
				CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
				poblarCotizacionDTO(cotizacionDTO, complementarForm,usuario.getNombreUsuario());
				
				if(cotizacionDTO.getDiasGracia() != null && (cotizacionDTO.getDiasGracia().intValue() < 1 || cotizacionDTO.getDiasGracia() > 365)){
					throw new SystemException("El rango de dias de gracia es de 1 a 365.");
				}
				
				cotizacionDN.modificar(cotizacionDTO);

				//Actualizar la lista de combos
				TipoNegocioDN tipoNegocioDN = TipoNegocioDN.getInstancia();
				request.getSession().setAttribute("tiposNegocios", 
						tipoNegocioDN.traerTodosPorIdTcTipoNegocio(
								new BigDecimal(complementarForm.getIdTcTipoNegocio())));
				mensajeExitoModificar(complementarForm);
			}
		}
		catch (SystemException e) {
			forward = mapping.findForward(Sistema.NO_DISPONIBLE);
			mensajeExcepcion(complementarForm, e);			
		}
		return forward;
	}
	
	public ActionForward emitir(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaDeNavegacion = Sistema.EXITOSO;
		ComplementarCotizacionForm complementarForm = (ComplementarCotizacionForm) form;
		try {
			Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(complementarForm.getIdToCotizacion());
			Set<String> errores = cotizacionDN.validarCotizacion(idToCotizacion,usuario);
			
			if (errores == null || errores.size() == 0) {
				
				CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
				Map<String, String> mensajeEmision;
				//Si el tipo de solicitud corresponde al de un endoso
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().equals (Sistema.SOLICITUD_TIPO_ENDOSO)) {
					
					if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
						mensajeEmision = EndosoDN.getInstancia(usuario.getNombreUsuario()).emitirEndoso(cotizacionDTO);
					} else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION) {
						mensajeEmision = CancelacionPolizaDN.getInstancia().cancelacionManualPolizaEndoso(cotizacionDTO, usuario.getNombreUsuario());
					} else{
						mensajeEmision = EndosoDN.getInstancia(usuario.getNombreUsuario()).emitirEndosoSP(cotizacionDTO);
					}
					
					agregarMensaje(mensajeEmision.get("mensaje"), mensajeEmision.get("icono"), complementarForm);	
					complementarForm.setIdToPoliza(mensajeEmision.get("idtopoliza"));
					complementarForm.setNoEndoso(mensajeEmision.get("noendoso"));
					complementarForm.setFechaCreacionPoliza(mensajeEmision.get("fechacreacion"));					
							
				} else {
					//Se trata de emision de poliza
					
					PolizaDTO polizaDTO= new PolizaDTO();
					polizaDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
					if(complementarForm.getFolioPolizaAsociada() != null && !complementarForm.getFolioPolizaAsociada().equals("")){
						String numeroDePolizaConFormato = complementarForm.getFolioPolizaAsociada();
						cotizacionDTO.setFolioPolizaAsociada(numeroDePolizaConFormato);
						CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);
						
					}
			        
					mensajeEmision = PolizaDN.getInstancia().emitirPoliza(idToCotizacion,usuario.getNombreUsuario());
					agregarMensaje(mensajeEmision.get("mensaje"), mensajeEmision.get("icono"), complementarForm);
					complementarForm.setIdToPoliza(mensajeEmision.get("idtopoliza"));
					complementarForm.setNoEndoso(mensajeEmision.get("noendoso"));
					complementarForm.setFechaCreacionPoliza(mensajeEmision.get("fechacreacion"));
					
				}
				if(mensajeEmision != null && mensajeEmision.get("icono").equals(Sistema.EXITO)){
					BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(mensajeEmision.get("idtopoliza"));
					BigDecimal noEndoso = UtileriasWeb.regresaBigDecimal(mensajeEmision.get("noendoso"));
					
    				/**
    				 * 29/04/2010 JLAB. Se agrega la notificaci�n de la emisi�n a reaseguro.
    				 */
    				SolicitudDN.getInstancia().registraEstadisticas(cotizacionDTO.getSolicitudDTO().getIdToSolicitud(), 
    					cotizacionDTO.getIdToCotizacion(), idToPoliza,noEndoso,usuario.getNombreUsuario(), Sistema.SE_EMITE_COTIZACION);
    				Date fechaEmision = null;
    				try {
    					fechaEmision = UtileriasWeb.getFechaFromString(mensajeEmision.get("fechacreacion"));
        				if(noEndoso.intValue() == 0){
        					SoporteReaseguroCotizacionDN soporte = new SoporteReaseguroCotizacionDN(cotizacionDTO.getIdToCotizacion());
        					soporte.notificarEmision(idToPoliza, noEndoso.intValue(), fechaEmision);
        				}else if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION ||
        						cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION ||
        						cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
        					EndosoDTO endosoAnterior = EndosoDN.getInstancia(usuario.getNombreUsuario()).getPenultimoEndoso(idToPoliza);
        					EndosoDTO endosoNuevo = EndosoDN.getInstancia(usuario.getNombreUsuario()).getUltimoEndoso(idToPoliza);
        					SoporteReaseguroCotizacionDN soporte = new SoporteReaseguroCotizacionDN(endosoAnterior.getIdToCotizacion());
        					if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION)
        						//Se envia el id de la cotizacion del endoso emitido, que es diferente a la cotizacion original
        						soporte.notificarEndosoCancelacion(idToPoliza, noEndoso.intValue(), endosoNuevo.getIdToCotizacion(), endosoNuevo.getFechaCreacion(), 
        								endosoNuevo.getFechaInicioVigencia(), endosoNuevo.getFactorAplicacion(), null,usuario,null,null);
        
        					if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION)
        						soporte.notificarEndosoRehabilitacion(idToPoliza, noEndoso.intValue(), cotizacionDTO.getIdToCotizacion(), endosoNuevo.getFechaCreacion(), 
        								endosoNuevo.getFechaInicioVigencia(), endosoNuevo.getFactorAplicacion(), null, usuario,null,null);
        					if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)
        						soporte.notificarEndosoCambioFormaPago(idToPoliza, noEndoso.intValue(), cotizacionDTO.getIdToCotizacion(), endosoNuevo.getFechaCreacion(), 
        								endosoNuevo.getFechaInicioVigencia(), endosoNuevo.getFactorAplicacion(), null,usuario,null,null);
        				}else{
        					SoporteReaseguroCotizacionEndosoDN soporteEndoso = new SoporteReaseguroCotizacionEndosoDN(idToPoliza, noEndoso.intValue()-1, cotizacionDTO.getIdToCotizacion(),false);
							soporteEndoso.notificarEmision(idToPoliza, noEndoso.intValue(), fechaEmision);
        				}
					} catch (ParseException e1) {
						LogDeMidasWeb.log("ComplementarCotizacionAction.emitir. Formato de fecha incorrecto: "+mensajeEmision.get("fechacreacion")+". No se realiz� la notificaci�n de emisi�n a reaseguro. idToPoliza:"+idToPoliza+", numeroEndoso: "+noEndoso, Level.SEVERE, e1);
					} catch (Exception e) {
						String error = "Ocurri� un error al realizar la notificaci�n de emisi�n a reaseguro. idToPoliza: "+idToPoliza+", numeroEndoso: "+noEndoso;
						UtileriasWeb.registraLogInteraccionReaseguro("ComplementarCotizacionAction.emitir(...)", Sistema.MODULO_REASEGURO, "SoporteReaseguroBase", 
								"notificarEmision(...)", usuario.getNombreUsuario(), "(ifToPoliza("+idToPoliza+"),noEndoso("+noEndoso+"),fechaEmision("+fechaEmision+"))", 
								"void", e, error, "La p�liza se emiti� correctamente, s�lo fall� la notificaci�n a reaseguro.", this.toString(), Sistema.MODULO_DANIOS);
					}
				}
			} else {
				// Generar mensajes para mostrarlo en el popup.
				final MessageResources messageResources = getResources(request);
				final Set<String> erroresTraducidos = new HashSet<String>();
				for (String key : errores) {
					final String error = messageResources.getMessage(key);
					erroresTraducidos.add(error == null ? key : error);
				}
				String mensajeInicial = messageResources.getMessage("cotizacion.emitir.faltanDatos");
				agregarMensajes(erroresTraducidos, Sistema.ERROR, complementarForm, mensajeInicial);
			}
		}
		catch (SystemException e) {
			reglaDeNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(complementarForm, e);			
		}
		return mapping.findForward(reglaDeNavegacion);
	}
	
	
	public void rehabilitacionEndosos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		boolean consultarSeycos = true;
		String consultarSeycosString = request.getParameter("consultaSeycos");
		if(!UtileriasWeb.esCadenaVacia(consultarSeycosString) && consultarSeycosString.equalsIgnoreCase("false")){
			consultarSeycos = false;
		}
		new TareaRehabilitacionEndoso().rehabilitaEndososPendientes(consultarSeycos);
	}
	
	public ActionForward solicitarAutorizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		//TODO:Implementar y probar
		String reglaDeNavegacion = Sistema.EXITOSO;
		ComplementarCotizacionForm complementarForm = (ComplementarCotizacionForm) form;
		try {
			Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, 
					Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(complementarForm.getIdToCotizacion());
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			cotizacionDTO.setClaveAutoEmisionDocOperIlicitas(
					Sistema.AUTORIZACION_REQUERIDA); //Cambiar el estatus a REQUERIDA.
			cotizacionDN.modificar(cotizacionDTO);
			
			//Mensaje de ex�to
			MessageResources messageResources = getResources(request);
			complementarForm.setMensaje(messageResources.getMessage(
					"cotizacion.complementar.solicitarAutorizacion.exito"));
			complementarForm.setTipoMensaje(Sistema.EXITO);
			complementarForm.setClaveAutoEmisionDocOperIlicitas(Sistema.AUTORIZACION_REQUERIDA);
		} 
		catch(SystemException e) {
			reglaDeNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(complementarForm, e);	
		}
		return mapping.findForward(reglaDeNavegacion);
	}
	
	public ActionForward autorizarOmitirDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaDeNavegacion = Sistema.EXITOSO;
		ComplementarCotizacionForm complementarForm = (ComplementarCotizacionForm) form;
		try {
			Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, 
					Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(
					complementarForm.getIdToCotizacion());
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			cotizacionDTO.setClaveAutoEmisionDocOperIlicitas(
					Sistema.AUTORIZADA); //Cambiar el estatus a REQUERIDA.
			cotizacionDN.modificar(cotizacionDTO);
			
			//Mensaje de ex�to
			MessageResources messageResources = getResources(request);
			complementarForm.setMensaje(messageResources.getMessage(
					"cotizacion.complementar.autorizador.autorizarMensaje"));
			complementarForm.setTipoMensaje(Sistema.EXITO);
			complementarForm.setClaveAutoEmisionDocOperIlicitas(Sistema.AUTORIZADA);
		}
		catch (SystemException e) {
			reglaDeNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(complementarForm, e);
		}
		return mapping.findForward(reglaDeNavegacion);
	}
	
	public ActionForward rechazarOmitirDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaDeNavegacion = Sistema.EXITOSO;
		ComplementarCotizacionForm complementarForm = (ComplementarCotizacionForm) form;
		try {
			Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, 
					Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(
					complementarForm.getIdToCotizacion());
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
			cotizacionDTO.setClaveAutoEmisionDocOperIlicitas(
					Sistema.RECHAZADA); //Cambiar el estatus a REQUERIDA.
			cotizacionDN.modificar(cotizacionDTO);
			
			//Mensaje de ex�to
			MessageResources messageResources = getResources(request);
			complementarForm.setMensaje(messageResources.getMessage(
					"cotizacion.complementar.autorizador.rechazarMensaje"));
			complementarForm.setTipoMensaje(Sistema.EXITO);
			complementarForm.setClaveAutoEmisionDocOperIlicitas(Sistema.RECHAZADA);
		}
		catch (SystemException e) {
			reglaDeNavegacion = Sistema.NO_DISPONIBLE;
			mensajeExcepcion(complementarForm, e);
		}
		return mapping.findForward(reglaDeNavegacion);
	}
	
	private void poblarCotizacionDTO(CotizacionDTO cotizacionDTO,
			ComplementarCotizacionForm complementarForm,String claveUsuario) throws SystemException {
		Integer diasGracia = new Integer(complementarForm.getDiasGracia());
		
		Integer diasGraciaDefault = DiasGraciaDN.getInstancia().obtenerDiasGraciaDefault();
		
		Integer diasGraciaAnterior = cotizacionDTO.getDiasGracia();
		
		//la autorizaci�n se requiere cuando los d�as son diferentes al valor default
		if(diasGracia.intValue() != diasGraciaDefault.intValue()){
			if(cotizacionDTO.getClaveAutorizacionDiasGracia() == null || 
					cotizacionDTO.getClaveAutorizacionDiasGracia().shortValue() == Sistema.AUTORIZACION_NO_REQUERIDA ||
					(diasGraciaAnterior != null && diasGraciaAnterior.intValue() != diasGracia.intValue())){
				cotizacionDTO.setClaveAutorizacionDiasGracia(Sistema.AUTORIZACION_REQUERIDA);
				cotizacionDTO.setCodigoUsuarioSolicitaAutDiasGracia(claveUsuario);
				cotizacionDTO.setFechaSolicitudAutDiasGracia(new Date());
			}
		}
		else{
			if(cotizacionDTO.getClaveAutorizacionDiasGracia() != null){
				cotizacionDTO.setClaveAutorizacionDiasGracia(Sistema.AUTORIZACION_NO_REQUERIDA);
			}
		}
		
		cotizacionDTO.setDiasGracia(diasGracia);
		cotizacionDTO.setFolioPolizaAsociada(complementarForm.getFolioPolizaAsociada());
		if (cotizacionDTO.getTipoNegocioDTO() == null) {
			cotizacionDTO.setTipoNegocioDTO(new TipoNegocioDTO());
		}
		cotizacionDTO.getTipoNegocioDTO().setIdTcTipoNegocio(new BigDecimal(
				complementarForm.getIdTcTipoNegocio()));
		cotizacionDTO.setIdMedioPago(new BigDecimal(
				complementarForm.getIdMedioPago()));
	}

	private void inicializarMostrar(ComplementarCotizacionForm complementarForm,HttpServletRequest request) throws SystemException {
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request,Sistema.USUARIO_ACCESO_MIDAS);
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());
		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(complementarForm.getIdToCotizacion());
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
		TipoNegocioDN tipoNegocioDN = TipoNegocioDN.getInstancia();
		MedioPagoDN medioPagoDN = MedioPagoDN.getInstancia();
		
		/*
		 * 13/06/2011 Los dias de gracia deben provenir de Seycos, se ignora el valor de la configuracion.
		 */
		Integer diasGraciaDefault = DiasGraciaDN.getInstancia().obtenerDiasGraciaDefault();
			//cotizacionDTO.getTipoPolizaDTO().getDiasGracia();
		Integer diasGracia = cotizacionDTO.getDiasGracia() == null ? diasGraciaDefault : cotizacionDTO.getDiasGracia();
		complementarForm.setDiasGracia(String.valueOf(diasGracia));
		complementarForm.setDiasGraciaDefault(String.valueOf(diasGraciaDefault));
		
		String polizaAsociada = cotizacionDTO.getFolioPolizaAsociada() == null ? "": cotizacionDTO.getFolioPolizaAsociada();
		
		if(cotizacionDTO.getSolicitudDTO().getEsRenovacion() != null && cotizacionDTO.getSolicitudDTO().getEsRenovacion().shortValue() == Sistema.ES_RENOVACION){
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getIdToPolizaAnterior());
			polizaAsociada = UtileriasWeb.getNumeroPoliza(polizaDTO);
		}
			
		complementarForm.setFolioPolizaAsociada(polizaAsociada);
		
		if (cotizacionDTO.getTipoNegocioDTO() == null ||
				cotizacionDTO.getTipoNegocioDTO().getIdTcTipoNegocio().intValue() == 0) {
			TipoNegocioDTO tipoNegocioDTO = TipoNegocioDN.getInstancia().buscarPorPropiedad("codigoTipoNegocio", 99).get(0); 
			complementarForm.setIdTcTipoNegocio(String.valueOf(tipoNegocioDTO.getIdTcTipoNegocio()));
		}
		else {
			complementarForm.setIdTcTipoNegocio(String.valueOf(
					cotizacionDTO.getTipoNegocioDTO().getIdTcTipoNegocio()));
		}
		complementarForm.setIdMedioPago(String.valueOf(cotizacionDTO.getIdMedioPago()));

		Integer idAgente = cotizacionDTO.getSolicitudDTO().getCodigoAgente().intValue();
		AgenteDN agenteDN = AgenteDN.getInstancia();
		AgenteDTO agenteDTO = new AgenteDTO();
		agenteDTO.setIdTcAgente(idAgente);
		agenteDTO = agenteDN.verDetalleAgente(agenteDTO, usuario.getNombreUsuario());
		complementarForm.setGeneraComision(agenteDTO.getGeneraComision());
		
		ClienteDTO clienteDTO = null;
		
		if (cotizacionDTO.getIdDomicilioAsegurado() != null
				&& cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0) {
			clienteDTO = new ClienteDTO();
			clienteDTO
					.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
			clienteDTO.setIdDomicilio(cotizacionDTO
					.getIdDomicilioAsegurado());
			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(
					clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
		} else {
			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(
					cotizacionDTO.getIdToPersonaAsegurado(),
					cotizacionDTO.getCodigoUsuarioCotizacion());
		}			
		complementarForm.setAseguradoTipoPersona(clienteDTO.getClaveTipoPersona());
		
		complementarForm.setClaveAutoEmisionDocOperIlicitas(cotizacionDTO.getClaveAutoEmisionDocOperIlicitas() == null? Sistema.AUTORIZACION_NO_REQUERIDA : cotizacionDTO.getClaveAutoEmisionDocOperIlicitas());
		
		/*Llenado de nombre de documento y el id del documento */
		DocPrevencionOperIlicitasDN docPrevencionOperIlicitasDN = DocPrevencionOperIlicitasDN.getInstancia();
		List<DocPrevencionOperIlicitasDTO> documentos = docPrevencionOperIlicitasDN.buscarPorPropiedad(
				"idToCotizacion", idToCotizacion);
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		for (DocPrevencionOperIlicitasDTO documento: documentos) {
			ControlArchivoDTO controlArchivoDTO = controlArchivoDN.getPorId(
					documento.getIdToControlArchivo());
			switch (documento.getClaveTipoDocumento()) {
				case 1:
					complementarForm.setIdentificacionAseguradoArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setIdentificacionAseguradoIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
				case 2:
					complementarForm.setRfcAseguradoArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setRfcAseguradoIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
				case 3:
					complementarForm.setComprobanteDomicilioAseguradoArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setComprobanteDomicilioAseguradoIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
				case 4:
					complementarForm.setActaConstitutivaEmpresaArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setActaConstitutivaEmpresaIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
				case 5:
					complementarForm.setPoderRepresentanteEmpresaArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setPoderRepresentanteEmpresaIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
				case 6:
					complementarForm.setIdentificacionRepresentanteEmpresaArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setIdentificacionRepresentanteEmpresaIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
				case 7:
					complementarForm.setComprobanteDomicilioEmpresaArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setComprobanteDomicilioEmpresaIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());

					break;
				case 8:
					complementarForm.setCedulaFiscalEmpresaArchivo(
							controlArchivoDTO.getNombreArchivoOriginal());
					complementarForm.setCedulaFiscalEmpresaIdArchivo(
							controlArchivoDTO.getIdToControlArchivo().toString());
					break;
					
			}
		}
		
		//Combos
		request.getSession().setAttribute("tiposNegocios", 
				tipoNegocioDN.traerTodosPorIdTcTipoNegocio(
						new BigDecimal(complementarForm.getIdTcTipoNegocio())));
		request.getSession().setAttribute("mediosPago", 
				medioPagoDN.listarTodos());

	}
	
	//Este metodo tambien estaria bien meterlo tal vez en MidasMappingDispatchAction
	/*private Map<String, String> generarMensajes(Set<String> errores, 
			HttpServletRequest request) {
		MessageResources messageResources = getResources(request);
		Map<String, String> mensajes = new HashMap<String,String>();
		for (String errorKey: errores) {
			String descripcionError = messageResources.getMessage(errorKey);
			mensajes.put(errorKey, (descripcionError != null ? descripcionError : errorKey) );
		}
		return mensajes;
	}*/
	
	public void notificarEmision(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToPoliza = request.getParameter("idToPoliza");
		Integer noEndoso = Integer.valueOf(request.getParameter("noEndoso"));
		String fechaCreacionPoliza = request.getParameter("fechaCreacionPoliza");
		String ignorarSoporteExistenteString = request.getParameter("ignorarSoporteExistente");
		String ignorarSoporteFacultativoString = request.getParameter("ignorarSoporteFacultativo");
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, 
				Sistema.USUARIO_ACCESO_MIDAS);		
		try{
			BigDecimal idToPolizaB = UtileriasWeb.regresaBigDecimal(idToPoliza);
			BigDecimal idToCotizacionB = UtileriasWeb.regresaBigDecimal(idToCotizacion);
			Date fechaEmision = UtileriasWeb.getFechaFromString(fechaCreacionPoliza);
//			SoporteReaseguro soporteReaseguro = null;
			SoporteReaseguroCotizacionDN soporte = null;
			SoporteReaseguroCotizacionEndosoDN soporteEndoso = null;
			
			Boolean ignorarSoporteExistente = null;
			if(!UtileriasWeb.esCadenaVacia(ignorarSoporteExistenteString) && ignorarSoporteExistenteString.equals("true")){
				ignorarSoporteExistente = true;
			}
			Boolean ignorarSoporteFacultativo = null;
			if(!UtileriasWeb.esCadenaVacia(ignorarSoporteFacultativoString) && ignorarSoporteFacultativoString.equals("true")){
				ignorarSoporteFacultativo = true;
			}
			
			if(noEndoso.intValue() == 0){
//				soporteReaseguro = new SoporteReaseguro(UtileriasWeb.regresaBigDecimal(idToCotizacion));
				soporte = new SoporteReaseguroCotizacionDN(idToCotizacionB);
				soporte.notificarEmision(idToPolizaB, noEndoso, fechaEmision);
			}else{
				/*
				 * 15/05/2010 JLAB. Para los casos de endosos de cancelacion y rehabilitaci�n, es probable que no exista el soporte,
				 * ya que �ste es generado como una copia a partir del soporte del endoso anterior.  
				 */
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(usuario.getNombreUsuario()).getPorId(idToCotizacionB);
				if(cotizacionDTO != null){
					if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && (
							cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION ||
							cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION ||
							cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO )){
						EndosoDTO endosoAnterior = EndosoDN.getInstancia(usuario.getNombreUsuario()).obtenerEndoso(idToPolizaB, (short)(noEndoso.intValue() -1));
						EndosoDTO endosoNuevo = EndosoDN.getInstancia(usuario.getNombreUsuario()).obtenerEndoso(idToPolizaB,noEndoso.shortValue());
						SoporteReaseguroCotizacionDN soporteReaseguroEndosoAnterior = new SoporteReaseguroCotizacionDN(endosoAnterior.getIdToCotizacion());
						if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION)
							soporteReaseguroEndosoAnterior.notificarEndosoCancelacion(idToPolizaB, noEndoso.intValue(), cotizacionDTO.getIdToCotizacion(), endosoNuevo.getFechaCreacion(),
									endosoNuevo.getFechaInicioVigencia(), endosoNuevo.getFactorAplicacion(), null, usuario,ignorarSoporteExistente,ignorarSoporteFacultativo);
						else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION)
							soporteReaseguroEndosoAnterior.notificarEndosoRehabilitacion(idToPolizaB, noEndoso.intValue(), cotizacionDTO.getIdToCotizacion(), endosoNuevo.getFechaCreacion(),
									endosoNuevo.getFechaInicioVigencia(), endosoNuevo.getFactorAplicacion(), null, usuario,ignorarSoporteExistente,ignorarSoporteFacultativo);
						else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)
							soporteReaseguroEndosoAnterior.notificarEndosoCambioFormaPago(idToPolizaB, noEndoso.intValue(), cotizacionDTO.getIdToCotizacion(), endosoNuevo.getFechaCreacion(),
									endosoNuevo.getFechaInicioVigencia(), endosoNuevo.getFactorAplicacion(), null, usuario,ignorarSoporteExistente,ignorarSoporteFacultativo);
					}else{
						soporteEndoso = new SoporteReaseguroCotizacionEndosoDN(idToPolizaB, noEndoso.intValue()-1, cotizacionDTO.getIdToCotizacion(),false);
						soporteEndoso.notificarEmision(idToPolizaB, noEndoso.intValue(), fechaEmision);
					}
				}
			}
		}catch (Exception e) {
			UtileriasWeb.registraLogInteraccionReaseguro("ComplementarCotizacionAction.notificarEmision", 
					Sistema.MODULO_REASEGURO, "SoporteReaseguro", "notificarEmision", 
					UtileriasWeb.obtieneNombreUsuario(request), 
					"idToCotizacion = "+ idToCotizacion + " idToPoliza = "+ idToPoliza+ " noEndoso = "+ noEndoso +" fechaCreacionPoliza = "+fechaCreacionPoliza, 
					"void()", e, "error al notificar la emision de una poliza", "no comments", "SoporteReaseguro", Sistema.MODULO_DANIOS);
		}
	}
}
