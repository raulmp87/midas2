package mx.com.afirme.midas.cotizacion.validacion;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CotizacionReaseguroFacultativoForm extends MidasBaseForm{
	private static final long serialVersionUID = -2267836449852581095L;
	private String tipoDistribucion;
	private String descripcionTipoNegocio;
	private String numeroInciso;
	private String numeroSubInciso;
	private String descripcionSeccion;
	private String descripcinoLinea;
	private String descripcionSumaAsegurada;
	private String idToCotizacionFormateada;
	private String idToCotizacion;
	private String fecha;
	private List<CoberturaCotizacionReaseguroFacultativoForm> listaCoberturas;
	private String idLineaSoporteReaseguro;
	private String claveEstatus;
	private String correos = "";
	
	public String getDescripcionTipoNegocio() {
		return descripcionTipoNegocio;
	}
	public void setDescripcionTipoNegocio(String descripcionTipoNegocio) {
		this.descripcionTipoNegocio = descripcionTipoNegocio;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getDescripcinoLinea() {
		return descripcinoLinea;
	}
	public void setDescripcinoLinea(String descripcinoLinea) {
		this.descripcinoLinea = descripcinoLinea;
	}
	public String getDescripcionSumaAsegurada() {
		return descripcionSumaAsegurada;
	}
	public void setDescripcionSumaAsegurada(String descripcionSumaAsegurada) {
		this.descripcionSumaAsegurada = descripcionSumaAsegurada;
	}
	public List<CoberturaCotizacionReaseguroFacultativoForm> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<CoberturaCotizacionReaseguroFacultativoForm> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getIdToCotizacionFormateada() {
		return idToCotizacionFormateada;
	}
	public void setIdToCotizacionFormateada(String idToCotizacionFormateada) {
		this.idToCotizacionFormateada = idToCotizacionFormateada;
	}
	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getTipoDistribucion() {
		return tipoDistribucion;
	}
	public void setTipoDistribucion(String tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}
	public void setIdLineaSoporteReaseguro(String idLineaSoporteReaseguro) {
		this.idLineaSoporteReaseguro = idLineaSoporteReaseguro;
	}
	public String getIdLineaSoporteReaseguro() {
		return idLineaSoporteReaseguro;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public String getCorreos() {
		return correos;
	}
	public void setCorreos(String correos) {
		this.correos = correos;
	}
}
