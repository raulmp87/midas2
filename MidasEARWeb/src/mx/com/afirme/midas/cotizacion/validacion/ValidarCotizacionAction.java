package mx.com.afirme.midas.cotizacion.validacion;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDN;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ValidarCotizacionAction extends MidasMappingDispatchAction {
	
	/**
	 * Method mostrarValidarReaseguroFacultativo
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarValidarReaseguroFacultativo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String idCotizacion = request.getParameter("idToCotizacion");
		CotizacionForm cotizacionForm=(CotizacionForm)form;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		Map<String,List<LineaSoporteReaseguroDTO>> mapaListaCumulos = new HashMap<String, List<LineaSoporteReaseguroDTO>>();
		if (!UtileriasWeb.esCadenaVacia(idCotizacion)){
			BigDecimal idToCotizacion = null;
			try {
				idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			if (idToCotizacion != null){
 			  request.getSession().removeAttribute("idToCotizacion");
			  request.getSession().setAttribute("idToCotizacion", idCotizacion);
  			  CotizacionDTO cotizacionDTO = null;
				try {
					cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(idToCotizacion);
					if(cotizacionDTO != null){
					        //Si el tipo de solicitud corresponde al de un endoso
        					if (cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().equals(Sistema.SOLICITUD_TIPO_ENDOSO)) {
        					    if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()!= Sistema.SOLICITUD_ENDOSO_DE_CANCELACION
        					    		&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()!= Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){
        					        CotizacionDN.getInstancia(nombreUsuario).validarReaseguroFacultativoEndoso(idToCotizacion, nombreUsuario,cotizacionForm,mapaListaCumulos);
        					    }
        					}else{
        					    CotizacionDN.getInstancia(nombreUsuario).validarReaseguroFacultativo(idToCotizacion,nombreUsuario,cotizacionForm,mapaListaCumulos);
        					}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
				request.getSession().removeAttribute("idToCotizacion");
				request.getSession().setAttribute("idToCotizacion", idCotizacion);
				List<LineaSoporteReaseguroDTO> listaLineasPoliza = mapaListaCumulos.get("listaCumuloPoliza");
				List<LineaSoporteReaseguroDTO> listaLineasInciso = mapaListaCumulos.get("listaCumuloInciso");
				List<LineaSoporteReaseguroDTO> listaLineasSubInciso = mapaListaCumulos.get("listaCumuloSubInciso");
				MidasJsonBase jsonCumuloPoliza = this.obtenerJsonListaCumulos(listaLineasPoliza, Sistema.TIPO_CUMULO_POLIZA);
				MidasJsonBase jsonCumuloInciso = this.obtenerJsonListaCumulos(listaLineasInciso, Sistema.TIPO_CUMULO_INCISO);
				MidasJsonBase jsonCumuloSubInciso = this.obtenerJsonListaCumulos(listaLineasSubInciso, Sistema.TIPO_CUMULO_SUBINCISO);
				
				if(jsonCumuloPoliza != null)
					cotizacionForm.setCumuloPoliza(jsonCumuloPoliza.toCSVString("\t"));
				else
					cotizacionForm.setCumuloPoliza("");
				
				if(jsonCumuloInciso != null)
					cotizacionForm.setCumuloInciso(jsonCumuloInciso.toCSVString("\t"));
				else
					cotizacionForm.setCumuloInciso("");
				
				if(jsonCumuloSubInciso != null)
					cotizacionForm.setCumuloSubInciso(jsonCumuloSubInciso.toCSVString("\t"));
				else
					cotizacionForm.setCumuloSubInciso("");
				
				List<LineaSoporteReaseguroDTO> listaLineasTotal = new ArrayList<LineaSoporteReaseguroDTO>();
				if(listaLineasPoliza != null)
					listaLineasTotal.addAll(listaLineasPoliza);
				if(listaLineasInciso != null)
					listaLineasTotal.addAll(listaLineasInciso);
				if(listaLineasSubInciso != null)
					listaLineasTotal.addAll(listaLineasSubInciso);
				
				try{
					actualizarTipoNegocio(cotizacionDTO, listaLineasTotal, nombreUsuario);
				}catch(Exception e){
					LogDeMidasWeb.log("Error al actualizar el tipo de negocio, cotizacion: "+idToCotizacion,Level.WARNING,e);
				}
//				actualizarTipoNegocio
			}
		}
		Runtime.getRuntime().gc();
		return mapping.findForward(Sistema.EXITOSO);
	}
	@SuppressWarnings("unchecked")
	public void obtenerListaCumuloPoliza(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String tipoCumulo = request.getParameter("tipoCumulo");
		int cumulo = Integer.valueOf(tipoCumulo).intValue();
		Object listaTMP = null;
		MidasJsonBase json = null;
		String idCotizacion = (String)request.getSession().getAttribute("idToCotizacion");
		CotizacionDN cotizacionDN= CotizacionDN.getInstancia("TMP");
		TipoNegocioDN tipoNegocioDN= TipoNegocioDN.getInstancia();
		List<TipoNegocioDTO> tiposNegocios=null;
//		TipoNegocioDTO tipoNegocioDTO=null;
		BigDecimal codigoTipoNegocio=null;
		CotizacionDTO cotizacionDTO=null;
		try {
		    cotizacionDTO= cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(idCotizacion));
		    if(cotizacionDTO.getTipoNegocioDTO()!=null){
			codigoTipoNegocio= cotizacionDTO.getTipoNegocioDTO().getCodigoTipoNegocio();
		    }else{
			codigoTipoNegocio= Sistema.CODIGO_TIPO_NEGOCIO_NORMAL;
		    }
		   
		
		} catch (SystemException e1) {
		    e1.printStackTrace();
		}
		
		if (cumulo == 1){
			listaTMP = request.getSession().getAttribute("listaCumuloPoliza");
			request.getSession().removeAttribute("listaCumuloPoliza");
		}
		else if (cumulo==2){//TODO Validar los registros que aplicaron primer riesgo, agregar una "p" en lugar del numero de inciso
			listaTMP = request.getSession().getAttribute("listaCumuloInciso");
			request.getSession().removeAttribute("listaCumuloInciso");
			
		}
		else if (cumulo == 3){
			listaTMP = request.getSession().getAttribute("listaCumuloSubInciso");
			request.getSession().removeAttribute("listaCumuloSubInciso");
		}
		json = new MidasJsonBase();
		if (listaTMP != null){
			List<Object> listaLineaSoporte = (List<Object>) listaTMP;
			
			for(Object object : listaLineaSoporte) {
				try{
					MidasJsonRow row = new MidasJsonRow();
					NumberFormat fMonto = new DecimalFormat("##0.00");
//					LineaSoporteReaseguro lineaSoporte = (LineaSoporteReaseguro)object;
					LineaSoporteReaseguroDTO lineaSoporte = (LineaSoporteReaseguroDTO)object;
					String idLineaSoporte = lineaSoporte.getId().getIdTmLineaSoporteReaseguro().toBigInteger().toString();
					row.setId(idLineaSoporte);
					
					String descripcionSubRamo= "No disponible";
					SubRamoDTO subRamo = new SubRamoDTO();
//					subRamo.setIdTcSubRamo(lineaSoporte.getIdSubramo());
					subRamo.setIdTcSubRamo(lineaSoporte.getLineaDTO().getSubRamo().getIdTcSubRamo());
					subRamo = lineaSoporte.getLineaDTO().getSubRamo();
					if(subRamo == null)
						subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
					if (subRamo != null)
						if (subRamo.getDescripcionSubRamo()!=null)
							descripcionSubRamo = subRamo.getDescripcionSubRamo();
					
					//String sumaAsegurada = "No disponible";
					Double sumaAsegurada = lineaSoporte.getMontoSumaAsegurada().doubleValue();
					
					int estatus = lineaSoporte.getEstatusFacultativo();
					BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
					BigDecimal idLineaSoporteBD = UtileriasWeb.regresaBigDecimal(idLineaSoporte);
					String [] datos = obtenerEstatusLineaSoporteReaseguroDTO(estatus, idLineaSoporteBD, idToCotizacion, cumulo);
					String descripcionEstatus = datos[0];
					String accion1 = datos[1];
					String accion2 = datos[2];
					
					if (cumulo == 1){
						row.setDatos(descripcionSubRamo,fMonto.format(sumaAsegurada),descripcionEstatus,accion1,accion2);
						
					
					}else if (cumulo == 2){
						int numeroInciso=0;
						try{
							numeroInciso = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporte);//lineaSoporte.getNumeroInciso();
						}
						catch(Exception e){}
						row.setDatos(numeroInciso>0?""+numeroInciso:"No disponible",descripcionSubRamo,fMonto.format(sumaAsegurada),descripcionEstatus,accion1,accion2);
//						row.setDatos((numeroInciso!=null)?""+numeroInciso.intValue():"No disponible",descripcionSubRamo,
//								sumaAsegurada,descripcionEstatus,accion1,accion2);
					}
					else if (cumulo == 3){
						int numeroInciso=0;
						try{
							numeroInciso = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporte);//lineaSoporte.getNumeroInciso();
						}
						catch(Exception e){}
						int numeroSubInciso=0;
						try{
							numeroSubInciso = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporte);//lineaSoporte.getNumeroSubInciso();
						}
						catch(Exception e){}
						row.setDatos((numeroInciso>0)?""+numeroInciso:"No disponible",(numeroSubInciso>0)?""+numeroSubInciso:"No disponible",descripcionSubRamo,
								fMonto.format(sumaAsegurada),descripcionEstatus,accion1,accion2);
//						row.setDatos((numeroInciso!=null)?""+numeroInciso.intValue():"No disponible",
//								(numeroSubInciso!=null)?""+numeroSubInciso.intValue():"No disponible",descripcionSubRamo,
//										sumaAsegurada,descripcionEstatus,accion1,accion2);
					}
					json.addRow(row);
					
					//** SE DEFINE EL TIPO DE NEGOCIO
					if(lineaSoporte.getEstatusFacultativo()==LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA || lineaSoporte.getEstatusFacultativo()==LineaSoporteReaseguroDN.ESTATUS_FACULTATIVO_INTEGRADO){//Sistema.FACULTATIVO_INTEGRADO){
					   if(LineaSoporteReaseguroDN.getInstancia().aplicaControlReclamo(lineaSoporte)) {
					       codigoTipoNegocio=Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS;
					   }else{
					       if(codigoTipoNegocio.compareTo(Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS)!=0){
					         codigoTipoNegocio= Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO;
					       }
					   }
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			//** SE MODIFICA---EL TIPO DE NEGOCIO SOBRE LA COTIZACION
			try {
			    tiposNegocios= tipoNegocioDN.buscarPorPropiedad("codigoTipoNegocio", codigoTipoNegocio);
			    
			    if(tiposNegocios!= null & !tiposNegocios.isEmpty()){
				cotizacionDTO.setTipoNegocioDTO(tiposNegocios.get(0));
				cotizacionDN.modificar(cotizacionDTO);
			    }
			    
			} catch (SystemException e1) {
			   
			    e1.printStackTrace();
			}
		}
		response.setContentType("text/json");
		System.out.println("Lista cumulo "+tipoCumulo+": "+json);
		PrintWriter pw;
		try {
			pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	private void actualizarTipoNegocio(CotizacionDTO cotizacionDTO,List<LineaSoporteReaseguroDTO> listaLineasReaseguro,String nombreUsuario) throws SystemException{
		BigDecimal codigoTipoNegocio=null;
		List<TipoNegocioDTO> tiposNegocios=null;
		BigDecimal codigoTipoNegocioOriginal=null;
		try {
		    if(cotizacionDTO.getTipoNegocioDTO()!=null){
		    	codigoTipoNegocio= cotizacionDTO.getTipoNegocioDTO().getCodigoTipoNegocio();
		    }else{
		    	codigoTipoNegocio= Sistema.CODIGO_TIPO_NEGOCIO_NORMAL;
		    }
		} catch (Exception e1) {
		    e1.printStackTrace();
		}
		codigoTipoNegocioOriginal=codigoTipoNegocio;
		//** SE DEFINE EL TIPO DE NEGOCIO
		for(LineaSoporteReaseguroDTO lineaSoporte : listaLineasReaseguro){
			if(lineaSoporte.getEstatusFacultativo()==LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA || lineaSoporte.getEstatusFacultativo()==LineaSoporteReaseguroDN.ESTATUS_FACULTATIVO_INTEGRADO){//Sistema.FACULTATIVO_INTEGRADO){
				   if(LineaSoporteReaseguroDN.getInstancia().aplicaControlReclamo(lineaSoporte)) {
				       codigoTipoNegocio=Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS;
				   }else{
				       if(codigoTipoNegocio.compareTo(Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS)!=0){
				         codigoTipoNegocio= Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO;
				       }
				   }
				}
		}
		
		//** SE MODIFICA---EL TIPO DE NEGOCIO SOBRE LA COTIZACION
		try {
			if(codigoTipoNegocioOriginal == null || codigoTipoNegocioOriginal.compareTo(codigoTipoNegocio) != 0){
			    tiposNegocios= TipoNegocioDN.getInstancia().buscarPorPropiedad("codigoTipoNegocio", codigoTipoNegocio);
			    
			    if(tiposNegocios!= null & !tiposNegocios.isEmpty()){
					cotizacionDTO.setTipoNegocioDTO(tiposNegocios.get(0));
					CotizacionDN.getInstancia(nombreUsuario).modificar(cotizacionDTO);
			    }
			}
		} catch (SystemException e1) {
		   
		    e1.printStackTrace();
		}
		
	}
	
	private MidasJsonBase obtenerJsonListaCumulos(List<LineaSoporteReaseguroDTO> listaLineaSoporte,int tipodistribucion){
		MidasJsonBase json = new MidasJsonBase();
		if (listaLineaSoporte != null){
			
			for(LineaSoporteReaseguroDTO lineaSoporte : listaLineaSoporte) {
				try{
					MidasJsonRow row = new MidasJsonRow();
					NumberFormat fMonto = new DecimalFormat("##0.00");
//					LineaSoporteReaseguro lineaSoporte = (LineaSoporteReaseguro)object;
					String idLineaSoporte = lineaSoporte.getId().getIdTmLineaSoporteReaseguro().toBigInteger().toString();
					row.setId(idLineaSoporte);
					
					String descripcionSubRamo= "No disponible";
					SubRamoDTO subRamo = new SubRamoDTO();
//					subRamo.setIdTcSubRamo(lineaSoporte.getIdSubramo());
					subRamo.setIdTcSubRamo(lineaSoporte.getLineaDTO().getSubRamo().getIdTcSubRamo());
					subRamo = lineaSoporte.getLineaDTO().getSubRamo();
					if(subRamo == null)
						subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
					if (subRamo != null)
						if (subRamo.getDescripcionSubRamo()!=null)
							descripcionSubRamo = subRamo.getDescripcionSubRamo();
					
					//String sumaAsegurada = "No disponible";
					Double sumaAsegurada = lineaSoporte.getMontoSumaAsegurada().doubleValue();
					
					int estatus = lineaSoporte.getEstatusFacultativo();
					BigDecimal idToCotizacion = lineaSoporte.getIdToCotizacion();
					BigDecimal idLineaSoporteBD = lineaSoporte.getId().getIdTmLineaSoporteReaseguro();
					String [] datos = obtenerEstatusLineaSoporteReaseguroDTO(estatus, idLineaSoporteBD, idToCotizacion, tipodistribucion);
					String descripcionEstatus = datos[0];
					String accion1 = datos[1];
					String accion2 = datos[2];
					
					if (tipodistribucion == 1){
						row.setDatos(descripcionSubRamo,fMonto.format(sumaAsegurada),descripcionEstatus,accion1,accion2);
					}else if (tipodistribucion == 2){
						int numeroInciso=0;
						try{
							numeroInciso = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporte);//lineaSoporte.getNumeroInciso();
						}
						catch(Exception e){}
						row.setDatos(numeroInciso>0?""+numeroInciso:"No disponible",descripcionSubRamo,fMonto.format(sumaAsegurada),descripcionEstatus,accion1,accion2);
					}
					else if (tipodistribucion== 3){
						int numeroInciso=0;
						try{
							numeroInciso = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporte);//lineaSoporte.getNumeroInciso();
						}
						catch(Exception e){}
						int numeroSubInciso=0;
						try{
							numeroSubInciso = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporte);//lineaSoporte.getNumeroSubInciso();
						}
						catch(Exception e){}
						row.setDatos((numeroInciso>0)?""+numeroInciso:"No disponible",(numeroSubInciso>0)?""+numeroSubInciso:"No disponible",descripcionSubRamo,
								fMonto.format(sumaAsegurada),descripcionEstatus,accion1,accion2);
					}
					json.addRow(row);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return json;
	}
	
	public void solicitarReaseguroFacultativo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String idLineaSoporteReaseguro = request.getParameter("idLinea");
		String idCotizacion = request.getParameter("idCotizacion");
		String tipoCumulo = request.getParameter("tipoCumulo");
		int cumulo = Integer.valueOf(tipoCumulo).intValue();
		StringBuffer buffer = new StringBuffer();
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		
		if (!UtileriasWeb.esCadenaVacia(idCotizacion) && !UtileriasWeb.esCadenaVacia(idLineaSoporteReaseguro)){
			BigDecimal idToCotizacion = null;
			try {
				idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
			} catch (SystemException e) {}
			BigDecimal idLineaSoporte = null;
			try {
				idLineaSoporte = UtileriasWeb.regresaBigDecimal(idLineaSoporteReaseguro);
			} catch (SystemException e) {}
//			SoporteReaseguro soporteReaseguro = null;
//			try {
//				soporteReaseguro = new SoporteReaseguro(idToCotizacion);
//			} catch (SystemException e) {}
			
			if (idToCotizacion != null /*&& soporteReaseguro != null*/ && idLineaSoporte != null){
//				LineaSoporteReaseguro lineaSoporteReaseguro = null;
//				try {
//					lineaSoporteReaseguro = soporteReaseguro.solicitarFacultarLinea(idLineaSoporte);
//				} catch (ExcepcionDeAccesoADatos e) {
//				} catch (SystemException e) {
//				}
//				if (lineaSoporteReaseguro != null){
				LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
				String descripcionEstatus = "";
				try {
					lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().solicitarFacultarLinea(idLineaSoporte);
					this.marcarCoberturasFacultadas(idLineaSoporte, 1);
				} catch (SystemException e) {
					LogDeMidasWeb.log("Excepcion al solicitar facultar linea "+ idLineaSoporte, Level.SEVERE, e);
					descripcionEstatus = "Error al solicitar facultativo";
				}
				if (lineaSoporteReaseguroDTO != null){
				    try {
				    	SoporteDanosDN soporteDanosDN= SoporteDanosDN.getInstancia();
				    	soporteDanosDN.enviaCorreoNotificacionSolicitudFacultativo(lineaSoporteReaseguroDTO,usuario);
				    } catch (SystemException e1) {
				    	e1.printStackTrace();
				    }
				  	Integer estatus = null;
					estatus = lineaSoporteReaseguroDTO.getEstatusFacultativo();
//					try {
//						estatus = lineaSoporteReaseguro.getEstatusFacultativo();
//					} catch (ExcepcionDeAccesoADatos e) {
//					} catch (SystemException e) {
//					} catch (ExcepcionDeLogicaNegocio e) {
//					}
					
					if(estatus != null){
						String [] datos = obtenerEstatusLineaSoporteReaseguroDTO(estatus, idLineaSoporte, idToCotizacion, cumulo);
						if(descripcionEstatus.equals("")) {
							descripcionEstatus = datos[0];
						}
						String accion1 = datos[1];
						String accion2 = datos[2];
						buffer.append("<estatus><![CDATA["+descripcionEstatus+"]]></estatus>");
						buffer.append("<accion1><![CDATA["+accion1+"]]></accion1>");
						buffer.append("<accion2><![CDATA["+accion2+"]]></accion2>");
					}
				}
			}
		}
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {	}
	}
	
	public void cancelarReaseguroFacultativo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String idLineaSoporteReaseguro = request.getParameter("idLinea");
		String idCotizacion = request.getParameter("idCotizacion");
		String tipoCumulo = request.getParameter("tipoCumulo");
		int cumulo = Integer.valueOf(tipoCumulo).intValue();
		StringBuffer buffer = new StringBuffer();
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		
		if (!UtileriasWeb.esCadenaVacia(idCotizacion) && !UtileriasWeb.esCadenaVacia(idLineaSoporteReaseguro)){
			BigDecimal idToCotizacion = null;
			try {
				idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
			} catch (SystemException e) {}
			BigDecimal idLineaSoporte = null;
			try {
				idLineaSoporte = UtileriasWeb.regresaBigDecimal(idLineaSoporteReaseguro);
			} catch (SystemException e) {}
//			SoporteReaseguro soporteReaseguro = null;
//			try {
//				soporteReaseguro = new SoporteReaseguro(idToCotizacion);
//			} catch (SystemException e) {}
			
			if (idToCotizacion != null /*&& soporteReaseguro != null*/ && idLineaSoporte != null){
//				LineaSoporteReaseguro lineaSoporteReaseguro = null;
				LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
				String descripcionEstatus = "";
				try {
//					lineaSoporteReaseguro = soporteReaseguro.cancelarSolicitudFacultativo(idLineaSoporte);
					lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().cancelarSolicitudFacultativo(idLineaSoporte);
					this.marcarCoberturasFacultadas(idLineaSoporte, 0);
				} catch (SystemException e) {
					LogDeMidasWeb.log("Excepcion al cancelar solicitud facultativo "+ idLineaSoporte, Level.SEVERE, e);
					descripcionEstatus  = "Error al cancelar solicitud facultativo";
				}
				if (lineaSoporteReaseguroDTO != null){
					Integer estatus = null;
					estatus = lineaSoporteReaseguroDTO.getEstatusFacultativo();
					//envio correo
					try {
						SoporteDanosDN soporteDanosDN= SoporteDanosDN.getInstancia();
						soporteDanosDN.enviaCorreoCancelacionSolicitudFacultativo (lineaSoporteReaseguroDTO, usuario);
					} catch (SystemException e1) {
						e1.printStackTrace();
					}
					
//					try {
//						estatus = lineaSoporteReaseguro.getEstatusFacultativo();
//					} catch (ExcepcionDeAccesoADatos e) {
//					} catch (SystemException e) {
//					} catch (ExcepcionDeLogicaNegocio e) {
//					}
					
					if(estatus != null){
						String [] datos = obtenerEstatusLineaSoporteReaseguroDTO(estatus, idLineaSoporte, idToCotizacion, cumulo);
						if(descripcionEstatus.equals("")) {
							descripcionEstatus = datos[0];
						}
						String accion1 = datos[1];
						String accion2 = datos[2];
						buffer.append("<estatus><![CDATA["+descripcionEstatus+"]]></estatus>");
						buffer.append("<accion1><![CDATA["+accion1+"]]></accion1>");
						buffer.append("<accion2><![CDATA["+accion2+"]]></accion2>");
						
						
					}
				}
			}
		}
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {	}
	}
	
	public ActionForward mostrarCotizacionReaseguroFacultativo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		CotizacionReaseguroFacultativoForm cotizacionReaseguroForm = (CotizacionReaseguroFacultativoForm) form;
		String idLineaSoporte = request.getParameter("idLinea");
		String idCotizacion = request.getParameter("idCotizacion");
		String tipoCumulo = request.getParameter("tipoCumulo");
		cotizacionReaseguroForm.setIdToCotizacion(idCotizacion);
		cotizacionReaseguroForm.setIdLineaSoporteReaseguro(idLineaSoporte);
		int cumulo = Integer.valueOf(tipoCumulo).intValue();
		String reglaNavegacion = Sistema.EXITOSO;
		BigDecimal idToCotizacion = null;
		BigDecimal idLineaSoporteReaseguro = null;
		CotizacionDTO cotizacionDTO;
		CotizacionDN cotizacionDN= CotizacionDN.getInstancia(null);
		double factor=1;
		
		try {
			idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
			cotizacionDTO=cotizacionDN.getPorId(idToCotizacion);
			if(cotizacionDTO!= null){
			   factor = UtileriasWeb.getFactorVigencia(cotizacionDTO);
			}
			idLineaSoporteReaseguro = UtileriasWeb.regresaBigDecimal(idLineaSoporte);
		} catch (SystemException e) {}
		
		if (idToCotizacion != null && idLineaSoporteReaseguro != null){
			Format formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionReaseguroForm.setFecha(formatter.format(new Date()));
			cotizacionReaseguroForm.setIdToCotizacionFormateada("COT-"+UtileriasWeb.llenarIzquierda(idToCotizacion.toBigInteger().toString(), "0", 8));
//			SoporteReaseguro soporteReaseguro = null;
//			try {
//				soporteReaseguro = new SoporteReaseguro(idToCotizacion);
//			} catch (SystemException e) {}
//			if(soporteReaseguro != null){
				LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
				try{
					lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().obtenerLineaPorIdTmLineaSoporteReaseguroId(idLineaSoporteReaseguro);// soporteReaseguro.getLineaSoporte(idLineaSoporteReaseguro);
				}catch(Exception e){}
				if (lineaSoporteReaseguroDTO != null){
					//Verificar si aplica control de reclamos para la l�nea
					boolean aplicaControlReclamos = false;
					try {
						aplicaControlReclamos = LineaSoporteReaseguroDN.getInstancia().aplicaControlReclamo(lineaSoporteReaseguroDTO);
					} catch (SystemException e2) {
						
					} //soporteReaseguro.isAplicaControlReclamoLinea(idLineaSoporteReaseguro);
					cotizacionReaseguroForm.setDescripcionTipoNegocio(aplicaControlReclamos?"P�liza facultativa con control de reclamos.":"P�liza Facultativa");
					
					//obtener el subramo para mostrarlo como descripcion de la linea
					SubRamoDTO subRamoDTO = lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo();
					cotizacionReaseguroForm.setDescripcinoLinea(subRamoDTO!=null?subRamoDTO.getDescripcionSubRamo():"No disponible");
					NumberFormat fMonto = Sistema.FORMATO_MONEDA;
					if (lineaSoporteReaseguroDTO.getMontoSumaAsegurada() != null)
						cotizacionReaseguroForm.setDescripcionSumaAsegurada(fMonto.format(lineaSoporteReaseguroDTO.getMontoSumaAsegurada()));
					else
						cotizacionReaseguroForm.setDescripcionSumaAsegurada("No disponible");
					//obtener la lista de coberturas
					List<LineaSoporteCoberturaDTO> listaCoberturas = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs();
					List<CoberturaCotizacionReaseguroFacultativoForm> listaCoberturaCotReaseguro = new ArrayList<CoberturaCotizacionReaseguroFacultativoForm>();
					Map<BigDecimal,CoberturaDTO> mapaCoberturas = new HashMap<BigDecimal, CoberturaDTO>();
					for(LineaSoporteCoberturaDTO lineaSoporteCobertura : listaCoberturas){
						CoberturaCotizacionReaseguroFacultativoForm coberturaCotReaseguroForm = new CoberturaCotizacionReaseguroFacultativoForm();
						if (lineaSoporteCobertura.getIdToCobertura() != null){
							if( !mapaCoberturas.containsKey(lineaSoporteCobertura.getIdToCobertura()) ){
								CoberturaDTO coberturaTMP = null;
								try{
									coberturaTMP = CoberturaDN.getInstancia().getPorId(lineaSoporteCobertura.getIdToCobertura());
									if(coberturaTMP != null)	mapaCoberturas.put(lineaSoporteCobertura.getIdToCobertura(), coberturaTMP);
								}catch(Exception e){}
							}
							if (mapaCoberturas.get(lineaSoporteCobertura.getIdToCobertura()) != null){
								coberturaCotReaseguroForm.setDescripcionCobertura(mapaCoberturas.get(lineaSoporteCobertura.getIdToCobertura()).getNombreComercial());
							} else
								coberturaCotReaseguroForm.setDescripcionCobertura("No disponible");
						}else{
							coberturaCotReaseguroForm.setDescripcionCobertura("No disponible");
						}
						//TODO formato de dos decimales:
						//TODO verificar en qu� casos se muestra como "N/A"
						coberturaCotReaseguroForm.setCoaseguro(""+lineaSoporteCobertura.getPorcentajeCoaseguro()+" %");
						coberturaCotReaseguroForm.setDeducible(""+lineaSoporteCobertura.getPorcentajeDeducible()+" %");
						
						if (lineaSoporteCobertura.getMontoPrimaSuscripcion() != null){
							coberturaCotReaseguroForm.setPrimaNeta(fMonto.format(lineaSoporteCobertura.getMontoPrimaSuscripcion().doubleValue() /factor));
					        coberturaCotReaseguroForm.setPrimaPorContrato(fMonto.format(
					        		LineaSoporteCoberturaDN.getInstancia().getMontoPrimaContratos(lineaSoporteReaseguroDTO, lineaSoporteCobertura)/factor));
						}else{
							coberturaCotReaseguroForm.setPrimaNeta(fMonto.format(0d));
							coberturaCotReaseguroForm.setPrimaPorContrato(fMonto.format(0d));
						 }
						if (lineaSoporteCobertura.getMontoPrimaFacultativo() != null)
							coberturaCotReaseguroForm.setPrimaFacultada(fMonto.format(
									LineaSoporteCoberturaDN.getInstancia().getMontoPrimaFacultada(lineaSoporteReaseguroDTO, lineaSoporteCobertura)/factor));
						else
							coberturaCotReaseguroForm.setPrimaFacultada(fMonto.format(0d));
						listaCoberturaCotReaseguro.add(coberturaCotReaseguroForm);
					}//fin ciclo for
					LineaSoporteCoberturaDTO lineaSoporteCobertura = null;
					if (listaCoberturas != null && !listaCoberturas.isEmpty())
						lineaSoporteCobertura = listaCoberturas.get(0);
					cotizacionReaseguroForm.setListaCoberturas(listaCoberturaCotReaseguro);
					switch(cumulo){
						case 1:
							cotizacionReaseguroForm.setTipoDistribucion("1");
							break;
						case 2:
							cotizacionReaseguroForm.setTipoDistribucion("2");
							try {
								cotizacionReaseguroForm.setNumeroInciso(""+LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteReaseguroDTO));
							} catch (Exception e1) {
								cotizacionReaseguroForm.setNumeroInciso("No disponible");
							}
							break;
						case 3:
							cotizacionReaseguroForm.setTipoDistribucion("3");
							try {
								cotizacionReaseguroForm.setNumeroInciso(""+LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteReaseguroDTO));
							} catch (Exception e1) {
								cotizacionReaseguroForm.setNumeroInciso("No disponible");
							}
							try {
								cotizacionReaseguroForm.setNumeroSubInciso(""+LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporteReaseguroDTO));
							} catch (Exception e1) {
								cotizacionReaseguroForm.setNumeroSubInciso("No disponible");
							}
							if (lineaSoporteCobertura != null){
								if (lineaSoporteCobertura.getIdToSeccion() != null){
									SeccionDTO seccionDTO = null;
									try {
										seccionDTO = SeccionDN.getInstancia().getPorId(lineaSoporteCobertura.getIdToSeccion());
									} catch (ExcepcionDeAccesoADatos e) {
									} catch (SystemException e) {
									}
									if (seccionDTO != null){
										cotizacionReaseguroForm.setDescripcionSeccion(seccionDTO.getNombreComercial());
									} else{
										cotizacionReaseguroForm.setDescripcionSeccion("No disponible");
									}
								}
							}
							break;
					}
				}//fin validar lineaSoporteReaseguro != null
//			}//fin validar soporteReaseguro != null
		}//fin validar idcotizacion != null idLinea != null
		if (cotizacionReaseguroForm.getListaCoberturas() == null){
			cotizacionReaseguroForm.setListaCoberturas(new ArrayList<CoberturaCotizacionReaseguroFacultativoForm>());
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private String[] obtenerEstatusLineaSoporteReaseguroDTO(Integer estatus,BigDecimal idLineaSoporte,BigDecimal idCotizacion,int cumulo){
		String descripcionEstatus = "No disponible";
		String accion1 = UtileriasWeb.generarLineaImagenDataGrid(null, null, null, null);
		String accion2 = UtileriasWeb.generarLineaImagenDataGrid(null, null, null, null);
		if (estatus != null){
			switch(estatus){
				case LineaSoporteReaseguroDN.ESTATUS_SOPORTADO_POR_REASEGURO:
					descripcionEstatus = "Soportado por Reaseguro";
					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_MODIFICAR, "Solicitar facultativo", "solicitarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_REQUIERE_FACULTATIVO:
					descripcionEstatus = "Requiere Facultativo";
					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_MODIFICAR, "Solicitar facultativo", "solicitarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_FACULTTIVO_SOLICITADO:
					descripcionEstatus = "Facultativo Solicitado";
					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					SlipDTO slipDTO = null;
					try{
						slipDTO = SlipDN.getInstancia().buscarPorIdTmLineaSoporteReaseguro(idLineaSoporte);
					}catch(Exception e){}
					if( slipDTO != null)
						accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_MODIFICAR, "Editar Slip", "mostrarSlipGeneral("+slipDTO.getIdToSlip()+",0,-1)", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA:
					descripcionEstatus = "Facultativo Autorizado";
					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_DETALLE, "Ver Cotizacion de Reaseguro Facultativo", "mostrarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				/*case LineaSoporteReaseguroDN.ESTATUS_COTIZACION_FAC_AUTORIZADA_POR_RETENCION:
					descripcionEstatus = "Retencion autorizada";
					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_DETALLE, "Ver Cotizacion de Reaseguro Facultativo", "mostrarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;*/
				case LineaSoporteReaseguroDN.ESTATUS_FACULTATIVO_INTEGRADO:
					descripcionEstatus = "Facultativo Integrado";
//					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_DETALLE, "Ver Cotizaci&oacute;n de Reaseguro Facultativo", null, null);
					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_LIBERADA:
					descripcionEstatus = "Liberada";
					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_AUTORIZADA_EMISION:
					descripcionEstatus = "En contrato facultativo";
//					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_CANCELACION_FAC_SOLICITADA:
					descripcionEstatus = "Cancelaci&oacute;n en proceso";
//					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_EMITIDA:
					descripcionEstatus = "Emitida";
					accion2 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_BORRAR, "Cancelar facultativo", "cancelarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				case LineaSoporteReaseguroDN.ESTATUS_LINEA_ENDOSO_FACULTATIVO:
					descripcionEstatus = "Endoso de contrato facultativo";
					accion1 = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_MODIFICAR, "Solicitar endoso facultativo", "solicitarReaseguroFacultativo("+idLineaSoporte+","+idCotizacion+","+cumulo+")", null);
					break;
				default:
					descripcionEstatus = "No disponible";
					accion1 = "";
					break;
			}
		}
		String []arrayString = new String[3];
		arrayString[0] = descripcionEstatus;
		arrayString[1] = accion1;
		arrayString[2] = accion2;
		return arrayString;
	}

	public ActionForward aceptarFacultativo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		CotizacionReaseguroFacultativoForm cotizacionReaseguroFacultativoForm = (CotizacionReaseguroFacultativoForm) form;
		String idCotizacion = cotizacionReaseguroFacultativoForm.getIdToCotizacion();
		String idLineaSoporteReaseguro = cotizacionReaseguroFacultativoForm.getIdLineaSoporteReaseguro();
		BigDecimal idToCotizacion = null;
		BigDecimal idLineaReaseguro = null;
		double factor=-1;
		CotizacionDN cotizacionDN=null;
		CotizacionDTO cotizacionDTO=null;
		BigDecimal codigoTipoNegocio=null;
		TipoNegocioDN tipoNegocioDN= TipoNegocioDN.getInstancia();
		List<TipoNegocioDTO> tiposNegocios=null;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		try {
			idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
			idLineaReaseguro = UtileriasWeb.regresaBigDecimal(idLineaSoporteReaseguro);
//			SoporteReaseguro soporteReaseguro = null;
			try {
        		cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
        		cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
				if (cotizacionDTO != null) {
					factor = UtileriasWeb.getFactorVigencia(cotizacionDTO);
					if (cotizacionDTO.getTipoNegocioDTO() != null) {
						codigoTipoNegocio = cotizacionDTO.getTipoNegocioDTO().getCodigoTipoNegocio();
					} else {
						codigoTipoNegocio = Sistema.CODIGO_TIPO_NEGOCIO_NORMAL;
					}
					/* DES-DAN-0172 */
					if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null
							&& cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0) {
						request.setAttribute("esEndoso", "true");
					}
					cotizacionReaseguroFacultativoForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
				}
//				soporteReaseguro = new SoporteReaseguro(idToCotizacion);
			} catch (SystemException e) {}
			
			if(factor > 0) {
				LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().obtenerLineaPorIdTmLineaSoporteReaseguroId(idLineaReaseguro);
				List<LineaSoporteCoberturaDTO> lineaSoporteCoberturas = LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(idLineaReaseguro);
				for(LineaSoporteCoberturaDTO cobertura : lineaSoporteCoberturas) {
					List<CoberturaCotizacionDTO> coberturasCotizacion = new ArrayList<CoberturaCotizacionDTO>();
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(lineaSoporteReaseguroDTO.getIdToCotizacion());
					id.setNumeroInciso(cobertura.getNumeroInciso());
					id.setIdToSeccion(cobertura.getIdToSeccion());
					id.setIdToCobertura(cobertura.getIdToCobertura());
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO = CoberturaCotizacionDN.getInstancia().getPorId(coberturaCotizacionDTO);
					coberturasCotizacion.add(coberturaCotizacionDTO);
					CotizacionDN.getInstancia(nombreUsuario).igualarPrimasFacultadas(coberturasCotizacion, cobertura.getMontoPrimaFacultativo().doubleValue() /factor, nombreUsuario);
				}
				List<String> errores = new ArrayList<String>();
				try {
				    if(LineaSoporteReaseguroDN.getInstancia().integrarCotizacionFacultativa(lineaSoporteReaseguroDTO, lineaSoporteCoberturas,errores)){
				    	if(LineaSoporteReaseguroDN.getInstancia().aplicaControlReclamo(lineaSoporteReaseguroDTO)) {
				    		codigoTipoNegocio=Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS;
				    	}else{
				    		if(codigoTipoNegocio.compareTo(Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS)!=0){
				    			codigoTipoNegocio= Sistema.CODIGO_TIPO_NEGOCIO_FACULTATIVO;
				    		}
				    	}
				    }
				    else{
				    	String error = new String();
				    	if(errores != null && !errores.isEmpty()){
				    		error= obtenerError(errores);		
				    	}
				    	cotizacionReaseguroFacultativoForm.setMensaje("Ocurrio un error al integrar el facultativo.<br>"+error);
						cotizacionReaseguroFacultativoForm.setTipoMensaje(Sistema.ERROR);
				    }
				} catch (ExcepcionDeAccesoADatos e) {
					cotizacionReaseguroFacultativoForm.setMensaje("Ocurrio un error al integrar el facultativo.<br>No se pudo calcular el factor de vigencia de la cotizaci&oacute;n.");
					cotizacionReaseguroFacultativoForm.setTipoMensaje(Sistema.ERROR);
				}
				
				tiposNegocios= tipoNegocioDN.buscarPorPropiedad("codigoTipoNegocio", codigoTipoNegocio);
				if(tiposNegocios!= null & !tiposNegocios.isEmpty()){
				    cotizacionDTO.setTipoNegocioDTO(tiposNegocios.get(0));
				    cotizacionDN.modificar(cotizacionDTO);
				}
				cotizacionReaseguroFacultativoForm.setMensaje("Facultativo Integrado");
				cotizacionReaseguroFacultativoForm.setTipoMensaje(Sistema.EXITO);
			} else {
				cotizacionReaseguroFacultativoForm.setMensaje("Ocurrio un error al integrar el facultativo.<br>No se pudo calcular el factor de vigencia de la cotizaci&oacute;n.");
				cotizacionReaseguroFacultativoForm.setTipoMensaje(Sistema.ERROR);
			}
		} catch (SystemException e) {
			cotizacionReaseguroFacultativoForm.setMensaje("Ocurrio un error al integrar el facultativo");
			cotizacionReaseguroFacultativoForm.setTipoMensaje(Sistema.ERROR);
			e.printStackTrace();
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	
//	public void validarReaseguroFacultativo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
//		String idCotizacion = request.getParameter("idToCotizacion");
//		if (!UtileriasWeb.esCadenaVacia(idCotizacion)){
//			BigDecimal idToCotizacion = null;
//			try {
//				idToCotizacion = UtileriasWeb.regresaBigDecimal(idCotizacion);
//			} catch (SystemException e) {
//				e.printStackTrace();
//			}
//			if (idToCotizacion != null){
//  			  CotizacionDTO cotizacionDTO = null;
//				try {
//					cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(idToCotizacion);
//					if(cotizacionDTO != null){
//					        //Si el tipo de solicitud corresponde al de un endoso
//        					if (cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().equals(Sistema.SOLICITUD_TIPO_ENDOSO)) {
//        					    if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
////        						CotizacionDN.getInstancia(null).validarReaseguroFacultativoEndosoCancelacion(idToCotizacion,UtileriasWeb.obtieneNombreUsuario(request));
//        					    }else{
////        					        CotizacionDN.getInstancia(null).validarReaseguroFacultativoEndoso(idToCotizacion, request,cotizacionForm);
//        					    }
//        					}else{
//        					    CotizacionDN.getInstancia(null).validarReaseguroFacultativo(cotizacionDTO,false,false);
//        					}
//					}
//				} catch (SystemException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		else{
//			try {
//				List<CotizacionDTO> listaCotizaciones = new CotizacionSN("ADMIN").listarTodos();
//				if(listaCotizaciones != null && !listaCotizaciones.isEmpty()){
//					for(CotizacionDTO cotizacionDTO : listaCotizaciones){
//						if (cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().equals(Sistema.SOLICITUD_TIPO_ENDOSO)) {
//    					    if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
//    					    	LogDeMidasWeb.log("Se omiti� la validaci�n de Reaseguro facultativo de la cotizacion"+cotizacionDTO.getIdToCotizacion()+". ENDOSO DE CANCELACION", Level.INFO, null);
////    						CotizacionDN.getInstancia(null).validarReaseguroFacultativoEndosoCancelacion(idToCotizacion,UtileriasWeb.obtieneNombreUsuario(request));
//    					    }else{
////    					        CotizacionDN.getInstancia(null).validarReaseguroFacultativoEndoso(idToCotizacion, request,cotizacionForm);
//    					    	LogDeMidasWeb.log("Se omiti� la validaci�n de Reaseguro facultativo de la cotizacion"+cotizacionDTO.getIdToCotizacion()+". ENDOSO DE MODIFICACION/REHABILITACION", Level.INFO, null);
//    					    }
//    					}else{
//    					    CotizacionDN.getInstancia(null).validarReaseguroFacultativo(cotizacionDTO,false,false);
//    					}
//					}
//				}
//			} catch (ExcepcionDeAccesoADatos e) {
//				e.printStackTrace();
//			} catch (SystemException e) {
//				e.printStackTrace();
//			}
//		}
//	}

	public String obtenerError(List<String> errores) {
		StringBuilder error= new StringBuilder("");
		for(String errorTMP : errores){
			error.append(errorTMP).append("<br>");
		}
		return error.toString();
	}
	private void marcarCoberturasFacultadas(BigDecimal idLineaSoporteReaseguro, int claveFacultativo)
			throws SystemException {
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
		lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().obtenerLineaPorIdTmLineaSoporteReaseguroId(idLineaSoporteReaseguro);
		List<LineaSoporteCoberturaDTO> listaCoberturas = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs();
		BigDecimal idToCotizacion = lineaSoporteReaseguroDTO.getIdToCotizacion();
		for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO: listaCoberturas) {
			BigDecimal numeroInciso = lineaSoporteCoberturaDTO.getNumeroInciso();
			BigDecimal idToSeccion = lineaSoporteCoberturaDTO.getIdToSeccion();
			BigDecimal idToCobertura = lineaSoporteCoberturaDTO.getIdToCobertura();
			@SuppressWarnings("unused")
			BigDecimal numeroSubInciso = lineaSoporteCoberturaDTO.getNumeroSubInciso();
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setIdToCobertura(idToCobertura);
			coberturaCotizacionDTO.setId(id);
			CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
			coberturaCotizacionDTO = coberturaCotizacionDN.getPorId(coberturaCotizacionDTO);
			if(coberturaCotizacionDTO != null){
				coberturaCotizacionDTO.setClaveFacultativo((short) claveFacultativo);
				coberturaCotizacionDN.modificar(coberturaCotizacionDTO);
			}
		}
	}
}