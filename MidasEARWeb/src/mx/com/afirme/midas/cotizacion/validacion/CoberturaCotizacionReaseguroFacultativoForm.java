package mx.com.afirme.midas.cotizacion.validacion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CoberturaCotizacionReaseguroFacultativoForm extends MidasBaseForm{
	private static final long serialVersionUID = 8777997376735823170L;
	private String descripcionCobertura;
	private String primaNeta;
	private String primaPorContrato;
	private String primaFacultada;
	private String coaseguro;
	private String deducible;
	
	
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}
	public String getPrimaPorContrato() {
		return primaPorContrato;
	}
	public void setPrimaPorContrato(String primaPorContrato) {
		this.primaPorContrato = primaPorContrato;
	}
	public String getPrimaFacultada() {
		return primaFacultada;
	}
	public void setPrimaFacultada(String primaFacultada) {
		this.primaFacultada = primaFacultada;
	}
	public String getCoaseguro() {
		return coaseguro;
	}
	public void setCoaseguro(String coaseguro) {
		this.coaseguro = coaseguro;
	}
	public String getDeducible() {
		return deducible;
	}
	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}
}
