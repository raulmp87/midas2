package mx.com.afirme.midas.cotizacion.riesgo.detalleprima;

import java.util.List;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetallePrimaRiesgoCotizacionDN {
	private static final DetallePrimaRiesgoCotizacionDN INSTANCIA = new DetallePrimaRiesgoCotizacionDN();

	public static DetallePrimaRiesgoCotizacionDN getInstancia (){
		return DetallePrimaRiesgoCotizacionDN.INSTANCIA;
	}
	
	public void agregar(DetallePrimaRiesgoCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DetallePrimaRiesgoCotizacionSN().agregar(recargoRiesgoCotizacionDTO);
	}
	
	public void borrar (DetallePrimaRiesgoCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DetallePrimaRiesgoCotizacionSN().borrar(recargoRiesgoCotizacionDTO);
	}
	
	public DetallePrimaRiesgoCotizacionDTO modificar (DetallePrimaRiesgoCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaRiesgoCotizacionSN().modificar(recargoRiesgoCotizacionDTO);
	}
	
	public DetallePrimaRiesgoCotizacionDTO getPorId(DetallePrimaRiesgoCotizacionId recargoRiesgoCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaRiesgoCotizacionSN().getPorId(recargoRiesgoCotizacionId);
	}
	
	public List<DetallePrimaRiesgoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaRiesgoCotizacionSN().listarTodos();
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades RecargoRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<RecargoRiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */
	public List<DetallePrimaRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaRiesgoCotizacionSN().findByRiesgoCotizacion(riesgoCotId);
	}

	public List<DetallePrimaRiesgoCotizacionDTO> listarFiltrado(DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DetallePrimaRiesgoCotizacionSN().listarFiltrado(detallePrimaRiesgoCotizacionId);
	}

	public void eliminarDetallePrimaRiesgoCotizacion(RiesgoCotizacionDTO riesgoCotizacionDTO) throws SystemException {
		new DetallePrimaRiesgoCotizacionSN().eliminarDetallePrimaRiesgoCotizacion(riesgoCotizacionDTO.getId());
	}
	public void borrarFiltrado(DetallePrimaRiesgoCotizacionId id) throws SystemException {
	new DetallePrimaRiesgoCotizacionSN().borrarFiltrado(id);
}

	public List<DetallePrimaRiesgoCotizacionDTO> listarPorSubInciso(
			SubIncisoCotizacionDTOId id) throws SystemException {
		return new DetallePrimaRiesgoCotizacionSN().listarPorSubInciso(id);
	}
}
