package mx.com.afirme.midas.cotizacion.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoCotizacionDN {
	public static final RiesgoCotizacionDN INSTANCIA = new RiesgoCotizacionDN();

	public static RiesgoCotizacionDN getInstancia (){
		return RiesgoCotizacionDN.INSTANCIA;
	}
	/**
	 * Crea un listado de los descuentos de Cobertura,por producto, 
	 * por tipo Poliza y por riesgo
	 * @param riesgoCotizacionDTO
	 * @param descuentosCobertura
	 * @param descuentosTipoPoliza
	 * @param descuentosProducto
	 * @return List<DescuentoRiesgoCotizacionDTO>
	 */
	public List<DescuentoRiesgoCotizacionDTO> getDescuentosRiesgoCotizacion(
			RiesgoCotizacionDTO riesgoCotizacionDTO, List<DescuentoVarioCoberturaDTO> descuentosCobertura,
			List<DescuentoVarioTipoPolizaDTO> descuentosTipoPoliza,
			List<DescuentoVarioProductoDTO> descuentosProducto) {

		List<DescuentoRiesgoCotizacionDTO> descuentos = new ArrayList<DescuentoRiesgoCotizacionDTO>();

		for(DescuentoVarioProductoDTO descuento : descuentosProducto) {
			DescuentoRiesgoCotizacionId id = new DescuentoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToDescuentoVario(descuento.getId().getIdToDescuentoVario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
			descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento.getDescuentoDTO());
			descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			descuentoRiesgoCotizacionDTO.setValorDescuento(descuento.getValor());
			descuentoRiesgoCotizacionDTO.setClaveObligatoriedad(Short.parseShort(descuento.getClaveObligatoriedad()));
			if(descuento.getClaveObligatoriedad().equals("1") || descuento.getClaveObligatoriedad().equals("3")) {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(Short.parseShort(descuento.getClaveComercialTecnico()));
			descuentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_PRODUCTO);
			descuentoRiesgoCotizacionDTO.setId(id);
			descuentos.add(descuentoRiesgoCotizacionDTO);
		}

		for(DescuentoVarioTipoPolizaDTO descuento : descuentosTipoPoliza) {
			DescuentoRiesgoCotizacionId id = new DescuentoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToDescuentoVario(descuento.getId().getIdtodescuentovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
			descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento.getDescuentoDTO());
			descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			descuentoRiesgoCotizacionDTO.setValorDescuento(descuento.getValor());
			descuentoRiesgoCotizacionDTO.setClaveObligatoriedad(descuento.getClaveobligatoriedad());
			if(descuento.getClaveobligatoriedad().shortValue() == 1 || descuento.getClaveobligatoriedad().shortValue() == 3) {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(descuento.getClavecomercialtecnico());
			descuentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_TIPO_POLIZA);
			descuentoRiesgoCotizacionDTO.setId(id);
			descuentos.add(descuentoRiesgoCotizacionDTO);
		}

		for(DescuentoVarioCoberturaDTO descuento : descuentosCobertura) {
			DescuentoRiesgoCotizacionId id = new DescuentoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToDescuentoVario(descuento.getId().getIdtodescuentovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO = new DescuentoRiesgoCotizacionDTO();
			descuentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			descuentoRiesgoCotizacionDTO.setDescuentoDTO(descuento.getDescuentoDTO());
			descuentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			descuentoRiesgoCotizacionDTO.setValorDescuento(descuento.getValor());
			descuentoRiesgoCotizacionDTO.setClaveObligatoriedad(descuento.getClaveobligatoriedad());
			if(descuento.getClaveobligatoriedad().shortValue() == 1 || descuento.getClaveobligatoriedad().shortValue() == 3) {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				descuentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			descuentoRiesgoCotizacionDTO.setClaveComercialTecnico(descuento.getClavecomercialtecnico());
			descuentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_COBERTURA);
			descuentoRiesgoCotizacionDTO.setId(id);
			descuentos.add(descuentoRiesgoCotizacionDTO);
		}
		return descuentos;
	}
	/**Crea un listado de los aumentos de Cobertura,por producto, 
	 * por tipo Poliza y por riesgo
	 * @param riesgoCotizacionDTO
	 * @param aumentosCobertura
	 * @param aumentosTipoPoliza
	 * @param aumentosProducto
	 * @return
	 */

	public List<AumentoRiesgoCotizacionDTO> getAumentosRiesgoCotizacion(
			RiesgoCotizacionDTO riesgoCotizacionDTO,
			List<AumentoVarioCoberturaDTO> aumentosCobertura,
			List<AumentoVarioTipoPolizaDTO> aumentosTipoPoliza,
			List<AumentoVarioProductoDTO> aumentosProducto) {

		List<AumentoRiesgoCotizacionDTO> aumentos = new ArrayList<AumentoRiesgoCotizacionDTO>();

		for(AumentoVarioProductoDTO aumento : aumentosProducto) {
			AumentoRiesgoCotizacionId id = new AumentoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToAumentoVario(aumento.getId().getIdtoaumentovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO = new AumentoRiesgoCotizacionDTO();
			aumentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			aumentoRiesgoCotizacionDTO.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
			aumentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			aumentoRiesgoCotizacionDTO.setValorAumento(aumento.getValor());
			aumentoRiesgoCotizacionDTO.setClaveObligatoriedad(aumento.getClaveobligatoriedad());
			if(aumento.getClaveobligatoriedad().shortValue() == 1 || aumento.getClaveobligatoriedad().shortValue() == 3) {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			aumentoRiesgoCotizacionDTO.setClaveComercialTecnico(aumento.getClavecomercialtecnico());
			aumentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_PRODUCTO);
			//aumentoRiesgoCotizacionDTO.setClaveTipo((short)1);
			aumentoRiesgoCotizacionDTO.setId(id);
			aumentos.add(aumentoRiesgoCotizacionDTO);
		}

		for(AumentoVarioTipoPolizaDTO aumento : aumentosTipoPoliza) {
			AumentoRiesgoCotizacionId id = new AumentoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToAumentoVario(aumento.getId().getIdtoaumentovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO = new AumentoRiesgoCotizacionDTO();
			aumentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			aumentoRiesgoCotizacionDTO.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
			aumentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			aumentoRiesgoCotizacionDTO.setValorAumento(aumento.getValor());
			aumentoRiesgoCotizacionDTO.setClaveObligatoriedad(aumento.getClaveobligatoriedad());
			if(aumento.getClaveobligatoriedad().shortValue() == 1 || aumento.getClaveobligatoriedad().shortValue() == 3) {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			aumentoRiesgoCotizacionDTO.setClaveComercialTecnico(aumento.getClavecomercialtecnico());
			aumentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_TIPO_POLIZA);
			//aumentoRiesgoCotizacionDTO.setClaveTipo((short)2);
			aumentoRiesgoCotizacionDTO.setId(id);
			aumentos.add(aumentoRiesgoCotizacionDTO);
		}

		for(AumentoVarioCoberturaDTO aumento : aumentosCobertura) {
			AumentoRiesgoCotizacionId id = new AumentoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToAumentoVario(aumento.getId().getIdtoaumentovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO = new AumentoRiesgoCotizacionDTO();
			aumentoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			aumentoRiesgoCotizacionDTO.setAumentoVarioDTO(aumento.getAumentoVarioDTO());
			aumentoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			aumentoRiesgoCotizacionDTO.setValorAumento(aumento.getValor());
			aumentoRiesgoCotizacionDTO.setClaveObligatoriedad(aumento.getClaveobligatoriedad());
			if(aumento.getClaveobligatoriedad().shortValue() == 1 || aumento.getClaveobligatoriedad().shortValue() == 3) {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				aumentoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			aumentoRiesgoCotizacionDTO.setClaveComercialTecnico(aumento.getClavecomercialtecnico());
			aumentoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_COBERTURA);
			//aumentoRiesgoCotizacionDTO.setClaveTipo((short)3);
			aumentoRiesgoCotizacionDTO.setId(id);
			aumentos.add(aumentoRiesgoCotizacionDTO);
		}
		return aumentos;
	}
	/**Crea un listado de los recargos de Cobertura,por producto, 
	 * por tipo Poliza y por riesgo
	 * @param riesgoCotizacionDTO
	 * @param recargosCobertura
	 * @param recargosTipoPoliza
	 * @param recargosProducto
	 * @return
	 */
	public List<RecargoRiesgoCotizacionDTO> getRecargosRiesgoCotizacion(
			RiesgoCotizacionDTO riesgoCotizacionDTO,
			List<RecargoVarioCoberturaDTO> recargosCobertura,
			List<RecargoVarioTipoPolizaDTO> recargosTipoPoliza,
			List<RecargoVarioProductoDTO> recargosProducto) {

		List<RecargoRiesgoCotizacionDTO> recargos = new ArrayList<RecargoRiesgoCotizacionDTO>();

		for(RecargoVarioProductoDTO recargo : recargosProducto) {
			RecargoRiesgoCotizacionId id = new RecargoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToRecargoVario(recargo.getId().getIdToRecargoVario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
			recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
			recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			recargoRiesgoCotizacionDTO.setValorRecargo(recargo.getValor());
			recargoRiesgoCotizacionDTO.setClaveObligatoriedad(Short.parseShort(recargo.getClaveObligatoriedad()));
			if(recargo.getClaveObligatoriedad().equals("1") || recargo.getClaveObligatoriedad().equals("3")) {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			recargoRiesgoCotizacionDTO.setClaveComercialTecnico(Short.parseShort(recargo.getClaveComercialTecnico()));
			recargoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_PRODUCTO);
			recargoRiesgoCotizacionDTO.setId(id);
			recargos.add(recargoRiesgoCotizacionDTO);
		}

		for(RecargoVarioTipoPolizaDTO recargo : recargosTipoPoliza) {
			RecargoRiesgoCotizacionId id = new RecargoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToRecargoVario(recargo.getId().getIdtorecargovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
			recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
			recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			recargoRiesgoCotizacionDTO.setValorRecargo(recargo.getValor());
			recargoRiesgoCotizacionDTO.setClaveObligatoriedad(recargo.getClaveobligatoriedad());
			if(recargo.getClaveobligatoriedad().shortValue() == 1 || recargo.getClaveobligatoriedad().shortValue() == 3) {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			recargoRiesgoCotizacionDTO.setClaveComercialTecnico(recargo.getClavecomercialtecnico());
			recargoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_TIPO_POLIZA);
			recargoRiesgoCotizacionDTO.setId(id);
			recargos.add(recargoRiesgoCotizacionDTO);
		}

		for(RecargoVarioCoberturaDTO recargo : recargosCobertura) {
			RecargoRiesgoCotizacionId id = new RecargoRiesgoCotizacionId();
			id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
			id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
			id.setIdToRecargoVario(recargo.getId().getIdtorecargovario());
			id.setIdToRiesgo(riesgoCotizacionDTO.getId().getIdToRiesgo());
			id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
			id.setNumeroInciso(riesgoCotizacionDTO.getId().getNumeroInciso());

			RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO = new RecargoRiesgoCotizacionDTO();
			recargoRiesgoCotizacionDTO.setRiesgoCotizacionDTO(riesgoCotizacionDTO);
			recargoRiesgoCotizacionDTO.setRecargoVarioDTO(recargo.getRecargoVarioDTO());
			recargoRiesgoCotizacionDTO.setClaveAutorizacion((short)0);
			recargoRiesgoCotizacionDTO.setValorRecargo(recargo.getValor());
			recargoRiesgoCotizacionDTO.setClaveObligatoriedad(recargo.getClaveobligatoriedad());
			if(recargo.getClaveobligatoriedad().shortValue() == 1 || recargo.getClaveobligatoriedad().shortValue() == 3) {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)1);
			} else {
				recargoRiesgoCotizacionDTO.setClaveContrato((short)0);
			}
			//Campos relacionados con el nivel del Aumento
			recargoRiesgoCotizacionDTO.setClaveComercialTecnico(recargo.getClavecomercialtecnico());
			recargoRiesgoCotizacionDTO.setClaveNivel(Sistema.NIVEL_COBERTURA);
			recargoRiesgoCotizacionDTO.setId(id);
			recargos.add(recargoRiesgoCotizacionDTO);
		}
		return recargos;
	}
	/**
	 * Busca los riesgos a partir de un filtro en <code>riesgoCotizacionDTO</code>
	 * @param riesgoCotizacionDTO
	 * @return List<RiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarFiltrado(RiesgoCotizacionDTO riesgoCotizacionDTO) throws SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN.listarFiltrado(riesgoCotizacionDTO);
	}
	/**
	 * Method modificar()
	 * @param riesgoCotizacionDTO
	 * @return List<RiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> modificar(RiesgoCotizacionDTO riesgoCotizacionDTO) throws SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		riesgoCotizacionDTO = riesgoCotizacionSN.modificar(riesgoCotizacionDTO);

		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(riesgoCotizacionDTO.getId().getIdToCotizacion());
		id.setIdToSeccion(riesgoCotizacionDTO.getId().getIdToSeccion());
		id.setIdToCobertura(riesgoCotizacionDTO.getId().getIdToCobertura());
		RiesgoCotizacionDTO riesgo = new RiesgoCotizacionDTO();
		riesgo.setId(id);
		return riesgoCotizacionSN.listarFiltrado(riesgo );
	}
	/**
	 * Method listarPorIdFiltrado().
	 * @param id
	 * @return List<RiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarPorIdFiltrado(RiesgoCotizacionId id) throws SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN.listarPorIdFiltrado(id);
	}
	/**
	 * Method listarRiesgoContratados().
	 * @param id
	 * @return List<RiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgoContratados(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos,
			SystemException{
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN.listarRiesgoContratados(id);
	}
	
	/**
	 * Method listarRiesgoContratadosPorCobertura().
	 * @param id
	 * @return List<RiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorCobertura(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos,
			SystemException{
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN.listarRiesgoContratadosPorCobertura(id);
	}
	/**
	 * Method listarRiesgoContratadosPorSeccion().
	 * @param id
	 * @return List<RiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorSeccion(
			SeccionCotizacionDTOId id) throws ExcepcionDeAccesoADatos,
			SystemException{
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN.listarRiesgoContratadosPorSeccion(id);
	}
	/**
	 * Method borrar().
	 * @param riesgoCotizacionDTO
	 * @throws SystemException
	 */
	public void borrar(RiesgoCotizacionDTO riesgoCotizacionDTO) throws SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		riesgoCotizacionSN.borrar(riesgoCotizacionDTO);
	}
	/**
	 * Method listarRiesgosContratadosPorCotizacion()
	 * @param idToCotizacion
	 * @return List<RiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgosContratadosPorCotizacion(BigDecimal idToCotizacion) throws SystemException{
		return new RiesgoCotizacionSN().listarRiesgosContratadosPorCotizacion(idToCotizacion);
	}
	/**
	 * Method getPorId().
	 * @param riesgoCotizacionid
	 * @return RiesgoCotizacionDTO
	 * @throws SystemException
	 */
	public RiesgoCotizacionDTO getPorId(RiesgoCotizacionId riesgoCotizacionid) throws SystemException {
		RiesgoCotizacionSN riesgoCotizacionSN = new RiesgoCotizacionSN();
		return riesgoCotizacionSN.getPorId(riesgoCotizacionid);
	}
	
	/**
	 * Obtiene el total de riesgos contratados por cobertura
	 * @param id
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public Long obtenerTotalRiesgoContratadosPorCobertura(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos,
			SystemException{
		return new RiesgoCotizacionSN().obtenerTotalRiesgoContratadosPorCobertura(id);
	}
	public List<RiesgoCotizacionDTO> listarRiesgosConCoasegurosDeduciblesPendientes(
			BigDecimal idToCotizacion) throws SystemException {
		return new RiesgoCotizacionSN().listarRiesgosConCoasegurosDeduciblesPendientes(idToCotizacion);
	}
}
