package mx.com.afirme.midas.cotizacion.riesgo.aumento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AumentoRiesgoCotizacionDN {
	private static final AumentoRiesgoCotizacionDN INSTANCIA = new AumentoRiesgoCotizacionDN();

	public static AumentoRiesgoCotizacionDN getInstancia (){
		return AumentoRiesgoCotizacionDN.INSTANCIA;
	}
	
	public void agregar(AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new AumentoRiesgoCotizacionSN().agregar(aumentoRiesgoCotizacionDTO);
	}
	
	public void borrar (AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new AumentoRiesgoCotizacionSN().borrar(aumentoRiesgoCotizacionDTO);
	}
	
	public AumentoRiesgoCotizacionDTO modificar (AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new AumentoRiesgoCotizacionSN().modificar(aumentoRiesgoCotizacionDTO);
	}
	
	public AumentoRiesgoCotizacionDTO getPorId(AumentoRiesgoCotizacionId aumentoRiesgoCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new AumentoRiesgoCotizacionSN().getPorId(aumentoRiesgoCotizacionId);
	}
	
	public List<AumentoRiesgoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new AumentoRiesgoCotizacionSN().listarTodos();
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades AumentoRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<RecargoRiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */
	public List<AumentoRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new AumentoRiesgoCotizacionSN().findByRiesgoCotizacion(riesgoCotId);
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades AumentoRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura.
	 * @param RiesgoCotizacionId  
	 * @return List<RecargoRiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */
	public List<AumentoRiesgoCotizacionDTO> findByCoberturaCotizacion(RiesgoCotizacionId riesgoCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new AumentoRiesgoCotizacionSN().findByCoberturaCotizacion(riesgoCotId);
	}

	public List<AumentoRiesgoCotizacionDTO> listarAumentosPendientesAutorizacion(
			BigDecimal idToCotizacion) throws SystemException {
		return new AumentoRiesgoCotizacionSN().listarAumentosPendientesAutorizacion(idToCotizacion);
	}
}
