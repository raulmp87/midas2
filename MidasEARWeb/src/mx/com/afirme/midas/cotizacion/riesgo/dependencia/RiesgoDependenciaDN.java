package mx.com.afirme.midas.cotizacion.riesgo.dependencia;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoDependenciaDN {
	private static final RiesgoDependenciaDN INSTANCIA = new RiesgoDependenciaDN();

	public static RiesgoDependenciaDN getInstancia() {
		return RiesgoDependenciaDN.INSTANCIA;
	}

	public List<RiesgoDependenciaDTO> buscarPorPropiedad(String propertyName,
			Object value) throws ExcepcionDeAccesoADatos, SystemException {
		RiesgoDependenciaSN riesgoDependenciaSN = new RiesgoDependenciaSN();
		return riesgoDependenciaSN.buscarPorPropiedad(propertyName, value);
	}
}
