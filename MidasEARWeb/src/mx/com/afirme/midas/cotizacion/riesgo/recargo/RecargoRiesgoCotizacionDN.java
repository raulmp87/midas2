package mx.com.afirme.midas.cotizacion.riesgo.recargo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RecargoRiesgoCotizacionDN {
	private static final RecargoRiesgoCotizacionDN INSTANCIA = new RecargoRiesgoCotizacionDN();

	public static RecargoRiesgoCotizacionDN getInstancia (){
		return RecargoRiesgoCotizacionDN.INSTANCIA;
	}
	
	public void agregar(RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new RecargoRiesgoCotizacionSN().agregar(recargoRiesgoCotizacionDTO);
	}
	
	public void borrar (RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new RecargoRiesgoCotizacionSN().borrar(recargoRiesgoCotizacionDTO);
	}
	
	public void modificar (RecargoRiesgoCotizacionDTO recargoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new RecargoRiesgoCotizacionSN().modificar(recargoRiesgoCotizacionDTO);
	}
	
	public RecargoRiesgoCotizacionDTO getPorId(RecargoRiesgoCotizacionId recargoRiesgoCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new RecargoRiesgoCotizacionSN().getPorId(recargoRiesgoCotizacionId);
	}
	
	public List<RecargoRiesgoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new RecargoRiesgoCotizacionSN().listarTodos();
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades RecargoRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<RecargoRiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */
	public List<RecargoRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new RecargoRiesgoCotizacionSN().findByRiesgoCotizacion(riesgoCotId);
	}

	public List<RecargoRiesgoCotizacionDTO> listarRecargosPendientesAutorizacion(
			BigDecimal idToCotizacion) throws SystemException {
		return new RecargoRiesgoCotizacionSN().listarRecargosPendientesAutorizacion(idToCotizacion);
	}
}
