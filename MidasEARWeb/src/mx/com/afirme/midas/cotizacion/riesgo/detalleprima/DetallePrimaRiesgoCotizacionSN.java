package mx.com.afirme.midas.cotizacion.riesgo.detalleprima;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetallePrimaRiesgoCotizacionSN {
	private DetallePrimaRiesgoCotizacionFacadeRemote beanRemoto;

	public DetallePrimaRiesgoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(DetallePrimaRiesgoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log(
				"bean Remoto DetallePrimaRiesgoCotizacion instanciado",
				Level.FINEST, null);
	}

	public void agregar(
			DetallePrimaRiesgoCotizacionDTO recargoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(recargoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(
			DetallePrimaRiesgoCotizacionDTO recargoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(recargoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DetallePrimaRiesgoCotizacionDTO modificar(
			DetallePrimaRiesgoCotizacionDTO recargoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(recargoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DetallePrimaRiesgoCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DetallePrimaRiesgoCotizacionDTO> buscarPorPropiedad(
			String propertyName, Object value) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public DetallePrimaRiesgoCotizacionDTO getPorId(
			DetallePrimaRiesgoCotizacionId id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * DetallePrimaRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotizacionId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<DetallePrimaRiesgoCotizacionDTO>
	 */
	public List<DetallePrimaRiesgoCotizacionDTO> findByRiesgoCotizacion(
			RiesgoCotizacionId riesgoCotId) {
		try {
			return beanRemoto.findByRiesgoCotizacion(riesgoCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DetallePrimaRiesgoCotizacionDTO> listarFiltrado(DetallePrimaRiesgoCotizacionId detaPrimaRiesgoCotizacionId) {
		try {
			return beanRemoto.listarFiltado(detaPrimaRiesgoCotizacionId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void eliminarDetallePrimaRiesgoCotizacion(RiesgoCotizacionId id) {
		try {
			beanRemoto.deleteDetallePrimaRiesgoCotizacion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void borrarFiltrado(DetallePrimaRiesgoCotizacionId id) {
		try {
			beanRemoto.borrarFiltrado(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DetallePrimaRiesgoCotizacionDTO> listarPorSubInciso(
			SubIncisoCotizacionDTOId id) {
		try {
			return beanRemoto.findBySubInciso(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
