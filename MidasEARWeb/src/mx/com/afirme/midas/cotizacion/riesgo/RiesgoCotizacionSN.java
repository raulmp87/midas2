package mx.com.afirme.midas.cotizacion.riesgo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoCotizacionSN {
	private RiesgoCotizacionFacadeRemote beanRemoto;

	public RiesgoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(RiesgoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void borrar(RiesgoCotizacionDTO riesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(riesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RiesgoCotizacionDTO modificar(RiesgoCotizacionDTO riesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(riesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RiesgoCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public RiesgoCotizacionDTO getPorId(RiesgoCotizacionId riesgoCotizacionid)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(riesgoCotizacionid);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> getPorPropiedad(String name, Object obj)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(name, obj);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void guardar(RiesgoCotizacionDTO riesgoCotizacionDTO) {
		try {
			beanRemoto.save(riesgoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoVarioCoberturaDTO> getDescuentosCobertura(
			BigDecimal idToCobertura, BigDecimal idToRiesgo) {
		try {
			return beanRemoto.getDescuentosCobertura(idToCobertura, idToRiesgo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoVarioCoberturaDTO> getAumentosCobertura(
			BigDecimal idToCobertura, BigDecimal idToRiesgo) {
		try {
			return beanRemoto.getAumentosCobertura(idToCobertura, idToRiesgo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RecargoVarioCoberturaDTO> getRecargosCobertura(
			BigDecimal idToCobertura, BigDecimal idToRiesgo) {
		try {
			return beanRemoto.getRecargosCobertura(idToCobertura, idToRiesgo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoVarioTipoPolizaDTO> getDescuentosTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToCobertura) {
		try {
			return beanRemoto.getDescuentosTipoPoliza(idToTipoPoliza,
					idToCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoVarioTipoPolizaDTO> getAumentosTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToCobertura) {
		try {
			return beanRemoto.getAumentosTipoPoliza(idToTipoPoliza,
					idToCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RecargoVarioTipoPolizaDTO> getRecargosTipoPoliza(
			BigDecimal idToTipoPoliza, BigDecimal idToCobertura) {
		try {
			return beanRemoto.getRecargosTipoPoliza(idToTipoPoliza,
					idToCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoVarioProductoDTO> getDescuentosProducto(
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		try {
			return beanRemoto.getDescuentosProducto(idToProducto,
					idToTipoPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoVarioProductoDTO> getAumentosProducto(
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		try {
			return beanRemoto.getAumentosProducto(idToProducto, idToTipoPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RecargoVarioProductoDTO> getRecargosProducto(
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		try {
			return beanRemoto.getRecargosProducto(idToProducto, idToTipoPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> listarRiesgoContratados(
			CoberturaCotizacionId id) {
		try {
			return beanRemoto.listarRiesgoContratados(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorCobertura(
			CoberturaCotizacionId id) {
		try {
			return beanRemoto.listarRiesgoContratadosPorCobertura(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> listarFiltrado(
			RiesgoCotizacionDTO riesgoCotizacionDTO) {
		try {
			return beanRemoto.listarFiltrado(riesgoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> listarPorIdFiltrado(RiesgoCotizacionId id) {
		try {
			return beanRemoto.listarPorIdFiltrado(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorSeccion(
			SeccionCotizacionDTOId id) {
		try {
			return beanRemoto.listarRiesgoContratadosPorSeccion(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void insertarARD(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToProducto, BigDecimal idToTipoPoliza) {
		try {
			beanRemoto.insertarARD(idToCotizacion, numeroInciso, idToProducto,
					idToTipoPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<BigDecimal> obtenerRiesgosContratadosCotizacion(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.obtenerRiesgosContratadosCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Long obtenerTotalRiesgoContratadosPorCobertura(
			CoberturaCotizacionId id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalRiesgoContratadosPorCobertura(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RiesgoCotizacionDTO> listarRiesgosConCoasegurosDeduciblesPendientes(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarRiesgosConCoasegurosDeduciblesPendientes(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<RiesgoCotizacionDTO> listarRiesgosContratadosPorCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarRiesgosContratadosPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(getClass().toString(),e,Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
