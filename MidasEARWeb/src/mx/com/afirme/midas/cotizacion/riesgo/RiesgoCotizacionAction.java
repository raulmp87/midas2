package mx.com.afirme.midas.cotizacion.riesgo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaId;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Fernando Alonzo
 * @since 19/10/2009
 * 
 */
public class RiesgoCotizacionAction extends MidasMappingDispatchAction {
	@SuppressWarnings("unused")
	private static final String ROL_AUTORIZADOR = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.usuario.rol.autorizador");
	private static final NumberFormat fCuota = Sistema.FORMATO_CUOTA;  
	private static final NumberFormat fMonto = new DecimalFormat("##0.00");  

	/**
	 * Method mostrarListarRiesgos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws SystemException 
	 */
	public ActionForward mostrarRiesgosPorCobertura(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException {
		String reglaNavegacion = Sistema.EXITOSO;
		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
		BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
		BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
		
		/*Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		for (Rol rol : usuario.getRoles()) {
			if (rol.getDescripcion().equalsIgnoreCase(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
				reglaNavegacion = Sistema.ALTERNO;
			}
		}*/
		if(request.getParameter("autorizar") != null && request.getParameter("autorizar").equals("1")) {
			reglaNavegacion = Sistema.ALTERNO;
		}

		CoberturaDN coberturaDN = CoberturaDN.getInstancia();
		CoberturaDTO coberturaDTO = coberturaDN.getPorId(idToCobertura);
		request.setAttribute("nombreCobertura", coberturaDTO.getNombreComercial());
		request.setAttribute("idCotizacion", idToCotizacion.intValue());
		request.setAttribute("numeroInciso", numeroInciso.intValue());
		request.setAttribute("idToSeccion", idToSeccion.intValue());
		request.setAttribute("idToCobertura", idToCobertura.intValue());
		request.setAttribute("fecha", cotizacionDTO.getFechaCreacion());
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarRiesgos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException 
	 */
	public void listarRiesgos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToSeccion = request.getParameter("idToSeccion");
		String numeroInciso = request.getParameter("numeroInciso");
		String idToCobertura = request.getParameter("idToCobertura");

		try {
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			id.setIdToSeccion(UtileriasWeb.regresaBigDecimal(idToSeccion));
			id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso));
			id.setIdToCobertura(UtileriasWeb.regresaBigDecimal(idToCobertura));
			RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
			riesgoCotizacionDTO.setId(id);
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarFiltrado(riesgoCotizacionDTO);
			MidasJsonBase json = new MidasJsonBase();
			for(RiesgoCotizacionDTO riesgo : riesgos) {
				Short tieneSubIncisos = riesgo.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getClaveSubIncisos();
				ComisionCotizacionDN comisionCotizacionDN = ComisionCotizacionDN.getInstancia();
				ComisionCotizacionId idComision = new ComisionCotizacionId();
				idComision.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
				idComision.setIdTcSubramo(riesgo.getIdTcSubramo());
				idComision.setTipoPorcentajeComision((short)1);
				ComisionCotizacionDTO comision = comisionCotizacionDN.getPorId(idComision );
				String iconoCoaseguro = "";
				String tooltipCoaseguro = "";
				switch(riesgo.getClaveAutCoaseguro()) {
				case Sistema.AUTORIZACION_NO_REQUERIDA:
					iconoCoaseguro = Sistema.ICONO_BLANCO;
					tooltipCoaseguro = "No requiere autorizaci&oacute;n";
					break;
				case Sistema.AUTORIZACION_REQUERIDA:
					iconoCoaseguro = Sistema.ICONO_AMARILLO;
					tooltipCoaseguro = "Autorizaci&oacute;n solicitada";
					break;
				case Sistema.AUTORIZADA:
					iconoCoaseguro = Sistema.ICONO_VERDE;
					tooltipCoaseguro = "Autorizado";
					break;
				case Sistema.RECHAZADA:
					iconoCoaseguro = Sistema.ICONO_ROJO;
					tooltipCoaseguro = "Rechazado";
					break;
				}
				String iconoDeducible = "";
				String tooltipDeducible = "";
				switch(riesgo.getClaveAutDeducible()) {
				case Sistema.AUTORIZACION_NO_REQUERIDA:
					iconoDeducible = Sistema.ICONO_BLANCO;
					tooltipDeducible = "No requiere autorizaci&oacute;n";
					break;
				case Sistema.AUTORIZACION_REQUERIDA:
					iconoDeducible = Sistema.ICONO_AMARILLO;
					tooltipDeducible = "Autorizaci&oacute;n solicitada";
					break;
				case Sistema.AUTORIZADA:
					iconoDeducible = Sistema.ICONO_VERDE;
					tooltipDeducible = "Autorizado";
					break;
				case Sistema.RECHAZADA:
					iconoDeducible = Sistema.ICONO_ROJO;
					tooltipDeducible = "Rechazado";
					break;
				}
				Double valorCoaseguro = 0D;
				Double valorDeducible = 0D;
				if(riesgo.getClaveContrato().shortValue() != 1) {
					CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN = CoaseguroRiesgoCoberturaDN.getInstancia();
					CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO = new CoaseguroRiesgoCoberturaDTO();
					CoaseguroRiesgoCoberturaId idCoaseguro = new CoaseguroRiesgoCoberturaId();
					idCoaseguro.setIdToCobertura(riesgo.getId().getIdToCobertura());
					idCoaseguro.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
					coaseguroRiesgoCoberturaDTO.setId(idCoaseguro );
					List<CoaseguroRiesgoCoberturaDTO> coaseguros = coaseguroRiesgoCoberturaDN.listarFiltrado(coaseguroRiesgoCoberturaDTO);
					for(CoaseguroRiesgoCoberturaDTO coaseguro : coaseguros) {
						if(coaseguro.getClaveDefault().shortValue() == 1) {
							valorCoaseguro = coaseguro.getValor();
						}
					}
					DeducibleRiesgoCoberturaDN deducibleRiesgoCoberturaDN = DeducibleRiesgoCoberturaDN.getInstancia();
					DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO = new DeducibleRiesgoCoberturaDTO();
					DeducibleRiesgoCoberturaId idDeducible = new DeducibleRiesgoCoberturaId();
					idDeducible.setIdToCobertura(riesgo.getId().getIdToCobertura());
					idDeducible.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
					deducibleRiesgoCoberturaDTO.setId(idDeducible );
					List<DeducibleRiesgoCoberturaDTO> deducibles = deducibleRiesgoCoberturaDN.listarFiltrado(deducibleRiesgoCoberturaDTO );
					for(DeducibleRiesgoCoberturaDTO deducible : deducibles) {
						if(deducible.getClaveDefault().shortValue() == 1) {
							valorDeducible = deducible.getValor();
						}
					}
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(riesgo.getId().getIdToCobertura() + "_" + riesgo.getId().getIdToRiesgo());	
				//idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada,"
				//"Riesgo,Contratado,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,
				Double cuotaRiesgo = riesgo.getValorSumaAsegurada() == 0? 0D : riesgo.getValorPrimaNeta() / riesgo.getValorSumaAsegurada() * 1000;
				row.setDatos(riesgo.getId().getIdToCotizacion().toString(),
						riesgo.getId().getNumeroInciso().toString(),
						riesgo.getId().getIdToSeccion().toString(),
						riesgo.getId().getIdToCobertura().toString(),
						riesgo.getId().getIdToRiesgo().toString(),
						riesgo.getIdTcSubramo().toString(),
						riesgo.getRiesgoCoberturaDTO().getClaveObligatoriedad().toString(),
						tieneSubIncisos.shortValue() == 0? riesgo.getRiesgoCoberturaDTO().getClaveTipoSumaAsegurada().toString() : "4",
						riesgo.getRiesgoCoberturaDTO().getDescripcionRiesgoCobertura(),
						riesgo.getClaveContrato().toString(),
						fMonto.format(riesgo.getValorSumaAsegurada()),
						riesgo.getClaveContrato().shortValue() == 1? fCuota.format(cuotaRiesgo) : "",
						riesgo.getClaveContrato().shortValue() == 1? fMonto.format(riesgo.getValorPrimaNeta()): "", 
						riesgo.getClaveContrato().shortValue() == 1? ((comision != null)?comision.getValorComisionCotizacion().toString() : "" ): "", //TODO pendiente comision
						valorCoaseguro > 0? valorCoaseguro.toString() : riesgo.getPorcentajeCoaseguro().toString(),
						MidasJsonRow.generarLineaImagenDataGrid(iconoCoaseguro, tooltipCoaseguro, "void(0);", null),
						valorDeducible > 0? valorDeducible.toString() : riesgo.getPorcentajeDeducible().toString(),
						MidasJsonRow.generarLineaImagenDataGrid(iconoDeducible, tooltipDeducible, "void(0);", null),
						riesgo.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getClaveBienSeccion().equals("1")? "1" : "0",
						riesgo.getClaveTipoDeducible()!= null?riesgo.getClaveTipoDeducible().toString():"",
						riesgo.getValorMinimoLimiteDeducible()!= null?riesgo.getValorMinimoLimiteDeducible().toString():"",
						riesgo.getValorMaximoLimiteDeducible()!=null?riesgo.getValorMaximoLimiteDeducible().toString():"",
						riesgo.getClaveTipoLimiteDeducible()!=null?riesgo.getClaveTipoLimiteDeducible().toString():"");
				json.addRow(row);
			}
			response.setContentType("text/json");
			System.out.println(json);
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method listarAutorizacionRiesgos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException 
	 */
	public void listarAutorizacionRiesgos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String idToCotizacion = request.getParameter("idToCotizacion");
		String idToSeccion = request.getParameter("idToSeccion");
		String numeroInciso = request.getParameter("numeroInciso");
		String idToCobertura = request.getParameter("idToCobertura");

		try {
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			id.setIdToSeccion(UtileriasWeb.regresaBigDecimal(idToSeccion));
			id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso));
			id.setIdToCobertura(UtileriasWeb.regresaBigDecimal(idToCobertura));
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarRiesgoContratados(id );
			MidasJsonBase json = new MidasJsonBase();
			for(RiesgoCotizacionDTO riesgo : riesgos) {
				ComisionCotizacionDN comisionCotizacionDN = ComisionCotizacionDN.getInstancia();
				ComisionCotizacionId idComision = new ComisionCotizacionId();
				idComision.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
				idComision.setIdTcSubramo(riesgo.getIdTcSubramo());
				idComision.setTipoPorcentajeComision((short)1);
				ComisionCotizacionDTO comision = comisionCotizacionDN.getPorId(idComision );
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(riesgo.getId().getIdToCobertura() + "_" + riesgo.getId().getIdToRiesgo());	
				//"idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo," +
				//"riesgo,sumaAsegurada,cuota,primaNeta,comision,coaseguro,coaseguroAutorizado,deducible,deducibleAutorizado"
				Double cuotaRiesgo = riesgo.getValorSumaAsegurada() == 0? 0D : riesgo.getValorPrimaNeta() / riesgo.getValorSumaAsegurada() * 1000;
				row.setDatos(riesgo.getId().getIdToCotizacion().toString(),
						riesgo.getId().getNumeroInciso().toString(),
						riesgo.getId().getIdToSeccion().toString(),
						riesgo.getId().getIdToCobertura().toString(),
						riesgo.getId().getIdToRiesgo().toString(),
						riesgo.getIdTcSubramo().toString(),
						riesgo.getRiesgoCoberturaDTO().getDescripcionRiesgoCobertura(),
						fMonto.format(riesgo.getValorSumaAsegurada()),
						fCuota.format(cuotaRiesgo),
						fMonto.format(riesgo.getValorPrimaNeta()), 
						comision.getValorComisionCotizacion().toString(), //TODO pendiente comision
						riesgo.getPorcentajeCoaseguro().toString(),
						riesgo.getClaveAutCoaseguro().toString(),
						riesgo.getPorcentajeDeducible().toString(),
						riesgo.getClaveAutDeducible().toString());
				json.addRow(row);
			}
			response.setContentType("text/json");
			System.out.println(json);
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Method guardarRiesgo
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException 
	 * @throws SystemException 
	 */
	public void guardarRiesgo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException {
		String gr_id = request.getParameter("gr_id");
		String status = request.getParameter("!nativeeditor_status");
		String action = "";
		if(status.equals("updated")) {
			action = "updated";
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			id.setIdToCotizacion(idToCotizacion);
			BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
			id.setNumeroInciso(numeroInciso);
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			id.setIdToSeccion(idToSeccion);
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
			id.setIdToCobertura(idToCobertura);
			BigDecimal idToRiesgo = UtileriasWeb.regresaBigDecimal(request.getParameter("idToRiesgo"));
			id.setIdToRiesgo(idToRiesgo);

			RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
			riesgoCotizacionDTO.setId(id);

			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			riesgoCotizacionDTO = riesgoCotizacionDN.getPorId(id);

			if(request.getParameter("contratado").equals("0")) {
				riesgoCotizacionDTO.setClaveContrato(Sistema.NO_CONTRATADO);
				riesgoCotizacionDN.modificar(riesgoCotizacionDTO);
				CalculoCotizacionDN.getInstancia().calcularRiesgo(riesgoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));
				//SE AGREGA ELIMINACION DE DETALLESPRIMASRIESGO
				CotizacionDN.getInstancia("").eliminaDetallesPrimasDeRiesgo(riesgoCotizacionDTO);
			} else {
				if(riesgoCotizacionDTO.getPorcentajeCoaseguro().doubleValue() != Double.parseDouble(request.getParameter("coaseguro"))) {
					riesgoCotizacionDTO.setPorcentajeCoaseguro(Double.parseDouble(request.getParameter("coaseguro")));
					if(riesgoCotizacionDTO.getPorcentajeCoaseguro().doubleValue() != 0) {
						riesgoCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZACION_REQUERIDA);
						CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN =  CoaseguroRiesgoCoberturaDN.getInstancia();
						CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO = new CoaseguroRiesgoCoberturaDTO();
						CoaseguroRiesgoCoberturaId idCoaseguro = new CoaseguroRiesgoCoberturaId();
						idCoaseguro.setIdToSeccion(idToSeccion);
						idCoaseguro.setIdToCobertura(idToCobertura);
						idCoaseguro.setIdToRiesgo(idToRiesgo);
						coaseguroRiesgoCoberturaDTO.setId(idCoaseguro);
						List<CoaseguroRiesgoCoberturaDTO> coaseguros = coaseguroRiesgoCoberturaDN.listarFiltrado(coaseguroRiesgoCoberturaDTO);
						for(CoaseguroRiesgoCoberturaDTO coaseguro : coaseguros) {
							if(coaseguro.getValor().doubleValue() == riesgoCotizacionDTO.getPorcentajeCoaseguro().doubleValue()) {
								riesgoCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZACION_NO_REQUERIDA);
								break;
							}
						}
					} else {
						riesgoCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZACION_REQUERIDA);
					}
				}
				if(riesgoCotizacionDTO.getPorcentajeDeducible().doubleValue() != Double.parseDouble(request.getParameter("deducible"))) {
					riesgoCotizacionDTO.setPorcentajeDeducible(Double.parseDouble(request.getParameter("deducible")));
					if(riesgoCotizacionDTO.getPorcentajeDeducible().doubleValue() != 0) {
						riesgoCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZACION_REQUERIDA);
						DeducibleRiesgoCoberturaDN deducibleRiesgoCoberturaDN =  DeducibleRiesgoCoberturaDN.getInstancia();
						DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO = new DeducibleRiesgoCoberturaDTO();
						DeducibleRiesgoCoberturaId idDeducible = new DeducibleRiesgoCoberturaId();
						idDeducible.setIdToSeccion(idToSeccion);
						idDeducible.setIdToCobertura(idToCobertura);
						idDeducible.setIdToRiesgo(idToRiesgo);
						deducibleRiesgoCoberturaDTO.setId(idDeducible );
						List<DeducibleRiesgoCoberturaDTO> deducibles = deducibleRiesgoCoberturaDN.listarFiltrado(deducibleRiesgoCoberturaDTO);
						for(DeducibleRiesgoCoberturaDTO deducible : deducibles) {
							if(deducible.getValor().doubleValue() == riesgoCotizacionDTO.getPorcentajeDeducible().doubleValue()) {
								riesgoCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZACION_NO_REQUERIDA);
								break;
							}
						}
					} else {
						riesgoCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZACION_NO_REQUERIDA);
					}
				}
				if(riesgoCotizacionDTO.getValorSumaAsegurada().doubleValue() != Double.parseDouble(request.getParameter("sumaAsegurada"))) {
					riesgoCotizacionDTO.setValorSumaAsegurada(Double.parseDouble(request.getParameter("sumaAsegurada")));
				}
				String tipoDeducible = request.getParameter("tipoDeducible");
				String minimo = request.getParameter("minimo");
				String maximo = request.getParameter("maximo");
				String tipoLimiteDeducible = request.getParameter("tipoLimiteDeducible");
				
				if(tipoDeducible != null && !tipoDeducible.equals("")) {
					riesgoCotizacionDTO.setClaveTipoDeducible(Short.parseShort(tipoDeducible));
				}

				if(minimo != null && !minimo.equals("")) {
					riesgoCotizacionDTO.setValorMinimoLimiteDeducible(Double.parseDouble(minimo));
				}
				
				if(maximo != null && !maximo.equals("")) {
					riesgoCotizacionDTO.setValorMaximoLimiteDeducible(Double.parseDouble(maximo));
				}

				if(tipoLimiteDeducible != null && !tipoLimiteDeducible.equals("")) {
					riesgoCotizacionDTO.setClaveTipoLimiteDeducible(Short.parseShort(tipoLimiteDeducible));
				}

				riesgoCotizacionDTO.setClaveContrato((short)1);
				riesgoCotizacionDN.modificar(riesgoCotizacionDTO);
				CalculoCotizacionDN.getInstancia().calcularRiesgo(riesgoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));
			}
		} else if(status.equals("inserted")) {
			action = "inserted";
		} else if(status.equals("deleted")) {
			action = "deleted";
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + gr_id + "\" tid=\"" + gr_id + "\" ></action></data>");
		pw.flush();
		pw.close();
	}

	public void getValoresCoaseguroDeducible(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws SystemException, IOException {
		
		String id = request.getParameter("tipo");
		BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion")) ;
		BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura")) ;
		BigDecimal idToRiesgo = UtileriasWeb.regresaBigDecimal(request.getParameter("idToRiesgo")) ;
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<id>");
		buffer.append("0.0");
		buffer.append("</id>");
		buffer.append("<description><![CDATA[");
		buffer.append("N/A");
		buffer.append("]]></description>");
		buffer.append("</item>");			
			if(id.equals("C")){//Coaseguro
				CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN =  CoaseguroRiesgoCoberturaDN.getInstancia();
				CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO = new CoaseguroRiesgoCoberturaDTO();
				CoaseguroRiesgoCoberturaId idCoaseguro = new CoaseguroRiesgoCoberturaId();
				idCoaseguro.setIdToSeccion(idToSeccion);
				idCoaseguro.setIdToCobertura(idToCobertura);
				idCoaseguro.setIdToRiesgo(idToRiesgo);
				coaseguroRiesgoCoberturaDTO.setId(idCoaseguro );
				List<CoaseguroRiesgoCoberturaDTO> coaseguros = coaseguroRiesgoCoberturaDN.listarFiltrado(coaseguroRiesgoCoberturaDTO);
				for(CoaseguroRiesgoCoberturaDTO coaseguro: coaseguros){
					buffer.append("<item>");
					buffer.append("<id>");
					buffer.append(coaseguro.getValor().toString());
					buffer.append("</id>");
					buffer.append("<description><![CDATA[");
					buffer.append(coaseguro.getValor().toString()+"%");
					buffer.append("]]></description>");
					buffer.append("</item>");					
				}
			}else{//Deducibles
				DeducibleRiesgoCoberturaDN deducibleRiesgoCoberturaDN =  DeducibleRiesgoCoberturaDN.getInstancia();
				DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO = new DeducibleRiesgoCoberturaDTO();
				DeducibleRiesgoCoberturaId idDeducible = new DeducibleRiesgoCoberturaId();
				idDeducible.setIdToSeccion(idToSeccion);
				idDeducible.setIdToCobertura(idToCobertura);
				idDeducible.setIdToRiesgo(idToRiesgo);
				deducibleRiesgoCoberturaDTO.setId(idDeducible );
				List<DeducibleRiesgoCoberturaDTO> deducibles = deducibleRiesgoCoberturaDN.listarFiltrado(deducibleRiesgoCoberturaDTO);
				for(DeducibleRiesgoCoberturaDTO deducible: deducibles){
					buffer.append("<item>");
					buffer.append("<id>");
					buffer.append(deducible.getValor().toString());
					buffer.append("</id>");
					buffer.append("<description><![CDATA[");
					buffer.append(deducible.getValor().toString()+"%");
					buffer.append("]]></description>");
					buffer.append("</item>");					
				}				
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());	
	}

	/**
	 * Method guardarRiesgoAutorizado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException 
	 * @throws SystemException 
	 */
	public void guardarRiesgoAutorizado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException {
		String gr_id = request.getParameter("gr_id");
		String status = request.getParameter("!nativeeditor_status");
		String action = "";
		String extra = "";
		if(status.equals("updated")) {
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			id.setIdToCotizacion(idToCotizacion);
			BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
			id.setNumeroInciso(numeroInciso);
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			id.setIdToSeccion(idToSeccion);
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
			id.setIdToCobertura(idToCobertura);
			BigDecimal idToRiesgo = UtileriasWeb.regresaBigDecimal(request.getParameter("idToRiesgo"));
			id.setIdToRiesgo(idToRiesgo);
			RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
			riesgoCotizacionDTO.setId(id);
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarFiltrado(riesgoCotizacionDTO);
			riesgoCotizacionDTO = riesgos.get(0);
			String coaseguroAutorizado = request.getParameter("coaseguroAutorizado");
			String deducibleAutorizado = request.getParameter("deducibleAutorizado");
			if(coaseguroAutorizado != null && coaseguroAutorizado.equals("1")) {
				action = "coaseguroActualizado";
				riesgoCotizacionDTO.setClaveAutCoaseguro(Sistema.AUTORIZADA);
			}
			if(deducibleAutorizado != null && deducibleAutorizado.equals("1")) {
				if(action.equals("")) {
					action = "deducibleActualizado";
				} else {
					extra = "deducibleActualizado";
				}
				riesgoCotizacionDTO.setClaveAutDeducible(Sistema.AUTORIZADA);
			}
			riesgoCotizacionDN.modificar(riesgoCotizacionDTO);
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + gr_id + "\" tid=\"" + gr_id + "\" extra=\"" + extra + "\"  ></action></data>");
		pw.flush();
		pw.close();
	}

	/**
	 * Method guardarRiesgoAutorizado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws IOException 
	 * @throws SystemException 
	 */
	public void guardarRiesgoRechazado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException {
		String gr_id = request.getParameter("gr_id");
		String status = request.getParameter("!nativeeditor_status");
		String action = "";
		String extra = "";
		if(status.equals("updated")) {
			RiesgoCotizacionId id = new RiesgoCotizacionId();
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			id.setIdToCotizacion(idToCotizacion);
			BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
			id.setNumeroInciso(numeroInciso);
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			id.setIdToSeccion(idToSeccion);
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
			id.setIdToCobertura(idToCobertura);
			BigDecimal idToRiesgo = UtileriasWeb.regresaBigDecimal(request.getParameter("idToRiesgo"));
			id.setIdToRiesgo(idToRiesgo);
			RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
			riesgoCotizacionDTO.setId(id);
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarFiltrado(riesgoCotizacionDTO);
			riesgoCotizacionDTO = riesgos.get(0);
			String coaseguroAutorizado = request.getParameter("coaseguroAutorizado");
			String deducibleAutorizado = request.getParameter("deducibleAutorizado");
			if(coaseguroAutorizado != null && coaseguroAutorizado.equals("1")) {
				action = "coaseguroActualizado";
				riesgoCotizacionDTO.setClaveAutCoaseguro(Sistema.RECHAZADA);
			}
			if(deducibleAutorizado != null && deducibleAutorizado.equals("1")) {
				if(action.equals("")) {
					action = "deducibleActualizado";
				} else {
					extra = "deducibleActualizado";
				}
				riesgoCotizacionDTO.setClaveAutDeducible(Sistema.RECHAZADA);
			}
			riesgoCotizacionDN.modificar(riesgoCotizacionDTO);
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + gr_id + "\" tid=\"" + gr_id + "\" extra=\"" + extra + "\"  ></action></data>");
		pw.flush();
		pw.close();
	}
}