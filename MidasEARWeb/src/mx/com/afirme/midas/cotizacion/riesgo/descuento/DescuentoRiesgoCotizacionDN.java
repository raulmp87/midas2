package mx.com.afirme.midas.cotizacion.riesgo.descuento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DescuentoRiesgoCotizacionDN {
	private static final DescuentoRiesgoCotizacionDN INSTANCIA = new DescuentoRiesgoCotizacionDN();

	public static DescuentoRiesgoCotizacionDN getInstancia (){
		return DescuentoRiesgoCotizacionDN.INSTANCIA;
	}
	
	public void agregar(DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DescuentoRiesgoCotizacionSN().agregar(descuentoRiesgoCotizacionDTO);
	}
	
	public void borrar (DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DescuentoRiesgoCotizacionSN().borrar(descuentoRiesgoCotizacionDTO);
	}
	
	public void modificar (DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new DescuentoRiesgoCotizacionSN().modificar(descuentoRiesgoCotizacionDTO);
	}
	
	public DescuentoRiesgoCotizacionDTO getPorId(DescuentoRiesgoCotizacionId descuentoRiesgoCotizacionId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DescuentoRiesgoCotizacionSN().getPorId(descuentoRiesgoCotizacionId);
	}
	
	public List<DescuentoRiesgoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new DescuentoRiesgoCotizacionSN().listarTodos();
	}
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades DescuentoRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<DescuentoRiesgoCotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */
	public List<DescuentoRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId) throws ExcepcionDeAccesoADatos, SystemException{
		return new DescuentoRiesgoCotizacionSN().findByRiesgoCotizacion(riesgoCotId);
	}

	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosPendientesAutorizacion(
			BigDecimal idToCotizacion) throws SystemException {
		return new DescuentoRiesgoCotizacionSN().listarDescuentosPendientesAutorizacion(idToCotizacion);
	}
}
