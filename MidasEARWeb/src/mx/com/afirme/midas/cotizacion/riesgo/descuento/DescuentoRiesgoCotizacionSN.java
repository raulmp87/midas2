package mx.com.afirme.midas.cotizacion.riesgo.descuento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DescuentoRiesgoCotizacionSN {
	private DescuentoRiesgoCotizacionFacadeRemote beanRemoto;

	public DescuentoRiesgoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(DescuentoRiesgoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(
			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(descuentoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(descuentoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(
			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(descuentoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DescuentoRiesgoCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public DescuentoRiesgoCotizacionDTO getPorId(
			DescuentoRiesgoCotizacionId descuentoRiesgoCotizacionid)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(descuentoRiesgoCotizacionid);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoRiesgoCotizacionDTO> bucarPorPropiedad(
			String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void guardar(
			DescuentoRiesgoCotizacionDTO descuentoRiesgoCotizacionDTO) {
		try {
			beanRemoto.save(descuentoRiesgoCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * DescuentoRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<DescuentoRiesgoCotizacionDTO>
	 */
	public List<DescuentoRiesgoCotizacionDTO> findByRiesgoCotizacion(
			RiesgoCotizacionId riesgoCotId) {
		try {
			return beanRemoto.findByRiesgoCotizacion(riesgoCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoRiesgoCotizacionDTO> buscarPorPropiedad(
			String propertyName, Object value) {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoRiesgoCotizacionDTO> findByCoberturaCotizacion(RiesgoCotizacionId riesgoCotId) {
		try {
			return beanRemoto.findByCoberturaCotizacion(riesgoCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosPendientesAutorizacion(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarDescuentosPendientesAutorizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
}
