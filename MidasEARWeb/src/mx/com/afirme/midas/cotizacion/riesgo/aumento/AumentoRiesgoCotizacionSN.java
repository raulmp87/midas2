package mx.com.afirme.midas.cotizacion.riesgo.aumento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AumentoRiesgoCotizacionSN {
	private AumentoRiesgoCotizacionFacadeRemote beanRemoto;

	public AumentoRiesgoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(AumentoRiesgoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto AumentoRiesgoCotizacion instanciado",
				Level.FINEST, null);
	}

	public void agregar(AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(aumentoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(aumentoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AumentoRiesgoCotizacionDTO modificar(
			AumentoRiesgoCotizacionDTO aumentoRiesgoCotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(aumentoRiesgoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<AumentoRiesgoCotizacionDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public AumentoRiesgoCotizacionDTO getPorId(AumentoRiesgoCotizacionId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * AumentoRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<AumentoRiesgoCotizacionDTO>
	 */
	public List<AumentoRiesgoCotizacionDTO> findByRiesgoCotizacion(
			RiesgoCotizacionId riesgoCotId) {
		try {
			return beanRemoto.findByRiesgoCotizacion(riesgoCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoRiesgoCotizacionDTO> buscarPorPropiedad(
			String propertyName, Object value) {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoRiesgoCotizacionDTO> findByCoberturaCotizacion(
			RiesgoCotizacionId riesgoCotId) {
		try {
			return beanRemoto.findByCoberturaCotizacion(riesgoCotId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoRiesgoCotizacionDTO> listarAumentosPendientesAutorizacion(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.listarAumentosPendientesAutorizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
