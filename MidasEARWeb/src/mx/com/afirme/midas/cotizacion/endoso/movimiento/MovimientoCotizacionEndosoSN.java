package mx.com.afirme.midas.cotizacion.endoso.movimiento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MovimientoCotizacionEndosoSN {
	private MovimientoCotizacionEndosoFacadeRemote beanRemoto;
	private String nombreUsuario;

	public MovimientoCotizacionEndosoSN(String nombreUsuario)
			throws SystemException {
		try {
			this.nombreUsuario = nombreUsuario;
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(MovimientoCotizacionEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(
			MovimientoCotizacionEndosoDTO movimientoCotizacionEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			movimientoCotizacionEndosoDTO
					.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.save(movimientoCotizacionEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrarPorIdToCotizacion(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.borrarPorIdToCotizacion(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(
			MovimientoCotizacionEndosoDTO movimientoCotizacionEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			movimientoCotizacionEndosoDTO
					.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.delete(movimientoCotizacionEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(
			MovimientoCotizacionEndosoDTO movimientoCotizacionEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			movimientoCotizacionEndosoDTO
					.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.update(movimientoCotizacionEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<MovimientoCotizacionEndosoDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public List<MovimientoCotizacionEndosoDTO> listarFiltrado(
			MovimientoCotizacionEndosoDTO dto) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(dto);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public MovimientoCotizacionEndosoDTO getPorId(
			BigDecimal idToMovimientoCotEnd) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToMovimientoCotEnd);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<MovimientoCotizacionEndosoDTO> buscarPorPropiedad(
			String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<MovimientoCotizacionEndosoDTO> obtenerMovimientosPorCotizacion(
			BigDecimal idToCotizacion) {
		try {
			return beanRemoto.obtenerMovimientosPorCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public BigDecimal obtenerDiferenciaPrimaNeta(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.obtenerDiferenciaPrimaNeta(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<MovimientoCotizacionEndosoDTO> obtieneMovimientosAEvaluar(
			BigDecimal idToCotizacion) {
		return beanRemoto.obtieneMovimientosAEvaluar(idToCotizacion);
	}
	
	public void registrarMovimientos(List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		beanRemoto.registrarMovimientos(listaMovimientos);
	}
}
