package mx.com.afirme.midas.cotizacion.endoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class CotizacionEndosoDN {
	private static final CotizacionEndosoDN INSTANCIA = new CotizacionEndosoDN();
	private static String nombreUsuario;

	public static CotizacionEndosoDN getInstancia(String nombreUsuario) {
		CotizacionEndosoDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	public List<CotizacionDTO> listarCotizacionFiltrado(
			CotizacionDTO cotizacionDTO, Usuario usuario) throws ExcepcionDeAccesoADatos,
			SystemException {
		String claveCotizacionesFiltradas = "midas.danios.cotizacion.endoso.busqueda.limitada";	
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveCotizacionesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveCotizacionesFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;					
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)	
			cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		
		return new CotizacionEndosoSN(CotizacionEndosoDN.nombreUsuario)
		.listarFiltrado(cotizacionDTO);
	}
	/**
	 * Lista todas las cotizaciones que no tenga nestatus de orden de trabajo.
	 * Los estatus para las cotizaciones son: 10:COT En Proceso, 13:COT
	 * Liberada, 14:COT Lista para emitir, 15: COT Asignada, 16:COT Emitida,
	 * 18:COT Rechazada y 19:COT Cancelada
	 * 
	 * @return List<CotizacionDTO>
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<CotizacionDTO> listarCotizaciones()
			throws ExcepcionDeAccesoADatos, SystemException {
		List<CotizacionDTO> listaResultado = new ArrayList<CotizacionDTO>();
		List<CotizacionDTO> listaTMP = new ArrayList<CotizacionDTO>();
		CotizacionEndosoSN cotizacionEndosoSN = new CotizacionEndosoSN(
				CotizacionEndosoDN.nombreUsuario);
		// Listar cotizaciones en proceso
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 10));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones asignadas
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 11));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones liberadas
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 13));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones listas para emitir
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 14));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones asignadas
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 15));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones emitidas
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 16));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones rechazadas
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 18));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);
		// Listar cotizaciones canceladas
		listaTMP = cotizacionEndosoSN.buscarPorPropiedad("claveEstatus",
				new Short((short) 19));
		if (listaTMP != null)
			listaResultado.addAll(listaTMP);

		return listaResultado;
	}
	
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO,Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException {
		String claveCotizacionesFiltradas = "midas.danios.cotizacion.endoso.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveCotizacionesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveCotizacionesFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;					
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)	
			cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		return new CotizacionEndosoSN(CotizacionEndosoDN.nombreUsuario).obtenerTotalFiltrado(cotizacionDTO);
	}
	
	public boolean existenMovimientoDePrimas(CotizacionDTO cotizacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		boolean existenMovimientosDePrimas = false;
		MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
		dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());

		if(cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().intValue() == 0)
			return true;
		
		List<MovimientoCotizacionEndosoDTO> movimientos = MovimientoCotizacionEndosoDN
				.getInstancia(nombreUsuario).listarFiltrado(dto);
		BigDecimal sumatoriaPrimaNeta = BigDecimal.ZERO;
		for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
			sumatoriaPrimaNeta = sumatoriaPrimaNeta.add(movimiento
					.getValorDiferenciaPrimaNeta());
		}

		if (sumatoriaPrimaNeta.intValue() != 0)
			existenMovimientosDePrimas = true;

		return existenMovimientosDePrimas;
	}

}
