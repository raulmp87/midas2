package mx.com.afirme.midas.cotizacion.endoso;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.SoporteEstructuraCotizacion;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoSN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author jose luis arellano
 */
public class SoporteEstructuraCotizacionEndoso extends SoporteEstructuraCotizacion{
	
	private SoporteEstructuraCotizacion estructuraCotizacionEndosoAnterior;
	private List<MovimientoCotizacionEndosoDTO> listaMovimientosEndoso;
	protected Map<Short, List<MovimientoCotizacionEndosoDTO>> mapaMovimientosEndosoPorTipoMovto;
	private Map<BigDecimal,List<CoberturaCotizacionDTO>> mapaCoberturasDesContratadasPorSubRamo;
	private Map<BigDecimal[],List<CoberturaCotizacionDTO>> mapaCoberturasDesContratadasPorIncisoSubRamo;
	protected List<SubRamoDTO> listaSubRamosDesContratados;
	protected List<SubRamoDTO> listaTotalSubRamosEndoso;
	protected List<IncisoCotizacionDTO> listaIncisosDesContratados;
	protected List<IncisoCotizacionDTO> listaTotalIncisosEndoso;
	protected EndosoDTO ultimoEndoso;

	private static final int CLAVE_PERSONA_ASEGURADO = 1;
	private static final int CLAVE_PERSONA_CONTRATANTE = 2;
	
	public SoporteEstructuraCotizacionEndoso(BigDecimal idToCotizacion,String nombreUsuario,SoporteEstructuraCotizacion estructuraCotizacionEndosoAnterior) throws SystemException {
		super(idToCotizacion, nombreUsuario);
		this.estructuraCotizacionEndosoAnterior = estructuraCotizacionEndosoAnterior;
	}
	
	public SoporteEstructuraCotizacionEndoso(BigDecimal idToCotizacion,BigDecimal idToCotizacionAnterior, String nombreUsuario) throws SystemException {
		super(idToCotizacion, nombreUsuario);
		this.estructuraCotizacionEndosoAnterior = new SoporteEstructuraCotizacion(idToCotizacionAnterior,nombreUsuario);
	}

	public SoporteEstructuraCotizacionEndoso(CotizacionDTO cotizacion,SoporteEstructuraCotizacion estructuraCotizacionEndosoAnterior) throws SystemException {
		super(cotizacion);
		this.estructuraCotizacionEndosoAnterior = estructuraCotizacionEndosoAnterior;
	}
	
	public SoporteEstructuraCotizacionEndoso(CotizacionDTO cotizacionDTO,CotizacionDTO cotizacionAnteriorDTO) throws SystemException {
		super(cotizacionDTO);
		this.estructuraCotizacionEndosoAnterior = new SoporteEstructuraCotizacion(cotizacionAnteriorDTO);
	}
	
	public SoporteEstructuraCotizacionEndoso(CotizacionDTO cotizacionDTO,BigDecimal idToCotizacionAnterior,EndosoDTO ultimoEndoso,String nombreUsuario) throws SystemException {
		super(cotizacionDTO);
		this.estructuraCotizacionEndosoAnterior = new SoporteEstructuraCotizacion(idToCotizacionAnterior,nombreUsuario);
		this.ultimoEndoso = ultimoEndoso;
	}

	/**
	 * Consulta toda la informacion de la cotizaci�n actual y del endoso anterior: incisos, subincisos, coberturas contratadas, 
	 * detalle de prima de coberturas y agrupaciones de primer riesgo o LUC, as� como los movimientos realizados en el endoso
	 * @param nombreUsuario
	 * @throws SystemException
	 */
	public void consultarInformacionValidacionCotizacion(String nombreUsuario) throws SystemException{
		super.consultarInformacionValidacionCotizacion(nombreUsuario);
		estructuraCotizacionEndosoAnterior.consultarInformacionValidacionCotizacion(nombreUsuario);
		
	}

	public void consultarMovimientosEndoso(String nombreUsuario) throws SystemException{
		listaMovimientosEndoso = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).obtenerMovimientosPorCotizacion(idToCotizacion);
	}
	
	public EndosoDTO obtenerUltimoEndoso(String nombreUsuario) throws SystemException{
		if(ultimoEndoso == null){
			ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(
					cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		}
		return ultimoEndoso;
	}
	
	/**
	 * Calcula la lista de los diferentes registros SubRamoDTO contratados y dados de baja en la cotizaci�n de endoso, utilizando 
	 * la lista de registros CoberturaCotizacionDTO del endoso anterior y la cotizacion actual, as� como los movimientos de la cotizaci�n.
	 * @return
	 * @throws SystemException
	 */
	public List<SubRamoDTO> listarSubRamosEndoso() throws SystemException{
		if(listaTotalSubRamosEndoso == null){
			listaTotalSubRamosEndoso = new ArrayList<SubRamoDTO>();
			super.listarSubRamosContratados();
			listarSubRamosDescontratados();
			listaTotalSubRamosEndoso.addAll(listaSubRamosContratados);
			for(SubRamoDTO subramoTMP : listaSubRamosDesContratados){
				if(!listaTotalSubRamosEndoso.contains(subramoTMP)){
					listaTotalSubRamosEndoso.add(subramoTMP);
				}
			}
		}
		return listaTotalSubRamosEndoso;
	}
	
	/**
	 * Calcula la lista de los diferentes subramos dados de baja en la cotizaci�n.
	 * @return
	 */
	public List<SubRamoDTO> listarSubRamosDescontratados(){
		if (listaSubRamosDesContratados == null){
			listaSubRamosDesContratados = new ArrayList<SubRamoDTO>();
			List<MovimientoCotizacionEndosoDTO> listaMovimientosBajaCobertura = obtenerListaMovimientosEndoso(Sistema.TIPO_MOV_BAJA_COBERTURA);
			//busqueda de las diferentes coberturas dadas de baja
			for(MovimientoCotizacionEndosoDTO movtoTMP : listaMovimientosBajaCobertura){
				CoberturaCotizacionDTO coberturaTMP = estructuraCotizacionEndosoAnterior.obtenerCoberturaContratadaPorId(
						movtoTMP.getNumeroInciso(), movtoTMP.getIdToSeccion(), movtoTMP.getIdToCobertura());
				if(coberturaTMP != null){
					SubRamoDTO subRamoTMP = coberturaTMP.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO();
					if(!listaSubRamosDesContratados.contains(subRamoTMP)){
						listaSubRamosDesContratados.add(subRamoTMP);
					}
				}
			}
		}
		return listaSubRamosDesContratados;
	}
	
	/**
	 * Calcula la lista de los diferentes coberturas de un subramo espec�fico dadas de baja en la cotizaci�n.
	 * @return
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasDescontratadasPorSubRamo(BigDecimal idTcSubRamo){
		List<CoberturaCotizacionDTO> listaCoberturasDescontratadas = null;
		if(mapaCoberturasDesContratadasPorSubRamo == null)
			mapaCoberturasDesContratadasPorSubRamo = new HashMap<BigDecimal, List<CoberturaCotizacionDTO>>();
		listaCoberturasDescontratadas = mapaCoberturasDesContratadasPorSubRamo.get(idTcSubRamo);
		if(listaCoberturasDescontratadas == null){
			listaCoberturasDescontratadas = new ArrayList<CoberturaCotizacionDTO>();
			List<MovimientoCotizacionEndosoDTO> listaMovimientosBajaCobertura = obtenerListaMovimientosEndoso(Sistema.TIPO_MOV_BAJA_COBERTURA);
			//busqueda de las diferentes coberturas dadas de baja
			for(MovimientoCotizacionEndosoDTO movtoTMP : listaMovimientosBajaCobertura){
				CoberturaCotizacionDTO coberturaTMP = estructuraCotizacionEndosoAnterior.obtenerCoberturaContratadaPorId(
						movtoTMP.getNumeroInciso(), movtoTMP.getIdToSeccion(), movtoTMP.getIdToCobertura());
				if(coberturaTMP != null){
					SubRamoDTO subRamoTMP = coberturaTMP.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO();
					if(subRamoTMP.getIdTcSubRamo().compareTo(idTcSubRamo) == 0){
						listaCoberturasDescontratadas.add(coberturaTMP);
					}
				}
			}
			mapaCoberturasDesContratadasPorSubRamo.put(idTcSubRamo, listaCoberturasDescontratadas);
		}
		return listaCoberturasDescontratadas;
	}
	
	/**
	 * Calcula la lista de coberturas contratadas pertenecientes a un subramo, sin importar el inciso, ni secci�n.
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> obtenerCoberturasDesContratadasPorIncisoSubRamo(BigDecimal numeroInciso,BigDecimal idTcSubRamo){
		List<CoberturaCotizacionDTO> listaCoberturasDescontratadas = null;
		if(mapaCoberturasDesContratadasPorIncisoSubRamo == null)
			mapaCoberturasDesContratadasPorIncisoSubRamo = new HashMap<BigDecimal[], List<CoberturaCotizacionDTO>>();
		BigDecimal []keyIncisoSubRamo = new BigDecimal[2];
		keyIncisoSubRamo[0] = numeroInciso;
		keyIncisoSubRamo[1] = idTcSubRamo;
		listaCoberturasDescontratadas = mapaCoberturasDesContratadasPorIncisoSubRamo.get(keyIncisoSubRamo);
		if(listaCoberturasDescontratadas == null){
			listaCoberturasDescontratadas = new ArrayList<CoberturaCotizacionDTO>();
			List<MovimientoCotizacionEndosoDTO> listaMovimientosBajaCobertura = obtenerListaMovimientosEndoso(Sistema.TIPO_MOV_BAJA_COBERTURA);
			//busqueda de las diferentes coberturas dadas de baja
			for(MovimientoCotizacionEndosoDTO movtoTMP : listaMovimientosBajaCobertura){
				CoberturaCotizacionDTO coberturaTMP = estructuraCotizacionEndosoAnterior.obtenerCoberturaContratadaPorId(
						movtoTMP.getNumeroInciso(), movtoTMP.getIdToSeccion(), movtoTMP.getIdToCobertura());
				if(coberturaTMP != null){
					SubRamoDTO subRamoTMP = coberturaTMP.getCoberturaSeccionDTO().getCoberturaDTO().getSubRamoDTO();
					BigDecimal numeroIncisoTMP = coberturaTMP.getId().getNumeroInciso();
					if(subRamoTMP.getIdTcSubRamo().compareTo(idTcSubRamo) == 0 && 
							numeroIncisoTMP.compareTo(numeroInciso) == 0){
						listaCoberturasDescontratadas.add(coberturaTMP);
					}
				}
			}
			mapaCoberturasDesContratadasPorIncisoSubRamo.put(keyIncisoSubRamo, listaCoberturasDescontratadas);
		}
		return listaCoberturasDescontratadas;
	}
	
	/**
	 * Calcula la lista de los diferentes registros IncisoCotizacionDTO contratados y dados de baja en la cotizaci�n de endoso, utilizando 
	 * la lista de incisos del endoso anterior y la cotizacion actual, as� como los movimientos de la cotizaci�n.
	 * @return
	 * @throws SystemException
	 */
	public List<IncisoCotizacionDTO> listarIncisosEndoso() throws SystemException{
		if(listaTotalIncisosEndoso == null){
			listaTotalIncisosEndoso = new ArrayList<IncisoCotizacionDTO>();
			listaTotalIncisosEndoso.addAll(listaIncisos);
			listarIncisosDescontratados();
			listaTotalIncisosEndoso.addAll(listaIncisosDesContratados);
		}
		return listaTotalIncisosEndoso;
	}
	
	/**
	 * Calcula la lista de los diferentes incisos dados de baja en la cotizaci�n. Regresa los incisos registrados en la cotizaci�n anterior,
	 * que ya no est�n registrados en la cotizaci�n actual.
	 * @return
	 */
	public List<IncisoCotizacionDTO> listarIncisosDescontratados(){
		if (listaIncisosDesContratados == null){
			listaIncisosDesContratados = new ArrayList<IncisoCotizacionDTO>();
			List<MovimientoCotizacionEndosoDTO> listaMovimientosBajaInciso = obtenerListaMovimientosEndoso(Sistema.TIPO_MOV_BAJA_INCISO);
			//busqueda de las diferentes coberturas dadas de baja
			for(MovimientoCotizacionEndosoDTO movtoTMP : listaMovimientosBajaInciso){
				IncisoCotizacionDTO incisoCotizacion = estructuraCotizacionEndosoAnterior.obtenerInciso(movtoTMP.getNumeroInciso());
				if(incisoCotizacion != null){
					listaIncisosDesContratados.add(incisoCotizacion);
				}
			}
		}
		return listaIncisosDesContratados;
	}
	
	/**
	 * Calcula la lista de subincisos eliminados en el endoso actual
	 * @param idToSeccion
	 * @param numeroInciso
	 * @return
	 */
	public List<SubIncisoCotizacionDTO> obtenerSubIncisosEliminados(BigDecimal idToSeccion,BigDecimal numeroInciso){
		List<SubIncisoCotizacionDTO> listaSubIncisosEliminados = new ArrayList<SubIncisoCotizacionDTO>();
		if (this.listaSubIncisosCotizacion != null && 
				getEstructuraCotizacionEndosoAnterior().getListaSubIncisosCotizacion() != null){
			
			List<SubIncisoCotizacionDTO> listaSubIncisosEndosoActual = super.obtenerSubIncisos(idToSeccion, numeroInciso);
			List<SubIncisoCotizacionDTO> listaSubIncisosEndosoAnterior = getEstructuraCotizacionEndosoAnterior().obtenerSubIncisos(idToSeccion, numeroInciso);
			
			for(SubIncisoCotizacionDTO subIncisoEndAnterior : listaSubIncisosEndosoAnterior){
				boolean subincisoEncontrado = false;
				for(SubIncisoCotizacionDTO subIncisoEndActual : listaSubIncisosEndosoActual){
					if(subIncisoEndAnterior.getId().getNumeroSubInciso().compareTo(
							subIncisoEndActual.getId().getNumeroSubInciso()) == 0){
						subincisoEncontrado = true;
						break;
					}
				}
				if(!subincisoEncontrado){
					listaSubIncisosEliminados.add(subIncisoEndAnterior);
				}
			}
			
		}
		return listaSubIncisosEliminados;
	}
	
	/**
	 * Calcula la lista de movimientos de la cotizaci�n usando como criterio de filtrado la clave del movimiento. Si recibe nulo, regresa la lista
	 * completa de movimientos.
	 * @param claveTipoMovimiento
	 * @return
	 */
	public List<MovimientoCotizacionEndosoDTO> obtenerListaMovimientosEndoso(Short claveTipoMovimiento){
		List<MovimientoCotizacionEndosoDTO> listaMovimientosResultado = null;
		if (mapaMovimientosEndosoPorTipoMovto == null){
			mapaMovimientosEndosoPorTipoMovto = new HashMap<Short, List<MovimientoCotizacionEndosoDTO>>();
		}
		if(claveTipoMovimiento != null){
			listaMovimientosResultado = mapaMovimientosEndosoPorTipoMovto.get(claveTipoMovimiento);
			if(listaMovimientosResultado == null){
				listaMovimientosResultado = new ArrayList<MovimientoCotizacionEndosoDTO>();
				for(MovimientoCotizacionEndosoDTO movimientoEnCurso : listaMovimientosEndoso){
					if(movimientoEnCurso.getClaveTipoMovimiento().compareTo(claveTipoMovimiento) == 0){
						listaMovimientosResultado.add(movimientoEnCurso);
					}
				}
				mapaMovimientosEndosoPorTipoMovto.put(claveTipoMovimiento, listaMovimientosResultado);
			}
		}
		else{
			listaMovimientosResultado = listaMovimientosEndoso;
		}
		return listaMovimientosResultado;
	}

	private void registrarDiferenciasDeEndosoPorProducto(List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		if (cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto().intValue() != 
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto().intValue()) {
			ProductoDTO producto = cotizacionDTO.getSolicitudDTO().getProductoDTO();
			
			this.generarMovimientoGeneral(Sistema.MSG_MOV_PRODUCTO + " "+ producto.getDescripcion(),
					Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false,listaMovimientos);
		}
	}
	
	private void registrarDiferenciasDeEndosoPorTipoPoliza(List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		if (cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza().intValue() != 
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getTipoPolizaDTO().getIdToTipoPoliza().intValue()) {
			TipoPolizaDTO tipoPoliza = cotizacionDTO.getTipoPolizaDTO();
			generarMovimientoGeneral(Sistema.MSG_MOV_TIPO_POLIZA + " "
					+ tipoPoliza.getDescripcion(), Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false, listaMovimientos);
		}
	}
	
	private void registrarDiferenciasDeEndosoPorMoneda(List<MovimientoCotizacionEndosoDTO> listaMovimientos) {
		if (cotizacionDTO.getIdMoneda().intValue() != 
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getIdMoneda().intValue()) {
			String descripcionMoneda = UtileriasWeb.obtenerDescripcionMoneda(cotizacionDTO.getIdMoneda().intValue());
			
			generarMovimientoGeneral(Sistema.MSG_MOV_MONEDA + " "
					+ descripcionMoneda, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true, listaMovimientos);
		}
	}

	private void registrarDiferenciasDeEndosoPorAsegurado(List<MovimientoCotizacionEndosoDTO> listaMovimientos,String nombreUsuario) throws SystemException{
		CotizacionDTO cotizacionEndosoActual = cotizacionDTO;
		CotizacionDTO cotizacionUltimoEndoso = estructuraCotizacionEndosoAnterior.getCotizacionDTO();

		ClienteDTO aseguradoEndosoActual = null;
		if(cotizacionEndosoActual.getIdDomicilioAsegurado() != null && cotizacionEndosoActual.getIdDomicilioAsegurado().intValue() > 0){
			aseguradoEndosoActual = new ClienteDTO();
			aseguradoEndosoActual.setIdCliente(cotizacionEndosoActual.getIdToPersonaAsegurado());
			aseguradoEndosoActual.setIdDomicilio(cotizacionEndosoActual.getIdDomicilioAsegurado());
			aseguradoEndosoActual = ClienteDN.getInstancia().verDetalleCliente(aseguradoEndosoActual, nombreUsuario);
		}else{
			aseguradoEndosoActual = ClienteDN.getInstancia().verDetalleCliente(
					cotizacionEndosoActual.getIdToPersonaAsegurado(),nombreUsuario);
		}
		ClienteDTO aseguradoUltimoEndoso = null;
		
		if(cotizacionUltimoEndoso.getIdDomicilioAsegurado() != null && cotizacionUltimoEndoso.getIdDomicilioAsegurado().intValue() > 0){
			aseguradoUltimoEndoso = new ClienteDTO();
			aseguradoUltimoEndoso.setIdCliente(cotizacionUltimoEndoso.getIdToPersonaAsegurado());
			aseguradoUltimoEndoso.setIdDomicilio(cotizacionUltimoEndoso.getIdDomicilioAsegurado());
			aseguradoUltimoEndoso = ClienteDN.getInstancia().verDetalleCliente(aseguradoUltimoEndoso, nombreUsuario);
		}else{
			aseguradoEndosoActual = ClienteDN.getInstancia().verDetalleCliente(
					cotizacionUltimoEndoso.getIdToPersonaAsegurado(),nombreUsuario);
		}
	
		registrarDiferenciasEndosoPorPersona(aseguradoEndosoActual, aseguradoUltimoEndoso, 
				CLAVE_PERSONA_ASEGURADO, listaMovimientos, nombreUsuario);
	}
	
	private void registrarDiferenciasDeEndosoPorContratante(List<MovimientoCotizacionEndosoDTO> listaMovimientos,String nombreUsuario)throws SystemException {
		CotizacionDTO cotizacionEndosoActual = cotizacionDTO;
		CotizacionDTO cotizacionUltimoEndoso = estructuraCotizacionEndosoAnterior.getCotizacionDTO();

		ClienteDTO contratanteEndosoActual = null;
		if(cotizacionEndosoActual.getIdDomicilioContratante() != null && cotizacionEndosoActual.getIdDomicilioContratante().intValue() > 0){
			contratanteEndosoActual = new ClienteDTO();
			contratanteEndosoActual.setIdCliente(cotizacionEndosoActual.getIdToPersonaContratante());
			contratanteEndosoActual.setIdDomicilio(cotizacionEndosoActual.getIdDomicilioContratante());
			contratanteEndosoActual = ClienteDN.getInstancia().verDetalleCliente(contratanteEndosoActual, nombreUsuario);
		}else{
			contratanteEndosoActual = ClienteDN.getInstancia().verDetalleCliente(
					cotizacionEndosoActual.getIdToPersonaContratante(),nombreUsuario);
		}
		
		ClienteDTO contratanteUltimoEndoso = null;
		if(cotizacionUltimoEndoso.getIdDomicilioContratante() != null && cotizacionUltimoEndoso.getIdDomicilioContratante().intValue() > 0){
			contratanteUltimoEndoso = new ClienteDTO();
			contratanteUltimoEndoso.setIdCliente(cotizacionUltimoEndoso.getIdToPersonaContratante());
			contratanteUltimoEndoso.setIdDomicilio(cotizacionUltimoEndoso.getIdDomicilioContratante());
			contratanteUltimoEndoso = ClienteDN.getInstancia().verDetalleCliente(contratanteUltimoEndoso, nombreUsuario);
		}else{
			contratanteEndosoActual = ClienteDN.getInstancia().verDetalleCliente(
					cotizacionUltimoEndoso.getIdToPersonaContratante(),nombreUsuario);
		}		
		registrarDiferenciasEndosoPorPersona(contratanteEndosoActual, contratanteUltimoEndoso, 
				CLAVE_PERSONA_CONTRATANTE, listaMovimientos, nombreUsuario);
	}
	
	private void registrarDiferenciasDeEndosoGenerales(List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws ExcepcionDeAccesoADatos, SystemException{
		CotizacionDTO cotizacionEndosoActual = cotizacionDTO;
		CotizacionDTO cotizacionUltimoEndoso = estructuraCotizacionEndosoAnterior.getCotizacionDTO();
		
		//Se validan los documentos Anexos
		List<DocAnexoCotDTO> documentosActual = DocAnexoCotDN.getInstancia().listarDocumentosAnexosPorCotizacion(idToCotizacion);
		List<DocAnexoCotDTO> documentosUltimo = DocAnexoCotDN.getInstancia().listarDocumentosAnexosPorCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
		if(documentosActual != null && documentosUltimo!= null){
			if(documentosActual.size() == documentosUltimo.size()){//Mismo numero documentos
				for (int i = 0; i < documentosActual.size(); i++) {
					DocAnexoCotDTO actual = documentosActual.get(i);
					DocAnexoCotDTO ultimo = documentosUltimo.get(i);
					if(actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO &&
							ultimo.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO){
						//Se agrego Anexo
						generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
								Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo(), listaMovimientos);
					}else if (actual.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO &&
							ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
						//Se elimino Anexo
						generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
								Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
					}
				}
			}else if(documentosActual.size() > documentosUltimo.size()){//Se agregaron documentos
				for (int i = 0; i < documentosActual.size(); i++) {
					if(documentosUltimo.size()>= i+1){
						DocAnexoCotDTO actual = documentosActual.get(i);
						DocAnexoCotDTO ultimo = documentosUltimo.get(i);
						if(actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO &&
								ultimo.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO){
							//Se agrego Anexo
							generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo(), listaMovimientos);
						}else if (actual.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO &&
								ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
							//Se elimino Anexo
							generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
						}							
					}else{
						DocAnexoCotDTO actual = documentosActual.get(i);
						//Se agrego Anexo
						if (actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
							generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo(), listaMovimientos);
						}
					}
				}
			}else if(documentosActual.size() < documentosUltimo.size()){//Se eliminaron documentos
				for (int i = 0; i < documentosUltimo.size(); i++) {
					if(documentosActual.size()>= i+1){
						DocAnexoCotDTO actual = documentosActual.get(i);
						DocAnexoCotDTO ultimo = documentosUltimo.get(i);
						if(actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO &&
								ultimo.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO){
							//Se agrego Anexo
							generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo(), listaMovimientos);
						}else if (actual.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO &&
								ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
							//Se elimino Anexo
							generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
						}							
					}else{
						DocAnexoCotDTO ultimo = documentosUltimo.get(i);
						//Se elimino Anexo
						if (ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO)
							generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + ultimo.getDescripcionDocumentoAnexo(),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
					}
				}
			}
		}
		//Se validan los documentos Anexos de Reaseguro
		List<DocumentoAnexoReaseguroCotizacionDTO> documentosReaActual = DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtieneAnexosReasCotPorIdCotizacion(cotizacionEndosoActual.getIdToCotizacion());
		List<DocumentoAnexoReaseguroCotizacionDTO> documentosReaUltimo = DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtieneAnexosReasCotPorIdCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
			
		for(DocumentoAnexoReaseguroCotizacionDTO actual: documentosReaActual){
			DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId(cotizacionUltimoEndoso.getIdToCotizacion(), actual.getId().getIdToControlArchivo());
			DocumentoAnexoReaseguroCotizacionDTO docReaUltimo =  DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtienePorId(id);
			if(docReaUltimo == null){
				//Se elimino Anexo
				generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
						Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo(), listaMovimientos);
			}else{
				if(!docReaUltimo.getDescripcionDocumentoAnexo().equals(actual.getDescripcionDocumentoAnexo())){
					//Se elimino Anexo
					generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + docReaUltimo.getDescripcionDocumentoAnexo(),
							Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
					//Se agrego Anexo
					generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),
							Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo(), listaMovimientos);
				}
			}
		}
			
		for(DocumentoAnexoReaseguroCotizacionDTO ultimo: documentosReaUltimo){
			DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId(cotizacionEndosoActual.getIdToCotizacion(), ultimo.getId().getIdToControlArchivo());
			DocumentoAnexoReaseguroCotizacionDTO docReaActual =  DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtienePorId(id);
			if(docReaActual == null){
				//Se elimino Anexo
				generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + ultimo.getDescripcionDocumentoAnexo(),
						Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
			}
		}			
		//Se validan textos Adicionales
		TexAdicionalCotDTO textoCotActual = new TexAdicionalCotDTO();
		textoCotActual.setCotizacion(cotizacionEndosoActual);
		List<TexAdicionalCotDTO> textosCotActual = TexAdicionalCotDN.getInstancia().listarFiltrado(textoCotActual);
		
		TexAdicionalCotDTO textoCotUltimoEnd = new TexAdicionalCotDTO();
		textoCotUltimoEnd.setCotizacion(cotizacionUltimoEndoso);
		List<TexAdicionalCotDTO> textosCotUltimoEnd = TexAdicionalCotDN.getInstancia().listarFiltrado(textoCotUltimoEnd);
		if(textosCotActual.size() == textosCotUltimoEnd.size()){//Mismo numero de textos
			for (int i = 0; i < textosCotActual.size(); i++) {
				TexAdicionalCotDTO actual = textosCotActual.get(i);
				TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
				if(!actual.getDescripcionTexto().equals(ultimo.getDescripcionTexto())){
					//Registra Movimiento: cambio de texto adicional
					generarMovimientoAnexos(Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL + " "
							+ actual.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
				}
			}
		}else if(textosCotActual.size() > textosCotUltimoEnd.size()){//se agregaron textos
			for (int i = 0; i < textosCotActual.size(); i++) {
				if(textosCotUltimoEnd.size()>= i+1){
					TexAdicionalCotDTO actual = textosCotActual.get(i);
					TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
					if(!actual.getDescripcionTexto().equals(ultimo.getDescripcionTexto())){
						//Registra Movimiento: cambio de texto adicional
						generarMovimientoAnexos(Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL + " "
								+ actual.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);	
					}			
				}else{
					TexAdicionalCotDTO actual = textosCotActual.get(i);
						//Registra movimiento: alta de texto adicional
					generarMovimientoAnexos(Sistema.MSG_MOV_ALTA_TEXTO_ADICIONAL + actual.getDescripcionTexto(),
							Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);
				}
			}
		}else if (textosCotActual.size() < textosCotUltimoEnd.size()){//se eliminaron textos
			for (int i = 0; i < textosCotUltimoEnd.size(); i++) {
				if(textosCotActual.size()>= i+1){
					TexAdicionalCotDTO actual = textosCotActual.get(i);
					TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
					if(!actual.getDescripcionTexto().equals(ultimo.getDescripcionTexto())){
						//Registra Movimiento: cambio de texto adicional
						generarMovimientoAnexos(Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL + " "
								+ actual.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO, listaMovimientos);	
					}
				}else{
					TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
						//Registra movimiento: baja de texto adicional
					generarMovimientoAnexos(Sistema.MSG_MOV_BAJA_TEXTO_ADICIONAL + ultimo.getDescripcionTexto(),
							Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, ultimo.getIdToTexAdicionalCot(), listaMovimientos);
				}
			}
		}
	}
	
	private void registrarDiferenciasDeEndosoPorAgrupacion(List<MovimientoCotizacionEndosoDTO> listaMovimientos)throws ExcepcionDeAccesoADatos, SystemException {
		CotizacionDTO cotizacionEndosoActual = cotizacionDTO;
//		CotizacionDTO cotizacionUltimoEndoso = estructuraCotizacionEndosoAnterior.getCotizacionDTO();
		
		List<AgrupacionCotDTO> primerRiesgoActual = obtenerListaAgrupacionCotizacion();
		List<AgrupacionCotDTO> primerRiesgoUltimo = estructuraCotizacionEndosoAnterior.obtenerListaAgrupacionCotizacion();
		
		AgrupacionCotDTO agrupacionUltimoEnd = null;
		AgrupacionCotDTO agrupacionEndosoActual = null;
		Map<BigDecimal,SeccionDTO> mapaSecciones = new HashMap<BigDecimal, SeccionDTO>();
		
		for(AgrupacionCotDTO agrupacion: primerRiesgoActual){
			agrupacionUltimoEnd = estructuraCotizacionEndosoAnterior.obtenerAgrupacionCotizacionPorNumeroAgrupacion(agrupacion.getId().getNumeroAgrupacion());
			if(agrupacion.getClaveTipoAgrupacion() == Sistema.TIPO_PRIMER_RIESGO){
				if(agrupacionUltimoEnd == null){
					
					agrupacion.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
					//TODO generar este filtrado en c�digo java usando las listas precargadas 
					List<SeccionCotizacionDTO> seccionesPrimerRiesgo = 
						SeccionCotizacionDN.getInstancia().listarSeccionesPrimerRiesgo(agrupacion.getId().getIdToCotizacion());
					StringBuffer sb = new StringBuffer();
					for(SeccionCotizacionDTO seccionCot: seccionesPrimerRiesgo){
						sb.append(seccionCot.getSeccionDTO().getNombreComercial() + ", ");
					}
					String descripcionMovimiento = Sistema.MSG_MOV_ID_ALTA_PRIMER_RIESGO + 
						UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.alta.primerriesgo", sb.toString(),
									Sistema.FORMATO_MONEDA.format(agrupacion.getValorSumaAsegurada()));
					generarMovimientoPrimerRiesgoLUC(agrupacion, descripcionMovimiento, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, listaMovimientos);

				}else{
					if(agrupacion.getValorSumaAsegurada().doubleValue() != agrupacionUltimoEnd.getValorSumaAsegurada().doubleValue()){
						List<SeccionCotizacionDTO> seccionesPrimerRiesgo = SeccionCotizacionDN.getInstancia().
								listarSeccionesPrimerRiesgo(agrupacion.getId().getIdToCotizacion());
						
						StringBuffer sb = new StringBuffer();
						for(SeccionCotizacionDTO seccionCot: seccionesPrimerRiesgo){
							sb.append(seccionCot.getSeccionDTO().getNombreComercial() + " ");
						}
						
						String descripcionMovimiento =Sistema.MSG_MOV_ID_MODIFICACION_PRIMER_RIESGO +
								UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.modificacion.primerriesgo",
										sb.toString(), Sistema.FORMATO_MONEDA.format(agrupacion.getValorSumaAsegurada())); 
						generarMovimientoPrimerRiesgoLUC(agrupacion, descripcionMovimiento, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, listaMovimientos);
					}
				}
			}else{//No es agrupaci�n de primer riesgo
				if(agrupacionUltimoEnd == null){
					SeccionDTO seccionDTO = obtenerSeccion(agrupacion.getIdToSeccion(), mapaSecciones);
					
					String descripcionMovimiento = Sistema.MSG_MOV_ID_ALTA_LUC +UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.alta.luc",seccionDTO.getNombreComercial(), 
							Sistema.FORMATO_MONEDA.format(agrupacion.getValorSumaAsegurada()));
					generarMovimientoPrimerRiesgoLUC(agrupacion, descripcionMovimiento, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, listaMovimientos);
				}else{
					if(agrupacion.getValorSumaAsegurada().doubleValue() != agrupacionUltimoEnd.getValorSumaAsegurada().doubleValue()){
						SeccionDTO seccionDTO = obtenerSeccion(agrupacion.getIdToSeccion(), mapaSecciones);
						
						String descripcionMovimiento = Sistema.MSG_MOV_ID_MODIFICACION_LUC +UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.modificacion.luc",
								seccionDTO.getNombreComercial(), Sistema.FORMATO_MONEDA.format(agrupacion.getValorSumaAsegurada()));
						generarMovimientoPrimerRiesgoLUC(agrupacion, descripcionMovimiento, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, listaMovimientos);
					}
				}
			}
		}
		for(AgrupacionCotDTO agrupacionUltimo: primerRiesgoUltimo){
			agrupacionEndosoActual = obtenerAgrupacionCotizacionPorNumeroAgrupacion(agrupacionUltimo.getId().getNumeroAgrupacion());
//			agrupacionesTemp = primerRiesgoLUCDN.listarFiltrado(agrupacionUltimo);		
			agrupacionUltimo.setValorCuota(agrupacionUltimo.getValorCuota() * -1D);
			agrupacionUltimo.setValorPrimaneta(agrupacionUltimo.getValorPrimaneta() * -1D);
			agrupacionUltimo.setValorSumaAsegurada(agrupacionUltimo.getValorSumaAsegurada() * -1D);
			
			if (agrupacionUltimo.getClaveTipoAgrupacion() == Sistema.TIPO_PRIMER_RIESGO) {
				if (agrupacionEndosoActual == null) {
					List<SeccionCotizacionDTO> seccionesPrimerRiesgo = SeccionCotizacionDN.getInstancia().
							listarSeccionesPrimerRiesgo(agrupacionUltimo.getId().getIdToCotizacion());
					StringBuffer sb = new StringBuffer();
					for (SeccionCotizacionDTO seccionCot : seccionesPrimerRiesgo) {
						sb.append(seccionCot.getSeccionDTO().getNombreComercial()+ ", ");
					}
					String descripcionMovimiento =Sistema.MSG_MOV_ID_BAJA_PRIMER_RIESGO +UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.baja.primerriesgo",sb.toString(),
							Sistema.FORMATO_MONEDA.format(agrupacionUltimo.getValorSumaAsegurada())); 
					generarMovimientoPrimerRiesgoLUC(agrupacionUltimo, descripcionMovimiento, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, listaMovimientos);
				}
			}else{//no es primer riesgo
				if (agrupacionEndosoActual == null) {

					SeccionDTO seccionDTO = obtenerSeccion(agrupacionUltimo.getIdToSeccion(), mapaSecciones);
					String descripcionMovimiento = Sistema.MSG_MOV_ID_BAJA_LUC + UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.baja.luc",
							seccionDTO.getNombreComercial(),Sistema.FORMATO_MONEDA.format(agrupacionUltimo.getValorSumaAsegurada()));
					generarMovimientoPrimerRiesgoLUC(agrupacionUltimo, descripcionMovimiento, Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, listaMovimientos);
				}
			}
			
		}
	}
	
	private SeccionDTO obtenerSeccion(BigDecimal idToSeccion,Map<BigDecimal,SeccionDTO> mapaSecciones) throws ExcepcionDeAccesoADatos, SystemException{
		SeccionDTO seccionDTO = mapaSecciones.get(idToSeccion);
		if(seccionDTO == null){
			seccionDTO = SeccionDN.getInstancia().getPorId(idToSeccion);
			mapaSecciones.put(idToSeccion, seccionDTO);
		}
		return seccionDTO;
	}
	
	public Integer calculaTipoEndoso() throws SystemException{
		 Integer tipoEndoso = new Integer(2);
		 
		 if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
			 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_CANCELACION);
		 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){
			 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_REHABILITACION);
		 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
			 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO);
		 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_DECLARACION){
			 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_AUMENTO);
		 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION){
			BigDecimal primaNetaCotizacion = BigDecimal.ZERO;
			for (MovimientoCotizacionEndosoDTO movimiento : listaMovimientosEndoso) {
				if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0)
					primaNetaCotizacion = primaNetaCotizacion.add(movimiento.getValorDiferenciaPrimaNeta());
			}
			if(primaNetaCotizacion.doubleValue() == 0){
				tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_CAMBIO_DATOS);
			}else if(primaNetaCotizacion.doubleValue() > 0){
				tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_AUMENTO);
			}else if(primaNetaCotizacion.doubleValue() < 0){
				tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_DISMINUCION);
			}
		 }
		 
		 return tipoEndoso;
	 }
	
//	public void consultarTotalesResumenCotizacion(EndosoDTO ultimoEndoso,List<SoporteResumen> resumenComisiones) throws SystemException{
//		BigDecimal factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
//		BigDecimal primaNetaCotizacion = BigDecimal.ZERO;
//		BigDecimal pctPagoFraccionado = BigDecimal.ZERO;
//		BigDecimal derechosPoliza = BigDecimal.ZERO;
//		BigDecimal factorIVA = BigDecimal.ZERO;
//		BigDecimal montoIVA = BigDecimal.ZERO;
//		BigDecimal valorBonifComision = BigDecimal.ZERO;
//		BigDecimal valorComision = BigDecimal.ZERO;
//		BigDecimal valorComisionRPF = BigDecimal.ZERO;
//		Double ivaObtenido=null;
//			
//		List<MovimientoCotizacionEndosoDTO> movimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
//		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
//			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
//				
//				Integer tipoEndoso = calculaTipoEndoso();
//				EndosoIDTO endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(ultimoEndoso.getId().getIdToPoliza().toString() + "|"
//								+ ultimoEndoso.getId().getNumeroEndoso(), cotizacionDTO.getCodigoUsuarioModificacion(),cotizacionDTO.getFechaInicioVigencia(),tipoEndoso);
//
//				factor = UtileriasWeb.getFactorVigencia(endosoIDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
//
//				primaNetaCotizacion = BigDecimal.valueOf(CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()));
//				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion.doubleValue());
//				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
//				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(endosoIDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
//				cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
//				CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).modificar(cotizacionDTO);
//			}else{
//				factor = UtileriasWeb.getFactorVigencia(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
//
//				for (MovimientoCotizacionEndosoDTO movimiento : listaMovimientosEndoso) {
//					if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0)
//						primaNetaCotizacion = primaNetaCotizacion.add(movimiento.getValorDiferenciaPrimaNeta());
//				}
//				cotizacionDTO.setPrimaNetaAnual(primaNetaCotizacion.doubleValue());
//				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
//				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
//			}
//		}
////		else{
////			if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
////					cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
////				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual());
////				cotizacionDTO.setDiasPorDevengar(365D);
////			}else{
////				primaNetaCotizacion = BigDecimal.valueOf(cotizacionDTO.getPrimaNetaAnual()).multiply(factor);
////				cotizacionDTO.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia()));
////			}
//			
////		}
//		List<SoporteResumen> resumenComisionesTmp = new ArrayList<SoporteResumen>();
//		//Prima neta de la cotizacion
//		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion.doubleValue());
//
//		//Obtiene el limite de SA
//		List<ParametroGeneralDTO> parametrosLimiteSA = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("id.codigoParametroGeneral",BigDecimal.valueOf(30010D));
//				
//		double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
//		
//		//Obtiene si existe una agrupacion a Primer riesgo
//		AgrupacionCotDTO agrupacion = obtenerAgrupacionCotizacionPorTipoAgrupacion(Sistema.TIPO_PRIMER_RIESGO);
//			PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),Sistema.TIPO_PRIMER_RIESGO);	
//
//		
//		boolean esEndosoCFP = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO;
//		boolean esEndosoCancelacion = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION;
//				 
//		//Obtiene % por forma de Pago	
//		CotizacionDTO cotizacionTmp = null;
//		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario() == null || cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0 || esEndosoCFP){
//			pctPagoFraccionado = BigDecimal.valueOf(CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO));						
//			cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_SISTEMA, pctPagoFraccionado.doubleValue(), false, false);
//		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){
//			cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getValorRecargoPagoFraccionadoUsuario(), false, false);
//		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
//			cotizacionTmp = CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_PORCENTAJE, cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario(), false, false);
//		}
//		
//		pctPagoFraccionado = BigDecimal.valueOf(cotizacionTmp.getPorcentajeRecargoPagoFraccionadoUsuario());				
//		cotizacionDTO.setPorcentajePagoFraccionado(pctPagoFraccionado.doubleValue());
//		
//		//Se obtienen los gastos de expedicion
//		if(cotizacionDTO.getValorDerechosUsuario()==null || cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_SISTEMA) == 0 || esEndosoCFP || esEndosoCancelacion){
//			derechosPoliza = BigDecimal.valueOf(CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO,false));
//			boolean sinAutorizacion = false;
//			if(esEndosoCFP || esEndosoCancelacion)
//				sinAutorizacion = true;						
//			CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_SISTEMA, derechosPoliza.doubleValue(), false, sinAutorizacion);
//		}else{
//			derechosPoliza = BigDecimal.valueOf(cotizacionDTO.getValorDerechosUsuario());
//		}
//		
//		cotizacionDTO.setDerechosPoliza(derechosPoliza.doubleValue());
//		
//		//Se obtiene % de IVA
//		ivaObtenido = CotizacionDN.getInstancia(null).obtenerPorcentajeIVA(cotizacionDTO.getIdToCotizacion());
//		if(ivaObtenido==null){
//		    throw new SystemException("No se pudo obtener el valor para el IVA.");
//		}
//		
//		factorIVA = BigDecimal.valueOf(ivaObtenido).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
//		cotizacionDTO.setFactorIVA(factorIVA.multiply(BigDecimal.valueOf(100D)).doubleValue());
//
//		// se guardan las comisiones q se eliminaron en este arreglo
//		int numeroEndosoAnterior = 0;
//		EndosoDTO endosoDTO = null;
//		List<SoporteResumen> resumenComisionesEliminadasTmp = new ArrayList<SoporteResumen>();
//		EndosoDTO endosoActualDTO = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
//		if (endosoActualDTO != null && endosoActualDTO.getId().getNumeroEndoso().intValue() != 0){
//			numeroEndosoAnterior = endosoActualDTO.getId().getNumeroEndoso().intValue() - 1;
//			endosoDTO = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPorId(new EndosoId(endosoActualDTO.getId().getIdToPoliza(), (short)numeroEndosoAnterior));
//		}else{
//			endosoDTO = EndosoDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
//		}
//		
//		
//
//		//se obtiene correctamente el monto de bonificacion de la comision
//		List<CoberturaCotizacionDTO> coberturas= new ArrayList<CoberturaCotizacionDTO>();
//		CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
//		if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!= null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() > 0){
//			for(MovimientoCotizacionEndosoDTO movimiento: movimientos){
//				if(movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() > 0){
//					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
//					CoberturaCotizacionId id = new CoberturaCotizacionId();
//					id.setIdToCotizacion(movimiento.getIdToCotizacion());
//					id.setNumeroInciso(movimiento.getNumeroInciso());
//					id.setIdToSeccion(movimiento.getIdToSeccion());
//					id.setIdToCobertura(movimiento.getIdToCobertura());
//					coberturaCotizacionDTO.setId(id);
//					coberturaCotizacionDTO = coberturaCotizacionDN.getPorId(coberturaCotizacionDTO);
//					if(coberturaCotizacionDTO != null){
//						coberturaCotizacionDTO.setValorPrimaNeta(movimiento.getValorDiferenciaPrimaNeta().doubleValue());
//						coberturas.add(coberturaCotizacionDTO);
//					}else{
//						CoberturaCotizacionDTO coberturaCotizacionTmp = new CoberturaCotizacionDTO();
//						CoberturaCotizacionId idTmp = new CoberturaCotizacionId();
//						idTmp.setIdToCotizacion(endosoDTO.getIdToCotizacion());
//						idTmp.setNumeroInciso(movimiento.getNumeroInciso());
//						idTmp.setIdToSeccion(movimiento.getIdToSeccion());
//						idTmp.setIdToCobertura(movimiento.getIdToCobertura());
//						coberturaCotizacionTmp.setId(idTmp);
//						coberturaCotizacionTmp = coberturaCotizacionDN.getPorId(coberturaCotizacionTmp);						
//						
//						ComisionCotizacionId idComis = new ComisionCotizacionId();
//						idComis.setIdTcSubramo(coberturaCotizacionTmp.getIdTcSubramo());
//						idComis.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
//						
//						ComisionCotizacionDTO comision = null;
//						SoporteResumen resumenComision = new SoporteResumen();
//						Double sumaAseguradaIncendio = 0D;					
//						
//						if (coberturaCotizacionTmp.getIdTcSubramo().intValue()== 1) {
//							limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
//							// Se obtiene la SA de Incendio por Inciso
//							sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
//									.obtenerSACoberturasBasicasIncendioPorCotizacion(
//											coberturaCotizacionTmp.getId().getIdToCotizacion());
//							if (agrupacion != null) {
//								if (coberturaCotizacionTmp.getNumeroAgrupacion().intValue() == agrupacion
//										.getId().getNumeroAgrupacion().intValue()) {
//									idComis.setTipoPorcentajeComision(Sistema.TIPO_PRR);
//								}else{
//									idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);
//								}
//							} else {
//								if (sumaAseguradaIncendio < limiteSA) {
//									idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);
//								} else if (sumaAseguradaIncendio >= limiteSA) {
//									idComis.setTipoPorcentajeComision(Sistema.TIPO_RCI);
//								}else{
//									idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);	
//								}
//							}
//						} else {
//							idComis.setTipoPorcentajeComision(Sistema.TIPO_RO);
//						}
//						comision = ComisionCotizacionDN.getInstancia().getPorId(idComis);		
//						//valorComision = valorComision.add(BigDecimal.valueOf(coberturaCotizacionTmp.getValorPrimaNeta()).multiply(comision.getPorcentajeComisionCotizacion()).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
//						SubRamoDTO subRamoDTO = new SubRamoDTO();
//						subRamoDTO.setIdTcSubRamo(coberturaCotizacionTmp.getIdTcSubramo());
//						
//						subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoDTO);
//
//						resumenComision.setMontoComision((coberturaCotizacionTmp.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D)*-1);
//						resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
//						resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
//						resumenComision.setMontoComisionCedida((resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D)*-1);
//						resumenComision.setId(comision.getId());
//						resumenComisionesEliminadasTmp.add(resumenComision);						
//						
//					}
//				}
//			}
//		}else{
//			coberturas = coberturaCotizacionDN.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
//		}
//			
//		
//
//		for(CoberturaCotizacionDTO cobertura: coberturas){
//			valorBonifComision = valorBonifComision.add(BigDecimal.valueOf(cobertura.getValorPrimaNeta()).multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
//			ComisionCotizacionId id = new ComisionCotizacionId();
//			id.setIdTcSubramo(cobertura.getIdTcSubramo());
//			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
//
//			ComisionCotizacionDTO comision = null;
//			SoporteResumen resumenComision = new SoporteResumen();
//			Double sumaAseguradaIncendio = 0D;
//
//			if (cobertura.getIdTcSubramo().intValue()== 1) {
//				limiteSA = limiteSA / cotizacionDTO.getTipoCambio();
//				// Se obtiene la SA de Incendio por Inciso
//				sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
//						.obtenerSACoberturasBasicasIncendioPorCotizacion(
//								cobertura.getId().getIdToCotizacion());
//				if (agrupacion != null) {
//					if (cobertura.getNumeroAgrupacion().intValue() == agrupacion
//							.getId().getNumeroAgrupacion().intValue()) {
//						id.setTipoPorcentajeComision(Sistema.TIPO_PRR);
//					}else{
//						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
//					}
//				} else {
//					if (sumaAseguradaIncendio < limiteSA) {
//						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
//					} else if (sumaAseguradaIncendio >= limiteSA) {
//						id.setTipoPorcentajeComision(Sistema.TIPO_RCI);
//					}else{
//						id.setTipoPorcentajeComision(Sistema.TIPO_RO);	
//					}
//				}
//			} else {
//				id.setTipoPorcentajeComision(Sistema.TIPO_RO);
//			}
//			comision = ComisionCotizacionDN.getInstancia().getPorId(id);		
//			valorComision = valorComision.add(BigDecimal.valueOf(cobertura.getValorPrimaNeta() * factor.doubleValue()).multiply(comision.getPorcentajeComisionCotizacion()).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP));
//			SubRamoDTO subRamoDTO = new SubRamoDTO();
//			subRamoDTO.setIdTcSubRamo(cobertura.getIdTcSubramo());
//			
//			subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(subRamoDTO);
//
//			resumenComision.setMontoComision(cobertura.getValorPrimaNeta() * comision.getPorcentajeComisionCotizacion().doubleValue() / 100D);
//			resumenComision.setDescripcionRamo(subRamoDTO.getDescripcionSubRamo());
//			resumenComision.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
//			resumenComision.setMontoComisionCedida(resumenComision.getMontoComision() * cotizacionDTO.getPorcentajebonifcomision() / 100D);
//			resumenComision.setId(comision.getId());
//			resumenComisionesTmp.add(resumenComision);
//		}
//		
//		// valorBonifComision = PrimaNeta de la cotizacion * porcentajeBonifComision/100		
//		cotizacionDTO.setMontoBonificacionComision(valorComision.multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());		
//		
//		EndosoDTO endosoEmitido = EndosoDN.getInstancia(null).buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
//		boolean esEndosoCE = false;
//		boolean esEndosoRE = false;
//		if(endosoEmitido != null){
//			esEndosoCE = endosoEmitido.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CE;
//			esEndosoRE = endosoEmitido.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_RE;
//		}
//		if(esEndosoCE || esEndosoRE){
//			primaNetaCotizacion = new BigDecimal(endosoEmitido.getValorPrimaNeta());
//			cotizacionDTO.setMontoBonificacionComision(endosoEmitido.getValorBonifComision());
//			cotizacionDTO.setMontoRecargoPagoFraccionado(endosoEmitido.getValorRecargoPagoFrac());
//			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(endosoEmitido.getValorBonifComisionRPF());
//			cotizacionDTO.setDerechosPoliza(endosoEmitido.getValorDerechos());			
//		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso()!=null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
//			BigDecimal idToPoliza = cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada();
//			double diasPorDevengar = UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
//			mx.com.afirme.midas.poliza.PolizaDN.getInstancia().setMontosRFPAcumulados(idToPoliza, diasPorDevengar, cotizacionDTO);
//			cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() * -1D);
//			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado() * -1D);
//		}else{
//			// valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
//			cotizacionDTO.setMontoRecargoPagoFraccionado(primaNetaCotizacion.multiply(pctPagoFraccionado).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());
//					
//			valorComisionRPF = valorComision.multiply(pctPagoFraccionado).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP);
//			// valorBonifComRecPagoFrac = valorRecargoPagoFrac * porcentajeBonifComision/100
//			cotizacionDTO.setMontoBonificacionComisionPagoFraccionado(valorComisionRPF.multiply(BigDecimal.valueOf(cotizacionDTO.getPorcentajebonifcomision())).divide(BigDecimal.valueOf(100D), 12, RoundingMode.HALF_UP).doubleValue());		
//		}
//								
//		// valorIVA = (primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
//		montoIVA = primaNetaCotizacion.add(BigDecimal.valueOf(cotizacionDTO.getMontoRecargoPagoFraccionado())).add(BigDecimal.valueOf(cotizacionDTO.getDerechosPoliza())).subtract(BigDecimal.valueOf(cotizacionDTO.getMontoBonificacionComision())).subtract(BigDecimal.valueOf(cotizacionDTO.getMontoBonificacionComisionPagoFraccionado())).multiply(factorIVA);
//		cotizacionDTO.setMontoIVA(montoIVA.doubleValue());
//
//		//Prime neta de la cotizacion
//		cotizacionDTO.setPrimaNetaCotizacion(primaNetaCotizacion.doubleValue() - cotizacionDTO.getMontoBonificacionComision());
//		cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionDTO.getMontoBonificacionComisionPagoFraccionado());
//		
//		// valorPrimaTotal = primaNeta + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA		
//		cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getPrimaNetaCotizacion()
//				+ cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()
//				+ cotizacionDTO.getDerechosPoliza().doubleValue()
//				+ montoIVA.doubleValue());
//		//set comisiones 
//		resumenComisionesTmp.addAll(resumenComisionesEliminadasTmp);
//		this.ordenarAgruparComisiones(resumenComisionesTmp, resumenComisiones);
//		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null && cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
//			if(ultimoEndoso.getIdToCotizacion()!= null){
//				CotizacionDTO cotizacionUltimoEndoso = CotizacionDN.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).getPorId(ultimoEndoso.getIdToCotizacion());
//				cotizacionUltimoEndoso.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
//				List<CoberturaCotizacionDTO> coberturasUltimoEndoso = coberturaCotizacionDN.listarCoberturasContratadas(cotizacionDTO.getIdToCotizacion());
//				calcularTotalesResumenCotizacion(cotizacionUltimoEndoso, coberturasUltimoEndoso);
//				cotizacionDTO.setPrimaNetaCotizacion(0D);
//				cotizacionDTO.setMontoRecargoPagoFraccionado(cotizacionDTO.getMontoRecargoPagoFraccionado() - cotizacionUltimoEndoso.getMontoRecargoPagoFraccionado());				
//				cotizacionDTO.setDerechosPoliza(0D);				
//				cotizacionDTO.setMontoIVA(cotizacionDTO.getMontoRecargoPagoFraccionado() * factorIVA.doubleValue());
//				cotizacionDTO.setPrimaNetaTotal(cotizacionDTO.getMontoRecargoPagoFraccionado() + cotizacionDTO.getMontoIVA());
//				
//				cotizacionDTO.setValorRecargoPagoFraccionadoUsuario(cotizacionDTO.getMontoRecargoPagoFraccionado());
//				CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_USUARIO_MONTO, cotizacionDTO.getMontoRecargoPagoFraccionado(), false, true);
//				
//				cotizacionDTO.setValorDerechosUsuario(cotizacionDTO.getDerechosPoliza());
//				CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_USUARIO, cotizacionDTO.getDerechosPoliza(), false, true);
//			}
//		}
//	}

	public void calcularDiferenciasDeEndosoDeCambioFormaPago(String nombreUsuario) throws SystemException{
		CotizacionDTO cotizacionEndosoActual = cotizacionDTO;
		
		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);
		
		List<MovimientoCotizacionEndosoDTO> listaMovimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		//Tipo de Endoso
		generarMovimientoGeneral(Sistema.MSG_MOV_TIPO_ENDOSO + " "+
				UtileriasWeb.getDescripcionCatalogoValorFijo(40,cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().intValue()), 
				Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false, listaMovimientos);
//		this.registraMovimientoGeneral(cotizacionEndosoActual.getIdToCotizacion(), Sistema.MSG_MOV_TIPO_ENDOSO + " "+ UtileriasWeb.getDescripcionCatalogoValorFijo(40,
//						cotizacionEndosoActual.getSolicitudDTO().getClaveTipoEndoso().intValue()),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
		
		registrarDiferenciasDeEndosoPorFormaPago(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorFormaPago(cotizacionEndosoActual,cotizacionUltimoEndoso);
		sn.registrarMovimientos(listaMovimientos);
		consultarMovimientosEndoso(nombreUsuario);
	}
	
	public void calcularDiferenciasDeEndosoDeDeclaracion(String nombreUsuario) throws SystemException{

		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);

		List<MovimientoCotizacionEndosoDTO> listaMovimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		registrarDiferenciasDeEndosoPorSeccion(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorSeccion(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		sn.registrarMovimientos(listaMovimientos);
		consultarMovimientosEndoso(nombreUsuario);
	}
	
	/**
	 * Calcula y registra los movimientos realizados a la p�liza en la cotizaci�n del endoso.
	 * Usado para endosos de tipo Cancelaci�n de p�liza.
	 */
	public void calcularDiferenciasDeEndosoDeCancelacion(String nombreUsuario) throws SystemException{
		if(listaCoberturasContratadasCotizacion == null){
			consultarCoberturasContratadasCotizacion();
		}
		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);
		
		List<CoberturaCotizacionDTO> coberturasUltimoEndoso = listaCoberturasContratadasCotizacion;
		
		List<MovimientoCotizacionEndosoDTO> listaMovimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		
		for(CoberturaCotizacionDTO cobertura: coberturasUltimoEndoso){
			CoberturaDTO coberturaDTO = cobertura.getCoberturaSeccionDTO().getCoberturaDTO();
			generarMovimientoBajaCobertura(cobertura, coberturaDTO.getDescripcion(), Sistema.NO_AGRUPADO, cobertura, listaMovimientos);
//			this.registraMovimientoBajaCobertura(cobertura, coberturaDTO.getDescripcion(), Sistema.NO_AGRUPADO,cobertura);				
		}
		
		sn.registrarMovimientos(listaMovimientos);
		consultarMovimientosEndoso(nombreUsuario);
	}
	
	/**
	 * Calcula y registra los movimientos realizados a la p�liza que endosa la cotizaci�n.
	 * Usado para endosos de tipo Modificaci�n de p�liza.
	 */
	public void calcularDiferenciasDeEndoso(String nombreUsuario) throws SystemException{
		
		CotizacionDTO cotizacionEndosoActual = getCotizacionDTO();
		
//		CotizacionDTO cotizacionUltimoEndoso = estructuraCotizacionEndosoAnterior.getCotizacionDTO();
		
		List<MovimientoCotizacionEndosoDTO> listaMovimientos = new ArrayList<MovimientoCotizacionEndosoDTO>();
		//Tipo de Endoso
		generarMovimientoGeneral(Sistema.MSG_MOV_TIPO_ENDOSO + " "
				+ UtileriasWeb.getDescripcionCatalogoValorFijo(40,cotizacionEndosoActual.getSolicitudDTO()
								.getClaveTipoEndoso().intValue()),
				Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false, listaMovimientos);
		
		registrarDiferenciasDeEndosoPorProducto(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorProducto(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorTipoPoliza(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorTipoPoliza(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorFormaPago(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorFormaPago(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorMedioPago(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorMedioPago(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorMoneda(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorMoneda(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorVigencia(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorVigencia(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorAgente(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorAgente(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorAsegurado(listaMovimientos, nombreUsuario);
//		this.registrarDiferenciasDeEndosoPorAsegurado(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorContratante(listaMovimientos, nombreUsuario);
//		this.registrarDiferenciasDeEndosoPorContratante(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoGenerales(listaMovimientos);
//		this.registrarDiferenciasDeEndosoGenerales(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorAgrupacion(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorAgrupacion(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorInciso(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorInciso(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		registrarDiferenciasDeEndosoPorSeccion(listaMovimientos);
//		this.registrarDiferenciasDeEndosoPorSeccion(cotizacionEndosoActual,
//				cotizacionUltimoEndoso);
		
		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);

		sn.registrarMovimientos(listaMovimientos);
		
		consultarMovimientosEndoso(nombreUsuario);
	}
	
	private void registrarDiferenciasDeEndosoPorInciso(List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws SystemException{
		List<IncisoCotizacionDTO> incisosEndosoActual = listarIncisos();
		List<IncisoCotizacionDTO> incisosUltimoEndoso = estructuraCotizacionEndosoAnterior.listarIncisos();
		
		for(IncisoCotizacionDTO incisoEndosoActual: incisosEndosoActual){
			IncisoCotizacionDTO incisoCotTmp = estructuraCotizacionEndosoAnterior.obtenerInciso(incisoEndosoActual.getId().getNumeroInciso());
			if(incisoCotTmp != null){
				this.generarDiferenciasDeIncisos(incisoEndosoActual, incisoCotTmp,listaMovimientos);
			}else{
				generarMovimientoInciso(incisoEndosoActual.getId().getNumeroInciso(),
						Sistema.TIPO_MOV_ALTA_INCISO, Sistema.MSG_MOV_ALTA_INCISO, Sistema.NO_AGRUPADO, listaMovimientos);
				List<CoberturaCotizacionDTO> coberturasContratadasEndosoActual = obtenerCoberturasContratadasPorInciso(incisoEndosoActual.getId().getNumeroInciso());
				
				for (CoberturaCotizacionDTO cobertura : coberturasContratadasEndosoActual) {
					generarMovimientoAltaCobertura(cobertura, 
						cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion(), Sistema.SI_AGRUPADO, listaMovimientos);
				}	
			}
		}

		for(IncisoCotizacionDTO incisoUltimoEndoso: incisosUltimoEndoso){
			IncisoCotizacionDTO incisoCotTmp = obtenerInciso(incisoUltimoEndoso.getId().getNumeroInciso());
			if(incisoCotTmp == null){
				generarMovimientoInciso(incisoUltimoEndoso.getId().getNumeroInciso(),Sistema.TIPO_MOV_BAJA_INCISO,
						Sistema.MSG_MOV_BAJA_INCISO, Sistema.NO_AGRUPADO, listaMovimientos);
				
				List<CoberturaCotizacionDTO> coberturasContratadasUltimoEndoso = estructuraCotizacionEndosoAnterior.
					obtenerCoberturasContratadasPorInciso(incisoUltimoEndoso.getId().getNumeroInciso());
				
				for (CoberturaCotizacionDTO cobertura : coberturasContratadasUltimoEndoso) {
					String descripcion = cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion();
					generarMovimientoBajaCobertura(cobertura, descripcion, Sistema.SI_AGRUPADO,cobertura, listaMovimientos);
				}						
			}
		}
	}
	
	private void registrarDiferenciasDeEndosoPorSeccion(List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws SystemException{

		List<IncisoCotizacionDTO> incisosEndosoActual = listarIncisos();

		consultarSeccionesNoContratadas();
		estructuraCotizacionEndosoAnterior.consultarSeccionesNoContratadas();
		
		for (IncisoCotizacionDTO incisoEndosoActual: incisosEndosoActual) {

			List<SeccionCotizacionDTO> seccionesEndosoActual = obtenerListaCompletaSeccionesPorInciso(incisoEndosoActual.getId().getNumeroInciso());
			for(SeccionCotizacionDTO seccionEndosoActual: seccionesEndosoActual){
				SeccionCotizacionDTO seccionUltimoEndoso = estructuraCotizacionEndosoAnterior.obtenerSeccionPorIncisoOmiteContrato(
						seccionEndosoActual.getId().getIdToSeccion(), seccionEndosoActual.getId().getNumeroInciso());
				
				if(seccionUltimoEndoso != null){
					if(existenDiferenciasDeEndosoPorSubInciso(seccionEndosoActual, seccionUltimoEndoso,listaMovimientos)){
						generarMovimientoSeccion(seccionEndosoActual, Sistema.TIPO_MOV_MODIFICACION_SECCION,
								Sistema.MSG_MOV_MODIFICACION_SUBINCISO_LISTADO,Sistema.NO_AGRUPADO, listaMovimientos);
					}					
					if(seccionEndosoActual.getClaveContrato() == Sistema.CONTRATADO &&
							seccionUltimoEndoso.getClaveContrato() == Sistema.CONTRATADO){
						registraDiferenciasDeEndosoPorSeccion(seccionEndosoActual, seccionUltimoEndoso, listaMovimientos);
					}else if(seccionEndosoActual.getClaveContrato() == Sistema.NO_CONTRATADO &&
							seccionUltimoEndoso.getClaveContrato() == Sistema.CONTRATADO){ //baja de seccion
						
						List<CoberturaCotizacionDTO> coberturasEndosoActual = obtenerListaCompletaCoberturas(
								seccionEndosoActual.getId().getNumeroInciso(), seccionEndosoActual.getId().getIdToSeccion());
						generarMovimientoSeccion(seccionEndosoActual, Sistema.TIPO_MOV_BAJA_SECCION, UtileriasWeb.getMensajeRecurso(
									Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.baja.seccion"),Sistema.NO_AGRUPADO, listaMovimientos);
						
						for(CoberturaCotizacionDTO coberturaEndosoActual: coberturasEndosoActual){
							if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO){
								CoberturaDTO cobertura = coberturaEndosoActual.getCoberturaSeccionDTO().getCoberturaDTO();
								generarMovimientoBajaCobertura(coberturaEndosoActual, cobertura.getDescripcion(),Sistema.SI_AGRUPADO,coberturaEndosoActual, listaMovimientos);
							}
						}
					}else if(seccionEndosoActual.getClaveContrato() == Sistema.CONTRATADO &&
							seccionUltimoEndoso.getClaveContrato() == Sistema.NO_CONTRATADO){//alta de seccion
						List<CoberturaCotizacionDTO> coberturasEndosoActual = obtenerListaCompletaCoberturas(
								seccionEndosoActual.getId().getNumeroInciso(), seccionEndosoActual.getId().getIdToSeccion());
						generarMovimientoSeccion(seccionEndosoActual, Sistema.TIPO_MOV_ALTA_SECCION,
								UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"endoso.movimiento.alta.seccion"),Sistema.NO_AGRUPADO, listaMovimientos);
						for(CoberturaCotizacionDTO coberturaEndosoActual: coberturasEndosoActual){
							if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO){
								CoberturaDTO cobertura = coberturaEndosoActual.getCoberturaSeccionDTO().getCoberturaDTO();
								generarMovimientoAltaCobertura(coberturaEndosoActual, cobertura.getDescripcion(),Sistema.SI_AGRUPADO, listaMovimientos);
							}
						}
					}
				}
			}
		}
	}
	
	private void registraDiferenciasDeEndosoPorSeccion(SeccionCotizacionDTO seccionEndosoActual,
			SeccionCotizacionDTO seccionUltimoEndoso,List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws SystemException{
		
		List<CoberturaCotizacionDTO> coberturasEndosoActual = obtenerListaCompletaCoberturas(
				seccionEndosoActual.getId().getNumeroInciso(),seccionEndosoActual.getId().getIdToSeccion());
		
		for(CoberturaCotizacionDTO coberturaEndosoActual: coberturasEndosoActual){
			
			CoberturaCotizacionDTO coberturaUltimoEndoso = estructuraCotizacionEndosoAnterior.
					obtenerCoberturaPorIdOmiteContrato(coberturaEndosoActual.getId().getNumeroInciso(),
							coberturaEndosoActual.getId().getIdToSeccion(), coberturaEndosoActual.getId().getIdToCobertura());
			CoberturaDTO cobertura = coberturaEndosoActual.getCoberturaSeccionDTO().getCoberturaDTO();
			if(coberturaUltimoEndoso != null){
				if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO 
					&& coberturaUltimoEndoso.getClaveContrato() == Sistema.NO_CONTRATADO){
					generarMovimientoAltaCobertura(coberturaEndosoActual, cobertura.getDescripcion(), Sistema.NO_AGRUPADO, listaMovimientos);
				}else if(coberturaEndosoActual.getClaveContrato() == Sistema.NO_CONTRATADO 
						&& coberturaUltimoEndoso.getClaveContrato() == Sistema.CONTRATADO){
					generarMovimientoBajaCobertura(coberturaEndosoActual, cobertura.getDescripcion(), Sistema.NO_AGRUPADO,coberturaUltimoEndoso, listaMovimientos);
				}else if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO && 
						(coberturaEndosoActual.getValorSumaAsegurada().compareTo(coberturaUltimoEndoso.getValorSumaAsegurada()) != 0)){
					
					String movimiento = UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", 
							" Suma Asegurada de "+ cobertura.getDescripcion(),Sistema.FORMATO_MONEDA.format(coberturaEndosoActual.getValorSumaAsegurada().doubleValue()));
					generarMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento, 
							Sistema.TIPO_MOV_MODIFICACION_COBERTURA, Sistema.NO_AGRUPADO, Sistema.NO_REGENERA_RECIBOS,false, listaMovimientos);
					
				}else if (coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO && 
						(coberturaEndosoActual.getValorPrimaNeta().compareTo(coberturaUltimoEndoso.getValorPrimaNeta()) != 0)){
					
					String movimiento = UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandarSimple")+
							" Prima Neta de "+ cobertura.getDescripcion();
					generarMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento,
							Sistema.TIPO_MOV_MODIFICACION_PRIMA_NETA,Sistema.SI_AGRUPADO, Sistema.SI_REGENERA_RECIBOS,false, listaMovimientos);
				}
				if (coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO && 
						(coberturaEndosoActual.getPorcentajeCoaseguro().compareTo(coberturaUltimoEndoso.getPorcentajeCoaseguro()) != 0)){
					
					String movimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, 
							"endoso.movimiento.modificacion.textoEstandar", " Valor de Coaseguro de "+ 
							cobertura.getDescripcion(),coberturaEndosoActual.getPorcentajeCoaseguro().doubleValue() +"%");
					generarMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento,
							Sistema.TIPO_MOV_MODIFICACION_COASEGURO,Sistema.NO_AGRUPADO, Sistema.NO_REGENERA_RECIBOS,true, listaMovimientos);
				}
				if (coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO &&
						(((coberturaEndosoActual.getPorcentajeDeducible() != null && coberturaUltimoEndoso.getPorcentajeDeducible()!= null) && 
						coberturaEndosoActual.getPorcentajeDeducible().doubleValue() != coberturaUltimoEndoso.getPorcentajeDeducible().doubleValue())||
						((coberturaEndosoActual.getClaveTipoDeducible() != null && coberturaUltimoEndoso.getClaveTipoDeducible()!= null) &&
						coberturaEndosoActual.getClaveTipoDeducible().doubleValue() != coberturaUltimoEndoso.getClaveTipoDeducible().doubleValue())||
						((coberturaEndosoActual.getClaveTipoLimiteDeducible() != null && coberturaUltimoEndoso.getClaveTipoLimiteDeducible()!= null) &&
						coberturaEndosoActual.getClaveTipoLimiteDeducible().doubleValue() != coberturaUltimoEndoso.getClaveTipoLimiteDeducible().doubleValue()) ||
						((coberturaEndosoActual.getValorMaximoLimiteDeducible() != null && coberturaUltimoEndoso.getValorMaximoLimiteDeducible()!= null) &&
						coberturaEndosoActual.getValorMaximoLimiteDeducible().doubleValue() != coberturaUltimoEndoso.getValorMaximoLimiteDeducible().doubleValue()) ||
						((coberturaEndosoActual.getValorMinimoLimiteDeducible() != null && coberturaUltimoEndoso.getValorMinimoLimiteDeducible()!= null) &&
						coberturaEndosoActual.getValorMinimoLimiteDeducible().doubleValue() != coberturaUltimoEndoso.getValorMinimoLimiteDeducible().doubleValue()))){
					
					String deducible = PlantillaCotizacionBase.obtenerDescripcionDeducible(coberturaEndosoActual);
					String movimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", " Deducible de "+ cobertura.getDescripcion(),deducible);
					
					generarMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento,
							Sistema.TIPO_MOV_MODIFICACION_DEDUCIBLE,Sistema.NO_AGRUPADO, Sistema.NO_REGENERA_RECIBOS, true, listaMovimientos);
				}
				
			}
		}	
	}
	
	private Boolean existenDiferenciasDeEndosoPorSubInciso(SeccionCotizacionDTO seccionEndosoActual,
			SeccionCotizacionDTO seccionUltimoEndoso,List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws SystemException{
		Boolean notificar = Boolean.FALSE;
		if(seccionEndosoActual != null && seccionUltimoEndoso != null){

			List<SubIncisoCotizacionDTO> subIncisosEndosoActual = obtenerSubIncisos(
					seccionEndosoActual.getId().getIdToSeccion(), seccionEndosoActual.getId().getNumeroInciso());
			
			List<SubIncisoCotizacionDTO> subIncisosUltimoEndoso = estructuraCotizacionEndosoAnterior.obtenerSubIncisos(
					seccionEndosoActual.getId().getIdToSeccion(), seccionEndosoActual.getId().getNumeroInciso());
			
			for(SubIncisoCotizacionDTO subIncisoEndosoActual: subIncisosEndosoActual){
				SubIncisoCotizacionDTO subIncisoTmp = estructuraCotizacionEndosoAnterior.obtenerSubInciso(subIncisoEndosoActual.getId().getIdToSeccion(),
						subIncisoEndosoActual.getId().getNumeroInciso(), subIncisoEndosoActual.getId().getNumeroSubInciso());
				if(subIncisoTmp != null){
					if(this.registrarDiferenciasDeSubIncisos(subIncisoEndosoActual, subIncisoTmp,listaMovimientos))
						notificar = Boolean.TRUE;
				}else{
					generarMovimientoSubInciso(subIncisoEndosoActual, Sistema.TIPO_MOV_ALTA_SUBINCISO, 
							Sistema.MSG_MOV_ALTA_SUBINCISO + " "+subIncisoEndosoActual.getDescripcionSubInciso(),false, listaMovimientos);
					notificar = Boolean.TRUE;
				}
			}
			for(SubIncisoCotizacionDTO subIncisoUltimoEndoso: subIncisosUltimoEndoso){
				SubIncisoCotizacionDTO subIncisoTmp = obtenerSubInciso(subIncisoUltimoEndoso.getId().getIdToSeccion(),
						subIncisoUltimoEndoso.getId().getNumeroInciso(), subIncisoUltimoEndoso.getId().getNumeroSubInciso());
				if(subIncisoTmp == null){
					//se da de baja el subinciso del ultimo endoso
					subIncisoUltimoEndoso.setValorSumaAsegurada(subIncisoUltimoEndoso.getValorSumaAsegurada() * -1D);
					generarMovimientoSubInciso(subIncisoUltimoEndoso, Sistema.TIPO_MOV_BAJA_SUBINCISO, 
							Sistema.MSG_MOV_BAJA_SUBINCISO + " "+subIncisoUltimoEndoso.getDescripcionSubInciso(),false, listaMovimientos);
					subIncisoUltimoEndoso.setValorSumaAsegurada(subIncisoUltimoEndoso.getValorSumaAsegurada() * -1D);
					notificar = Boolean.TRUE;
				}
			}
			
		}
		return notificar;
	}
	
	private Boolean registrarDiferenciasDeSubIncisos(SubIncisoCotizacionDTO subIncisoEndosoActual,
			SubIncisoCotizacionDTO subIncisoUltimoEndoso,List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws SystemException{
		Boolean diferencias = Boolean.FALSE;

		if(!subIncisoEndosoActual.getDescripcionSubInciso().equals(subIncisoUltimoEndoso.getDescripcionSubInciso())
			|| subIncisoEndosoActual.getId().getNumeroSubInciso().intValue() != subIncisoUltimoEndoso.getId().getNumeroSubInciso().intValue()){
			//se da de baja el subinciso del ultimo endoso
			subIncisoUltimoEndoso.setValorSumaAsegurada(subIncisoUltimoEndoso.getValorSumaAsegurada() * -1D);
			generarMovimientoSubInciso(subIncisoUltimoEndoso, Sistema.TIPO_MOV_BAJA_SUBINCISO,
					Sistema.MSG_MOV_BAJA_SUBINCISO + " "+subIncisoUltimoEndoso.getDescripcionSubInciso(),false, listaMovimientos);
			//se da de alta el subinciso del endoso actual
			generarMovimientoSubInciso(subIncisoEndosoActual, Sistema.TIPO_MOV_ALTA_SUBINCISO,
					Sistema.MSG_MOV_ALTA_SUBINCISO + " "+subIncisoEndosoActual.getDescripcionSubInciso(),false, listaMovimientos);
			diferencias = Boolean.TRUE;
		}else if(subIncisoEndosoActual.getValorSumaAsegurada().doubleValue() != subIncisoUltimoEndoso.getValorSumaAsegurada().doubleValue()){//JACC
			//se modifico la suma asegurada del subinciso
			generarMovimientoModificacionSubInciso(subIncisoEndosoActual, subIncisoUltimoEndoso, listaMovimientos);
			diferencias = Boolean.TRUE;
		}
		
		//COMPARAR DATOS DEL SUBINCISO
		
//		DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
//		id.setIdToCotizacion(subIncisoEndosoActual.getId().getIdToCotizacion());
//		id.setNumeroInciso(subIncisoEndosoActual.getId().getNumeroInciso());
//		id.setNumeroSubinciso(subIncisoEndosoActual.getId().getNumeroSubInciso());
		List<DatoIncisoCotizacionDTO> listaDatosSubIncisoCotizacion = //DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
			administradorDatosInciso.obtenerDatoIncisoCotPorIncisoSubInciso(
				subIncisoEndosoActual.getId().getNumeroInciso(), subIncisoEndosoActual.getId().getNumeroSubInciso());
		
//		id = new DatoIncisoCotizacionId();
//		id.setIdToCotizacion(subIncisoUltimoEndoso.getId().getIdToCotizacion());
//		id.setNumeroInciso(subIncisoUltimoEndoso.getId().getNumeroInciso());
//		id.setNumeroSubinciso(subIncisoUltimoEndoso.getId().getNumeroSubInciso());
		List<DatoIncisoCotizacionDTO> listaDatosSubIncisoUltimoEndoso = //DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
			estructuraCotizacionEndosoAnterior.getAdministradorDatosInciso().obtenerDatoIncisoCotPorIncisoSubInciso(
					subIncisoUltimoEndoso.getId().getNumeroInciso(), subIncisoUltimoEndoso.getId().getNumeroSubInciso());
		
		for(DatoIncisoCotizacionDTO datoSubIncisoCotizacionDTO: listaDatosSubIncisoCotizacion){
			
			DatoIncisoCotizacionDTO datoSubIncisoCotizacionUltEndDTO = 
				obtenerDatoInciso(datoSubIncisoCotizacionDTO.getId(), listaDatosSubIncisoUltimoEndoso);
			
			if(datoSubIncisoCotizacionUltEndDTO != null){
				if(!datoSubIncisoCotizacionDTO.getValor().trim().equals(datoSubIncisoCotizacionUltEndDTO.getValor().trim())){
					
					String [] descripcionDato = administradorDatosInciso.obtenerDescripcionDatoInciso(datoSubIncisoCotizacionDTO, null);
//						ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(
//							datoSubIncisoCotizacionDTO, null);
					if(descripcionDato != null && descripcionDato.length >= 2){
						String descripcionMovimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
								"endoso.movimiento.modificacion.textoEstandar", descripcionDato[0],descripcionDato[1]);
						//registra movimientos de datos del subinciso
						//Se le pone la suma asegurada en 0 para que no se entienda que hubo una diferencia en
						//sumas aseguradas. Estos movimientos representan un cambio en un dato de subinciso.
						generarMovimientoSubInciso(subIncisoEndosoActual, Sistema.TIPO_MOV_MODIFICACION_SUBINCISO, descripcionMovimiento,true, listaMovimientos);
						diferencias = Boolean.TRUE;
					}
				}
			}
		}

		return diferencias;
	}
	
	private void generarDiferenciasDeIncisos(IncisoCotizacionDTO incisoEndosoActual,
			IncisoCotizacionDTO incisoUltimoEndoso,List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws SystemException
			{
		if (incisoEndosoActual.getId().getNumeroInciso().compareTo(incisoUltimoEndoso.getId().getNumeroInciso()) == 0) {

			if(!incisoEndosoActual.getDireccionDTO().getCodigoPostal().equals(incisoUltimoEndoso.getDireccionDTO().getCodigoPostal())
				|| !incisoEndosoActual.getDireccionDTO().getNombreCalle().equals(incisoUltimoEndoso.getDireccionDTO().getNombreCalle()) 
				|| !incisoEndosoActual.getDireccionDTO().getNumeroExterior().equals(incisoUltimoEndoso.getDireccionDTO().getNumeroExterior())
				|| incisoEndosoActual.getDireccionDTO().getIdEstado().intValue() != incisoUltimoEndoso.getDireccionDTO().getIdEstado().intValue()
				|| incisoEndosoActual.getDireccionDTO().getIdMunicipio().intValue() != incisoUltimoEndoso.getDireccionDTO().getIdMunicipio().intValue()
				|| incisoEndosoActual.getDireccionDTO().getNumeroInterior() == null && incisoUltimoEndoso.getDireccionDTO().getNumeroInterior() != null
				|| incisoEndosoActual.getDireccionDTO().getNumeroInterior() != null && incisoUltimoEndoso.getDireccionDTO().getNumeroInterior() == null
				|| (incisoEndosoActual.getDireccionDTO().getNumeroInterior() != null && incisoUltimoEndoso.getDireccionDTO().getNumeroInterior() != null
					&& !incisoEndosoActual.getDireccionDTO().getNumeroInterior().equals(incisoUltimoEndoso.getDireccionDTO().getNumeroInterior()))){
				
				String descripcionDireccion = DireccionDN.getInstancia().obtenerDescripcionDireccion(incisoEndosoActual.getDireccionDTO());
				generarMovimientoInciso(incisoEndosoActual.getId().getNumeroInciso(), Sistema.TIPO_MOV_MODIFICACION_INCISO,
						Sistema.MSG_MOV_MODIFICACION_INCISO+ " " + descripcionDireccion, Sistema.NO_AGRUPADO, listaMovimientos);
			}
			//COMPARAR DATOS DEL RIESGO
			List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion = //DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
				administradorDatosInciso.obtenerDatoIncisoCotPorNumeroInciso(incisoEndosoActual.getId().getNumeroInciso());
			List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacionUltEnd = //DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
				estructuraCotizacionEndosoAnterior.getAdministradorDatosInciso().obtenerDatoIncisoCotPorNumeroInciso(incisoEndosoActual.getId().getNumeroInciso());
			for(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO: listaDatosIncisoCotizacion){
				DatoIncisoCotizacionId datoIncisoCotizacionId = datoIncisoCotizacionDTO.getId();
				datoIncisoCotizacionId.setIdToCotizacion(incisoUltimoEndoso.getId().getIdToCotizacion());
				DatoIncisoCotizacionDTO datoIncisoCotizacionUltEndDTO = obtenerDatoInciso(datoIncisoCotizacionId, listaDatosIncisoCotizacionUltEnd);
				
				if(datoIncisoCotizacionUltEndDTO != null){
					if(!datoIncisoCotizacionDTO.getValor().trim().equals(datoIncisoCotizacionUltEndDTO.getValor().trim())){
						String[] descripcionDatoInciso = null;
						try{
							descripcionDatoInciso = administradorDatosInciso.obtenerDescripcionDatoInciso(datoIncisoCotizacionDTO, null);
//								ConfiguracionDatoIncisoCotizacionDN.getInstancia().obtenerDescripcionDatoInciso(
//									datoIncisoCotizacionDTO, null);
						}catch(Exception e){
							LogDeMidasWeb.log("Error al consultar valor de datoIncisoCot.", Level.WARNING, e);
						}
						
						if(descripcionDatoInciso != null && descripcionDatoInciso.length >= 2){
							String descripcionMovimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", 
									descripcionDatoInciso[0],descripcionDatoInciso[1]);
							
							generarMovimientoInciso(incisoEndosoActual.getId().getNumeroInciso(),
									Sistema.TIPO_MOV_MODIFICACION_INCISO, descripcionMovimiento, Sistema.NO_AGRUPADO, listaMovimientos);
						}
					}
				}
			}
		}
	}
	
	private DatoIncisoCotizacionDTO obtenerDatoInciso(DatoIncisoCotizacionId idFiltro,List<DatoIncisoCotizacionDTO> listaDatosBusqueda){
		DatoIncisoCotizacionDTO datoEncontrado = null;
		if(listaDatosBusqueda != null){
			for(DatoIncisoCotizacionDTO datoEnCurso : listaDatosBusqueda){
				DatoIncisoCotizacionId idEnCurso = datoEnCurso.getId();
				if(idFiltro.getIdDato().compareTo(idEnCurso.getIdDato())==0){
					if(idFiltro.getClaveDetalle().compareTo(idEnCurso.getClaveDetalle()) == 0){
						if(idFiltro.getNumeroInciso().compareTo(idEnCurso.getNumeroInciso()) == 0 && idFiltro.getNumeroSubinciso().compareTo(idEnCurso.getNumeroSubinciso()) == 0){
							if(idFiltro.getIdTcRamo().compareTo(idEnCurso.getIdTcRamo()) == 0 && idFiltro.getIdTcSubramo().compareTo(idEnCurso.getIdTcSubramo()) == 0){
								if(idFiltro.getIdToCobertura().compareTo(idEnCurso.getIdToCobertura())==0 && idFiltro.getIdToRiesgo().compareTo(idEnCurso.getIdToRiesgo())==0){
									if(idFiltro.getIdToSeccion().compareTo(idEnCurso.getIdToSeccion())==0 ){
										if(idFiltro.getNumeroSubinciso() == null){
											datoEncontrado = datoEnCurso;
											break;
										}
										else if (idFiltro.getNumeroSubinciso().compareTo(idEnCurso.getNumeroSubinciso()) == 0){
											datoEncontrado = datoEnCurso;
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return datoEncontrado;
	}
	
	private void registrarDiferenciasEndosoPorPersona(ClienteDTO personaEndosoActual,ClienteDTO personaUltimoEndoso,int clavePersona,
			List<MovimientoCotizacionEndosoDTO> listaMovimientos,String nombreUsuario){
		if(personaEndosoActual.getNombre() != null && personaUltimoEndoso!= null){
			if(personaEndosoActual.getClaveTipoPersona().intValue() == 2){//Moral
				if (!personaEndosoActual.getNombre().equals(personaUltimoEndoso.getNombre())) {
					generarMovimientoGeneral(
							(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_NOMBRE_ASEGURADO : Sistema.MSG_MOV_NOMBRE_CONTRATANTE )+ " "
							+ MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).obtenerNombreCliente(personaEndosoActual),
							Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, clavePersona == CLAVE_PERSONA_CONTRATANTE, listaMovimientos);
				}
			}else{//Fisica
				if (!personaEndosoActual.getNombre().equals(personaUltimoEndoso.getNombre())
						|| !personaEndosoActual.getApellidoPaterno().equals(personaUltimoEndoso.getApellidoPaterno())
						|| !personaEndosoActual.getApellidoMaterno().equals(personaUltimoEndoso.getApellidoMaterno())) {
					generarMovimientoGeneral(
							(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_NOMBRE_ASEGURADO : Sistema.MSG_MOV_NOMBRE_CONTRATANTE)+ " "
							+ MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).obtenerNombreCliente(personaEndosoActual),
							Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, clavePersona == CLAVE_PERSONA_CONTRATANTE, listaMovimientos);
				}					
			}
		}
		
		if (personaEndosoActual.getClaveTipoPersona().intValue() != personaUltimoEndoso.getClaveTipoPersona().intValue()) {
			String tipoPersona = personaEndosoActual.getClaveTipoPersona().intValue() == 2 ? "MORAL" : "FISICA" ;
			generarMovimientoGeneral(
				(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_TIPO_PERSONA_ASEGURADO : Sistema.MSG_MOV_TIPO_PERSONA_CONTRATANTE)+ " "
						+ tipoPersona,Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, clavePersona == CLAVE_PERSONA_CONTRATANTE, listaMovimientos);
		}

		if (!personaEndosoActual.getCodigoRFC().equals(
				personaUltimoEndoso.getCodigoRFC())) {
			generarMovimientoGeneral(
				(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_RFC_ASEGURADO : Sistema.MSG_MOV_RFC_CONTRATANTE)+ " " + 
				personaEndosoActual.getCodigoRFC(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, clavePersona == CLAVE_PERSONA_CONTRATANTE, listaMovimientos);
		}
		if ((personaEndosoActual.getFechaNacimiento()!= null && personaUltimoEndoso.getFechaNacimiento()!= null)
				&&personaEndosoActual.getFechaNacimiento().compareTo(personaUltimoEndoso.getFechaNacimiento()) != 0) {
			Format formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			generarMovimientoGeneral(
				(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_FECHA_NACIMIENTO_ASEGURADO :Sistema.MSG_MOV_FECHA_NACIMIENTO_CONTRATANTE)+" "+
					formatoFecha.format(personaEndosoActual.getFechaNacimiento()),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false, listaMovimientos);
		}
		if (personaEndosoActual.getTelefono() != null && 
				!personaEndosoActual.getTelefono().equals(personaUltimoEndoso.getTelefono())) {
			generarMovimientoGeneral(
				(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_TELEFONO_ASEGURADO : Sistema.MSG_MOV_TELEFONO_CONTRATANTE)+ " "
				+ personaEndosoActual.getTelefono(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false, listaMovimientos);
		}
		if(personaEndosoActual.getEmail() != null && personaUltimoEndoso.getEmail() != null){
			if (!personaEndosoActual.getEmail().equals(personaUltimoEndoso.getEmail())) {
				generarMovimientoGeneral(
					(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_CORREO_ASEGURADO:Sistema.MSG_MOV_CORREO_CONTRATANTE)+ " " + 
					personaEndosoActual.getEmail(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false, listaMovimientos);
			}				
		}
		if (!personaEndosoActual.getNombreCalle().equals(personaUltimoEndoso.getNombreCalle())
				|| !personaEndosoActual.getNombreColonia().equals(personaUltimoEndoso.getNombreColonia())
				|| !personaEndosoActual.getNombreDelegacion().equals(personaUltimoEndoso.getNombreDelegacion())
				|| !personaEndosoActual.getDescripcionEstado().equals(personaUltimoEndoso.getDescripcionEstado())
				|| !personaEndosoActual.getCodigoPostal().equals(personaUltimoEndoso.getCodigoPostal())) {
			generarMovimientoGeneral(
				(clavePersona == CLAVE_PERSONA_ASEGURADO ? Sistema.MSG_MOV_DOMICILIO_ASEGURADO:Sistema.MSG_MOV_DOMICILIO_CONTRATANTE)+ " "+ MovimientoCotizacionEndosoDN.
					getInstancia(nombreUsuario).obtenerDireccionCliente(personaEndosoActual),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, 
					clavePersona == CLAVE_PERSONA_CONTRATANTE, listaMovimientos);
		}
	}
	
	private void registrarDiferenciasDeEndosoPorAgente(List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		if (cotizacionDTO.getSolicitudDTO().getCodigoAgente().intValue() != 
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getSolicitudDTO().getCodigoAgente().intValue()) {

			generarMovimientoGeneral(Sistema.MSG_MOV_AGENTE+ " "+ cotizacionDTO.getSolicitudDTO().getNombreAgente(),
					Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true, listaMovimientos);
		}
	}
	
	private void registrarDiferenciasDeEndosoPorVigencia(List<MovimientoCotizacionEndosoDTO> listaMovimientos)throws ExcepcionDeAccesoADatos, SystemException {
		if (!UtileriasWeb.sonFechasIguales(cotizacionDTO.getFechaFinVigencia(),
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getFechaFinVigencia())) {
			Format formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			generarMovimientoGeneral(Sistema.MSG_MOV_VIGENCIA
					+ " "+ formatoFecha.format(cotizacionDTO.getFechaInicioVigencia())
					+ " - "+ formatoFecha.format(cotizacionDTO.getFechaFinVigencia()),
					Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true, listaMovimientos);
		}
	}
	
	private void registrarDiferenciasDeEndosoPorFormaPago(List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionDTO.getIdFormaPago().intValue() != 
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getIdFormaPago().intValue()) {
			FormaPagoDTO formaPago = new FormaPagoDTO();
			formaPago.setIdFormaPago(cotizacionDTO.getIdFormaPago().intValue());
			formaPago = FormaPagoDN.getInstancia().getPorId(formaPago);

			generarMovimientoGeneral(Sistema.MSG_MOV_FORMA_PAGO + " "
					+ formaPago.getDescripcion(), Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true, listaMovimientos);
		}
	}
	
	private void registrarDiferenciasDeEndosoPorMedioPago(List<MovimientoCotizacionEndosoDTO> listaMovimientos) throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionDTO.getIdMedioPago().intValue() != 
				estructuraCotizacionEndosoAnterior.getCotizacionDTO().getIdMedioPago().intValue()) {
			MedioPagoDTO medioPago = new MedioPagoDTO();
			medioPago.setIdMedioPago(cotizacionDTO.getIdMedioPago().intValue());
			medioPago = MedioPagoDN.getInstancia().getPorId(medioPago);
			
			generarMovimientoGeneral(Sistema.MSG_MOV_MEDIO_PAGO + " "
					+ medioPago.getDescripcion(), Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,false, listaMovimientos);
		}
	}
	
	private void generarMovimientoModificacionSubInciso(SubIncisoCotizacionDTO subIncisoActual, SubIncisoCotizacionDTO subIncisoUltimo,
			List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(subIncisoActual.getId().getNumeroInciso());
		movimientoEndoso.setNumeroSubInciso(subIncisoActual.getId().getNumeroSubInciso());
		movimientoEndoso.setIdToSeccion(subIncisoActual.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_SUBINCISO);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(subIncisoActual.getValorSumaAsegurada()- subIncisoUltimo.getValorSumaAsegurada()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(Sistema.MSG_MOV_ALTA_SUBINCISO + " "+ subIncisoActual.getDescripcionSubInciso());
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoSeccion(SeccionCotizacionDTO seccCotizacionDTO, short tipoMovimiento,
			String descripcionMovimiento,short agrupaMovimientos,List<MovimientoCotizacionEndosoDTO> listaMovimientos){

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(seccCotizacionDTO.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(seccCotizacionDTO.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		listaMovimientos.add(movimientoEndoso);

	}
	
	private void generarMovimientoSubInciso(SubIncisoCotizacionDTO subInciso, short tipoMovimiento,
			String descripcionMovimiento,boolean aplicaSACero,List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(subInciso.getId().getNumeroInciso());
		movimientoEndoso.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
		movimientoEndoso.setIdToSeccion(subInciso.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		if(aplicaSACero){
			movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		}
		else{
			movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(subInciso.getValorSumaAsegurada()));
		}
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoInciso(BigDecimal numeroInciso, short tipoMovimiento,
			String descripcionMovimiento,short agrupaMovimientos,List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(numeroInciso);
		movimientoEndoso.setIdToSeccion(BigDecimal.ZERO);
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoAltaCobertura(CoberturaCotizacionDTO cobertura, String descripcionMovimiento,
			short agrupaMovimientos,List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		//TODO Los movimientos de alta de coberturas no se est�n registrando.
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(cobertura.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(cobertura.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(cobertura.getId().getIdToCobertura());
		movimientoEndoso.setClaveTipoMovimiento(Sistema.TIPO_MOV_ALTA_COBERTURA);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(cobertura.getValorSumaAsegurada()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.valueOf(cobertura.getValorPrimaNeta()));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(cobertura.getValorCuota()));
		movimientoEndoso.setDescripcionMovimiento(Sistema.MSG_MOV_ALTA_COBERTURA + " "+ descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoBajaCobertura(CoberturaCotizacionDTO cobertura, String descripcionMovimiento,
			short agrupaMovimientos,CoberturaCotizacionDTO coberturaAnterior,List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(cobertura.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(cobertura.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(cobertura.getId().getIdToCobertura());
		movimientoEndoso.setClaveTipoMovimiento(Sistema.TIPO_MOV_BAJA_COBERTURA);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(coberturaAnterior.getValorSumaAsegurada()).multiply(BigDecimal.valueOf(-1D)));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.valueOf(coberturaAnterior.getValorPrimaNeta()).multiply(BigDecimal.valueOf(-1D)));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(coberturaAnterior.getValorCuota()).multiply(BigDecimal.valueOf(-1D)));
		movimientoEndoso.setDescripcionMovimiento(Sistema.MSG_MOV_BAJA_COBERTURA + " "+ descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoModificacionCobertura(CoberturaCotizacionDTO cobertura,CoberturaCotizacionDTO coberturaUltEnd,String descripcionMovimiento,
			short tipoMovimiento, short agrupaMovimientos, short generaRecibos,boolean aplicarCifrasEnCeros,List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(cobertura.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(cobertura.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(cobertura.getId().getIdToCobertura());
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(cobertura
				.getValorCuota().doubleValue()- coberturaUltEnd.getValorCuota().doubleValue()));
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(generaRecibos);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		if(aplicarCifrasEnCeros){
			movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
			movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		}
		else{
			movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal
					.valueOf(cobertura.getValorSumaAsegurada().doubleValue()- coberturaUltEnd.getValorSumaAsegurada().doubleValue()));
			movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal
					.valueOf(cobertura.getValorPrimaNeta().doubleValue()- coberturaUltEnd.getValorPrimaNeta().doubleValue()));
		}
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoPrimerRiesgoLUC(AgrupacionCotDTO agrupacion, String descripcionMovimiento,
			short tipoMovimiento,List<MovimientoCotizacionEndosoDTO> listaMovimientos){
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToSeccion(agrupacion.getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(agrupacion.getValorSumaAsegurada()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.valueOf(agrupacion.getValorPrimaneta()));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(agrupacion.getValorCuota()));
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoAnexos(String descripcionMovimiento, short tipoMovimiento,
			BigDecimal idToControArchivo,List<MovimientoCotizacionEndosoDTO> listaMovimientos){

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToSeccion(BigDecimal.ZERO);
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(idToControArchivo);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		listaMovimientos.add(movimientoEndoso);
	}
	
	private void generarMovimientoGeneral(String descripcionMovimiento, short tipoMovimiento, boolean regeneraRecibos,
			List<MovimientoCotizacionEndosoDTO> listaMovimientos){

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToSeccion(BigDecimal.ZERO);
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(regeneraRecibos ? Sistema.SI_REGENERA_RECIBOS: Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		
		listaMovimientos.add(movimientoEndoso);
	}
	
	//Getters y setters
	public SoporteEstructuraCotizacion getEstructuraCotizacionEndosoAnterior() {
		return estructuraCotizacionEndosoAnterior;
	}

	public void setEstructuraCotizacionEndosoAnterior(
			SoporteEstructuraCotizacion estructuraCotizacionEndosoAnterior) {
		this.estructuraCotizacionEndosoAnterior = estructuraCotizacionEndosoAnterior;
	}
}
