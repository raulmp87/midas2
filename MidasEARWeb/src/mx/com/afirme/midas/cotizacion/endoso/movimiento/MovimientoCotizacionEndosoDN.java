package mx.com.afirme.midas.cotizacion.endoso.movimiento;

import java.math.BigDecimal;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.danios.reportes.PlantillaCotizacionBase;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import org.apache.commons.lang.StringUtils;

public class MovimientoCotizacionEndosoDN {
	private static final MovimientoCotizacionEndosoDN INSTANCIA = new MovimientoCotizacionEndosoDN();
	private static String nombreUsuario;
	private static final Format fmt = new SimpleDateFormat("dd/MM/yyyy");
	private static final NumberFormat fMonto = Sistema.FORMATO_MONEDA;

	public static MovimientoCotizacionEndosoDN getInstancia(String nombreUsuario) {
		MovimientoCotizacionEndosoDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}

	public void agregar(
			MovimientoCotizacionEndosoDTO movimientoCotizacionEndosoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoCotizacionEndosoDTO);
	}
	
	public BigDecimal obtenerDiferenciaPrimaNeta(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		return movimientoCotizacionEndosoSN
				.obtenerDiferenciaPrimaNeta(idToCotizacion);

	}

	public List<MovimientoCotizacionEndosoDTO> obtenerMovimientosPorCotizacion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		return movimientoCotizacionEndosoSN
				.obtenerMovimientosPorCotizacion(idToCotizacion);
	}
	
	public List<MovimientoCotizacionEndosoDTO> listarFiltrado(
			MovimientoCotizacionEndosoDTO dto) throws ExcepcionDeAccesoADatos,
			SystemException {
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		return movimientoCotizacionEndosoSN.listarFiltrado(dto);
	}
	public void calcularDiferenciasDeEndosoDeCambioFormaPago(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {

		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacionEndosoActual = cotizacionDN
				.getPorId(idToCotizacion);
		
		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);
		
		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario)
				.getUltimoEndoso(
						cotizacionEndosoActual.getSolicitudDTO()
								.getIdToPolizaEndosada());
		CotizacionDTO cotizacionUltimoEndoso = cotizacionDN
				.getPorId(ultimoEndoso.getIdToCotizacion());
		//Tipo de Endoso
		this.registraMovimientoGeneral(cotizacionEndosoActual
				.getIdToCotizacion(), Sistema.MSG_MOV_TIPO_ENDOSO + " "
				+ UtileriasWeb.getDescripcionCatalogoValorFijo(40,
						cotizacionEndosoActual.getSolicitudDTO()
								.getClaveTipoEndoso().intValue()),
				Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
		this.registrarDiferenciasDeEndosoPorFormaPago(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
	}	
	public void calcularDiferenciasDeEndosoDeCancelacion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);
		
		List<CoberturaCotizacionDTO> coberturasUltimoEndoso = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(idToCotizacion); 
		for(CoberturaCotizacionDTO cobertura: coberturasUltimoEndoso){
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(cobertura.getId().getIdToCobertura());
			this.registraMovimientoBajaCobertura(cobertura, coberturaDTO.getDescripcion(), Sistema.NO_AGRUPADO,cobertura);				
		}
		
	}
	public void calcularDiferenciasDeEndosoDeRehabilitacion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);
		
		List<CoberturaCotizacionDTO> coberturasUltimoEndoso = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(idToCotizacion); 
		for(CoberturaCotizacionDTO cobertura: coberturasUltimoEndoso){
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(cobertura.getId().getIdToCobertura());
			this.registraMovimientoAltaCobertura(cobertura, coberturaDTO.getDescripcion(), Sistema.NO_AGRUPADO);				
		}
		
	}	
	public void calcularDiferenciasDeEndosoDeDeclaracion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {

		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacionEndosoActual = cotizacionDN
				.getPorId(idToCotizacion);

		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);

		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario)
				.getUltimoEndoso(
						cotizacionEndosoActual.getSolicitudDTO()
								.getIdToPolizaEndosada());
		CotizacionDTO cotizacionUltimoEndoso = cotizacionDN
				.getPorId(ultimoEndoso.getIdToCotizacion());

		List<IncisoCotizacionDTO> incisosEndosoActual = IncisoCotizacionDN
		.getInstancia().listarPorCotizacionId(
				cotizacionEndosoActual.getIdToCotizacion());
		List<IncisoCotizacionDTO> incisosUltimoEndoso = IncisoCotizacionDN
				.getInstancia().listarPorCotizacionId(
						cotizacionUltimoEndoso.getIdToCotizacion());

		cotizacionEndosoActual.setIncisoCotizacionDTOs(incisosEndosoActual);
		cotizacionUltimoEndoso.setIncisoCotizacionDTOs(incisosUltimoEndoso);

		this.registrarDiferenciasDeEndosoPorSeccion(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
	}
	public void calcularDiferenciasDeEndoso(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos, SystemException {
		
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacionEndosoActual = cotizacionDN
				.getPorId(idToCotizacion);
		
		MovimientoCotizacionEndosoSN sn = new MovimientoCotizacionEndosoSN(nombreUsuario);
		sn.borrarPorIdToCotizacion(idToCotizacion);
		
		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario)
				.getUltimoEndoso(
						cotizacionEndosoActual.getSolicitudDTO()
								.getIdToPolizaEndosada());
		CotizacionDTO cotizacionUltimoEndoso = cotizacionDN
				.getPorId(ultimoEndoso.getIdToCotizacion());
		//Tipo de Endoso
		this.registraMovimientoGeneral(cotizacionEndosoActual
				.getIdToCotizacion(), Sistema.MSG_MOV_TIPO_ENDOSO + " "
				+ UtileriasWeb.getDescripcionCatalogoValorFijo(40,
						cotizacionEndosoActual.getSolicitudDTO()
								.getClaveTipoEndoso().intValue()),
				Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
		
		this.registrarDiferenciasDeEndosoPorProducto(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorTipoPoliza(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorFormaPago(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorMedioPago(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorMoneda(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorVigencia(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorAgente(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorAsegurado(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorContratante(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoGenerales(cotizacionEndosoActual,
				cotizacionUltimoEndoso);		
		this.registrarDiferenciasDeEndosoPorAgrupacion(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorInciso(cotizacionEndosoActual,
				cotizacionUltimoEndoso);
		this.registrarDiferenciasDeEndosoPorSeccion(cotizacionEndosoActual,
				cotizacionUltimoEndoso);

	}

	private void registrarDiferenciasDeEndosoGenerales(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			//Se validan los documentos Anexos
			List<DocAnexoCotDTO> documentosActual = DocAnexoCotDN.getInstancia().listarDocumentosAnexosPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
			List<DocAnexoCotDTO> documentosUltimo = DocAnexoCotDN.getInstancia().listarDocumentosAnexosPorCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
			if(documentosActual != null && documentosUltimo!= null){
				if(documentosActual.size() == documentosUltimo.size()){//Mismo numero documentos
					for (int i = 0; i < documentosActual.size(); i++) {
						DocAnexoCotDTO actual = documentosActual.get(i);
						DocAnexoCotDTO ultimo = documentosUltimo.get(i);
						if(actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO &&
								ultimo.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO){
							//Se agrego Anexo
							this.registraMovimientoAnexos(cotizacionEndosoActual
									.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo());													
							
						}else if (actual.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO &&
								ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
							//Se elimino Anexo
							this.registraMovimientoAnexos(cotizacionEndosoActual
									.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);													
							
						}
					}					
				}else if(documentosActual.size() > documentosUltimo.size()){//Se agregaron documentos
					for (int i = 0; i < documentosActual.size(); i++) {
						if(documentosUltimo.size()>= i+1){
							DocAnexoCotDTO actual = documentosActual.get(i);
							DocAnexoCotDTO ultimo = documentosUltimo.get(i);
							if(actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO &&
									ultimo.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO){
								//Se agrego Anexo
								this.registraMovimientoAnexos(cotizacionEndosoActual
										.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo());													
								
							}else if (actual.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO &&
									ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
								//Se elimino Anexo
								this.registraMovimientoAnexos(cotizacionEndosoActual
										.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);													
								
							}							
						}else{
							DocAnexoCotDTO actual = documentosActual.get(i);
							//Se agrego Anexo
							if (actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO)							
								this.registraMovimientoAnexos(cotizacionEndosoActual
										.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo());													
						}
					}					
				}else if(documentosActual.size() < documentosUltimo.size()){//Se eliminaron documentos
					for (int i = 0; i < documentosUltimo.size(); i++) {
						if(documentosActual.size()>= i+1){
							DocAnexoCotDTO actual = documentosActual.get(i);
							DocAnexoCotDTO ultimo = documentosUltimo.get(i);
							if(actual.getClaveSeleccion().intValue() == Sistema.SELECCIONADO &&
									ultimo.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO){
								//Se agrego Anexo
								this.registraMovimientoAnexos(cotizacionEndosoActual
										.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo());													
								
							}else if (actual.getClaveSeleccion().intValue() == Sistema.NO_SELECCIONADO &&
									ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO){
								//Se elimino Anexo
								this.registraMovimientoAnexos(cotizacionEndosoActual
										.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);													
								
							}							
						}else{
							DocAnexoCotDTO ultimo = documentosUltimo.get(i);
							//Se elimino Anexo
							if (ultimo.getClaveSeleccion().intValue() == Sistema.SELECCIONADO)
								this.registraMovimientoAnexos(cotizacionEndosoActual
										.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + ultimo.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);							}
					}					
				}

			}
			//Se validan los documentos Anexos de Reaseguro
			List<DocumentoAnexoReaseguroCotizacionDTO> documentosReaActual = DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtieneAnexosReasCotPorIdCotizacion(cotizacionEndosoActual.getIdToCotizacion());
			List<DocumentoAnexoReaseguroCotizacionDTO> documentosReaUltimo = DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtieneAnexosReasCotPorIdCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
				
			for(DocumentoAnexoReaseguroCotizacionDTO actual: documentosReaActual){
				DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId(cotizacionUltimoEndoso.getIdToCotizacion(), actual.getId().getIdToControlArchivo());
				DocumentoAnexoReaseguroCotizacionDTO docReaUltimo =  DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtienePorId(id);
				if(docReaUltimo == null){
					//Se elimino Anexo
					this.registraMovimientoAnexos(cotizacionEndosoActual
							.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo());																			
				}else{
					if(!docReaUltimo.getDescripcionDocumentoAnexo().equals(actual.getDescripcionDocumentoAnexo())){
						//Se elimino Anexo
						this.registraMovimientoAnexos(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + docReaUltimo.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);
						//Se agrego Anexo
						this.registraMovimientoAnexos(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO + " " + actual.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,actual.getId().getIdToControlArchivo());													
						
					}
				}
			}
				
			for(DocumentoAnexoReaseguroCotizacionDTO ultimo: documentosReaUltimo){
				DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId(cotizacionEndosoActual.getIdToCotizacion(), ultimo.getId().getIdToControlArchivo());
				DocumentoAnexoReaseguroCotizacionDTO docReaActual =  DocumentoAnexoReaseguroCotizacionDN.getInstancia().obtienePorId(id);
				if(docReaActual == null){
					//Se elimino Anexo
					this.registraMovimientoAnexos(cotizacionEndosoActual
							.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO + " " + ultimo.getDescripcionDocumentoAnexo(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);																			
				}
			}			
			//Se validan textos Adicionales
			TexAdicionalCotDTO textoCotActual = new TexAdicionalCotDTO();
			textoCotActual.setCotizacion(cotizacionEndosoActual);
			List<TexAdicionalCotDTO> textosCotActual = TexAdicionalCotDN.getInstancia().listarFiltrado(textoCotActual);
			
			TexAdicionalCotDTO textoCotUltimoEnd = new TexAdicionalCotDTO();
			textoCotUltimoEnd.setCotizacion(cotizacionUltimoEndoso);
			List<TexAdicionalCotDTO> textosCotUltimoEnd = TexAdicionalCotDN.getInstancia().listarFiltrado(textoCotUltimoEnd);
			if(textosCotActual.size() == textosCotUltimoEnd.size()){//Mismo numero de textos
				for (int i = 0; i < textosCotActual.size(); i++) {
					TexAdicionalCotDTO actual = textosCotActual.get(i);
					TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
					if(!actual.getDescripcionTexto().equals(ultimo.getDescripcionTexto())){
						//Registra Movimiento: cambio de texto adicional
						this.registraMovimientoAnexos(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL + " "
								+ actual.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);						
					}
				}
			}else if(textosCotActual.size() > textosCotUltimoEnd.size()){//se agregaron textos
				for (int i = 0; i < textosCotActual.size(); i++) {
					if(textosCotUltimoEnd.size()>= i+1){
						TexAdicionalCotDTO actual = textosCotActual.get(i);
						TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
						if(!actual.getDescripcionTexto().equals(ultimo.getDescripcionTexto())){
							//Registra Movimiento: cambio de texto adicional
							this.registraMovimientoAnexos(cotizacionEndosoActual
									.getIdToCotizacion(), Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL + " "
									+ actual.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);	
						}			
					}else{
						TexAdicionalCotDTO actual = textosCotActual.get(i);
							//Registra movimiento: alta de texto adicional
						this.registraMovimientoAnexos(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_ALTA_TEXTO_ADICIONAL + actual.getDescripcionTexto() ,Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);								
					}

				}
				
			}else if (textosCotActual.size() < textosCotUltimoEnd.size()){//se eliminaron textos
				for (int i = 0; i < textosCotUltimoEnd.size(); i++) {
					if(textosCotActual.size()>= i+1){
						TexAdicionalCotDTO actual = textosCotActual.get(i);
						TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
						if(!actual.getDescripcionTexto().equals(ultimo.getDescripcionTexto())){
							//Registra Movimiento: cambio de texto adicional
							this.registraMovimientoAnexos(cotizacionEndosoActual
									.getIdToCotizacion(), Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL + " "
									+ actual.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, BigDecimal.ZERO);	
						}			
					}else{
						TexAdicionalCotDTO ultimo = textosCotUltimoEnd.get(i);
							//Registra movimiento: baja de texto adicional
						this.registraMovimientoAnexos(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_BAJA_TEXTO_ADICIONAL + ultimo.getDescripcionTexto(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, ultimo.getIdToTexAdicionalCot());							
					}

				}
			}
		}
	}
	private void registraDiferenciasDeEndosoPorSeccion(SeccionCotizacionDTO seccionEndosoActual, SeccionCotizacionDTO seccionUltimoEndoso)throws ExcepcionDeAccesoADatos, SystemException{
		SeccionCotizacionDTOId id2 = new SeccionCotizacionDTOId();
		id2.setIdToCotizacion(seccionEndosoActual.getId().getIdToCotizacion());
		id2.setIdToSeccion(seccionEndosoActual.getId().getIdToSeccion());
		id2.setNumeroInciso(seccionEndosoActual.getId().getNumeroInciso());
		
		List<CoberturaCotizacionDTO> coberturasEndosoActual = CoberturaCotizacionDN
				.getInstancia().listarPorSeccionCotizacionId(id2,Boolean.FALSE);
		for(CoberturaCotizacionDTO coberturaEndosoActual: coberturasEndosoActual){
			CoberturaCotizacionId idCot = new CoberturaCotizacionId();
			idCot.setIdToCotizacion(seccionUltimoEndoso.getId().getIdToCotizacion());
			idCot.setNumeroInciso(coberturaEndosoActual.getId().getNumeroInciso());
			idCot.setIdToSeccion(coberturaEndosoActual.getId().getIdToSeccion());
			idCot.setIdToCobertura(coberturaEndosoActual.getId().getIdToCobertura());
			CoberturaCotizacionDTO coberturaUltimoEndoso = new CoberturaCotizacionDTO();
			coberturaUltimoEndoso.setId(idCot);
			coberturaUltimoEndoso = CoberturaCotizacionDN.getInstancia().getPorId(coberturaUltimoEndoso);
			
			if(coberturaUltimoEndoso != null){
				if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO 
					&& coberturaUltimoEndoso.getClaveContrato() == Sistema.NO_CONTRATADO){
					CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
					this.registraMovimientoAltaCobertura(coberturaEndosoActual, cobertura.getDescripcion(),Sistema.NO_AGRUPADO);
				}else if(coberturaEndosoActual.getClaveContrato() == Sistema.NO_CONTRATADO 
						&& coberturaUltimoEndoso.getClaveContrato() == Sistema.CONTRATADO){
					CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
					this.registraMovimientoBajaCobertura(coberturaEndosoActual, cobertura.getDescripcion(),Sistema.NO_AGRUPADO,coberturaUltimoEndoso);								
				}else if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO && (coberturaEndosoActual.getValorSumaAsegurada().doubleValue() != coberturaUltimoEndoso.getValorSumaAsegurada().doubleValue())){
					CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
					String movimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", " Suma Asegurada de "+ cobertura.getDescripcion(),fMonto.format(coberturaEndosoActual.getValorSumaAsegurada().doubleValue()));
					this.registraMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento, Sistema.TIPO_MOV_MODIFICACION_COBERTURA, Sistema.NO_AGRUPADO, Sistema.NO_REGENERA_RECIBOS);
				}else if (coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO && (coberturaEndosoActual.getValorPrimaNeta().doubleValue() != coberturaUltimoEndoso.getValorPrimaNeta().doubleValue())){
					CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
					String movimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandarSimple")+ " Prima Neta de "+ cobertura.getDescripcion();
					this.registraMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento, Sistema.TIPO_MOV_MODIFICACION_PRIMA_NETA,Sistema.SI_AGRUPADO, Sistema.SI_REGENERA_RECIBOS);								
				}
				if (coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO && (coberturaEndosoActual.getPorcentajeCoaseguro().doubleValue() != coberturaUltimoEndoso.getPorcentajeCoaseguro().doubleValue())){
					CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
					String movimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", " Valor de Coaseguro de "+ cobertura.getDescripcion(),coberturaEndosoActual.getPorcentajeCoaseguro().doubleValue() +"%");
					coberturaEndosoActual.setValorPrimaNeta(0D);
					coberturaUltimoEndoso.setValorPrimaNeta(0D);
					coberturaEndosoActual.setValorSumaAsegurada(0D);
					coberturaUltimoEndoso.setValorSumaAsegurada(0D);
					this.registraMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento, Sistema.TIPO_MOV_MODIFICACION_COASEGURO,Sistema.NO_AGRUPADO, Sistema.NO_REGENERA_RECIBOS);																
				}
				if (coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO &&
					(((coberturaEndosoActual.getPorcentajeDeducible() != null && coberturaUltimoEndoso.getPorcentajeDeducible()!= null) && 
					coberturaEndosoActual.getPorcentajeDeducible().doubleValue() != coberturaUltimoEndoso.getPorcentajeDeducible().doubleValue())||
					((coberturaEndosoActual.getClaveTipoDeducible() != null && coberturaUltimoEndoso.getClaveTipoDeducible()!= null) &&
					coberturaEndosoActual.getClaveTipoDeducible().doubleValue() != coberturaUltimoEndoso.getClaveTipoDeducible().doubleValue())||
					((coberturaEndosoActual.getClaveTipoLimiteDeducible() != null && coberturaUltimoEndoso.getClaveTipoLimiteDeducible()!= null) &&
					coberturaEndosoActual.getClaveTipoLimiteDeducible().doubleValue() != coberturaUltimoEndoso.getClaveTipoLimiteDeducible().doubleValue()) ||
					((coberturaEndosoActual.getValorMaximoLimiteDeducible() != null && coberturaUltimoEndoso.getValorMaximoLimiteDeducible()!= null) &&
					coberturaEndosoActual.getValorMaximoLimiteDeducible().doubleValue() != coberturaUltimoEndoso.getValorMaximoLimiteDeducible().doubleValue()) ||
					((coberturaEndosoActual.getValorMinimoLimiteDeducible() != null && coberturaUltimoEndoso.getValorMinimoLimiteDeducible()!= null) &&
					coberturaEndosoActual.getValorMinimoLimiteDeducible().doubleValue() != coberturaUltimoEndoso.getValorMinimoLimiteDeducible().doubleValue()))){
					CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
					String deducible = PlantillaCotizacionBase.obtenerDescripcionDeducible(coberturaEndosoActual);
					String movimiento = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", " Deducible de "+ cobertura.getDescripcion(),deducible);
					coberturaEndosoActual.setValorPrimaNeta(0D);
					coberturaUltimoEndoso.setValorPrimaNeta(0D);
					coberturaEndosoActual.setValorSumaAsegurada(0D);
					coberturaUltimoEndoso.setValorSumaAsegurada(0D);
					this.registraMovimientoModificacionCobertura(coberturaEndosoActual, coberturaUltimoEndoso, movimiento, Sistema.TIPO_MOV_MODIFICACION_DEDUCIBLE,Sistema.NO_AGRUPADO, Sistema.NO_REGENERA_RECIBOS);																
				}
				
			}
		}		
		
	}
	private void registrarDiferenciasDeEndosoPorSeccion(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			List<IncisoCotizacionDTO> incisosEndosoActual = cotizacionEndosoActual.getIncisoCotizacionDTOs();

			for (IncisoCotizacionDTO incisoEndosoActual: incisosEndosoActual) {

				List<SeccionCotizacionDTO> seccionesEndosoActual = SeccionCotizacionDN
						.getInstancia().listarPorCotizacionNumeroInciso(
								cotizacionEndosoActual.getIdToCotizacion(), incisoEndosoActual.getId().getNumeroInciso()); 
				for(SeccionCotizacionDTO seccionEndosoActual: seccionesEndosoActual){
					SeccionCotizacionDTOId id = new SeccionCotizacionDTOId();
					id.setIdToCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
					id.setNumeroInciso(seccionEndosoActual.getId().getNumeroInciso());
					id.setIdToSeccion(seccionEndosoActual.getId().getIdToSeccion());
					
					SeccionCotizacionDTO seccionUltimoEndoso = SeccionCotizacionDN.getInstancia().buscarPorId(id);
	
					if(seccionUltimoEndoso != null){
						if(existenDiferenciasDeEndosoPorSubInciso(seccionEndosoActual, seccionUltimoEndoso)){
							this.registraMovimientoSeccion(seccionEndosoActual, Sistema.TIPO_MOV_MODIFICACION_SECCION , Sistema.MSG_MOV_MODIFICACION_SUBINCISO_LISTADO,Sistema.NO_AGRUPADO);
						}					
						if(seccionEndosoActual.getClaveContrato() == Sistema.CONTRATADO &&
								seccionUltimoEndoso.getClaveContrato() == Sistema.CONTRATADO){
							this.registraDiferenciasDeEndosoPorSeccion(seccionEndosoActual, seccionUltimoEndoso);
						}else if(seccionEndosoActual.getClaveContrato() == Sistema.NO_CONTRATADO &&
								seccionUltimoEndoso.getClaveContrato() == Sistema.CONTRATADO){ //baja de seccion
							List<CoberturaCotizacionDTO> coberturasEndosoActual = CoberturaCotizacionDN
							.getInstancia().listarPorSeccionCotizacionId(seccionEndosoActual.getId(),Boolean.FALSE);
							this.registraMovimientoSeccion(seccionEndosoActual,
									Sistema.TIPO_MOV_BAJA_SECCION,
									UtileriasWeb.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"endoso.movimiento.baja.seccion"),Sistema.NO_AGRUPADO);
							for(CoberturaCotizacionDTO coberturaEndosoActual: coberturasEndosoActual){
								if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO){
									CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
									this.registraMovimientoBajaCobertura(coberturaEndosoActual, cobertura.getDescripcion(),Sistema.SI_AGRUPADO,coberturaEndosoActual);									
								}
							}
						}else if(seccionEndosoActual.getClaveContrato() == Sistema.CONTRATADO &&
								seccionUltimoEndoso.getClaveContrato() == Sistema.NO_CONTRATADO){//alta de seccion
							List<CoberturaCotizacionDTO> coberturasEndosoActual = CoberturaCotizacionDN
							.getInstancia().listarPorSeccionCotizacionId(seccionEndosoActual.getId(),Boolean.FALSE);
							this.registraMovimientoSeccion(seccionEndosoActual,
									Sistema.TIPO_MOV_ALTA_SECCION,
									UtileriasWeb.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"endoso.movimiento.alta.seccion"),Sistema.NO_AGRUPADO);							
							for(CoberturaCotizacionDTO coberturaEndosoActual: coberturasEndosoActual){
								if(coberturaEndosoActual.getClaveContrato() == Sistema.CONTRATADO){
									CoberturaDTO cobertura = CoberturaDN.getInstancia().getPorId(coberturaEndosoActual.getId().getIdToCobertura());
									this.registraMovimientoAltaCobertura(coberturaEndosoActual, cobertura.getDescripcion(),Sistema.SI_AGRUPADO);									
								}
							}
						}						
					}
				}
			}
		}
	}

	private Boolean existenDiferenciasDeEndosoPorSubInciso(
			SeccionCotizacionDTO seccionEndosoActual,
			SeccionCotizacionDTO seccionUltimoEndoso)throws ExcepcionDeAccesoADatos, SystemException {
		Boolean notificar = Boolean.FALSE;
		if(seccionEndosoActual != null && seccionUltimoEndoso != null){
			SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
			id.setIdToCotizacion(seccionEndosoActual.getId().getIdToCotizacion());
			id.setNumeroInciso(seccionEndosoActual.getId().getNumeroInciso());
			id.setIdToSeccion(seccionEndosoActual.getId().getIdToSeccion());
			SubIncisoCotizacionDTO subIncisoDTO = new SubIncisoCotizacionDTO();
			subIncisoDTO.setId(id);

			List<SubIncisoCotizacionDTO> subIncisosEndosoActual = SubIncisoCotizacionDN.getInstancia().listarFiltrado(subIncisoDTO);
			subIncisoDTO.getId().setIdToCotizacion(seccionUltimoEndoso.getId().getIdToCotizacion());
			
			List<SubIncisoCotizacionDTO> subIncisosUltimoEndoso = SubIncisoCotizacionDN.getInstancia().listarFiltrado(subIncisoDTO);
			for(SubIncisoCotizacionDTO subIncisoEndosoActual: subIncisosEndosoActual){
				SubIncisoCotizacionDTO subIncisoTmp = new SubIncisoCotizacionDTO();
				subIncisoTmp.setId(subIncisoEndosoActual.getId());
				subIncisoTmp.getId().setIdToCotizacion(seccionUltimoEndoso.getId().getIdToCotizacion());
				subIncisoTmp = SubIncisoCotizacionDN.getInstancia().getPorId(subIncisoTmp.getId());
				if(subIncisoTmp != null){
					if(this.registrarDiferenciasDeSubIncisos(subIncisoEndosoActual, subIncisoTmp))
						notificar = Boolean.TRUE;
				}else{
					subIncisoEndosoActual.getId().setIdToCotizacion(seccionEndosoActual.getId().getIdToCotizacion());
					this.registraMovimientoSubInciso(subIncisoEndosoActual, Sistema.TIPO_MOV_ALTA_SUBINCISO, Sistema.MSG_MOV_ALTA_SUBINCISO + " "+subIncisoEndosoActual.getDescripcionSubInciso());
					notificar = Boolean.TRUE;
				}
			}
			for(SubIncisoCotizacionDTO subIncisoUltimoEndoso: subIncisosUltimoEndoso){
				SubIncisoCotizacionDTO subIncisoTmp = new SubIncisoCotizacionDTO();
				subIncisoTmp.setId(subIncisoUltimoEndoso.getId());
				subIncisoTmp.getId().setIdToCotizacion(seccionEndosoActual.getId().getIdToCotizacion());
				subIncisoTmp = SubIncisoCotizacionDN.getInstancia().getPorId(subIncisoTmp.getId());	
				if(subIncisoTmp == null){
					//se da de baja el subinciso del ultimo endoso
					subIncisoUltimoEndoso.setValorSumaAsegurada(subIncisoUltimoEndoso.getValorSumaAsegurada() * -1D);
					this.registraMovimientoSubInciso(subIncisoUltimoEndoso, Sistema.TIPO_MOV_BAJA_SUBINCISO, Sistema.MSG_MOV_BAJA_SUBINCISO + " "+subIncisoUltimoEndoso.getDescripcionSubInciso());
					notificar = Boolean.TRUE;
				}
			}
			
		}
		return notificar;
	}

	private Boolean registrarDiferenciasDeSubIncisos(
			SubIncisoCotizacionDTO subIncisoEndosoActual,
			SubIncisoCotizacionDTO subIncisoUltimoEndoso) throws ExcepcionDeAccesoADatos, SystemException {
		Boolean diferencias = Boolean.FALSE;

		if(!subIncisoEndosoActual.getDescripcionSubInciso().equals(subIncisoUltimoEndoso.getDescripcionSubInciso())
			|| subIncisoEndosoActual.getId().getNumeroSubInciso().intValue() != subIncisoUltimoEndoso.getId().getNumeroSubInciso().intValue()){
			//se da de baja el subinciso del ultimo endoso
			subIncisoUltimoEndoso.setValorSumaAsegurada(subIncisoUltimoEndoso.getValorSumaAsegurada() * -1D);
			this.registraMovimientoSubInciso(subIncisoUltimoEndoso, Sistema.TIPO_MOV_BAJA_SUBINCISO, Sistema.MSG_MOV_BAJA_SUBINCISO + " "+subIncisoUltimoEndoso.getDescripcionSubInciso());
			//se da de alta el subinciso del endoso actual
			this.registraMovimientoSubInciso(subIncisoEndosoActual, Sistema.TIPO_MOV_ALTA_SUBINCISO, Sistema.MSG_MOV_ALTA_SUBINCISO + " "+subIncisoEndosoActual.getDescripcionSubInciso());
			diferencias = Boolean.TRUE;
		}else if(subIncisoEndosoActual.getValorSumaAsegurada().doubleValue() != subIncisoUltimoEndoso.getValorSumaAsegurada().doubleValue()){//JACC
			//se modifico la suma asegurada del subinciso		
			this.registraModificacionSubInciso(subIncisoEndosoActual, subIncisoUltimoEndoso);
			diferencias = Boolean.TRUE;
		}
		
		//COMPARAR DATOS DEL SUBINCISO
		//Se le pone la suma asegurada en 0 para que no se entienda que hubo una diferencia en
		//sumas aseguradas. Estos movimientos representan un cambio en un dato de subinciso.
		subIncisoEndosoActual.setValorSumaAsegurada(0D);
		
		DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
		id.setIdToCotizacion(subIncisoEndosoActual.getId().getIdToCotizacion());
		id.setNumeroInciso(subIncisoEndosoActual.getId().getNumeroInciso());
		id.setNumeroSubinciso(subIncisoEndosoActual.getId().getNumeroSubInciso());
		List<DatoIncisoCotizacionDTO> listaDatosSubIncisoCotizacion = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
		for(DatoIncisoCotizacionDTO datoSubIncisoCotizacionDTO: listaDatosSubIncisoCotizacion){
			DatoIncisoCotizacionId datoSubIncisoCotizacionId = datoSubIncisoCotizacionDTO.getId();
			datoSubIncisoCotizacionId.setIdToCotizacion(subIncisoUltimoEndoso.getId().getIdToCotizacion());
			DatoIncisoCotizacionDTO datoSubIncisoCotizacionUltEndDTO = new DatoIncisoCotizacionDTO();
			datoSubIncisoCotizacionUltEndDTO.setId(datoSubIncisoCotizacionId);
			datoSubIncisoCotizacionUltEndDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoSubIncisoCotizacionUltEndDTO);
			
			if(datoSubIncisoCotizacionUltEndDTO != null){
				if(!datoSubIncisoCotizacionDTO.getValor().trim().equals(datoSubIncisoCotizacionUltEndDTO.getValor().trim())){
					ConfiguracionDatoIncisoCotizacionDTO datoConfiguracion = this.getDatoConfiguracion(datoSubIncisoCotizacionDTO);
					if(datoConfiguracion != null){
						//registra movimientos de datos del subinciso
						this.registraMovimientoSubInciso(subIncisoEndosoActual, Sistema.TIPO_MOV_MODIFICACION_SUBINCISO,
								UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", datoConfiguracion.getDescripcionEtiqueta(),getValorDatoConfiguracion(datoSubIncisoCotizacionUltEndDTO, datoConfiguracion)));
						diferencias = Boolean.TRUE;
					}
				}
			}
		}

		return diferencias;
	}

	private void registrarDiferenciasDeEndosoPorAgrupacion(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			PrimerRiesgoLUCDN primerRiesgoLUCDN = PrimerRiesgoLUCDN.getInstancia();
			List<AgrupacionCotDTO> primerRiesgoActual = primerRiesgoLUCDN.listarPorCotizacion(cotizacionEndosoActual.getIdToCotizacion());
			
			List<AgrupacionCotDTO> primerRiesgoUltimo = primerRiesgoLUCDN.listarPorCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
			List<AgrupacionCotDTO> agrupacionesTemp = null;
			SeccionDN seccionDN = SeccionDN.getInstancia();
			
			for(AgrupacionCotDTO agrupacion: primerRiesgoActual){
				agrupacion.getId().setIdToCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
				agrupacionesTemp = primerRiesgoLUCDN.listarFiltrado(agrupacion);				
				if(agrupacion.getClaveTipoAgrupacion() == Sistema.TIPO_PRIMER_RIESGO){
					if(agrupacionesTemp == null || agrupacionesTemp.size() == 0){
						agrupacion.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
						List<SeccionCotizacionDTO> seccionesPrimerRiesgo = SeccionCotizacionDN
								.getInstancia().listarSeccionesPrimerRiesgo(
										agrupacion.getId().getIdToCotizacion());
						StringBuffer sb = new StringBuffer();
						for(SeccionCotizacionDTO seccionCot: seccionesPrimerRiesgo){
							sb.append(seccionCot.getSeccionDTO().getNombreComercial() + ", ");
						}
						this.registraMovimientoPrimerRiesgoLUC(agrupacion,
								Sistema.MSG_MOV_ID_ALTA_PRIMER_RIESGO +
								UtileriasWeb.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"endoso.movimiento.alta.primerriesgo", sb
												.toString(),
										fMonto.format(agrupacion
												.getValorSumaAsegurada())),
								Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);							
					}else if(agrupacionesTemp.size() > 0){
						AgrupacionCotDTO agrupacionUltimoEnd = agrupacionesTemp.get(0);
						if(agrupacion.getValorSumaAsegurada().doubleValue() != agrupacionUltimoEnd.getValorSumaAsegurada().doubleValue()){
							agrupacion.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
							
							List<SeccionCotizacionDTO> seccionesPrimerRiesgo = SeccionCotizacionDN
							.getInstancia().listarSeccionesPrimerRiesgo(
									agrupacion.getId().getIdToCotizacion());
							
							StringBuffer sb = new StringBuffer();
							for(SeccionCotizacionDTO seccionCot: seccionesPrimerRiesgo){
								sb.append(seccionCot.getSeccionDTO().getNombreComercial() + " ");
							}							
							this.registraMovimientoPrimerRiesgoLUC(agrupacion,
									Sistema.MSG_MOV_ID_MODIFICACION_PRIMER_RIESGO +
							UtileriasWeb.getMensajeRecurso(
									Sistema.ARCHIVO_RECURSOS,
									"endoso.movimiento.modificacion.primerriesgo",
									sb.toString(), fMonto.format(agrupacion
											.getValorSumaAsegurada())),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
						}
					}
				}else{
					if(agrupacionesTemp == null || agrupacionesTemp.size() == 0){
						agrupacion.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
						SeccionDTO seccionDTO = seccionDN.getPorId(agrupacion.getIdToSeccion());
						this.registraMovimientoPrimerRiesgoLUC(agrupacion,
								Sistema.MSG_MOV_ID_ALTA_LUC +
								UtileriasWeb.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"endoso.movimiento.alta.luc",
										seccionDTO.getNombreComercial(), fMonto.format(agrupacion
												.getValorSumaAsegurada())),
								Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);							
					}else if(agrupacionesTemp.size() > 0){
						AgrupacionCotDTO agrupacionUltimoEnd = agrupacionesTemp.get(0);
						if(agrupacion.getValorSumaAsegurada().doubleValue() != agrupacionUltimoEnd.getValorSumaAsegurada().doubleValue()){
							SeccionDTO seccionDTO = seccionDN.getPorId(agrupacion.getIdToSeccion());
							agrupacion.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
							
							this.registraMovimientoPrimerRiesgoLUC(agrupacion,
									Sistema.MSG_MOV_ID_MODIFICACION_LUC +
									UtileriasWeb.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"endoso.movimiento.modificacion.luc",
											seccionDTO.getNombreComercial(), fMonto.format(agrupacion
													.getValorSumaAsegurada())),
									Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);	
						}
					}					
				}				
			}
			for(AgrupacionCotDTO agrupacionUltimo: primerRiesgoUltimo){
				agrupacionUltimo.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
				agrupacionesTemp = primerRiesgoLUCDN.listarFiltrado(agrupacionUltimo);		
				agrupacionUltimo.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
				agrupacionUltimo.setValorCuota(agrupacionUltimo.getValorCuota() * -1D);
				agrupacionUltimo.setValorPrimaneta(agrupacionUltimo.getValorPrimaneta() * -1D);
				agrupacionUltimo.setValorSumaAsegurada(agrupacionUltimo.getValorSumaAsegurada() * -1D);		
				
				if (agrupacionUltimo.getClaveTipoAgrupacion() == Sistema.TIPO_PRIMER_RIESGO) {
					if (agrupacionesTemp == null
							|| agrupacionesTemp.size() == 0) {

						List<SeccionCotizacionDTO> seccionesPrimerRiesgo = SeccionCotizacionDN
								.getInstancia().listarSeccionesPrimerRiesgo(
										agrupacionUltimo.getId()
												.getIdToCotizacion());
						StringBuffer sb = new StringBuffer();
						for (SeccionCotizacionDTO seccionCot : seccionesPrimerRiesgo) {
							sb.append(seccionCot.getSeccionDTO()
									.getNombreComercial()
									+ ", ");
						}
						this.registraMovimientoPrimerRiesgoLUC(
								agrupacionUltimo,
								Sistema.MSG_MOV_ID_BAJA_PRIMER_RIESGO +
								UtileriasWeb.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"endoso.movimiento.baja.primerriesgo",
										sb.toString(),
										fMonto.format(agrupacionUltimo
												.getValorSumaAsegurada())),
								Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
					}
				}else{
					if (agrupacionesTemp == null
							|| agrupacionesTemp.size() == 0) {

						SeccionDTO seccionDTO = seccionDN
								.getPorId(agrupacionUltimo.getIdToSeccion());
						this.registraMovimientoPrimerRiesgoLUC(
								agrupacionUltimo,
								Sistema.MSG_MOV_ID_BAJA_LUC +
								UtileriasWeb.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"endoso.movimiento.baja.luc",
										seccionDTO.getNombreComercial(),
										fMonto.format(agrupacionUltimo
												.getValorSumaAsegurada())),
								Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);

					}				
				}
				
			}
		}
	}
	private void registrarDiferenciasDeEndosoPorInciso(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			List<IncisoCotizacionDTO> incisosEndosoActual = IncisoCotizacionDN
					.getInstancia().listarPorCotizacionId(
							cotizacionEndosoActual.getIdToCotizacion());
			List<IncisoCotizacionDTO> incisosUltimoEndoso = IncisoCotizacionDN
					.getInstancia().listarPorCotizacionId(
							cotizacionUltimoEndoso.getIdToCotizacion());
			cotizacionEndosoActual.setIncisoCotizacionDTOs(incisosEndosoActual);
			cotizacionUltimoEndoso.setIncisoCotizacionDTOs(incisosUltimoEndoso);
			
			for(IncisoCotizacionDTO incisoEndosoActual: incisosEndosoActual){
				IncisoCotizacionDTO incisoCotTmp = new IncisoCotizacionDTO();
				IncisoCotizacionId idTmp = new IncisoCotizacionId();
				idTmp.setIdToCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
				idTmp.setNumeroInciso(incisoEndosoActual.getId().getNumeroInciso());
				incisoCotTmp.setId(idTmp);
				incisoCotTmp = IncisoCotizacionDN.getInstancia().getPorId(incisoCotTmp);
				if(incisoCotTmp != null){
					this.registrarDiferenciasDeIncisos(incisoEndosoActual, incisoCotTmp);
				}else{
					this.registraMovimientoInciso(
							cotizacionEndosoActual.getIdToCotizacion(),
							incisoEndosoActual.getId()
									.getNumeroInciso(),
							Sistema.TIPO_MOV_ALTA_INCISO,
							Sistema.MSG_MOV_ALTA_INCISO, Sistema.NO_AGRUPADO);			
					
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
					id.setNumeroInciso(incisoEndosoActual.getId().getNumeroInciso());
					SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
					seccionCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
					coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO.setSeccionCotizacionDTO(seccionCotizacionDTO);
					List<CoberturaCotizacionDTO> coberturasContratadasEndosoActual = CoberturaCotizacionDN
							.getInstancia().listarFiltrado(coberturaCotizacionDTO);
					
					for (CoberturaCotizacionDTO cobertura : coberturasContratadasEndosoActual) {
						CoberturaDTO cob = CoberturaDN.getInstancia()
								.getPorId(
										cobertura.getId()
												.getIdToCobertura());
						this.registraMovimientoAltaCobertura(cobertura,
								cob.getDescripcion(),Sistema.SI_AGRUPADO);
					}	
				}
			}

			for(IncisoCotizacionDTO incisoUltimoEndoso: incisosUltimoEndoso){
				IncisoCotizacionDTO incisoCotTmp = new IncisoCotizacionDTO();
				incisoCotTmp.setId(incisoUltimoEndoso.getId());
				incisoCotTmp.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
				incisoCotTmp = IncisoCotizacionDN.getInstancia().getPorId(incisoCotTmp);
				if(incisoCotTmp == null){
					this.registraMovimientoInciso(
							cotizacionEndosoActual.getIdToCotizacion(),
							incisoUltimoEndoso.getId()
									.getNumeroInciso(),
							Sistema.TIPO_MOV_BAJA_INCISO,
							Sistema.MSG_MOV_BAJA_INCISO, Sistema.NO_AGRUPADO);			
					
					CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToCotizacion(cotizacionUltimoEndoso.getIdToCotizacion());
					id.setNumeroInciso(incisoUltimoEndoso.getId().getNumeroInciso());
					SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
					seccionCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
					coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
					coberturaCotizacionDTO.setId(id);
					coberturaCotizacionDTO.setSeccionCotizacionDTO(seccionCotizacionDTO);						


					List<CoberturaCotizacionDTO> coberturasContratadasUltimoEndoso = CoberturaCotizacionDN
							.getInstancia().listarFiltrado(coberturaCotizacionDTO);
					
					for (CoberturaCotizacionDTO cobertura : coberturasContratadasUltimoEndoso) {
						CoberturaDTO cob = CoberturaDN.getInstancia()
								.getPorId(
										cobertura.getId()
												.getIdToCobertura());
						cobertura.getId().setIdToCotizacion(cotizacionEndosoActual.getIdToCotizacion());
						this.registraMovimientoBajaCobertura(cobertura,
								cob.getDescripcion(), Sistema.SI_AGRUPADO,cobertura);
					}						
				}
			}
		}
	}
	
	private void registrarDiferenciasDeIncisos(			
			IncisoCotizacionDTO incisoEndosoActual,
			IncisoCotizacionDTO incisoUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (incisoEndosoActual.getId().getNumeroInciso().intValue() == incisoUltimoEndoso
				.getId().getNumeroInciso().intValue()) {

			if(!incisoEndosoActual.getDireccionDTO().getCodigoPostal().equals(incisoUltimoEndoso.getDireccionDTO().getCodigoPostal())
				|| !incisoEndosoActual.getDireccionDTO().getNombreCalle().equals(incisoUltimoEndoso.getDireccionDTO().getNombreCalle()) 
				|| !incisoEndosoActual.getDireccionDTO().getNumeroExterior().equals(incisoUltimoEndoso.getDireccionDTO().getNumeroExterior())
				|| incisoEndosoActual.getDireccionDTO().getIdEstado().intValue() != incisoUltimoEndoso.getDireccionDTO().getIdEstado().intValue()
				|| incisoEndosoActual.getDireccionDTO().getIdMunicipio().intValue() != incisoUltimoEndoso.getDireccionDTO().getIdMunicipio().intValue()
				|| incisoEndosoActual.getDireccionDTO().getNumeroInterior() == null && incisoUltimoEndoso.getDireccionDTO().getNumeroInterior() != null
				|| incisoEndosoActual.getDireccionDTO().getNumeroInterior() != null && incisoUltimoEndoso.getDireccionDTO().getNumeroInterior() == null
				|| (incisoEndosoActual.getDireccionDTO().getNumeroInterior() != null && incisoUltimoEndoso.getDireccionDTO().getNumeroInterior() != null
					&& !incisoEndosoActual.getDireccionDTO().getNumeroInterior().equals(incisoUltimoEndoso.getDireccionDTO().getNumeroInterior()))){
				
				this.registraMovimientoInciso(incisoEndosoActual.getId().getIdToCotizacion(),
								incisoEndosoActual.getId().getNumeroInciso(),
								Sistema.TIPO_MOV_MODIFICACION_INCISO,
								Sistema.MSG_MOV_MODIFICACION_INCISO
								+ " " + obtenerDireccion(incisoEndosoActual.getDireccionDTO()),Sistema.NO_AGRUPADO);
				
			}
//				}
			//COMPARAR DATOS DEL RIESGO
			DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
			id.setIdToCotizacion(incisoEndosoActual.getId().getIdToCotizacion());
			id.setNumeroInciso(incisoEndosoActual.getId().getNumeroInciso());
			List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
			for(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO: listaDatosIncisoCotizacion){
				DatoIncisoCotizacionId datoIncisoCotizacionId = datoIncisoCotizacionDTO.getId();
				datoIncisoCotizacionId.setIdToCotizacion(incisoUltimoEndoso.getId().getIdToCotizacion());
				DatoIncisoCotizacionDTO datoIncisoCotizacionUltEndDTO = new DatoIncisoCotizacionDTO();
				datoIncisoCotizacionUltEndDTO.setId(datoIncisoCotizacionId);
				datoIncisoCotizacionUltEndDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionUltEndDTO);
				
				if(datoIncisoCotizacionUltEndDTO != null){
					if(!datoIncisoCotizacionDTO.getValor().trim().equals(datoIncisoCotizacionUltEndDTO.getValor().trim())){
						ConfiguracionDatoIncisoCotizacionDTO datoConfiguracion = this.getDatoConfiguracion(datoIncisoCotizacionDTO);
						if(datoConfiguracion != null){
							this.registraMovimientoInciso(incisoEndosoActual.getId().getIdToCotizacion(),
									incisoEndosoActual.getId().getNumeroInciso(),
									Sistema.TIPO_MOV_MODIFICACION_INCISO,
									UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoEstandar", datoConfiguracion.getDescripcionEtiqueta(),getValorDatoConfiguracion(datoIncisoCotizacionDTO, datoConfiguracion)),Sistema.NO_AGRUPADO);
						}
					}
				}
			}
		}
	}
	private void registrarDiferenciasDeEndosoPorContratante(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {

			ClienteDTO contratanteEndosoActual = null;

			if(cotizacionEndosoActual.getIdDomicilioContratante() != null && cotizacionEndosoActual.getIdDomicilioContratante().intValue() > 0){
				contratanteEndosoActual = new ClienteDTO();
				contratanteEndosoActual.setIdCliente(cotizacionEndosoActual.getIdToPersonaContratante());
				contratanteEndosoActual.setIdDomicilio(cotizacionEndosoActual.getIdDomicilioContratante());
				contratanteEndosoActual = ClienteDN.getInstancia().verDetalleCliente(contratanteEndosoActual, nombreUsuario);
			}else{
				contratanteEndosoActual = ClienteDN.getInstancia().verDetalleCliente(cotizacionEndosoActual.getIdToPersonaContratante(), nombreUsuario);
			}			
			
			ClienteDTO contratanteUltimoEndoso =null; 

			if(cotizacionUltimoEndoso.getIdDomicilioContratante() != null && cotizacionUltimoEndoso.getIdDomicilioContratante().intValue() > 0){
				contratanteUltimoEndoso = new ClienteDTO();
				contratanteUltimoEndoso.setIdCliente(cotizacionUltimoEndoso.getIdToPersonaContratante());
				contratanteUltimoEndoso.setIdDomicilio(cotizacionUltimoEndoso.getIdDomicilioContratante());
				contratanteUltimoEndoso = ClienteDN.getInstancia().verDetalleCliente(contratanteUltimoEndoso, nombreUsuario);
			}else{
				contratanteUltimoEndoso = ClienteDN.getInstancia().verDetalleCliente(cotizacionUltimoEndoso.getIdToPersonaContratante(), nombreUsuario);
			}			
			
			if (!contratanteEndosoActual.getNombre().equals(contratanteUltimoEndoso.getNombre())
					|| (contratanteEndosoActual.getApellidoPaterno()!= null && 
							!contratanteEndosoActual.getApellidoPaterno().equals(contratanteUltimoEndoso.getApellidoPaterno()))
					|| (contratanteEndosoActual.getApellidoMaterno() != null &&!contratanteEndosoActual.getApellidoMaterno().equals(
							contratanteUltimoEndoso.getApellidoMaterno()))){

				this
						.registraMovimientoGeneral(
								cotizacionEndosoActual.getIdToCotizacion(),
								Sistema.MSG_MOV_NOMBRE_CONTRATANTE
										+ " "
										+ obtenerNombreCliente(contratanteEndosoActual), Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}

			if (contratanteEndosoActual.getClaveTipoPersona().intValue() != contratanteUltimoEndoso
					.getClaveTipoPersona().intValue()) {
				String tipoPersona = "FISICA";
				if (contratanteEndosoActual.getClaveTipoPersona().intValue() == 2)
					tipoPersona = "MORAL";
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(),
						Sistema.MSG_MOV_TIPO_PERSONA_CONTRATANTE + " "
								+ tipoPersona,Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}

			if (contratanteEndosoActual.getCodigoRFC() != null &&
					!contratanteEndosoActual.getCodigoRFC().equals(
					contratanteUltimoEndoso.getCodigoRFC())) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_RFC_CONTRATANTE
						+ " " + contratanteEndosoActual.getCodigoRFC(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}
			if ((contratanteEndosoActual.getFechaNacimiento()!= null && contratanteUltimoEndoso.getFechaNacimiento()== null)|| 
					contratanteEndosoActual.getFechaNacimiento()!= null &&
					contratanteEndosoActual.getFechaNacimiento().compareTo(
					contratanteUltimoEndoso.getFechaNacimiento()) != 0) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(),
						Sistema.MSG_MOV_FECHA_NACIMIENTO_CONTRATANTE
								+ " "
								+ fmt.format(contratanteEndosoActual
										.getFechaNacimiento()),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}
			if (contratanteEndosoActual.getTelefono() != null && 
					!contratanteEndosoActual.getTelefono().equals(
					contratanteUltimoEndoso.getTelefono())) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(),
						Sistema.MSG_MOV_TELEFONO_CONTRATANTE + " "
								+ contratanteEndosoActual.getTelefono(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}

			if(contratanteEndosoActual.getEmail() != null && contratanteUltimoEndoso.getEmail() != null){
				if (!contratanteEndosoActual.getEmail().equals(
						contratanteUltimoEndoso.getEmail())) {
					this.registraMovimientoGeneral(cotizacionEndosoActual
							.getIdToCotizacion(),
							Sistema.MSG_MOV_CORREO_CONTRATANTE + " "
									+ contratanteEndosoActual.getEmail(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
				}
				
			}

			if (!contratanteEndosoActual.getNombreCalle().equals(
					contratanteUltimoEndoso.getNombreCalle())
					|| !contratanteEndosoActual.getNombreColonia().equals(contratanteUltimoEndoso.getNombreColonia())
					|| !contratanteEndosoActual.getNombreDelegacion().equals(contratanteUltimoEndoso.getNombreDelegacion())
					|| !contratanteEndosoActual.getDescripcionEstado().equals(contratanteUltimoEndoso.getDescripcionEstado())
					|| !contratanteEndosoActual.getCodigoPostal().equals(
							contratanteUltimoEndoso.getCodigoPostal())) {

				this
						.registraMovimientoGeneral(
								cotizacionEndosoActual.getIdToCotizacion(),
								Sistema.MSG_MOV_DOMICILIO_CONTRATANTE
										+ " "
										+ obtenerDireccionCliente(contratanteEndosoActual),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorAsegurado(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			ClienteDTO aseguradoEndosoActual = null;

			if(cotizacionEndosoActual.getIdDomicilioAsegurado() != null && cotizacionEndosoActual.getIdDomicilioAsegurado().intValue() > 0){
				aseguradoEndosoActual = new ClienteDTO();
				aseguradoEndosoActual.setIdCliente(cotizacionEndosoActual.getIdToPersonaAsegurado());
				aseguradoEndosoActual.setIdDomicilio(cotizacionEndosoActual.getIdDomicilioAsegurado());
				aseguradoEndosoActual = ClienteDN.getInstancia().verDetalleCliente(aseguradoEndosoActual, nombreUsuario);
			}else{
				aseguradoEndosoActual = ClienteDN.getInstancia().verDetalleCliente(cotizacionEndosoActual.getIdToPersonaAsegurado(), nombreUsuario);
			}			
			
			ClienteDTO aseguradoUltimoEndoso =null; 

			if(cotizacionUltimoEndoso.getIdDomicilioAsegurado() != null && cotizacionUltimoEndoso.getIdDomicilioAsegurado().intValue() > 0){
				aseguradoUltimoEndoso = new ClienteDTO();
				aseguradoUltimoEndoso.setIdCliente(cotizacionUltimoEndoso.getIdToPersonaAsegurado());
				aseguradoUltimoEndoso.setIdDomicilio(cotizacionUltimoEndoso.getIdDomicilioAsegurado());
				aseguradoUltimoEndoso = ClienteDN.getInstancia().verDetalleCliente(aseguradoUltimoEndoso, nombreUsuario);
			}else{
				aseguradoUltimoEndoso = ClienteDN.getInstancia().verDetalleCliente(cotizacionUltimoEndoso.getIdToPersonaAsegurado(), nombreUsuario);
			}			
			
			if(aseguradoEndosoActual.getNombre() != null && aseguradoUltimoEndoso!= null){
				if(aseguradoEndosoActual.getClaveTipoPersona().intValue() == 2){//Moral
					if (!aseguradoEndosoActual.getNombre().equals(
							aseguradoUltimoEndoso.getNombre())) {
						
						this.registraMovimientoGeneral(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_NOMBRE_ASEGURADO
								+ " "
								+ obtenerNombreCliente(aseguradoEndosoActual),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
					}					
					
				}else{//Fisica
					if (!aseguradoEndosoActual.getNombre().equals(
							aseguradoUltimoEndoso.getNombre())
							|| !aseguradoEndosoActual.getApellidoPaterno().equals(
									aseguradoUltimoEndoso.getApellidoPaterno())
							|| !aseguradoEndosoActual.getApellidoMaterno().equals(
									aseguradoUltimoEndoso.getApellidoMaterno())) {

						this.registraMovimientoGeneral(cotizacionEndosoActual
								.getIdToCotizacion(), Sistema.MSG_MOV_NOMBRE_ASEGURADO
								+ " "
								+ obtenerNombreCliente(aseguradoEndosoActual),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
					}					
				}
			}

			if (aseguradoEndosoActual.getClaveTipoPersona().intValue() != aseguradoUltimoEndoso
					.getClaveTipoPersona().intValue()) {
				String tipoPersona = "FISICA";
				if (aseguradoEndosoActual.getClaveTipoPersona().intValue() == 2)
					tipoPersona = "MORAL";
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(),
						Sistema.MSG_MOV_TIPO_PERSONA_ASEGURADO + " "
								+ tipoPersona,Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}

			if (!aseguradoEndosoActual.getCodigoRFC().equals(
					aseguradoUltimoEndoso.getCodigoRFC())) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_RFC_ASEGURADO
						+ " " + aseguradoEndosoActual.getCodigoRFC(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}
			if ((aseguradoEndosoActual.getFechaNacimiento()!= null && aseguradoUltimoEndoso.getFechaNacimiento()!= null)
					&&aseguradoEndosoActual.getFechaNacimiento().compareTo(
					aseguradoUltimoEndoso.getFechaNacimiento()) != 0) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(),
						Sistema.MSG_MOV_FECHA_NACIMIENTO_ASEGURADO
								+ " "
								+ fmt.format(aseguradoEndosoActual
										.getFechaNacimiento()),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}
			if (aseguradoEndosoActual.getTelefono() != null && 
					!aseguradoEndosoActual.getTelefono().equals(
					aseguradoUltimoEndoso.getTelefono())) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(),
						Sistema.MSG_MOV_TELEFONO_ASEGURADO + " "
								+ aseguradoEndosoActual.getTelefono(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}
			if(aseguradoEndosoActual.getEmail() != null && aseguradoUltimoEndoso.getEmail() != null){
				if (!aseguradoEndosoActual.getEmail().equals(
						aseguradoUltimoEndoso.getEmail())) {
					this.registraMovimientoGeneral(cotizacionEndosoActual
							.getIdToCotizacion(), Sistema.MSG_MOV_CORREO_ASEGURADO
							+ " " + aseguradoEndosoActual.getEmail(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
				}				
			}
			if (!aseguradoEndosoActual.getNombreCalle().equals(
					aseguradoUltimoEndoso.getNombreCalle())
					|| !aseguradoEndosoActual.getNombreColonia().equals(aseguradoUltimoEndoso.getNombreColonia())
					|| !aseguradoEndosoActual.getNombreDelegacion().equals(aseguradoUltimoEndoso.getNombreDelegacion())
					|| !aseguradoEndosoActual.getDescripcionEstado().equals(aseguradoUltimoEndoso.getDescripcionEstado())
					|| !aseguradoEndosoActual.getCodigoPostal().equals(
							aseguradoUltimoEndoso.getCodigoPostal())) {

				this
						.registraMovimientoGeneral(
								cotizacionEndosoActual.getIdToCotizacion(),
								Sistema.MSG_MOV_DOMICILIO_ASEGURADO
										+ " "
										+ obtenerDireccionCliente(aseguradoEndosoActual),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorAgente(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			if (cotizacionEndosoActual.getSolicitudDTO().getCodigoAgente()
					.intValue() != cotizacionUltimoEndoso.getSolicitudDTO()
					.getCodigoAgente().intValue()) {

				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_AGENTE
						+ " "
						+ cotizacionEndosoActual.getSolicitudDTO()
								.getNombreAgente(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorVigencia(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {

			if (!UtileriasWeb.sonFechasIguales(cotizacionEndosoActual.getFechaFinVigencia(),cotizacionUltimoEndoso.getFechaFinVigencia())) {
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_VIGENCIA
						+ " "
						+ fmt.format(cotizacionUltimoEndoso
								.getFechaInicioVigencia())
						+ " - "
						+ fmt.format(cotizacionEndosoActual
								.getFechaFinVigencia()),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorMoneda(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			if (cotizacionEndosoActual.getIdMoneda().intValue() != cotizacionUltimoEndoso
					.getIdMoneda().intValue()) {
				MonedaDTO moneda = MonedaDN.getInstancia()
						.getPorId(
								(short) cotizacionEndosoActual.getIdMoneda()
										.intValue());

				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_MONEDA + " "
						+ moneda.getDescripcion(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorMedioPago(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			if (cotizacionEndosoActual.getIdMedioPago().intValue() != cotizacionUltimoEndoso
					.getIdMedioPago().intValue()) {
				MedioPagoDTO medioPago = new MedioPagoDTO();
				medioPago.setIdMedioPago(cotizacionEndosoActual
						.getIdMedioPago().intValue());
				medioPago = MedioPagoDN.getInstancia().getPorId(medioPago);

				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_MEDIO_PAGO + " "
						+ medioPago.getDescripcion(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,false);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorFormaPago(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			if (cotizacionEndosoActual.getIdFormaPago().intValue() != cotizacionUltimoEndoso
					.getIdFormaPago().intValue()) {
				FormaPagoDTO formaPago = new FormaPagoDTO();
				formaPago.setIdFormaPago(cotizacionEndosoActual
						.getIdFormaPago().intValue());
				formaPago = FormaPagoDN.getInstancia().getPorId(formaPago);

				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_FORMA_PAGO + " "
						+ formaPago.getDescripcion(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, true);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorTipoPoliza(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			if (cotizacionEndosoActual.getTipoPolizaDTO().getIdToTipoPoliza()
					.intValue() != cotizacionUltimoEndoso.getTipoPolizaDTO()
					.getIdToTipoPoliza().intValue()) {
				TipoPolizaDTO tipoPoliza = new TipoPolizaDTO();
				tipoPoliza = TipoPolizaDN.getInstancia().getPorId(
						cotizacionEndosoActual.getTipoPolizaDTO());

				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_TIPO_POLIZA + " "
						+ tipoPoliza.getDescripcion(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL,false);
			}
		}
	}

	private void registrarDiferenciasDeEndosoPorProducto(
			CotizacionDTO cotizacionEndosoActual,
			CotizacionDTO cotizacionUltimoEndoso)
			throws ExcepcionDeAccesoADatos, SystemException {

		if (cotizacionEndosoActual != null && cotizacionUltimoEndoso != null) {
			if (cotizacionEndosoActual.getSolicitudDTO().getProductoDTO()
					.getIdToProducto().intValue() != cotizacionUltimoEndoso
					.getSolicitudDTO().getProductoDTO().getIdToProducto()
					.intValue()) {
				ProductoDTO producto = ProductoDN.getInstancia().getPorId(
						cotizacionEndosoActual.getSolicitudDTO()
								.getProductoDTO());
				this.registraMovimientoGeneral(cotizacionEndosoActual
						.getIdToCotizacion(), Sistema.MSG_MOV_PRODUCTO + " "
						+ producto.getDescripcion(),Sistema.TIPO_MOV_MODIFICACION_APP_GRAL, false);
			}
		}
	}

	private void registraMovimientoPrimerRiesgoLUC(
			AgrupacionCotDTO agrupacion, String descripcionMovimiento, short tipoMovimiento)
			throws ExcepcionDeAccesoADatos, SystemException {
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(agrupacion.getId()
				.getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToSeccion(agrupacion.getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso
				.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal
				.valueOf(agrupacion.getValorSumaAsegurada()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal
				.valueOf(agrupacion.getValorPrimaneta()));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(agrupacion
				.getValorCuota()));
		movimientoEndoso
				.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);
	}	
	private void registraModificacionSubInciso(
			SubIncisoCotizacionDTO subIncisoActual, SubIncisoCotizacionDTO subIncisoUltimo) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(subIncisoActual.getId()
				.getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(subIncisoActual.getId().getNumeroInciso());
		movimientoEndoso.setNumeroSubInciso(subIncisoActual.getId()
				.getNumeroSubInciso());
		movimientoEndoso.setIdToSeccion(subIncisoActual.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso
				.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_SUBINCISO);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal
				.valueOf(subIncisoActual.getValorSumaAsegurada()
						- subIncisoUltimo.getValorSumaAsegurada()));

		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso
				.setDescripcionMovimiento(Sistema.MSG_MOV_ALTA_SUBINCISO + " "
						+ subIncisoActual.getDescripcionSubInciso());
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);

	}

	private void registraMovimientoSubInciso(
			SubIncisoCotizacionDTO subInciso, short tipoMovimiento, String descripcionMovimiento)
			throws ExcepcionDeAccesoADatos, SystemException {
		
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(subInciso.getId().getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(subInciso.getId().getNumeroInciso());
		movimientoEndoso.setNumeroSubInciso(subInciso.getId().getNumeroSubInciso());
		movimientoEndoso.setIdToSeccion(subInciso.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(subInciso.getValorSumaAsegurada()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);
	}
	private void registraMovimientoModificacionCobertura(CoberturaCotizacionDTO cobertura,
			CoberturaCotizacionDTO coberturaUltEnd, String descripcionMovimiento, short tipoMovimiento, short agrupaMovimientos, short generaRecibos)
			throws ExcepcionDeAccesoADatos, SystemException {
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(cobertura.getId()
				.getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(cobertura.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(cobertura.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(cobertura.getId().getIdToCobertura());
		movimientoEndoso
				.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal
				.valueOf(cobertura.getValorSumaAsegurada().doubleValue()- coberturaUltEnd.getValorSumaAsegurada().doubleValue()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal
				.valueOf(cobertura.getValorPrimaNeta().doubleValue()- coberturaUltEnd.getValorPrimaNeta().doubleValue()));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(cobertura
				.getValorCuota().doubleValue()- coberturaUltEnd.getValorCuota().doubleValue()));
		movimientoEndoso
				.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(generaRecibos);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);
	}
	private void registraMovimientoAltaCobertura(
			CoberturaCotizacionDTO cobertura, String descripcionMovimiento, short agrupaMovimientos)
			throws ExcepcionDeAccesoADatos, SystemException {
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(cobertura.getId()
				.getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(cobertura.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(cobertura.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(cobertura.getId().getIdToCobertura());
		movimientoEndoso
				.setClaveTipoMovimiento(Sistema.TIPO_MOV_ALTA_COBERTURA);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal
				.valueOf(cobertura.getValorSumaAsegurada()));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal
				.valueOf(cobertura.getValorPrimaNeta()));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(cobertura
				.getValorCuota()));
		movimientoEndoso
				.setDescripcionMovimiento(Sistema.MSG_MOV_ALTA_COBERTURA + " "
						+ descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);
	}

	private void registraMovimientoBajaCobertura(
			CoberturaCotizacionDTO cobertura, String descripcionMovimiento,short agrupaMovimientos,CoberturaCotizacionDTO coberturaAnterior)
			throws ExcepcionDeAccesoADatos, SystemException {
		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(cobertura.getId()
				.getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(cobertura.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(cobertura.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(cobertura.getId().getIdToCobertura());
		movimientoEndoso
				.setClaveTipoMovimiento(Sistema.TIPO_MOV_BAJA_COBERTURA);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.valueOf(
				coberturaAnterior.getValorSumaAsegurada()).multiply(
				BigDecimal.valueOf(-1D)));
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.valueOf(
				coberturaAnterior.getValorPrimaNeta())
				.multiply(BigDecimal.valueOf(-1D)));
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.valueOf(
				coberturaAnterior.getValorCuota()).multiply(BigDecimal.valueOf(-1D)));
		movimientoEndoso
				.setDescripcionMovimiento(Sistema.MSG_MOV_BAJA_COBERTURA + " "
						+ descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);
	}

	private void registraMovimientoInciso(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, short tipoMovimiento,
			String descripcionMovimiento,short agrupaMovimientos) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(numeroInciso);
		movimientoEndoso.setIdToSeccion(BigDecimal.ZERO);
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);

	}

	private void registraMovimientoGeneral(BigDecimal idToCotizacion,
			String descripcionMovimiento, short tipoMovimiento, boolean regeneraRecibos) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToSeccion(BigDecimal.ZERO);
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso
				.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(regeneraRecibos == true? Sistema.SI_REGENERA_RECIBOS: Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);

	}

	private void registraMovimientoAnexos(BigDecimal idToCotizacion,
			String descripcionMovimiento, short tipoMovimiento, BigDecimal idToControArchivo) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(idToCotizacion);
		movimientoEndoso.setNumeroInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToSeccion(BigDecimal.ZERO);
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso
				.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(idToControArchivo);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(Sistema.NO_AGRUPADO);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);

	}	
	private void registraMovimientoSeccion(SeccionCotizacionDTO seccCotizacionDTO, short tipoMovimiento,
			String descripcionMovimiento,short agrupaMovimientos) throws ExcepcionDeAccesoADatos,
			SystemException {

		MovimientoCotizacionEndosoDTO movimientoEndoso = new MovimientoCotizacionEndosoDTO();
		movimientoEndoso.setIdToCotizacion(seccCotizacionDTO.getId().getIdToCotizacion());
		movimientoEndoso.setNumeroInciso(seccCotizacionDTO.getId().getNumeroInciso());
		movimientoEndoso.setIdToSeccion(seccCotizacionDTO.getId().getIdToSeccion());
		movimientoEndoso.setIdToCobertura(BigDecimal.ZERO);
		movimientoEndoso.setClaveTipoMovimiento(tipoMovimiento);
		movimientoEndoso.setValorDiferenciaSumaAsegurada(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaPrimaNeta(BigDecimal.ZERO);
		movimientoEndoso.setValorDiferenciaCuota(BigDecimal.ZERO);
		movimientoEndoso.setNumeroSubInciso(BigDecimal.ZERO);
		movimientoEndoso.setIdToControlArchivo(BigDecimal.ZERO);
		movimientoEndoso.setDescripcionMovimiento(descripcionMovimiento);
		movimientoEndoso.setClaveRegeneraRecibos(Sistema.NO_REGENERA_RECIBOS);
		movimientoEndoso.setClaveAgrupacion(agrupaMovimientos);
		MovimientoCotizacionEndosoSN movimientoCotizacionEndosoSN = new MovimientoCotizacionEndosoSN(
				nombreUsuario);
		movimientoCotizacionEndosoSN.agregar(movimientoEndoso);

	}	
	public String obtenerNombreCliente(ClienteDTO clienteDTO) {
		String descripcionCliente = "";
		if (clienteDTO != null) {
			if (clienteDTO.getClaveTipoPersona().intValue() == 1) {
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO
						.getNombre())) ? clienteDTO.getNombre() + " " : "";
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO
						.getApellidoPaterno())) ? clienteDTO
						.getApellidoPaterno()
						+ " " : "";
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO
						.getApellidoMaterno())) ? clienteDTO
						.getApellidoMaterno()
						+ " " : "";
			} else if (clienteDTO.getClaveTipoPersona().intValue() == 2) {
				descripcionCliente += (!UtileriasWeb.esCadenaVacia(clienteDTO
						.getNombre())) ? clienteDTO.getNombre() + " " : "";
			}
		}
		return descripcionCliente;
	}

	public String obtenerDireccionCliente(ClienteDTO clienteDTO) {
		String descripcionDireccion = "";
		if (clienteDTO != null) {
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO
					.getNombreCalle())) ? clienteDTO.getNombreCalle() + ", "
					: "";
			descripcionDireccion += (!UtileriasWeb.esObjetoNulo(clienteDTO
					.getNumeroExterior())) ? "No."
					+ clienteDTO.getNumeroExterior().toString() + ", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO
					.getNombreColonia())) ? "Colonia: "
					+ clienteDTO.getNombreColonia() + ", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO
					.getCodigoPostal())) ? "CP: "
					+ clienteDTO.getCodigoPostal() + ", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO
					.getNombreDelegacion())) ? clienteDTO.getNombreDelegacion()
					+ ", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(clienteDTO
					.getDescripcionEstado())) ? clienteDTO
					.getDescripcionEstado()
					+ " " : "";
		}
		return descripcionDireccion;
	}

	public String obtenerDireccion(DireccionDTO direccionDTO) {
		String descripcionDireccion = "";
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		if (direccionDTO != null) {
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(direccionDTO
					.getNombreCalle())) ? direccionDTO.getNombreCalle() + ", "
					: "";
			descripcionDireccion += (!UtileriasWeb.esObjetoNulo(direccionDTO
					.getNumeroExterior())) ? "No."
					+ direccionDTO.getNumeroExterior().toString() + ", " : "";

			descripcionDireccion += (!UtileriasWeb.esObjetoNulo(direccionDTO
					.getNumeroInterior())) ? "Int."
					+ direccionDTO.getNumeroInterior()+ ", " : "";					
			if (direccionDTO.getNombreColonia() != null){
				String descripcionColonia = "";
				try{
					ColoniaFacadeRemote beanRemoto = serviceLocator.getEJB(ColoniaFacadeRemote.class);
					descripcionColonia = beanRemoto.findById(direccionDTO.getNombreColonia()).getColonyName();
				}catch(SystemException e){}
				descripcionDireccion += " Colonia: "+descripcionColonia;
			}			
			descripcionDireccion += (!UtileriasWeb.esObjetoNulo(direccionDTO
					.getCodigoPostal())) ? " CP: "
					+ direccionDTO.getCodigoPostal() + ", " : "";
			descripcionDireccion += (!UtileriasWeb.esCadenaVacia(direccionDTO
					.getNombreDelegacion())) ? direccionDTO.getNombreDelegacion()
					+ ", " : "";
			if (direccionDTO.getIdMunicipio() != null){
				MunicipioFacadeRemote beanRemoto;
				try {
					beanRemoto = ServiceLocator.getInstance().getEJB(MunicipioFacadeRemote.class);
					String idMunicipio = StringUtils.leftPad(direccionDTO.getIdMunicipio().toBigInteger().toString(), 5, '0');
					descripcionDireccion += beanRemoto.findById(idMunicipio).getMunicipalityName() + ", ";
				} catch (SystemException e) {}	
			}					
			if(!UtileriasWeb.esObjetoNulo(direccionDTO.getIdEstado())){
				EstadoFacadeRemote beanRemoto;
				try {
					beanRemoto = serviceLocator.getEJB(EstadoFacadeRemote.class);
					String idEstado = direccionDTO.getIdEstado().toString().length() < 5 ? "0"+direccionDTO.getIdEstado().toString(): direccionDTO.getIdEstado().toString();
					EstadoDTO estadoDTO = beanRemoto.findByProperty("stateId", idEstado).get(0);
					descripcionDireccion += (!UtileriasWeb.esObjetoNulo(estadoDTO)) ? estadoDTO.getDescription() : "";					
				} catch (SystemException e) {}				
			}
		}
		return descripcionDireccion;
	}
	
	private ConfiguracionDatoIncisoCotizacionDTO getDatoConfiguracion(
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO){
		ConfiguracionDatoIncisoCotizacionId id = new ConfiguracionDatoIncisoCotizacionId();
		id.setIdTcRamo(datoIncisoCotizacionDTO.getId().getIdTcRamo());
		id.setIdTcSubramo(datoIncisoCotizacionDTO.getId().getIdTcSubramo());
		id.setIdToRiesgo(datoIncisoCotizacionDTO.getId().getIdToRiesgo());
		id.setClaveDetalle(datoIncisoCotizacionDTO.getId().getClaveDetalle());
		id.setIdDato(datoIncisoCotizacionDTO.getId().getIdDato());
		ConfiguracionDatoIncisoCotizacionDTO dto = new ConfiguracionDatoIncisoCotizacionDTO();
		try {
			dto = new ConfiguracionDatoIncisoCotizacionSN()
					.getPorId(id);
		} catch (ExcepcionDeAccesoADatos e) {} catch (SystemException e) {}
		return dto;
	}
	
	@SuppressWarnings("unchecked")
	private String getValorDatoConfiguracion(
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO,
			ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO){
		ServiceLocator serviceLocator;
		String valor = datoIncisoCotizacionDTO.getValor();
		String claseRemota = "";
		String valorConfiguracion = "";
		if (configuracionDatoIncisoCotizacionDTO.getClaveTipoControl() == 1){
			claseRemota = configuracionDatoIncisoCotizacionDTO.getDescripcionClaseRemota();
			try {
				serviceLocator = ServiceLocator.getInstance();
				MidasInterfaceBase beanBase = serviceLocator.getEJB(claseRemota);
				CacheableDTO dto = beanBase.findById(UtileriasWeb.regresaBigDecimal(valor));
				valorConfiguracion = dto.getDescription();
			} catch (ClassNotFoundException e) {} catch (SystemException e) {}			
		}
		if(configuracionDatoIncisoCotizacionDTO.getClaveTipoControl() == 2){
			try {			
				serviceLocator = ServiceLocator.getInstance();
				CatalogoValorFijoFacadeRemote beanCatalogoValorFijo = serviceLocator.getEJB(CatalogoValorFijoFacadeRemote.class);
				CatalogoValorFijoId id = new CatalogoValorFijoId();
				id.setIdDato(Integer.valueOf(valor));
				id.setIdGrupoValores(configuracionDatoIncisoCotizacionDTO.getIdGrupo().intValue());
				CatalogoValorFijoDTO dto = (CatalogoValorFijoDTO) beanCatalogoValorFijo.findById(id);
				valorConfiguracion = dto.getDescripcion();
			} catch (SystemException e) {}	
		}
		if(configuracionDatoIncisoCotizacionDTO.getClaveTipoControl() == 3 ||
				configuracionDatoIncisoCotizacionDTO.getClaveTipoControl() == 4){
			valorConfiguracion = valor;
		}
		return valorConfiguracion;
	}
	
	/**
	 * Obtiene movimientos con los distintos incisos y coberturas manejadas en la cotizacion de un endoso,
	 * esto para su evaluacion en el proceso de Cancelacion de Endosos
	 * @param idToCotizacion Id de la cotizacion del endoso a evaluar
	 * @return Lista de movimientos con los distintos incisos y coberturas manejadas en la cotizacion de un endoso
	 * @throws SystemException
	 */
	public List<MovimientoCotizacionEndosoDTO> obtieneMovimientosAEvaluar(
			BigDecimal idToCotizacion) throws SystemException {
		return new MovimientoCotizacionEndosoSN("").obtieneMovimientosAEvaluar(idToCotizacion);
	}

	/**
	 * Obtiene el inverso a un movimiento
	 * @param movimiento Movimiento a invertir
	 * @return Movimiento inverso
	 */
	public MovimientoCotizacionEndosoDTO obtieneMovimientoInverso (MovimientoCotizacionEndosoDTO movimiento) {
		/**
		 * 1:Alta de Cobertura, 2:Baja de Cobertura, 3:Modificacion de Cobertura, 4:Alta de Seccion, 5:Baja de Seccion, 
		 * 6:Modificacion de Seccion, 7:Alta de Inciso, 8:Baja de Inciso, 9:Modificacion de Inciso, 10:Movimiento de aplicacion general, 
		 * 11:Alta de SubInciso, 12:Baja de SubInciso, 13:Modificacion de SubInciso, 14:Alta Primer Riesgo, 15:Baja Primer Riesgo, 
		 * 16:Modificacion Primer Riesgo, 17:Modificacion de Prima Neta para cobertura, 18:Modificacion de Coaseguro para Cobertura, 
		 * 19:Modificacion de Deducible para Cobertura
		 *  
		 */
		
		MovimientoCotizacionEndosoDTO movimientoInverso = copiaMovimiento(movimiento, Sistema.MSG_MOV_PREFIJO_CANCELADO);
		
		
		switch (movimiento.getClaveTipoMovimiento().shortValue()) {
		
			case Sistema.TIPO_MOV_ALTA_COBERTURA: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_BAJA_COBERTURA);
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaPrimaNeta(
						movimientoInverso.getValorDiferenciaPrimaNeta().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaCuota(
						movimientoInverso.getValorDiferenciaCuota().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_BAJA_COBERTURA: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_ALTA_COBERTURA);
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaPrimaNeta(
						movimientoInverso.getValorDiferenciaPrimaNeta().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaCuota(
						movimientoInverso.getValorDiferenciaCuota().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_COBERTURA: {
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaPrimaNeta(
						movimientoInverso.getValorDiferenciaPrimaNeta().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaCuota(
						movimientoInverso.getValorDiferenciaCuota().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_ALTA_SECCION: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_BAJA_SECCION);
				break;
			}
			case Sistema.TIPO_MOV_BAJA_SECCION: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_ALTA_SECCION);
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_SECCION: {
				//Nada
				break;
			}
			case Sistema.TIPO_MOV_ALTA_INCISO: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_BAJA_INCISO);
				break;
			}
			case Sistema.TIPO_MOV_BAJA_INCISO: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_ALTA_INCISO);
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_INCISO: {
				//Nada
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_APP_GRAL: {
				//Nada
				break;
			}
			case Sistema.TIPO_MOV_ALTA_SUBINCISO: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_BAJA_SUBINCISO);
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_BAJA_SUBINCISO: {
				movimientoInverso.setClaveTipoMovimiento(Sistema.TIPO_MOV_ALTA_SUBINCISO);
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_SUBINCISO: {
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_ALTA_1ER_RIESGO_LUC: {
				//No hay movimientos de este tipo aun
				break;
			}
			case Sistema.TIPO_MOV_BAJA_1ER_RIESGO_LUC: {
				//No hay movimientos de este tipo aun
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_1ER_RIESGO_LUC: {
				//No hay movimientos de este tipo aun
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_PRIMA_NETA: {
				movimientoInverso.setValorDiferenciaSumaAsegurada(
						movimientoInverso.getValorDiferenciaSumaAsegurada().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaPrimaNeta(
						movimientoInverso.getValorDiferenciaPrimaNeta().multiply(BigDecimal.valueOf(-1D)));
				movimientoInverso.setValorDiferenciaCuota(
						movimientoInverso.getValorDiferenciaCuota().multiply(BigDecimal.valueOf(-1D)));
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_COASEGURO: {
				//Nada
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_DEDUCIBLE: {
				//Nada
				break;
			}
			
		}
		
		return movimientoInverso;
			
	}
	
	
	/**
	 * Aplica un movimiento a una cotizaci�n
	 * @param movimiento Movimiento a aplicar
	 * @param cotizacion Cotizaci�n a la cual se le aplicar� el movimiento
	 * @param cotizacionAnterior Cotizacion del endoso anterior al movimiento, esto es para obtener los antiguos datos
	 * @param nombreUsuario Nombre del usuario que realiza la operacion
	 * @return Objeto de la estructura de la cotizacion del Endoso en caso de que necesite ser recalculado, null en caso
	 * contrario
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public Object aplicaMovimientoACotizacion(MovimientoCotizacionEndosoDTO movimiento, CotizacionDTO cotizacion, 
			CotizacionDTO cotizacionAnterior, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException {
		/**
		 * Es importante que los movimientos aplicados a la cotizacion esten en el orden correcto
		 * (Ej. No se puede aplicar un movimiento de alta de seccion sin antes aplicar un movimiento de alta del inciso al cual pertenece)
		 * 
		 * 
		 * 
		 * 1:Alta de Cobertura, 2:Baja de Cobertura, 3:Modificacion de Cobertura, 4:Alta de Seccion, 5:Baja de Seccion, 
		 * 6:Modificacion de Seccion, 7:Alta de Inciso, 8:Baja de Inciso, 9:Modificacion de Inciso, 10:Movimiento de aplicacion general, 
		 * 11:Alta de SubInciso, 12:Baja de SubInciso, 13:Modificacion de SubInciso, 14:Alta Primer Riesgo, 15:Baja Primer Riesgo, 
		 * 16:Modificacion Primer Riesgo, 17:Modificacion de Prima Neta para cobertura, 18:Modificacion de Coaseguro para Cobertura, 
		 * 19:Modificacion de Deducible para Cobertura
		 */
		
		//Se guarda el movimiento en BD, antes referenciandolo a la nueva cotizacion
		movimiento.setIdToCotizacion(cotizacion.getIdToCotizacion());
		this.agregar(movimiento);
		
		SoporteMovimientoCotizacionEndoso soporte = new SoporteMovimientoCotizacionEndoso(movimiento, cotizacion, cotizacionAnterior);
		
		switch (movimiento.getClaveTipoMovimiento().shortValue()) {
		
			case Sistema.TIPO_MOV_ALTA_COBERTURA: {
				soporte.contrataCobertura();
				break;
			}
			case Sistema.TIPO_MOV_BAJA_COBERTURA: {
				soporte.descontrataCobertura();
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_COBERTURA: {
				soporte.modificaPrimaNetaSumaAseguradaCobertura();
				break;
			}
			case Sistema.TIPO_MOV_ALTA_SECCION: {
				soporte.contrataSeccion();
				break;
			}
			case Sistema.TIPO_MOV_BAJA_SECCION: {
				soporte.descontrataSeccion();
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_SECCION: {
				//Aqui no se hace nada, este movimiento solo es creado cuando se crea otro movimiento relacionado 
				//(cobertura, subinciso, etc)
				break;
			}
			case Sistema.TIPO_MOV_ALTA_INCISO: {
				soporte.agregaInciso();
				break;
			}
			case Sistema.TIPO_MOV_BAJA_INCISO: {
				soporte.eliminaInciso();
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_INCISO: {
				
				if (movimiento.getDescripcionMovimiento().indexOf(Sistema.MSG_MOV_MODIFICACION_INCISO) != -1) {
					soporte.modificaInciso();
				} else {
					soporte.modificaDatosInciso();
				}
				
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_APP_GRAL: {
				soporte.aplicaMovimientoGeneral();
				break;
			}
			case Sistema.TIPO_MOV_ALTA_SUBINCISO: {
				soporte.agregaSubInciso();
				break;
			}
			case Sistema.TIPO_MOV_BAJA_SUBINCISO: {
				soporte.eliminaSubInciso();
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_SUBINCISO: {
				
				if (movimiento.getValorDiferenciaSumaAsegurada().intValue() != 0) {
					soporte.modificaSubInciso();
				} else {
					soporte.modificaDatosSubInciso();
				}
				break;
			}
			case Sistema.TIPO_MOV_ALTA_1ER_RIESGO_LUC: {
				//No se utiliza
				break;
			}
			case Sistema.TIPO_MOV_BAJA_1ER_RIESGO_LUC: {
				//No se utiliza
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_1ER_RIESGO_LUC: {
				//No se utiliza
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_PRIMA_NETA: {
				soporte.modificaPrimaNetaSumaAseguradaCobertura();
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_COASEGURO: {
				soporte.modificaPorcentajeCoaseguroCobertura();
				break;
			}
			case Sistema.TIPO_MOV_MODIFICACION_DEDUCIBLE: {
				soporte.modificaDeducibleCobertura();
				break;
			}
			
		}
		return soporte.getObjetoARecalcular();
		
	}
	
	/**
	 * Copia un movimiento
	 * @param movimientoOrginal Movimiento original
	 * @param prefijoCopia Prefijo que se antepondra a la descripcion de la copia del movimiento, esto para facilitar su identificacion
	 * Ej. '(C)' para Cancelado, '(R)' para Rehabilitado
	 * @return La copia del movimiento original
	 */
	public MovimientoCotizacionEndosoDTO copiaMovimiento (MovimientoCotizacionEndosoDTO movimientoOriginal, String prefijoCopia) {
		
		MovimientoCotizacionEndosoDTO movimiento = new MovimientoCotizacionEndosoDTO();
		movimiento.setIdToCotizacion(movimientoOriginal.getIdToCotizacion());
		movimiento.setClaveAgrupacion(movimientoOriginal.getClaveAgrupacion());
		movimiento.setNumeroInciso(movimientoOriginal.getNumeroInciso());
		movimiento.setIdToSeccion(movimientoOriginal.getIdToSeccion());
		movimiento.setDescripcionSeccion(movimientoOriginal.getDescripcionSeccion());
		movimiento.setNumeroSubInciso(movimientoOriginal.getNumeroSubInciso());
		movimiento.setIdToCobertura(movimientoOriginal.getIdToCobertura());
		movimiento.setClaveTipoMovimiento(movimientoOriginal.getClaveTipoMovimiento());
		movimiento.setDescripcionMovimiento(prefijoCopia + " " + movimientoOriginal.getDescripcionMovimiento());
		movimiento.setValorDiferenciaSumaAsegurada(movimientoOriginal.getValorDiferenciaSumaAsegurada());
		movimiento.setValorDiferenciaPrimaNeta(movimientoOriginal.getValorDiferenciaPrimaNeta());
		movimiento.setValorDiferenciaCuota(movimientoOriginal.getValorDiferenciaCuota());
		movimiento.setClaveRegeneraRecibos(movimientoOriginal.getClaveRegeneraRecibos());
		movimiento.setIdToControlArchivo(movimientoOriginal.getIdToControlArchivo());
		
		return movimiento;
	}
	
}
