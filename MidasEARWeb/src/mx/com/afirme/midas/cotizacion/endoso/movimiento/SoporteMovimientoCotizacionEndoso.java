package mx.com.afirme.midas.cotizacion.endoso.movimiento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotId;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoSN;
import mx.com.afirme.midas.endoso.cancelacion.CoberturaPrimaModificada;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SoporteMovimientoCotizacionEndoso {

	private MovimientoCotizacionEndosoDTO movimiento;
	private CotizacionDTO cotizacion;
	private CotizacionDTO cotizacionAnterior;
	private boolean movimientoTextoAdicionalAplicado;
	private boolean movimientoContratanteAplicado;
	private boolean movimientoAseguradoAplicado;
	private Object objetoARecalcular;
	
	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}
	
	public Object getObjetoARecalcular() {
		return objetoARecalcular;
	}

	public SoporteMovimientoCotizacionEndoso(MovimientoCotizacionEndosoDTO movimiento, CotizacionDTO cotizacion, 
			CotizacionDTO cotizacionAnterior) {
		
		this.movimiento = movimiento;
		this.cotizacion = cotizacion;
		this.cotizacionAnterior = cotizacionAnterior;
		this.movimientoTextoAdicionalAplicado = false;
		this.movimientoContratanteAplicado = false;
		this.movimientoAseguradoAplicado = false;
		this.objetoARecalcular = null;
	}
	
	 
	public void contrataCobertura() throws ExcepcionDeAccesoADatos, SystemException {
		contrataDescontrataCobertura(true);
	}
	
	public void descontrataCobertura() throws ExcepcionDeAccesoADatos, SystemException {
		contrataDescontrataCobertura(false);
	}
	
	
	public void modificaPrimaNetaSumaAseguradaCobertura() throws ExcepcionDeAccesoADatos, SystemException {
		
//		boolean modificar = false;
		
		CoberturaCotizacionDTO coberturaCotizacion = obtieneCoberturaCotizacion();
		CoberturaCotizacionDTO coberturaCotizacionAnterior = obtieneCoberturaCotizacionAnterior();
		
		if (coberturaCotizacion != null) {
			if (movimiento.getValorDiferenciaSumaAsegurada().intValue() != 0) { 
				coberturaCotizacion.setValorSumaAsegurada(coberturaCotizacion.getValorSumaAsegurada() + 
						movimiento.getValorDiferenciaSumaAsegurada().doubleValue());
//				modificar = true;
			}
			
//			if (movimiento.getValorDiferenciaPrimaNeta().intValue() != 0) {
//				coberturaCotizacion.setValorPrimaNeta(coberturaCotizacion.getValorPrimaNeta() + 
//						movimiento.getValorDiferenciaPrimaNeta().doubleValue());
//				modificar = true;
//			}
			
//			if (modificar) {
			
//				CoberturaCotizacionDN.getInstancia().modificar(coberturaCotizacion);
				
//				if (movimiento.getValorDiferenciaSumaAsegurada().intValue() != 0) {
//					objetoARecalcular = coberturaCotizacion;
//				} else {
//					CoberturaPrimaModificada coberturaPrimaMod = new CoberturaPrimaModificada();
//					coberturaPrimaMod.setIdToCotizacion(coberturaCotizacion.getId().getIdToCotizacion());
//					coberturaPrimaMod.setNumeroInciso(coberturaCotizacion.getId().getNumeroInciso());
//					coberturaPrimaMod.setIdToSeccion(coberturaCotizacion.getId().getIdToSeccion());
//					coberturaPrimaMod.setIdToCobertura(coberturaCotizacion.getId().getIdToCobertura());
//					coberturaPrimaMod.setPrimaNeta(coberturaCotizacion.getValorPrimaNeta());
//					
//					objetoARecalcular = coberturaPrimaMod;
				
//				}
//			}
			
					coberturaCotizacion.setValorPrimaNeta(coberturaCotizacionAnterior.getValorPrimaNeta());
					
					CoberturaCotizacionDN.getInstancia().modificar(coberturaCotizacion);
					
					CoberturaPrimaModificada coberturaPrimaMod = new CoberturaPrimaModificada();
					coberturaPrimaMod.setIdToCotizacion(coberturaCotizacion.getId().getIdToCotizacion());
					coberturaPrimaMod.setNumeroInciso(coberturaCotizacion.getId().getNumeroInciso());
					coberturaPrimaMod.setIdToSeccion(coberturaCotizacion.getId().getIdToSeccion());
					coberturaPrimaMod.setIdToCobertura(coberturaCotizacion.getId().getIdToCobertura());
					coberturaPrimaMod.setPrimaNeta(coberturaCotizacion.getValorPrimaNeta());
					
					objetoARecalcular = coberturaPrimaMod;
					
					
					
		}
		
	}
	
	
	public void modificaPorcentajeCoaseguroCobertura() throws ExcepcionDeAccesoADatos, SystemException {
		
		CoberturaCotizacionDTO coberturaCotizacion = obtieneCoberturaCotizacion();
		CoberturaCotizacionDTO coberturaCotizacionAnterior = obtieneCoberturaCotizacionAnterior();
		if (coberturaCotizacion != null) {
			coberturaCotizacion.setPorcentajeCoaseguro(coberturaCotizacionAnterior.getPorcentajeCoaseguro());
			
			CoberturaCotizacionDN.getInstancia().modificar(coberturaCotizacion);
		}
	}
	
	public void modificaDeducibleCobertura() throws ExcepcionDeAccesoADatos, SystemException {
				
		CoberturaCotizacionDTO coberturaCotizacion = obtieneCoberturaCotizacion();
		CoberturaCotizacionDTO coberturaCotizacionAnterior = obtieneCoberturaCotizacionAnterior();
		if (coberturaCotizacion != null) {
			coberturaCotizacion.setPorcentajeDeducible(coberturaCotizacionAnterior.getPorcentajeDeducible());
			coberturaCotizacion.setClaveTipoDeducible(coberturaCotizacionAnterior.getClaveTipoDeducible());
			coberturaCotizacion.setClaveTipoLimiteDeducible(coberturaCotizacionAnterior.getClaveTipoLimiteDeducible());
			coberturaCotizacion.setValorMaximoLimiteDeducible(coberturaCotizacionAnterior.getValorMaximoLimiteDeducible());
			coberturaCotizacion.setValorMinimoLimiteDeducible(coberturaCotizacionAnterior.getValorMinimoLimiteDeducible());
			
			CoberturaCotizacionDN.getInstancia().modificar(coberturaCotizacion);		
		}		
	}
	
	public void contrataSeccion() throws ExcepcionDeAccesoADatos, SystemException {
		contrataDescontrataSeccion(true);
	}
	
	public void descontrataSeccion() throws ExcepcionDeAccesoADatos, SystemException {
		contrataDescontrataSeccion(false);
	}
	
	public void agregaSubInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		SubIncisoCotizacionDTO subIncisoCotizacionAnterior = obtieneSubIncisoCotizacionAnterior();
		SubIncisoCotizacionDTO subIncisoCotizacion = new SubIncisoCotizacionDTO();
		
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId();
		subIncisoCotizacionDTOId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		subIncisoCotizacionDTOId.setNumeroInciso(subIncisoCotizacionAnterior.getId().getNumeroInciso());
		subIncisoCotizacionDTOId.setIdToSeccion(subIncisoCotizacionAnterior.getId().getIdToSeccion());
		subIncisoCotizacionDTOId.setNumeroSubInciso(subIncisoCotizacionAnterior.getId().getNumeroSubInciso());

		subIncisoCotizacion.setId(subIncisoCotizacionDTOId);
		subIncisoCotizacion.setClaveAutReaseguro(subIncisoCotizacionAnterior.getClaveAutReaseguro());
		subIncisoCotizacion.setCodigoUsuarioAutReaseguro(subIncisoCotizacionAnterior.getCodigoUsuarioAutReaseguro());
		subIncisoCotizacion.setDescripcionSubInciso(subIncisoCotizacionAnterior.getDescripcionSubInciso());
		subIncisoCotizacion.setValorSumaAsegurada(subIncisoCotizacionAnterior.getValorSumaAsegurada());
		subIncisoCotizacion.setClaveEstatusDeclaracion(subIncisoCotizacionAnterior.getClaveEstatusDeclaracion());
		
		SubIncisoCotizacionDN.getInstancia().agregar(subIncisoCotizacion, Sistema.USUARIO_SISTEMA);
		objetoARecalcular = subIncisoCotizacion;
		
		//Agregar los datos de reisgo del subinciso para evitar que los SP de c�lculo no generen los detalles de prima.
		DatoIncisoCotizacionId idDatoInciso = new DatoIncisoCotizacionId(subIncisoCotizacionAnterior.getId().getIdToCotizacion(),
				subIncisoCotizacionAnterior.getId().getNumeroInciso(), 
				subIncisoCotizacionAnterior.getId().getIdToSeccion(), 
				null, null, subIncisoCotizacionAnterior.getId().getNumeroSubInciso(), null, null, (short)2, null);
		List<DatoIncisoCotizacionDTO> listaDatosIncisoCotAnterior = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(idDatoInciso);
		if(listaDatosIncisoCotAnterior != null && !listaDatosIncisoCotAnterior.isEmpty()){
			for(DatoIncisoCotizacionDTO datoCotNueva : listaDatosIncisoCotAnterior){
				datoCotNueva.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());
				DatoIncisoCotizacionDN.getINSTANCIA().agregar(datoCotNueva);
			}
		}
	}
	
	public void eliminaSubInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		SubIncisoCotizacionDTO subIncisoCotizacion = obtieneSubIncisoCotizacion();
		
		List<SubIncisoCotizacionDTO> subIncisoCotList = SubIncisoCotizacionDN.getInstancia()
			.borrarSubIncisoCotizacionCE(subIncisoCotizacion, Sistema.USUARIO_SISTEMA);
		
		//SE AGREGA ELIMINACION DE DETALLES PRIMAS COBERTURAS Y DETALLES PRIMAS RIESGO
		CotizacionDN.getInstancia(Sistema.USUARIO_SISTEMA)
			.eliminaDetallesPrimasDeCoberturayRiesgoPorSubInciso(subIncisoCotizacion);
		
		if (subIncisoCotList != null && subIncisoCotList.size() > 0) {
			objetoARecalcular = subIncisoCotList.get(0);	
		}
		
		
	}
	
	
	public void modificaSubInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		SubIncisoCotizacionDTO subIncisoCotizacion = obtieneSubIncisoCotizacion();
				
		subIncisoCotizacion.setValorSumaAsegurada(subIncisoCotizacion.getValorSumaAsegurada() + 
				movimiento.getValorDiferenciaSumaAsegurada().doubleValue());
		
		SubIncisoCotizacionDN.getInstancia().modificarSinCalcular(subIncisoCotizacion, Sistema.USUARIO_SISTEMA);
		objetoARecalcular = subIncisoCotizacion;
	}
	
	public void modificaDatosSubInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		DatoIncisoCotizacionDTO datoSubIncisoCotizacionAnterior;
		DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
		id.setIdToCotizacion(cotizacion.getIdToCotizacion());
		id.setNumeroInciso(movimiento.getNumeroInciso());
		id.setNumeroSubinciso(movimiento.getNumeroSubInciso());
		List<DatoIncisoCotizacionDTO> listaDatosSubIncisoCotizacion = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
		
		for (DatoIncisoCotizacionDTO datoSubIncisoCotizacion: listaDatosSubIncisoCotizacion) {
			
			id = datoSubIncisoCotizacion.getId();
			id.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
			datoSubIncisoCotizacionAnterior = new DatoIncisoCotizacionDTO();
			datoSubIncisoCotizacionAnterior.setId(id);
			datoSubIncisoCotizacionAnterior = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoSubIncisoCotizacionAnterior);
			
			if (datoSubIncisoCotizacionAnterior != null) {
				if (!datoSubIncisoCotizacion.getValor().trim().equals(datoSubIncisoCotizacionAnterior.getValor().trim())) {
					datoSubIncisoCotizacion.setValor(datoSubIncisoCotizacionAnterior.getValor().trim());
					id.setIdToCotizacion(cotizacion.getIdToCotizacion());
					DatoIncisoCotizacionDN.getINSTANCIA().modificar(datoSubIncisoCotizacion);
				}
			}
		}
		
	}
	public void agregaInciso() throws ExcepcionDeAccesoADatos, SystemException {
	
		copiaIncisoConRelaciones(cotizacionAnterior, cotizacion, movimiento.getNumeroInciso());
		
	}
	
	public void eliminaInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		IncisoCotizacionDTO incisoCotizacion = obtieneIncisoCotizacion();
		
		IncisoCotizacionDN.getInstancia().borrarInciso(incisoCotizacion);
	}
	
	
	public void modificaInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		IncisoCotizacionDTO incisoCotizacion = obtieneIncisoCotizacion();
		IncisoCotizacionDTO incisoCotizacionAnterior = obtieneIncisoCotizacionAnterior();
		
		incisoCotizacion.setDireccionDTO(incisoCotizacionAnterior.getDireccionDTO());
		
		IncisoCotizacionDN.getInstancia().modificar(incisoCotizacion);
	}
	
	public void modificaDatosInciso() throws ExcepcionDeAccesoADatos, SystemException {
		
		DatoIncisoCotizacionDTO datoIncisoCotizacionAnterior;
		DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
		id.setIdToCotizacion(cotizacion.getIdToCotizacion());
		id.setNumeroInciso(movimiento.getNumeroInciso());
		List<DatoIncisoCotizacionDTO> listaDatosIncisoCotizacion = DatoIncisoCotizacionDN.getINSTANCIA().listarPorIdFiltrado(id);
		
		for (DatoIncisoCotizacionDTO datoIncisoCotizacion: listaDatosIncisoCotizacion) {
						
			id = datoIncisoCotizacion.getId();
			id.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
			datoIncisoCotizacionAnterior = new DatoIncisoCotizacionDTO();
			datoIncisoCotizacionAnterior.setId(id);
			datoIncisoCotizacionAnterior = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionAnterior);
			
			if (datoIncisoCotizacionAnterior != null) {
				if (!datoIncisoCotizacion.getValor().trim().equals(datoIncisoCotizacionAnterior.getValor().trim())) {
					datoIncisoCotizacion.setValor(datoIncisoCotizacionAnterior.getValor().trim());
					id.setIdToCotizacion(cotizacion.getIdToCotizacion());
					DatoIncisoCotizacionDN.getINSTANCIA().modificar(datoIncisoCotizacion);
				}
			}
		}
		
	}
	
	public void aplicaMovimientoGeneral() throws ExcepcionDeAccesoADatos, SystemException {
		
		String descripcionMovimiento = movimiento.getDescripcionMovimiento();
		
		if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_TIPO_ENDOSO) != -1) {
			//Nada
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ALTA_DOCUMENTO_ANEXO) != -1) {
			aplicaMovimientosDocumentoAnexo();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_BAJA_DOCUMENTO_ANEXO) != -1) {
			aplicaMovimientosDocumentoAnexo();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_MODIFICACION_TEXTO_ADICIONAL) != -1) {
			aplicaMovimientosTextoAdicional();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ALTA_TEXTO_ADICIONAL) != -1) {
			aplicaMovimientosTextoAdicional();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_BAJA_TEXTO_ADICIONAL) != -1) {
			aplicaMovimientosTextoAdicional();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ID_ALTA_PRIMER_RIESGO) != -1) {
			if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_PREFIJO_CANCELADO) != -1) {//Si es un movimiento de cancelacion
				eliminaPrimerRiesgoLUC();
			} else {
				agregaPrimerRiesgoLUC();
			}
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ID_BAJA_PRIMER_RIESGO) != -1) {
			if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_PREFIJO_CANCELADO) != -1) {//Si es un movimiento de cancelacion
				agregaPrimerRiesgoLUC();
			} else {
				eliminaPrimerRiesgoLUC();
			}
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ID_MODIFICACION_PRIMER_RIESGO) != -1) {
			modificaPrimerRiesgoLUC();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ID_ALTA_LUC) != -1) {
			if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_PREFIJO_CANCELADO) != -1) {//Si es un movimiento de cancelacion
				eliminaPrimerRiesgoLUC();
			} else {
				agregaPrimerRiesgoLUC();
			}
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ID_BAJA_LUC) != -1) {
			if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_PREFIJO_CANCELADO) != -1) {//Si es un movimiento de cancelacion
				agregaPrimerRiesgoLUC();
			} else {
				eliminaPrimerRiesgoLUC();
			}
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_ID_MODIFICACION_LUC) != -1) {
			modificaPrimerRiesgoLUC();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_NOMBRE_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_TIPO_PERSONA_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_RFC_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_FECHA_NACIMIENTO_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_TELEFONO_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_CORREO_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_DOMICILIO_CONTRATANTE) != -1) {
			aplicaMovimientosContratante();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_NOMBRE_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_TIPO_PERSONA_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_RFC_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_FECHA_NACIMIENTO_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_TELEFONO_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_CORREO_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_DOMICILIO_ASEGURADO) != -1) {
			aplicaMovimientosAsegurado();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_AGENTE) != -1) {
			modificaAgente();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_VIGENCIA) != -1) {
			modificaVigencia();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_MONEDA) != -1) {
			modificaMoneda();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_MEDIO_PAGO) != -1) {
			modificaMedioPago();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_FORMA_PAGO) != -1) {
			modificaFormaPago();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_TIPO_POLIZA) != -1) {
			modificaTipoPoliza();
		} else if (descripcionMovimiento.indexOf(Sistema.MSG_MOV_PRODUCTO) != -1) {
			modificaProducto();
		}
		
	}
			
	private void aplicaMovimientosDocumentoAnexo() throws ExcepcionDeAccesoADatos, SystemException {
		
		boolean existe = false;
		DocAnexoCotDTO documentoAnexo = this.obtieneDocumentoAnexoCotizacion();
		DocAnexoCotDTO documentoAnexoAnterior = this.obtieneDocumentoAnexoCotizacionAnterior();
		
		if(documentoAnexoAnterior != null) {
			if (documentoAnexo != null) {
				existe = true;
				documentoAnexo.setClaveSeleccion(documentoAnexoAnterior.getClaveSeleccion());
				
			} else {
				
				documentoAnexo = new DocAnexoCotDTO();
				
				DocAnexoCotId docAnexoCotId = new DocAnexoCotId();
				docAnexoCotId.setIdToCotizacion(cotizacion.getIdToCotizacion());
				docAnexoCotId.setIdToControlArchivo(documentoAnexoAnterior.getId().getIdToControlArchivo());
						
				documentoAnexo.setId(docAnexoCotId);
						
				documentoAnexo.setFechaCreacion(documentoAnexoAnterior.getFechaCreacion());
				documentoAnexo.setCodigoUsuarioCreacion(documentoAnexoAnterior.getCodigoUsuarioCreacion());
				documentoAnexo.setNombreUsuarioCreacion(documentoAnexoAnterior.getNombreUsuarioCreacion());
				documentoAnexo.setFechaModificacion(documentoAnexoAnterior.getFechaModificacion());
				documentoAnexo.setCodigoUsuarioModificacion(documentoAnexoAnterior.getCodigoUsuarioModificacion());
				documentoAnexo.setNombreUsuarioModificacion(documentoAnexoAnterior.getNombreUsuarioModificacion());
				documentoAnexo.setDescripcionDocumentoAnexo(documentoAnexoAnterior.getDescripcionDocumentoAnexo());
				documentoAnexo.setClaveObligatoriedad(documentoAnexoAnterior.getClaveObligatoriedad());
				documentoAnexo.setClaveSeleccion(documentoAnexoAnterior.getClaveSeleccion());
				documentoAnexo.setOrden(documentoAnexoAnterior.getOrden());
				documentoAnexo.setClaveTipo(documentoAnexoAnterior.getClaveTipo());
				
				
			}
			
			if (existe) {
				DocAnexoCotDN.getInstancia().modificar(documentoAnexo);
			} else {
				DocAnexoCotDN.getInstancia().agregar(documentoAnexo);
			}
		} else {
			if (documentoAnexo != null) {
				documentoAnexo.setClaveSeleccion((short)Sistema.NO_SELECCIONADO);
				DocAnexoCotDN.getInstancia().modificar(documentoAnexo);
			}
		}
		
		
		
	}
	
	private void aplicaMovimientosTextoAdicional() throws ExcepcionDeAccesoADatos, SystemException {
		
		if (!movimientoTextoAdicionalAplicado) {
			//Se borran los textos Adicionales originales
			List<TexAdicionalCotDTO> textosAdicionales = TexAdicionalCotDN.getInstancia()
			.buscarPorPropiedad("cotizacion.idToCotizacion", cotizacion.getIdToCotizacion());
			
			for (TexAdicionalCotDTO textoAdicional : textosAdicionales) {
				TexAdicionalCotDN.getInstancia().borrar(textoAdicional);
			}
			
			//Cada uno de los textos Adicionales anteriores se vuelve a guardar
			List<TexAdicionalCotDTO> textosAdicionalesAnteriores = TexAdicionalCotDN.getInstancia()
			.buscarPorPropiedad("cotizacion.idToCotizacion", cotizacionAnterior.getIdToCotizacion());
			
			
			for (TexAdicionalCotDTO textoAdicional : textosAdicionalesAnteriores) {
				textoAdicional.setIdToTexAdicionalCot(null);
				textoAdicional.setCotizacion(cotizacion);
				TexAdicionalCotDN.getInstancia().agregar(textoAdicional);
			}
			
			movimientoTextoAdicionalAplicado = true;
		}
		
	}
	
	private void agregaPrimerRiesgoLUC() throws ExcepcionDeAccesoADatos, SystemException {
		
		AgrupacionCotDTO primerRiesgoLUCAnterior = this.obtienePrimerRiesgoLUCCotizacionAnterior();
		
		AgrupacionCotDTO primerRiesgoLUC = new AgrupacionCotDTO();
		
		AgrupacionCotId agrupacionCotId = new AgrupacionCotId();
		agrupacionCotId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		agrupacionCotId.setNumeroAgrupacion(primerRiesgoLUCAnterior.getId().getNumeroAgrupacion());
		
		primerRiesgoLUC.setId(agrupacionCotId);
		
		primerRiesgoLUC.setClaveTipoAgrupacion(primerRiesgoLUCAnterior.getClaveTipoAgrupacion());
		primerRiesgoLUC.setIdToSeccion(primerRiesgoLUCAnterior.getIdToSeccion());
		primerRiesgoLUC.setValorSumaAsegurada(primerRiesgoLUCAnterior.getValorSumaAsegurada());
		primerRiesgoLUC.setValorCuota(primerRiesgoLUCAnterior.getValorCuota());
		primerRiesgoLUC.setValorPrimaneta(primerRiesgoLUCAnterior.getValorPrimaneta());
		primerRiesgoLUC.setDescripcionSeccion(primerRiesgoLUCAnterior.getDescripcionSeccion());
		
		PrimerRiesgoLUCDN.getInstancia().agregar(primerRiesgoLUC);
		
		
	}
	
	private void eliminaPrimerRiesgoLUC() throws ExcepcionDeAccesoADatos, SystemException {
		
		AgrupacionCotDTO primerRiesgoLUC = this.obtienePrimerRiesgoLUCCotizacion();
		PrimerRiesgoLUCDN.getInstancia().borrar(primerRiesgoLUC);
		
	}
	
	private void modificaPrimerRiesgoLUC() throws ExcepcionDeAccesoADatos, SystemException {
		
		AgrupacionCotDTO primerRiesgoLUC = this.obtienePrimerRiesgoLUCCotizacion();
		AgrupacionCotDTO primerRiesgoLUCAnterior = this.obtienePrimerRiesgoLUCCotizacionAnterior();
		
		primerRiesgoLUC.setValorSumaAsegurada(primerRiesgoLUCAnterior.getValorSumaAsegurada());
		primerRiesgoLUC.setValorCuota(primerRiesgoLUCAnterior.getValorCuota());
		primerRiesgoLUC.setValorPrimaneta(primerRiesgoLUCAnterior.getValorPrimaneta());
		PrimerRiesgoLUCDN.getInstancia().modificar(primerRiesgoLUC);
		
	}
	
	private void aplicaMovimientosContratante() throws ExcepcionDeAccesoADatos, SystemException {
		
		if (!movimientoContratanteAplicado) {
			cotizacion.setIdToPersonaContratante(cotizacionAnterior.getIdToPersonaContratante());			
			movimientoContratanteAplicado = true;
		}
		
	}
	
	private void aplicaMovimientosAsegurado() throws ExcepcionDeAccesoADatos, SystemException {
		
		if (!movimientoAseguradoAplicado) {
			cotizacion.setIdToPersonaAsegurado(cotizacionAnterior.getIdToPersonaAsegurado());			
			movimientoAseguradoAplicado = true;
		}
		
	}
	
	
	private void modificaAgente() throws ExcepcionDeAccesoADatos, SystemException {
		
		cotizacion.getSolicitudDTO().setCodigoAgente(cotizacionAnterior.getSolicitudDTO().getCodigoAgente()); 
		cotizacion.getSolicitudDTO().setNombreAgente(cotizacionAnterior.getSolicitudDTO().getNombreAgente());
		cotizacion.getSolicitudDTO().setNombreOficinaAgente(cotizacionAnterior.getSolicitudDTO().getNombreOficinaAgente());
		
	}
	
	private void modificaVigencia() throws ExcepcionDeAccesoADatos, SystemException {
		cotizacion.setFechaFinVigencia(cotizacionAnterior.getFechaFinVigencia());
	}
	
	private void modificaMoneda() throws ExcepcionDeAccesoADatos, SystemException {
		cotizacion.setIdMoneda(cotizacionAnterior.getIdMoneda());
	}
	
	private void modificaMedioPago() throws ExcepcionDeAccesoADatos, SystemException {
		cotizacion.setIdMedioPago(cotizacionAnterior.getIdMedioPago());
	}
	
	private void modificaFormaPago() throws ExcepcionDeAccesoADatos, SystemException {
		cotizacion.setIdFormaPago(cotizacionAnterior.getIdFormaPago());
	}
	
	private void modificaTipoPoliza() throws ExcepcionDeAccesoADatos, SystemException {
		cotizacion.setTipoPolizaDTO(cotizacionAnterior.getTipoPolizaDTO());
	}
	
	private void modificaProducto() throws ExcepcionDeAccesoADatos, SystemException {
		cotizacion.getSolicitudDTO().setProductoDTO(cotizacionAnterior.getSolicitudDTO().getProductoDTO()); 
	}
	
	private void contrataDescontrataCobertura(boolean contrata) throws ExcepcionDeAccesoADatos, SystemException {
		
		CoberturaCotizacionDTO coberturaCotizacion = obtieneCoberturaCotizacion();
		if (coberturaCotizacion != null) {
			if (contrata) {
				coberturaCotizacion.setClaveContrato(Sistema.CONTRATADO);
//				CoberturaCotizacionDTO coberturaCotizacionAnterior = obtieneCoberturaCotizacionAnterior();
				
//				coberturaCotizacion.setValorPrimaNeta(coberturaCotizacionAnterior.getValorPrimaNeta());
				coberturaCotizacion.setValorPrimaNeta(this.movimiento.getValorDiferenciaPrimaNeta().abs().doubleValue());
				coberturaCotizacion.setValorSumaAsegurada(this.movimiento.getValorDiferenciaSumaAsegurada().abs().doubleValue());
				coberturaCotizacion.setValorCuota(this.movimiento.getValorDiferenciaCuota().abs().doubleValue());
				
				//Aplicar la SA a los riesgos para el calculo posterior en la igualacion
				
				RiesgoCotizacionId idRiesgoCot = new RiesgoCotizacionId(coberturaCotizacion.getId().getIdToCotizacion(),
						coberturaCotizacion.getId().getNumeroInciso(),
						coberturaCotizacion.getId().getIdToSeccion(),
						coberturaCotizacion.getId().getIdToCobertura(),
						null);
				List<RiesgoCotizacionDTO> listaRiesgos = RiesgoCotizacionDN.getInstancia().listarPorIdFiltrado(idRiesgoCot);
				
				for(RiesgoCotizacionDTO riesgoTMP : listaRiesgos){
					riesgoTMP.setValorSumaAsegurada(coberturaCotizacion.getValorSumaAsegurada());
					RiesgoCotizacionDN.getInstancia().modificar(riesgoTMP);
				}
				
				CoberturaCotizacionDN.getInstancia().modificar(coberturaCotizacion);
				
				CoberturaPrimaModificada coberturaPrimaMod = new CoberturaPrimaModificada();
				coberturaPrimaMod.setIdToCotizacion(coberturaCotizacion.getId().getIdToCotizacion());
				coberturaPrimaMod.setNumeroInciso(coberturaCotizacion.getId().getNumeroInciso());
				coberturaPrimaMod.setIdToSeccion(coberturaCotizacion.getId().getIdToSeccion());
				coberturaPrimaMod.setIdToCobertura(coberturaCotizacion.getId().getIdToCobertura());
				coberturaPrimaMod.setPrimaNeta(coberturaCotizacion.getValorPrimaNeta());
				
				objetoARecalcular = coberturaPrimaMod;
			} else {
				coberturaCotizacion.setClaveContrato(Sistema.NO_CONTRATADO);
				CoberturaCotizacionDN.getInstancia().modificar(coberturaCotizacion);
				objetoARecalcular = coberturaCotizacion;
			}
						
			
		}
	}
	
	private void contrataDescontrataSeccion(boolean contrata) throws ExcepcionDeAccesoADatos, SystemException {
		
		SeccionCotizacionDTO seccionCotizacion = obtieneSeccionCotizacion();
		if (seccionCotizacion != null) {
			if (contrata) {
				seccionCotizacion.setClaveContrato(Sistema.CONTRATADO);
			} else {
				seccionCotizacion.setClaveContrato(Sistema.NO_CONTRATADO);
			}
			
			SeccionCotizacionDN.getInstancia().modificar(seccionCotizacion);
		}
	}
	
	private CoberturaCotizacionDTO obtieneCoberturaCotizacion() throws ExcepcionDeAccesoADatos, SystemException {
		
		CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId();
		coberturaCotizacionId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		coberturaCotizacionId.setNumeroInciso(movimiento.getNumeroInciso());
		coberturaCotizacionId.setIdToSeccion(movimiento.getIdToSeccion());
		coberturaCotizacionId.setIdToCobertura(movimiento.getIdToCobertura());
				
		CoberturaCotizacionDTO coberturaCotizacion = new CoberturaCotizacionDTO();
		coberturaCotizacion.setId(coberturaCotizacionId);
		return CoberturaCotizacionDN.getInstancia().getPorId(coberturaCotizacion);
	}
	
	private CoberturaCotizacionDTO obtieneCoberturaCotizacionAnterior() throws ExcepcionDeAccesoADatos, SystemException {
		
		CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId();
		coberturaCotizacionId.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
		coberturaCotizacionId.setNumeroInciso(movimiento.getNumeroInciso());
		coberturaCotizacionId.setIdToSeccion(movimiento.getIdToSeccion());
		coberturaCotizacionId.setIdToCobertura(movimiento.getIdToCobertura());
				
		CoberturaCotizacionDTO coberturaCotizacion = new CoberturaCotizacionDTO();
		coberturaCotizacion.setId(coberturaCotizacionId);
		return CoberturaCotizacionDN.getInstancia().getPorId(coberturaCotizacion);
	}
	
	
	private SeccionCotizacionDTO obtieneSeccionCotizacion() throws ExcepcionDeAccesoADatos, SystemException {
		
		SeccionCotizacionDTOId seccionCotizacionId = new SeccionCotizacionDTOId();
		seccionCotizacionId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		seccionCotizacionId.setNumeroInciso(movimiento.getNumeroInciso());
		seccionCotizacionId.setIdToSeccion(movimiento.getIdToSeccion());
		
		return SeccionCotizacionDN.getInstancia().buscarPorId(seccionCotizacionId);
	}
	
	private SubIncisoCotizacionDTO obtieneSubIncisoCotizacion() throws ExcepcionDeAccesoADatos, SystemException {
		
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId();
		subIncisoCotizacionDTOId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		subIncisoCotizacionDTOId.setNumeroInciso(movimiento.getNumeroInciso());
		subIncisoCotizacionDTOId.setIdToSeccion(movimiento.getIdToSeccion());
		subIncisoCotizacionDTOId.setNumeroSubInciso(movimiento.getNumeroSubInciso());
		
		return SubIncisoCotizacionDN.getInstancia().getPorId(subIncisoCotizacionDTOId);
	}
	
	private SubIncisoCotizacionDTO obtieneSubIncisoCotizacionAnterior() throws ExcepcionDeAccesoADatos, SystemException {
		
		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId();
		subIncisoCotizacionDTOId.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
		subIncisoCotizacionDTOId.setNumeroInciso(movimiento.getNumeroInciso());
		subIncisoCotizacionDTOId.setIdToSeccion(movimiento.getIdToSeccion());
		subIncisoCotizacionDTOId.setNumeroSubInciso(movimiento.getNumeroSubInciso());
		
		return SubIncisoCotizacionDN.getInstancia().getPorId(subIncisoCotizacionDTOId);
	}
	
	private IncisoCotizacionDTO obtieneIncisoCotizacion() throws ExcepcionDeAccesoADatos, SystemException {
		
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		incisoCotizacionId.setNumeroInciso(movimiento.getNumeroInciso());
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		incisoCotizacion.setId(incisoCotizacionId);
		
		return IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacion);
	}
	
	private IncisoCotizacionDTO obtieneIncisoCotizacionAnterior() throws ExcepcionDeAccesoADatos, SystemException {
		
		IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
		incisoCotizacionId.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
		incisoCotizacionId.setNumeroInciso(movimiento.getNumeroInciso());
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		incisoCotizacion.setId(incisoCotizacionId);
		
		return IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacion);
	}
	
	private DocAnexoCotDTO obtieneDocumentoAnexoCotizacion() throws ExcepcionDeAccesoADatos, SystemException {
		DocAnexoCotId docAnexoCotId = new DocAnexoCotId();
		
		docAnexoCotId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		docAnexoCotId.setIdToControlArchivo(movimiento.getNumeroSubInciso()); //Ahi se guarda el idToControlArchivo en estos movimientos
		
		return DocAnexoCotDN.getInstancia().getPorId(docAnexoCotId);
		
	}
	
	private DocAnexoCotDTO obtieneDocumentoAnexoCotizacionAnterior() throws ExcepcionDeAccesoADatos, SystemException {
		DocAnexoCotId docAnexoCotId = new DocAnexoCotId();
		
		docAnexoCotId.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
		docAnexoCotId.setIdToControlArchivo(movimiento.getNumeroSubInciso()); //Ahi se guarda el idToControlArchivo en estos movimientos
		
		return DocAnexoCotDN.getInstancia().getPorId(docAnexoCotId);
	}
	
	private AgrupacionCotDTO obtienePrimerRiesgoLUCCotizacion() throws ExcepcionDeAccesoADatos, SystemException {
		
		AgrupacionCotId agrupacionCotId = new AgrupacionCotId();
		agrupacionCotId.setIdToCotizacion(cotizacion.getIdToCotizacion());
		
		AgrupacionCotDTO agrupacionCotizacion = new AgrupacionCotDTO();
		agrupacionCotizacion.setId(agrupacionCotId);
		agrupacionCotizacion.setIdToSeccion(movimiento.getIdToSeccion());
		
		//se establecen los atributos nulos para que funcione correctamente el "listarFiltrado"
		agrupacionCotizacion.setClaveTipoAgrupacion(null);
		agrupacionCotizacion.setValorCuota(null);
		agrupacionCotizacion.setValorSumaAsegurada(null);
		agrupacionCotizacion.setValorPrimaneta(null);
		
		List<AgrupacionCotDTO> agrupacionesCotizacion = PrimerRiesgoLUCDN.getInstancia().listarFiltrado(agrupacionCotizacion);	
		
		if (agrupacionesCotizacion != null && agrupacionesCotizacion.size() > 0) {
			return agrupacionesCotizacion.get(0);
		}
		
		return null;
	}
	
	private AgrupacionCotDTO obtienePrimerRiesgoLUCCotizacionAnterior() throws ExcepcionDeAccesoADatos, SystemException {

		AgrupacionCotId agrupacionCotId = new AgrupacionCotId();
		agrupacionCotId.setIdToCotizacion(cotizacionAnterior.getIdToCotizacion());
		
		AgrupacionCotDTO agrupacionCotizacion = new AgrupacionCotDTO();
		agrupacionCotizacion.setId(agrupacionCotId);
		agrupacionCotizacion.setIdToSeccion(movimiento.getIdToSeccion());
		
		//se establecen los atributos nulos para que funcione correctamente el "listarFiltrado"
		agrupacionCotizacion.setClaveTipoAgrupacion(null);
		agrupacionCotizacion.setValorCuota(null);
		agrupacionCotizacion.setValorSumaAsegurada(null);
		agrupacionCotizacion.setValorPrimaneta(null);
		
		List<AgrupacionCotDTO> agrupacionesCotizacion = PrimerRiesgoLUCDN.getInstancia().listarFiltrado(agrupacionCotizacion);	
		
		if (agrupacionesCotizacion != null && agrupacionesCotizacion.size() > 0) {
			return agrupacionesCotizacion.get(0);
		}
		
		return null;
	}
	
	public void copiaIncisoConRelaciones (CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino, BigDecimal numeroInciso) throws SystemException {
		new CancelacionEndosoSN().copiaIncisoConRelaciones(cotizacionOrigen, cotizacionDestino, numeroInciso);
	} 
	
	
}
