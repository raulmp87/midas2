package mx.com.afirme.midas.cotizacion.endoso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CotizacionEndosoSN {
	private CotizacionEndosoFacadeRemote beanRemoto;
	private String nombreUsuario;

	public CotizacionEndosoSN(String nombreUsuario) throws SystemException {
		try {
			this.nombreUsuario = nombreUsuario;
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CotizacionEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public CotizacionDTO agregar(CotizacionDTO cotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			cotizacionDTO.setNombreUsuarioLog(this.nombreUsuario);
			return beanRemoto.save(cotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(CotizacionDTO cotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			cotizacionDTO.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.delete(cotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(CotizacionDTO cotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			cotizacionDTO.setNombreUsuarioLog(this.nombreUsuario);
			beanRemoto.update(cotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public CotizacionDTO getPorId(BigDecimal idToCotizacion)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CotizacionDTO> listarCotizaciones()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarCotizaciones();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalFiltrado(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalFiltrado de las COT de endoso");
		}
		
	}
}
