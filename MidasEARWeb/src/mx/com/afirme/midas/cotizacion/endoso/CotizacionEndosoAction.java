package mx.com.afirme.midas.cotizacion.endoso;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.cliente.ClienteForm;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.CotizacionSN;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.endoso.DiferenciaCotizacionEndosoDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.SoporteEndosoCambioCuota;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoCoberturaDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CotizacionEndosoAction extends MidasMappingDispatchAction {
	private static final NumberFormat fMonto = new DecimalFormat("$#,##0.00");	
	private static final NumberFormat fCuota = new DecimalFormat("##0.0000");
	private static final NumberFormat fPorcentaje = new DecimalFormat("##0.00%");
	
	/**
	 * Method listarCotizaciones
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarCotizaciones(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		return listarCotizacionesFiltrado(mapping, form, request, response);
		
	}	
	/**
	 * Method listarCotizacionesFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarCotizacionesFiltrado(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			poblarDTOCotizacion(cotizacionForm, cotizacionDTO);
			
			Long totalRegistros = CotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
				.obtenerTotalFiltrado(cotizacionDTO,usuario);
			
			cotizacionForm.setTotalRegistros(totalRegistros.toString()); 
			
			return listarFiltradoPaginado(mapping, form, request, response);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
		
	}

	public ActionForward listarFiltradoPaginado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			Usuario usuario = getUsuarioMidas(request);
			if (!listaEnCache(form, request)) {
				CotizacionForm cotizacionForm = (CotizacionForm) form;
				CotizacionDTO cotizacionDTO = new CotizacionDTO();
				poblarDTOCotizacion(cotizacionForm, cotizacionDTO);
				
				cotizacionDTO.setPrimerRegistroACargar(cotizacionForm.getPrimerRegistroACargar());
				cotizacionDTO.setNumeroMaximoRegistrosACargar(cotizacionForm.getNumeroMaximoRegistrosACargar());
				cotizacionForm.setListaPaginada(CotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
						.listarCotizacionFiltrado(cotizacionDTO,usuario), request);
			
			} 
			
			request.setAttribute("listaCotizacion", obtieneListaAMostrar(form, request));
		
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
		
	}
	
	private void poblarDTOCotizacion(CotizacionForm cotizacionForm, CotizacionDTO cotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreSolicitante())) {
			cotizacionDTO.setNombreAsegurado(cotizacionForm.getNombreSolicitante());
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToCotizacion())) {
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
		}
		if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveEstatus())) {
			cotizacionDTO.setClaveEstatus(UtileriasWeb.regresaShort(cotizacionForm.getClaveEstatus()));
		}
	}
	
	/**
	 * Method mostrarCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */

	public ActionForward mostrarCotizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm)form;
		try {
			CotizacionSN cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = cotizacionSN.getPorId(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			if(cotizacionDTO.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_ASIGNADA) {
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
				.getUltimoEndoso(cotizacionDTO.getSolicitudDTO()
								.getIdToPolizaEndosada());
				CotizacionDTO cotizacionUltimoEndoso = cotizacionSN.getPorId(ultimoEndoso.getIdToCotizacion());				
				cotizacionDTO.setFechaFinVigencia(cotizacionUltimoEndoso.getFechaFinVigencia());
				cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
				cotizacionSN.modificar(cotizacionDTO);
			}
			request.setAttribute("idToCotizacion", idToCotizacion);
			cotizacionForm.setIdToCotizacion(idToCotizacion);
			//Bloque usado para definir si el usuario puede o no visualizar el tag de inisos
			if (cotizacionDTO.getTipoPolizaDTO() != null){
				cotizacionForm.setEditaIncisos((cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null)?"true":"false");
				cotizacionForm.setPermiteCambiarPoliza((cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null)?"false":"true");
			}
			else{
				cotizacionForm.setEditaIncisos("false");
				cotizacionForm.setPermiteCambiarPoliza("true");
			}
			//Bloque usado para definir si el usuario puede o no visualizar el tag de primer riesgo/LUC
			List<IncisoCotizacionDTO> listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(cotizacionDTO.getIdToCotizacion());
			if (listaIncisos != null)
				cotizacionForm.setEditaPrimerRiesgo((listaIncisos.size() > 0)?"true":"false");
			else
				cotizacionForm.setEditaIncisos("false");
			
			//Bloque usado para definir qu� tipo de edici�n realizar� el usuario en las comisiones de la cotizacion,
			//Puede editar el porcentaje de la comision y la autorizacion
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			for (Rol rol : usuario.getRoles()) {
				if (rol.getDescripcion().equals(Sistema.ROL_SUPERVISOR_SUSCRIPTOR) || rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_TECNICO)){
					cotizacionForm.setAutorizacionComision("autorizacion");
					request.setAttribute("autorizacionComision", "autorizacion");
					request.setAttribute("autorizarTexAdicional", "autorizarTexAdicional");
					break;
				}
			}	
			cotizacionForm.setEdicionComision("comision");
			request.setAttribute("edicionComision", "comision");
			cotizacionForm.setClaveTipoEndoso(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().toString());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarDatosGenerales
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDatosGenerales(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String tipoMensaje = request.getParameter("tipoMensaje");
		String mensaje = request.getParameter("mensaje");
		
		try {
			if(!UtileriasWeb.esObjetoNulo(tipoMensaje) && !UtileriasWeb.esObjetoNulo(mensaje)){
				cotizacionForm.setTipoMensaje(tipoMensaje);
				cotizacionForm.setMensaje(mensaje);
			}			
			cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm,request);
			
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
			documentoDigitalSolicitudDTOList = DocumentoDigitalSolicitudDN.getInstancia().listarDocumentosDigitalesPorSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			request.setAttribute("documentosList", documentoDigitalSolicitudDTOList);

		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}	
	/**
	 * Method guardarEndoso
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("deprecation")
	public ActionForward guardarEndoso(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;		
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String icono = Sistema.ERROR;
		String mensaje = "";
		
		try {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			if (cotizacionForm.getClaveMotivo() != null && !cotizacionForm.getClaveMotivo().equals(""))
				cotizacionDTO.setClaveMotivoEndoso(Short.valueOf(cotizacionForm.getClaveMotivo()));
			
			cotizacionDTO.setCodigoUsuarioModificacion(UtileriasWeb.obtieneNombreUsuario(request));
			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaFinVigencia())){
					String datos[] = cotizacionForm.getFechaFinVigencia().split("/");
					Date fecha = new Date();
					fecha.setDate(Integer.valueOf(datos[0]));
					fecha.setMonth(Integer.valueOf(datos[1])-1);
					fecha.setYear(Integer.valueOf(datos[2])-1900);
					cotizacionDTO.setFechaFinVigencia(fecha);
				}
				cotizacionDN.modificar(cotizacionDTO);
			}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				if(cotizacionDTO.getIdFormaPago() != null ){
					cotizacionDTO.setIdFormaPago(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdFormaPago()));
					cotizacionDN.modificar(cotizacionDTO);
					MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
					movimientoCotizacionEndosoDN.calcularDiferenciasDeEndoso(cotizacionDTO.getIdToCotizacion());					
					MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
					dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
					dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN.listarFiltrado(dto);
					cotizacionForm.setMovimientosGenerales(movimientosGenerales);

					ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,new ArrayList<SoporteResumen>());
					cotizacionForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO.getPrimaNetaAnual()));
					cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
					cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
					cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
					cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
					cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
					cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));
					cotizacionForm.setDiasPorDevengar(cotizacionDTO.getDiasPorDevengar().toString());
					
				}
			}else{
				cotizacionDN.modificar(cotizacionDTO);
			}
			
			this.poblarForm(cotizacionDTO, cotizacionForm, request);
			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION){
				MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				movimientoCotizacionEndosoDN.calcularDiferenciasDeEndoso(cotizacionDTO.getIdToCotizacion());
				List<MovimientoCotizacionEndosoDTO> movimientosInciso = movimientoCotizacionEndosoDN.obtenerMovimientosPorCotizacion(cotizacionDTO.getIdToCotizacion());
				cotizacionForm.setMovimientosInciso(movimientosInciso);
				MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
				dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
				dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN.listarFiltrado(dto);
				cotizacionForm.setMovimientosGenerales(movimientosGenerales);
				ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,new ArrayList<SoporteResumen>());
				cotizacionForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO.getPrimaNetaAnual()));
				cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
				cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
				cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
				cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
				cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
				cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));
				cotizacionForm.setDiasPorDevengar(cotizacionDTO.getDiasPorDevengar().toString());
				
				String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
				List<DiferenciaCotizacionEndosoDTO> diferencias = this.obtenerDiferenciasCotizacionEndosoDTOs(cotizacionDTO.getIdToCotizacion(), nombreUsuario);
				cotizacionForm.setDiferenciaCotizacionEndosoDTOs(diferencias);
			}
			icono = Sistema.EXITO;
			mensaje = "La cotizaci&oacute;n se actualizo correctamente.</br>";	
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje);			
		} catch (ExcepcionDeAccesoADatos e) {
			icono = Sistema.ERROR;
			mensaje = "Ocurri&oacute; un error al actualizar la cotizaci&oacute;n.";			
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje);
		} catch (SystemException e) {
			icono = Sistema.ERROR;
			mensaje = "Ocurri&oacute; un error al actualizar la cotizaci&oacute;n.</br> El sistema no se encuentra disponible, intente mas tarde.";			
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Method mostrarDatosEndoso
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDatosEndoso(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;		
		String idToCotizacion = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		try {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm, request);
			MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			
			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION){
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
				SoporteEstructuraCotizacionEndoso estructuraEndoso = new SoporteEstructuraCotizacionEndoso(
						cotizacionDTO,ultimoEndoso.getIdToCotizacion(),ultimoEndoso,nombreUsuario);
				estructuraEndoso.consultarInformacionValidacionCotizacion(nombreUsuario);
				estructuraEndoso.calcularDiferenciasDeEndoso(nombreUsuario);
				List<MovimientoCotizacionEndosoDTO> movimientosInciso = estructuraEndoso.obtenerListaMovimientosEndoso(null);
				
				MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
				dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
				dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				//La consulta de movimientos generales se hace aparte por que en la consulta por cotizacion se filtran los movtos que no son de aplicacion general.
				List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN.listarFiltrado(dto);
				
				cotizacionForm.setMovimientosInciso(movimientosInciso);
				cotizacionForm.setMovimientosGenerales(movimientosGenerales);
				
				estructuraEndoso = null;
				Runtime.getRuntime().gc();
				
				ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,new ArrayList<SoporteResumen>());
				cotizacionForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO.getPrimaNetaAnual()));
				cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
				cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
				cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
				cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
				cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
				cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));
				cotizacionForm.setDiasPorDevengar(cotizacionDTO.getDiasPorDevengar().toString());
				
				List<DiferenciaCotizacionEndosoDTO> diferencias = this.obtenerDiferenciasCotizacionEndosoDTOs(cotizacionDTO.getIdToCotizacion(), nombreUsuario);
				cotizacionForm.setDiferenciaCotizacionEndosoDTOs(diferencias);
				
				this.llenarCamposModificacionDerechosRPF(cotizacionDTO, cotizacionForm);
				
			}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
				SoporteEstructuraCotizacionEndoso estructuraEndoso = new SoporteEstructuraCotizacionEndoso(
						cotizacionDTO,ultimoEndoso.getIdToCotizacion(),ultimoEndoso,nombreUsuario);
				estructuraEndoso.consultarInformacionValidacionCotizacion(nombreUsuario);
				estructuraEndoso.calcularDiferenciasDeEndosoDeCancelacion(nombreUsuario);
//		    	movimientoCotizacionEndosoDN.calcularDiferenciasDeEndosoDeCancelacion(cotizacionDTO.getIdToCotizacion());
				estructuraEndoso = null;
				Runtime.getRuntime().gc();
				
				ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacionEndoso(cotizacionDTO);
				cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
				cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
				cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
				cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
				cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
				cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));	
			}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_DECLARACION){
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
				SoporteEstructuraCotizacionEndoso estructuraEndoso = new SoporteEstructuraCotizacionEndoso(
						cotizacionDTO,ultimoEndoso.getIdToCotizacion(),ultimoEndoso,nombreUsuario);
				estructuraEndoso.consultarInformacionValidacionCotizacion(nombreUsuario);
				estructuraEndoso.calcularDiferenciasDeEndosoDeDeclaracion(nombreUsuario);
				
//				movimientoCotizacionEndosoDN.calcularDiferenciasDeEndosoDeDeclaracion(cotizacionDTO.getIdToCotizacion());				
			}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
				SoporteEstructuraCotizacionEndoso estructuraEndoso = new SoporteEstructuraCotizacionEndoso(
						cotizacionDTO,ultimoEndoso.getIdToCotizacion(),ultimoEndoso,nombreUsuario);
				
				estructuraEndoso.calcularDiferenciasDeEndosoDeCambioFormaPago(nombreUsuario);
//				movimientoCotizacionEndosoDN.calcularDiferenciasDeEndosoDeCambioFormaPago(cotizacionDTO.getIdToCotizacion());
				MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
				dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
				dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN.listarFiltrado(dto);
				cotizacionForm.setMovimientosGenerales(movimientosGenerales);

				ResumenCotizacionDN.getInstancia().setTotalesResumenCambioFormaPago(cotizacionDTO);
				//cotizacionForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO.getPrimaNetaAnual()));
				cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
				cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
				cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
				cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
				cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
				cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));
				//cotizacionForm.setDiasPorDevengar(cotizacionDTO.getDiasPorDevengar().toString());				
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaInicioVigencia(sdf.format(cotizacionDTO.getFechaInicioVigencia()));			
			cotizacionForm.setFechaFinVigencia(sdf.format(cotizacionDTO.getFechaFinVigencia()));
			
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method consultarDatosEndoso
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward consultarDatosEndoso(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;		
		String idToCotizacion = request.getParameter("id");
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		//String numeroEndoso
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		try {
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm, request);
			ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,new ArrayList<SoporteResumen>());
			
			if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
				EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).buscarPorCotizacion(cotizacionDTO.getIdToCotizacion());
				List<EndosoCoberturaDTO> coberturasAgrupadas = EndosoIDN.getInstancia().obtieneCoberturasAgrupadas(endosoDTO.getId(), nombreUsuario);
				cotizacionForm.setCoberturasAgrupadas(coberturasAgrupadas);
				cotizacionForm.setPrimaNetaCotizacion(fMonto.format(endosoDTO.getValorPrimaNeta().doubleValue()));
				cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(endosoDTO.getValorRecargoPagoFrac().doubleValue()));
				cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
				cotizacionForm.setMontoIVA(fMonto.format(endosoDTO.getValorIVA().doubleValue()));
				cotizacionForm.setDerechosPoliza(fMonto.format(endosoDTO.getValorDerechos().doubleValue()));
				cotizacionForm.setPrimaNetaTotal(fMonto.format(endosoDTO.getValorPrimaTotal().doubleValue()));
				cotizacionForm.setDiasPorDevengar(cotizacionDTO.getDiasPorDevengar().toString());	

			}else{
				MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				List<MovimientoCotizacionEndosoDTO> movimientosInciso = movimientoCotizacionEndosoDN.obtenerMovimientosPorCotizacion(cotizacionDTO.getIdToCotizacion());
				cotizacionForm.setMovimientosInciso(movimientosInciso);
				MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
				dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
				dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				
				List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN.listarFiltrado(dto);
				cotizacionForm.setMovimientosGenerales(movimientosGenerales);
				cotizacionForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO.getPrimaNetaAnual()));
				cotizacionForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion().doubleValue()));
				cotizacionForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado().doubleValue()));
				cotizacionForm.setFactorIVA(cotizacionDTO.getFactorIVA().toString()+ " %");
				cotizacionForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA().doubleValue()));
				cotizacionForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza().doubleValue()));
				cotizacionForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal().doubleValue()));
				cotizacionForm.setDiasPorDevengar(cotizacionDTO.getDiasPorDevengar().toString());
				
				List<DiferenciaCotizacionEndosoDTO> diferencias = this.obtenerDiferenciasCotizacionEndosoDTOs(cotizacionDTO.getIdToCotizacion(), nombreUsuario);
				cotizacionForm.setDiferenciaCotizacionEndosoDTOs(diferencias);				

			}
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private List<DiferenciaCotizacionEndosoDTO> obtenerDiferenciasCotizacionEndosoDTOs(
			BigDecimal idToCotizacion, String nombreUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		EndosoDN endosoDN = EndosoDN.getInstancia(nombreUsuario);
		List<DiferenciaCotizacionEndosoDTO> diferenciaCotizacionEndosoDTOs = endosoDN.obtenerDiferenciasCotizacionEndoso(idToCotizacion, nombreUsuario);
		double totalCotizacionOriginal = 0D;
		double totalCotizacionEndoso = 0D;
		double totalCotizacionMovimiento = 0D;
		double totalSeccionOriginal = 0D;
		double totalSeccionEndoso = 0D;
		double totalSeccionMovimiento = 0D;
		List<DiferenciaCotizacionEndosoDTO> diferencias = new ArrayList<DiferenciaCotizacionEndosoDTO>();
		List<DiferenciaCotizacionEndosoDTO> diferenciasTMP = new ArrayList<DiferenciaCotizacionEndosoDTO>();
		BigDecimal idToSeccion = BigDecimal.ZERO;
		for(DiferenciaCotizacionEndosoDTO diferencia : diferenciaCotizacionEndosoDTOs) {
			if(idToSeccion.intValue() == 0 || idToSeccion.intValue() != diferencia.getIdToSeccion().intValue()) {
				if(idToSeccion.intValue() != 0) {
					DiferenciaCotizacionEndosoDTO diferenciaCotizacionEndosoDTO = new DiferenciaCotizacionEndosoDTO();
					diferenciaCotizacionEndosoDTO.setRowStyle("font-weight: bold;");
					diferenciaCotizacionEndosoDTO.setPadding("5px");
					SeccionDN seccionDN = SeccionDN.getInstancia();
					SeccionDTO seccionDTO = seccionDN.getPorId(idToSeccion);
					diferenciaCotizacionEndosoDTO.setTituloPrimeraColumna(seccionDTO.getNombreComercial());
					diferenciaCotizacionEndosoDTO.setOriginalPrimaNeta(totalSeccionOriginal == 0? null : totalSeccionOriginal);
					diferenciaCotizacionEndosoDTO.setDiferenciaPrimaNeta(totalSeccionEndoso == 0? null : totalSeccionEndoso);
					diferenciaCotizacionEndosoDTO.setNuevaPrimaNeta(totalSeccionMovimiento == 0? null : totalSeccionMovimiento);
					diferenciasTMP.add(0, diferenciaCotizacionEndosoDTO);
				}
				diferencias.addAll(diferenciasTMP);
				diferenciasTMP = new ArrayList<DiferenciaCotizacionEndosoDTO>();
				idToSeccion = diferencia.getIdToSeccion();
				totalSeccionOriginal = 0D;
				totalSeccionEndoso = 0D;
				totalSeccionMovimiento = 0D;
			}
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(diferencia.getIdToCobertura());
			diferencia.setTituloPrimeraColumna(coberturaDTO.getNombreComercial());
			diferencia.setPadding("10px");
			totalCotizacionOriginal += diferencia.getOriginalPrimaNeta();
			totalCotizacionEndoso += diferencia.getDiferenciaPrimaNeta();
			totalCotizacionMovimiento += diferencia.getNuevaPrimaNeta();
			totalSeccionOriginal += diferencia.getOriginalPrimaNeta();
			totalSeccionEndoso += diferencia.getDiferenciaPrimaNeta();
			totalSeccionMovimiento += diferencia.getNuevaPrimaNeta();
			if(diferencia.getDiferenciaSumaAsegurada() == 0) {
				diferencia.setDiferenciaSumaAsegurada(null);
			}
			if(diferencia.getDiferenciaPrimaNeta() == 0) {
				diferencia.setDiferenciaPrimaNeta(null);
			}
			DecimalFormat df = new DecimalFormat("#.####");
			String cuotaOriginal = df.format(diferencia.getOriginalCuota() * 1000);
			String nuevaCuota = df.format(diferencia.getNuevaCuota() * 1000);
			if(!cuotaOriginal.equals(nuevaCuota)) {
				diferencia.setTieneCuotasDiferentes("true");
			}
			diferenciasTMP.add(diferencia);
		}
		DiferenciaCotizacionEndosoDTO diferenciaCotizacionEndosoDTO = new DiferenciaCotizacionEndosoDTO();
		diferenciaCotizacionEndosoDTO.setRowStyle("font-weight: bold;");
		diferenciaCotizacionEndosoDTO.setPadding("5px");
		SeccionDN seccionDN = SeccionDN.getInstancia();
		SeccionDTO seccionDTO = seccionDN.getPorId(idToSeccion);
		diferenciaCotizacionEndosoDTO.setTituloPrimeraColumna(seccionDTO.getNombreComercial());
		diferenciaCotizacionEndosoDTO.setOriginalPrimaNeta(totalSeccionOriginal == 0? null : totalSeccionOriginal);
		diferenciaCotizacionEndosoDTO.setDiferenciaPrimaNeta(totalSeccionEndoso == 0? null : totalSeccionEndoso);
		diferenciaCotizacionEndosoDTO.setNuevaPrimaNeta(totalSeccionMovimiento == 0? null : totalSeccionMovimiento);
		diferenciasTMP.add(0, diferenciaCotizacionEndosoDTO);
		diferencias.addAll(diferenciasTMP);
		
		diferenciaCotizacionEndosoDTO = new DiferenciaCotizacionEndosoDTO();
		diferenciaCotizacionEndosoDTO.setRowStyle("font-weight: bold;");
		diferenciaCotizacionEndosoDTO.setPadding("0px");
		diferenciaCotizacionEndosoDTO.setTituloPrimeraColumna("TOTAL COTIZACI�N");
		diferenciaCotizacionEndosoDTO.setOriginalPrimaNeta(totalCotizacionOriginal);
		diferenciaCotizacionEndosoDTO.setDiferenciaPrimaNeta(totalCotizacionEndoso);
		diferenciaCotizacionEndosoDTO.setNuevaPrimaNeta(totalCotizacionMovimiento);
		diferencias.add(0, diferenciaCotizacionEndosoDTO);
		
		return diferencias;
	}

	/**
	 * Method liberar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */	
	public ActionForward liberar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm)form;
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String icono = Sistema.ERROR;
		StringBuilder mensaje = new StringBuilder();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		CotizacionDTO cotizacionDTO;
		try {
			cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
			//Se calculan los Movimientos
		    if (cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().equals(Sistema.SOLICITUD_TIPO_ENDOSO)) {
				MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));		    	
			    if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
			    	movimientoCotizacionEndosoDN.calcularDiferenciasDeEndosoDeCancelacion(cotizacionDTO.getIdToCotizacion());
			    }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
			    	movimientoCotizacionEndosoDN.calcularDiferenciasDeEndosoDeCambioFormaPago(cotizacionDTO.getIdToCotizacion());
					MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
					dto.setClaveTipoMovimiento(Sistema.TIPO_MOV_MODIFICACION_APP_GRAL);
					dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					List<MovimientoCotizacionEndosoDTO> movimientosGenerales = movimientoCotizacionEndosoDN.listarFiltrado(dto);
					cotizacionForm.setMovimientosGenerales(movimientosGenerales);			    	
			    }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_DECLARACION){	
			    	movimientoCotizacionEndosoDN.calcularDiferenciasDeEndosoDeDeclaracion(cotizacionDTO.getIdToCotizacion());
			    }
		    }
		}catch(Exception e){
			icono = Sistema.ERROR;
			mensaje= new StringBuilder();
			mensaje.append("Ocurri&oacute; un error al aplicar los movimientos del endoso.");
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());	
			return mapping.findForward(reglaNavegacion);
		}		
		try{
			//Se notifica a reaseguro cotizaci�n
		    if (cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud().equals(Sistema.SOLICITUD_TIPO_ENDOSO)) {
			    if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CANCELACION
				    ||cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION ){
			    	/**
			    	 * 25/05/2010. Jose Luis Arellano. EN LOS CASOS DE ENDOSOS DE REHABILITACI�N Y CANCELACI�N, NO SE DEBE VALIDAR, YA QUE EL SOPORTE
			    	 * SE GENERAR� HASTA QUE SE EMITA EL ENDOSO.
			    	 */
//				CotizacionDN.getInstancia(null).validarReaseguroFacultativoEndosoCancelacionRehab(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()), usuario.getNombreUsuario(),false);
			    }else{
			        CotizacionDN.getInstancia(null).validarReaseguroFacultativoEndoso(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()), UtileriasWeb.obtieneNombreUsuario(request),cotizacionForm,null);
			    }
			}
			
		}catch(Exception e){
			icono = Sistema.ERROR;
			mensaje= new StringBuilder();
			mensaje.append("Ocurri&oacute; un error al validar el endoso con reaseguro: " +e.getMessage());
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());	
			return mapping.findForward(reglaNavegacion);
		}	
		try{
			//se modifican los valores de los subincisos
			
			SubIncisoCotizacionDTO subInciso = new SubIncisoCotizacionDTO();
			subInciso.setId(new SubIncisoCotizacionDTOId());
			subInciso.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			subInciso.setClaveEstatusDeclaracion((short)0);
			
			SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();			
			List<SubIncisoCotizacionDTO> subIncisos = subIncisoCotizacionDN.listarFiltrado(subInciso);	
			
			for(SubIncisoCotizacionDTO subIncDTO: subIncisos){
				subIncDTO.setClaveEstatusDeclaracion((short)1);
				subIncisoCotizacionDN.modificar(subIncDTO, UtileriasWeb.obtieneNombreUsuario(request));
			}
			//se actualiza el estatus de la cotizacion
			cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_LIBERADA);
			CotizacionDN.getInstancia(usuario.getNombreUsuario()).modificar(cotizacionDTO);

			icono = Sistema.EXITO;
			mensaje.append("La cotizaci&oacute;n fue liberada exitosamente.</br>");			
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());				
		}catch(Exception e){
			icono = Sistema.ERROR;
			mensaje= new StringBuilder();
			mensaje.append("Ocurri&oacute; un error al liberar la cotizaci&oacute;n.");
			cotizacionForm.setTipoMensaje(icono);
			cotizacionForm.setMensaje(mensaje.toString());	
			return mapping.findForward(reglaNavegacion);
		}	

		return mapping.findForward(reglaNavegacion);
	}	
	protected void poblarForm(CotizacionDTO cotizacionDTO, CotizacionForm cotizacionForm,HttpServletRequest request) throws SystemException{
		if (cotizacionDTO.getIdToCotizacion() != null){
			cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion().toString());
			cotizacionForm.setIdToCotizacionFormateada("COT-" + UtileriasWeb.llenarIzquierda(cotizacionForm.getIdToCotizacion(), "0", 8));
		}
		cotizacionForm.setDireccionCobro(new DireccionForm());
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		cotizacionForm.setClienteAsegurado(new ClienteForm());
		if (cotizacionDTO.getIdToPersonaAsegurado() != null){
			ClienteDTO asegurado = null;
			try{
			
				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
					asegurado = new ClienteDTO();
					asegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
					asegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, UtileriasWeb.obtieneNombreUsuario(request));
				}else{
					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
				}
			
			}catch(Exception exc){asegurado = new ClienteDTO();}
			if (asegurado != null)
				ClienteAction.poblarClienteForm(asegurado, cotizacionForm.getClienteAsegurado());
		}
		cotizacionForm.setClienteContratante(new ClienteForm());
		if (cotizacionDTO.getIdToPersonaContratante() != null){
			ClienteDTO contratante = null;
			try{
			if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
				contratante = new ClienteDTO();
				contratante.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
				contratante.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
				contratante = ClienteDN.getInstancia().verDetalleCliente(contratante, UtileriasWeb.obtieneNombreUsuario(request));
			}else{
				contratante = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), UtileriasWeb.obtieneNombreUsuario(request));					
			}
			
			}catch(Exception exc){contratante = new ClienteDTO();}
			if (contratante != null)
				ClienteAction.poblarClienteForm(contratante, cotizacionForm.getClienteContratante());
		}
		Format formatter=null;
		if ( cotizacionDTO.getFechaInicioVigencia() != null){
			formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaInicioVigencia(formatter.format(cotizacionDTO.getFechaInicioVigencia()));
		}
		if ( cotizacionDTO.getFechaFinVigencia() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			cotizacionForm.setFechaFinVigencia(formatter.format(cotizacionDTO.getFechaFinVigencia()));
		}
		if ( cotizacionDTO.getFechaCreacion() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaCreacion(formatter.format(cotizacionDTO.getFechaCreacion()));
		}
		if ( cotizacionDTO.getFechaModificacion() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy");
			cotizacionForm.setFechaModificacion(formatter.format(cotizacionDTO.getFechaModificacion()));
		}
		if(cotizacionDTO.getTipoPolizaDTO() != null) {
			if (cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null){
				cotizacionForm.setIdToTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza().toBigInteger().toString());
				cotizacionForm.setListaMoneda(new MonedaSN().listarMonedasAsociadasTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza()));
				TipoPolizaDTO tipoPolizaDTO = TipoPolizaDN.getInstancia().getPorId(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
				cotizacionForm.setTipoDePoliza(tipoPolizaDTO.getDescripcion());
			}
		} else {
			cotizacionForm.setListaMoneda(new ArrayList<MonedaDTO>());
		}
		if (cotizacionDTO.getIdMoneda() != null){
			cotizacionForm.setIdMoneda(cotizacionDTO.getIdMoneda().toBigInteger().toString());
			Short idMoneda = new Short(cotizacionDTO.getIdMoneda().toBigInteger().toString());
			MonedaDTO monedaDTO = MonedaDN.getInstancia().getPorId(idMoneda);
			cotizacionForm.setMoneda(monedaDTO.getDescripcion());
			List<FormaPagoIDTO> listaFormaPago = new ArrayList<FormaPagoIDTO>();
			try{
			//22/12/2009 JLAB, se debe obtener la lista de formas de pago v�lidas para el tipo de moneda de la cotizaci�n.
				listaFormaPago = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).listarTodos(idMoneda);
			}catch(Exception e){}
			cotizacionForm.setListaFormaPago(listaFormaPago);
		}
		if ( cotizacionDTO.getIdFormaPago() != null){
			cotizacionForm.setIdFormaPago(cotizacionDTO.getIdFormaPago().toBigInteger().toString());
			FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
			formaPagoDTO.setIdFormaPago(new Integer(cotizacionDTO.getIdFormaPago().intValue()));
			formaPagoDTO = FormaPagoDN.getInstancia().getPorId(formaPagoDTO);
			cotizacionForm.setFormaPago(formaPagoDTO.getDescripcion());
		}
		if ( cotizacionDTO.getIdMedioPago() != null){
			cotizacionForm.setIdMedioPago(cotizacionDTO.getIdMedioPago().toBigInteger().toString());
			MedioPagoDTO medioPagoDTO = new MedioPagoDTO();
			medioPagoDTO.setIdMedioPago(new Integer(cotizacionDTO.getIdMedioPago().intValue()));
			medioPagoDTO = MedioPagoDN.getInstancia().getPorId(medioPagoDTO);
			try{
				cotizacionForm.setMedioDePago(medioPagoDTO.getDescripcion());
			}catch(Exception exc){cotizacionForm.setMedioDePago("NO DISPONIBLE");}
		}
		if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getNombreEmpresaContratante()))
			cotizacionForm.setNombreEmpresaContratante(cotizacionDTO.getNombreEmpresaContratante());
		if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getNombreEmpresaAsegurado()))
			cotizacionForm.setNombreEmpresaAsegurado(cotizacionDTO.getNombreEmpresaAsegurado());
		if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getCodigoUsuarioOrdenTrabajo()))
			cotizacionForm.setCodigoUsuarioOrdenTrabajo(cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
		//Agente de seguros
		if ( cotizacionDTO.getSolicitudDTO() != null){
			if (cotizacionDTO.getSolicitudDTO().getIdToSolicitud() != null)
				cotizacionForm.setIdToSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud().toString());
			if (cotizacionDTO.getSolicitudDTO().getTelefonoContacto() != null)
				cotizacionForm.setTelefonoContacto(cotizacionDTO.getSolicitudDTO().getTelefonoContacto());
			ProductoDTO productoTMP = new ProductoSN().encontrarPorSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
			cotizacionForm.setProducto(productoTMP.getNombreComercial());
			cotizacionForm.setIdToProducto(productoTMP.getIdToProducto().toString());
			cotizacionForm.setListaTipoPoliza(new TipoPolizaSN().listarVigentesPorIdProducto(productoTMP.getIdToProducto()));
			String nombreSolicitante = "NO DISPONIBLE";
			String apellidoMaterno = cotizacionDTO.getSolicitudDTO().getApellidoMaterno() != null? cotizacionDTO.getSolicitudDTO().getApellidoMaterno() : "";
			if (cotizacionDTO.getSolicitudDTO().getClaveTipoPersona().intValue() == 1)
				nombreSolicitante = cotizacionDTO.getSolicitudDTO().getNombrePersona()+" " +cotizacionDTO.getSolicitudDTO().getApellidoPaterno() +" "+apellidoMaterno;
			else
				nombreSolicitante = cotizacionDTO.getSolicitudDTO().getNombrePersona();
			cotizacionForm.setNombreSolicitante(nombreSolicitante);

			if (cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null){
				try{
					AgenteDN agenteDN = AgenteDN.getInstancia();
					AgenteDTO agenteDTO = new AgenteDTO();
					agenteDTO.setIdTcAgente(Integer.valueOf(cotizacionDTO.getSolicitudDTO().getCodigoAgente().toBigInteger().toString()));
					agenteDTO = agenteDN.verDetalleAgente(agenteDTO, nombreUsuario);
					cotizacionForm.setNombreAgente(agenteDTO.getNombre());
					cotizacionForm.setOficina(agenteDTO.getNumeroOficina());
				}
				catch(Exception exc){
					cotizacionForm.setNombreAgente("NO DISPONIBLE");
					cotizacionForm.setOficina("NO DISPONIBLE");
					}
			}
			if(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada() != null){
				PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
				cotizacionForm.setNumeroPolizaFormateada(UtileriasWeb.getNumeroPoliza(polizaDTO));
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).getUltimoEndoso(polizaDTO.getIdToPoliza());
				CotizacionDTO cotizacionAnterior = CotizacionDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).getPorId(ultimoEndoso.getIdToCotizacion());
				cotizacionForm.setIdFormaPagoAnterior(cotizacionAnterior.getIdFormaPago().toString());
			}
		}
		if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getCodigoUsuarioCreacion()))
			cotizacionForm.setCodigoUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
		if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getCodigoUsuarioModificacion()))
			cotizacionForm.setCodigoUsuarioModificacion(cotizacionDTO.getCodigoUsuarioModificacion());
		
		if ( cotizacionDTO.getClaveEstatus() != null)
			cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
		
//		cotizacionForm.setListaFormaPago(FormaPagoDN.getInstancia().listarTodos());
		cotizacionForm.setListaMedioPago(MedioPagoDN.getInstancia().listarTodos());
		
		if(cotizacionDTO.getClaveAutRetroacDifer()!= null)
			cotizacionForm.setClaveAutRetroacDifer(cotizacionDTO.getClaveAutRetroacDifer().toString());
		
		if(cotizacionDTO.getClaveAutVigenciaMaxMin() != null)
			cotizacionForm.setClaveAutVigenciaMaxMin(cotizacionDTO.getClaveAutVigenciaMaxMin().toString());
		
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso() != null)
			cotizacionForm.setClaveTipoEndoso(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().toString());

		if ( cotizacionDTO.getFechaInicioVigencia() != null){
			if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			cotizacionForm.setFechaInicioVigenciaEndoso(formatter.format(cotizacionDTO.getFechaInicioVigencia()));
		}
		if (cotizacionDTO.getClaveMotivoEndoso() != null)
			cotizacionForm.setClaveMotivo(cotizacionDTO.getClaveMotivoEndoso().toString());	

	}
	
	public ActionForward guardarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		ActionForward forward;
		BigDecimal idToTipoPolizaAnterior = null;
		BigDecimal idToTipoPolizaNueva = null;
		String parametrosMensaje = null; 
		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			if (cotizacionDTO.getTipoPolizaDTO() != null){
				idToTipoPolizaAnterior = cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza();
			}
			Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
			cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			cotizacionDTO.setFechaModificacion(new Date());
			
			poblarDTO(cotizacionForm, cotizacionDTO);

			//OrdenTrabajoDN.getInstancia().actualizarCotizacion(cotizacionDTO);
			if (cotizacionDTO.getTipoPolizaDTO() != null){
				idToTipoPolizaNueva = cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza();
			}
			//Generar las nuevas comisiones y borrar los incisos correspondientes al tipopoliza anterior.
			CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
				.cambiarTipoPoliza(cotizacionDTO, idToTipoPolizaAnterior, idToTipoPolizaNueva);
			CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
			SolicitudDN.getInstancia().actualizar(cotizacionDTO.getSolicitudDTO());
			parametrosMensaje = "tipoMensaje="+Sistema.EXITO+ "&mensaje=Se registro correctamente la Informaci\u00f3n.";
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		if (!UtileriasWeb.esObjetoNulo(cotizacionForm.getEsCotizacion()) && cotizacionForm.getEsCotizacion().equals("1")) {
//			forward = new ActionForward(mapping.findForward(reglaNavegacion));
//			String path;
//			path = forward.getPath();
//			forward.setPath(path + "?esCotizacion=1&id=" + cotizacionDTO.getIdToCotizacion().toString());
			forward = new ActionForward();
			String path;
			path = "/cotizacion/mostrarDatosGenerales.do?id="+cotizacionForm.getIdToCotizacion()+"&"+parametrosMensaje;
			forward.setPath(path);
			return forward;
		}
		
		return mapping.findForward(reglaNavegacion);
	}	
	
	@SuppressWarnings({ "deprecation" })
	protected void poblarDTO(CotizacionForm cotizacionForm, CotizacionDTO cotizacionDTO) throws SystemException{
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToCotizacion()))
				cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			//direcciones
			DireccionDN direccionDN = DireccionDN.getInstancia();

			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToDireccionCobro()))
				cotizacionDTO.setDireccionCobroDTO(direccionDN.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToDireccionCobro())));
			direccionDN = null;
			//personas
			ClienteDTO clienteDTO = null;
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToPersonaAsegurado())){

				if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
					clienteDTO = new ClienteDTO();
					clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
					clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
				}else{
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), cotizacionDTO.getCodigoUsuarioCreacion());					
				}
				
				if(clienteDTO != null) {
					String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
                    nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
                    nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
                    cotizacionDTO.setNombreAsegurado(nombreCliente);
				}
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToPersonaContratante())){

				if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
					clienteDTO = new ClienteDTO();
					clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
					clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
				}else{
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), cotizacionDTO.getCodigoUsuarioCreacion());					
				}				
				if(clienteDTO != null) {
					String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
                    nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
                    nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
                    cotizacionDTO.setNombreContratante(nombreCliente);
				}
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToTipoPoliza())){
				TipoPolizaDTO poliza = new TipoPolizaDTO();
				poliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToTipoPoliza()));
				cotizacionDTO.setTipoPolizaDTO(TipoPolizaDN.getInstancia().getPorId(poliza));
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToSolicitud()))
				cotizacionDTO.setSolicitudDTO(SolicitudDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToSolicitud())));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMoneda()))
				cotizacionDTO.setIdMoneda(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdMoneda()));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdFormaPago()))
				cotizacionDTO.setIdFormaPago(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdFormaPago()));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMedioPago()))
				cotizacionDTO.setIdMedioPago(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdMedioPago()));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaInicioVigencia())){
				String datos[] = cotizacionForm.getFechaInicioVigencia().split("/");
				Date fecha = new Date();
				fecha.setDate(Integer.valueOf(datos[0]));
				fecha.setMonth(Integer.valueOf(datos[1])-1);
				fecha.setYear(Integer.valueOf(datos[2])-1900);
				cotizacionDTO.setFechaInicioVigencia(fecha);
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaFinVigencia())){
				String datos[] = cotizacionForm.getFechaFinVigencia().split("/");
				Date fecha = new Date();
				fecha.setDate(Integer.valueOf(datos[0]));
				fecha.setMonth(Integer.valueOf(datos[1])-1);
				fecha.setYear(Integer.valueOf(datos[2])-1900);
				cotizacionDTO.setFechaFinVigencia(fecha);
				
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreEmpresaContratante()))
				cotizacionDTO.setNombreEmpresaContratante(cotizacionForm.getNombreEmpresaContratante());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreEmpresaAsegurado()))
				cotizacionDTO.setNombreEmpresaAsegurado(cotizacionForm.getNombreEmpresaAsegurado());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioOrdenTrabajo()))
				cotizacionDTO.setCodigoUsuarioOrdenTrabajo(cotizacionForm.getCodigoUsuarioOrdenTrabajo());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioCotizacion()))
				cotizacionDTO.setCodigoUsuarioCotizacion(cotizacionForm.getCodigoUsuarioCotizacion());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaCreacion()))
				cotizacionDTO.setFechaCreacion(new Date(cotizacionForm.getFechaCreacion()));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioCreacion()))
				cotizacionDTO.setCodigoUsuarioCreacion(cotizacionForm.getCodigoUsuarioCreacion());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaModificacion()))
				cotizacionDTO.setFechaModificacion(new Date(cotizacionForm.getFechaModificacion()));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioModificacion()))
				cotizacionDTO.setCodigoUsuarioModificacion(cotizacionForm.getCodigoUsuarioModificacion());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveEstatus()))
				cotizacionDTO.setClaveEstatus(Short.valueOf(cotizacionForm.getClaveEstatus()));
			if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveAutRetroacDifer()))
				cotizacionDTO.setClaveAutRetroacDifer(Short.valueOf(cotizacionForm.getClaveAutRetroacDifer()));
			if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveAutVigenciaMaxMin()))
				cotizacionDTO.setClaveAutVigenciaMaxMin(Short.valueOf(cotizacionForm.getClaveAutVigenciaMaxMin()));
			if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveMotivo()))
				cotizacionDTO.setClaveMotivoEndoso(Short.valueOf(cotizacionForm.getClaveMotivo()));
			if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdTcAgente())) {
				SolicitudDTO solicitudDTO = SolicitudDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
				solicitudDTO.setCodigoAgente(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdTcAgente()));
				cotizacionDTO.setSolicitudDTO(solicitudDTO);
				AgenteDN agenteDN = AgenteDN.getInstancia();
				try{
					AgenteDTO agenteDTO = new AgenteDTO();
					agenteDTO.setIdTcAgente(cotizacionDTO.getSolicitudDTO().getCodigoAgente().intValue());
					agenteDTO = agenteDN.verDetalleAgente(agenteDTO,cotizacionDTO.getCodigoUsuarioModificacion());
					if(agenteDTO != null){
						cotizacionDTO.getSolicitudDTO().setNombreAgente(agenteDTO.getNombre());
		                solicitudDTO.setNombreOficinaAgente(agenteDTO.getNombreOficina());
		                solicitudDTO.setNombreEjecutivo(agenteDTO.getNombreOficina());
					}
	                if(agenteDTO.getIdOficina()!=null){
	                    BigDecimal idOficina=UtileriasWeb.regresaBigDecimal(agenteDTO.getIdOficina().trim());
	                    solicitudDTO.setIdOficina(idOficina);
	                    solicitudDTO.setCodigoEjecutivo(idOficina);
	                }
					
				}catch(Exception e){}
			}
		}
	/**
	 * Method guardarEmbarque
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public void guardarEmbarque(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
		BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
		BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
		String periodo = request.getParameter("periodo");
		String embarques = request.getParameter("numeroEmbarques");
		Double valor = Double.parseDouble(request.getParameter("valor"));
		String idGrid = request.getParameter("gr_id");
		String action = "";
		try{
			SubIncisoCotizacionDN subIncisoCotizacionDN = SubIncisoCotizacionDN.getInstancia();
			BigDecimal numeroSubInciso = subIncisoCotizacionDN.obtenerNumeroSubIncisoSiguiente(idToCotizacion, numeroInciso, idToSeccion);
			
			SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
			subIncisoCotizacionDTO.setClaveAutReaseguro((short)0);
			subIncisoCotizacionDTO.setClaveEstatusDeclaracion((short)0);
			subIncisoCotizacionDTO.setDescripcionSubInciso("Declaraci�n de embarque");			
			subIncisoCotizacionDTO.setValorSumaAsegurada(valor);
			
			SubIncisoCotizacionDTOId id = new SubIncisoCotizacionDTOId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(numeroInciso);
			id.setIdToSeccion(idToSeccion);
			id.setNumeroSubInciso(numeroSubInciso);
			subIncisoCotizacionDTO.setId(id);
			
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			
			DatoIncisoCotizacionId idSubInciso = new DatoIncisoCotizacionId();
			idSubInciso.setIdToCotizacion(subIncisoCotizacionDTO.getId().getIdToCotizacion());
			idSubInciso.setNumeroInciso(subIncisoCotizacionDTO.getId().getNumeroInciso());
			idSubInciso.setClaveDetalle((short)2);
			idSubInciso.setIdTcRamo(BigDecimal.valueOf(2D));
			idSubInciso.setIdTcSubramo(BigDecimal.valueOf(2D));
			idSubInciso.setIdToCobertura(BigDecimal.ZERO);
			idSubInciso.setIdToRiesgo(BigDecimal.valueOf(3120D));
			idSubInciso.setIdToSeccion(subIncisoCotizacionDTO.getId().getIdToSeccion());
			idSubInciso.setNumeroSubinciso(subIncisoCotizacionDTO.getId().getNumeroSubInciso());

			DatoIncisoCotizacionDTO periodoDeclaracion = new DatoIncisoCotizacionDTO();
			periodoDeclaracion.setValor(periodo);

			DatoIncisoCotizacionDTO numeroEmbarques = new DatoIncisoCotizacionDTO();
			numeroEmbarques.setValor(embarques);
			
			DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();		
			CalculoCotizacionDN calculoCotizacionDN = CalculoCotizacionDN.getInstancia();
			if(request.getParameter("!nativeeditor_status").equals("inserted") || request.getParameter("!nativeeditor_status").equals("updated")) {

				if(request.getParameter("!nativeeditor_status").equals("inserted")) {		
					subIncisoCotizacionDTO = cotizacionDN.agregarSubIncisoCotizacionDTO(subIncisoCotizacionDTO, 
							UtileriasWeb.obtieneNombreUsuario(request));
					//i.	Periodo de la declaraci�n:
					idSubInciso.setIdDato(BigDecimal.valueOf(10D));
					periodoDeclaracion.setId(idSubInciso);					
					datoIncisoCotizacionSN.agregar(periodoDeclaracion);
					//ii.	N�mero total de embarques en el periodo de la declaraci�n:
					idSubInciso.setIdDato(BigDecimal.valueOf(20D));
					numeroEmbarques.setId(idSubInciso);
					datoIncisoCotizacionSN.agregar(numeroEmbarques);
				
					calculoCotizacionDN.calcularRiesgoEmbarque(subIncisoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));
					action = "insert";
				} else {
					String[] ids = idGrid.split("_");
					Double noSub = Double.valueOf(ids[1]);
					subIncisoCotizacionDTO.getId().setNumeroSubInciso(BigDecimal.valueOf(noSub));
					subIncisoCotizacionDTO = cotizacionDN.modificarSubIncisoCotizacionDTO(subIncisoCotizacionDTO, 
							UtileriasWeb.obtieneNombreUsuario(request));
					//i.	Periodo de la declaraci�n:
					idSubInciso.setIdDato(BigDecimal.valueOf(10D));
					periodoDeclaracion.setId(idSubInciso);										
					datoIncisoCotizacionSN.modificar(periodoDeclaracion);
					//ii.	N�mero total de embarques en el periodo de la declaraci�n:
					idSubInciso.setIdDato(BigDecimal.valueOf(20D));
					numeroEmbarques.setId(idSubInciso);
					datoIncisoCotizacionSN.modificar(numeroEmbarques);

					calculoCotizacionDN.calcularRiesgoEmbarque(subIncisoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));
					action = "update";
				}			
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				String[] ids = idGrid.split("_");
				Double noSub = Double.valueOf(ids[1]);
				id.setNumeroSubInciso(BigDecimal.valueOf(noSub));
				subIncisoCotizacionDTO = subIncisoCotizacionDN.getPorId(id);
				
//				idSubInciso.setNumeroSubinciso(BigDecimal.valueOf(noSub));
//				//i.	Periodo de la declaraci�n:
//				idSubInciso.setIdDato(BigDecimal.valueOf(10D));
//				periodoDeclaracion.setId(idSubInciso);										
//				datoIncisoCotizacionSN.borrar(periodoDeclaracion);
//				//ii.	N�mero total de embarques en el periodo de la declaraci�n:
//				idSubInciso.setIdDato(BigDecimal.valueOf(20D));
//				numeroEmbarques.setId(idSubInciso);
//				datoIncisoCotizacionSN.borrar(numeroEmbarques);
//				
//				subIncisoCotizacionDTO.getId().setNumeroSubInciso(BigDecimal.valueOf(noSub));
//				cotizacionDN.borrarSubIncisoCotizacion(subIncisoCotizacionDTO);				
				
				calculoCotizacionDN.eliminarRiesgoEmbarque(subIncisoCotizacionDTO, UtileriasWeb.obtieneNombreUsuario(request));
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();			
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	/**
	 * Method mostrarEmbarques
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws SystemException 
	 * @throws IOException 
	 */
	public void mostrarEmbarques(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("id"));
		try{
			SubIncisoCotizacionDTO subInciso = new SubIncisoCotizacionDTO();
			subInciso.setId(new SubIncisoCotizacionDTOId());
			subInciso.getId().setIdToCotizacion(idToCotizacion);
			subInciso.setClaveEstatusDeclaracion((short)0);
			
			MidasJsonBase json = new MidasJsonBase();
			
			List<SubIncisoCotizacionDTO> subIncisos = SubIncisoCotizacionDN.getInstancia().listarFiltrado(subInciso);
			for(SubIncisoCotizacionDTO subIncisoCot: subIncisos){
				DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
				id.setIdToCotizacion(idToCotizacion);
				id.setNumeroInciso(subIncisoCot.getId().getNumeroInciso());
				id.setClaveDetalle((short)2);
				id.setIdTcRamo(BigDecimal.valueOf(2D));
				id.setIdTcSubramo(BigDecimal.valueOf(2D));
				id.setIdToCobertura(BigDecimal.ZERO);
				id.setIdToRiesgo(BigDecimal.valueOf(3120D));
				id.setIdToSeccion(subIncisoCot.getId().getIdToSeccion());
				id.setNumeroSubinciso(subIncisoCot.getId().getNumeroSubInciso());
				id.setIdDato(BigDecimal.valueOf(10D));				
				
				DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
				DatoIncisoCotizacionDTO periodoDeclaracion = datoIncisoCotizacionSN.getPorId(id);
				
				id.setIdDato(BigDecimal.valueOf(20D));
				DatoIncisoCotizacionDTO numeroEmbarques = datoIncisoCotizacionSN.getPorId(id);
				
				id.setIdDato(BigDecimal.valueOf(170D));
				id.setClaveDetalle((short)0);
				id.setNumeroSubinciso(BigDecimal.ZERO);
				id.setIdToSeccion(BigDecimal.ZERO);
				DatoIncisoCotizacionDTO cuota = datoIncisoCotizacionSN.getPorId(id);
				
//				DetallePrimaRiesgoCotizacionId idDetalle = new DetallePrimaRiesgoCotizacionId();
//				idDetalle.setIdToCotizacion(idToCotizacion);
//				idDetalle.setIdToSeccion(subIncisoCot.getId().getIdToSeccion());
//				idDetalle.setNumeroInciso(subIncisoCot.getId().getNumeroInciso());
//				idDetalle.setNumeroSubInciso(subIncisoCot.getId().getNumeroSubInciso());
//				
//				DetallePrimaRiesgoCotizacionDN detallePrimaRiesgoCotizacionDN = DetallePrimaRiesgoCotizacionDN.getInstancia();
//				
//				List<DetallePrimaRiesgoCotizacionDTO> detalles = detallePrimaRiesgoCotizacionDN.listarFiltrado(idDetalle);
//				
//				Double primaNeta = 0D;
//				
//				for(DetallePrimaRiesgoCotizacionDTO detalle: detalles){
//					primaNeta += detalle.getValorPrimaNeta();
//				}
				
				Double primaNeta = 0D;
				if(cuota != null && subIncisoCot.getValorSumaAsegurada()!= null){
					primaNeta = Double.valueOf(cuota.getValor()) * subIncisoCot.getValorSumaAsegurada()/1000D;
				}
				MidasJsonRow row = new MidasJsonRow();
				row.setId(subIncisoCot.getId().getNumeroInciso().toString() + "_"
						+subIncisoCot.getId().getNumeroSubInciso().toString()+ "_"
						+subIncisoCot.getId().getIdToSeccion().toString());

				row.setDatos(idToCotizacion.toString(),
						subIncisoCot.getId().getNumeroInciso().toString(),
						subIncisoCot.getId().getIdToSeccion().toString(),
						periodoDeclaracion.getValor(),
						numeroEmbarques.getValor(),
						subIncisoCot.getValorSumaAsegurada().toString(),
						cuota!=null?fCuota.format(Double.valueOf(cuota.getValor())):"",
						fMonto.format(primaNeta));
				json.addRow(row);
			}
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();			
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}	

	/**
	 * Method getIncisos
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws SystemException 
	 * @throws IOException 
	 */	
	public void getIncisos(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws SystemException, IOException {

		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request
				.getParameter("id"));
		StringBuffer buffer = new StringBuffer();
		
		List<IncisoCotizacionDTO> incisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
		
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		for(IncisoCotizacionDTO inciso: incisos){
			buffer.append("<item>");
			buffer.append("<id>");
			buffer.append(inciso.getId().getNumeroInciso().toString());
			buffer.append("</id>");
			buffer.append("<description><![CDATA[");
			buffer.append(inciso.getId().getNumeroInciso().toString());
			buffer.append("]]></description>");
			buffer.append("</item>");			
		}
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		response.getWriter().write(buffer.toString());		
	}
	/**
	 * Method getSecciones
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws SystemException 
	 * @throws IOException 
	 */	
	public void getSecciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws SystemException, IOException {
		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request
				.getParameter("id"));
		BigDecimal numeroInciso = null;
		try{
			numeroInciso = UtileriasWeb.regresaBigDecimal(request
					.getParameter("numeroInciso"));
		}catch (NumberFormatException e){}
		StringBuffer buffer = new StringBuffer();

		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		
		if(numeroInciso != null){
			List<SeccionCotizacionDTO> secciones = SeccionCotizacionDN.getInstancia().listarSeccionesContratadasSinCoberturaSinAgrupar(idToCotizacion, numeroInciso);

			for(SeccionCotizacionDTO seccion: secciones){
				
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(seccion.getId().getIdToSeccion().toString());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(seccion.getSeccionDTO().getNombreComercial());
				buffer.append("]]></description>");
				buffer.append("</item>");			
			}
			
		}else{
			buffer.append("<item>");
			buffer.append("<id>");
			buffer.append("");
			buffer.append("</id>");
			buffer.append("<description><![CDATA[");
			buffer.append("Seleccione...");
			buffer.append("]]></description>");
			buffer.append("</item>");				
		}
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		response.getWriter().write(buffer.toString());				
	}
	/**
	 * Method validaEndosoDeCancelacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws SystemException 
	 * @throws IOException 
	 */	
	public void validaEndosoDeCancelacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws SystemException, IOException {

		BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request
				.getParameter("id"));
		StringBuffer buffer = new StringBuffer();
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());

		CotizacionDTO cotizacionBase = CotizacionDN.getInstancia(nombreUsuario).getPorId(endosoDTO.getIdToCotizacion());
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		if (cotizacionBase.getClaveMotivoEndoso()!= null && (cotizacionBase.getClaveMotivoEndoso() == Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO)){
			buffer.append("<id>");
			buffer.append("true");
			buffer.append("</id>");			
		}else{
			buffer.append("<id>");
			buffer.append("false");
			buffer.append("</id>");						
		}
		buffer.append("</item>");
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		response.getWriter().write(buffer.toString());		
	}	

	/**
	 * Method llenarCamposModificacionDerechosRPF
	 * Llena los campos de cotizacionForm relacionados con la modificaci�n de derechos y recargo por pago fraccionado
	 * @param cotizacionDTO
	 * @param cotizacionForm
	 */
	public void llenarCamposModificacionDerechosRPF(CotizacionDTO cotizacionDTO, CotizacionForm cotizacionForm){
		//Llenar campos de modificaci�n de recargo por pago fraccionado y derechos
		
		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario() == null)
			cotizacionDTO.setClaveRecargoPagoFraccionadoUsuario(Sistema.CLAVE_RPF_SISTEMA);
		
		cotizacionForm.setTipoCalculoRPF(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().toString() );
					
		if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_SISTEMA) == 0){
			cotizacionForm.setValorRPFEditable(null);
			cotizacionForm.setPorcentajeRPFSoloLectura(null);				
			cotizacionForm.setPorcentajeRPFEditable(null);
			cotizacionForm.setValorRPFSoloLectura(null);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){
			cotizacionForm.setValorRPFEditable(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario());
			cotizacionForm.setPorcentajeRPFSoloLectura(fPorcentaje.format(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario()/100));				
			cotizacionForm.setPorcentajeRPFEditable(null);
			cotizacionForm.setValorRPFSoloLectura(null);
		}else if(cotizacionDTO.getClaveRecargoPagoFraccionadoUsuario().compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
			cotizacionForm.setValorRPFEditable(null);
			cotizacionForm.setPorcentajeRPFSoloLectura(null);				
			cotizacionForm.setPorcentajeRPFEditable(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario());
			cotizacionForm.setValorRPFSoloLectura(fMonto.format(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario()));
		}
								
		if(cotizacionDTO.getClaveDerechosUsuario() == null)
			cotizacionDTO.setClaveDerechosUsuario(Sistema.CLAVE_DERECHOS_SISTEMA);
		
		cotizacionForm.setTipoCalculoDerechos(cotizacionDTO.getClaveDerechosUsuario().toString());
		
		if(cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_SISTEMA) == 0){			
			cotizacionForm.setValorDerechos(null);
		}else if(cotizacionDTO.getClaveDerechosUsuario().compareTo(Sistema.CLAVE_DERECHOS_USUARIO) == 0){
			cotizacionForm.setValorDerechos(cotizacionDTO.getValorDerechosUsuario());
		}
		
		if(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() == null)
			cotizacionDTO.setClaveAutorizacionRecargoPagoFraccionado(Sistema.AUTORIZACION_NO_REQUERIDA);						
		cotizacionForm.setClaveAutorizacionRPF(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().toString());
		
		if(cotizacionDTO.getClaveAutorizacionDerechos() == null)
			cotizacionDTO.setClaveAutorizacionDerechos(Sistema.AUTORIZACION_NO_REQUERIDA);			
		cotizacionForm.setClaveAutorizacionDerechos(cotizacionDTO.getClaveAutorizacionDerechos().toString());				
	}
	
	/**
	 * Modifica los derechos y recargo por pago fraccionado de una cotizaci�n.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void modificarRPFDerechos(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		CotizacionForm cotizacionEndosoForm = (CotizacionForm)form;
		
		BigDecimal idToCotizacion = new BigDecimal(cotizacionEndosoForm.getIdToCotizacion());
		
		Short tipoCalculoDerechos = Short.valueOf(cotizacionEndosoForm.getTipoCalculoDerechos());																					
		Double valorDerechos = cotizacionEndosoForm.getValorDerechos();
		
		Short tipoCalculoRPF = Short.valueOf(cotizacionEndosoForm.getTipoCalculoRPF());	
		Double valorRecargoPagoFraccionado = 0D;
						
		if(tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0)
			valorRecargoPagoFraccionado = cotizacionEndosoForm.getValorRPFEditable();					
		else if(tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0)
			valorRecargoPagoFraccionado = cotizacionEndosoForm.getPorcentajeRPFEditable();		
		
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		
		CotizacionDTO cotizacionDTO = null;
		List<SoporteResumen>resumenComisiones = new ArrayList<SoporteResumen>();
		try{
			cotizacionDTO = cotizacionDN.modificarRecargoPagoFraccionado(idToCotizacion, tipoCalculoRPF, valorRecargoPagoFraccionado, false, false);
			cotizacionDTO = cotizacionDN.modificarDerechos(idToCotizacion, tipoCalculoDerechos, valorDerechos, false, false);
			cotizacionDTO.setPrimaNetaAnual(cotizacionDN.getPrimaNetaCotizacion(idToCotizacion));							
			ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,resumenComisiones);
		}catch(Exception e){
			e.getMessage();
			e.printStackTrace();
		}
				
		NumberFormat fMonto = new DecimalFormat("$#,##0.00");
		NumberFormat fPorcentaje = new DecimalFormat("##0.00##%");
		NumberFormat fPorcentajeSinDecimales = new DecimalFormat("##0%");
		NumberFormat fNumero = new DecimalFormat("0.00########");
		
		cotizacionEndosoForm.setPrimaNetaAnual(fMonto.format(cotizacionDTO.getPrimaNetaAnual()));
		cotizacionEndosoForm.setPrimaNetaCotizacion(fMonto.format(cotizacionDTO.getPrimaNetaCotizacion()));
		cotizacionEndosoForm.setMontoRecargoPagoFraccionado(fMonto.format(cotizacionDTO.getMontoRecargoPagoFraccionado()));
		cotizacionEndosoForm.setFactorIVA(fPorcentajeSinDecimales.format(cotizacionDTO.getFactorIVA()/100));
		cotizacionEndosoForm.setMontoIVA(fMonto.format(cotizacionDTO.getMontoIVA()));
		cotizacionEndosoForm.setDerechosPoliza(fMonto.format(cotizacionDTO.getDerechosPoliza()));
		cotizacionEndosoForm.setPrimaNetaTotal(fMonto.format(cotizacionDTO.getPrimaNetaTotal()));	
		cotizacionEndosoForm.setResumenComisiones(resumenComisiones);		
		cotizacionEndosoForm.setClaveAutorizacionRPF(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().toString());
		cotizacionEndosoForm.setClaveAutorizacionDerechos(cotizacionDTO.getClaveAutorizacionDerechos().toString());		

		valorDerechos = cotizacionDTO.getValorDerechosUsuario();
		valorRecargoPagoFraccionado = cotizacionDTO.getValorRecargoPagoFraccionadoUsuario();						
		
		String valorRPFEditable = " ",
	       porcentajeRPFSoloLectura = " ",
	       valorRPFSoloLectura = " ",
	       porcentajeRPFEditable = " ",
	       textoDerechos = " ";
							
		if(tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_MONTO) == 0){			
			valorRPFEditable = fNumero.format(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario());
			porcentajeRPFSoloLectura = fPorcentaje.format(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario()/100);
		}
		else if(tipoCalculoRPF.compareTo(Sistema.CLAVE_RPF_USUARIO_PORCENTAJE) == 0){
			valorRPFSoloLectura = fMonto.format(cotizacionDTO.getValorRecargoPagoFraccionadoUsuario());
			porcentajeRPFEditable = fNumero.format(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario());
		}
		
		if(tipoCalculoDerechos.compareTo(Sistema.CLAVE_DERECHOS_USUARIO) == 0)
			textoDerechos = fNumero.format(valorDerechos);
						
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<valorRPFEditable>" + valorRPFEditable + "</valorRPFEditable>");
		buffer.append("<porcentajeRPFSoloLectura>" + porcentajeRPFSoloLectura + "</porcentajeRPFSoloLectura>");
		buffer.append("<valorRPFSoloLectura>" + valorRPFSoloLectura + "</valorRPFSoloLectura>");
		buffer.append("<porcentajeRPFEditable>" + porcentajeRPFEditable + "</porcentajeRPFEditable>");
		buffer.append("<rpf>" + fMonto.format(valorRecargoPagoFraccionado) + "</rpf>");
		buffer.append("<rpfResumen>" + cotizacionEndosoForm.getMontoRecargoPagoFraccionado() + "</rpfResumen>");
		buffer.append("<textoDerechos>" + textoDerechos + "</textoDerechos>");
		buffer.append("<derechos>" + fMonto.format(valorDerechos) + "</derechos>");
		buffer.append("<iva>" + cotizacionEndosoForm.getMontoIVA() + "</iva>");
		buffer.append("<primaTotal>" + cotizacionEndosoForm.getPrimaNetaTotal() + "</primaTotal>");
		buffer.append("<claveAutRPF>" + cotizacionEndosoForm.getClaveAutorizacionRPF() + "</claveAutRPF>");
		buffer.append("<claveAutDerechos>" + cotizacionEndosoForm.getClaveAutorizacionDerechos() + "</claveAutDerechos>");
		buffer.append("</item>");
		buffer.append("</response>");

		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());

		try{
		response.getWriter().write(buffer.toString());
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
	
	/**
	 * Method mostrar modificar Cuota
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarModificarCuotas(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));			
			Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionForm cotitzacionForm = (CotizacionForm)form;
			
			SoporteEndosoCambioCuota datosCambioCuota = EndosoDN.getInstancia(
					usuario.getNombreUsuario()).getDatosCambioCuotaEndoso(
					idToCotizacion, idToSeccion, idToCobertura, usuario);

			cotitzacionForm.setDatosCambioCuota(datosCambioCuota);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
		
	}

	public void modificarCuotas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		BigDecimal idToCotizacion = null;
		BigDecimal idToSeccion = null;
		BigDecimal idToCobertura = null;
		BigDecimal numeroInciso = null;
		Double primaNeta = null;
		String operacion = null;
		StringBuffer buffer = new StringBuffer();
		Boolean resultado = Boolean.FALSE;
		Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
		
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
		
		try {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(usuario.getNombreUsuario());

			idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
			numeroInciso = UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso"));
			idToSeccion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToSeccion"));
			idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
			primaNeta = UtileriasWeb.regresaDouble(request.getParameter("primaNeta"));
			operacion = request.getParameter("operacion");
			
			
			if(operacion != null && operacion.equals("ALL")){
				SoporteEndosoCambioCuota datosCambioCuota = EndosoDN.getInstancia(
						usuario.getNombreUsuario()).getDatosCambioCuotaEndoso(
						idToCotizacion, idToSeccion, idToCobertura, usuario);	

				for(CoberturaCotizacionDTO coberturaInvolucrada: datosCambioCuota.getCoberturasInvolucradas()){
					primaNeta = coberturaInvolucrada.getValorCuotaOriginal() * coberturaInvolucrada.getValorSumaAsegurada();
					resultado = cotizacionDN
							.igualarPrimas(coberturaInvolucrada.getId()
									.getIdToCotizacion(),
									coberturaInvolucrada.getId()
											.getIdToSeccion(),
									coberturaInvolucrada.getId()
											.getIdToCobertura(), primaNeta,
									coberturaInvolucrada.getId()
											.getNumeroInciso(),
											usuario.getNombreUsuario());						
				}
				
			}else{
				resultado = cotizacionDN.igualarPrimas(idToCotizacion,idToSeccion,idToCobertura,primaNeta,numeroInciso, usuario.getNombreUsuario());
			}

			
			if(resultado){
				//Exito
				buffer.append("1</id><description><![CDATA[La Cuota se Modific&oacute; correctamente.]]></description>");
				
			}else{
				//error
				buffer.append("2</id><description><![CDATA[Ocurri&oacute un error al modificar la cuota, Intente nuevemante.]]></description>");				
			}
		} catch (SystemException e) {
			buffer.append("2</id><description><![CDATA[Ocurri&oacute un error al modificar la cuota, Intente nuevemante.]]></description>");
		} catch (ExcepcionDeAccesoADatos e) {
			buffer.append("2</id><description><![CDATA[Ocurri&oacute un error al modificar la cuota, Intente nuevemante.]]></description>");
		}
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}			
	}
		
}
