package mx.com.afirme.midas.cotizacion.seccion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;


/**
 * 
 * @author Rodrigo M�rquez Castillo
 * @since 9 Septiembre de 2009
 */
public class SeccionCotSN {
	private SeccionCotizacionFacadeRemote beanRemoto;
	
	public SeccionCotSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SeccionCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<SeccionCotizacionDTO> buscarPorNumeroInciso(int numeroInciso) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findByProperty("id.numeroInciso", numeroInciso);
	}
	
	public SeccionCotizacionDTO buscarPorId(SeccionCotizacionDTOId seccionCotizacionDTOId) throws ExcepcionDeAccesoADatos{
		
		return beanRemoto.findById(seccionCotizacionDTOId);
	}
	
	public SeccionCotizacionDTO actualizarSeccionCotizacion(SeccionCotizacionDTO entity){
		return beanRemoto.update(entity);
	}

	public List<SeccionCotizacionDTO> listarPorCotizacionNumeroInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso) throws ExcepcionDeAccesoADatos{
		return beanRemoto.listarPorCotizacionNumeroInciso(idToCotizacion, numeroInciso);
		
	}
	
	public Long obtenerTotalSeccionesContratadas(BigDecimal idToCotizacion, BigDecimal numeroInciso) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalSeccionesContratadas(idToCotizacion, numeroInciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
