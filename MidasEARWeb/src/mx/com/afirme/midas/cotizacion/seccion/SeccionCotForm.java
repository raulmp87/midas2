package mx.com.afirme.midas.cotizacion.seccion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SeccionCotForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 587585260120681078L;
	
	private String idToCotizacion;
	private String fechaCreacion;
	private String numeroInciso;
	private String idToSeccion;
	private String numeroSubInciso;
	private String claveTipoEndoso;
	
	// getters and setters
	
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public String getClaveTipoEndoso() {
		return claveTipoEndoso;
	}
	public void setClaveTipoEndoso(String claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}
	
}
