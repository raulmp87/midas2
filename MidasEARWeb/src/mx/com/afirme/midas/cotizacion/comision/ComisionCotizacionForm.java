package mx.com.afirme.midas.cotizacion.comision;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ComisionCotizacionForm extends MidasBaseForm {
	private static final long serialVersionUID = 5694605095248816798L;
	private String idToCotizacion;
	private String idTcSubramo;
	private String idTcRamo;
	private String porcentajeComisionDefault;
	private String porcentajeComisionCotizacion;
	private String valorComisionCotizacion;
	private String claveAutComision;
	private String codigoUsuarioAutComision;
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getIdTcSubramo() {
		return idTcSubramo;
	}
	public void setIdTcSubramo(String idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}
	public String getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public String getPorcentajeComisionDefault() {
		return porcentajeComisionDefault;
	}
	public void setPorcentajeComisionDefault(String porcentajeComisionDefault) {
		this.porcentajeComisionDefault = porcentajeComisionDefault;
	}
	public String getPorcentajeComisionCotizacion() {
		return porcentajeComisionCotizacion;
	}
	public void setPorcentajeComisionCotizacion(String porcentajeComisionCotizacion) {
		this.porcentajeComisionCotizacion = porcentajeComisionCotizacion;
	}
	public String getValorComisionCotizacion() {
		return valorComisionCotizacion;
	}
	public void setValorComisionCotizacion(String valorComisionCotizacion) {
		this.valorComisionCotizacion = valorComisionCotizacion;
	}
	public String getClaveAutComision() {
		return claveAutComision;
	}
	public void setClaveAutComision(String claveAutComision) {
		this.claveAutComision = claveAutComision;
	}
	public String getCodigoUsuarioAutComision() {
		return codigoUsuarioAutComision;
	}
	public void setCodigoUsuarioAutComision(String codigoUsuarioAutComision) {
		this.codigoUsuarioAutComision = codigoUsuarioAutComision;
	}
}
