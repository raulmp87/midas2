package mx.com.afirme.midas.cotizacion.comision;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ComisionCotizacionSN {
	private ComisionCotizacionFacadeRemote beanRemoto;

	public ComisionCotizacionSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en TipoPolizaSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ComisionCotizacionFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto ComisionCotizacion instanciado", Level.FINEST, null);
	}
	public List<ComisionCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}
	public void agregar(ComisionCotizacionDTO comisionCotizacionDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(comisionCotizacionDTO);
	}
	public void modificar(ComisionCotizacionDTO comisionCotizacionDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.update(comisionCotizacionDTO);
	}
	public ComisionCotizacionDTO getPorId(ComisionCotizacionId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}
	public void borrar(ComisionCotizacionDTO comisionCotizacionDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(comisionCotizacionDTO);
	}
	public List<ComisionCotizacionDTO> buscarPorPropiedad(String nombrePropiedad,Object valor) {
		return beanRemoto.findByProperty(nombrePropiedad, valor);
	}
	public List<ComisionCotizacionDTO> buscarPorClaveAutorizacion(BigDecimal idToCotizacion, Short claveAutorizacion) {
		return beanRemoto.encontrarPorClaveAutirizacionPorCotizacion(idToCotizacion, claveAutorizacion);
	}
	public List<ComisionCotizacionDTO> buscarPendientesPorAutorizar(BigDecimal idToCotizacion) {
		return beanRemoto.buscarComisionesPorAutorizar(idToCotizacion);
	}
}
