package mx.com.afirme.midas.cotizacion.comision;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * ComisionCotizacionAction
 * @author Jos� Luis Arellano
 * @since 21 de septiembre de 2009
 */
public class ComisionCotizacionAction extends MidasMappingDispatchAction {
	private static final NumberFormat fMonto = Sistema.FORMATO_MONEDA;		
	
	/**
	 * Method mostrar
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String id = request.getParameter("id");
		cotizacionForm.setIdToCotizacion(id);
		String tipo = request.getParameter("tipo");
		BigDecimal idToCotizacion;
		try {
			idToCotizacion = UtileriasWeb.regresaBigDecimal(id);
			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(
					UtileriasWeb.obtieneNombreUsuario(request)).getPorId(
					idToCotizacion);
			cotizacionForm.setBonificacionComision(cotizacionDTO
					.getPorcentajebonifcomision().toString());		
			if (tipo != null){
				if (tipo.equals("comision"))
					reglaNavegacion = "editaPorcentaje";
				else if (tipo.equals("autorizacion"))
					reglaNavegacion = "editaAutorizacion";
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}			
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarComisionesCotizacion
	 * 
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public void mostrarComisionesCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {

		String id = request.getParameter("id").toString();
		String tipoEdicion = request.getParameter("tipoEdicion").toString();
		
		MidasJsonBase json = new MidasJsonBase();
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
		cotizacionDTO.setPrimaNetaAnual(cotizacionDN.getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion()));
		List<SoporteResumen>resumenComisiones = new ArrayList<SoporteResumen>();
		ResumenCotizacionDN.getInstancia().setTotalesResumenCotizacion(cotizacionDTO,resumenComisiones);

		if (tipoEdicion.equals("comision")){
			ComisionCotizacionDN comisionCotizacionDN = ComisionCotizacionDN.getInstancia();
			for(SoporteResumen comision: resumenComisiones){
			
				String icono = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_RECHAZADO, "Rechazado", null, null);//"/MidasWeb/img/b_rechazar.gif^Rechazado^javascript:void(0);^_self";
				ComisionCotizacionDTO comisionCotizacionDTO = comisionCotizacionDN.getPorId(comision.getId());
				
				SubRamoDTO subRamo = new SubRamoDTO();
				subRamo.setIdTcSubRamo(comisionCotizacionDTO.getId().getIdTcSubramo());
				subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);		

				String tipoPorcentaje = "RO";
				if (comisionCotizacionDTO.getId().getTipoPorcentajeComision() == 2)
					tipoPorcentaje = "RCI";
				else if (comisionCotizacionDTO.getId().getTipoPorcentajeComision() == 3)
					tipoPorcentaje = "PRR";
				
				//si claveAutComision es 0, la comision fu� autorizada
				if (comisionCotizacionDTO.getClaveAutComision().intValue()==0)
					icono = UtileriasWeb.generarLineaImagenDataGrid(null, "Autorizado", null, null);// "/MidasWeb/img/blank.gif^Autorizado";
				//Si es 1, la comisi�n est� pendiente por autorizar
				else if (comisionCotizacionDTO.getClaveAutComision().intValue()==1)
					icono = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_CALENDARIO, "Pendiente", null, null);//"/MidasWeb/img/b_pendiente.gif^Pendiente^javascript:void(0);^_self";
				//Verificar el significado de los valores 7 y 8, que son permitidos en este campo
				//La clave 7 corresponde a "autorizada", la clave 8 corresponde a "rechazada"
				else if (comisionCotizacionDTO.getClaveAutComision().intValue() == 7)
					icono = UtileriasWeb.generarLineaImagenDataGrid(Sistema.ICONO_AUTORIZADO, "Autorizado", null, null);//"/MidasWeb/img/b_autorizar.gif^Autorizado^javascript:void(0);^_self";
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(id+"|"+comisionCotizacionDTO.getId().getIdTcSubramo()+"|"+comisionCotizacionDTO.getId().getTipoPorcentajeComision());
				row.setDatos(
						subRamo.getRamoDTO().getDescripcion(),
						subRamo.getDescripcionSubRamo(),
						tipoPorcentaje,
						comisionCotizacionDTO.getPorcentajeComisionDefault().toString(),
						comisionCotizacionDTO.getPorcentajeComisionCotizacion().toString(),
						fMonto.format(comision.getMontoComision()),
						fMonto.format(comision.getMontoComisionCedida()),
						fMonto.format(comision.getMontoComision() * cotizacionDTO.getDiasPorDevengar()/365),
						fMonto.format((comision.getMontoComision() * cotizacionDTO.getDiasPorDevengar()/365)*cotizacionDTO.getPorcentajePagoFraccionado()/100),
						"",
						icono
				);
				json.addRow(row);			
			}
		}else if(tipoEdicion.equals("autorizacion")){

			List<ComisionCotizacionDTO> comisionesPendietes = ComisionCotizacionDN.getInstancia().buscarPendientesPorAutorizar(UtileriasWeb.regresaBigDecimal(id));
			for(ComisionCotizacionDTO comisionCotizacionDTO: comisionesPendietes){
				for(SoporteResumen comision: resumenComisiones){
					if(comisionCotizacionDTO.getId().equals(comision.getId())){
						String icono = "/MidasWeb/img/b_rechazar.gif^Rechazado";

						
						SubRamoDTO subRamo = new SubRamoDTO();
						subRamo.setIdTcSubRamo(comisionCotizacionDTO.getId().getIdTcSubramo());
						subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);

						String tipoPorcentaje = "RO";
						if (comisionCotizacionDTO.getId().getTipoPorcentajeComision() == 2)
							tipoPorcentaje = "RCI";
						else if (comisionCotizacionDTO.getId().getTipoPorcentajeComision() == 3)
							tipoPorcentaje = "PRR";		
						//si claveAutComision es 1, la comision est� pendiente y no se muestra icono
						//Si no es 1, debe ser 8, que significa que fu� rechazado, se toma como rechazada.
						if (comisionCotizacionDTO.getClaveAutComision().intValue()==1)
							icono = "/MidasWeb/img/blank.gif^Pendiente por autorizar";
						
						MidasJsonRow row = new MidasJsonRow();
						row.setId(id+"|"+comisionCotizacionDTO.getId().getIdTcSubramo()+"|"+comisionCotizacionDTO.getId().getTipoPorcentajeComision());
						row.setDatos(
								subRamo.getRamoDTO().getDescripcion(),
								subRamo.getDescripcionSubRamo(),
								tipoPorcentaje,
								comisionCotizacionDTO.getPorcentajeComisionDefault() + "%",
								comisionCotizacionDTO.getPorcentajeComisionCotizacion().toString(),
								fMonto.format(comision.getMontoComision()),
								fMonto.format(comision.getMontoComisionCedida()),
								icono,
								"0"
						);
						json.addRow(row);					
					}
				}
			}			
		}
		
		response.setContentType("text/json");
		System.out.println("Comisiones de la cotizacion "+id+", : "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	/**
	 * Method guardarComisionCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarComisionCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		ActionForward result = mapping.findForward(Sistema.EXITOSO);
		ComisionCotizacionId id = new ComisionCotizacionId();
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		String action = "";
		try {
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdTcSubramo(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setTipoPorcentajeComision(Short.valueOf(ids[2]));
			
			ComisionCotizacionDTO comisionCotizacion = new ComisionCotizacionDTO();
			comisionCotizacion = ComisionCotizacionDN.getInstancia().getPorId(id);
			if(request.getParameter("!nativeeditor_status").equals("updated")) {
				
				CotizacionDTO cotizacion = new CotizacionDTO();
				cotizacion.setIdToCotizacion(id.getIdToCotizacion());
				cotizacion = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(cotizacion);
				
				SubRamoDTO subRamo = new SubRamoDTO();
				subRamo.setIdTcSubRamo(id.getIdTcSubramo());
				subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
				
				comisionCotizacion.setCotizacionDTO(cotizacion);
				comisionCotizacion.setSubRamoDTO(subRamo);
				comisionCotizacion.setPorcentajeComisionCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("comisionCotizacion")));
				//Porcentaje de la Comision no requiere autorizacion, estatus 0
				if (comisionCotizacion.getPorcentajeComisionCotizacion().compareTo(comisionCotizacion.getPorcentajeComisionDefault()) == -1)
					comisionCotizacion.setClaveAutComision(Short.valueOf("0"));
				//Porcentaje pendiente
				else
					comisionCotizacion.setClaveAutComision(Short.valueOf("1"));
				ComisionCotizacionDN.getInstancia().modificar(comisionCotizacion);
				action = "update";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			result = null;
		} catch (SystemException e) {
			result = mapping.findForward(Sistema.NO_DISPONIBLE);
		} catch (ExcepcionDeAccesoADatos e) {
			result = mapping.findForward(Sistema.NO_EXITOSO);
		} catch (IOException e) {	e.printStackTrace();}
		return result;
	}
	
	/**
	 * Method autorizarComisionCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward autorizarComisionCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		ActionForward result = mapping.findForward(Sistema.EXITOSO);
		ComisionCotizacionId id = new ComisionCotizacionId();
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		String action = "";
		try {
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdTcSubramo(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setTipoPorcentajeComision(Short.valueOf(ids[2]));
			String seleccion = request.getParameter("autorizacion");
			if (seleccion.equals("1")){
				ComisionCotizacionDTO comisionCotizacion = new ComisionCotizacionDTO();
				comisionCotizacion = ComisionCotizacionDN.getInstancia().getPorId(id);
				if(request.getParameter("!nativeeditor_status").equals("updated")) {
					
					CotizacionDTO cotizacion = new CotizacionDTO();
					cotizacion.setIdToCotizacion(id.getIdToCotizacion());
					cotizacion = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(cotizacion);
					
					SubRamoDTO subRamo = new SubRamoDTO();
					subRamo.setIdTcSubRamo(id.getIdTcSubramo());
					subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
					
					comisionCotizacion.setCotizacionDTO(cotizacion);
					comisionCotizacion.setSubRamoDTO(subRamo);
					Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
//					for (Rol rol : usuario.getRoles()) {
//						if (rol.getDescripcion().equals("cabinero")){
					comisionCotizacion.setClaveAutComision(Short.valueOf("7"));
					comisionCotizacion.setCodigoUsuarioAutComision(usuario.getId().toString());
					action = "delete";
//						break;
//						}
//			    	}
					ComisionCotizacionDN.getInstancia().modificar(comisionCotizacion);
				}
			}
			else	action="update";
				
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			result = null;
		} catch (SystemException e) {
			result = mapping.findForward(Sistema.NO_DISPONIBLE);
		} catch (ExcepcionDeAccesoADatos e) {
			result = mapping.findForward(Sistema.NO_EXITOSO);
		} catch (IOException e) {	e.printStackTrace();}
		return result;
	}
	
	/**
	 * Method rechazarComisionCotizacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward rechazarComisionCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		ActionForward result = mapping.findForward(Sistema.EXITOSO);
		ComisionCotizacionId id = new ComisionCotizacionId();
		String idRegistro = request.getParameter("gr_id");
		String ids[] = idRegistro.split("\\|");
		String action = "";
		try {
			id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(ids[0]));
			id.setIdTcSubramo(UtileriasWeb.regresaBigDecimal(ids[1]));
			id.setTipoPorcentajeComision(Short.valueOf(ids[2]));
			String seleccion = request.getParameter("autorizacion");
			if (seleccion.equals("1")){
				ComisionCotizacionDTO comisionCotizacion = new ComisionCotizacionDTO();
				comisionCotizacion = ComisionCotizacionDN.getInstancia().getPorId(id);
				if(request.getParameter("!nativeeditor_status").equals("updated")) {
					CotizacionDTO cotizacion = new CotizacionDTO();
					cotizacion.setIdToCotizacion(id.getIdToCotizacion());
					cotizacion = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(cotizacion);
					
					SubRamoDTO subRamo = new SubRamoDTO();
					subRamo.setIdTcSubRamo(id.getIdTcSubramo());
					subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
					
					comisionCotizacion.setCotizacionDTO(cotizacion);
					comisionCotizacion.setSubRamoDTO(subRamo);
					Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
					for (Rol rol : usuario.getRoles()) {
						if (rol.getDescripcion().equals(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
							comisionCotizacion.setClaveAutComision(Short.valueOf("8"));
							comisionCotizacion.setCodigoUsuarioAutComision(usuario.getId().toString());
							break;
						}
			    	}
					ComisionCotizacionDN.getInstancia().modificar(comisionCotizacion);
					action = "delete";
				}
			}
			else	action="update";
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			result = null;
		} catch (SystemException e) {
			result = mapping.findForward(Sistema.NO_DISPONIBLE);
		} catch (ExcepcionDeAccesoADatos e) {
			result = mapping.findForward(Sistema.NO_EXITOSO);
		} catch (IOException e) {	e.printStackTrace();}
		return result;
	}
	/**
	 * Method guardarBonificacion
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarBonificacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm)form;
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		Double porcentaje = UtileriasWeb.regresaDouble(cotizacionForm.getBonificacionComision());
		
		try {
			CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
			cotizacionDTO.setPorcentajebonifcomision(porcentaje);
			cotizacionDN.modificar(cotizacionDTO);
			ActionForward forward = new ActionForward(mapping.findForward(reglaNavegacion));
			forward.setPath(forward.getPath()+"?id="+cotizacionForm.getIdToCotizacion()+"&tipo=comision");
			return forward;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),request);
		}
		return mapping.findForward(reglaNavegacion);
	}		
}
