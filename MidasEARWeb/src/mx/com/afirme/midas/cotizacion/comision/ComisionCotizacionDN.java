package mx.com.afirme.midas.cotizacion.comision;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ComisionCotizacionDN {
	private static final ComisionCotizacionDN INSTANCIA = new ComisionCotizacionDN();

	public static ComisionCotizacionDN getInstancia() {
		return ComisionCotizacionDN.INSTANCIA;
	}

	public List<ComisionCotizacionDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new ComisionCotizacionSN().listarTodos();
	}

	public void agregar(ComisionCotizacionDTO comisionCotizacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new ComisionCotizacionSN().agregar(comisionCotizacionDTO);
	}

	public void modificar(ComisionCotizacionDTO comisionCotizacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new ComisionCotizacionSN().modificar(comisionCotizacionDTO);
	}

	public ComisionCotizacionDTO getPorId(ComisionCotizacionId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new ComisionCotizacionSN().getPorId(id);
	}

	public void borrar(ComisionCotizacionDTO comisionCotizacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new ComisionCotizacionSN().borrar(comisionCotizacionDTO);
	}
	
	public List<ComisionCotizacionDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws SystemException, ExcepcionDeAccesoADatos {
		return new ComisionCotizacionSN().buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public List<ComisionCotizacionDTO> listarPorCotizacion(BigDecimal idToCotizacion) throws SystemException, ExcepcionDeAccesoADatos {
		return new ComisionCotizacionSN().buscarPorPropiedad("id.idToCotizacion", idToCotizacion);
	} 
	
	public List<ComisionCotizacionDTO> listarPorClaveAutorizacionPorCotizacion(BigDecimal idToCotizacion,Short claveAutorizacion) throws SystemException, ExcepcionDeAccesoADatos {
		return new ComisionCotizacionSN().buscarPorClaveAutorizacion(idToCotizacion, claveAutorizacion);
	}
	
	public List<ComisionCotizacionDTO> buscarPendientesPorAutorizar(BigDecimal idToCotizacion) throws SystemException, ExcepcionDeAccesoADatos {
		return new ComisionCotizacionSN().buscarPendientesPorAutorizar(idToCotizacion);
	}
}