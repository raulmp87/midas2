package mx.com.afirme.midas.cotizacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.cliente.ClienteForm;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.resumen.ResumenIncisoCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.SoporteResumen;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.endoso.DiferenciaCotizacionEndosoDTO;
import mx.com.afirme.midas.endoso.SoporteEndosoCambioCuota;
import mx.com.afirme.midas.interfaz.endoso.EndosoCoberturaDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.persona.PersonaForm;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.gestionPendientes.Pendiente;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class CotizacionForm extends MidasBaseForm {
	/**
	 * 
	 */
	java.text.DateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
	private static final long serialVersionUID = 587585260120681078L;
	private String idToCotizacion;
	private String idToDireccionContratante;
	private String idToDireccionAsegurado;
	private String idToTipoPoliza;
	private String idToSolicitud;
	private String idToDireccionCobro;
	private String idToPersonaContratante;
	private String idToPersonaAsegurado;
	private String idMoneda;
	private String idFormaPago;
	private String idMedioPago;
	private String fechaInicioVigencia;
	private String fechaFinVigencia;
	private String nombreEmpresaContratante;
	private String nombreEmpresaAsegurado;
	private String codigoUsuarioOrdenTrabajo;
	private String codigoUsuarioCotizacion;
	private String fechaCreacion;
	private String codigoUsuarioCreacion;
	private String fechaModificacion;
	private String codigoUsuarioModificacion;
	private String claveEstatus;
	private String diasGracia;
	private DireccionForm direccionContratante;
	private DireccionForm direccionAsegurado = new DireccionForm();
	private DireccionForm direccionCobro;
	private DireccionForm direccionGeneral;
	private PersonaForm personaContratante;
	private PersonaForm personaAsegurado = new PersonaForm();
	private PersonaForm personaGeneral;
	private ClienteForm clienteAsegurado;
	private ClienteForm clienteContratante;
	private ClienteForm clienteGeneral;
	private String telefonoContacto;
	private String producto;
	private List<TipoPolizaDTO> listaTipoPoliza = new ArrayList<TipoPolizaDTO>();
	private List<MonedaDTO> listaMoneda = new ArrayList<MonedaDTO>();
	private List<FormaPagoIDTO> listaFormaPago = new ArrayList<FormaPagoIDTO>();
	private List<MedioPagoDTO> listaMedioPago = new ArrayList<MedioPagoDTO>();
	private List<AgenteDTO> agentes;
	private String nombreAgente;
	private String idTcAgente;
	private String idToCotizacionFormateada;
	private List<ComisionCotizacionDTO> comisiones;
	private String edicionComision = "no";
	private String autorizacionComision = "no";
	private String contratada;
	private String fecha;
	private String usuario;
	private String bonificacionComision = "0";
	private List<Pendiente> listaPendientes = new ArrayList<Pendiente>();
	private String contienePendientes;
	private String idOrdenTrabajoFormateada;
	private String nombreSolicitante;
	private String apellidoPaternoSolicitante;
	private String apellidoMaternoSolicitante;
	private String idToProducto;
	private String idMotivoRechazo;
	private List<ResumenIncisoCotizacionForm> listaIncisos;
	private String editaIncisos;
	private String editaPrimerRiesgo;
	private String permiteCambiarPoliza;
	private String editaDatoGeneral = "false";
	private String claveAutRetroacDifer;
	private String claveAutVigenciaMaxMin;
	private String deshabilitaCambioFP = "false";
	
	/*Variables usadas en la validación con reaseguro*/
	private String cumuloPoliza;
	private String cumuloInciso;
	private String cumuloSubInciso;

	/* Variables adicionales usadas en Tab Datos Generales */
	private String moneda;
	private String tipoDePoliza;
	private String formaPago;
	private String oficina;
	private String medioDePago;
	private String esCotizacion; // 0:Es OT, 1:Es Cotización
	private List<Usuario> usuarios;

	/* Variables adicionales para cotizacion de endosos */
	private String claveTipoEndoso;
	private String claveMotivo;
	private String fechaInicioVigenciaEndoso;
	private List<MovimientoCotizacionEndosoDTO> movimientosGenerales;
	private List<MovimientoCotizacionEndosoDTO> movimientosInciso;
	private String numeroPolizaFormateada;
	private String idFormaPagoAnterior;

	/* Variables adicionales para resumen de incisos */
	private String primaNetaAnual;
	private String primaNetaCotizacion;
	private String montoRecargoPagoFraccionado;
	private String derechosPoliza;
	private String factorIVA;
	private String montoIVA;
	private String primaNetaTotal;
	private String diasPorDevengar;
	private List<SoporteResumen> resumenComisiones;
	private String idToPoliza;
	private String mensajeErrorCumulo;

	private List<DiferenciaCotizacionEndosoDTO> diferenciaCotizacionEndosoDTOs;
	private List<EndosoCoberturaDTO> coberturasAgrupadas;

	private SoporteEndosoCambioCuota datosCambioCuota;

	/* Variable para el tab de aclaraciones */
	private String aclaraciones;

	/* Variable para el tab de endosos */
	private PolizaDTO polizaDTO;

	/* Variables para datos de licitación */
	private String textoLicitacion;
	private String claveImpresionSumaAsegurada;
	private String mostrarRadioBttnClaveImpresionSA;

	/* Variables para modificación de recargo por pago fraccionado y derechos */
	private String tipoCalculoRPF;
	private Double valorRPFEditable;
	private String valorRPFSoloLectura;
	private Double porcentajeRPFEditable;
	private String porcentajeRPFSoloLectura;
	private String tipoCalculoDerechos;
	private Double valorDerechos;
	private String claveAutorizacionRPF;
	private String claveAutorizacionDerechos;
	private String mostrarEdicionDerechosRPF;
	private String claveAutorizacionCliente;
	private String claveAutorizacionDiasGracia;

	private String mostrarChkAutRetroDifer;
	private String mostrarChkAutVigMaxMin;
	private String mostrarChkAutRPF;
	private String mostrarChkAutDerechos;
	private String mostrarChkAutCliente;
	private String mostrarChkAutDiasGracia;

	/* Variables para mensajes */
	private String mensaje;
	private String tipoMensaje;

	//Renovaciones
	private String esRenovacion; // 0:No es, 1:Si es
	
	public String getMensajeErrorCumulo() {
		return mensajeErrorCumulo;
	}

	public void setMensajeErrorCumulo(String mensajeErrorCumulo) {
		this.mensajeErrorCumulo = mensajeErrorCumulo;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public String getIdToDireccionContratante() {
		return idToDireccionContratante;
	}

	public void setIdToDireccionContratante(String idToDireccionContratante) {
		this.idToDireccionContratante = idToDireccionContratante;
	}

	public String getIdToDireccionAsegurado() {
		return idToDireccionAsegurado;
	}

	public void setIdToDireccionAsegurado(String idToDireccionAsegurado) {
		this.idToDireccionAsegurado = idToDireccionAsegurado;
	}

	public String getIdToTipoPoliza() {
		return idToTipoPoliza;
	}

	public void setIdToTipoPoliza(String idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}

	public String getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(String idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public String getIdToDireccionCobro() {
		return idToDireccionCobro;
	}

	public void setIdToDireccionCobro(String idToDireccionCobro) {
		this.idToDireccionCobro = idToDireccionCobro;
	}

	public String getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(String idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public String getFechaInicioVigencia() {
		if (this.fechaInicioVigencia == null
				|| this.fechaInicioVigencia.equals("")) {
			return sdf.format(new Date());
		}
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getFechaFinVigencia() {
		if (this.fechaInicioVigencia == null
				|| this.fechaInicioVigencia.equals("")) {
			String[] fechaArr = getFechaInicioVigencia().split("/");
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(Integer.valueOf(fechaArr[2]),
					Integer.valueOf(fechaArr[1]) - 1, Integer
							.valueOf(fechaArr[0]));
			gc.add(GregorianCalendar.YEAR, 1);
			return sdf.format(gc.getTime());
		}
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getNombreEmpresaContratante() {
		return nombreEmpresaContratante;
	}

	public void setNombreEmpresaContratante(String nombreEmpresaContratante) {
		this.nombreEmpresaContratante = nombreEmpresaContratante;
	}

	public String getNombreEmpresaAsegurado() {
		return nombreEmpresaAsegurado;
	}

	public void setNombreEmpresaAsegurado(String nombreEmpresaAsegurado) {
		this.nombreEmpresaAsegurado = nombreEmpresaAsegurado;
	}

	public String getCodigoUsuarioOrdenTrabajo() {
		return codigoUsuarioOrdenTrabajo;
	}

	public void setCodigoUsuarioOrdenTrabajo(String codigoUsuarioOrdenTrabajo) {
		this.codigoUsuarioOrdenTrabajo = codigoUsuarioOrdenTrabajo;
	}

	public String getCodigoUsuarioCotizacion() {
		return codigoUsuarioCotizacion;
	}

	public void setCodigoUsuarioCotizacion(String codigoUsuarioCotizacion) {
		this.codigoUsuarioCotizacion = codigoUsuarioCotizacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	public String getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public DireccionForm getDireccionContratante() {
		return direccionContratante;
	}

	public void setDireccionContratante(DireccionForm direccionContratante) {
		this.direccionContratante = direccionContratante;
	}

	public DireccionForm getDireccionAsegurado() {
		return direccionAsegurado;
	}

	public void setDireccionAsegurado(DireccionForm direccionAsegurado) {
		this.direccionAsegurado = direccionAsegurado;
	}

	public DireccionForm getDireccionCobro() {
		return direccionCobro;
	}

	public void setDireccionCobro(DireccionForm direccionCobro) {
		this.direccionCobro = direccionCobro;
	}

	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public List<TipoPolizaDTO> getListaTipoPoliza() {
		return listaTipoPoliza;
	}

	public void setListaTipoPoliza(List<TipoPolizaDTO> listaTipoPoliza) {
		this.listaTipoPoliza = listaTipoPoliza;
	}

	public List<MonedaDTO> getListaMoneda() {
		return listaMoneda;
	}

	public void setListaMoneda(List<MonedaDTO> listaMoneda) {
		this.listaMoneda = listaMoneda;
	}

	public List<FormaPagoIDTO> getListaFormaPago() {
		return listaFormaPago;
	}

	public void setListaFormaPago(List<FormaPagoIDTO> listaFormaPago) {
		this.listaFormaPago = listaFormaPago;
	}

	public List<MedioPagoDTO> getListaMedioPago() {
		return listaMedioPago;
	}

	public void setListaMedioPago(List<MedioPagoDTO> listaMedioPago) {
		this.listaMedioPago = listaMedioPago;
	}

	public String getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(String idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public String getIdToPersonaAsegurado() {
		return idToPersonaAsegurado;
	}

	public void setIdToPersonaAsegurado(String idToPersonaAsegurado) {
		this.idToPersonaAsegurado = idToPersonaAsegurado;
	}

	public DireccionForm getDireccionGeneral() {
		return direccionGeneral;
	}

	public void setDireccionGeneral(DireccionForm direccionGeneral) {
		this.direccionGeneral = direccionGeneral;
	}

	public PersonaForm getPersonaContratante() {
		return personaContratante;
	}

	public void setPersonaContratante(PersonaForm personaContratante) {
		this.personaContratante = personaContratante;
	}

	public PersonaForm getPersonaAsegurado() {
		return personaAsegurado;
	}

	public void setPersonaAsegurado(PersonaForm personaAsegurado) {
		this.personaAsegurado = personaAsegurado;
	}

	public PersonaForm getPersonaGeneral() {
		return personaGeneral;
	}

	public void setPersonaGeneral(PersonaForm personaGeneral) {
		this.personaGeneral = personaGeneral;
	}

	public String getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(String idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public void setAgentes(List<AgenteDTO> agentes) {
		this.agentes = agentes;
	}

	public List<AgenteDTO> getAgentes() {
		return agentes;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public void setIdTcAgente(String idTcAgente) {
		this.idTcAgente = idTcAgente;
	}

	public String getIdTcAgente() {
		return idTcAgente;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getTipoDePoliza() {
		return tipoDePoliza;
	}

	public void setTipoDePoliza(String tipoDePoliza) {
		this.tipoDePoliza = tipoDePoliza;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getMedioDePago() {
		return medioDePago;
	}

	public void setMedioDePago(String medioDePago) {
		this.medioDePago = medioDePago;
	}

	public String getIdToCotizacionFormateada() {
		return idToCotizacionFormateada;
	}

	public void setIdToCotizacionFormateada(String idToCotizacionFormateada) {
		this.idToCotizacionFormateada = idToCotizacionFormateada;
	}

	public String getEsCotizacion() {
		return esCotizacion;
	}

	public void setEsCotizacion(String esCotizacion) {
		this.esCotizacion = esCotizacion;
	}

	public List<ComisionCotizacionDTO> getComisiones() {
		return comisiones;
	}

	public void setComisiones(List<ComisionCotizacionDTO> comisiones) {
		this.comisiones = comisiones;
	}

	public String getContratada() {
		return contratada;
	}

	public void setContratada(String contratada) {
		this.contratada = contratada;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEdicionComision() {
		return edicionComision;
	}

	public void setEdicionComision(String edicionComision) {
		this.edicionComision = edicionComision;
	}

	public String getAutorizacionComision() {
		return autorizacionComision;
	}

	public void setAutorizacionComision(String autorizacionComision) {
		this.autorizacionComision = autorizacionComision;
	}

	public ClienteForm getClienteAsegurado() {
		return clienteAsegurado;
	}

	public void setClienteAsegurado(ClienteForm clienteAsegurado) {
		this.clienteAsegurado = clienteAsegurado;
	}

	public ClienteForm getClienteContratante() {
		return clienteContratante;
	}

	public void setClienteContratante(ClienteForm clienteContratante) {
		this.clienteContratante = clienteContratante;
	}

	public ClienteForm getClienteGeneral() {
		return clienteGeneral;
	}

	public void setClienteGeneral(ClienteForm clienteGeneral) {
		this.clienteGeneral = clienteGeneral;
	}

	public String getBonificacionComision() {
		return bonificacionComision;
	}

	public void setBonificacionComision(String bonificacionComision) {
		this.bonificacionComision = bonificacionComision;
	}

	public List<Pendiente> getListaPendientes() {
		return listaPendientes;
	}

	public void setListaPendientes(List<Pendiente> listaPendientes) {
		this.listaPendientes = listaPendientes;
	}

	public String getContienePendientes() {
		return contienePendientes;
	}

	public void setContienePendientes(String contienePendientes) {
		this.contienePendientes = contienePendientes;
	}

	public String getIdOrdenTrabajoFormateada() {
		return idOrdenTrabajoFormateada;
	}

	public void setIdOrdenTrabajoFormateada(String idOrdenTrabajoFormateada) {
		this.idOrdenTrabajoFormateada = idOrdenTrabajoFormateada;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
		if (!UtileriasWeb.esCadenaVacia(idToProducto)) {
			try {
				listaTipoPoliza = new TipoPolizaSN()
						.listarVigentesPorIdProducto(UtileriasWeb
								.regresaBigDecimal(idToProducto));
				if (!UtileriasWeb.esCadenaVacia(idToTipoPoliza))
					listaMoneda = new MonedaSN()
							.listarMonedasAsociadasTipoPoliza(UtileriasWeb
									.regresaBigDecimal(idToTipoPoliza));
			} catch (ExcepcionDeAccesoADatos e) {
			} catch (SystemException e) {
			}
		}
		try {
			if (!UtileriasWeb.esCadenaVacia(idFormaPago)) {
				listaFormaPago = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN
						.getInstancia(idToCotizacion).listarTodos(
								UtileriasWeb.regresaShort(idFormaPago));
			} else {
				listaFormaPago = new ArrayList<FormaPagoIDTO>();
			}
			// listaFormaPago = FormaPagoDN.getInstancia().listarTodos();
			listaMedioPago = MedioPagoDN.getInstancia().listarTodos();
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {
		} catch (Exception e) {
		}
		if (this.agentes == null) {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);
			mx.com.afirme.midas.interfaz.agente.AgenteDN agenteDN = mx.com.afirme.midas.interfaz.agente.AgenteDN
					.getInstancia();
			List<AgenteDTO> agentes;
			try {
				agentes = agenteDN.getAgentesPorUsuario(usuario);
				if (agentes != null && !agentes.isEmpty()) {
					this.agentes = agentes;
				} else {
					this.agentes = new ArrayList<AgenteDTO>();
				}
			} catch (SystemException e) {
				e.printStackTrace();
				this.agentes = new ArrayList<AgenteDTO>();
			}

		}
		return errors;
	}

	public String getIdToProducto() {
		return idToProducto;
	}

	public void setIdToProducto(String idToProducto) {
		this.idToProducto = idToProducto;
	}

	public String getIdMotivoRechazo() {
		return idMotivoRechazo;
	}

	public void setIdMotivoRechazo(String idMotivoRechazo) {
		this.idMotivoRechazo = idMotivoRechazo;
	}

	public List<ResumenIncisoCotizacionForm> getListaIncisos() {
		return listaIncisos;
	}

	public void setListaIncisos(List<ResumenIncisoCotizacionForm> listaIncisos) {
		this.listaIncisos = listaIncisos;
	}

	public String getEditaIncisos() {
		return editaIncisos;
	}

	public void setEditaIncisos(String editaIncisos) {
		this.editaIncisos = editaIncisos;
	}

	public String getEditaPrimerRiesgo() {
		return editaPrimerRiesgo;
	}

	public void setEditaPrimerRiesgo(String editaPrimerRiesgo) {
		this.editaPrimerRiesgo = editaPrimerRiesgo;
	}

	public String getPermiteCambiarPoliza() {
		return permiteCambiarPoliza;
	}

	public void setPermiteCambiarPoliza(String permiteCambiarPoliza) {
		this.permiteCambiarPoliza = permiteCambiarPoliza;
	}

	public String getEditaDatoGeneral() {
		return editaDatoGeneral;
	}

	public void setEditaDatoGeneral(String editaDatoGeneral) {
		this.editaDatoGeneral = editaDatoGeneral;
	}

	public String getClaveAutVigenciaMaxMin() {
		return claveAutVigenciaMaxMin;
	}

	public void setClaveAutVigenciaMaxMin(String claveAutVigenciaMaxMin) {
		this.claveAutVigenciaMaxMin = claveAutVigenciaMaxMin;
	}

	public String getClaveAutRetroacDifer() {
		return claveAutRetroacDifer;
	}

	public void setClaveAutRetroacDifer(String claveAutRetroacDifer) {
		this.claveAutRetroacDifer = claveAutRetroacDifer;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public String getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(String claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public String getClaveMotivo() {
		return claveMotivo;
	}

	public void setClaveMotivo(String claveMotivo) {
		this.claveMotivo = claveMotivo;
	}

	public String getFechaInicioVigenciaEndoso() {
		return fechaInicioVigenciaEndoso;
	}

	public void setFechaInicioVigenciaEndoso(String fechaInicioVigenciaEndoso) {
		this.fechaInicioVigenciaEndoso = fechaInicioVigenciaEndoso;
	}

	public List<MovimientoCotizacionEndosoDTO> getMovimientosGenerales() {
		return movimientosGenerales;
	}

	public void setMovimientosGenerales(
			List<MovimientoCotizacionEndosoDTO> movimientosGenerales) {
		this.movimientosGenerales = movimientosGenerales;
	}

	public List<MovimientoCotizacionEndosoDTO> getMovimientosInciso() {
		return movimientosInciso;
	}

	public void setMovimientosInciso(
			List<MovimientoCotizacionEndosoDTO> movimientosInciso) {
		this.movimientosInciso = movimientosInciso;
	}

	public String getPrimaNetaAnual() {
		return primaNetaAnual;
	}

	public void setPrimaNetaAnual(String primaNetaAnual) {
		this.primaNetaAnual = primaNetaAnual;
	}

	public String getPrimaNetaCotizacion() {
		return primaNetaCotizacion;
	}

	public void setPrimaNetaCotizacion(String primaNetaCotizacion) {
		this.primaNetaCotizacion = primaNetaCotizacion;
	}

	public String getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}

	public void setMontoRecargoPagoFraccionado(
			String montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}

	public String getDerechosPoliza() {
		return derechosPoliza;
	}

	public void setDerechosPoliza(String derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}

	public String getFactorIVA() {
		return factorIVA;
	}

	public void setFactorIVA(String factorIVA) {
		this.factorIVA = factorIVA;
	}

	public String getMontoIVA() {
		return montoIVA;
	}

	public void setMontoIVA(String montoIVA) {
		this.montoIVA = montoIVA;
	}

	public String getPrimaNetaTotal() {
		return primaNetaTotal;
	}

	public void setPrimaNetaTotal(String primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}

	public String getNumeroPolizaFormateada() {
		return numeroPolizaFormateada;
	}

	public void setNumeroPolizaFormateada(String numeroPolizaFormateada) {
		this.numeroPolizaFormateada = numeroPolizaFormateada;
	}

	public List<SoporteResumen> getResumenComisiones() {
		return resumenComisiones;
	}

	public void setResumenComisiones(List<SoporteResumen> resumenComisiones) {
		this.resumenComisiones = resumenComisiones;
	}

	public String getApellidoPaternoSolicitante() {
		return apellidoPaternoSolicitante;
	}

	public void setApellidoPaternoSolicitante(String apellidoPaternoSolicitante) {
		this.apellidoPaternoSolicitante = apellidoPaternoSolicitante;
	}

	public String getApellidoMaternoSolicitante() {
		return apellidoMaternoSolicitante;
	}

	public void setApellidoMaternoSolicitante(String apellidoMaternoSolicitante) {
		this.apellidoMaternoSolicitante = apellidoMaternoSolicitante;
	}

	public void setIdToPoliza(String idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public String getIdToPoliza() {
		return idToPoliza;
	}

	public void setAclaraciones(String aclaraciones) {
		this.aclaraciones = aclaraciones;
	}

	public String getAclaraciones() {
		return aclaraciones;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public String getDiasPorDevengar() {
		return diasPorDevengar;
	}

	public void setDiasPorDevengar(String diasPorDevengar) {
		this.diasPorDevengar = diasPorDevengar;
	}

	public void setDiferenciaCotizacionEndosoDTOs(
			List<DiferenciaCotizacionEndosoDTO> diferenciaCotizacionEndosoDTOs) {
		this.diferenciaCotizacionEndosoDTOs = diferenciaCotizacionEndosoDTOs;
	}

	public List<DiferenciaCotizacionEndosoDTO> getDiferenciaCotizacionEndosoDTOs() {
		return diferenciaCotizacionEndosoDTOs;
	}

	/**
	 * @return the idFormaPagoAnterior
	 */
	public String getIdFormaPagoAnterior() {
		return idFormaPagoAnterior;
	}

	/**
	 * @param idFormaPagoAnterior
	 *            the idFormaPagoAnterior to set
	 */
	public void setIdFormaPagoAnterior(String idFormaPagoAnterior) {
		this.idFormaPagoAnterior = idFormaPagoAnterior;
	}

	/**
	 * @return the deshabilitaCambioFP
	 */
	public String getDeshabilitaCambioFP() {
		return deshabilitaCambioFP;
	}

	/**
	 * @param deshabilitaCambioFP
	 *            the deshabilitaCambioFP to set
	 */
	public void setDeshabilitaCambioFP(String deshabilitaCambioFP) {
		this.deshabilitaCambioFP = deshabilitaCambioFP;
	}

	public List<EndosoCoberturaDTO> getCoberturasAgrupadas() {
		return coberturasAgrupadas;
	}

	public void setCoberturasAgrupadas(
			List<EndosoCoberturaDTO> coberturasAgrupadas) {
		this.coberturasAgrupadas = coberturasAgrupadas;
	}

	public String getTextoLicitacion() {
		return textoLicitacion;
	}

	public void setTextoLicitacion(String textoLicitacion) {
		this.textoLicitacion = textoLicitacion;
	}

	public String getClaveImpresionSumaAsegurada() {
		return claveImpresionSumaAsegurada;
	}

	public void setClaveImpresionSumaAsegurada(
			String claveImpresionSumaAsegurada) {
		this.claveImpresionSumaAsegurada = claveImpresionSumaAsegurada;
	}

	public String getMostrarChkAutRetroDifer() {
		return mostrarChkAutRetroDifer;
	}

	public void setMostrarChkAutRetroDifer(String mostrarChkAutRetroDifer) {
		this.mostrarChkAutRetroDifer = mostrarChkAutRetroDifer;
	}

	public String getMostrarChkAutVigMaxMin() {
		return mostrarChkAutVigMaxMin;
	}

	public void setMostrarChkAutVigMaxMin(String mostrarChkAutVigMaxMin) {
		this.mostrarChkAutVigMaxMin = mostrarChkAutVigMaxMin;
	}

	public String getMostrarChkAutRPF() {
		return mostrarChkAutRPF;
	}

	public void setMostrarChkAutRPF(String mostrarChkAutRPF) {
		this.mostrarChkAutRPF = mostrarChkAutRPF;
	}

	public String getMostrarChkAutDerechos() {
		return mostrarChkAutDerechos;
	}

	public void setMostrarChkAutDerechos(String mostrarChkAutDerechos) {
		this.mostrarChkAutDerechos = mostrarChkAutDerechos;
	}

	public String getMostrarRadioBttnClaveImpresionSA() {
		return mostrarRadioBttnClaveImpresionSA;
	}

	public void setMostrarRadioBttnClaveImpresionSA(
			String mostrarRadioBttnClaveImpresionSA) {
		this.mostrarRadioBttnClaveImpresionSA = mostrarRadioBttnClaveImpresionSA;
	}

	public String getTipoCalculoRPF() {
		return tipoCalculoRPF;
	}

	public void setTipoCalculoRPF(String tipoCalculoRPF) {
		this.tipoCalculoRPF = tipoCalculoRPF;
	}

	public Double getValorRPFEditable() {
		return valorRPFEditable;
	}

	public void setValorRPFEditable(Double valorRPFEditable) {
		this.valorRPFEditable = valorRPFEditable;
	}

	public String getValorRPFSoloLectura() {
		return valorRPFSoloLectura;
	}

	public void setValorRPFSoloLectura(String valorRPFSoloLectura) {
		this.valorRPFSoloLectura = valorRPFSoloLectura;
	}

	public Double getPorcentajeRPFEditable() {
		return porcentajeRPFEditable;
	}

	public void setPorcentajeRPFEditable(Double porcentajeRPFEditable) {
		this.porcentajeRPFEditable = porcentajeRPFEditable;
	}

	public String getPorcentajeRPFSoloLectura() {
		return porcentajeRPFSoloLectura;
	}

	public void setPorcentajeRPFSoloLectura(String porcentajeRPFSoloLectura) {
		this.porcentajeRPFSoloLectura = porcentajeRPFSoloLectura;
	}

	public String getTipoCalculoDerechos() {
		return tipoCalculoDerechos;
	}

	public void setTipoCalculoDerechos(String tipoCalculoDerechos) {
		this.tipoCalculoDerechos = tipoCalculoDerechos;
	}

	public Double getValorDerechos() {
		return valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	public String getClaveAutorizacionRPF() {
		return claveAutorizacionRPF;
	}

	public void setClaveAutorizacionRPF(String claveAutorizacionRPF) {
		this.claveAutorizacionRPF = claveAutorizacionRPF;
	}

	public String getClaveAutorizacionDerechos() {
		return claveAutorizacionDerechos;
	}

	public void setClaveAutorizacionDerechos(String claveAutorizacionDerechos) {
		this.claveAutorizacionDerechos = claveAutorizacionDerechos;
	}

	public String getMostrarEdicionDerechosRPF() {
		return mostrarEdicionDerechosRPF;
	}

	public void setMostrarEdicionDerechosRPF(String mostrarEdicionDerechosRPF) {
		this.mostrarEdicionDerechosRPF = mostrarEdicionDerechosRPF;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public SoporteEndosoCambioCuota getDatosCambioCuota() {
		return datosCambioCuota;
	}

	public void setDatosCambioCuota(SoporteEndosoCambioCuota datosCambioCuota) {
		this.datosCambioCuota = datosCambioCuota;
	}

	public String getCumuloPoliza() {
		return cumuloPoliza;
	}

	public void setCumuloPoliza(String cumuloPoliza) {
		this.cumuloPoliza = cumuloPoliza;
	}

	public String getCumuloInciso() {
		return cumuloInciso;
	}

	public void setCumuloInciso(String cumuloInciso) {
		this.cumuloInciso = cumuloInciso;
	}

	public String getCumuloSubInciso() {
		return cumuloSubInciso;
	}

	public void setCumuloSubInciso(String cumuloSubInciso) {
		this.cumuloSubInciso = cumuloSubInciso;
	}

	public String getMostrarChkAutCliente() {
		return mostrarChkAutCliente;
	}

	public void setMostrarChkAutCliente(String mostrarChkAutCliente) {
		this.mostrarChkAutCliente = mostrarChkAutCliente;
	}

	public String getClaveAutorizacionCliente() {
		return claveAutorizacionCliente;
	}

	public void setClaveAutorizacionCliente(String claveAutorizacionCliente) {
		this.claveAutorizacionCliente = claveAutorizacionCliente;
	}

	public String getEsRenovacion() {
		return esRenovacion;
	}

	public void setEsRenovacion(String esRenovacion) {
		this.esRenovacion = esRenovacion;
	}

	public String getMostrarChkAutDiasGracia() {
		return mostrarChkAutDiasGracia;
	}

	public void setMostrarChkAutDiasGracia(String mostrarChkAutDiasGracia) {
		this.mostrarChkAutDiasGracia = mostrarChkAutDiasGracia;
	}

	public String getClaveAutorizacionDiasGracia() {
		return claveAutorizacionDiasGracia;
	}

	public void setClaveAutorizacionDiasGracia(String claveAutorizacionDiasGracia) {
		this.claveAutorizacionDiasGracia = claveAutorizacionDiasGracia;
	}

	public String getDiasGracia() {
		return diasGracia;
	}

	public void setDiasGracia(String diasGracia) {
		this.diasGracia = diasGracia;
	}

}