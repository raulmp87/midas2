package mx.com.afirme.midas.cotizacion.reaseguro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Jos� Luis Arellano
 * @since 01 Septiembre de 2009
 */
public class ReaseguroDetalleCoberturaCotizacionSN {
	private ReaseguroDetalleCoberturaCotizacionFacadeRemote beanRemoto;

	public ReaseguroDetalleCoberturaCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReaseguroDetalleCoberturaCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public ReaseguroDetalleCoberturaCotizacionDTO agregar(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(reaseguroDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(reaseguroDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ReaseguroDetalleCoberturaCotizacionDTO modificar(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(reaseguroDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ReaseguroDetalleCoberturaCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public ReaseguroDetalleCoberturaCotizacionDTO getPorId(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(reaseguroDetalleCoberturaCotizacionDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ReaseguroDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(reaseguroDetalleCoberturaCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ReaseguroDetalleCoberturaCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ReaseguroDetalleCoberturaCotizacionDTO> obtenerDetalleCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.obtenerDetalleCoberturasCotizacion(idCotizacion, idSubramo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}