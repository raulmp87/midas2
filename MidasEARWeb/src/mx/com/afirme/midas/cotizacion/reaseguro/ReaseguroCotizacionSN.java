package mx.com.afirme.midas.cotizacion.reaseguro;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroCotizacionSN {
	private ReaseguroCotizacionFacadeRemote beanRemoto;

	public ReaseguroCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReaseguroCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto ReaseguroCotizacionFacadeRemote instanciado", Level.FINEST, null);
	}

	public void agregar(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try { beanRemoto.save(reaseguroCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public void borrar(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {beanRemoto.delete(reaseguroCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public ReaseguroCotizacionDTO modificar(ReaseguroCotizacionDTO reaseguroCotizacionDTODTO) throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.update(reaseguroCotizacionDTODTO);
		} catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName()); }
	}

	public List<ReaseguroCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.findAll();
		} catch (Exception e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
	
	public List<ReaseguroCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {	return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}

	public ReaseguroCotizacionDTO getPorId(ReaseguroCotizacionId id) throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.findById(id);
		} catch (Exception e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO); }
	}
	
	public List<ReaseguroCotizacionDTO> listarFiltrado(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {	return beanRemoto.listarFiltrado(reaseguroCotizacionDTO);
		} catch (Exception e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
}
