package mx.com.afirme.midas.cotizacion.reaseguro.inciso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroIncisoDetalleCoberturaCotizacionDN  {
	private static final ReaseguroIncisoDetalleCoberturaCotizacionDN INSTANCIA = new ReaseguroIncisoDetalleCoberturaCotizacionDN();

	public static ReaseguroIncisoDetalleCoberturaCotizacionDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReaseguroIncisoDetalleCoberturaCotizacionSN reaseguroIncisoDetalleCoberturaCotizacionSN = new ReaseguroIncisoDetalleCoberturaCotizacionSN(); 
		return reaseguroIncisoDetalleCoberturaCotizacionSN.listarFiltrado(reaseguroIncisoDetalleCoberturaCotizacionDTO);
	}

	public ReaseguroIncisoDetalleCoberturaCotizacionDTO getPorId(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroIncisoDetalleCoberturaCotizacionSN reaseguroIncisoDetalleCoberturaCotizacionSN = new ReaseguroIncisoDetalleCoberturaCotizacionSN();
		return reaseguroIncisoDetalleCoberturaCotizacionSN.getPorId(reaseguroIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public ReaseguroIncisoDetalleCoberturaCotizacionDTO agregar(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroIncisoDetalleCoberturaCotizacionSN reaseguroIncisoDetalleCoberturaCotizacionSN = new ReaseguroIncisoDetalleCoberturaCotizacionSN();
		return reaseguroIncisoDetalleCoberturaCotizacionSN.agregar(reaseguroIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public ReaseguroIncisoDetalleCoberturaCotizacionDTO modificar(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroIncisoDetalleCoberturaCotizacionSN reaseguroIncisoDetalleCoberturaCotizacionSN = new ReaseguroIncisoDetalleCoberturaCotizacionSN();
		return reaseguroIncisoDetalleCoberturaCotizacionSN.modificar(reaseguroIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public void borrar(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroIncisoDetalleCoberturaCotizacionSN reaseguroIncisoDetalleCoberturaCotizacionSN = new ReaseguroIncisoDetalleCoberturaCotizacionSN();
		reaseguroIncisoDetalleCoberturaCotizacionSN.borrar(reaseguroIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleIncisoCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo, BigDecimal numeroInciso) throws SystemException{
		ReaseguroIncisoDetalleCoberturaCotizacionSN reaseguroIncisoDetalleCoberturaCotizacionSN = new ReaseguroIncisoDetalleCoberturaCotizacionSN();
		
		return reaseguroIncisoDetalleCoberturaCotizacionSN.obtenerDetalleIncisoCoberturasCotizacion(idCotizacion, idSubramo, numeroInciso);
	}
}
