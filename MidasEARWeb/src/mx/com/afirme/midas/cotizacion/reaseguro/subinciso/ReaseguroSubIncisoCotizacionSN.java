package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroSubIncisoCotizacionSN {
	private ReaseguroSubIncisoCotizacionFacadeRemote beanRemoto;

	public ReaseguroSubIncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReaseguroSubIncisoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto ReaseguroSubIncisoCotizacionFacadeRemote instanciado", Level.FINEST, null);
	}

	public void agregar(ReaseguroSubIncisoCotizacionDTO reaseguroSubIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try { beanRemoto.save(reaseguroSubIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public void borrar(ReaseguroSubIncisoCotizacionDTO reaseguroSubIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {beanRemoto.delete(reaseguroSubIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public ReaseguroSubIncisoCotizacionDTO modificar(ReaseguroSubIncisoCotizacionDTO reaseguroSubIncisoCotizacionDTODTO) throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.update(reaseguroSubIncisoCotizacionDTODTO);
		} catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName()); }
	}

	public List<ReaseguroSubIncisoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.findAll();
		} catch (Exception e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
	
	public List<ReaseguroSubIncisoCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {	return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}

	public ReaseguroSubIncisoCotizacionDTO getPorId(ReaseguroSubIncisoCotizacionId id) throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.findById(id);
		} catch (Exception e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO); }
	}
	
	public List<ReaseguroSubIncisoCotizacionDTO> listarFiltrado(ReaseguroSubIncisoCotizacionDTO reaseguroSubIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {	return beanRemoto.listarFiltrado(reaseguroSubIncisoCotizacionDTO);
		} catch (Exception e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
	
	public List<ReaseguroSubIncisoCotizacionDTO> listarReaseguroSubIncisoCotizacion(ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ReaseguroSubIncisoCotizacionDTO entity = new ReaseguroSubIncisoCotizacionDTO();
		ReaseguroSubIncisoCotizacionId key = new ReaseguroSubIncisoCotizacionId();
		key.setIdTcSubramo(reaseguroIncisoCotizacionDTO.getId().getIdTcSubRamo());
		key.setIdToCotizacion(reaseguroIncisoCotizacionDTO.getId().getIdToCotizacion());
		key.setNumeroInciso(reaseguroIncisoCotizacionDTO.getId().getNumeroInciso());
		entity.setId(key);
		return new ReaseguroSubIncisoCotizacionSN().listarFiltrado(entity);
	}
}
