package mx.com.afirme.midas.cotizacion.reaseguro.inciso;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroIncisoCotizacionSN {
	private ReaseguroIncisoCotizacionFacadeRemote beanRemoto;

	public ReaseguroIncisoCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReaseguroIncisoCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto ReaseguroIncisoCotizacionFacadeRemote instanciado", Level.FINEST, null);
	}

	public void agregar(ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try { beanRemoto.save(reaseguroIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public void borrar(ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {beanRemoto.delete(reaseguroIncisoCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());}
	}

	public ReaseguroIncisoCotizacionDTO modificar(ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTODTO) throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.update(reaseguroIncisoCotizacionDTODTO);
		} catch (EJBTransactionRolledbackException e) { throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName()); }
	}

	public List<ReaseguroIncisoCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.findAll();
		} catch (Exception e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
	
	public List<ReaseguroIncisoCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {	return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}

	public ReaseguroIncisoCotizacionDTO getPorId(ReaseguroIncisoCotizacionId id) throws ExcepcionDeAccesoADatos {
		try { return beanRemoto.findById(id);
		} catch (Exception e) { throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO); }
	}
	
	public List<ReaseguroIncisoCotizacionDTO> listarFiltrado(ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {	return beanRemoto.listarFiltrado(reaseguroIncisoCotizacionDTO);
		} catch (Exception e) {throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);}
	}
}
