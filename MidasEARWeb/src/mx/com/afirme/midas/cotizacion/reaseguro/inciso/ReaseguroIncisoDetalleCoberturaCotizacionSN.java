package mx.com.afirme.midas.cotizacion.reaseguro.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Jos� Luis Arellano
 * @since 01 Septiembre de 2009
 */
public class ReaseguroIncisoDetalleCoberturaCotizacionSN {
	private ReaseguroIncisoDetalleCoberturaCotizacionFacadeRemote beanRemoto;

	public ReaseguroIncisoDetalleCoberturaCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReaseguroIncisoDetalleCoberturaCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public ReaseguroIncisoDetalleCoberturaCotizacionDTO agregar(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(reaseguroIncisoDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(reaseguroIncisoDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ReaseguroIncisoDetalleCoberturaCotizacionDTO modificar(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(reaseguroIncisoDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public ReaseguroIncisoDetalleCoberturaCotizacionDTO getPorId(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(reaseguroIncisoDetalleCoberturaCotizacionDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(reaseguroIncisoDetalleCoberturaCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleIncisoCoberturasCotizacion(BigDecimal idToCotizacion, BigDecimal idSubramo, BigDecimal numeroInciso){
		
		return beanRemoto.obtenerDetalleIncisoCoberturasCotizacion(idToCotizacion, idSubramo, numeroInciso);
	}
}