package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReaseguroSubIncisoCotizacionForm extends MidasBaseForm{
	private static final long serialVersionUID = 8033432170707007854L;
	private ReaseguroSubIncisoCotizacionId id;
	private String idToCotizacion;
	private String numeroInciso;
	private String idToSeccion;
	private String numeroSubInciso;
	private String idTcSubRamo;
    private SubRamoDTO subRamoDTO;
    private String valorSumaAsegurada;
    private String valorPrimaNeta;
    private String claveTipoOrigen;
    private String claveEstatus;
    private String editable;

    public ReaseguroSubIncisoCotizacionId getId() {
		return id;
	}
	public void setId(ReaseguroSubIncisoCotizacionId id) {
		this.id = id;
	}
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}
	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}
	public String getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(String valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
	public String getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(String valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	public String getClaveTipoOrigen() {
		return claveTipoOrigen;
	}
	public void setClaveTipoOrigen(String claveTipoOrigen) {
		this.claveTipoOrigen = claveTipoOrigen;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getEditable() {
		return editable;
	}
	public void setEditable(String editable) {
		this.editable = editable;
	}
}
