package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Jos� Luis Arellano
 * @since 01 Septiembre de 2009
 */
public class ReaseguroSubIncisoDetalleCoberturaCotizacionSN {
	private ReaseguroSubIncisoDetalleCoberturaCotizacionFacadeRemote beanRemoto;

	public ReaseguroSubIncisoDetalleCoberturaCotizacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReaseguroSubIncisoDetalleCoberturaCotizacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO agregar(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO modificar(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO getPorId(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor) {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleSubIncisosCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo,
			BigDecimal numeroInciso, BigDecimal  idSeccion, BigDecimal numeroSubinciso) throws SystemException{
		
		try {
			return beanRemoto.obtenerDetalleSubIncisosCoberturasCotizacion(idCotizacion, idSubramo, numeroInciso, idSeccion, numeroSubinciso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}