package mx.com.afirme.midas.cotizacion.reaseguro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroCotizacionDN {
	private static final ReaseguroCotizacionDN INSTANCIA = new ReaseguroCotizacionDN();

	public static ReaseguroCotizacionDN getInstancia() {
		return ReaseguroCotizacionDN.INSTANCIA;
	}

	public List<ReaseguroCotizacionDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroCotizacionSN().listarTodos();
	}

	public void agregar(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new ReaseguroCotizacionSN().agregar(reaseguroCotizacionDTO);
	}

	public ReaseguroCotizacionDTO modificar(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		return new ReaseguroCotizacionSN().modificar(reaseguroCotizacionDTO);
	}

	public ReaseguroCotizacionDTO getPorId(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroCotizacionSN().getPorId(reaseguroCotizacionDTO.getId());
	}
	
	public ReaseguroCotizacionDTO getPorId(ReaseguroCotizacionId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroCotizacionSN().getPorId(id);
	}

	public void borrar(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguroCotizacionSN reaseguroCotizacionSN = new ReaseguroCotizacionSN();
		reaseguroCotizacionSN.borrar(reaseguroCotizacionDTO);
	}

	public List<ReaseguroCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroCotizacionSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<ReaseguroIncisoCotizacionDTO> listarReaseguroIncisoCotizacion(ReaseguroCotizacionDTO reaseguroCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ReaseguroIncisoCotizacionDTO entity = new ReaseguroIncisoCotizacionDTO();
		ReaseguroIncisoCotizacionId key = new ReaseguroIncisoCotizacionId();
		key.setIdTcSubRamo(reaseguroCotizacionDTO.getId().getIdTcSubRamo());
		key.setIdToCotizacion(reaseguroCotizacionDTO.getId().getIdToCotizacion());
		entity.setId(key);
		return new ReaseguroIncisoCotizacionSN().listarFiltrado(entity);
	}
	
	public void actualizaEstatus(BigDecimal idCotizacion, BigDecimal idSubRamo) throws SystemException{
		ReaseguroCotizacionSN reaseguroCotizacionSN = new ReaseguroCotizacionSN();
		ReaseguroCotizacionId id = new ReaseguroCotizacionId();
		id.setIdToCotizacion(idCotizacion);
		id.setIdTcSubRamo(idSubRamo);
		ReaseguroCotizacionDTO reaseguroCotizacionDTO = reaseguroCotizacionSN.getPorId(id);
		if (!UtileriasWeb.esObjetoNulo(reaseguroCotizacionDTO)){
			//TODO definir qu� procede puesto que el campo ya no existe
//			reaseguroCotizacionDTO.setClaveEstatus(new Short("1"));
			reaseguroCotizacionSN.modificar(reaseguroCotizacionDTO);
		}
		
	}
}
