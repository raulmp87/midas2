package mx.com.afirme.midas.cotizacion.reaseguro.inciso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroIncisoCotizacionDN {
	private static final ReaseguroIncisoCotizacionDN INSTANCIA = new ReaseguroIncisoCotizacionDN();

	public static ReaseguroIncisoCotizacionDN getInstancia() {
		return ReaseguroIncisoCotizacionDN.INSTANCIA;
	}

	public List<ReaseguroIncisoCotizacionDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroIncisoCotizacionSN().listarTodos();
	}

	public void agregar(ReaseguroIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new ReaseguroIncisoCotizacionSN().agregar(reaseguroCotizacionDTO);
	}

	public ReaseguroIncisoCotizacionDTO modificar(ReaseguroIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		return new ReaseguroIncisoCotizacionSN().modificar(reaseguroCotizacionDTO);
	}

	public ReaseguroIncisoCotizacionDTO getPorId(ReaseguroIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroIncisoCotizacionSN().getPorId(reaseguroCotizacionDTO.getId());
	}
	
	public ReaseguroIncisoCotizacionDTO getPorId(ReaseguroIncisoCotizacionId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroIncisoCotizacionSN().getPorId(id);
	}

	public void borrar(ReaseguroIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguroIncisoCotizacionSN reaseguroCotizacionSN = new ReaseguroIncisoCotizacionSN();
		reaseguroCotizacionSN.borrar(reaseguroCotizacionDTO);
	}

	public List<ReaseguroIncisoCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroIncisoCotizacionSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<ReaseguroSubIncisoCotizacionDTO> listarReaseguroSubIncisoCotizacion(ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ReaseguroSubIncisoCotizacionDTO entity = new ReaseguroSubIncisoCotizacionDTO();
		ReaseguroSubIncisoCotizacionId key = new ReaseguroSubIncisoCotizacionId();
		key.setIdTcSubramo(reaseguroIncisoCotizacionDTO.getId().getIdTcSubRamo());
		key.setIdToCotizacion(reaseguroIncisoCotizacionDTO.getId().getIdToCotizacion());
		key.setNumeroInciso(reaseguroIncisoCotizacionDTO.getId().getNumeroInciso());
		entity.setId(key);
		return new ReaseguroSubIncisoCotizacionSN().listarFiltrado(entity);
	}
	
	public void actualizaEstatus(BigDecimal idCotizacion, BigDecimal idSubRamo, BigDecimal numeroInciso) throws SystemException{
		ReaseguroIncisoCotizacionSN reaseguroIncisoCotizacionSN = new ReaseguroIncisoCotizacionSN();
		ReaseguroIncisoCotizacionId id = new ReaseguroIncisoCotizacionId();
		id.setIdToCotizacion(idCotizacion);
		id.setIdTcSubRamo(idSubRamo);
		id.setNumeroInciso(numeroInciso);
		ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO = reaseguroIncisoCotizacionSN.getPorId(id);
		if (!UtileriasWeb.esObjetoNulo(reaseguroIncisoCotizacionDTO)){
			reaseguroIncisoCotizacionDTO.setClaveEstatus(new Short("1"));
			reaseguroIncisoCotizacionSN.modificar(reaseguroIncisoCotizacionDTO);
		}
		
	}
}
