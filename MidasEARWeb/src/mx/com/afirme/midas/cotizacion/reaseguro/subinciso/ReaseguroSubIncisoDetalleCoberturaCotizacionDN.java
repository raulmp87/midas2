package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroSubIncisoDetalleCoberturaCotizacionDN  {
	private static final ReaseguroSubIncisoDetalleCoberturaCotizacionDN INSTANCIA = new ReaseguroSubIncisoDetalleCoberturaCotizacionDN();

	public static ReaseguroSubIncisoDetalleCoberturaCotizacionDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReaseguroSubIncisoDetalleCoberturaCotizacionSN reaseguroSubIncisoDetalleCoberturaCotizacionSN = new ReaseguroSubIncisoDetalleCoberturaCotizacionSN(); 
		return reaseguroSubIncisoDetalleCoberturaCotizacionSN.listarFiltrado(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	}

	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO getPorId(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroSubIncisoDetalleCoberturaCotizacionSN reaseguroSubIncisoDetalleCoberturaCotizacionSN = new ReaseguroSubIncisoDetalleCoberturaCotizacionSN();
		return reaseguroSubIncisoDetalleCoberturaCotizacionSN.getPorId(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO agregar(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroSubIncisoDetalleCoberturaCotizacionSN reaseguroSubIncisoDetalleCoberturaCotizacionSN = new ReaseguroSubIncisoDetalleCoberturaCotizacionSN();
		return reaseguroSubIncisoDetalleCoberturaCotizacionSN.agregar(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public ReaseguroSubIncisoDetalleCoberturaCotizacionDTO modificar(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroSubIncisoDetalleCoberturaCotizacionSN reaseguroSubIncisoDetalleCoberturaCotizacionSN = new ReaseguroSubIncisoDetalleCoberturaCotizacionSN();
		return reaseguroSubIncisoDetalleCoberturaCotizacionSN.modificar(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public void borrar(ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroSubIncisoDetalleCoberturaCotizacionSN reaseguroSubIncisoDetalleCoberturaCotizacionSN = new ReaseguroSubIncisoDetalleCoberturaCotizacionSN();
		reaseguroSubIncisoDetalleCoberturaCotizacionSN.borrar(reaseguroSubIncisoDetalleCoberturaCotizacionDTO);
	}
	
	public List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> obtenerDetalleSubIncisosCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo,
			BigDecimal numeroInciso, BigDecimal  idSeccion, BigDecimal numeroSubinciso) throws SystemException{
		ReaseguroSubIncisoDetalleCoberturaCotizacionSN reaseguroSubIncisoDetalleCoberturaCotizacionSN = new ReaseguroSubIncisoDetalleCoberturaCotizacionSN();
		
		return reaseguroSubIncisoDetalleCoberturaCotizacionSN.obtenerDetalleSubIncisosCoberturasCotizacion(idCotizacion, idSubramo, numeroInciso, idSeccion, numeroSubinciso);
	}
}
