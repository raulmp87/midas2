package mx.com.afirme.midas.cotizacion.reaseguro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroDetalleCoberturaCotizacionDN  {
	private static final ReaseguroDetalleCoberturaCotizacionDN INSTANCIA = new ReaseguroDetalleCoberturaCotizacionDN();

	public static ReaseguroDetalleCoberturaCotizacionDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReaseguroDetalleCoberturaCotizacionDTO> listarFiltrado(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReaseguroDetalleCoberturaCotizacionSN reaseguroDetalleCoberturaCotizacionSN = new ReaseguroDetalleCoberturaCotizacionSN();
		return reaseguroDetalleCoberturaCotizacionSN.listarFiltrado(reaseguroDetalleCoberturaCotizacionDTO);
	}

	public ReaseguroDetalleCoberturaCotizacionDTO getPorId(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroDetalleCoberturaCotizacionSN reaseguroDetalleCoberturaCotizacionSN = new ReaseguroDetalleCoberturaCotizacionSN();
		return reaseguroDetalleCoberturaCotizacionSN.getPorId(reaseguroDetalleCoberturaCotizacionDTO);
	}
	
	public ReaseguroDetalleCoberturaCotizacionDTO agregar(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroDetalleCoberturaCotizacionSN reaseguroDetalleCoberturaCotizacionSN = new ReaseguroDetalleCoberturaCotizacionSN();
		return reaseguroDetalleCoberturaCotizacionSN.agregar(reaseguroDetalleCoberturaCotizacionDTO);
	}
	
	public ReaseguroDetalleCoberturaCotizacionDTO modificar(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroDetalleCoberturaCotizacionSN reaseguroDetalleCoberturaCotizacionSN = new ReaseguroDetalleCoberturaCotizacionSN();
		return reaseguroDetalleCoberturaCotizacionSN.modificar(reaseguroDetalleCoberturaCotizacionDTO);
	}
	
	public void borrar(ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO) throws SystemException{
		ReaseguroDetalleCoberturaCotizacionSN reaseguroDetalleCoberturaCotizacionSN = new ReaseguroDetalleCoberturaCotizacionSN();
		reaseguroDetalleCoberturaCotizacionSN.borrar(reaseguroDetalleCoberturaCotizacionDTO);
	}
	
	public List<ReaseguroDetalleCoberturaCotizacionDTO> obtenerDetalleCoberturasCotizacion(BigDecimal idCotizacion, BigDecimal idSubramo) throws SystemException{
		ReaseguroDetalleCoberturaCotizacionSN reaseguroDetalleCoberturaCotizacionSN = new ReaseguroDetalleCoberturaCotizacionSN();
		return reaseguroDetalleCoberturaCotizacionSN.obtenerDetalleCoberturasCotizacion(idCotizacion, idSubramo);
	}
}
