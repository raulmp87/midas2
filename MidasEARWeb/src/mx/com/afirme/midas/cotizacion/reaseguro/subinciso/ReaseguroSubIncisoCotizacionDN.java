package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguroSubIncisoCotizacionDN {
	private static final ReaseguroSubIncisoCotizacionDN INSTANCIA = new ReaseguroSubIncisoCotizacionDN();

	public static ReaseguroSubIncisoCotizacionDN getInstancia() {
		return ReaseguroSubIncisoCotizacionDN.INSTANCIA;
	}

	public List<ReaseguroSubIncisoCotizacionDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroSubIncisoCotizacionSN().listarTodos();
	}

	public void agregar(ReaseguroSubIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		new ReaseguroSubIncisoCotizacionSN().agregar(reaseguroCotizacionDTO);
	}

	public ReaseguroSubIncisoCotizacionDTO modificar(ReaseguroSubIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		return new ReaseguroSubIncisoCotizacionSN().modificar(reaseguroCotizacionDTO);
	}

	public ReaseguroSubIncisoCotizacionDTO getPorId(ReaseguroSubIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroSubIncisoCotizacionSN().getPorId(reaseguroCotizacionDTO.getId());
	}
	
	public ReaseguroSubIncisoCotizacionDTO getPorId(ReaseguroSubIncisoCotizacionId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroSubIncisoCotizacionSN().getPorId(id);
	}

	public void borrar(ReaseguroSubIncisoCotizacionDTO reaseguroCotizacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguroSubIncisoCotizacionSN reaseguroCotizacionSN = new ReaseguroSubIncisoCotizacionSN();
		reaseguroCotizacionSN.borrar(reaseguroCotizacionDTO);
	}

	public List<ReaseguroSubIncisoCotizacionDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new ReaseguroSubIncisoCotizacionSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public void actualizaEstatus(BigDecimal idCotizacion, BigDecimal idSubRamo, BigDecimal numeroInciso, BigDecimal idSeccion, BigDecimal numeroSubInciso) throws SystemException{
		ReaseguroSubIncisoCotizacionSN reaseguroSubIncisoCotizacionSN = new ReaseguroSubIncisoCotizacionSN();
		ReaseguroSubIncisoCotizacionId id = new ReaseguroSubIncisoCotizacionId();
		id.setIdToCotizacion(idCotizacion);
		id.setIdTcSubramo(idSubRamo);
		id.setNumeroInciso(numeroInciso);
		id.setIdToSeccion(idSeccion);
		id.setNumeroSubinciso(numeroSubInciso);
		
		ReaseguroSubIncisoCotizacionDTO reaseguroSubIncisoCotizacionDTO = reaseguroSubIncisoCotizacionSN.getPorId(id);
		if (!UtileriasWeb.esObjetoNulo(reaseguroSubIncisoCotizacionDTO)){
			reaseguroSubIncisoCotizacionDTO.setClaveEstatus(new Short("1"));
			reaseguroSubIncisoCotizacionSN.modificar(reaseguroSubIncisoCotizacionDTO);
		}
		
	}
}
