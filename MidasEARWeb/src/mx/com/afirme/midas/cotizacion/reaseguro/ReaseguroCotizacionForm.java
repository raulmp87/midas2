package mx.com.afirme.midas.cotizacion.reaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReaseguroCotizacionForm extends MidasBaseForm{
	private static final long serialVersionUID = 316699064641890824L;
    private String idToCotizacion;
    private String idTcSubRamo;
    private String valorSumaAsegurada;
    private String valorPrimaNeta;
    private String claveTipoOrigen;
    private String claveEstatus;
    private List<ReaseguroIncisoCotizacionForm> listaReaseguroIncisos=new ArrayList<ReaseguroIncisoCotizacionForm>();
    
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(String valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
	public String getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(String valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	public String getClaveTipoOrigen() {
		return claveTipoOrigen;
	}
	public void setClaveTipoOrigen(String claveTipoOrigen) {
		this.claveTipoOrigen = claveTipoOrigen;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public List<ReaseguroIncisoCotizacionForm> getListaReaseguroIncisos() {
		return listaReaseguroIncisos;
	}
	public void setListaReaseguroIncisos(List<ReaseguroIncisoCotizacionForm> listaReaseguroIncisos) {
		this.listaReaseguroIncisos = listaReaseguroIncisos;
	}
}
