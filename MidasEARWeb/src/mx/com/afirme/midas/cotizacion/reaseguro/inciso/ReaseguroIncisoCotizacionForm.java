package mx.com.afirme.midas.cotizacion.reaseguro.inciso;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReaseguroIncisoCotizacionForm extends MidasBaseForm{
	private static final long serialVersionUID = 5564841179343688113L;
	private String idToCotizacion;
	private String numeroInciso;
	private String idTcSubRamo;
    private IncisoCotizacionDTO incisoCotizacionDTO;
    private String valorSumaAsegurada;
    private String valorPrimaNeta;
    private String claveTipoOrigen;
    private String claveEstatus;
    private String permisoConsulta;
    private String editable;
    private List<ReaseguroSubIncisoCotizacionForm> listaReaseguroSubIncisos=new ArrayList<ReaseguroSubIncisoCotizacionForm>();
	
    public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public IncisoCotizacionDTO getIncisoCotizacionDTO() {
		return incisoCotizacionDTO;
	}
	public void setIncisoCotizacionDTO(IncisoCotizacionDTO incisoCotizacionDTO) {
		this.incisoCotizacionDTO = incisoCotizacionDTO;
	}
	public String getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(String valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
	public String getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(String valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	public String getClaveTipoOrigen() {
		return claveTipoOrigen;
	}
	public void setClaveTipoOrigen(String claveTipoOrigen) {
		this.claveTipoOrigen = claveTipoOrigen;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getPermisoConsulta() {
		return permisoConsulta;
	}
	public void setPermisoConsulta(String permisoConsulta) {
		this.permisoConsulta = permisoConsulta;
	}
	public List<ReaseguroSubIncisoCotizacionForm> getListaReaseguroSubIncisos() {
		return listaReaseguroSubIncisos;
	}
	public void setListaReaseguroSubIncisos(List<ReaseguroSubIncisoCotizacionForm> listaReaseguroSubIncisos) {
		this.listaReaseguroSubIncisos = listaReaseguroSubIncisos;
	}
	public String getEditable() {
		return editable;
	}
	public void setEditable(String editable) {
		this.editable = editable;
	}
}
