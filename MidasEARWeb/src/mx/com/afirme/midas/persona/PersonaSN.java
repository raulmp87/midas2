package mx.com.afirme.midas.persona;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * 
 * @author Jos� Luis Arellano
 * @since 07 Septiembre de 2009
 */
public class PersonaSN {
	private PersonaFacadeRemote beanRemoto;

	public PersonaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PersonaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public PersonaDTO agregar(PersonaDTO personaDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(personaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(PersonaDTO personaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(personaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(PersonaDTO personaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(personaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<PersonaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public PersonaDTO getPorId(BigDecimal idToPersona) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToPersona);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Encuentra un registro de PersonaDTO especificado por un id de CotizacionDTO.
	 * Debido a que CotizacionDTO contiene dos referencias a PersonaDTO, el m�todo recibe un entero indicando qu� persona
	 * se desea obtener, en base al siguiente orden: 
	 * 1 - Persona contratante.
	 * 2 - Persona asegurada.    
	 * @param int personaOrden
	  @param idToCotizacion el id del registro idToCotizacion
	  	  @return PersonaDTO found by query
	 */
	public PersonaDTO getPorIdCotizacion(BigDecimal idToCotizacion,int personaOrden) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByIdCotizacion(idToCotizacion, personaOrden);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Encuentra un registro de PersonaDTO especificado por un id de ProveedorInspeccionDTO.    
	  @param idToProveedorInspeccion el id del registro ProveedorInspeccionDTO
	 *@return PersonaDTO found by query
	 */
	public PersonaDTO getPorIdProveedorInspeccion(BigDecimal idToProveedorInspeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByIdProveedorInspeccion(idToProveedorInspeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
