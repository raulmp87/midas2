package mx.com.afirme.midas.persona;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.SystemException;

public class PersonaDN {
	public static final PersonaDN INSTANCIA = new PersonaDN();

	public static PersonaDN getInstancia (){
		return PersonaDN.INSTANCIA;
	}
	public PersonaDTO agregar(PersonaDTO personaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		return new PersonaSN().agregar(personaDTO);
	}

	public void borrar(PersonaDTO personaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		new PersonaSN().borrar(personaDTO);
	}

	public void modificar(PersonaDTO personaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		new PersonaSN().modificar(personaDTO);
	}

	public List<PersonaDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException {
		return new PersonaSN().listarTodos();
	}
	
	public PersonaDTO getPorId(BigDecimal idToPersona) throws ExcepcionDeAccesoADatos, SystemException{
		return new PersonaSN().getPorId(idToPersona);
	}
}
