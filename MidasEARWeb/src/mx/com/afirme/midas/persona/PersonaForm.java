package mx.com.afirme.midas.persona;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class PersonaForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7925761200806479155L;
	private String idToPersona;
    private String claveTipoPersona="1";
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String fechaNacimiento;
    private String codigoRFC;
    private String idPadre;
    private String codigoPersona; //"1"-Persona contratante. "3"-Persona asegurado. Para el caso de Cotizacion
    private String descripcionPadre; //String para indicar a qu� tipo de entidad pertenece la persona.
    private String telefono;
    private String email;
    private String descripcionTipoPersona="1";
    private String esNuevoRegistro="1";
    private String numeroAsegurado;
	public String getIdToPersona() {
		return idToPersona;
	}
	public void setIdToPersona(String idToPersona) {
		this.idToPersona = idToPersona;
	}
	public String getClaveTipoPersona() {
		return claveTipoPersona;
	}
	public void setClaveTipoPersona(String claveTipoPersona) {
		this.claveTipoPersona = claveTipoPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCodigoRFC() {
		return codigoRFC;
	}
	public void setCodigoRFC(String codigoRFC) {
		this.codigoRFC = codigoRFC;
	}
	public String getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}
	public String getCodigoPersona() {
		return codigoPersona;
	}
	public void setCodigoPersona(String codigoPersona) {
		this.codigoPersona = codigoPersona;
	}
	public String getDescripcionPadre() {
		return descripcionPadre;
	}
	public void setDescripcionPadre(String descripcionPadre) {
		this.descripcionPadre = descripcionPadre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDescripcionTipoPersona() {
		return descripcionTipoPersona;
	}
	public void setDescripcionTipoPersona(String descripcionTipoPersona) {
		this.descripcionTipoPersona = descripcionTipoPersona;
	}
	public String getEsNuevoRegistro() {
		return esNuevoRegistro;
	}
	public void setEsNuevoRegistro(String esNuevoRegistro) {
		this.esNuevoRegistro = esNuevoRegistro;
	}
	public String getNumeroAsegurado() {
		return numeroAsegurado;
	}
	public void setNumeroAsegurado(String numeroAsegurado) {
		this.numeroAsegurado = numeroAsegurado;
	}
}
