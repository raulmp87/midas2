package mx.com.afirme.midas.usuario;

import java.io.IOException;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class LoginUsuarioAction extends MidasMappingDispatchAction{
	
	private static final Logger LOG = Logger.getLogger(LoginUsuarioAction.class);
	private final String rolGeneraLigas = "Rol_M2_Cliente_Liga_Agente";
	
	private SistemaContext getSistemaContext(HttpServletRequest request) {
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());		
		return webApplicationContext.getBean(SistemaContext.class);
	}
	
	public ActionForward logout(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		SistemaContext sistemaContext = getSistemaContext(request);
		
		String reglaNavegacion = sistemaContext.getExitoso();
		HttpSession session = request.getSession();
		Usuario usuario = null;
		String errorURL = null;
		boolean expirarSesion = true;
		
		if (!sistemaContext.getSeguridadActivada() || !sistemaContext.getAsmActivo()) {
			//Si no esta activada la seguridad, se va a la pagina de inicio alterna
			if(!sistemaContext.getAsmActivo()){
				if ((Boolean) session.getAttribute(sistemaContext.getUsuarioAccesoPortal())) {
						reglaNavegacion = sistemaContext.getAlternoPortal();
					} else {
						reglaNavegacion = sistemaContext.getExitoso();
					}
			} else {
				reglaNavegacion = sistemaContext.getAlterno();		
			}
			request.getSession().invalidate();			
		}else if ((Boolean) session.getAttribute(sistemaContext.getUsuarioAccesoPortal())) {
				reglaNavegacion = sistemaContext.getAlternoPortal();
				request.getSession().invalidate();
			} else {
				//Se revisa si se cuenta con sesion local
				usuario = (Usuario)session.getAttribute(sistemaContext.getUsuarioAccesoMidas());
				if(usuario != null){				
					//Se envia a ASM para que se registre
					errorURL = sistemaContext.getAsmLogin() + "?clientApplicationId=" + sistemaContext.getMidasAppId() + "&clientUrl=" + sistemaContext.getMidasLogin();
					if(usuario.getIdSesionUsuario() != null && usuario.getNombreUsuario()!= null){
						try {														
							if(validaPermiso(usuario, rolGeneraLigas)) {
								expirarSesion = false;
							} else {
								UsuarioWSDN.getInstancia().LogOutUsuario(usuario.getNombreUsuario(), usuario.getIdSesionUsuario());
								LogDeMidasWeb.log("Logout usuario: " + usuario.getNombreUsuario() , Level.INFO, null);
							}
							
						} catch (ExcepcionDeAccesoADatos e) {
							e.printStackTrace();
						} catch (SystemException e) {
							e.printStackTrace();
						}
					} 
		    	}
				if(expirarSesion) {
					request.getSession().invalidate();
					LogDeMidasWeb.log("Expira sesión usuario: " + usuario.getNombreUsuario() , Level.INFO, null);
				}				    	
		    	response.sendRedirect(errorURL);
		    }
		return mapping.findForward(reglaNavegacion);
	}
	
	public void validarUsuario(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)  {
		try {
			boolean acceso = false;
			ValidarUsuarioForm validarUsuarioForm = (ValidarUsuarioForm) form;
			
			String nombreusuario = validarUsuarioForm.getUsuario();
			String password = validarUsuarioForm.getPassword();
			ValidarUsuarioDN validaUsuarioDN = null;
			SistemaContext sistemaContext = getSistemaContext(request);
			
			//Si por alguna razon ya existia la variable de sesion del usuario, la elimina
			if (request.getSession().getAttribute(sistemaContext.getUsuarioAccesoMidas()) != null) {
					request.getSession().removeAttribute(
							sistemaContext.getUsuarioAccesoMidas());
			}
			
			//Se valida el usuario (que exista y que la contrase�a sea correcta)
			try {
				
		
				validaUsuarioDN = new ValidarUsuarioDN();
				acceso = validaUsuarioDN.validarUsuario(nombreusuario, password);
				
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Excepcion en ValidarUsuarioAction" , Level.SEVERE, null);
		
			}	
			
			if (acceso == true) {

				 Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(nombreusuario.trim());
				 if (usuario != null) {
					//Se crea una nueva sesi�n con el objeto usuario recibido
					request.getSession().setAttribute(sistemaContext.getUsuarioAccesoMidas(), usuario);
					response.sendRedirect(sistemaContext.getUrlInicio());
				 }
				
				
			} else {
				response.sendRedirect(sistemaContext.getAccesoDenegado());
			}
			
		} catch (Exception ex) {
			LogDeMidasWeb.log("Fallo el metodo de validar usuario de MIDAS", Level.INFO, ex);
		}
	
	}
	
	
	public void cambiarPassword(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String nombreusuario = request.getParameter("nombreusuario");
		String password = request.getParameter("password");
		String nuevopassword = request.getParameter("nuevopassword");
		String renuevopassword = request.getParameter("renuevopassword");
		boolean resultado = false;
		boolean acceso = false;
		String claveError = "3"; //Hubo un error al actualizar
		ValidarUsuarioDN validaUsuarioDN = null;
		SistemaContext sistemaContext = getSistemaContext(request);
		
		//Se valida la contrase�a nueva
		if (nuevopassword.equals(renuevopassword)) {
			
			//Se valida el usuario (que exista y que la contrase�a sea correcta)
			try {
				validaUsuarioDN = new ValidarUsuarioDN();
				acceso = validaUsuarioDN.validarUsuario(nombreusuario, password);
				if (acceso) {
					resultado = validaUsuarioDN.actualizarPasswordUsuario(nombreusuario, nuevopassword);
				} else {
					claveError = "1"; //El usuario no es valido
				}
				
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Excepcion en ValidarUsuarioAction" , Level.SEVERE, null);
				claveError = "3"; //Hubo un error al actualizar
			}	
		} else {
			claveError = "2"; //Las nuevas contrase�as no son iguales
		}
		
		//Si la contrase�a se actualizo correctamente				
		if (resultado == true) {
			try {
				response.sendRedirect(sistemaContext.getUrlError()); //Esto no es un error. Redirecciona al login
			} catch (IOException e) {
				//e.printStackTrace();
			}
			
		} else {
			try {
				response.sendRedirect(sistemaContext.getUrlCambioPassword() + "?mensaje=" + claveError);
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		
	}
	
	private boolean validaPermiso(Usuario usuario, String permiso) {
		for (Rol rol : usuario.getRoles()) {
			if (rol.getDescripcion().equals(permiso)) {
				return true;
			}
		}
		return false;
	}
	
	
}
