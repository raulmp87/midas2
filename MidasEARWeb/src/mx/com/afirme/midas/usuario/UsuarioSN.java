package mx.com.afirme.midas.usuario;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class UsuarioSN {
	private UsuarioFacadeRemote beanRemoto;

	public UsuarioSN() throws SystemException {
		try {
			LogDeMidasWeb.log("Entrando en UsuarioSN - Constructor",
					Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(UsuarioFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void guardar(UsuarioDTO usuarioDTO) {
		LogDeMidasWeb.log("Ejecutando guardar", Level.FINEST, null);
		beanRemoto.guardar(usuarioDTO);
		
	}
	
	public UsuarioDTO actualizar(UsuarioDTO usuarioDTO) {
		LogDeMidasWeb.log("Ejecutando actualizar", Level.FINEST, null);
		return beanRemoto.actualizar(usuarioDTO);
	}
	
	public void borrar(UsuarioDTO usuarioDTO) {
		LogDeMidasWeb.log("Ejecutando borrar", Level.FINEST, null);
		beanRemoto.borrar(usuarioDTO);
	}
	
	public List<UsuarioDTO> listarTodos() {
		LogDeMidasWeb.log("Ejecutando listarTodos ", Level.FINEST, null);
		return beanRemoto.buscaTodos(0);
		
	}
	
	public List<UsuarioDTO> buscarPorPropiedad(String propiedad, Object valor) {
		LogDeMidasWeb.log("Ejecutando buscarPorPropiedad ", Level.FINEST, null);
		return beanRemoto.buscarPorPropiedad(propiedad, valor, 0);
	}
	
	
}
