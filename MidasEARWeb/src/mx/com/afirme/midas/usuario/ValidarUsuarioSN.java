package mx.com.afirme.midas.usuario;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.usuario.UsuarioFacadeRemote;


/**
 * @author Enrique Aldana
 * 
 */
public class ValidarUsuarioSN {
	private UsuarioFacadeRemote beanRemoto;

	public ValidarUsuarioSN() throws ExcepcionDeAccesoADatos {
		try {
			LogDeMidasWeb.log("Entrando en RegistroDeUsuarioSN - Constructor",
					Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(UsuarioFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		} catch (SystemException es) {
			throw new ExcepcionDeAccesoADatos(es.getMessage());
		}
	}

	public UsuarioDTO validarUsuarioSN(UsuarioDTO usuarioDTO) {
		LogDeMidasWeb.log("Ejecutando validarUsuarioSN", Level.FINEST, null);
		UsuarioDTO usuarioDTORespuesta;
		usuarioDTORespuesta = beanRemoto.validarUsuario(usuarioDTO);
		return usuarioDTORespuesta;
	}
	
	public UsuarioDTO actualizarUsuario(UsuarioDTO usuarioDTO) {
		LogDeMidasWeb.log("Ejecutando actualizarUsuario", Level.FINEST, null);
		UsuarioDTO usuarioDTORespuesta;
		usuarioDTORespuesta = beanRemoto.actualizar(usuarioDTO);
		return usuarioDTORespuesta;
	}
	
	public UsuarioDTO buscarPorNombreUsuario(String nombre) {
		LogDeMidasWeb.log("Ejecutando buscarPorNombreUsuario " + nombre, Level.FINEST, null);
		List<UsuarioDTO> usuarioDTORespuestaList = null;
		usuarioDTORespuestaList = beanRemoto.buscarPorPropiedad("usuarionombre", nombre, 0);
		
		if (usuarioDTORespuestaList != null && usuarioDTORespuestaList.size() > 0) {
			return usuarioDTORespuestaList.get(0);
		}
		
		return null;
	}
	
	
	
	
	
}
