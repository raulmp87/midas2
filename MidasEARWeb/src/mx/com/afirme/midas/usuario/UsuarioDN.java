package mx.com.afirme.midas.usuario;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class UsuarioDN {

private static final UsuarioDN INSTANCIA = new UsuarioDN();
	
	public static UsuarioDN getInstancia() {
		return INSTANCIA;
	}
	
	public void guardar(UsuarioDTO usuarioDTO) {
		UsuarioSN usuarioSN;
		try {
			usuarioSN = new UsuarioSN();
			usuarioSN.guardar(usuarioDTO);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public UsuarioDTO actualizar(UsuarioDTO usuarioDTO) {
		UsuarioSN usuarioSN;
		try {
			usuarioSN = new UsuarioSN();
			return usuarioSN.actualizar(usuarioDTO);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void borrar(UsuarioDTO usuarioDTO) {
		UsuarioSN usuarioSN;
		try {
			usuarioSN = new UsuarioSN();
			usuarioSN.borrar(usuarioDTO);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public List<UsuarioDTO> listarTodos() {
		UsuarioSN usuarioSN;
		try {
			usuarioSN = new UsuarioSN();
			return usuarioSN.listarTodos();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<UsuarioDTO> buscarPorPropiedad(String propiedad, Object valor) {
		UsuarioSN usuarioSN;
		try {
			usuarioSN = new UsuarioSN();
			return usuarioSN.buscarPorPropiedad(propiedad, valor);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public UsuarioDTO buscarPorId(Long id) {
		UsuarioSN usuarioSN;
		try {
			usuarioSN = new UsuarioSN();
			List<UsuarioDTO> usuarioList = usuarioSN.buscarPorPropiedad("usuarioid", id);
			if (usuarioList != null && usuarioList.size() > 0) {
				return usuarioList.get(0);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
		
	}
}
