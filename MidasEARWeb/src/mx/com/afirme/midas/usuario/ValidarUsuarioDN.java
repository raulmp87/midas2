/**
 * 
 */
package mx.com.afirme.midas.usuario;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.usuario.ValidarUsuarioSN;
import mx.com.afirme.midas.usuario.UsuarioDTO;

/**
 * @author jesus.aldana
 * 
 */
public class ValidarUsuarioDN {
	public ValidarUsuarioDN() throws ExcepcionDeAccesoADatos {
	}

	public boolean validarUsuario(String nombreUsuario, String password)
			throws ExcepcionDeAccesoADatos {
		LogDeMidasWeb.log("Ejecutando validarUsuario", Level.FINEST, null);
		
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		UsuarioDTO usuarioDTORespuesta = null;
		
		try {
		        ValidarUsuarioSN registroDeUsuarioSN = new ValidarUsuarioSN();
		
					if (nombreUsuario != null && password != null && !nombreUsuario.trim().equals("") && !password.trim().equals("")) {
						//Codifica la contraseņa
						password = UtileriasWeb.codificaCadena(password);
						usuarioDTO.setUsuarionombre(nombreUsuario.toUpperCase());
						usuarioDTO.setUsuariocontrasena(password);
						
						usuarioDTORespuesta = registroDeUsuarioSN.validarUsuarioSN(usuarioDTO);
						if (usuarioDTORespuesta != null){ //Si el usuario es valido
							return true;
						}
					}
		} catch (ExcepcionDeAccesoADatos ex) {
			LogDeMidasWeb.log("Ejecutando validarUsuario en ValidarUsuarioDN , excepcion de accceso a datos", Level.SEVERE, null);
			
			throw ex;
		}
		return false; 
	}
	
	public boolean actualizarPasswordUsuario(String nombreUsuario, String nuevoPassword)
			throws ExcepcionDeAccesoADatos {
		
		LogDeMidasWeb.log("Ejecutando actualizarPasswordUsuario", Level.FINEST, null);
		
		UsuarioDTO usuarioDTO = null;
		UsuarioDTO usuarioDTORespuesta = null;
		
		ValidarUsuarioSN registroDeUsuarioSN = new ValidarUsuarioSN();
		if (nombreUsuario != null && nuevoPassword != null && !nombreUsuario.trim().equals("") && !nuevoPassword.trim().equals("")) {
			
			//Obtiene el usuario original
			usuarioDTO = registroDeUsuarioSN.buscarPorNombreUsuario(nombreUsuario.toUpperCase());
			if (usuarioDTO != null) {
				//Codifica la contraseņa
				nuevoPassword = UtileriasWeb.codificaCadena(nuevoPassword);
				usuarioDTO.setUsuariocontrasena(nuevoPassword);
				
				usuarioDTORespuesta = registroDeUsuarioSN.actualizarUsuario(usuarioDTO);
				if (usuarioDTORespuesta != null){ //Si la contraseņa del usuario se actualizo correctamente
					return true;
				}
			}
		}
		
		return false; 
	}
	
}
