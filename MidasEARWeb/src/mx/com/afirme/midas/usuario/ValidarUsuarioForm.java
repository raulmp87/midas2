package mx.com.afirme.midas.usuario;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ValidarUsuarioForm  extends MidasBaseForm {

	
	private static final long serialVersionUID = 9053711230183340914L;
	
	private String usuario;
	private String password;
	private String nuevopassword;
	private String renuevopassword;
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the nuevopassword
	 */
	public String getNuevopassword() {
		return nuevopassword;
	}
	/**
	 * @param nuevopassword the nuevopassword to set
	 */
	public void setNuevopassword(String nuevopassword) {
		this.nuevopassword = nuevopassword;
	}
	/**
	 * @return the renuevopassword
	 */
	public String getRenuevopassword() {
		return renuevopassword;
	}
	/**
	 * @param renuevopassword the renuevopassword to set
	 */
	public void setRenuevopassword(String renuevopassword) {
		this.renuevopassword = renuevopassword;
	}
	
	
	
	
}
