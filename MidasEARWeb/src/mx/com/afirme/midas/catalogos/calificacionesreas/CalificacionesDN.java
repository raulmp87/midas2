package mx.com.afirme.midas.catalogos.calificacionesreas;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class CalificacionesDN {

	public static final CalificacionesDN INSTANCIA = new CalificacionesDN();

	public static CalificacionesDN getInstancia() {
		return CalificacionesDN.INSTANCIA;
	}

	public List<CalificacionAgenciaDTO> listarTodos() throws SystemException
			 {
		CalificacionesSN calificacionesSN = new CalificacionesSN();
		return calificacionesSN.listarTodos();
	}

	public void agregar(CalificacionAgenciaDTO calificacionAgenciaDTO) throws SystemException
	{
		CalificacionesSN calificacionesSN = new CalificacionesSN();
		calificacionesSN.agregar(calificacionAgenciaDTO);
	}

	public void modificar(CalificacionAgenciaDTO calificacionAgenciaDTO) throws SystemException {
		CalificacionesSN calificacionesSN = new CalificacionesSN();
		calificacionesSN.modificar(calificacionAgenciaDTO);
	}

	public CalificacionAgenciaDTO getPorId(CalificacionAgenciaDTO calificacionAgenciaDTO)
			throws SystemException {
		CalificacionesSN calificacionesSN = new CalificacionesSN();
		return calificacionesSN.getPorId(calificacionAgenciaDTO.getId());
	}
	
	public void borrar(CalificacionAgenciaDTO CalificacionAgenciaDTO) throws SystemException{
		CalificacionesSN calificacionesSN = new CalificacionesSN();
		calificacionesSN.borrar(CalificacionAgenciaDTO);
	}

	public List<CalificacionAgenciaDTO> listarFiltrado(CalificacionAgenciaDTO calificacionAgenciaDTO)
			throws SystemException {
		CalificacionesSN calificacionesSN = new CalificacionesSN();
		return calificacionesSN.listarFiltrado(calificacionAgenciaDTO);
	}

}