package mx.com.afirme.midas.catalogos.calificacionesreas;


import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @struts.form name="calificacionForm" Carga REAS
 */
public class CalificacionesForm extends MidasBaseForm{

	private static final long serialVersionUID = -5655417772797679102L;
	private String idtcContacto;
	private String idAgencia;
	private String calificacion;
	private String valor;
	/**
	 * @return the idtcContacto
	 */
	public String getIdtcContacto() {
		return idtcContacto;
	}
	/**
	 * @param idtcContacto the idtcContacto to set
	 */
	public void setIdtcContacto(String idtcContacto) {
		this.idtcContacto = idtcContacto;
	}
	
	public String getIdAgencia() {
		return idAgencia;
	}
	public void setIdAgencia(String idAgencia) {
		this.idAgencia = idAgencia;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}