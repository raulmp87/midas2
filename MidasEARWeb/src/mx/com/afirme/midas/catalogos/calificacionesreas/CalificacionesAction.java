package mx.com.afirme.midas.catalogos.calificacionesreas;


import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDN;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



public class CalificacionesAction extends MidasMappingDispatchAction{

	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			limpiarForm((CalificacionesForm) form);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log("SystemException", Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void limpiarForm(CalificacionesForm form){
		if(form != null){
			form.setIdAgencia("");
			form.setCalificacion("");
			form.setValor("");
		}
	}
	
	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		CalificacionesForm calForm = (CalificacionesForm) form;
		CalificacionAgenciaDTO contactoDTO = new CalificacionAgenciaDTO();
		poblarDTO(calForm, contactoDTO);
		try {
			List<CalificacionAgenciaDTO> contactos = CalificacionesDN.getInstancia().listarFiltrado(contactoDTO);
			request.setAttribute("contactos", contactos);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
		} catch (SystemException e) {
			LogDeMidasWeb.log("SystemException", Level.INFO, e);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request)throws SystemException {
		CalificacionesDN contactoDN = CalificacionesDN.getInstancia();
		List<CalificacionAgenciaDTO> contactos = contactoDN.listarTodos();
		request.setAttribute("contactos",contactos);
	}

	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */ 
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CalificacionesForm calForm = (CalificacionesForm) form;
		CalificacionAgenciaDTO contactoDTO = new CalificacionAgenciaDTO();
		this.poblarDTO(calForm, contactoDTO);
		CalificacionesDN contactoDN = CalificacionesDN.getInstancia();
		try {
			contactoDN.agregar(contactoDTO);
			calForm.setMensajeUsuario(null,"guardar");
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		CalificacionesForm calificacionForm = (CalificacionesForm) form;
		CalificacionAgenciaDTO contactoDTO = new CalificacionAgenciaDTO();
		this.poblarDTO(calificacionForm, contactoDTO);
		CalificacionesDN contactoDN = CalificacionesDN.getInstancia();
		try {
			contactoDN.modificar(contactoDTO);
			calificacionForm.setMensajeUsuario(null,"modificar");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	
	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		CalificacionesForm contactoForm = (CalificacionesForm) form;
		CalificacionAgenciaDTO contactoDTO = new CalificacionAgenciaDTO();
		this.poblarDTO(contactoForm, contactoDTO);
		
		CalificacionesDN contactoDN = CalificacionesDN.getInstancia();
		try {
				contactoDN.borrar(contactoDTO);
				this.listarTodos(request);
				contactoForm.setMensajeUsuario(null,"eliminar");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
		} catch (Exception e){
			contactoForm.setMensaje("No Se Pudo Borrar El Contacto Seleccionado Por Que Existen Referencias A Este Contacto");
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
			return mapping.findForward(reglaNavegacion);
		}
		
		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);

	}

	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		CalificacionesForm calificacionesForm 		= (CalificacionesForm) form;
		CalificacionAgenciaDTO contactoDTO 		= new CalificacionAgenciaDTO();
		String id 						= request.getParameter("id");
		
		contactoDTO.setId(BigDecimal.valueOf(Double.valueOf(id)));
		this.poblarDTO(calificacionesForm, contactoDTO);
		CalificacionesDN contactoDN = CalificacionesDN.getInstancia();
		try {
			contactoDTO = contactoDN.getPorId(contactoDTO);
			this.poblarForm(contactoDTO, calificacionesForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log("SystemException", Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContactoDTO
	 * @param calificacionesForm
	 */
	private void poblarForm(CalificacionAgenciaDTO calificacionDTO, CalificacionesForm calificacionesForm) {
		
		if (calificacionDTO.getId()!= null){
			calificacionesForm.setIdtcContacto(calificacionDTO.getId().toString());
		}
		
		if (calificacionDTO.getAgencia()!= null){
			calificacionesForm.setIdAgencia(calificacionDTO.getAgencia().getIdagencia()+"");
		}
		
		if (!StringUtil.isEmpty(calificacionDTO.getCalificacion())){
			calificacionesForm.setCalificacion(calificacionDTO.getCalificacion());
		}
		
		if (calificacionDTO.getValor()!= null){
			calificacionesForm.setValor(calificacionDTO.getValor().toString());
		}

	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param calificacionesForm
	 * @param ContactoDTO
	 */
	private void poblarDTO(CalificacionesForm calificacionesForm, CalificacionAgenciaDTO calificacioAgenciaDTO) {
		BigDecimal id;
		BigDecimal valor;
	
		if (!StringUtil.isEmpty(calificacionesForm.getIdtcContacto())) {
			id = new BigDecimal(calificacionesForm.getIdtcContacto());
			calificacioAgenciaDTO.setId(id);
		}
		
		if (!StringUtil.isEmpty(calificacionesForm.getCalificacion())) 
			calificacioAgenciaDTO.setCalificacion(calificacionesForm.getCalificacion());
		
		
		if (!StringUtil.isEmpty(calificacionesForm.getValor())) {
			valor = new BigDecimal(calificacionesForm.getValor());
			calificacioAgenciaDTO.setValor(valor);
		}
		
		if (!StringUtil.isEmpty(calificacionesForm.getIdAgencia())){
			AgenciaCalificadoraDTO agenciaDTO = new AgenciaCalificadoraDTO();
			agenciaDTO.setIdagencia(new BigDecimal( calificacionesForm.getIdAgencia()));
			try {
				agenciaDTO = AgenciaCalificadoraDN.getInstancia().obtenerAgenciaPorId(agenciaDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("ExcepcionDeAccesoADatos", Level.INFO, e);
			} catch (SystemException e) {
				LogDeMidasWeb.log("SystemException", Level.INFO, e);
			}
			calificacioAgenciaDTO.setAgencia(agenciaDTO);
			
			
		}
	}
	
	
	
}