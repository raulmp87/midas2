package mx.com.afirme.midas.catalogos.calificacionesreas;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class CalificacionesSN {

	private CalificacionAgenciaFacadeRemote beanRemoto;

	public CalificacionesSN() throws SystemException {
		LogDeMidasWeb.log("Entrando enContactoSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CalificacionAgenciaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CalificacionAgenciaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<CalificacionAgenciaDTO> contactoBancos = beanRemoto.findAll();
		return contactoBancos;
	}

	public void agregar(CalificacionAgenciaDTO CalificacionAgenciaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(CalificacionAgenciaDTO);
	}

	public void modificar(CalificacionAgenciaDTO CalificacionAgenciaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(CalificacionAgenciaDTO);
	}

	public CalificacionAgenciaDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		
		List<CalificacionAgenciaDTO> agenciaDTOs =  beanRemoto.findByPropertyID(id);  
		return agenciaDTOs.get(0);
		//return beanRemoto.findById(id); 

	}

	public void borrar(CalificacionAgenciaDTO CalificacionAgenciaDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(CalificacionAgenciaDTO);
	}

	public List<CalificacionAgenciaDTO> listarFiltrado(CalificacionAgenciaDTO CalificacionAgenciaDTO)
			throws ExcepcionDeAccesoADatos {
		List<CalificacionAgenciaDTO> contactos = beanRemoto.listarFiltrado(CalificacionAgenciaDTO);
		return contactos;
	}

}
