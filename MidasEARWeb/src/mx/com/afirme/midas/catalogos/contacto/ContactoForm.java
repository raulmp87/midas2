package mx.com.afirme.midas.catalogos.contacto;


import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @struts.form name="contactoForm"
 */
public class ContactoForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5655417772797679102L;
	private String idtcContacto;
	private String idtcReaseguradorCorredor;
	private String nombre;
	private String telefono;
	private String idtcParticipacion;
	/**
	 * @return the idtcContacto
	 */
	public String getIdtcContacto() {
		return idtcContacto;
	}
	/**
	 * @param idtcContacto the idtcContacto to set
	 */
	public void setIdtcContacto(String idtcContacto) {
		this.idtcContacto = idtcContacto;
	}
	/**
	 * @return the idtcReaseguradorCorredor
	 */
	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}
	/**
	 * @param idtcReaseguradorCorredor the idtcReaseguradorCorredor to set
	 */
	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the idtcParticipacion
	 */
	public String getIdtcParticipacion() {
		return idtcParticipacion;
	}
	/**
	 * @param idtcParticipacion the idtcParticipacion to set
	 */
	public void setIdtcParticipacion(String idtcParticipacion) {
		this.idtcParticipacion = idtcParticipacion;
	}
	
}