package mx.com.afirme.midas.catalogos.contacto;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContactoDN {

	public static final ContactoDN INSTANCIA = new ContactoDN();

	public static ContactoDN getInstancia() {
		return ContactoDN.INSTANCIA;
	}

	public List<ContactoDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		return contactoSN.listarTodos();
	}

	public void agregar(ContactoDTO contactoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		contactoSN.agregar(contactoDTO);
	}

	public void modificar(ContactoDTO contactoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		contactoSN.modificar(contactoDTO);
	}

	public ContactoDTO getPorId(ContactoDTO contactoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		return contactoSN.getPorId(contactoDTO.getIdtccontacto());
	}

	public ContactoDTO getPorId(BigDecimal id) throws SystemException,
			ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		return contactoSN.getPorId(id);
	}

	public void borrar(ContactoDTO contactoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		contactoSN.borrar(contactoDTO);
	}

	public List<ContactoDTO> listarFiltrado(ContactoDTO contactoDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ContactoSN contactoSN = new ContactoSN();
		return contactoSN.listarFiltrado(contactoDTO);
	}

}