package mx.com.afirme.midas.catalogos.contacto;


import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



public class ContactoAction extends MidasMappingDispatchAction{

	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			limpiarForm((ContactoForm) form);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void limpiarForm(ContactoForm form){
		if(form != null){
			form.setIdtcReaseguradorCorredor("");
			form.setNombre("");
			form.setTelefono("");
		}
	}
	
	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		ContactoDTO contactoDTO = new ContactoDTO();
		ContactoForm contactoForm = (ContactoForm) form;
		ReaseguradorCorredorDTO reaseguradorCorredor = new ReaseguradorCorredorDTO();
		if (!UtileriasWeb.esObjetoNulo(contactoForm.getIdtcReaseguradorCorredor()))
			reaseguradorCorredor.setIdtcreaseguradorcorredor(new BigDecimal(contactoForm.getIdtcReaseguradorCorredor()));
		contactoDTO.setReaseguradorCorredor(reaseguradorCorredor);
		contactoDTO.setNombre(contactoForm.getNombre());
		if (!UtileriasWeb.esObjetoNulo(contactoForm.getTelefono()))
				contactoDTO.setTelefono(new BigDecimal(contactoForm.getTelefono()));
		try {
			List<ContactoDTO> contactos = ContactoDN.getInstancia().listarFiltrado(contactoDTO);
			request.setAttribute("contactos", contactos);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request)throws SystemException, ExcepcionDeAccesoADatos {
		ContactoDN contactoDN = ContactoDN.getInstancia();
		List<ContactoDTO> contactos = contactoDN.listarTodos();
		request.setAttribute("contactos",contactos);

	}

	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ContactoForm contactoForm = (ContactoForm) form;
		ContactoDTO contactoDTO = new ContactoDTO();
		this.poblarDTO(contactoForm, contactoDTO);
		ContactoDN contactoDN = ContactoDN.getInstancia();
		try {
			contactoDN.agregar(contactoDTO);
			contactoForm.setMensajeUsuario(null,"guardar");
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		ContactoForm contactoForm = (ContactoForm) form;
		ContactoDTO contactoDTO = new ContactoDTO();
		this.poblarDTO(contactoForm, contactoDTO);
		ContactoDN contactoDN = ContactoDN.getInstancia();
		try {
			contactoDN.modificar(contactoDTO);
			contactoForm.setMensajeUsuario(null,"modificar");
//			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	
	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		ContactoForm contactoForm = (ContactoForm) form;
		ContactoDTO contactoDTO = new ContactoDTO();
		this.poblarDTO(contactoForm, contactoDTO);
		String reglaNavegacion = Sistema.EXITOSO;
		ContactoDN contactoDN = ContactoDN.getInstancia();
		try {
				contactoDN.borrar(contactoDTO);
				this.listarTodos(request);
				contactoForm.setMensajeUsuario(null,"eliminar");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (Exception e){
			if(e.getClass().getName().split("saction")[1].equals("RolledbackException")){
				contactoForm.setMensaje("No Se Pudo Borrar El Contacto Seleccionado Por Que Existen Referencias A Este Contacto");
				return mapping.findForward(reglaNavegacion);
			}
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		ContactoForm contactoForm 		= (ContactoForm) form;
		ContactoDTO contactoDTO 		= new ContactoDTO();
		this.poblarDTO(contactoForm, contactoDTO);
		ContactoDN contactoDN 			= ContactoDN.getInstancia();
		try {
			contactoDTO = contactoDN.getPorId(contactoDTO);
			this.poblarForm(contactoDTO,contactoForm);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}

	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		ContactoForm contactoForm 		= (ContactoForm) form;
		ContactoDTO contactoDTO 		= new ContactoDTO();
		String id 						= request.getParameter("id");
		
		contactoDTO.setIdtccontacto(BigDecimal.valueOf(Double.valueOf(id)));
		this.poblarDTO(contactoForm, contactoDTO);
		ContactoDN contactoDN = ContactoDN.getInstancia();
		try {
			contactoDTO = contactoDN.getPorId(contactoDTO);
			this.poblarForm(contactoDTO, contactoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContactoDTO
	 * @param ContactoForm
	 */
	private void poblarForm(ContactoDTO contactoDTO, ContactoForm contactoForm) {

		if (contactoDTO.getIdtccontacto()!= null) contactoForm.setIdtcContacto(contactoDTO.getIdtccontacto().toString());
		
		if (contactoDTO.getReaseguradorCorredor() != null){
			contactoForm.setIdtcReaseguradorCorredor(contactoDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor().toString());
		}
		
		if (!StringUtil.isEmpty(contactoDTO.getNombre())) contactoForm.setNombre(contactoDTO.getNombre());
		
		if (contactoDTO.getTelefono()!= null) contactoForm.setTelefono(contactoDTO.getTelefono().toString());
	
	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param ContactoForm
	 * @param ContactoDTO
	 */
	private void poblarDTO(ContactoForm contactoForm, ContactoDTO contactoDTO) {
		BigDecimal id;
		BigDecimal telefono;
	
		if (!StringUtil.isEmpty(contactoForm.getIdtcContacto())) {
			id = new BigDecimal(contactoForm.getIdtcContacto());
			contactoDTO.setIdtccontacto(id);
		}
		
		if (!StringUtil.isEmpty(contactoForm.getIdtcReaseguradorCorredor())){
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(contactoForm.getIdtcReaseguradorCorredor()));
			try {
				reaseguradorCorredorDTO = ReaseguradorCorredorDN.getInstancia().obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
			contactoDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
		}
		
		if (!StringUtil.isEmpty(contactoForm.getNombre())) contactoDTO.setNombre(contactoForm.getNombre());
		
		
		if (!StringUtil.isEmpty(contactoForm.getTelefono())) {
			telefono = new BigDecimal(contactoForm.getTelefono());
			contactoDTO.setTelefono(telefono);
		}
		
	}
	
}