package mx.com.afirme.midas.catalogos.contacto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;

/**
 * @author
 * 
 */

public class ContactoSN {

	private ContactoFacadeRemote beanRemoto;

	public ContactoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando enContactoSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ContactoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ContactoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ContactoDTO> contactoBancos = beanRemoto.findAll();
		return contactoBancos;
	}

	public void agregar(ContactoDTO contactoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(contactoDTO);
	}

	public void modificar(ContactoDTO contactoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(contactoDTO);
	}

	public ContactoDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);

	}

	public void borrar(ContactoDTO contactoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(contactoDTO);
	}

	public List<ContactoDTO> listarFiltrado(ContactoDTO contactoDTO)
			throws ExcepcionDeAccesoADatos {
		List<ContactoDTO> contactos = beanRemoto.listarFiltrado(contactoDTO);
		return contactos;
	}

}
