package mx.com.afirme.midas.catalogos.tipodestinotransporte;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteDTO;
import mx.com.afirme.midas.catalogos.tipodestinotransporte.TipoDestinoTransporteDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoDestinoTransporteAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request) throws SystemException, ExcepcionDeAccesoADatos {
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		List<TipoDestinoTransporteDTO> tipoDestinoTransporteList = tipoDestinoTransporteDN.listarTodos();
		request.setAttribute("tipoDestinoTransporteList", tipoDestinoTransporteList);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoDestinoTransporteForm tipoDestinoTransporteForm = (TipoDestinoTransporteForm) form;
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		try {
			poblarDTO(tipoDestinoTransporteForm, tipoDestinoTransporteDTO);
			request.setAttribute("tipoDestinoTransporteList", tipoDestinoTransporteDN.listarFiltrado(tipoDestinoTransporteDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(TipoDestinoTransporteForm tipoDestinoTransporteForm, TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws SystemException, ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(tipoDestinoTransporteForm.getCodigoTipoDestinoTransporte()))
			tipoDestinoTransporteDTO.setCodigoTipoDestinoTransporte(new BigDecimal(tipoDestinoTransporteForm.getCodigoTipoDestinoTransporte()));
		if (!UtileriasWeb.esCadenaVacia(tipoDestinoTransporteForm.getDescripcionTipoDestinoTransporte()))
			tipoDestinoTransporteDTO.setDescripcionTipoDestinoTransporte(tipoDestinoTransporteForm.getDescripcionTipoDestinoTransporte());
	}
	
	private void poblarForm(TipoDestinoTransporteForm tipoDestinoTransporteForm, TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(""+tipoDestinoTransporteDTO.getIdTipoDestinoTransporte()))
			tipoDestinoTransporteForm.setIdTipoDestinoTransporte(tipoDestinoTransporteDTO.getIdTipoDestinoTransporte().toBigInteger().toString());
		if (!UtileriasWeb.esCadenaVacia(tipoDestinoTransporteDTO.getCodigoTipoDestinoTransporte().toString()))
			tipoDestinoTransporteForm.setCodigoTipoDestinoTransporte(tipoDestinoTransporteDTO.getCodigoTipoDestinoTransporte().toBigInteger().toString());
		if (!UtileriasWeb.esCadenaVacia(tipoDestinoTransporteDTO.getDescripcionTipoDestinoTransporte()))
			tipoDestinoTransporteForm.setDescripcionTipoDestinoTransporte(tipoDestinoTransporteDTO.getDescripcionTipoDestinoTransporte());
		
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoDestinoTransporteForm tipoDestinoTransporteForm = (TipoDestinoTransporteForm) form;
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		try {
			poblarDTO(tipoDestinoTransporteForm, tipoDestinoTransporteDTO);
			tipoDestinoTransporteDN.agregar(tipoDestinoTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoDestinoTransporteForm tipoDestinoTransporteForm = (TipoDestinoTransporteForm) form;
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		try {
			
			tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(new BigDecimal(tipoDestinoTransporteForm.getIdTipoDestinoTransporte()));
			tipoDestinoTransporteDTO = tipoDestinoTransporteDN.getTipoDestinoTransportePorId(tipoDestinoTransporteDTO);
			poblarDTO(tipoDestinoTransporteForm, tipoDestinoTransporteDTO);
			tipoDestinoTransporteDN.modificar(tipoDestinoTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoDestinoTransporteForm tipoDestinoTransporteForm = (TipoDestinoTransporteForm) form;
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		try {
			tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(UtileriasWeb.regresaBigDecimal(tipoDestinoTransporteForm.getIdTipoDestinoTransporte()));
			tipoDestinoTransporteDTO = tipoDestinoTransporteDN.getTipoDestinoTransportePorId(tipoDestinoTransporteDTO);
			tipoDestinoTransporteDN.borrar(tipoDestinoTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		TipoDestinoTransporteForm tipoDestinoTransporteForm = (TipoDestinoTransporteForm) form;
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(new BigDecimal(id));
			tipoDestinoTransporteDTO = tipoDestinoTransporteDN.getTipoDestinoTransportePorId(tipoDestinoTransporteDTO);
			System.out.println("\n\n\n codigo:"+tipoDestinoTransporteDTO.getCodigoTipoDestinoTransporte());
			poblarForm(tipoDestinoTransporteForm, tipoDestinoTransporteDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
}
