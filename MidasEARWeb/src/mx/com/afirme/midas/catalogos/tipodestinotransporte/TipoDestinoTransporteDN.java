package mx.com.afirme.midas.catalogos.tipodestinotransporte;

import java.util.List;

import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoDestinoTransporteDN {
	
	public static final TipoDestinoTransporteDN INSTANCIA = new TipoDestinoTransporteDN();

	public static TipoDestinoTransporteDN getInstancia (){
		return TipoDestinoTransporteDN.INSTANCIA;
	}
	
	public String agregar(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDestinoTransporteSN tipoDestinoTransporteSN = new TipoDestinoTransporteSN();
		return tipoDestinoTransporteSN.agregar(tipoDestinoTransporteDTO);
	}
	
	public String borrar (TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDestinoTransporteSN tipoDestinoTransporteSN = new TipoDestinoTransporteSN();
		return tipoDestinoTransporteSN.borrar(tipoDestinoTransporteDTO);
	}
	
	public String modificar (TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDestinoTransporteSN tipoDestinoTransporteSN = new TipoDestinoTransporteSN();
		return tipoDestinoTransporteSN.modificar(tipoDestinoTransporteDTO);
	}
	
	public TipoDestinoTransporteDTO getTipoDestinoTransportePorId(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDestinoTransporteSN tipoDestinoTransporteSN = new TipoDestinoTransporteSN();
		return tipoDestinoTransporteSN.getTipoDestinoTransportePorId(tipoDestinoTransporteDTO);
	}
	
	public List<TipoDestinoTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoDestinoTransporteSN tipoDestinoTransporteSN = new TipoDestinoTransporteSN();
		return tipoDestinoTransporteSN.listarTodos();
	}
	
	public List<TipoDestinoTransporteDTO> listarFiltrado(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDestinoTransporteSN tipoDestinoTransporteSN = new TipoDestinoTransporteSN();
		return tipoDestinoTransporteSN.listarFiltrado(tipoDestinoTransporteDTO);
	}

}
