package mx.com.afirme.midas.catalogos.tipodestinotransporte;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteDTO;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoDestinoTransporteSN {

	private TipoDestinoTransporteFacadeRemote beanRemoto;

	public TipoDestinoTransporteSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoDestinoTransporteFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoDestinoTransporteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoDestinoTransporteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoDestinoTransporteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<TipoDestinoTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public TipoDestinoTransporteDTO getTipoDestinoTransportePorId(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoDestinoTransporteDTO.getIdTipoDestinoTransporte());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoDestinoTransporteDTO> listarFiltrado(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoDestinoTransporteDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
