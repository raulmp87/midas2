package mx.com.afirme.midas.catalogos.tipodestinotransporte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoDestinoTransporteForm extends MidasBaseForm {

	/**
	 *@author Rodrigo Marquez
	 *@since 12/08/2009
	 */
	private static final long serialVersionUID = 6237257107555440878L;

	private String idTipoDestinoTransporte;
	private String descripcionTipoDestinoTransporte;
	private String codigoTipoDestinoTransporte;

	public String getIdTipoDestinoTransporte() {
		return idTipoDestinoTransporte;
	}

	public void setIdTipoDestinoTransporte(String idTipoDestinoTransporte) {
		this.idTipoDestinoTransporte = idTipoDestinoTransporte;
	}

	public String getDescripcionTipoDestinoTransporte() {
		return descripcionTipoDestinoTransporte;
	}

	public void setDescripcionTipoDestinoTransporte(
			String descripcionTipoDestinoTransporte) {
		this.descripcionTipoDestinoTransporte = descripcionTipoDestinoTransporte;
	}

	public String getCodigoTipoDestinoTransporte() {
		return codigoTipoDestinoTransporte;
	}

	public void setCodigoTipoDestinoTransporte(
			String codigoTipoDestinoTransporte) {
		this.codigoTipoDestinoTransporte = codigoTipoDestinoTransporte;
	}

}
