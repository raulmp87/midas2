package mx.com.afirme.midas.catalogos.lloyds;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class LloydsDN {
	private static final LloydsDN INSTANCIA = new LloydsDN();

	public static LloydsDN getInstancia() {
		return LloydsDN.INSTANCIA;
	}
	
	public List<LloydsDTO> buscarPorPropiedad(String propiedad, Object valor) throws SystemException {
		return new LloydsSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<LloydsDTO> buscarTodos() throws SystemException {
		return new LloydsSN().buscarTodos();
	}
	
	public List<LloydsDTO> buscarTodosOrdenadosPor(String...propiedades) throws SystemException {
		return new LloydsSN().buscarTodosOrdenadosPor(propiedades);
	}	
	
}
