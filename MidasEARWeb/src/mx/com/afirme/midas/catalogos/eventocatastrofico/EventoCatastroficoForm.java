package mx.com.afirme.midas.catalogos.eventocatastrofico;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 *@author clozada
 *@since 12/02/20110
 */
public class EventoCatastroficoForm extends MidasBaseForm {

	
	private static final long serialVersionUID = 1L;
	
	private String idTcEventoCatastrofico;
	private String codigoEvento;
	private String descripcionEvento;
	
	
	/**
	 * @return the idTcEventoCatastrofico
	 */
	public String getIdTcEventoCatastrofico() {
		return idTcEventoCatastrofico;
	}
	/**
	 * @param idTcEventoCatastrofico the idTcEventoCatastrofico to set
	 */
	public void setIdTcEventoCatastrofico(String idTcEventoCatastrofico) {
		this.idTcEventoCatastrofico = idTcEventoCatastrofico;
	}
	/**
	 * @return the codigoEvento
	 */
	public String getCodigoEvento() {
		return codigoEvento;
	}
	/**
	 * @param codigoEvento the codigoEvento to set
	 */
	public void setCodigoEvento(String codigoEvento) {
		this.codigoEvento = codigoEvento;
	}
	/**
	 * @return the descripcionEvento
	 */
	public String getDescripcionEvento() {
		return descripcionEvento;
	}
	/**
	 * @param descripcionEvento the descripcionEvento to set
	 */
	public void setDescripcionEvento(String descripcionEvento) {
		this.descripcionEvento = descripcionEvento;
	}
	
	
}
