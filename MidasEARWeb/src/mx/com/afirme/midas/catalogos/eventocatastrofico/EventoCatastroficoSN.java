package mx.com.afirme.midas.catalogos.eventocatastrofico;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EventoCatastroficoSN {

	private EventoCatastroficoFacadeRemote beanRemoto;
	
	public EventoCatastroficoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en EventoCatastroficoSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(EventoCatastroficoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto EventoCatastroficoDTOFacadeRemote instanciado", Level.FINEST, null);
	}

	public List<EventoCatastroficoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<EventoCatastroficoDTO> eventos = beanRemoto.findAll();
		return eventos;

	}	

	public void agregar(EventoCatastroficoDTO eventoCatastroficoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(eventoCatastroficoDTO);
	}

	public void modificar(EventoCatastroficoDTO eventoCatastroficoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(eventoCatastroficoDTO);
	}

	public EventoCatastroficoDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(EventoCatastroficoDTO eventoCatastroficoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(eventoCatastroficoDTO);
	}
	
	public List<EventoCatastroficoDTO> listarFiltrado(EventoCatastroficoDTO eventoCatastroficoDTO) throws ExcepcionDeAccesoADatos {
		List<EventoCatastroficoDTO> eventos = beanRemoto.listarFiltrado(eventoCatastroficoDTO);
		return eventos;

	}	

}
