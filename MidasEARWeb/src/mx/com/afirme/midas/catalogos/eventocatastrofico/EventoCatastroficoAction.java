package mx.com.afirme.midas.catalogos.eventocatastrofico;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author clozada
 * @since 12/02/2010
 *
 */
public class EventoCatastroficoAction extends MidasMappingDispatchAction {

	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
		EventoCatastroficoForm eventoCatastroficoForm = (EventoCatastroficoForm) form;
		try {
			List<EventoCatastroficoDTO> lista = eventoCatastroficoDN.listarTodos();
			request.setAttribute("listaEventosCatastroficos", lista);
			limpiarForm(eventoCatastroficoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EventoCatastroficoForm eventoCatastroficoForm = (EventoCatastroficoForm) form;
		EventoCatastroficoDTO eventoCatastroficoDTO = new EventoCatastroficoDTO();
		EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
		try {
			if(eventoCatastroficoForm.getCodigoEvento() != null && !eventoCatastroficoForm.getCodigoEvento().equals("")){
				eventoCatastroficoDTO.setCodigoEvento(new BigDecimal(eventoCatastroficoForm.getCodigoEvento()));
			}
			if(eventoCatastroficoForm.getDescripcionEvento() != null && !eventoCatastroficoForm.getDescripcionEvento().equals("")){
				eventoCatastroficoDTO.setDescripcionEvento(eventoCatastroficoForm.getDescripcionEvento());
			}
			request.setAttribute("listaEventosCatastroficos", eventoCatastroficoDN.listarFiltrado(eventoCatastroficoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EventoCatastroficoForm eventoCatastroficoForm = (EventoCatastroficoForm) form;
		EventoCatastroficoDTO eventoCatastroficoDTO = new EventoCatastroficoDTO();
		EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
		try {
			this.poblarDTO(eventoCatastroficoForm, eventoCatastroficoDTO);
			eventoCatastroficoDN.agregar(eventoCatastroficoDTO);
			limpiarForm(eventoCatastroficoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EventoCatastroficoForm eventoCatastroficoForm = (EventoCatastroficoForm) form;
		EventoCatastroficoDTO eventoCatastroficoDTO = new EventoCatastroficoDTO();
		EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
		BigDecimal idTcEventoCatastrofico = new BigDecimal(0);
		try {
			if(eventoCatastroficoForm.getIdTcEventoCatastrofico() != null){
				idTcEventoCatastrofico = new BigDecimal(eventoCatastroficoForm.getIdTcEventoCatastrofico());
			}
			eventoCatastroficoDTO = eventoCatastroficoDN.getPorId(idTcEventoCatastrofico);
			poblarForm(eventoCatastroficoForm, eventoCatastroficoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EventoCatastroficoForm eventoCatastroficoForm = (EventoCatastroficoForm) form;
		EventoCatastroficoDTO eventoCatastroficoDTO = new EventoCatastroficoDTO();
		EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
		BigDecimal idTcEventoCatastrofico = new BigDecimal(0);
		try {
			if(eventoCatastroficoForm.getIdTcEventoCatastrofico() != null){
				idTcEventoCatastrofico = new BigDecimal(eventoCatastroficoForm.getIdTcEventoCatastrofico());
			}
			eventoCatastroficoDTO = eventoCatastroficoDN.getPorId(idTcEventoCatastrofico);
			this.poblarDTO(eventoCatastroficoForm, eventoCatastroficoDTO);
			eventoCatastroficoDN.modificar(eventoCatastroficoDTO);
			limpiarForm(eventoCatastroficoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EventoCatastroficoForm eventoCatastroficoForm = (EventoCatastroficoForm) form;
		EventoCatastroficoDTO eventoCatastroficoDTO = new EventoCatastroficoDTO();
		EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
		BigDecimal idTcEventoCatastrofico = new BigDecimal(0);
		try {
			if(eventoCatastroficoForm.getIdTcEventoCatastrofico() != null){
				idTcEventoCatastrofico = new BigDecimal(eventoCatastroficoForm.getIdTcEventoCatastrofico());
			}
			eventoCatastroficoDTO = eventoCatastroficoDN.getPorId(idTcEventoCatastrofico);
			eventoCatastroficoDN.borrar(eventoCatastroficoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void limpiarForm(EventoCatastroficoForm form){
		form.setDescripcionEvento("");
	}
	
	private void poblarDTO(EventoCatastroficoForm eventoCatastroficoForm, EventoCatastroficoDTO eventoCatastroficoDTO) throws SystemException {
		if(eventoCatastroficoForm.getCodigoEvento() != null){
			eventoCatastroficoDTO.setCodigoEvento(new BigDecimal(eventoCatastroficoForm.getCodigoEvento()));
		}
		if(eventoCatastroficoForm.getDescripcionEvento() != null){
			eventoCatastroficoDTO.setDescripcionEvento(eventoCatastroficoForm.getDescripcionEvento());
		}
	}
	
	private void poblarForm(EventoCatastroficoForm eventoCatastroficoForm, EventoCatastroficoDTO eventoCatastroficoDTO) throws SystemException {
		if(eventoCatastroficoDTO.getIdTcEventoCatastrofico() != null){
			eventoCatastroficoForm.setIdTcEventoCatastrofico(eventoCatastroficoDTO.getIdTcEventoCatastrofico().toString());
		}
		if(eventoCatastroficoDTO.getCodigoEvento() != null){
			eventoCatastroficoForm.setCodigoEvento(eventoCatastroficoDTO.getCodigoEvento().toString());
		}
		if(eventoCatastroficoDTO.getDescripcionEvento() != null){
			eventoCatastroficoForm.setDescripcionEvento(eventoCatastroficoDTO.getDescripcionEvento());
		}
	}

}
