package mx.com.afirme.midas.catalogos.eventocatastrofico;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EventoCatastroficoDN {

	private static final EventoCatastroficoDN INSTANCIA = new EventoCatastroficoDN();

	public static EventoCatastroficoDN getInstancia() {
		return EventoCatastroficoDN.INSTANCIA;
	}

	public List<EventoCatastroficoDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		EventoCatastroficoSN eventoCatastroficoSN = new EventoCatastroficoSN();
		return eventoCatastroficoSN.listarTodos();
	}

	public void agregar(EventoCatastroficoDTO eventoCatastroficoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		EventoCatastroficoSN eventoCatastroficoSN = new EventoCatastroficoSN();
		eventoCatastroficoSN.agregar(eventoCatastroficoDTO);
	}

	public void modificar(EventoCatastroficoDTO eventoCatastroficoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		EventoCatastroficoSN eventoCatastroficoSN = new EventoCatastroficoSN();
		eventoCatastroficoSN.modificar(eventoCatastroficoDTO);
	}

	public EventoCatastroficoDTO getPorId(BigDecimal id)
			throws SystemException, ExcepcionDeAccesoADatos {
		EventoCatastroficoSN eventoCatastroficoSN = new EventoCatastroficoSN();
		return eventoCatastroficoSN.getPorId(id);
	}

	public void borrar(EventoCatastroficoDTO eventoCatastroficoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		EventoCatastroficoSN eventoCatastroficoSN = new EventoCatastroficoSN();
		eventoCatastroficoSN.borrar(eventoCatastroficoDTO);
	}
	public  List<EventoCatastroficoDTO> listarFiltrado(EventoCatastroficoDTO eventoCatastroficoDTO) throws SystemException,
		ExcepcionDeAccesoADatos {
		EventoCatastroficoSN eventoCatastroficoSN = new EventoCatastroficoSN();
		return eventoCatastroficoSN.listarFiltrado(eventoCatastroficoDTO);
	}
}
