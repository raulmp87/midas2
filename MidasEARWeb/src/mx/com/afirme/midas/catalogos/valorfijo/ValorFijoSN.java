package mx.com.afirme.midas.catalogos.valorfijo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;

public class ValorFijoSN {
	private CatalogoValorFijoFacadeRemote beanRemoto;

	public ValorFijoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CatalogoValorFijoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(CatalogoValorFijoDTO valorFijoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(valorFijoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(CatalogoValorFijoDTO valorFijoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(valorFijoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(CatalogoValorFijoDTO valorFijoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(valorFijoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CatalogoValorFijoDTO> listarTodos()
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CatalogoValorFijoDTO> buscarPorPropiedad(String propiedad,
			Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public CatalogoValorFijoDTO getPorId(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		try {
			return (CatalogoValorFijoDTO) beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
