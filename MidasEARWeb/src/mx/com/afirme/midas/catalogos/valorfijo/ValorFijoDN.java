package mx.com.afirme.midas.catalogos.valorfijo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;

public class ValorFijoDN {
	private static final ValorFijoDN INSTANCIA = new ValorFijoDN();

	public static ValorFijoDN getInstancia() {
		return ValorFijoDN.INSTANCIA;
	}

	public CatalogoValorFijoDTO getPorId(BigDecimal id) throws SystemException {
		ValorFijoSN valorFijoSN = new ValorFijoSN();
		return valorFijoSN.getPorId(id);
	}

	public List<CatalogoValorFijoDTO> listarTodos() throws SystemException {
		ValorFijoSN valorFijoSN = new ValorFijoSN();
		return valorFijoSN.listarTodos();
	}

	public List<CatalogoValorFijoDTO> buscarPorPropiedad(String propiedad,
			Object valor) throws SystemException {
		ValorFijoSN valorFijoSN = new ValorFijoSN();
		return valorFijoSN.buscarPorPropiedad(propiedad, valor);
	}

	public void agregar(CatalogoValorFijoDTO valorFijoDTO)
			throws SystemException {
		ValorFijoSN valorFijoSN = new ValorFijoSN();
		valorFijoSN.agregar(valorFijoDTO);
	}

	public void borrar(CatalogoValorFijoDTO valorFijoDTO)
			throws SystemException {
		ValorFijoSN valorFijoSN = new ValorFijoSN();
		valorFijoSN.borrar(valorFijoDTO);
	}
}
