package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PaisTipoDestinoTransporteDN {
	
	public static final PaisTipoDestinoTransporteDN INSTANCIA = new PaisTipoDestinoTransporteDN();

	public static PaisTipoDestinoTransporteDN getInstancia (){
		return PaisTipoDestinoTransporteDN.INSTANCIA;
	}
	
	public String agregar(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.agregar(paisTipoDestinoTransporteDTO);
	}
	
	public String borrar (PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.borrar(paisTipoDestinoTransporteDTO);
	}
	
	public String modificar (PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.modificar(paisTipoDestinoTransporteDTO);
	}
	
	public PaisTipoDestinoTransporteDTO getPaisTipoDestinoTransportePorId(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.getPaisTipoDestinoTransportePorId(paisTipoDestinoTransporteDTO);
	}
	
	public List<PaisTipoDestinoTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.listarTodos();
	}
	
	public List<PaisTipoDestinoTransporteDTO> listarFiltrado(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.listarFiltrado(paisTipoDestinoTransporteDTO);
	}
	
	public PaisDTO getPaisPorId(PaisDTO paisDTO) throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.getPaisPorId(paisDTO);
	}
	
	public List<PaisDTO> listarTodosPaises() throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.listarTodosPaises();
	}
	
	public List<PaisDTO> listarPaisesNoAsociadosATipoDestino() throws ExcepcionDeAccesoADatos, SystemException{
		PaisTipoDestinoTransporteSN paisTipoDestinoTransporteSN = new PaisTipoDestinoTransporteSN();
		return paisTipoDestinoTransporteSN.listarPaisesNoAsociadosATipoDestino();
	}
	
}
