package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PaisTipoDestinoTransporteSN {

	private PaisTipoDestinoTransporteFacadeRemote beanRemoto;
	private PaisFacadeRemote beanRemotoPais;

	public PaisTipoDestinoTransporteSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PaisTipoDestinoTransporteFacadeRemote.class);
			beanRemotoPais = serviceLocator.getEJB(PaisFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(paisTipoDestinoTransporteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(paisTipoDestinoTransporteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(paisTipoDestinoTransporteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<PaisTipoDestinoTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public PaisTipoDestinoTransporteDTO getPaisTipoDestinoTransportePorId(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(paisTipoDestinoTransporteDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<PaisTipoDestinoTransporteDTO> listarFiltrado(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(paisTipoDestinoTransporteDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public PaisDTO getPaisPorId(PaisDTO paisDTO){
		
		return beanRemotoPais.findById(paisDTO.getCountryId());
	}
	
	public List<PaisDTO> listarTodosPaises() throws ExcepcionDeAccesoADatos{
		
		try {
			return beanRemotoPais.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public List<PaisDTO> listarPaisesNoAsociadosATipoDestino() throws ExcepcionDeAccesoADatos{
		
		try {
			return beanRemotoPais.getPaisDTONotAssociatedToTipoDestino();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}
