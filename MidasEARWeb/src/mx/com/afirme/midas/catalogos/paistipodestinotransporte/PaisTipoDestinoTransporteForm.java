package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class PaisTipoDestinoTransporteForm extends MidasBaseForm{

	/**
	 *@author Rodrigo M�rquez
	 *@since 13/08/2009
	 */
	
	private static final long serialVersionUID = 6237757103555440838L;

	private String idPais;
	private String countryName;
	private String idTipoDestinoTransporte;
	private String codigoTipoDestinoTransporte;
	private String descripcionTipoDestinoTransporte;
	
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCodigoTipoDestinoTransporte() {
		return codigoTipoDestinoTransporte;
	}
	public void setCodigoTipoDestinoTransporte(String codigoTipoDestinoTransporte) {
		this.codigoTipoDestinoTransporte = codigoTipoDestinoTransporte;
	}
	public String getDescripcionTipoDestinoTransporte() {
		return descripcionTipoDestinoTransporte;
	}
	public void setDescripcionTipoDestinoTransporte(
			String descripcionTipoDestinoTransporte) {
		this.descripcionTipoDestinoTransporte = descripcionTipoDestinoTransporte;
	}
	public String getIdTipoDestinoTransporte() {
		return idTipoDestinoTransporte;
	}
	public void setIdTipoDestinoTransporte(String idTipoDestinoTransporte) {
		this.idTipoDestinoTransporte = idTipoDestinoTransporte;
	}
	public String getIdPais() {
		return idPais;
	}
	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}	
	
}
