package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDN;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDTO;
import mx.com.afirme.midas.catalogos.tipodestinotransporte.TipoDestinoTransporteDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PaisTipoDestinoTransporteAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		List<PaisTipoDestinoTransporteDTO> paisTipoDestinoTransporteList = paisTipoDestinoTransporteDN.listarTodos();
		request.setAttribute("paisTipoDestinoTransporteList", paisTipoDestinoTransporteList);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		PaisTipoDestinoTransporteForm paisTipoDestinoTransporteForm = (PaisTipoDestinoTransporteForm) form;
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = new PaisTipoDestinoTransporteDTO();
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		try {
			
			PaisDTO paisDTO= new PaisDTO();
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getIdPais()))
				paisDTO.setCountryId(paisTipoDestinoTransporteForm.getIdPais().toUpperCase());
			//else
				//paisDTO.setCountryId("");
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getCountryName()))
				paisDTO.setCountryName(paisTipoDestinoTransporteForm.getCountryName().toUpperCase());
			//else
				//paisDTO.setCountryName("");
			TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getCodigoTipoDestinoTransporte()))
					tipoDestinoTransporteDTO.setCodigoTipoDestinoTransporte(new BigDecimal(paisTipoDestinoTransporteForm.getCodigoTipoDestinoTransporte()));
			//else
				//tipoDestinoTransporteDTO.setCodigoTipoDestinoTransporte(new BigDecimal("0"));
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getDescripcionTipoDestinoTransporte()))
					tipoDestinoTransporteDTO.setDescripcionTipoDestinoTransporte(paisTipoDestinoTransporteForm.getDescripcionTipoDestinoTransporte());
			//else
				//tipoDestinoTransporteDTO.setDescripcionTipoDestinoTransporte("");
			
			paisTipoDestinoTransporteDTO.setPais(paisDTO);
			paisTipoDestinoTransporteDTO.setTipoDestinoTransporte(tipoDestinoTransporteDTO);
			List<PaisTipoDestinoTransporteDTO> paisTipoDestinoTransporteList = paisTipoDestinoTransporteDN.listarFiltrado(paisTipoDestinoTransporteDTO);
			
			request.setAttribute("paisTipoDestinoTransporteList", paisTipoDestinoTransporteList);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(PaisTipoDestinoTransporteForm paisTipoDestinoTransporteForm, PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws SystemException, ExcepcionDeAccesoADatos {
		
			PaisDTO paisDTO = new PaisDTO();
			
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getIdPais())){
				paisDTO.setCountryId(paisTipoDestinoTransporteForm.getIdPais());
				paisTipoDestinoTransporteDTO.setIdPais(paisTipoDestinoTransporteForm.getIdPais());
			}
			
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getCountryName()))
				paisDTO.setCountryName(paisTipoDestinoTransporteForm.getCountryName());
			
			paisTipoDestinoTransporteDTO.setPais(paisDTO);
			
			TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
			
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getIdTipoDestinoTransporte()))					
				tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(new BigDecimal(paisTipoDestinoTransporteForm.getIdTipoDestinoTransporte()));
			
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getCodigoTipoDestinoTransporte()))
				tipoDestinoTransporteDTO.setCodigoTipoDestinoTransporte(new BigDecimal(paisTipoDestinoTransporteForm.getCodigoTipoDestinoTransporte()));
			
			if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteForm.getDescripcionTipoDestinoTransporte()))
				tipoDestinoTransporteDTO.setDescripcionTipoDestinoTransporte(paisTipoDestinoTransporteForm.getDescripcionTipoDestinoTransporte());
			
			paisTipoDestinoTransporteDTO.setTipoDestinoTransporte(tipoDestinoTransporteDTO);
		
	}
	
	private void poblarForm(PaisTipoDestinoTransporteForm paisTipoDestinoTransporteForm, PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO) throws SystemException {
		
		if (!UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteDTO.getIdPais()))
			paisTipoDestinoTransporteForm.setIdPais(paisTipoDestinoTransporteDTO.getIdPais());
		
		if (!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTO.getPais()) && !UtileriasWeb.esCadenaVacia(paisTipoDestinoTransporteDTO.getPais().getCountryName()))
			paisTipoDestinoTransporteForm.setCountryName(paisTipoDestinoTransporteDTO.getPais().getCountryName());
	
		if (!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte())){
			if (!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getIdTipoDestinoTransporte()))
				paisTipoDestinoTransporteForm.setIdTipoDestinoTransporte(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getIdTipoDestinoTransporte().toBigInteger().toString());
				
			if (!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getCodigoTipoDestinoTransporte()))
				paisTipoDestinoTransporteForm.setCodigoTipoDestinoTransporte(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getCodigoTipoDestinoTransporte().toBigInteger().toString());
		
			if (!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getDescripcionTipoDestinoTransporte()))
				paisTipoDestinoTransporteForm.setDescripcionTipoDestinoTransporte(paisTipoDestinoTransporteDTO.getTipoDestinoTransporte().getDescripcionTipoDestinoTransporte());
		
		}

	}
	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		String action = "";
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = new PaisTipoDestinoTransporteDTO();
		PaisDTO paisDTO = new PaisDTO();
		
		try {
			
			String idPais = request.getParameter("idPais");
			BigDecimal idTipoDestinoTransporte = new BigDecimal(request.getParameter("idTipoDestinoTransporte"));
			
			tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(idTipoDestinoTransporte);
			paisDTO.setCountryId(idPais);		
			tipoDestinoTransporteDTO = tipoDestinoTransporteDN.getTipoDestinoTransportePorId(tipoDestinoTransporteDTO);
			paisDTO = paisTipoDestinoTransporteDN.getPaisPorId(paisDTO);
			
			paisTipoDestinoTransporteDTO.setPais(paisDTO);
			paisTipoDestinoTransporteDTO.setTipoDestinoTransporte(tipoDestinoTransporteDTO);
			paisTipoDestinoTransporteDTO.setIdPais(paisDTO.getCountryId());
			paisTipoDestinoTransporteDTO.setIdTipoDestinoTransporte(tipoDestinoTransporteDTO.getId());
			
			if(request.getParameter("!nativeeditor_status").equals("inserted")) {
				paisTipoDestinoTransporteDN.agregar(paisTipoDestinoTransporteDTO);				
				
				action = "insert";
			} else if(request.getParameter("!nativeeditor_status").equals("deleted")) {
				paisTipoDestinoTransporteDN.borrar(paisTipoDestinoTransporteDTO);
				
				action = "deleted";
			}
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
			pw.flush();
			pw.close();
			return null;
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
//		PaisTipoDestinoTransporteForm paisTipoDestinoTransporteForm = (PaisTipoDestinoTransporteForm) form;
//		System.out.println("\n\n\npaisForm idPais: " + paisTipoDestinoTransporteForm.getIdPais() + ", countryid: " + paisTipoDestinoTransporteForm.getCountryId());
//		System.out.println("\nTipoForm codigo: " + paisTipoDestinoTransporteForm.getCodigoTipoDestinoTransporte() + ", id: " + paisTipoDestinoTransporteForm.getIdTipoDestinoTransporte());
//		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = new PaisTipoDestinoTransporteDTO();
//		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
//		try {
//			
//			poblarDTO(paisTipoDestinoTransporteForm, paisTipoDestinoTransporteDTO);
//			
//			paisTipoDestinoTransporteDN.modificar(paisTipoDestinoTransporteDTO);
//			System.out.println("\n\n\npais: " + paisTipoDestinoTransporteDTO.getId().getIdPais());
//			System.out.println("\n\n\ntipo: " + paisTipoDestinoTransporteDTO.getId().getIdTipoDestinoTransporte());
//			
//			listarTodos(request);
//			//limpiarForm(paisTipoDestinoTransporteForm);
//			
//		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		}
		
		// PRUEBA DE FILTRADO DE PAISES Q NO HAN SIDO ASOCIADOS A TIPODESTINO
//		try{
//		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN =  new PaisTipoDestinoTransporteDN();
//		List<PaisDTO> paisesFiltrados = paisTipoDestinoTransporteDN.listarPaisesNoAsociadosATipoDestino();
//		System.out.println("\n\n\n Pa�ses: \n");
//		for (PaisDTO paisDTO : paisesFiltrados) {
//			System.out.println(paisDTO.getCountryName() + "\n");
//		}
//		}catch (SystemException e){
//			e.printStackTrace();
//		}catch (ExcepcionDeAccesoADatos e){
//			e.printStackTrace();
//		}
		
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		PaisTipoDestinoTransporteForm paisTipoDestinoTransporteForm = (PaisTipoDestinoTransporteForm) form;
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = new PaisTipoDestinoTransporteDTO();
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		try {
			
			poblarDTO(paisTipoDestinoTransporteForm, paisTipoDestinoTransporteDTO);
			
			paisTipoDestinoTransporteDN.borrar(paisTipoDestinoTransporteDTO);
			listarTodos(request);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = new PaisTipoDestinoTransporteDTO();
		PaisTipoDestinoTransporteForm paisTipoDestinoTransporteForm = (PaisTipoDestinoTransporteForm) form;
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		try {
			
			String idPais = request.getParameter("idPais");
			paisTipoDestinoTransporteDTO.setIdPais(idPais);
			paisTipoDestinoTransporteDTO = paisTipoDestinoTransporteDN.getPaisTipoDestinoTransportePorId(paisTipoDestinoTransporteDTO);
			
			poblarForm(paisTipoDestinoTransporteForm, paisTipoDestinoTransporteDTO);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		try {			
			
			request.setAttribute("paisesList", paisTipoDestinoTransporteDN.listarTodosPaises());
			
			TipoDestinoTransporteDN tipoDestinoTransporteDN = new TipoDestinoTransporteDN();
			request.setAttribute("tipoDestinoTransporteList", tipoDestinoTransporteDN.listarTodos());
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward agregarborrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;	
	
		return mapping.findForward(reglaNavegacion);
	}
	
	public void mostrarPaisesPorAsociar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN =  new PaisTipoDestinoTransporteDN();
		List<PaisDTO> paisesFiltrados = paisTipoDestinoTransporteDN.listarPaisesNoAsociadosATipoDestino();
		String idTipoDestinoTransporte = request.getParameter("idTipoDestinoTransporte");
//		String json = "{rows:[";
//		if (!UtileriasWeb.esObjetoNulo(paisesFiltrados) && paisesFiltrados.size() > 0){
//			for (PaisDTO paisDTO : paisesFiltrados) {
//				json += "{id:\"" + paisDTO.getCountryId() + "\",data:[";
//				json += "\"" + idTipoDestinoTransporte + "\",";
//				json += "\"" + paisDTO.getCountryId() + "\",";
//				json += "\"" + paisDTO.getCountryName() + "\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		}else{
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if (!UtileriasWeb.esObjetoNulo(paisesFiltrados) && paisesFiltrados.size() > 0){
			for (PaisDTO paisDTO : paisesFiltrados) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(paisDTO.getCountryId());
				row.setDatos(
						idTipoDestinoTransporte,
						paisDTO.getCountryId(),
						paisDTO.getCountryName()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	public void mostrarPaisesAsociados(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		
		String idTipoDestinoTransporte = request.getParameter("idTipoDestinoTransporte");
		PaisTipoDestinoTransporteDN paisTipoDestinoTransporteDN = PaisTipoDestinoTransporteDN.getInstancia();
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = new PaisTipoDestinoTransporteDTO();
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
		tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(new BigDecimal(idTipoDestinoTransporte));
		
		paisTipoDestinoTransporteDTO.setTipoDestinoTransporte(tipoDestinoTransporteDTO);
		paisTipoDestinoTransporteDTO.setPais(new PaisDTO());
		List<PaisTipoDestinoTransporteDTO> paisTipoDestinoTransporteDTOList = paisTipoDestinoTransporteDN.listarFiltrado(paisTipoDestinoTransporteDTO);
//		String json = "{rows:[";
//		if(!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTOList) && paisTipoDestinoTransporteDTOList.size() > 0 ) {
//		for (PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO2 : paisTipoDestinoTransporteDTOList) {
//			json += "{id:\"" + paisTipoDestinoTransporteDTO2.getPais().getCountryId() + "\",data:[";
//			json += "\"" + paisTipoDestinoTransporteDTO2.getTipoDestinoTransporte().getIdTipoDestinoTransporte() + "\",";
//			json += "\"" + paisTipoDestinoTransporteDTO2.getPais().getCountryId() + "\",";
//			json += "\"" + paisTipoDestinoTransporteDTO2.getPais().getCountryName() + "\"]},";
//		}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(!UtileriasWeb.esObjetoNulo(paisTipoDestinoTransporteDTOList) && paisTipoDestinoTransporteDTOList.size() > 0 ) {
			for (PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO2 : paisTipoDestinoTransporteDTOList) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(paisTipoDestinoTransporteDTO2.getPais().getCountryId());
				row.setDatos(
						paisTipoDestinoTransporteDTO2.getTipoDestinoTransporte().getIdTipoDestinoTransporte().toString(),
						paisTipoDestinoTransporteDTO2.getPais().getCountryId(),
						paisTipoDestinoTransporteDTO2.getPais().getCountryName()
				);
				json.addRow(row);
				
			}
		}
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
}
