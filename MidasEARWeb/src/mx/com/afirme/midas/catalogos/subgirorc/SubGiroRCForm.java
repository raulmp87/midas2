package mx.com.afirme.midas.catalogos.subgirorc;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author Esteban Cabezudo
 * @since 14/08/2009
 */
public class SubGiroRCForm extends MidasBaseForm {
	private static final long serialVersionUID = 8489105806202000753L;

	private String idSubGiroRC;
	private String idGiroRC;
	private String codigo;
	private String descripcionSubGiro;	
	private String codigoSubGiroRC;
	private String descripcionGiroRC;
	private String descripcionSubGiroRC;
	private String claveTipoRiesgo;
	private String claveAutorizacion;

	public String getIdSubGiroRC() {
		return idSubGiroRC;
	}

	public void setIdSubGiroRC(String idSubGiroRC) {
		this.idSubGiroRC = idSubGiroRC;
	}

	public String getIdGiroRC() {
		return idGiroRC;
	}

	public void setIdGiroRC(String idGiroRC) {
		this.idGiroRC = idGiroRC;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcionSubGiro() {
		return descripcionSubGiro;
	}

	public void setDescripcionSubGiro(String descripcionSubGiro) {
		this.descripcionSubGiro = descripcionSubGiro;
	}

	public String getCodigoSubGiroRC() {
		return codigoSubGiroRC;
	}

	public void setCodigoSubGiroRC(String codigoSubGiroRC) {
		this.codigoSubGiroRC = codigoSubGiroRC;
	}

	public String getDescripcionGiroRC() {
		return descripcionGiroRC;
	}

	public void setDescripcionGiroRC(String descripcionGiroRC) {
		this.descripcionGiroRC = descripcionGiroRC;
	}

	public String getDescripcionSubGiroRC() {
		return descripcionSubGiroRC;
	}

	public void setDescripcionSubGiroRC(String descripcionSubGiroRC) {
		this.descripcionSubGiroRC = descripcionSubGiroRC;
	}

	public String getClaveTipoRiesgo() {
		return claveTipoRiesgo;
	}

	public void setClaveTipoRiesgo(String claveTipoRiesgo) {
		this.claveTipoRiesgo = claveTipoRiesgo;
	}

	public String getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(String claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

}
