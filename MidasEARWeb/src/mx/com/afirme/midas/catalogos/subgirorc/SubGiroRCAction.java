package mx.com.afirme.midas.catalogos.subgirorc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.girorc.GiroRCDN;
import mx.com.afirme.midas.catalogos.girorc.GiroRCDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroRCAction extends MidasMappingDispatchAction {
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;  
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		SubGiroRCDN subGiroRCDN = SubGiroRCDN.getInstancia();
		List<SubGiroRCDTO> subGirosRC = subGiroRCDN.listarTodos();
		request.setAttribute("subGirosRC", subGirosRC);
	}

	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroRCForm subGiroRCForm = (SubGiroRCForm) form;
		SubGiroRCDTO subGiroRCDTO = new SubGiroRCDTO();
		SubGiroRCDN subGiroRCDN = SubGiroRCDN.getInstancia();
		try {
			poblarDTO(subGiroRCForm, subGiroRCDTO);
			request.setAttribute("subGirosRC", subGiroRCDN.listarFiltrado(subGiroRCDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(SubGiroRCForm subGiroRCForm, SubGiroRCDTO subGiroRCDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		
		if (!UtileriasWeb.esCadenaVacia(subGiroRCForm.getIdGiroRC())) {
			GiroRCDN giroRCDN = GiroRCDN.getInstancia();
			GiroRCDTO giroRCDTO = new GiroRCDTO();
			giroRCDTO.setIdTcGiroRC(UtileriasWeb.regresaBigDecimal(subGiroRCForm.getIdGiroRC()));
			giroRCDTO = giroRCDN.getGiroRCPorId(giroRCDTO);
			subGiroRCDTO.setGiroRC(giroRCDTO);
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroRCForm.getCodigoSubGiroRC())) {
			subGiroRCDTO.setCodigoSubGiroRC(UtileriasWeb.regresaBigDecimal(subGiroRCForm.getCodigoSubGiroRC()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroRCForm.getDescripcionSubGiroRC())) {
			subGiroRCDTO.setDescripcionSubGiroRC(subGiroRCForm.getDescripcionSubGiroRC());
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroRCForm.getClaveTipoRiesgo())) {
			subGiroRCDTO.setClaveTipoRiesgo(UtileriasWeb.regresaShort(subGiroRCForm.getClaveTipoRiesgo()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroRCForm.getClaveAutorizacion())) {
			subGiroRCDTO.setClaveAutorizacion(UtileriasWeb.regresaShort(subGiroRCForm.getClaveAutorizacion()));
		}
	}

	private void poblarForm(SubGiroRCForm subGiroRCForm, SubGiroRCDTO subGiroRCDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia("" + subGiroRCDTO.getIdTcSubGiroRC())) {
			subGiroRCForm.setIdSubGiroRC(subGiroRCDTO.getIdTcSubGiroRC().toBigInteger().toString());
		}
		if (subGiroRCDTO.getGiroRC() != null) {
			subGiroRCForm.setIdGiroRC(subGiroRCDTO.getGiroRC().getIdTcGiroRC().toString());
			subGiroRCForm.setDescripcionGiroRC(subGiroRCDTO.getGiroRC().getDescripcionGiroRC());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroRCDTO.getCodigoSubGiroRC())) {
			subGiroRCForm.setCodigoSubGiroRC(subGiroRCDTO.getCodigoSubGiroRC().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroRCDTO.getDescripcionSubGiroRC())) {
			subGiroRCForm.setDescripcionSubGiroRC(subGiroRCDTO.getDescripcionSubGiroRC());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroRCDTO.getClaveTipoRiesgo())) {
			subGiroRCForm.setClaveTipoRiesgo(subGiroRCDTO.getClaveTipoRiesgo().toString());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroRCDTO.getClaveAutorizacion())) {
			subGiroRCForm.setClaveAutorizacion(subGiroRCDTO.getClaveAutorizacion().toString());
		}
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroRCForm subGiroRCForm = (SubGiroRCForm) form;
		SubGiroRCDTO subGiroRCDTO = new SubGiroRCDTO();
		SubGiroRCDN subGiroRCDN = SubGiroRCDN.getInstancia();
		try {
			poblarDTO(subGiroRCForm, subGiroRCDTO);
			subGiroRCDN.agregar(subGiroRCDTO);
			listarTodos(request);
			limpiarForm(subGiroRCForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void limpiarForm(SubGiroRCForm subGiroRCForm) {
		subGiroRCForm.setIdGiroRC("");
		subGiroRCForm.setDescripcionGiroRC("");
		subGiroRCForm.setCodigoSubGiroRC("");
		subGiroRCForm.setDescripcionSubGiroRC("");
		subGiroRCForm.setClaveTipoRiesgo("");
		subGiroRCForm.setClaveAutorizacion("");
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroRCForm subGiroRCForm = (SubGiroRCForm) form;
		SubGiroRCDTO subGiroRCDTO = new SubGiroRCDTO();
		SubGiroRCDN subGiroRCDN = SubGiroRCDN.getInstancia();
		try {
			subGiroRCDTO.setIdTcSubGiroRC(UtileriasWeb.regresaBigDecimal(subGiroRCForm.getIdSubGiroRC()));
			subGiroRCDTO = subGiroRCDN.getSubGiroRCPorId(subGiroRCDTO);
			poblarDTO(subGiroRCForm, subGiroRCDTO);
			subGiroRCDN.modificar(subGiroRCDTO);
			listarTodos(request);
			limpiarForm(subGiroRCForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroRCForm subGiroRCForm = (SubGiroRCForm) form;
		SubGiroRCDTO subGiroRCDTO = new SubGiroRCDTO();
		SubGiroRCDN subGiroRCDN = SubGiroRCDN.getInstancia();
		try {
			subGiroRCDTO.setIdTcSubGiroRC(UtileriasWeb.regresaBigDecimal(subGiroRCForm.getIdSubGiroRC()));
			subGiroRCDTO = subGiroRCDN.getSubGiroRCPorId(subGiroRCDTO);
			subGiroRCDN.borrar(subGiroRCDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}

	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroRCDTO subGiroRCDTO = new SubGiroRCDTO();
		SubGiroRCForm subGiroRCForm = (SubGiroRCForm) form;
		SubGiroRCDN subGiroRCDN = SubGiroRCDN.getInstancia();
		try {
			String id = request.getParameter("id");
			subGiroRCDTO.setIdTcSubGiroRC(UtileriasWeb.regresaBigDecimal(id));
			subGiroRCDTO = subGiroRCDN.getSubGiroRCPorId(subGiroRCDTO);
			poblarForm(subGiroRCForm, subGiroRCDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}

	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
