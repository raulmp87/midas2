package mx.com.afirme.midas.catalogos.subgirorc;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroRCDN {
	public static final SubGiroRCDN INSTANCIA = new SubGiroRCDN();

	public static SubGiroRCDN getInstancia() {
		return SubGiroRCDN.INSTANCIA;
	}

	public String agregar(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.agregar(subGiroRCDTO);
	}

	public String borrar(SubGiroRCDTO subGiroRCDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.borrar(subGiroRCDTO);
	}

	public String modificar(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.modificar(subGiroRCDTO);
	}

	public SubGiroRCDTO getSubGiroRCPorId(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.getSubgiroRCPorId(subGiroRCDTO);
	}
	
	public SubGiroRCDTO getSubGiroRCPorId(BigDecimal idTcSubGiroRC)throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.getSubgiroRCPorId(idTcSubGiroRC);
	}

	public List<SubGiroRCDTO> listarTodos() throws ExcepcionDeAccesoADatos,
			SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.listarTodos();
	}

	public List<SubGiroRCDTO> listarFiltrado(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroRCSN subGiroRCSN = new SubGiroRCSN();
		return subGiroRCSN.listarFiltrado(subGiroRCDTO);
	}

}
