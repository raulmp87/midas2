package mx.com.afirme.midas.catalogos.subgirorc;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroRCSN {

	private SubGiroRCFacadeRemote beanRemoto;

	public SubGiroRCSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(SubGiroRCFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.INFO, null);
	}

	public String agregar(SubGiroRCDTO subGiroRCDTO) throws ExcepcionDeAccesoADatos {
		LogDeMidasWeb.log("agregar(SubGiroRCDTO)", Level.INFO, null);
		beanRemoto.save(subGiroRCDTO);
		return null;
	}

	public String borrar(SubGiroRCDTO subGiroRCDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(subGiroRCDTO);
		return null;
	}

	public String modificar(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(subGiroRCDTO);
		return null;
	}

	public List<SubGiroRCDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public SubGiroRCDTO getSubgiroRCPorId(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(subGiroRCDTO.getIdTcSubGiroRC());
	}
	
	public SubGiroRCDTO getSubgiroRCPorId(BigDecimal idTcSubGiroRC) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(idTcSubGiroRC);
	}

	public List<SubGiroRCDTO> listarFiltrado(SubGiroRCDTO subGiroRCDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(subGiroRCDTO);
	}
}
