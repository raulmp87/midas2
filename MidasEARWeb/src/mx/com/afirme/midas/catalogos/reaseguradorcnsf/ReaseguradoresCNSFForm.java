package mx.com.afirme.midas.catalogos.reaseguradorcnsf;


import mx.com.afirme.midas.sistema.MidasBaseForm;
 
public class ReaseguradoresCNSFForm extends MidasBaseForm{

	private static final long serialVersionUID = -5655417772797679102L;
	private String idReasegurador;
	private String idAgencia;
	private String nombreReas;
	private String cveReas;
	private String cveReasAnt;
	private String agenciaCalificacion;
	private String calificacion;
	
	public String getIdReasegurador() {
		return idReasegurador;
	}
	public void setIdReasegurador(String idReasegurador) {
		this.idReasegurador = idReasegurador;
	}
	public String getNombreReas() {
		return nombreReas;
	}
	public void setNombreReas(String nombreReas) {
		this.nombreReas = nombreReas;
	}
	public String getCveReas() {
		return cveReas;
	}
	public void setCveReas(String cveReas) {
		this.cveReas = cveReas;
	}
	public String getCveReasAnt() {
		return cveReasAnt;
	}
	public void setCveReasAnt(String cveReasAnt) {
		this.cveReasAnt = cveReasAnt;
	}	
	public String getAgenciaCalificacion() {
		return agenciaCalificacion;
	}
	public void setAgenciaCalificacion(String agenciaCalificacion) {
		this.agenciaCalificacion = agenciaCalificacion;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}	
	public String getIdAgencia() {
		return idAgencia;
	}
	public void setIdAgencia(String idAgencia) {
		this.idAgencia = idAgencia;
	}
	
}