package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.CargaMasivaReaseguradoresExcel;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class ReaseguradoresCNSFAction extends MidasMappingDispatchAction{

	public static final String SYSTEM_EXCEPTION = "SystemException";
	public static final String DATA_EXCEPTION = "ExcepcionDe AccesoADatos";
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			limpiarForm((ReaseguradoresCNSFForm) form);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log(SYSTEM_EXCEPTION, Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void limpiarForm(ReaseguradoresCNSFForm form){
		if(form != null){
			form.setIdReasegurador("");
			form.setNombreReas("");
			form.setCalificacion("");
			form.setCveReas("");
			form.setAgenciaCalificacion("");
		}
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReaseguradoresCNSFForm calForm = (ReaseguradoresCNSFForm) form;
		ReaseguradorCnsfDTO contactoDTO = new ReaseguradorCnsfDTO();
		poblarDTO(calForm, contactoDTO);
		try {
			List<ReaseguradorCnsfDTO> contactos = ReaseguradorCNSFDN.getInstancia().listarFiltrado(contactoDTO);
			request.setAttribute("contactos", contactos);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		} catch (SystemException e) {
			LogDeMidasWeb.log(SYSTEM_EXCEPTION, Level.INFO, e);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request)throws SystemException {
		ReaseguradorCNSFDN reaseguradorCNSFDN = ReaseguradorCNSFDN.getInstancia();
		List<ReaseguradorCnsfDTO> contactos = reaseguradorCNSFDN.listarTodos();
		request.setAttribute("contactos",contactos);
		
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ReaseguradoresCNSFForm calForm = (ReaseguradoresCNSFForm) form;
		ReaseguradorCnsfDTO contactoDTO = new ReaseguradorCnsfDTO();
		this.poblarDTO(calForm, contactoDTO);
		ReaseguradorCNSFDN contactoDN = ReaseguradorCNSFDN.getInstancia();
		try {
			Usuario usuario = getUsuarioMidas(request);			
			usuario.getNombreUsuario();
			contactoDTO.setUsuario(usuario.getNombreUsuario() + "  - CCNSF");
			contactoDN.agregar(contactoDTO);
			//contactoDN.agregar(cnsfMovDTO);
			calForm.setMensajeUsuario(null,"guardar");
			
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		ReaseguradoresCNSFForm calificacionForm = (ReaseguradoresCNSFForm) form;
		ReaseguradorCnsfDTO contactoDTO = new ReaseguradorCnsfDTO();
		ReaseguradorCnsfDTO contactoDTOPer = new ReaseguradorCnsfDTO();
		ReaseguradorCorredorDTO corredorDTO = new ReaseguradorCorredorDTO();
		this.poblarDTO(calificacionForm, contactoDTO);
		ReaseguradorCNSFDN reaseguradorCNSFDN = ReaseguradorCNSFDN.getInstancia();
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
		try {
			Usuario usuario = getUsuarioMidas(request);			
			usuario.getNombreUsuario();
			contactoDTO.setUsuario(usuario.getNombreUsuario()+ "  - CCNSF");
			
			contactoDTOPer = reaseguradorCNSFDN.getPorId(contactoDTO);
			
			if(contactoDTOPer.getClaveCnsf() != contactoDTO.getClaveCnsf())
				contactoDTO.setClaveCnsfAnt(contactoDTOPer.getClaveCnsf());
			
			contactoDTO.setIdTcReasegurador(contactoDTOPer.getIdTcReasegurador());
			reaseguradorCNSFDN.modificar(contactoDTO);
			
			corredorDTO = reaseguradorCorredorDN.findByProperty("idtcreaseguradorcorredor",contactoDTOPer.getIdTcReasegurador());
			corredorDTO.setCalificacionCNSF(contactoDTO.getCalificacion());
			corredorDTO.setAgenciaCalCNSF(contactoDTO.getIdagencia());
			if(corredorDTO.getIdtcreaseguradorcorredor() != null)
				reaseguradorCorredorDN.modificar(corredorDTO);
			
			calificacionForm.setMensajeUsuario(null,"modificar");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	
	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReaseguradoresCNSFForm contactoForm = (ReaseguradoresCNSFForm) form;
		ReaseguradorCnsfDTO contactoDTO = new ReaseguradorCnsfDTO();
		ReaseguradorCnsfDTO contactoDTOPer = new ReaseguradorCnsfDTO();
		this.poblarDTO(contactoForm, contactoDTO);
		
		ReaseguradorCNSFDN reaseguradorCNSFDN = ReaseguradorCNSFDN.getInstancia();
		try {
			Usuario usuario = getUsuarioMidas(request);			
			usuario.getNombreUsuario();
			
			contactoDTOPer = reaseguradorCNSFDN.getPorId(contactoDTO);
			
			contactoDTOPer.setUsuario(usuario.getNombreUsuario()+ "  - CCNSF" +"3");
			
			reaseguradorCNSFDN.modificar(contactoDTOPer);	
			
			reaseguradorCNSFDN.borrar(contactoDTOPer);
				this.listarTodos(request);
				contactoForm.setMensajeUsuario(null,"eliminar");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		} catch (Exception e){
			contactoForm.setMensaje("No Se Pudo Borrar El Contacto Seleccionado Por Que Existen Referencias A Este Contacto");
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
			return mapping.findForward(reglaNavegacion);
		}
		
		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		ReaseguradoresCNSFForm reaseguradoresCNSFForm 		= (ReaseguradoresCNSFForm) form;
		ReaseguradorCnsfDTO contactoDTO 		= new ReaseguradorCnsfDTO();
		String id 						= request.getParameter("id");
		
		contactoDTO.setIdReasegurador(BigDecimal.valueOf(Double.valueOf(id)));
		this.poblarDTO(reaseguradoresCNSFForm, contactoDTO);
		ReaseguradorCNSFDN contactoDN = ReaseguradorCNSFDN.getInstancia();
		try {
			contactoDTO = contactoDN.getPorId(contactoDTO);
			this.poblarForm(contactoDTO, reaseguradoresCNSFForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log(SYSTEM_EXCEPTION, Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		}
 
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarBitacora(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String forward = Sistema.EXITOSO;
		String reglaNavegacion 			= Sistema.EXITOSO;
		ReaseguradoresCNSFForm reaseguradoresCNSFForm 		= (ReaseguradoresCNSFForm) form;
		ReaseguradorCNSFBitacoraDTO contactoDTO 		= new ReaseguradorCNSFBitacoraDTO();
		String nombreArchivo = "";
		StringBuilder fecha = new StringBuilder();
		
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			ReaseguradorCNSFDN contactoDN = ReaseguradorCNSFDN.getInstancia();
			
			byte [] byteArray = this.toArrayByte( contactoDN.getBitacora(contactoDTO) );;
			if ( byteArray != null){
			
				
				ZipEntry entry = new ZipEntry( "Bitacora" + ".txt");
				entry.setSize( byteArray.length );
				zos.putNextEntry( entry );
				zos.write( byteArray );
				zos.closeEntry();
				
				ZipEntry entryCSV = new ZipEntry( "Bitacora" +".csv");
				entry.setSize( byteArray.length );
				zos.putNextEntry( entryCSV );
				zos.write( byteArray );
				zos.closeEntry();
						zos.close();
				this.writeBytes(response, baos.toByteArray(), Sistema.TIPO_ZIP,"Bitacora"+ ".zip");
				baos.close();
			}
			else{
				throw new SystemException("ERROR",20);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log(SYSTEM_EXCEPTION, Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward(forward);
	}
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContactoDTO
	 * @param ReaseguradoresCNSFForm
	 **/
	private void poblarForm(ReaseguradorCnsfDTO calificacionDTO, ReaseguradoresCNSFForm reaseguradoresCNSFForm) {
		
		if (calificacionDTO.getId()!= null){ 
			reaseguradoresCNSFForm.setIdReasegurador(calificacionDTO.getIdReasegurador().toString());
		}
		
		if (calificacionDTO.getNombreReas()!= null){
			reaseguradoresCNSFForm.setNombreReas(calificacionDTO.getNombreReas());
		}
		
		if (calificacionDTO.getClaveCnsf()!= null){
			reaseguradoresCNSFForm.setCveReas(calificacionDTO.getClaveCnsf());
		}
		
		if (calificacionDTO.getClaveCnsfAnt()!= null){
			reaseguradoresCNSFForm.setCveReasAnt(calificacionDTO.getClaveCnsfAnt());
		}
		
		if (calificacionDTO.getIdagencia()!= null){
			reaseguradoresCNSFForm.setAgenciaCalificacion(calificacionDTO.getIdagencia()+"");
		}
		
		if (calificacionDTO.getCalificacion()!= null){
			reaseguradoresCNSFForm.setCalificacion(calificacionDTO.getCalificacion()+"");
		}
	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param reaseguradoresCNSFForm
	 * @param ContactoDTO
	 **/
	private void poblarDTO(ReaseguradoresCNSFForm reaseguradoresCNSFForm, ReaseguradorCnsfDTO ReaseguradorCnsfDTO) {
		BigDecimal id;
		BigDecimal valor;
	
		if (!StringUtil.isEmpty(reaseguradoresCNSFForm.getIdReasegurador())) {
			id = new BigDecimal(reaseguradoresCNSFForm.getIdReasegurador());
			ReaseguradorCnsfDTO.setIdReasegurador(id);
		}
		
		if (!StringUtil.isEmpty(reaseguradoresCNSFForm.getNombreReas())) 
			ReaseguradorCnsfDTO.setNombreReas(reaseguradoresCNSFForm.getNombreReas());
		
		
		if (!StringUtil.isEmpty(reaseguradoresCNSFForm.getCveReas())) {
			ReaseguradorCnsfDTO.setClaveCnsf(reaseguradoresCNSFForm.getCveReas());
		}
		
		if (!StringUtil.isEmpty(reaseguradoresCNSFForm.getCveReasAnt())) {
			ReaseguradorCnsfDTO.setClaveCnsfAnt(reaseguradoresCNSFForm.getCveReasAnt());
		}
		
		if (!StringUtil.isEmpty(reaseguradoresCNSFForm.getAgenciaCalificacion())) {
			valor = new BigDecimal(reaseguradoresCNSFForm.getAgenciaCalificacion());
			ReaseguradorCnsfDTO.setIdagencia(valor);
		}
		 
		if (!StringUtil.isEmpty(reaseguradoresCNSFForm.getCalificacion())) {
			ReaseguradorCnsfDTO.setCalificacion(reaseguradoresCNSFForm.getCalificacion());
		}
		
	}
	
	private byte[] toArrayByte( List<ReaseguradorCNSFBitacoraDTO> list ) throws Exception {
		
		byte[] bytes = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			out.writeBytes("Fecha movimiento|Usuario|Cambio\n");
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
			for (ReaseguradorCNSFBitacoraDTO item : list) {

				String lineSeparator = "\r\n";
				if(item.getUsuario().contains("3"))
					continue;
				else if(item.getCambio().isEmpty())
					continue;
				
				String element = sdf.format(item.getFechaMov())  + "|" + item.getUsuario() + "|" + item.getCambio()+lineSeparator;
			    out.writeBytes(element);
			}
			bytes = baos.toByteArray();
		
		return bytes;
	}
	
	
	public ActionForward mostrarCarga(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;		
		try {
			limpiarForm((ReaseguradoresCNSFForm) form);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			LogDeMidasWeb.log(SYSTEM_EXCEPTION, Level.INFO, e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			LogDeMidasWeb.log(DATA_EXCEPTION, Level.INFO, e);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward cargaEsquemasReaseguradoras(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		String idToControlArchivo = request.getParameter("idToControlArchivo");
		try {
			CargaMasivaReaseguradoresExcel<ReaseguradorCargaCNSF> cargaMasivaReasExcel = new CargaMasivaReaseguradoresExcel<ReaseguradorCargaCNSF>(ReaseguradorCargaCNSF.class);
			        						
			ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
			ControlArchivoDTO archivoDTO =  controlArchivoDN.getPorId(new BigDecimal(idToControlArchivo));
			
			String nombreOriginalArchivo = archivoDTO.getNombreArchivoOriginal();
			String extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
							nombreOriginalArchivo.lastIndexOf('.'),
							nombreOriginalArchivo.length());

			
			if(!cargaMasivaReasExcel.cargaArchivoEsquemas(new BigDecimal(idToControlArchivo),extension))
			{
				if(!".xls".equals(extension)){
					throw new SystemException("[Esquemas]Extension de archivo no Valido.: " + extension ,20);
				}
				else
				{
					throw new SystemException("[Esquemas]Formato no Valido. " ,20);
				}
					
			}else
			{
				throw new SystemException("[Esquemas]El procreso ha sido Finalizado" ,20);
			}
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no se ejecuto el proceso de Solvencia.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>" +e.getMessage());
			request.setAttribute("exc", e);
			return mapping.findForward("error");
		}
		
	} 

	
}