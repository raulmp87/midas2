package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguradorCNSFSN {  

	private ReaseguradorCnsfFacadeRemote beanRemoto;

	public ReaseguradorCNSFSN() throws SystemException {
		LogDeMidasWeb.log("Entrando enContactoSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ReaseguradorCnsfFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ReaseguradorCnsfDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ReaseguradorCnsfDTO> contactoBancos = beanRemoto.findAll();
		return contactoBancos;
	}

	public void agregar(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(ReaseguradorCnsfDTO);
	}

	public void modificar(ReaseguradorCnsfDTO ReaseguradorCnsfDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(ReaseguradorCnsfDTO);
	}

	public ReaseguradorCnsfDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		
		List<ReaseguradorCnsfDTO> agenciaDTOs =  beanRemoto.findByPropertyID(id);  
		return agenciaDTOs.get(0);
	}

	public void borrar(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(ReaseguradorCnsfDTO);
	}

	public List<ReaseguradorCnsfDTO> listarFiltrado(ReaseguradorCnsfDTO ReaseguradorCnsfDTO)
			throws ExcepcionDeAccesoADatos {
		List<ReaseguradorCnsfDTO> contactos = beanRemoto.listarFiltrado(ReaseguradorCnsfDTO);
		return contactos;
	}
	
	public List<ReaseguradorCNSFBitacoraDTO> getBitacora(BigDecimal id) throws ExcepcionDeAccesoADatos {
		
		List<ReaseguradorCNSFBitacoraDTO> agenciaDTOs =  beanRemoto.findAllBitacora();  
		return agenciaDTOs;
	}
}
