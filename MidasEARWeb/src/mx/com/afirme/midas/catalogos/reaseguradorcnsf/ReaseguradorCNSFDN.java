package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.util.List;

import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCnsfDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseguradorCNSFDN {
	
	public static final ReaseguradorCNSFDN INSTANCIA = new ReaseguradorCNSFDN();

	public static ReaseguradorCNSFDN getInstancia() {
		return ReaseguradorCNSFDN.INSTANCIA;
	}

	public List<ReaseguradorCnsfDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
		return ReaseguradorCNSFSN.listarTodos();
	}

	public void agregar(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
		ReaseguradorCNSFSN.agregar(ReaseguradorCnsfDTO);
	}
	
 
	public void modificar(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
		ReaseguradorCNSFSN.modificar(ReaseguradorCnsfDTO);
	}

	public ReaseguradorCnsfDTO getPorId(ReaseguradorCnsfDTO ReaseguradorCnsfDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
		return ReaseguradorCNSFSN.getPorId(ReaseguradorCnsfDTO.getIdReasegurador());
	}
 
	public void borrar(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
		ReaseguradorCNSFSN.borrar(ReaseguradorCnsfDTO);
	}

	public List<ReaseguradorCnsfDTO> listarFiltrado(ReaseguradorCnsfDTO ReaseguradorCnsfDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
		return ReaseguradorCNSFSN.listarFiltrado(ReaseguradorCnsfDTO);
	} 
	
	public List<ReaseguradorCNSFBitacoraDTO> getBitacora(ReaseguradorCNSFBitacoraDTO ReaseguradorCnsfDTO)
	throws SystemException, ExcepcionDeAccesoADatos {
	ReaseguradorCNSFSN ReaseguradorCNSFSN = new ReaseguradorCNSFSN();
	return ReaseguradorCNSFSN.getBitacora(ReaseguradorCnsfDTO.getIdReasBit());
	}
}
