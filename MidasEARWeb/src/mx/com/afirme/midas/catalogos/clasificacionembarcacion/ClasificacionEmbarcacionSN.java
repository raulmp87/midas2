/**
 * 
 */
package mx.com.afirme.midas.catalogos.clasificacionembarcacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ClasificacionEmbarcacionSN {
	private ClasificacionEmbarcacionFacadeRemote beanRemoto;

	public ClasificacionEmbarcacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ClasificacionEmbarcacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ClasificacionEmbarcacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ClasificacionEmbarcacionDTO> clasificacionEmbarcaciones;
		try {
			clasificacionEmbarcaciones = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return clasificacionEmbarcaciones;

	}
	
	public List<ClasificacionEmbarcacionDTO> listarFiltrados(ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO) throws ExcepcionDeAccesoADatos {
		List<ClasificacionEmbarcacionDTO> clasificacionEmbarcaciones;
		try {
			clasificacionEmbarcaciones = beanRemoto.listarFiltrado(clasificacionEmbarcacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
		return clasificacionEmbarcaciones;
	}
	
	public ClasificacionEmbarcacionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
	}
}
