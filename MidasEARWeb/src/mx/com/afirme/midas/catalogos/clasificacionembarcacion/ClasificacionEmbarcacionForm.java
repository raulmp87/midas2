/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package mx.com.afirme.midas.catalogos.clasificacionembarcacion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * MyEclipse Struts Creation date: 06-25-2009
 * 
 * XDoclet definition:
 * 
 * @struts.form name="tipoBanderaForm"
 */
public class ClasificacionEmbarcacionForm extends MidasBaseForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5252977731486236095L;

	private String idTcClasificacionEmbarcacion;
	private String codigoClasificacionEmbarcacion;
	private String descripcionClasifEmbarcacion;

	/**
	 * @return the idTcClasificacionEmbarcacion
	 */
	public String getIdTcClasificacionEmbarcacion() {
		return idTcClasificacionEmbarcacion;
	}

	/**
	 * @param idTcClasificacionEmbarcacion
	 *            the idTcClasificacionEmbarcacion to set
	 */
	public void setIdTcClasificacionEmbarcacion(
			String idTcClasificacionEmbarcacion) {
		this.idTcClasificacionEmbarcacion = idTcClasificacionEmbarcacion;
	}

	/**
	 * @return the codigoClasificacionEmbarcacion
	 */
	public String getCodigoClasificacionEmbarcacion() {
		return codigoClasificacionEmbarcacion;
	}

	/**
	 * @param codigoClasificacionEmbarcacion
	 *            the codigoClasificacionEmbarcacion to set
	 */
	public void setCodigoClasificacionEmbarcacion(
			String codigoClasificacionEmbarcacion) {
		this.codigoClasificacionEmbarcacion = codigoClasificacionEmbarcacion;
	}

	/**
	 * @return the descripcionClasifEmbarcacion
	 */
	public String getDescripcionClasifEmbarcacion() {
		return descripcionClasifEmbarcacion;
	}

	/**
	 * @param descripcionClasifEmbarcacion
	 *            the descripcionClasifEmbarcacion to set
	 */
	public void setDescripcionClasifEmbarcacion(
			String descripcionClasifEmbarcacion) {
		this.descripcionClasifEmbarcacion = descripcionClasifEmbarcacion;
	}
}