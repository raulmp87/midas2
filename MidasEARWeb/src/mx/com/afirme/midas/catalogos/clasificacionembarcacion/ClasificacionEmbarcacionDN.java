package mx.com.afirme.midas.catalogos.clasificacionembarcacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ClasificacionEmbarcacionDN {
	private static final ClasificacionEmbarcacionDN INSTANCIA = new ClasificacionEmbarcacionDN();

	public static ClasificacionEmbarcacionDN getInstancia() {
		return ClasificacionEmbarcacionDN.INSTANCIA;
	}

	public List<ClasificacionEmbarcacionDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ClasificacionEmbarcacionSN clasificacionEmbarcacionSN = new ClasificacionEmbarcacionSN();
		return clasificacionEmbarcacionSN.listarTodos();
	}	
	
	public List<ClasificacionEmbarcacionDTO> listarFiltrados(ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO)
	throws SystemException, ExcepcionDeAccesoADatos {
	ClasificacionEmbarcacionSN clasificacionEmbarcacionSN = new ClasificacionEmbarcacionSN();
	return clasificacionEmbarcacionSN.listarFiltrados(clasificacionEmbarcacionDTO);
}

	public ClasificacionEmbarcacionDTO getPorId(ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ClasificacionEmbarcacionSN clasificacionEmbarcacionSN = new ClasificacionEmbarcacionSN();
		return clasificacionEmbarcacionSN.getPorId(clasificacionEmbarcacionDTO.getIdTcClasificacionEmbarcacion());
	}
}