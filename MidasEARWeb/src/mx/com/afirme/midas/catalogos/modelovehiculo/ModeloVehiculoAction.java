package mx.com.afirme.midas.catalogos.modelovehiculo;

import static mx.com.afirme.midas.sistema.UtileriasWeb.mandaMensajeExcepcionRegistrado;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDN;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDN;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ModeloVehiculoAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		List<ModeloVehiculoDTO> listModeloVehiculo = new ArrayList<ModeloVehiculoDTO>(1);
		request.setAttribute("listModeloVehiculo", listModeloVehiculo);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoForm modeloVehiculoForm = (ModeloVehiculoForm) form;
		ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		try {
			poblarDTO(modeloVehiculoForm, modeloVehiculoDTO);
			request.setAttribute("listModeloVehiculo", modeloVehiculoDN.listarFiltrado(modeloVehiculoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(ModeloVehiculoForm modeloVehiculoForm,
			ModeloVehiculoDTO modeloVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		modeloVehiculoForm.descomponerIdEstiloVehiculo();
		filler.estableceMapeoResultados(ModeloVehiculoDTO.class, ModeloVehiculoDN.MAPEO_ATRIBUTOS_DTO, ModeloVehiculoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(modeloVehiculoDTO, modeloVehiculoForm);
	}
	
	private void poblarForm(ModeloVehiculoForm modeloVehiculoForm,ModeloVehiculoDTO modeloVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		TipoVehiculoDTO tipoVehiculoDTOTemp = new TipoVehiculoDTO();
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		MarcaVehiculoDTO marcaVehiculoDTOTemp = new MarcaVehiculoDTO();
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		
		filler.estableceMapeoResultados(ModeloVehiculoForm.class, ModeloVehiculoDN.MAPEO_ATRIBUTOS_FORM, ModeloVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(modeloVehiculoForm,modeloVehiculoDTO);
		modeloVehiculoForm.componerIdEstiloVehiculo();
		
		tipoVehiculoDTOTemp.setIdTcTipoVehiculo(modeloVehiculoDTO.getEstiloVehiculoDTO().getIdTcTipoVehiculo());
		tipoVehiculoDTO = tipoVehiculoDN.getTipoVehiculoPorId(tipoVehiculoDTOTemp);
		modeloVehiculoForm.getTipoVehiculoForm().setIdTcTipoVehiculo(modeloVehiculoDTO.getEstiloVehiculoDTO().getIdTcTipoVehiculo().toString());
		modeloVehiculoForm.getTipoVehiculoForm().setDescripcionTipoVehiculo(tipoVehiculoDTO.getDescripcionTipoVehiculo());
		modeloVehiculoForm.setIdTcTipoVehiculo(modeloVehiculoDTO.getEstiloVehiculoDTO().getIdTcTipoVehiculo().toString());
		
		marcaVehiculoDTOTemp.setIdTcMarcaVehiculo(modeloVehiculoDTO.getEstiloVehiculoDTO().getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
		marcaVehiculoDTO = marcaVehiculoDN.getMarcaVehiculoPorId(marcaVehiculoDTOTemp);
		modeloVehiculoForm.getMarcaVehiculoForm().setIdTcMarcaVehiculo(marcaVehiculoDTO.getIdTcMarcaVehiculo().toString());
		modeloVehiculoForm.getMarcaVehiculoForm().setDescripcionMarcaVehiculo(marcaVehiculoDTO.getDescripcionMarcaVehiculo());
		modeloVehiculoForm.setIdTcMarcaVehiculo(marcaVehiculoDTO.getIdTcMarcaVehiculo().toString());		
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoForm modeloVehiculoForm = (ModeloVehiculoForm) form;
		ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		try {
			poblarDTO(modeloVehiculoForm, modeloVehiculoDTO);
			modeloVehiculoDN.agregar(modeloVehiculoDTO,UtileriasWeb.obtieneNombreUsuario(request));
			//listarTodos(request);
			limpiarForm(modeloVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoForm modeloVehiculoForm = (ModeloVehiculoForm) form;
		ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		try {
			poblarDTO(modeloVehiculoForm, modeloVehiculoDTO);
			modeloVehiculoDTO = modeloVehiculoDN.getModeloVehiculoPorId(modeloVehiculoDTO);
			poblarDTO(modeloVehiculoForm, modeloVehiculoDTO);
			modeloVehiculoDN.modificar(modeloVehiculoDTO);
			//listarTodos(request);
			limpiarForm(modeloVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoForm modeloVehiculoForm = (ModeloVehiculoForm) form;
		ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		try {
			poblarDTO(modeloVehiculoForm, modeloVehiculoDTO);
			modeloVehiculoDN.borrar(modeloVehiculoDTO);
			limpiarForm(modeloVehiculoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoForm modeloVehiculoForm = (ModeloVehiculoForm) form;
		ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		try {
			String claveEstilo = request.getParameter("claveEstilo");
			String claveTipoBien = request.getParameter("claveTipoBien");
			String idVersionCarga = request.getParameter("idVersionCarga");
			String modeloVehiculo = request.getParameter("modeloVehiculo");
			String idMoneda = request.getParameter("idMoneda");
			ModeloVehiculoId id = new ModeloVehiculoId();
			id.setClaveEstilo(claveEstilo);
			id.setClaveTipoBien(claveTipoBien);
			id.setIdVersionCarga(UtileriasWeb.regresaBigDecimal(idVersionCarga));
			id.setIdMoneda(UtileriasWeb.regresaBigDecimal(idMoneda));
			id.setModeloVehiculo(new Short(modeloVehiculo));
			modeloVehiculoDTO.setId(id);
			modeloVehiculoDTO = modeloVehiculoDN.getModeloVehiculoPorId(modeloVehiculoDTO);
			poblarForm(modeloVehiculoForm, modeloVehiculoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward descargar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoForm modeloVehiculoForm = (ModeloVehiculoForm) form;
		ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		try {
			poblarDTO(modeloVehiculoForm, modeloVehiculoDTO);
			List<ModeloVehiculoDTO> listaModeloVehiculo = modeloVehiculoDN.listarFiltrado(modeloVehiculoDTO);
			ExcelExporter exporter = new ExcelExporter( ModeloVehiculoDTO.class );
			
			TransporteImpresionDTO transporte =  exporter.exportXLS(listaModeloVehiculo, "Lista_Modelo_Vehiculo");
			this.writeBytes(response, transporte.getGenericInputStream(), ConstantesReporte.TIPO_XLS, transporte.getFileName(), "");
		} catch (IOException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(ModeloVehiculoForm form){
		form.setIdTcMarcaVehiculo("");
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(ModeloVehiculoForm.class, ModeloVehiculoDN.MAPEO_ATRIBUTOS_FORM, ModeloVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}

	public ActionForward actualizar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){

		String reglaNavegacion = Sistema.EXITOSO;
		ModeloVehiculoDN modeloVehiculoDN = ModeloVehiculoDN.getInstancia();
		String idToControlArchivo = request.getParameter("idToControlArchivo");
		try {
			modeloVehiculoDN.actualizarMasivo(idToControlArchivo);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
}