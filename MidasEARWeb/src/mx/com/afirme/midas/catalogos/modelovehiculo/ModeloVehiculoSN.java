package mx.com.afirme.midas.catalogos.modelovehiculo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ModeloVehiculoSN {

	private ModeloVehiculoFacadeRemote beanRemoto;

	public ModeloVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ModeloVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto ModeloVehiculo instanciado", Level.FINEST, null);
	}
	
	public void agregar(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(modeloVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(modeloVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(modeloVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ModeloVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ModeloVehiculoDTO getModeloVehiculoPorId(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(modeloVehiculoDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ModeloVehiculoDTO> listarFiltrado(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(modeloVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public String actualizarMasivo(String idToControlArchivo)throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.actualizarMasivo(idToControlArchivo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
