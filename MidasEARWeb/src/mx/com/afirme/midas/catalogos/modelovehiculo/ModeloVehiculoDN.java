package mx.com.afirme.midas.catalogos.modelovehiculo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ModeloVehiculoDN {
	public static final ModeloVehiculoDN INSTANCIA = new ModeloVehiculoDN();

	public static final String[] MAPEO_ATRIBUTOS_DTO={
		"id.claveTipoBien",
		"id.claveEstilo",
		"id.idVersionCarga",
		"id.modeloVehiculo",
		"id.idMoneda",
		"valorNuevo",
		"valorComercial",
		"claveCondRiesgo",
		"estiloVehiculoDTO.descripcionEstilo",
		"monedaDTO.descripcion",
		"valorCaratula"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={
		"id_claveTipoBien",
		"id_claveEstilo",
		"id_idVersionCarga",
		"id_modeloVehiculo",
		"id_idMoneda",
		"valorNuevo",
		"valorComercial",
		"claveCondRiesgo",
		"descripcionEstiloVehiculo",
		"descripcionMoneda",
		"valorCaratula"};
	
	public static ModeloVehiculoDN getInstancia (){
		return ModeloVehiculoDN.INSTANCIA;
	}
	
	public void agregar(ModeloVehiculoDTO modeloVehiculoDTO,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		modeloVehiculoDTO.setCodigoUsuarioCreacion(nombreUsuario);
		modeloVehiculoDTO.setFechaCreacion(new java.util.Date());
		new ModeloVehiculoSN().agregar(modeloVehiculoDTO);
	}
	
	public void borrar (ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ModeloVehiculoSN().borrar(modeloVehiculoDTO);
	}
	
	public void modificar (ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new ModeloVehiculoSN().modificar(modeloVehiculoDTO);
	}
	
	public ModeloVehiculoDTO getModeloVehiculoPorId(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new ModeloVehiculoSN().getModeloVehiculoPorId(modeloVehiculoDTO);
	}
	
	public List<ModeloVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new ModeloVehiculoSN().listarTodos();
	}
	
	public List<ModeloVehiculoDTO> listarFiltrado(ModeloVehiculoDTO modeloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new ModeloVehiculoSN().listarFiltrado(modeloVehiculoDTO);
	}

	public String actualizarMasivo(String idToControlArchivo) throws ExcepcionDeAccesoADatos, SystemException{
		return new ModeloVehiculoSN().actualizarMasivo(idToControlArchivo);
	}
}
