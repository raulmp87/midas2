package mx.com.afirme.midas.catalogos.modelovehiculo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoForm;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ModeloVehiculoForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2594915206135914531L;
	private String id_claveTipoBien;
    private String id_claveEstilo;
    private String id_idVersionCarga;
    /*
     * Atributo en el que se guardar�n los datos de claveTipoBien, claveEstilo y versionCarga
     * separados por un "_".
     */
//    private String idCompuestoTipoVehiculo;
    private TipoVehiculoForm tipoVehiculoForm=new TipoVehiculoForm();
    private MarcaVehiculoForm marcaVehiculoForm=new MarcaVehiculoForm();
    private String idCompuestoTcVehiculo;
    private String idTcTipoVehiculo;
    private String idTcMarcaVehiculo;
    private String id_idMoneda;
    private String id_modeloVehiculo;
    private String valorNuevo;
    private String valorComercial;
    private String claveCondRiesgo;
    private String fechaCreacion;
    private String codigoUsuarioCreacion;
    
    //Campos usados para mostrar descripcion de los ID's
    private String descripcionEstiloVehiculo;
    private String descripcionMoneda;
    
    private String valorCaratula;
    
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
		if (UtileriasWeb.esCadenaVacia(id_claveTipoBien)) {
			errors.add(
					"id_claveTipoBien",
					new ActionMessage(
							"errors.required",
							UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"catalogos.estilovehiculo.id_claveTipoBien")));
		}
		if (UtileriasWeb.esCadenaVacia(idTcTipoVehiculo)) {
			errors.add(
					"idTcTipoVehiculo",
					new ActionMessage("errors.required", UtileriasWeb
							.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
									"catalogos.estilovehiculo.tipoVehiculo")));
		}
		if (UtileriasWeb.esCadenaVacia(idTcMarcaVehiculo)) {
			errors.add(
					"idTcMarcaVehiculo",
					new ActionMessage("errors.required", UtileriasWeb
							.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
									"catalogos.estilovehiculo.marcaVehiculo")));
		}
		if (UtileriasWeb.esCadenaVacia(id_idVersionCarga)) {
			errors.add(
					"id_idVersionCarga",
					new ActionMessage(
							"errors.required",
							UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"catalogos.estilovehiculo.id_idVersionCarga")));
		}	
		
		return errors;
	}
	
	public void descomponerIdEstiloVehiculo(){
    	if(idCompuestoTcVehiculo != null){
    		String[] ids = idCompuestoTcVehiculo.split("_");
    		if(ids != null && ids.length == 3){
    			id_claveTipoBien = ids[0];
    			id_claveEstilo = ids[1];
    			id_idVersionCarga = ids[2];
    		}
    	}
    }
    
    public void componerIdEstiloVehiculo(){
    	if(id_claveTipoBien != null && id_claveEstilo!=null && id_idVersionCarga!=null){
    		idCompuestoTcVehiculo = id_claveTipoBien+"_"+id_claveEstilo+"_"+id_idVersionCarga;
    	}
    }
    
	public String getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}

	public void setIdTcTipoVehiculo(String idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}

	public String getIdTcMarcaVehiculo() {
		return idTcMarcaVehiculo;
	}

	public void setIdTcMarcaVehiculo(String idTcMarcaVehiculo) {
		this.idTcMarcaVehiculo = idTcMarcaVehiculo;
	}

	public String getId_claveTipoBien() {
		return id_claveTipoBien;
	}
	
	public void setId_claveTipoBien(String idClaveTipoBien) {
		id_claveTipoBien = idClaveTipoBien;
	}
	
	public String getId_claveEstilo() {
		return id_claveEstilo;
	}
	
	public void setId_claveEstilo(String idClaveEstilo) {
		id_claveEstilo = idClaveEstilo;
	}
	
	public String getId_modeloVehiculo() {
		return id_modeloVehiculo;
	}
	
	public void setId_modeloVehiculo(String idModeloVehiculo) {
		id_modeloVehiculo = idModeloVehiculo;
	}
	
	public String getId_idMoneda() {
		return id_idMoneda;
	}
	
	public void setId_idMoneda(String idIdMoneda) {
		id_idMoneda = idIdMoneda;
	}
	
	public String getId_idVersionCarga() {
		return id_idVersionCarga;
	}
	
	public void setId_idVersionCarga(String idIdVersionCarga) {
		id_idVersionCarga = idIdVersionCarga;
	}
	
	public String getIdCompuestoTcVehiculo() {
		return idCompuestoTcVehiculo;
	}

	public void setIdCompuestoTcVehiculo(String idCompuestoTcVehiculo) {
		this.idCompuestoTcVehiculo = idCompuestoTcVehiculo;
	}

	public String getValorNuevo() {
		return valorNuevo;
	}
	
	public void setValorNuevo(String valorNuevo) {
		this.valorNuevo = valorNuevo;
	}
	
	public String getValorComercial() {
		return valorComercial;
	}
	
	public void setValorComercial(String valorComercial) {
		this.valorComercial = valorComercial;
	}
	
	public String getClaveCondRiesgo() {
		return claveCondRiesgo;
	}
	
	public void setClaveCondRiesgo(String claveCondRiesgo) {
		this.claveCondRiesgo = claveCondRiesgo;
	}
	
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	public String getDescripcionEstiloVehiculo() {
		return descripcionEstiloVehiculo;
	}
	
	public void setDescripcionEstiloVehiculo(String descripcionEstiloVehiculo) {
		this.descripcionEstiloVehiculo = descripcionEstiloVehiculo;
	}
	
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public TipoVehiculoForm getTipoVehiculoForm() {
		return tipoVehiculoForm;
	}
	public void setTipoVehiculoForm(TipoVehiculoForm tipoVehiculoForm) {
		this.tipoVehiculoForm = tipoVehiculoForm;
	}
	public MarcaVehiculoForm getMarcaVehiculoForm() {
		return marcaVehiculoForm;
	}
	public void setMarcaVehiculoForm(MarcaVehiculoForm marcaVehiculoForm) {
		this.marcaVehiculoForm = marcaVehiculoForm;
	}

	public String getValorCaratula() {
		return valorCaratula;
	}

	public void setValorCaratula(String valorCaratula) {
		this.valorCaratula = valorCaratula;
	}
	
}
