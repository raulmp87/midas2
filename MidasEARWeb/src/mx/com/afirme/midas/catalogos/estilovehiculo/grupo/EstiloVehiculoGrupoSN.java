package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstiloVehiculoGrupoSN {
	private EstiloVehiculoGrupoFacadeRemote beanRemoto;

	public EstiloVehiculoGrupoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(EstiloVehiculoGrupoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(EstiloVehiculoGrupoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoTechoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(EstiloVehiculoGrupoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoTechoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(EstiloVehiculoGrupoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoTechoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<EstiloVehiculoGrupoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public EstiloVehiculoGrupoDTO getEstiloVehiculoGrupoPorId(EstiloVehiculoGrupoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoTechoDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<EstiloVehiculoGrupoDTO> listarFiltrado(EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(estiloVehiculoGrupoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
