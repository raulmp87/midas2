package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class EstiloVehiculoGrupoForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6879035848489338632L;
	private String id_claveTipoBien;
    private String id_claveEstilo;
    private String id_idVersionCarga;
    private String idCompuestoTcVehiculo;
    private String idTcTipoVehiculo;
    private String idTcMarcaVehiculo;
    
    private String claveAMIS;
    private String fechaInicioVigencia;
    private String fechaFinVigencia;
    private String idGrupoDM;
    private String idGrupoRT;
    private String idGrupoRC;

  //Campos usados para mostrar descripcion de los ID's
    private String descripcionEstiloVehiculo;
         
    
    public void descomponerIdEstiloVehiculo(){
    	if(idCompuestoTcVehiculo != null){
    		String[] ids = idCompuestoTcVehiculo.split("_");
    		if(ids != null && ids.length == 3){
    			id_claveTipoBien = ids[0];
    			id_claveEstilo = ids[1];
    			id_idVersionCarga = ids[2];
    		}
    	}
    }
    
    public void componerIdEstiloVehiculo(){
    	if(id_claveTipoBien != null && id_claveEstilo!=null && id_idVersionCarga!=null){
    		idCompuestoTcVehiculo = id_claveTipoBien+"_"+id_claveEstilo+"_"+id_idVersionCarga;
    	}
    }
	public String getIdCompuestoTcVehiculo() {
		return idCompuestoTcVehiculo;
	}
	public void setIdCompuestoTcVehiculo(String idCompuestoTcVehiculo) {
		this.idCompuestoTcVehiculo = idCompuestoTcVehiculo;
	}
	public String getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}
	public void setIdTcTipoVehiculo(String idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}
	public String getIdTcMarcaVehiculo() {
		return idTcMarcaVehiculo;
	}
	public void setIdTcMarcaVehiculo(String idTcMarcaVehiculo) {
		this.idTcMarcaVehiculo = idTcMarcaVehiculo;
	}
	public String getId_claveTipoBien() {
		return id_claveTipoBien;
	}
	public void setId_claveTipoBien(String idClaveTipoBien) {
		id_claveTipoBien = idClaveTipoBien;
	}
	public String getId_claveEstilo() {
		return id_claveEstilo;
	}
	public void setId_claveEstilo(String idClaveEstilo) {
		id_claveEstilo = idClaveEstilo;
	}
	public String getId_idVersionCarga() {
		return id_idVersionCarga;
	}
	public void setId_idVersionCarga(String idIdVersionCarga) {
		id_idVersionCarga = idIdVersionCarga;
	}
	public String getClaveAMIS() {
		return claveAMIS;
	}
	public void setClaveAMIS(String claveAMIS) {
		this.claveAMIS = claveAMIS;
	}
	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getIdGrupoDM() {
		return idGrupoDM;
	}
	public void setIdGrupoDM(String idGrupoDM) {
		this.idGrupoDM = idGrupoDM;
	}
	public String getIdGrupoRT() {
		return idGrupoRT;
	}
	public void setIdGrupoRT(String idGrupoRT) {
		this.idGrupoRT = idGrupoRT;
	}
	public String getIdGrupoRC() {
		return idGrupoRC;
	}
	public void setIdGrupoRC(String idGrupoRC) {
		this.idGrupoRC = idGrupoRC;
	}
	public String getDescripcionEstiloVehiculo() {
		return descripcionEstiloVehiculo;
	}
	public void setDescripcionEstiloVehiculo(String descripcionEstiloVehiculo) {
		this.descripcionEstiloVehiculo = descripcionEstiloVehiculo;
	}

}
