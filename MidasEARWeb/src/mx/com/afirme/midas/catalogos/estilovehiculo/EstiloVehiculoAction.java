package mx.com.afirme.midas.catalogos.estilovehiculo;

import static mx.com.afirme.midas.sistema.UtileriasWeb.mandaMensajeExcepcionRegistrado;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EstiloVehiculoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		/*try {
			//tengo 
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}*/
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		List<EstiloVehiculoDTO> listEstiloVehiculo = estiloVehiculoDN.listarTodos();
		request.setAttribute("listEstiloVehiculo", listEstiloVehiculo);
	}

	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoForm estiloVehiculoForm = (EstiloVehiculoForm) form;
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		try {
			poblarDTO(estiloVehiculoForm, estiloVehiculoDTO);
			List<EstiloVehiculoDTO> listarFiltrado=estiloVehiculoDN.listarFiltrado(estiloVehiculoDTO);
			LogDeMidasWeb.log("xxlistarFiltrado",Level.INFO, null);			

			request.setAttribute("listEstiloVehiculo",listarFiltrado );
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(EstiloVehiculoForm estiloVehiculoForm,
			EstiloVehiculoDTO estiloVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(EstiloVehiculoDTO.class, EstiloVehiculoDN.MAPEO_ATRIBUTOS_DTO, EstiloVehiculoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(estiloVehiculoDTO, estiloVehiculoForm);
		if(estiloVehiculoForm.getTipoSubMarcaVacio()!=null &&  estiloVehiculoForm.getTipoSubMarcaVacio()){
			estiloVehiculoDTO.setTipoSubMarcaVacio(true);
		}else{
			estiloVehiculoDTO.setTipoSubMarcaVacio(false);	
		}
		if(estiloVehiculoDTO.getId() != null && UtileriasWeb.esCadenaVacia(estiloVehiculoDTO.getId().getClaveTipoBien())){
			estiloVehiculoDTO.setTipoBienAutosDTO(null);
		}
		if(estiloVehiculoDTO.getSubMarcaVehiculoDTO() == null || estiloVehiculoDTO.getSubMarcaVehiculoDTO().getIdSubTcMarcaVehiculo()==null){
			estiloVehiculoDTO.setSubMarcaVehiculoDTO(null);

		}
		
		
		else if(estiloVehiculoDTO.getTipoBienAutosDTO() != null){
			estiloVehiculoDTO.getTipoBienAutosDTO().setClaveTipoBien(estiloVehiculoDTO.getId().getClaveTipoBien());
		}
	}
	
	private void poblarForm(EstiloVehiculoForm estiloVehiculoForm,
		EstiloVehiculoDTO estiloVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(EstiloVehiculoForm.class, EstiloVehiculoDN.MAPEO_ATRIBUTOS_FORM, EstiloVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(estiloVehiculoForm, estiloVehiculoDTO);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoForm estiloVehiculoForm = (EstiloVehiculoForm) form;
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		try {
			poblarDTO(estiloVehiculoForm, estiloVehiculoDTO);
			//estiloVehiculoDTO.setTipoBienAutosDTO(null);
			estiloVehiculoDN.agregar(estiloVehiculoDTO,UtileriasWeb.obtieneNombreUsuario(request));
			listarTodos(request);
			limpiarForm(estiloVehiculoForm);
			String Path=forward.getPath();
			forward.setPath(Path + "?mensaje=guardado&tipoMensaje="+Sistema.EXITO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			estiloVehiculoForm.setMensaje("Verifique la informaci\u00f3n registrada");
			estiloVehiculoForm.setTipoMensaje(Sistema.ERROR);
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
			estiloVehiculoForm.setMensaje("El c\u00f3digo introducido ya ha sido usado en otro registro");
			estiloVehiculoForm.setTipoMensaje(Sistema.ERROR);
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
//		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoForm estiloVehiculoForm = (EstiloVehiculoForm) form;
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		SubMarcaVehiculoDTO subMarcaVehiculoDTO = new SubMarcaVehiculoDTO();
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		try {
			poblarDTO(estiloVehiculoForm, estiloVehiculoDTO);
			estiloVehiculoDTO = estiloVehiculoDN.getEstiloVehiculoPorId(estiloVehiculoDTO);
			estiloVehiculoDTO.setSubMarcaVehiculoDTO(subMarcaVehiculoDTO);
			poblarDTO(estiloVehiculoForm, estiloVehiculoDTO);			
			estiloVehiculoDN.modificar(estiloVehiculoDTO);
			listarTodos(request);
			limpiarForm(estiloVehiculoForm);
		} catch (SystemException e) {
			estiloVehiculoForm.setMensaje("Verifique la Informacion Registrada");
			estiloVehiculoForm.setTipoMensaje(Sistema.ERROR);
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
			estiloVehiculoForm.setMensaje("El c�digo introducido ya ha sido usado en otro registro");
			estiloVehiculoForm.setTipoMensaje(Sistema.ERROR);
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return forward;
//		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoForm estiloVehiculoForm = (EstiloVehiculoForm) form;
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		try {
			estiloVehiculoDTO.getId().setClaveEstilo(estiloVehiculoForm.getId_claveEstilo());
			estiloVehiculoDTO.getId().setClaveTipoBien(estiloVehiculoForm.getId_claveTipoBien());
			estiloVehiculoDTO.getId().setIdVersionCarga(UtileriasWeb.regresaBigDecimal(estiloVehiculoForm.getId_idVersionCarga()));
//			estiloVehiculoDTO = estiloVehiculoDN.getEstiloVehiculoPorId(estiloVehiculoDTO);
			estiloVehiculoDN.borrar(estiloVehiculoDTO);
			limpiarForm(estiloVehiculoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoForm estiloVehiculoForm = (EstiloVehiculoForm) form;
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		try {
			String claveEstilo = request.getParameter("claveEstilo");
			String claveTipoBien = request.getParameter("claveTipoBien");
			String idVersionCarga = request.getParameter("idVersionCarga");
			EstiloVehiculoId id = new EstiloVehiculoId();
			id.setClaveEstilo(claveEstilo);
			id.setClaveTipoBien(claveTipoBien);
			id.setIdVersionCarga(UtileriasWeb.regresaBigDecimal(idVersionCarga));
			estiloVehiculoDTO.setId(id);
			estiloVehiculoDTO = estiloVehiculoDN.getEstiloVehiculoPorId(estiloVehiculoDTO);
			poblarForm(estiloVehiculoForm, estiloVehiculoDTO);
			poblarCaracteristicas(estiloVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		poblarCaracteristicas((EstiloVehiculoForm) form);
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void poblarCaracteristicas(EstiloVehiculoForm estiloVehiculoForm){
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		VarModifDescripcion varModifDescripcion = new VarModifDescripcion();		
		try {
			varModifDescripcion.setIdGrupo(10);
			List<VarModifDescripcion> transmisionList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setTransmisionList(transmisionList);
			
			varModifDescripcion.setIdGrupo(30);
			List<VarModifDescripcion> bolsasAireList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setBolsasDeAireList(bolsasAireList);
			
			varModifDescripcion.setIdGrupo(40);
			List<VarModifDescripcion> sistemaElectricoList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setSistemaElectricoList(sistemaElectricoList);
			
			varModifDescripcion.setIdGrupo(60);
			List<VarModifDescripcion> quemacocosList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setQuemacocosList(quemacocosList);
			
			varModifDescripcion.setIdGrupo(80);
			List<VarModifDescripcion> frenosList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setFrenosList(frenosList);
			
			varModifDescripcion.setIdGrupo(100);
			List<VarModifDescripcion> aireAcondicionadoList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setAireAcondicionadoList(aireAcondicionadoList);
			
			varModifDescripcion.setIdGrupo(120);
			List<VarModifDescripcion> alarmaFabricanteList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setAlarmaFabricanteList(alarmaFabricanteList);
			
			varModifDescripcion.setIdGrupo(130);
			List<VarModifDescripcion> inyeccionList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setInyeccionList(inyeccionList);
			
			varModifDescripcion.setIdGrupo(110);
			List<VarModifDescripcion> combustibleList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setCombustibleList(combustibleList);

			varModifDescripcion.setIdGrupo(90);
			List<VarModifDescripcion> sonidoList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setSonidoList(sonidoList);
			
			varModifDescripcion.setIdGrupo(70);
			List<VarModifDescripcion> versionList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setVersionList(versionList);
			
			varModifDescripcion.setIdGrupo(50);
			List<VarModifDescripcion> vestidurasList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setVestidurasList(vestidurasList);
			
			varModifDescripcion.setIdGrupo(20);
			List<VarModifDescripcion> numeroCilindrosList = estiloVehiculoDN.findByFiltersModifDescripcion(varModifDescripcion);
			estiloVehiculoForm.setNumeroCilindrosList(numeroCilindrosList);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void limpiarForm(EstiloVehiculoForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(EstiloVehiculoForm.class, EstiloVehiculoDN.MAPEO_ATRIBUTOS_FORM, EstiloVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
	public ActionForward descargar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoForm estiloVehiculoForm = (EstiloVehiculoForm) form;
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		try {
			poblarDTO(estiloVehiculoForm, estiloVehiculoDTO);
			List<EstiloVehiculoDTO> listaVehiculo = estiloVehiculoDN.listarFiltrado(estiloVehiculoDTO);
			ExcelExporter exporter = new ExcelExporter( EstiloVehiculoDTO.class );			
			TransporteImpresionDTO transporte =  exporter.exportXLS(listaVehiculo, "Lista_Estilo_Vehiculo");
			this.writeBytes(response, transporte.getGenericInputStream(), ConstantesReporte.TIPO_XLS, transporte.getFileName(), "");
		} catch (IOException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward actualizar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){

		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		String idToControlArchivo = request.getParameter("idToControlArchivo");

		try {
			ControlArchivoDTO controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idToControlArchivo));
			estiloVehiculoDN.actualizarMasivo(idToControlArchivo);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
}
