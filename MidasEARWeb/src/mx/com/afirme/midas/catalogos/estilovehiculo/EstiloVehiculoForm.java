package mx.com.afirme.midas.catalogos.estilovehiculo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoForm;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoForm;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosForm;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class EstiloVehiculoForm extends MidasBaseForm{
	private static final long serialVersionUID = -2015527792133978323L;
	private String id_claveTipoBien;
    private String id_claveEstilo;
    private String id_idVersionCarga;
//    private String idCompuestoTipoVehiculo;
    private TipoVehiculoForm tipoVehiculoForm=new TipoVehiculoForm();
    private MarcaVehiculoForm marcaVehiculoForm=new MarcaVehiculoForm();
    private SubMarcaVehiculoForm subMarcaVehiculoForm=new SubMarcaVehiculoForm();
    private TipoBienAutosForm tipoBienAutosForm=new TipoBienAutosForm();//Llave foranea, no editable
    private String fechaInicioVigencia;
    private String fechaFinVigencia;
    private String descripcionEstilo;
    private String numeroAsientos;
    private String numeroPuertas;
    private String capacidadToneladas;
    private String claveAlarmaFabricante;
    private String claveUnidadMedida;
    private String claveCilindros;
    private String claveTipoInyeccion;
    private String claveFrenos;
    private String claveAireAcondicionado;
    private String claveSistemaElectrico;
    private String claveVestidura;
    private String claveSonido;
    private String claveQuemaCocos;
    private String claveBolsasAire;
    private String claveTransmision;
    private String claveCatalogoFabricante;
    private String claveVersion;
    private String claveTipoCombustible;
    private String fechaCreacion;
    private String codigoUsuarioCreacion;
    private Boolean altoRiesgo;
    private Boolean altaFrecuencia;
    private Boolean tipoSubMarcaVacio;
    //
    private List<VarModifDescripcion> transmisionList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> bolsasDeAireList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> sistemaElectricoList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> quemacocosList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> frenosList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> aireAcondicionadoList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> alarmaFabricanteList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> inyeccionList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> combustibleList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> sonidoList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> versionList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> vestidurasList = new ArrayList<VarModifDescripcion>(1);
    private List<VarModifDescripcion> numeroCilindrosList = new ArrayList<VarModifDescripcion>(1);
    
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		
		if (errors == null) {
			errors = new ActionErrors();
		}
		if (UtileriasWeb.esCadenaVacia(numeroAsientos)) {
			errors.add(
					"numeroAsientos",
					new ActionMessage(
							"errors.required",
							UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"catalogos.estilovehiculo.numeroAsientos")));
		}
		if (UtileriasWeb.esCadenaVacia(descripcionEstilo)) {
			errors.add(
					"descripcionEstilo",
					new ActionMessage(
							"errors.required",
							UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"catalogos.estilovehiculo.descripcionEstilo")));
		}
		if (UtileriasWeb.esCadenaVacia(numeroPuertas)) {
			errors.add(
					"numeroPuertas",
					new ActionMessage( 
							"errors.required",
							UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"catalogos.estilovehiculo.numeroPuertas")));
		}	
		if (mapping.getParameter().equalsIgnoreCase("agregar") && subMarcaVehiculoForm != null && StringUtils.isEmpty(subMarcaVehiculoForm.getIdSubTcMarcaVehiculo()) ) {
			errors.add(
					"submarcaVehiculoForm.idSubTcMarcaVehiculo",
					new ActionMessage(
							"errors.required",
							UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"catalogos.estilovehiculo.submarcaVehiculo")));
		}
//		if (UtileriasWeb.esCadenaVacia("idCompuestoTipoVehiculo")) {
//			errors.add(
//					"idCompuestoTipoVehiculo",
//					new ActionMessage(
//							"errors.required",
//							UtileriasWeb
//									.getMensajeRecurso(
//											Sistema.ARCHIVO_RECURSOS,
//											"catalogos.estilovehiculo.tipoVehiculo")));
//		}			
		return errors;
	}    
    
//	public String getIdCompuestoTipoVehiculo() {
//		return idCompuestoTipoVehiculo;
//	}
//
//	public void setIdCompuestoTipoVehiculo(String idCompuestoTipoVehiculo) {
//		this.idCompuestoTipoVehiculo = idCompuestoTipoVehiculo;
//	}
	
	public String getId_claveTipoBien() {
		return id_claveTipoBien;
	}
	public void setId_claveTipoBien(String idClaveTipoBien) {
		id_claveTipoBien = idClaveTipoBien;
	}
	public String getId_claveEstilo() {
		return id_claveEstilo;
	}
	public void setId_claveEstilo(String idClaveEstilo) {
		id_claveEstilo = idClaveEstilo;
	}
	public String getId_idVersionCarga() {
		return id_idVersionCarga;
	}
	public void setId_idVersionCarga(String idIdVersionCarga) {
		id_idVersionCarga = idIdVersionCarga;
	}
	public TipoVehiculoForm getTipoVehiculoForm() {
		return tipoVehiculoForm;
	}
	public void setTipoVehiculoForm(TipoVehiculoForm tipoVehiculoForm) {
		this.tipoVehiculoForm = tipoVehiculoForm;
	}
	public MarcaVehiculoForm getMarcaVehiculoForm() {
		return marcaVehiculoForm;
	}
	public void setMarcaVehiculoForm(MarcaVehiculoForm marcaVehiculoForm) {
		this.marcaVehiculoForm = marcaVehiculoForm;
	}
	public TipoBienAutosForm getTipoBienAutosForm() {
		return tipoBienAutosForm;
	}
	public void setTipoBienAutosForm(TipoBienAutosForm tipoBienAutosForm) {
		this.tipoBienAutosForm = tipoBienAutosForm;
	}
	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getDescripcionEstilo() {
		return descripcionEstilo;
	}
	public void setDescripcionEstilo(String descripcionEstilo) {
		this.descripcionEstilo = descripcionEstilo;
	}
	public String getNumeroAsientos() {
		return numeroAsientos;
	}
	public void setNumeroAsientos(String numeroAsientos) {
		this.numeroAsientos = numeroAsientos;
	}
	public String getNumeroPuertas() {
		return numeroPuertas;
	}
	public void setNumeroPuertas(String numeroPuertas) {
		this.numeroPuertas = numeroPuertas;
	}
	public String getCapacidadToneladas() {
		return capacidadToneladas;
	}
	public void setCapacidadToneladas(String capacidadToneladas) {
		this.capacidadToneladas = capacidadToneladas;
	}
	public String getClaveAlarmaFabricante() {
		return claveAlarmaFabricante;
	}
	public void setClaveAlarmaFabricante(String claveAlarmaFabricante) {
		this.claveAlarmaFabricante = claveAlarmaFabricante;
	}
	public String getClaveUnidadMedida() {
		return claveUnidadMedida;
	}
	public void setClaveUnidadMedida(String claveUnidadMedida) {
		this.claveUnidadMedida = claveUnidadMedida;
	}
	public String getClaveCilindros() {
		return claveCilindros;
	}
	public void setClaveCilindros(String claveCilindros) {
		this.claveCilindros = claveCilindros;
	}
	public String getClaveTipoInyeccion() {
		return claveTipoInyeccion;
	}
	public void setClaveTipoInyeccion(String claveTipoInyeccion) {
		this.claveTipoInyeccion = claveTipoInyeccion;
	}
	public String getClaveFrenos() {
		return claveFrenos;
	}
	public void setClaveFrenos(String claveFrenos) {
		this.claveFrenos = claveFrenos;
	}
	public String getClaveAireAcondicionado() {
		return claveAireAcondicionado;
	}
	public void setClaveAireAcondicionado(String claveAireAcondicionado) {
		this.claveAireAcondicionado = claveAireAcondicionado;
	}
	public String getClaveSistemaElectrico() {
		return claveSistemaElectrico;
	}
	public void setClaveSistemaElectrico(String claveSistemaElectrico) {
		this.claveSistemaElectrico = claveSistemaElectrico;
	}
	public String getClaveVestidura() {
		return claveVestidura;
	}
	public void setClaveVestidura(String claveVestidura) {
		this.claveVestidura = claveVestidura;
	}
	public String getClaveSonido() {
		return claveSonido;
	}
	public void setClaveSonido(String claveSonido) {
		this.claveSonido = claveSonido;
	}
	public String getClaveQuemaCocos() {
		return claveQuemaCocos;
	}
	public void setClaveQuemaCocos(String claveQuemaCocos) {
		this.claveQuemaCocos = claveQuemaCocos;
	}
	public String getClaveBolsasAire() {
		return claveBolsasAire;
	}
	public void setClaveBolsasAire(String claveBolsasAire) {
		this.claveBolsasAire = claveBolsasAire;
	}
	public String getClaveTransmision() {
		return claveTransmision;
	}
	public void setClaveTransmision(String claveTransmision) {
		this.claveTransmision = claveTransmision;
	}
	public String getClaveCatalogoFabricante() {
		return claveCatalogoFabricante;
	}
	public void setClaveCatalogoFabricante(String claveCatalogoFabricante) {
		this.claveCatalogoFabricante = claveCatalogoFabricante;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public String getClaveVersion() {
		return claveVersion;
	}
	public void setClaveVersion(String claveVersion) {
		this.claveVersion = claveVersion;
	}
	public String getClaveTipoCombustible() {
		return claveTipoCombustible;
	}
	public void setClaveTipoCombustible(String claveTipoCombustible) {
		this.claveTipoCombustible = claveTipoCombustible;
	}

	public Boolean getAltoRiesgo() {
		return altoRiesgo;
	}

	public void setAltoRiesgo(Boolean altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}

	public Boolean getAltaFrecuencia() {
		return altaFrecuencia;
	}

	public void setAltaFrecuencia(Boolean altaFrecuencia) {
		this.altaFrecuencia = altaFrecuencia;
	}

	public void setTransmisionList(List<VarModifDescripcion> transmisionList) {
		this.transmisionList = transmisionList;
	}

	public List<VarModifDescripcion> getTransmisionList() {
		return transmisionList;
	}

	public void setBolsasDeAireList(List<VarModifDescripcion> bolsasDeAireList) {
		this.bolsasDeAireList = bolsasDeAireList;
	}

	public List<VarModifDescripcion> getBolsasDeAireList() {
		return bolsasDeAireList;
	}

	public void setSistemaElectricoList(List<VarModifDescripcion> sistemaElectricoList) {
		this.sistemaElectricoList = sistemaElectricoList;
	}

	public List<VarModifDescripcion> getSistemaElectricoList() {
		return sistemaElectricoList;
	}

	public void setQuemacocosList(List<VarModifDescripcion> quemacocosList) {
		this.quemacocosList = quemacocosList;
	}

	public List<VarModifDescripcion> getQuemacocosList() {
		return quemacocosList;
	}

	public void setFrenosList(List<VarModifDescripcion> frenosList) {
		this.frenosList = frenosList;
	}

	public List<VarModifDescripcion> getFrenosList() {
		return frenosList;
	}

	public void setAireAcondicionadoList(List<VarModifDescripcion> aireAcondicionadoList) {
		this.aireAcondicionadoList = aireAcondicionadoList;
	}

	public List<VarModifDescripcion> getAireAcondicionadoList() {
		return aireAcondicionadoList;
	}

	public void setAlarmaFabricanteList(List<VarModifDescripcion> alarmaFabricanteList) {
		this.alarmaFabricanteList = alarmaFabricanteList;
	}

	public List<VarModifDescripcion> getAlarmaFabricanteList() {
		return alarmaFabricanteList;
	}

	public void setInyeccionList(List<VarModifDescripcion> inyeccionList) {
		this.inyeccionList = inyeccionList;
	}

	public List<VarModifDescripcion> getInyeccionList() {
		return inyeccionList;
	}

	public void setCombustibleList(List<VarModifDescripcion> combustibleList) {
		this.combustibleList = combustibleList;
	}

	public List<VarModifDescripcion> getCombustibleList() {
		return combustibleList;
	}

	public void setSonidoList(List<VarModifDescripcion> sonidoList) {
		this.sonidoList = sonidoList;
	}

	public List<VarModifDescripcion> getSonidoList() {
		return sonidoList;
	}

	public void setVersionList(List<VarModifDescripcion> versionList) {
		this.versionList = versionList;
	}

	public List<VarModifDescripcion> getVersionList() {
		return versionList;
	}

	public void setVestidurasList(List<VarModifDescripcion> vestidurasList) {
		this.vestidurasList = vestidurasList;
	}

	public List<VarModifDescripcion> getVestidurasList() {
		return vestidurasList;
	}

	public void setNumeroCilindrosList(List<VarModifDescripcion> numeroCilindrosList) {
		this.numeroCilindrosList = numeroCilindrosList;
	}

	public List<VarModifDescripcion> getNumeroCilindrosList() {
		return numeroCilindrosList;
	}

	public SubMarcaVehiculoForm getSubMarcaVehiculoForm() {
		return subMarcaVehiculoForm;
	}

	public void setSubMarcaVehiculoForm(SubMarcaVehiculoForm subMarcaVehiculoForm) {
		this.subMarcaVehiculoForm = subMarcaVehiculoForm;
	}

	public Boolean getTipoSubMarcaVacio() {
		return tipoSubMarcaVacio;
	}

	public void setTipoSubMarcaVacio(Boolean tipoSubMarcaVacio) {
		this.tipoSubMarcaVacio = tipoSubMarcaVacio;
	}


	
}
