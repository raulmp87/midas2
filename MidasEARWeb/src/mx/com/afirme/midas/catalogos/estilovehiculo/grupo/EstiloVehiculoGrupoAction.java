package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDN;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDN;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EstiloVehiculoGrupoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		List<EstiloVehiculoGrupoDTO> listEstiloVehiculoGrupo = new ArrayList<EstiloVehiculoGrupoDTO>();
		request.setAttribute("listEstiloVehiculoGrupo", listEstiloVehiculoGrupo);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoGrupoForm estiloVehiculoGrupoForm = (EstiloVehiculoGrupoForm) form;
		EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = new EstiloVehiculoGrupoDTO();
		EstiloVehiculoGrupoDN estiloVehiculoGrupoDN = EstiloVehiculoGrupoDN.getInstancia();
		try {
			poblarDTO(estiloVehiculoGrupoForm, estiloVehiculoGrupoDTO);
			request.setAttribute("listEstiloVehiculoGrupo", estiloVehiculoGrupoDN.listarFiltrado(estiloVehiculoGrupoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(EstiloVehiculoGrupoForm estiloVehiculoGrupoForm,
			EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		estiloVehiculoGrupoForm.descomponerIdEstiloVehiculo();
		filler.estableceMapeoResultados(EstiloVehiculoGrupoDTO.class, EstiloVehiculoGrupoDN.MAPEO_ATRIBUTOS_DTO, EstiloVehiculoGrupoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(estiloVehiculoGrupoDTO, estiloVehiculoGrupoForm);
	}
	
	private void poblarForm(EstiloVehiculoGrupoForm estiloVehiculoGrupoForm,EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws SystemException {
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		EstiloVehiculoDTO estiloVehiculoDTOTemp = new EstiloVehiculoDTO();
		EstiloVehiculoDN estiloVehiculoDN = EstiloVehiculoDN.getInstancia();
		
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(EstiloVehiculoGrupoForm.class, EstiloVehiculoGrupoDN.MAPEO_ATRIBUTOS_FORM, EstiloVehiculoGrupoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(estiloVehiculoGrupoForm,estiloVehiculoGrupoDTO);
		estiloVehiculoGrupoForm.componerIdEstiloVehiculo();
		
		estiloVehiculoDTOTemp.getId().setClaveTipoBien(estiloVehiculoGrupoDTO.getId().getClaveTipoBien());
		estiloVehiculoDTOTemp.getId().setClaveEstilo(estiloVehiculoGrupoDTO.getId().getClaveEstilo());
		estiloVehiculoDTOTemp.getId().setIdVersionCarga(estiloVehiculoGrupoDTO.getId().getIdVersionCarga());
		estiloVehiculoDTO = estiloVehiculoDN.getEstiloVehiculoPorId(estiloVehiculoDTOTemp);
		estiloVehiculoGrupoForm.setIdTcMarcaVehiculo(estiloVehiculoDTO.getMarcaVehiculoDTO().getIdTcMarcaVehiculo().toString());
		estiloVehiculoGrupoForm.setIdTcTipoVehiculo(estiloVehiculoDTO.getIdTcTipoVehiculo().toString());
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoGrupoForm estiloVehiculoGrupoForm = (EstiloVehiculoGrupoForm) form;
		EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = new EstiloVehiculoGrupoDTO();
		EstiloVehiculoGrupoDN estiloVehiculoGrupoDN = EstiloVehiculoGrupoDN.getInstancia();
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		String Path=forward.getPath();
		try {
			poblarDTO(estiloVehiculoGrupoForm, estiloVehiculoGrupoDTO);
			estiloVehiculoGrupoDN.agregar(estiloVehiculoGrupoDTO,UtileriasWeb.obtieneNombreUsuario(request));
			listarTodos(request);
			limpiarForm(estiloVehiculoGrupoForm);
			mensajeExitoAgregar(estiloVehiculoGrupoForm);
		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);			
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			Path=forward.getPath();
			estiloVehiculoGrupoForm.setMensaje("El codigo introducido ya ha sido usado en otro registro");
			estiloVehiculoGrupoForm.setTipoMensaje(Sistema.ERROR);
			forward.setPath(Path + "?mensaje=El c\u00F3digo introducido ya ha sido usado en otro registro&tipoMensaje="+Sistema.ERROR);
		}
		return forward;
//		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoGrupoForm estiloVehiculoGrupoForm = (EstiloVehiculoGrupoForm) form;
		EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = new EstiloVehiculoGrupoDTO();
		EstiloVehiculoGrupoDN estiloVehiculoGrupoDN = EstiloVehiculoGrupoDN.getInstancia();
		ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		String Path=forward.getPath();
		try {
			poblarDTO(estiloVehiculoGrupoForm, estiloVehiculoGrupoDTO);
			estiloVehiculoGrupoDTO = estiloVehiculoGrupoDN.getEstiloVehiculoGrupoPorId(estiloVehiculoGrupoDTO);
			poblarDTO(estiloVehiculoGrupoForm, estiloVehiculoGrupoDTO);
			estiloVehiculoGrupoDN.modificar(estiloVehiculoGrupoDTO);
			listarTodos(request);
			limpiarForm(estiloVehiculoGrupoForm);
		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			forward.setPath(Path + "?mensaje=guardado&tipoMensaje="+Sistema.EXITO);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);			
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			Path=forward.getPath();
			estiloVehiculoGrupoForm.setMensaje("El codigo introducido ya ha sido usado en otro registro");
			estiloVehiculoGrupoForm.setTipoMensaje(Sistema.ERROR);
			forward.setPath(Path + "?mensaje=El codigo introducido ya ha sido usado en otro registro&tipoMensaje="+Sistema.ERROR);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoGrupoForm estiloVehiculoGrupoForm = (EstiloVehiculoGrupoForm) form;
		EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = new EstiloVehiculoGrupoDTO();
		EstiloVehiculoGrupoDN estiloVehiculoGrupoDN = EstiloVehiculoGrupoDN.getInstancia();
		try {
			poblarDTO(estiloVehiculoGrupoForm, estiloVehiculoGrupoDTO);
			estiloVehiculoGrupoDTO = estiloVehiculoGrupoDN.getEstiloVehiculoGrupoPorId(estiloVehiculoGrupoDTO);
			estiloVehiculoGrupoDN.borrar(estiloVehiculoGrupoDTO);
			limpiarForm(estiloVehiculoGrupoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = new EstiloVehiculoGrupoDTO();
		EstiloVehiculoGrupoForm estiloVehiculoGrupoForm = (EstiloVehiculoGrupoForm) form;
		EstiloVehiculoGrupoDN estiloVehiculoGrupoDN = EstiloVehiculoGrupoDN.getInstancia();
		try {
			String claveEstilo = request.getParameter("claveEstilo");
			String claveTipoBien = request.getParameter("claveTipoBien");
			String idVersionCarga = request.getParameter("idVersionCarga");
			EstiloVehiculoGrupoId id = new EstiloVehiculoGrupoId();
			id.setClaveEstilo(claveEstilo);
			id.setClaveTipoBien(claveTipoBien);
			id.setIdVersionCarga(UtileriasWeb.regresaBigDecimal(idVersionCarga));
			estiloVehiculoGrupoDTO.setId(id);
			estiloVehiculoGrupoDTO = estiloVehiculoGrupoDN.getEstiloVehiculoGrupoPorId(estiloVehiculoGrupoDTO);
			poblarForm(estiloVehiculoGrupoForm, estiloVehiculoGrupoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(EstiloVehiculoGrupoForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(EstiloVehiculoGrupoForm.class, EstiloVehiculoGrupoDN.MAPEO_ATRIBUTOS_FORM, EstiloVehiculoGrupoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
}
