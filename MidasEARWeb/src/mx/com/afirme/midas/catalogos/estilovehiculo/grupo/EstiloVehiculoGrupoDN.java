package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstiloVehiculoGrupoDN {
	public static final EstiloVehiculoGrupoDN INSTANCIA = new EstiloVehiculoGrupoDN();

	public static final String[] MAPEO_ATRIBUTOS_DTO={
		"id.claveTipoBien",
		"id.claveEstilo",
		"id.idVersionCarga",
		"claveAMIS",
		"fechaInicioVigencia",
		"fechaFinVigencia",
		"idGrupoDM",
		"idGrupoRT",
		"idGrupoRC",
		"estiloVehiculoDTO.descripcionEstilo"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={
		"id_claveTipoBien",
		"id_claveEstilo",
		"id_idVersionCarga",
		"claveAMIS",
		"fechaInicioVigencia",
		"fechaFinVigencia",
		"idGrupoDM",
		"idGrupoRT",
		"idGrupoRC",
		"descripcionEstiloVehiculo"};
	
	public static EstiloVehiculoGrupoDN getInstancia (){
		return EstiloVehiculoGrupoDN.INSTANCIA;
	}
	
	public void agregar(EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		estiloVehiculoGrupoDTO.setFechaCreacion(new Date());
		estiloVehiculoGrupoDTO.setCodigoUsuarioCreacion(nombreUsuario);
		new EstiloVehiculoGrupoSN().agregar(estiloVehiculoGrupoDTO);
	}
	
	public void borrar (EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new EstiloVehiculoGrupoSN().borrar(estiloVehiculoGrupoDTO);
	}
	
	public void modificar (EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new EstiloVehiculoGrupoSN().modificar(estiloVehiculoGrupoDTO);
	}
	
	public EstiloVehiculoGrupoDTO getEstiloVehiculoGrupoPorId(EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoGrupoSN().getEstiloVehiculoGrupoPorId(estiloVehiculoGrupoDTO);
	}
	
	public List<EstiloVehiculoGrupoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoGrupoSN().listarTodos();
	}
	
	public List<EstiloVehiculoGrupoDTO> listarFiltrado(EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoGrupoSN().listarFiltrado(estiloVehiculoGrupoDTO);
	}
}
