package mx.com.afirme.midas.catalogos.estilovehiculo;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;

public class EstiloVehiculoDN {
	public static final EstiloVehiculoDN INSTANCIA = new EstiloVehiculoDN();
	public static final String[] MAPEO_ATRIBUTOS_DTO={
		"id.claveTipoBien",
		"id.claveEstilo",
		"id.idVersionCarga",
		"idTcTipoVehiculo",
		"altoRiesgo",
		"altaFrecuencia",
		"marcaVehiculoDTO.idTcMarcaVehiculo",
		"marcaVehiculoDTO.descripcionMarcaVehiculo",
		"subMarcaVehiculoDTO.idSubTcMarcaVehiculo",
		"subMarcaVehiculoDTO.descripcionSubMarcaVehiculo",
		"tipoBienAutosDTO.claveTipoBien",
		"tipoBienAutosDTO.descripcionTipoBien",
		"fechaInicioVigencia",
		"fechaFinVigencia",
		"descripcionEstilo",
		"numeroAsientos",
		"numeroPuertas",
		"capacidadToneladas",
		"claveAlarmaFabricante",
		"claveUnidadMedida",
		"claveCilindros",
     	"claveTipoInyeccion",
     	"claveFrenos",
     	"claveAireAcondicionado",
     	"claveSistemaElectrico",
     	"claveVestidura",
     	"claveSonido",
     	"claveQuemaCocos",
     	"claveBolsasAire",
		"claveTransmision",
     	"claveCatalogoFabricante",
		"claveVersion",
		"claveTipoCombustible,",
		"tipoSubMarcaVacio"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={
		"id_claveTipoBien",
		"id_claveEstilo",
		"id_idVersionCarga",
		"tipoVehiculoForm.idTcTipoVehiculo",
		"altoRiesgo",
		"altaFrecuencia",
		"marcaVehiculoForm.idTcMarcaVehiculo",
		"marcaVehiculoForm.descripcionMarcaVehiculo",
		"subMarcaVehiculoForm.idSubTcMarcaVehiculo",
		"subMarcaVehiculoForm.descripcionSubMarcaVehiculo",
		"id_claveTipoBien",
		"tipoBienAutosForm.descripcionTipoBien",
		"fechaInicioVigencia",
	    "fechaFinVigencia",
	    "descripcionEstilo",
	    "numeroAsientos",
	    "numeroPuertas",
	    "capacidadToneladas",
	    "claveAlarmaFabricante",
	    "claveUnidadMedida",
	    "claveCilindros",
	    "claveTipoInyeccion",
	    "claveFrenos",
	    "claveAireAcondicionado",
	    "claveSistemaElectrico",
	    "claveVestidura",
	    "claveSonido",
	    "claveQuemaCocos",
	    "claveBolsasAire",
	    "claveTransmision",
	    "claveCatalogoFabricante",
	    "claveVersion",
		"claveTipoCombustible",
		"tipoSubMarcaVacio"};

	public static EstiloVehiculoDN getInstancia (){
		return EstiloVehiculoDN.INSTANCIA;
	}
	
	public void agregar(EstiloVehiculoDTO estiloVehiculoDTO,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		estiloVehiculoDTO.setFechaCreacion(new Date());
		estiloVehiculoDTO.setCodigoUsuarioCreacion(nombreUsuario);
		new EstiloVehiculoSN().agregar(estiloVehiculoDTO);
	}
	
	public void borrar (EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new EstiloVehiculoSN().borrar(estiloVehiculoDTO);
	}
	
	public void modificar (EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new EstiloVehiculoSN().modificar(estiloVehiculoDTO);
	}
	
	public EstiloVehiculoDTO getEstiloVehiculoPorId(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoSN().getEstiloVehiculoPorId(estiloVehiculoDTO);
	}
	
	public List<EstiloVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoSN().listarTodos();
	}
	
	public List<EstiloVehiculoDTO> listarFiltrado(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoSN().listarFiltrado(estiloVehiculoDTO);
	}
	
	public List<VarModifDescripcion> findByFiltersModifDescripcion(VarModifDescripcion arg0) throws SystemException {
		return new EstiloVehiculoSN().findByFiltersModifDescripcion(arg0);
	}
	public String actualizarMasivo(String idToControlArchivo) throws ExcepcionDeAccesoADatos, SystemException{
		return new EstiloVehiculoSN().actualizarMasivo(idToControlArchivo);
	}
}
