package mx.com.afirme.midas.catalogos.estilovehiculo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;

public class EstiloVehiculoSN {
	private EstiloVehiculoFacadeRemote beanRemoto;

	public EstiloVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(EstiloVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(estiloVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(estiloVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(estiloVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<EstiloVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public EstiloVehiculoDTO getEstiloVehiculoPorId(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(estiloVehiculoDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<EstiloVehiculoDTO> listarFiltrado(EstiloVehiculoDTO estiloVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(estiloVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<VarModifDescripcion> findByFiltersModifDescripcion(VarModifDescripcion arg0) {
		return beanRemoto.findByFiltersModifDescripcion(arg0);
	}
	
	public String actualizarMasivo(String idToControlArchivo)throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.actualizarMasivo(idToControlArchivo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

			
}
