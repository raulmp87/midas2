/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package mx.com.afirme.midas.catalogos.zonacresta;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * MyEclipse Struts Creation date: 06-25-2009
 * 
 * XDoclet definition:
 * 
 * @struts.form name="ajustadorForm"
 */
public class ZonaCrestaViejoForm extends MidasBaseForm {

	/**
	 * private BigDecimal idtczonacrestaviejo;
     private String geocodigoviejo;
     private String nombreareaviejo;
     private BigDecimal numeroareaviejo;
     private BigDecimal tipozona;
	 */
	private static final long serialVersionUID = -1L;
	private String idTcZonaCrestaViejo;
	private String nombreAreaViejo;
	private String numeroAreaViejo;
	private String geoCodigoViejo;
	private String tipoZona;

	/**
	 * @return the idTcZonaCrestaViejo
	 */
	public String getIdTcZonaCrestaViejo() {
		return idTcZonaCrestaViejo;
	}

	/**
	 * @param idTcZonaCrestaViejo
	 *            the idTcZonaCrestaViejo to set
	 */
	public void setIdTcZonaCrestaViejo(String idTcZonaCrestaViejo) {
		this.idTcZonaCrestaViejo = idTcZonaCrestaViejo;
	}

	/**
	 * @return the nombreAreaViejo
	 */
	public String getNombreAreaViejo() {
		return nombreAreaViejo;
	}

	/**
	 * @param nombreAreaViejo
	 *            the nombreAreaViejo to set
	 */
	public void setNombreAreaViejo(String nombreAreaViejo) {
		this.nombreAreaViejo = nombreAreaViejo;
	}

	/**
	 * @return the numeroAreaViejo
	 */
	public String getNumeroAreaViejo() {
		return numeroAreaViejo;
	}

	/**
	 * @param numeroAreaViejo
	 *            the numeroAreaViejo to set
	 */
	public void setNumeroAreaViejo(String numeroAreaViejo) {
		this.numeroAreaViejo = numeroAreaViejo;
	}

	/**
	 * @return the geoCodigoViejo
	 */
	public String getGeoCodigoViejo() {
		return geoCodigoViejo;
	}

	/**
	 * @param geoCodigoViejo
	 *            the geoCodigoViejo to set
	 */
	public void setGeoCodigoViejo(String geoCodigoViejo) {
		this.geoCodigoViejo = geoCodigoViejo;
	}

	/**
	 * @return the tipoZona
	 */
	public String getTipoZona() {
		return tipoZona;
	}

	/**
	 * @param tipoZona
	 *            the tipoZona to set
	 */
	public void setTipoZona(String tipoZona) {
		this.tipoZona = tipoZona;
	}
}