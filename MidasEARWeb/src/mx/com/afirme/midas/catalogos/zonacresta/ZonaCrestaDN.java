package mx.com.afirme.midas.catalogos.zonacresta;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ZonaCrestaDN {
	private static final ZonaCrestaDN INSTANCIA = new ZonaCrestaDN();

	public static ZonaCrestaDN getInstancia() {
		return ZonaCrestaDN.INSTANCIA;
	}
	
	/**
	 * Zonas Cresta 
	 */
	public List<ZonaCrestaDTO> listarTodosZonaCrestaNuevo() throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.listarTodosZonaCrestaNuevo();
	}
	
	public List<ZonaCrestaDTO> listarZonaCrestaNuevoFiltrado(ZonaCrestaDTO zonaCrestaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.listarZonaCrestaNuevoFiltrado(zonaCrestaDTO);
	}
	
	public String agregarZonaCrestaNuevo(ZonaCrestaDTO zonaCresta) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.agregarZonaCrestaNuevo(zonaCresta);
		return null;
	}
	
	public String modificarZonaCrestaNuevo(ZonaCrestaDTO zonaCresta) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.modificarZonaCrestaNuevo(zonaCresta);
		return null;
	}
	
	public ZonaCrestaDTO getZonaCrestaNuevoPorId(BigDecimal id) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.getZonaCrestaNuevoPorId(id);
	}
	
	public String borrarZonaCrestaNuevo(ZonaCrestaDTO zonaCrestaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.borrarZonaCrestaNuevo(zonaCrestaDTO);
		return null;
	}
	
	
	/**
	 * Para zonas cresta viejas
	 * @throws ExcepcionDeAccesoADatos 
	 * @throws SystemException 
	 */
	
	public List<ZonaCrestaViejoDTO> listarTodasZonasCrestaViejos() throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.listarTodasZonasCrestaViejos();
	}
	
	public List<ZonaCrestaViejoDTO> listarZonasCrestaViejosAsignadas(BigDecimal idNuevo) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.listarZonasCrestaViejosAsignadas(idNuevo);
	}
	
	public String agregarZonaCrestaViejo(ZonaCrestaViejoDTO zonaCresta) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.agregarZonaCrestaViejo(zonaCresta);
		return null;
	}
	
	public String borrarZonaCrestaViejo(ZonaCrestaViejoDTO zonaCrestaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.borrarZonaCrestaViejo(zonaCrestaDTO);
		return null;
	}
	
	public List<ZonaCrestaViejoDTO> listarFiltradoZonasCrestaViejos(ZonaCrestaViejoDTO zonaCrestaViejoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.listarZonasCrestaViejosFiltradas(zonaCrestaViejoDTO);
	}
	
	/**
	 * Para la tr
	 */
	public String agregarTrZonasCresta(ZonaCrestaNuevaViejoDTO trZonasCresta) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.agregarTrZonasCresta(trZonasCresta);
		return null;
	}
	
	public boolean yaExisteTr(ZonaCrestaNuevaViejoId trId) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.yaExisteTr(trId);
	}
	
	public String borrarTr(ZonaCrestaNuevaViejoId trId) throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		zonaCrestaSN.borrarTr(trId);
		return null;
	}
	
	public List<ZonaCrestaNuevaViejoDTO> listarTr() throws ExcepcionDeAccesoADatos, SystemException{
		ZonaCrestaSN zonaCrestaSN = new ZonaCrestaSN();
		return zonaCrestaSN.listarTr();
	}
	
}
