/**
 * 
 */
package mx.com.afirme.midas.catalogos.zonacresta;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.zonacresta.ZonaCrestaDTO;
import mx.com.afirme.midas.catalogos.zonacresta.ZonaCrestaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ZonaCrestaSN {
	private ZonaCrestaFacadeRemote beanRemotoZonaCrestaNuevo;
	private ZonaCrestaViejoFacadeRemote beanRemotoZonaCrestaViejo;
	private ZonaCrestaNuevaViejoFacadeRemote beanRemotoTrNuevoViejo;

	public ZonaCrestaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemotoZonaCrestaNuevo = serviceLocator.getEJB(ZonaCrestaFacadeRemote.class);
			beanRemotoZonaCrestaViejo = serviceLocator.getEJB(ZonaCrestaViejoFacadeRemote.class);
			beanRemotoTrNuevoViejo = serviceLocator.getEJB(ZonaCrestaNuevaViejoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	/*
	 * Accedemos a zonas cresta nuevos
	 */
	public List<ZonaCrestaDTO> listarTodosZonaCrestaNuevo() throws ExcepcionDeAccesoADatos {
		List<ZonaCrestaDTO> zonasCresta;
		try {
			zonasCresta = beanRemotoZonaCrestaNuevo.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return zonasCresta;
	}
	
	public List<ZonaCrestaDTO> listarZonaCrestaNuevoFiltrado(ZonaCrestaDTO zonaCrestaDTO) throws ExcepcionDeAccesoADatos {
		List<ZonaCrestaDTO> zonasCresta;
		try {
			zonasCresta = beanRemotoZonaCrestaNuevo.listarFiltrado(zonaCrestaDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return zonasCresta;
	}

	public String agregarZonaCrestaNuevo(ZonaCrestaDTO zonaCresta)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemotoZonaCrestaNuevo.save(zonaCresta);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String modificarZonaCrestaNuevo(ZonaCrestaDTO zonaCresta)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemotoZonaCrestaNuevo.update(zonaCresta);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public ZonaCrestaDTO getZonaCrestaNuevoPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemotoZonaCrestaNuevo.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public String borrarZonaCrestaNuevo(ZonaCrestaDTO zonaCrestaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemotoZonaCrestaNuevo.delete(zonaCrestaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	
	/*
	 *Accedemos a Zonas Cresta Viejos
	 */
	public List<ZonaCrestaViejoDTO> listarTodasZonasCrestaViejos() throws ExcepcionDeAccesoADatos {
		List<ZonaCrestaViejoDTO> zonasViejasAsignadas;
		try {
			zonasViejasAsignadas = beanRemotoZonaCrestaViejo.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return zonasViejasAsignadas;
	}
	
	public List<ZonaCrestaViejoDTO> listarZonasCrestaViejosAsignadas(BigDecimal idNuevo) throws ExcepcionDeAccesoADatos {
		List<ZonaCrestaViejoDTO> zonasViejasAsignadas;
		try {
			zonasViejasAsignadas = beanRemotoZonaCrestaViejo.buscarFiltradoSegunIdNuevo(idNuevo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return zonasViejasAsignadas;
	}
	
	public List<ZonaCrestaViejoDTO> listarZonasCrestaViejosFiltradas(ZonaCrestaViejoDTO zonaCrestaViejoDTO) throws ExcepcionDeAccesoADatos {
		List<ZonaCrestaViejoDTO> zonasViejas;
		try {
			zonasViejas = beanRemotoZonaCrestaViejo.listarFiltrado(zonaCrestaViejoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return zonasViejas;
	}

	public String agregarZonaCrestaViejo(ZonaCrestaViejoDTO zonaCresta)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemotoZonaCrestaViejo.save(zonaCresta);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String borrarZonaCrestaViejo(ZonaCrestaViejoDTO zonaCrestaDTO)
		throws ExcepcionDeAccesoADatos {
		try {
			beanRemotoZonaCrestaViejo.delete(zonaCrestaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	/*
	 *Accedemos a La Tr 
	 */
	public String agregarTrZonasCresta(ZonaCrestaNuevaViejoDTO trZonasCresta)
		throws ExcepcionDeAccesoADatos{
		try {
			beanRemotoTrNuevoViejo.save(trZonasCresta);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public boolean yaExisteTr(ZonaCrestaNuevaViejoId trId)
		throws ExcepcionDeAccesoADatos{
		try {
			return beanRemotoTrNuevoViejo.equals(trId);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public String borrarTr(ZonaCrestaNuevaViejoId trId)
		throws ExcepcionDeAccesoADatos{
		try {
			ZonaCrestaNuevaViejoDTO zonaCrestaNuevaViejoDTO = beanRemotoTrNuevoViejo.findById(trId);
			beanRemotoTrNuevoViejo.delete(zonaCrestaNuevaViejoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<ZonaCrestaNuevaViejoDTO> listarTr() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemotoTrNuevoViejo.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	
}
