/**
 * 
 */
package mx.com.afirme.midas.catalogos.recargovario;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;

/**
 * @author admin
 * 
 */
public class RecargoVarioSN {
	private RecargoVarioFacadeRemote beanRemoto;

	public RecargoVarioSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RecargoVarioFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RecargoVarioDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<RecargoVarioDTO> recargoVarios;
		try {
			recargoVarios = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return recargoVarios;
	}

	public List<RecargoVarioDTO> listarFiltrados(RecargoVarioDTO recargoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		List<RecargoVarioDTO> recargoVarios;
		try {
			recargoVarios = beanRemoto.listarFiltrado(recargoVarioDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return recargoVarios;

	}

	public void agregar(RecargoVarioDTO recargoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(recargoVarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(RecargoVarioDTO recargoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(recargoVarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RecargoVarioDTO getPorId(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}

	}

	public void borrar(RecargoVarioDTO recargoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(recargoVarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RecargoVarioDTO> listarRecargosPorAsociar(
			BigDecimal idToProducto) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarRecargosPorAsociar(idToProducto);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}

	public List<RecargoVarioDTO> listarRecargosPorAsociarTipoPoliza(
			BigDecimal idToTipoPoliza)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarRecargosPorAsociarTipoPoliza(
					idToTipoPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}

	public List<RecargoVarioDTO> listarRecargosPorAsociarCobertura(
			BigDecimal idToCobertura) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarRecargosPorAsociarCobertura(idToCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}

	public List<RecargoVarioDTO> listarRecargosEspeciales() {
		try {
			return beanRemoto.listarRecargosEspeciales();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
	}
}
