package mx.com.afirme.midas.catalogos.recargovario;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RecargoVarioDN {
	private static final RecargoVarioDN INSTANCIA = new RecargoVarioDN();

	public static RecargoVarioDN getInstancia() {
		return RecargoVarioDN.INSTANCIA;
	}

	public List<RecargoVarioDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.listarTodos();
	}

	public List<RecargoVarioDTO> listarFiltrados(RecargoVarioDTO recargoVarioDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.listarFiltrados(recargoVarioDTO);
	}

	public void agregar(RecargoVarioDTO recargoVarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		recargoVarioSN.agregar(recargoVarioDTO);
	}

	public void modificar(RecargoVarioDTO recargoVarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		recargoVarioSN.modificar(recargoVarioDTO);
	}

	public RecargoVarioDTO getPorId(RecargoVarioDTO recargoVarioDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.getPorId(recargoVarioDTO.getIdtorecargovario());
	}

	public void borrar(RecargoVarioDTO recargoVarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		recargoVarioSN.borrar(recargoVarioDTO);
	}

	public List<RecargoVarioDTO> listarRecargosEspeciales() throws SystemException {
		RecargoVarioSN recargoVarioSN = new RecargoVarioSN();
		return recargoVarioSN.listarRecargosEspeciales();
	}
}
