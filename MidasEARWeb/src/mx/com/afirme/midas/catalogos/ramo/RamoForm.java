package mx.com.afirme.midas.catalogos.ramo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class RamoForm extends MidasBaseForm{
	
	/**
	 *@author Christian Ceballos
	 *@since 23/07/2009
	 *@modificacion Jose Luis Arellano
	 *@since 05/08/09
	 */
	private static final long serialVersionUID = -4426430534960591954L;

	private String idRamo;
	private String descripcion;
	private String idEmision;
	private String idTecnico;
	private String codigo;
	

	public String getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(String idRamo) {
		this.idRamo = idRamo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getIdEmision() {
		return idEmision;
	}

	public void setIdEmision(String idEmision) {
		this.idEmision = idEmision;
	}

	public String getIdTecnico() {
		return idTecnico;
	}

	public void setIdTecnico(String idTecnico) {
		this.idTecnico = idTecnico;
	}

	
}
