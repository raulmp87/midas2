package mx.com.afirme.midas.catalogos.ramo;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Christian Ceballos
 * @since 22/07/2009
 * @modificacion Jos� Luis Arellano
 * @since 05/08/2009
 *
 */
public class RamoAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		RamoDN ramoDN = RamoDN.getInstancia();
		List<RamoDTO> ramos = ramoDN.listarTodos();
		request.setAttribute("ramos", ramos);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RamoForm ramoForm = (RamoForm) form;
		RamoDTO ramoDTO = new RamoDTO();
		RamoDN ramoDN = RamoDN.getInstancia();
		try {
			poblarDTO(ramoForm, ramoDTO);
			request.setAttribute("ramos", ramoDN.listarFiltrado(ramoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(RamoForm ramoForm, RamoDTO ramoDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(ramoForm.getDescripcion()))
			ramoDTO.setDescripcion(ramoForm.getDescripcion());
		if (!UtileriasWeb.esCadenaVacia(ramoForm.getIdRamo()))
			ramoDTO.setIdTcRamo(UtileriasWeb.regresaBigDecimal(ramoForm.getIdRamo()));
		if (!UtileriasWeb.esCadenaVacia(ramoForm.getCodigo()))
			ramoDTO.setCodigo(UtileriasWeb.regresaBigDecimal(ramoForm.getCodigo()));
	}
	
	private void poblarForm(RamoForm ramoForm, RamoDTO ramoDTO) throws SystemException {
		ramoForm.setIdRamo(ramoDTO.getIdTcRamo().toBigInteger().toString());
		ramoForm.setDescripcion(ramoDTO.getDescripcion());
		ramoForm.setCodigo(ramoDTO.getCodigo().toBigInteger().toString());
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RamoForm ramoForm = (RamoForm) form;
		RamoDTO ramoDTO = new RamoDTO();
		RamoDN ramoDN = RamoDN.getInstancia();
		try {
			poblarDTO(ramoForm, ramoDTO);
			ramoDN.agregar(ramoDTO);
			listarTodos(request);
			limpiarForm(ramoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}
	
	private void limpiarForm(RamoForm form){
		form.setCodigo("");
		form.setDescripcion("");
		form.setIdRamo("");
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RamoForm ramoForm = (RamoForm) form;
		RamoDN ramoDN = RamoDN.getInstancia();
		RamoDTO ramoDTO = new RamoDTO();
		try {
			poblarDTO(ramoForm, ramoDTO);
			ramoDTO = ramoDN.getRamoPorId(ramoDTO);
			poblarDTO(ramoForm, ramoDTO);
			ramoDN.modificar(ramoDTO);
			listarTodos(request);
			limpiarForm(ramoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RamoForm ramoForm = (RamoForm) form;
		RamoDTO ramoDTO = new RamoDTO();
		RamoDN ramoDN = RamoDN.getInstancia();
		try {
			ramoDTO.setIdTcRamo(UtileriasWeb.regresaBigDecimal(ramoForm.getIdRamo()));
			ramoDTO = ramoDN.getRamoPorId(ramoDTO);
			ramoDN.borrar(ramoDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		RamoDTO ramoDTO = new RamoDTO();
		RamoForm ramoForm = (RamoForm) form;
		RamoDN ramoDN = RamoDN.getInstancia();
		try {
			String id = request.getParameter("id");
			ramoDTO.setIdTcRamo(UtileriasWeb.regresaBigDecimal(id));
			ramoDTO = ramoDN.getRamoPorId(ramoDTO);
			poblarForm(ramoForm, ramoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
}
