package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 *@author Christian Ceballos
 *@since 23/07/2009
 */
public class RamoDN {

	public static final RamoDN INSTANCIA = new RamoDN();

	public static RamoDN getInstancia (){
		return RamoDN.INSTANCIA;
	}
	
	public String agregar(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		RamoSN ramoSN = new RamoSN();
		return ramoSN.agregar(ramoDTO);
	}
	
	public String borrar (RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		RamoSN ramoSN = new RamoSN();
		return ramoSN.borrar(ramoDTO);
	}
	
	public String modificar (RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		RamoSN ramoSN = new RamoSN();
		return ramoSN.modificar(ramoDTO);
	}
	
	public RamoDTO getRamoPorId(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		RamoSN ramoSN = new RamoSN();
		return ramoSN.getRamoPorId(ramoDTO);
	}
	
	public List<RamoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		RamoSN ramoSN = new RamoSN();
		return ramoSN.listarTodos();
	}
	
	public List<RamoDTO> listarFiltrado(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		RamoSN ramoSN = new RamoSN();
		return ramoSN.listarFiltrado(ramoDTO);
	}

	/**
	 * Lista todos los ramos que no se puedan desasociar a un producto.
	 * Los ramos que no se pueden desasociar del producto son aquellos que se encuentran asociados a alguna P�liza VIGENTE que le pertenece al producto. 
	 * @param BigDecimal idToProducto el id del Producto.
	 * @return List<RamoDTO> registros RamoProducto que no se deban desasociar al producto.
	 * @throws SystemException 
	 */
	public List<RamoDTO> obtenerRamosObligatoriosPorProducto(BigDecimal idToProducto) throws SystemException{
		return new RamoSN().obtenerRamosObligatoriosPorProducto(idToProducto);
	}
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un producto.
	 * Los ramos que no se pueden desasociar del producto son aquellos que se encuentran asociados a alguna P�liza VIGENTE que le pertenece al producto. 
	 * @param BigDecimal idToProducto el id del Producto.
	 * @return List<RamoDTO> registros RamoProducto que no se deban desasociar al producto.
	 * @throws SystemException 
	 */
	public List<RamoDTO> obtenerRamosObligatoriosPorTipoPoliza(BigDecimal idToTipoPoliza) throws SystemException{
		return new RamoSN().obtenerRamosObligatoriosPorTipoPoliza(idToTipoPoliza);
	}
}
