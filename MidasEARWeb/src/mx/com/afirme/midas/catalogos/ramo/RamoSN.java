package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 *@author Christian Ceballos
 *@since 23/07/2009
 */
public class RamoSN {

	private RamoFacadeRemote beanRemoto;

	public RamoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RamoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String agregar(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(ramoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String borrar(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(ramoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String modificar(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(ramoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public List<RamoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public RamoDTO getRamoPorId(RamoDTO ramoDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(ramoDTO.getIdTcRamo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<RamoDTO> listarFiltrado(RamoDTO ramoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(ramoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	/**
	 * Lista todos los Ramos que no est�n asociadas una secci�n determinada por
	 * el id que recibe el m�todo
	 * 
	 * @param BigDecimal
	 *            idToSeccion el id de la seccion.
	 * @return List<RamoDTO> registros Ramo que no est�n asociados a la seccion.
	 */
	public List<RamoDTO> obtenerRamosSinAsociarSeccion(SeccionDTO seccionDTO) {
		return beanRemoto.obtenerRamosSinAsociarSeccion(seccionDTO.getIdToSeccion(), seccionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
	}

	/**
	 * Lista todos los Ramos que no est�n asociadas un producto determinado por
	 * el id que recibe el m�todo
	 * 
	 * @param BigDecimal
	 *            idToProducto el id del producto.
	 * @return List<RamoDTO> registros Ramo que no est�n asociados al producto.
	 */
	public List<RamoDTO> obtenerRamosSinAsociarProducto(BigDecimal idToProducto) {
		return beanRemoto.obtenerRamosSinAsociarProducto(idToProducto);
	}

	/**
	 * Lista todos los Ramos que no est�n asociadas un TipoPoliza determinado
	 * por el id que recibe el m�todo
	 * 
	 * @param TipoPolizaDTO
	 *            tipoPolizaDTO el tipoPoliza.
	 * @return List<RamoDTO> registros Ramo que no est�n asociados al
	 *         TipoPoliza.
	 */
	public List<RamoDTO> obtenerRamosSinAsociarTipoPoliza(
			TipoPolizaDTO tipoPolizaDTO) {
		return beanRemoto.obtenerRamosSinAsociarTipoPoliza(tipoPolizaDTO
				.getIdToTipoPoliza(), tipoPolizaDTO.getProductoDTO()
				.getIdToProducto());
	}
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un producto.
	 * Los ramos que no se pueden desasociar del producto son aquellos que se encuentran asociados a alguna P�liza VIGENTE que le pertenece al producto. 
	 * @param BigDecimal idToProducto el id del Producto.
	 * @return List<RamoDTO> registros Ramo que no se deban desasociar al producto.
	 */
	public List<RamoDTO> obtenerRamosObligatoriosPorProducto(BigDecimal idToProducto){
		return beanRemoto.obtenerRamosObligatoriosPorProducto(idToProducto);
	}
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un tipoPoliza.
	 * Los ramos que no se pueden desasociar del tipoPoliza son aquellos que se encuentran asociados a alguna Seccion VIGENTE que le pertenece al tipoPoliza. 
	 * @param BigDecimal idToTipoPoliza el id del TipPoliza.
	 * @return List<RamoDTO> registros Ramo que no se deban desasociar al tipoPoliza.
	 */
	public List<RamoDTO> obtenerRamosObligatoriosPorTipoPoliza(BigDecimal idToTipoPoliza){
		return beanRemoto.obtenerRamosObligatoriosPorTipoPoliza(idToTipoPoliza);
	}
}
