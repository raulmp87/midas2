/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package mx.com.afirme.midas.catalogos.zonahidro;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * MyEclipse Struts Creation date: 06-25-2009
 * 
 * XDoclet definition:
 * 
 * @struts.action path="/zonaHidro" name="zonaHidroForm"
 *                input="/WEB-INF/jsp/zonaHidro.jsp" parameter="metodo"
 *                scope="request" validate="true"
 * @struts.action-forward name="noDisponible" path="/WEB-INF/jsp/error.jsp"
 *                        contextRelative="true"
 */
public class ZonaHidroAction extends MidasMappingDispatchAction {
	/*
	 * Generated Methods
	 */

	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		try {
			ZonaHidroForm zonaHidroForm = (ZonaHidroForm) form;
			ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
			this.poblarDTO(zonaHidroForm, zonaHidroDTO);
			ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
			List<ZonaHidroDTO> zonaHidros = zonaHidroDN.listarFiltrados(zonaHidroDTO);
			request.setAttribute("zonaHidros", zonaHidros);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ZonaHidroForm zonaHidroForm = (ZonaHidroForm) form;
		ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
		ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
		try {
			this.poblarDTO(zonaHidroForm, zonaHidroDTO);
			zonaHidroDN.agregar(zonaHidroDTO);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ZonaHidroForm zonaHidroForm = (ZonaHidroForm) form;			
		ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
		ZonaHidroDN zonaHidroDN = null;		
		try {	
		        zonaHidroDN = ZonaHidroDN.getInstancia();
			this.poblarDTO(zonaHidroForm, zonaHidroDTO);
			zonaHidroDN.modificar(zonaHidroDTO);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ZonaHidroForm zonaHidroForm = (ZonaHidroForm) form;
		ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
		String reglaNavegacion = Sistema.EXITOSO;
		ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
		try {
			this.poblarDTO(zonaHidroForm, zonaHidroDTO);
			zonaHidroDN.borrar(zonaHidroDTO);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ZonaHidroForm zonaHidroForm = (ZonaHidroForm) form;
		ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
		ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
		try {
			this.poblarDTO(zonaHidroForm, zonaHidroDTO);
			zonaHidroDTO = zonaHidroDN.getPorId(zonaHidroDTO);
			this.poblarForm(zonaHidroDTO, zonaHidroForm);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ZonaHidroForm zonaHidroForm = (ZonaHidroForm) form;
		ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
		String idParam = request.getParameter("id");
		ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
		try {
			BigDecimal id = UtileriasWeb.regresaBigDecimal(idParam);
			zonaHidroDTO.setIdTcZonaHidro(id);
			zonaHidroDTO = zonaHidroDN.getPorId(zonaHidroDTO);
			this.poblarForm(zonaHidroDTO, zonaHidroForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
		List<ZonaHidroDTO> zonaHidros = zonaHidroDN.listarTodos();
		request.setAttribute("zonaHidros", zonaHidros);
	}

	/**
	 * Method poblarDTO
	 * 
	 * @param ZonaHidroForm
	 * @param ZonaHidroDTO
	 * @throws SystemException 
	 */
	private void poblarDTO(ZonaHidroForm zonaHidroForm,
			ZonaHidroDTO zonaHidroDTO) throws SystemException {
		

		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getIdTcZonaHidro())) {
			zonaHidroDTO.setIdTcZonaHidro(UtileriasWeb.regresaBigDecimal(zonaHidroForm.getIdTcZonaHidro()));
		}
		
		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getCodigoZonaHidro())) {
			zonaHidroDTO.setCodigoZonaHidro(zonaHidroForm.getCodigoZonaHidro());
		}
		
		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getDescripcionZonaHidro())) {
			zonaHidroDTO.setDescripcionZonaHidro(zonaHidroForm.getDescripcionZonaHidro());
		}

		//******************/
		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getValorDefaultCoaseguro())) {			
			zonaHidroDTO.setValorDefaultCoaseguro(UtileriasWeb.regresaBigDecimal(zonaHidroForm.getValorDefaultCoaseguro()));
		}	
		
		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getValorDefaultCoaseguroBMCE())) {			
			zonaHidroDTO.setValorDefaultCoaseguroBMCE(UtileriasWeb.regresaBigDecimal(zonaHidroForm.getValorDefaultCoaseguroBMCE()));
		}
		
		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getValorDefaultDeducible())) {			
			zonaHidroDTO.setValorDefaultDeducible(UtileriasWeb.regresaBigDecimal(zonaHidroForm.getValorDefaultDeducible()));
		}
		
		if (!UtileriasWeb.esCadenaVacia(zonaHidroForm.getValorDefaultDeducibleBMCE())) {			
			zonaHidroDTO.setValorDefaultDeducibleBMCE(UtileriasWeb.regresaBigDecimal(zonaHidroForm.getValorDefaultDeducibleBMCE()));
		}
		
		//************/
	}

	/**
	 * Method poblarForm
	 * 
	 * @param ZonaHidroDTO
	 * @param ZonaHidroForm
	 */
	private void poblarForm(ZonaHidroDTO zonaHidroDTO,
			ZonaHidroForm zonaHidroForm) {

		if (zonaHidroDTO.getIdTcZonaHidro() != null)
			zonaHidroForm.setIdTcZonaHidro(zonaHidroDTO.getIdTcZonaHidro().toString());

		if (zonaHidroDTO.getCodigoZonaHidro() != null)
			zonaHidroForm.setCodigoZonaHidro(zonaHidroDTO.getCodigoZonaHidro().toString());
		
		if (zonaHidroDTO.getDescripcionZonaHidro() != null)
			zonaHidroForm.setDescripcionZonaHidro(zonaHidroDTO.getDescripcionZonaHidro());
		//************************/
		if(zonaHidroDTO.getValorDefaultCoaseguro()!=null){
		    zonaHidroForm.setValorDefaultCoaseguro(zonaHidroDTO.getValorDefaultCoaseguro().toString());
		}
		
		if(zonaHidroDTO.getValorDefaultCoaseguroBMCE()!=null){
		    zonaHidroForm.setValorDefaultCoaseguroBMCE(zonaHidroDTO.getValorDefaultCoaseguroBMCE().toString());
		}
		
		if(zonaHidroDTO.getValorDefaultDeducible()!=null){
		    zonaHidroForm.setValorDefaultDeducible(zonaHidroDTO.getValorDefaultDeducible().toString());
		}
		
		if(zonaHidroDTO.getValorDefaultDeducibleBMCE()!=null){
		    zonaHidroForm.setValorDefaultDeducibleBMCE(zonaHidroDTO.getValorDefaultDeducibleBMCE().toString());
		}
		//***************************/
		
	}
}