/**
 * 
 */
package mx.com.afirme.midas.catalogos.zonahidro;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ZonaHidroSN {
	private ZonaHidroFacadeRemote beanRemoto;

	public ZonaHidroSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ZonaHidroFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(ZonaHidroDTO zonaHidroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(zonaHidroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(ZonaHidroDTO zonaHidroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(zonaHidroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(ZonaHidroDTO zonaHidroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(zonaHidroDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<ZonaHidroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public ZonaHidroDTO getPorId(ZonaHidroDTO zonaHidroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(zonaHidroDTO.getIdTcZonaHidro());
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ZonaHidroDTO> listarFiltrados(ZonaHidroDTO zonaHidroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(zonaHidroDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
