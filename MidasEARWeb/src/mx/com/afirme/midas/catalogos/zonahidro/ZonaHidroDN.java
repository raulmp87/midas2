package mx.com.afirme.midas.catalogos.zonahidro;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ZonaHidroDN {
	private static final ZonaHidroDN INSTANCIA = new ZonaHidroDN();

	public static ZonaHidroDN getInstancia() {
		return ZonaHidroDN.INSTANCIA;
	}

	public List<ZonaHidroDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ZonaHidroSN zonaHidroSN = new ZonaHidroSN();
		return zonaHidroSN.listarTodos();
	}

	public List<ZonaHidroDTO> listarFiltrados(ZonaHidroDTO zonaHidroDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ZonaHidroSN zonaHidroSN = new ZonaHidroSN();
		return zonaHidroSN.listarFiltrados(zonaHidroDTO);
	}

	public void agregar(ZonaHidroDTO zonaHidroDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ZonaHidroSN zonaHidroSN = new ZonaHidroSN();
		zonaHidroSN.agregar(zonaHidroDTO);
	}

	public void modificar(ZonaHidroDTO zonaHidroDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ZonaHidroSN zonaHidroSN = new ZonaHidroSN();
		zonaHidroSN.modificar(zonaHidroDTO);
	}

	public ZonaHidroDTO getPorId(ZonaHidroDTO zonaHidroDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ZonaHidroSN zonaHidroSN = new ZonaHidroSN();
		return zonaHidroSN.getPorId(zonaHidroDTO);
	}

	public void borrar(ZonaHidroDTO zonaHidroDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ZonaHidroSN zonaHidroSN = new ZonaHidroSN();
		zonaHidroSN.borrar(zonaHidroDTO);
	}
}
