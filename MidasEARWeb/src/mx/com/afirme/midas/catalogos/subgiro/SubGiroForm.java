package mx.com.afirme.midas.catalogos.subgiro;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroForm extends MidasBaseForm {
	private static final long serialVersionUID = 8489105806202000753L;

	private String idSubGiro;
	private String idTcGiro;
	private String codigo;
	private String descripcionSubGiro;	
	private String codigoSubGiro;
	private String descripcionGiro;
	private String idGrupoRobo;
	private String idGrupoDineroValores;
	private String idGrupoCristales;
	private String idGrupoPlenos;
	private String claveInspeccion;


	public String getIdSubGiro() {
		return idSubGiro;
	}

	public void setIdSubGiro(String idSubGiro) {
		this.idSubGiro = idSubGiro;
	}

	public String getIdTcGiro() {
		return idTcGiro;
	}

	public void setIdTcGiro(String idTcGiro) {
		this.idTcGiro = idTcGiro;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcionSubGiro() {
		return descripcionSubGiro;
	}

	public void setDescripcionSubGiro(String descripcionSubGiro) {
		this.descripcionSubGiro = descripcionSubGiro;
	}

	public String getCodigoSubGiro() {
		return codigoSubGiro;
	}

	public void setCodigoSubGiro(String codigoSubGiro) {
		this.codigoSubGiro = codigoSubGiro;
	}

	public String getDescripcionGiro() {
		return descripcionGiro;
	}

	public void setDescripcionGiro(String descripcionGiro) {
		this.descripcionGiro = descripcionGiro;
	}

	public String getIdGrupoRobo() {
		return idGrupoRobo;
	}

	public void setIdGrupoRobo(String idGrupoRobo) {
		this.idGrupoRobo = idGrupoRobo;
	}

	public String getIdGrupoDineroValores() {
		return idGrupoDineroValores;
	}

	public void setIdGrupoDineroValores(String idGrupoDineroValores) {
		this.idGrupoDineroValores = idGrupoDineroValores;
	}

	public String getIdGrupoCristales() {
		return idGrupoCristales;
	}

	public void setIdGrupoCristales(String idGrupoCristales) {
		this.idGrupoCristales = idGrupoCristales;
	}

	public String getIdGrupoPlenos() {
		return idGrupoPlenos;
	}

	public void setIdGrupoPlenos(String idGrupoPlenos) {
		this.idGrupoPlenos = idGrupoPlenos;
	}

	public String getClaveInspeccion() {
		return claveInspeccion;
	}

	public void setClaveInspeccion(String claveInspeccion) {
		this.claveInspeccion = claveInspeccion;
	}


}
