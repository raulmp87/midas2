package mx.com.afirme.midas.catalogos.subgiro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroDN {
	public static final SubGiroDN INSTANCIA = new SubGiroDN();

	public static SubGiroDN getInstancia() {
		return SubGiroDN.INSTANCIA;
	}

	public String agregar(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroSN subGiroSN = new SubGiroSN();
		return subGiroSN.agregar(subGiroDTO);
	}

	public String borrar(SubGiroDTO subGiroDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		SubGiroSN subGiroSN = new SubGiroSN();
		return subGiroSN.borrar(subGiroDTO);
	}

	public String modificar(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroSN subGiroSN = new SubGiroSN();
		return subGiroSN.modificar(subGiroDTO);
	}

	public SubGiroDTO getSubGiroPorId(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroSN subGiroSN = new SubGiroSN();
		return subGiroSN.getSubgiroPorId(subGiroDTO);
	}
	
	public SubGiroDTO getSubGiroPorId(BigDecimal idTcSubGiro) throws ExcepcionDeAccesoADatos, SystemException {
	SubGiroSN subGiroSN = new SubGiroSN();
	return subGiroSN.getSubgiroPorId(idTcSubGiro);
	}

	public List<SubGiroDTO> listarTodos() throws ExcepcionDeAccesoADatos,
			SystemException {
		SubGiroSN subGiroSN = new SubGiroSN();
		return subGiroSN.listarTodos();
	}

	public List<SubGiroDTO> listarFiltrado(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		SubGiroSN subGiroSN = new SubGiroSN();
		return subGiroSN.listarFiltrado(subGiroDTO);
	}

}
