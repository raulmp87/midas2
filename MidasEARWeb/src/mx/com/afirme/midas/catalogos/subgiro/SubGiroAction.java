package mx.com.afirme.midas.catalogos.subgiro;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.giro.GiroDN;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroAction extends MidasMappingDispatchAction {
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		SubGiroDN subGiroDN = SubGiroDN.getInstancia();
		List<SubGiroDTO> subGiros = subGiroDN.listarTodos();
		request.setAttribute("subGiros", subGiros);
	}

	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroForm subGiroForm = (SubGiroForm) form;
		SubGiroDTO subGiroDTO = new SubGiroDTO();
		SubGiroDN subGiroDN = SubGiroDN.getInstancia();
		try {
			poblarDTO(subGiroForm, subGiroDTO);
			request.setAttribute("subGiros", subGiroDN.listarFiltrado(subGiroDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(SubGiroForm subGiroForm, SubGiroDTO subGiroDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getIdTcGiro())) {
			// localizamos el giro a almacenar
			GiroDN giroDN = GiroDN.getInstancia();
			GiroDTO giroDTO = new GiroDTO();
			giroDTO.setIdTcGiro(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdTcGiro()));
			giroDTO = giroDN.getGiroPorId(giroDTO);
			subGiroDTO.setGiroDTO(giroDTO);
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getCodigoSubGiro())) {
			subGiroDTO.setCodigoSubGiro(UtileriasWeb.regresaBigDecimal(subGiroForm.getCodigoSubGiro()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getDescripcionSubGiro())) {
			subGiroDTO.setDescripcionSubGiro(subGiroForm.getDescripcionSubGiro());
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getIdGrupoRobo())) {
			subGiroDTO.setIdGrupoRobo(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdGrupoRobo()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getIdGrupoDineroValores())) {
			subGiroDTO.setIdGrupoDineroValores(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdGrupoDineroValores()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getIdGrupoCristales())) {
			subGiroDTO.setIdGrupoCristales(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdGrupoCristales()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getIdGrupoPlenos())) {
			subGiroDTO.setIdGrupoPlenos(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdGrupoPlenos()));
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroForm.getClaveInspeccion())) {
			subGiroDTO.setClaveInspeccion(UtileriasWeb.regresaShort(subGiroForm.getClaveInspeccion()));
		}
	}

	private void poblarForm(SubGiroForm subGiroForm, SubGiroDTO subGiroDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getIdTcSubGiro())) {
			subGiroForm.setIdSubGiro(subGiroDTO.getIdTcSubGiro().toBigInteger().toString());
		}
		if (subGiroDTO.getGiroDTO() != null) {
			subGiroForm.setIdTcGiro(subGiroDTO.getGiroDTO().getIdTcGiro().toString());
			subGiroForm.setDescripcionGiro(subGiroDTO.getGiroDTO().getDescripcionGiro());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getCodigoSubGiro())) {
			subGiroForm.setCodigoSubGiro(subGiroDTO.getCodigoSubGiro().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia(subGiroDTO.getDescripcionSubGiro())) {
			subGiroForm.setDescripcionSubGiro(subGiroDTO.getDescripcionSubGiro());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getIdGrupoRobo())) {
			subGiroForm.setIdGrupoRobo(subGiroDTO.getIdGrupoRobo().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getIdGrupoDineroValores())) {
			subGiroForm.setIdGrupoDineroValores(subGiroDTO.getIdGrupoDineroValores().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getIdGrupoCristales())) {
			subGiroForm.setIdGrupoCristales(subGiroDTO.getIdGrupoCristales().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getIdGrupoPlenos())) {
			subGiroForm.setIdGrupoPlenos(subGiroDTO.getIdGrupoPlenos().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia("" + subGiroDTO.getClaveInspeccion())) {
			subGiroForm.setClaveInspeccion(subGiroDTO.getClaveInspeccion().toString());
		}
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroForm subGiroForm = (SubGiroForm) form;
		SubGiroDTO subGiroDTO = new SubGiroDTO();
		SubGiroDN subGiroDN = SubGiroDN.getInstancia();
		try {
			poblarDTO(subGiroForm, subGiroDTO);
			subGiroDN.agregar(subGiroDTO);
			listarTodos(request);
			limpiarForm(subGiroForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void limpiarForm(SubGiroForm subGiroForm) {
		subGiroForm.setIdTcGiro("");
		subGiroForm.setDescripcionGiro("");
		subGiroForm.setCodigoSubGiro("");
		subGiroForm.setDescripcionSubGiro("");
		subGiroForm.setIdGrupoDineroValores("");
		subGiroForm.setIdGrupoCristales("");
		subGiroForm.setIdGrupoPlenos("");
		subGiroForm.setClaveInspeccion("");
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroForm subGiroForm = (SubGiroForm) form;
		SubGiroDTO subGiroDTO = new SubGiroDTO();
		SubGiroDN subGiroDN = SubGiroDN.getInstancia();
		try {
			subGiroDTO.setIdTcSubGiro(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdSubGiro()));
			subGiroDTO = subGiroDN.getSubGiroPorId(subGiroDTO);
			poblarDTO(subGiroForm, subGiroDTO);
			subGiroDN.modificar(subGiroDTO);
			listarTodos(request);
			limpiarForm(subGiroForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroForm subGiroForm = (SubGiroForm) form;
		SubGiroDTO subGiroDTO = new SubGiroDTO();
		SubGiroDN subGiroDN = SubGiroDN.getInstancia();
		try {
			subGiroDTO.setIdTcSubGiro(UtileriasWeb.regresaBigDecimal(subGiroForm.getIdSubGiro()));
			subGiroDTO = subGiroDN.getSubGiroPorId(subGiroDTO);
			subGiroDN.borrar(subGiroDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}

	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubGiroDTO subGiroDTO = new SubGiroDTO();
		SubGiroForm subGiroForm = (SubGiroForm) form;
		SubGiroDN subGiroDN = SubGiroDN.getInstancia();
		try {
			String id = request.getParameter("id");
			subGiroDTO.setIdTcSubGiro(UtileriasWeb.regresaBigDecimal(id));
			subGiroDTO = subGiroDN.getSubGiroPorId(subGiroDTO);
			poblarForm(subGiroForm, subGiroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}

	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
