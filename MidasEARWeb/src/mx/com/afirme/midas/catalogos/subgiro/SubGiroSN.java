package mx.com.afirme.midas.catalogos.subgiro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class SubGiroSN {

	private SubGiroFacadeRemote beanRemoto;

	public SubGiroSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(SubGiroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.INFO, null);
	}

	public String agregar(SubGiroDTO subGiroDTO) throws ExcepcionDeAccesoADatos {
		LogDeMidasWeb.log("agregar(SubGiroDTO)", Level.INFO, null);
		beanRemoto.save(subGiroDTO);
		return null;
	}

	public String borrar(SubGiroDTO subGiroDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(subGiroDTO);
		return null;
	}

	public String modificar(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(subGiroDTO);
		return null;
	}

	public List<SubGiroDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public SubGiroDTO getSubgiroPorId(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(subGiroDTO.getIdTcSubGiro());
	}
	
	public SubGiroDTO getSubgiroPorId(BigDecimal idTcSubGiro) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(idTcSubGiro);
	}

	public List<SubGiroDTO> listarFiltrado(SubGiroDTO subGiroDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(subGiroDTO);
	}
}
