package mx.com.afirme.midas.catalogos.giro;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroForm extends MidasBaseForm {
	private static final long serialVersionUID = -4426430534960591954L;

	private String idTcGiro;
	private String codigoGiro;
	private String descripcionGiro;
	private String idGrupoRobo;
	private String idGrupoDineroValores;
	private String idGrupoCristales;
	private String idGrupoPlenos;
	private String claveInspeccion;


	public String getIdTcGiro() {
		return idTcGiro;
	}

	public void setIdTcGiro(String idTcGiro) {
		this.idTcGiro = idTcGiro;
	}

	public String getCodigoGiro() {
		return codigoGiro;
	}

	public void setCodigoGiro(String codigoGiro) {
		this.codigoGiro = codigoGiro;
	}

	public String getDescripcionGiro() {
		return descripcionGiro;
	}

	public void setDescripcionGiro(String descripcionGiro) {
		this.descripcionGiro = descripcionGiro;
	}

	public String getIdGrupoRobo() {
		return idGrupoRobo;
	}

	public void setIdGrupoRobo(String idGrupoRobo) {
		this.idGrupoRobo = idGrupoRobo;
	}

	public String getIdGrupoDineroValores() {
		return idGrupoDineroValores;
	}

	public void setIdGrupoDineroValores(String idGrupoDineroValores) {
		this.idGrupoDineroValores = idGrupoDineroValores;
	}

	public String getIdGrupoCristales() {
		return idGrupoCristales;
	}

	public void setIdGrupoCristales(String idGrupoCristales) {
		this.idGrupoCristales = idGrupoCristales;
	}

	public String getIdGrupoPlenos() {
		return idGrupoPlenos;
	}

	public void setIdGrupoPlenos(String idGrupoPlenos) {
		this.idGrupoPlenos = idGrupoPlenos;
	}

	public String getClaveInspeccion() {
		return claveInspeccion;
	}

	public void setClaveInspeccion(String claveInspeccion) {
		this.claveInspeccion = claveInspeccion;
	}
}
