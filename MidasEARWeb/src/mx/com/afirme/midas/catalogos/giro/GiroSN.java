package mx.com.afirme.midas.catalogos.giro;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroSN {
	
	private GiroFacadeRemote beanRemoto;

	public GiroSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(GiroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(giroDTO);
		}catch(EJBTransactionRolledbackException e){
			System.out.println("EXCEPCION GENERADA ::::  ".concat(e.getCausedByException().toString()));
		}
		return null;
	}
	
	public String borrar(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.delete(giroDTO);
		return null;
	}
	
	public String modificar(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.update(giroDTO);
		return null;
	}
	
	public List<GiroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		return beanRemoto.findAll();
	}
	
	public GiroDTO getGiroPorId(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findById(giroDTO.getIdTcGiro());
	}
	
	public List<GiroDTO> listarFiltrado(GiroDTO ramoDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.listarFiltrado(ramoDTO);
	}
	
	public EntityTransaction getTransaccion(){
		return beanRemoto.getTransaction();
	}
}
