package mx.com.afirme.midas.catalogos.giro;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		GiroDN giroDN = GiroDN.getInstancia();
		List<GiroDTO> giros = giroDN.listarTodos();
		request.setAttribute("giros", giros);
	}

	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroForm giroForm = (GiroForm) form;
		GiroDTO giroDTO = new GiroDTO();
		GiroDN giroDN = GiroDN.getInstancia();
		try {
			poblarDTO(giroForm, giroDTO);
			request.setAttribute("giros", giroDN.listarFiltrado(giroDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(GiroForm giroForm, GiroDTO giroDTO)
			throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(giroForm.getIdTcGiro())) {
			giroDTO.setIdTcGiro(UtileriasWeb.regresaBigDecimal(giroForm.getIdTcGiro()));
		}
		if (!UtileriasWeb.esCadenaVacia(giroForm.getCodigoGiro())) {
			giroDTO.setCodigoGiro(UtileriasWeb.regresaBigDecimal(giroForm.getCodigoGiro()));
		}
		if (!UtileriasWeb.esCadenaVacia(giroForm.getDescripcionGiro())) {
			giroDTO.setDescripcionGiro(giroForm.getDescripcionGiro());
		}
	}

	private void poblarForm(GiroForm giroForm, GiroDTO giroDTO)
			throws SystemException {
		giroForm.setIdTcGiro(giroDTO.getIdTcGiro().toBigInteger().toString());
		giroForm.setCodigoGiro(giroDTO.getCodigoGiro().toBigInteger().toString());
		giroForm.setDescripcionGiro(giroDTO.getDescripcionGiro());
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroForm giroForm = (GiroForm) form;
		GiroDTO giroDTO = new GiroDTO();
		GiroDN giroDN = GiroDN.getInstancia();
		try {
			poblarDTO(giroForm, giroDTO);
			giroDN.agregar(giroDTO);
			listarTodos(request);
			limpiarForm(giroForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (Exception e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void limpiarForm(GiroForm form) {
		form.setIdTcGiro("");
		form.setDescripcionGiro("");
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroForm giroForm = (GiroForm) form;
		GiroDN giroDN = GiroDN.getInstancia();
		GiroDTO giroDTO = new GiroDTO();
		try {
			poblarDTO(giroForm, giroDTO);
			giroDTO = giroDN.getGiroPorId(giroDTO);
			poblarDTO(giroForm, giroDTO);
			giroDN.modificar(giroDTO);
			listarTodos(request);
			limpiarForm(giroForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroForm giroForm = (GiroForm) form;
		GiroDTO giroDTO = new GiroDTO();
		GiroDN giroDN = GiroDN.getInstancia();
		try {
			giroDTO.setIdTcGiro(UtileriasWeb.regresaBigDecimal(giroForm.getIdTcGiro()));
			giroDTO = giroDN.getGiroPorId(giroDTO);
			giroDN.borrar(giroDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroDTO giroDTO = new GiroDTO();
		GiroForm giroForm = (GiroForm) form;
		GiroDN giroDN = GiroDN.getInstancia();
		try {
			String id = request.getParameter("id");
			giroDTO.setIdTcGiro(UtileriasWeb.regresaBigDecimal(id));
			giroDTO = giroDN.getGiroPorId(giroDTO);
			poblarForm(giroForm, giroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
