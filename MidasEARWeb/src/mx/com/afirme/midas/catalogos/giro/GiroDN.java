package mx.com.afirme.midas.catalogos.giro;

import java.util.List;

import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroDN {

	public static final GiroDN INSTANCIA = new GiroDN();

	public static GiroDN getInstancia() {
		return GiroDN.INSTANCIA;
	}

	public String agregar(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.agregar(giroDTO);
	}

	public String borrar(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.borrar(giroDTO);
	}

	public String modificar(GiroDTO giroDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.modificar(giroDTO);
	}

	public GiroDTO getGiroPorId(GiroDTO giroDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.getGiroPorId(giroDTO);
	}

	public List<GiroDTO> listarTodos() throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.listarTodos();
	}

	public List<GiroDTO> listarFiltrado(GiroDTO giroDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.listarFiltrado(giroDTO);
	}

	public EntityTransaction getTransaccion() throws SystemException {
		GiroSN giroSN = new GiroSN();
		return giroSN.getTransaccion();
	}
}
