package mx.com.afirme.midas.catalogos.materialcombustible;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MaterialCombustibleSN {

	private MaterialCombustibleFacadeRemote beanRemoto;

	public MaterialCombustibleSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MaterialCombustibleFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(materialCombustibleDTO);
		}catch(EJBTransactionRolledbackException e){
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(materialCombustibleDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(materialCombustibleDTO);
		}catch (EJBTransactionRolledbackException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public List<MaterialCombustibleDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public MaterialCombustibleDTO getMaterialCombustiblePorId(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(materialCombustibleDTO.getIdMaterialCombustible());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<MaterialCombustibleDTO> listarFiltrado(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(materialCombustibleDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}
