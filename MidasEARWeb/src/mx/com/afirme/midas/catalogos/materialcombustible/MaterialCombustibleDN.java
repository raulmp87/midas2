package mx.com.afirme.midas.catalogos.materialcombustible;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MaterialCombustibleDN {

	public static final MaterialCombustibleDN INSTANCIA = new MaterialCombustibleDN();

	public static MaterialCombustibleDN getInstancia (){
		return MaterialCombustibleDN.INSTANCIA;
	}
	
	public String agregar(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MaterialCombustibleSN materialCombustibleSN = new MaterialCombustibleSN();
		return materialCombustibleSN.agregar(materialCombustibleDTO);
	}
	
	public String borrar (MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MaterialCombustibleSN materialCombustibleSN = new MaterialCombustibleSN();
		return materialCombustibleSN.borrar(materialCombustibleDTO);
	}
	
	public String modificar (MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MaterialCombustibleSN materialCombustibleSN = new MaterialCombustibleSN();
		return materialCombustibleSN.modificar(materialCombustibleDTO);
	}
	
	public MaterialCombustibleDTO getMaterialCombustiblePorId(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MaterialCombustibleSN materialCombustibleSN = new MaterialCombustibleSN();
		return materialCombustibleSN.getMaterialCombustiblePorId(materialCombustibleDTO);
	}
	
	public List<MaterialCombustibleDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		MaterialCombustibleSN materialCombustibleSN = new MaterialCombustibleSN();
		return materialCombustibleSN.listarTodos();
	}
	
	public List<MaterialCombustibleDTO> listarFiltrado(MaterialCombustibleDTO materialCombustibleDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MaterialCombustibleSN materialCombustibleSN = new MaterialCombustibleSN();
		return materialCombustibleSN.listarFiltrado(materialCombustibleDTO);
	}
	
}
