package mx.com.afirme.midas.catalogos.materialcombustible;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class MaterialCombustibleForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -949186838188796141L;

	private String idtcmaterialcombustiblesistema;

	private String idtcmaterialcombustible;

	private String descripcionmaterialcombustible;

	public String getIdtcmaterialcombustiblesistema() {
		return idtcmaterialcombustiblesistema;
	}

	public void setIdtcmaterialcombustiblesistema(
			String idtcmaterialcombustiblesistema) {
		this.idtcmaterialcombustiblesistema = idtcmaterialcombustiblesistema;
	}

	public String getIdtcmaterialcombustible() {
		return idtcmaterialcombustible;
	}

	public void setIdtcmaterialcombustible(String idtcmaterialcombustible) {
		this.idtcmaterialcombustible = idtcmaterialcombustible;
	}

	public String getDescripcionmaterialcombustible() {
		return descripcionmaterialcombustible;
	}

	public void setDescripcionmaterialcombustible(
			String descripcionmaterialcombustible) {
		this.descripcionmaterialcombustible = descripcionmaterialcombustible;
	}

}
