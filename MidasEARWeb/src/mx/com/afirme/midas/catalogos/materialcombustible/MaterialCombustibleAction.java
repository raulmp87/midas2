package mx.com.afirme.midas.catalogos.materialcombustible;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MaterialCombustibleAction extends MidasMappingDispatchAction{
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		MaterialCombustibleDN materialCombustibleDN = MaterialCombustibleDN.getInstancia();
		List<MaterialCombustibleDTO> materiales = materialCombustibleDN.listarTodos();
		request.setAttribute("materiales", materiales);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MaterialCombustibleForm materialCombustibleForm = (MaterialCombustibleForm) form;
		MaterialCombustibleDTO materialCombustibleDTO = new MaterialCombustibleDTO();
		MaterialCombustibleDN materialCombustibleDN = MaterialCombustibleDN.getInstancia();
		try {
			poblarDTO(materialCombustibleForm, materialCombustibleDTO);
			request.setAttribute("materiales", materialCombustibleDN.listarFiltrado(materialCombustibleDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(MaterialCombustibleForm materialCombustibleForm,
			MaterialCombustibleDTO materialCombustibleDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(materialCombustibleForm.getIdtcmaterialcombustiblesistema()))
			materialCombustibleDTO.setIdMaterialCombustible(UtileriasWeb.regresaBigDecimal(materialCombustibleForm.getIdtcmaterialcombustiblesistema()));
		if (!UtileriasWeb.esCadenaVacia(materialCombustibleForm.getIdtcmaterialcombustible()))
			materialCombustibleDTO.setCodigoMaterialCombustible(UtileriasWeb.regresaBigDecimal(materialCombustibleForm.getIdtcmaterialcombustible()));
		if(!UtileriasWeb.esCadenaVacia(materialCombustibleForm.getDescripcionmaterialcombustible()))
			materialCombustibleDTO.setDescripcionMaterialCombustible(materialCombustibleForm.getDescripcionmaterialcombustible().trim().toUpperCase());
		return null;
	}
	
	private String poblarForm(MaterialCombustibleForm materialCombustibleForm,
			MaterialCombustibleDTO materialCombustibleDTO) throws SystemException {
		materialCombustibleForm.setDescripcionmaterialcombustible(materialCombustibleDTO.getDescripcionMaterialCombustible());
		materialCombustibleForm.setIdtcmaterialcombustible(materialCombustibleDTO.getCodigoMaterialCombustible().toBigInteger().toString());
		materialCombustibleForm.setIdtcmaterialcombustiblesistema(materialCombustibleDTO.getIdMaterialCombustible().toBigInteger().toString());
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MaterialCombustibleForm materialCombustibleForm = (MaterialCombustibleForm) form;
		MaterialCombustibleDTO materialCombustibleDTO = new MaterialCombustibleDTO();
		MaterialCombustibleDN materialCombustibleDN = MaterialCombustibleDN.getInstancia();
		try {
			poblarDTO(materialCombustibleForm, materialCombustibleDTO);
			materialCombustibleDN.agregar(materialCombustibleDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MaterialCombustibleForm materialCombustibleForm = (MaterialCombustibleForm) form;
		MaterialCombustibleDN materialCombustibleDN = MaterialCombustibleDN.getInstancia();
		MaterialCombustibleDTO materialCombustibleDTO = new MaterialCombustibleDTO();
		try {
			poblarDTO(materialCombustibleForm, materialCombustibleDTO);
			materialCombustibleDTO = materialCombustibleDN.getMaterialCombustiblePorId(materialCombustibleDTO);
			poblarDTO(materialCombustibleForm, materialCombustibleDTO);
			materialCombustibleDN.modificar(materialCombustibleDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MaterialCombustibleForm materialCombustibleForm = (MaterialCombustibleForm) form;
		MaterialCombustibleDTO materialCombustibleDTO = new MaterialCombustibleDTO();
		MaterialCombustibleDN materialCombustibleDN = MaterialCombustibleDN.getInstancia();
		try {
			poblarDTO(materialCombustibleForm, materialCombustibleDTO);
			materialCombustibleDTO = materialCombustibleDN.getMaterialCombustiblePorId(materialCombustibleDTO);
			materialCombustibleDN.borrar(materialCombustibleDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MaterialCombustibleDTO materialCombustibleDTO = new MaterialCombustibleDTO();
		MaterialCombustibleForm materialCombustibleForm = (MaterialCombustibleForm) form;
		MaterialCombustibleDN materialCombustibleDN = MaterialCombustibleDN.getInstancia();
		try {
			String id = request.getParameter("id");
			materialCombustibleDTO.setIdMaterialCombustible(UtileriasWeb.regresaBigDecimal(id));
			materialCombustibleDTO = materialCombustibleDN.getMaterialCombustiblePorId(materialCombustibleDTO);
			poblarForm(materialCombustibleForm, materialCombustibleDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
