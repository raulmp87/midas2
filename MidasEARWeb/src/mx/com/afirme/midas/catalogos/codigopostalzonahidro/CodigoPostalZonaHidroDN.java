package mx.com.afirme.midas.catalogos.codigopostalzonahidro;

import java.util.List;

import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CodigoPostalZonaHidroDN {
	private static final CodigoPostalZonaHidroDN INSTANCIA = new CodigoPostalZonaHidroDN();

	public static CodigoPostalZonaHidroDN getInstancia() {
		return CodigoPostalZonaHidroDN.INSTANCIA;
	}

	public List<CodigoPostalZonaHidroDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		CodigoPostalZonaHidroSN codigoPostalZonaHidroSN = new CodigoPostalZonaHidroSN();
		return codigoPostalZonaHidroSN.listarTodos();
	}

	public List<CodigoPostalZonaHidroDTO> listarFiltrado(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		CodigoPostalZonaHidroSN codigoPostalZonaHidroSN = new CodigoPostalZonaHidroSN();
		return codigoPostalZonaHidroSN.listarFiltrado(codigoPostalZonaHidroDTO);
	}
	
	public CodigoPostalZonaHidroDTO getPorId(CodigoPostalZonaHidroId id)
	throws SystemException, ExcepcionDeAccesoADatos {
		CodigoPostalZonaHidroSN codigoPostalZonaHidroSN = new CodigoPostalZonaHidroSN();
		return codigoPostalZonaHidroSN.getPorId(id);
	}
	
	public void agregarCodigoPostalZonaHidroZonaSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, CodigoPostalZonaSismoDTO
		codigoPostalZonaSismoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		CodigoPostalZonaHidroSN codigoPostalZonaHidroSN = new CodigoPostalZonaHidroSN();
		codigoPostalZonaHidroSN.agregarCodigoPostalZonaHidroZonaSismo(codigoPostalZonaHidroDTO, codigoPostalZonaSismoDTO);
	}
	
	
	public void modificarCodigoPostalZonaHidroZonaSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, CodigoPostalZonaSismoDTO
		codigoPostalZonaSismoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		CodigoPostalZonaHidroSN codigoPostalZonaHidroSN = new CodigoPostalZonaHidroSN();
		codigoPostalZonaHidroSN.modificarCodigoPostalZonaHidroZonaSismo
		(codigoPostalZonaHidroDTO, codigoPostalZonaSismoDTO);
	}
	
	public Long obtenerTotalFiltrado(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		CodigoPostalZonaHidroSN codigoPostalZonaHidroSN = new CodigoPostalZonaHidroSN();
		return codigoPostalZonaHidroSN.obtenerTotalFiltrado(codigoPostalZonaHidroDTO);
	}
}