/**
 * 
 */
package mx.com.afirme.midas.catalogos.codigopostalzonahidro;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class CodigoPostalZonaHidroSN {
	private CodigoPostalZonaHidroFacadeRemote beanRemoto;

	public CodigoPostalZonaHidroSN() throws SystemException {
		try {
			LogDeMidasWeb.log("Entrando en CodigoPostalZonaHidroSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CodigoPostalZonaHidroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CodigoPostalZonaHidroDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<CodigoPostalZonaHidroDTO> codigoPostalZonaHidros = beanRemoto.findAll();
		return codigoPostalZonaHidros;

	}
	public List<CodigoPostalZonaHidroDTO> listarFiltrado(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO) throws ExcepcionDeAccesoADatos {
		try {
			List<CodigoPostalZonaHidroDTO> codigoPostalZonaHidros = beanRemoto.listarFiltrado(codigoPostalZonaHidroDTO);
			return codigoPostalZonaHidros;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al listarFiltrado de codigoPostalZonaHidro");
		}
	}
	
	public CodigoPostalZonaHidroDTO getPorId(CodigoPostalZonaHidroId id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	
	public void agregarCodigoPostalZonaHidroZonaSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO,
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.agregarCodigoPostalZonasHidroSismo
		(codigoPostalZonaHidroDTO, codigoPostalZonaSismoDTO);
	}
	
	public void modificarCodigoPostalZonaHidroZonaSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO,
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO){
		beanRemoto.modificarCodigoPostalZonasHidroSismo
		(codigoPostalZonaHidroDTO, codigoPostalZonaSismoDTO);
	}
	
	public Long obtenerTotalFiltrado(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalFiltrado(codigoPostalZonaHidroDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalFiltrado de codigoPostalZonaHidro");
		}
		
	}
	
}
