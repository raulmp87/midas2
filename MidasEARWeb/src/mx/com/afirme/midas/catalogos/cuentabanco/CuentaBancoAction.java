package mx.com.afirme.midas.catalogos.cuentabanco;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



public class CuentaBancoAction extends MidasMappingDispatchAction{
	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();
			CuentaBancoForm cuentaBancoForm = (CuentaBancoForm) form;
			CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();
			this.poblarDTO(cuentaBancoForm, cuentaBancoDTO);
			cuentaBancoDTO.setEstatus(null);
			List<CuentaBancoDTO> cuentasbancos = new ArrayList<CuentaBancoDTO>();
			cuentasbancos = cuentaBancoDN.listarFiltrado(cuentaBancoDTO);
			//listar En tabla, Pesos y Dolares en vez de 840 y 484
			for(int i = 0; i < cuentasbancos.size(); i++)
				cuentasbancos.get(i).setFfc((cuentasbancos.get(i).getIdTcMoneda().toString().equals("840"))? "D&oacute;lares":"Pesos");
			request.setAttribute("cuentasbancos",cuentasbancos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		limpiarForm((CuentaBancoForm)form);
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);

	}
	
  	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request)throws SystemException, ExcepcionDeAccesoADatos {
		CuentaBancoDN cuentabancoDN = CuentaBancoDN.getInstancia();
		List<CuentaBancoDTO> cuentasbancos = cuentabancoDN.listarTodos();
		//listar En tabla, Pesos y Dolares en vez de 840 y 484
		for(int i = 0; i < cuentasbancos.size(); i++)
			cuentasbancos.get(i).setFfc((cuentasbancos.get(i).getIdTcMoneda().toString().equals("840"))? "D&oacute;lares":"Pesos");
		request.setAttribute("cuentasbancos",cuentasbancos);
	}

	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 				= Sistema.EXITOSO;
		CuentaBancoForm cuentabancoForm 	= (CuentaBancoForm) form;
		CuentaBancoDTO cuentabancoDTO 		= new CuentaBancoDTO();
		CuentaBancoDN cuentabancoDN 		= CuentaBancoDN.getInstancia();
		try {
			this.poblarDTO(cuentabancoForm, cuentabancoDTO);
			cuentabancoDN.agregar(cuentabancoDTO);
			this.listarTodos(request);
			cuentabancoForm.setMensajeUsuario(null,"guardar");
	 	} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
	 	} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
	 	}
 		return mapping.findForward(reglaNavegacion);

	}
	
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion 				= Sistema.EXITOSO;
		CuentaBancoForm cuentabancoForm 	= (CuentaBancoForm) form;
		CuentaBancoDTO cuentabancoDTO 		= new CuentaBancoDTO();
		CuentaBancoDN cuentabancoDN 		= CuentaBancoDN.getInstancia();
		try {
			if (!StringUtil.isEmpty(cuentabancoForm.getIdtcCuentaBanco()))
				cuentabancoDTO = cuentabancoDN.getPorId(new BigDecimal(cuentabancoForm.getIdtcCuentaBanco()));
			this.poblarDTO(cuentabancoForm, cuentabancoDTO);
			cuentabancoDN.modificar(cuentabancoDTO);
//			this.listarTodos(request);
			limpiarForm(cuentabancoForm);
			cuentabancoForm.setMensajeUsuario(null,"modificar");
  		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
 		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
 		}
		return mapping.findForward(reglaNavegacion);

	}

	private void limpiarForm(CuentaBancoForm form){
		if(form!= null){
			form.setReaseguradorCorredor("");
			form.setPais("");
			form.setNombreBanco("");
			form.setNumeroCuenta("");
			form.setNumeroClabe("");
			form.setIdTcMoneda("");
		}
	}
	
	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		CuentaBancoForm cuentabancoForm 	= (CuentaBancoForm) form;
		CuentaBancoDTO cuentabancoDTO 		= new CuentaBancoDTO();
		String reglaNavegacion 				= Sistema.EXITOSO;
		CuentaBancoDN cuentabancoDN 		= CuentaBancoDN.getInstancia();
		try {
			if (!StringUtil.isEmpty(cuentabancoForm.getIdtcCuentaBanco())){
				cuentabancoDTO = cuentabancoDN.getPorId(new BigDecimal(cuentabancoForm.getIdtcCuentaBanco()));
				this.poblarDTO(cuentabancoForm, cuentabancoDTO);
				cuentabancoDN.borrar(cuentabancoDTO);
				cuentabancoForm.setMensajeUsuario(null,"eliminar");
			}
			this.listarTodos(request);
		} catch (SystemException e) {
			try {
				this.listarTodos(request);
			}catch (SystemException ex) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			}
		} catch (ExcepcionDeAccesoADatos e) {
			try {
				this.listarTodos(request);
			}catch (SystemException ex) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
		} catch (Exception e){
			if(e.getClass().getName().split("saction")[1].equals("RolledbackException")){
				cuentabancoForm.setMensaje("No Se Pudo Borrar La Cuenta De Banco Seleccionada Por Que Existen Referencias A Esta Cuenta");
				try {
					this.listarTodos(request);
				} catch (SystemException e1) {
					reglaNavegacion = Sistema.NO_EXITOSO;
				}
			}
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		CuentaBancoForm cuentabancoForm = (CuentaBancoForm) form;
		CuentaBancoDTO cuentabancoDTO 	= new CuentaBancoDTO();
		CuentaBancoDN cuentabancoDN 	= CuentaBancoDN.getInstancia();
		try {
			this.poblarDTO(cuentabancoForm, cuentabancoDTO);
			cuentabancoDTO = cuentabancoDN.getPorId(cuentabancoDTO);
			this.poblarForm(cuentabancoDTO,cuentabancoForm);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}

	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		CuentaBancoForm cuentabancoForm = (CuentaBancoForm) form;
		CuentaBancoDTO cuentabancoDTO 	= new CuentaBancoDTO();
		String id 						= request.getParameter("id");
		
		cuentabancoDTO.setIdtccuentabanco(BigDecimal.valueOf(Double.valueOf(id)));
		CuentaBancoDN cuentabancoDN = CuentaBancoDN.getInstancia();
		try {
			this.poblarDTO(cuentabancoForm, cuentabancoDTO);
			cuentabancoDTO = cuentabancoDN.getPorId(cuentabancoDTO);
			this.poblarForm(cuentabancoDTO, cuentabancoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method poblarForm
	 * 
	 * @param CuentaBancoDTO
	 * @param ContactoForm
	 */

	private void poblarForm(CuentaBancoDTO cuentabancoDTO, CuentaBancoForm cuentabancoForm) {
		if (cuentabancoDTO.getIdtccuentabanco() != null) cuentabancoForm.setIdtcCuentaBanco(cuentabancoDTO.getIdtccuentabanco().toString());
		if (!StringUtil.isEmpty(cuentabancoDTO.getBeneficiariocuenta())) cuentabancoForm.setBeneficiarioCuenta(cuentabancoDTO.getBeneficiariocuenta());
		if (!StringUtil.isEmpty(cuentabancoDTO.getCuentaffc())) cuentabancoForm.setCuentaFfc(cuentabancoDTO.getCuentaffc());
		if (cuentabancoDTO.getIdTcMoneda() != null) cuentabancoForm.setIdTcMoneda(cuentabancoDTO.getIdTcMoneda().toString());
		if (!StringUtil.isEmpty(cuentabancoDTO.getDireccionbanco())) cuentabancoForm.setDireccionBanco(cuentabancoDTO.getDireccionbanco());
		if (cuentabancoDTO.getEstatus() != null) cuentabancoForm.setStatus(cuentabancoDTO.getEstatus().toString());
		if (!StringUtil.isEmpty(cuentabancoDTO.getFfc())) cuentabancoForm.setFfc(cuentabancoDTO.getFfc());
		if (!StringUtil.isEmpty(cuentabancoDTO.getNombrebanco())) cuentabancoForm.setNombreBanco(cuentabancoDTO.getNombrebanco());
		if (cuentabancoDTO.getNumeroaba() != null) cuentabancoForm.setNumeroAba(cuentabancoDTO.getNumeroaba());
		if (cuentabancoDTO.getNumeroclabe() != null) cuentabancoForm.setNumeroClabe((cuentabancoDTO.getNumeroclabe()));
		if (cuentabancoDTO.getNumerocuenta() != null) cuentabancoForm.setNumeroCuenta(cuentabancoDTO.getNumerocuenta());
		if (cuentabancoDTO.getNumeroswift() != null) cuentabancoForm.setNumeroSwift(cuentabancoDTO.getNumeroswift());
		if (!StringUtil.isEmpty(cuentabancoDTO.getPais())) cuentabancoForm.setPais(cuentabancoDTO.getPais().toString());
		if (cuentabancoDTO.getReaseguradorCorredor() != null){
			cuentabancoForm.setReaseguradorCorredor(cuentabancoDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor().toString());
		}
	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param ContactoForm
	 * @param CuentaBancoDTO
	 */
	private void poblarDTO(CuentaBancoForm cuentabancoForm, CuentaBancoDTO cuentabancoDTO) throws SystemException{
		if (!StringUtil.isEmpty(cuentabancoForm.getBeneficiarioCuenta())) cuentabancoDTO.setBeneficiariocuenta(cuentabancoForm.getBeneficiarioCuenta());
		cuentabancoDTO.setCuentaffc(cuentabancoForm.getCuentaFfc());
		if (!StringUtil.isEmpty(cuentabancoForm.getIdTcMoneda())) cuentabancoDTO.setIdTcMoneda(BigDecimal.valueOf( Double.valueOf( cuentabancoForm.getIdTcMoneda())));
		if (!StringUtil.isEmpty(cuentabancoForm.getDireccionBanco())) cuentabancoDTO.setDireccionbanco(cuentabancoForm.getDireccionBanco());
		cuentabancoDTO.setEstatus(new BigDecimal(1)); //Valor por default: Activo
		cuentabancoDTO.setFfc(cuentabancoForm.getFfc());
		if (!StringUtil.isEmpty(cuentabancoForm.getNombreBanco())) cuentabancoDTO.setNombrebanco(cuentabancoForm.getNombreBanco());
		cuentabancoDTO.setNumeroaba(cuentabancoForm.getNumeroAba());
		cuentabancoDTO.setNumeroclabe(cuentabancoForm.getNumeroClabe());
		cuentabancoDTO.setNumerocuenta(cuentabancoForm.getNumeroCuenta());

		cuentabancoDTO.setNumeroswift(cuentabancoForm.getNumeroSwift());
		if (!StringUtil.isEmpty(cuentabancoForm.getPais())) cuentabancoDTO.setPais(cuentabancoForm.getPais());
		if (!StringUtil.isEmpty(cuentabancoForm.getReaseguradorCorredor())){
			ReaseguradorCorredorDN reaseguradorCorredorDN = new ReaseguradorCorredorDN();
			ReaseguradorCorredorDTO redto = new ReaseguradorCorredorDTO();
			redto.setIdtcreaseguradorcorredor(new BigDecimal(cuentabancoForm.getReaseguradorCorredor()));
			redto = reaseguradorCorredorDN.obtenerReaseguradorPorId(redto);
			if (redto != null)
				cuentabancoDTO.setReaseguradorCorredor(redto);
		}
		if (!StringUtil.isEmpty(cuentabancoForm.getIdTcMoneda()))
			cuentabancoDTO.setIdTcMoneda(new BigDecimal(cuentabancoForm.getIdTcMoneda()));
	}
	
}//end class