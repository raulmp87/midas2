package mx.com.afirme.midas.catalogos.cuentabanco;


import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @struts.form name="cuentabancoForm"
 */
public class CuentaBancoForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	private String idtcCuentaBanco;	
	private String reaseguradorCorredor;
	private String nombreBanco;
	private String numeroCuenta;
	private String numeroAba;
	private String numeroSwift;
	private String beneficiarioCuenta;
	private String numeroClabe;
	private String direccionBanco;
	private String status;
	private String pais;
	private String ffc;
	private String cuentaFfc;
	private String idTcMoneda;
    private String numeroBanco;

    
    /**
	 * @return the idtcCuentaBanco
	 */
	public String getIdtcCuentaBanco() {
		return idtcCuentaBanco;
	}
	/**
	 * @param idtcCuentaBanco the idtcCuentaBanco to set
	 */
	public void setIdtcCuentaBanco(String idtcCuentaBanco) {
		this.idtcCuentaBanco = idtcCuentaBanco;
	}
	/**
	 * @return the numeroBanco
	 */
	public String getNumeroBanco() {
		return numeroBanco;
	}
	/**
	 * @param numeroBanco the numeroBanco to set
	 */
	public void setNumeroBanco(String numeroBanco) {
		this.numeroBanco = numeroBanco;
	}
	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	/**
	 * @param numeroCuenta the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	/**
	 * @return the numeroAba
	 */
	public String getNumeroAba() {
		return numeroAba;
	}
	/**
	 * @param numeroAba the numeroAba to set
	 */
	public void setNumeroAba(String numeroAba) {
		this.numeroAba = numeroAba;
	}
	/**
	 * @return the numeroSwift
	 */
	public String getNumeroSwift() {
		return numeroSwift;
	}
	/**
	 * @param numeroSwift the numeroSwift to set
	 */
	public void setNumeroSwift(String numeroSwift) {
		this.numeroSwift = numeroSwift;
	}
	/**
	 * @return the beneficiarioCuenta
	 */
	public String getBeneficiarioCuenta() {
		return beneficiarioCuenta;
	}
	/**
	 * @param beneficiarioCuenta the beneficiarioCuenta to set
	 */
	public void setBeneficiarioCuenta(String beneficiarioCuenta) {
		this.beneficiarioCuenta = beneficiarioCuenta;
	}
	/**
	 * @return the numeroClabe
	 */
	public String getNumeroClabe() {
		return numeroClabe;
	}
	/**
	 * @param numeroClabe the numeroClabe to set
	 */
	public void setNumeroClabe(String numeroClabe) {
		this.numeroClabe = numeroClabe;
	}
	/**
	 * @return the direccionBanco
	 */
	public String getDireccionBanco() {
		return direccionBanco;
	}
	/**
	 * @param direccionBanco the direccionBanco to set
	 */
	public void setDireccionBanco(String direccionBanco) {
		this.direccionBanco = direccionBanco;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}
	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	/**
	 * @return the ffc
	 */
	public String getFfc() {
		return ffc;
	}
	/**
	 * @param ffc the ffc to set
	 */
	public void setFfc(String ffc) {
		this.ffc = ffc;
	}
	/**
	 * @return the cuentaFfc
	 */
	public String getCuentaFfc() {
		return cuentaFfc;
	}
	/**
	 * @param cuentaFfc the cuentaFfc to set
	 */
	public void setCuentaFfc(String cuentaFfc) {
		this.cuentaFfc = cuentaFfc;
	}
	/**
	 * @return the dato1
	 */
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	/**
	 * @param dato1 the dato1 to set
	 */
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	/**
	 * @the reaseguradorCorredor
	 */
	public String getReaseguradorCorredor() {
		return reaseguradorCorredor;
	}
	
	public void setReaseguradorCorredor(String reaseguradorCorredor) {
		this.reaseguradorCorredor = reaseguradorCorredor;
	}
	/**
	 * @return the nombreBanco
	 */
	public String getNombreBanco() {
		return nombreBanco;
	}
	/**
	 * @param nombreBanco the nombreBanco to set
	 */
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	
}
