package mx.com.afirme.midas.catalogos.cuentabanco;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;

/**
 * @author 
 * 
 */

public class CuentaBancoSN  {

	private CuentaBancoFacadeRemote beanRemoto;

	public CuentaBancoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando enCuentaBancoSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CuentaBancoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CuentaBancoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<CuentaBancoDTO> cuentasBancos= beanRemoto.findAll();
		return cuentasBancos;
	}

	public void agregar(CuentaBancoDTO cuentabancoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(cuentabancoDTO);
	}

	public void modificar(CuentaBancoDTO cuentabancoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(cuentabancoDTO);
	}

	public CuentaBancoDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
		
	}

	public void borrar(CuentaBancoDTO cuentabancoDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(cuentabancoDTO);
	}
	
	public List<CuentaBancoDTO> listarFiltrado(CuentaBancoDTO cuentabancoDTO) throws ExcepcionDeAccesoADatos {
		List<CuentaBancoDTO> cuentasBancos= beanRemoto.listarFiltrado(cuentabancoDTO);
		return cuentasBancos;
	}
	
	
}


 

