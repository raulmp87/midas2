package mx.com.afirme.midas.catalogos.cuentabanco;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CuentaBancoDN {

	private static final CuentaBancoDN INSTANCIA = new CuentaBancoDN();

	public static CuentaBancoDN getInstancia() {
		return CuentaBancoDN.INSTANCIA;
	}

	public List<CuentaBancoDTO> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		CuentaBancoSN  cuentabancoSN = new CuentaBancoSN();
		return cuentabancoSN.listarTodos();
	}

	public void agregar(CuentaBancoDTO cuentabancoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		CuentaBancoSN cuentabancoSN = new CuentaBancoSN();
		cuentabancoSN.agregar(cuentabancoDTO);
	}

	public void modificar(CuentaBancoDTO cuentabancoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		CuentaBancoSN cuentabancoSN = new CuentaBancoSN();
		cuentabancoSN.modificar(cuentabancoDTO);
	}

	public CuentaBancoDTO getPorId(CuentaBancoDTO cuentabancoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		CuentaBancoSN cuentabancoSN = new CuentaBancoSN();
		return cuentabancoSN.getPorId(cuentabancoDTO.getIdtccuentabanco());
	}

	public CuentaBancoDTO getPorId(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		CuentaBancoSN cuentabancoSN = new CuentaBancoSN();
		return cuentabancoSN.getPorId(id);
	}
	
	
	public void borrar(CuentaBancoDTO cuentabancoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		CuentaBancoSN cuentabancoSN = new CuentaBancoSN();
		cuentabancoSN.borrar(cuentabancoDTO);
	}
	
	public List<CuentaBancoDTO> listarFiltrado(CuentaBancoDTO cuentabancoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		CuentaBancoSN  cuentabancoSN = new CuentaBancoSN();
		return cuentabancoSN.listarFiltrado(cuentabancoDTO);
	}
	
}
