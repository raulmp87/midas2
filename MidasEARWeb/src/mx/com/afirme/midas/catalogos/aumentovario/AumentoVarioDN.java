package mx.com.afirme.midas.catalogos.aumentovario;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AumentoVarioDN {
	private static final AumentoVarioDN INSTANCIA = new AumentoVarioDN();

	public static AumentoVarioDN getInstancia() {
		return AumentoVarioDN.INSTANCIA;
	}

	public List<AumentoVarioDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.listarTodos();
	}

	public List<AumentoVarioDTO> listarFiltrados(AumentoVarioDTO aumentoVarioDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.listarFiltrados(aumentoVarioDTO);
	}

	public void agregar(AumentoVarioDTO aumentoVarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		aumentoVarioSN.agregar(aumentoVarioDTO);
	}

	public void modificar(AumentoVarioDTO aumentoVarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		aumentoVarioSN.modificar(aumentoVarioDTO);
	}

	public AumentoVarioDTO getPorId(AumentoVarioDTO aumentoVarioDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		return aumentoVarioSN.getPorId(aumentoVarioDTO.getIdAumentoVario());
	}

	public void borrar(AumentoVarioDTO aumentoVarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		AumentoVarioSN aumentoVarioSN = new AumentoVarioSN();
		aumentoVarioSN.borrar(aumentoVarioDTO);
	}
}
