/**
 * 
 */
package mx.com.afirme.midas.catalogos.aumentovario;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;

/**
 * @author admin
 * 
 */
public class AumentoVarioSN {
	private AumentoVarioFacadeRemote beanRemoto;

	public AumentoVarioSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(AumentoVarioFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<AumentoVarioDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<AumentoVarioDTO> aumentoVarios;
		try {
			aumentoVarios = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return aumentoVarios;

	}

	public List<AumentoVarioDTO> listarFiltrados(AumentoVarioDTO aumentoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		List<AumentoVarioDTO> aumentoVarios;
		try {
			aumentoVarios = beanRemoto.listarFiltrado(aumentoVarioDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}

		return aumentoVarios;

	}

	public void agregar(AumentoVarioDTO aumentoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(aumentoVarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(AumentoVarioDTO aumentoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(aumentoVarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AumentoVarioDTO getPorId(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}

	}

	public void borrar(AumentoVarioDTO aumentoVarioDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(aumentoVarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<AumentoVarioDTO> listarAumentosPorAsociar(
			BigDecimal idToProducto) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarAumentosPorAsociar(idToProducto);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoVarioDTO> listarAumentosPorAsociarTipoPoliza(
			BigDecimal idToTipoPoliza)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarAumentosPorAsociarTipoPoliza(
					idToTipoPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<AumentoVarioDTO> listarAumentosPorAsociarCobertura(
			BigDecimal idToCobertura) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarAumentosPorAsociarCobetura(idToCobertura);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
