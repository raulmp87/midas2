package mx.com.afirme.midas.catalogos.reaseguradorcorredor;



/**
 * @author jorge.cano 
 * @author Christian Ceballos
 */
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReaseaguradorCorredorSN {

	private ReaseguradorCorredorFacadeRemote beanRemoto;

	public ReaseaguradorCorredorSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(ReaseguradorCorredorFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(reaseguradorCorredorDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(reaseguradorCorredorDTO);
			
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public List<ReaseguradorCorredorDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		List<ReaseguradorCorredorDTO> reaseguradorCorredorList;
		try {
			reaseguradorCorredorList = beanRemoto
			.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return reaseguradorCorredorList;
	}
	
	public String borrar(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
	throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(reaseguradorCorredorDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<ReaseguradorCorredorDTO> listarFiltrado(ReaseguradorCorredorDTO reaseguradorCorredorDTO) throws SystemException, ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(reaseguradorCorredorDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ReaseguradorCorredorDTO obtenerReaseguradorPorId(ReaseguradorCorredorDTO reaseguradorCorredorDTO) throws SystemException, ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ReaseguradorCorredorDTO obtenerReaseguradorPorId(BigDecimal idtcreaseguradorcorredor) throws SystemException, ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idtcreaseguradorcorredor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ReaseguradorCorredorDTO findByProperty(String property, Object value) throws SystemException, ExcepcionDeAccesoADatos {
		try {
			List<ReaseguradorCorredorDTO> corredorDTOs= beanRemoto.findByProperty(property, value);
			
			if (corredorDTOs.size() > 0 )
				return corredorDTOs.get(0);
			else
				return new ReaseguradorCorredorDTO();
			
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	

	public boolean existe(ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
		boolean existe = false;
		try {
			List<ReaseguradorCorredorDTO> list = beanRemoto.findByProperty("cnfs", 
					reaseguradorCorredorDTO.getCnfs());
			if(!UtileriasWeb.esObjetoNulo(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor())){
				if (list.size() > 1) // Si hay m�s de un registro con el mismo CNFS entonces s� est� duplicado
					existe = true;
				else
					if (list.size()==1){ //Si solamente hay un registro entonces hay que validar que no se trate del mismo elemento (en el caso en que se actualiza)
						ReaseguradorCorredorDTO reaseguradorCorredorDTO2 = list.get(0);
						if (!reaseguradorCorredorDTO2.getIdtcreaseguradorcorredor().toBigInteger().equals((reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().toBigInteger())))
							existe = true;
					}
						
			}else
				if (list.size() > 0) {
					existe = true;
				}
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return existe;
	}
	
	
	/*public boolean existerfc(ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
		boolean existe = false;
		try {
			List<ReaseguradorCorredorDTO> list = beanRemoto.findByProperty("rfc", 
					reaseguradorCorredorDTO.getrfc);
			if(!UtileriasWeb.esObjetoNulo(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor())){
				if (list.size() > 1) // Si hay m�s de un registro con el mismo CNFS entonces s� est� duplicado
					existe = true;
				else
					if (list.size()==1){ //Si solamente hay un registro entonces hay que validar que no se trate del mismo elemento (en el caso en que se actualiza)
						ReaseguradorCorredorDTO reaseguradorCorredorDTO2 = list.get(0);
						if (!reaseguradorCorredorDTO2.getIdtcreaseguradorcorredor().toBigInteger().equals((reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().toBigInteger())))
							existe = true;
					}
						
			}else
				if (list.size() > 0) {
					existe = true;
				}
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return existe;
	}*/
	
}
