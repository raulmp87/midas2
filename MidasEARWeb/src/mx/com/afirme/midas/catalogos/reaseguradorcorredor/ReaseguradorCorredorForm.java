/**
 * 
 */
package mx.com.afirme.midas.catalogos.reaseguradorcorredor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.impuestoresidenciafiscal.ImpuestoResidenciaFiscalDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jorge.cano
 * @author Christian Ceballos
 * 
 */
public class ReaseguradorCorredorForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idTcReaseguradorCorredor;
	private String nombre;
	private String ubicacion;
	private String correoElectronico;
	private String cnfs;
	private String tipo;
	private String estatus;
	private String nombreCorto;
	private String procedencia;
	private String ciudad;
	private String estado;
	private String pais;
	private String telefonoFijo;
	private String telefonoMovil;
	private String Rfc;	
    private String idContable;	
	private Set<CuentaBancoDTO> tcCuentaBancos = new HashSet<CuentaBancoDTO>(0);
		
	private String idTcImpuestoResidenciaFiscal;
	private String tieneConstanciaResidenciaFiscal;
	private String fechaInicioVigenciaConstanciaResidenciaFiscal;
	private String fechaFinVigenciaConstanciaResidenciaFiscal;
	
	private String calificacionCNSF;
	private String agenciaCalificacion;

	/**
	 * @return the idTcReaseguradorCorredor
	 */
	public String getIdTcReaseguradorCorredor() {
		return idTcReaseguradorCorredor;
	}

	/**
	 * @param idTcReaseguradorCorredor the idTcReaseguradorCorredor to set
	 */
	public void setIdTcReaseguradorCorredor(String idTcReaseguradorCorredor) {
		this.idTcReaseguradorCorredor = idTcReaseguradorCorredor;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the ubicacion
	 */
	public String getUbicacion() {
		return ubicacion;
	}

	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	/**
	 * @return the correoElectronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * @param correoElectronico the correoElectronico to set
	 */
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * @return the cnfs
	 */
	public String getCnfs() {
		return cnfs;
	}

	/**
	 * @param cnfs the cnfs to set
	 */
	public void setCnfs(String cnfs) {
		this.cnfs = cnfs;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the nombreCorto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * @param nombreCorto the nombreCorto to set
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	/**
	 * @return the procedencia
	 */
	public String getProcedencia() {
		return procedencia;
	}

	/**
	 * @param procedencia the procedencia to set
	 */
	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	/**
	 * @return the ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}

	/**
	 * @param ciudad the ciudad to set
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return the telefonoFijo
	 */
	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	/**
	 * @param telefonoFijo the telefonoFijo to set
	 */
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	/**
	 * @return the telefonoMovil
	 */
	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	/**
	 * @param telefonoMovil the telefonoMovil to set
	 */
	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}
	
	/**
	 * @return the Rfc
	 */
	public String getRfc() {
		return Rfc;
	}

	/**
	 * @param Rfc the Rfc to set
	 */
	public void setRfc(String Rfc) {
		this.Rfc = Rfc;
	}

	/**
	 * @return the tcCuentaBancos
	 */
	public Set<CuentaBancoDTO> getTcCuentaBancos() {
		return tcCuentaBancos;
	}

	/**
	 * @param tcCuentaBancos the tcCuentaBancos to set
	 */
	public void setTcCuentaBancos(Set<CuentaBancoDTO> tcCuentaBancos) {
		this.tcCuentaBancos = tcCuentaBancos;
	}

	public String getIdContable() {
		return idContable;
	}

	public void setIdContable(String idContable) {
		this.idContable = idContable;
	}

	public String getIdTcImpuestoResidenciaFiscal() {
		return idTcImpuestoResidenciaFiscal;
	}

	public void setIdTcImpuestoResidenciaFiscal(String idTcImpuestoResidenciaFiscal) {
		this.idTcImpuestoResidenciaFiscal = idTcImpuestoResidenciaFiscal;
	}

	public String getTieneConstanciaResidenciaFiscal() {
		return tieneConstanciaResidenciaFiscal;
	}

	public void setTieneConstanciaResidenciaFiscal(
			String tieneConstanciaResidenciaFiscal) {
		this.tieneConstanciaResidenciaFiscal = tieneConstanciaResidenciaFiscal;
	}

	public String getFechaInicioVigenciaConstanciaResidenciaFiscal() {
		return fechaInicioVigenciaConstanciaResidenciaFiscal;
	}

	public void setFechaInicioVigenciaConstanciaResidenciaFiscal(
			String fechaInicioVigenciaConstanciaResidenciaFiscal) {
		this.fechaInicioVigenciaConstanciaResidenciaFiscal = fechaInicioVigenciaConstanciaResidenciaFiscal;
	}

	public String getFechaFinVigenciaConstanciaResidenciaFiscal() {
		return fechaFinVigenciaConstanciaResidenciaFiscal;
	}

	public void setFechaFinVigenciaConstanciaResidenciaFiscal(
			String fechaFinVigenciaConstanciaResidenciaFiscal) {
		this.fechaFinVigenciaConstanciaResidenciaFiscal = fechaFinVigenciaConstanciaResidenciaFiscal;
	}

	public String getCalificacionCNSF() {
		return calificacionCNSF;
	}

	public void setCalificacionCNSF(String calificacionCNSF) {
		this.calificacionCNSF = calificacionCNSF;
	}

	public String getAgenciaCalificacion() {
		return agenciaCalificacion;
	}

	public void setAgenciaCalificacion(String agenciaCalificacion) {
		this.agenciaCalificacion = agenciaCalificacion;
	}
	
	
}
