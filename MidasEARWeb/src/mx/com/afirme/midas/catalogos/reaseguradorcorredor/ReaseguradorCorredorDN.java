package mx.com.afirme.midas.catalogos.reaseguradorcorredor;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * This class will retrieve requested resources.
 * 
 * @author jorge.cano
 * @author Christian Ceballos
 * @version 1.00, Junio 26, 2009
 */
public class ReaseguradorCorredorDN {
	private static final ReaseguradorCorredorDN INSTANCIA = new ReaseguradorCorredorDN();

	/**
	 * Retorna <code>ReaseguradorCorredorDN</code> instancia singleton.
	 * 
	 * @return <code>ReaseguradorCorredorDN</code> instancia singleton.
	 */
	public static ReaseguradorCorredorDN getInstancia() {
		return ReaseguradorCorredorDN.INSTANCIA;
	}

	public String guardar(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		reaseaguradorCorredorSN.guardar(reaseguradorCorredorDTO);
		return null;
	}

	public String modificar(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		reaseaguradorCorredorSN.modificar(reaseguradorCorredorDTO);
		return null;
	}

	public String borrar(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		reaseaguradorCorredorSN.borrar(reaseguradorCorredorDTO);
		return null;
	}

	public ReaseguradorCorredorDTO obtenerReaseguradorPorId(
			ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		return reaseaguradorCorredorSN
				.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
	}

	public ReaseguradorCorredorDTO obtenerReaseguradorPorId(
			BigDecimal idTcReaseguradorCorredor) throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		return reaseaguradorCorredorSN
				.obtenerReaseguradorPorId(idTcReaseguradorCorredor);
	}
	
	
	public ReaseguradorCorredorDTO findByProperty(String property, Object value)
	throws SystemException,
	ExcepcionDeAccesoADatos 
	{ 
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		return reaseaguradorCorredorSN
				.findByProperty(property, value);
	}
	
	public List<ReaseguradorCorredorDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		return reaseaguradorCorredorSN.listarTodos();
	}

	public List<ReaseguradorCorredorDTO> listarFiltrado(
			ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		return reaseaguradorCorredorSN.listarFiltrado(reaseguradorCorredorDTO);
	}

	public boolean existe(ReaseguradorCorredorDTO reaseguradorCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReaseaguradorCorredorSN reaseaguradorCorredorSN;
		reaseaguradorCorredorSN = new ReaseaguradorCorredorSN();
		return reaseaguradorCorredorSN.existe(reaseguradorCorredorDTO);
	}
}
