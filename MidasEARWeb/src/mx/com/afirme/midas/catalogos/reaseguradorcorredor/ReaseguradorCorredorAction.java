package mx.com.afirme.midas.catalogos.reaseguradorcorredor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.impuestoresidenciafiscal.ImpuestoResidenciaFiscalDN;
import mx.com.afirme.midas.catalogos.impuestoresidenciafiscal.ImpuestoResidenciaFiscalDTO;
import mx.com.afirme.midas.catalogos.impuestoresidenciafiscal.ImpuestoResidenciaFiscalForm;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * @author jorge.cano 
 * @author Christian Ceballos
 */
public class ReaseguradorCorredorAction extends MidasMappingDispatchAction {

	/**
	 * Method guardar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward;
		forward = mapping.findForward(Sistema.EXITOSO);
		ReaseguradorCorredorForm reaseguradorCorredorForm = (ReaseguradorCorredorForm) form;
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
							
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN
				.getInstancia();			
		try {
			this.poblarDTO(reaseguradorCorredorForm, reaseguradorCorredorDTO);
			reaseguradorCorredorDTO.setUsuarioModificacion(usuario.getNombreUsuario());
			//Verificar que el cnfs no exista.
			if (reaseguradorCorredorDN.existe(reaseguradorCorredorDTO)) {
				ActionMessages messages = new ActionMessages();
				messages.add("cnfs",
						new ActionMessage("errors.registro.duplicado"));
				saveErrors(request, messages);
				forward = mapping.getInputForward();
			}
			else {
				reaseguradorCorredorDN.guardar(reaseguradorCorredorDTO);
			 	reaseguradorCorredorForm.setMensajeUsuario(null,"guardar");
			 	limpiarForm(reaseguradorCorredorForm);
				listar(mapping, reaseguradorCorredorForm, request, response);
	 		}

		} catch (ExcepcionDeAccesoADatos e) {
			forward = mapping.findForward(Sistema.NO_EXITOSO);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
  		} catch (SystemException e) {
			forward = mapping.findForward(Sistema.NO_DISPONIBLE);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
 	 	} catch (ParseException e) {
 	 		forward = mapping.findForward(Sistema.NO_EXITOSO);
 	 		reaseguradorCorredorForm.setMensaje("Formato de fecha inv�lido.");
		}
		return forward;
	}
	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ActionForward forward;
		forward = mapping.findForward(Sistema.EXITOSO);
		ReaseguradorCorredorForm reaseguradorCorredorForm = (ReaseguradorCorredorForm) form;
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();		
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			this.poblarDTO(reaseguradorCorredorForm, reaseguradorCorredorDTO);
			//Verificar que el cnfs no exista.
			if (reaseguradorCorredorDN.existe(reaseguradorCorredorDTO)) {
				ActionMessages messages = new ActionMessages();
				messages.add("cnfs",new ActionMessage("errors.registro.duplicado"));
				saveErrors(request, messages);
				forward = mapping.getInputForward();
			}
			else {
				reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
				reaseguradorCorredorDTO.setUsuarioModificacion(usuario.getNombreUsuario() + " - M1");
				this.poblarDTO(reaseguradorCorredorForm, reaseguradorCorredorDTO);
				reaseguradorCorredorDN.modificar(reaseguradorCorredorDTO);
			 	reaseguradorCorredorForm.setMensajeUsuario(null,"modificar");
			 	limpiarForm(reaseguradorCorredorForm);
			  	listar(mapping, reaseguradorCorredorForm, request, response);
 			}
		} catch (ExcepcionDeAccesoADatos e) {
			forward = mapping.findForward(Sistema.NO_EXITOSO);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
 		} catch (SystemException e) {
			forward = mapping.findForward(Sistema.NO_DISPONIBLE);
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
  		}  catch (ParseException e) {
 	 		forward = mapping.findForward(Sistema.NO_EXITOSO);
 	 		reaseguradorCorredorForm.setMensaje("Formato de fecha inv�lido.");
		}
		return forward;
	}
	
	private void limpiarForm(ReaseguradorCorredorForm form){
		if(form != null){
			form.setNombre("");
			form.setTipo("");
			form.setTelefonoFijo("");
		}
	}
	
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ReaseguradorCorredorForm reaseguradorCorredorForm = (ReaseguradorCorredorForm) form;
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		ReaseguradorCorredorDTO reaseguradorCorredorDTO2 = new ReaseguradorCorredorDTO();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN
				.getInstancia();
		try {
			this.poblarDTO(reaseguradorCorredorForm, reaseguradorCorredorDTO);
			
			
			reaseguradorCorredorDTO2 = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor());
			reaseguradorCorredorDTO2.setUsuarioModificacion(usuario.getNombreUsuario());
			
			reaseguradorCorredorDN.modificar(reaseguradorCorredorDTO2);
			
			reaseguradorCorredorDN.borrar(reaseguradorCorredorDTO);
			listar(mapping, reaseguradorCorredorForm, request, response);
		  	reaseguradorCorredorForm.setMensajeUsuario(null,"eliminar");
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			if(e.getMessage().equals(Sistema.EXCEPCION_ROLLBACK))
				reaseguradorCorredorForm.setMensaje("No Se Pudo Borrar El Resgitro Seleccionado Por Que Existen Referencias A Este Registro");
			else
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			return listar(mapping, form, request, response);
	 	} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			request.setAttribute("mensaje", "No se pudo eliminar la informacion");
		} catch (ParseException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
 	 		reaseguradorCorredorForm.setMensaje("Formato de fecha inv�lido.");
		}
		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		try {
			ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
			List<ReaseguradorCorredorDTO> reaseguradores = reaseguradorCorredorDN.listarTodos();
			limpiarForm(form != null ? (ReaseguradorCorredorForm)form : null);
			request.setAttribute("reaseguradores", reaseguradores);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		// AjustadorForm ajustadorForm = (AjustadorForm) form;
		
		ReaseguradorCorredorForm reaseguradorCorredorForm = (ReaseguradorCorredorForm) form;
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		
		try {			
			poblarDTO(reaseguradorCorredorForm, reaseguradorCorredorDTO);
			List<ReaseguradorCorredorDTO> reaseguradores = reaseguradorCorredorDN.listarFiltrado(reaseguradorCorredorDTO);
			request.setAttribute("reaseguradores", reaseguradores);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ParseException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
 	 		reaseguradorCorredorForm.setMensaje("Formato de fecha inv�lido."); 	 		
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method poblarDTO
	 * 
	 * @param form
	 * @param DTO
	 * @return ReaseguradorCorredorDTO
	 * @throws ParseException 
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	private String poblarDTO(ReaseguradorCorredorForm reaseguradorCorredorForm,
			ReaseguradorCorredorDTO reaseguradorCorredorDTO) throws ParseException, ExcepcionDeAccesoADatos, SystemException {
		//TODO:Por mientras que se pide un control de cambios para que
		//-Telefono fijo, Ubicacion, Correo y CNFS no sean requeridos
		//Se va a insertar un espacio en blanco para estos campos y asi
		//permitir ser guardados en la tabla. Queda pendiente tambien
		//Estado y Ciudad
		Double id;
		if (!StringUtil.isEmpty(reaseguradorCorredorForm
				.getIdTcReaseguradorCorredor())) {
			id = Double.valueOf(reaseguradorCorredorForm
					.getIdTcReaseguradorCorredor());
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(BigDecimal
					.valueOf(id.doubleValue()));
		}
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getNombre()))
			reaseguradorCorredorDTO.setNombre(reaseguradorCorredorForm
					.getNombre().trim().toUpperCase());
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getTipo()))
			reaseguradorCorredorDTO.setTipo(reaseguradorCorredorForm.getTipo()
					.trim().toUpperCase());
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getEstatus())) {
			id = Double.valueOf(reaseguradorCorredorForm.getEstatus());
			reaseguradorCorredorDTO.setEstatus(BigDecimal.valueOf(id
					.doubleValue()));
		}
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getNombreCorto()))
			reaseguradorCorredorDTO.setNombrecorto(reaseguradorCorredorForm
					.getNombreCorto().trim().toUpperCase());
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getProcedencia()))
			reaseguradorCorredorDTO.setProcedencia(reaseguradorCorredorForm
					.getProcedencia().trim().toUpperCase());
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getCiudad())){
			reaseguradorCorredorDTO.setCiudad(reaseguradorCorredorForm.getCiudad().trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO.setCiudad(" ");			
		}
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getEstado())){
			reaseguradorCorredorDTO.setEstado(reaseguradorCorredorForm.getEstado().trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO.setEstado(" ");			
		}
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getPais()))
			reaseguradorCorredorDTO.setPais(reaseguradorCorredorForm.getPais()
					.trim().toUpperCase());
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getTelefonoMovil())) {
			reaseguradorCorredorDTO.setTelefonomovil(
					reaseguradorCorredorForm.getTelefonoMovil());
		}
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getTelefonoFijo())) {
			reaseguradorCorredorDTO.setTelefonofijo(
					reaseguradorCorredorForm.getTelefonoFijo());
		}
		
		//Borrar esto cuando ya quiten el nullable en las tablas y dejar
		//lo comentarizado (abajo)
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getUbicacion())) {
			reaseguradorCorredorDTO.setUbicacion(reaseguradorCorredorForm
					.getUbicacion().trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO.setUbicacion(" ");			
		}
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getCnfs())) {
			reaseguradorCorredorDTO.setCnfs(reaseguradorCorredorForm.getCnfs()
					.trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO.setCnfs(" ");			
		}
		if (!StringUtil
				.isEmpty(reaseguradorCorredorForm.getCorreoElectronico())) {
			reaseguradorCorredorDTO
					.setCorreoelectronico(reaseguradorCorredorForm
							.getCorreoElectronico().trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO
			.setCorreoelectronico(" ");			
		}
		
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getIdContable())) {
			id = Double.valueOf(reaseguradorCorredorForm
					.getIdContable());
			reaseguradorCorredorDTO.setIdContable(BigDecimal
					.valueOf(id.doubleValue()));
		}		
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getRfc())) {
			reaseguradorCorredorDTO.setRfc(reaseguradorCorredorForm.getRfc()
					.trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO.setRfc(" ");			
		}
		//Descomentarizar cunado quiten el constraint de las tablas y borar lo
		//de arriba
		/*if (!StringUtil.isEmpty(reaseguradorCorredorForm.getUbicacion())) {
			reaseguradorCorredorDTO.setUbicacion(reaseguradorCorredorForm
					.getUbicacion().trim().toUpperCase());
		}
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getCnfs())) {
			reaseguradorCorredorDTO.setCnfs(reaseguradorCorredorForm.getCnfs()
					.trim().toUpperCase());
		}
		if (!StringUtil
				.isEmpty(reaseguradorCorredorForm.getCorreoElectronico())) {
			reaseguradorCorredorDTO
					.setCorreoelectronico(reaseguradorCorredorForm
							.getCorreoElectronico().trim().toUpperCase());
		}*/		
		
		if(!StringUtil.isEmpty(reaseguradorCorredorForm.getIdTcImpuestoResidenciaFiscal())){
			ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscal = new ImpuestoResidenciaFiscalDTO();
			impuestoResidenciaFiscal.setIdTcImpuestoResidenciaFiscal(Long.valueOf(reaseguradorCorredorForm.getIdTcImpuestoResidenciaFiscal()));
			impuestoResidenciaFiscal = ImpuestoResidenciaFiscalDN.getInstancia().getPorId(impuestoResidenciaFiscal);
			reaseguradorCorredorDTO.setImpuestoResidenciaFiscal(impuestoResidenciaFiscal);
		}
		
		if(!StringUtil.isEmpty(reaseguradorCorredorForm.getTieneConstanciaResidenciaFiscal())){
			reaseguradorCorredorDTO.setTieneConstanciaResidenciaFiscal(Short.valueOf("1")); //Si tiene constancia
		}else
			reaseguradorCorredorDTO.setTieneConstanciaResidenciaFiscal(Short.valueOf("0")); //No tiene constancia
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(!StringUtil.isEmpty(reaseguradorCorredorForm.getFechaInicioVigenciaConstanciaResidenciaFiscal())){						
			Date fecha = sdf.parse(reaseguradorCorredorForm.getFechaInicioVigenciaConstanciaResidenciaFiscal());
			reaseguradorCorredorDTO.setFechaInicioVigenciaConstanciaResidenciaFiscal(fecha);
		}
		
		if(!StringUtil.isEmpty(reaseguradorCorredorForm.getFechaFinVigenciaConstanciaResidenciaFiscal())){			
			Date fecha = sdf.parse(reaseguradorCorredorForm.getFechaFinVigenciaConstanciaResidenciaFiscal());
			reaseguradorCorredorDTO.setFechaFinVigenciaConstanciaResidenciaFiscal(fecha);
		} 
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getCalificacionCNSF())) {
			reaseguradorCorredorDTO.setCalificacionCNSF(reaseguradorCorredorForm.getCalificacionCNSF()
					.trim().toUpperCase());
		}
		else {
			reaseguradorCorredorDTO.setCalificacionCNSF(" ");			
		}
		
		if (!StringUtil.isEmpty(reaseguradorCorredorForm.getAgenciaCalificacion())) {
			reaseguradorCorredorDTO.setAgenciaCalCNSF(new BigDecimal(reaseguradorCorredorForm.getAgenciaCalificacion()));
		}
		else {
			reaseguradorCorredorDTO.setAgenciaCalCNSF(new BigDecimal(0));			
		}
		return null;

	}
	
	public static void poblarForm(ReaseguradorCorredorForm reaseguradorCorredorForm,
			ReaseguradorCorredorDTO reaseguradorCorredorDTO){
		reaseguradorCorredorForm.setCiudad(reaseguradorCorredorDTO.getCiudad());
		reaseguradorCorredorForm.setEstado(reaseguradorCorredorDTO.getEstado());
		reaseguradorCorredorForm.setEstatus(reaseguradorCorredorDTO.getEstatus().toBigInteger().toString());
		reaseguradorCorredorForm.setIdTcReaseguradorCorredor(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().toString());
		reaseguradorCorredorForm.setNombre(reaseguradorCorredorDTO.getNombre());
		reaseguradorCorredorForm.setNombreCorto(reaseguradorCorredorDTO.getNombrecorto());
		reaseguradorCorredorForm.setPais(reaseguradorCorredorDTO.getPais());
		reaseguradorCorredorForm.setProcedencia(reaseguradorCorredorDTO.getProcedencia());
		if (reaseguradorCorredorDTO.getTelefonomovil() != null) {
			reaseguradorCorredorForm.setTelefonoMovil(reaseguradorCorredorDTO.getTelefonomovil());
		}
		reaseguradorCorredorForm.setTipo(reaseguradorCorredorDTO.getTipo());
		reaseguradorCorredorForm.setTelefonoFijo(reaseguradorCorredorDTO.getTelefonofijo());
		
		
		//TODO:Por mientras que se pide un control de cambios para que
		//Ubicacion, Correo y CNFS no sean requeridos
		//Se va a insertar un espacio en blanco para estos campos y asi
		//permitir ser guardados en la tabla. Queda pendiente tambien
		//Estado y Ciudad.
		//Descomentarizar lo de abajo cuando ya este solucionado y quitar esto.
		if (!reaseguradorCorredorDTO.getUbicacion().equals(" ")) {
			reaseguradorCorredorForm.setUbicacion(reaseguradorCorredorDTO.getUbicacion());
		}
		if (!reaseguradorCorredorDTO.getCnfs().equals(" ")) {
			reaseguradorCorredorForm.setCnfs(reaseguradorCorredorDTO.getCnfs());
		}
		if (!reaseguradorCorredorDTO.getCorreoelectronico().equals(" ")) {
			reaseguradorCorredorForm.setCorreoElectronico(reaseguradorCorredorDTO.getCorreoelectronico());
		}
		
		if (reaseguradorCorredorDTO.getIdContable()!= null)
			reaseguradorCorredorForm.setIdContable(reaseguradorCorredorDTO.getIdContable().toBigInteger().toString());

		if (reaseguradorCorredorDTO.getRfc()!= null)
			reaseguradorCorredorForm.setRfc(reaseguradorCorredorDTO.getRfc().toString());
		/*
		reaseguradorCorredorForm.setUbicacion(reaseguradorCorredorDTO.getUbicacion());
		reaseguradorCorredorForm.setCnfs(reaseguradorCorredorDTO.getCnfs());
		reaseguradorCorredorForm.setCorreoElectronico(reaseguradorCorredorDTO.getCorreoelectronico()); */
		
		if(reaseguradorCorredorDTO.getImpuestoResidenciaFiscal() != null){
			reaseguradorCorredorForm.setIdTcImpuestoResidenciaFiscal(
					reaseguradorCorredorDTO.getImpuestoResidenciaFiscal().getIdTcImpuestoResidenciaFiscal().toString());
		}
		
		if(reaseguradorCorredorDTO.getTieneConstanciaResidenciaFiscal() != null){
			reaseguradorCorredorForm.setTieneConstanciaResidenciaFiscal(
					reaseguradorCorredorDTO.getTieneConstanciaResidenciaFiscal().toString());			
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(reaseguradorCorredorDTO.getFechaInicioVigenciaConstanciaResidenciaFiscal() != null){
			String fecha = sdf.format(reaseguradorCorredorDTO.getFechaInicioVigenciaConstanciaResidenciaFiscal());
			reaseguradorCorredorForm.setFechaInicioVigenciaConstanciaResidenciaFiscal(fecha);
		}
		
		if(reaseguradorCorredorDTO.getFechaFinVigenciaConstanciaResidenciaFiscal() != null){
			String fecha = sdf.format(reaseguradorCorredorDTO.getFechaFinVigenciaConstanciaResidenciaFiscal());
			reaseguradorCorredorForm.setFechaFinVigenciaConstanciaResidenciaFiscal(fecha);
		}				
		if (reaseguradorCorredorDTO.getCalificacionCNSF() != null) {
			reaseguradorCorredorForm.setCalificacionCNSF(reaseguradorCorredorDTO.getCalificacionCNSF());
		}
		if (reaseguradorCorredorDTO.getAgenciaCalCNSF() != null) {
			reaseguradorCorredorForm.setAgenciaCalificacion(reaseguradorCorredorDTO.getAgenciaCalCNSF()+"");
		}
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ReaseguradorCorredorForm reaseguradorCorredorForm = (ReaseguradorCorredorForm) form;
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		String id = request.getParameter("id");
		reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(BigDecimal.valueOf(Double.valueOf(id).doubleValue()));		
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
		try {
			this.poblarDTO(reaseguradorCorredorForm, reaseguradorCorredorDTO);
			reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			poblarForm(reaseguradorCorredorForm, reaseguradorCorredorDTO);
			this.setListaImpuestosResidenciaFiscal(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ParseException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
 	 		reaseguradorCorredorForm.setMensaje("Formato de fecha inv�lido."); 	 		
		}				
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			this.setListaImpuestosResidenciaFiscal(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void setListaImpuestosResidenciaFiscal(HttpServletRequest request) throws ExcepcionDeAccesoADatos, SystemException{
		List<ImpuestoResidenciaFiscalDTO> listaImpuestosResidenciaFiscal = ImpuestoResidenciaFiscalDN.getInstancia().listarTodos();
		List<ImpuestoResidenciaFiscalForm> impuestosResidenciaFiscal = new ArrayList<ImpuestoResidenciaFiscalForm>();					
		for(ImpuestoResidenciaFiscalDTO impuestoDTO : listaImpuestosResidenciaFiscal){
			ImpuestoResidenciaFiscalForm impuestoForm = new ImpuestoResidenciaFiscalForm();
			impuestoForm.setIdTcImpuestoResidenciaFiscal(impuestoDTO.getIdTcImpuestoResidenciaFiscal().toString());
			impuestoForm.setDescripcion(impuestoDTO.getDescripcion());				
			impuestoForm.setPorcentaje(impuestoDTO.getPorcentaje().toString());						
			impuestosResidenciaFiscal.add(impuestoForm);
		}		
		request.getSession().setAttribute("impuestosResidenciaFiscal", impuestosResidenciaFiscal);
	}

}
