package mx.com.afirme.midas.catalogos.tiponegocio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;


public class TipoNegocioDN {
	private static final TipoNegocioDN INSTANCIA = new TipoNegocioDN();

	public static TipoNegocioDN getInstancia() {
		return TipoNegocioDN.INSTANCIA;
	}
	
	public List<TipoNegocioDTO> buscarTodos() throws SystemException {
		return new TipoNegocioSN().buscarTodos();
	}
	
	public List<TipoNegocioDTO> buscarPorPropiedad(String propiedad, Object valor) throws SystemException {
		return new TipoNegocioSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public TipoNegocioDTO buscarPorIdTcTipoNegocio(BigDecimal idTcTipoNegocio) throws SystemException {
		return new TipoNegocioSN().buscarPorIdTcTipoNegocio(idTcTipoNegocio);
	}
	
	public List<TipoNegocioDTO> traerTodosPorIdTcTipoNegocio(BigDecimal idTcTipoNegocio) throws SystemException{
		final BigDecimal polizaFacultativaIdTcTipoNegocio = new BigDecimal(4);
		final BigDecimal polizaFacultativaControlIdTcTipoNegocio = new BigDecimal(3);
		final BigDecimal polizaNegocioEspecialIdTcTipoNegocio = new BigDecimal(2);
		final BigDecimal polizaNormalIdTcTipoNegocio = new BigDecimal(5);
		List<TipoNegocioDTO> tipoNegociosDTO = new ArrayList<TipoNegocioDTO>();
		TipoNegocioDN tipoNegocioDN = TipoNegocioDN.getInstancia();
		
		//Solo va a traer poliza facultativa y de negocio especial
		if (idTcTipoNegocio.equals(polizaFacultativaIdTcTipoNegocio) ||
				idTcTipoNegocio.equals(polizaFacultativaControlIdTcTipoNegocio)) {
			TipoNegocioDTO tipoNegocioDTOFacultativa = tipoNegocioDN.buscarPorIdTcTipoNegocio(
					idTcTipoNegocio);	
			TipoNegocioDTO tipoNegocioDTOEspecial = tipoNegocioDN.buscarPorIdTcTipoNegocio(
					polizaNegocioEspecialIdTcTipoNegocio);	
			tipoNegociosDTO.add(tipoNegocioDTOFacultativa);
			tipoNegociosDTO.add(tipoNegocioDTOEspecial);
		}
		else//Solo va a traer poliza normal y de negocio especial 
			if(idTcTipoNegocio.equals(polizaNormalIdTcTipoNegocio)) {
			TipoNegocioDTO tipoNegocioDTO = tipoNegocioDN.buscarPorIdTcTipoNegocio(
					idTcTipoNegocio);	
			TipoNegocioDTO tipoNegocioDTOEspecial = tipoNegocioDN.buscarPorIdTcTipoNegocio(
					polizaNegocioEspecialIdTcTipoNegocio);	
			tipoNegociosDTO.add(tipoNegocioDTO);
			tipoNegociosDTO.add(tipoNegocioDTOEspecial);			
		}else{//Solo va a traer poliza de negocio especial
			TipoNegocioDTO tipoNegocioDTOEspecial = tipoNegocioDN.buscarPorIdTcTipoNegocio(
					polizaNegocioEspecialIdTcTipoNegocio);				
			tipoNegociosDTO.add(tipoNegocioDTOEspecial);
		}
		
		return tipoNegociosDTO;
	}
}
