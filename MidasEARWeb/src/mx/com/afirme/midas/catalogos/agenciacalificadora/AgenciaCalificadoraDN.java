package mx.com.afirme.midas.catalogos.agenciacalificadora;

import java.util.List;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseaguradorCorredorSN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AgenciaCalificadoraDN {
	private static final AgenciaCalificadoraDN INSTANCIA = new AgenciaCalificadoraDN();
	
	public static AgenciaCalificadoraDN getInstancia() {
		return AgenciaCalificadoraDN.INSTANCIA;
	}
	
	public List<AgenciaCalificadoraDTO> listarTodos() throws SystemException,
		ExcepcionDeAccesoADatos {
	AgenciaCalificadoraSN agenciaCalificadoraSN;
	agenciaCalificadoraSN = new AgenciaCalificadoraSN();
	return agenciaCalificadoraSN.listarTodos();
	}
	
	public AgenciaCalificadoraDTO obtenerAgenciaPorId(
			AgenciaCalificadoraDTO agenciaCalDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		AgenciaCalificadoraSN agenciaCalSN;
		agenciaCalSN = new AgenciaCalificadoraSN();
		return agenciaCalSN
				.obtenerAgenciaPorId(agenciaCalDTO);
	}
}
