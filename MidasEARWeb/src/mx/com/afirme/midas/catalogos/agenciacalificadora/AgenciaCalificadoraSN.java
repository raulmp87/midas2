package mx.com.afirme.midas.catalogos.agenciacalificadora;

import java.util.List;
import java.util.logging.Level;


import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AgenciaCalificadoraSN {

	private AgenciaCalificadoraFacadeRemote beanRemoto;

	public AgenciaCalificadoraSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(AgenciaCalificadoraFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<AgenciaCalificadoraDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		List<AgenciaCalificadoraDTO> reaseguradorCorredorList;
		try {
			reaseguradorCorredorList = beanRemoto
			.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return reaseguradorCorredorList;
	}
	
	public AgenciaCalificadoraDTO obtenerAgenciaPorId(AgenciaCalificadoraDTO agenciaCalDTO) throws SystemException, ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(agenciaCalDTO.getIdagencia());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
