package mx.com.afirme.midas.catalogos.tipoequipocontratista;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoEquipoContratistaSN {

	/**
	 * @fecha 06/08/2009
	 */
	
	private TipoEquipoContratistaFacadeRemote beanRemoto;
	
	//constructor
	public TipoEquipoContratistaSN() 
			throws SystemException{
		
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(TipoEquipoContratistaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<TipoEquipoContratistaDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos{
	
		return beanRemoto.findAll();
	}

	public void agregar(TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		beanRemoto.save(tipoEquipoContratistaDTO);
	}

	public TipoEquipoContratistaDTO getTipoEqContrPorId(
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos {
		
		return beanRemoto.findById(tipoEquipoContratistaDTO.getIdTipoEquipoContratista());
	}

	public void borrar(TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		beanRemoto.delete(tipoEquipoContratistaDTO);
	}

	public void modificar(TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		beanRemoto.update(tipoEquipoContratistaDTO);
	}

	public List<TipoEquipoContratistaDTO> listarFiltrado(TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		return beanRemoto.listarFiltrado(tipoEquipoContratistaDTO);
	}

}
