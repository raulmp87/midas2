package mx.com.afirme.midas.catalogos.tipoequipocontratista;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoEquipoContratistaForm extends MidasBaseForm {
	
	/**
	 * fecha 06/08/2009
	 */
	
	private static final long serialVersionUID = -4303110045753803352L;

	private String idTipoEquipoContratista;
	private String codigoTipoEquipoContratista;
	private String descripcionTipoEqContr;

	
	/**
	 * @param idTipoEquipoContratista
	 *            the IdTipoRecipientePresion to set
	 */
	public void setIdTipoEquipoContratista(String idTipoEquipoContratista) {
		this.idTipoEquipoContratista = idTipoEquipoContratista;
	}
	/**
	 * @return the idTipoEquipoContratista
	 */
	public String getIdTipoEquipoContratista() {
		return idTipoEquipoContratista;
	}
	
	
	/**
	 * @param codigoTipoEquipoContratista
	 *            the CodigoTipoEquipoContratista to set
	 */
	public void setCodigoTipoEquipoContratista(
			String codigoTipoEquipoContratista) {
		this.codigoTipoEquipoContratista = codigoTipoEquipoContratista;
	}
	/**
	 * @return the codigoTipoEquipoContratista
	 */
	public String getCodigoTipoEquipoContratista() {
		return codigoTipoEquipoContratista;
	}
	
	
	/**
	 * @param descripcionTipoEqContr
	 *            the DescripcionTipoEqContr to set
	 */
	public void setDescripcionTipoEqContr(String descripcionTipoEqContr) {
		this.descripcionTipoEqContr = descripcionTipoEqContr;
	}
	/**
	 * @return the descripcionTipoEqContr
	 */
	public String getDescripcionTipoEqContr() {
		return descripcionTipoEqContr;
	}

}
