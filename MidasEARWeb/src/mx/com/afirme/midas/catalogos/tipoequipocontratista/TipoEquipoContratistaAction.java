package mx.com.afirme.midas.catalogos.tipoequipocontratista;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoEquipoContratistaAction extends MidasMappingDispatchAction {
	
	/**
	 *@fecha 06/08/2009
	 */

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		try{
			listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		TipoEquipoContratistaDN tipoEquipoContratistaDN      = TipoEquipoContratistaDN.getInstancia();
		List<TipoEquipoContratistaDTO> tipoEquipoContratista = tipoEquipoContratistaDN.listarTodos();
		request.setAttribute("tipoEquipoContratista", tipoEquipoContratista);
	}
	
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest requiest, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEquipoContratistaForm tipoEquipoContratistaForm = (TipoEquipoContratistaForm) form;
		TipoEquipoContratistaDTO  tipoEquipoContratistaDTO  = new TipoEquipoContratistaDTO();
		TipoEquipoContratistaDN   tipoEquipoContratistaDN   = TipoEquipoContratistaDN.getInstancia();
		
		try{
			this.poblarDTO(tipoEquipoContratistaForm, tipoEquipoContratistaDTO);
			request.setAttribute("tipoEquipoContratista", tipoEquipoContratistaDN.listarFiltrado(tipoEquipoContratistaDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEquipoContratistaForm tipoEquipoContratistaForm = (TipoEquipoContratistaForm) form;
		TipoEquipoContratistaDTO  tipoEquipoContratistaDTO  = new TipoEquipoContratistaDTO();
		TipoEquipoContratistaDN   tipoEquipoContratistaDN   = TipoEquipoContratistaDN.getInstancia();
		
		try{
			this.poblarDTO(tipoEquipoContratistaForm, tipoEquipoContratistaDTO);
			tipoEquipoContratistaDN.agregar(tipoEquipoContratistaDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
	
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEquipoContratistaForm tipoEquipoContratistaForm = (TipoEquipoContratistaForm) form;
		TipoEquipoContratistaDTO  tipoEquipoContratistaDTO  = new TipoEquipoContratistaDTO();
		TipoEquipoContratistaDN	  tipoEquipoContratistaDN   = TipoEquipoContratistaDN.getInstancia();
		
		try{
			this.poblarDTO(tipoEquipoContratistaForm, tipoEquipoContratistaDTO);
			tipoEquipoContratistaDTO = tipoEquipoContratistaDN.getTipoEqContrPorId(tipoEquipoContratistaDTO);
			tipoEquipoContratistaDN.borrar(tipoEquipoContratistaDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEquipoContratistaForm tipoEquipoContratistaForm = (TipoEquipoContratistaForm) form;
		TipoEquipoContratistaDTO  tipoEquipoContratistaDTO  = new TipoEquipoContratistaDTO();
		TipoEquipoContratistaDN	  tipoEquipoContratistaDN   = TipoEquipoContratistaDN.getInstancia();
		
		try{
			this.poblarDTO(tipoEquipoContratistaForm, tipoEquipoContratistaDTO);
			tipoEquipoContratistaDTO = tipoEquipoContratistaDN.getTipoEqContrPorId(tipoEquipoContratistaDTO);
			this.poblarDTO(tipoEquipoContratistaForm, tipoEquipoContratistaDTO);
			tipoEquipoContratistaDN.modificar(tipoEquipoContratistaDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEquipoContratistaForm tipoEquipoContratistaForm = (TipoEquipoContratistaForm) form;
		TipoEquipoContratistaDTO  tipoEquipoContratistaDTO  = new TipoEquipoContratistaDTO();
		TipoEquipoContratistaDN	  tipoEquipoContratistaDN   = TipoEquipoContratistaDN.getInstancia();
		
		try{
			String id = request.getParameter("id");
			tipoEquipoContratistaDTO.setIdTipoEquipoContratista(UtileriasWeb.regresaBigDecimal(id));
			tipoEquipoContratistaDTO = tipoEquipoContratistaDN.getTipoEqContrPorId(tipoEquipoContratistaDTO);
			this.poblarForm(tipoEquipoContratistaForm, tipoEquipoContratistaDTO);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	private void poblarForm(TipoEquipoContratistaForm tipoEquipoContratistaForm,
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO) throws SystemException{
	
		tipoEquipoContratistaForm.setIdTipoEquipoContratista(tipoEquipoContratistaDTO.getIdTipoEquipoContratista().toBigInteger().toString());
		tipoEquipoContratistaForm.setCodigoTipoEquipoContratista(tipoEquipoContratistaDTO.getCodigoTipoEquipoContratista().toBigInteger().toString());
		tipoEquipoContratistaForm.setDescripcionTipoEqContr(tipoEquipoContratistaDTO.getDescripcionTipoEqContr());
	}

	private void poblarDTO(TipoEquipoContratistaForm tipoEquipoContratistaForm,
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO) throws SystemException {
		
		if(!UtileriasWeb.esCadenaVacia(tipoEquipoContratistaForm.getIdTipoEquipoContratista()))
			tipoEquipoContratistaDTO.setIdTipoEquipoContratista(UtileriasWeb.regresaBigDecimal(tipoEquipoContratistaForm.getIdTipoEquipoContratista()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoEquipoContratistaForm.getCodigoTipoEquipoContratista()))
			tipoEquipoContratistaDTO.setCodigoTipoEquipoContratista(UtileriasWeb.regresaBigDecimal(tipoEquipoContratistaForm.getCodigoTipoEquipoContratista()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoEquipoContratistaForm.getDescripcionTipoEqContr()))
			tipoEquipoContratistaDTO.setDescripcionTipoEqContr(tipoEquipoContratistaForm.getDescripcionTipoEqContr());
	}
}
