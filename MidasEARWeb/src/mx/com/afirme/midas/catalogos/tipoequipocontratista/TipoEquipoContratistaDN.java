package mx.com.afirme.midas.catalogos.tipoequipocontratista;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoEquipoContratistaDN {

	/**
	 *@fecha 06/08/2009
	 */
	
	private static final TipoEquipoContratistaDN INSTANCIA = new TipoEquipoContratistaDN();
	private TipoEquipoContratistaSN tipoEquipoContratistaSN;
	
	public static TipoEquipoContratistaDN getInstancia() {
		
		return TipoEquipoContratistaDN.INSTANCIA;
	}

	public List<TipoEquipoContratistaDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		tipoEquipoContratistaSN = new TipoEquipoContratistaSN();
		return tipoEquipoContratistaSN.listarTodos();
	}

	public void agregar(TipoEquipoContratistaDTO tipoEquipoContratistaDTO)
			throws SystemException, ExcepcionDeAccesoADatos{
		
		tipoEquipoContratistaSN = new TipoEquipoContratistaSN();
		tipoEquipoContratistaSN.agregar(tipoEquipoContratistaDTO);
	}

	public TipoEquipoContratistaDTO getTipoEqContrPorId(TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		tipoEquipoContratistaSN = new TipoEquipoContratistaSN();
		return tipoEquipoContratistaSN.getTipoEqContrPorId(tipoEquipoContratistaDTO);
	}

	public void borrar(TipoEquipoContratistaDTO tipoEquipoContratistaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
			
		tipoEquipoContratistaSN = new TipoEquipoContratistaSN();
		tipoEquipoContratistaSN.borrar(tipoEquipoContratistaDTO);
	}

	public void modificar(TipoEquipoContratistaDTO tipoEquipoContratistaDTO)
			throws SystemException, ExcepcionDeAccesoADatos{
		
		tipoEquipoContratistaSN = new TipoEquipoContratistaSN();
		tipoEquipoContratistaSN.modificar(tipoEquipoContratistaDTO);
	}

	public List<TipoEquipoContratistaDTO> listarFiltrado(TipoEquipoContratistaDTO tipoEquipoContratistaDTO)
			throws SystemException, ExcepcionDeAccesoADatos{
		
		tipoEquipoContratistaSN = new TipoEquipoContratistaSN();
		return tipoEquipoContratistaSN.listarFiltrado(tipoEquipoContratistaDTO);
	}

}
