package mx.com.afirme.midas.catalogos.productoscnsf;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDN;
import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDTO;
import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfForm;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Gustavo Rodriguez
 * @since  06/SEPTIEMBRE/2017
 *
 */
public class ProductoCnsfAction extends MidasMappingDispatchAction{

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		/*try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}*/
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		ProductoCnsfDN ProductoCnsDN = ProductoCnsfDN.getInstancia();
		List<ProductoCnsfDTO> productos = ProductoCnsDN.listarTodos();
		request.setAttribute("productos", productos);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoCnsfForm ProductoCnsfForm = (ProductoCnsfForm) form;
		ProductoCnsfDTO ProductoCnsfDTO = new ProductoCnsfDTO();
		ProductoCnsfDN ProductoCnsDN = ProductoCnsfDN.getInstancia();
		try {
			poblarDTO(ProductoCnsfForm, ProductoCnsfDTO);
			request.setAttribute("productos", ProductoCnsDN.listarFiltrado(ProductoCnsfDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(ProductoCnsfForm ProductoCnsfForm, ProductoCnsfDTO ProductoCnsfDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(ProductoCnsfForm.getId()))
			ProductoCnsfDTO.setId(UtileriasWeb.regresaBigDecimal(ProductoCnsfForm.getId()));
		if (!UtileriasWeb.esCadenaVacia(ProductoCnsfForm.getDescripcionProd()))
			ProductoCnsfDTO.setDescripcionProd(ProductoCnsfForm.getDescripcionProd());
		if (!UtileriasWeb.esCadenaVacia(ProductoCnsfForm.getClaveProdServ()))
			ProductoCnsfDTO.setClaveProdServ(ProductoCnsfForm.getClaveProdServ());
		if (!UtileriasWeb.esCadenaVacia(ProductoCnsfForm.getComplementoProd()))
			ProductoCnsfDTO.setComplementoProd(ProductoCnsfForm.getComplementoProd());
		if (!UtileriasWeb.esCadenaVacia(ProductoCnsfForm.getIvaTrasladado()))
			ProductoCnsfDTO.setIvaTrasladado(ProductoCnsfForm.getIvaTrasladado());
		if (!UtileriasWeb.esCadenaVacia(ProductoCnsfForm.getIepsTrasladado()))
			ProductoCnsfDTO.setIepsTrasladado(ProductoCnsfForm.getIepsTrasladado());
		
	}
	
	private void poblarForm(ProductoCnsfForm ProductoCnsfForm, ProductoCnsfDTO ProductoCnsfDTO) throws SystemException {
		ProductoCnsfForm.setId(ProductoCnsfDTO.getId().toBigInteger().toString());
		ProductoCnsfForm.setClaveProdServ(ProductoCnsfDTO.getClaveProdServ());
		ProductoCnsfForm.setDescripcionProd(ProductoCnsfDTO.getDescripcionProd());
		ProductoCnsfForm.setIvaTrasladado(ProductoCnsfDTO.getIvaTrasladado());
		ProductoCnsfForm.setIepsTrasladado(ProductoCnsfDTO.getIepsTrasladado());
		ProductoCnsfForm.setComplementoProd(ProductoCnsfDTO.getComplementoProd());
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoCnsfForm ProductoCnsfForm = (ProductoCnsfForm) form;
		ProductoCnsfDTO ProductoCnsfDTO = new ProductoCnsfDTO();
		ProductoCnsfDN ProductoCnsDN = ProductoCnsfDN.getInstancia();
		try {
			poblarDTO(ProductoCnsfForm, ProductoCnsfDTO);
			ProductoCnsDN.agregar(ProductoCnsfDTO);
			listarTodos(request);
			limpiarForm(ProductoCnsfForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}
	
	private void limpiarForm(ProductoCnsfForm form){
		form.setClaveProdServ("");
		form.setDescripcionProd("");
		form.setComplementoProd("");
		form.setIepsTrasladado("");
		form.setIvaTrasladado("");
		form.setId("");
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoCnsfForm ProductoCnsfForm = (ProductoCnsfForm) form;
		ProductoCnsfDN ProductoCnsDN = ProductoCnsfDN.getInstancia();
		ProductoCnsfDTO ProductoCnsfDTO = new ProductoCnsfDTO();
		try {
			poblarDTO(ProductoCnsfForm, ProductoCnsfDTO);
			ProductoCnsfDTO = ProductoCnsDN.getProductoPorId(ProductoCnsfDTO);
			poblarDTO(ProductoCnsfForm, ProductoCnsfDTO);
			ProductoCnsDN.modificar(ProductoCnsfDTO);
			listarTodos(request);
			limpiarForm(ProductoCnsfForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoCnsfForm ProductoCnsfForm = (ProductoCnsfForm) form;
		ProductoCnsfDTO ProductoCnsfDTO = new ProductoCnsfDTO();
		ProductoCnsfDN ProductoCnsDN = ProductoCnsfDN.getInstancia();
		try {
			ProductoCnsfDTO.setId(UtileriasWeb.regresaBigDecimal(ProductoCnsfForm.getId()));
			ProductoCnsfDTO = ProductoCnsDN.getProductoPorId(ProductoCnsfDTO);
			ProductoCnsDN.borrar(ProductoCnsfDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ProductoCnsfDTO ProductoCnsfDTO = new ProductoCnsfDTO();
		ProductoCnsfForm ProductoCnsfForm = (ProductoCnsfForm) form;
		ProductoCnsfDN ProductoCnsDN = ProductoCnsfDN.getInstancia();
		try {
			String id = request.getParameter("id");
			ProductoCnsfDTO.setId(UtileriasWeb.regresaBigDecimal(id));
			ProductoCnsfDTO = ProductoCnsDN.getProductoPorId(ProductoCnsfDTO);
			poblarForm(ProductoCnsfForm, ProductoCnsfDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
}
