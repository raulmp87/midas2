package mx.com.afirme.midas.catalogos.productoscnsf;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * 
 * @author Gustavo Rodriguez
 *
 */
public class ProductoCnsfForm extends MidasBaseForm {
	private static final long serialVersionUID = -4426430534960591954L;
	
	private String id;
	private String claveProdServ;
	private String descripcionProd;
	private String ivaTrasladado;
	private String iepsTrasladado;
	private String complementoProd;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getClaveProdServ() {
		return claveProdServ;
	}
	public void setClaveProdServ(String claveProdServ) {
		this.claveProdServ = claveProdServ;
	}
	public String getDescripcionProd() {
		return descripcionProd;
	}
	public void setDescripcionProd(String descripcion) {
		this.descripcionProd = descripcion;
	}
	public String getIvaTrasladado() {
		return ivaTrasladado;
	}
	public void setIvaTrasladado(String ivaTrasladado) {
		this.ivaTrasladado = ivaTrasladado;
	}
	public String getIepsTrasladado() {
		return iepsTrasladado;
	}
	public void setIepsTrasladado(String iepsTrasladado) {
		this.iepsTrasladado = iepsTrasladado;
	}
	public String getComplementoProd() {
		return complementoProd;
	}
	public void setComplementoProd(String complementoProd) {
		this.complementoProd = complementoProd;
	}

}
