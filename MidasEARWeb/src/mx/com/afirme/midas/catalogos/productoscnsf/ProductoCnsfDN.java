package mx.com.afirme.midas.catalogos.productoscnsf;

import java.util.List;

import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDN;
import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDTO;
import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ProductoCnsfDN {

	public static final ProductoCnsfDN INSTANCIA = new ProductoCnsfDN();

	public static ProductoCnsfDN getInstancia (){
		return ProductoCnsfDN.INSTANCIA;
	}
	
	public String agregar(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ProductoCnsfSN ProductoCnsfSN = new ProductoCnsfSN();
		return ProductoCnsfSN.agregar(ProductoCnsfDTO);
	}
	
	public String borrar (ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ProductoCnsfSN ProductoCnsfSN = new ProductoCnsfSN();
		return ProductoCnsfSN.borrar(ProductoCnsfDTO);
	}
	
	public String modificar (ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ProductoCnsfSN ProductoCnsfSN = new ProductoCnsfSN();
		return ProductoCnsfSN.modificar(ProductoCnsfDTO);
	}
	
	public ProductoCnsfDTO getProductoPorId(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ProductoCnsfSN ProductoCnsfSN = new ProductoCnsfSN();
		return ProductoCnsfSN.getProductoPorId(ProductoCnsfDTO);
	}
	
	public List<ProductoCnsfDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		ProductoCnsfSN ProductoCnsfSN = new ProductoCnsfSN();
		return ProductoCnsfSN.listarTodos();
	}
	
	public List<ProductoCnsfDTO> listarFiltrado(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ProductoCnsfSN ProductoCnsfSN = new ProductoCnsfSN();
		return ProductoCnsfSN.listarFiltrado(ProductoCnsfDTO);
	}
	
	public List<ProductoCnsfDTO> findByProperty(String propertyName, final Object value) throws SystemException{
		return new ProductoCnsfSN().findByProperty(propertyName, value);
	}
}
