package mx.com.afirme.midas.catalogos.productoscnsf;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDTO;
import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ProductoCnsfSN {

	private ProductoCnsfFacadeRemote beanRemoto;

	public ProductoCnsfSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ProductoCnsfFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String agregar(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(ProductoCnsfDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String borrar(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(ProductoCnsfDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String modificar(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(ProductoCnsfDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public List<ProductoCnsfDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public ProductoCnsfDTO getProductoPorId(ProductoCnsfDTO ProductoCnsfDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(ProductoCnsfDTO.getId());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ProductoCnsfDTO> listarFiltrado(ProductoCnsfDTO ProductoCnsfDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(ProductoCnsfDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ProductoCnsfDTO> findByProperty(String propertyName, final Object value){
		return beanRemoto.findByProperty(propertyName, value);
	}
	
}
