package mx.com.afirme.midas.catalogos.moneda;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MonedaDN {
	private static final MonedaDN INSTANCIA = new MonedaDN();

	public static MonedaDN getInstancia() {
		return MonedaDN.INSTANCIA;
	}

	public List<MonedaDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		MonedaSN monedaSN = new MonedaSN();
		return monedaSN.listarTodos();
	}

	public void agregar(MonedaDTO monedaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		MonedaSN monedaSN = new MonedaSN();
		monedaSN.agregar(monedaDTO);
	}

	public void modificar(MonedaDTO monedaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		MonedaSN monedaSN = new MonedaSN();
		monedaSN.modificar(monedaDTO);
	}

	public MonedaDTO getPorId(Short id)
			throws SystemException, ExcepcionDeAccesoADatos {
		MonedaSN monedaSN = new MonedaSN();
		return monedaSN.getPorId(id);
	}

	public void borrar(MonedaDTO monedaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		MonedaSN monedaSN = new MonedaSN();
		monedaSN.borrar(monedaDTO);
	}
}
