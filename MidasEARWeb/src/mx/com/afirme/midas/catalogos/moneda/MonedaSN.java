/**
 * 
 */
package mx.com.afirme.midas.catalogos.moneda;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class MonedaSN {
	private MonedaFacadeRemote beanRemoto;

	public MonedaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en MonedaSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(MonedaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<MonedaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<MonedaDTO> monedas = beanRemoto.findAll();
		return monedas;

	}	

	public void agregar(MonedaDTO monedaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(monedaDTO);
	}

	public void modificar(MonedaDTO monedaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(monedaDTO);
	}

	public MonedaDTO getPorId(Short id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(MonedaDTO monedaDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(monedaDTO);
	}

	public List<MonedaDTO> listarMonedasPorAsociar(BigDecimal idToProducto) {
		return beanRemoto.listarMonedasPorAsociar(idToProducto);
	}
	
	public List<MonedaDTO> listarMonedasAsociadasProducto(BigDecimal idToProducto) {
		return beanRemoto.listarMonedasAsociadas(idToProducto);
	}
	
	public List<MonedaDTO> listarMonedasAsociadasTipoPoliza(BigDecimal idToTipoPoliza) {
		return beanRemoto.listarMonedasAsociadasTipoPoliza(idToTipoPoliza);
	}
	
	public List<MonedaDTO> listarMonedasNoAsociadasTipoPoliza(BigDecimal idToTipoPoliza,BigDecimal idToProducto){
		return beanRemoto.findNotRelatedTipoPoliza(idToTipoPoliza,idToProducto);
	}
}
