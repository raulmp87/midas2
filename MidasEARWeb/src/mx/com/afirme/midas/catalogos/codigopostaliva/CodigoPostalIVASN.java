package mx.com.afirme.midas.catalogos.codigopostaliva;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;


import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class CodigoPostalIVASN {
    
    private CodigoPostalIVAFacadeRemote beanRemoto;
    
    public CodigoPostalIVASN() throws SystemException {
	LogDeMidasWeb.log("Entrando en CodigoPostalSN - Constructor",
			Level.INFO, null);
	ServiceLocator serviceLocator = ServiceLocator.getInstance();
	beanRemoto = serviceLocator.getEJB(CodigoPostalIVAFacadeRemote.class);
	LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
    }
    
    
    
    public CodigoPostalIVADTO getPorId(CodigoPostalIVAId id){
	return beanRemoto.findById(id);
    }
    
    public List<CodigoPostalIVADTO> listarTodos(){
	return beanRemoto.findAll();
	
    }
    
	public List<CodigoPostalIVADTO> listarPorCodigoPostal(
			BigDecimal codigoPostal) {
		return beanRemoto.findByCodigoPostal(codigoPostal);
	}

	public void agregar(CodigoPostalIVADTO codigoPostalIVADTO) {
		beanRemoto.save(codigoPostalIVADTO);
	}

	public void modificar(CodigoPostalIVADTO codigoPostalIVADTO) {
		beanRemoto.update(codigoPostalIVADTO);
	}
}
