package mx.com.afirme.midas.catalogos.codigopostaliva;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;


public class CodigoPostalIVADN {
    	private static final CodigoPostalIVADN INSTANCIA = new CodigoPostalIVADN();

	public static CodigoPostalIVADN getInstancia() {
		return CodigoPostalIVADN.INSTANCIA;
	}
	
	public CodigoPostalIVADTO getPorId(CodigoPostalIVAId id) throws SystemException{
	    return new CodigoPostalIVASN().getPorId(id);
	}
	
	public List<CodigoPostalIVADTO> listarTodos() throws SystemException{
	    return new CodigoPostalIVASN().listarTodos();
	}
	
	public Double getIVAPorIdCotizacion(BigDecimal idToCotizacion, String nombreUsuario) throws SystemException{
	
		 Double iva = null;
		 StringBuffer claveColonia;
		 CotizacionDTO cotizacionDTO= CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		    
		 if (cotizacionDTO != null) {
			ClienteDTO clienteDTO = null;
			
			try{
				if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
					clienteDTO = new ClienteDTO();
					clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
					clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, nombreUsuario);
				}else{
					clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), nombreUsuario);
				}
				
		    		
		    		if (clienteDTO != null) {
		    		    claveColonia= new StringBuffer();
		    		    CodigoPostalIVAId codigoPostalIVAId= new CodigoPostalIVAId();
		    		    codigoPostalIVAId.setCodigoPostal(new BigDecimal(clienteDTO.getCodigoPostal().trim()));
		    		    claveColonia.append(clienteDTO.getCodigoPostal().trim());
		    		    claveColonia.append('-');
		    		    //claveColonia.append(clienteDTO.getIdColonia().toString());
		    		    codigoPostalIVAId.setNombreColonia(claveColonia.toString());
		    		    CodigoPostalIVADTO  codigoPostalIVADTO= getPorId(codigoPostalIVAId);
		    		    if (codigoPostalIVADTO != null) {
		    		    	iva = codigoPostalIVADTO.getValorIVA();
		    		    }else{
		    			LogDeMidasWeb.log("No se pudo obtener el valor del IVA, para la colonia:"+claveColonia+" y codigo postal:"+clienteDTO.getCodigoPostal().trim()+"...", Level.WARNING,null);
		    		    }
		    		}else{
		    		    LogDeMidasWeb.log("No se pudo encontrar una instancia de Cliente para el contratante con id: "+idToCotizacion+" en la busqueda del IVA.", Level.WARNING, null);
		    		}
			
			} catch (Exception e) { 
				
				LogDeMidasWeb.log("Error en la ejecucion de CodigoPostalIVADN.getIVAPorIdCotizacion...", Level.WARNING, e);
			}
			
		}else{
		    LogDeMidasWeb.log("No se pudo encontrar una instancia de Cotizacion para el id: "+idToCotizacion+" en la busqueda del IVA.", Level.WARNING, null);
		}
		return iva;
	
	}

	public List<CodigoPostalIVADTO> listarPorCodigoPostal(BigDecimal regresaBigDecimal) throws SystemException {
		 return new CodigoPostalIVASN().listarPorCodigoPostal(regresaBigDecimal);
	}

	public void agregar(CodigoPostalIVADTO codigoPostalIVADTO) throws SystemException {
		new CodigoPostalIVASN().agregar(codigoPostalIVADTO);
	}

	public void modificar(CodigoPostalIVADTO codigoPostalIVADTO) throws SystemException {
		new CodigoPostalIVASN().modificar(codigoPostalIVADTO);
	}
}
