package mx.com.afirme.midas.catalogos.plenoreaseguro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PlenoReaseguroDN {
	private static final PlenoReaseguroDN INSTANCIA = new PlenoReaseguroDN();

	public static PlenoReaseguroDN getInstancia (){
		return PlenoReaseguroDN.INSTANCIA;
	}
	
	public void agregar(PlenoReaseguroDTO PlenoReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new PlenoReaseguroSN().agregar(PlenoReaseguroDTO);
	}
	
	public void borrar (PlenoReaseguroDTO PlenoReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new PlenoReaseguroSN().borrar(PlenoReaseguroDTO);
	}
	
	public void modificar (PlenoReaseguroDTO PlenoReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new PlenoReaseguroSN().modificar(PlenoReaseguroDTO);
	}
	
	public PlenoReaseguroDTO getPorId(BigDecimal idTcPlenoReaseguro) throws ExcepcionDeAccesoADatos, SystemException{
		return new PlenoReaseguroSN().getPorId(idTcPlenoReaseguro);
	}
	
	public List<PlenoReaseguroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new PlenoReaseguroSN().listarTodos();
	}
}
