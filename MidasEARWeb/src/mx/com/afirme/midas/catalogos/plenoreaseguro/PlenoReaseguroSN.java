package mx.com.afirme.midas.catalogos.plenoreaseguro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PlenoReaseguroSN {
	private PlenoReaseguroFacadeRemote beanRemoto;

	public PlenoReaseguroSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PlenoReaseguroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto PlenoReaseguro instanciado", Level.FINEST, null);
	}
	
	public void agregar(PlenoReaseguroDTO PlenoReaseguroDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(PlenoReaseguroDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(PlenoReaseguroDTO PlenoReaseguroDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(PlenoReaseguroDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(PlenoReaseguroDTO PlenoReaseguroDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(PlenoReaseguroDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<PlenoReaseguroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public PlenoReaseguroDTO getPorId(BigDecimal idTcPlenoReaseguro) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcPlenoReaseguro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}
