package mx.com.afirme.midas.catalogos.subtiporecipientepresion;

import java.util.List;

import mx.com.afirme.midas.catalogos.tiporecipientepresion.SubtipoRecipientePresionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoRecipientePresionDN {

	/**
	 * fecha: 04/Agosto/2009
	 */

	public static final SubTipoRecipientePresionDN INSTANCIA = new SubTipoRecipientePresionDN();
	private SubTipoRecipientePresionSN subTipoRecPresionSN;

	public static SubTipoRecipientePresionDN getInstancia(){
	
		return SubTipoRecipientePresionDN.INSTANCIA;
	}

	public List<SubtipoRecipientePresionDTO> listarTodos()
			throws SystemException, ExcepcionDeAccesoADatos{
		
		subTipoRecPresionSN = new SubTipoRecipientePresionSN();
		return subTipoRecPresionSN.listarTodos();
	}

	public List<SubtipoRecipientePresionDTO> listarFiltrado(SubtipoRecipientePresionDTO subTipoRecPresionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {

		subTipoRecPresionSN = new SubTipoRecipientePresionSN();
		return subTipoRecPresionSN.listarFiltrado(subTipoRecPresionDTO);
	}

	public void agregar(SubtipoRecipientePresionDTO subTipoRecPresionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		subTipoRecPresionSN = new SubTipoRecipientePresionSN();
		subTipoRecPresionSN.agregar(subTipoRecPresionDTO);
	}

	public SubtipoRecipientePresionDTO getSubTipoRecPresionPorId(SubtipoRecipientePresionDTO subTipoRecPresionDTO) 
			throws ExcepcionDeAccesoADatos, SystemException{
		
		subTipoRecPresionSN = new SubTipoRecipientePresionSN();
		return subTipoRecPresionSN.getSubTipoRecPresionPorId(subTipoRecPresionDTO);
	}

	public void borrar(SubtipoRecipientePresionDTO subTipoRecPresionDTO) 
			throws ExcepcionDeAccesoADatos, SystemException{
	
		subTipoRecPresionSN = new SubTipoRecipientePresionSN();
		subTipoRecPresionSN.borrar(subTipoRecPresionDTO);
	}

	public void modificar(SubtipoRecipientePresionDTO subTipoRecPresionDTO)
	 		throws ExcepcionDeAccesoADatos, SystemException{
		
		subTipoRecPresionSN = new SubTipoRecipientePresionSN();
		subTipoRecPresionSN.modificar(subTipoRecPresionDTO);
	}


}
