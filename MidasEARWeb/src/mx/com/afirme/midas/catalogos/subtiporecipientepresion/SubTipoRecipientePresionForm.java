package mx.com.afirme.midas.catalogos.subtiporecipientepresion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SubTipoRecipientePresionForm extends MidasBaseForm{

	/**
	 * fecha: 04/Agosto/2009
	 */
	private static final long serialVersionUID = 4527825178066055788L;
	
	private String idSubTipoRecipientePresion;
	private String tipoRecipientePresionDTO;
	private String descripcionSubTipoRecPresion;
	private String codigoSubTipoRecPresion;

	/**
	 * @param idSubTipoRecipientePresion
	 *            the idSubTipoRecipientePresion to set
	 */
	public void setIdSubTipoRecipientePresion(String idSubTipoRecipientePresion) {
		this.idSubTipoRecipientePresion = idSubTipoRecipientePresion;
	}
	/**
	 * @return the idSubTipoRecipientePresion
	 */
	public String getIdSubTipoRecipientePresion() {
		return idSubTipoRecipientePresion;
	}

	/**
	 * @param descripcionSubTipoRecPresion
	 *            the descripcionSubTipoRecPresion to set
	 */
	public void setDescripcionSubTipoRecPresion(
			String descripcionSubTipoRecPresion) {
		this.descripcionSubTipoRecPresion = descripcionSubTipoRecPresion;
	}
	/**
	 * @return the descripcionSubTipoRecPresion
	 */
	public String getDescripcionSubTipoRecPresion() {
		return descripcionSubTipoRecPresion;
	}
	
	/**
	 * @param tipoRecipientePresionDTO
	 *            the tipoRecipientePresionDTO to set
	 */
	public void setTipoRecipientePresionDTO(String tipoRecipientePresionDTO) {
		this.tipoRecipientePresionDTO = tipoRecipientePresionDTO;
	}
	/**
	 * @return the tipoRecipientePresionDTO
	 */
	public String getTipoRecipientePresionDTO() {
		return tipoRecipientePresionDTO;
	}
	
	/**
	 * @param codigoSubTipoRecPresion
	 *            the codigoSubTipoRecPresion to set
	 */
	public void setCodigoSubTipoRecPresion(String codigoSubTipoRecPresion) {
		this.codigoSubTipoRecPresion = codigoSubTipoRecPresion;
	}
	/**
	 * @return the codigoSubTipoRecPresion
	 */
	public String getCodigoSubTipoRecPresion() {
		return codigoSubTipoRecPresion;
	}
	
}
