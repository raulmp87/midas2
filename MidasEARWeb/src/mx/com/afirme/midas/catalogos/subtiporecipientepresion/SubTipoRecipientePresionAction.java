package mx.com.afirme.midas.catalogos.subtiporecipientepresion;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.tiporecipientepresion.SubtipoRecipientePresionDTO;
import mx.com.afirme.midas.catalogos.tiporecipientepresion.TipoRecipientePresionDN;
import mx.com.afirme.midas.catalogos.tiporecipientepresion.TipoRecipientePresionDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoRecipientePresionAction extends MidasMappingDispatchAction{

	/**
	 * fecha: 04/Agosto/2009
	 */
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
	    String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			listarTodos(request);
		}catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		SubTipoRecipientePresionDN subTipoRecPresionDN = SubTipoRecipientePresionDN.getInstancia();
		List<SubtipoRecipientePresionDTO> subTipoRecipientePresion = subTipoRecPresionDN.listarTodos();
		request.setAttribute("subTipoRecipientePresion", subTipoRecipientePresion);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubTipoRecipientePresionForm subTipoRecPresionForm = (SubTipoRecipientePresionForm)form;
		SubtipoRecipientePresionDTO  subTipoRecPresionDTO  = new SubtipoRecipientePresionDTO();
		SubTipoRecipientePresionDN 	 subTipoRecPresionDN   = SubTipoRecipientePresionDN.getInstancia();
		try{
			poblarDTO(subTipoRecPresionForm, subTipoRecPresionDTO);
			request.setAttribute("subTipoRecipientePresion", subTipoRecPresionDN.listarFiltrado(subTipoRecPresionDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoRecipientePresionDTO subTipoRecPresionDTO   = new SubtipoRecipientePresionDTO();
		SubTipoRecipientePresionForm subTipoRecPresionForm = (SubTipoRecipientePresionForm) form;
		SubTipoRecipientePresionDN subTipoRecPresionDN     = SubTipoRecipientePresionDN.getInstancia();
		try {
			String id = request.getParameter("id");
			subTipoRecPresionDTO.setIdSubtipoRecipientePresion(UtileriasWeb.regresaBigDecimal(id));
			subTipoRecPresionDTO = subTipoRecPresionDN.getSubTipoRecPresionPorId(subTipoRecPresionDTO);
			poblarForm(subTipoRecPresionForm, subTipoRecPresionDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);


	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubTipoRecipientePresionForm subTipoRecPresionForm = (SubTipoRecipientePresionForm)form;
		SubtipoRecipientePresionDTO subTipoRecPresionDTO   = new SubtipoRecipientePresionDTO();
		SubTipoRecipientePresionDN subTipoRecPresionDN     = SubTipoRecipientePresionDN.getInstancia();
		try{
			poblarDTO(subTipoRecPresionForm, subTipoRecPresionDTO);
			subTipoRecPresionDN.agregar(subTipoRecPresionDTO);
			listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubTipoRecipientePresionForm subTipoRecPresionForm = (SubTipoRecipientePresionForm)form;
		SubtipoRecipientePresionDTO subTipoRecPresionDTO   = new SubtipoRecipientePresionDTO();
		SubTipoRecipientePresionDN subTipoRecPresionDN     = SubTipoRecipientePresionDN.getInstancia();
		try{
			poblarDTO(subTipoRecPresionForm, subTipoRecPresionDTO);
			subTipoRecPresionDTO = subTipoRecPresionDN.getSubTipoRecPresionPorId(subTipoRecPresionDTO);
			subTipoRecPresionDN.borrar(subTipoRecPresionDTO);
			listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubTipoRecipientePresionForm subTipoRecPresionForm = (SubTipoRecipientePresionForm)form;
		SubtipoRecipientePresionDTO subTipoRecPresionDTO   = new SubtipoRecipientePresionDTO();
		SubTipoRecipientePresionDN subTipoRecPresionDN     = SubTipoRecipientePresionDN.getInstancia();
		try {
			poblarDTO(subTipoRecPresionForm, subTipoRecPresionDTO);
			subTipoRecPresionDTO = subTipoRecPresionDN.getSubTipoRecPresionPorId(subTipoRecPresionDTO);
			poblarDTO(subTipoRecPresionForm, subTipoRecPresionDTO);
			subTipoRecPresionDN.modificar(subTipoRecPresionDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void poblarForm(SubTipoRecipientePresionForm subTipoRecPresionForm,
			SubtipoRecipientePresionDTO subTipoRecPresionDTO)  throws SystemException {
		
		subTipoRecPresionForm.setIdSubTipoRecipientePresion(subTipoRecPresionDTO.getIdSubtipoRecipientePresion().toBigInteger().toString());
		subTipoRecPresionForm.setDescripcionSubTipoRecPresion(subTipoRecPresionDTO.getDescripcionSubtipoRecPresion());
		subTipoRecPresionForm.setCodigoSubTipoRecPresion(subTipoRecPresionDTO.getCodigoSubtipoRecipientePresion().toBigInteger().toString());
		
		TipoRecipientePresionDTO tipoRecPresion = subTipoRecPresionDTO.getTipoRecipientePresionDTO();
		subTipoRecPresionForm.setTipoRecipientePresionDTO(tipoRecPresion.getIdTipoRecipientePresion().toString());
		
	}
	
	private void poblarDTO(SubTipoRecipientePresionForm subTipoRecPresionForm,
			SubtipoRecipientePresionDTO subTipoRecPresionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		
		if(!UtileriasWeb.esCadenaVacia(subTipoRecPresionForm.getIdSubTipoRecipientePresion()))
			subTipoRecPresionDTO.setIdSubtipoRecipientePresion(UtileriasWeb.regresaBigDecimal(subTipoRecPresionForm.getIdSubTipoRecipientePresion()));
		
		if(!UtileriasWeb.esCadenaVacia(subTipoRecPresionForm.getDescripcionSubTipoRecPresion()))
			subTipoRecPresionDTO.setDescripcionSubtipoRecPresion(subTipoRecPresionForm.getDescripcionSubTipoRecPresion().trim().toUpperCase());
		
		if(!UtileriasWeb.esCadenaVacia(subTipoRecPresionForm.getCodigoSubTipoRecPresion()))
			subTipoRecPresionDTO.setCodigoSubtipoRecipientePresion(UtileriasWeb.regresaBigDecimal(subTipoRecPresionForm.getCodigoSubTipoRecPresion()));

	
		if(!UtileriasWeb.esCadenaVacia(subTipoRecPresionForm.getTipoRecipientePresionDTO())){
			//el tipo recipiente presion
			TipoRecipientePresionDN tipoRecPresionDN = TipoRecipientePresionDN.getInstancia();
			TipoRecipientePresionDTO tipoRecPresionDTO = new TipoRecipientePresionDTO();
			tipoRecPresionDTO.setIdTipoRecipientePresion(UtileriasWeb.regresaBigDecimal(subTipoRecPresionForm.getTipoRecipientePresionDTO()));
			tipoRecPresionDTO = tipoRecPresionDN.getTipoRecPresionPorId(tipoRecPresionDTO);
			subTipoRecPresionDTO.setTipoRecipientePresionDTO(tipoRecPresionDTO);
		}
	}
}
