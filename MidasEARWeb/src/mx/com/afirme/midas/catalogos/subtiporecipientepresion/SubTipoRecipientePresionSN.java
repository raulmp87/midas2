package mx.com.afirme.midas.catalogos.subtiporecipientepresion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.tiporecipientepresion.SubtipoRecipientePresionDTO;
import mx.com.afirme.midas.catalogos.tiporecipientepresion.SubtipoRecipientePresionFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoRecipientePresionSN {

	private SubtipoRecipientePresionFacadeRemote beanRemoto;

	public SubTipoRecipientePresionSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubtipoRecipientePresionFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(SubtipoRecipientePresionDTO subtipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(subtipoRecipientePresionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(SubtipoRecipientePresionDTO subtipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(subtipoRecipientePresionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(SubtipoRecipientePresionDTO subtipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(subtipoRecipientePresionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<SubtipoRecipientePresionDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubtipoRecipientePresionDTO getSubTipoRecPresionPorId(SubtipoRecipientePresionDTO subtipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(subtipoRecipientePresionDTO.getIdSubtipoRecipientePresion());
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SubtipoRecipientePresionDTO> listarFiltrado(SubtipoRecipientePresionDTO subtipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(subtipoRecipientePresionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}


}
