package mx.com.afirme.midas.catalogos.tipomuro;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoMuroForm extends MidasBaseForm {

	/**
	 * @author Christian Ceballos
	 * 
	 */
	private static final long serialVersionUID = -7841685890359827185L;

	private String idTipoMuro;

	private String codigoTipoMuro;

	private String descripcionTipoMuro;

	public String getIdTipoMuro() {
		return idTipoMuro;
	}

	public void setIdTipoMuro(String idTipoMuro) {
		this.idTipoMuro = idTipoMuro;
	}

	public String getCodigoTipoMuro() {
		return codigoTipoMuro;
	}

	public void setCodigoTipoMuro(String codigoTipoMuro) {
		this.codigoTipoMuro = codigoTipoMuro;
	}

	public String getDescripcionTipoMuro() {
		return descripcionTipoMuro;
	}

	public void setDescripcionTipoMuro(String descripcionTipoMuro) {
		this.descripcionTipoMuro = descripcionTipoMuro;
	}

}
