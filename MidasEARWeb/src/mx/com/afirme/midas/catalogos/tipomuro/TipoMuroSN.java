package mx.com.afirme.midas.catalogos.tipomuro;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoMuroSN {

	private TipoMuroFacadeRemote beanRemoto;

	public TipoMuroSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoMuroFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(tipoMuroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(tipoMuroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(tipoMuroDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<TipoMuroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoMuroDTO getTipoMuroPorId(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(tipoMuroDTO.getIdTipoMuro());
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoMuroDTO> listarFiltrado(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(tipoMuroDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}
