package mx.com.afirme.midas.catalogos.tipomuro;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoMuroAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoMuroDN tipoMuroDN = TipoMuroDN.getInstancia();
		List<TipoMuroDTO> muros = tipoMuroDN.listarTodos();
		request.setAttribute("muros", muros);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMuroForm tipoMuroForm = (TipoMuroForm) form;
		TipoMuroDTO tipoMuroDTO = new TipoMuroDTO();
		TipoMuroDN tipoMuroDN = TipoMuroDN.getInstancia();
		try {
			poblarDTO(tipoMuroForm, tipoMuroDTO);
			request.setAttribute("muros", tipoMuroDN.listarFiltrado(tipoMuroDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(TipoMuroForm tipoMuroForm,
			TipoMuroDTO tipoMuroDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(tipoMuroForm.getDescripcionTipoMuro()))
			tipoMuroDTO.setDescripcionTipoMuro(tipoMuroForm.getDescripcionTipoMuro().trim().toUpperCase());
		if (!UtileriasWeb.esCadenaVacia(tipoMuroForm.getCodigoTipoMuro()))
			tipoMuroDTO.setCodigoTipoMuro(UtileriasWeb.regresaBigDecimal(tipoMuroForm.getCodigoTipoMuro()));
		if (!UtileriasWeb.esCadenaVacia(tipoMuroForm.getIdTipoMuro()))
			tipoMuroDTO.setIdTipoMuro(UtileriasWeb.regresaBigDecimal(tipoMuroForm.getIdTipoMuro()));
		return null;
	}
	
	private String poblarForm(TipoMuroForm tipoMuroForm,
			TipoMuroDTO tipoMuroDTO) throws SystemException {
		tipoMuroForm.setDescripcionTipoMuro(tipoMuroDTO.getDescripcionTipoMuro());
		tipoMuroForm.setCodigoTipoMuro(tipoMuroDTO.getCodigoTipoMuro().toBigInteger().toString());
		tipoMuroForm.setIdTipoMuro(tipoMuroDTO.getIdTipoMuro().toBigInteger().toString());
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMuroForm tipoMuroForm = (TipoMuroForm) form;
		TipoMuroDTO tipoMuroDTO = new TipoMuroDTO();
		TipoMuroDN tipoMuroDN = TipoMuroDN.getInstancia();
		try {
			poblarDTO(tipoMuroForm, tipoMuroDTO);
			tipoMuroDN.agregar(tipoMuroDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMuroForm tipoMuroForm = (TipoMuroForm) form;
		TipoMuroDN tipoMuroDN = TipoMuroDN.getInstancia();
		TipoMuroDTO tipoMuroDTO = new TipoMuroDTO();
		try {
			poblarDTO(tipoMuroForm, tipoMuroDTO);
			tipoMuroDTO = tipoMuroDN.getTipoMuroPorId(tipoMuroDTO);
			poblarDTO(tipoMuroForm, tipoMuroDTO);
			tipoMuroDN.modificar(tipoMuroDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMuroForm tipoMuroForm = (TipoMuroForm) form;
		TipoMuroDTO tipoMuroDTO = new TipoMuroDTO();
		TipoMuroDN tipoMuroDN = TipoMuroDN.getInstancia();
		try {
			poblarDTO(tipoMuroForm, tipoMuroDTO);
			tipoMuroDTO = tipoMuroDN.getTipoMuroPorId(tipoMuroDTO);
			tipoMuroDN.borrar(tipoMuroDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMuroDTO tipoMuroDTO = new TipoMuroDTO();
		TipoMuroForm tipoMuroForm = (TipoMuroForm) form;
		TipoMuroDN tipoMuroDN = TipoMuroDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoMuroDTO.setIdTipoMuro(UtileriasWeb.regresaBigDecimal(id));
			tipoMuroDTO = tipoMuroDN.getTipoMuroPorId(tipoMuroDTO);
			poblarForm(tipoMuroForm, tipoMuroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
}
