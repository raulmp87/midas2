package mx.com.afirme.midas.catalogos.tipomuro;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoMuroDN {

	public static final TipoMuroDN INSTANCIA = new TipoMuroDN();

	public static TipoMuroDN getInstancia (){
		return TipoMuroDN.INSTANCIA;
	}
	
	public String agregar(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMuroSN tipoMuroSN = new TipoMuroSN();
		return tipoMuroSN.agregar(tipoMuroDTO);
	}
	
	public String borrar (TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMuroSN tipoMuroSN = new TipoMuroSN();
		return tipoMuroSN.borrar(tipoMuroDTO);
	}
	
	public String modificar (TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMuroSN tipoMuroSN = new TipoMuroSN();
		return tipoMuroSN.modificar(tipoMuroDTO);
	}
	
	public TipoMuroDTO getTipoMuroPorId(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMuroSN tipoMuroSN = new TipoMuroSN();
		return tipoMuroSN.getTipoMuroPorId(tipoMuroDTO);
	}
	
	public List<TipoMuroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoMuroSN tipoMuroSN = new TipoMuroSN();
		return tipoMuroSN.listarTodos();
	}
	
	public List<TipoMuroDTO> listarFiltrado(TipoMuroDTO tipoMuroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMuroSN tipoMuroSN = new TipoMuroSN();
		return tipoMuroSN.listarFiltrado(tipoMuroDTO);
	}
	
}
