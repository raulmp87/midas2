package mx.com.afirme.midas.catalogos.girotransporte;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class GiroTransporteSN {

	private GiroTransporteFacadeRemote beanRemoto;

	public GiroTransporteSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(GiroTransporteFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(giroTransporteDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.delete(giroTransporteDTO);
		return null;
	}
	
	public String modificar(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(giroTransporteDTO);
		}catch (EJBTransactionRolledbackException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public List<GiroTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		return beanRemoto.findAll();
	}
	
	public GiroTransporteDTO getGiroTransportePorId(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findById(giroTransporteDTO.getIdGiroTransporte());
	}
	
	public List<GiroTransporteDTO> listarFiltrado(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.listarFiltrado(giroTransporteDTO);
	}
	
}
