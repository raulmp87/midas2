package mx.com.afirme.midas.catalogos.girotransporte;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class GiroTransporteAction extends MidasMappingDispatchAction{
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) { e.printStackTrace();
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) { e.printStackTrace();
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request) throws SystemException, ExcepcionDeAccesoADatos {
		GiroTransporteDN giroTransporteDN = GiroTransporteDN.getInstancia();
		List<GiroTransporteDTO> giroTransporteLista = giroTransporteDN.listarTodos();
		request.setAttribute("giroTransporteLista", giroTransporteLista);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroTransporteForm giroTransporteForm = (GiroTransporteForm) form;
		GiroTransporteDTO giroTransporteDTO = new GiroTransporteDTO();
		GiroTransporteDN giroTransporteDN = GiroTransporteDN.getInstancia();
		try {
			poblarDTO(giroTransporteForm, giroTransporteDTO);
			request.setAttribute("giroTransporteLista", giroTransporteDN.listarFiltrado(giroTransporteDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(GiroTransporteForm giroTransporteForm,
			GiroTransporteDTO giroTransporteDTO) throws SystemException {
		
		if (!UtileriasWeb.esCadenaVacia(giroTransporteForm.getIdGiroTransporte()))
			giroTransporteDTO.setIdGiroTransporte(new BigDecimal(giroTransporteForm.getIdGiroTransporte()));
		
		if (!UtileriasWeb.esCadenaVacia(giroTransporteForm.getCodigoGiroTransporte()))
			giroTransporteDTO.setCodigoGiroTransporte(new BigDecimal(giroTransporteForm.getCodigoGiroTransporte()));
		
		if (!UtileriasWeb.esCadenaVacia(giroTransporteForm.getDescripcionGiroTransporte()))
			giroTransporteDTO.setDescripcionGiroTransporte(giroTransporteForm.getDescripcionGiroTransporte());
		
		if (!UtileriasWeb.esCadenaVacia(giroTransporteForm.getIdGrupoROT()))
			giroTransporteDTO.setIdGrupoROT(new BigDecimal(giroTransporteForm.getIdGrupoROT()));
	}
	
	private String poblarForm(GiroTransporteForm giroTransporteForm,
			GiroTransporteDTO giroTransporteDTO) throws SystemException {
		giroTransporteForm.setIdGiroTransporte(giroTransporteDTO.getIdGiroTransporte().toBigInteger().toString());
		giroTransporteForm.setDescripcionGiroTransporte(giroTransporteDTO.getDescripcionGiroTransporte());
		giroTransporteForm.setIdGrupoROT(giroTransporteDTO.getIdGrupoROT().toBigInteger().toString());
		giroTransporteForm.setCodigoGiroTransporte(giroTransporteDTO.getCodigoGiroTransporte().toBigInteger().toString());
		
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroTransporteForm giroTransporteForm = (GiroTransporteForm) form;
		GiroTransporteDTO giroTransporteDTO = new GiroTransporteDTO();
		GiroTransporteDN giroTransporteDN = GiroTransporteDN.getInstancia();
		try {
			poblarDTO(giroTransporteForm, giroTransporteDTO);
			giroTransporteDN.agregar(giroTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroTransporteForm giroTransporteForm = (GiroTransporteForm) form;		
		GiroTransporteDN giroTransporteDN = GiroTransporteDN.getInstancia();
		GiroTransporteDTO giroTransporteDTO = new GiroTransporteDTO();
		try {
			poblarDTO(giroTransporteForm, giroTransporteDTO);
			giroTransporteDTO = giroTransporteDN.getGiroTransportePorId(giroTransporteDTO);
			poblarDTO(giroTransporteForm, giroTransporteDTO);
			giroTransporteDN.modificar(giroTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroTransporteForm giroTransporteForm = (GiroTransporteForm) form;
		GiroTransporteDTO giroTransporteDTO = new GiroTransporteDTO();
		GiroTransporteDN giroTransporteDN = GiroTransporteDN.getInstancia();
		try {
			poblarDTO(giroTransporteForm, giroTransporteDTO);
			giroTransporteDTO = giroTransporteDN.getGiroTransportePorId(giroTransporteDTO);
			giroTransporteDN.borrar(giroTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroTransporteDTO giroTransporteDTO = new GiroTransporteDTO();
		GiroTransporteForm giroTransporteForm = (GiroTransporteForm) form;
		GiroTransporteDN giroTransporteDN = GiroTransporteDN.getInstancia();
		try {
			String id = request.getParameter("id");
			giroTransporteDTO.setIdGiroTransporte(UtileriasWeb.regresaBigDecimal(id));
			giroTransporteDTO = giroTransporteDN.getGiroTransportePorId(giroTransporteDTO);
			poblarForm(giroTransporteForm, giroTransporteDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
