package mx.com.afirme.midas.catalogos.girotransporte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class GiroTransporteForm extends MidasBaseForm {

	private static final long serialVersionUID = -949186838188796141L;

	private String idGiroTransporte;
	private String codigoGiroTransporte;
	private String idGrupoROT;
	private String descripcionGiroTransporte;

	// getters & setters

	public String getIdGiroTransporte() {
		return idGiroTransporte;
	}

	public void setIdGiroTransporte(String idGiroTransporte) {
		this.idGiroTransporte = idGiroTransporte;
	}

	public String getCodigoGiroTransporte() {
		return codigoGiroTransporte;
	}

	public void setCodigoGiroTransporte(String codigoGiroTransporte) {
		this.codigoGiroTransporte = codigoGiroTransporte;
	}

	public String getIdGrupoROT() {
		return idGrupoROT;
	}

	public void setIdGrupoROT(String idGrupoROT) {
		this.idGrupoROT = idGrupoROT;
	}

	public String getDescripcionGiroTransporte() {
		return descripcionGiroTransporte;
	}

	public void setDescripcionGiroTransporte(String descripcionGiroTransporte) {
		this.descripcionGiroTransporte = descripcionGiroTransporte;
	}

}
