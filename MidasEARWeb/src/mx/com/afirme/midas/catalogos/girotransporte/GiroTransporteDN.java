package mx.com.afirme.midas.catalogos.girotransporte;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class GiroTransporteDN {

	public static final GiroTransporteDN INSTANCIA = new GiroTransporteDN();

	public static GiroTransporteDN getInstancia (){
		return GiroTransporteDN.INSTANCIA;
	}
	
	public String agregar(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		GiroTransporteSN giroTransporteSN = new GiroTransporteSN();
		return giroTransporteSN.agregar(giroTransporteDTO);
	}
	
	public String borrar (GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		GiroTransporteSN giroTransporteSN = new GiroTransporteSN();
		return giroTransporteSN.borrar(giroTransporteDTO);
	}
	
	public String modificar (GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		GiroTransporteSN giroTransporteSN = new GiroTransporteSN();
		return giroTransporteSN.modificar(giroTransporteDTO);
	}
	
	public GiroTransporteDTO getGiroTransportePorId(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		GiroTransporteSN giroTransporteSN = new GiroTransporteSN();
		return giroTransporteSN.getGiroTransportePorId(giroTransporteDTO);
	}
	
	public List<GiroTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		GiroTransporteSN giroTransporteSN = new GiroTransporteSN();
		return giroTransporteSN.listarTodos();
	}
	
	public List<GiroTransporteDTO> listarFiltrado(GiroTransporteDTO giroTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		GiroTransporteSN giroTransporteSN = new GiroTransporteSN();
		return giroTransporteSN.listarFiltrado(giroTransporteDTO);
	}
	
}
