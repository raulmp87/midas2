package mx.com.afirme.midas.catalogos.tipoempaque;

import java.util.List;


import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoEmpaqueDN {
	public static final TipoEmpaqueDN INSTANCIA = new TipoEmpaqueDN();

	public static TipoEmpaqueDN getInstancia (){
		return TipoEmpaqueDN.INSTANCIA;
	}
	
	public String agregar(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoEmpaqueSN tipoEmpaqueSN = new TipoEmpaqueSN();
		return tipoEmpaqueSN.agregar(tipoEmpaqueDTO);
	}
	
	public String borrar (TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoEmpaqueSN tipoEmpaqueSN = new TipoEmpaqueSN();
		return tipoEmpaqueSN.borrar(tipoEmpaqueDTO);
	}
	
	public String modificar (TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoEmpaqueSN tipoEmpaqueSN = new TipoEmpaqueSN();
		return tipoEmpaqueSN.modificar(tipoEmpaqueDTO);
	}
	
	public TipoEmpaqueDTO getTipoEmpaquePorId(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoEmpaqueSN tipoEmpaqueSN = new TipoEmpaqueSN();
		return tipoEmpaqueSN.getTipoEmpaquePorId(tipoEmpaqueDTO);
	}
	
	public List<TipoEmpaqueDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoEmpaqueSN tipoEmpaqueSN = new TipoEmpaqueSN();
		return tipoEmpaqueSN.listarTodos();
	}
	
	public List<TipoEmpaqueDTO> listarFiltrado(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoEmpaqueSN tipoEmpaqueSN = new TipoEmpaqueSN();
		return tipoEmpaqueSN.listarFiltrado(tipoEmpaqueDTO);
	}
}
