package mx.com.afirme.midas.catalogos.tipoempaque;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Christian Ceballos
 * @since 22/07/2009
 *
 */
public class TipoEmpaqueAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
		List<TipoEmpaqueDTO> tiposEmpaques = tipoEmpaqueDN.listarTodos();
		request.setAttribute("empaques", tiposEmpaques);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEmpaqueForm tipoEmpaqueForm = (TipoEmpaqueForm) form;
		TipoEmpaqueDTO tipoEmpaqueDTO = new TipoEmpaqueDTO();
		TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
		try {
			poblarDTO(tipoEmpaqueForm, tipoEmpaqueDTO);
			request.setAttribute("empaques", tipoEmpaqueDN.listarFiltrado(tipoEmpaqueDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(TipoEmpaqueForm tipoEmpaqueForm,
			TipoEmpaqueDTO tipoEmpaqueDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(tipoEmpaqueForm.getIdTabla()))
			tipoEmpaqueDTO.setIdTipoEmpaque(UtileriasWeb.regresaBigDecimal(tipoEmpaqueForm.getIdTabla()));
		if (!UtileriasWeb.esCadenaVacia(tipoEmpaqueForm.getDescripcionTipoEmpaque()))
			tipoEmpaqueDTO.setDescripcionTipoEmpaque(tipoEmpaqueForm.getDescripcionTipoEmpaque().trim().toUpperCase());
		if (!UtileriasWeb.esCadenaVacia(tipoEmpaqueForm.getIdTipoEmpaque()))
			tipoEmpaqueDTO.setCodigoTipoEmpaque(UtileriasWeb.regresaBigDecimal(tipoEmpaqueForm.getIdTipoEmpaque()));
		return null;
	}
	
	private String poblarForm(TipoEmpaqueForm tipoEmpaqueForm,
			TipoEmpaqueDTO tipoEmpaqueDTO) throws SystemException {
		tipoEmpaqueForm.setIdTipoEmpaque(tipoEmpaqueDTO.getCodigoTipoEmpaque().toBigInteger().toString());
		tipoEmpaqueForm.setDescripcionTipoEmpaque(tipoEmpaqueDTO.getDescripcionTipoEmpaque().trim().toUpperCase());
		tipoEmpaqueForm.setIdTabla(tipoEmpaqueDTO.getIdTipoEmpaque().toBigInteger().toString());
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEmpaqueForm tipoEmpaqueForm = (TipoEmpaqueForm) form;
		TipoEmpaqueDTO tipoEmpaqueDTO = new TipoEmpaqueDTO();
		TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
		try {
			poblarDTO(tipoEmpaqueForm, tipoEmpaqueDTO);
			tipoEmpaqueDN.agregar(tipoEmpaqueDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEmpaqueForm tipoEmpaqueForm = (TipoEmpaqueForm) form;
		TipoEmpaqueDTO tipoEmpaqueDTO = new TipoEmpaqueDTO();
		TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
		try {
			poblarDTO(tipoEmpaqueForm, tipoEmpaqueDTO);
			tipoEmpaqueDTO = tipoEmpaqueDN.getTipoEmpaquePorId(tipoEmpaqueDTO);
			poblarDTO(tipoEmpaqueForm, tipoEmpaqueDTO);
			tipoEmpaqueDN.modificar(tipoEmpaqueDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEmpaqueForm tipoEmpaqueForm = (TipoEmpaqueForm) form;
		TipoEmpaqueDTO tipoEmpaqueDTO = new TipoEmpaqueDTO();
		TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
		try {
			poblarDTO(tipoEmpaqueForm, tipoEmpaqueDTO);
			tipoEmpaqueDTO = tipoEmpaqueDN.getTipoEmpaquePorId(tipoEmpaqueDTO);
			tipoEmpaqueDN.borrar(tipoEmpaqueDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoEmpaqueDTO tipoEmpaqueDTO = new TipoEmpaqueDTO();
		TipoEmpaqueForm tipoEmpaqueForm = (TipoEmpaqueForm) form;
		TipoEmpaqueDN tipoEmpaqueDN = TipoEmpaqueDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoEmpaqueDTO.setIdTipoEmpaque(UtileriasWeb.regresaBigDecimal(id));
			tipoEmpaqueDTO = tipoEmpaqueDN.getTipoEmpaquePorId(tipoEmpaqueDTO);
			poblarForm(tipoEmpaqueForm, tipoEmpaqueDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	

	
	
}
