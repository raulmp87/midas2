package mx.com.afirme.midas.catalogos.tipoempaque;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class TipoEmpaqueSN {
	
	private TipoEmpaqueFacadeRemote beanRemoto;

	public TipoEmpaqueSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoEmpaqueFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(tipoEmpaqueDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(tipoEmpaqueDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public String modificar(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(tipoEmpaqueDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<TipoEmpaqueDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public TipoEmpaqueDTO getTipoEmpaquePorId(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoEmpaqueDTO.getIdTipoEmpaque());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public List<TipoEmpaqueDTO> listarFiltrado(TipoEmpaqueDTO tipoEmpaqueDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoEmpaqueDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	
}
