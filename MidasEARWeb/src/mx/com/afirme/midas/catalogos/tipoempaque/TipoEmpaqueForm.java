package mx.com.afirme.midas.catalogos.tipoempaque;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoEmpaqueForm extends MidasBaseForm {

	/**
	 * @author Christian Ceballos
	 * @since 22/07/2009
	 */
	private static final long serialVersionUID = -2532329317236540698L;

	private String idTabla;

	private String idTipoEmpaque;

	private String descripcionTipoEmpaque;

	public String getIdTipoEmpaque() {
		return idTipoEmpaque;
	}

	public void setIdTipoEmpaque(String idTipoEmpaque) {
		this.idTipoEmpaque = idTipoEmpaque;
	}

	public String getDescripcionTipoEmpaque() {
		return descripcionTipoEmpaque;
	}

	public void setDescripcionTipoEmpaque(String descripcionTipoEmpaque) {
		this.descripcionTipoEmpaque = descripcionTipoEmpaque;
	}

	public String getIdTabla() {
		return idTabla;
	}

	public void setIdTabla(String idTabla) {
		this.idTabla = idTabla;
	}

}
