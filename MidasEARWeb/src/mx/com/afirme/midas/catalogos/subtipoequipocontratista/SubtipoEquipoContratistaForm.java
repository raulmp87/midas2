package mx.com.afirme.midas.catalogos.subtipoequipocontratista;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SubtipoEquipoContratistaForm extends MidasBaseForm {
	
	/**
	 * @fecha 06/08/2009
	 */
	
	private static final long serialVersionUID = -5754766351764171096L;
	
	private String idSubtipoEquipoContratista;
	private String tipoEquipoContratistaDTO;
	private String codigoSubtipoEquipoContratista;
	private String descripcionSubtipoEqCont;
	private String claveAutorizacion;
	
	
	/**
	 * @param idSubtipoEquipoContratista
	 *            the IdSubtipoEquipoContratista to set
	 */
	public void setIdSubtipoEquipoContratista(String idSubtipoEquipoContratista) {
		this.idSubtipoEquipoContratista = idSubtipoEquipoContratista;
	}
	/**
	 * @return the idSubtipoEquipoContratista
	 */
	public String getIdSubtipoEquipoContratista() {
		return idSubtipoEquipoContratista;
	}
	
	
	/**
	 * @param tipoEquipoContratistaDTO
	 *            the TipoEquipoContratistaDTO to set
	 */
	public void setTipoEquipoContratistaDTO(String tipoEquipoContratistaDTO) {
		this.tipoEquipoContratistaDTO = tipoEquipoContratistaDTO;
	}
	/**
	 * @return the tipoEquipoContratistaDTO
	 */
	public String getTipoEquipoContratistaDTO() {
		return tipoEquipoContratistaDTO;
	}
	
	
	/**
	 * @param codigoSubtipoEquipoContratista
	 *            the CodigoSubtipoEquipoContratista to set
	 */
	public void setCodigoSubtipoEquipoContratista(
			String codigoSubtipoEquipoContratista) {
		this.codigoSubtipoEquipoContratista = codigoSubtipoEquipoContratista;
	}
	/**
	 * @return the codigoSubtipoEquipoContratista
	 */
	public String getCodigoSubtipoEquipoContratista() {
		return codigoSubtipoEquipoContratista;
	}
	
	
	/**
	 * @param descripcionSubtipoEqCont
	 *            the DescripcionSubtipoEqCont to set
	 */
	public void setDescripcionSubtipoEqCont(String descripcionSubtipoEqCont) {
		this.descripcionSubtipoEqCont = descripcionSubtipoEqCont;
	}
	/**
	 * @return the descripcionSubtipoEqCont
	 */
	public String getDescripcionSubtipoEqCont() {
		return descripcionSubtipoEqCont;
	}
	
	
	/**
	 * @param claveAutorizacion
	 *            the ClaveAutorizacion to set
	 */
	public void setClaveAutorizacion(String claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}
	/**
	 * @return the claveAutorizacion
	 */
	public String getClaveAutorizacion() {
		return claveAutorizacion;
	}
}
