package mx.com.afirme.midas.catalogos.subtipoequipocontratista;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubtipoEquipoContratistaSN {

	private SubtipoEquipoContratistaFacadeRemote beanRemoto;
	
	public SubtipoEquipoContratistaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en SubtipoEquipoContratistaSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(SubtipoEquipoContratistaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<SubtipoEquipoContratistaDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos{

		return beanRemoto.findAll();
	}

	public List<SubtipoEquipoContratistaDTO> listarFiltrado(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		return beanRemoto.listarFiltrado(subtipoEquipoContratistaDTO);
	}

	public SubtipoEquipoContratistaDTO getSubtipoEqContrPorId(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO)
			throws ExcepcionDeAccesoADatos{
		
		return beanRemoto.findById(subtipoEquipoContratistaDTO.getIdSubtipoEquipoContratista());
	}
	
	public SubtipoEquipoContratistaDTO getSubtipoEqContrPorId(BigDecimal idSubtipoEquipoContratista)throws ExcepcionDeAccesoADatos{
	
		return beanRemoto.findById(idSubtipoEquipoContratista);
	}

	public void agregar(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		beanRemoto.save(subtipoEquipoContratistaDTO);
	}

	public void borrar(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		beanRemoto.delete(subtipoEquipoContratistaDTO);
	}

	public void modificar(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		beanRemoto.update(subtipoEquipoContratistaDTO);
	}

}
