package mx.com.afirme.midas.catalogos.subtipoequipocontratista;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.TipoEquipoContratistaDN;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.TipoEquipoContratistaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubtipoEquipoContratistaAction extends MidasMappingDispatchAction{

	/**
	 *@fecha 06/08/2009 
	 */

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
	
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos{

		SubtipoEquipoContratistaDN subtipoEquipoContratistaDN         = SubtipoEquipoContratistaDN.getInstancia();
		List<SubtipoEquipoContratistaDTO> subtipoEquipoContratistaDTO = subtipoEquipoContratistaDN.listarTodos();
		request.setAttribute("subtipoEquipoContratista", subtipoEquipoContratistaDTO);
	}
			
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoEquipoContratistaForm subtipoEquipoContratistaForm = (SubtipoEquipoContratistaForm) form;
		SubtipoEquipoContratistaDTO  subtipoEquipoContratistaDTO  = new SubtipoEquipoContratistaDTO();
		SubtipoEquipoContratistaDN   subtipoEquipoContratistaDN   = SubtipoEquipoContratistaDN.getInstancia();
		
		try{
			this.poblarDTO(subtipoEquipoContratistaForm,subtipoEquipoContratistaDTO);
			request.setAttribute("subtipoEquipoContratista", subtipoEquipoContratistaDN.listarFiltrado(subtipoEquipoContratistaDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoEquipoContratistaForm subtipoEquipoContratistaForm = (SubtipoEquipoContratistaForm) form;
		SubtipoEquipoContratistaDTO  subtipoEquipoContratistaDTO  = new SubtipoEquipoContratistaDTO();
		SubtipoEquipoContratistaDN   subtipoEquipoContratistaDN   = SubtipoEquipoContratistaDN.getInstancia();
		
		try{
			String id = request.getParameter("id");
			subtipoEquipoContratistaDTO.setIdSubtipoEquipoContratista(UtileriasWeb.regresaBigDecimal(id));
			subtipoEquipoContratistaDTO = subtipoEquipoContratistaDN.getSubtipoEqContrPorId(subtipoEquipoContratistaDTO);
			this.poblarForm(subtipoEquipoContratistaForm, subtipoEquipoContratistaDTO);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		SubtipoEquipoContratistaForm subtipoEquipoContratistaForm = (SubtipoEquipoContratistaForm) form;
		SubtipoEquipoContratistaDTO  subtipoEquipoContratistaDTO  = new SubtipoEquipoContratistaDTO();
		SubtipoEquipoContratistaDN   subtipoEquipoContratistaDN   = SubtipoEquipoContratistaDN.getInstancia();
		try{
			this.poblarDTO(subtipoEquipoContratistaForm,subtipoEquipoContratistaDTO);
			subtipoEquipoContratistaDN.agregar(subtipoEquipoContratistaDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		SubtipoEquipoContratistaForm subtipoEquipoContratistaForm = (SubtipoEquipoContratistaForm) form;
		SubtipoEquipoContratistaDTO  subtipoEquipoContratistaDTO  = new SubtipoEquipoContratistaDTO();
		SubtipoEquipoContratistaDN   subtipoEquipoContratistaDN   = SubtipoEquipoContratistaDN.getInstancia();
		
		try{
			this.poblarDTO(subtipoEquipoContratistaForm,subtipoEquipoContratistaDTO);
			subtipoEquipoContratistaDTO = subtipoEquipoContratistaDN.getSubtipoEqContrPorId(subtipoEquipoContratistaDTO);
			subtipoEquipoContratistaDN.borrar(subtipoEquipoContratistaDTO);
			listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		SubtipoEquipoContratistaForm subtipoEquipoContratistaForm = (SubtipoEquipoContratistaForm) form;
		SubtipoEquipoContratistaDTO  subtipoEquipoContratistaDTO  = new SubtipoEquipoContratistaDTO();
		SubtipoEquipoContratistaDN   subtipoEquipoContratistaDN   = SubtipoEquipoContratistaDN.getInstancia();
		
		try {
			this.poblarDTO(subtipoEquipoContratistaForm,subtipoEquipoContratistaDTO);
			subtipoEquipoContratistaDTO = subtipoEquipoContratistaDN.getSubtipoEqContrPorId(subtipoEquipoContratistaDTO);
			this.poblarDTO(subtipoEquipoContratistaForm,subtipoEquipoContratistaDTO);
			subtipoEquipoContratistaDN.modificar(subtipoEquipoContratistaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void poblarForm(SubtipoEquipoContratistaForm subtipoEquipoContratistaForm,
			SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) throws SystemException {
	
		subtipoEquipoContratistaForm.setIdSubtipoEquipoContratista(subtipoEquipoContratistaDTO.getIdSubtipoEquipoContratista().toBigInteger().toString());
		subtipoEquipoContratistaForm.setClaveAutorizacion(subtipoEquipoContratistaDTO.getClaveAutorizacion().toString());
		subtipoEquipoContratistaForm.setCodigoSubtipoEquipoContratista(subtipoEquipoContratistaDTO.getCodigoSubtipoEquipoContratista().toBigInteger().toString());
		subtipoEquipoContratistaForm.setDescripcionSubtipoEqCont(subtipoEquipoContratistaDTO.getDescripcionSubtipoEqCont());
		
		TipoEquipoContratistaDTO tipoEquipoContratistaDTO = subtipoEquipoContratistaDTO.getTipoEquipoContratistaDTO();
		subtipoEquipoContratistaForm.setTipoEquipoContratistaDTO(tipoEquipoContratistaDTO.getIdTipoEquipoContratista().toString());
	}
	
	private void poblarDTO(SubtipoEquipoContratistaForm subtipoEquipoContratistaForm,
			SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		
		if(!UtileriasWeb.esCadenaVacia(subtipoEquipoContratistaForm.getIdSubtipoEquipoContratista()))
			subtipoEquipoContratistaDTO.setIdSubtipoEquipoContratista(UtileriasWeb.regresaBigDecimal(subtipoEquipoContratistaForm.getIdSubtipoEquipoContratista()));
		
		if(!UtileriasWeb.esCadenaVacia(subtipoEquipoContratistaForm.getClaveAutorizacion()))
			subtipoEquipoContratistaDTO.setClaveAutorizacion(UtileriasWeb.regresaShort(subtipoEquipoContratistaForm.getClaveAutorizacion()));
		
		if(!UtileriasWeb.esCadenaVacia(subtipoEquipoContratistaForm.getCodigoSubtipoEquipoContratista()))
			subtipoEquipoContratistaDTO.setCodigoSubtipoEquipoContratista(UtileriasWeb.regresaBigDecimal(subtipoEquipoContratistaForm.getCodigoSubtipoEquipoContratista()));
		
		if(!UtileriasWeb.esCadenaVacia(subtipoEquipoContratistaForm.getDescripcionSubtipoEqCont()))
			subtipoEquipoContratistaDTO.setDescripcionSubtipoEqCont(subtipoEquipoContratistaForm.getDescripcionSubtipoEqCont());
		
		if(!UtileriasWeb.esCadenaVacia(subtipoEquipoContratistaForm.getTipoEquipoContratistaDTO())){
			TipoEquipoContratistaDN  tipoEquipoContratistaDN  = TipoEquipoContratistaDN.getInstancia();
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO = new TipoEquipoContratistaDTO();
			
			tipoEquipoContratistaDTO.setIdTipoEquipoContratista(UtileriasWeb.regresaBigDecimal(subtipoEquipoContratistaForm.getTipoEquipoContratistaDTO()));
			tipoEquipoContratistaDTO  = tipoEquipoContratistaDN.getTipoEqContrPorId(tipoEquipoContratistaDTO);
			subtipoEquipoContratistaDTO.setTipoEquipoContratistaDTO(tipoEquipoContratistaDTO);
		}
			
		
	}
}
