package mx.com.afirme.midas.catalogos.subtipoequipocontratista;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubtipoEquipoContratistaDN {

	private static final SubtipoEquipoContratistaDN INSTANCIA = new SubtipoEquipoContratistaDN();
	
	public static SubtipoEquipoContratistaDN getInstancia() {
		
		return SubtipoEquipoContratistaDN.INSTANCIA;
	}

	public List<SubtipoEquipoContratistaDTO> listarTodos()
			throws SystemException, ExcepcionDeAccesoADatos{
	
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
		return subtipoEquipoContratistaSN.listarTodos();
	}

	public Object listarFiltrado(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
		return subtipoEquipoContratistaSN.listarFiltrado(subtipoEquipoContratistaDTO);
	}

	public SubtipoEquipoContratistaDTO getSubtipoEqContrPorId(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws ExcepcionDeAccesoADatos,SystemException{
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
		return subtipoEquipoContratistaSN.getSubtipoEqContrPorId(subtipoEquipoContratistaDTO);
	}
	
	public SubtipoEquipoContratistaDTO getSubtipoEqContrPorId(BigDecimal idSubtipoEquipoContratista) 
		throws ExcepcionDeAccesoADatos,SystemException{
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
	return subtipoEquipoContratistaSN.getSubtipoEqContrPorId(idSubtipoEquipoContratista);
	}

	public void agregar(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO)
			throws SystemException, ExcepcionDeAccesoADatos{
		
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
		subtipoEquipoContratistaSN.agregar(subtipoEquipoContratistaDTO);
	}

	public void borrar(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO)
			throws SystemException, ExcepcionDeAccesoADatos{
		
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
		subtipoEquipoContratistaSN.borrar(subtipoEquipoContratistaDTO);
	}

	public void modificar(SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		SubtipoEquipoContratistaSN subtipoEquipoContratistaSN = new SubtipoEquipoContratistaSN();
		subtipoEquipoContratistaSN.modificar(subtipoEquipoContratistaDTO);
	}

}
