package mx.com.afirme.midas.catalogos.tipovehiculo;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDN;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoVehiculoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		List<TipoVehiculoDTO> listTipoVehiculo = tipoVehiculoDN.listarTodos();
		request.setAttribute("listTipoVehiculo", listTipoVehiculo);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoVehiculoForm tipoVehiculoForm = (TipoVehiculoForm) form;
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoVehiculoForm, tipoVehiculoDTO);
			request.setAttribute("listTipoVehiculo", tipoVehiculoDN.listarFiltrado(tipoVehiculoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(TipoVehiculoForm tipoVehiculoForm,
			TipoVehiculoDTO tipoVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoVehiculoDTO.class, TipoVehiculoDN.MAPEO_ATRIBUTOS_DTO, TipoVehiculoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(tipoVehiculoDTO, tipoVehiculoForm);
	}
	
	private void poblarForm(TipoVehiculoForm tipoVehiculoForm,
		TipoVehiculoDTO tipoVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoVehiculoForm.class, TipoVehiculoDN.MAPEO_ATRIBUTOS_FORM, TipoVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(tipoVehiculoForm, tipoVehiculoDTO);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoVehiculoForm tipoVehiculoForm = (TipoVehiculoForm) form;
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoVehiculoForm, tipoVehiculoDTO);
			TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO(tipoVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			tipoVehiculoDTO.setTipoBienAutosDTO(tipoBienAutosDTO);
			tipoVehiculoDN.agregar(tipoVehiculoDTO);
			listarTodos(request);
			limpiarForm(tipoVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoVehiculoForm tipoVehiculoForm = (TipoVehiculoForm) form;
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoVehiculoForm, tipoVehiculoDTO);
			tipoVehiculoDTO = tipoVehiculoDN.getTipoVehiculoPorId(tipoVehiculoDTO);
			poblarDTO(tipoVehiculoForm, tipoVehiculoDTO);
			tipoVehiculoDN.modificar(tipoVehiculoDTO);
			listarTodos(request);
			limpiarForm(tipoVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoVehiculoForm tipoVehiculoForm = (TipoVehiculoForm) form;
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoVehiculoForm, tipoVehiculoDTO);
//			tipoVehiculoDTO = tipoVehiculoDN.getTipoVehiculoPorId(tipoVehiculoDTO);
			tipoVehiculoDN.borrar(tipoVehiculoDTO);
			limpiarForm(tipoVehiculoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		TipoVehiculoForm tipoVehiculoForm = (TipoVehiculoForm) form;
		TipoVehiculoDN tipoVehiculoDN = TipoVehiculoDN.getInstancia();
		try {
			String idTcTipoVehiculo = request.getParameter("idTcTipoVehiculo");
			String claveTipoBien = request.getParameter("claveTipoBien");

			tipoVehiculoDTO.setIdTcTipoVehiculo(BigDecimal.valueOf(Double.valueOf(idTcTipoVehiculo)));			
			tipoVehiculoDTO = tipoVehiculoDN.getTipoVehiculoPorId(tipoVehiculoDTO);
			poblarForm(tipoVehiculoForm, tipoVehiculoDTO);
			TipoBienAutosDTO filtro = new TipoBienAutosDTO(claveTipoBien);
			TipoBienAutosDTO bien = TipoBienAutosDN.getInstancia().getTipoBienAutosPorId(filtro);
			tipoVehiculoForm.getTipoBienAutosForm().setDescripcionTipoBien(bien.getDescripcionTipoBien());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(TipoVehiculoForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoVehiculoForm.class, TipoVehiculoDN.MAPEO_ATRIBUTOS_FORM, TipoVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
}
