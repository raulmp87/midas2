package mx.com.afirme.midas.catalogos.tipovehiculo;

import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoVehiculoForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6242599123124723610L;
	private String idTcTipoVehiculo;	
    private TipoBienAutosForm tipoBienAutosForm=new TipoBienAutosForm();
    private String codigoTipoVehiculo;
    private String descripcionTipoVehiculo;

	public String getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}
	public void setIdTcTipoVehiculo(String idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}
	public TipoBienAutosForm getTipoBienAutosForm() {
		return tipoBienAutosForm;
	}
	public void setTipoBienAutosForm(TipoBienAutosForm tipoBienAutosForm) {
		this.tipoBienAutosForm = tipoBienAutosForm;
	}
	public String getCodigoTipoVehiculo() {
		return codigoTipoVehiculo;
	}
	public void setCodigoTipoVehiculo(String codigoTipoVehiculo) {
		this.codigoTipoVehiculo = codigoTipoVehiculo;
	}
	public String getDescripcionTipoVehiculo() {
		return descripcionTipoVehiculo;
	}
	public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
		this.descripcionTipoVehiculo = descripcionTipoVehiculo;
	}
    
    
}
