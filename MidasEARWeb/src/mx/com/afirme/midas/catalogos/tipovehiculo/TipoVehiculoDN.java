package mx.com.afirme.midas.catalogos.tipovehiculo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoVehiculoDN {
	public static final TipoVehiculoDN INSTANCIA = new TipoVehiculoDN();

	public static final String[] MAPEO_ATRIBUTOS_DTO={"idTcTipoVehiculo","tipoBienAutosDTO.claveTipoBien","codigoTipoVehiculo","descripcionTipoVehiculo"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={"idTcTipoVehiculo","tipoBienAutosForm.claveTipoBien","codigoTipoVehiculo","descripcionTipoVehiculo"};
	
	public static TipoVehiculoDN getInstancia (){
		return TipoVehiculoDN.INSTANCIA;
	}
	
	public void agregar(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoVehiculoSN().agregar(tipoVehiculoDTO);
	}
	
	public void borrar (TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoVehiculoSN().borrar(tipoVehiculoDTO);
	}
	
	public void modificar (TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoVehiculoSN().modificar(tipoVehiculoDTO);
	}
	
	public TipoVehiculoDTO getTipoVehiculoPorId(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoVehiculoSN().getTipoVehiculoPorId(tipoVehiculoDTO);
	}
	
	public List<TipoVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoVehiculoSN().listarTodos();
	}
	
	public List<TipoVehiculoDTO> listarFiltrado(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoVehiculoSN().listarFiltrado(tipoVehiculoDTO);
	}
}
