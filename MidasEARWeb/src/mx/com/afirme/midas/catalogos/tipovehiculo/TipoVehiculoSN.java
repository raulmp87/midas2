package mx.com.afirme.midas.catalogos.tipovehiculo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoVehiculoSN {

	private TipoVehiculoFacadeRemote beanRemoto;

	public TipoVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto TipoVehiculo instanciado", Level.FINEST, null);
	}
	
	public void agregar(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TipoVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoVehiculoDTO getTipoVehiculoPorId(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoVehiculoDTO.getIdTcTipoVehiculo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoVehiculoDTO> listarFiltrado(TipoVehiculoDTO tipoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
