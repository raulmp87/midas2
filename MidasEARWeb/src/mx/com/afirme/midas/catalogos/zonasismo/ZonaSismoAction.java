package mx.com.afirme.midas.catalogos.zonasismo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ZonaSismoAction extends MidasMappingDispatchAction{
	
	/**
	 * listar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	/***
	 * listarTodos
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		ZonaSismoDN zonaSismoDN = ZonaSismoDN.getInstancia();
		List<ZonaSismoDTO> zonaSismo = zonaSismoDN.listarTodos();
		request.setAttribute("zonaSismo", zonaSismo);
	}
	
	/***
	 * mostrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * listarFiltrado
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			ZonaSismoForm zonaSismoForm = (ZonaSismoForm) form;
			ZonaSismoDTO  zonaSismoDTO  = new ZonaSismoDTO();
			ZonaSismoDN zonaSismoDN 	= ZonaSismoDN.getInstancia();
			this.poblarDTO(zonaSismoForm, zonaSismoDTO);
			request.setAttribute("zonaSismo", zonaSismoDN.listarFiltrado(zonaSismoDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	
	}

	/**
	 * agregar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;

		try{
			ZonaSismoForm zonaSismoForm = (ZonaSismoForm) form;
			ZonaSismoDTO  zonaSismoDTO  = new ZonaSismoDTO();
			this.poblarDTO(zonaSismoForm, zonaSismoDTO);
			ZonaSismoDN zonaSismoDN = ZonaSismoDN.getInstancia();
			zonaSismoDN.agregar(zonaSismoDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * borrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			ZonaSismoForm zonaSismoForm = (ZonaSismoForm) form;
			ZonaSismoDTO  zonaSismoDTO  = new ZonaSismoDTO();
			ZonaSismoDN zonaSismoDN	    = ZonaSismoDN.getInstancia();
			this.poblarDTO(zonaSismoForm, zonaSismoDTO);
			zonaSismoDTO = zonaSismoDN.getZonaSismoPorId(zonaSismoDTO);
			zonaSismoDN.borrar(zonaSismoDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * modificar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			ZonaSismoForm zonaSismoForm = (ZonaSismoForm) form;
			ZonaSismoDTO  zonaSismoDTO  = new ZonaSismoDTO();
			this.poblarDTO(zonaSismoForm, zonaSismoDTO);
			ZonaSismoDN zonaSismoDN = ZonaSismoDN.getInstancia();
			zonaSismoDN.modificar(zonaSismoDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
		
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		ZonaSismoForm zonaSismoForm = (ZonaSismoForm) form;
		ZonaSismoDTO  zonaSismoDTO  = new ZonaSismoDTO();
		ZonaSismoDN   zonaSismoDN 	= ZonaSismoDN.getInstancia();

		try {
			String idParam = request.getParameter("id");
			zonaSismoDTO.setIdZonaSismo(UtileriasWeb.regresaBigDecimal(idParam));
			zonaSismoDTO = zonaSismoDN.getZonaSismoPorId(zonaSismoDTO);
			this.poblarForm(zonaSismoDTO, zonaSismoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * poblarForm
	 * @param zonaSismoDTO
	 * @param zonaSismoForm
	 * @throws SystemException
	 */
	private void poblarForm(ZonaSismoDTO zonaSismoDTO,
			ZonaSismoForm zonaSismoForm) throws SystemException{
	
		if(zonaSismoDTO.getIdZonaSismo()!= null)
			zonaSismoForm.setIdZonaSismo(zonaSismoDTO.getIdZonaSismo().toBigInteger().toString());
		
		if(zonaSismoDTO.getCodigoZonaSismo()!= null)
			zonaSismoForm.setCodigoZonaSismo(zonaSismoDTO.getCodigoZonaSismo());
		
		if(zonaSismoDTO.getDescripcionZonaSismo() != null)
			zonaSismoForm.setDescripcionZonaSismo(zonaSismoDTO.getDescripcionZonaSismo());
		//******************/
		if(zonaSismoDTO.getValorDefaultCoaseguro()!=null){
		    zonaSismoForm.setValorDefaultCoaseguro(zonaSismoDTO.getValorDefaultCoaseguro().toString());
		}
		if(zonaSismoDTO.getValorDefaultCoaseguroROCH()!=null){
		    zonaSismoForm.setValorDefaultCoaseguroROCH(zonaSismoDTO.getValorDefaultCoaseguroROCH().toString());
		}
		if(zonaSismoDTO.getValorDefaultDeducible()!=null){
		    zonaSismoForm.setValorDefaultDeducible(zonaSismoDTO.getValorDefaultDeducible().toString());
		}
		
		if(zonaSismoDTO.getValorDefaultDeducibleROCH()!=null){
		    zonaSismoForm.setValorDefaultDeducibleROCH(zonaSismoDTO.getValorDefaultDeducibleROCH().toString());
		}
		
		//*******************/
	}

	/**
	 * poblarDTO
	 * @param zonaSismoForm
	 * @param zonaSismoDTO
	 * @throws SystemException
	 */
	private void poblarDTO(ZonaSismoForm zonaSismoForm,
			ZonaSismoDTO zonaSismoDTO) throws SystemException{
		
		if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getIdZonaSismo()))
			zonaSismoDTO.setIdZonaSismo(UtileriasWeb.regresaBigDecimal(zonaSismoForm.getIdZonaSismo()));
		
		if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getCodigoZonaSismo()))
			zonaSismoDTO.setCodigoZonaSismo(zonaSismoForm.getCodigoZonaSismo());
		
		if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getDescripcionZonaSismo()))
			zonaSismoDTO.setDescripcionZonaSismo(zonaSismoForm.getDescripcionZonaSismo().trim().toUpperCase());
		
		//*****************************/
		if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getValorDefaultCoaseguro())){
		    zonaSismoDTO.setValorDefaultCoaseguro(UtileriasWeb.regresaBigDecimal(zonaSismoForm.getValorDefaultCoaseguro()));
		}
                if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getValorDefaultCoaseguroROCH())){
                    zonaSismoDTO.setValorDefaultCoaseguroROCH(UtileriasWeb.regresaBigDecimal(zonaSismoForm.getValorDefaultCoaseguroROCH()));
		}
                if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getValorDefaultDeducible())){
                    zonaSismoDTO.setValorDefaultDeducible(UtileriasWeb.regresaBigDecimal(zonaSismoForm.getValorDefaultDeducible()));
		}
                if(!UtileriasWeb.esCadenaVacia(zonaSismoForm.getValorDefaultDeducibleROCH())){
        	   zonaSismoDTO.setValorDefaultDeducibleROCH(UtileriasWeb.regresaBigDecimal(zonaSismoForm.getValorDefaultDeducibleROCH()));
		}
		//****************************/
		
	}

}
