package mx.com.afirme.midas.catalogos.zonasismo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ZonaSismoDN {

	private static final ZonaSismoDN INSTANCIA = new ZonaSismoDN();
	private ZonaSismoSN zonaSismoSN;
	
	public static ZonaSismoDN getInstancia() {
		
		return ZonaSismoDN.INSTANCIA;
	}

	public List<ZonaSismoDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos, SystemException{
	
		zonaSismoSN = new ZonaSismoSN();
		return zonaSismoSN.listarTodos();
	}

	public List<ZonaSismoDTO> listarFiltrado(ZonaSismoDTO zonaSismoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		zonaSismoSN = new ZonaSismoSN();
		return zonaSismoSN.listarFiltrado(zonaSismoDTO);
	}

	public void agregar(ZonaSismoDTO zonaSismoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		zonaSismoSN = new ZonaSismoSN();
		zonaSismoSN.agregar(zonaSismoDTO);
	}

	public void borrar(ZonaSismoDTO zonaSismoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		zonaSismoSN = new ZonaSismoSN();
		zonaSismoSN.borrar(zonaSismoDTO);
	}

	public void modificar(ZonaSismoDTO zonaSismoDTO)
	        throws SystemException, ExcepcionDeAccesoADatos{

		zonaSismoSN = new ZonaSismoSN();
		zonaSismoSN.modificar(zonaSismoDTO);
		
	}

	public ZonaSismoDTO getZonaSismoPorId(ZonaSismoDTO zonaSismoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		zonaSismoSN = new ZonaSismoSN();
		return zonaSismoSN.getZonaSismoPorId(zonaSismoDTO);
	}

}
