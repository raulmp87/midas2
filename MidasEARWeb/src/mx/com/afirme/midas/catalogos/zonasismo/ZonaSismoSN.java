package mx.com.afirme.midas.catalogos.zonasismo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ZonaSismoSN {
	
	private ZonaSismoFacadeRemote beanRemoto;
	
	public ZonaSismoSN()
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en ZonaSismoSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ZonaSismoFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}

	public List<ZonaSismoDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ZonaSismoDTO> listarFiltrado(ZonaSismoDTO zonaSismoDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.listarFiltrado(zonaSismoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void agregar(ZonaSismoDTO zonaSismoDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			beanRemoto.save(zonaSismoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(ZonaSismoDTO zonaSismoDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			beanRemoto.delete(zonaSismoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(ZonaSismoDTO zonaSismoDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			beanRemoto.update(zonaSismoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ZonaSismoDTO getZonaSismoPorId(ZonaSismoDTO zonaSismoDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findById(zonaSismoDTO.getIdZonaSismo());
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
