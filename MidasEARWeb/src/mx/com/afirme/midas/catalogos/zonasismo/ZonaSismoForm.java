package mx.com.afirme.midas.catalogos.zonasismo;



import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ZonaSismoForm extends MidasBaseForm {

	private static final long serialVersionUID = 3642907366618335740L;

	private String idZonaSismo;
	private String descripcionZonaSismo;
	private String codigoZonaSismo;
	private String valorDefaultCoaseguro;
	private String valorDefaultCoaseguroROCH;
	private String valorDefaultDeducible;
	private String valorDefaultDeducibleROCH;

	/**
	 * @param idZonaSismo
	 *            the idZonaSismo to set
	 */
	public void setIdZonaSismo(String idZonaSismo) {
		this.idZonaSismo = idZonaSismo;
	}

	/**
	 * @return the idZonaSismo
	 */
	public String getIdZonaSismo() {
		return idZonaSismo;
	}

	/**
	 * @param descripcionZonaSismo
	 *            the descripcionZonaSismo to set
	 */
	public void setDescripcionZonaSismo(String descripcionZonaSismo) {
		this.descripcionZonaSismo = descripcionZonaSismo;
	}

	/**
	 * @return the descripcionZonaSismo
	 */
	public String getDescripcionZonaSismo() {
		return descripcionZonaSismo;
	}

	/**
	 * @param codigoZonaSismo
	 *            the codigoZonaSismo to set
	 */
	public void setCodigoZonaSismo(String codigoZonaSismo) {
		this.codigoZonaSismo = codigoZonaSismo;
	}

	/**
	 * @return the codigoZonaSismo
	 */
	public String getCodigoZonaSismo() {
		return codigoZonaSismo;
	}

	public void setValorDefaultCoaseguro(String valorDefaultCoaseguro) {
	    this.valorDefaultCoaseguro = valorDefaultCoaseguro;
	}

	public String getValorDefaultCoaseguro() {
	    return valorDefaultCoaseguro;
	}

	public void setValorDefaultCoaseguroROCH(
		String valorDefaultCoaseguroROCH) {
	    this.valorDefaultCoaseguroROCH = valorDefaultCoaseguroROCH;
	}

	public String getValorDefaultCoaseguroROCH() {
	    return valorDefaultCoaseguroROCH;
	}

	public void setValorDefaultDeducible(String valorDefaultDeducible) {
	    this.valorDefaultDeducible = valorDefaultDeducible;
	}

	public String getValorDefaultDeducible() {
	    return valorDefaultDeducible;
	}

	public void setValorDefaultDeducibleROCH(String valorDefaultDeducibleROCH) {
	    this.valorDefaultDeducibleROCH = valorDefaultDeducibleROCH;
	}

	public String getValorDefaultDeducibleROCH() {
	    return valorDefaultDeducibleROCH;
	}

}
