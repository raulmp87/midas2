package mx.com.afirme.midas.catalogos.subramo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubRamoSN {

	private SubRamoFacadeRemote beanRemoto;

	public SubRamoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubRamoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(subRamoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(subRamoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(subRamoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<SubRamoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public SubRamoDTO getSubramoPorId(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(subRamoDTO.getIdTcSubRamo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubRamoDTO getSubramoPorId(BigDecimal idTcSubRamo) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcSubRamo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SubRamoDTO> listarFiltrado(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(subRamoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<SubRamoDTO> getSubRamosInCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.getSubRamosInCotizacion(idToCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Long obtenerTotalFiltrado(SubRamoDTO subRamoDTO){
		try {
			return beanRemoto.obtenerTotalFiltrado(subRamoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	
	
}
