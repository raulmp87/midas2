package mx.com.afirme.midas.catalogos.subramo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SubRamoForm extends MidasBaseForm {

	/**
	 *@author Christian Ceballos
	 *@since 23/07/2009
	 *@modificacion Jose Luis Arellano
	 *@since 05/08/09
	 */
	private static final long serialVersionUID = 6237757103555440838L;

	private String idSubRamo;
	private String descripcion;
	private String descripcionRamo;
	private String codigoRamo;
	private String idRamo;
	private String codigo;
//	private String idemision;
//	private String idtecnico;
	private String porcentajeMaximoComisionRO;
	private String porcentajeMaximoComisionRCI;
	private String porcentajeMaximoComisionPRR;

	public String getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(String idRamo) {
		this.idRamo = idRamo;
	}

	public String getDescripcionRamo() {
		return descripcionRamo;
	}

	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}

	public String getIdSubRamo() {
		return idSubRamo;
	}

	public void setIdSubRamo(String idSubRamo) {
		this.idSubRamo = idSubRamo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/*public String getIdemision() {
		return idemision;
	}

	public void setIdemision(String idemision) {
		this.idemision = idemision;
	}

	public String getIdtecnico() {
		return idtecnico;
	}

	public void setIdtecnico(String idtecnico) {
		this.idtecnico = idtecnico;
	}*/

	public String getPorcentajeMaximoComisionRO() {
		return porcentajeMaximoComisionRO;
	}

	public void setPorcentajeMaximoComisionRO(String porcentajeMaximoComisionRO) {
		this.porcentajeMaximoComisionRO = porcentajeMaximoComisionRO;
	}

	public String getPorcentajeMaximoComisionRCI() {
		return porcentajeMaximoComisionRCI;
	}

	public void setPorcentajeMaximoComisionRCI(
			String porcentajeMaximoComisionRCI) {
		this.porcentajeMaximoComisionRCI = porcentajeMaximoComisionRCI;
	}

	public String getPorcentajeMaximoComisionPRR() {
		return porcentajeMaximoComisionPRR;
	}

	public void setPorcentajeMaximoComisionPRR(
			String porcentajeMaximoComisionPRR) {
		this.porcentajeMaximoComisionPRR = porcentajeMaximoComisionPRR;
	}

	public String getCodigoRamo() {
		return codigoRamo;
	}

	public void setCodigoRamo(String codigoRamo) {
		this.codigoRamo = codigoRamo;
	}
}
