package mx.com.afirme.midas.catalogos.subramo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ramo.RamoDN;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SubRamoAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
//		String reglaNavegacion = Sistema.EXITOSO;
//		try {
//			
//			listarTodos(request);
//			
//			
//		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		}
//		return mapping.findForward(reglaNavegacion);
	
		return listarFiltrado(mapping, form, request, response);
		
	}
	
	
	
	
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		SubRamoDN subRamoDN = SubRamoDN.getInstancia();
		List<SubRamoDTO> subRamos = subRamoDN.listarTodos();
		request.setAttribute("subRamos", subRamos);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
//		String reglaNavegacion = Sistema.EXITOSO;
//		SubRamoForm subRamoForm = (SubRamoForm) form;
//		SubRamoDTO subRamoDTO = new SubRamoDTO();
//		SubRamoDN subRamoDN = SubRamoDN.getInstancia();
//		try {
//			poblarDTO(subRamoForm, subRamoDTO);
//			request.setAttribute("subRamos", subRamoDN.listarFiltrado(subRamoDTO));
//		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		}
//		return mapping.findForward(reglaNavegacion);
		
		
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			SubRamoForm subRamoForm = (SubRamoForm) form;
			SubRamoDTO subRamoDTO = new SubRamoDTO();
			poblarDTO(subRamoForm, subRamoDTO);
			Long totalRegistros = SubRamoDN.getInstancia().obtenerTotalFiltrado(subRamoDTO);
			subRamoForm.setTotalRegistros(totalRegistros.toString()); 
			
			return listarFiltradoPaginado(mapping, form, request, response);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward listarFiltradoPaginado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			
			if (!listaEnCache(form, request)) {
				
				SubRamoForm subRamoForm = (SubRamoForm) form;
				SubRamoDTO subRamoDTO = new SubRamoDTO();
				SubRamoDN subRamoDN = SubRamoDN.getInstancia();
				poblarDTO(subRamoForm, subRamoDTO);
				
				subRamoDTO.setPrimerRegistroACargar(subRamoForm.getPrimerRegistroACargar());
				subRamoDTO.setNumeroMaximoRegistrosACargar(subRamoForm.getNumeroMaximoRegistrosACargar());
				subRamoForm.setListaPaginada(subRamoDN.listarFiltrado(subRamoDTO), request);
			
			} 
			
			request.setAttribute("subRamos", obtieneListaAMostrar(form, request));
		
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(SubRamoForm subRamoForm, SubRamoDTO subRamoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getDescripcion()))
			subRamoDTO.setDescripcionSubRamo(subRamoForm.getDescripcion());
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getIdSubRamo()))
			subRamoDTO.setIdTcSubRamo(UtileriasWeb.regresaBigDecimal(subRamoForm.getIdSubRamo()));
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getIdRamo())){
			//localizamos el ramo a almacenar
			RamoDN ramoDN = RamoDN.getInstancia();
			RamoDTO ramoDTO = new RamoDTO();
			ramoDTO.setIdTcRamo(UtileriasWeb.regresaBigDecimal(subRamoForm.getIdRamo()));
			ramoDTO = ramoDN.getRamoPorId(ramoDTO);
			subRamoDTO.setRamoDTO(ramoDTO);
		}
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getCodigo()))
			subRamoDTO.setCodigoSubRamo(UtileriasWeb.regresaBigDecimal(subRamoForm.getCodigo()));
		// campos modificados
		/*if (!UtileriasWeb.esCadenaVacia(subRamoForm.getIdemision()))
			subRamoDTO.setIdemision(UtileriasWeb.regresaBigDecimal(subRamoForm.getIdemision()));
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getIdtecnico()))
			subRamoDTO.setIdtecnico(UtileriasWeb.regresaBigDecimal(subRamoForm.getIdtecnico()));*/
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getPorcentajeMaximoComisionRO()))
			subRamoDTO.setPorcentajeMaximoComisionRO(Double.parseDouble(subRamoForm.getPorcentajeMaximoComisionRO()));
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getPorcentajeMaximoComisionRCI()))
			subRamoDTO.setPorcentajeMaximoComisionRCI(Double.parseDouble(subRamoForm.getPorcentajeMaximoComisionRCI()));
		if (!UtileriasWeb.esCadenaVacia(subRamoForm.getPorcentajeMaximoComisionPRR()))
			subRamoDTO.setPorcentajeMaximoComisionPRR(Double.parseDouble(subRamoForm.getPorcentajeMaximoComisionPRR()));
		
		
	}
	
	private void poblarForm(SubRamoForm subRamoForm, SubRamoDTO subRamoDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getIdTcSubRamo()))
			subRamoForm.setIdSubRamo(subRamoDTO.getIdTcSubRamo().toBigInteger().toString());
		if (!UtileriasWeb.esCadenaVacia(subRamoDTO.getDescripcionSubRamo()))
			subRamoForm.setDescripcion(subRamoDTO.getDescripcionSubRamo());
		if (subRamoDTO.getRamoDTO()!=null){
			subRamoForm.setIdRamo(subRamoDTO.getRamoDTO().getIdTcRamo().toString());
			subRamoForm.setDescripcionRamo(subRamoDTO.getRamoDTO().getDescripcion());
			subRamoForm.setCodigoRamo(subRamoDTO.getRamoDTO().getCodigo().toBigInteger().toString());
		}
		if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getCodigoSubRamo()))
			subRamoForm.setCodigo(subRamoDTO.getCodigoSubRamo().toBigInteger().toString());
		// campos modificados
		/*if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getIdemision()))
			subRamoForm.setIdemision(subRamoDTO.getIdemision().toBigInteger().toString());
		if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getIdtecnico()))
			subRamoForm.setIdtecnico(subRamoDTO.getIdtecnico().toBigInteger().toString());*/
		if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getPorcentajeMaximoComisionPRR()))
			subRamoForm.setPorcentajeMaximoComisionPRR(subRamoDTO.getPorcentajeMaximoComisionPRR().toString());
		if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getPorcentajeMaximoComisionRCI()))
			subRamoForm.setPorcentajeMaximoComisionRCI(subRamoDTO.getPorcentajeMaximoComisionRCI().toString());
		if (!UtileriasWeb.esCadenaVacia(""+subRamoDTO.getPorcentajeMaximoComisionRO()))
			subRamoForm.setPorcentajeMaximoComisionRO(subRamoDTO.getPorcentajeMaximoComisionRO().toString());
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubRamoForm subRamoForm = (SubRamoForm) form;
		SubRamoDTO subRamoDTO = new SubRamoDTO();
		SubRamoDN subRamoDN = SubRamoDN.getInstancia();
		try {
			poblarDTO(subRamoForm, subRamoDTO);
			//TODO definir valores para idCuentaContable
			subRamoDTO.setIdCuentaContable(UtileriasWeb.regresaBigDecimal("0"));
			//TODO definir valores para idSubCuentaContable
			subRamoDTO.setIdSubCuentaContable(UtileriasWeb.regresaBigDecimal("0"));
			subRamoDN.agregar(subRamoDTO);
			listarTodos(request);
			limpiarForm(subRamoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	private void limpiarForm(SubRamoForm subRamoForm) {
		subRamoForm.setIdSubRamo("");
		subRamoForm.setDescripcion("");
		subRamoForm.setIdRamo("");
		subRamoForm.setDescripcionRamo("");
		subRamoForm.setCodigo("");
		//subRamoForm.setIdemision("");
		//subRamoForm.setIdtecnico("");
		subRamoForm.setPorcentajeMaximoComisionPRR("");
		subRamoForm.setPorcentajeMaximoComisionRCI("");
		subRamoForm.setPorcentajeMaximoComisionRO("");
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubRamoForm subRamoForm = (SubRamoForm) form;
		SubRamoDTO subRamoDTO = new SubRamoDTO();
		SubRamoDN subRamoDN = SubRamoDN.getInstancia();
		try {
			subRamoDTO.setIdTcSubRamo(UtileriasWeb.regresaBigDecimal(subRamoForm.getIdSubRamo()));
			subRamoDTO = subRamoDN.getSubRamoPorId(subRamoDTO);
			poblarDTO(subRamoForm, subRamoDTO);
			subRamoDN.modificar(subRamoDTO);
			listarTodos(request);
			limpiarForm(subRamoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubRamoForm subRamoForm = (SubRamoForm) form;
		SubRamoDTO subRamoDTO = new SubRamoDTO();
		SubRamoDN subRamoDN = SubRamoDN.getInstancia();
		try {
			subRamoDTO.setIdTcSubRamo(UtileriasWeb.regresaBigDecimal(subRamoForm.getIdSubRamo()));
			subRamoDTO = subRamoDN.getSubRamoPorId(subRamoDTO);
			subRamoDN.borrar(subRamoDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubRamoDTO subRamoDTO = new SubRamoDTO();
		SubRamoForm subRamoForm = (SubRamoForm) form;
		SubRamoDN subRamoDN = SubRamoDN.getInstancia();
		try {
			String id = request.getParameter("id");
			subRamoDTO.setIdTcSubRamo(UtileriasWeb.regresaBigDecimal(id));
			subRamoDTO = subRamoDN.getSubRamoPorId(subRamoDTO);
			poblarForm(subRamoForm, subRamoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
}
