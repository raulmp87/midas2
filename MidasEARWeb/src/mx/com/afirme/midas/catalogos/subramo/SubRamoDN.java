package mx.com.afirme.midas.catalogos.subramo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubRamoDN {
	
	public static final SubRamoDN INSTANCIA = new SubRamoDN();

	public static SubRamoDN getInstancia (){
		return SubRamoDN.INSTANCIA;
	}
	
	public String agregar(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.agregar(subRamoDTO);
	}
	
	public String borrar (SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.borrar(subRamoDTO);
	}
	
	public String modificar (SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.modificar(subRamoDTO);
	}
	
	public SubRamoDTO getSubRamoPorId(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.getSubramoPorId(subRamoDTO);
	}
	
	public SubRamoDTO getSubRamoPorId(BigDecimal idTcSubRamo) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.getSubramoPorId(idTcSubRamo);
	}
	
	public List<SubRamoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.listarTodos();
	}
	
	public List<SubRamoDTO> listarFiltrado(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.listarFiltrado(subRamoDTO);
	}

	public Long obtenerTotalFiltrado(SubRamoDTO subRamoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubRamoSN subRamoSN = new SubRamoSN();
		return subRamoSN.obtenerTotalFiltrado(subRamoDTO);
	}

}
