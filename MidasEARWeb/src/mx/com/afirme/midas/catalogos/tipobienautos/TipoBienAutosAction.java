package mx.com.afirme.midas.catalogos.tipobienautos;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoBienAutosAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
		List<TipoBienAutosDTO> listTipoBienAutos = tipoBienAutosDN.listarTodos();
		request.setAttribute("listTipoBienAutos", listTipoBienAutos);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoBienAutosForm tipoBienAutosForm = (TipoBienAutosForm) form;
		TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
		TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
		try {
			poblarDTO(tipoBienAutosForm, tipoBienAutosDTO);
			request.setAttribute("listTipoBienAutos", tipoBienAutosDN.listarFiltrado(tipoBienAutosDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(TipoBienAutosForm tipoBienAutosForm,
			TipoBienAutosDTO tipoBienAutosDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoBienAutosDTO.class, TipoBienAutosDN.MAPEO_ATRIBUTOS_DTO, TipoBienAutosDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(tipoBienAutosDTO, tipoBienAutosForm);
	}
	
	private void poblarForm(TipoBienAutosForm tipoBienAutosForm,
		TipoBienAutosDTO tipoBienAutosDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoBienAutosForm.class, TipoBienAutosDN.MAPEO_ATRIBUTOS_FORM, TipoBienAutosDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(tipoBienAutosForm, tipoBienAutosDTO);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoBienAutosForm tipoBienAutosForm = (TipoBienAutosForm) form;
		TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
		TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
		try {
			poblarDTO(tipoBienAutosForm, tipoBienAutosDTO);
			tipoBienAutosDN.agregar(tipoBienAutosDTO);
			listarTodos(request);
			limpiarForm(tipoBienAutosForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoBienAutosForm tipoBienAutosForm = (TipoBienAutosForm) form;
		TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
		TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
		try {
			poblarDTO(tipoBienAutosForm, tipoBienAutosDTO);
			tipoBienAutosDTO = tipoBienAutosDN.getTipoBienAutosPorId(tipoBienAutosDTO);
			poblarDTO(tipoBienAutosForm, tipoBienAutosDTO);
			tipoBienAutosDN.modificar(tipoBienAutosDTO);
			listarTodos(request);
			limpiarForm(tipoBienAutosForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoBienAutosForm tipoBienAutosForm = (TipoBienAutosForm) form;
		TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
		TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
		try {
			poblarDTO(tipoBienAutosForm, tipoBienAutosDTO);
//			tipoBienAutosDTO = tipoBienAutosDN.getTipoBienAutosPorId(tipoBienAutosDTO);
			tipoBienAutosDN.borrar(tipoBienAutosDTO);
			limpiarForm(tipoBienAutosForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
		TipoBienAutosForm tipoBienAutosForm = (TipoBienAutosForm) form;
		TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoBienAutosDTO.setClaveTipoBien(id);
			tipoBienAutosDTO = tipoBienAutosDN.getTipoBienAutosPorId(tipoBienAutosDTO);
			poblarForm(tipoBienAutosForm, tipoBienAutosDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(TipoBienAutosForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoBienAutosForm.class, TipoBienAutosDN.MAPEO_ATRIBUTOS_FORM, TipoBienAutosDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
}
