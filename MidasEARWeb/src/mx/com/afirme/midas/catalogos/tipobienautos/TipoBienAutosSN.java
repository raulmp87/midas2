package mx.com.afirme.midas.catalogos.tipobienautos;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoBienAutosSN {

	private TipoBienAutosFacadeRemote beanRemoto;

	public TipoBienAutosSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoBienAutosFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto TipoBienAutos instanciado", Level.FINEST, null);
	}
	
	public void agregar(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoBienAutosDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoBienAutosDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoBienAutosDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TipoBienAutosDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoBienAutosDTO getTipoBienAutosPorId(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoBienAutosDTO.getClaveTipoBien());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoBienAutosDTO> listarFiltrado(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoBienAutosDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
