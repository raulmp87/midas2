package mx.com.afirme.midas.catalogos.tipobienautos;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoBienAutosForm extends MidasBaseForm{
    /**
	 * 
	 */
	private static final long serialVersionUID = 3025984237145433244L;
	private String claveTipoBien;
    private String descripcionTipoBien;
	public String getClaveTipoBien() {
		return claveTipoBien;
	}
	public void setClaveTipoBien(String claveTipoBien) {
		this.claveTipoBien = claveTipoBien;
	}
	public String getDescripcionTipoBien() {
		return descripcionTipoBien;
	}
	public void setDescripcionTipoBien(String descripcionTipoBien) {
		this.descripcionTipoBien = descripcionTipoBien;
	}
}
