package mx.com.afirme.midas.catalogos.tipobienautos;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoBienAutosDN {
	public static final TipoBienAutosDN INSTANCIA = new TipoBienAutosDN();
	public static final String[] MAPEO_ATRIBUTOS_DTO={"claveTipoBien","descripcionTipoBien"};
	public static final String[] MAPEO_ATRIBUTOS_FORM=MAPEO_ATRIBUTOS_DTO;
	public static TipoBienAutosDN getInstancia (){
		return TipoBienAutosDN.INSTANCIA;
	}
	
	public void agregar(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoBienAutosSN().agregar(tipoBienAutosDTO);
	}
	
	public void borrar (TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoBienAutosSN().borrar(tipoBienAutosDTO);
	}
	
	public void modificar (TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoBienAutosSN().modificar(tipoBienAutosDTO);
	}
	
	public TipoBienAutosDTO getTipoBienAutosPorId(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoBienAutosSN().getTipoBienAutosPorId(tipoBienAutosDTO);
	}
	
	public List<TipoBienAutosDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoBienAutosSN().listarTodos();
	}
	
	public List<TipoBienAutosDTO> listarFiltrado(TipoBienAutosDTO tipoBienAutosDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoBienAutosSN().listarFiltrado(tipoBienAutosDTO);
	}
}
