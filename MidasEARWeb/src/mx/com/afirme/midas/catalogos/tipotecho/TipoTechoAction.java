package mx.com.afirme.midas.catalogos.tipotecho;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoTechoAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoTechoDN tipoTechoDN = TipoTechoDN.getInstancia();
		List<TipoTechoDTO> listTipoTecho = tipoTechoDN.listarTodos();
		request.setAttribute("listTipoTecho", listTipoTecho);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoTechoForm tipoTechoForm = (TipoTechoForm) form;
		TipoTechoDTO tipoTechoDTO = new TipoTechoDTO();
		TipoTechoDN tipoTechoDN = TipoTechoDN.getInstancia();
		try {
			poblarDTO(tipoTechoForm, tipoTechoDTO);
			request.setAttribute("listTipoTecho", tipoTechoDN.listarFiltrado(tipoTechoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(TipoTechoForm tipoTechoForm,
			TipoTechoDTO tipoTechoDTO) throws SystemException {
		if (!StringUtil.isEmpty(tipoTechoForm.getDescripciontipotecho()))
			tipoTechoDTO.setDescripcionTipoTecho(tipoTechoForm.getDescripciontipotecho().trim().toUpperCase());
		if (!StringUtil.isEmpty(tipoTechoForm.getIdtctipotecho()))
			tipoTechoDTO.setCodigoTipoTecho(UtileriasWeb.regresaBigDecimal(tipoTechoForm.getIdtctipotecho()));
		if (!StringUtil.isEmpty(tipoTechoForm.getIdtctipotechosistema()))
			tipoTechoDTO.setIdTipoTecho(UtileriasWeb.regresaBigDecimal(tipoTechoForm.getIdtctipotechosistema()));
	}
	
	private void poblarForm(TipoTechoForm tipoTechoForm,
		TipoTechoDTO tipoTechoDTO) throws SystemException {
		tipoTechoForm.setIdtctipotecho(tipoTechoDTO.getCodigoTipoTecho().toBigInteger().toString());
		tipoTechoForm.setDescripciontipotecho(tipoTechoDTO.getDescripcionTipoTecho());
		tipoTechoForm.setIdtctipotechosistema(tipoTechoDTO.getIdTipoTecho().toBigInteger().toString());
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoTechoForm tipoTechoForm = (TipoTechoForm) form;
		TipoTechoDTO tipoTechoDTO = new TipoTechoDTO();
		TipoTechoDN tipoTechoDN = TipoTechoDN.getInstancia();
		try {
			poblarDTO(tipoTechoForm, tipoTechoDTO);
			tipoTechoDN.agregar(tipoTechoDTO);
			listarTodos(request);
			limpiarForm(tipoTechoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoTechoForm tipoTechoForm = (TipoTechoForm) form;
		TipoTechoDTO tipoTechoDTO = new TipoTechoDTO();
		TipoTechoDN tipoTechoDN = TipoTechoDN.getInstancia();
		try {
			poblarDTO(tipoTechoForm, tipoTechoDTO);
			tipoTechoDTO = tipoTechoDN.getTipoTechoPorIdSistema(tipoTechoDTO);
			poblarDTO(tipoTechoForm, tipoTechoDTO);
			tipoTechoDN.modificar(tipoTechoDTO);
			listarTodos(request);
			limpiarForm(tipoTechoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoTechoForm tipoTechoForm = (TipoTechoForm) form;
		TipoTechoDTO tipoTechoDTO = new TipoTechoDTO();
		TipoTechoDN tipoTechoDN = TipoTechoDN.getInstancia();
		try {
			poblarDTO(tipoTechoForm, tipoTechoDTO);
			tipoTechoDTO = tipoTechoDN.getTipoTechoPorIdSistema(tipoTechoDTO);
			tipoTechoDN.borrar(tipoTechoDTO);
			limpiarForm(tipoTechoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoTechoDTO tipoTechoDTO = new TipoTechoDTO();
		TipoTechoForm tipoTechoForm = (TipoTechoForm) form;
		TipoTechoDN tipoTechoDN = TipoTechoDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoTechoDTO.setIdTipoTecho(UtileriasWeb.regresaBigDecimal(id));
			tipoTechoDTO = tipoTechoDN.getTipoTechoPorIdSistema(tipoTechoDTO);
			poblarForm(tipoTechoForm, tipoTechoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(TipoTechoForm form){
		form.setDescripciontipotecho("");
		form.setIdtctipotecho("");
		form.setIdtctipotechosistema("");
	}
}
