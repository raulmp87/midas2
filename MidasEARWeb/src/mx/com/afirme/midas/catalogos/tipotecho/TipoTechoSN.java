package mx.com.afirme.midas.catalogos.tipotecho;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoTechoSN {
	private TipoTechoFacadeRemote beanRemoto;

	public TipoTechoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoTechoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoTechoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoTechoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoTechoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean existeIdUsuario (BigDecimal idTipoTecho) throws ExcepcionDeAccesoADatos{
		try {
			return (beanRemoto.findByProperty("idtctipotecho", idTipoTecho))!=null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoTechoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoTechoDTO getTipoTechoPorIdSistema(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoTechoDTO.getIdTipoTecho());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoTechoDTO> listarFiltrado(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoTechoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
