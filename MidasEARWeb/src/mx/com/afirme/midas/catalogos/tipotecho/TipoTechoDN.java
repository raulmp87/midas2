package mx.com.afirme.midas.catalogos.tipotecho;

import java.math.BigDecimal;
import java.util.List;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoTechoDN {
	public static final TipoTechoDN INSTANCIA = new TipoTechoDN();

	public static TipoTechoDN getInstancia (){
		return TipoTechoDN.INSTANCIA;
	}
	
	public void agregar(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoTechoSN().agregar(tipoTechoDTO);
	}
	
	public void borrar (TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoTechoSN().borrar(tipoTechoDTO);
	}
	
	public boolean existeIdUsuario (BigDecimal idTipoTecho) throws SystemException, ExcepcionDeAccesoADatos{
		return (new TipoTechoSN().existeIdUsuario(idTipoTecho));
	}
	
	public void modificar (TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoTechoSN().modificar(tipoTechoDTO);
	}
	
	public TipoTechoDTO getTipoTechoPorIdSistema(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoTechoSN().getTipoTechoPorIdSistema(tipoTechoDTO);
	}
	
	public List<TipoTechoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoTechoSN().listarTodos();
	}
	
	public List<TipoTechoDTO> listarFiltrado(TipoTechoDTO tipoTechoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoTechoSN().listarFiltrado(tipoTechoDTO);
	}
}
