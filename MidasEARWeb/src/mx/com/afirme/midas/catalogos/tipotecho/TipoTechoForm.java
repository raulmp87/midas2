package mx.com.afirme.midas.catalogos.tipotecho;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * MyEclipse Struts Creation date: 06-25-2009
 * 
 * XDoclet definition:
 * 
 * @struts.form name="tipoTechoForm"
 */

public class TipoTechoForm extends MidasBaseForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6134711324175821989L;

	private String idtctipotechosistema;
	private String idtctipotecho;
	private String descripciontipotecho;

	public String getIdtctipotecho() {
		return idtctipotecho;
	}

	public void setIdtctipotecho(String idtctipotecho) {
		this.idtctipotecho = idtctipotecho;
	}

	public String getDescripciontipotecho() {
		return descripciontipotecho;
	}

	public void setDescripciontipotecho(String descripciontipotecho) {
		this.descripciontipotecho = descripciontipotecho;
	}

	public String getIdtctipotechosistema() {
		return idtctipotechosistema;
	}

	public void setIdtctipotechosistema(String idtctipotechosistema) {
		this.idtctipotechosistema = idtctipotechosistema;
	}
}
