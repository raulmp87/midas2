/**
 * 
 */
package mx.com.afirme.midas.catalogos.impuestoresidenciafiscal;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ImpuestoResidenciaFiscalSN {
	private ImpuestoResidenciaFiscalFacadeRemote beanRemoto;

	public ImpuestoResidenciaFiscalSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ImpuestoResidenciaFiscalFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto ImpuestoResidenciaFiscalFacadeRemote instanciado", Level.FINEST, null);
	}

	public List<ImpuestoResidenciaFiscalDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ImpuestoResidenciaFiscalDTO> impuestosResidenciaFiscal;
		try {
			impuestosResidenciaFiscal = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return impuestosResidenciaFiscal;

	}
	
	public void agregar(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(impuestoResidenciaFiscalDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(impuestoResidenciaFiscalDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ImpuestoResidenciaFiscalDTO getPorId(Long idTcImpuestoResidenciaFiscal) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idTcImpuestoResidenciaFiscal);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}

	public void borrar(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(impuestoResidenciaFiscalDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
