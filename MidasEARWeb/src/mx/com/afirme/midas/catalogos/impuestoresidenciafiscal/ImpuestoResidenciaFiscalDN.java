package mx.com.afirme.midas.catalogos.impuestoresidenciafiscal;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ImpuestoResidenciaFiscalDN {
	private static final ImpuestoResidenciaFiscalDN INSTANCIA = new ImpuestoResidenciaFiscalDN();

	public static ImpuestoResidenciaFiscalDN getInstancia() {
		return ImpuestoResidenciaFiscalDN.INSTANCIA;
	}

	public List<ImpuestoResidenciaFiscalDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ImpuestoResidenciaFiscalSN impuestoResidenciaFiscalSN = new ImpuestoResidenciaFiscalSN();
		return impuestoResidenciaFiscalSN.listarTodos();
	}	

	public void agregar(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ImpuestoResidenciaFiscalSN impuestoResidenciaFiscalSN = new ImpuestoResidenciaFiscalSN();
		impuestoResidenciaFiscalSN.agregar(impuestoResidenciaFiscalDTO);
	}

	public void modificar(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ImpuestoResidenciaFiscalSN impuestoResidenciaFiscalSN = new ImpuestoResidenciaFiscalSN();
		impuestoResidenciaFiscalSN.modificar(impuestoResidenciaFiscalDTO);
	}

	public ImpuestoResidenciaFiscalDTO getPorId(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ImpuestoResidenciaFiscalSN impuestoResidenciaFiscalSN = new ImpuestoResidenciaFiscalSN();
		return impuestoResidenciaFiscalSN.getPorId(impuestoResidenciaFiscalDTO.getIdTcImpuestoResidenciaFiscal());
	}

	public void borrar(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ImpuestoResidenciaFiscalSN impuestoResidenciaFiscalSN = new ImpuestoResidenciaFiscalSN();
		impuestoResidenciaFiscalSN.borrar(impuestoResidenciaFiscalDTO);
	}
}
