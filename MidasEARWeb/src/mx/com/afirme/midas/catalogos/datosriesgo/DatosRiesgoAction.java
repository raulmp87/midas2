package mx.com.afirme.midas.catalogos.datosriesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionId;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DatosRiesgoAction extends MidasMappingDispatchAction {
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		BigDecimal idTcRamo = null;
		BigDecimal idTcSubRamo = null;
		List<ConfiguracionDatoIncisoCotizacionDTO> datosIncisoList = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
		
		try {
			DatosRiesgoForm datosRiesgoForm = (DatosRiesgoForm) form;
			
			if (!UtileriasWeb.esCadenaVacia(datosRiesgoForm.getIdTcRamo()) 
					&& !UtileriasWeb.esCadenaVacia(datosRiesgoForm.getIdTcSubRamo())) {

				idTcRamo = new BigDecimal(datosRiesgoForm.getIdTcRamo());
				idTcSubRamo = new BigDecimal(datosRiesgoForm.getIdTcSubRamo());
			
				datosIncisoList = ConfiguracionDatoIncisoCotizacionDN.getInstancia().getDatosRiesgoImpresionPoliza(idTcRamo, idTcSubRamo);

				
			}
						
			request.setAttribute("listaDatosRiesgo", datosIncisoList);
			
		} catch (SystemException e) {
			e.printStackTrace();
			reglaNavegacion = Sistema.ERROR;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		try {
			DatosRiesgoForm datosRiesgoForm = (DatosRiesgoForm) form;
			
			ConfiguracionDatoIncisoCotizacionId id = new ConfiguracionDatoIncisoCotizacionId();
			id.setIdTcRamo(new BigDecimal(datosRiesgoForm.getIdTcRamo()));
			id.setIdTcSubramo(new BigDecimal(datosRiesgoForm.getIdTcSubRamo()));
			id.setIdToRiesgo(new BigDecimal(datosRiesgoForm.getIdToRiesgo()));
			id.setClaveDetalle(Short.valueOf(datosRiesgoForm.getClaveDetalle()));
			id.setIdDato(new BigDecimal(datosRiesgoForm.getIdDato()));
						
			ConfiguracionDatoIncisoCotizacionDN.getInstancia()
				.actualizaEstatusRiesgoImpresionPoliza(id, datosRiesgoForm.getNuevoEstatus());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void modificarCodigoFormato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		try {
			DatosRiesgoForm datosRiesgoForm = (DatosRiesgoForm) form;
			
			ConfiguracionDatoIncisoCotizacionId id = new ConfiguracionDatoIncisoCotizacionId();
			id.setIdTcRamo(new BigDecimal(datosRiesgoForm.getIdTcRamo()));
			id.setIdTcSubramo(new BigDecimal(datosRiesgoForm.getIdTcSubRamo()));
			id.setIdToRiesgo(new BigDecimal(datosRiesgoForm.getIdToRiesgo()));
			id.setClaveDetalle(Short.valueOf(datosRiesgoForm.getClaveDetalle()));
			id.setIdDato(new BigDecimal(datosRiesgoForm.getIdDato()));
						
			ConfiguracionDatoIncisoCotizacionDN.getInstancia()
				.actualizaCodigoFormatoImpresionPoliza(id, datosRiesgoForm.getCodigoFormato());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
