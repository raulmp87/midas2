package mx.com.afirme.midas.catalogos.datosriesgo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class DatosRiesgoForm extends MidasBaseForm {

	
	private static final long serialVersionUID = 8034458098394787097L;
	
	private String idTcRamo;
	private String idTcSubRamo;
	private String descripcionRiesgo;
	private String descripcionEtiqueta;
	private String claveImpresionPoliza;
	
	private String idToRiesgo;
	private String claveDetalle;
	private String idDato;
	private String nuevoEstatus;
	private String codigoFormato;
	
	public String getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}
	public String getDescripcionEtiqueta() {
		return descripcionEtiqueta;
	}
	public void setDescripcionEtiqueta(String descripcionEtiqueta) {
		this.descripcionEtiqueta = descripcionEtiqueta;
	}
	public String getClaveImpresionPoliza() {
		return claveImpresionPoliza;
	}
	public void setClaveImpresionPoliza(String claveImpresionPoliza) {
		this.claveImpresionPoliza = claveImpresionPoliza;
	}
	public String getIdToRiesgo() {
		return idToRiesgo;
	}
	public void setIdToRiesgo(String idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}
	public String getClaveDetalle() {
		return claveDetalle;
	}
	public void setClaveDetalle(String claveDetalle) {
		this.claveDetalle = claveDetalle;
	}
	public String getIdDato() {
		return idDato;
	}
	public void setIdDato(String idDato) {
		this.idDato = idDato;
	}
	public String getNuevoEstatus() {
		return nuevoEstatus;
	}
	public void setNuevoEstatus(String nuevoEstatus) {
		this.nuevoEstatus = nuevoEstatus;
	}
	public String getCodigoFormato() {
		return codigoFormato;
	}
	public void setCodigoFormato(String codigoFormato) {
		this.codigoFormato = codigoFormato;
	}

	
	
	
	
}
