package mx.com.afirme.midas.catalogos.tipodocumentosiniestro;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoDocumentoSiniestroDN {
	
	public static final TipoDocumentoSiniestroDN INSTANCIA = new TipoDocumentoSiniestroDN();

	public static TipoDocumentoSiniestroDN getInstancia (){
		return TipoDocumentoSiniestroDN.INSTANCIA;
	}
	
	public String agregar(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDocumentoSiniestroSN tipoDocumentoSiniestroSN = new TipoDocumentoSiniestroSN();
		return tipoDocumentoSiniestroSN.agregar(tipoDocumentoSiniestroDTO);
	}
	
	public String borrar (TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDocumentoSiniestroSN tipoDocumentoSiniestroSN = new TipoDocumentoSiniestroSN();
		return tipoDocumentoSiniestroSN.borrar(tipoDocumentoSiniestroDTO);
	}
	
	public String modificar (TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDocumentoSiniestroSN tipoDocumentoSiniestroSN = new TipoDocumentoSiniestroSN();
		return tipoDocumentoSiniestroSN.modificar(tipoDocumentoSiniestroDTO);
	}
	
	public TipoDocumentoSiniestroDTO getTipoDocumentoSiniestroPorId(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoDocumentoSiniestroSN tipoDocumentoSiniestroSN = new TipoDocumentoSiniestroSN();
		return tipoDocumentoSiniestroSN.getTipoDocumentoSiniestroPorId(tipoDocumentoSiniestroDTO);
	}
	
	public List<TipoDocumentoSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoDocumentoSiniestroSN tipoDocumentoSiniestroSN = new TipoDocumentoSiniestroSN();
		return tipoDocumentoSiniestroSN.listarTodos();
	}

}
