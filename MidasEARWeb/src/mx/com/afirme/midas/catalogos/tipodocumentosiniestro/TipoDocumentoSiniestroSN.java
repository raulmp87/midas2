package mx.com.afirme.midas.catalogos.tipodocumentosiniestro;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoDocumentoSiniestroSN {
	
	private TipoDocumentoSiniestroFacadeRemote beanRemoto;

	public TipoDocumentoSiniestroSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoDocumentoSiniestroFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(tipoDocumentoSiniestroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(tipoDocumentoSiniestroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(tipoDocumentoSiniestroDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<TipoDocumentoSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoDocumentoSiniestroDTO getTipoDocumentoSiniestroPorId(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(tipoDocumentoSiniestroDTO.getIdTcTipoDocumentoSiniestro());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
