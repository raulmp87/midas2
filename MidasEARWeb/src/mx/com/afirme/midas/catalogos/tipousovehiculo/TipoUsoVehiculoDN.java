package mx.com.afirme.midas.catalogos.tipousovehiculo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoUsoVehiculoDN {
	public static final TipoUsoVehiculoDN INSTANCIA = new TipoUsoVehiculoDN();
	public static final String[] MAPEO_ATRIBUTOS_DTO={"idTcTipoUsoVehiculo","idTcTipoVehiculo","codigoTipoUsoVehiculo","descripcionTipoUsoVehiculo", "claveAmisTipoServicio"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={"idTcTipoUsoVehiculo","idTcTipoVehiculo","codigoTipoUsoVehiculo","descripcionTipoUsoVehiculo", "claveAmisTipoServicio"};

	public static TipoUsoVehiculoDN getInstancia (){
		return TipoUsoVehiculoDN.INSTANCIA;
	}
	
	public void agregar(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoUsoVehiculoSN().agregar(tipoUsoVehiculoDTO);
	}
	
	public void borrar (TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoUsoVehiculoSN().borrar(tipoUsoVehiculoDTO);
	}
	
	public void modificar (TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoUsoVehiculoSN().modificar(tipoUsoVehiculoDTO);
	}
	
	public TipoUsoVehiculoDTO getTipoUsoVehiculoPorId(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoUsoVehiculoSN().getTipoUsoVehiculoPorId(tipoUsoVehiculoDTO);
	}
	
	public List<TipoUsoVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoUsoVehiculoSN().listarTodos();
	}
	
	public List<TipoUsoVehiculoDTO> listarFiltrado(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoUsoVehiculoSN().listarFiltrado(tipoUsoVehiculoDTO);
	}
}
