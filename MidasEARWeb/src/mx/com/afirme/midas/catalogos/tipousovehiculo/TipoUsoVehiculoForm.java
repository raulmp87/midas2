package mx.com.afirme.midas.catalogos.tipousovehiculo;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoUsoVehiculoForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5935065578666332623L;
	private String idTcTipoUsoVehiculo;
	private String idTcTipoVehiculo;
    private String codigoTipoUsoVehiculo;
    private String descripcionTipoUsoVehiculo;
    private String claveAmisTipoServicio;
    private TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();


	public String getIdTcTipoUsoVehiculo() {
		return idTcTipoUsoVehiculo;
	}
	public void setIdTcTipoUsoVehiculo(String idTcTipoUsoVehiculo) {
		this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
	}
	public String getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}
	public void setIdTcTipoVehiculo(String idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}
	public String getCodigoTipoUsoVehiculo() {
		return codigoTipoUsoVehiculo;
	}
	public void setCodigoTipoUsoVehiculo(String codigoTipoUsoVehiculo) {
		this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
	}
	public String getDescripcionTipoUsoVehiculo() {
		return descripcionTipoUsoVehiculo;
	}
	public void setDescripcionTipoUsoVehiculo(String descripcionTipoUsoVehiculo) {
		this.descripcionTipoUsoVehiculo = descripcionTipoUsoVehiculo;
	}
	public String getClaveAmisTipoServicio() {
		return claveAmisTipoServicio;
	}
	public void setClaveAmisTipoServicio(String claveAmisTipoServicio) {
		this.claveAmisTipoServicio = claveAmisTipoServicio;
	}
	public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}
	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}
    
}
