package mx.com.afirme.midas.catalogos.tipousovehiculo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDN;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoUsoVehiculoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoUsoVehiculoDN tipoUsoVehiculoDN = TipoUsoVehiculoDN.getInstancia();
		List<TipoUsoVehiculoDTO> listTipoUsoVehiculo = tipoUsoVehiculoDN.listarTodos();
		request.setAttribute("listTipoUsoVehiculo", listTipoUsoVehiculo);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoUsoVehiculoForm tipoUsoVehiculoForm = (TipoUsoVehiculoForm) form;
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
		TipoUsoVehiculoDN tipoUsoVehiculoDN = TipoUsoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
			request.setAttribute("listTipoUsoVehiculo", tipoUsoVehiculoDN.listarFiltrado(tipoUsoVehiculoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(TipoUsoVehiculoForm tipoUsoVehiculoForm,
			TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
//		tipoUsoVehiculoForm.descomponerIdCompuestoTipoVehiculo();
		filler.estableceMapeoResultados(TipoUsoVehiculoDTO.class, TipoUsoVehiculoDN.MAPEO_ATRIBUTOS_DTO, TipoUsoVehiculoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(tipoUsoVehiculoDTO, tipoUsoVehiculoForm);
	}
	
	private void poblarForm(TipoUsoVehiculoForm tipoUsoVehiculoForm,
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoUsoVehiculoForm.class, TipoUsoVehiculoDN.MAPEO_ATRIBUTOS_FORM, TipoUsoVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoUsoVehiculoForm tipoUsoVehiculoForm = (TipoUsoVehiculoForm) form;
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
		TipoUsoVehiculoDN tipoUsoVehiculoDN = TipoUsoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
			tipoUsoVehiculoDN.agregar(tipoUsoVehiculoDTO);
			listarTodos(request);
			limpiarForm(tipoUsoVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoUsoVehiculoForm tipoUsoVehiculoForm = (TipoUsoVehiculoForm) form;
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
		TipoUsoVehiculoDN tipoUsoVehiculoDN = TipoUsoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
			tipoUsoVehiculoDTO = tipoUsoVehiculoDN.getTipoUsoVehiculoPorId(tipoUsoVehiculoDTO);
			poblarDTO(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
			tipoUsoVehiculoDN.modificar(tipoUsoVehiculoDTO);
			listarTodos(request);
			limpiarForm(tipoUsoVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoUsoVehiculoForm tipoUsoVehiculoForm = (TipoUsoVehiculoForm) form;
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
		TipoUsoVehiculoDN tipoUsoVehiculoDN = TipoUsoVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
//			tipoUsoVehiculoDTO = tipoUsoVehiculoDN.getTipoUsoVehiculoPorId(tipoUsoVehiculoDTO);
			tipoUsoVehiculoDN.borrar(tipoUsoVehiculoDTO);
			limpiarForm(tipoUsoVehiculoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
		TipoUsoVehiculoForm tipoUsoVehiculoForm = (TipoUsoVehiculoForm) form;
		TipoUsoVehiculoDN tipoUsoVehiculoDN = TipoUsoVehiculoDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoUsoVehiculoDTO.setIdTcTipoUsoVehiculo(UtileriasWeb.regresaBigDecimal(id));
			tipoUsoVehiculoDTO = tipoUsoVehiculoDN.getTipoUsoVehiculoPorId(tipoUsoVehiculoDTO);
			
			TipoVehiculoDTO filtro = new TipoVehiculoDTO();
			filtro.setIdTcTipoVehiculo(tipoUsoVehiculoDTO.getIdTcTipoVehiculo());
			TipoVehiculoDTO tipoVehiculoDTO = TipoVehiculoDN.getInstancia().getTipoVehiculoPorId(filtro);
			tipoUsoVehiculoDTO.setTipoVehiculoDTO(tipoVehiculoDTO);
			poblarForm(tipoUsoVehiculoForm, tipoUsoVehiculoDTO);
			tipoUsoVehiculoForm.setTipoVehiculoDTO(tipoVehiculoDTO);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(TipoUsoVehiculoForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoUsoVehiculoForm.class, TipoUsoVehiculoDN.MAPEO_ATRIBUTOS_FORM, TipoUsoVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
}
