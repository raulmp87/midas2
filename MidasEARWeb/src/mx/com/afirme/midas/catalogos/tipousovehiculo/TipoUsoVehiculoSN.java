package mx.com.afirme.midas.catalogos.tipousovehiculo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoUsoVehiculoSN {

	private TipoUsoVehiculoFacadeRemote beanRemoto;

	public TipoUsoVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoUsoVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto TipoUsoVehiculo instanciado", Level.FINEST, null);
	}
	
	public void agregar(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoUsoVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoUsoVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoUsoVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TipoUsoVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoUsoVehiculoDTO getTipoUsoVehiculoPorId(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoUsoVehiculoDTO.getIdTcTipoUsoVehiculo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoUsoVehiculoDTO> listarFiltrado(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoUsoVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
