package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoMontajeMaquinaAction extends MidasMappingDispatchAction{

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoMontajeMaquinaDN tipoMontajeMaquinaDN = TipoMontajeMaquinaDN.getInstancia();
		List<TipoMontajeMaquinaDTO> montajes = tipoMontajeMaquinaDN.listarTodos();
		request.setAttribute("montajes", montajes);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMontajeMaquinaForm tipoMontajeMaquinaForm = (TipoMontajeMaquinaForm) form;
		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = new TipoMontajeMaquinaDTO();
		TipoMontajeMaquinaDN tipoMontajeMaquinaDN = TipoMontajeMaquinaDN.getInstancia();
		try {
			poblarDTO(tipoMontajeMaquinaForm, tipoMontajeMaquinaDTO);
			request.setAttribute("montajes", tipoMontajeMaquinaDN.listarFiltrado(tipoMontajeMaquinaDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(TipoMontajeMaquinaForm tipoMontajeMaquinaForm,
			TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(tipoMontajeMaquinaForm.getCodigoTipoMontajeMaq()))
			tipoMontajeMaquinaDTO.setCodigotipomontajemaq(UtileriasWeb.regresaBigDecimal(tipoMontajeMaquinaForm.getCodigoTipoMontajeMaq().toString()));
		if (!UtileriasWeb.esCadenaVacia(tipoMontajeMaquinaForm.getDescripcionTipoMontajeMaq()))
			tipoMontajeMaquinaDTO.setDescripciontipomontajemaq(tipoMontajeMaquinaForm.getDescripcionTipoMontajeMaq().trim().toUpperCase());
		if (!UtileriasWeb.esCadenaVacia(tipoMontajeMaquinaForm.getIdTcTipoMontajeMaq()))
			tipoMontajeMaquinaDTO.setIdtctipomontajemaq(UtileriasWeb.regresaBigDecimal(tipoMontajeMaquinaForm.getIdTcTipoMontajeMaq()));
		return null;
	}
	
	private String poblarForm(TipoMontajeMaquinaForm tipoMontajeMaquinaForm,
			TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws SystemException {
		tipoMontajeMaquinaForm.setCodigoTipoMontajeMaq(tipoMontajeMaquinaDTO.getCodigotipomontajemaq().toBigInteger().toString());
		tipoMontajeMaquinaForm.setDescripcionTipoMontajeMaq(tipoMontajeMaquinaDTO.getDescripciontipomontajemaq());
		tipoMontajeMaquinaForm.setIdTcTipoMontajeMaq(tipoMontajeMaquinaDTO.getIdtctipomontajemaq().toBigInteger().toString());
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMontajeMaquinaForm tipoMontajeMaquinaForm = (TipoMontajeMaquinaForm) form;
		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = new TipoMontajeMaquinaDTO();
		TipoMontajeMaquinaDN tipoMontajeMaquinaDN = TipoMontajeMaquinaDN.getInstancia();
		try {
			poblarDTO(tipoMontajeMaquinaForm, tipoMontajeMaquinaDTO);
			tipoMontajeMaquinaDN.agregar(tipoMontajeMaquinaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMontajeMaquinaForm tipoMontajeMaquinaForm = (TipoMontajeMaquinaForm) form;
		TipoMontajeMaquinaDN tipoMontajeMaquinaDN = TipoMontajeMaquinaDN.getInstancia();
		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = new TipoMontajeMaquinaDTO();
		try {
			poblarDTO(tipoMontajeMaquinaForm, tipoMontajeMaquinaDTO);
			tipoMontajeMaquinaDTO = tipoMontajeMaquinaDN.getTipoMontajeMaquinaPorId(tipoMontajeMaquinaDTO);
			poblarDTO(tipoMontajeMaquinaForm, tipoMontajeMaquinaDTO);
			tipoMontajeMaquinaDN.modificar(tipoMontajeMaquinaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMontajeMaquinaForm tipoMontajeMaquinaForm = (TipoMontajeMaquinaForm) form;
		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = new TipoMontajeMaquinaDTO();
		TipoMontajeMaquinaDN tipoMontajeMaquinaDN = TipoMontajeMaquinaDN.getInstancia();
		try {
			poblarDTO(tipoMontajeMaquinaForm, tipoMontajeMaquinaDTO);
			tipoMontajeMaquinaDTO = tipoMontajeMaquinaDN.getTipoMontajeMaquinaPorId(tipoMontajeMaquinaDTO);
			tipoMontajeMaquinaDN.borrar(tipoMontajeMaquinaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = new TipoMontajeMaquinaDTO();
		TipoMontajeMaquinaForm tipoMontajeMaquinaForm = (TipoMontajeMaquinaForm) form;
		TipoMontajeMaquinaDN tipoMontajeMaquinaDN = TipoMontajeMaquinaDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoMontajeMaquinaDTO.setIdtctipomontajemaq(UtileriasWeb.regresaBigDecimal(id));
			tipoMontajeMaquinaDTO = tipoMontajeMaquinaDN.getTipoMontajeMaquinaPorId(tipoMontajeMaquinaDTO);
			poblarForm(tipoMontajeMaquinaForm, tipoMontajeMaquinaDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
}
