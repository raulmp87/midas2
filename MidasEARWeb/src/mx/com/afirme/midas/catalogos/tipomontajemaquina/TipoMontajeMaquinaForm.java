package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoMontajeMaquinaForm extends MidasBaseForm{
	
	/**
	 * @author Christian Ceballos
	 */
	private static final long serialVersionUID = 3696592400729258256L;
	
	private String idTcTipoMontajeMaq;
	
	private String codigoTipoMontajeMaq;
	
	private String descripcionTipoMontajeMaq;

	public String getIdTcTipoMontajeMaq() {
		return idTcTipoMontajeMaq;
	}

	public void setIdTcTipoMontajeMaq(String idTcTipoMontajeMaq) {
		this.idTcTipoMontajeMaq = idTcTipoMontajeMaq;
	}

	public String getCodigoTipoMontajeMaq() {
		return codigoTipoMontajeMaq;
	}

	public void setCodigoTipoMontajeMaq(String codigoTipoMontajeMaq) {
		this.codigoTipoMontajeMaq = codigoTipoMontajeMaq;
	}

	public String getDescripcionTipoMontajeMaq() {
		return descripcionTipoMontajeMaq;
	}

	public void setDescripcionTipoMontajeMaq(String descripcionTipoMontajeMaq) {
		this.descripcionTipoMontajeMaq = descripcionTipoMontajeMaq;
	}
	
	

}
