package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubtipoMontajeMaquinaDN {
	
	public static final SubtipoMontajeMaquinaDN INSTANCIA = new SubtipoMontajeMaquinaDN();

	public static SubtipoMontajeMaquinaDN getInstancia (){
		return SubtipoMontajeMaquinaDN.INSTANCIA;
	}
	
	public String agregar(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.agregar(subtipoMontajeMaquinaDTO);
	}
	
	public String borrar (SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.borrar(subtipoMontajeMaquinaDTO);
	}
	
	public String modificar (SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.modificar(subtipoMontajeMaquinaDTO);
	}
	
	public SubtipoMontajeMaquinaDTO getSubtipoMontajeMaquinaPorId(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.getSubtipoMontajeMaquinaPorId(subtipoMontajeMaquinaDTO);
	}
	
	public SubtipoMontajeMaquinaDTO getSubtipoMontajeMaquinaPorId(BigDecimal idtcsubtipomontajemaq) throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.getSubtipoMontajeMaquinaPorId(idtcsubtipomontajemaq);
	}
	
	public List<SubtipoMontajeMaquinaDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.listarTodos();
	}
	
	public List<SubtipoMontajeMaquinaDTO> listarFiltrado(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubtipoMontajeMaquinaSN subtipoMontajeMaquinaSN = new SubtipoMontajeMaquinaSN();
		return subtipoMontajeMaquinaSN.listarFiltrado(subtipoMontajeMaquinaDTO);
	}

}
