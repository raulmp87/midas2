package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubtipoMontajeMaquinaSN {
	
	private SubtipoMontajeMaquinaFacadeRemote beanRemoto;

	public SubtipoMontajeMaquinaSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubtipoMontajeMaquinaFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(subtipoMontajeMaquinaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(subtipoMontajeMaquinaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(subtipoMontajeMaquinaDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<SubtipoMontajeMaquinaDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubtipoMontajeMaquinaDTO getSubtipoMontajeMaquinaPorId(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(subtipoMontajeMaquinaDTO.getIdtcsubtipomontajemaq());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubtipoMontajeMaquinaDTO getSubtipoMontajeMaquinaPorId(BigDecimal idtcsubtipomontajemaq) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(idtcsubtipomontajemaq);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SubtipoMontajeMaquinaDTO> listarFiltrado(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(subtipoMontajeMaquinaDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
