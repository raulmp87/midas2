package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubtipoMontajeMaquinaAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		SubtipoMontajeMaquinaDN subtipoMontajeMaquinaDN = SubtipoMontajeMaquinaDN.getInstancia();
		List<SubtipoMontajeMaquinaDTO> subtiposMontaje = subtipoMontajeMaquinaDN.listarTodos();
		request.setAttribute("subtiposMontaje", subtiposMontaje);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm = (SubtipoMontajeMaquinaForm) form;
		SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO = new SubtipoMontajeMaquinaDTO();
		SubtipoMontajeMaquinaDN subtipoMontajeMaquinaDN = SubtipoMontajeMaquinaDN.getInstancia();
		try {
			poblarDTO(subtipoMontajeMaquinaForm, subtipoMontajeMaquinaDTO);
			request.setAttribute("subtiposMontaje", subtipoMontajeMaquinaDN.listarFiltrado(subtipoMontajeMaquinaDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm,
			SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(subtipoMontajeMaquinaForm.getClaveautorizacion()))
			subtipoMontajeMaquinaDTO.setClaveAutorizacion(UtileriasWeb.regresaShort(subtipoMontajeMaquinaForm.getClaveautorizacion()));
		if (!UtileriasWeb.esCadenaVacia(subtipoMontajeMaquinaForm.getCodigosubtipomontajemaq()))
			subtipoMontajeMaquinaDTO.setCodigosubtipomontajemaq(UtileriasWeb.regresaBigDecimal(subtipoMontajeMaquinaForm.getCodigosubtipomontajemaq()));
		if (!UtileriasWeb.esCadenaVacia(subtipoMontajeMaquinaForm.getDescripcionsubtipomontajemaq()))
			subtipoMontajeMaquinaDTO.setDescripcionsubtipomontajemaq(subtipoMontajeMaquinaForm.getDescripcionsubtipomontajemaq());
		if (!UtileriasWeb.esCadenaVacia(subtipoMontajeMaquinaForm.getIdtcsubtipomontajemaq()))
			subtipoMontajeMaquinaDTO.setIdtcsubtipomontajemaq(UtileriasWeb.regresaBigDecimal(subtipoMontajeMaquinaForm.getIdtcsubtipomontajemaq()));
		if (!UtileriasWeb.esCadenaVacia(subtipoMontajeMaquinaForm.getIdTipoMontaje())){
			TipoMontajeMaquinaDN tipoMontajeMaquinaDN  = TipoMontajeMaquinaDN.getInstancia();
			TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = new TipoMontajeMaquinaDTO();
			tipoMontajeMaquinaDTO.setIdtctipomontajemaq(UtileriasWeb.regresaBigDecimal(subtipoMontajeMaquinaForm.getIdTipoMontaje()));
			tipoMontajeMaquinaDTO = tipoMontajeMaquinaDN.getTipoMontajeMaquinaPorId(tipoMontajeMaquinaDTO);
			subtipoMontajeMaquinaDTO.setTipoMontajeMaquinaDTO(tipoMontajeMaquinaDTO);
		}
			
		return null;
	}
	
	private String poblarForm(SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm,
			SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO) throws SystemException {
		subtipoMontajeMaquinaForm.setCodigosubtipomontajemaq(subtipoMontajeMaquinaDTO.getCodigosubtipomontajemaq().toBigInteger().toString());
		subtipoMontajeMaquinaForm.setDescripcionsubtipomontajemaq(subtipoMontajeMaquinaDTO.getDescripcionsubtipomontajemaq());
		subtipoMontajeMaquinaForm.setIdtcsubtipomontajemaq(subtipoMontajeMaquinaDTO.getIdtcsubtipomontajemaq().toBigInteger().toString());
		subtipoMontajeMaquinaForm.setIdTipoMontaje(subtipoMontajeMaquinaDTO.getTipoMontajeMaquinaDTO().getId().toString());
		subtipoMontajeMaquinaForm.setDescripcionTipoMontaje(subtipoMontajeMaquinaDTO.getTipoMontajeMaquinaDTO().getDescription());
		String clave = subtipoMontajeMaquinaDTO.getClaveAutorizacion().toString();
		subtipoMontajeMaquinaForm.setClaveautorizacion(clave);
		if (clave.equals("0"))
			clave = "NO REQUERIDA";
		if (clave.equals("1"))
			clave = "AREA TECNICA";
		if (clave.equals("2"))
			clave = "AREA DE REASEGURO";
		subtipoMontajeMaquinaForm.setDescripcionClave(clave);
		
		
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm = (SubtipoMontajeMaquinaForm) form;
		SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO = new SubtipoMontajeMaquinaDTO();
		SubtipoMontajeMaquinaDN subtipoMontajeMaquinaDN = SubtipoMontajeMaquinaDN.getInstancia();
		try {
			poblarDTO(subtipoMontajeMaquinaForm, subtipoMontajeMaquinaDTO);
			subtipoMontajeMaquinaDN.agregar(subtipoMontajeMaquinaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm = (SubtipoMontajeMaquinaForm) form;
		SubtipoMontajeMaquinaDN subtipoMontajeMaquinaDN = SubtipoMontajeMaquinaDN.getInstancia();
		SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO = new SubtipoMontajeMaquinaDTO();
		try {
			poblarDTO(subtipoMontajeMaquinaForm, subtipoMontajeMaquinaDTO);
			subtipoMontajeMaquinaDTO = subtipoMontajeMaquinaDN.getSubtipoMontajeMaquinaPorId(subtipoMontajeMaquinaDTO);
			poblarDTO(subtipoMontajeMaquinaForm, subtipoMontajeMaquinaDTO);
			subtipoMontajeMaquinaDN.modificar(subtipoMontajeMaquinaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm = (SubtipoMontajeMaquinaForm) form;
		SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO = new SubtipoMontajeMaquinaDTO();
		SubtipoMontajeMaquinaDN subtipoMontajeMaquinaDN = SubtipoMontajeMaquinaDN.getInstancia();
		try {
			poblarDTO(subtipoMontajeMaquinaForm, subtipoMontajeMaquinaDTO);
			subtipoMontajeMaquinaDTO = subtipoMontajeMaquinaDN.getSubtipoMontajeMaquinaPorId(subtipoMontajeMaquinaDTO);
			subtipoMontajeMaquinaDN.borrar(subtipoMontajeMaquinaDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO = new SubtipoMontajeMaquinaDTO();
		SubtipoMontajeMaquinaForm subtipoMontajeMaquinaForm = (SubtipoMontajeMaquinaForm) form;
		SubtipoMontajeMaquinaDN subtipoMontajeMaquinaDN = SubtipoMontajeMaquinaDN.getInstancia();
		try {
			String id = request.getParameter("id");
			subtipoMontajeMaquinaDTO.setIdtcsubtipomontajemaq(UtileriasWeb.regresaBigDecimal(id));
			subtipoMontajeMaquinaDTO = subtipoMontajeMaquinaDN.getSubtipoMontajeMaquinaPorId(subtipoMontajeMaquinaDTO);
			poblarForm(subtipoMontajeMaquinaForm, subtipoMontajeMaquinaDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
