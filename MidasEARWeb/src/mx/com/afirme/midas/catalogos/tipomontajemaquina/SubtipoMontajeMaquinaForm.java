package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SubtipoMontajeMaquinaForm extends MidasBaseForm {

	/**
	 * @author Christian Ceballos
	 * 
	 */
	private static final long serialVersionUID = 3877473532653957504L;

	private String idtcsubtipomontajemaq;

	private String codigosubtipomontajemaq;

	private String descripcionsubtipomontajemaq;

	private String claveautorizacion;

	private String idTipoMontaje;

	private String descripcionTipoMontaje;

	private String descripcionClave;

	public String getIdtcsubtipomontajemaq() {
		return idtcsubtipomontajemaq;
	}

	public void setIdtcsubtipomontajemaq(String idtcsubtipomontajemaq) {
		this.idtcsubtipomontajemaq = idtcsubtipomontajemaq;
	}

	public String getCodigosubtipomontajemaq() {
		return codigosubtipomontajemaq;
	}

	public void setCodigosubtipomontajemaq(String codigosubtipomontajemaq) {
		this.codigosubtipomontajemaq = codigosubtipomontajemaq;
	}

	public String getDescripcionsubtipomontajemaq() {
		return descripcionsubtipomontajemaq;
	}

	public void setDescripcionsubtipomontajemaq(
			String descripcionsubtipomontajemaq) {
		this.descripcionsubtipomontajemaq = descripcionsubtipomontajemaq;
	}

	public String getClaveautorizacion() {
		return claveautorizacion;
	}

	public void setClaveautorizacion(String claveautorizacion) {
		this.claveautorizacion = claveautorizacion;
	}

	public String getIdTipoMontaje() {
		return idTipoMontaje;
	}

	public void setIdTipoMontaje(String idTipoMontaje) {
		this.idTipoMontaje = idTipoMontaje;
	}

	public String getDescripcionTipoMontaje() {
		return descripcionTipoMontaje;
	}

	public void setDescripcionTipoMontaje(String descripcionTipoMontaje) {
		this.descripcionTipoMontaje = descripcionTipoMontaje;
	}

	public String getDescripcionClave() {
		return descripcionClave;
	}

	public void setDescripcionClave(String descripcionClave) {
		this.descripcionClave = descripcionClave;
	}

}
