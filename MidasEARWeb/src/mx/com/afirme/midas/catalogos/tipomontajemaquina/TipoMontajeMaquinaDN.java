package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoMontajeMaquinaDN {

	public static final TipoMontajeMaquinaDN INSTANCIA = new TipoMontajeMaquinaDN();

	public static TipoMontajeMaquinaDN getInstancia (){
		return TipoMontajeMaquinaDN.INSTANCIA;
	}
	
	public String agregar(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMontajeMaquinaSN tipoMontajeMaquinaSN = new TipoMontajeMaquinaSN();
		return tipoMontajeMaquinaSN.agregar(tipoMontajeMaquinaDTO);
	}
	
	public String borrar (TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMontajeMaquinaSN tipoMontajeMaquinaSN = new TipoMontajeMaquinaSN();
		return tipoMontajeMaquinaSN.borrar(tipoMontajeMaquinaDTO);
	}
	
	public String modificar (TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMontajeMaquinaSN tipoMontajeMaquinaSN = new TipoMontajeMaquinaSN();
		return tipoMontajeMaquinaSN.modificar(tipoMontajeMaquinaDTO);
	}
	
	public TipoMontajeMaquinaDTO getTipoMontajeMaquinaPorId(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMontajeMaquinaSN tipoMontajeMaquinaSN = new TipoMontajeMaquinaSN();
		return tipoMontajeMaquinaSN.getTipoMontajeMaquinaPorId(tipoMontajeMaquinaDTO);
	}
	
	public List<TipoMontajeMaquinaDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoMontajeMaquinaSN tipoMontajeMaquinaSN = new TipoMontajeMaquinaSN();
		return tipoMontajeMaquinaSN.listarTodos();
	}
	
	public List<TipoMontajeMaquinaDTO> listarFiltrado(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoMontajeMaquinaSN tipoMontajeMaquinaSN = new TipoMontajeMaquinaSN();
		return tipoMontajeMaquinaSN.listarFiltrado(tipoMontajeMaquinaDTO);
	}
	
}
