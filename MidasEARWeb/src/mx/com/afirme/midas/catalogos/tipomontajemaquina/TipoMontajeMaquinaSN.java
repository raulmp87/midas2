package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoMontajeMaquinaSN {

	private TipoMontajeMaquinaFacadeRemote beanRemoto;

	public TipoMontajeMaquinaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoMontajeMaquinaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(tipoMontajeMaquinaDTO);
		}catch(EJBTransactionRolledbackException e){
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(tipoMontajeMaquinaDTO);
		}catch(EJBTransactionRolledbackException e){
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(tipoMontajeMaquinaDTO);
		}catch (EJBTransactionRolledbackException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public List<TipoMontajeMaquinaDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public TipoMontajeMaquinaDTO getTipoMontajeMaquinaPorId(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoMontajeMaquinaDTO.getIdtctipomontajemaq());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoMontajeMaquinaDTO> listarFiltrado(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoMontajeMaquinaDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
}
