package mx.com.afirme.midas.catalogos.tipomaquinaria;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoMaquinariaSN {
	private TipoMaquinariaFacadeRemote beanRemoto;

	public TipoMaquinariaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoMaquinariaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto TipoMaquinaria instanciado", Level.FINEST, null);
	}
	
	public void agregar(TipoMaquinariaDTO TipoMaquinariaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(TipoMaquinariaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(TipoMaquinariaDTO TipoMaquinariaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(TipoMaquinariaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(TipoMaquinariaDTO TipoMaquinariaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(TipoMaquinariaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TipoMaquinariaDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoMaquinariaDTO getPorId(BigDecimal idTcTipoMaquinaria) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcTipoMaquinaria);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
