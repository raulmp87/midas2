package mx.com.afirme.midas.catalogos.tipomaquinaria;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoMaquinariaSN {
	private SubTipoMaquinariaFacadeRemote beanRemoto;

	public SubTipoMaquinariaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubTipoMaquinariaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SubTipoMaquinaria instanciado", Level.FINEST, null);
	}
	
	public void agregar(SubTipoMaquinariaDTO SubTipoMaquinariaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(SubTipoMaquinariaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(SubTipoMaquinariaDTO SubTipoMaquinariaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(SubTipoMaquinariaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(SubTipoMaquinariaDTO SubTipoMaquinariaDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(SubTipoMaquinariaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SubTipoMaquinariaDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubTipoMaquinariaDTO getPorId(BigDecimal idTcSubTipoMaquinaria) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcSubTipoMaquinaria);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
