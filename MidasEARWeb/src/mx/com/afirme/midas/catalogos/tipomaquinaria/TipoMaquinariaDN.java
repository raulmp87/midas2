package mx.com.afirme.midas.catalogos.tipomaquinaria;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoMaquinariaDN {
	private static final TipoMaquinariaDN INSTANCIA = new TipoMaquinariaDN();

	public static TipoMaquinariaDN getInstancia (){
		return TipoMaquinariaDN.INSTANCIA;
	}
	
	public void agregar(TipoMaquinariaDTO TipoMaquinariaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoMaquinariaSN().agregar(TipoMaquinariaDTO);
	}
	
	public void borrar (TipoMaquinariaDTO TipoMaquinariaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoMaquinariaSN().borrar(TipoMaquinariaDTO);
	}
	
	public void modificar (TipoMaquinariaDTO TipoMaquinariaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoMaquinariaSN().modificar(TipoMaquinariaDTO);
	}
	
	public TipoMaquinariaDTO getPorId(BigDecimal idTcTipoMaquinaria) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoMaquinariaSN().getPorId(idTcTipoMaquinaria);
	}
	
	public List<TipoMaquinariaDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoMaquinariaSN().listarTodos();
	}
	
}
