package mx.com.afirme.midas.catalogos.tipomaquinaria;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoMaquinariaDN {
	private static final SubTipoMaquinariaDN INSTANCIA = new SubTipoMaquinariaDN();

	public static SubTipoMaquinariaDN getInstancia (){
		return SubTipoMaquinariaDN.INSTANCIA;
	}
	
	public void agregar(SubTipoMaquinariaDTO SubTipoMaquinariaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubTipoMaquinariaSN().agregar(SubTipoMaquinariaDTO);
	}
	
	public void borrar (SubTipoMaquinariaDTO SubTipoMaquinariaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubTipoMaquinariaSN().borrar(SubTipoMaquinariaDTO);
	}
	
	public void modificar (SubTipoMaquinariaDTO SubTipoMaquinariaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubTipoMaquinariaSN().modificar(SubTipoMaquinariaDTO);
	}
	
	public SubTipoMaquinariaDTO getPorId(BigDecimal idTcSubTipoMaquinaria) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubTipoMaquinariaSN().getPorId(idTcSubTipoMaquinaria);
	}
	
	public List<SubTipoMaquinariaDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new SubTipoMaquinariaSN().listarTodos();
	}
}
