package mx.com.afirme.midas.catalogos.distanciaciudad;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DistanciaCiudadDN {
	private static final DistanciaCiudadDN INSTANCIA = new DistanciaCiudadDN();

	public static DistanciaCiudadDN getInstancia() {
		return DistanciaCiudadDN.INSTANCIA;
	}

	public List<DistanciaCiudadDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		DistanciaCiudadSN distanciaCiudadSN = new DistanciaCiudadSN();
		return distanciaCiudadSN.listarTodos();
	}

	public List<DistanciaCiudadDTO> listarFiltrados(DistanciaCiudadDTO distanciaCiudadDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DistanciaCiudadSN distanciaCiudadSN = new DistanciaCiudadSN();
		return distanciaCiudadSN.listarFiltrados(distanciaCiudadDTO);
	}

	public void agregar(DistanciaCiudadDTO distanciaCiudadDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		DistanciaCiudadSN distanciaCiudadSN = new DistanciaCiudadSN();
		distanciaCiudadSN.agregar(distanciaCiudadDTO);
	}

	public void modificar(DistanciaCiudadDTO distanciaCiudadDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		DistanciaCiudadSN distanciaCiudadSN = new DistanciaCiudadSN();
		distanciaCiudadSN.modificar(distanciaCiudadDTO);
	}

	public DistanciaCiudadDTO getPorId(DistanciaCiudadDTO distanciaCiudadDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		DistanciaCiudadSN distanciaCiudadSN = new DistanciaCiudadSN();
		return distanciaCiudadSN.getPorId(distanciaCiudadDTO.getIdDistanciaCiudad());
	}

	public void borrar(DistanciaCiudadDTO distanciaCiudadDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		DistanciaCiudadSN distanciaCiudadSN = new DistanciaCiudadSN();
		distanciaCiudadSN.borrar(distanciaCiudadDTO);
	}
}
