/**
 * 
 */
package mx.com.afirme.midas.catalogos.distanciaciudad;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class DistanciaCiudadSN {
	private DistanciaCiudadFacadeRemote beanRemoto;

	public DistanciaCiudadSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DistanciaCiudadFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<DistanciaCiudadDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<DistanciaCiudadDTO> distanciaCiudades;
		try {
			distanciaCiudades = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return distanciaCiudades;

	}
	public List<DistanciaCiudadDTO> listarFiltrados(DistanciaCiudadDTO distanciaCiudadDTO) throws ExcepcionDeAccesoADatos {
		List<DistanciaCiudadDTO> distanciaCiudades;
		try {
			distanciaCiudades = beanRemoto.listarFiltrado(distanciaCiudadDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		return distanciaCiudades;
	}

	public void agregar(DistanciaCiudadDTO distanciaCiudadDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(distanciaCiudadDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(DistanciaCiudadDTO distanciaCiudadDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(distanciaCiudadDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DistanciaCiudadDTO getPorId(Long id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}

	public void borrar(DistanciaCiudadDTO distanciaCiudadDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(distanciaCiudadDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
