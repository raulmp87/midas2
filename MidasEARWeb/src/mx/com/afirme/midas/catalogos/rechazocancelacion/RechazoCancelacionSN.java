package mx.com.afirme.midas.catalogos.rechazocancelacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RechazoCancelacionSN {
	private RechazoCancelacionFacadeRemote beanRemoto;

	public RechazoCancelacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RechazoCancelacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto RechazoCancelacion instanciado", Level.FINEST, null);
	}
	
	public RechazoCancelacionDTO agregar(RechazoCancelacionDTO rechazoCancelacionDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.save(rechazoCancelacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(RechazoCancelacionDTO RechazoCancelacionDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(RechazoCancelacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(RechazoCancelacionDTO RechazoCancelacionDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(RechazoCancelacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<RechazoCancelacionDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public RechazoCancelacionDTO getPorId(BigDecimal idTcRechazoCancelacion) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcRechazoCancelacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
