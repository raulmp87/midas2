package mx.com.afirme.midas.catalogos.rechazocancelacion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RechazoCancelacionDN {
	private static final RechazoCancelacionDN INSTANCIA = new RechazoCancelacionDN();

	public static RechazoCancelacionDN getInstancia (){
		return RechazoCancelacionDN.INSTANCIA;
	}
	
	public void agregar(RechazoCancelacionDTO RechazoCancelacion) throws ExcepcionDeAccesoADatos, SystemException{
		new  RechazoCancelacionSN().agregar( RechazoCancelacion);
	}
	
	public void borrar ( RechazoCancelacionDTO  RechazoCancelacion) throws ExcepcionDeAccesoADatos, SystemException{
		new  RechazoCancelacionSN().borrar( RechazoCancelacion);
	}
	
	public void modificar ( RechazoCancelacionDTO  RechazoCancelacion) throws ExcepcionDeAccesoADatos, SystemException{
		new  RechazoCancelacionSN().modificar( RechazoCancelacion);
	}
	
	public  RechazoCancelacionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos, SystemException{
		return new RechazoCancelacionSN().getPorId(id);
	}
	
	public List<RechazoCancelacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new RechazoCancelacionSN().listarTodos();
	}
}
