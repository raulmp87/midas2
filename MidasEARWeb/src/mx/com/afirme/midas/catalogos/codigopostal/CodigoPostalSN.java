/**
 * 
 */
package mx.com.afirme.midas.catalogos.codigopostal;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.RecargosDescuentosCPDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class CodigoPostalSN {
	private PaisFacadeRemote beanRemotoPais;
	private EstadoFacadeRemote beanRemotoEstado;
	private CiudadFacadeRemote beanRemotoCiudad;
	private MunicipioFacadeRemote beanRemotoMunicipio;

	public CodigoPostalSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en CodigoPostalSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemotoPais = serviceLocator.getEJB(PaisFacadeRemote.class);
		beanRemotoEstado = serviceLocator.getEJB(EstadoFacadeRemote.class);
		beanRemotoCiudad = serviceLocator.getEJB(CiudadFacadeRemote.class);
		beanRemotoMunicipio = serviceLocator
				.getEJB(MunicipioFacadeRemote.class);

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<PaisDTO> listarPaisTodos() throws ExcepcionDeAccesoADatos {
		List<PaisDTO> paises = beanRemotoPais.findAll();
		return paises;

	}

	public List<EstadoDTO> listarEstadoTodos() throws ExcepcionDeAccesoADatos {
		List<EstadoDTO> estados = beanRemotoEstado.findAll();
		return estados;

	}

	public List<CiudadDTO> listarCiudadTodos() throws ExcepcionDeAccesoADatos {
		List<CiudadDTO> ajustadores = beanRemotoCiudad.findAll();
		return ajustadores;

	}

	
	public MunicipioDTO getMunicipioPorId(String id) {
		return beanRemotoMunicipio.findById(id);
	}

	public List<CiudadDTO> listarCiudadesPorDescripcion(String descripcion)
			throws ExcepcionDeAccesoADatos {

		return null;
	}

	public CiudadDTO getCiudadPorId(String id) throws ExcepcionDeAccesoADatos {		
		return beanRemotoCiudad.findById(id);
	}
	
	public EstadoDTO getEstadoPorId(String id) throws ExcepcionDeAccesoADatos {		
		return beanRemotoEstado.findById(id);
	}
	
	public String validarCodigoPostal(String codigoPostal,int idEstado) {
		return beanRemotoEstado.validarCodigoPostal(codigoPostal,idEstado);
	}
	
	public List<RecargosDescuentosCPDTO> getRegargosCP(String idCobertura,String idSeccion) {
		return beanRemotoEstado.getRegargosCP(idCobertura,idSeccion);
	}
	
}
