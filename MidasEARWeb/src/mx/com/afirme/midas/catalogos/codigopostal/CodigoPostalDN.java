package mx.com.afirme.midas.catalogos.codigopostal;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.RecargosDescuentosCPDTO;
import mx.com.afirme.midas.catalogos.colonia.ColoniaDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CodigoPostalDN {
	private static final CodigoPostalDN INSTANCIA = new CodigoPostalDN();

	public static CodigoPostalDN getInstancia() {
		return CodigoPostalDN.INSTANCIA;
	}

	@SuppressWarnings("unchecked")
	public List<PaisDTO> listarPaisTodos() throws ExcepcionDeAccesoADatos {
		CodigoPostalSN codigoPostalSN = null;
		List listaPaisesTodos = new ArrayList();
		try {
			codigoPostalSN = new CodigoPostalSN();
			listaPaisesTodos= codigoPostalSN.listarPaisTodos();
		} catch (SystemException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos("Error en listado de Paises"); 
		}
			return listaPaisesTodos;
	}
	@SuppressWarnings("unchecked")
	public List<EstadoDTO> listarEstadoTodos() throws ExcepcionDeAccesoADatos {
		CodigoPostalSN codigoPostalSN = null;
		@SuppressWarnings("unused")
		List listaEstadosTodos = new ArrayList();
		try {
			codigoPostalSN = new CodigoPostalSN();
			listaEstadosTodos= codigoPostalSN.listarEstadoTodos();
		} catch (SystemException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos("Error en listado de Estados"); 
		}
	    return codigoPostalSN.listarEstadoTodos();

	}
	@SuppressWarnings("unchecked")
	public List<CiudadDTO> listarCiudadTodos() throws ExcepcionDeAccesoADatos {
		CodigoPostalSN codigoPostalSN = null;
		List listaCiudadesTodos = new ArrayList();
		try {
			codigoPostalSN = new CodigoPostalSN();
			listaCiudadesTodos = codigoPostalSN.listarCiudadTodos();
		} catch (SystemException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos("Error en listado de Ciudades"); 
		}
		return listaCiudadesTodos;

	}
	@SuppressWarnings("unchecked")
	public List<CiudadDTO> listarColoniaTodos() throws ExcepcionDeAccesoADatos {
		List listaCiudadesTodos = new ArrayList();
		listaCiudadesTodos = ColoniaDN.getInstancia().listarColoniaTodos();
		return listaCiudadesTodos;

	}
	
	public List<CiudadDTO> listarCiudadesPorDescripcion(String descripcion) throws ExcepcionDeAccesoADatos {
		
		return null;
	}

	public EstadoDTO getEstadoPorId(String id) throws ExcepcionDeAccesoADatos {
		CodigoPostalSN codigoPostalSN = null;
		EstadoDTO estadoDTO = null;
		try {
			codigoPostalSN = new CodigoPostalSN();
			estadoDTO = codigoPostalSN.getEstadoPorId(id);
		} catch (SystemException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos("Error al obtener estado por id.");
		}
		return estadoDTO;		
	}
	
	public CiudadDTO getCiudadPorId(String id) throws ExcepcionDeAccesoADatos {
		CodigoPostalSN codigoPostalSN = null;
		CiudadDTO ciudadDTO = null;
		try {
			codigoPostalSN = new CodigoPostalSN();
			ciudadDTO = codigoPostalSN.getCiudadPorId(id);
		} catch (SystemException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos("Error al obtener ciudad por id.");
		}
		return ciudadDTO;		
	}				
	
	public MunicipioDTO getMunicipioPorId(String id) throws ExcepcionDeAccesoADatos{
		CodigoPostalSN codigoPostalSN = null;

		try {
			codigoPostalSN = new CodigoPostalSN();
			return codigoPostalSN.getMunicipioPorId(id);
		} catch (SystemException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos("Error al obtener municipio por id.");
		}
	}
	
	public ColoniaDTO getColoniaPorId(String id) throws ExcepcionDeAccesoADatos{
		return ColoniaDN.getInstancia().getColoniaPorId(id);
		
	}	
	
	public String getDescripcionmunicipio(String id) throws ExcepcionDeAccesoADatos{
		MunicipioDTO municipioDTO = getMunicipioPorId(id);
		return municipioDTO.getDescription();
	}
	
	public String validarCodigoPostal(String codigoPostal,int idEstado) {
		CodigoPostalSN codigoPostalSN = null;
		try {
			codigoPostalSN = new CodigoPostalSN();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return codigoPostalSN.validarCodigoPostal(codigoPostal,idEstado);
	}
	
	public List<RecargosDescuentosCPDTO> getRegargosCP(String idCobertura,String idSeccion) throws Exception{
		CodigoPostalSN codigoPostalSN = null;
		try {
			codigoPostalSN = new CodigoPostalSN();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return codigoPostalSN.getRegargosCP(idCobertura,idSeccion);
			
	}
	
}
