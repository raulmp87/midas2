package mx.com.afirme.midas.catalogos.mediotransporte;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MedioTransporteSN {

	private MedioTransporteFacadeRemote beanRemoto;

	public MedioTransporteSN() throws SystemException{
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MedioTransporteFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(medioTransporteDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(medioTransporteDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public String modificar(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(medioTransporteDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
		return null;
	}
	
	public List<MedioTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}
	
	public MedioTransporteDTO getMedioTransportePorId(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(medioTransporteDTO.getIdMedioTransporte());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<MedioTransporteDTO> listarFiltrado(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(medioTransporteDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}
