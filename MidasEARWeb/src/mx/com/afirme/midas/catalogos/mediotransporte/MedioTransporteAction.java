package mx.com.afirme.midas.catalogos.mediotransporte;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MedioTransporteAction extends MidasMappingDispatchAction{
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		MedioTransporteDN medioTransporteDN = MedioTransporteDN.getInstancia();
		List<MedioTransporteDTO> transportes = medioTransporteDN.listarTodos();
		request.setAttribute("transportes", transportes);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MedioTransporteForm medioTransporteForm = (MedioTransporteForm) form;
		MedioTransporteDTO medioTransporteDTO = new MedioTransporteDTO();
		MedioTransporteDN medioTransporteDN = MedioTransporteDN.getInstancia();
		try {
			poblarDTO(medioTransporteForm, medioTransporteDTO);
			request.setAttribute("transportes", medioTransporteDN.listarFiltrado(medioTransporteDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(MedioTransporteForm medioTransporteForm,
			MedioTransporteDTO medioTransporteDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(medioTransporteForm.getIdtcmediotransportesistema()))
			medioTransporteDTO.setIdMedioTransporte(UtileriasWeb.regresaBigDecimal(medioTransporteForm.getIdtcmediotransportesistema()));
		if (!UtileriasWeb.esCadenaVacia(medioTransporteForm.getIdtcmediotransporte()))
			medioTransporteDTO.setCodigoMedioTransporte(UtileriasWeb.regresaBigDecimal(medioTransporteForm.getIdtcmediotransporte()));
		if(!UtileriasWeb.esCadenaVacia(medioTransporteForm.getDescripcionmediotransporte()))
			medioTransporteDTO.setDescripcionMedioTransporte(medioTransporteForm.getDescripcionmediotransporte().trim().toUpperCase());
		return null;
	}
	
	private String poblarForm(MedioTransporteForm medioTransporteForm,
			MedioTransporteDTO medioTransporteDTO) throws SystemException {
		medioTransporteForm.setDescripcionmediotransporte(medioTransporteDTO.getDescripcionMedioTransporte());
		medioTransporteForm.setIdtcmediotransporte(medioTransporteDTO.getCodigoMedioTransporte().toBigInteger().toString());
		medioTransporteForm.setIdtcmediotransportesistema(medioTransporteDTO.getIdMedioTransporte().toBigInteger().toString());
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MedioTransporteForm medioTransporteForm = (MedioTransporteForm) form;
		MedioTransporteDTO medioTransporteDTO = new MedioTransporteDTO();
		MedioTransporteDN medioTransporteDN = MedioTransporteDN.getInstancia();
		try {
			poblarDTO(medioTransporteForm, medioTransporteDTO);
			medioTransporteDN.agregar(medioTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MedioTransporteForm medioTransporteForm = (MedioTransporteForm) form;
		MedioTransporteDN medioTransporteDN = MedioTransporteDN.getInstancia();
		MedioTransporteDTO medioTransporteDTO = new MedioTransporteDTO();
		try {
			poblarDTO(medioTransporteForm, medioTransporteDTO);
			medioTransporteDTO = medioTransporteDN.getMedioTransportePorId(medioTransporteDTO);
			poblarDTO(medioTransporteForm, medioTransporteDTO);
			medioTransporteDN.modificar(medioTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MedioTransporteForm medioTransporteForm = (MedioTransporteForm) form;
		MedioTransporteDTO medioTransporteDTO = new MedioTransporteDTO();
		MedioTransporteDN medioTransporteDN = MedioTransporteDN.getInstancia();
		try {
			poblarDTO(medioTransporteForm, medioTransporteDTO);
			medioTransporteDTO = medioTransporteDN.getMedioTransportePorId(medioTransporteDTO);
			medioTransporteDN.borrar(medioTransporteDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MedioTransporteDTO medioTransporteDTO = new MedioTransporteDTO();
		MedioTransporteForm medioTransporteForm = (MedioTransporteForm) form;
		MedioTransporteDN medioTransporteDN = MedioTransporteDN.getInstancia();
		try {
			String id = request.getParameter("id");
			medioTransporteDTO.setIdMedioTransporte(UtileriasWeb.regresaBigDecimal(id));
			medioTransporteDTO = medioTransporteDN.getMedioTransportePorId(medioTransporteDTO);
			poblarForm(medioTransporteForm, medioTransporteDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}


}
