package mx.com.afirme.midas.catalogos.mediotransporte;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MedioTransporteDN {

	public static final MedioTransporteDN INSTANCIA = new MedioTransporteDN();

	public static MedioTransporteDN getInstancia (){
		return MedioTransporteDN.INSTANCIA;
	}
	
	public String agregar(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MedioTransporteSN medioTransporteSN = new MedioTransporteSN();
		return medioTransporteSN.agregar(medioTransporteDTO);
	}
	
	public String borrar (MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MedioTransporteSN medioTransporteSN = new MedioTransporteSN();
		return medioTransporteSN.borrar(medioTransporteDTO);
	}
	
	public String modificar (MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MedioTransporteSN medioTransporteSN = new MedioTransporteSN();
		return medioTransporteSN.modificar(medioTransporteDTO);
	}
	
	public MedioTransporteDTO getMedioTransportePorId(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MedioTransporteSN medioTransporteSN = new MedioTransporteSN();
		return medioTransporteSN.getMedioTransportePorId(medioTransporteDTO);
	}
	
	public List<MedioTransporteDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		MedioTransporteSN medioTransporteSN = new MedioTransporteSN();
		return medioTransporteSN.listarTodos();
	}
	
	public List<MedioTransporteDTO> listarFiltrado(MedioTransporteDTO medioTransporteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MedioTransporteSN medioTransporteSN = new MedioTransporteSN();
		return medioTransporteSN.listarFiltrado(medioTransporteDTO);
	}
	
}
