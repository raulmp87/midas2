package mx.com.afirme.midas.catalogos.mediotransporte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class MedioTransporteForm extends MidasBaseForm {

	/**
	 * @author Christian Ceballos
	 * @since 31/07/09
	 */
	private static final long serialVersionUID = 5758465361929034549L;

	private String idtcmediotransportesistema;

	private String idtcmediotransporte;

	private String descripcionmediotransporte;

	public String getIdtcmediotransportesistema() {
		return idtcmediotransportesistema;
	}

	public void setIdtcmediotransportesistema(String idtcmediotransportesistema) {
		this.idtcmediotransportesistema = idtcmediotransportesistema;
	}

	public String getIdtcmediotransporte() {
		return idtcmediotransporte;
	}

	public void setIdtcmediotransporte(String idtcmediotransporte) {
		this.idtcmediotransporte = idtcmediotransporte;
	}

	public String getDescripcionmediotransporte() {
		return descripcionmediotransporte;
	}

	public void setDescripcionmediotransporte(String descripcionmediotransporte) {
		this.descripcionmediotransporte = descripcionmediotransporte;
	}

}
