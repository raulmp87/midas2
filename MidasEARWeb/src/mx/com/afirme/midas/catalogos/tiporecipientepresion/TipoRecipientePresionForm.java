package mx.com.afirme.midas.catalogos.tiporecipientepresion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoRecipientePresionForm extends MidasBaseForm{
	
	/**
	 *@fecha 01/08/2009
	 */
	
	private static final long serialVersionUID = 2320256725324751444L;
	
	private String idTipoRecipientePresion;
	private String descripcionTipoRecPresion;
	private String codigoTipoRecPresion;
	
	
	
	/**
	 * @param IdTipoRecipientePresion
	 *            the IdTipoRecipientePresion to set
	 */
	public void setIdTipoRecipientePresion(String idTipoRecipientePresion) {
		this.idTipoRecipientePresion = idTipoRecipientePresion;
	}
	/**
	 * @return the IdTipoRecipientePresion
	 */
	public String getIdTipoRecipientePresion() {
		return idTipoRecipientePresion;
	}
	
	/**
	 * @param DescripcionTipoRecPresion
	 *            the DescripcionTipoRecPresion to set
	 */
	public void setDescripcionTipoRecPresion(String descripcionTipoRecPresion) {
		this.descripcionTipoRecPresion = descripcionTipoRecPresion;
	}
	/**
	 * @return the DescripcionTipoRecPresion
	 */
	public String getDescripcionTipoRecPresion() {
		return descripcionTipoRecPresion;
	}
	
	/**
	 * @param codigoTipoRecPresion
	 *            the codigoTipoRecPresion to set
	 */
	public void setCodigoTipoRecPresion(String codigoTipoRecPresion) {
		this.codigoTipoRecPresion = codigoTipoRecPresion;
	}
	/**
	 * @return the codigoTipoRecPresion
	 */
	public String getCodigoTipoRecPresion() {
		return codigoTipoRecPresion;
	}
	
}
