package mx.com.afirme.midas.catalogos.tiporecipientepresion;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoRecipientePresionAction  extends MidasMappingDispatchAction{

	/**
	 *@fecha 01/08/2009
	 */
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
				
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos{

		TipoRecipientePresionDN tipoRecipientePresionDN = TipoRecipientePresionDN.getInstancia();
		List<TipoRecipientePresionDTO> tipoRecipientePresion = tipoRecipientePresionDN.listarTodos();
		request.setAttribute("tipoRecipientePresion", tipoRecipientePresion);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoRecipientePresionForm tipoRecipientePresionForm = (TipoRecipientePresionForm) form;
		TipoRecipientePresionDTO tipoRecipientePresionDTO = new TipoRecipientePresionDTO();
		TipoRecipientePresionDN tipoRecipientePresionDN = TipoRecipientePresionDN.getInstancia();
		
		try {
			poblarDTO(tipoRecipientePresionForm, tipoRecipientePresionDTO);
			request.setAttribute("tipoRecipientePresion", tipoRecipientePresionDN.listarFiltrado(tipoRecipientePresionDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoRecipientePresionForm tipoRecipientePresionForm = (TipoRecipientePresionForm) form;
		TipoRecipientePresionDTO tipoRecipientePresionDTO = new TipoRecipientePresionDTO();
		TipoRecipientePresionDN tipoRecipientePresionDN = TipoRecipientePresionDN.getInstancia();
		try {
			poblarDTO(tipoRecipientePresionForm, tipoRecipientePresionDTO);
			tipoRecipientePresionDN.agregar(tipoRecipientePresionDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoRecipientePresionForm tipoRecipientePresionForm = (TipoRecipientePresionForm) form;
		TipoRecipientePresionDTO tipoRecipientePresionDTO = new TipoRecipientePresionDTO();
		TipoRecipientePresionDN tipoRecipientePresionDN = TipoRecipientePresionDN.getInstancia();
		try {
			poblarDTO(tipoRecipientePresionForm, tipoRecipientePresionDTO);
			tipoRecipientePresionDTO = tipoRecipientePresionDN.getTipoRecPresionPorId(tipoRecipientePresionDTO);
			tipoRecipientePresionDN.borrar(tipoRecipientePresionDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoRecipientePresionForm tipoRecipientePresionForm = (TipoRecipientePresionForm) form;
		TipoRecipientePresionDN tipoRecipientePresionDN = TipoRecipientePresionDN.getInstancia();
		TipoRecipientePresionDTO tipoRecipientePresionDTO = new TipoRecipientePresionDTO();
		try {
			poblarDTO(tipoRecipientePresionForm, tipoRecipientePresionDTO);
			tipoRecipientePresionDTO = tipoRecipientePresionDN.getTipoRecPresionPorId(tipoRecipientePresionDTO);
			poblarDTO(tipoRecipientePresionForm, tipoRecipientePresionDTO);
			tipoRecipientePresionDN.modificar(tipoRecipientePresionDTO);
			listarTodos(request);
			limpiarForm(tipoRecipientePresionForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		TipoRecipientePresionDTO tipoRecipientePresionDTO = new TipoRecipientePresionDTO();
		TipoRecipientePresionForm tipoRecipientePresionForm = (TipoRecipientePresionForm) form;
		TipoRecipientePresionDN tipoRecipientePresionDN = TipoRecipientePresionDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoRecipientePresionDTO.setIdTipoRecipientePresion(UtileriasWeb.regresaBigDecimal(id));
			tipoRecipientePresionDTO = tipoRecipientePresionDN.getTipoRecPresionPorId(tipoRecipientePresionDTO);
			poblarForm(tipoRecipientePresionForm, tipoRecipientePresionDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void poblarForm(TipoRecipientePresionForm tipoRecipientePresionForm,
			TipoRecipientePresionDTO tipoRecipientePresionDTO) throws SystemException {
		
		tipoRecipientePresionForm.setIdTipoRecipientePresion(tipoRecipientePresionDTO.getIdTipoRecipientePresion().toBigInteger().toString());
		tipoRecipientePresionForm.setCodigoTipoRecPresion(tipoRecipientePresionDTO.getCodigoTipoRecipientePresion().toBigInteger().toString());
		tipoRecipientePresionForm.setDescripcionTipoRecPresion(tipoRecipientePresionDTO.getDescripcionTipoRecPresion());
	}

	private void poblarDTO(TipoRecipientePresionForm tipoRecipientePresionForm,
			TipoRecipientePresionDTO tipoRecipientePresionDTO) throws SystemException {

		if (!UtileriasWeb.esCadenaVacia(tipoRecipientePresionForm.getIdTipoRecipientePresion()))
		tipoRecipientePresionDTO.setIdTipoRecipientePresion(UtileriasWeb.regresaBigDecimal(tipoRecipientePresionForm.getIdTipoRecipientePresion()));
	 
		if (!UtileriasWeb.esCadenaVacia(tipoRecipientePresionForm.getCodigoTipoRecPresion()))
			tipoRecipientePresionDTO.setCodigoTipoRecipientePresion(UtileriasWeb.regresaBigDecimal(tipoRecipientePresionForm.getCodigoTipoRecPresion()));
		
		if (!UtileriasWeb.esCadenaVacia(tipoRecipientePresionForm.getDescripcionTipoRecPresion()))
			tipoRecipientePresionDTO.setDescripcionTipoRecPresion(tipoRecipientePresionForm.getDescripcionTipoRecPresion().trim().toUpperCase());
			
	}
	
	private void limpiarForm(TipoRecipientePresionForm tipoRecipientePresionForm) {
		tipoRecipientePresionForm.setCodigoTipoRecPresion("");
		tipoRecipientePresionForm.setDescripcionTipoRecPresion("");
		
	}
}
