package mx.com.afirme.midas.catalogos.tiporecipientepresion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoRecipientePresionDN {

	/**
	 *@fecha 01/08/2009
	 */
	
	public static final TipoRecipientePresionDN INSTANCIA = new TipoRecipientePresionDN();
	
	public static TipoRecipientePresionDN getInstancia() {
		
		return TipoRecipientePresionDN.INSTANCIA;
	}

	public List<TipoRecipientePresionDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoRecipientePresionSN tipoRecipientePresionSN = new TipoRecipientePresionSN();
		return tipoRecipientePresionSN.listarTodos();
	}

	public List<TipoRecipientePresionDTO> listarFiltrado(TipoRecipientePresionDTO tipoRecipientePresionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoRecipientePresionSN tipoRecipientePresionSN = new TipoRecipientePresionSN();
		return tipoRecipientePresionSN.listarFiltrado(tipoRecipientePresionDTO);
	}

	public void agregar(TipoRecipientePresionDTO tipoRecipientePresionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoRecipientePresionSN tipoRecipientePresionSN = new TipoRecipientePresionSN();
		tipoRecipientePresionSN.agregar(tipoRecipientePresionDTO);
	}

	public TipoRecipientePresionDTO getTipoRecPresionPorId(TipoRecipientePresionDTO tipoRecipientePresionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {

		TipoRecipientePresionSN tipoRecipientePresionSN = new TipoRecipientePresionSN();
		return tipoRecipientePresionSN.getTipoRecPresionPorId(tipoRecipientePresionDTO);
	}

	public String borrar(TipoRecipientePresionDTO tipoRecipientePresionDTO)
			throws ExcepcionDeAccesoADatos, SystemException{
	
		TipoRecipientePresionSN tipoRecipientePresionSN = new TipoRecipientePresionSN();
		return tipoRecipientePresionSN.borrar(tipoRecipientePresionDTO);
		
	}

	public String modificar(TipoRecipientePresionDTO tipoRecipientePresionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoRecipientePresionSN tipoRecipientePresionSN = new TipoRecipientePresionSN();
		return tipoRecipientePresionSN.modificar(tipoRecipientePresionDTO);
	}

}
