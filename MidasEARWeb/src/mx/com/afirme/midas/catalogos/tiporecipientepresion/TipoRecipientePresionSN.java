package mx.com.afirme.midas.catalogos.tiporecipientepresion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoRecipientePresionSN {

	/**
	 *@fecha 01/08/2009
	 */
	
	private TipoRecipientePresionFacadeRemote beanRemoto;

	public TipoRecipientePresionSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoRecipientePresionFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoRecipientePresionDTO tipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(tipoRecipientePresionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoRecipientePresionDTO tipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(tipoRecipientePresionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(TipoRecipientePresionDTO tipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(tipoRecipientePresionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<TipoRecipientePresionDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoRecipientePresionDTO getTipoRecPresionPorId(TipoRecipientePresionDTO tipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(tipoRecipientePresionDTO.getIdTipoRecipientePresion());
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoRecipientePresionDTO> listarFiltrado(TipoRecipientePresionDTO tipoRecipientePresionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(tipoRecipientePresionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}



}
