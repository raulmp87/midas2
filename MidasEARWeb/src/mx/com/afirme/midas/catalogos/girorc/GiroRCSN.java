package mx.com.afirme.midas.catalogos.girorc;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroRCSN {
	
	private GiroRCFacadeRemote beanRemoto;

	public GiroRCSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(GiroRCFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(giroRCDTO);
		}catch(EJBTransactionRolledbackException e){
			System.out.println("EXCEPCION GENERADA ::::  ".concat(e.getCausedByException().toString()));
		}
		return null;
	}
	
	public String borrar(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.delete(giroRCDTO);
		return null;
	}
	
	public String modificar(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.update(giroRCDTO);
		return null;
	}
	
	public List<GiroRCDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		return beanRemoto.findAll();
	}
	
	public GiroRCDTO getGiroRCPorId(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findById(giroRCDTO.getIdTcGiroRC());
	}
	
	public List<GiroRCDTO> listarFiltrado(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.listarFiltrado(giroRCDTO);
	}
	
	public EntityTransaction getTransaccion(){
		return beanRemoto.getTransaction();
	}
}
