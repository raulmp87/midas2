package mx.com.afirme.midas.catalogos.girorc;

import java.util.List;

import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Esteban Cabezudo
 * @since 13/08/2009
 */
public class GiroRCDN {

	public static final GiroRCDN INSTANCIA = new GiroRCDN();

	public static GiroRCDN getInstancia() {
		return GiroRCDN.INSTANCIA;
	}

	public String agregar(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.agregar(giroRCDTO);
	}

	public String borrar(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.borrar(giroRCDTO);
	}

	public String modificar(GiroRCDTO giroRCDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.modificar(giroRCDTO);
	}

	public GiroRCDTO getGiroRCPorId(GiroRCDTO giroRCDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.getGiroRCPorId(giroRCDTO);
	}

	public List<GiroRCDTO> listarTodos() throws ExcepcionDeAccesoADatos,
			SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.listarTodos();
	}

	public List<GiroRCDTO> listarFiltrado(GiroRCDTO giroRCDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.listarFiltrado(giroRCDTO);
	}

	public EntityTransaction getTransaccion() throws SystemException {
		GiroRCSN giroRCSN = new GiroRCSN();
		return giroRCSN.getTransaccion();
	}
}
