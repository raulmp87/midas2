package mx.com.afirme.midas.catalogos.girorc;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroRCForm extends MidasBaseForm {
	private static final long serialVersionUID = -4426430534960591954L;

	private String idTcGiroRC;
	private String codigoGiroRC;
	private String descripcionGiroRC;

	public String getIdTcGiroRC() {
		return idTcGiroRC;
	}

	public void setIdTcGiroRC(String idTcGiroRC) {
		this.idTcGiroRC = idTcGiroRC;
	}

	public String getCodigoGiroRC() {
		return codigoGiroRC;
	}

	public void setCodigoGiroRC(String codigoGiroRC) {
		this.codigoGiroRC = codigoGiroRC;
	}

	public String getDescripcionGiroRC() {
		return descripcionGiroRC;
	}

	public void setDescripcionGiroRC(String descripcionGiroRC) {
		this.descripcionGiroRC = descripcionGiroRC;
	}
}
