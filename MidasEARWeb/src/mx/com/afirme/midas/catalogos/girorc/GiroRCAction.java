package mx.com.afirme.midas.catalogos.girorc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Esteban Cabezudo
 * @since 06/08/2009
 */
public class GiroRCAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		GiroRCDN giroRCDN = GiroRCDN.getInstancia();
		List<GiroRCDTO> girosRC = giroRCDN.listarTodos();
		request.setAttribute("girosRC", girosRC);
	}

	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroRCForm giroRCForm = (GiroRCForm) form;
		GiroRCDTO giroRCDTO = new GiroRCDTO();
		GiroRCDN giroRCDN = GiroRCDN.getInstancia();
		try {
			poblarDTO(giroRCForm, giroRCDTO);
			request.setAttribute("girosRC", giroRCDN.listarFiltrado(giroRCDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(GiroRCForm giroRCForm, GiroRCDTO giroRCDTO)
			throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(giroRCForm.getIdTcGiroRC())) {
			giroRCDTO.setIdTcGiroRC(UtileriasWeb.regresaBigDecimal(giroRCForm.getIdTcGiroRC()));
		}
		if (!UtileriasWeb.esCadenaVacia(giroRCForm.getCodigoGiroRC())) {
			giroRCDTO.setCodigoGiroRC(UtileriasWeb.regresaBigDecimal(giroRCForm.getCodigoGiroRC()));
		}
		if (!UtileriasWeb.esCadenaVacia(giroRCForm.getDescripcionGiroRC())) {
			giroRCDTO.setDescripcionGiroRC(giroRCForm.getDescripcionGiroRC());
		}
	}

	private void poblarForm(GiroRCForm giroRCForm, GiroRCDTO giroRCDTO)
			throws SystemException {
		giroRCForm.setIdTcGiroRC(giroRCDTO.getIdTcGiroRC().toBigInteger().toString());
		giroRCForm.setCodigoGiroRC(giroRCDTO.getCodigoGiroRC().toBigInteger().toString());
		giroRCForm.setDescripcionGiroRC(giroRCDTO.getDescripcionGiroRC());
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroRCForm giroRCForm = (GiroRCForm) form;
		GiroRCDTO giroRCDTO = new GiroRCDTO();
		GiroRCDN giroRCDN = GiroRCDN.getInstancia();
		try {
			poblarDTO(giroRCForm, giroRCDTO);
			giroRCDN.agregar(giroRCDTO);
			listarTodos(request);
			limpiarForm(giroRCForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (Exception e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void limpiarForm(GiroRCForm form) {
		form.setIdTcGiroRC("");
		form.setDescripcionGiroRC("");
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroRCForm giroRCForm = (GiroRCForm) form;
		GiroRCDN giroRCDN = GiroRCDN.getInstancia();
		GiroRCDTO giroRCDTO = new GiroRCDTO();
		try {
			poblarDTO(giroRCForm, giroRCDTO);
			giroRCDTO = giroRCDN.getGiroRCPorId(giroRCDTO);
			poblarDTO(giroRCForm, giroRCDTO);
			giroRCDN.modificar(giroRCDTO);
			listarTodos(request);
			limpiarForm(giroRCForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroRCForm giroRCForm = (GiroRCForm) form;
		GiroRCDTO giroRCDTO = new GiroRCDTO();
		GiroRCDN giroRCDN = GiroRCDN.getInstancia();
		try {
			giroRCDTO.setIdTcGiroRC(UtileriasWeb.regresaBigDecimal(giroRCForm.getIdTcGiroRC()));
			giroRCDTO = giroRCDN.getGiroRCPorId(giroRCDTO);
			giroRCDN.borrar(giroRCDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		GiroRCDTO giroRCDTO = new GiroRCDTO();
		GiroRCForm giroRCForm = (GiroRCForm) form;
		GiroRCDN giroRCDN = GiroRCDN.getInstancia();
		try {
			String id = request.getParameter("id");
			giroRCDTO.setIdTcGiroRC(UtileriasWeb.regresaBigDecimal(id));
			giroRCDTO = giroRCDN.getGiroRCPorId(giroRCDTO);
			poblarForm(giroRCForm, giroRCDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
