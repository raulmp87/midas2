package mx.com.afirme.midas.catalogos.colonia;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADTO;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAId;
import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroDN;
import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroId;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDN;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoId;
import mx.com.afirme.midas.catalogos.zonahidro.ZonaHidroDN;
import mx.com.afirme.midas.catalogos.zonahidro.ZonaHidroDTO;
import mx.com.afirme.midas.catalogos.zonasismo.ZonaSismoDN;
import mx.com.afirme.midas.catalogos.zonasismo.ZonaSismoDTO;
import mx.com.afirme.midas.interfaz.codigopostalcolonia.CodigoPostalColoniaDN;
import mx.com.afirme.midas.interfaz.codigopostalcolonia.CodigoPostalColoniaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ColoniaAction extends MidasMappingDispatchAction{
    
    
    
    public ActionForward listarColonias(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response){
	
    	return listarFiltrado(mapping, form, request, response);
	
    }

     
    
    public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
    		HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		try{
			ColoniaForm coloniaForm=(ColoniaForm)form;
	       	ColoniaDTO coloniaDTO= new ColoniaDTO();
	       	poblarDTO(coloniaForm, coloniaDTO);
	    	Long totalRegistros = ColoniaDN.getInstancia().obtenerTotalFiltrado(coloniaDTO);
	    	coloniaForm.setTotalRegistros(totalRegistros.toString()); 
			
			return listarFiltradoPaginado(mapping, form, request, response);
	        	
		}catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
    }
    
    
    public ActionForward listarFiltradoPaginado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			
			if (!listaEnCache(form, request)) {
				
				ColoniaForm coloniaForm = (ColoniaForm) form;
				ColoniaDTO coloniaDTO = new ColoniaDTO();
				poblarDTO(coloniaForm, coloniaDTO);
				
				coloniaDTO.setPrimerRegistroACargar(coloniaForm.getPrimerRegistroACargar());
				coloniaDTO.setNumeroMaximoRegistrosACargar(coloniaForm.getNumeroMaximoRegistrosACargar());
				coloniaForm.setListaPaginada(ColoniaDN.getInstancia().listarFiltrado(coloniaDTO), request);
			
			} 
			
			request.setAttribute("listaColonias", obtieneListaAMostrar(form, request));
		
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
    
    private void poblarDTO(ColoniaForm coloniaForm, ColoniaDTO coloniaDTO) throws SystemException, ExcepcionDeAccesoADatos {
    	
    	if (!UtileriasWeb.esCadenaVacia(coloniaForm.getCodigoPostal()))
    		coloniaDTO.setZipCode(coloniaForm.getCodigoPostal());
    	
    	
    	if (!UtileriasWeb.esCadenaVacia(coloniaForm.getDescripcionColonia()))
    		coloniaDTO.setColonyName(coloniaForm.getDescripcionColonia());
    }
    
    
    public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,
	HttpServletRequest request, HttpServletResponse response){
	
	String idColonia= request.getParameter("idColonia");
	ColoniaForm coloniaForm=(ColoniaForm)form;
	String reglaNavegacion = Sistema.EXITOSO;
	try{
	if(!UtileriasWeb.esCadenaVacia(idColonia)){
	      ColoniaDTO coloniaDTO= ColoniaDN.getInstancia().getColoniaPorId(idColonia);
	      if(!UtileriasWeb.esObjetoNulo(coloniaDTO)){
		  poblarForm(coloniaForm, coloniaDTO);
	      }else{
		  reglaNavegacion = Sistema.NO_EXITOSO;
		  UtileriasWeb.mandaMensajeExcepcionRegistrado("No se pudo obtener una instancia para colonia", request); 
	      }
	
	 }
	} catch (SystemException e) {
		reglaNavegacion = Sistema.NO_DISPONIBLE;
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
	}catch (ExcepcionDeAccesoADatos e) {
		reglaNavegacion = Sistema.NO_EXITOSO;
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
	}
	
	return mapping.findForward(reglaNavegacion);
    
    }
    
       
    
    public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response){
		
		return  mapping.findForward(Sistema.EXITOSO);
		
	    
    }
    
    
     public void modificar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response){
		
		ColoniaForm coloniaForm=(ColoniaForm)form;
		int valorDevuelto=0;
		try{
		    CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO= new CodigoPostalZonaHidroDTO();
		    CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO= new CodigoPostalZonaSismoDTO();
		    CodigoPostalColoniaDTO codigoPostalColoniaDTO= new CodigoPostalColoniaDTO();
		    CodigoPostalIVADTO codigoPostalIVADTO = new CodigoPostalIVADTO();
		    this.poblarDTOs(codigoPostalZonaHidroDTO,codigoPostalZonaSismoDTO,coloniaForm, 
			    codigoPostalColoniaDTO, codigoPostalIVADTO);
		    
		    if(!(UtileriasWeb.esObjetoNulo(codigoPostalColoniaDTO))&&
			    !(UtileriasWeb.esObjetoNulo(codigoPostalZonaHidroDTO))&&
			    !(UtileriasWeb.esObjetoNulo(codigoPostalZonaSismoDTO))){
			    valorDevuelto=CodigoPostalColoniaDN.getInstancia().modificarColoniaCodigoPostalZonaHidroZonaSismo
			    (codigoPostalColoniaDTO, codigoPostalZonaHidroDTO,codigoPostalZonaSismoDTO, UtileriasWeb.obtieneNombreUsuario(request));
			    switch (valorDevuelto) {
			    	case 0:
			    		if(!UtileriasWeb.esObjetoNulo(codigoPostalIVADTO)) {
					    	CodigoPostalIVADN.getInstancia().modificar(codigoPostalIVADTO);
					    }
			    		UtileriasWeb.imprimeMensajeXML(Sistema.EXITO, "Los datos de la colonia se actualizaron correctamente.", response);
			    		break;
			    	case 1:
			    		UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al actualizar la colonia,en la Base de Datos.", response);
			    		break;
			    	case 2:
			    		UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al actualizar los datos de codigos postales por zonas", response);
			    		break;
			    	case 3:
			    		UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "La colonia ya existe.", response);
			    		break;
			    	default:
			    		break;
			    }
		    }
		    
		} catch (SystemException e) {
			UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al actualizar la colonia.", response);
			
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al actualizar la colonia.", response);
			
		}catch (Throwable e) {
		    UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al actualizar la colonia.", response);
		}
		
		
	    
	  }
     
     
     public void agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response){
		int valorDevuelto=0;
		ColoniaForm coloniaForm=(ColoniaForm)form;
		
		try{
		    CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO= new CodigoPostalZonaHidroDTO();
		    CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO= new CodigoPostalZonaSismoDTO();
		    CodigoPostalColoniaDTO codigoPostalColoniaDTO= new CodigoPostalColoniaDTO();
		    CodigoPostalIVADTO codigoPostalIVADTO = new CodigoPostalIVADTO();
		    this.poblarDTOs(codigoPostalZonaHidroDTO,codigoPostalZonaSismoDTO,coloniaForm, 
			    codigoPostalColoniaDTO, codigoPostalIVADTO);
		    
		    if(!(UtileriasWeb.esObjetoNulo(codigoPostalColoniaDTO))&&
			    !(UtileriasWeb.esObjetoNulo(codigoPostalZonaHidroDTO))&&
			    !(UtileriasWeb.esObjetoNulo(codigoPostalZonaSismoDTO))){
		    	valorDevuelto=CodigoPostalColoniaDN.getInstancia().agregarColoniaCodigoPostalZonaHidroZonaSismo
			    (codigoPostalColoniaDTO, codigoPostalZonaHidroDTO,codigoPostalZonaSismoDTO, UtileriasWeb.obtieneNombreUsuario(request));
			    switch (valorDevuelto) {
				    case 0:
				    	if(!UtileriasWeb.esObjetoNulo(codigoPostalIVADTO)) {
				    		CodigoPostalIVADN.getInstancia().agregar(codigoPostalIVADTO);
				    	}
				    	UtileriasWeb.imprimeMensajeXML(Sistema.EXITO, "Los datos de la colonia se agregaron correctamente.", response);
				    	break;
				    case 1:
				    	UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al guardar la colonia,en la Base de Datos.", response);
				    	break;
				    case 2:
				    	UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al guardar los datos de codigos postales por zonas.", response);
				    	break;
				    case 3:
						UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "La colonia ya existe.", response);
						break;
				    default:
				    	break;
			    }
		    }
		    
		} catch (SystemException e) {
			UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al agregar la colonia.", response);
		}catch (ExcepcionDeAccesoADatos e) {
		    UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al agregar la colonia.", response);
		
		}catch (Throwable e) {
	          UtileriasWeb.imprimeMensajeXML(Sistema.ERROR, "Ocurrio un error al agregar la colonia.", response);
		}
		
	  }
    
    
    private void poblarForm(ColoniaForm coloniaForm, ColoniaDTO coloniaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO=null;
		CodigoPostalZonaHidroId codigoPostalZonaHidroId=null;
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO=null;
		CodigoPostalZonaSismoId codigoPostalZonaSismoId=null;
		codigoPostalZonaHidroId  = new CodigoPostalZonaHidroId();
		codigoPostalZonaSismoId= new CodigoPostalZonaSismoId();
		
		if(!UtileriasWeb.esCadenaVacia(coloniaDTO.getColonyId())){
		    coloniaForm.setIdColonia(coloniaDTO.getColonyId());
		    codigoPostalZonaHidroId.setNombreColonia(coloniaDTO.getColonyId());
		    codigoPostalZonaSismoId.setNombreColonia(coloniaDTO.getColonyId());
		}
		
		if(!UtileriasWeb.esCadenaVacia(coloniaDTO.getZipCode())){
		    coloniaForm.setCodigoPostal(coloniaDTO.getZipCode());
		    codigoPostalZonaHidroId.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaDTO.getZipCode()));
		    codigoPostalZonaSismoId.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaDTO.getZipCode()));
		}
		
		if(!UtileriasWeb.esCadenaVacia(coloniaDTO.getColonyName())){
		    coloniaForm.setDescripcionColonia(coloniaDTO.getColonyName());
		}
		
		codigoPostalZonaHidroDTO= CodigoPostalZonaHidroDN.getInstancia().getPorId(codigoPostalZonaHidroId) ;
		if(!UtileriasWeb.esObjetoNulo(codigoPostalZonaHidroDTO)){
		    coloniaForm.setIdZonaHidro(codigoPostalZonaHidroDTO.getZonaHidro().getIdTcZonaHidro().toString());
		}
		 codigoPostalZonaSismoDTO= CodigoPostalZonaSismoDN.getInstancia().getPorId(codigoPostalZonaSismoId);
		if(!UtileriasWeb.esObjetoNulo(codigoPostalZonaSismoDTO)){
		    coloniaForm.setIdZonaSismo(codigoPostalZonaSismoDTO.getZonaSismoDTO().getIdZonaSismo().toString());
		}
		
		if(coloniaDTO.getZipCodeUserId()!=null){
		    coloniaForm.setZipCodeUserId(String.valueOf(coloniaDTO.getZipCodeUserId()));
		}

		if(!UtileriasWeb.esCadenaVacia(coloniaDTO.getColonyId()) && !UtileriasWeb.esCadenaVacia(coloniaDTO.getZipCode())) {
			CodigoPostalIVAId id = new CodigoPostalIVAId();
			id.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaDTO.getZipCode()));
			id.setNombreColonia(coloniaDTO.getColonyId());
			CodigoPostalIVADTO codigoPostalIVADTO = CodigoPostalIVADN.getInstancia().getPorId(id);
			if(codigoPostalIVADTO != null) {
				coloniaForm.setValorIVA(codigoPostalIVADTO.getValorIVA().toString());
			}
		}
    }
    
    private void poblarDTOs(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO,
			CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO,
			ColoniaForm coloniaForm,
			CodigoPostalColoniaDTO codigoPostalColoniaDTO,
			CodigoPostalIVADTO codigoPostalIVADTO) throws SystemException {
		CodigoPostalZonaHidroId codigoPostalZonaHidroId;
		CodigoPostalZonaSismoId codigoPostalZonaSismoId;
		ZonaHidroDTO zonaHidroDTO;
		ZonaSismoDTO zonaSismoDTO;
		
		if(!UtileriasWeb.esCadenaVacia(coloniaForm.getIdColonia())
			&& !UtileriasWeb.esCadenaVacia(coloniaForm.getCodigoPostal())){
		    codigoPostalZonaHidroId= new CodigoPostalZonaHidroId();
		    codigoPostalZonaHidroId.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaForm.getCodigoPostal().trim()));
		    codigoPostalZonaHidroId.setNombreColonia(coloniaForm.getIdColonia());
		    codigoPostalZonaHidroDTO.setId(codigoPostalZonaHidroId);
		    
		    codigoPostalZonaSismoId= new CodigoPostalZonaSismoId();
		    codigoPostalZonaSismoId.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaForm.getCodigoPostal().trim()));
		    codigoPostalZonaSismoId.setNombreColonia(coloniaForm.getIdColonia());
		    codigoPostalZonaSismoDTO.setId(codigoPostalZonaSismoId);
		    
		    codigoPostalColoniaDTO.setCodigoPostal(coloniaForm.getCodigoPostal());
		}
		
		if(!UtileriasWeb.esCadenaVacia(coloniaForm.getZipCodeUserId())){
		    codigoPostalColoniaDTO.setIdColonia(Short.valueOf(coloniaForm.getZipCodeUserId()));
		}
		
		if(!UtileriasWeb.esCadenaVacia(coloniaForm.getDescripcionColonia())){
		    codigoPostalColoniaDTO.setNombreColonia(coloniaForm.getDescripcionColonia());
		}
		
		if(!UtileriasWeb.esCadenaVacia(coloniaForm.getIdZonaHidro())){
		    zonaHidroDTO= new ZonaHidroDTO();
		    zonaHidroDTO.setIdTcZonaHidro(UtileriasWeb.regresaBigDecimal(coloniaForm.getIdZonaHidro()));
		    zonaHidroDTO= ZonaHidroDN.getInstancia().getPorId(zonaHidroDTO);
		    if(!UtileriasWeb.esObjetoNulo(zonaHidroDTO)){
			codigoPostalZonaHidroDTO.setZonaHidro(zonaHidroDTO);
		    }
		}
		
		if(!UtileriasWeb.esCadenaVacia(coloniaForm.getIdZonaSismo())){
		    zonaSismoDTO= new ZonaSismoDTO();
		    zonaSismoDTO.setIdZonaSismo(UtileriasWeb.regresaBigDecimal(coloniaForm.getIdZonaSismo()));
		    zonaSismoDTO= ZonaSismoDN.getInstancia().getZonaSismoPorId(zonaSismoDTO);
		    if(!UtileriasWeb.esObjetoNulo(zonaSismoDTO)){
			codigoPostalZonaSismoDTO.setZonaSismoDTO(zonaSismoDTO);
		    }
		}

		if(!UtileriasWeb.esCadenaVacia(coloniaForm.getIdColonia()) && !UtileriasWeb.esCadenaVacia(coloniaForm.getCodigoPostal()) && !UtileriasWeb.esCadenaVacia(coloniaForm.getValorIVA())) {
			CodigoPostalIVAId id = new CodigoPostalIVAId();
			id.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaForm.getCodigoPostal().trim()));
		    id.setNombreColonia(coloniaForm.getIdColonia());
			codigoPostalIVADTO.setId(id);
			codigoPostalIVADTO.setValorIVA(Double.parseDouble(coloniaForm.getValorIVA()));
		}
    }
    
	public void validaCodigoPostal(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String codigoPostal = request.getParameter("codPostal");
		List<CodigoPostalColoniaDTO> listaColonias;
		short idMaximoColonia = 0;
		StringBuffer buffer = new StringBuffer();
		buffer
				.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response>");
		if (!UtileriasWeb.esCadenaVacia(codigoPostal)) {

			try {
				listaColonias = CodigoPostalColoniaDN.getInstancia()
						.listaColonias(codigoPostal,
								UtileriasWeb.obtieneNombreUsuario(request));

				if (listaColonias != null && !listaColonias.isEmpty()) {
					CodigoPostalZonaHidroId codigoPostalZonaHidroId  = new CodigoPostalZonaHidroId();
					CodigoPostalZonaSismoId codigoPostalZonaSismoId= new CodigoPostalZonaSismoId();		
					
					for (CodigoPostalColoniaDTO colonia : listaColonias) {
						if (colonia.getIdColonia().compareTo(idMaximoColonia) > 0) {
							idMaximoColonia = colonia.getIdColonia();
						}
					}
					ColoniaDTO coloniaDTO= ColoniaDN.getInstancia().getColoniaPorId(codigoPostal +"-"+idMaximoColonia);
					codigoPostalZonaHidroId.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaDTO.getZipCode().trim()));
					codigoPostalZonaSismoId.setCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaDTO.getZipCode().trim()));		
				    codigoPostalZonaHidroId.setNombreColonia(coloniaDTO.getColonyId());
				    codigoPostalZonaSismoId.setNombreColonia(coloniaDTO.getColonyId());					
					CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO = CodigoPostalZonaHidroDN.getInstancia().getPorId(codigoPostalZonaHidroId);
					CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO = CodigoPostalZonaSismoDN.getInstancia().getPorId(codigoPostalZonaSismoId);
					List<CodigoPostalIVADTO> iva = CodigoPostalIVADN.getInstancia().listarPorCodigoPostal(UtileriasWeb.regresaBigDecimal(coloniaDTO.getZipCode().trim()));
					
					idMaximoColonia = (short) (idMaximoColonia + Short
							.valueOf("1"));
					buffer.append("<item>");
					buffer
							.append("<codPostal>" + codigoPostal
									+ "</codPostal>");
					buffer.append("<idAsignar>" + idMaximoColonia
							+ "</idAsignar>");
					buffer.append("<idZonaHidro>" + codigoPostalZonaHidroDTO.getZonaHidro().getIdTcZonaHidro()
							+ "</idZonaHidro>");
					buffer.append("<idZonaSismo>" + codigoPostalZonaSismoDTO.getZonaSismoDTO().getIdZonaSismo()
							+ "</idZonaSismo>");
					if(!iva.isEmpty()) {
						buffer.append("<valorIVA>" + iva.get(0).getValorIVA() + "</valorIVA>");
					}
					buffer.append("</item>");

				}

			} catch (SystemException e) {
				e.printStackTrace();
			}

		}
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());

		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
    
    
    
    

}
