package mx.com.afirme.midas.catalogos.colonia;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ColoniaForm extends MidasBaseForm {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String codigoPostal;
    private String idColonia;
    private String zipCodeUserId;
    private String descripcionColonia;
    private String idZonaSismo;
    private String idZonaHidro;
    private String valorIVA;
    
    public void setCodigoPostal(String codigoPostal) {
	this.codigoPostal = codigoPostal;
    }
    public String getCodigoPostal() {
	return codigoPostal;
    }
    public void setIdColonia(String idColonia) {
	this.idColonia = idColonia;
    }
    public String getIdColonia() {
	return idColonia;
    }
    public void setDescripcionColonia(String descripcionColonia) {
	this.descripcionColonia = descripcionColonia;
    }
    public String getDescripcionColonia() {
	return descripcionColonia;
    }
    public void setIdZonaSismo(String idZonaSismo) {
	this.idZonaSismo = idZonaSismo;
    }
    public String getIdZonaSismo() {
	return idZonaSismo;
    }
    public void setIdZonaHidro(String idZonaHidro) {
	this.idZonaHidro = idZonaHidro;
    }
    public String getIdZonaHidro() {
	return idZonaHidro;
    }
    public void setZipCodeUserId(String zipCodeUserId) {
	this.zipCodeUserId = zipCodeUserId;
    }
    public String getZipCodeUserId() {
	return zipCodeUserId;
    }
	public void setValorIVA(String valorIVA) {
		this.valorIVA = valorIVA;
	}
	public String getValorIVA() {
		return valorIVA;
	}
}
