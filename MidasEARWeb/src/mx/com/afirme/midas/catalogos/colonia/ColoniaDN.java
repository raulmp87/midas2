package mx.com.afirme.midas.catalogos.colonia;

import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ColoniaDN {
    private static final ColoniaDN INSTANCIA = new ColoniaDN();

	public static ColoniaDN getInstancia() {
		return ColoniaDN.INSTANCIA;
	}
    
    public ColoniaDTO getColoniaPorId(String id) throws ExcepcionDeAccesoADatos{
	try {
		return new ColoniaSN().getColoniaPorId(id);
	} catch (SystemException e) {
		e.printStackTrace();
		throw new ExcepcionDeAccesoADatos("Error al obtener colonia por id.");
	}
    }	
    
    
   public List<ColoniaDTO> listarColoniaTodos() throws ExcepcionDeAccesoADatos{
	try {
		return new ColoniaSN().listarColoniaTodos();
	} catch (SystemException e) {
		e.printStackTrace();
		throw new ExcepcionDeAccesoADatos("Error al listar todas las colonias");
	}
    }	
   
   public List<ColoniaDTO> listarFiltrado(ColoniaDTO coloniaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		return new ColoniaSN().listarFiltrado(coloniaDTO);
   }	
    
   public Long obtenerTotalFiltrado(ColoniaDTO coloniaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new ColoniaSN().obtenerTotalFiltrado(coloniaDTO);
   }

}
