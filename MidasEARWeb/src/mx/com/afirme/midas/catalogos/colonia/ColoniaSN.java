package mx.com.afirme.midas.catalogos.colonia;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ColoniaSN {
        private ColoniaFacadeRemote beanRemotoColonia;

	public ColoniaSN() throws SystemException {
		try {
			LogDeMidasWeb.log("Entrando en ColoniaSN - Constructor",
					Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemotoColonia = serviceLocator.getEJB(ColoniaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public ColoniaDTO getColoniaPorId(String id) throws ExcepcionDeAccesoADatos {		
		return beanRemotoColonia.findById(id);
	}
	
	public List<ColoniaDTO> listarColoniaTodos() throws ExcepcionDeAccesoADatos {
		List<ColoniaDTO> colonias = beanRemotoColonia.findAll();
		return colonias;

	}
	
	
	public List<ColoniaDTO> listarFiltrado(ColoniaDTO coloniaDTO) throws ExcepcionDeAccesoADatos {
		try {
			List<ColoniaDTO> colonias = beanRemotoColonia.listarFiltrado(coloniaDTO);
			return colonias;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al listarFiltrado de las colonias");
		}
	}
	
	public Long obtenerTotalFiltrado(ColoniaDTO coloniaDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemotoColonia.obtenerTotalFiltrado(coloniaDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalFiltrado de las colonias");
		}
		
	}
	
	
	
	
	
}
