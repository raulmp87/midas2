package mx.com.afirme.midas.catalogos.tiponavegacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoNavegacionDN {
	private static final TipoNavegacionDN INSTANCIA = new TipoNavegacionDN();

	public static TipoNavegacionDN getInstancia() {
		return TipoNavegacionDN.INSTANCIA;
	}

	public List<TipoNavegacionDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoNavegacionSN tipoNavegacionSN = new TipoNavegacionSN();
		return tipoNavegacionSN.listarTodos();
	}
	
	public List<TipoNavegacionDTO> listarFiltrados(TipoNavegacionDTO tipoNavegacionDTO)
		throws SystemException, ExcepcionDeAccesoADatos {
		TipoNavegacionSN tipoNavegacionSN = new TipoNavegacionSN();
		return tipoNavegacionSN.listarFiltrados(tipoNavegacionDTO);
	}

	public TipoNavegacionDTO getPorId(TipoNavegacionDTO tipoNavegacionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		TipoNavegacionSN tipoNavegacionSN = new TipoNavegacionSN();
		return tipoNavegacionSN.getPorId(tipoNavegacionDTO.getIdTcTipoNavegacion());
	}
}
