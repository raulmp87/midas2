/**
 * 
 */
package mx.com.afirme.midas.catalogos.tiponavegacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class TipoNavegacionSN {
	private TipoNavegacionFacadeRemote beanRemoto;

	public TipoNavegacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoNavegacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<TipoNavegacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<TipoNavegacionDTO> tipoNavegaciones;
		try {
			tipoNavegaciones = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return tipoNavegaciones;

	}		
	
	public List<TipoNavegacionDTO> listarFiltrados(TipoNavegacionDTO tipoNavegacionDTO) throws ExcepcionDeAccesoADatos {
		List<TipoNavegacionDTO> tipoNavegaciones;
		try {
			tipoNavegaciones = beanRemoto.listarFiltrado(tipoNavegacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
		return tipoNavegaciones;
	}
	
	public TipoNavegacionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
	}
}
