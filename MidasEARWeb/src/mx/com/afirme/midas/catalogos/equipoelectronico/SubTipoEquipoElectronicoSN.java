package mx.com.afirme.midas.catalogos.equipoelectronico;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoEquipoElectronicoSN {
	private SubtipoEquipoElectronicoFacadeRemote beanRemoto;

	public SubTipoEquipoElectronicoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubtipoEquipoElectronicoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SubTipoEquipoElectronico instanciado", Level.FINEST, null);
	}
	
	public void agregar(SubtipoEquipoElectronicoDTO SubTipoEquipoElectronicoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(SubTipoEquipoElectronicoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(SubtipoEquipoElectronicoDTO SubTipoEquipoElectronicoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(SubTipoEquipoElectronicoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(SubtipoEquipoElectronicoDTO SubTipoEquipoElectronicoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(SubTipoEquipoElectronicoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
	public List<SubtipoEquipoElectronicoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubtipoEquipoElectronicoDTO getPorId(BigDecimal idTcSubTipoEquipoElectronico) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcSubTipoEquipoElectronico);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}
