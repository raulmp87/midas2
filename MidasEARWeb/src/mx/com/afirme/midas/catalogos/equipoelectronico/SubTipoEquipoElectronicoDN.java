package mx.com.afirme.midas.catalogos.equipoelectronico;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubTipoEquipoElectronicoDN {
	private static final SubTipoEquipoElectronicoDN INSTANCIA = new SubTipoEquipoElectronicoDN();

	public static SubTipoEquipoElectronicoDN getInstancia (){
		return SubTipoEquipoElectronicoDN.INSTANCIA;
	}
	
	public void agregar(SubtipoEquipoElectronicoDTO SubTipoEquipoElectronicoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubTipoEquipoElectronicoSN().agregar(SubTipoEquipoElectronicoDTO);
	}
	
	public void borrar (SubtipoEquipoElectronicoDTO SubTipoEquipoElectronicoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubTipoEquipoElectronicoSN().borrar(SubTipoEquipoElectronicoDTO);
	}
	
	public void modificar (SubtipoEquipoElectronicoDTO SubTipoEquipoElectronicoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubTipoEquipoElectronicoSN().modificar(SubTipoEquipoElectronicoDTO);
	}
	
	public SubtipoEquipoElectronicoDTO getPorId(BigDecimal idTcSubTipoEquipoElectronico) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubTipoEquipoElectronicoSN().getPorId(idTcSubTipoEquipoElectronico);
	}
	
	public List<SubtipoEquipoElectronicoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new SubTipoEquipoElectronicoSN().listarTodos();
	}
	
}
