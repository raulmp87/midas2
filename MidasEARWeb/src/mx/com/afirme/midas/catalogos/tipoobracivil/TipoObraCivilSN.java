package mx.com.afirme.midas.catalogos.tipoobracivil;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class TipoObraCivilSN {
	
	private TipoObraCivilFacadeRemote beanRemoto;

	public TipoObraCivilSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoObraCivilFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(tipoObraCivilDTO);
		}catch(EJBTransactionRolledbackException e){
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(tipoObraCivilDTO);
		}catch(EJBTransactionRolledbackException e){
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(tipoObraCivilDTO);
		}catch (EJBTransactionRolledbackException e) {
			e.printStackTrace();
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<TipoObraCivilDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.MENSAJE_EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoObraCivilDTO getTipoObraCivilPorId(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoObraCivilDTO.getIdTipoObraCivil());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.MENSAJE_EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoObraCivilDTO getTipoObraCivilPorId(BigDecimal idTipoObraCivil) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTipoObraCivil);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.MENSAJE_EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoObraCivilDTO> listarFiltrado(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoObraCivilDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.MENSAJE_EXCEPCION_OBTENER_DTO);
		}
	}
}
