package mx.com.afirme.midas.catalogos.tipoobracivil;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoObraCivilAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoObraCivilDN tipoObraCivilDN = TipoObraCivilDN.getInstancia();
		List<TipoObraCivilDTO> obras = tipoObraCivilDN.listarTodos();
		request.setAttribute("obras", obras);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoObraCivilForm tipoObraCivilForm = (TipoObraCivilForm) form;
		TipoObraCivilDTO tipoObraCivilDTO = new TipoObraCivilDTO();
		TipoObraCivilDN tipoObraCivilDN = TipoObraCivilDN.getInstancia();
		try {
			poblarDTO(tipoObraCivilForm, tipoObraCivilDTO);
			request.setAttribute("obras", tipoObraCivilDN.listarFiltrado(tipoObraCivilDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private String poblarDTO(TipoObraCivilForm tipoObraCivilForm,
			TipoObraCivilDTO tipoObraCivilDTO) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(tipoObraCivilForm.getClaveAutorizacion()))
			tipoObraCivilDTO.setClaveAutorizacion(UtileriasWeb.regresaShort(tipoObraCivilForm.getClaveAutorizacion()));
		if (!UtileriasWeb.esCadenaVacia(tipoObraCivilForm.getDescripcion()))
			tipoObraCivilDTO.setDescripcionTipoObraCivil(tipoObraCivilForm.getDescripcion().trim().toUpperCase());
		if (!UtileriasWeb.esCadenaVacia(tipoObraCivilForm.getIdObra()))
			tipoObraCivilDTO.setCodigoTipoObraCivil(UtileriasWeb.regresaBigDecimal(tipoObraCivilForm.getIdObra()));
		if (!UtileriasWeb.esCadenaVacia(tipoObraCivilForm.getIdTabla()))
			tipoObraCivilDTO.setIdTipoObraCivil(UtileriasWeb.regresaBigDecimal(tipoObraCivilForm.getIdTabla()));
		if (!UtileriasWeb.esCadenaVacia(tipoObraCivilForm.getPeriodoNormalConstruccion()))
			tipoObraCivilDTO.setIdPeriodoNormalConstruccion(UtileriasWeb.regresaBigDecimal(tipoObraCivilForm.getPeriodoNormalConstruccion()));
		return null;
	}
	
	private String poblarForm(TipoObraCivilForm tipoObraCivilForm,
			TipoObraCivilDTO tipoObraCivilDTO) throws SystemException {
		tipoObraCivilForm.setDescripcion(tipoObraCivilDTO.getDescripcionTipoObraCivil().toString());
		tipoObraCivilForm.setIdObra(tipoObraCivilDTO.getCodigoTipoObraCivil().toBigInteger().toString());
		tipoObraCivilForm.setIdTabla(tipoObraCivilDTO.getIdTipoObraCivil().toBigInteger().toString());
		String clave = tipoObraCivilDTO.getClaveAutorizacion().toString();
		tipoObraCivilForm.setClaveAutorizacion(clave);
		if (clave.equals("0"))
			clave = "NO REQUERIDA";
		if (clave.equals("1"))
			clave = "AREA TECNICA";
		if (clave.equals("2"))
			clave = "AREA DE REASEGURO";
		tipoObraCivilForm.setDescripcionClave(clave);
		tipoObraCivilForm.setPeriodoNormalConstruccion(tipoObraCivilDTO.getIdPeriodoNormalConstruccion().toString());
		tipoObraCivilForm.setDescripcionPeriodoNormalConstruccion(UtileriasWeb.getDescripcionCatalogoValorFijo(45, tipoObraCivilDTO.getIdPeriodoNormalConstruccion().intValue()));
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoObraCivilForm tipoObraCivilForm = (TipoObraCivilForm) form;
		TipoObraCivilDTO tipoObraCivilDTO = new TipoObraCivilDTO();
		TipoObraCivilDN tipoObraCivilDN = TipoObraCivilDN.getInstancia();
		try {
			poblarDTO(tipoObraCivilForm, tipoObraCivilDTO);
			tipoObraCivilDN.agregar(tipoObraCivilDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoObraCivilForm tipoObraCivilForm = (TipoObraCivilForm) form;
		TipoObraCivilDN tipoObraCivilDN = TipoObraCivilDN.getInstancia();
		TipoObraCivilDTO tipoObraCivilDTO = new TipoObraCivilDTO();
		try {
			poblarDTO(tipoObraCivilForm, tipoObraCivilDTO);
			tipoObraCivilDTO = tipoObraCivilDN.getTipoObraCivilPorId(tipoObraCivilDTO);
			poblarDTO(tipoObraCivilForm, tipoObraCivilDTO);
			tipoObraCivilDN.modificar(tipoObraCivilDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoObraCivilForm tipoObraCivilForm = (TipoObraCivilForm) form;
		TipoObraCivilDTO tipoObraCivilDTO = new TipoObraCivilDTO();
		TipoObraCivilDN tipoObraCivilDN = TipoObraCivilDN.getInstancia();
		try {
			poblarDTO(tipoObraCivilForm, tipoObraCivilDTO);
			tipoObraCivilDTO = tipoObraCivilDN.getTipoObraCivilPorId(tipoObraCivilDTO);
			tipoObraCivilDN.borrar(tipoObraCivilDTO);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoObraCivilDTO tipoObraCivilDTO = new TipoObraCivilDTO();
		TipoObraCivilForm tipoObraCivilForm = (TipoObraCivilForm) form;
		TipoObraCivilDN tipoObraCivilDN = TipoObraCivilDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoObraCivilDTO.setIdTipoObraCivil(UtileriasWeb.regresaBigDecimal(id));
			tipoObraCivilDTO = tipoObraCivilDN.getTipoObraCivilPorId(tipoObraCivilDTO);
			poblarForm(tipoObraCivilForm, tipoObraCivilDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
}
