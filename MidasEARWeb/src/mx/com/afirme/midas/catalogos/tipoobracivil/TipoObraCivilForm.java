package mx.com.afirme.midas.catalogos.tipoobracivil;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoObraCivilForm extends MidasBaseForm{

	/**
	 * @author Christian Ceballos
	 * @since 28/07/09
	 */
	private static final long serialVersionUID = -4207971052132126120L;
	
	private String idTabla;
    private String idObra;
    private String descripcion;
    private String claveAutorizacion;
    private String descripcionClave;
    private String periodoNormalConstruccion;
    private String descripcionPeriodoNormalConstruccion;
    
	public String getIdTabla() {
		return idTabla;
	}
	public void setIdTabla(String idTabla) {
		this.idTabla = idTabla;
	}
	public String getIdObra() {
		return idObra;
	}
	public void setIdObra(String idObra) {
		this.idObra = idObra;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getClaveAutorizacion() {
		return claveAutorizacion;
	}
	public void setClaveAutorizacion(String claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}
	/**
	 * @return the descripcionClave
	 */
	public String getDescripcionClave() {
		return descripcionClave;
	}
	/**
	 * @param descripcionClave the descripcionClave to set
	 */
	public void setDescripcionClave(String descripcionClave) {
		this.descripcionClave = descripcionClave;
	}
	public void setPeriodoNormalConstruccion(String periodoNormalConstruccion) {
		this.periodoNormalConstruccion = periodoNormalConstruccion;
	}
	public String getPeriodoNormalConstruccion() {
		return periodoNormalConstruccion;
	}
	public void setDescripcionPeriodoNormalConstruccion(
			String descripcionPeriodoNormalConstruccion) {
		this.descripcionPeriodoNormalConstruccion = descripcionPeriodoNormalConstruccion;
	}
	public String getDescripcionPeriodoNormalConstruccion() {
		return descripcionPeriodoNormalConstruccion;
	}
}
