package mx.com.afirme.midas.catalogos.tipoobracivil;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoObraCivilDN {
	public static final TipoObraCivilDN INSTANCIA = new TipoObraCivilDN();

	public static TipoObraCivilDN getInstancia (){
		return TipoObraCivilDN.INSTANCIA;
	}
	
	public String agregar(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.agregar(tipoObraCivilDTO);
	}
	
	public String borrar (TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.borrar(tipoObraCivilDTO);
	}
	
	public String modificar (TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.modificar(tipoObraCivilDTO);
	}
	
	public TipoObraCivilDTO getTipoObraCivilPorId(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.getTipoObraCivilPorId(tipoObraCivilDTO);
	}
	
	public TipoObraCivilDTO getTipoObraCivilPorId(BigDecimal idTipoObraCivil) throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.getTipoObraCivilPorId(idTipoObraCivil);
	}
	
	public List<TipoObraCivilDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.listarTodos();
	}
	
	public List<TipoObraCivilDTO> listarFiltrado(TipoObraCivilDTO tipoObraCivilDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TipoObraCivilSN tipoObraCivilSN = new TipoObraCivilSN();
		return tipoObraCivilSN.listarFiltrado(tipoObraCivilDTO);
	}

}
