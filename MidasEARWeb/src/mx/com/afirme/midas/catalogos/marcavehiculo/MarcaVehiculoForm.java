package mx.com.afirme.midas.catalogos.marcavehiculo;

import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class MarcaVehiculoForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2537994971201558627L;
	private String idTcMarcaVehiculo;
	private String idTcTipoVehiculo;
    private TipoBienAutosForm tipoBienAutosForm=new TipoBienAutosForm();
    private String codigoMarcaVehiculo;
    private String descripcionMarcaVehiculo;
	public String getIdTcMarcaVehiculo() {
		return idTcMarcaVehiculo;
	}
	public void setIdTcMarcaVehiculo(String idTcMarcaVehiculo) {
		this.idTcMarcaVehiculo = idTcMarcaVehiculo;
	}
	public TipoBienAutosForm getTipoBienAutosForm() {
		return tipoBienAutosForm;
	}
	public void setTipoBienAutosForm(TipoBienAutosForm tipoBienAutosForm) {
		this.tipoBienAutosForm = tipoBienAutosForm;
	}
	public String getCodigoMarcaVehiculo() {
		return codigoMarcaVehiculo;
	}
	public void setCodigoMarcaVehiculo(String codigoMarcaVehiculo) {
		this.codigoMarcaVehiculo = codigoMarcaVehiculo;
	}
	public String getDescripcionMarcaVehiculo() {
		return descripcionMarcaVehiculo;
	}
	public void setDescripcionMarcaVehiculo(String descripcionMarcaVehiculo) {
		this.descripcionMarcaVehiculo = descripcionMarcaVehiculo;
	}
	public String getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}
	public void setIdTcTipoVehiculo(String idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}
}
