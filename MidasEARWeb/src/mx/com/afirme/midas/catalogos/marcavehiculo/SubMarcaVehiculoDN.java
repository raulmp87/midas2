package mx.com.afirme.midas.catalogos.marcavehiculo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubMarcaVehiculoDN {
	public static final SubMarcaVehiculoDN INSTANCIA = new SubMarcaVehiculoDN();


	
	
	public static final String[] MAPEO_ATRIBUTOS_DTO={"idSubTcMarcaVehiculo","descripcionSubMarcaVehiculo","marcaVehiculoDTO.idTcMarcaVehiculo","tipoBienAutosDTO.claveTipoBien",
		"tipoBienAutosDTO.descripcionTipoBien","marcaVehiculoDTO.codigoMarcaVehiculo","marcaVehiculoDTO.descripcionMarcaVehiculo"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={"idSubTcMarcaVehiculo","descripcionSubMarcaVehiculo", "marcaVehiculoForm.idTcMarcaVehiculo","tipoBienAutosForm.claveTipoBien",
		"tipoBienAutosForm.descripcionTipoBien","marcaVehiculoForm.codigoMarcaVehiculo","marcaVehiculoForm.descripcionMarcaVehiculo"};
	
	public static SubMarcaVehiculoDN getInstancia (){
		return SubMarcaVehiculoDN.INSTANCIA;
	}
	
	public void agregar(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubMarcaVehiculoSN().agregar(subMarcaVehiculoDTO);
	}
	
	public void borrar (SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubMarcaVehiculoSN().borrar(subMarcaVehiculoDTO);
	}
	
	public void modificar (SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new SubMarcaVehiculoSN().modificar(subMarcaVehiculoDTO);
	}
	
	public SubMarcaVehiculoDTO getMarcaVehiculoPorId(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubMarcaVehiculoSN().getSubMarcaVehiculoPorId(subMarcaVehiculoDTO);
	}
	
	public List<SubMarcaVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new SubMarcaVehiculoSN().listarTodos();
	}
	
	public List<SubMarcaVehiculoDTO> listarFiltrado(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new SubMarcaVehiculoSN().listarFiltrado(subMarcaVehiculoDTO);
	}
}
