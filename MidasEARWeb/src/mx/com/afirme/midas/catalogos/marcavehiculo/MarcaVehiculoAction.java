package mx.com.afirme.midas.catalogos.marcavehiculo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MarcaVehiculoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		List<MarcaVehiculoDTO> listMarcaVehiculo = marcaVehiculoDN.listarTodos();
		request.setAttribute("listMarcaVehiculo", listMarcaVehiculo);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MarcaVehiculoForm marcaVehiculoForm = (MarcaVehiculoForm) form;
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		try {
			poblarDTO(marcaVehiculoForm, marcaVehiculoDTO);
			request.setAttribute("listMarcaVehiculo", marcaVehiculoDN.listarFiltrado(marcaVehiculoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(MarcaVehiculoForm marcaVehiculoForm,
			MarcaVehiculoDTO marcaVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(MarcaVehiculoDTO.class, MarcaVehiculoDN.MAPEO_ATRIBUTOS_DTO, MarcaVehiculoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(marcaVehiculoDTO, marcaVehiculoForm);
	}
	
	private void poblarForm(MarcaVehiculoForm marcaVehiculoForm,
		MarcaVehiculoDTO marcaVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(MarcaVehiculoForm.class, MarcaVehiculoDN.MAPEO_ATRIBUTOS_FORM, MarcaVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(marcaVehiculoForm, marcaVehiculoDTO);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MarcaVehiculoForm marcaVehiculoForm = (MarcaVehiculoForm) form;
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		try {
			poblarDTO(marcaVehiculoForm, marcaVehiculoDTO);
			MarcaVehiculoDTO filtro = new MarcaVehiculoDTO(); 
			filtro.setCodigoMarcaVehiculo(marcaVehiculoDTO.getCodigoMarcaVehiculo());
			filtro.setDescripcionMarcaVehiculo(marcaVehiculoDTO.getDescripcionMarcaVehiculo());
			filtro.setTipoBienAutosDTO(marcaVehiculoDTO.getTipoBienAutosDTO());
			List<MarcaVehiculoDTO> resultados = marcaVehiculoDN.listarFiltrado(filtro);
			if(resultados != null && !resultados.isEmpty()){
				marcaVehiculoForm.setTipoMensaje(Sistema.ERROR);
				marcaVehiculoForm
						.setMensaje("El c&oacute;digo y descripci&oacute;n introducido ya ha sido usado en otro registro.");
				reglaNavegacion = Sistema.NO_EXITOSO;
			}else{
				marcaVehiculoDN.agregar(marcaVehiculoDTO);
				listarTodos(request);
				limpiarForm(marcaVehiculoForm);		
				marcaVehiculoForm.setTipoMensaje(Sistema.EXITO);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MarcaVehiculoForm marcaVehiculoForm = (MarcaVehiculoForm) form;
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		try {
			poblarDTO(marcaVehiculoForm, marcaVehiculoDTO);
			marcaVehiculoDTO = marcaVehiculoDN.getMarcaVehiculoPorId(marcaVehiculoDTO);
			poblarDTO(marcaVehiculoForm, marcaVehiculoDTO);
			marcaVehiculoDN.modificar(marcaVehiculoDTO);
			listarTodos(request);
			limpiarForm(marcaVehiculoForm);
			marcaVehiculoForm.setTipoMensaje(Sistema.ACCIONMODIFICAR);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MarcaVehiculoForm marcaVehiculoForm = (MarcaVehiculoForm) form;
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		try {
			poblarDTO(marcaVehiculoForm, marcaVehiculoDTO);
//			marcaVehiculoDTO = marcaVehiculoDN.getMarcaVehiculoPorId(marcaVehiculoDTO);
			marcaVehiculoDN.borrar(marcaVehiculoDTO);
			limpiarForm(marcaVehiculoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		MarcaVehiculoForm marcaVehiculoForm = (MarcaVehiculoForm) form;
		MarcaVehiculoDN marcaVehiculoDN = MarcaVehiculoDN.getInstancia();
		try {
			String id = request.getParameter("id");
			marcaVehiculoDTO.setIdTcMarcaVehiculo(UtileriasWeb.regresaBigDecimal(id));
			marcaVehiculoDTO = marcaVehiculoDN.getMarcaVehiculoPorId(marcaVehiculoDTO);
			poblarForm(marcaVehiculoForm, marcaVehiculoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(MarcaVehiculoForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(MarcaVehiculoForm.class, MarcaVehiculoDN.MAPEO_ATRIBUTOS_FORM, MarcaVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
}
