package mx.com.afirme.midas.catalogos.marcavehiculo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MarcaVehiculoDN {
	public static final MarcaVehiculoDN INSTANCIA = new MarcaVehiculoDN();
	public static final String[] MAPEO_ATRIBUTOS_DTO={"idTcMarcaVehiculo","tipoBienAutosDTO.claveTipoBien",
		"tipoBienAutosDTO.descripcionTipoBien","codigoMarcaVehiculo","descripcionMarcaVehiculo"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={"idTcMarcaVehiculo","tipoBienAutosForm.claveTipoBien",
		"tipoBienAutosForm.descripcionTipoBien","codigoMarcaVehiculo","descripcionMarcaVehiculo"};
	
	public static MarcaVehiculoDN getInstancia (){
		return MarcaVehiculoDN.INSTANCIA;
	}
	
	public void agregar(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new MarcaVehiculoSN().agregar(marcaVehiculoDTO);
	}
	
	public void borrar (MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new MarcaVehiculoSN().borrar(marcaVehiculoDTO);
	}
	
	public void modificar (MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new MarcaVehiculoSN().modificar(marcaVehiculoDTO);
	}
	
	public MarcaVehiculoDTO getMarcaVehiculoPorId(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new MarcaVehiculoSN().getMarcaVehiculoPorId(marcaVehiculoDTO);
	}
	
	public List<MarcaVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new MarcaVehiculoSN().listarTodos();
	}
	
	public List<MarcaVehiculoDTO> listarFiltrado(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new MarcaVehiculoSN().listarFiltrado(marcaVehiculoDTO);
	}
}
