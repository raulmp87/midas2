package mx.com.afirme.midas.catalogos.marcavehiculo;

import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SubMarcaVehiculoForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String idSubTcMarcaVehiculo;
	private String descripcionSubMarcaVehiculo;
	private MarcaVehiculoForm marcaVehiculoForm=new MarcaVehiculoForm();
    private TipoBienAutosForm tipoBienAutosForm=new TipoBienAutosForm();
	
	public TipoBienAutosForm getTipoBienAutosForm() {
		return tipoBienAutosForm;
	}
	public void setTipoBienAutosForm(TipoBienAutosForm tipoBienAutosForm) {
		this.tipoBienAutosForm = tipoBienAutosForm;
	}
	public String getIdSubTcMarcaVehiculo() {
		return idSubTcMarcaVehiculo;
	}
	public void setIdSubTcMarcaVehiculo(String idSubTcMarcaVehiculo) {
		this.idSubTcMarcaVehiculo = idSubTcMarcaVehiculo;
	}
	public String getDescripcionSubMarcaVehiculo() {
		return descripcionSubMarcaVehiculo;
	}
	public void setDescripcionSubMarcaVehiculo(String descripcionSubMarcaVehiculo) {
		this.descripcionSubMarcaVehiculo = descripcionSubMarcaVehiculo;
	}
	public MarcaVehiculoForm getMarcaVehiculoForm() {
		return marcaVehiculoForm;
	}
	public void setMarcaVehiculoForm(MarcaVehiculoForm marcaVehiculoForm) {
		this.marcaVehiculoForm = marcaVehiculoForm;
	}


}
