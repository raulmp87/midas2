package mx.com.afirme.midas.catalogos.marcavehiculo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubMarcaVehiculoSN {
	private SubMarcaVehiculoFacadeRemote beanRemoto;

	public SubMarcaVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SubMarcaVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(subMarcaVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(subMarcaVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(subMarcaVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	} 
	
	public List<SubMarcaVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public SubMarcaVehiculoDTO getSubMarcaVehiculoPorId(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(subMarcaVehiculoDTO.getIdSubTcMarcaVehiculo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SubMarcaVehiculoDTO> listarFiltrado(SubMarcaVehiculoDTO subMarcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(subMarcaVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
