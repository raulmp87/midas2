package mx.com.afirme.midas.catalogos.marcavehiculo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MarcaVehiculoSN {
	private MarcaVehiculoFacadeRemote beanRemoto;

	public MarcaVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MarcaVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(marcaVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(marcaVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(marcaVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<MarcaVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public MarcaVehiculoDTO getMarcaVehiculoPorId(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(marcaVehiculoDTO.getIdTcMarcaVehiculo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<MarcaVehiculoDTO> listarFiltrado(MarcaVehiculoDTO marcaVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(marcaVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
