package mx.com.afirme.midas.catalogos.descuento;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DescuentoDN {
	/**
	 * @author Christian Ceballos
	 * @since 9 de Julio de 2009
	 */
	private static final DescuentoDN INSTANCIA = new DescuentoDN();
	
	public static DescuentoDN getInstancia(){
		return DescuentoDN.INSTANCIA;
	}
	
	public String guardar(DescuentoDTO descuentoDTO) throws SystemException, ExcepcionDeAccesoADatos{
		DescuentoSN descuentoSN = new DescuentoSN();
		descuentoSN.guardar(descuentoDTO);
		return null;
	}
	
	public String actualizar(DescuentoDTO descuentoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoSN descuentoSN = new DescuentoSN();
		descuentoSN.actualizar(descuentoDTO);
		return null;
	} 
	
	public String borrar(DescuentoDTO descuentoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		DescuentoSN descuentoSN = new DescuentoSN();
		descuentoSN.borrar(descuentoDTO);
		return null;
	}
	
	public List<DescuentoDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos{
		DescuentoSN descuentoSN = new DescuentoSN();
		return descuentoSN.listarTodos();
	}
	
	public List<DescuentoDTO> listarFiltrados(DescuentoDTO descuentoDTO) throws SystemException, ExcepcionDeAccesoADatos{
		DescuentoSN descuentoSN =  new DescuentoSN();
		return descuentoSN.listarFiltrado(descuentoDTO);
	}
	
	public DescuentoDTO getPorId(DescuentoDTO descuentoDTO) throws SystemException, ExcepcionDeAccesoADatos{
		DescuentoSN descuentoSN =  new DescuentoSN();
		return descuentoSN.getPorId(descuentoDTO.getIdToDescuentoVario());
	}

	public List<DescuentoDTO> listarDescuentosEspeciales() throws SystemException {
		DescuentoSN descuentoSN =  new DescuentoSN();
		return descuentoSN.listarDescuentosEspeciales();
	}
}
