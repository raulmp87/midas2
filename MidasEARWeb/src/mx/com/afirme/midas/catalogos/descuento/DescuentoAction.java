package mx.com.afirme.midas.catalogos.descuento;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DescuentoAction extends MidasMappingDispatchAction {

	public ActionForward guardar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DescuentoForm descuentoForm = (DescuentoForm) form;
		DescuentoDTO descuentoDTO = new DescuentoDTO();
		DescuentoDN descuentoDN = DescuentoDN.getInstancia();
		try {
			this.poblarDTO(descuentoDTO, descuentoForm);
			descuentoDN.guardar(descuentoDTO);
			listar(mapping, descuentoForm, request, response);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);

	}

	public String poblarDTO(DescuentoDTO descuentoDTO,
			DescuentoForm descuentoForm) throws SystemException {
		if (!UtileriasWeb.esCadenaVacia(descuentoForm.getClaveTipo()))
			descuentoDTO.setClaveTipo(UtileriasWeb.regresaShort(descuentoForm.getClaveTipo()));
		if (!UtileriasWeb.esCadenaVacia(descuentoForm.getDescripcion()))
			descuentoDTO.setDescripcion(descuentoForm.getDescripcion().trim().toUpperCase());
		if (!UtileriasWeb.esCadenaVacia(descuentoForm.getIdDescuento()))
			descuentoDTO.setIdToDescuentoVario(UtileriasWeb.regresaBigDecimal(descuentoForm.getIdDescuento()));
		return null;
	}

	public String poblarForm(DescuentoDTO descuentoDTO,
			DescuentoForm descuentoForm) {
		String claveTipo = descuentoDTO.getClaveTipo().toString();
		descuentoForm.setClaveTipo(claveTipo);
		if (claveTipo.equals(Sistema.CLAVE_TIPO_PORCENTAJE)){
			descuentoForm.setDescripcionClave("PORCENTAJE");
		}else{
			descuentoForm.setDescripcionClave("IMPORTE");
		}
		descuentoForm.setDescripcion(descuentoDTO.getDescripcion());
		descuentoForm.setIdDescuento(descuentoDTO.getIdToDescuentoVario()
				.toString());
		return null;
	}

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		try {
			DescuentoDN descuentoDN = DescuentoDN.getInstancia();
			List<DescuentoDTO> descuentos = descuentoDN.listarTodos();
			request.setAttribute("descuentos", descuentos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		try {
			DescuentoDN descuentoDN = DescuentoDN.getInstancia();
			DescuentoForm descuentoForm = (DescuentoForm) form;
			DescuentoDTO descuentoDTO = new DescuentoDTO();
			this.poblarDTO(descuentoDTO, descuentoForm);
			List<DescuentoDTO> descuentos = descuentoDN
					.listarFiltrados(descuentoDTO);
			request.setAttribute("descuentos", descuentos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DescuentoForm descuentoForm = (DescuentoForm) form;
		DescuentoDTO descuentoDTO = new DescuentoDTO();
		DescuentoDN descuentoDN = DescuentoDN.getInstancia();
		try {
			this.poblarDTO(descuentoDTO, descuentoForm);
			descuentoDTO = descuentoDN.getPorId(descuentoDTO);
			this.poblarDTO(descuentoDTO, descuentoForm);
			descuentoDN.actualizar(descuentoDTO);
			listar(mapping, descuentoForm, request, response);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DescuentoForm descuentoForm = (DescuentoForm) form;
		DescuentoDTO descuentoDTO = new DescuentoDTO();
		DescuentoDN descuentoDN = DescuentoDN.getInstancia();
		try {
			this.poblarDTO(descuentoDTO, descuentoForm);
			descuentoDN.borrar(descuentoDTO);
			listar(mapping, descuentoForm, request, response);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DescuentoForm descuentoForm = (DescuentoForm) form;
		DescuentoDTO descuentoDTO = new DescuentoDTO();
		String id = request.getParameter("id");
		DescuentoDN descuentoDN = DescuentoDN.getInstancia();
		try {
			descuentoDTO.setIdToDescuentoVario(UtileriasWeb.regresaBigDecimal(id));
			descuentoDTO = descuentoDN.getPorId(descuentoDTO);
			this.poblarForm(descuentoDTO, descuentoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}

}
