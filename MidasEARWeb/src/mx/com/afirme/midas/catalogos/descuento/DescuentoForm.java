package mx.com.afirme.midas.catalogos.descuento;

import mx.com.afirme.midas.sistema.MidasBaseForm;


public class DescuentoForm  extends MidasBaseForm{
	/**
	 * @author Christian Ceballos
	 * @since 9 de julio de 2009
	 */
	private static final long serialVersionUID = -7509034099225547734L;
	private String idDescuento;
	private String claveTipo;
    private String descripcion;
    private String descripcionClave;
    
	public String getIdDescuento() {
		return idDescuento;
	}
	public void setIdDescuento(String idDescuento) {
		this.idDescuento = idDescuento;
	}
	public String getClaveTipo() {
		return claveTipo;
	}
	public void setClaveTipo(String claveTipo) {
		this.claveTipo = claveTipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcionClave() {
		return descripcionClave;
	}
	public void setDescripcionClave(String descripcionClave) {
		this.descripcionClave = descripcionClave;
	}
	
}
