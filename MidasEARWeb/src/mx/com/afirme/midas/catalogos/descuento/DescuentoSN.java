package mx.com.afirme.midas.catalogos.descuento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DescuentoSN {
	/**
	 * @author Christian Ceballos
	 * @since 9 de Julio de 2009
	 */

	private DescuentoFacadeRemote beanRemoto;

	public DescuentoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DescuentoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String guardar(DescuentoDTO descuentoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(descuentoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String actualizar(DescuentoDTO descuentoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(descuentoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}

	public String borrar(DescuentoDTO descuentoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(descuentoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

		return null;
	}

	public List<DescuentoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public List<DescuentoDTO> listarFiltrado(DescuentoDTO descuentoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(descuentoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public DescuentoDTO getPorId(BigDecimal idDescuentoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idDescuentoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public List<DescuentoDTO> listarDescuentosPorAsociar(BigDecimal idProducto)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarDescuentosPorAsociar(idProducto);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoDTO> listarDescuentosPorAsociarTipoPoliza(
			BigDecimal idToTipoPoliza)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarDescuentosPorAsociarTipoPoliza(
					idToTipoPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoDTO> listarDescuentosPorAsociarCobertura(
			BigDecimal idToCobertura) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto
					.listarDescuentosPorAsociarCobertura(idToCobertura);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DescuentoDTO> listarDescuentosEspeciales() {
		try {
			return beanRemoto.listarDescuentosEspeciales();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
