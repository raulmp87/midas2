package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoServicioVehiculoForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8735537793507155833L;
	private String idTcTipoServicioVehiculo;
	private String idTcTipoVehiculo;
    private String codigoTipoServicioVehiculo;
    private String descripcionTipoServVehiculo;
    private TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
    

	public String getIdTcTipoServicioVehiculo() {
		return idTcTipoServicioVehiculo;
	}
	public void setIdTcTipoServicioVehiculo(String idTcTipoServicioVehiculo) {
		this.idTcTipoServicioVehiculo = idTcTipoServicioVehiculo;
	}
	public String getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}
	public void setIdTcTipoVehiculo(String idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}
	public String getCodigoTipoServicioVehiculo() {
		return codigoTipoServicioVehiculo;
	}
	public void setCodigoTipoServicioVehiculo(String codigoTipoServicioVehiculo) {
		this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
	}
	public String getDescripcionTipoServVehiculo() {
		return descripcionTipoServVehiculo;
	}
	public void setDescripcionTipoServVehiculo(String descripcionTipoServVehiculo) {
		this.descripcionTipoServVehiculo = descripcionTipoServVehiculo;
	}
	public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}
	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}
	
}
