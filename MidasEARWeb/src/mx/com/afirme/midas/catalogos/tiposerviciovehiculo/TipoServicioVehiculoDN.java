package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoServicioVehiculoDN {
	public static final TipoServicioVehiculoDN INSTANCIA = new TipoServicioVehiculoDN();
	public static final String[] MAPEO_ATRIBUTOS_DTO={"idTcTipoServicioVehiculo","idTcTipoVehiculo","codigoTipoServicioVehiculo","descripcionTipoServVehiculo"};
	public static final String[] MAPEO_ATRIBUTOS_FORM={"idTcTipoServicioVehiculo","idTcTipoVehiculo","codigoTipoServicioVehiculo","descripcionTipoServVehiculo"};
	
	public static TipoServicioVehiculoDN getInstancia (){
		return TipoServicioVehiculoDN.INSTANCIA;
	}
	
	public void agregar(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoServicioVehiculoSN().agregar(tipoServicioVehiculoDTO);
	}
	
	public TipoServicioVehiculoDTO obtenerPorId(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoServicioVehiculoSN().obtenerPorId(tipoServicioVehiculoDTO.getIdTcTipoServicioVehiculo());
	}
	
	public void borrar (TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoServicioVehiculoSN().borrar(tipoServicioVehiculoDTO);
	}
	
	public void modificar (TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		new TipoServicioVehiculoSN().modificar(tipoServicioVehiculoDTO);
	}
	
	public TipoServicioVehiculoDTO getTipoServicioVehiculoPorIdSistema(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoServicioVehiculoSN().getTipoServicioVehiculoPorIdSistema(tipoServicioVehiculoDTO);
	}
	
	public List<TipoServicioVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoServicioVehiculoSN().listarTodos();
	}
	
	public List<TipoServicioVehiculoDTO> listarFiltrado(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new TipoServicioVehiculoSN().listarFiltrado(tipoServicioVehiculoDTO);
	}
}
