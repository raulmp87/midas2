package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDN;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.utileriasweb.BeanFiller;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoServicioVehiculoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void listarTodos(HttpServletRequest request)
	throws SystemException, ExcepcionDeAccesoADatos {
		TipoServicioVehiculoDN tipoServicioVehiculoDN = TipoServicioVehiculoDN.getInstancia();
		List<TipoServicioVehiculoDTO> listTipoServicioVehiculo = tipoServicioVehiculoDN.listarTodos();
		request.setAttribute("listTipoServicioVehiculo", listTipoServicioVehiculo);
	}

	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoServicioVehiculoForm tipoServicioVehiculoForm = (TipoServicioVehiculoForm) form;
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO();
		TipoServicioVehiculoDN tipoServicioVehiculoDN = TipoServicioVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
			request.setAttribute("listTipoServicioVehiculo", tipoServicioVehiculoDN.listarFiltrado(tipoServicioVehiculoDTO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarDTO(TipoServicioVehiculoForm tipoServicioVehiculoForm,
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
//		tipoServicioVehiculoForm.descomponerIdCompuestoTipoVehiculo();
		filler.estableceMapeoResultados(TipoServicioVehiculoDTO.class, TipoServicioVehiculoDN.MAPEO_ATRIBUTOS_DTO, TipoServicioVehiculoDN.MAPEO_ATRIBUTOS_FORM);
		filler.obtenerResultadoMapeo(tipoServicioVehiculoDTO, tipoServicioVehiculoForm);
	}
	
	private void poblarForm(TipoServicioVehiculoForm tipoServicioVehiculoForm,
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws SystemException {
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoServicioVehiculoForm.class, TipoServicioVehiculoDN.MAPEO_ATRIBUTOS_FORM, TipoServicioVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.obtenerResultadoMapeo(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
	}

	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoServicioVehiculoForm tipoServicioVehiculoForm = (TipoServicioVehiculoForm) form;
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO();
		TipoServicioVehiculoDN tipoServicioVehiculoDN = TipoServicioVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
			tipoServicioVehiculoDN.agregar(tipoServicioVehiculoDTO);
			listarTodos(request);
			limpiarForm(tipoServicioVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoServicioVehiculoForm tipoServicioVehiculoForm = (TipoServicioVehiculoForm) form;
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO();
		TipoServicioVehiculoDN tipoServicioVehiculoDN = TipoServicioVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
			tipoServicioVehiculoDTO = tipoServicioVehiculoDN.obtenerPorId(tipoServicioVehiculoDTO);			
			poblarDTO(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
			tipoServicioVehiculoDN.modificar(tipoServicioVehiculoDTO);
			listarTodos(request);
			limpiarForm(tipoServicioVehiculoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoServicioVehiculoForm tipoServicioVehiculoForm = (TipoServicioVehiculoForm) form;
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO();
		TipoServicioVehiculoDN tipoServicioVehiculoDN = TipoServicioVehiculoDN.getInstancia();
		try {
			poblarDTO(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
//			tipoServicioVehiculoDTO = tipoServicioVehiculoDN.getTipoServicioVehiculoPorId(tipoServicioVehiculoDTO);
			tipoServicioVehiculoDN.borrar(tipoServicioVehiculoDTO);
			limpiarForm(tipoServicioVehiculoForm);
			listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO();
		TipoServicioVehiculoForm tipoServicioVehiculoForm = (TipoServicioVehiculoForm) form;
		TipoServicioVehiculoDN tipoServicioVehiculoDN = TipoServicioVehiculoDN.getInstancia();
		try {
			String id = request.getParameter("id");
			tipoServicioVehiculoDTO.setIdTcTipoServicioVehiculo(UtileriasWeb.regresaBigDecimal(id));
			tipoServicioVehiculoDTO = tipoServicioVehiculoDN.obtenerPorId(tipoServicioVehiculoDTO);
			TipoVehiculoDTO filtro = new TipoVehiculoDTO();
			filtro.setIdTcTipoVehiculo(tipoServicioVehiculoDTO.getIdTcTipoVehiculo());
			TipoVehiculoDTO tipoVehiculoDTO = TipoVehiculoDN.getInstancia().getTipoVehiculoPorId(filtro);
			tipoServicioVehiculoDTO.setTipoVehiculoDTO(tipoVehiculoDTO);
			poblarForm(tipoServicioVehiculoForm, tipoServicioVehiculoDTO);
			tipoServicioVehiculoForm.setTipoVehiculoDTO(tipoVehiculoDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	private void limpiarForm(TipoServicioVehiculoForm form){
		BeanFiller filler = new BeanFiller();
		filler.estableceMapeoResultados(TipoServicioVehiculoForm.class, TipoServicioVehiculoDN.MAPEO_ATRIBUTOS_FORM, TipoServicioVehiculoDN.MAPEO_ATRIBUTOS_DTO);
		filler.limpiarBean(form);
	}
}
