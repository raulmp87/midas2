package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoServicioVehiculoSN {

	private TipoServicioVehiculoFacadeRemote beanRemoto;

	public TipoServicioVehiculoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoServicioVehiculoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto TipoServicioVehiculo instanciado", Level.FINEST, null);
	}
	
	public void agregar(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(tipoServicioVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public TipoServicioVehiculoDTO obtenerPorId(BigDecimal idTcTipoServicioVehiculo) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(idTcTipoServicioVehiculo);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(tipoServicioVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.update(tipoServicioVehiculoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TipoServicioVehiculoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public TipoServicioVehiculoDTO getTipoServicioVehiculoPorIdSistema(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.findById(tipoServicioVehiculoDTO.getIdTcTipoServicioVehiculo());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoServicioVehiculoDTO> listarFiltrado(TipoServicioVehiculoDTO tipoServicioVehiculoDTO) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.listarFiltrado(tipoServicioVehiculoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
