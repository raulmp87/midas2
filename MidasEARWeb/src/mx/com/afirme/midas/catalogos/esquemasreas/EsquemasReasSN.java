package mx.com.afirme.midas.catalogos.esquemasreas;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class EsquemasReasSN {

	private EsquemasReasFacadeRemote beanRemoto;

	public EsquemasReasSN() throws SystemException {
		LogDeMidasWeb.log("Entrando enContactoSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(EsquemasReasFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<EsquemasDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}

	public void agregar(EsquemasDTO esquemasDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.save(esquemasDTO);
	}

	public void modificar(EsquemasDTO esquemasDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(esquemasDTO);
	}

	public EsquemasDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		
		List<EsquemasDTO> agenciaDTOs =  beanRemoto.findByPropertyID(id);  
		return agenciaDTOs.get(0);
	}

	public void borrar(EsquemasDTO esquemasDTO) throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(esquemasDTO);
	}

	public List<EsquemasDTO> listarFiltrado(EsquemasDTO esquemasDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(esquemasDTO);
	}

}