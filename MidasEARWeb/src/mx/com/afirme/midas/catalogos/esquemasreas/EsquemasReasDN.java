
package mx.com.afirme.midas.catalogos.esquemasreas;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class EsquemasReasDN {

	public static final EsquemasReasDN INSTANCIA = new EsquemasReasDN();

	public static EsquemasReasDN getInstancia() {
		return EsquemasReasDN.INSTANCIA;
	}

	public List<EsquemasDTO> listarTodos() throws SystemException
			 {
		EsquemasReasSN esquemasReasSN = new EsquemasReasSN();
		return esquemasReasSN.listarTodos();
	}

	public void agregar(EsquemasDTO esquemasDTO) throws SystemException {
		EsquemasReasSN esquemasReasSN = new EsquemasReasSN();
		esquemasReasSN.agregar(esquemasDTO);
	}

	public void modificar(EsquemasDTO esquemasDTO) throws SystemException {
		EsquemasReasSN esquemasReasSN = new EsquemasReasSN();
		esquemasReasSN.modificar(esquemasDTO);
	}

	public EsquemasDTO getPorId(EsquemasDTO esquemasDTO)
			throws SystemException {
		EsquemasReasSN esquemasReasSN = new EsquemasReasSN();
		return esquemasReasSN.getPorId(esquemasDTO.getId());
	}

	public void borrar(EsquemasDTO esquemasDTO) throws SystemException
			 {
		EsquemasReasSN esquemasReasSN = new EsquemasReasSN();
		esquemasReasSN.borrar(esquemasDTO);
	}

	public List<EsquemasDTO> listarFiltrado(EsquemasDTO esquemasDTO)
			throws SystemException {
		EsquemasReasSN esquemasReasSN = new EsquemasReasSN();
		return esquemasReasSN.listarFiltrado(esquemasDTO);
	}

}