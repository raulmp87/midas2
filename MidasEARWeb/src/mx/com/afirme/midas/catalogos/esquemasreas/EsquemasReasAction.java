package mx.com.afirme.midas.catalogos.esquemasreas;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



public class EsquemasReasAction extends MidasMappingDispatchAction{
	private static final Logger LOGGER = Logger.getLogger(EsquemasReasAction.class);
	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		EsquemasReasForm esquemasForm = (EsquemasReasForm) form;
		
		try {
			limpiarForm((EsquemasReasForm) form);
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			esquemasForm.setMensaje("Error SystemException : " + e.getMessage());
			LOGGER.info(e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			esquemasForm.setMensaje("Error ExcepcionDeAccesoADatos : " + e.getMessage());
			LOGGER.info(e);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	private void limpiarForm(EsquemasReasForm form){
		if(form != null){
			form.setIdContrato("");
			form.setCer("");
			form.setAnio("");
		}
	}
	
	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		EsquemasReasForm esquemasForm = (EsquemasReasForm) form;
		EsquemasDTO esquemasDTO = new EsquemasDTO();
		poblarDTO(esquemasForm, esquemasDTO);
		try {
			List<EsquemasDTO> contactos = EsquemasReasDN.getInstancia().listarFiltrado(esquemasDTO);
			request.setAttribute("contactos", contactos);
		} catch (ExcepcionDeAccesoADatos e) {
			esquemasForm.setMensaje("Error ExcepcionDeAccesoADatos : " + e.getMessage());
			LOGGER.info(e);
		} catch (SystemException e) {
			esquemasForm.setMensaje("Error SystemException : " + e.getMessage());
			LOGGER.info(e);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void listarTodos(HttpServletRequest request)throws SystemException {
		EsquemasReasDN contactoDN = EsquemasReasDN.getInstancia();
		List<EsquemasDTO> esquemas = contactoDN.listarTodos();
		request.setAttribute("contactos",esquemas);
	}

	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		EsquemasReasForm esquemasForm = (EsquemasReasForm) form;
		EsquemasDTO contactoDTO = new EsquemasDTO();
		this.poblarDTO(esquemasForm, contactoDTO);
		EsquemasReasDN contactoDN = EsquemasReasDN.getInstancia();
		try {
			contactoDN.agregar(contactoDTO);
			esquemasForm.setMensajeUsuario(null,"guardar");
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			esquemasForm.setMensaje("Error SystemException : " + e.getMessage());
			LOGGER.info(e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			esquemasForm.setMensaje("Error ExcepcionDeAccesoADatos : " + e.getMessage());
			LOGGER.info(e);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		EsquemasReasForm esquemasForm = (EsquemasReasForm) form;
		EsquemasDTO esquemaDTO = new EsquemasDTO();
		this.poblarDTO(esquemasForm, esquemaDTO);
		EsquemasReasDN esquemaDN = EsquemasReasDN.getInstancia();
		try {
			esquemaDN.modificar(esquemaDTO);
			esquemasForm.setMensajeUsuario(null,"modificar");
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			esquemasForm.setMensaje("Error SystemException : " + e.getMessage());
			LOGGER.info(e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			esquemasForm.setMensaje("Error ExcepcionDeAccesoADatos : " + e.getMessage());
			LOGGER.info(e);
		}

		return mapping.findForward(reglaNavegacion);

	}

	
	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		EsquemasReasForm esquemasForm = (EsquemasReasForm) form;
		EsquemasDTO contactoDTO = new EsquemasDTO();
		this.poblarDTO(esquemasForm, contactoDTO);
		
		EsquemasReasDN contactoDN = EsquemasReasDN.getInstancia();
		try {
				contactoDN.borrar(contactoDTO);
				this.listarTodos(request);
				esquemasForm.setMensajeUsuario(null,"eliminar");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			esquemasForm.setMensaje("Error : " + e.getMessage());
			LOGGER.info(e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			esquemasForm.setMensaje("Error : " + e.getMessage());
			LOGGER.info(e);
		} catch (Exception e){
			
			esquemasForm.setMensaje("Error : " + e.getMessage());
			LOGGER.info(e);
		}
		
		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method getPorId
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);

	}

	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	

	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion 			= Sistema.EXITOSO;
		EsquemasReasForm esquemasForm 		= (EsquemasReasForm) form;
		EsquemasDTO esquemaDTO 		= new EsquemasDTO();
		String idContrato		    = request.getParameter("idContrato");
		BigDecimal cer				= new BigDecimal(request.getParameter("cer"));
		BigDecimal nivel			= new BigDecimal(request.getParameter("nivel"));
		BigDecimal ordenEntrada		= new BigDecimal(request.getParameter("ordenEntrada"));
		
		esquemaDTO.setIdContrato(idContrato);
		esquemaDTO.setCer(cer);
		esquemaDTO.setNivel(nivel);
		esquemaDTO.setOrdenEntrada(ordenEntrada);
		this.poblarDTO(esquemasForm, esquemaDTO);
		EsquemasReasDN contactoDN = EsquemasReasDN.getInstancia();
		try {
			esquemaDTO = contactoDN.listarFiltrado(esquemaDTO).get(0);
			this.poblarForm(esquemaDTO, esquemasForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			esquemasForm.setMensaje("Error SystemException: " + e.getMessage());
			LOGGER.info(e);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			esquemasForm.setMensaje("Error ExcepcionDeAccesoADatos: " + e.getMessage());
			LOGGER.info(e);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContactoDTO
	 * @param EsquemasReasForm
	 */
	private void poblarForm(EsquemasDTO esquemaDTO, EsquemasReasForm esquemasReasForm) {
		
		if (esquemaDTO.getIdContrato()!= null) {
			esquemasReasForm.setIdContrato(esquemaDTO.getIdContrato());
		}
		
		if (esquemaDTO.getCer()!= null){
			esquemasReasForm.setCer(esquemaDTO.getCer()+"");
		}
		
		if (esquemaDTO.getAnio()!= null){
			esquemasReasForm.setAnio(esquemaDTO.getAnio()+"");
		}
		
		if (!StringUtil.isEmpty(esquemaDTO.getReinstalaciones()+"")) 
		{
			esquemasReasForm.setReinstalaciones(esquemaDTO.getReinstalaciones()+"");
		}
		

	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param esquemasReasForm
	 * @param ContactoDTO
	 */
	private void poblarDTO(EsquemasReasForm esquemasReasForm, EsquemasDTO esquemaDTO) {
	
		if (!StringUtil.isEmpty(esquemasReasForm.getIdContrato())) {
			
			esquemaDTO.setIdContrato(esquemasReasForm.getIdContrato());
		}
		
		if (!StringUtil.isEmpty(esquemasReasForm.getAnio())) 
			esquemaDTO.setAnio(new BigDecimal(esquemasReasForm.getAnio()));
		
		
		if (!StringUtil.isEmpty(esquemasReasForm.getCer())) {
			esquemaDTO.setCer(new BigDecimal(esquemasReasForm.getCer()));
		}
		
		if (!StringUtil.isEmpty(esquemasReasForm.getReinstalaciones()+"")) {
			esquemaDTO.setReinstalaciones(new BigDecimal(esquemasReasForm.getReinstalaciones()));
		}
		
		if (!StringUtil.isEmpty(esquemasReasForm.getNivel())) {
			esquemaDTO.setNivel(new BigDecimal(esquemasReasForm.getNivel()));
		}
		
		if (!StringUtil.isEmpty(esquemasReasForm.getOrdenEntrada())) {
			esquemaDTO.setOrdenEntrada(new BigDecimal(esquemasReasForm.getOrdenEntrada()));
		}
		
	}
	
}