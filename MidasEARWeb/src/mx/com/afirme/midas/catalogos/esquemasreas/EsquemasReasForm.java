package mx.com.afirme.midas.catalogos.esquemasreas;

import mx.com.afirme.midas.sistema.MidasBaseForm;;
 
/**
 * @struts.form name="EsquemasReasForm" Carga REAS
 */
public class EsquemasReasForm extends MidasBaseForm{

	private static final long serialVersionUID = -5655417772797679102L;
	private String idContrato;
	private String anio;
	private String cer;
	private String tipoCobertura;
	private String nivel;
	private String ordenEntrada;
	private String retencion;
	private String reinstalaciones;
	
	public String getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(String idContrato) {
		this.idContrato = idContrato;
	}
	public String getAnio() {
		if(anio == null)
			anio = "";
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCer() {
		if(cer == null)
			cer = "";
		return cer;
	}
	public void setCer(String cer) {
		this.cer = cer;
	}
	public String getTipoCobertura() {
		if(tipoCobertura == null)
			tipoCobertura = "";
		return tipoCobertura;
	}
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	public String getNivel() {
		if(nivel == null)
			nivel = "";
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getOrdenEntrada() {
		if(ordenEntrada == null)
			ordenEntrada = "";
		return ordenEntrada;
	}
	public void setOrdenEntrada(String ordenEntrada) {
		this.ordenEntrada = ordenEntrada;
	}
	public String getRetencion() {
		if(retencion == null)
			retencion = "";
		return retencion;
	}
	public void setRetencion(String retencion) {
		this.retencion = retencion;
	}
	public String getReinstalaciones() {
		if(reinstalaciones == null)
			reinstalaciones = "";
		return reinstalaciones;
	}
	public void setReinstalaciones(String reinstalaciones) {
		this.reinstalaciones = reinstalaciones;
	}
		
}