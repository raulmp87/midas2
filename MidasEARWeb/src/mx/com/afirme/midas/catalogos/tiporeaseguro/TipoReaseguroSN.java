package mx.com.afirme.midas.catalogos.tiporeaseguro;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoReaseguroSN {
	private TipoReaseguroFacadeRemote beanRemoto;
	
	public TipoReaseguroSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoReaseguroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("Bean Remoto instanciado", Level.FINEST, null);
	}
	
	public TipoReaseguroDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos, SystemException{
		
		return beanRemoto.findById(id.intValue());
	}
	
	public List<TipoReaseguroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		
		return beanRemoto.findAll();
	}

}
