package mx.com.afirme.midas.catalogos.tiporeaseguro;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoReaseguroDN {
	private static final TipoReaseguroDN INSTANCIA = new TipoReaseguroDN();
	
	public static TipoReaseguroDN getINSTANCIA() {
		return INSTANCIA;
	}

	public TipoReaseguroDTO getPorId(BigDecimal id)throws ExcepcionDeAccesoADatos, SystemException{
		TipoReaseguroSN tipoReaseguroSN = new TipoReaseguroSN();
		
		return tipoReaseguroSN.getPorId(id);
	}
	
	public List<TipoReaseguroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TipoReaseguroSN tipoReaseguroSN = new TipoReaseguroSN();
		
		return tipoReaseguroSN.listarTodos();
	}

}
