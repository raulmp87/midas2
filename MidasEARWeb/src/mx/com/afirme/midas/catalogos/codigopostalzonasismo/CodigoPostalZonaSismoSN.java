package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CodigoPostalZonaSismoSN {
	
	private CodigoPostalZonaSismoFacadeRemote beanRemoto;
	
	public CodigoPostalZonaSismoSN()
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en CodigoPostalZonaSismoSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CodigoPostalZonaSismoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<CodigoPostalZonaSismoDTO> listarFiltrado(
			CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(codigoPostalZonaSismoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al listarFiltrado de codigoPostalZonaSismo");
		}
	}

	public CodigoPostalZonaSismoDTO getPorId(CodigoPostalZonaSismoId codigoPostalZonaSismoId) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findById(codigoPostalZonaSismoId);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<CodigoPostalZonaSismoDTO> listarTodos() 
	 		throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Long obtenerTotalFiltrado(CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalFiltrado(codigoPostalZonaSismoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalFiltrado de codigoPostalZonaSismo");
		}
		
	}

}
