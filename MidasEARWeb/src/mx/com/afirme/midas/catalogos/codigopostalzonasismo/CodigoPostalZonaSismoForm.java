package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CodigoPostalZonaSismoForm extends MidasBaseForm {

	private static final long serialVersionUID = 3252331902911847221L;

	private String nombreColonia;
	private String codigoZonaSismo;
	private String descripcionZonaSismo;
	private String codigoPostal;

	/**
	 * @param NombreColonia
	 *            the NombreColonia to set
	 */
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	/**
	 * @return the NombreColonia
	 */
	public String getNombreColonia() {
		return nombreColonia;
	}

	/**
	 * @param descripcionZonaSismo
	 *            the descripcionZonaSismo to set
	 */
	public void setDescripcionZonaSismo(String descripcionZonaSismo) {
		this.descripcionZonaSismo = descripcionZonaSismo;
	}

	/**
	 * @return the descripcionZonaSismo
	 */
	public String getDescripcionZonaSismo() {
		return descripcionZonaSismo;
	}

	/**
	 * @param codigoZonaSismo
	 *            the codigoZonaSismo to set
	 */
	public void setCodigoZonaSismo(String codigoZonaSismo) {
		this.codigoZonaSismo = codigoZonaSismo;
	}

	/**
	 * @return the codigoZonaSismo
	 */
	public String getCodigoZonaSismo() {
		return codigoZonaSismo;
	}

	/**
	 * @param codigoPostal
	 *            the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}

}
