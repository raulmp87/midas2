package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CodigoPostalZonaSismoDN {

	private static final CodigoPostalZonaSismoDN INSTANCIA = new CodigoPostalZonaSismoDN();
	private CodigoPostalZonaSismoSN codigoPostalZonaSismoSN;
	
	public static CodigoPostalZonaSismoDN getInstancia() {
	
		return CodigoPostalZonaSismoDN.INSTANCIA;
		
	}

	public List<CodigoPostalZonaSismoDTO> listarFiltrado(CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		codigoPostalZonaSismoSN = new CodigoPostalZonaSismoSN();
		return codigoPostalZonaSismoSN.listarFiltrado(codigoPostalZonaSismoDTO);
	}

	public CodigoPostalZonaSismoDTO getPorId(CodigoPostalZonaSismoId codigoPostalZonaSismoId) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		codigoPostalZonaSismoSN = new CodigoPostalZonaSismoSN();
		return codigoPostalZonaSismoSN.getPorId(codigoPostalZonaSismoId);
	}

	public List<CodigoPostalZonaSismoDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos, SystemException{
		
		codigoPostalZonaSismoSN = new CodigoPostalZonaSismoSN();
		return codigoPostalZonaSismoSN.listarTodos();
	}
	
	public Long obtenerTotalFiltrado(CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		codigoPostalZonaSismoSN = new CodigoPostalZonaSismoSN();
		return codigoPostalZonaSismoSN.obtenerTotalFiltrado(codigoPostalZonaSismoDTO);
	}
}
