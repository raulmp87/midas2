package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CodigoPostalZonaSismoAction extends MidasMappingDispatchAction{

	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		return listarFiltrado(mapping, form, request, response);

	}

	/**
	 * Method listarFiltrado
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			CodigoPostalZonaSismoForm codigoPostalZonaSismoForm = (CodigoPostalZonaSismoForm) form;
			CodigoPostalZonaSismoDTO  codigoPostalZonaSismoDTO 	= new CodigoPostalZonaSismoDTO();
			poblarDTO(codigoPostalZonaSismoForm, codigoPostalZonaSismoDTO);
			
			Long totalRegistros = CodigoPostalZonaSismoDN.getInstancia().obtenerTotalFiltrado(codigoPostalZonaSismoDTO);
			codigoPostalZonaSismoForm.setTotalRegistros(totalRegistros.toString()); 
			
			return listarFiltradoPaginado(mapping, form, request, response);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	 public ActionForward listarFiltradoPaginado(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response) {
			
			String reglaNavegacion = Sistema.EXITOSO;
			
			try {
				
				if (!listaEnCache(form, request)) {
					
					CodigoPostalZonaSismoForm codigoPostalZonaSismoForm = (CodigoPostalZonaSismoForm) form;
					CodigoPostalZonaSismoDTO  codigoPostalZonaSismoDTO 	= new CodigoPostalZonaSismoDTO();
					poblarDTO(codigoPostalZonaSismoForm, codigoPostalZonaSismoDTO);
					
					codigoPostalZonaSismoDTO.setPrimerRegistroACargar(codigoPostalZonaSismoForm.getPrimerRegistroACargar());
					codigoPostalZonaSismoDTO.setNumeroMaximoRegistrosACargar(codigoPostalZonaSismoForm.getNumeroMaximoRegistrosACargar());
					codigoPostalZonaSismoForm.setListaPaginada(CodigoPostalZonaSismoDN.getInstancia().listarFiltrado(codigoPostalZonaSismoDTO), request);
				
				} 
				
				request.setAttribute("codigoPostalZonaSismo", obtieneListaAMostrar(form, request));
			
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
			return mapping.findForward(reglaNavegacion);
	}

	 
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		CodigoPostalZonaSismoForm codigoPostalZonaSismoForm = (CodigoPostalZonaSismoForm) form;
		CodigoPostalZonaSismoDTO  codigoPostalZonaSismoDTO  = new CodigoPostalZonaSismoDTO();
		String id = request.getParameter("id");
		String cp = request.getParameter("cp");
		CodigoPostalZonaSismoId codigoPostalZonaSismoId = new CodigoPostalZonaSismoId();
		codigoPostalZonaSismoId.setNombreColonia(id);
		codigoPostalZonaSismoId.setCodigoPostal(new BigDecimal(cp));
		CodigoPostalZonaSismoDN codigoPostalZonaSismoDN = CodigoPostalZonaSismoDN.getInstancia();
		try {
			codigoPostalZonaSismoDTO = codigoPostalZonaSismoDN.getPorId(codigoPostalZonaSismoId);
			this.poblarForm(codigoPostalZonaSismoDTO, codigoPostalZonaSismoForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	

	/**
	 * Method poblarDTO
	 * 
	 * @param CodigoPostalZonaSismoForm
	 * @param CodigoPostalZonaSismoDTO
	 */
	private void poblarDTO(CodigoPostalZonaSismoForm codigoPostalZonaSismoForm,
			CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO){		
		
		if(codigoPostalZonaSismoDTO.getId()==null)
			codigoPostalZonaSismoDTO.setId(new CodigoPostalZonaSismoId());
		
		if (!StringUtil.isEmpty(codigoPostalZonaSismoForm.getNombreColonia())) {
			codigoPostalZonaSismoDTO.getId().setNombreColonia(codigoPostalZonaSismoForm.getNombreColonia());
		}
		
		if (!StringUtil.isEmpty(codigoPostalZonaSismoForm.getCodigoPostal())) {
			BigDecimal cp = new BigDecimal(codigoPostalZonaSismoForm.getCodigoPostal());			
			codigoPostalZonaSismoDTO.getId().setCodigoPostal(cp);
		}				
	}

	/**
	 * Method poblarForm
	 * 
	 * @param CodigoPostalZonaSismoDTO
	 * @param CodigoPostalZonaSismoForm
	 */
	private void poblarForm(CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO,
			CodigoPostalZonaSismoForm codigoPostalZonaSismoForm) {

		if (codigoPostalZonaSismoDTO.getNombreColonia() != null)
			codigoPostalZonaSismoForm.setNombreColonia(codigoPostalZonaSismoDTO.getNombreColonia().toString());					

		if (codigoPostalZonaSismoDTO.getCodigoPostal() != null)
			codigoPostalZonaSismoForm.setCodigoPostal(codigoPostalZonaSismoDTO.getCodigoPostal().toString());			
				
		if (codigoPostalZonaSismoDTO.getCodigoZonaSismo() != null)
			codigoPostalZonaSismoForm.setCodigoZonaSismo(codigoPostalZonaSismoDTO.getCodigoZonaSismo().toString());
		
		if (codigoPostalZonaSismoDTO.getDescripcionZonaSismo() != null)
			codigoPostalZonaSismoForm.setDescripcionZonaSismo(codigoPostalZonaSismoDTO.getDescripcionZonaSismo());
	}
}
