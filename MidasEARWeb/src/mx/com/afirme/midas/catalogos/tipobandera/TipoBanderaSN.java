/**
 * 
 */
package mx.com.afirme.midas.catalogos.tipobandera;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class TipoBanderaSN {
	private TipoBanderaFacadeRemote beanRemoto;

	public TipoBanderaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoBanderaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<TipoBanderaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<TipoBanderaDTO> tipoBanderas;
		try {
			tipoBanderas = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return tipoBanderas;

	}
	
	public List<TipoBanderaDTO> listarFiltrados(TipoBanderaDTO tipoBanderaDTO) throws ExcepcionDeAccesoADatos {
		List<TipoBanderaDTO> tipoBanderas;
		try {
			tipoBanderas = beanRemoto.listarFiltrado(tipoBanderaDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
		return tipoBanderas;
	}
	
	public TipoBanderaDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
	}
}
