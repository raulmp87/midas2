package mx.com.afirme.midas.catalogos.tipobandera;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoBanderaDN {
	private static final TipoBanderaDN INSTANCIA = new TipoBanderaDN();

	public static TipoBanderaDN getInstancia() {
		return TipoBanderaDN.INSTANCIA;
	}

	public List<TipoBanderaDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoBanderaSN tipoBanderaSN = new TipoBanderaSN();
		return tipoBanderaSN.listarTodos();
	}	
	
	public List<TipoBanderaDTO> listarFiltrados(TipoBanderaDTO tipoBanderaDTO)
	throws SystemException, ExcepcionDeAccesoADatos {
	TipoBanderaSN tipoBanderaSN = new TipoBanderaSN();
	return tipoBanderaSN.listarFiltrados(tipoBanderaDTO);
}

	public TipoBanderaDTO getPorId(TipoBanderaDTO tipoBanderaDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		TipoBanderaSN tipoBanderaSN = new TipoBanderaSN();
		return tipoBanderaSN.getPorId(tipoBanderaDTO.getIdTcTipoBandera());
	}
}