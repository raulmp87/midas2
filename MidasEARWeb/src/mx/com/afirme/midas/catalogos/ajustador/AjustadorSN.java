/**
 * 
 */
package mx.com.afirme.midas.catalogos.ajustador;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.catalogos.ajustador.AjustadorFacadeRemote;

/**
 * @author admin
 * 
 */
public class AjustadorSN {
	private AjustadorFacadeRemote beanRemoto;

	public AjustadorSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(AjustadorFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<AjustadorDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public List<AjustadorDTO> listarFiltrados(AjustadorDTO ajustadorDTO) throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.listarFiltrado(ajustadorDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void agregar(AjustadorDTO ajustadorDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(ajustadorDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(AjustadorDTO ajustadorDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(ajustadorDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public AjustadorDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void borrar(AjustadorDTO ajustadorDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(ajustadorDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<AjustadorDTO> listarPorOrdenAsignacion() throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.listarPorOrdenAsignacion();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<AjustadorDTO> ajustadoresPorEstatus(Short estatus)throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.ajustadoresPorEstatus(estatus);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
