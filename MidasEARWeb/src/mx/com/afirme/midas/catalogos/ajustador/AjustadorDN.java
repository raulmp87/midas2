package mx.com.afirme.midas.catalogos.ajustador;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import mx.com.afirme.midas.catalogos.distanciaciudad.DistanciaCiudadDN;
import mx.com.afirme.midas.catalogos.distanciaciudad.DistanciaCiudadDTO;
import mx.com.afirme.midas.siniestro.cabina.AsignacionAjustadorWrapper;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AjustadorDN {
	private static final AjustadorDN INSTANCIA = new AjustadorDN();

	public static AjustadorDN getInstancia() {
		return AjustadorDN.INSTANCIA;
	}

	public List<AjustadorDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		return ajustadorSN.listarTodos();
	}

	public List<AjustadorDTO> listarFiltrados(AjustadorDTO ajustadorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		return ajustadorSN.listarFiltrados(ajustadorDTO);
	}

	public void agregar(AjustadorDTO ajustadorDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		ajustadorSN.agregar(ajustadorDTO);
	}

	public void modificar(AjustadorDTO ajustadorDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		ajustadorSN.modificar(ajustadorDTO);
	}

	public AjustadorDTO getPorId(AjustadorDTO ajustadorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		return ajustadorSN.getPorId(ajustadorDTO.getIdTcAjustador());
	}

	public void borrar(AjustadorDTO ajustadorDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		ajustadorSN.borrar(ajustadorDTO);
	}
	
	@SuppressWarnings("unchecked")
	public AsignacionAjustadorWrapper obtenerAsignacionAjustador(String ciudad) throws SystemException,ExcepcionDeAccesoADatos {
		AsignacionAjustadorWrapper asignacionAjustadorWrapper = new AsignacionAjustadorWrapper();
		
		AjustadorSN ajustadorSN = new AjustadorSN();
		List<AjustadorDTO> listaAjustadoresPorAsignacion = ajustadorSN.listarPorOrdenAsignacion();
		AjustadorDTO ajustadorSugerido = listaAjustadoresPorAsignacion.get(0);
		String ciudadAjustadorSugerido = ajustadorSugerido.getCiudad();
		asignacionAjustadorWrapper.setAjustadorSugerido(ajustadorSugerido);
		if(ciudad.equals(ciudadAjustadorSugerido)){
			asignacionAjustadorWrapper.setListaAsignacion(listaAjustadoresPorAsignacion);
		}else{
			Comparator<AjustadorDTO> comparador = new DistanciaAjustadorComparator();
			TreeSet<AjustadorDTO> listaAjustadoresxDistancia = new TreeSet<AjustadorDTO>(comparador);
			DistanciaCiudadDN distanciaCiudadDN = DistanciaCiudadDN.getInstancia();
			for (AjustadorDTO ajustadorDTO : listaAjustadoresPorAsignacion) {
				String ciudadAjustadorTemporal = ajustadorDTO.getCiudad();
				DistanciaCiudadDTO  distanciaCiudadDTO = new DistanciaCiudadDTO();
				distanciaCiudadDTO.setIdCiudadOrigen(ciudadAjustadorTemporal);
				distanciaCiudadDTO.setIdCiudadDestino(ciudad);
				
				List<DistanciaCiudadDTO> listaDistancias = distanciaCiudadDN.listarFiltrados(distanciaCiudadDTO);
				DistanciaCiudadDTO resultadoDistanciaCiudadDTO;
				if(listaDistancias.size() > 0 ){
					resultadoDistanciaCiudadDTO = listaDistancias.get(0);
					Long distanciaAjustador = resultadoDistanciaCiudadDTO.getDistancia();
					ajustadorDTO.setDistancia( Integer.valueOf(distanciaAjustador.intValue()));
					listaAjustadoresxDistancia.add(ajustadorDTO);
				}else{
					DistanciaCiudadDTO  distanciaCiudadDTOContraria = new DistanciaCiudadDTO();
					distanciaCiudadDTOContraria.setIdCiudadOrigen(ciudad);
					distanciaCiudadDTOContraria.setIdCiudadDestino(ciudadAjustadorTemporal);
					List<DistanciaCiudadDTO> listaDistanciasContraria = distanciaCiudadDN.listarFiltrados(distanciaCiudadDTOContraria);
					DistanciaCiudadDTO resultadoDistanciaCiudadDTOContraria;
					if(listaDistanciasContraria.size() > 0 ){
						resultadoDistanciaCiudadDTOContraria = listaDistanciasContraria.get(0);
						Long distanciaAjustadorContraria = resultadoDistanciaCiudadDTOContraria.getDistancia();
						ajustadorDTO.setDistancia( Integer.valueOf(distanciaAjustadorContraria.intValue()));
						listaAjustadoresxDistancia.add(ajustadorDTO);
					}
				} 
			}
			List<AjustadorDTO> listaFinalAjustadoresxDistancia = new ArrayList<AjustadorDTO>();
			Iterator iter = listaAjustadoresxDistancia.iterator();
			while(iter.hasNext()){
				AjustadorDTO ajustador = (AjustadorDTO)iter.next();
				listaFinalAjustadoresxDistancia.add(ajustador);
			}
			asignacionAjustadorWrapper.setListaAsignacion(listaFinalAjustadoresxDistancia);
		}
		
		return asignacionAjustadorWrapper;
	}
	
	public List<AjustadorDTO> ajustadoresPorEstatus(Short estatus)	throws SystemException, ExcepcionDeAccesoADatos {
		AjustadorSN ajustadorSN = new AjustadorSN();
		return ajustadorSN.ajustadoresPorEstatus(estatus);
	}
}
