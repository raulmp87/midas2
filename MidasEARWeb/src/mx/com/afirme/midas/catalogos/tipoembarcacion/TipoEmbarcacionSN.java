/**
 * 
 */
package mx.com.afirme.midas.catalogos.tipoembarcacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class TipoEmbarcacionSN {
	private TipoEmbarcacionFacadeRemote beanRemoto;

	public TipoEmbarcacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoEmbarcacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<TipoEmbarcacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<TipoEmbarcacionDTO> tipoEmbarcaciones;
		try {
			tipoEmbarcaciones = beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		return tipoEmbarcaciones;

	}
	
	public List<TipoEmbarcacionDTO> listarFiltrados(TipoEmbarcacionDTO tipoEmbarcacionDTO) throws ExcepcionDeAccesoADatos {
		List<TipoEmbarcacionDTO> tipoEmbarcaciones;
		try {
			tipoEmbarcaciones = beanRemoto.listarFiltrado(tipoEmbarcacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
		return tipoEmbarcaciones;
	}
	
	public TipoEmbarcacionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_ACCESO_DATOS);
		}
		
	}
}
