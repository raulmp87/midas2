package mx.com.afirme.midas.catalogos.tipoembarcacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoEmbarcacionDN {
	private static final TipoEmbarcacionDN INSTANCIA = new TipoEmbarcacionDN();

	public static TipoEmbarcacionDN getInstancia() {
		return TipoEmbarcacionDN.INSTANCIA;
	}

	public List<TipoEmbarcacionDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		TipoEmbarcacionSN tipoEmbarcacionSN = new TipoEmbarcacionSN();
		return tipoEmbarcacionSN.listarTodos();
	}	
	
	public List<TipoEmbarcacionDTO> listarFiltrados(TipoEmbarcacionDTO tipoEmbarcacionDTO)
	throws SystemException, ExcepcionDeAccesoADatos {
	TipoEmbarcacionSN tipoEmbarcacionSN = new TipoEmbarcacionSN();
	return tipoEmbarcacionSN.listarFiltrados(tipoEmbarcacionDTO);
}

	public TipoEmbarcacionDTO getPorId(TipoEmbarcacionDTO tipoEmbarcacionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		TipoEmbarcacionSN tipoEmbarcacionSN = new TipoEmbarcacionSN();
		return tipoEmbarcacionSN.getPorId(tipoEmbarcacionDTO.getIdTcTipoEmbarcacion());
	}
}