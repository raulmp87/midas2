package mx.com.afirme.midas.sistema.mail;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
@Deprecated
public class MailAction extends MidasMappingDispatchAction {
	
	private static final String emailFromAddress = Sistema.EMAIL_FROM_ADDRESS;
    private static final String SMTP_HOST_NAME = Sistema.EMAIL_HOST_NAME;
	private static final String SMTP_PORT = Sistema.EMAIL_PORT;
	
	//private static final String emailMsgTxt = "Test Message Contents";
	//private static final String emailSubjectTxt = "A test from gmail";
	//private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	//private static final String[] sendTo = {};

	@SuppressWarnings("unchecked")
	public void enviarMail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws MessagingException {

		/*List<String> recipients = Arrays.asList(sendTo);
		String subject = emailSubjectTxt;
		String messageContent = emailMsgTxt;
		String from = emailFromAddress;*/
		
		List<String> recipients = (List<String>) request.getAttribute("destinatarios");
		String subject = (String) request.getAttribute("asunto");
		String messageContent = (String) request.getAttribute("contenido");
		String from = emailFromAddress;

		String[] attachments = (String[]) request.getAttribute("adjuntos");
		sendMessage(recipients, subject, messageContent, from, attachments, null);
	}
	
	/**
	 * Envia un correo sencillo desde la cuenta configurada del sistema
	 * @param destinatarios Destinatarios del correo
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 * @param adjuntos Rutas absolutas de archivos adjuntos al correo (en caso de haberlos)
	 */
	@Deprecated
	public static void enviaCorreo(List<String> destinatarios, String titulo, String contenido, String[] adjuntos) {
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, adjuntos, null);
		
	}
	
	/**
	 * Envia un correo sencillo desde la cuenta configurada del sistema
	 * @param destinatarios Destinatarios del correo
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 * @param adjuntos Listado de objetos archivo adjuntos al correo (en caso de haberlos)
	 */
	public static void enviaCorreo(List<String> destinatarios, String titulo, String contenido, List<ByteArrayAttachment> adjuntos) {
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, adjuntos);
		
	}
	
	/**
	 * Envia un correo sencillo desde la cuenta configurada del sistema
	 * @param destinatarios Destinatarios del correo
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 */
	public static void enviaCorreo(List<String> destinatarios, String titulo, String contenido) {
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
		
	}
	
	/**
	 * Envia un correo sencillo desde la cuenta configurada del sistema
	 * @param destinatarios Destinatarios del correo
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 * @param emailFromAddress se modifica el correo de from
	 */
	public static void enviaCorreo(List<String> destinatarios, String titulo, String contenido, String emailFromAddress) {
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
		
	}
	
	/**
	 * Envia un correo sencillo de notificacion a los correos de los administradores de Midas y Sistemas Afirme
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 */
	public static void enviaCorreoNotificacionAdmins(String titulo, String contenido) {
		
		List<String> destinatarios = new ArrayList<String>();
		destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_MIDAS);
		destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_AFIRME);
		
		Date fecha = new Date();
		
		contenido = contenido + " - Registrado el " + fecha;
		
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
	}
	
	

	/**
	 * @param recipients
	 * @param subject
	 * @param messageContent
	 * @param from
	 * @param attachments 
	 * @throws MessagingException
	 */
	private static void sendMessage(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments, List<ByteArrayAttachment> fileAttachments) {
		try {
			if(Sistema.ENVIO_CORREOS_ACTIVADO){
				LogDeMidasWeb.log("El env�o de correos se encuentra activado en ambiente: "+Sistema.AMBIENTE_SISTEMA+". Se intentar� realizar el env�o de mail.", Level.INFO, null);
				boolean debug = false;
				Properties props = new Properties();
				props.put("mail.smtp.host", SMTP_HOST_NAME);
				props.put("mail.smtp.auth", "false");
				//props.put("mail.debug", "true");
				props.put("mail.smtp.port", SMTP_PORT);
				props.put("mail.smtp.socketFactory.port", SMTP_PORT);
				props.put("mail.smtp.socketFactory.fallback", "false");
				props.put("mail.smtp.starttls.enable", "true");
		
				Session session = javax.mail.Session.getInstance(props,new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						
						return new PasswordAuthentication(Sistema.EMAIL_FROM_USER, Sistema.EMAIL_FROM_PASSWORD);
					}
				});
				session.setDebug(debug);
				Message message = new MimeMessage(session);
				InternetAddress addressFrom = new InternetAddress(from);
				message.setFrom(addressFrom);
				for (Iterator<String> it = recipients.iterator(); it.hasNext();) {
					String destinatario = (String) it.next();
					if(Sistema.FILTRO_ENVIO_CORREOS_ACTIVO){
						for(String correoPermitido: Sistema.FILTRO_CORREOS_PERMITIDOS){
							if(correoPermitido.equalsIgnoreCase(destinatario.trim())){
								message.addRecipient(Message.RecipientType.TO, new InternetAddress(
										destinatario));
							}
						}
					}else{
						for(String correoPermitido: destinatario.split(",")){
							message.addRecipient(Message.RecipientType.TO, new InternetAddress(
									correoPermitido));													
						}
					}
				}
				// Setting the Subject and Content Type
				message.setSubject(subject);
				// Create a message part to represent the body text
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(messageContent, "text/html");
				// use a MimeMultipart as we need to handle the file attachments
				Multipart multipart = new MimeMultipart();
				// add the message body to the mime message
				multipart.addBodyPart(messageBodyPart);
				
				// Crear la firma
				BodyPart firma = new MimeBodyPart();
				firma.setContent(Sistema.AMBIENTE_SISTEMA + " " + Sistema.VERSION_SISTEMA + " " + UtileriasWeb.obtenerNombreEIpDelServidor(), "text/html");
				multipart.addBodyPart(firma);
				// add any file attachments to the message
				if(attachments != null && attachments.length > 0) {
					addAtachments(attachments, multipart);
				}
				if(fileAttachments != null && fileAttachments.size() > 0) {
					addAtachments(fileAttachments, multipart);
				}
				// Put all message parts in the message
				message.setContent(multipart);
				Transport.send(message);
				System.out.println("Envio de correo acertado");
			}
			else{
				LogDeMidasWeb.log("El env�o de correos se encuentra desactivado en ambiente: "+Sistema.AMBIENTE_SISTEMA+". Se omite el env�o de mail.", Level.INFO, null);
			}
		} catch (Exception mex) {
			System.out.println("Excepcion en el envio de Correo electr�nico " + mex);
		}

	}

	protected static void addAtachments(String[] attachments, Multipart multipart)
			throws MessagingException, AddressException {
		for (int i = 0; i <= attachments.length - 1; i++) {
			String filename = attachments[i];
			MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			// use a JAF FileDataSource as it does MIME type detection
			DataSource source = new FileDataSource(filename);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			// assume that the filename you want to send is the same as the
			// actual file name - could alter this to remove the file path
			String cleanFileName = filename.substring(filename.lastIndexOf("\\") + 1);
			attachmentBodyPart.setFileName(cleanFileName);
			// add the attachment
			multipart.addBodyPart(attachmentBodyPart);
		}
	}
	
	protected static void addAtachments(List<ByteArrayAttachment> files, Multipart multipart)
			throws MessagingException, AddressException {
		
		for (ByteArrayAttachment file : files) {
			MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			// ByteArrayDataSource as it does MIME type detection
			
			String tipoMIME = null;
			switch (file.getTipoArchivo()) {
			
				case PDF: {
					tipoMIME = "application/pdf";
					break;
				}
				case IMAGEN_JPG: {
					tipoMIME = "image/jpeg";
					break;
				}
				case TEXTO: {
					tipoMIME = "text/plain";
					break;
				}
				case HTML: {
					tipoMIME = "text/html";
					break;
				}
				case DESCONOCIDO: {
					tipoMIME = "application/unknown";
					break;
				}
				default : {
					tipoMIME = "application/unknown";
					break;
				}
			}
				
			DataSource source = new ByteArrayDataSource(file.getContenidoArchivo(), tipoMIME);
			
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			// assume that the filename you want to send is the same as the
			// actual file name - could alter this to remove the file path
			attachmentBodyPart.setFileName(file.getNombreArchivo());
			// add the attachment
			multipart.addBodyPart(attachmentBodyPart);
		}
	}
	
	
	public static  void enviarCorreosPorRol(String rol,String titulo, String contenido, Usuario usuarioSess) throws ExcepcionDeAccesoADatos, SystemException{
	    if(rol==null){
		return;
	    }
	    List<String> destinatarios= new ArrayList<String>();
	    List<Usuario> usuariosPorRol = UsuarioWSDN.getInstancia().obtieneUsuariosSinRolesPorNombreRol(rol, usuarioSess.getNombreUsuario(), usuarioSess.getIdSesionUsuario());
	    if(usuariosPorRol!=null&&!usuariosPorRol.isEmpty() ){
		for(Usuario usuario:usuariosPorRol){
		    if(!UtileriasWeb.esCadenaVacia(usuario.getEmail())){
			destinatarios.add(usuario.getEmail());
		    }
		}
	    }
	    if(!destinatarios.isEmpty()){
	     sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
	    }
	}
	
	
	public static  void enviarCorreosPorRoles(String[] roles,String titulo, String contenido, Usuario usuarioSess) throws ExcepcionDeAccesoADatos, SystemException{
	    List<Usuario> usuariosPorRol;
	    List<String> destinatarios= new ArrayList<String>();
	    List<String> destinatariosPorRol;
	    if(roles==null || roles.length==0){
		return;
	    }
	    for(String rol:roles){
    	       usuariosPorRol = UsuarioWSDN.getInstancia().obtieneUsuariosSinRolesPorNombreRol(rol, usuarioSess.getNombreUsuario(), usuarioSess.getIdSesionUsuario());
        	  if(usuariosPorRol!=null&&!usuariosPorRol.isEmpty() ){
        		destinatariosPorRol=new ArrayList<String>();
        		for(Usuario usuario:usuariosPorRol){
        		    if(!UtileriasWeb.esCadenaVacia(usuario.getEmail())){
        			destinatariosPorRol.add(usuario.getEmail());
        		    }
        		}
        		if(!destinatariosPorRol.isEmpty()){
        		    destinatarios.addAll(destinatariosPorRol);
        		}
        	 }
	    }
	    if(!destinatarios.isEmpty()){
	      sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
	    }
	}
	
	public static  void enviarCorreosPorRoles(List<String> roles,String titulo, String contenido,Usuario usuarioSess) throws ExcepcionDeAccesoADatos, SystemException{
	    List<Usuario> usuariosPorRol;
	    List<String> destinatarios= new ArrayList<String>();
	    List<String> destinatariosPorRol;
	    if(roles==null || roles.isEmpty()){
		return;
	    }
	    for(String rol:roles){
    	       usuariosPorRol = UsuarioWSDN.getInstancia().obtieneUsuariosSinRolesPorNombreRol(rol, usuarioSess.getNombreUsuario(), usuarioSess.getIdSesionUsuario());
        	  if(usuariosPorRol!=null&&!usuariosPorRol.isEmpty() ){
        		destinatariosPorRol=new ArrayList<String>();
        		for(Usuario usuario:usuariosPorRol){
        		    if(!UtileriasWeb.esCadenaVacia(usuario.getEmail())){
        			destinatariosPorRol.add(usuario.getEmail());
        		    }
        		}
        		if(!destinatariosPorRol.isEmpty()){
        		    destinatarios.addAll(destinatariosPorRol);
        		}
        	 }
	    }
	    if(!destinatarios.isEmpty()){
	      sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
	    }
	}
	
	/**
	 * Envia un correo sencillo de notificacion a los correos de los administradores de Midas 
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 */
	public static void enviaCorreoNotificacionAdminsMidas(String titulo, String contenido) {
		
		List<String> destinatarios = new ArrayList<String>();
		destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_MIDAS);
			
		Date fecha = new Date();
		
		contenido = contenido + " - Registrado el " + fecha;
		
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
	}
	/**
	 * Envia un correo sencillo de notificacion a los correos de los administradores de Afirme
	 * @param titulo Titulo del correo
	 * @param contenido Contenido del correo
	 */
	public static void enviaCorreoNotificacionAdminsAfirme(String titulo, String contenido) {
		
		List<String> destinatarios = new ArrayList<String>();
		destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_AFIRME);
		
		Date fecha = new Date();
		
		contenido = contenido + " - Registrado el " + fecha;
		
		sendMessage(destinatarios, titulo, contenido, emailFromAddress, null, null);
	}
	
	public static void enviaCorreoConTemplate(List<String> destinatarios,String titulo, String contenido, String dirigidoA,List<ByteArrayAttachment> fileAttachments) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> <title>"+titulo+"</title>");
		buffer.append("<style type=\"text/css\"> .txt_mail { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #666666; text-align: left;}</style> </head>");
		buffer.append("<body><table width=\"615\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr><td height=\"18\"></td><td width=\"593\">&nbsp;</td><td height=\"18\"></td></tr>");
		buffer.append("<tr><td width=\"12\" height=\"12\" background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_left1.jpg\"></td><td background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_line1.jpg\"></td><td width=\"12\" height=\"12\" background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_right1.jpg\"></td></tr>");
		buffer.append("<tr><td width=\"10\" height=\"150\" background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_line3.jpg\"></td><td width=\"593\" valign=\"top\"><table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
		buffer.append("<tr><td width=\"100%\" valign=\"bottom\"><img src=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "Cintillo_Seguros.jpg\" width=\"600\" height=\"50\" /></td></tr><tr><td height=\"10\" colspan=\"2\"></td></tr><tr><td colspan=\"2\"><img src=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "line_green.gif\" width=\"600\" height=\"4\" /></td></tr><tr>");
		buffer.append("<td colspan=\"2\"><table width=\"100%\" border=\"0\" cellspacing=\"0\">");
		buffer.append("<tr>");
		buffer.append("<td valign=\"top\" class=\"txt_mail\">");
		buffer.append("<p><br />Estimado:<b><span style=\"color: #333333\">"+dirigidoA+"</span></b></p>");            
		buffer.append("<p>"+contenido+"</p>");                          
		buffer.append("<br />");                                                   
		buffer.append("<br />");
		buffer.append("<p> Atentamente,");
		buffer.append("<br />Seguros Afirme<br />");
		buffer.append("<br />");
		buffer.append("</p></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td width=\"77%\"></td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("<td width=\"12\" height=\"150\" valign=\"top\" background=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_line4.jpg\"></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td width=\"10\"><img src=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_left2.jpg\" width=\"12\" height=\"12\" /></td>");
		buffer.append("<td background=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_line2.jpg\"></td>");
		buffer.append("<td width=\"12\"><img src=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_right2.jpg\" width=\"12\" height=\"12\" /></td>");
		buffer.append("</tr>");
		buffer.append("</table>");
		buffer.append("</body>");
		buffer.append("</html>");		
		if (fileAttachments == null || fileAttachments.isEmpty()){
			MailAction.enviaCorreo(destinatarios, titulo, buffer.toString());
		}else{
			MailAction.enviaCorreo(destinatarios, titulo, buffer.toString(), fileAttachments);
		}
		
	}
	
	public static void enviaCorreoConTemplate(List<String> destinatarios,String titulo, String contenido, Boolean siniestro){
		StringBuffer buffer = new StringBuffer();
		buffer.append("<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> <title>"+titulo+"</title>");
		buffer.append("<style type=\"text/css\"> .txt_mail { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #666666; text-align: left;}</style> </head>");
		buffer.append("<body><table width=\"615\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr><td height=\"18\"></td><td width=\"593\">&nbsp;</td><td height=\"18\"></td></tr>");
		buffer.append("<tr><td width=\"12\" height=\"12\" background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_left1.jpg\"></td><td background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_line1.jpg\"></td><td width=\"12\" height=\"12\" background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_right1.jpg\"></td></tr>");
		buffer.append("<tr><td width=\"10\" height=\"150\" background=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "e_line3.jpg\"></td><td width=\"593\" valign=\"top\"><table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
		buffer.append("<tr><td width=\"100%\" valign=\"bottom\"><img src=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "Cintillo_Seguros.jpg\" width=\"630\" height=\"50\" /></td></tr><tr><td height=\"10\" colspan=\"2\"></td></tr><tr><td colspan=\"2\"><img src=\""
						+ Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE
						+ "line_green.gif\" width=\"630\" height=\"4\" /></td></tr><tr>");
		buffer.append("<td colspan=\"2\"><table width=\"630\" border=\"0\" cellspacing=\"0\">");
		buffer.append("<tr>");
		buffer.append("<td valign=\"top\" class=\"txt_mail\">");
		buffer.append("<br />");
		buffer.append("<center><b>NOTIFICACION REPORTE DE SINIESTRO</b></center>");
		buffer.append("<br />");
		buffer.append("<p align='right'>MONTERREY, N.L. A "+UtileriasWeb.getFechaStringConNombreMes(new Date())+"</p>");  
		buffer.append("<p>"+contenido+"</p>");                          
		buffer.append("<br />");                                                   
		buffer.append("<br />");
		buffer.append("<p> Atentamente,");
		buffer.append("<br />Seguros AFIRME, S.A. DE C.V.<br />");
		buffer.append("<br />AFIRME Grupo Financiero<br />");
		buffer.append("<br />");
		buffer.append("</p></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td width=\"77%\"></td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("<td width=\"12\" height=\"150\" valign=\"top\" background=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_line4.jpg\"></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td width=\"10\"><img src=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_left2.jpg\" width=\"12\" height=\"12\" /></td>");
		buffer.append("<td background=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_line2.jpg\"></td>");
		buffer.append("<td width=\"12\"><img src=\""+Sistema.DIRECCION_IMAGENES_MAIL_TEMPLATE+"e_right2.jpg\" width=\"12\" height=\"12\" /></td>");
		buffer.append("</tr>");
		buffer.append("</table>");
		buffer.append("</body>");
		buffer.append("</html>");		
		if ( siniestro ){
			MailAction.enviaCorreo(destinatarios, titulo, buffer.toString(), "siniestrosdanos@afirme.com");
		}else{
			MailAction.enviaCorreo(destinatarios, titulo, buffer.toString());
		}
		
	}
}