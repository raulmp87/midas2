package mx.com.afirme.midas.sistema;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public abstract class MidasBaseReporte {
	/**
	 * Este metodo permite obtener un Reporte Compilado apartir de una plantilla
	 * JRXML localizada en el algun paquete del proyecto.
	 * 
	 * @param String
	 *            sourceFileName es la plantilla que se pretende compilar
	 * @return JasperReport el reporte Jasper compilado
	 */
	protected JasperReport getJasperReport(String sourceFileName) {
		JasperReport jasperReport = null;
		try {
			// InputStream input = new FileInputStream(new
			// File(sourceFileName));
			InputStream input = MidasBaseReporte.class
					.getResourceAsStream(sourceFileName);
			JasperDesign design = JRXmlLoader.load(input);
			jasperReport = JasperCompileManager.compileReport(design);
		} catch (JRException e) {
			LogDeMidasWeb.log(
					"Error to generate report... producer JRException ",
					Level.WARNING, e);
		}
		return jasperReport;
	}
	
	/**
	 * Genera un documento pdf, xls ó rtf a partir de una plantilla jrxml, un conjunto de parámetros y la lista de objetos que conforma
	 * el contenido del reporte; y lo devuelve como un arreglo de bytes
	 * @param tipoReporte. Daos permitidos: "application/pdf", "application/vnd.ms-excel", "application/vnd.ms-word". Si recibe null o ""
	 * se utiliza el valor por default, que es: "application/pdf"
	 * @param sourceFileName. nombre completo de la plantilla JRXML, incluyendo su ubicación dentro de los paquetes fuente del proyecto.
	 * @param parametrosReporte. Mapa de parámetros que se utilizarán para poblar el reporte.
	 * @param listaObjetos. Lista de objetos con los cuales se poblará el contenido del reporte.
	 * @return byte[] arreglo de bytes que corresponde al reporte.
	 * @throws JRException.
	 */
	public byte[] generaReporte(String tipoReporte,String sourceFileName,Map<String,Object> parametrosReporte,List<Object> listaObjetos) throws JRException{
		InputStream input = MidasBaseReporte.class.getResourceAsStream(sourceFileName);
		JasperDesign design = JRXmlLoader.load(input);
		JasperReport jasperReport = JasperCompileManager.compileReport(design);
		
		JasperPrint jasperPrint;
		if (listaObjetos == null)
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte);
		else{
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listaObjetos);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte, dataSource);
		}

		if (UtileriasWeb.esCadenaVacia(tipoReporte))
			tipoReporte = Sistema.TIPO_PDF;
		if (tipoReporte.equals(Sistema.TIPO_PDF)) {
			return this.generarReportePDF(jasperPrint);
		} else if (tipoReporte.equals(Sistema.TIPO_XLS)) {
			return this.generarReporteXLS(jasperPrint);
		} else if (tipoReporte.equals(Sistema.TIPO_DOC)) {
			return this.generarReporteRTF(jasperPrint);
		}
		else throw new JRException("tipo de reporte no válido.");
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param JRDataSource
	 *            jrDataSource es el dataSource de la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, JRDataSource jrDataSource) throws JRException {
		JasperPrint jasperPrint;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, jrDataSource);
			if (reportType.equals(Sistema.TIPO_PDF)) {
				return this.generarReportePDF(jasperPrint);
			} else if (reportType.equals(Sistema.TIPO_XLS)) {
				return this.generarReporteXLS(jasperPrint);
			} else if (reportType.equals(Sistema.TIPO_DOC)) {
				return this.generarReporteRTF(jasperPrint);
			}
			return null;
		} catch (JRException e) {
			LogDeMidasWeb.log(
					"Error to generate report... producer JRException ",
					Level.WARNING, e);
			throw e;
		}
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param Connection
	 *            connection es la conexion que utilizara la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, Connection connection) throws JRException {
		try{
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, connection);
			if (reportType.equals(Sistema.TIPO_PDF)) {
				return this.generarReportePDF(jasperPrint);
			} else if (reportType.equals(Sistema.TIPO_XLS)) {
				return this.generarReporteXLS(jasperPrint);
			} else if (reportType.equals(Sistema.TIPO_DOC)) {
				return this.generarReporteRTF(jasperPrint);
			}
			return null;
		}catch(Exception e){
			System.out.println("Error en sistema.MidasBaseReporte.generaReporte ==> " + e.getMessage());
			return null;
		}
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param JRResultSetDataSource
	 *            set es el resultSet con datos que se utilizaran para pintar el
	 *            contenido de la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, JRResultSetDataSource set) throws JRException {
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
				parameters, set);
		if (reportType.equals(Sistema.TIPO_PDF)) {
			return this.generarReportePDF(jasperPrint);
		} else if (reportType.equals(Sistema.TIPO_XLS)) {
			return this.generarReporteXLS(jasperPrint);
		} else if (reportType.equals(Sistema.TIPO_DOC)) {
			return this.generarReporteRTF(jasperPrint);
		}
		return null;
	}

	/**
	 * Este metodo genera un reporte apartir de una platilla JRXML y lo regresa
	 * como un arreglo de bites
	 * 
	 * @param String
	 *            reportType es el tipo de reporte que se pretende generar,
	 *            puden ser PDF, RTF o XLS
	 * @param JasperReport
	 *            jasperReport es el reporte compilado que se tranformara
	 * @param Map
	 *            <?, ?> parameters son los parametros que necesita la platilla
	 * @param JRBeanCollectionDataSource
	 *            dataSource es el Arreglo de Beans que se utilizaran para
	 *            pintar el contenido de la plantilla
	 * @return JasperReport el reporte Jasper compilado
	 */
	public byte[] generaReporte(String reportType, JasperReport jasperReport,
			Map<?, ?> parameters, JRBeanCollectionDataSource dataSource)
			throws JRException {
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
				parameters, dataSource);
		if (reportType.equals(Sistema.TIPO_PDF)) {
			return this.generarReportePDF(jasperPrint);
		} else if (reportType.equals(Sistema.TIPO_XLS)) {
			return this.generarReporteXLS(jasperPrint);
		} else if (reportType.equals(Sistema.TIPO_DOC)) {
			return this.generarReporteRTF(jasperPrint);
		}
		return null;
	}

	protected byte[] generarReportePDF(JasperPrint jasperPrint)
			throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRPdfExporter pdfExporter = new JRPdfExporter();
		pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		pdfExporter.exportReport();
		return outputStream.toByteArray();
	}

	protected byte[] generarReporteXLS(JasperPrint jasperPrint) throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXlsExporter xlsExporter = new JRXlsExporter();
		xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,outputStream);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
		xlsExporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
		xlsExporter.exportReport();
		return outputStream.toByteArray();
	}

	protected byte[] generarReporteRTF(JasperPrint jasperPrint)
			throws JRException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRRtfExporter rtfExporter = new JRRtfExporter();
		rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		rtfExporter.exportReport();
		return outputStream.toByteArray();
	}
}