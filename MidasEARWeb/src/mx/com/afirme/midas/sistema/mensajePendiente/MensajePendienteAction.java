package mx.com.afirme.midas.sistema.mensajePendiente;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.danios.reportes.reportemonitoreosol.ReporteMonitoreoSolicitudesDN;
import mx.com.afirme.midas.danios.reportes.reportemonitoreosol.ReporteMonitoreoSolicitudesDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.interfaz.agente.tipocedula.TipoCedulaDTO;
import mx.com.afirme.midas.interfaz.centroemisor.CentroEmisorDN;
import mx.com.afirme.midas.interfaz.centroemisor.CentroEmisorDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.codigopostalcolonia.CodigoPostalColoniaDN;
import mx.com.afirme.midas.interfaz.codigopostalcolonia.CodigoPostalColoniaDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoDN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.interfaz.moneda.MonedaDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDN;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.tareas.endoso.TareaCancelacionEndosoAutomatica;
import mx.com.afirme.midas.wsCliente.emision.EmisionDN;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class MensajePendienteAction extends MidasMappingDispatchAction {

	public void listarPendientesDanios(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		Object obj = request.getSession().getAttribute("usuarioMIDAS");
		List<MensajePendienteDTO> mensajes = new ArrayList<MensajePendienteDTO>();
		if(obj != null) {
			/*String idsRoles = ((Usuario)obj).getRoles().toString();
			MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
			mensajes = mensajePendienteDN.listarPendientesDanios(idsRoles.substring(1, idsRoles.length() - 1));*/
		}
		String idsRoles = "[0,1,2]";
		MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
		mensajes = mensajePendienteDN.listarPendientesDanios(idsRoles.substring(1, idsRoles.length() - 1));
//		String json = "{rows:[";
//		if(mensajes != null && mensajes.size() > 0) {
//			for(MensajePendienteDTO mensaje : mensajes) {
//				json += "{id:\"" + mensaje.getIdToMensajePendiente() + "\",data:[\"";
//				json += DateFormat.getDateInstance(DateFormat.SHORT).format(mensaje.getFechaCreacion()) + "\",\"";
//				json += mensaje.getDescripcionMensaje() + "^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;" + mensaje.getUrl();
//				if (mensaje.getParametro1() != null) {
//					json += "?" +  mensaje.getParametro1();
//					if(mensaje.getParametro2() != null) {
//						json += "&" + mensaje.getParametro2();
//						if(mensaje.getParametro3() != null) {
//							json += "&" + mensaje.getParametro3();
//						}
//					}
//				}
//				json += "&#39;,&#39;contenido&#39;,";
//				json += mensaje.getSiguienteFuncion() != null? "&#39;" + mensaje.getSiguienteFuncion() + "&#39;" : "null";
//				json += ");^_self\",";
//				json += "\"Remover^javascript:daniosProcessor.setUpdated(" + mensaje.getIdToMensajePendiente() + ",true,&#39;deleted&#39;);daniosProcessor.sendData(" + mensaje.getIdToMensajePendiente() + ");^_self\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(mensajes != null && mensajes.size() > 0) {
			StringBuilder parametros = new StringBuilder("");
			for(MensajePendienteDTO mensaje : mensajes) {
				parametros.delete(0,parametros.length());
				
				if (mensaje.getParametro1() != null) {
					parametros.append("?").append(mensaje.getParametro1());
					if(mensaje.getParametro2() != null) {
						parametros.append("&").append(mensaje.getParametro2());
						if(mensaje.getParametro3() != null) {
							parametros.append("&").append(mensaje.getParametro3());
						}
					}
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(mensaje.getIdToMensajePendiente().toString());
				row.setDatos(
						DateFormat.getDateInstance(DateFormat.SHORT).format(mensaje.getFechaCreacion()),
						mensaje.getDescripcionMensaje() + "^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;" + mensaje.getUrl()+
						parametros.toString() + "&#39;,&#39;contenido&#39;," +
						 (mensaje.getSiguienteFuncion() != null? "&#39;" + mensaje.getSiguienteFuncion() + "&#39;" : "null") + ");^_self",
						"Remover^javascript:daniosProcessor.setUpdated(" + mensaje.getIdToMensajePendiente() + ",true,&#39;deleted&#39;);daniosProcessor.sendData(" + mensaje.getIdToMensajePendiente() + ");^_self"
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void listarPendientesReaseguro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		Object obj = request.getSession().getAttribute("usuarioMIDAS");
		List<MensajePendienteDTO> mensajes = new ArrayList<MensajePendienteDTO>();
		if(obj != null) {
			/*String idsRoles = ((Usuario)obj).getRoles().toString();
			MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
			mensajes = mensajePendienteDN.listarPendientesReaseguro(idsRoles.substring(1, idsRoles.length() - 1));*/
		}
		String idsRoles = "[0,1,2]";
		MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
		mensajes = mensajePendienteDN.listarPendientesReaseguro(idsRoles.substring(1, idsRoles.length() - 1));
//		String json = "{rows:[";
//		if(mensajes != null && mensajes.size() > 0) {
//			for(MensajePendienteDTO mensaje : mensajes) {
//				json += "{id:\"" + mensaje.getIdToMensajePendiente() + "\",data:[\"";
//				json += DateFormat.getDateInstance(DateFormat.SHORT).format(mensaje.getFechaCreacion()) + "\",\"";
//				json += mensaje.getDescripcionMensaje() + "^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;" + mensaje.getUrl();
//				if (mensaje.getParametro1() != null) {
//					json += "?" +  mensaje.getParametro1();
//					if(mensaje.getParametro2() != null) {
//						json += "&" + mensaje.getParametro2();
//						if(mensaje.getParametro3() != null) {
//							json += "&" + mensaje.getParametro3();
//						}
//					}
//				}
//				json += "&#39;,&#39;contenido&#39;,";
//				json += mensaje.getSiguienteFuncion() != null? "&#39;" + mensaje.getSiguienteFuncion() + "&#39;" : "null";
//				json += ");^_self\",";
//				json += "\"Remover^javascript:reaseguroProcessor.setUpdated(" + mensaje.getIdToMensajePendiente() + ",true,&#39;deleted&#39;);reaseguroProcessor.sendData(" + mensaje.getIdToMensajePendiente() + ");^_self\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(mensajes != null && mensajes.size() > 0) {
			StringBuilder parametros = new StringBuilder("");
			for(MensajePendienteDTO mensaje : mensajes) {
				parametros.delete(0,parametros.length());
				if (mensaje.getParametro1() != null) {
					parametros.append("?").append(mensaje.getParametro1());
					if(mensaje.getParametro2() != null) {
						parametros.append("&").append(mensaje.getParametro2());
						if(mensaje.getParametro3() != null) {
							parametros.append("&").append(mensaje.getParametro3());
						}
					}
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(mensaje.getIdToMensajePendiente().toString());
				row.setDatos(
						DateFormat.getDateInstance(DateFormat.SHORT).format(mensaje.getFechaCreacion()),
						mensaje.getDescripcionMensaje() + "^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;" + mensaje.getUrl()+
						parametros.toString() + "&#39;,&#39;contenido&#39;," +
						 (mensaje.getSiguienteFuncion() != null? "&#39;" + mensaje.getSiguienteFuncion() + "&#39;" : "null") + ");^_self",
						"Remover^javascript:reaseguroProcessor.setUpdated(" + mensaje.getIdToMensajePendiente() + ",true,&#39;deleted&#39;);reaseguroProcessor.sendData(" + mensaje.getIdToMensajePendiente() + ");^_self"
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}

	public void listarPendientesSiniestros(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		Object obj = request.getSession().getAttribute("usuarioMIDAS");
		List<MensajePendienteDTO> mensajes = new ArrayList<MensajePendienteDTO>();
		if(obj != null) {
			/*String idsRoles = ((Usuario)obj).getRoles().toString();
			MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
			mensajes = mensajePendienteDN.listarPendientesSiniestros(idsRoles.substring(1, idsRoles.length() - 1));*/
		}
		String idsRoles = "[0,1,2]";
		MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
		mensajes = mensajePendienteDN.listarPendientesSiniestros(idsRoles.substring(1, idsRoles.length() - 1));
//		String json = "{rows:[";
//		if(mensajes != null && mensajes.size() > 0) {
//			for(MensajePendienteDTO mensaje : mensajes) {
//				json += "{id:\"" + mensaje.getIdToMensajePendiente() + "\",data:[\"";
//				json += DateFormat.getDateInstance(DateFormat.SHORT).format(mensaje.getFechaCreacion()) + "\",\"";
//				json += mensaje.getDescripcionMensaje() + "^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;" + mensaje.getUrl();
//				if (mensaje.getParametro1() != null) {
//					json += "?" +  mensaje.getParametro1();
//					if(mensaje.getParametro2() != null) {
//						json += "&" + mensaje.getParametro2();
//						if(mensaje.getParametro3() != null) {
//							json += "&" + mensaje.getParametro3();
//						}
//					}
//				}
//				json += "&#39;,&#39;contenido&#39;,";
//				json += mensaje.getSiguienteFuncion() != null? "&#39;" + mensaje.getSiguienteFuncion() + "&#39;" : "null";
//				json += ");^_self\",";
//				json += "\"Remover^javascript:siniestrosProcessor.setUpdated(" + mensaje.getIdToMensajePendiente() + ",true,&#39;deleted&#39;);siniestrosProcessor.sendData(" + mensaje.getIdToMensajePendiente() + ");^_self\"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(mensajes != null && mensajes.size() > 0) {
			StringBuilder parametros = new StringBuilder("");
			for(MensajePendienteDTO mensaje : mensajes) {
				
				parametros.delete(0,parametros.length());
				if (mensaje.getParametro1() != null) {
					parametros.append("?").append(mensaje.getParametro1());
					if(mensaje.getParametro2() != null) {
						parametros.append("&").append(mensaje.getParametro2());
						if(mensaje.getParametro3() != null) {
							parametros.append("&").append(mensaje.getParametro3());
						}
					}
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(mensaje.getIdToMensajePendiente().toString());
				row.setDatos(
						DateFormat.getDateInstance(DateFormat.SHORT).format(mensaje.getFechaCreacion()),
						mensaje.getDescripcionMensaje() + "^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;" + mensaje.getUrl()+
						parametros.toString() + "&#39;,&#39;contenido&#39;," +
						 (mensaje.getSiguienteFuncion() != null? "&#39;" + mensaje.getSiguienteFuncion() + "&#39;" : "null") + ");^_self",
						"Remover^javascript:siniestrosProcessor.setUpdated(" + mensaje.getIdToMensajePendiente() + ",true,&#39;deleted&#39;);siniestrosProcessor.sendData(" + mensaje.getIdToMensajePendiente() + ");^_self"
				);
				json.addRow(row);
				
			}
		}
		System.out.println(json);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
	}

	public void remover(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {

		String action = "";
		if(request.getParameter("!nativeeditor_status").equals("deleted")) {
			String id = request.getParameter("gr_id");
			MensajePendienteDN mensajePendienteDN = MensajePendienteDN.getInstancia();
			MensajePendienteDTO mensajePendienteDTO = mensajePendienteDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			mensajePendienteDN.atenderMensajePendiente(mensajePendienteDTO);
			action = "delete";
		}
		
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write("<data><action type=\"" + action + "\" sid=\"" + request.getParameter("gr_id") + "\" tid=\"" + request.getParameter("gr_id") + "\" /></data>");
		pw.flush();
		pw.close();
	}

	@SuppressWarnings("finally")
	public ActionForward mostrarAcerca(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
	    	boolean esAlterno=Boolean.FALSE;
	    	try {
	    		request.setAttribute("ambiente", Sistema.AMBIENTE_SISTEMA);
				try {
					Enumeration<URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
					while (resources.hasMoreElements()) {
						 Properties prop = new Properties();
						 prop.load(resources.nextElement().openStream());
						 if("MidasEARWeb".equals(prop.get("Application-Name"))){
							 request.setAttribute("version", prop.getProperty("Implementation-Version"));
							 break;
						 }
					}
				} catch (Exception ex) { 
					request.setAttribute("version", Sistema.VERSION_SISTEMA);
				}
				
	    		// Identifica el Servidor.
	    		String nombreServidor= null;
	    		nombreServidor= UtileriasWeb.obtenerNombreEIpDelServidor();
	    		System.out.println("Nombre del Servidor e IP " + nombreServidor );
	    		// Ponerlo en request
	    		request.setAttribute("nombreServidor",nombreServidor);
	    		System.out.println("----------------------------------------------");

	    		if(request.getParameter("accion")!=null){
	    			request.getSession().setAttribute("accion", request.getParameter("accion")) ;
	    			esAlterno=true;
	    		}
	    		if(request.getParameter("llavemagica")!=null){
	    			request.getSession().setAttribute("llavemagica", request.getParameter("llavemagica")) ;
	    		}
	    		if(request.getParameter("idControlArchivo")!=null){
	    			request.getSession().setAttribute("idControlArchivo", request.getParameter("idControlArchivo")) ;
	    		}
	    		if(request.getParameter("nuevonombre")!=null){
	    			request.getSession().setAttribute("nuevonombre", request.getParameter("nuevonombre")) ;
	    		}
	    		if(request.getParameter("idSolicitud")!=null){
	    			request.getSession().setAttribute("archivoIdToSolicitud", request.getParameter("idSolicitud")) ;

	    		}
	    		if(request.getParameter("idCotizacion")!=null){
	    			request.getSession().setAttribute("archivoIdToCotizacion", request.getParameter("idCotizacion")) ;
	    		}

	    	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return esAlterno?mapping.findForward(Sistema.ALTERNO):mapping.findForward(Sistema.EXITOSO);
		}
		
	}
	
	@SuppressWarnings("finally")
	public ActionForward operacionesAlternas(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response){
	   String parametroAction=null;
	   String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
	   request.getSession(true).removeAttribute("ejecutaAgregar");
	   TareaCancelacionEndosoAutomatica tareaCancelacionEndosoAutomatica;
	   
	    try{//Acciones varias para Sistema
		    parametroAction=(String)request.getSession().getAttribute("accion");
			if(parametroAction!=null){
			    
			    if(Sistema.ACCION_VERIFICARINTERFAZ.equalsIgnoreCase(parametroAction.trim())){
				/* Metodo par verificar llamados a procedimientos almacenados de Seycos (Interfaces) de consulta de datos */
				this.verificaInterfaz(nombreUsuario);
			    }else{ 
				if(request.getSession().getAttribute("llavemagica") !=null &&  Sistema.LLAVE_MAGICA.equalsIgnoreCase((String)request.getSession().getAttribute("llavemagica"))){
				    this.accionesConArchivos(mapping,form,request, response,parametroAction);
				}
			    }
			    if(Sistema.ACCION_EJECUTACANCELACIONESAUTOMATICAS.equalsIgnoreCase(parametroAction.trim())) {
			    	try {
			    		System.out.println("Ejecutando  Cancelacion de Endosos Automatica. Ejecucion Manual: " );
				       
				        tareaCancelacionEndosoAutomatica = new TareaCancelacionEndosoAutomatica();
				        tareaCancelacionEndosoAutomatica.cancelaEndosoPendienteEjecucionManual();
				        System.out.println("Concluyo  Cancelacion de Endosos Automatica. Ejecucion Manual: " );
				    	
				    } catch (Exception ex) {
				    	System.out.println("Excepcion encontrada mientras se ejecutaba la  Cancelacion de Endosos Automatica. Ejecucion Manual: " );
				    }
			    	
			    }
			    if(Sistema.RENOMBRAARCHIVOPDF.equalsIgnoreCase(parametroAction.trim())) {
			    	try {
			    		System.out.println("Ejecutando  RENOMBRAR ARCHIVO pdf . Ejecucion Manual: " );
			    		String uploadFolder;
			    		if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1)
			    			uploadFolder = Sistema.UPLOAD_FOLDER;
			    		else
			    			uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
			    		String nombreArchivo = request.getParameter("NOMBREARCHIVOPDF");
			    		File archivoAcambiar= new File(uploadFolder + nombreArchivo);
			    		String nombreArchivoNuevo = nombreArchivo + "OLD";
			    		File archivoNuevo = new File(uploadFolder + nombreArchivoNuevo);
			    		if (archivoAcambiar.exists()){
			    			archivoAcambiar.renameTo(archivoNuevo);
			    		
						    	
			    		}
				    	
				    } catch (Exception ex) {
				    	System.out.println("Excepcion encontrada mientras se ejecutaba EL  RENOMBRAR ARCHIVO pdf. Ejecucion Manual: " );
				    }
			    	
			    }
			    
			    
			 }
			
	    	} catch (ExcepcionDeAccesoADatos e) {
		e.printStackTrace();
        	} catch (Exception e) {
        		e.printStackTrace();
        	} finally {
            	    	request.getSession().removeAttribute("accion") ;
            	    	request.getSession().removeAttribute("llavemagica");
            	    	request.getSession().removeAttribute("idControlArchivo");
            	    	request.getSession().removeAttribute("nuevonombre");
            	    	return mapping.findForward(Sistema.EXITOSO);
        	}
	}
	
	
	
	
	private void verificaInterfaz(String nombreUsuario){
		LogDeMidasWeb.log("Entrando a Acerca de...  (Bloque de codigo para probar las Interfaces con Seycos)", Level.INFO, null);
		// Prueba de envio de emails.
	      	List<String> destinatarios = new ArrayList<String>();
		destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_MIDAS);
		System.out.println("Verificaci�n de envio de Email - al administrador : " + Sistema.EMAIL_ADMINISTRADOR_MIDAS);
		MailAction.enviaCorreo(destinatarios, "Verificacion de Envio de email desde AcercaDe", "Se ha verificado el envio de email..desde la opcion de AcercaDe.");
	
		System.out.println("----------------------------------------------");
		
		// Verifica Ruta de Archivos.
		String uploadFolder;
		String OSName = System.getProperty("os.name");
		if (OSName.toLowerCase().indexOf("windows")!=-1)
			uploadFolder = Sistema.UPLOAD_FOLDER;
		else
			uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;

		System.out.println("Verifica ruta de archivos " + uploadFolder);
		
		System.out.println("----------------------------------------------");
		
//		Clientes
		
		String apellidoPaterno = "ALONSO";
		ClienteDTO clienteFiltro = new ClienteDTO();
		clienteFiltro.setApellidoPaterno(apellidoPaterno);
		List<ClienteDTO> clienteList = null;
		BigDecimal idCliente = null;
		try {
			clienteList = ClienteDN.getInstancia().listarFiltrado(clienteFiltro, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Interfaz - Listado de Clientes filtrado por apellido paterno = " + apellidoPaterno);
		
		if (clienteList != null) {
			for (ClienteDTO cliente : clienteList) {
				if (idCliente == null) {
					idCliente = cliente.getIdCliente();
				}
				
				System.out.println("Interfaz - Cliente: " + cliente.getNombre() + " " + cliente.getApellidoPaterno() + " " + cliente.getApellidoMaterno() +  
						" RFC: " + cliente.getCodigoRFC() + " Domicilio: " + cliente.getDireccionCompleta());
			}
		}
		System.out.println("----------------------------------------------");
		if (idCliente != null) {
			ClienteDTO cliente = null;
			try {
				cliente = ClienteDN.getInstancia().verDetalleCliente(idCliente, nombreUsuario);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.out.println("Interfaz - Detalle de cliente con id = " + idCliente);
			if (cliente != null) {
				System.out.println("Interfaz - Cliente: " + cliente.getNombre() + " " + cliente.getApellidoPaterno() + " " + cliente.getApellidoMaterno() +  
						" RFC: " + cliente.getCodigoRFC());
			} else {
				System.out.println("No regreso detalle del cliente!");
			}
			
			System.out.println("----------------------------------------------");
		}
		
//		Agentes
		
		Usuario usuario = new Usuario();
		usuario.setNombreUsuario("KMGONVAZ");
		List<AgenteDTO> agenteList = null;
		Integer idAgente = null;
		try {
			agenteList = AgenteDN.getInstancia().getAgentesPorUsuario(usuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Interfaz - Listado de Agentes usuario KMGONVAZ");
		
		if (agenteList != null) {
			for (AgenteDTO agente : agenteList) {
				if (idAgente == null) {
					idAgente = agente.getIdTcAgente();
				}
				System.out.println("Interfaz - Agente: " + agente.getNombre() + " RFC: " + agente.getRfc());
			}
		} else {
			System.out.println("No regreso agentes!");
		}
		
		System.out.println("----------------------------------------------");
					
		if (idAgente != null) {
			AgenteDTO agenteFiltro = new AgenteDTO();
			agenteFiltro.setIdTcAgente(idAgente);
			AgenteDTO agente = null;
			try {
				agente = AgenteDN.getInstancia().verDetalleAgente(agenteFiltro, nombreUsuario);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.out.println("Interfaz - Detalle de Agente id : " + idAgente);
			if (agente != null) {
				System.out.println("Interfaz - Agente: " + agente.getNombre() + " RFC: " + agente.getRfc());
			} else {
				System.out.println("No regreso detalle del agente!");
			}
			
			System.out.println("----------------------------------------------");
		}
		
//		Tipo de Cedula
		List<TipoCedulaDTO> tipoCedulaList = null;
		try {
			tipoCedulaList = mx.com.afirme.midas.interfaz.agente.AgenteDN.getInstancia()
					.listarTiposCedula(nombreUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}	
		System.out.println("Interfaz - Listado de Tipos de Cedula :");
		
		if (tipoCedulaList != null) {
			
			for (TipoCedulaDTO tipoCedula : tipoCedulaList) {
				System.out.println("Interfaz - Tipo de Cedula: " + tipoCedula.getCedula() + " / " + tipoCedula.getDescripcion());
			}
		} else {
			System.out.println("No regreso tipos de cedula!");
		}
		System.out.println("----------------------------------------------");
			
//		Ejecutivos
		
		List<AgenteDTO> ejecutivoList = null;
		String idOficina = null;
		System.out.println("Interfaz - Listado de Ejecutivos");
		try {
			ejecutivoList = AgenteDN.getInstancia().listaEjecutivos(nombreUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		if (ejecutivoList != null) {
			for (AgenteDTO ejecutivo : ejecutivoList) {
				if (idOficina == null) {
					idOficina = ejecutivo.getIdOficina();
				}
				System.out.println("Interfaz - Ejecutivo: " + ejecutivo.getNombre() + " RFC: " +
						ejecutivo.getRfc() + ", idOficina: " + ejecutivo.getIdOficina());
			}
		} else {
			System.out.println("No regreso ejecutivos!");
		}
		
		System.out.println("----------------------------------------------");
	
		
//		Agentes de Ejecutivo
		
		if (idOficina != null) {
			List<AgenteDTO> ejecutivoAgenteList = null;
			System.out.println("Interfaz - Listado de Agentes de Ejecutivo de idOficina = " + idOficina);
			try {
				ejecutivoAgenteList = AgenteDN.getInstancia().listaEjecutivoAgentes(idOficina, nombreUsuario);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			
			if (ejecutivoAgenteList != null) {
				for (AgenteDTO agente : ejecutivoAgenteList) {
					System.out.println("Interfaz - Agentes de Ejecutivo de idOficina = " + idOficina  + ": "+ agente.getNombre() + " RFC: " +
							agente.getRfc());
				}
			} else {
				System.out.println("No regreso Agentes de Ejecutivo de idOficina = " + idOficina + "!");
			}
			
			System.out.println("----------------------------------------------");
			
		}
	
//		Interfaz de listado de formas de pago
		
		List<FormaPagoIDTO> formaPagoList = null;
		List<FormaPagoIDTO> formaPagoFiltradoList = null;
		Integer filtroIdFormaPago = null;
		String filtroDescripcionFormaPago = null;
		
		try {
			formaPagoList = FormaPagoDN.getInstancia(nombreUsuario).listarTodos((short)Sistema.MONEDA_PESOS);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Interfaz - Listado de Forma de pago (Todos)");
		if (formaPagoList != null) {
			for (FormaPagoIDTO formaPago : formaPagoList) {
				if (filtroIdFormaPago == null) {
					filtroIdFormaPago = formaPago.getIdFormaPago();
				}
				System.out.println("Interfaz - Forma de pago: " + formaPago.getDescripcion() + 
						" PorcentajeRecargoPagoFraccionado: " + formaPago.getPorcentajeRecargoPagoFraccionado() + " %" +
						" numero de recibos generados : " + formaPago.getNumeroRecibosGenerados());
			}
		} else {
			System.out.println("No regreso Formas de pago!");
		}
		System.out.println("----------------------------------------------");
		
		if (filtroIdFormaPago != null) {
			try {
				formaPagoFiltradoList = FormaPagoDN.getInstancia(nombreUsuario)
					.listarFiltrado(filtroIdFormaPago, filtroDescripcionFormaPago, (short)Sistema.MONEDA_DOLARES);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.out.println("Interfaz - Listado de Forma de pago (Filtrados) por id :" + filtroIdFormaPago + " y descripcion : " + filtroDescripcionFormaPago);
			
			if (formaPagoFiltradoList != null) {
				for (FormaPagoIDTO formaPago : formaPagoFiltradoList) {
					System.out.println("Interfaz - Forma de pago: " + formaPago.getDescripcion());
				}
			} else {
				System.out.println("No regreso Formas de pago filtradas!");
			}
			System.out.println("----------------------------------------------");
		}
		
//		Interfaz de Moneda
		
		List<MonedaDTO> monedaList = null;
		
		try {
			monedaList = MonedaDN.getInstancia(nombreUsuario).listarTodos();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Interfaz - Listado de Moneda (Todos)");
		if (monedaList != null) {
			for (MonedaDTO moneda : monedaList) {
				System.out.println("Interfaz - Moneda: " + moneda.getDescripcion());
			}
		} else {
			System.out.println("No regreso Listado de monedas!");
		}
		System.out.println("----------------------------------------------");
		
		
//		Medio de Pago
		List<MedioPagoDTO> medioPagoList = null;
		Integer filtroIdMedioPago = null;
		try {
			medioPagoList = mx.com.afirme.midas.interfaz.mediopago.MedioPagoDN
				.getInstancia(nombreUsuario).listarTodos();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
		System.out.println("Interfaz - Listado de Medio de pago Todos:");
		
		if (medioPagoList != null) {
			
			for (MedioPagoDTO medioPago : medioPagoList) {
				if (filtroIdMedioPago == null) {
					filtroIdMedioPago = medioPago.getIdMedioPago();
				}
				System.out.println("Interfaz - Medio de pago: " + medioPago.getDescripcion());
			}
		} else {
			System.out.println("No regreso Medios de pago!");
		}
		System.out.println("----------------------------------------------");
		
//		Medio de Pago Filtrado		
		
		if (filtroIdMedioPago != null) {
			String filtroDescripcionMedioPago = null;
			List<MedioPagoDTO> medioPagoFiltradoList = null;
			try {
				medioPagoFiltradoList = mx.com.afirme.midas.interfaz.mediopago.MedioPagoDN
					.getInstancia(nombreUsuario).listarFiltrado(filtroIdMedioPago, filtroDescripcionMedioPago);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			System.out.println("Interfaz - Listado de Medio de pago (Filtrados) por id :" + filtroIdMedioPago +
					" y descripcion : " + filtroDescripcionMedioPago);
			
			if (medioPagoFiltradoList != null) {
				
				for (MedioPagoDTO medioPago : medioPagoFiltradoList) {
					System.out.println("Interfaz - Medio de pago: " + medioPago.getDescripcion());
				}
			} else {
				System.out.println("No regreso Medios de pago filtrados!");
			}
			System.out.println("----------------------------------------------");
		}
	
//		Consulta de recibos
		System.out.println("Consultando la primer poliza para consultar recibos...");
		BigDecimal filtroIdPoliza = null;
		PolizaDTO poliza;
		String llaveFiscal = null;
		byte[] archivoFactura = null;
		
		try {
		    	poliza = PolizaDN.getInstancia().buscaPrimerRegistro();
			if (poliza != null) {
        			System.out.println("Poliza encontrada.");
        			
        			List<ReciboDTO> reciboList = null;
        			try {
        				filtroIdPoliza = poliza.getIdToPoliza();
        				reciboList = ReciboDN.getInstancia(nombreUsuario)
        					.consultaRecibos(filtroIdPoliza);
        			} catch (Exception ex) {
        				ex.printStackTrace();
        			}
        			
        			System.out.println("Interfaz - Consulta de recibos con id de poliza:" + filtroIdPoliza);
        			if (reciboList != null) {
        				for (ReciboDTO recibo : reciboList) {
        					if (llaveFiscal == null) {
        						llaveFiscal = recibo.getLlaveFiscal();
        					}
        					System.out.println("Interfaz - Consulta de Recibo de poliza " + filtroIdPoliza + " : " + recibo.getLlaveFiscal());
        				}
        				
        			} else {
        				System.out.println("Fallo consulta de recibos!");
        			}
        		} else {
        			System.out.println("No se encontro ninguna poliza!");
        		}
			
			//Prueba de WebServices impresion
			if (llaveFiscal != null) {
				System.out.println("----------------------------------------------");
				System.out.println("Probando webServices de impresion factura de llave " +
							"Fiscal: " + llaveFiscal + ". URL:" + Sistema.URL_WS_IMPRESION + ", Puerto:"  
						+ Sistema.PUERTO_WS_IMPRESION);
				
				archivoFactura = EmisionDN.getInstancia().obtieneFactura(llaveFiscal);
				
				if (archivoFactura != null && archivoFactura.length > 0) {
					System.out.println("webServices de impresion factura Exitoso. Se obtuvo la factura de la llave " +
							"Fiscal: " + llaveFiscal + ". URL:" + Sistema.URL_WS_IMPRESION + ", Puerto:"  
							+ Sistema.PUERTO_WS_IMPRESION);
				} else {
					System.out.println("webServices de impresion factura Fallo. no se obtuvo la factura de la llave " +
							"Fiscal: " + llaveFiscal + ". URL:" + Sistema.URL_WS_IMPRESION + ", Puerto:"  
							+ Sistema.PUERTO_WS_IMPRESION);
				}
				
				
			}
				
		
		System.out.println("----------------------------------------------");
		} catch (SystemException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}	
	
	
//		Centro Emisor
		List<CentroEmisorDTO> centroEmisorList = null;
		try {
			centroEmisorList = CentroEmisorDN
				.getInstancia(nombreUsuario).listarCentrosEmisores();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		System.out.println("Interfaz - Listado de Centros Emisores. Usuario: " + nombreUsuario);
		
		if (centroEmisorList != null) {
			
			for (CentroEmisorDTO centroEmisor : centroEmisorList) {
				System.out.println("Interfaz - Centro Emisor: " + centroEmisor.getClave() + "/" + centroEmisor.getNombre());
			}
		} else {
			System.out.println("Fallo listado de centros emisores!");
		}
		System.out.println("----------------------------------------------");
		
		
//		Tipo de cambio (SP de Mizar)
		Double tipocambio;
		
		tipocambio = TipoCambioDN.getInstancia(nombreUsuario).obtieneTipoCambioPorDia(new Date(), (short)Sistema.MONEDA_PESOS);
		
		System.out.println("Interfaz - Tipo de cambio del dia : " + new Date() + " en MONEDA NACIONAL es de: " + tipocambio);
		
		tipocambio = TipoCambioDN.getInstancia(nombreUsuario).obtieneTipoCambioPorDia(new Date(), (short)Sistema.MONEDA_DOLARES);
		if(tipocambio!=null)
		System.out.println("Interfaz - Tipo de cambio del dia : " + new Date() + " en U.S. DOLARES es de: " + tipocambio);
		else System.out.println("Interfaz - Tipo de cambio del dia : " + new Date() + " en U.S. DOLARES es de: " + "No hay datos para el tipo de Cambio en Mizar");
		
		
		System.out.println("----------------------------------------------");
		
		
		System.out.println("Interfaz - Tipo de cambio PROMEDIO del mes : " + new Date() + " en MONEDA NACIONAL es de: " + tipocambio);
		Calendar fechaActual = Calendar.getInstance();		
		Integer mes = new Integer(fechaActual.get(Calendar.MONTH)+1);
		Integer anio = new Integer(fechaActual.get(Calendar.YEAR));
		tipocambio = TipoCambioDN.getInstancia(nombreUsuario).obtieneTipoCambioPorMes(mes, anio, (short)Sistema.MONEDA_DOLARES);
		if(tipocambio!=null)
		System.out.println("Interfaz - Tipo de cambio promedio del mes : " + mes + " en U.S. DOLARES es de: " + tipocambio);
		else System.out.println("Interfaz - Tipo de cambio del mes : " + mes + " en U.S. DOLARES es de: " + "No hay datos para el tipo de Cambio en Mizar");
		
		
		System.out.println("----------------------------------------------");
		
		
//		Estatus de Solicitud de Cheque
		BigDecimal idPago = null;
		System.out.println("Consultando la primer orden de pago para consultar estatus de sol. de cheque...");
		try{
		    OrdenDePagoDTO ordenPago = OrdenDePagoDN.getInstancia().buscaPrimerRegistro();
        		if (ordenPago != null) {
        			System.out.println("orden de pago encontrada.");
        			
        			String estatusSolicitud = null;
        			try {
        				idPago = ordenPago.getIdToOrdenPago();
        				estatusSolicitud = SolicitudChequeDN.getInstancia(nombreUsuario).consultaEstatusSolicitudCheque(idPago).getSituacionPago();
//        				estatusSolicitud = SinietroServiciosDN.getInstancia(nombreUsuario).consultaEstatusSolicitudCheque(idPago).getSituacionPago();
        			} catch (Exception ex) {
        				ex.printStackTrace();
        			}
        			if(estatusSolicitud != null){
        				System.out.println("Interfaz - El estatus de la solicitud de cheque con id : " + idPago + " es de: " + estatusSolicitud  );
        			}
        		} else {
        			System.out.println("No se encontro ninguna orden de pago!");
        		}
        		
        		System.out.println("----------------------------------------------");
		}catch (SystemException e) {}
		
//		Prestadores de Servicios
		
		
		List<PrestadorServiciosDTO> prestadorServiciosList = null;
		String nombrePrestadorServicios = null;
		BigDecimal idPrestadorServicios = null;
		try {
			prestadorServiciosList = PrestadorServiciosDN.getInstancia().listarPrestadores(nombrePrestadorServicios, nombreUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		System.out.println("Interfaz - Listado de Prestadores de Servicios con nombre: " + nombrePrestadorServicios);
		
		if (prestadorServiciosList != null) {
			
			for (PrestadorServiciosDTO prestadorServicios : prestadorServiciosList) {
				if (idPrestadorServicios == null) {
					idPrestadorServicios = prestadorServicios.getIdPrestadorServicios();
				}
				System.out.println("Interfaz - Prestador de Servicios: " + prestadorServicios.getIdPrestadorServicios() + "/" +
						prestadorServicios.getNombrePrestador() + "/" + prestadorServicios.getTipoPrestador());
			}
		} else {
			System.out.println("Fallo Listado de Prestadores de Servicios!");
		}
		System.out.println("----------------------------------------------");
		
//		Detalle de Prestador de Servicios
		
		if (idPrestadorServicios != null) {
			PrestadorServiciosDTO prestadorServicios = null;
			
			try {
				prestadorServicios = PrestadorServiciosDN.getInstancia().detallePrestador(idPrestadorServicios, nombreUsuario);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			System.out.println("Interfaz - Detalle de Prestador de Servicios con id: " + idPrestadorServicios);
			
			if (prestadorServicios != null) {
				System.out.println("Interfaz - Prestador de Servicios: " + prestadorServicios.getIdPrestadorServicios() + "/" +
						prestadorServicios.getNombrePrestador() + "/" + prestadorServicios.getTipoPrestador());
			} else {
				System.out.println("Fallo Detalle de Prestador de Servicios!");
			}
			
			System.out.println("----------------------------------------------");
		}

		
//		Listado de Colonias por Codigo Postal
		
		
		List<CodigoPostalColoniaDTO> coloniaList = null;
		
		String codigoPostal = "64000";
		
		try {
			coloniaList = CodigoPostalColoniaDN.getInstancia().listaColonias(codigoPostal, nombreUsuario);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		System.out.println("Interfaz - Listado de Colonias por Codigo Postal : " + codigoPostal);
		
		if (coloniaList != null) {
			
			for (CodigoPostalColoniaDTO codigoPostalColonia : coloniaList) {
				System.out.println("Interfaz - Codigo Postal/Colonia : " + codigoPostalColonia.getCodigoPostal() + "/" +
						codigoPostalColonia.getIdColonia() + "/" + codigoPostalColonia.getNombreColonia());
			}
		} else {
			System.out.println("Fallo Listado de Colonias por Codigo Postal!");
		}
		
		System.out.println("----------------------------------------------");
		
		
//		System.out.println("Probando serializacion / Deserializacion...");
//		
//		
//		List<ZonaHidroDTO> zonaHidroList = ZonaHidroDN.getInstancia().listarTodos();
//		
//		ZonaHidroDTO zonaHidro = zonaHidroList.get(0);
//		
//		
//		if (zonaHidro.getTcCodigoPostalZonaHidros() != null) {
//			System.out.println("zonaHidro.getTcCodigoPostalZonaHidros() NO es null ...");
//			
//			
//			List<CodigoPostalZonaHidroDTO> codigoPostalZonaHidrosList = zonaHidro.getTcCodigoPostalZonaHidros();
//			
//			if (codigoPostalZonaHidrosList.isEmpty()) {
//				System.out.println("zonaHidro.getTcCodigoPostalZonaHidros() esta vacia ...");
//			} else {
//				System.out.println("zonaHidro.getTcCodigoPostalZonaHidros() NO esta vacia ...");
//			}
//			
//			System.out.println("serializando zonaHidro ...");
//			byte[] bytes = SerializationHelper.serialize(zonaHidro);
//			System.out.println("zonaHidro serializado.");
//			
//			System.out.println("deserializando zonaHidro ...");
//			//ZonaHidroDTO serZonaHidro = (ZonaHidroDTO) SerializationHelper.deserialize(bytes);
//			Object objZonaHidro = SerializationHelper.deserialize(bytes);
//			System.out.println("zonaHidro deserializado.");
//			
//			ZonaHidroDTO serZonaHidro = (ZonaHidroDTO) objZonaHidro;
//			
//			if (serZonaHidro.getTcCodigoPostalZonaHidros() != null) {
//				System.out.println("serZonaHidro.getTcCodigoPostalZonaHidros() NO es null ...");
//			} else {
//				System.out.println("serZonaHidro.getTcCodigoPostalZonaHidros() es null ...");
//			}
//			
//		} else {
//			System.out.println("zonaHidro.getTcCodigoPostalZonaHidros() es null ...");
//		}
//		
//		System.out.println("----------------------------------------------");
		
		
		
////		Probando performance de la aplicacion
//		
//		List<UsuarioDTO> usuarioList = null;
//		UsuarioDTO usuarioDTONuevo = null;
//		UsuarioDTO usuarioDTOEncontrado = null;
//		String nombreUsuarioDTO = "NuevoUser";
//		int iteraciones = 2;
//		
//		
//		System.out.println("Probando performance de la aplicacion...");
//		
//		for (int iteracion = 1; iteracion <= iteraciones; iteracion ++) {
//			
//			System.out.println("Iteracion No. " + iteracion + " de " + iteraciones);
//			
//			//Listado inicial de usuarios
//			System.out.println("Listando todos los usuarios...");
//			try {
//				usuarioList = UsuarioDN.getInstancia().listarTodos();
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//			
//			if (usuarioList != null) {
//				
//				for (UsuarioDTO usuarioDTO : usuarioList) {
//					System.out.println("Usuario: " + usuarioDTO.getUsuarioid() + "/" +
//							usuarioDTO.getUsuarionombre());
//				}
//			} else {
//				System.out.println("no hay usuarios!");
//			}
//			
//			//Agregado de nuevo usuario
//			System.out.println("Agregando nuevo usuario...");
//			
//			usuarioDTONuevo = new UsuarioDTO((long)999, nombreUsuarioDTO, "deleteme");
//			
//			try {
//				UsuarioDN.getInstancia().guardar(usuarioDTONuevo);
//				System.out.println("Nuevo usuario agregado.");
//			} catch (Exception ex) {
//				System.out.println("Error al agregar nuevo usuario!");
//				ex.printStackTrace();
//			}
//			
//			//Listado de usuarios (incluyendo el insertado anteriormente)
//			System.out.println("Listando todos los usuarios (incluyendo al nuevo usuario)...");
//			
//			try {
//				usuarioList = UsuarioDN.getInstancia().listarTodos();
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//						
//			
//			
//			if (usuarioList != null) {
//				
//				for (UsuarioDTO usuarioDTO : usuarioList) {
//					System.out.println("Usuario: " + usuarioDTO.getUsuarioid() + "/" +
//							usuarioDTO.getUsuarionombre());
//				}
//			} else {
//				System.out.println("no hay usuarios!");
//			}
//			
//			
//			
//			
//			//Buscando al nuevo usuario
//			System.out.println("Buscando al usuario " + nombreUsuarioDTO);
//			
//			try {
//				usuarioList = UsuarioDN.getInstancia().buscarPorPropiedad("usuarionombre", nombreUsuarioDTO);
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//			
//			if (usuarioList != null && usuarioList.size() > 0) {
//				usuarioDTOEncontrado = usuarioList.get(0);
//				System.out.println("Usuario encontrado.");
//				System.out.println("Usuario: " + usuarioDTOEncontrado.getUsuarioid() + "/" +
//						usuarioDTOEncontrado.getUsuarionombre());
//				
//			} else {
//				System.out.println("no se encontro al usuario!");
//			}
//			
//			
//			//Modificacion del nuevo usuario
//			
//			if (usuarioDTOEncontrado != null) {
//				
//				System.out.println("Modificando al nuevo usuario...");
//								
//				try {
//					
//					usuarioDTOEncontrado.setUsuarionombre(usuarioDTOEncontrado.getUsuarionombre() + "modificado");
//					
//					usuarioDTOEncontrado = UsuarioDN.getInstancia().actualizar(usuarioDTOEncontrado);
//					System.out.println("Nuevo usuario modificado.");
//				} catch (Exception ex) {
//					System.out.println("Error al modificar al nuevo usuario!");
//					ex.printStackTrace();
//				}
//				
//			}
//			
//			//Listado de usuarios (incluyendo el modificado anteriormente)
//			System.out.println("Listando todos los usuarios (incluyendo al nuevo usuario modificado)...");
//			
//			try {
//				usuarioList = UsuarioDN.getInstancia().listarTodos();
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//						
//			
//			
//			if (usuarioList != null) {
//				
//				for (UsuarioDTO usuarioDTO : usuarioList) {
//					System.out.println("Usuario: " + usuarioDTO.getUsuarioid() + "/" +
//							usuarioDTO.getUsuarionombre());
//				}
//			} else {
//				System.out.println("no hay usuarios!");
//			}
//			
//			//Eliminacion del nuevo usuario
//			
//			if (usuarioDTOEncontrado != null) {
//				
//				System.out.println("Eliminando al nuevo usuario...");
//								
//				try {
//					
//					UsuarioDN.getInstancia().borrar(usuarioDTOEncontrado);
//					System.out.println("Nuevo usuario eliminado.");
//				} catch (Exception ex) {
//					System.out.println("Error al eliminar al nuevo usuario!");
//					ex.printStackTrace();
//				}
//				
//			}
//			
//			//Listado de usuarios (sin el eliminado anteriormente)
//			System.out.println("Listando todos los usuarios (sin el recien eliminado)...");
//			
//			try {
//				usuarioList = UsuarioDN.getInstancia().listarTodos();
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//						
//			
//			
//			if (usuarioList != null) {
//				
//				for (UsuarioDTO usuarioDTO : usuarioList) {
//					System.out.println("Usuario: " + usuarioDTO.getUsuarioid() + "/" +
//							usuarioDTO.getUsuarionombre());
//				}
//			} else {
//				System.out.println("no hay usuarios!");
//			}
//		
//		}
//		
//		System.out.println("----------------------------------------------");
		
		
		
////		Reporte PML TEV
//		
//		
//		List<ReportePMLTEVDTO> reportePMLTEVList = null;
//		Date hoy = new Date();			
//		try {
//			reportePMLTEVList = ReportePMLTEVDN.getInstancia().obtieneReportePMLTEV(hoy, tipocambio, nombreUsuario);
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		
//		System.out.println("Reporte PML TEV : fecha " + hoy + ", tipo de Cambio " + tipocambio);
//		
//		if (reportePMLTEVList != null) {
//			
//			for (ReportePMLTEVDTO reportePMLTEVDTO : reportePMLTEVList) {
//				System.out.println("PML TEV - " + reportePMLTEVDTO.getNumeroPoliza() + "/" +
//						reportePMLTEVDTO.getMoneda() + "/" + reportePMLTEVDTO.getOfiEmi());
//			}
//		} else {
//			System.out.println("Fallo Reporte PML TEV!");
//		}
//		
//		System.out.println("----------------------------------------------");
//		
////		Reporte PML Hidro
//		
//		
//		List<ReportePMLHidroDTO> reportePMLHidroList = null;
//					
//		try {
//			reportePMLHidroList = ReportePMLHidroDN.getInstancia().obtieneReportePMLHidro(hoy, tipocambio, nombreUsuario);
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		
//		System.out.println("Reporte PML Hidro : fecha " + hoy + ", tipo de Cambio " + tipocambio);
//		
//		if (reportePMLHidroList != null) {
//			
//			for (ReportePMLHidroDTO reportePMLHidroDTO : reportePMLHidroList) {
//				System.out.println("PML Hidro - " + reportePMLHidroDTO.getNumeroPoliza() + "/" +
//						reportePMLHidroDTO.getMoneda() + "/" + reportePMLHidroDTO.getOfiEmi());
//			}
//		} else {
//			System.out.println("Fallo Reporte PML Hidro!");
//		}
//		
//		System.out.println("----------------------------------------------");
		
		System.out.println("----------------------------------------------");
		
		// Muestra informacion de tareas programadas
		SimpleDateFormat formatear = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println(" Fechas para NO Emite: " );
		System.out.println(" Fecha Inicial para NO Emite: " + Sistema.FECHANOEMITEINICIAL );
		System.out.println(" Fecha Actual: " + formatear.format(new Date()));
		System.out.println(" Fecha Final para NO Emite: " + Sistema.FECHANOEMITEFINAL);
		System.out.println("----------------------------------------------");
		
		//Temporizador Contabilizar Siniestros
		System.out.println(" Temporizador Contabilizar Siniestros Hora de inicio : " + Sistema.HORA_INICIO_PROCESO_SINIESTRO);
		System.out.println(" Temporizador Contabilizar Siniestros Intervalo : " + Sistema.TIEMPO_INTERVALO_SINIESTRO);
		
					
		System.out.println("----------------------------------------------");
		
		//Temporizador Estado de Cuenta Vigentes
		System.out.println(" Temporizador Estado de Cuenta Vigentes Hora de inicio : " + Sistema.HORA_INICIO_PROCESO_EDOCUENTA_VIGENTES);
		System.out.println("----------------------------------------------");
		
		//Temporizador de respaldo en el log de operaciones
		System.out.println(" Temporizador de respaldo en el log de operaciones Hora de inicio : " + Sistema.HORA_INICIO_PROCESO_LOG);
		System.out.println(" Temporizador de respaldo en el log de operaciones Intervalo : " + Sistema.TIEMPO_INTERVALO_LOG);
		System.out.println(" Temporizador de respaldo en el log de operaciones Num maximo de Registros : " + Sistema.MAX_REGISTROS_LOG);
		
		
		System.out.println("----------------------------------------------");
		
		//Temporizador Reaseguro Distribucion
		System.out.println(" Temporizador Reaseguro Distribucion Hora de inicio : " + Sistema.TIEMPO_INICIAR_DISTRIBUIR_PRIMA);
		System.out.println(" Temporizador Reaseguro Distribucion Intervalo : " + Sistema.TIEMPO_INTERVALO_DISTRIBUIR_PRIMA );
		
			
		System.out.println("----------------------------------------------");
		//Temporizador Reaseguro Distribucion Poliza
		System.out.println(" Temporizador Reaseguro Distribucion Poliza Hora de inicio : " + Sistema.TIEMPO_INICIAR_DISTRIBUIR_POLIZA + " Milisegundos");
		
					
		System.out.println("----------------------------------------------");
		
		//Tarea programada REAS
		System.out.println(" Tarea programada REAS en  : " + Sistema.EXPRESION_CRON_REPORTE_REAS );
		
		//Tarea programada Cancelacion 
		//System.out.println(" Tarea programada Cancelacion Automatica de Polizas en  : " + Sistema.EXPRESION_CRON_CANCEL_ENDOSOS_AUTOM);
		System.out.println(" Tarea programada Cancelacion Automatica de Polizas en  : " + sistemaContext.getExpresionCronCancelacionEndososAuto());
			
		System.out.println("----------------------------------------------");
		
		//Tarea programada Rehabilitacion
		System.out.println(" Tarea programada Rehabilitacion Automatica de Polizas en  : " + Sistema.EXPRESION_CRON_REHABILITACION_ENDOSO);
			
		System.out.println("----------------------------------------------");
		
		//Tarea programada TareaReporteSinInformePreeliminar
		System.out.println(" Tarea programada Reporte Sin Informe Preeliminar en :" + Sistema.EXPRESION_CRON_REPORTESININFORMEPREELIMINAR);
			
		System.out.println("----------------------------------------------");
		//Tarea programada TareaOrdenDePagoPendientes
		System.out.println(" Tarea programada Orden De Pago Pendientes de Siniestros en :" + Sistema.EXPRESION_CRON_ORDENDEPAGOPENDIENTES);
			
		System.out.println("----------------------------------------------");
		
		//Tarea programada TareaOrdenDePagoPendientes de reaseguro
		System.out.println(" Tarea programada Orden Pend Y Canceladas de Reaseguro :" + Sistema.EXPRESION_CRON_ORDENDEPAGOPENDIENTESREASEGURO);
			
		System.out.println("----------------------------------------------");
		
		
		
		

//		Reporte Monitoreo Solicitudes
			
			
			List<ReporteMonitoreoSolicitudesDTO> reporteMonitSolList = null;
			Date fechaInicio = new Date();
			Date fechaFin = new Date();
			BigDecimal idProducto = null;
			Integer codigoEjecutivo = null; 
			Integer codigoAgente = null; 
			String codigoUsuarioSolicitud = null;
			
			System.out.println("Reporte Monitoreo Solicitudes : " +
					"fechaInicio= " + fechaInicio + ", " +
					"fechaFin= " + fechaFin + ", " +
					"idProducto= " + idProducto + ", " +
					"codigoEjecutivo= " + codigoEjecutivo + ", " +
					"codigoAgente= " + codigoAgente + ", " +
					"codigoUsuarioSolicitud= " + codigoUsuarioSolicitud);
			
			try {
				reporteMonitSolList = ReporteMonitoreoSolicitudesDN.getInstancia()
				.obtieneReporteMonitoreoSolicitudes(fechaInicio, fechaFin, idProducto, codigoEjecutivo, codigoAgente, 
						codigoUsuarioSolicitud, nombreUsuario);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			
			if (reporteMonitSolList != null) {
				
				for (ReporteMonitoreoSolicitudesDTO reporteMonitoreoSolicitudesDTO : reporteMonitSolList) {
					System.out.println("idSolicitud =" + reporteMonitoreoSolicitudesDTO.getIdSOL() + 
							", Estatus  - " + reporteMonitoreoSolicitudesDTO.getEstatusSOL());
				}
			} else {
				System.out.println("Fallo Reporte Monitoreo Solicitudes!");
			}
			
			System.out.println("----------------------------------------------");			
		
			
			
			
//			Cancelacion de Endosos
			String numPolizaEndoso = null;
			EndosoIDTO endosoIDTO = null;
			Date fechaCancelacion = new Date();
			BigDecimal idToPoliza = null;
			Short numeroEndoso = null;
			//boolean esCancelable = false;
			
//			Listado de Endosos Cancelables
			List<EndosoIDTO> endosoCancelableList = null;
			try {
				endosoCancelableList = EndosoIDN
					.getInstancia().obtieneListaCancelables(new Date(), nombreUsuario);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			System.out.println("Interfaz - Listado de Endosos Cancelables. Usuario: " + nombreUsuario);
			
			if (endosoCancelableList != null) {
				
				for (EndosoIDTO endoso : endosoCancelableList) {
					if (numPolizaEndoso == null) {
						numPolizaEndoso = endoso.getIdPoliza() + "|" + endoso.getNumeroEndoso();
						fechaCancelacion = endoso.getFechaInicioVigencia();
						idToPoliza = endoso.getIdPoliza();
						numeroEndoso =  endoso.getNumeroEndoso();
					}
					
					System.out.println("Interfaz - Endoso cancelable: " + endoso.getIdPoliza() + "/" + endoso.getNumeroEndoso());
				}
			} else {
				System.out.println("Fallo listado de Endosos Cancelables!");
			}
			System.out.println("----------------------------------------------");
			
			
//			Valida si la poliza es cancelable
			if (numPolizaEndoso != null) {
				
				endosoIDTO = null;
				
				try {
					if (idToPoliza != null){
						EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).getPorId(new EndosoId(idToPoliza, numeroEndoso));
						endosoIDTO = EndosoIDN
						.getInstancia().validaEsCancelable(numPolizaEndoso, nombreUsuario, fechaCancelacion, endosoDTO.getClaveTipoEndoso().intValue());						
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				System.out.println("Interfaz - Validacion si la poliza es cancelable. Poliza " + numPolizaEndoso + " Usuario: " + nombreUsuario);
				
				if (endosoIDTO != null) {
					
					if (endosoIDTO.getEsValido().intValue() == 1) {
						//esCancelable = true;
						System.out.println("Interfaz - La poliza SI es Cancelable");
					} else {
						System.out.println("Interfaz - La poliza NO es Cancelable por el sig. motivo: " + endosoIDTO.getMotivoNoCancelable());
					}
					
				} else {
					System.out.println("Fallo Validacion si la poliza es cancelable!");
				}
				System.out.println("----------------------------------------------");
			}

			
			
			
			
//			Obtiene diferencia de cotizaciones de endoso
//			BigDecimal idCotizacion = BigDecimal.valueOf(18D);
//			List<DiferenciaCotizacionEndosoDTO> diferenciaCotizacionEndosoDTOList = null;
//			
//			try {
//				diferenciaCotizacionEndosoDTOList = EndosoDN.getInstancia(nombreUsuario)
//					.obtenerDiferenciasCotizacionEndoso(idCotizacion, nombreUsuario);
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//			
//			System.out.println("Revisando listado de diferencias de cotizaciones de endoso con id cotizacion: " + idCotizacion);
//			
//			if (diferenciaCotizacionEndosoDTOList != null) {
//				
//				for (DiferenciaCotizacionEndosoDTO diferenciaCotizacionEndosoDTO : diferenciaCotizacionEndosoDTOList) {
//					System.out.println("Diferencia -> Seccion: " + diferenciaCotizacionEndosoDTO.getIdToSeccion() + "/" +
//							"Cuota original: " + diferenciaCotizacionEndosoDTO.getOriginalCuota() + "/" +
//							"Cuota nueva: " + diferenciaCotizacionEndosoDTO.getNuevaCuota());
//				}
//			} else {
//				System.out.println("Fallo listado de diferencias de cotizaciones de endoso con id cotizacion: " + idCotizacion + "!");
//			}
//			System.out.println("----------------------------------------------");
		
		
		LogDeMidasWeb.log("Saliendo de Acerca de...  (Bloque de codigo para probar las Interfaces con Seycos)", Level.INFO, null);
	}
	
	private void accionesConArchivos(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response,String accion) throws SystemException{
	     String urlDestino=null;
	     String idControlArchivo=null;
	     ControlArchivoDTO controlArchivoDTO=null;
	     request.removeAttribute("abreModal");
	     request.removeAttribute("ejecutaAgregar");
	     if(Sistema.ACCION_AGREGAR.equalsIgnoreCase(accion.trim())){
		 request.setAttribute("ejecutaAgregar", 1);
	     }else{
		 if(Sistema.ACCION_ASOCIAR.equalsIgnoreCase(accion.trim())){
		     if(request.getSession(true).getAttribute("archivoIdToSolicitud")!=null){
			 request.setAttribute("ejecutaAsociarSol", 1);
		     }else{
        		     if(request.getSession(true).getAttribute("archivoIdToCotizacion")!=null){
        			 request.setAttribute("ejecutaAsociarCot", 1);
        		     }
		     }
		 }else{
		     	request.setAttribute("abreModal", 1);
        		 if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1){
        		     urlDestino = Sistema.UPLOAD_FOLDER;
        		  }else{
        		     urlDestino = Sistema.LINUX_UPLOAD_FOLDER;
        		  }
        		 if(request.getParameter("idControlArchivo")!= null){
        		     	idControlArchivo = request.getParameter("idControlArchivo");
        			controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idControlArchivo.trim()));
        			if(controlArchivoDTO!=null){
        			    	String fileName=this.obtenerNombreArchivo(controlArchivoDTO);
                 			File archivoOriginal = new File(urlDestino +fileName );
                 			if(archivoOriginal.exists()){
                 			    if(Sistema.ACCION_RENOMBRAR.equalsIgnoreCase(accion.trim())&& request.getSession().getAttribute("nuevonombre")!=null){
                 				this.renombrarArchivo(archivoOriginal, controlArchivoDTO,(String) request.getSession().getAttribute("nuevonombre"), urlDestino);
                 			    }else if(Sistema.ACCION_RENOMBRAR.equalsIgnoreCase(accion.trim())){
                                 		this.borrarArchivo(archivoOriginal);
                 			    }else if(Sistema.ACCION_DESCARGAR.equalsIgnoreCase(accion.trim())){
                                 		try {
						    this.descargarArchivo(archivoOriginal,fileName,request,response);
						} catch (IOException e) {
						    e.printStackTrace();
						}
                 			    }
                         		 }
        			}
        		 }
        		
        	     }
	     }
	}
	
	private void renombrarArchivo(File archivoOriginal,ControlArchivoDTO controlArchivoDTO,String nuevoNombre, String urlDestino){
	    LogDeMidasWeb.log("Entra a renombrar archivo... "+archivoOriginal.getName(), Level.INFO, null);
	     /*controlArchivoDTO.setNombreArchivoOriginal(obtenerNuevoNombreArchivo(controlArchivoDTO, request.getParameter("nuevonombre")));
	    ControlArchivoDN.getInstancia().actualizar(controlArchivoDTO);*/
	    File archivoNuevoNombre = new File(urlDestino + this.obtenerNuevoNombreArchivo(controlArchivoDTO, nuevoNombre.trim()));
	    archivoOriginal.renameTo(archivoNuevoNombre);
	    LogDeMidasWeb.log("Ahora el  archivo se llama... "+archivoOriginal.getName(), Level.INFO, null);
	}
	private void borrarArchivo(File archivoOriginal){
	    LogDeMidasWeb.log("Eliminando Archivo... "+archivoOriginal.getName(), Level.INFO, null);
	    archivoOriginal.delete();
	}
	
	private void descargarArchivo(File file,String fileName,
		HttpServletRequest request, HttpServletResponse response)
		throws SystemException, IOException {
	    FileInputStream fin =null;
	    try
	    {
	    	fin = new FileInputStream(file);
	    	 byte byteArray[] = new byte[(int) file.length()];
	    	    fin.read(byteArray);
	            String userAgent = request.getHeader("User-Agent");
	    	    if (userAgent.indexOf("MSIE") != -1) { // IE
	    		response.setHeader("Content-Disposition", "attachment; filename="
	    				+ URLEncoder.encode(fileName, "ISO-8859-1"));
	    	    } else { // Mozilla, Firefox...
	    		response.setHeader("Content-Disposition", "attachment; filename="
	    				+ URLEncoder.encode(fileName, "ISO-8859-1"));
	    	    }
	    	    OutputStream outputStream = response.getOutputStream();
	    	    String contentType = "application/unknown";
	    	    response.setContentType(contentType);
	    	    response.setContentLength(byteArray.length);
	    	    response.setBufferSize(1024 * 15);
	    	    outputStream.write(byteArray);
	    	    outputStream.flush();
	    	    outputStream.close();
	    } finally {
	    	IOUtils.closeQuietly(fin);
	    }
    	   
	}
	
	private String obtenerNombreArchivo(ControlArchivoDTO controlArchivoDTO) {
		String fileName = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName
				.substring(fileName.lastIndexOf("."), fileName.length());
		return controlArchivoDTO.getIdToControlArchivo().toString() + extension;
	}
	
	private String obtenerNuevoNombreArchivo(ControlArchivoDTO controlArchivoDTO,String nuevoNombre) {
		String fileName = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName
				.substring(fileName.lastIndexOf("."), fileName.length());
		return nuevoNombre + extension;
	}
	
	public void mostrarHelp(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String path = Sistema.AYUDAMIDAS;		
		response.setContentType("text/plain");
		response.getWriter().println(path.toString());		
		response.getWriter().flush();
		
	}
	private SistemaContext sistemaContext;
	
	@Autowired
	@Qualifier("sistemaContextEJB")
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
}