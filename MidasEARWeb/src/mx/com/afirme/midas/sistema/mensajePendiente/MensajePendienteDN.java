package mx.com.afirme.midas.sistema.mensajePendiente;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class MensajePendienteDN {
	private static final MensajePendienteDN INSTANCIA = new MensajePendienteDN();

	public static MensajePendienteDN getInstancia() {
		return MensajePendienteDN.INSTANCIA;
	}

	public void registrarMensajePendiente(
			MensajePendienteDTO mensajePendienteDTO) throws SystemException {
		MensajePendienteSN mensajePendienteSN = new MensajePendienteSN();
		mensajePendienteSN.registrarMensajePendiente(mensajePendienteDTO);
	}

	public List<MensajePendienteDTO> listarPendientesDanios(String idsRoles)
			throws SystemException {
		MensajePendienteSN mensajePendienteSN = new MensajePendienteSN();
		return mensajePendienteSN.listarPendientesDanios(idsRoles);
	}

	public List<MensajePendienteDTO> listarPendientesReaseguro(String idsRoles)
			throws SystemException {
		MensajePendienteSN mensajePendienteSN = new MensajePendienteSN();
		return mensajePendienteSN.listarPendientesReaseguro(idsRoles);
	}

	public List<MensajePendienteDTO> listarPendientesSiniestros(String idsRoles)
			throws SystemException {
		MensajePendienteSN mensajePendienteSN = new MensajePendienteSN();
		return mensajePendienteSN.listarPendientesSiniestros(idsRoles);
	}

	public void atenderMensajePendiente(MensajePendienteDTO mensajePendienteDTO)
			throws SystemException {
		MensajePendienteSN mensajePendienteSN = new MensajePendienteSN();
		mensajePendienteSN.atenderMensajePendiente(mensajePendienteDTO);
	}

	public MensajePendienteDTO getPorId(BigDecimal id)
			throws SystemException {
		MensajePendienteSN mensajePendienteSN = new MensajePendienteSN();
		return mensajePendienteSN.getPorId(id);
	}
}
