package mx.com.afirme.midas.sistema.mensajePendiente;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Fernando Alonzo
 * @since 27 de Agosto del 2009
 * 
 */
public class MensajePendienteSN {
	private MensajePendienteFacadeRemote beanRemoto;

	public MensajePendienteSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(MensajePendienteFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void registrarMensajePendiente(
			MensajePendienteDTO mensajePendienteDTO) {
		try {
			beanRemoto.save(mensajePendienteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<MensajePendienteDTO> listarPendientesDanios(String idsRoles) {
		try {
			return beanRemoto.listarPendientesDanios(idsRoles);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<MensajePendienteDTO> listarPendientesReaseguro(String idsRoles) {
		try {
			return beanRemoto.listarPendientesReaseguro(idsRoles);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<MensajePendienteDTO> listarPendientesSiniestros(String idsRoles) {
		try {
			return beanRemoto.listarPendientesSiniestros(idsRoles);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void atenderMensajePendiente(MensajePendienteDTO mensajePendienteDTO) {
		try {
			mensajePendienteDTO.setEstatus(new BigDecimal(1));
			beanRemoto.update(mensajePendienteDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public MensajePendienteDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
