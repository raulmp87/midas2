package mx.com.afirme.midas.sistema.intermoduloerrorlog;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class InterModuloErrorLogSN {
	private InterModuloErrorLogFacadeRemote beanRemoto;

	public InterModuloErrorLogSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(InterModuloErrorLogFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto InterModuloErrorLog instanciado", Level.FINEST, null);
	}
	
	public void agregar(InterModuloErrorLogDTO entity) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.save(entity);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<InterModuloErrorLogDTO> listarFiltrado(InterModuloErrorLogDTO entity){
		try {
			return beanRemoto.listarFiltrado(entity);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void eliminar(InterModuloErrorLogDTO entity) throws ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(entity);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public InterModuloErrorLogDTO modificar(InterModuloErrorLogDTO entity) throws ExcepcionDeAccesoADatos{
		try {
			return beanRemoto.update(entity);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
