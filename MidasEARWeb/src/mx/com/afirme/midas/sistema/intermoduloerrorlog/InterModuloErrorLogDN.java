package mx.com.afirme.midas.sistema.intermoduloerrorlog;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class InterModuloErrorLogDN {
	public static final InterModuloErrorLogDN INSTANCIA = new InterModuloErrorLogDN();

	private InterModuloErrorLogDN(){
	}
	
	public static InterModuloErrorLogDN getInstancia(){
		return INSTANCIA;
	}
	
	public void agregar(InterModuloErrorLogDTO entity) throws ExcepcionDeAccesoADatos, SystemException{
		new InterModuloErrorLogSN().agregar(entity);
	}
	
	public List<InterModuloErrorLogDTO> listarFiltrado(InterModuloErrorLogDTO entity) throws SystemException{
		return new InterModuloErrorLogSN().listarFiltrado(entity);
	}
	
	public void eliminar(InterModuloErrorLogDTO entity) throws ExcepcionDeAccesoADatos, SystemException{
		new InterModuloErrorLogSN().eliminar(entity);
	}
	
	public InterModuloErrorLogDTO modificar(InterModuloErrorLogDTO entity) throws ExcepcionDeAccesoADatos, SystemException{
		return new InterModuloErrorLogSN().modificar(entity);
	}
}
