package mx.com.afirme.midas.sistema;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.ArrayList;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.entorno.Entorno;

/**
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.sistema.SistemaContext}
 */
@Deprecated
public class Sistema {
	
	public static final String EXITOSO = "exitoso";
	public static final String EXITOSO_PROD = "exitosoprod";
	public static final String NO_EXITOSO = "noExitoso";
	public static final String NO_DISPONIBLE = "noDisponible";
	public static final String ALTERNO = "alterno";
	public static final String ERROR_ESCRIBIR_TAG = "No esta disponible la implementacion del componente," +
													" Reportelo al administrador e intentelo mas tarde.";
	public static final String ACCIONMODIFICAR = "Acci\u00F3n Realizada Correctamente";
	
	public static final String USUARIO_MIDAS="usuarioAccesoMIDAS";
	public static final String METODO_ENVIO_FORMULARIOS = "POST";
	public static final String RUTAESTATICA="<html:rewrite page=\"\"/>";
	public static final int REGISTROS_PERMITIDOS = 2;
	public static final String CLASE_CSS_CAJA_TEXTO = "cajaTexto";
	
	//Constantes para excepciones de acceso a datos
	public static final String EXCEPCION_ROLLBACK = EJBTransactionRolledbackException.class.getCanonicalName();
	public static final String EXCEPCION_OBTENER_DTO = "excepcionAlObtenerDTO";
	public static final String MENSAJE_EXCEPCION_ROLLBACK = "mensaje.excepcion.rollback";
	public static final String MENSAJE_EXCEPCION_SISTEMA_NO_DISPONIBLE = "mensaje.excepcion.sistema.nodisponible";
	public static final String MENSAJE_EXCEPCION_OBTENER_DTO = "mensaje.excepcion.acceso.dto";
	public static final String ARCHIVO_RECURSOS = "mx.com.afirme.midas.RecursoDeMensajes";
	public static final String CLAVE_ERROR = "midasMensajeUsuarioError";
	public static final String MENSAJE_ERROR = "mensajeError";
	public static final String MENSAJE_INFO = "mensajeInfo";
	public static final String MENSAJE_USUARIO = "mensajeUsuario";
	public static final String DATOS_INVALIDOS = "datosInvalidos";
	public static final String CONSTANTES_WS = "mx.com.afirme.midas.ConstantesWS";
	
	public static final String EXCEPCION_ACCESO_DATOS = "No es posible accesar al sistema en este momento.";
	
	//Constantes cargadas de un archivo de propiedades externo a la aplicacion
	public static final String AMBIENTE_SISTEMA=Entorno.obtenerVariable("midas.sistema.ambiente","DES");
	public static final String VERSION_SISTEMA= "2.0.0.90"; //No debe estar externalizado en el Websphere, mas bien en una archivo de propiedad ya que esto es de conocimiento del sistema y no del servidor de aplicaciones.
	public static final boolean NOTIFICA_DANIOS_A_REASEGURO= Boolean.parseBoolean(Entorno.obtenerVariable("midas.sistema.notifica.danios.a.reaseguro","false"));
	public static final String EMAIL_FROM_ADDRESS=Entorno.obtenerVariable("email.emailFromAddress","general@cdsi.com.mx");
	public static final String EMAIL_HOST_NAME=Entorno.obtenerVariable("email.smtp.host.name","172.30.4.175");
	public static final String EMAIL_PORT=Entorno.obtenerVariable("email.smtp.port","2525");
	public static final String EMAIL_FROM_USER=Entorno.obtenerVariable("email.smtp.usuario","general@cdsi.com.mx");
	public static final String EMAIL_FROM_PASSWORD=Entorno.obtenerVariable("email.smtp.contrasenia","general");
	public static final String EMAIL_ADMINISTRADOR_MIDAS=Entorno.obtenerVariable("email.administrador.midas","afirmemidas@yahoo.com.mx");
	public static final String EMAIL_ADMINISTRADOR_AFIRME=Entorno.obtenerVariable("email.administrador.afirme","algo@hotmail.com");
	public static final String EMAIL_ADMINISTRADOR_REASEGURO=Entorno.obtenerVariable("email.administrador.midas.reaseguro","sistema@afirme.com");
	public static final String UPLOAD_FOLDER=Entorno.obtenerVariable("midas.sistema.uploadFolder","c:/MidasWeb/ArchivosAnexos/");
	public static final String LINUX_UPLOAD_FOLDER=Entorno.obtenerVariable("midas.sistema.linux.uploadFolder","/apps/profile1/MIDASPDF/");//profile1 para azkaban profile2 para qa y produccion
	public static final String EXPRESION_CRON_REPORTE_REAS=Entorno.obtenerVariable("midas.sistema.tareas.reaseguro.reportereas.expresioncron","0 0 3 * * ? 2011");
	public static final String EXPRESION_CRON_NOTIFICACION_RENOVACION=Entorno.obtenerVariable("midas.sistema.tareas.notificacion.renovacion.expresioncron","0 0 5 * * ? 2011");	
	
	//TODO:se cambio a la clase SistemaContext
	//public static final String EXPRESION_CRON_CANCEL_ENDOSOS_AUTOM=Entorno.obtenerVariable("midas.sistema.tareas.endoso.cancelendosoautomatica.expresioncron","0 0 5 * * ? 2011");
	
	public static final String EXPRESION_CRON_REHABILITACION_ENDOSO=Entorno.obtenerVariable("midas.sistema.tareas.endoso.rehabendoso.expresioncron","0 0 4 * * ? 2011");
	public static final String EXPRESION_CRON_REPORTESININFORMEPREELIMINAR=Entorno.obtenerVariable("midas.sistema.tareas.siniestros.reportesininformepreeliminar.expresioncron","0 0 8 * * ? 2011");
	public static final String EXPRESION_CRON_ORDENDEPAGOPENDIENTES=Entorno.obtenerVariable("midas.sistema.tareas.siniestros.ordendepagopendientes.expresioncron","0 0/45 * * * ? 2011");
	public static final String EXPRESION_CRON_ORDENDEPAGOPENDIENTESREASEGURO=Entorno.obtenerVariable("midas.sistema.tareas.reaseguro.ordendepagopendientes.expresioncron","0 0/15 * * * ? 2011");
	public static final String EXPRESION_CRON_INICIO_DISTRIBUCION_POLIZAS=Entorno.obtenerVariable("midas.sistema.tareas.reaseguro.iniciardistribucionpolizas.expresioncron","0 0 18 * * ? 2011");
	public static final String EXPRESION_CRON_FIN_DISTRIBUCION_POLIZAS=Entorno.obtenerVariable("midas.sistema.tareas.reaseguro.detenerdistribucionpolizas.expresioncron","0 0 7 ? * MON-FRI");
	public static final String EXPRESION_CRON_MONITOREO_PAGOS_PENDIENTES=Entorno.obtenerVariable("midas.sistema.tareas.reaseguro.monitoreopagospendientesprogramar.expresioncron","0 0 7 ? * MON");
	public static final String FOLDER_ARCHIVOS_XML_REPORTES_WINDOWS=Entorno.obtenerVariable("midas.sistema.folderXMLReportesWindows","c:/MidasWeb/MIDASXMLREPORTES/");
	public static final String FOLDER_ARCHIVOS_XML_REPORTES_LINUX=Entorno.obtenerVariable("midas.sistema.folderXMLReportesLinux","/apps/profile2/MIDASXMLREPORTES/");
	public static final String FOLDER_ARCHIVOS_XLS_REPORTES_WINDOWS=Entorno.obtenerVariable("midas.sistema.folderXLSReportesWindows","c:/MidasWeb/MIDASXLSREPORTES/");
	public static final String FOLDER_ARCHIVOS_XLS_REPORTES_LINUX=Entorno.obtenerVariable("midas.sistema.folderXLSReportesLinux","/apps/profile2/MIDASXLSREPORTES/");
	public static final String FIJA_NIVEL_LOGGER_MIDASWEB=Entorno.obtenerVariable("midas.sistema.nivel.logger","1");
	public static final String FECHANOEMITEINICIAL=Entorno.obtenerVariable("midas.sistema.fecha.no.emite.inicial","2010-3-26");
	public static final String FECHANOEMITEFINAL=Entorno.obtenerVariable("midas.sistema.fecha.no.emite.final","2010-4-1");
	public static final String AYUDAMIDAS=Entorno.obtenerVariable("midas.sistema.url.ayuda","http://www.afirme.com.mx");
	public static final long PAUSAARRANQUEWEB= Long.parseLong(Entorno.obtenerVariable("midas.sistema.arranque.web","0"));
	public static final boolean ENVIO_CORREOS_ACTIVADO = Boolean.parseBoolean(Entorno.obtenerVariable("midas.sistema.enviocorreosactivado","false"));
	public static final String EXPRESION_CRON_CARGA_MASIVA=Entorno.obtenerVariable("midas.sistema.tareas.cargamasiva.expresioncron","0 0 0 * * ?");
	public static final String EXPRESION_CRON_OPERACIONES_SAP=Entorno.obtenerVariable("midas.sistema.tareas.operacionesSAP.expresioncron","0 0/1 * * * ?");

	//Constantes: Llaves de tablas de base de datos
	public static final int MONEDA_PESOS = Integer.parseInt(Entorno.obtenerVariable("midas.moneda.pesos.id","484"));
	public static final int MONEDA_DOLARES = Integer.parseInt(Entorno.obtenerVariable("midas.moneda.dolares.id","840"));
	public static final int PARTICIPANTE_CORREDOR = 0;
	public static final int PARTICIPANTE_REASEGURADOR = 1;
	
	//Constantes para Configuracion de seguridad de ASM
	public static final String MIDAS_APPID = Entorno.obtenerVariable("midas.appid","5");

	public static final String MIDAS_LOGIN = Entorno.obtenerVariable("midas.login","http://192.168.1.121:9080/MidasWeb/sistema/inicio.do");
	//ACTIVA_ASM public static final String MIDAS_LOGIN = Entorno.obtenerVariable("midas.login","http://172.30.4.36:9080/MidasWeb/sistema/inicio.do");
	public static final String ASM_LOGIN = Entorno.obtenerVariable("midas.ws.asm.login","http://192.168.0.90:9081/ASMWeb/security/showLogin.do");
	//ACTIVA_ASM public static final String ASM_LOGIN = Entorno.obtenerVariable("midas.ws.asm.login","http://172.30.4.36:9080/ASMWeb/security/showLogin.do");
	public static final String IIOP_ASM_URL = Entorno.obtenerVariable("midas.asm.corba.url","corbaloc:iiop:192.168.0.90");
	public static final String IIOP_ASM_PUERTO = Entorno.obtenerVariable("midas.asm.corba.puerto","2809");

	public static final String ASM_SEGURIDAD_USUARIO = Entorno.obtenerVariable("midas.ws.asm.seguridad.usuario","softnet");
	public static final String ASM_SEGURIDAD_PASSWORD = Entorno.obtenerVariable("midas.ws.asm.seguridad.usuario.contrasenia","041112");
	public static final boolean ASM_ACTIVO = Boolean.valueOf(Entorno.obtenerVariable("midas.asm.activo","false"));
	//ACTIVA_ASM public static final boolean ASM_ACTIVO = Boolean.valueOf(Entorno.obtenerVariable("midas.asm.activo","true"));
	
	
	//Constantes para Configuracion de WebService de validacion de CURP
	public static final String END_POINT_WS_VALIDADOR_CURP = Entorno.obtenerVariable("midas.ws.endpoint.validador.curp","http://172.30.4.45:40390/WSCurp/CurpAnalyzerWSDL");
	
	//Constantes para Configuracion de WebService de impresion de recibos de poliza
    //	(Desarrollo)
	public static final String URL_WS_IMPRESION = Entorno.obtenerVariable("midas.ws.impresion.url","172.30.4.36");
	public static final String PUERTO_WS_IMPRESION = Entorno.obtenerVariable("midas.ws.impresion.puerto","9080");

	//Constantes para manejo de paginacion
	public static final String ACCION_PAGINADO=Entorno.obtenerVariable("midas.sistema.accionpaginado","listarFiltradoPaginado.do");
	public static final String LISTA_PAGINADA = "listaPaginada";
	public static final int PAGINAS_CACHE = Integer.parseInt(Entorno.obtenerVariable("midas.sistema.paginascache","5"));
	public static final String REGISTROS_PAGINADOS = Entorno.obtenerVariable("midas.sistema.tamanopagina","10");
	
	public static final String URL_INICIO = "/MidasWeb/sistema/inicio.do";
	public static final String URL_ERROR = Entorno.obtenerVariable("midas.login.error","http://localhost:9080/MidasWeb/login.jsp");	
	public static final String URL_LOGIN_SIN_SEGURIDAD = "/MidasWeb/loginB.jsp";
	public static final String URL_CAMBIO_PASSWORD = "/MidasWeb/cambiaPassword.jsp";
	public static final String ACCESO_DENEGADO = "/MidasWeb/accesoDenegado.jsp";
	public static final String USUARIO_ACCESO_MIDAS = "usuarioAccesoMIDAS";
	public static final String ARCHIVO_MENU_MIDAS = "MenuMIDAS.xml";
	public static final String BOTON_BUSCAR = "buscar";
	public static final String BOTON_GUARDAR = "guardar";
	public static final String BOTON_REGRESAR = "regresar";
	public static final String BOTON_AGREGAR = "agregar";
	public static final String BOTON_BORRAR = "borrar";
	public static final String BOTON_MODIFICAR = "modificar";
	public static final String BOTON_NUEVA_VERSION = "nuevaVersion";
	public static final String BOTON_CONTINUAR = "continuar";
	public static final String BOTON_SELECCIONAR = "seleccionar";
	public static final String BOTON_ASIGNAR = "asignar";
	public static final String BOTON_CONSULTAR = "consultar";
	public static final String BOTON_ACEPTAR = "aceptar";
		
	
	//Roles de Da�os 
	public static final String ROL_ADMIN_PRODUCTOS = "Rol_Admin_Productos";
	public static final String ROL_AGENTE = "Rol_Agente";
	public static final String ROL_AGENTE_EXTERNO = "Rol_Agente_Ext";
	public static final String ROL_AGENTE_ESPECIAL = "Rol_Agente_Especial";
	public static final String ROL_ASIGNADOR_OT = "Rol_Asignador_Ot";
	public static final String ROL_ASIGNADOR_SOL = "Rol_Asignador_Sol";
	public static final String ROL_COORDINADOR_EMI = "Rol_Coordinador_Emi";
	public static final String ROL_DIRECTOR_TECNICO = "Rol_Director_Tecnico";
	public static final String ROL_EMISOR = "Rol_Emisor";
	public static final String ROL_MESA_CONTROL = "Rol_Mesa_Control";
	public static final String ROL_SUPERVISOR_SUSCRIPTOR = "Rol_Superv_Suscrptor";
	public static final String ROL_SUSCRIPTOR_OT = "Rol_Suscriptor_Ot";
	public static final String ROL_SUSCRIPTOR_COT = "Rol_Suscriptor_Cot";
	public static final String ROL_REPORTES_DANIOS = "Rol_Reportes_Danios";
	//public static final String ROL_INSPECTOR = "inspector";
	public static final String ROL_CONSULTA_DANIOS = "Rol_Consulta_Danios";
	public static final String ROL_SUSCRIPTOR_EXT = "Rol_Suscriptor_Ext";
		
	//Roles Siniestros
	public static final String ROL_CABINERO = "Rol_Cabinero";
	public static final String ROL_COORDINADOR_SINIESTROS = "Rol_Coord_Siniestros";
	public static final String ROL_COORDINADOR_SINIESTROS_FACULTATIVO = "Rol_Coord_Sin_Facult";
	public static final String ROL_GERENTE_SINIESTROS = "Rol_Gerente_Sin";
	public static final String ROL_GERENTE_SINIESTROS_FACULTATIVO = "Rol_Grnte_Sin_Facult";
	public static final String ROL_ANALISTA_ADMINISTRATIVO = "Rol_Analista_Admin";
	public static final String ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO = "Rol_Anlist_Adm_Facul";
	public static final String ROL_DIRECTOR_DE_OPERACIONES = "Rol_Dir_De_Operac";
	public static final String ROL_AJUSTADOR = "Rol_Ajustador"; //Al parecer no la usan
	public static final String ROL_REPORTES_SINIESTROS = "Rol_Reportes_Sin";
	//public static final String ROL_GERENTE_INDEMNIZACIONES = "gerenteIndemnizaciones";
	public static final String ROL_CONSULTA_SINIESTROS = "Rol_Consulta_Sin";
	
	//Roles Reaseguro
	public static final String ROL_OP_REASEGURO_AUTOMATICO = "Rol_Op_Reaseguro_Aut";
	public static final String ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO = "Rol_Op_Ast_Sbdir_Rea";
	public static final String ROL_SUBDIRECTOR_REASEGURO = "Rol_Subdirector_Rea";
	public static final String ROL_DIRECTOR_REASEGURO = "Rol_Director_Rea";
	public static final String ROL_GERENTE_REASEGURO = "Rol_Gerente_Rea";
	public static final String ROL_OP_REASEGURO_FACULTATIVO = "Rol_Op_Rea_Facult";//Nuevos Roles Reaseguro
	public static final String ROL_ACTUARIO = "Rol_Actuario";
	public static final String ROL_OP_PAGOS_COBROS_REASEGURADORES = "Rol_Op_Pagos_Cob_Rea";
	public static final String ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO = "Rol_Coord_Adm_Rea";
	public static final String ROL_REPORTES_REASEGURO = "Rol_Reportes_Rea";
	public static final String ROL_CONSULTA_REASEGURO = "Rol_Consulta_Rea";
	public static final String ROL_MAIL_REASEGURO = "Rol_Mail_Rea";
	
	//Roles especiales
	public static final String ROL_ESP_ADMINISTRADOR_COLONIAS = "Rol_Esp_Adm_Colonias";
	
	
	//Constantes de tipo de mensaje para el usuario final
	public static final String EXITO = "30";
	public static final String INFORMACION = "20";
	public static final String ERROR = "10";
	
	//Tipo de acciones a realizar en las ActionForms
	public static final String ACCION_GUARDAR = "GUARDAR";
	public static final String ACCION_BORRAR = "BORRAR";
	public static final String ACCION_MODIFICAR = "MODIFICAR";
	
	//Constantes para catalogo de valores fijo
	public static final int CLAVE_TIPO_CALCULO_EMISION=22;
	public static final int CLAVE_TIPO_CALCULO_CANCELACION=22;
	public static final int CLAVE_UNIDAD_VIGENCIA=23;
	public static final int CLAVE_TIPO_SUMA_ASEGURADA=24;
	public static final int CLAVE_TIPO_COASEGURO=36;
	public static final int CLAVE_TIPO_DEDUCIBLE=38;
	public static final int CLAVE_TIPO_LIMITE_COASEGURO=37;
	public static final int CLAVE_TIPO_LIMITE_DEDUCIBLE=39;
	public static final int GRUPO_CLAVE_BIEN_SECCION=26;
	public static final int GRUPO_CLAVE_OBLIGATORIEDAD=27;
	public static final int GRUPO_CLAVE_ESTATUS_PRODUCTO=32;
	public static final int GRUPO_CLAVE_TIPO_DEDUCIBLE=38;
	public static final int GRUPO_CLAVE_ESTATUS_SOLICITUD = 29;
	public static final int GRUPO_CLAVE_ESTATUS_COTIZACION = 330;
	public static final int GRUPO_CLAVE_TIPO_LIMITE_DEDUCIBLE=39;
	
	//Constantes de imagenes usadas en los forms.
	public static final String ICONO_DETALLE = "/MidasWeb/img/details.gif";
	public static final String ICONO_REFRESCAR = "/MidasWeb/img/b_refrescar.gif";
	public static final String ICONO_BORRAR = "/MidasWeb/img/delete14.gif";
	public static final String ICONO_MODIFICAR = "/MidasWeb/img/Edit14.gif";
	public static final String ICONO_VERDE = "/MidasWeb/img/ico_green.gif";
	public static final String ICONO_AMARILLO = "/MidasWeb/img/ico_yel.gif";
	public static final String ICONO_ROJO = "/MidasWeb/img/ico_red.gif";	
	public static final String ICONO_BLANCO = "/MidasWeb/img/blank.gif";
	public static final String ICONO_CALENDARIO = "/MidasWeb/img/b_pendiente.gif";
	public static final String ICONO_AUTORIZADO = "/MidasWeb/img/b_autorizar.gif";
	public static final String ICONO_RECHAZADO = "/MidasWeb/img/b_rechazar.gif";
	public static final String LOGOTIPO_SEGUROS_AFIRME = "/img/logo_AS.jpg";
	public static final String FIRMA_FUNCIONARIO1 = "/img/sign1.jpg";
	public static final String FIRMA_FUNCIONARIO2 = "/img/sign2.jpg";
	public static final String FONDO_PLANTILLA_COTIZACION = "/img/cotizacion1.gif";
	public static final String FIRMA_FUNCIONARIO_AUTOS ="/img/reportes/firmas.jpg";
	public static final String LOGO_SEGUROS="/img/reportes/logo_seguros_11.gif";
	public static final String DUPLICADO="/img/reportes/duplicado.gif";
	public static final String ENCABEZADO_EXPRESS="/img/reportes/encabezadoCotExpres.gif";
	public static final String COBERTURAS="/img/reportes/coberturas.jpg";
	
	
	//Constantes para ARD especiales
	public static final BigDecimal DESCUENTO_IGUALACION = BigDecimal.valueOf(-1D);
	public static final BigDecimal RECARGO_IGUALACION = BigDecimal.valueOf(-1D);
	public static final BigDecimal DESCUENTO_PRIMER_RIESGO = BigDecimal.valueOf(-2D);
	public static final BigDecimal RECARGO_PRIMER_RIESGO= BigDecimal.valueOf(-2D);
	public static final BigDecimal DESCUENTO_LUC = BigDecimal.valueOf(-3D);
	public static final BigDecimal RECARGO_LUC= BigDecimal.valueOf(-3D);
	public static final BigDecimal DESCUENTO_REASEGURO_FACULTATIVO = BigDecimal.valueOf(-4D);
	public static final BigDecimal RECARGO_REASEGURO_FACULTATIVO = BigDecimal.valueOf(-4D);
	
	//Constantes para Estatus
	public static final short AUTORIZACION_NO_REQUERIDA = 0;
	public static final short AUTORIZACION_REQUERIDA = 1;
	public static final String CLAVES_AUTORIZADA_RECHAZADA = "78";
	public static final short AUTORIZADA = 7;
	public static final short RECHAZADA = 8;
	public static final short ESTATUS_ODT_ASIGNADA = 1;
	public static final short ESTATUS_ODT_ENPROCESO = 0;
	public static final short ESTATUS_ODT_TERMINADA = 2;
	public static final short ESTATUS_ODT_RECHAZADA = 8;
	public static final short ESTATUS_ODT_CANCELADA = 9;
	public static final short ESTATUS_COT_ENPROCESO = 10;	
	public static final short ESTATUS_COT_ASIGNADA = 11;
	public static final short ESTATUS_COT_LIBERADA = 13;
	public static final short ESTATUS_COT_LISTA_EMITIR = 14;
	public static final short ESTATUS_COT_ASIGNADA_EMISION = 15;
	public static final short ESTATUS_COT_EMITIDA = 16;
	public static final short ESTATUS_COT_PENDIENTE_AUTORIZACION = 17;
	public static final short ESTATUS_SOL_PENDIENTE_AUTORIZACION = 3;	
	public static final short ESTATUS_SOL_TERMINADA = 2;
	public static final short ESTATUS_SOL_ASIGNADA = 1;
	public static final Short ESTATUS_SOL_EN_PROCESO = (short)0;
	
	//Constantes para Tipo de ARD
	public static final short TIPO_COMERCIAL = 1;
	public static final short TIPO_TECNICO = 2;
	
	//Constantes para Tipo vALOR de ARD
	public static final short TIPO_PORCENTAJE = 1;
	public static final short TIPO_VALOR = 2;		
	
	//Constantes para NIVEL de ARD
	public static final short NIVEL_PRODUCTO = 1;
	public static final short NIVEL_TIPO_POLIZA = 2;		
	public static final short NIVEL_COBERTURA = 3;

	//Constantes para Obligatoriedad
	public static final short DEFAULT = 0;
	public static final short OPCIONAL = 1;		
	public static final short OBLIGATORIO_PARCIAL = 2;	
	public static final short OBLIGATORIO = 3;
	
	
	//Constantes para variables de sesion de mensajes de usuario
	public static final String TIPO_MENSAJE = "tipoMensaje";
	public static final String MENSAJE = "mensaje";
	
	//Constantes para configuracion del Temporizador de respaldo en el log de operaciones
	public static final String HORA_INICIO_PROCESO_LOG = Entorno.obtenerVariable("midas.temporizador.log.horainiciar","01:51"); //Formato HH:mm (24 hrs.)
	public static final long TIEMPO_INTERVALO_LOG = Long.parseLong(Entorno.obtenerVariable("midas.temporizador.log.intervalo","86400000")); //Cada 24 hrs.
	public static final String MAX_REGISTROS_LOG = Entorno.obtenerVariable("midas.temporizador.log.maxregistros","100000");
	
	//Constantes para Temporizador Contabilizar Siniestros
	public static final String HORA_INICIO_PROCESO_SINIESTRO = Entorno.obtenerVariable("midas.temporizador.siniestros.horainiciar","23:00"); //Formato HH:mm (24 hrs.)
	public static final long TIEMPO_INTERVALO_SINIESTRO = Long.parseLong(Entorno.obtenerVariable("midas.temporizador.siniestros.intervalo","21600000")); //Cada 6 hrs.
	
	//Constantes para Temporizador Estado de Cuenta Vigentes
	public static final String HORA_INICIO_PROCESO_EDOCUENTA_VIGENTES = Entorno.obtenerVariable("midas.temporizador.estadocuentavigentes.horainiciar","02:51"); //Formato HH:mm (24 hrs.)
	public static final long TIEMPO_INTERVALO_EDOCUENTA_VIGENTES = Long.parseLong(Entorno.obtenerVariable("midas.temporizador.estadocuentavigentes.intervalo","86400000")); //Cada 24 hrs.
	
	//Constantes para Temporizador Reaseguro Distribucion
	public static final long TIEMPO_INICIAR_DISTRIBUIR_PRIMA = Long.parseLong(Entorno.obtenerVariable("midas.temporizador.distribucionprima.iniciar","10000"));
	public static final long TIEMPO_INTERVALO_DISTRIBUIR_PRIMA = Long.parseLong(Entorno.obtenerVariable("midas.temporizador.distribucionprima.intervalo","180000")); //Cada 3 hrs.
	
	//Constantes para Temporizador Reaseguro Distribucion Poliza
	public static final long TIEMPO_INICIAR_DISTRIBUIR_POLIZA = Long.parseLong(Entorno.obtenerVariable("midas.temporizador.distribucionpoliza.iniciar","1000"));
	
	//Constantes para tipos de documentos
	public static final String TIPO_PDF = "application/pdf";
	public static final String TIPO_XLS = "application/vnd.ms-excel";
	public static final String TIPO_CSV = "text/csv";
	public static final String TIPO_TXT = "text/plain";
	public static final String TIPO_DOC = "application/vnd.ms-word";
	public static final String TIPO_ZIP = "application/zip";
	
	//Constantes para tipo de salida de documento
	public static final String RPT_COMO_ADJUNTO = "attachment";
	public static final String RPT_EN_NAVEGADOR = "inline";
	
	//Constantes de DataSources
	public static final String MIDAS_DATASOURCE = "jdbc/MidasDataSource";

	//Constantes de Estatus de Ingresos
	public static final short ABIERTO = 0;
	public static final short RECIBIDO = 1;
	public static final short CANCELADO = 2;
	
	//Constantes para Numero de Renovacion en una poliza
	public static final int POLIZA_NUEVA = 0;
	
	//Constantes de Estatus de poliza; 
	public static final short ESTATUS_VIGENTE = 1;
	public static final short ESTATUS_CANCELADA = 0;

	
	//Constante que indica el numero predeterminado maximo de copias para un inciso
	public static final int MAXIMO_COPIAS_INCISO = Integer.parseInt(Entorno.obtenerVariable("midas.inciso.maximocopias","20"));
	
	//Constantes para tipos de comision
	public static final short TIPO_RO = 1;
	public static final short TIPO_RCI = 2;
	public static final short TIPO_PRR = 3;

	//Constantes para tipos de agrupoacion
	public static final short TIPO_PRIMER_RIESGO = 1;
	public static final short TIPO_LUC = 2;
	
	public static final short SOLICITUD_TIPO_ENDOSO = 2;
	public static final short SOLICITUD_TIPO_POLIZA = 1;

	//Constantes para renovacion
	public static final short ES_RENOVACION = 1;
	public static final short NO_ES_RENOVACION = 0;
	
	//Contantes para tipos de movimientos de endoso
	public static final short TIPO_MOV_ALTA_COBERTURA = 1;
	public static final short TIPO_MOV_BAJA_COBERTURA = 2;
	public static final short TIPO_MOV_MODIFICACION_COBERTURA = 3;
	public static final short TIPO_MOV_ALTA_SECCION = 4;
	public static final short TIPO_MOV_BAJA_SECCION = 5;	
	public static final short TIPO_MOV_MODIFICACION_SECCION = 6;
	public static final short TIPO_MOV_ALTA_INCISO = 7;
	public static final short TIPO_MOV_BAJA_INCISO = 8;
	public static final short TIPO_MOV_MODIFICACION_INCISO = 9;
	public static final short TIPO_MOV_MODIFICACION_APP_GRAL = 10;
	public static final short TIPO_MOV_ALTA_SUBINCISO = 11;
	public static final short TIPO_MOV_BAJA_SUBINCISO = 12;
	public static final short TIPO_MOV_MODIFICACION_SUBINCISO = 13;
	public static final short TIPO_MOV_ALTA_1ER_RIESGO_LUC = 14;
	public static final short TIPO_MOV_BAJA_1ER_RIESGO_LUC = 15;
	public static final short TIPO_MOV_MODIFICACION_1ER_RIESGO_LUC = 16;
	public static final short TIPO_MOV_MODIFICACION_PRIMA_NETA = 17;
	public static final short TIPO_MOV_MODIFICACION_COASEGURO = 18;
	public static final short TIPO_MOV_MODIFICACION_DEDUCIBLE = 19;	


	//Constantes para contratado y no contratado
	public static final short CONTRATADO = 1;
	public static final short NO_CONTRATADO = 0;
	
	//Constantes para Mensajes de los movimientos de endoso
	public static final String MSG_MOV_TIPO_ENDOSO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.tipoendoso");
	public static final String MSG_MOV_PRODUCTO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.producto");
	public static final String MSG_MOV_TIPO_POLIZA = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.tipopoliza");
	public static final String MSG_MOV_FORMA_PAGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.formapago");
	public static final String MSG_MOV_MEDIO_PAGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.mediopago");
	public static final String MSG_MOV_MONEDA = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.moneda");
	public static final String MSG_MOV_VIGENCIA = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.vigencia");
	public static final String MSG_MOV_AGENTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.agente");
	public static final String MSG_MOV_NOMBRE_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.nombre");
	public static final String MSG_MOV_TIPO_PERSONA_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.tipopersona");
	public static final String MSG_MOV_RFC_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.rfc");
	public static final String MSG_MOV_FECHA_NACIMIENTO_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.fechanacimiento");
	public static final String MSG_MOV_TELEFONO_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.telefono");
	public static final String MSG_MOV_CORREO_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.correo");
	public static final String MSG_MOV_DOMICILIO_ASEGURADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.asegurado.domicilio");
	public static final String MSG_MOV_NOMBRE_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.nombre");
	public static final String MSG_MOV_TIPO_PERSONA_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.tipopersona");
	public static final String MSG_MOV_RFC_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.rfc");
	public static final String MSG_MOV_FECHA_NACIMIENTO_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.fechanacimiento");
	public static final String MSG_MOV_TELEFONO_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.telefono");
	public static final String MSG_MOV_CORREO_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.correo");
	public static final String MSG_MOV_DOMICILIO_CONTRATANTE = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.contratante.domicilio");
	public static final String MSG_MOV_BAJA_INCISO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.inciso");
	public static final String MSG_MOV_ALTA_INCISO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.inciso");
	public static final String MSG_MOV_MODIFICACION_INCISO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.inciso");
	public static final String MSG_MOV_BAJA_COBERTURA = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.cobertura");
	public static final String MSG_MOV_ALTA_COBERTURA = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.cobertura");
	public static final String MSG_MOV_MODIFICACION_COBERTURA = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.cobertura");
	public static final String MSG_MOV_SUBINCISO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.subinciso.listado");
	public static final String MSG_MOV_MODIFICACION_SUBINCISO_LISTADO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.subinciso.listado");
	public static final String MSG_MOV_ALTA_SUBINCISO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.subinciso");
	public static final String MSG_MOV_BAJA_SUBINCISO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.subinciso");
	public static final String MSG_MOV_BAJA_PRIMER_RIESGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.primerriesgo");
	public static final String MSG_MOV_ALTA_PRIMER_RIESGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.primerriesgo");
	public static final String MSG_MOV_MODIFICACION_PRIMER_RIESGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.primerriesgo");
	public static final String MSG_MOV_BAJA_LUC = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.luc");
	public static final String MSG_MOV_ALTA_LUC = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.luc");
	public static final String MSG_MOV_MODIFICACION_LUC = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.luc");
	public static final String MSG_MOV_MODIFICACION_TEXTO_ADICIONAL = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.modificacion.textoAdicional");
    public static final String MSG_MOV_ALTA_TEXTO_ADICIONAL = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.textoAdicional");
    public static final String MSG_MOV_BAJA_TEXTO_ADICIONAL = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.textoAdicional");   	
    public static final String MSG_MOV_ALTA_DOCUMENTO_ANEXO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.alta.documentoanexo");   	
    public static final String MSG_MOV_BAJA_DOCUMENTO_ANEXO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.baja.documentoanexo");   	
    
    public static final String MSG_MOV_ID_ALTA_PRIMER_RIESGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.identificador.alta.primerriesgo");
    public static final String MSG_MOV_ID_ALTA_LUC = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.identificador.alta.luc");
    public static final String MSG_MOV_ID_BAJA_PRIMER_RIESGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.identificador.baja.primerriesgo");
    public static final String MSG_MOV_ID_BAJA_LUC = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.identificador.baja.luc");
    public static final String MSG_MOV_ID_MODIFICACION_PRIMER_RIESGO = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.identificador.modificacion.primerriesgo");
    public static final String MSG_MOV_ID_MODIFICACION_LUC = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.movimiento.identificador.modificacion.luc");
    
    public static final String MSG_MOV_PREFIJO_CANCELADO = "(C)"; 
    public static final String MSG_MOV_PREFIJO_REHABILITADO = "(R)"; 
    
	public static final String CLAVE_SUMA_ASEGURADA_BASICA="1";
	public static final String CLAVE_SUMA_ASEGURADA_AMPARADA="2";
	public static final String CLAVE_SUMA_ASEGURADA_SUBLIMITE="3";
	
	
	//Constantes de ID�s de subramos
	public static final BigDecimal SUBRAMO_INCENDIO = new BigDecimal(1d);
	public static final BigDecimal SUBRAMO_OBRA_CIVIL_EN_CONSTRUCCION = new BigDecimal(16d);
	public static final BigDecimal SUBRAMO_EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA = new BigDecimal(14d);
	public static final BigDecimal SUBRAMO_EQUIPO_ELECTRONICO = new BigDecimal(13d);
	public static final BigDecimal SUBRAMO_MONTAJE_MAQUINA = new BigDecimal(17d);
	public static final BigDecimal SUBRAMO_ROTURA_MAQUINARIA = new BigDecimal(12d);
	public static final BigDecimal SUBRAMO_RESPONSABILIDAD_CIVIL_GENERAL = new BigDecimal(31d);
	
	//Constantes para Clave de transacci�n Contable
	public static final String REASEGURO_EMIS = "REASEGURO-EMIS";
	public static final String SINIESTRO = "SINIESTRO";
	public static final String REASEGURO_SIN = "REASEGURO-SIN";
	public static final String REASEGURO_IN = "REASEGURO-INGRESO";
		
	
	public static final int TIPO_SLIP_AVIACION=1;
	public static final int TIPO_SLIP_BARCOS=2;
	public static final int TIPO_SLIP_EQUIPO_CONTRATISTA=3;
	public static final int TIPO_SLIP_INCENDIO_SOPORTE=4;
	public static final int TIPO_SLIP_OBRA_CIVIL_SOPORTE=5;
	public static final int TIPO_SLIP_CONSTRUCTORES_SOPORTE=6;
	public static final int TIPO_SLIP_FUNCIONARIOS_SOPORTE=7;
	public static final int TIPO_SLIP_TRANSPORTE_SOPORTE=8;
	public static final int TIPO_SLIP_GENERAL_SOPORTE=0;
	
	public static final int TIPO_CUMULO_POLIZA=1;
	public static final int TIPO_CUMULO_INCISO=2;
	public static final int TIPO_CUMULO_SUBINCISO=3;
	
	public static final int POSICION_IDTORIESGO=0;
	public static final int POSICION_IDTCRAMO=1;
	public static final int POSICION_IDTCSUBRAMO=2;
	
	public static final BigDecimal[] AVION_CLAVES_CONFIGURACION= {new BigDecimal("3280"), new BigDecimal("2"), new BigDecimal("3")};
	public static final BigDecimal[] AVION_CLAVES_CONFIGURACION_ALTERNA= {new BigDecimal("3290"), new BigDecimal("2"), new BigDecimal("3")};
	public static final BigDecimal AVION_ID_DATO_MARCA=new BigDecimal("30");
	public static final BigDecimal AVION_ID_DATO_MODELO=new BigDecimal("40");
	public static final BigDecimal AVION_ID_DATO_TIPOAERONAVE=new BigDecimal("10");
	public static final BigDecimal AVION_ID_DATO_ANIO=new BigDecimal("80");
	public static final BigDecimal AVION_ID_DATO_SERIE=new BigDecimal("60");
	public static final BigDecimal AVION_ID_DATO_MATRICULA=new BigDecimal("70");
	public static final BigDecimal AVION_ID_DATO_USO=new BigDecimal("20");
	public static final BigDecimal AVION_ID_DATO_CAPACIDAD=new BigDecimal("90");
	public static final BigDecimal AVION_ID_DATO_LIMITES_GEOGRAFICOS=new BigDecimal("140");
	public static final BigDecimal AVION_ID_DATO_AEROPUERTOBASE=new BigDecimal("110");
	public static final BigDecimal AVION_ID_DATO_TIPO_AEROPUERTO=new BigDecimal("120");
	public static final BigDecimal AVION_COBERTURA_PAGOS_VOLUNTARIOS_TRIPULACION = new BigDecimal("1420");
	public static final BigDecimal AVION_COBERTURA_PAGOS_MEDICOS_PASAJEROS= new BigDecimal("1430");
	public static final BigDecimal AVION_COBERTURA_PAGOS_MEDICOS_TRIPULACION= new BigDecimal("1440");
	public static final BigDecimal[] BARCO_CLAVES_CONFIGURACION= {new BigDecimal("3450"), new BigDecimal("2"), new BigDecimal("4")};
	public static final BigDecimal[] BARCO_CLAVES_CONFIGURACION_ALTERNA= {new BigDecimal("3500"), new BigDecimal("2"), new BigDecimal("4")};
	public static final BigDecimal BARCO_ID_DATO_MATERIAL_CASCO=new BigDecimal("30");
	public static final BigDecimal BARCO_ID_DATO_TIPO_EMBARCACION=new BigDecimal("10");
	public static final BigDecimal BARCO_ID_DATO_ANIO_CONSTRUCCION=new BigDecimal("40");
	public static final BigDecimal BARCO_ID_DATO_AGUAS_NAVEGACION=new BigDecimal("100");
	public static final BigDecimal BARCO_ID_DATO_USO=new BigDecimal("20");
	public static final BigDecimal BARCO_ID_DATO_PUERTO_REGISTRO=new BigDecimal("110");
	public static final BigDecimal BARCO_ID_DATO_CLASIFICACION=new BigDecimal("70");
	public static final BigDecimal BARCO_ID_DATO_BANDERA=new BigDecimal("80");
	public static final BigDecimal BARCO_ID_DATO_ESLORA=new BigDecimal("150");
	public static final BigDecimal BARCO_ID_DATO_MANGA=new BigDecimal("160");
	public static final BigDecimal BARCO_ID_DATO_PUNTAL=new BigDecimal("170");
	public static final BigDecimal BARCO_ID_DATO_TONELAJE_BRUTO=new BigDecimal("130");
	public static final BigDecimal BARCO_ID_DATO_TONELAJE_NETO=new BigDecimal("140");
	public static final BigDecimal BARCO_COBERTURA_VALOR_CASCO = new BigDecimal("1570");
	public static final BigDecimal BARCO_COBERTURA_VALOR_RC= new BigDecimal("1560");
	public static final BigDecimal BARCO_COBERTURA_VALOR_VI= new BigDecimal("1580");
	public static final BigDecimal[] OBRA_CIVIL_CLAVES_CONFIGURACION= {new BigDecimal("2710"), new BigDecimal("3"), new BigDecimal("16")};
	public static final BigDecimal OBRA_CIVIL_ID_DATO_TIPO_OBRA=new BigDecimal("10");
	public static final BigDecimal OBRA_CIVIL_ID_DATO_CARACTERISTICA_OBRA=new BigDecimal("50");
	public static final BigDecimal[] RC_CONSTRUCTORES_CLAVES_CONFIGURACION= {new BigDecimal("2910"), new BigDecimal("5"), new BigDecimal("31")};
	public static final BigDecimal[] RC_CONSTRUCTORES_CLAVES_CONFIGURACION_ALTERNA= {new BigDecimal("3060"), new BigDecimal("5"), new BigDecimal("31")};
	public static final BigDecimal RC_CONSTRUCTORES_ID_DATO_DESCRIPCION_OBRA=new BigDecimal("10");
	public static final BigDecimal RC_CONSTRUCTORES_ID_DATO_DURACION_OBRA=new BigDecimal("20");
	public static final BigDecimal[] TRANSPORTE_CLAVES_CONFIGURACION= {new BigDecimal("3120"), new BigDecimal("2"), new BigDecimal("2")};
	public static final BigDecimal TRANSPORTE_ID_DATO_LUGAR_ORIGEN=new BigDecimal("60");
	public static final BigDecimal TRANSPORTE_ID_DATO_LUGAR_DESTINO=new BigDecimal("70");
	public static final BigDecimal TRANSPORTE_ID_DATO_BIENES_ASEGURADOS=new BigDecimal("20");
	public static final BigDecimal TRANSPORTE_ID_DATO_TIPO_EMPAQUE=new BigDecimal("40");
	public static final BigDecimal TRANSPORTE_ID_DATO_MEDIO_TRANSPORTE=new BigDecimal("30");
	
	//***********CONSTANTES DOCUMENTOS ANEXOS **************//
	//Constantes para Documentos de los productos
	public static final String DOC_PRODUCTO = "P";
	public static final String DOC_TIPO_POLIZA = "TP";		
	public static final String DOC_COBERTURA = "C";
	public static final String DOC_INVALIDO = "N/A";
	
	//Constantes para Documentos de los productos
	public static final BigDecimal CLAVE_DOC_ANEXO_PRODUCTO =BigDecimal.ZERO;
	public static final BigDecimal CLAVE_DOC_ANEXO_TIPO_POLIZA = BigDecimal.ONE;		
	public static final BigDecimal CLAVE_DOC_ANEXO_COBERTURA = new BigDecimal(2d);
	
	public static final Short DOCUMENTO_ANEXO_COTIZACION_SELECCIONADO =(short)1;
	public static final Short DOCUMENTO_ANEXO_COTIZACION_SIN_SELECCIONAR = (short)0;
	
	public static final Short DOCUMENTO_ANEXO_OBLIGATORIO =(short)3;
	public static final Short DOCUMENTO_ANEXO_OPCIONAL = (short)0;
	
	//Constantes generales
	public static final short ESTATUS_INSPECCION_INCISO_REQUERIDO = 1;
	public static final short ESTATUS_INSPECCION_INCISO_NO_REQUERIDO = 0;
	public static final short ESTATUS_INSPECCION_INCISO_ACEPTADO =7;
	public static final short ESTATUS_INSPECCION_INCISO_RECHAZADO =8;
	
	public static final String SIN_MENSAJE_PARA_MOSTRAR="0";
	
	/*
	 * Identificadores de reportes de reaseguro
	 */
	public static final int REA_REPORTE_MOVTOS_REASEGURO = 1;
	public static final int REA_REPORTE_CONTRATOS_POLIZA = 2;
	
	
	//*******   CONSTANTES PARA LLENAR COMBOS DE AGENTES*********//
	// Usuario Temporal para obtener Agentes
	public static final String USUARIO_TEMPORAL_OBTENER_AGENTES="KMGONVAZ";
	public static final String[] CEDULAS_DE_AGENTES = {
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.A"),
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.A2"),
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.B"),
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.B1"),
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.B2"),
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.C") };
	/** Cedulas de agentes permitidos para una solicitud de emision/endoso */
	public static final String[] CEDULAS_DE_AGENTES_PERMITIDOS = {
	UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.A2"),
			UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.B"),
			UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.B1"),
			UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.B2"),
			UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.agente.tipoCedula.C") };
	
	
	public static final int SD_APLICA_PRIMER_RIESGO = 1;
	public static final int SD_NO_APLICA_PRIMER_RIESGO = 0;
	public static final int SD_NO_SE_ENCONTRO_COBERTURA = -1;
	public static final int SD_NO_SE_ENCONTRO_ENDOSO = -2;
	
	public static final BigDecimal CODIGO_PARAMETRO_EMAIL_REASEGURO=new BigDecimal("00050010");
	public static final BigDecimal ID_GRUPO_EMAIL_REASEGURO=new BigDecimal("5");
	
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_PRODUCTO = new BigDecimal(10070d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RCFUNCIONARIOS_TIPO_POLIZA = new BigDecimal(60010d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RCMEDICOS_TIPO_POLIZA = new BigDecimal(60020d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_RC_PROFESIONAL_MEDICOS_TIPO_POLIZA = new BigDecimal(60030d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_PAQ_EMPRESARIAL= new BigDecimal(10010d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_IMPRESION_TRANSPORTES = new BigDecimal(10020d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_PAQ_FAMILIAR= new BigDecimal(10030d);
	public static final BigDecimal CODIGO_PARAMETRO_GENERAL_INCENDIO= new BigDecimal(10040d);
	
	
	public   static final String[]  ROLES_ENVIOS_CORREOS_FACULTATIVO= {ROL_OP_REASEGURO_FACULTATIVO, ROL_DIRECTOR_REASEGURO};
	public static final String FALSO="false";
/**
 * Cosntantes para los modulos de MIDAS
 * 1: Da�os, 
 * 2: Reaseguro, 
 * 3: Siniestros. 	
 */
	public static final short MODULO_DANIOS = 1;
	public static final short MODULO_REASEGURO = 2;
	public static final short MODULO_SINIESTROS = 3;	
	
	
	public static final BigDecimal CODIGO_TIPO_NEGOCIO_FACULTATIVO_CONTROL_RECLAMOS=BigDecimal.valueOf(97D);
	public static final BigDecimal CODIGO_TIPO_NEGOCIO_FACULTATIVO=BigDecimal.valueOf(98D);
	public static final BigDecimal CODIGO_TIPO_NEGOCIO_NORMAL=BigDecimal.valueOf(99D);
	
	public static final int FACULTATIVO_INTEGRADO=4;
	public static final int SOPORTADO_POR_REASEGURO=0;
	
	
	public static short NUM_AGRUPACION_PRIMER_RIESGO=(short)1;
	public static final BigDecimal ID_COBERTURA_HIDRO=new BigDecimal(30d);
	public static final BigDecimal ID_COBERTURA_TERREMOTO=new BigDecimal(50d);
	
	public static final BigDecimal IDTCSUBRAMO_HIDRO=new BigDecimal(41d);
	public static final BigDecimal IDTCSUBRAMO_TERREMOTO=new BigDecimal(37d);

	public static final NumberFormat FORMATO_MONEDA = new DecimalFormat("$#,##0.00");
	public static final NumberFormat FORMATO_CUOTA = new DecimalFormat("##0.0000");
	public static final NumberFormat FORMATO_PORCENTAJE_ENTERO = new DecimalFormat("##0%");
	public static final NumberFormat FORMATO_COMAS = new DecimalFormat("#,##0");
	
	public static final short PERMITE_SUBINCISOS = (short)1;
	public static final short NO_PERMITE_SUBINCISOS = (short)0;
	
	public static final String CLAVE_TIPO_PORCENTAJE="1";
	public static final String CLAVE_TIPO_PERSONA_FISICA="1";
	public static final String CLAVE_TIPO_PERSONA_MORAL="2";

	// Constantes de endosos para tipo de solicitud;
	public static final int SOLICITUD_ENDOSO_DE_CANCELACION = 1;
	public static final int SOLICITUD_ENDOSO_DE_REHABILITACION = 2;
	public static final int SOLICITUD_ENDOSO_DE_MODIFICACION = 3;
	public static final int SOLICITUD_ENDOSO_DE_DECLARACION = 4;
	public static final int SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO = 5;
	
	
	// Constantes de tipo de endoso;
	public static final short TIPO_ENDOSO_CANCELACION = 4;
	public static final short TIPO_ENDOSO_REHABILITACION = 5;
	public static final short TIPO_ENDOSO_AUMENTO = 2;
	public static final short TIPO_ENDOSO_DISMINUCION = 3;
	public static final short TIPO_ENDOSO_CAMBIO_DATOS = 1;
	public static final short TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO = 6;
	public static final short TIPO_ENDOSO_CE = 7;
	public static final short TIPO_ENDOSO_RE = 8;
	
	//Constante para limite de tiempo en la transaccion para emitir un endoso de CE o RE (en segundos) (0 = Sin limite)
	public static final int TIEMPO_LIMITE_TX_EMISION_CE_RE = Integer.parseInt(
			Entorno.obtenerVariable("midas.emision.cancelacionrehabilitacion.timeout","0"));
	
	
	//Constantes para Motivos e Cancelacion para Endosos
	
	public static final int MOTIVO_A_PETICION_DEL_ASEGURADO = 1;
	public static final int MOTIVO_REEXPEDICION_POR_ERROR_DEL_ASEGURADO = 2;
	public static final int MOTIVO_REEXPEDICION_POR_ERROR_DEL_SUSCRIPCION = 3;
	public static final int MOTIVO_REEXPEDICION_POR_CAMBIOS_SOLICITADOS = 4;
	public static final int MOTIVO_CANCELACION_POR_ASI_CONVENIR_A_INSTITUCION = 5;
	public static final int MOTIVO_CANCELACION_POR_CAMBIO_DE_AGENTE = 6;
	public static final int MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO = 7;

	public static final int SELECCIONADO = 1;
	public static final int NO_SELECCIONADO = 0;
	
	//Constante para representar el usuario de Sistema
	public static final String USUARIO_SISTEMA = "SISTEMA";
	
	public static final short ES_CANCELABLE = 1;
	public static final short NO_CANCELABLE = 0;
	
	//Constante para Registar las estadisticas
	public static final int SE_CREA_SOLICITUD=1;
	public static final int SE_ASIGNA_SOL_A_ODT=2;
	public static final int SE_ASIGNA_ODT_A_COT=3;
	public static final int SE_ASIGNA_COT_A_EMISION=4;
	public static final int SE_EMITE_COTIZACION=5;
	public static final int SE_ASIGNA_SOL_A_COT=6;

	//Constante para acciones en MensajePendientesAction
	public static String LLAVE_MAGICA="midas2010";
	public static final String ACCION_AGREGAR="AGREGAR";
	public static final String ACCION_ASOCIAR="ASOCIAR";
	public static final String ACCION_RENOMBRAR="AGREGAR";
	public static final String ACCION_BORRADO="BORRAR";
	public static final String ACCION_DESCARGAR="DESCARGAR";
	public static final String ACCION_VERIFICARINTERFAZ="VERIFICARINTERFAZ";
	public static final String ACCION_EJECUTACANCELACIONESAUTOMATICAS="EJECUTACANCELACIONESAUTOMATICAS";
	public static final String ACCION_MIGRAR_ARCHIVOS = "migrarArchivos";
	public static final String RENOMBRAARCHIVOPDF="RENOMBRAARCHIVOPDF";
	public static final String NOMBREARCHIVOPDF="NOMBREARCHIVOPDF";
	
	//Clave de regenacion de recibos
	public static final short SI_REGENERA_RECIBOS = 1;
	public static final short NO_REGENERA_RECIBOS = 0;	

	//Clave de AGRUP
	public static final short SI_AGRUPADO= 1;
	public static final short NO_AGRUPADO= 0;		
	
	//status de recibos 
	public static final String RECIBO_EMITIDO = "EMITIDO";
	public static final String RECIBO_PAGADO = "PAGADO";
	public static final String RECIBO_CANCELADO = "CANCELADO";
	
	/**
	 * Cosntante para notificacion via email a Coordinadores y Gerentes de Siniestros
	 * true: Si se Notificara a los usuarios 
	 * false: No se Notificara a los usuarios	
	 */ 
	public static final boolean NOTIFICACION_CORREO_SINIESTROS = Boolean.parseBoolean(Entorno.obtenerVariable("email.notifica.correo.siniestros.coordinadores.gerentes","false"));
	/*
	 * fija constantes para nivel del logger
	 * SEVERE ES NIVEL MAS ALTO .... LOGEA MUY POCO
	 * FINEST ES NIVEL MAS BAJO ----LOGEA TODO
	 */
	public static final int NIVEL_LOGGER_SEVERE=8;
	public static final int NIVEL_LOGGER_WARNING=7;
	public static final int NIVEL_LOGGER_INFO=6;
	public static final int NIVEL_LOGGER_CONFIG=5;
	public static final int NIVEL_LOGGER_FINE=4;
	public static final int NIVEL_LOGGER_FINER=3;
	public static final int NIVEL_LOGGER_FINEST=2;
	public static final int NIVEL_LOGGER_ALL=1;
	public static final int NIVEL_LOGGER_OFF=0;	
	public static final int NIVEL_LOGGER_DEFECTO=9;

	//Constantes para el tipo de Calculo
	public static final String TIPO_CALCULO_SEYCOS = "S";
	public static final String TIPO_CALCULO_MIDAS = "M";	
	
	//Constante para definir si se debe regenerar el REAS de todo el mes o s�lo del d�a anterior en el job de creaci�n del REAS.
	public static final Boolean REGENERAR_REAS_MENSUAL_JOB = new Boolean(Entorno.obtenerVariable("midas.reaseguro.reportereas.regenerarmensual","false"));
	
	//Constantes para definir id's de coberturas b�sicas de R.C. 
	public static final BigDecimal ID_COB_RC_BASICA_INMUEBLES_Y_ACTIVIDADES = new BigDecimal(980);	
	public static final BigDecimal ID_COB_RC_TALLERES = new BigDecimal(1000);
	public static final BigDecimal ID_COB_RC_CONTRATISTA = new BigDecimal(1010);
	public static final BigDecimal ID_COB_RC_HOTELERIA = new BigDecimal(1020);
	public static final BigDecimal ID_COB_RC_EQUIPO_CONTRATISTA = new BigDecimal(1030);
	public static final BigDecimal ID_COB_RC_FAMILIAR = new BigDecimal(1200);
	
	//Constantes para definir id's de coberturas amparadas de R.C.	
	public static final BigDecimal ID_COB_RC_ESTACIONAMIENTOS = new BigDecimal(990);
	public static final BigDecimal ID_COB_RC_INSTALACIONES_SUBTERRANEAS = new BigDecimal(1040);
	public static final BigDecimal ID_COB_RC_TRABAJOS_DE_SOLDADURA = new BigDecimal(1050);
	public static final BigDecimal ID_COB_RC_CARGA_Y_DESCARGA = new BigDecimal(1060);
	public static final BigDecimal ID_COB_RC_DEMOLICION = new BigDecimal(1070);
	public static final BigDecimal ID_COB_RC_EXPLOSIVOS = new BigDecimal(1080);
	public static final BigDecimal ID_COB_RC_MAQUINAS_DE_TRABAJO = new BigDecimal(1090);
	public static final BigDecimal ID_COB_RC_APUNTALAMIENTO = new BigDecimal(1100);
	public static final BigDecimal ID_COB_RC_OTRAS_OBRAS_ESPECIALES = new BigDecimal(1110);
	public static final BigDecimal ID_COB_RC_CONTAMINACION_AL_MEDIO_AMBIENTE = new BigDecimal(1120);
	public static final BigDecimal ID_COB_RC_ASUMIDA = new BigDecimal(1130);
	public static final BigDecimal ID_COB_RC_CRUZADA = new BigDecimal(1140);
	public static final BigDecimal ID_COB_RC_PRODUCTOS_EN_MEXICO = new BigDecimal(1150);
	public static final BigDecimal ID_COB_RC_CONTRATISTA_INDEPENDIENTE = new BigDecimal(1160);
	public static final BigDecimal ID_COB_RC_TRABAJOS_TERMINADOS = new BigDecimal(1170);
	public static final BigDecimal ID_COB_RC_ARRENDATARIO = new BigDecimal(1180);
	public static final BigDecimal ID_COB_RC_PRODUCTOS_EN_EL_EXTRANJERO = new BigDecimal(1190);
	
	//Listas constantes que almacenan las coberturas b�sicas y amparadas de R.C.
	public static final List<BigDecimal> COBERTURAS_BASICAS_RC = new ArrayList<BigDecimal>();
	public static final List<BigDecimal> COBERTURAS_AMPARADAS_RC = new ArrayList<BigDecimal>();
	
	//Constantes para configuraci�n de los estados de cuenta en reaseguro
	public static final Short CLAVE_FORMATO_DEFAULT_FACULTATIVO = (short)1;
	public static final Short CLAVE_FORMATO_DEFAULT_AUTOMATICO = (short)1;
	public static final Short CLAVE_FORMATO_AUTOMATICO_REPORTE = (short)2;
	
	//Carga de las listas constantes que almacenan las coberturas b�sicas y amparadas de R.C.
	static{
		COBERTURAS_BASICAS_RC.add(ID_COB_RC_BASICA_INMUEBLES_Y_ACTIVIDADES);
		COBERTURAS_BASICAS_RC.add(ID_COB_RC_TALLERES);
		COBERTURAS_BASICAS_RC.add(ID_COB_RC_CONTRATISTA);
		COBERTURAS_BASICAS_RC.add(ID_COB_RC_HOTELERIA);
		COBERTURAS_BASICAS_RC.add(ID_COB_RC_EQUIPO_CONTRATISTA);
		COBERTURAS_BASICAS_RC.add(ID_COB_RC_FAMILIAR);				
		
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_ESTACIONAMIENTOS);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_INSTALACIONES_SUBTERRANEAS);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_TRABAJOS_DE_SOLDADURA);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_CARGA_Y_DESCARGA);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_DEMOLICION);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_EXPLOSIVOS);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_MAQUINAS_DE_TRABAJO);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_APUNTALAMIENTO);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_OTRAS_OBRAS_ESPECIALES);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_CONTAMINACION_AL_MEDIO_AMBIENTE);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_ASUMIDA);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_CRUZADA);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_PRODUCTOS_EN_MEXICO);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_CONTRATISTA_INDEPENDIENTE);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_TRABAJOS_TERMINADOS);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_ARRENDATARIO);
		COBERTURAS_AMPARADAS_RC.add(ID_COB_RC_PRODUCTOS_EN_EL_EXTRANJERO);		
	}
	
	//Constantes para modificaci�n de derechos y recargo por pago fraccionado
	public static final Short CLAVE_DERECHOS_SISTEMA = 0;
	public static final  Short CLAVE_DERECHOS_USUARIO = 1;
	public static final  Short CLAVE_RPF_SISTEMA = 0;
	public static final  Short CLAVE_RPF_USUARIO_MONTO = 1;
	public static final  Short CLAVE_RPF_USUARIO_PORCENTAJE = 2;
	
	//Constantes para datos de licitaci�n
	public static final Short CLAVE_IMPRESION_SUMA_ASEGURADA_CANTIDAD = 0;
	public static final Short CLAVE_IMPRESION_SUMA_ASEGURADA_SEA = 1;
	
	//Constantes para clave estatus detalle carga masiva
	public static final Short CORRECTO = 1;
	public static final Short INCORRECTO = 0;
	
	//Constantes para tipos de destinatario idgrupovalores en tabla de valores fijos 51
	public static final String DESTINATARIO_CORREO_EJECUTIVO_VENTAS = "10";
	public static final String DESTINATARIO_CORREO_AGENTE = "20";
	public static final String DESTINATARIO_CORREO_CLIENTE = "30";
	public static final String DESTINATARIO_CORREO_OTRO = "40";
	
	//Constantes para tipos de movimientos en el seguimiento de renovaciones
	public static final BigDecimal TIPO_ASIGNACION_SUSCRIPTOR = BigDecimal.valueOf(10);
	public static final BigDecimal TIPO_RENOVACION_ACEPTADA = BigDecimal.valueOf(15);
	public static final BigDecimal TIPO_ENVIO_EMAIL = BigDecimal.valueOf(20);
	public static final BigDecimal TIPO_ENVIO_ALERTAMIENTO = BigDecimal.valueOf(30);
	public static final BigDecimal TIPO_VENCIMIENTO_PLAZO = BigDecimal.valueOf(40);
	public static final BigDecimal TIPO_POLIZA_RENOVADA = BigDecimal.valueOf(50);
	
	public static final String DESTINATARIOS_MONITOREO_PAGOS_PENDIENTES_REASEGURO = Entorno.obtenerVariable("midas.reaseguro.egresos.monitoreopagospendientes.destinatarios","");


	public static final boolean NOTIFICACION_RENOVACION_ACTIVO = Boolean.valueOf(Entorno.obtenerVariable("midas.notificacion.renovacion.activo","false"));
	public static final String DIRECCION_IMAGENES_MAIL_TEMPLATE = Entorno.obtenerVariable("midas.directorio.imagenes","http://localhost:9080/MidasWeb/img/");
	public static final String[] RENOVACION_POLITICA_DIAS_VENCIMIENTO = Entorno.obtenerVariable("midas.renovacion.politica.dias.vencimiento","30,15,10,5,4,3,2,1,0").split(",");
	public static final String[] RENOVACION_POLITICA_DESTINATARIOS = Entorno.obtenerVariable("midas.renovacion.politica.destinatarios","10,20,30").split(",");

	public static final boolean FILTRO_ENVIO_CORREOS_ACTIVO = Boolean.valueOf(Entorno.obtenerVariable("midas.sistema.filtro.enviocorreos","true"));
	public static final String[] FILTRO_CORREOS_PERMITIDOS = Entorno.obtenerVariable("midas.sistema.filtro.destinatarios","0carlos.madrid@afirme.com").split(",");
	public static final boolean NOTIFICACION_CORREO_ERROR_REHABILITACION = Boolean.valueOf(Entorno.obtenerVariable("midas.notificacion.correo.rehabilitacion.activo","false"));
	
	public static final String URL_WS_ARCHIVOS = "http://localhost:9080/InsuranceWebServices/services/PrintReport";
	public static final String ROOT_DIR_FOR_FILES = "/apps/profile3/";
	public static final String AVISO_PRIVACIDAD_DANIOS = "AP_Danios.pdf";
	public static final String AVISO_PRIVACIDAD_AUTOS = "AP_Autos.pdf";
	public static final String AVISO_PRIVACIDAD_VIDA = "AP_Vida.pdf";
	public static final String CARTA_RENOVACION_CASA_AFIRME_PLUS = "Carta_Ren_Casa_Afirme_Plus.pdf";
	public static final String CARTA_RENOVACION_CASA_AFIRME_PLUS_SIN_HIDRO = "Carta_Ren_Casa_Afirme_Plus_Sin_Hidro.pdf";
	
	//Claves de Negocio
	public static final String CLAVE_NEGOCIO_DANIOS = "D";
	public static final String CLAVE_NEGOCIO_AUTOS = "A";
	
	public static final Integer EDAD_MAXIMA = 65;
	public static final Integer EDAD_MINIMA = 18;
	
	
	//EIBS CONFIG
	public static final String EIBS_CONFIG_HOST = Entorno.obtenerVariable("eibs.config.host","172.20.1.196");
	public static final String EIBS_CONFIG_PORT = Entorno.obtenerVariable("eibs.config.port","47800");
	public static final String EIBS_CONFIG_TIMEOUT = Entorno.obtenerVariable("eibs.config.timeout","120000");	
}
