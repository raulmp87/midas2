/**
 * Clase para realizar anotaciones en la bit�cora de Midas
 */
package mx.com.afirme.midas.sistema.log;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

/**
 * @author andres.avalos
 *
 */
public class LogOperacionDN {

	public static final LogOperacionDN INSTANCIA = new LogOperacionDN();

	public static LogOperacionDN getInstancia (){
		return LogOperacionDN.INSTANCIA;
	}
		
	
	/**
	 * Agrega el registro de una acci�n realizada sobre una p�liza a la bit�cora de Midas
	 * @param cotizacion El objeto a registrar
	 * @param tipoAccion Tipo de acci�n realizada sobre el objeto (GUARDAR, BORRAR, ACTUALIZAR)
	 * @param nombreUsuario Nombre del usuario que est� realizando la acci�n
	 */
	public void agregarLogPoliza(CotizacionDTO cotizacion, String tipoAccion, String nombreUsuario) {
		//TODO: checar y agregar la llamada al metodo cuando se sepa cuales son el DTO y Facade que se van a usar en poliza
//		agregarLog(cotizacion.getIdToCotizacion(), tipoAccion, nombreUsuario, "Cotizaci�n",
//				cotizacion.getClass().getSimpleName(), "TOCOTIZACION");
		
	}
	
	/**
	 * Agrega el registro de una acci�n realizada sobre un endoso a la bit�cora de Midas
	 * @param cotizacion El objeto a registrar
	 * @param tipoAccion Tipo de acci�n realizada sobre el objeto (GUARDAR, BORRAR, ACTUALIZAR)
	 * @param nombreUsuario Nombre del usuario que est� realizando la acci�n
	 */
	public void agregarLogEndoso(CotizacionDTO cotizacion, String tipoAccion, String nombreUsuario) {
		//TODO: checar y agregar la llamada al metodo cuando se sepa cuales son el DTO y Facade que se van a usar en endoso
//		agregarLog(cotizacion.getIdToCotizacion(), tipoAccion, nombreUsuario, "Cotizaci�n",
//				cotizacion.getClass().getSimpleName(), "TOCOTIZACION");
		
	}
	
	/**
	 * Agrega un registro en la bit�cora de Midas
	 * @param idRegistro id del objeto a registrar
	 * @param tipoAccion Tipo de acci�n realizada sobre el objeto a registrar (GUARDAR, BORRAR, ACTUALIZAR)
	 * @param nombreUsuario Nombre del usuario que est� realizando la acci�n
	 * @param tipoEntidad Clasificaci�n de la objeto a registrar por su �rea de negocio
	 * @param nombreEntidad Nombre de clase del objeto a registrar
	 * @param nombreTablaEntidad Nombre de la Tabla en Base de Datos asociada al objeto a registrar
	 */
	@SuppressWarnings("unused")
	private void agregarLog(BigDecimal idRegistro, String tipoAccion, String nombreUsuario,
			String tipoEntidad, String nombreEntidad, String nombreTablaEntidad) {
		try {
			
			LogOperacionDTO registroLog = new LogOperacionDTO();
			
			registroLog.setTipoaccion(tipoAccion);
			registroLog.setIdregistro(idRegistro);
			registroLog.setNombreusuario(nombreUsuario);
			registroLog.setTipoentidad(tipoEntidad);
			registroLog.setNombreentidad(nombreEntidad);
			registroLog.setNombretablaentidad(nombreTablaEntidad);
					
			new LogOperacionSN().agregar(registroLog);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	
		
}
