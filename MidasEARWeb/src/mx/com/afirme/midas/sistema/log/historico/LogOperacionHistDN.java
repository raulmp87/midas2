/**
 * 
 */
package mx.com.afirme.midas.sistema.log.historico;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class LogOperacionHistDN {

	public static final LogOperacionHistDN INSTANCIA = new LogOperacionHistDN();

	public static LogOperacionHistDN getInstancia (){
		return LogOperacionHistDN.INSTANCIA;
	}
	
	public void agregar(LogOperacionHistDTO registroLog) throws ExcepcionDeAccesoADatos, SystemException {
		new LogOperacionHistSN().agregar(registroLog);
	}
	
}
