/**
 * 
 */
package mx.com.afirme.midas.sistema.log.historico;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class LogOperacionHistSN {

private LogOperacionHistFacadeRemote beanRemoto;
	
	public LogOperacionHistSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(LogOperacionHistFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(LogOperacionHistDTO registroLog) {
		try {
			beanRemoto.save(registroLog);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	
	
}
