package mx.com.afirme.midas.sistema;

import java.lang.reflect.Array;
import java.util.List;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.commons.beanutils.ConversionException;
import org.apache.struts.validator.DynaValidatorActionForm;

public class MidasDynaBaseForm extends DynaValidatorActionForm {
	private static final long serialVersionUID = 1L;
	private String mensaje;
	private String tipoMensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public void setMensajeUsuario(Exception e) {
		if (e == null) {
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
					"mensaje.informacion.exito"));
			setTipoMensaje(Sistema.EXITO);
		} else if (e instanceof SystemException) {
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
					"mensaje.excepcion.sistema.nodisponible"));
			setTipoMensaje(Sistema.INFORMACION);
		} else if (e instanceof ExcepcionDeAccesoADatos) {
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
					"mensaje.excepcion.rollback"));
			setTipoMensaje(Sistema.ERROR);
		} else if (e instanceof Exception) {
			setMensaje(e.getCause().toString());
			setTipoMensaje(Sistema.ERROR);
		}
	}

	@SuppressWarnings("unchecked")
	public void set(String name, int index, Object value) {
		Object prop = this.dynaValues.get(name);
		Class arrayClass = prop.getClass();
		if (prop == null) {
			throw new NullPointerException("No indexed value for '" + name
					+ "[" + index + "]'");
		}
		if (arrayClass.isArray()) {
			if (Array.getLength(prop) < index + 1) {
				Object newArray = Array.newInstance(arrayClass
						.getComponentType(), index + 1);
				System.arraycopy(prop, 0, newArray, 0, Array.getLength(prop));
				this.dynaValues.put(name, newArray);
				prop = newArray;
			}
			Array.set(prop, index, value);
		} else if (prop instanceof List) {
			List list = (List) prop;
			if (list.size() < index + 1) {
				for (int i = list.size(); i <= index; i++) {
					list.add(null);
				}
			}
			try {
				list.set(index, value);
			} catch (ClassCastException e) {
				throw new ConversionException(e.getMessage());
			}
		} else {
			throw new IllegalArgumentException("Non-indexed property for '"
					+ name + "[" + index + "]'");
		}
	}
}
