package mx.com.afirme.midas.sistema;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class MidasRequest extends HttpServletRequestWrapper {

	
	public MidasRequest(HttpServletRequest request) {
		super(request);
	}
	
	public java.lang.String getQueryString() {
		
		String parametroQueryString = "nocache=" + (new java.util.Date()).getTime();
		String queryString = "";
		
		if (super.getQueryString() != null) {
			return queryString.concat(super.getQueryString()).concat("&").concat(parametroQueryString);
		} else {
			return queryString.concat(parametroQueryString);
		}
		
		
	}

}
