package mx.com.afirme.midas.sistema.download;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DownloadAction extends MidasMappingDispatchAction {

	/** Log de DownloadAction */
	private static final Logger LOG = LoggerFactory.getLogger(DownloadAction.class);
	private FileManagerService fileManagerService;
	
	public DownloadAction() {
		try {
			fileManagerService = ServiceLocator.getInstance().getEJB(FileManagerService.class);
		} catch (SystemException e) {
			LOG.error("-- DownloadAction()", e);
		}
	}	
	
	public void descargarArchivo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		OutputStream output = null;

		try {

			final String idControlArchivo = request.getParameter("idControlArchivo");
			final ControlArchivoDTO controlArchivoDTO = ControlArchivoDN
					.getInstancia().getPorId(new BigDecimal(idControlArchivo));
			final String fileName = fileManagerService.getFileName(controlArchivoDTO);
			final String nodo = controlArchivoDTO.getCodigoExterno();
			byte[] byteArray = fileManagerService.downloadFile(fileName, nodo);
			response.setHeader("Content-Disposition", "attachment; filename="
					+ URLEncoder.encode(controlArchivoDTO.getNombreArchivoOriginal(), "ISO-8859-1"));
			response.setContentType("application/unknown");
			response.setContentLength(byteArray.length);
			response.setBufferSize(1024 * 15);
			
			output = response.getOutputStream(); 
			output.write(byteArray);
			output.flush();
			output.close();


		} catch (Exception e) {
			LOG.error("-- descargarArchivo()", e);
		} finally {
			IOUtils.closeQuietly(output);

		}
	}

}