package mx.com.afirme.midas.sistema.creadorxls;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

/**
 * @author Jos� Luis Arellano
 */
public class MidasHojaXLS extends MidasXLSCreator{
	private String nombreHoja;
	private MidasDocumentoXLS documentoXLS;
	
	public MidasHojaXLS(String nombreHoja,List<String> nombreColumnas,List<String> nombreAtributos,MidasDocumentoXLS documentoXLS,Short formatoReporte) {
		super(nombreColumnas, nombreAtributos,formatoReporte);
		this.documentoXLS = documentoXLS;
		this.nombreHoja = nombreHoja;
	}
	
	public MidasHojaXLS(String nombreHoja,String[] nombreColumnas,String[] nombreAtributos,MidasDocumentoXLS documentoXLS,Short formatoReporte) {
		super(nombreColumnas, nombreAtributos,formatoReporte);
		this.documentoXLS = documentoXLS;
		this.nombreHoja = nombreHoja;
	}
	
	@Override
	public void iniciarProcesamientoArchivoXLS(String nombreHoja) {
		contadorFilas = contadorHojas = 0;
		if(documentoXLS != null){
			if (documentoXLS.getDocumentoXLSX() == null){
				documentoXLS.setDocumentoXLSX(new XSSFWorkbook());
			}
			if(!UtileriasWeb.esCadenaVacia(nombreHoja)){
				this.nombreHoja = nombreHoja;
			}
			if(UtileriasWeb.esCadenaVacia(this.nombreHoja)){
				this.nombreHoja = "Hoja 1";
			}
			hojaXLSX = documentoXLS.getDocumentoXLSX().createSheet(this.nombreHoja);
			try {
				incrementarContadorHojas();
			} catch (SystemException e) {}
			XSSFRow fila = hojaXLSX.createRow(contadorFilas);
			int contadorColumna = 0;
			for(String nombreColumna : nombreColumnas){
				fila.createCell(contadorColumna).setCellValue(new XSSFRichTextString(nombreColumna));
				contadorColumna++;
			}
			try {
				incrementarContadorFilas();
			} catch (SystemException e) {}
		}
	}
	
	public void iniciarProcesamientoArchivoXLS() {
		iniciarProcesamientoArchivoXLS(null);
	}

	public MidasDocumentoXLS getDocumentoXLS() {
		return documentoXLS;
	}
	public void setDocumentoXLS(MidasDocumentoXLS documentoXLS) {
		this.documentoXLS = documentoXLS;
	}
	public String getNombreHoja() {
		return nombreHoja;
	}
	public void setNombreHoja(String nombreHoja) {
		this.nombreHoja = nombreHoja;
	}
}
