package mx.com.afirme.midas.sistema.creadorxls;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 * 
 * @author Jos� Luis Arellano
 *
 */
public class MidasXLSCreator {
	
	public static final Short FORMATO_XLS = new Short("1");
	public static final Short FORMATO_XLSX = new Short("2");
	
	private static final int MAXIMO_FILAS = 65536;
	private static final int MAXIMO_HOJAS = 5;
	protected List<String> nombreColumnas;
	protected List<String> nombreAtributos;
//	protected List<String> listaFormatoCeldas;
	protected HSSFWorkbook documentoXLS;
	protected HSSFSheet hojaXLS;
	protected XSSFWorkbook documentoXLSX;
	protected XSSFSheet hojaXLSX;

	protected int contadorFilas=0;
	protected int contadorHojas = 0;
	protected String nombreHoja;
	private Short formatoReporte;

	public MidasXLSCreator(List<String> nombreColumnas,List<String> nombreAtributos,Short formatoReporte){
		this.nombreColumnas = nombreColumnas;
		setFormatoReporte(formatoReporte);
		if(nombreAtributos != null){
			this.nombreAtributos = new ArrayList<String>();
			for(String nombreAtributo : nombreAtributos){
				nombreAtributos.add("get"+nombreAtributo.substring(0, 1).toUpperCase()+nombreAtributo.substring(1));
			}
		}
	}
	
	public MidasXLSCreator(String[] nombreColumnas,String []nombreAtributos,Short formatoReporte){
		this.nombreAtributos = new ArrayList<String>();
		setFormatoReporte(formatoReporte);
		for(int i=0;i<nombreAtributos.length;i++)
			this.nombreAtributos.add("get"+nombreAtributos[i].substring(0, 1).toUpperCase()+nombreAtributos[i].substring(1));
		this.nombreColumnas = new ArrayList<String>();
		for(int i=0;i<nombreColumnas.length;i++)
			this.nombreColumnas.add(nombreColumnas[i]);
	}
	
	public void iniciarProcesamientoArchivoXLS(String nombreHoja) throws SystemException {
		contadorFilas = contadorHojas = 0;
		crearDocumento();
		crearHoja(this.nombreHoja);
		Object fila = crearFila();
		int contadorColumna = 0;
		for(String nombreColumna : nombreColumnas){
			Object celda = crearCelda(fila, contadorColumna);
			estableceValorCelda(celda, nombreColumna);
//			fila.createCell(contadorColumna).setCellValue(new HSSFRichTextString(nombreColumna));
			contadorColumna++;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void insertarFilasArchivoXLS(List<Object> registros) throws SystemException{
		if(registros != null && !registros.isEmpty()){
			java.lang.reflect.Method metodoGetter = null;
			Object valor = null;
			Class []sinParametros = new Class[0];
			for(Object registro : registros){
//				HSSFRow fila = hojaXLS.createRow(contadorFilas);
				Object fila = crearFila();

				int contadorColumna = 0;
				for(String nombreAtributo : nombreAtributos){
					try{
						metodoGetter = registro.getClass().getMethod(nombreAtributo,sinParametros);
						valor = metodoGetter.invoke(registro);
					}catch(Exception e){
						throw new SystemException("Ocurri� un error al intentar objtener el atributo '"+nombreAtributo+"' del objeto: "+registro);
					}
					if(valor != null){
//						String formato = null;
//						if (listaFormatoCeldas != null){
//							try{
//								formato = listaFormatoCeldas.get(contadorColumna);
//							}catch(Exception e){}
//						}
						Object celda = crearCelda(fila, contadorColumna);
						estableceValorCelda(celda, valor);
//						HSSFCell celda = fila.createCell(contadorColumna);
//						if(formato != null){
//							try{
//								celda.setCellType( HSSFCell.CELL_TYPE_NUMERIC);
//								celda.setCellValue(Double.valueOf((getStringFromObject(valor))));
//							}catch(Exception e){
//								celda.setCellValue(new HSSFRichTextString((getStringFromObject(valor))));
//							}
//						}
//						else{
//							try{
//								celda.setCellType( obtenerTipoCelda(valor));
//								if (valor instanceof String)
//									celda.setCellValue(new HSSFRichTextString((String)valor));
//								else
//									celda.setCellValue(Double.valueOf((getStringFromObject(valor))));
//							}catch(Exception e){
//								celda.setCellValue(new HSSFRichTextString((getStringFromObject(valor))));
//							}
//						}

					}
//					else
//						fila.createCell(contadorColumna).setCellValue(new HSSFRichTextString(""));
					contadorColumna++;
				}
			}
		}
	}
	
	public byte[] finalizarProcesamientoArchivoXLS(String nombreArchivo) throws IOException{
		byte [] archivo = null;
		if(nombreArchivo != null){
			FileOutputStream fileOut = new FileOutputStream(nombreArchivo);
			if(documentoXLS != null){
				documentoXLS.write(fileOut);
			}
			else if (documentoXLSX != null){
				documentoXLSX.write(fileOut);
			}
			fileOut.close();
		}
		if(documentoXLS != null){
			archivo = documentoXLS.getBytes();
		}
		else if (documentoXLSX != null){
			ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
			documentoXLSX.write(outputByteArray);
			archivo = outputByteArray.toByteArray();
		}
		return archivo;
	}
	
	public byte[] finalizarProcesamientoArchivoXLS() throws IOException{
		return finalizarProcesamientoArchivoXLS(null);
	}
	
	private void crearDocumento(){
		if(formatoReporte.equals(FORMATO_XLS)){
			documentoXLS = new HSSFWorkbook();
		}
		else if(formatoReporte.equals(FORMATO_XLSX)){
			documentoXLSX = new XSSFWorkbook();
		}
	}
	
	private void crearHoja(String nombreHoja) throws SystemException{
		if(documentoXLS != null){
			hojaXLS = documentoXLS.createSheet(this.nombreHoja = nombreHoja!=null ? nombreHoja : "Hoja 1");
		}
		else if (documentoXLSX != null){
			hojaXLSX = documentoXLSX.createSheet(this.nombreHoja = nombreHoja!=null ? nombreHoja : "Hoja 1");
		}
		incrementarContadorHojas();
	}
	
	private Object crearFila() throws SystemException{
		Object fila = null;
		if(hojaXLS != null){
			fila = hojaXLS.createRow(contadorFilas);
		}
		else if (hojaXLSX != null){
			fila = hojaXLSX.createRow(contadorFilas);
		}
		if(fila != null){
			incrementarContadorFilas();
		}
		return fila;
	}
	
	private Object crearCelda(Object fila,int contadorColumna){
		Object celda = null;
		if(fila != null){
			if(fila instanceof HSSFRow){
				celda = ((HSSFRow)fila).createCell(contadorColumna);
			}
			else if(fila instanceof XSSFRow){
				celda = ((XSSFRow)fila).createCell(contadorColumna);
			}
		}
		return celda;
	}
	
	private void estableceValorCelda(Object celda, Object valor){
		if(celda != null && valor != null){
			if(celda instanceof HSSFCell){
				HSSFCell celdaParseada = (HSSFCell)celda;
				try{
					celdaParseada.setCellType( obtenerTipoCelda(celda,valor));
					if (valor instanceof String)
						celdaParseada.setCellValue(new HSSFRichTextString((String)valor));
					else
						celdaParseada.setCellValue(Double.valueOf((getStringFromObject(valor))));
				}catch(Exception e){
					celdaParseada.setCellValue(new HSSFRichTextString((getStringFromObject(valor))));
				}
			}
			else if(celda instanceof XSSFCell){
				XSSFCell celdaParseada = (XSSFCell)celda;
				try{
					celdaParseada.setCellType( obtenerTipoCelda(celda,valor));
					if (valor instanceof String)
						celdaParseada.setCellValue(new XSSFRichTextString((String)valor));
					else
						celdaParseada.setCellValue(Double.valueOf((getStringFromObject(valor))));
				}catch(Exception e){
					celdaParseada.setCellValue(new XSSFRichTextString((getStringFromObject(valor))));
				}
			}
		}
		else if(celda != null){
			estableceValorCelda(celda, "");
		}
	}
	
	private int obtenerTipoCelda(Object celda,Object object){
		int tipo = 0;
		if(celda instanceof HSSFCell){
			tipo = HSSFCell.CELL_TYPE_STRING;
			if (object != null){
				if(object instanceof BigDecimal || object instanceof Integer || object instanceof Double || object instanceof Short || object instanceof Long)
					tipo = HSSFCell.CELL_TYPE_NUMERIC;
			}
		}
		else if(celda instanceof XSSFCell){
			tipo = XSSFCell.CELL_TYPE_STRING;
			if (object != null){
				if(object instanceof BigDecimal || object instanceof Integer || object instanceof Double || object instanceof Short || object instanceof Long)
					tipo = XSSFCell.CELL_TYPE_NUMERIC;
			}
		}
		return tipo;
	}
	
	private String getStringFromObject(Object object){
		String result = null;
		if (object != null){
			if(object instanceof Date){
				Date fecha = (Date) object;
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				result = dateFormat.format(fecha);
			}
			else{
				if(object instanceof BigDecimal){
					result = ((BigDecimal)(object)).toPlainString();
				}
				else if(object instanceof Integer){
					result = Double.valueOf((Integer)(object)).toString();
				}
				else if(object instanceof Double){
					result = ((Double)(object)).toString();
				}
				else if(object instanceof Short){
					result = ((Short)object).toString();
				}
				else if(object instanceof Long){
					result = ((Long)(object)).toString();
				} else
					result = object.toString();
			}
		}
		return result;
	}
	
	protected void incrementarContadorFilas() throws SystemException{
		contadorFilas++;
		if(contadorFilas >= MAXIMO_FILAS){
			contadorFilas = 0;
			incrementarContadorHojas();
			hojaXLS = documentoXLS.createSheet(nombreHoja = nombreHoja+"_"+contadorHojas);
			HSSFRow fila = hojaXLS.createRow(contadorFilas);
			int contadorColumna = 0;
			for(String nombreColumna : nombreColumnas){
				fila.createCell(contadorColumna).setCellValue(new HSSFRichTextString(nombreColumna));
				contadorColumna++;
			}
			incrementarContadorFilas();
		}
	}
	
	protected void incrementarContadorHojas() throws SystemException{
		contadorHojas ++;
		if(contadorHojas > MAXIMO_HOJAS){
			throw new SystemException("Se alcanz� el n�mero m�ximo de hojas permitidas: "+contadorHojas);
		}
	}

//	public List<String> getListaFormatoCeldas() {
//		return listaFormatoCeldas;
//	}
//	public void setListaFormatoCeldas(List<String> listaFormatoCeldas) {
//		this.listaFormatoCeldas = listaFormatoCeldas;
//	}
//	public void setListaFormatoCeldas(String [] arrayFormatoCeldas) {
//		if(arrayFormatoCeldas != null){
//			listaFormatoCeldas = new ArrayList<String>();
//			for(int i=0;i<arrayFormatoCeldas.length;i++){
//				listaFormatoCeldas.add(arrayFormatoCeldas[i]);
//			}
//		}
//	}

	public Short getFormatoReporte() {
		return formatoReporte;
	}

	public void setFormatoReporte(Short formatoReporte) {
		if(formatoReporte != null && 
				(formatoReporte.compareTo(FORMATO_XLS) ==0 || 
				formatoReporte.compareTo(FORMATO_XLSX) == 0)){
			this.formatoReporte = formatoReporte;
		}
		else throw new IllegalArgumentException("Formato de reporte no v�lido, utilice las constantes de MidasXLSCreator");
	}

	public HSSFWorkbook obtieneHSSFWorkbook() {
		return documentoXLS;
	}
}
