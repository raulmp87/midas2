package mx.com.afirme.midas.sistema.creadorxls;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author Jos� Luis Arellano
 */
public class MidasDocumentoXLS {
	private List<MidasHojaXLS> hojas = new ArrayList<MidasHojaXLS>();
	protected XSSFWorkbook documentoXLSX;
//	protected XSSFWorkbook documentoXLS;
	
	public MidasDocumentoXLS(){
		documentoXLSX = new XSSFWorkbook();
	}
	
	public byte[] finalizarProcesamientoArchivoXLS() throws IOException{
		return finalizarProcesamientoArchivoXLS(null);
	}
	
	public byte[] finalizarProcesamientoArchivoXLS(String nombreArchivo) throws IOException{
		byte [] archivo = null;
		if(nombreArchivo != null){
			FileOutputStream fileOut = new FileOutputStream(nombreArchivo);
			documentoXLSX.write(fileOut);
			fileOut.close();
		}
		ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
		documentoXLSX.write(outputByteArray);
		archivo = outputByteArray.toByteArray();
		return archivo;
	}
	
	public void agregarHojaXLS(MidasHojaXLS hoja){
		if(hoja != null){
			hoja.setDocumentoXLS(this);
			hojas.add(hoja);
		}
	}
	
	public XSSFWorkbook getDocumentoXLSX() {
		return documentoXLSX;
	}

	public void setDocumentoXLSX(XSSFWorkbook documentoXLSX) {
		this.documentoXLSX = documentoXLSX;
	}
}
