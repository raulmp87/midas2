package mx.com.afirme.midas.sistema.excepcion;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;

public class ExcepcionDeAccesoADatos extends RuntimeException{
	private static final long serialVersionUID = 1L;

	private String claseOrigen;
	private String descripcion;
	private Throwable ex;
	
	public ExcepcionDeAccesoADatos(String descripcion) {
		super(descripcion);
	}
	
	public ExcepcionDeAccesoADatos(String claseOrigen, Throwable ex) {
		this.claseOrigen = claseOrigen;
		this.ex = ex;
		LogDeMidasWeb.log("Excepcion en capa de Acceso a Datos en " + this.claseOrigen.trim() + " de tipo " + 
				this.ex.getClass().getCanonicalName(), Level.SEVERE, this.ex);
		this.ex.printStackTrace();
	}
	
	public ExcepcionDeAccesoADatos(String claseOrigen, Throwable ex, String descripcion) {
		this.claseOrigen = claseOrigen;
		this.ex = ex;
		this.descripcion = descripcion;
		LogDeMidasWeb.log("Excepcion en capa de Acceso a Datos en " + this.claseOrigen.trim() + " de tipo " + 
				this.ex.getClass().getCanonicalName(), Level.SEVERE, this.ex);
		this.ex.printStackTrace();
	}

	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	/**
	 * @return the claseOrigen
	 */
	public String getClaseOrigen() {
		return claseOrigen;
	}

	/**
	 * @return the ex
	 */
	public Throwable getEx() {
		return ex;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	
	
	
	
}
