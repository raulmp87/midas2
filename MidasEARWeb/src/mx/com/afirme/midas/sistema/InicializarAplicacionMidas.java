/**
 * 
 */
package mx.com.afirme.midas.sistema;

import java.util.logging.Level;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoDN;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorContabilizarSiniestrosDN;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDN;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorEstadoCuentaVigentesDN;
import mx.com.afirme.midas2.service.OracleCurrentDateService;
import net.sf.jasperreports.engine.util.JRProperties;

import org.quartz.SchedulerException;
import org.quartz.ee.jta.UserTransactionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;



/**
 * @author Jesus Enrique Aldana Sanchez
 *
 */
public class InicializarAplicacionMidas implements ServletContextListener {

	
	@Autowired
	private OracleCurrentDateService oracleCurrentDateService;
	
	/**
	 * 
	 */
	public InicializarAplicacionMidas() {
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {		
		WebApplicationContextUtils
				.getRequiredWebApplicationContext(arg0.getServletContext())
				.getAutowireCapableBeanFactory().autowireBean(this);

		javax.servlet.ServletContext contexto = arg0.getServletContext();
		inicializarRutaEstatica(contexto);
		
		//Obtiene el UserTransaction del contexto del servidor para utilizarlo cuando la cancelacion y/o rehabilitacion
		//de endosos por medio del job lo requiera
		try {
			UserTransaction ut = UserTransactionHelper.lookupUserTransaction();
			System.out.println(ut.getStatus());
			CancelacionEndosoDN.getInstancia2(ut);
			
		} catch (SchedulerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		try {
			LogDeMidasWeb.log("En espera del arranque de la persistencia...5 Minutos",
					Level.ALL, null);			
			Thread.sleep(Sistema.PAUSAARRANQUEWEB);
		} catch (InterruptedException e) {
			LogDeMidasWeb.log(
					"No es posible ejecutar el retardo de arranque.. ",
					Level.ALL, null);
		}
		//Inicia el temporizador de respaldo de log de Midas 
		TemporizadorDN.getInstancia().iniciar(Sistema.HORA_INICIO_PROCESO_LOG);
		//Inicia el temporizador para contabilizar siniestros
		TemporizadorContabilizarSiniestrosDN.getInstancia().iniciar(Sistema.HORA_INICIO_PROCESO_SINIESTRO);
		//Inicia el temporizador de Estados de Cuenta Vigentes
		TemporizadorEstadoCuentaVigentesDN.getInstancia().iniciar(Sistema.HORA_INICIO_PROCESO_EDOCUENTA_VIGENTES);
		
		JRProperties.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
		
		//configurar eibs
		try{
            com.afirme.commons.CommonDefinitions.ISERIES_SERVER= Sistema.EIBS_CONFIG_HOST;
            com.afirme.commons.CommonDefinitions.INITIAL_SOCKET= Sistema.EIBS_CONFIG_PORT;
            com.afirme.commons.CommonDefinitions.SOCKET_TIME_OUT= Sistema.EIBS_CONFIG_TIMEOUT;
		}catch(Exception ex){
			LogDeMidasWeb.log("No es posible configurar los parametros para el EIBS", Level.ALL, null);
		}
		
		//Cambiar implementaciÃ³n para que se utilice la fecha de la base de datos. 
		TimeUtils.setCurrentDateProvider(oracleCurrentDateService);
	}

	private void inicializarRutaEstatica(javax.servlet.ServletContext contexto) { 		
		
		try {
			
			contexto.setAttribute(Sistema.RUTAESTATICA, contexto.getRealPath("/")+"/");
			
			
		} catch (Exception e) {
			LogDeMidasWeb.log("No es posible fijar la ruta estatica" , Level.SEVERE, null);
			
		}
	}
}
