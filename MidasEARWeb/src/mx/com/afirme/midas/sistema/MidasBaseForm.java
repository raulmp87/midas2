package mx.com.afirme.midas.sistema;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.validator.ValidatorForm;

public class MidasBaseForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mensaje;

	private String tipoMensaje;
	
	private String totalRegistros;
	
	private String numeroPaginaActual;
	
	private String paginaInferiorCache;
	
	private String paginaSuperiorCache;
	
	private Integer primerRegistroACargar;
	
	private Integer numeroMaximoRegistrosACargar;
		
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public String getTotalRegistros() {
		if (totalRegistros != null) {
			return totalRegistros;
		} else {
			return "0";
		}
		
	}

	public void setTotalRegistros(String totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public String getNumeroPaginaActual() {
		if (numeroPaginaActual != null) {
			return numeroPaginaActual;
		} else {
			return "1";
		}
	}

	public void setNumeroPaginaActual(String numeroPaginaActual) {
		this.numeroPaginaActual = numeroPaginaActual;
	}

	public String getPaginaInferiorCache() {
		if (paginaInferiorCache != null) {
			return paginaInferiorCache;
		} else {
			return "0";
		}
	}

	public void setPaginaInferiorCache(String paginaInferiorCache) {
		this.paginaInferiorCache = paginaInferiorCache;
	}

	public String getPaginaSuperiorCache() {
		if (paginaSuperiorCache != null) {
			return paginaSuperiorCache;
		} else {
			return "0";
		}
	}

	public void setPaginaSuperiorCache(String paginaSuperiorCache) {
		this.paginaSuperiorCache = paginaSuperiorCache;
	}

	public Integer getPrimerRegistroACargar() {
		return primerRegistroACargar;
	}

	public void setPrimerRegistroACargar(Integer primerRegistroACargar) {
		this.primerRegistroACargar = primerRegistroACargar;
	}

	public Integer getNumeroMaximoRegistrosACargar() {
		return numeroMaximoRegistrosACargar;
	}

	public void setNumeroMaximoRegistrosACargar(Integer numeroMaximoRegistrosACargar) {
		this.numeroMaximoRegistrosACargar = numeroMaximoRegistrosACargar;
	}

	@SuppressWarnings("unchecked")
	public List getListaPaginada(HttpServletRequest request) {
		if (request.getSession().getAttribute(Sistema.LISTA_PAGINADA) != null) {
			return (List) request.getSession().getAttribute(Sistema.LISTA_PAGINADA);
		} else {
			return null;
		}
		
	}

	@SuppressWarnings("unchecked")
	public void setListaPaginada(List listaPaginada, HttpServletRequest request) {
		
		if (request.getSession().getAttribute(Sistema.LISTA_PAGINADA) != null) {
			request.getSession().removeAttribute(Sistema.LISTA_PAGINADA);
		}
		
		request.getSession().setAttribute(Sistema.LISTA_PAGINADA, listaPaginada);
		
	}

	public void setMensajeUsuario (Exception e){
		
		if (e == null){
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito"));
			setTipoMensaje(Sistema.EXITO);			
		}else if (e instanceof SystemException){
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.sistema.nodisponible"));
			setTipoMensaje(Sistema.INFORMACION);						
		}else if (e instanceof ExcepcionDeAccesoADatos){
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.rollback"));
			setTipoMensaje(Sistema.ERROR);						
		}else if (e instanceof Exception){
			setMensaje(e.getCause().toString());
			setTipoMensaje(Sistema.ERROR);	
		}
	}
	
	/**
	 * funcion para mostrar mensajes al usuario una vez que se agrega, modifica o borrar informacion
	 * tipo: "guardar", "modificar", "eliminar"
	 */
	public void setMensajeUsuario (Exception e, String tipo){
	 	if (e == null){
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito." + tipo.toLowerCase()));
		 	setTipoMensaje(Sistema.EXITO);			
		}else if (e instanceof SystemException){
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.sistema.nodisponible"));
			setTipoMensaje(Sistema.INFORMACION);						
		}else if (e instanceof ExcepcionDeAccesoADatos){
			setMensaje(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.rollback"));
			setTipoMensaje(Sistema.ERROR);						
		}else if (e instanceof Exception){
			setMensaje(e.getCause().toString());
			setTipoMensaje(Sistema.ERROR);	
		}
	}
}
