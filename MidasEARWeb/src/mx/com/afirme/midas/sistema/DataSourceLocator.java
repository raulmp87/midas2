package mx.com.afirme.midas.sistema;

import java.util.logging.Level;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class DataSourceLocator {

  private static final DataSourceLocator INSTANCE = new DataSourceLocator();

  private Context context = null;

  private DataSourceLocator() {
    try{
      this.initializeContext();
    }catch (NamingException nException){
    	LogDeMidasWeb.log("Unable to initialize Context.", Level.INFO,nException);
    } // End of try/catch
  }

  private void initializeContext() throws NamingException {
    NamingUtil.close(this.context);
    this.context = new InitialContext();
    LogDeMidasWeb.log("Context Initialized to: " + this.context,Level.INFO,null);
  }

  public static DataSource getDataSource(String name) throws SystemException {
    LogDeMidasWeb.log("Retrieving dataSource: " + name,Level.INFO,null);
    DataSource dataSource = null;
    try{
      if(INSTANCE.context == null)
        INSTANCE.initializeContext();
      try{
        dataSource =
            (DataSource)INSTANCE.context.lookup("java:comp/env/" + name);
      }catch (NamingException nException){
        dataSource = (DataSource)INSTANCE.context.lookup(name);
      } // End of try/catch
      if(dataSource == null){
        throw new NamingException("Resource " + name + " not found.");
      } // End of if
    }catch (NamingException nException){
      throw new SystemException(nException);
    } // End of try/catch
    return dataSource;
  }

}
