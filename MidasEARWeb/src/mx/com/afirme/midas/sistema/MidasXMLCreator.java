package mx.com.afirme.midas.sistema;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Jose Luis Arellano
 */
public class MidasXMLCreator {
	private List<String> nombreAtributos;
	
	public MidasXMLCreator(List<String> nombreAtributos){
		this.nombreAtributos = nombreAtributos;
	}
	
	public MidasXMLCreator(String[] nombreAtributos){
		this.nombreAtributos = new ArrayList<String>();
		for(int i=0;i<nombreAtributos.length;i++)
			this.nombreAtributos.add(nombreAtributos[i]);
	}
	
	/**
	 * Regresa la cadena de caracteres correspondiente a un documento XML en base al orden de los atributos establecidos (nombreAtributos).
	 * El m�todo intenta obtener cada atributo del bean que recibe, en caso de no encontrarlo lanza una excepci�n.
	 * @param bean
	 * @param nombreNodoRaiz
	 * @param nombreNodoSecundario
	 * @return String documento XML generado.
	 * @throws ParserConfigurationException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws SystemException
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 */
	public String obtenerDocumentoXML(Object bean,String nombreNodoRaiz,String nombreNodoSecundario) throws Exception{
		Document xmlDoc = null;
		Element nodoRaiz = null;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactoryImpl.newInstance();
        DocumentBuilder docBuilder = null;
		docBuilder = dbFactory.newDocumentBuilder();
		xmlDoc = docBuilder.newDocument();
		nodoRaiz = xmlDoc.createElement(nombreNodoRaiz);
		xmlDoc.appendChild(nodoRaiz);
		nodoRaiz.appendChild(obtenerElementoXML(bean, nombreNodoSecundario, xmlDoc));
		return obtenerString(xmlDoc);
	}

	/**
	 * Regresa la cadena de caracteres correspondiente a un documento XML en base al orden de los atributos establecidos (nombreAtributos).
	 * El m�todo intenta obtener cada atributo de la lista de beans que recibe, en caso de no encontrarlo lanza una excepci�n.
	 * @param listaBeans
	 * @param nombreNodoRaiz
	 * @param nombreNodoSecundario
	 * @return
	 * @throws Exception
	 */
	public String obtenerDocumentoXML(List<Object> listaBeans,String nombreNodoRaiz,String nombreNodoSecundario) throws Exception{
		Document xmlDoc = null;
		Element nodoRaiz = null;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactoryImpl.newInstance();
        DocumentBuilder docBuilder = null;
		docBuilder = dbFactory.newDocumentBuilder();
		xmlDoc = docBuilder.newDocument();
		nodoRaiz = xmlDoc.createElement(nombreNodoRaiz);
		xmlDoc.appendChild(nodoRaiz);
		for(Object bean : listaBeans){
			nodoRaiz.appendChild(obtenerElementoXML(bean, nombreNodoSecundario, xmlDoc));
		}
		return obtenerString(xmlDoc);
	}

	/**
	 * Genera un documento XML con la informaci�n recibida en la lista de objetos. El archivo es generado con el nombre recibido y en la carpeta recibida.
	 * El orden de los atributos corresponde con la lista de nombres de atributos recibida en el constructor. Cada atributo es extra�do  cada objeto de la lista recibida. 
	 * @param listaBeans
	 * @param nombreNodoRaiz
	 * @param nombreNodoSecundario
	 * @param carpeta
	 * @param nombreArchivo
	 * @return true, si el archivo se genera exitosamente, false de lo contrario.
	 */
	public Boolean generarDocumentoXML(List<Object> listaBeans,String nombreNodoRaiz,String nombreNodoSecundario,String carpeta,String nombreArchivo) {
		Document xmlDoc = null;
		Element nodoRaiz = null;
		boolean result = true;
		try{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactoryImpl.newInstance();
        DocumentBuilder docBuilder = null;
		docBuilder = dbFactory.newDocumentBuilder();
		xmlDoc = docBuilder.newDocument();
		nodoRaiz = xmlDoc.createElement(nombreNodoRaiz);
		xmlDoc.appendChild(nodoRaiz);
		for(Object bean : listaBeans){
			nodoRaiz.appendChild(obtenerElementoXML(bean, nombreNodoSecundario, xmlDoc));
		}
		String contenido = obtenerString(xmlDoc);
		generarArchivoXML(contenido, carpeta+nombreArchivo);
		} catch(Exception e){
			result = false;
		}
		return result;
	}
	
	/**
	 * Genera un documento XML con la informaci�n recibida en la lista de objetos. El archivo es generado con el nombre recibido y en la por default de documentos Anexos de MidasWeb.
	 * El orden de los atributos corresponde con la lista de nombres de atributos recibida en el constructor. Cada atributo es extra�do  cada objeto de la lista recibida. 
	 * @param listaBeans
	 * @param nombreNodoRaiz
	 * @param nombreNodoSecundario
	 * @param nombreArchivo
	 * @return true, si el archivo se genera exitosamente, false de lo contrario.
	 */
	public Boolean generarDocumentoXML(List<Object> listaBeans,String nombreNodoRaiz,String nombreNodoSecundario,String nombreArchivo) {
		Document xmlDoc = null;
		Element nodoRaiz = null;
		boolean result = true;
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactoryImpl.newInstance();
			DocumentBuilder docBuilder = null;
			docBuilder = dbFactory.newDocumentBuilder();
			xmlDoc = docBuilder.newDocument();
			nodoRaiz = xmlDoc.createElement(nombreNodoRaiz);
			xmlDoc.appendChild(nodoRaiz);
			for(Object bean : listaBeans){
				nodoRaiz.appendChild(obtenerElementoXML(bean, nombreNodoSecundario, xmlDoc));
			}
			String contenido = obtenerString(xmlDoc);
			generarArchivoXML(contenido, nombreArchivo);
		} catch(Exception e){
			result = false;
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private Element obtenerElementoXML(Object bean,String nombreNodo,Document xmlDoc) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		Element nodoBean = null;
		Element node = null;
		Method metodoGetter = null;
		Object valor = null;
		nodoBean = xmlDoc.createElement(nombreNodo);
		Class []sinParametros = new Class[0];
		for(String childName : nombreAtributos){
			String getterMethodName = "get"+childName.substring(0, 1).toUpperCase()+childName.substring(1);
			metodoGetter = bean.getClass().getMethod(getterMethodName,sinParametros);
			valor = metodoGetter.invoke(bean);
			node = xmlDoc.createElement(childName);
			if(valor != null)
				node.appendChild(xmlDoc.createCDATASection(new String(getStringFromObject(valor))));
			nodoBean.appendChild(node);
		}
		return nodoBean;
	}
	
	private String obtenerString(Document xmlDoc) throws SystemException{
		StringWriter stringWriter = null;
		XMLSerializer xmlSerializer = null;
		OutputFormat outFormat = null;
		try{
			xmlSerializer = new XMLSerializer();
			stringWriter = new StringWriter();
			outFormat = new OutputFormat();
			outFormat.setEncoding("ISO-8859-1");
			outFormat.setVersion("1.0");
			outFormat.setIndenting(true);
			outFormat.setIndent(4);
			
			xmlSerializer.setOutputCharStream(stringWriter);
			xmlSerializer.setOutputFormat(outFormat);
			xmlSerializer.serialize(xmlDoc);
			
			stringWriter.close();
		} catch(IOException e){
			throw new SystemException("Ocurri� un error al convertir el documento en String");
		}
		return stringWriter.toString();
	}
	
	private void generarArchivoXML(String contenido,String nombreCompletoArchivo) throws IOException{
		OutputStream fout= new FileOutputStream(nombreCompletoArchivo);
		OutputStream bout= new BufferedOutputStream(fout);
		OutputStreamWriter out = new OutputStreamWriter(bout);
		out.write(contenido);
		out.flush();
		out.close();
	}
	
	@SuppressWarnings("deprecation")
	private String getStringFromObject(Object object){
		String result = "";
		if (object != null){
			if(object instanceof Date){
				Date fecha = (Date) object;
				result += (fecha.getDate()<10?"0"+fecha.getDate():fecha.getDate())+""+((fecha.getMonth()+1)<10?"0"+(fecha.getMonth()+1):(fecha.getMonth()+1))+""+(fecha.getYear()+1900);
			}
			else{
				if(object.getClass().getName().equals("java.math.BigDecimal")){
					result = ((BigDecimal)(object)).toPlainString();
				}
				else if(object.getClass().getName().equals("java.lang.Integer")){
					result = ((Integer)(object)).toString();
				}
				else if(object.getClass().getName().equals("java.lang.Double")){
					result = Sistema.FORMATO_CUOTA.format(((Double)(object)));
				}
				else if(object.getClass().getName().equals("java.lang.Short")){
					result = object.toString();
				}
				else if(object.getClass().getName().equals("java.lang.Long")){
					result = Sistema.FORMATO_CUOTA.format(((Long)(object)));
				} else
					result = object.toString();
			}
		}
		return result;
	}
	
	public List<String> getNombreAtributos() {
		return nombreAtributos;
	}
	public void setNombreAtributos(List<String> nombreAtributos) {
		this.nombreAtributos = nombreAtributos;
	}
}