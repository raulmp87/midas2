package mx.com.afirme.midas.sistema.mensaje;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class MensajeAction extends MidasMappingDispatchAction {

	public void verMensaje(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		StringBuffer buffer = new StringBuffer();
		
		String tipoMensaje = (String)UtileriasWeb.obtenValorSessionScope(request, Sistema.TIPO_MENSAJE);
		String mensaje = (String)UtileriasWeb.obtenValorSessionScope(request, Sistema.MENSAJE);
		
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<id>");
		buffer.append(tipoMensaje);
		buffer.append("</id>");
		buffer.append("<description><![CDATA[");
		buffer.append(mensaje);
		buffer.append("]]></description>");	
		buffer.append("</item>");
		buffer.append("</response>");
				
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
