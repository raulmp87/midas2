/**
 * 
 */
package mx.com.afirme.midas.sistema.gestionPendientes;

import static mx.com.afirme.midas.sistema.gestionPendientes.Pendiente.TIPO_SINIESTRO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import com.opensymphony.oscache.base.NeedsRefreshException;
import com.opensymphony.oscache.general.GeneralCacheAdministrator;

/**
 * @author user
 *
 */
public class ControladorGestionadores {
	
	private Map<Integer,GestionadorPendientes> gestionadores;
	public static ControladorGestionadores INSTANCIA = null ;
	public static int REFRESH_TIME = 900;
	public static GeneralCacheAdministrator CACHE =  new GeneralCacheAdministrator();

	
	

	private void init(){
		GestionadorPendientesSiniestroDanos gestionadorPendientesSiniestroDanos = new GestionadorPendientesSiniestroDanos();
		gestionadores= new HashMap<Integer, GestionadorPendientes>();
		gestionadores.put(TIPO_SINIESTRO, gestionadorPendientesSiniestroDanos);
//		GestorPendientesDanos gestorPendientesDanios = new GestorPendientesDanos();
//		gestionadores.put(Pendiente.TIPO_DANOS, gestorPendientesDanios);
	}
	private ControladorGestionadores() {
		init();
	}

	public static ControladorGestionadores getInstance(){
		if (INSTANCIA == null){
			INSTANCIA = new ControladorGestionadores();
		}
		return INSTANCIA;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pendiente> obtenerPendientes(Usuario usuario, int tipo, boolean refresh){
		List<Pendiente> lista = new ArrayList<Pendiente>();
		if(tipo <= 1){
			GestionadorPendientes gestionadorPendientes = gestionadores.get(tipo);
			Integer idUsuario = usuario!=null?usuario.getId():0;
			String keyCache = idUsuario.toString()+"-"+tipo;
			if(refresh){
				lista = gestionadorPendientes.obtenerPendientes(usuario);
				CACHE.putInCache(idUsuario.toString()+"-"+tipo, lista);
			}else{
				try {
					lista = (List<Pendiente>) CACHE.getFromCache(keyCache, REFRESH_TIME);
				} catch (NeedsRefreshException nre) {
					try {
						lista = gestionadorPendientes.obtenerPendientes(usuario);
						CACHE.putInCache(keyCache, lista);
				    } catch (ExcepcionDeAccesoADatos e) {
				    	lista = (List<Pendiente>) nre.getCacheContent();
				    	CACHE.cancelUpdate(keyCache);
				    }
	
				}
			}
		}
		System.out.println("obtenerPendientes: "+lista.size());
		Collections.sort(lista);
		return lista;
	}
}
