/**
 * 
 */
package mx.com.afirme.midas.sistema.gestionPendientes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

/**
 * @author user
 *
 */
public class GestionadorPendientesSiniestroDanos implements	GestionadorPendientes {
	
	
	private static final String RECURSO_MENSAJES_PENDIENTES = "mx.com.afirme.midas.sistema.gestionPendientes.mensajes_pendientes";
	Map<BigDecimal, ReporteSiniestroDTO> reporteAbiertos;
	
	@SuppressWarnings("static-access")
	public List<Pendiente> obtenerPendientes(Usuario usuario) {
		List<Pendiente> listaResultado = new ArrayList<Pendiente>();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(usuario.getNombreUsuario());
		List<ReporteSiniestroDTO> listaReportes = new ArrayList<ReporteSiniestroDTO>();
		Pendiente pendiente;
		try {
			listaReportes = reporteSiniestroDN.listarReportesAbiertos();
			if(reporteAbiertos == null){
				reporteAbiertos = new HashMap<BigDecimal, ReporteSiniestroDTO>();
			}
			for(ReporteSiniestroDTO reporteSiniestro : listaReportes) {
				reporteAbiertos.put(reporteSiniestro.getIdToReporteSiniestro(), reporteSiniestro);
				ReporteEstatusDTO reporteEstatusDTO = reporteSiniestro.getReporteEstatus();
				
					BigDecimal 		idToReporteSiniestro  = reporteSiniestro.getIdToReporteSiniestro();
					Integer 		idUsuario 	  = usuario!=null?usuario.getId():0;
					Byte 			idEstatus 	  = reporteEstatusDTO.getIdTcReporteEstatus();
					BigDecimal 		idCoordinador = reporteSiniestro.getIdCoordinador();
					BigDecimal 		idCabinero 	  = reporteSiniestro.getIdCabinero(); 
					String 			noReporte	  	= reporteSiniestro.getNumeroReporte();
					String 			asegurado 		= reporteSiniestro.getNombreAsegurado()!=null?reporteSiniestro.getNombreAsegurado():"";
					if(reporteSiniestro.getPolizaDTO() != null){
						BigDecimal 		tipoNegocio   = reporteSiniestro.getPolizaDTO().getCotizacionDTO().getTipoNegocioDTO().getCodigoTipoNegocio();
						BigDecimal 		tipoMoneda	  = reporteSiniestro.getPolizaDTO().getCotizacionDTO().getIdMoneda();
	
						AjustadorDTO ajustadorDTO = reporteSiniestro.getAjustador();
						//Pendiente 1
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_ASIGNAR_AJUSTADOR)  &&  asignarPendienteAsignarAjustador(tipoNegocio, idUsuario, usuario,idCabinero, ajustadorDTO)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.1"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.1", reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.funcion.1"));
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);
						}
						//Pendiente 2
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_CONFIRMAR_AJUSTADOR) &&  asignarPendienteConfirmarAjustador(tipoNegocio, idUsuario, usuario,idCabinero)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.2"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.2",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.funcion.1"));
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);
						}
						//Pendiente 3
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_VALIDAR_INFORME_PRELIMINAR) &&  asignarPendienteEvaluarInformePreliminar(tipoNegocio, idUsuario, usuario,idCoordinador)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaAsignacionAjustador());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.3"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.3",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 4
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_TERMINAR_REPORTE_NO_INDEMNIZADO) &&  asignarPendienteTerminarReporteNoIndemnizado(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.5"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.5",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);		
						}
						//Pendiente 6
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_ASOCIAR_CARTA_RECHAZO) &&  asignarPendienteAsociarCartaRechazo(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.9"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.9",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);		
						}
						//Pendiente 7
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_APROBAR_CARTA_RECHAZO) &&  asignarPendienteAprobarCartaRechazo(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.11"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.11", reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);	
						}
						//Pendiente 8
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_IMPRIMIR_CARTA_RECHAZO) &&  asignarPendienteImprimirCartaRechazo(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.13"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.13", reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);	
						}
						//Pendiente 9
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_MODIFICAR_CARTA_RECHAZO) &&  asignarPendienteModificarCartaRechazo(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.14"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.14", reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);	
						}
						//Pendiente 10
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_ESTIMAR_RESERVA) &&  asignarPendienteEstimarReserva(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.4"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.4",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 11
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_AUTORIZAR_RESERVA) &&  asignarPendienteAutorizarReserva(idToReporteSiniestro,usuario,tipoNegocio,tipoMoneda)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.6"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.6",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 12
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_EVALUACION_INFORME_FINAL) &&  asignarPendienteEvaluacionInformeFinal(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.7"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.7",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 13
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_AGREGAR_INDEMNIZACION) &&  asignarPendienteAgregarIndemnizacion(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.8"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.8",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.funcion.8"));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 14
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_MODIFICAR_RESERVA) &&  asignarPendienteModificarReserva(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.20"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.20",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.funcion.20"));
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 15
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_AGREGAR_PAGO_PARCIAL_INDEMNIZACION) &&  asignarPendienteAgregarPagoParcialIndemnizacion(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.18"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.18",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.funcion.18"));
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
						//Pendiente 16
						if(idEstatus.equals(reporteSiniestro.PENDIENTE_AUTORIZAR_INDEMNIZACION) &&  asignarPendienteAutorizarIndemnizacion(usuario,tipoNegocio)){
							pendiente = new Pendiente();
							pendiente.setFecha(reporteSiniestro.getFechaHoraReporte());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.19"));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.19",reporteSiniestro.getIdToReporteSiniestro().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_SINIESTRO);
							pendiente.setResponsable(asegurado);
							pendiente.setReporte(noReporte);
							listaResultado.add(pendiente);						
						}
					}
			}
			pendientesAlternos(listaResultado,usuario);
			
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		return listaResultado;
	}
	//Pendiente 1
	private boolean  asignarPendienteAsignarAjustador(BigDecimal varTipoNegocio, Integer varIdUsuario, Usuario varUsuario,BigDecimal varIdCabinero, AjustadorDTO ajustadorDTO){
		boolean result = false;
		if((UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null)){
			if(ajustadorDTO != null && (UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS))){
				result = true;
			}else if(ajustadorDTO == null && (UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_CABINERO,Sistema.ROL_COORDINADOR_SINIESTROS))){
				result = true;
			}
					
			return result;
		}		
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;
	}
	//Pendiente 2
	private boolean  asignarPendienteConfirmarAjustador(BigDecimal varTipoNegocio, Integer varIdUsuario, Usuario varUsuario,BigDecimal varIdCabinero){
		if((UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO,Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;
	}
	//Pendiente 3
	private boolean  asignarPendienteEvaluarInformePreliminar(BigDecimal varTipoNegocio, Integer varIdUsuario, Usuario varUsuario,BigDecimal idCoordinador){
		if((UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && varIdUsuario.intValue() == idCoordinador.intValue()){
			return true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){//Sistema.ROL_GERENTE_FACULTATIVO
			return true;
		}		
		return false;
	}
	//Pendiente 4
	private boolean  asignarPendienteTerminarReporteNoIndemnizado(Usuario varUsuario,BigDecimal varTipoNegocio){
		if((UtileriasWeb.esTipoNegocio(varTipoNegocio, 99 )|| varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 5
	//TODO El 
	//Pendiente 6
	private boolean asignarPendienteAsociarCartaRechazo(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 7
	private boolean asignarPendienteAprobarCartaRechazo(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 8
	private boolean asignarPendienteImprimirCartaRechazo(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 9
	private boolean asignarPendienteModificarCartaRechazo(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 10
	private boolean asignarPendienteEstimarReserva(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 11
	private boolean  asignarPendienteAutorizarReserva(BigDecimal idToReporteSiniestro,Usuario varUsuario,BigDecimal varTipoNegocio,BigDecimal varTipoMoneda){
		ReservaDN  reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN  reservaDetalleDN = ReservaDetalleDN.getInstancia();
		TipoCambioDN tipoCambioDN = TipoCambioDN.getInstancia(varUsuario.getNombreUsuario());
		ReservaDTO reservaDTO =null;
		Double suma = new Double(0);
		try {
		
			reservaDTO = reservaDN.listarReservaPorAutorizar(idToReporteSiniestro);
			if(reservaDTO != null){
				suma = reservaDetalleDN.sumaReserva(reservaDTO.getIdtoreservaestimada());
			}else{
				 return false;
			}
			
			if(varTipoMoneda.intValue() == Sistema.MONEDA_DOLARES){
				Calendar fechaActual = Calendar.getInstance();		
				Integer mes = new Integer(fechaActual.get(Calendar.MONTH)+1);
				Integer anio = new Integer(fechaActual.get(Calendar.YEAR));
				
				Double tipoCambioMes = tipoCambioDN.obtieneTipoCambioPorMes(mes, anio, Short.parseShort(varTipoMoneda.toString()));
				
				//Si el tipo de cambio es null entonces no se asigna el pendiente
				if(tipoCambioMes==null){
					return false;
				}
				suma= suma * tipoCambioMes;
			}
		
			if(suma < 15000 && (UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_COORDINADOR_SINIESTROS)){
				 return true;
			}else if(suma < 15000 && UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
				return true;
			}else if(suma >= 15000 &&  (UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_GERENTE_SINIESTROS)){
				return true;
			}else if(suma >= 15000 &&  UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO)){
				return true;
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return false;
	}
	//Pendiente 12
	private boolean asignarPendienteEvaluacionInformeFinal(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 13
	private boolean asignarPendienteAgregarIndemnizacion(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			return true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			return true;
		}
		return false;
	}
	//Pendiente 14
	private boolean asignarPendienteModificarReserva(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS,Sistema.ROL_GERENTE_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO,Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 15
	private boolean asignarPendienteAgregarPagoParcialIndemnizacion(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			return true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 16
	private boolean asignarPendienteAutorizarIndemnizacion(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS,Sistema.ROL_GERENTE_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO,Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;	
	}
	//Pendiente 19
	private boolean asignarPendienteGenerarAutorizacionTecnica(Usuario varUsuario,BigDecimal varTipoNegocio){
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			return true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			return true;
		}
		return false;		
	}
	//Pendiente 20
	private boolean asignarPendienteAutorizarAutorizacionTecnica(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_GERENTE_SINIESTROS)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO)){
			return true;
		}
		return false;		
	}
	//Pendiente 21
	private boolean asignarPendienteGenerarOrdenDePago(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			return true;
		}
		return false;		
	}
	//Pendiente 22
	private boolean asignarPendienteAplicarIngreso(Usuario varUsuario,BigDecimal varTipoNegocio){
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			return true;
		}
		if( UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			return true;
		}
		return false;		
	}
	private void pendientesAlternos(List<Pendiente> listaResultado,Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionDN  indemnizacionDN = IndemnizacionDN.getInstancia();
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(usuario.getNombreUsuario());
		Pendiente pendiente;
		List<IndemnizacionDTO> listIndemnizacionDTO;
		List<IngresoSiniestroDTO> listaIngresosDTO;
		List<GastoSiniestroDTO> listaGastosDTO;
		String 	asegurado = "";
			listIndemnizacionDTO = indemnizacionDN.obtenerVariosPagos();
			listaIngresosDTO = ingresoSiniestroDN.listarIngresosPendientes();
			listaGastosDTO = gastoSiniestroDN.listarGastosPendientes();
			
			for(IndemnizacionDTO indemnizacionDTO:listIndemnizacionDTO){

				ReporteSiniestroDTO reporteSiniestroDTO = reporteAbiertos.get(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
				if(reporteSiniestroDTO == null){
					reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
					reporteAbiertos.put(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), reporteSiniestroDTO);
				}				

				AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
				AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionIndemnizacion(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), indemnizacionDTO.getIdToIndemnizacion());
				
				asegurado = reporteSiniestroDTO.getNombreAsegurado()!=null?reporteSiniestroDTO.getNombreAsegurado():"";
				BigDecimal tipoNegocio = reporteSiniestroDTO.getPolizaDTO().getCotizacionDTO().getTipoNegocioDTO().getCodigoTipoNegocio();

				if(indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.ABIERTO)== 0 &&  asignarPendienteGenerarAutorizacionTecnica(usuario,tipoNegocio)){
					pendiente = new Pendiente();
					
					pendiente.setFecha(indemnizacionDTO.getFechaDelPago());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.17.indemnizacion"));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.17.indemnizacion", indemnizacionDTO.getIdToIndemnizacion().toBigInteger().toString()));
					pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
					pendiente.setResponsable(asegurado);
					pendiente.setReporte(indemnizacionDTO.getReporteSiniestroDTO().getNumeroReporte());
					listaResultado.add(pendiente);	
				}else if(indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.POR_AUTORIZAR)== 0 &&  asignarPendienteAutorizarAutorizacionTecnica(usuario,tipoNegocio)){
					if(autorizacionTecnicaDTO != null){
						pendiente = new Pendiente();
						pendiente.setFecha(indemnizacionDTO.getFechaDelPago());
						pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.15.indemnizacion"));
						pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.15", autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toBigInteger().toString()));
						pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
						pendiente.setResponsable(asegurado);
						pendiente.setReporte(indemnizacionDTO.getReporteSiniestroDTO().getNumeroReporte());
						listaResultado.add(pendiente);	
					}
				}else if(indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.AUTORIZADO) == 0 &&  asignarPendienteGenerarOrdenDePago(usuario,tipoNegocio)){
					if(autorizacionTecnicaDTO != null){
						pendiente = new Pendiente();
						pendiente.setFecha(indemnizacionDTO.getFechaDelPago());
						pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.21.indemnizacion"));
						pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.21", indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(),autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toBigInteger().toString()));
						pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
						pendiente.setResponsable(asegurado);
						pendiente.setReporte(indemnizacionDTO.getReporteSiniestroDTO().getNumeroReporte());
						listaResultado.add(pendiente);
					}
				}
			}
			for(IngresoSiniestroDTO ingresoSiniestroDTO:listaIngresosDTO){

				ReporteSiniestroDTO reporteSiniestroDTO = reporteAbiertos.get(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
				if(reporteSiniestroDTO == null){
					reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
					reporteAbiertos.put(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), reporteSiniestroDTO);
				}				
				
				AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
				AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionIngreso(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), ingresoSiniestroDTO.getIdToIngresoSiniestro());
				
				asegurado = reporteSiniestroDTO.getNombreAsegurado()!=null?reporteSiniestroDTO.getNombreAsegurado():"";
				BigDecimal tipoNegocio = reporteSiniestroDTO.getPolizaDTO().getCotizacionDTO().getTipoNegocioDTO().getCodigoTipoNegocio();
				
				if(ingresoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.ABIERTO) == 0 &&  asignarPendienteGenerarAutorizacionTecnica(usuario,tipoNegocio)){
					pendiente = new Pendiente();
					pendiente.setFecha(ingresoSiniestroDTO.getFechaCreacion());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.17.ingreso"));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.17.ingreso",ingresoSiniestroDTO.getIdToIngresoSiniestro().toBigInteger().toString()));
					pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
					pendiente.setResponsable(asegurado);
					pendiente.setReporte(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
					listaResultado.add(pendiente);	
				}else if(ingresoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.POR_AUTORIZAR)==0 &&  asignarPendienteAutorizarAutorizacionTecnica(usuario,tipoNegocio)){
					if(autorizacionTecnicaDTO != null){
						pendiente = new Pendiente();
						pendiente.setFecha(ingresoSiniestroDTO.getFechaCreacion());
						pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.15.ingreso"));
						pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.15", autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toBigInteger().toString()));
						pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
						pendiente.setResponsable(asegurado);
						pendiente.setReporte(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
						listaResultado.add(pendiente);	
					}
				}else if(ingresoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.AUTORIZADO)==0 &&  asignarPendienteAplicarIngreso(usuario,tipoNegocio)){
						pendiente = new Pendiente();
						pendiente.setFecha(ingresoSiniestroDTO.getFechaCreacion());
						pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.22"));
						pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.22", ingresoSiniestroDTO.getIdToIngresoSiniestro(),ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro().toBigInteger().toString()));
						pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
						pendiente.setResponsable(asegurado);
						pendiente.setReporte(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
						listaResultado.add(pendiente);	
				}
				
			}
			for(GastoSiniestroDTO gastoSiniestroDTO:listaGastosDTO){

				ReporteSiniestroDTO reporteSiniestroDTO = reporteAbiertos.get(gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
				if(reporteSiniestroDTO == null){
					reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
					reporteAbiertos.put(gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), reporteSiniestroDTO);
				}		
				
				AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
				AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionGasto(gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), gastoSiniestroDTO.getIdToGastoSiniestro());
				
				asegurado = reporteSiniestroDTO.getNombreAsegurado()!=null?reporteSiniestroDTO.getNombreAsegurado():"";
				BigDecimal tipoNegocio = reporteSiniestroDTO.getPolizaDTO().getCotizacionDTO().getTipoNegocioDTO().getCodigoTipoNegocio();
						
				if(gastoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.ABIERTO)==0 &&  asignarPendienteGenerarAutorizacionTecnica(usuario,tipoNegocio)){
					pendiente = new Pendiente();
					pendiente.setFecha(gastoSiniestroDTO.getFechaCreacion());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.17.gasto"));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.17.gasto", gastoSiniestroDTO.getIdToGastoSiniestro().toBigInteger().toString()));
					pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
					pendiente.setResponsable(asegurado);
					pendiente.setReporte(gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
					listaResultado.add(pendiente);	
				}else if(gastoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.POR_AUTORIZAR)==0 &&  asignarPendienteAutorizarAutorizacionTecnica(usuario,tipoNegocio)){
					if(autorizacionTecnicaDTO != null){
						pendiente = new Pendiente();
						pendiente.setFecha(gastoSiniestroDTO.getFechaCreacion());
						pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.15.gasto"));
						pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.15", autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toBigInteger().toString()));
						pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
						pendiente.setResponsable(asegurado);
						pendiente.setReporte(gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
						listaResultado.add(pendiente);	
					}
				}else if(gastoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().compareTo(EstatusFinanzasDTO.AUTORIZADO)==0 &&  asignarPendienteGenerarOrdenDePago(usuario,tipoNegocio)){
					if(autorizacionTecnicaDTO != null){
						pendiente = new Pendiente();
						pendiente.setFecha(gastoSiniestroDTO.getFechaCreacion());
						pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.mensaje.21.gasto"));
						pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "siniestro.url.21", gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(),autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toBigInteger().toString()));
						pendiente.setTipo(Pendiente.TIPO_SINIESTRO);
						pendiente.setResponsable(asegurado);
						pendiente.setReporte(gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
						listaResultado.add(pendiente);	
					}
				}
			}
	}
	
}
