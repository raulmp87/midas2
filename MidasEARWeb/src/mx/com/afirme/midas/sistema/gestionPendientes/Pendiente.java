/**
 * 
 */
package mx.com.afirme.midas.sistema.gestionPendientes;

import java.io.Serializable;
import java.util.Date;

/**
 * @author user
 *
 */
@SuppressWarnings("unchecked")
public class Pendiente implements Serializable, Comparable {
	
	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private String url;
	private Date fecha;
	private int tipo;
	private String funcionJavaScript;
	private String responsable;
//	private int idMensaje;
	private String reporte;
	private String poliza;
	
	
	public static final int TIPO_SINIESTRO = 1;
	public static final int TIPO_DANOS = 2;
	public static final int TIPO_REASEGURO = 3;
	
	
	
	public Pendiente() {
	}



	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}



	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}



	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}



	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}



	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}



	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	/**
	 * @return the tipo
	 */
	public int getTipo() {
		return tipo;
	}



	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}



	public int compareTo(Object o) {
		Pendiente temporalPendiente = (Pendiente) o;
		return fecha.compareTo(temporalPendiente.getFecha());

//		return 0;
	}

	/**
	 * @return Devuelve la cadena de funciones JavaScript que se ejecutar�n despu�s de cargar la url del pendiente
	 */
	public String getFuncionJavaScript() {
		return funcionJavaScript;
	}

	/**
	 * Establece la cadena de funciones JavaScript que se ejecutar�n despu�s de cargar la url del pendiente
	 * @param funcionJavaScript
	 */
	public void setFuncionJavaScript(String funcionJavaScript) {
		this.funcionJavaScript = funcionJavaScript;
	}

	/**
	 * Atributo para visualizar la persona que creo o genero el pendiente
	 * @return the responsable
	 */
	public String getResponsable() {
		return responsable;
	}

	/**
	 * @param responsable the responsable to set
	 */
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	/**
	 * @return the reporte
	 */
	public String getReporte() {
		return reporte;
	}

	/**
	 * @param reporte the reporte to set
	 */
	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

	/**
	 * @return the poliza
	 */
	public String getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}


	

}
