package mx.com.afirme.midas.sistema.gestionPendientes;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class PendientesAction extends MidasMappingDispatchAction {
		
	public void listarPendientes(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException, IOException {
		Object tipoLista = request.getParameter("tipoLista");
		Object objRefresh = request.getSession().getAttribute("banderaRefresh")!=null?request.getSession().getAttribute("banderaRefresh"):"true";
		boolean banderaRefresh = Boolean.valueOf(objRefresh.toString());
		int tipo_lista = Integer.parseInt(tipoLista.toString()); 
		
		List<Pendiente> mensajes = new ArrayList<Pendiente>();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		ControladorPendientesUsuario controladorPendientesUsuario = new ControladorPendientesUsuario();
		
		mensajes = controladorPendientesUsuario.obtenerPendientes(usuario, tipo_lista,banderaRefresh);
		MidasJsonBase json = new MidasJsonBase();
		if(mensajes != null && mensajes.size() > 0) {
			int id = 0;
			for(Pendiente mensaje : mensajes) {
					MidasJsonRow row = new MidasJsonRow();
					row.setId(String.valueOf(id));
					StringBuffer dato = new StringBuffer();
					dato.append(mensaje.getMensaje());
					dato.append("^javascript:mensajesPendientesWindow.close();sendRequest(null,&#39;");
					dato.append(mensaje.getUrl());
					dato.append("&#39;,&#39;contenido&#39;,");
					if(mensaje.getFuncionJavaScript()!=null){
						dato.append("&#39;");
						dato.append(mensaje.getFuncionJavaScript());
						dato.append("&#39;");
					}else{
						dato.append("null");
					}
					dato.append(");^_self");
					if(mensaje.getResponsable() != null){
						row.setDatos(UtileriasWeb.getFechaString(mensaje.getFecha()),dato.toString(),mensaje.getResponsable(),mensaje.getReporte(),mensaje.getPoliza() );
					}else{
						row.setDatos(UtileriasWeb.getFechaString(mensaje.getFecha()),dato.toString(),mensaje.getReporte(),mensaje.getPoliza() );
					}
					json.addRow(row);
					id++;
			}
		}
		if (tipoLista.toString().equals("1")){
			request.getSession().setAttribute("banderaRefresh", "false");
		}
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
	}

}
