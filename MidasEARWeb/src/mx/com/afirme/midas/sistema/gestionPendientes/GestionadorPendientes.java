/**
 * 
 */
package mx.com.afirme.midas.sistema.gestionPendientes;

import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;

/**
 * @author user
 *
 */
public interface GestionadorPendientes {
	public List<Pendiente> obtenerPendientes(Usuario usuario);
}
