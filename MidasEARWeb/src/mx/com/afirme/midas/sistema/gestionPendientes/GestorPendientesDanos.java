package mx.com.afirme.midas.sistema.gestionPendientes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import org.apache.commons.lang.StringUtils;

public class GestorPendientesDanos implements GestionadorPendientes{

	private static final Short ESTATUS_REQUIERE_AUTORIZACION = Sistema.AUTORIZACION_REQUERIDA;
	private static final Short ESTATUS_INSPECCION_RECHAZADA = Sistema.RECHAZADA;
	private static final Short ESTATUS_OMISION_INSPECCION_AUTORIZADA = Sistema.ESTATUS_INSPECCION_INCISO_ACEPTADO;
	private static final Short ESTATUS_SOL_OT_EN_PROCESO = Sistema.ESTATUS_ODT_ENPROCESO;
	private static final Short ESTATUS_SOLICITUD_TERMINADA = Sistema.ESTATUS_SOL_TERMINADA;
	private static final Short ESTATUS_OT_ASIGNADA = Sistema.ESTATUS_ODT_ASIGNADA;
	private static final Short ESTATUS_OT_TERMINADA = Sistema.ESTATUS_ODT_TERMINADA;
	private static final Short ESTATUS_COT_EN_PROCESO = Sistema.ESTATUS_COT_ENPROCESO;
	private static final Short ESTATUS_COT_LISTA_EMISION = Sistema.ESTATUS_COT_LISTA_EMITIR;
	private static final Short ESTATUS_COT_ASIGNADA = Sistema.ESTATUS_COT_ASIGNADA;
	private static final Object ESTATUS_COT_ASIGNADA_EMISION = Sistema.ESTATUS_COT_ASIGNADA_EMISION;
	private static final String RECURSO_MENSAJES_PENDIENTES = "mx.com.afirme.midas.sistema.gestionPendientes.mensajes_pendientes";
	
	
	
	public List<Pendiente> obtenerPendientes(Usuario usuario) {
		List<Pendiente> listaPendientes = new ArrayList<Pendiente>();
		for (Rol rol : usuario.getRoles()) {
			if (rol.getDescripcion().equalsIgnoreCase(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
				try {
					List<CotizacionDTO> listaCotizacion = CotizacionDN.getInstancia(usuario.getNombreUsuario()).buscarPorPropiedad("claveEstatus", new Short("10"));
					for(CotizacionDTO cotizacion : listaCotizacion){
						//Verificar las comisiones de la cotizacion
						verificarPendientesComisionCotizacion(cotizacion,listaPendientes,true);
						
						//Verificar los Textos adicionales de la cotizacion
						verificarPendientesTexAdicionalCotizacion(cotizacion,listaPendientes,true);
						
						//Verificar los incisos de la cotizacion
						verificarPendientesIncisoCotizacion(cotizacion,listaPendientes,true,false,true);
						
						//Verificar las coberturas y riesgos de la cotizacion
						verificarPendientesCoberturaRiesgoCotizacion(cotizacion,listaPendientes,true);
						
						if(cotizacion.getClaveAutoEmisionDocOperIlicitas()!= null && cotizacion.getClaveAutoEmisionDocOperIlicitas().shortValue() == 1) {
							verificarCotizacionPendienteOmisionDoc(cotizacion, listaPendientes, false);
						}
					}
				} catch (SystemException e) {	e.printStackTrace();}
			}
			if (rol.getDescripcion().equalsIgnoreCase(
					Sistema.ROL_SUSCRIPTOR_COT)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
						|| rol.getDescripcion().equalsIgnoreCase(
								Sistema.ROL_SUSCRIPTOR_EXT)) {
				try {
					List<CotizacionDTO> listaCotizacion = CotizacionDN.getInstancia(usuario.getNombreUsuario()).buscarPorPropiedad("claveEstatus", new Short("10"));
					for(CotizacionDTO cotizacion : listaCotizacion){
						//Verificar los incisos de la cotizacion
						verificarPendientesIncisoCotizacion(cotizacion,listaPendientes,true,true,false);
					}
				} catch (SystemException e) {	e.printStackTrace();}
			} else if (rol.getDescripcion()
					.equalsIgnoreCase(Sistema.ROL_AGENTE)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_SUSCRIPTOR_EXT)) {
				try {
					List<SolicitudDTO> listaSolicitudes = SolicitudDN.getInstancia().listarPorEstatus(ESTATUS_SOL_OT_EN_PROCESO, usuario);
					for(SolicitudDTO solicitud : listaSolicitudes){
						//Verificar el estatus 0 - en proceso de cada solicitud
						verificarSolicitudEnProceso(solicitud,listaPendientes,true);
					}
				} catch (SystemException e) {	e.printStackTrace();}
			} else if (rol.getDescripcion().equalsIgnoreCase(
					Sistema.ROL_ASIGNADOR_SOL)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_SUSCRIPTOR_EXT)) {
				try {
					List<SolicitudDTO> listaSolicitudes = SolicitudDN.getInstancia().listarTodos();
					for(SolicitudDTO solicitud : listaSolicitudes){
						//Verificar si la solicitud tiene estatus de terminada
						verificarSolicitudTerminada(solicitud,listaPendientes,true);
					}
				} catch (SystemException e) {	e.printStackTrace();}
			}else if (rol.getDescripcion().equalsIgnoreCase(
					Sistema.ROL_SUSCRIPTOR_OT)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_SUSCRIPTOR_EXT)) {
				CotizacionDTO cotizaciontmp = new CotizacionDTO();
				cotizaciontmp.setCodigoUsuarioOrdenTrabajo(String.format("%1$-8s", usuario.getNombreUsuario()));
				List<CotizacionDTO> listaCotizacion;
				try {
					listaCotizacion = CotizacionDN.getInstancia(usuario.getNombreUsuario()).listarCotizacionFiltrado(cotizaciontmp,usuario);
					for(CotizacionDTO cotizacion : listaCotizacion){
						//Verificar el estatus "en proceso" de la cotizacion y generar el pendiente
						verificarOrdenTrabajoEnProceso(cotizacion,listaPendientes,true);
						
						//Verificar el estatus "Asignada" de la cotizacion y generar el pendiente
						verificarOrdenTrabajoAsignada(cotizacion,listaPendientes,true);
					}
				} catch (ExcepcionDeAccesoADatos e) {	e.printStackTrace();
				} catch (SystemException e) {			e.printStackTrace();}
			} else if (rol.getDescripcion().equalsIgnoreCase(
					Sistema.ROL_ASIGNADOR_OT)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_SUSCRIPTOR_EXT)) {
				try {
					List<CotizacionDTO> listaCotizacion = CotizacionDN.getInstancia(usuario.getNombreUsuario()).buscarPorPropiedad("claveEstatus", ESTATUS_OT_TERMINADA);
					for(CotizacionDTO cotizacion : listaCotizacion){
						//Verificar si la cotizacion tiene estatus de terminada
						verificarOrdenTrabajoTerminada(cotizacion,listaPendientes,true);

						//Verificar el estatus "lista para emitir" de la cotizacion y generar el pendiente
						verificarCotizacionPendienteAsignacion(cotizacion,listaPendientes,true);
					}
				} catch (SystemException e) {	e.printStackTrace();}
			} else if (rol.getDescripcion().equalsIgnoreCase(
					Sistema.ROL_SUSCRIPTOR_COT)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_SUSCRIPTOR_EXT)) {
				CotizacionDTO cotizaciontmp = new CotizacionDTO();
				cotizaciontmp.setCodigoUsuarioOrdenTrabajo(String.format("%1$-8s", usuario.getId().toString()));
				cotizaciontmp.setClaveEstatus(ESTATUS_COT_EN_PROCESO);
				List<CotizacionDTO> listaCotizacion;
				try {
					listaCotizacion = CotizacionDN.getInstancia(usuario.getNombreUsuario()).listarCotizacionFiltrado(cotizaciontmp,usuario);
					for(CotizacionDTO cotizacion : listaCotizacion){
						//Verificar el estatus "en proceso" de la cotizacion y generar el pendiente
						verificarCotizacionEnProceso(cotizacion,listaPendientes,true);
						verificarCotizacionAsignada(cotizacion,listaPendientes,true);
					}
				} catch (ExcepcionDeAccesoADatos e) {	e.printStackTrace();
				} catch (SystemException e) {			e.printStackTrace();}
			} else if (rol.getDescripcion()
					.equalsIgnoreCase(Sistema.ROL_EMISOR)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_AGENTE_EXTERNO)
					|| rol.getDescripcion().equalsIgnoreCase(
							Sistema.ROL_SUSCRIPTOR_EXT)) {
				try {
					List<CotizacionDTO> listaCotizacion = CotizacionDN.getInstancia(usuario.getNombreUsuario()).buscarPorPropiedad("claveEstatus", ESTATUS_COT_LISTA_EMISION);
					for(CotizacionDTO cotizacion : listaCotizacion){
						if(usuario.getId().toString().equals(cotizacion.getCodigoUsuarioCotizacion())) {
							verificarCotizacionListaEmision(cotizacion, listaPendientes, false);
						}
					}
				} catch (ExcepcionDeAccesoADatos e) {	e.printStackTrace();
				} catch (SystemException e) {			e.printStackTrace();}
			}
		}
		return listaPendientes;
	}
	
	public List<Pendiente> obtenerPendientesPorCotizacion(CotizacionDTO cotizacion) {
		if(cotizacion != null){
			List<Pendiente> listaPendientes = new ArrayList<Pendiente>();
			if (cotizacion.getClaveEstatus().intValue() == 10){
				//Verificar cotizacion en proceso
				/**
				 * 01/12/09 Jorge A. Cano
				 * Se comento por que impedia que mostrara el icono de liberar cotizacion
				 * verificarCotizacionEnProceso(cotizacion, listaPendientes, false);
				 */
				//Verificar las comisiones de la cotizacion
				verificarPendientesComisionCotizacion(cotizacion,listaPendientes,false);
				
				//Verificar los Textos adicionales de la cotizacion
				verificarPendientesTexAdicionalCotizacion(cotizacion,listaPendientes,false);
				
				//Verificar los incisosde la cotizacion
				verificarPendientesIncisoCotizacion(cotizacion,listaPendientes,false,true,true);
				
				//Verificar las coberturas y riesgos de la cotizacion
				verificarPendientesCoberturaRiesgoCotizacion(cotizacion,listaPendientes,false);
				
				//Verificar las autorizacion pendientes por otras validaciones
				verificarPendientesCotizacionOtrasAutorizacion(cotizacion,listaPendientes,false);
			}
			//verificar si la cotizacion esta lista para emitir
			verificarCotizacionPendienteAsignacion(cotizacion, listaPendientes, false);
			
			verificarCotizacionListaEmision(cotizacion, listaPendientes, false);
			
			verificarCotizacionPendienteOmisionDoc(cotizacion, listaPendientes, false);
			
			//verificar 0:OT En Proceso
			verificarOrdenTrabajoEnProceso(cotizacion, listaPendientes, false);

			//verificar 1:OT Asignada
			verificarOrdenTrabajoAsignada(cotizacion, listaPendientes, false);
			
			//verificar 2:OT Terminada
			verificarOrdenTrabajoTerminada(cotizacion, listaPendientes, false);
			
			

			return listaPendientes;
		}
		else	return null;
	}
	
	public Boolean cotizacionListaParaLiberar(CotizacionDTO cotizacion,List<String> listaErrores,Boolean verificarMaximoErrores) {
		if(cotizacion != null){
			Boolean cotizacionListaLiberar = Boolean.TRUE;
//			if (cotizacion.getClaveEstatus().intValue() == 10){
			if (listaErrores == null)
				listaErrores = new ArrayList<String>();
			//Verificar las comisiones de la cotizacion
			if (!verificarPendientesComisionCotizacion(cotizacion,listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}
			
			//Verificar los Textos adicionales de la cotizacion
			if (!verificarPendientesTexAdicionalCotizacion(cotizacion,listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}
			
			//Verificar los incisos de la cotizacion
			if (!verificarPendientesIncisoCotizacion(cotizacion,true,true,listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}
			
			//Verificar las coberturas y riesgos de la cotizacion
			if (!verificarPendientesCoberturaRiesgoCotizacion(cotizacion,listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}
			
			//Verificar si reaseguro permite la liberaci�n de la p�liza
			if (!verificarAutorizacionLiberarReaseguroFacultativo(cotizacion, listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}

			//Verificar los gastos de expedicion
			if (!verificarPendientesRecargoPagoFraccionado(cotizacion, listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}
			
			//Verificar los derechos
			if (!verificarPendientesDerechos(cotizacion, listaErrores)){
				if (verificarMaximoErrores)
					cotizacionListaLiberar = Boolean.FALSE;
				else
					return Boolean.FALSE;
			}
			
			return cotizacionListaLiberar;
		}
		else	return null;
	}

	private Boolean verificarAutorizacionLiberarReaseguroFacultativo(CotizacionDTO cotizacionDTO,List<String> listaErrores){
		Boolean result = Boolean.TRUE;
//		SoporteReaseguro soporteReaseguro = null;
		/**
		 * JACC
		 * 19/04/2010 Se agrega validacion:  2)  No pedir permiso a reaseguro para liberar ni emitir
		 * 20/08/2010 JLAB. Se agrega par�metro "tipoEndoso" del lado de reaseguro
		 */
		SoporteReaseguroCotizacionDN soporteReaseguroCot=null;
		try {
//			soporteReaseguro = new SoporteReaseguro(cotizacionDTO.getIdToCotizacion());
			soporteReaseguroCot = new SoporteReaseguroCotizacionDN(cotizacionDTO.getIdToCotizacion(),false);
		} catch (SystemException e) {
		}
		if (soporteReaseguroCot != null){
			try{
				List<String> motivosError = new ArrayList<String>();
				result = soporteReaseguroCot.autorizadoLiberacion(cotizacionDTO,motivosError);
				if(!result){
					if(motivosError != null && !motivosError.isEmpty()){
						for(String motivoError : motivosError){
							if(motivoError != null)
								listaErrores.add(motivoError);
						}
					}
					else
						listaErrores.add(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "cotizacion.liberar.error.reaseguro"));
				}
			}catch(Exception e){}
		}
		return result;
	}
	
	private void verificarCotizacionPendienteOmisionDoc(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(GestorPendientesDanos.ESTATUS_COT_LISTA_EMISION)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.20":"danios.mensaje.20.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.20",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(Pendiente.TIPO_DANOS);
			pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.20",cotizacion.getIdToCotizacion()));
			listaPendientes.add(pendiente);
		}
	}

	private void verificarCotizacionPendienteAsignacion(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(GestorPendientesDanos.ESTATUS_COT_LISTA_EMISION)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.18":"danios.mensaje.18.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.18",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(Pendiente.TIPO_DANOS);
			//pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.18",cotizacion.getIdToCotizacion()));
			listaPendientes.add(pendiente);
		}
	}

	private void verificarCotizacionListaEmision(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(GestorPendientesDanos.ESTATUS_COT_ASIGNADA_EMISION)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.19":"danios.mensaje.19.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.19",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(Pendiente.TIPO_DANOS);
			pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.19",cotizacion.getIdToCotizacion()));
			listaPendientes.add(pendiente);
		}
	}

	private void verificarCotizacionAsignada(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(GestorPendientesDanos.ESTATUS_COT_ASIGNADA)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.21":"danios.mensaje.21.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.21",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(Pendiente.TIPO_DANOS);
			listaPendientes.add(pendiente);
		}
	}

	@SuppressWarnings("static-access")
	private void verificarOrdenTrabajoTerminada(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(this.ESTATUS_OT_TERMINADA)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.16":"danios.mensaje.16.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.16",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(pendiente.TIPO_DANOS);
			pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.16",cotizacion.getIdToCotizacion()));
			listaPendientes.add(pendiente);
		}
	}

	@SuppressWarnings("static-access")
	private void verificarOrdenTrabajoAsignada(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(this.ESTATUS_OT_ASIGNADA)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.15":"danios.mensaje.15.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.15",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(pendiente.TIPO_DANOS);
			listaPendientes.add(pendiente);
		}
	}
	
	@SuppressWarnings("static-access")
	private void verificarCotizacionEnProceso(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(this.ESTATUS_COT_EN_PROCESO)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.17":"danios.mensaje.17.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.17",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(pendiente.TIPO_DANOS);
			pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.17",cotizacion.getIdToCotizacion()));
			listaPendientes.add(pendiente);
		}
	}
	
	@SuppressWarnings("static-access")
	private void verificarOrdenTrabajoEnProceso(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (cotizacion.getClaveEstatus().equals(this.ESTATUS_SOL_OT_EN_PROCESO)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(cotizacion.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.14":"danios.mensaje.14.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.14",cotizacion.getIdToCotizacion().toBigInteger().toString()));
			pendiente.setTipo(pendiente.TIPO_DANOS);
			pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.14",cotizacion.getIdToCotizacion()));
			listaPendientes.add(pendiente);
		}
	}
	
	@SuppressWarnings("static-access")
	private void verificarSolicitudTerminada(SolicitudDTO solicitud,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (solicitud.getClaveEstatus().equals(this.ESTATUS_SOLICITUD_TERMINADA)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(solicitud.getFechaModificacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.13":"danios.mensaje.13.sinclave"),this.formateaIdCotizacion(solicitud.getIdToSolicitud())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.13",solicitud.getIdToSolicitud().toBigInteger().toString()));
			pendiente.setTipo(pendiente.TIPO_DANOS);
			listaPendientes.add(pendiente);
		}
	}
	
	@SuppressWarnings("static-access")
	private void verificarSolicitudEnProceso(SolicitudDTO solicitud,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		//Se cambio el == por equals al ser comparacion de objetos
		if (solicitud.getClaveEstatus().equals(this.ESTATUS_SOL_OT_EN_PROCESO)){
			Pendiente pendiente = new Pendiente();
			pendiente.setFecha(solicitud.getFechaCreacion());
			pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.12":"danios.mensaje.12.sinclave"),this.formateaIdCotizacion(solicitud.getIdToSolicitud())));
			pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.12",solicitud.getIdToSolicitud().toBigInteger().toString()));
			pendiente.setTipo(pendiente.TIPO_DANOS);
			pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.12"));
			listaPendientes.add(pendiente);
		}
	}

	private void verificarPendientesCoberturaRiesgoCotizacion(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		try {
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(cotizacion.getIdToCotizacion());
			coberturaCotizacionDTO.setId(id);
			coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
			coberturaCotizacionDTO.setClaveAutCoaseguro(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION);
			List <CoberturaCotizacionDTO> listaCoberturaCotizacion = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotizacionDTO);
			for(CoberturaCotizacionDTO coberturaCotizacion : listaCoberturaCotizacion){
				Pendiente pendiente = new Pendiente();
				pendiente.setFecha(cotizacion.getFechaModificacion());
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.5":"danios.mensaje.5.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),coberturaCotizacion.getId().getNumeroInciso(),coberturaCotizacion.getCoberturaSeccionDTO().getSeccionDTO().getDescripcion()));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.5",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(Pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.5",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}
			coberturaCotizacionDTO.setClaveAutCoaseguro(null);
			coberturaCotizacionDTO.setClaveAutDeducible(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION);
			listaCoberturaCotizacion = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotizacionDTO);
			for(CoberturaCotizacionDTO coberturaCotizacion : listaCoberturaCotizacion){
				Pendiente pendiente = new Pendiente();
				pendiente.setFecha(cotizacion.getFechaModificacion());
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.6":"danios.mensaje.6.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),coberturaCotizacion.getId().getNumeroInciso(),coberturaCotizacion.getCoberturaSeccionDTO().getSeccionDTO().getDescripcion()));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.6",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(Pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.6",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}

			List<RiesgoCotizacionDTO> listaRiesgoCotizacion = RiesgoCotizacionDN.getInstancia().listarRiesgosConCoasegurosDeduciblesPendientes(cotizacion.getIdToCotizacion());
			for(RiesgoCotizacionDTO riesgoCot : listaRiesgoCotizacion){
				if(riesgoCot.getClaveAutCoaseguro().equals(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION)){
					Pendiente pendiente = new Pendiente();
					pendiente.setFecha(cotizacion.getFechaModificacion());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.7":"danios.mensaje.7.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),riesgoCot.getId().getNumeroInciso(),riesgoCot.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getDescripcion(),riesgoCot.getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion()));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.7",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					pendiente.setTipo(Pendiente.TIPO_DANOS);
					pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.7",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					listaPendientes.add(pendiente);
				}
				if(riesgoCot.getClaveAutDeducible().equals(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION)){
					Pendiente pendiente = new Pendiente();
					pendiente.setFecha(cotizacion.getFechaModificacion());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.8":"danios.mensaje.8.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),riesgoCot.getId().getNumeroInciso(),riesgoCot.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getDescripcion(),riesgoCot.getCoberturaCotizacionDTO().getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion()));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.8",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					pendiente.setTipo(Pendiente.TIPO_DANOS);
					pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.8",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					listaPendientes.add(pendiente);
				}
			}

			List<DescuentoRiesgoCotizacionDTO> listaDescuentos = DescuentoRiesgoCotizacionDN.getInstancia().listarDescuentosPendientesAutorizacion(cotizacion.getIdToCotizacion());
			for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
				Pendiente pendiente = new Pendiente();
				pendiente.setFecha(cotizacion.getFechaModificacion());
				
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCotizacion(descuento.getId().getIdToCotizacion());
				riesgoCotizacionId.setNumeroInciso(descuento.getId().getNumeroInciso());
				riesgoCotizacionId.setIdToSeccion(descuento.getId().getIdToSeccion());
				riesgoCotizacionId.setIdToCobertura(descuento.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToRiesgo(descuento.getId().getIdToRiesgo());
				descuento.setRiesgoCotizacionDTO(RiesgoCotizacionDN.getInstancia().getPorId(riesgoCotizacionId));
				
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.9":"danios.mensaje.9.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),descuento.getId().getNumeroInciso(),descuento.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getSeccionDTO().getDescripcion(),descuento.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion()));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.9",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(Pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.9",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}
			List<AumentoRiesgoCotizacionDTO> listaAumentos = AumentoRiesgoCotizacionDN.getInstancia().listarAumentosPendientesAutorizacion(cotizacion.getIdToCotizacion());
			for(AumentoRiesgoCotizacionDTO aumento : listaAumentos){
				Pendiente pendiente = new Pendiente();
				pendiente.setFecha(cotizacion.getFechaModificacion());
				
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCotizacion(aumento.getId().getIdToCotizacion());
				riesgoCotizacionId.setNumeroInciso(aumento.getId().getNumeroInciso());
				riesgoCotizacionId.setIdToSeccion(aumento.getId().getIdToSeccion());
				riesgoCotizacionId.setIdToCobertura(aumento.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToRiesgo(aumento.getId().getIdToRiesgo());
				aumento.setRiesgoCotizacionDTO(RiesgoCotizacionDN.getInstancia().getPorId(riesgoCotizacionId));
				
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.10":"danios.mensaje.10.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),aumento.getId().getNumeroInciso(),aumento.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getSeccionDTO().getDescripcion(),aumento.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion()));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.10",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(Pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.10",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}
			List<RecargoRiesgoCotizacionDTO> listaRecargos = RecargoRiesgoCotizacionDN.getInstancia().listarRecargosPendientesAutorizacion(cotizacion.getIdToCotizacion());
			for(RecargoRiesgoCotizacionDTO recargo : listaRecargos){
				Pendiente pendiente = new Pendiente();
				pendiente.setFecha(cotizacion.getFechaModificacion());
				
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCotizacion(recargo.getId().getIdToCotizacion());
				riesgoCotizacionId.setNumeroInciso(recargo.getId().getNumeroInciso());
				riesgoCotizacionId.setIdToSeccion(recargo.getId().getIdToSeccion());
				riesgoCotizacionId.setIdToCobertura(recargo.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToRiesgo(recargo.getId().getIdToRiesgo());
				recargo.setRiesgoCotizacionDTO(RiesgoCotizacionDN.getInstancia().getPorId(riesgoCotizacionId));
				
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.11":"danios.mensaje.11.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),recargo.getId().getNumeroInciso(),recargo.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getSeccionDTO().getDescripcion(),recargo.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion()));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.11",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(Pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.11",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	private Boolean verificarPendientesCoberturaRiesgoCotizacion(CotizacionDTO cotizacion,List<String> listaErrores){
		try {
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			CoberturaCotizacionId id = new CoberturaCotizacionId();
			id.setIdToCotizacion(cotizacion.getIdToCotizacion());
			coberturaCotizacionDTO.setId(id);
			coberturaCotizacionDTO.setClaveContrato(Sistema.CONTRATADO);
			coberturaCotizacionDTO.setClaveAutCoaseguro(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION);
			List <CoberturaCotizacionDTO> listaCoberturaCotizacion = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotizacionDTO);
			for(CoberturaCotizacionDTO coberturaCotizacion : listaCoberturaCotizacion){
				if (coberturaCotizacion.getSeccionCotizacionDTO().getClaveContrato().shortValue() == Sistema.CONTRATADO
						&& coberturaCotizacion.getClaveContrato().shortValue() == Sistema.CONTRATADO) {
					listaErrores.add("La cobertura \""+coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial()+
							"\" del inciso "+coberturaCotizacion.getId().getNumeroInciso().toBigInteger()+" requiere autorizaci�n de coaseguro.");
					return Boolean.FALSE;
				}
			}
			coberturaCotizacionDTO.setClaveAutCoaseguro(null);
			coberturaCotizacionDTO.setClaveAutDeducible(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION);
			listaCoberturaCotizacion = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotizacionDTO);
			for(CoberturaCotizacionDTO coberturaCotizacion : listaCoberturaCotizacion){
				if (coberturaCotizacion.getSeccionCotizacionDTO().getClaveContrato().shortValue() == Sistema.CONTRATADO
						&& coberturaCotizacion.getClaveContrato().shortValue() == Sistema.CONTRATADO) {
					listaErrores.add("La cobertura \""+coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial()+
							"\" del inciso "+coberturaCotizacion.getId().getNumeroInciso().toBigInteger()+" requiere autorizaci�n de deducible.");
					return Boolean.FALSE;
				}
			}
			
			List<RiesgoCotizacionDTO> listaRiesgoCotizacion = RiesgoCotizacionDN.getInstancia().listarRiesgosConCoasegurosDeduciblesPendientes(cotizacion.getIdToCotizacion());
			for(RiesgoCotizacionDTO riesgoCot : listaRiesgoCotizacion){
				if(riesgoCot.getClaveAutCoaseguro().equals(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION)){
					listaErrores.add("El riesgo \""+riesgoCot.getRiesgoCoberturaDTO().getRiesgoDTO().getNombreComercial()+
							"\" del inciso "+riesgoCot.getId().getNumeroInciso()+" requiere autorizaci�n de coaseguro.");
					return Boolean.FALSE;
				}
				if(riesgoCot.getClaveAutDeducible().equals(GestorPendientesDanos.ESTATUS_REQUIERE_AUTORIZACION)){
					listaErrores.add("El riesgo \""+riesgoCot.getRiesgoCoberturaDTO().getRiesgoDTO().getNombreComercial()+
							"\" del inciso "+riesgoCot.getId().getNumeroInciso()+" requiere autorizaci�n de deducible.");
					return Boolean.FALSE;
				}
			}
			
			List<DescuentoRiesgoCotizacionDTO> listaDescuentos = DescuentoRiesgoCotizacionDN.getInstancia().listarDescuentosPendientesAutorizacion(cotizacion.getIdToCotizacion());
			for(DescuentoRiesgoCotizacionDTO descuento : listaDescuentos){
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCotizacion(descuento.getId().getIdToCotizacion());
				riesgoCotizacionId.setNumeroInciso(descuento.getId().getNumeroInciso());
				riesgoCotizacionId.setIdToSeccion(descuento.getId().getIdToSeccion());
				riesgoCotizacionId.setIdToCobertura(descuento.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToRiesgo(descuento.getId().getIdToRiesgo());
				descuento.setRiesgoCotizacionDTO(RiesgoCotizacionDN.getInstancia().getPorId(riesgoCotizacionId));
				
				listaErrores.add("El descuento \""+descuento.getDescuentoDTO().getDescripcion()+"\" del riesgo "+
						descuento.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getRiesgoDTO().getNombreComercial()+
						"\", perteneciente al inciso "+descuento.getId().getNumeroInciso()+", requiere autorizaci�n.");
				return Boolean.FALSE;
			}
			List<AumentoRiesgoCotizacionDTO> listaAumentos = AumentoRiesgoCotizacionDN.getInstancia().listarAumentosPendientesAutorizacion(cotizacion.getIdToCotizacion());
			for(AumentoRiesgoCotizacionDTO aumento : listaAumentos){
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCotizacion(aumento.getId().getIdToCotizacion());
				riesgoCotizacionId.setNumeroInciso(aumento.getId().getNumeroInciso());
				riesgoCotizacionId.setIdToSeccion(aumento.getId().getIdToSeccion());
				riesgoCotizacionId.setIdToCobertura(aumento.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToRiesgo(aumento.getId().getIdToRiesgo());
				aumento.setRiesgoCotizacionDTO(RiesgoCotizacionDN.getInstancia().getPorId(riesgoCotizacionId));
				
				listaErrores.add("El aumento \""+aumento.getAumentoVarioDTO().getDescripcionAumento()+"\" del riesgo "+
						aumento.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getRiesgoDTO().getNombreComercial()+
						"\", perteneciente al inciso "+aumento.getId().getNumeroInciso()+", requiere autorizaci�n.");
				return Boolean.FALSE;
			}
			List<RecargoRiesgoCotizacionDTO> listaRecargos = RecargoRiesgoCotizacionDN.getInstancia().listarRecargosPendientesAutorizacion(cotizacion.getIdToCotizacion());
			for(RecargoRiesgoCotizacionDTO recargo : listaRecargos){
				RiesgoCotizacionId riesgoCotizacionId = new RiesgoCotizacionId();
				riesgoCotizacionId.setIdToCotizacion(recargo.getId().getIdToCotizacion());
				riesgoCotizacionId.setNumeroInciso(recargo.getId().getNumeroInciso());
				riesgoCotizacionId.setIdToSeccion(recargo.getId().getIdToSeccion());
				riesgoCotizacionId.setIdToCobertura(recargo.getId().getIdToCobertura());
				riesgoCotizacionId.setIdToRiesgo(recargo.getId().getIdToRiesgo());
				recargo.setRiesgoCotizacionDTO(RiesgoCotizacionDN.getInstancia().getPorId(riesgoCotizacionId));
				
				listaErrores.add("El aumento \""+recargo.getRecargoVarioDTO().getDescripcionrecargo()+"\" del riesgo "+
						recargo.getRiesgoCotizacionDTO().getRiesgoCoberturaDTO().getRiesgoDTO().getNombreComercial()+
						"\", perteneciente al inciso "+recargo.getId().getNumeroInciso()+", requiere autorizaci�n.");
				return Boolean.FALSE;
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return Boolean.TRUE;
	}

	@SuppressWarnings("static-access")
	private void verificarPendientesIncisoCotizacion(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion,boolean verificarEstatusInspeccion,boolean verificarAutorizacionInspeccion){
		List<IncisoCotizacionDTO> listaIncisos = null;
		try {
			listaIncisos = IncisoCotizacionDN.getInstancia().getPorPropiedad("id.idToCotizacion", cotizacion.getIdToCotizacion());
		} catch (SystemException e) { return;}
		for (IncisoCotizacionDTO inciso : listaIncisos){
			if (verificarEstatusInspeccion) {
				if (inciso.getClaveEstatusInspeccion().equals(this.ESTATUS_REQUIERE_AUTORIZACION)){
					if (inciso.getClaveAutInspeccion().shortValue() != 7){
						if (inciso.getClaveAutInspeccion().shortValue() == 8){
							Pendiente pendiente = new Pendiente();
							pendiente.setFecha(cotizacion.getFechaModificacion());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.3":"danios.mensaje.3.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),inciso.getId().getNumeroInciso()));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.3",cotizacion.getIdToCotizacion().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_DANOS);
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.3",cotizacion.getIdToCotizacion().toBigInteger().toString()));
							listaPendientes.add(pendiente);
						} else {
							Pendiente pendiente = new Pendiente();
							pendiente.setFecha(cotizacion.getFechaModificacion());
							pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.3":"danios.mensaje.3.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),inciso.getId().getNumeroInciso()));
							pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.3",cotizacion.getIdToCotizacion().toBigInteger().toString()));
							pendiente.setTipo(pendiente.TIPO_DANOS);
							pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.3",cotizacion.getIdToCotizacion().toBigInteger().toString()));
							listaPendientes.add(pendiente);
						}
					}
				}
				
				if (inciso.getClaveEstatusInspeccion().equals(this.ESTATUS_INSPECCION_RECHAZADA)){
					Pendiente pendiente = new Pendiente();
					pendiente.setFecha(cotizacion.getFechaModificacion());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.3":"danios.mensaje.3.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),inciso.getId().getNumeroInciso()));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.3",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					pendiente.setTipo(pendiente.TIPO_DANOS);
					pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.3",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					listaPendientes.add(pendiente);
				}
			}
			
			if (verificarAutorizacionInspeccion)
				if (inciso.getClaveAutInspeccion().equals(this.ESTATUS_REQUIERE_AUTORIZACION)){
					Pendiente pendiente = new Pendiente();
					pendiente.setFecha(cotizacion.getFechaModificacion());
					pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.4":"danios.mensaje.4.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion()),inciso.getId().getNumeroInciso()));
					pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.4",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					pendiente.setTipo(pendiente.TIPO_DANOS);
					pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.4",cotizacion.getIdToCotizacion().toBigInteger().toString()));
					listaPendientes.add(pendiente);
				}
		}
	}
	
	@SuppressWarnings("static-access")
	private Boolean verificarPendientesIncisoCotizacion(CotizacionDTO cotizacion,boolean verificarEstatusInspeccion,boolean verificarAutorizacionInspeccion,List<String> listaErrores){
		List<IncisoCotizacionDTO> listaIncisos = null;
		try {
			listaIncisos = IncisoCotizacionDN.getInstancia().getPorPropiedad("id.idToCotizacion", cotizacion.getIdToCotizacion());
		} catch (SystemException e) {}
		for (IncisoCotizacionDTO inciso : listaIncisos){
			if (verificarEstatusInspeccion) {
				if (inciso.getClaveEstatusInspeccion().equals(this.ESTATUS_REQUIERE_AUTORIZACION)){
					if(!inciso.getClaveAutInspeccion().equals(this.ESTATUS_OMISION_INSPECCION_AUTORIZADA)){
						listaErrores.add("El inciso n�mero "+inciso.getId().getNumeroInciso().toBigInteger()+" requiere autorizaci�n de inspecci�n.");
						return Boolean.FALSE;						
					}
				}
				
				if (inciso.getClaveEstatusInspeccion().equals(this.ESTATUS_INSPECCION_RECHAZADA)){
					listaErrores.add("El inciso n�mero "+inciso.getId().getNumeroInciso().toBigInteger()+" ha sido rechazado.");
					return Boolean.FALSE;
				}
				
			}
			
			if (verificarAutorizacionInspeccion)
				if (inciso.getClaveAutInspeccion().equals(this.ESTATUS_REQUIERE_AUTORIZACION)){
					listaErrores.add("El inciso n�mero "+inciso.getId().getNumeroInciso().toBigInteger()+" requiere autorizaci�n de inspecci�n.");
					return Boolean.FALSE;
				}
		}
		return Boolean.TRUE;
	}
	
	@SuppressWarnings("static-access")
	private void verificarPendientesTexAdicionalCotizacion(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		try {
			List<TexAdicionalCotDTO> listaTextoAdicional =TexAdicionalCotDN.getInstancia().buscarPorClaveAutorizacionPorCotizacion(cotizacion.getIdToCotizacion(), this.ESTATUS_REQUIERE_AUTORIZACION);
			if (listaTextoAdicional != null && listaTextoAdicional.size()>0){
				Pendiente pendiente = new Pendiente();
				//pendiente.setFecha(cotizacion.getFechaCreacion());
				try{
				pendiente.setFecha((listaTextoAdicional.get(0).getFechaModificacion()!=null)?listaTextoAdicional.get(0).getFechaModificacion():listaTextoAdicional.get(0).getFechaCreacion());
				}catch(Exception e){pendiente.setFecha(cotizacion.getFechaCreacion());}
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.2":"danios.mensaje.2.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.2",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.2",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {}
	}
	
	@SuppressWarnings("static-access")
	private Boolean verificarPendientesTexAdicionalCotizacion(CotizacionDTO cotizacion,List<String> listaErrores){
		try {
			List<TexAdicionalCotDTO> listaTextoAdicional =TexAdicionalCotDN.getInstancia().buscarPorClaveAutorizacionPorCotizacion(cotizacion.getIdToCotizacion(), this.ESTATUS_REQUIERE_AUTORIZACION);
			if (listaTextoAdicional != null && listaTextoAdicional.size()>0){
				listaErrores.add("La cotizaci�n tiene Textos adicionales por autorizar.");
				return Boolean.FALSE;
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {}
		return Boolean.TRUE;
	}
	
	@SuppressWarnings("static-access")
	private void verificarPendientesComisionCotizacion(CotizacionDTO cotizacion,List<Pendiente> listaPendientes,boolean mostrarClaveCotizacion){
		try {
			List<ComisionCotizacionDTO> listaComision= ComisionCotizacionDN.getInstancia().listarPorClaveAutorizacionPorCotizacion(cotizacion.getIdToCotizacion(), this.ESTATUS_REQUIERE_AUTORIZACION);
			if (listaComision != null && listaComision.size()>0){
				Pendiente pendiente = new Pendiente();
				pendiente.setFecha(cotizacion.getFechaModificacion());
				pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.1":"danios.mensaje.1.sinclave"),this.formateaIdCotizacion(cotizacion.getIdToCotizacion())));
				pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.1",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				pendiente.setTipo(pendiente.TIPO_DANOS);
				pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.1",cotizacion.getIdToCotizacion().toBigInteger().toString()));
				listaPendientes.add(pendiente);
			}
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {	}
	}
	
	@SuppressWarnings("static-access")
	private Boolean verificarPendientesComisionCotizacion(CotizacionDTO cotizacion,List<String> listaErrores){
		try {
			List<ComisionCotizacionDTO> listaComision= ComisionCotizacionDN.getInstancia().listarPorClaveAutorizacionPorCotizacion(cotizacion.getIdToCotizacion(), this.ESTATUS_REQUIERE_AUTORIZACION);
			if (listaComision != null && listaComision.size()>0){
				listaErrores.add("La cotizaci�n tiene comisiones pendientes por autorizar.");
				return Boolean.FALSE;
			}
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {	}
		return Boolean.TRUE;
	}
	
	
		
//	private Boolean verificarPendientesReaseguroFacultativo(CotizacionDTO cotizacionDTO){
//		
//	}
	
	public String formateaIdCotizacion(BigDecimal idToCotizacion){
		return StringUtils.leftPad(idToCotizacion.toBigInteger().toString(), 8, '0');	
	}
	/**
	 * METODO PARA PRESENTAR MENSAJES DE PENDIENTES POR VALIDACION DE FECHAS DE INCIO DE VIGENCIA, ENTRE OTROS, QUE FALTABAN
	 * @param cotizacionDTO
	 * @param pendientes
	 * @param mostrarClaveCotizacion
	 * @author cesar morales
	 */
	private void verificarPendientesCotizacionOtrasAutorizacion
	(CotizacionDTO cotizacionDTO, List<Pendiente> pendientes,boolean mostrarClaveCotizacion){
	    Pendiente pendiente =null;
	    if(cotizacionDTO.getClaveAutRetroacDifer()!= null && cotizacionDTO.getClaveAutRetroacDifer().equals(ESTATUS_REQUIERE_AUTORIZACION)){
		pendiente = new Pendiente();
		pendiente.setFecha(cotizacionDTO.getFechaModificacion());
		pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.22":"danios.mensaje.22.sinclave"),this.formateaIdCotizacion(cotizacionDTO.getIdToCotizacion())));
		pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.22",cotizacionDTO.getIdToCotizacion().toBigInteger().toString()));
		pendiente.setTipo(Pendiente.TIPO_DANOS);
		pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.22",cotizacionDTO.getIdToCotizacion().toBigInteger().toString()));
		pendientes.add(pendiente);
	    }
	    if(cotizacionDTO.getClaveAutVigenciaMaxMin()!= null && cotizacionDTO.getClaveAutVigenciaMaxMin().equals(ESTATUS_REQUIERE_AUTORIZACION)){
		pendiente = new Pendiente();
		pendiente.setFecha(cotizacionDTO.getFechaModificacion());
		pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.23":"danios.mensaje.23.sinclave"),this.formateaIdCotizacion(cotizacionDTO.getIdToCotizacion())));
		pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.23",cotizacionDTO.getIdToCotizacion().toBigInteger().toString()));
		pendiente.setTipo(Pendiente.TIPO_DANOS);
		pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.23",cotizacionDTO.getIdToCotizacion().toBigInteger().toString()));
		pendientes.add(pendiente);
	    }
	   if(cotizacionDTO.getClaveAutoEmisionDocOperIlicitas()!=null && cotizacionDTO.getClaveAutoEmisionDocOperIlicitas().equals(ESTATUS_REQUIERE_AUTORIZACION)){
		pendiente = new Pendiente();
		pendiente.setFecha(cotizacionDTO.getFechaModificacion());
		pendiente.setMensaje(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, ((mostrarClaveCotizacion)?"danios.mensaje.24":"danios.mensaje.24.sinclave"),this.formateaIdCotizacion(cotizacionDTO.getIdToCotizacion())));
		pendiente.setUrl(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.url.24",cotizacionDTO.getIdToCotizacion().toBigInteger().toString()));
		pendiente.setTipo(Pendiente.TIPO_DANOS);
		pendiente.setFuncionJavaScript(UtileriasWeb.getMensajeRecurso(RECURSO_MENSAJES_PENDIENTES, "danios.funcion.24",cotizacionDTO.getIdToCotizacion().toBigInteger().toString()));
		pendientes.add(pendiente);
	    }
	}
	
	private Boolean verificarPendientesRecargoPagoFraccionado(CotizacionDTO cotizacionDTO, List<String> listaErrores){
		if(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() != null && 
		   cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().compareTo(Sistema.AUTORIZACION_REQUERIDA) == 0){
			listaErrores.add("El recargo financiero a�n no ha sido autorizado.");
			return Boolean.FALSE;
		}		
		if(cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado() != null && 
		   cotizacionDTO.getClaveAutorizacionRecargoPagoFraccionado().compareTo(Sistema.RECHAZADA) == 0){
			listaErrores.add("El recargo financiero fue rechazado.");
			return Boolean.FALSE;
		}		
		return Boolean.TRUE;		  	
	}
	
	private Boolean verificarPendientesDerechos(CotizacionDTO cotizacionDTO, List<String> listaErrores){
		if(cotizacionDTO.getClaveAutorizacionDerechos() != null && 
		   cotizacionDTO.getClaveAutorizacionDerechos().compareTo(Sistema.AUTORIZACION_REQUERIDA) == 0){
			listaErrores.add("Los gastos de expedici�n a�n no han sido autorizados.");
			return Boolean.FALSE;
		}		
		if(cotizacionDTO.getClaveAutorizacionDerechos() != null &&
		   cotizacionDTO.getClaveAutorizacionDerechos().compareTo(Sistema.RECHAZADA) == 0){
			listaErrores.add("Los gastos de expedici�n fueron rechazados.");
			return Boolean.FALSE;
		}		
		return Boolean.TRUE;		  	
	}
	
}
