/**
 * 
 */
package mx.com.afirme.midas.sistema.gestionPendientes;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;

/**
 * @author user
 *
 */
public class ControladorPendientesUsuario {
	
//	private Map<Integer,List<Pendiente>> mapaPendientes;
	
	public ControladorPendientesUsuario(){
		
	}
		
	public List<Pendiente> obtenerPendientes(Usuario usuario, int tipo, boolean refresh){
		List<Pendiente> lista = new ArrayList<Pendiente>();
		ControladorGestionadores controladorGestionadores = ControladorGestionadores.getInstance();
		lista = controladorGestionadores.obtenerPendientes(usuario, tipo,refresh);
		return lista;
	}

}
