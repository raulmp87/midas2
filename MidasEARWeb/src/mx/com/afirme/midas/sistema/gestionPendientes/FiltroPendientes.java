/**
 * 
 */
package mx.com.afirme.midas.sistema.gestionPendientes;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.UtileriasWeb;

public class FiltroPendientes implements Filter{

	 @SuppressWarnings("unused")
	private String onErrorUrl = null;
	 @SuppressWarnings("unchecked")
	private Map mapa;
	  
	  
	@SuppressWarnings("unchecked")
	public void init(FilterConfig filterConfig)
	      throws ServletException {
	    onErrorUrl = filterConfig.getInitParameter("onError");
	    mapa = new HashMap();
	    boolean banderaTipo = true;
	    int indicePendientes = 1;
	    int indiceMapa = 1;
	    while (banderaTipo){
	    	String tipoPendiente = UtileriasWeb.getMensajeRecurso("mx.com.afirme.midas.sistema.gestionPendientes.mensajes_pendientes", "pendientes."+indicePendientes);
			if(tipoPendiente != null){    
	    		boolean bandera = true;
			    while(bandera!=false){
			    	String urlFiltrar = UtileriasWeb.getMensajeRecurso("mx.com.afirme.midas.sistema.gestionPendientes.mensajes_pendientes", tipoPendiente+".url."+indiceMapa);
			    	if(urlFiltrar != null){
			    		mapa.put(indiceMapa, eliminaParametros(urlFiltrar));
			    	}else{
			    		bandera=false;
			    	}
			    	indiceMapa++;
			    }
			}else{
				banderaTipo=false;
			}
			indicePendientes++;
	    }
	}
	  
	public void doFilter(ServletRequest request,  
	                     ServletResponse response,
	                     FilterChain chain)
	                 throws IOException, ServletException {
	    
		HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        
        String urlLlamada = req.getContextPath()+req.getServletPath();
        
        if(mapa.containsValue(urlLlamada)){
        
	        Object banderaRefresh = req.getSession().getAttribute("banderaRefresh");
	        if(banderaRefresh != null){
	        	req.getSession().setAttribute("banderaRefresh", "true");
	        }
        }
        chain.doFilter(req, res);
	      
	}
	  
	public void destroy() {
	}
	
	private String eliminaParametros(String texto){
		String resultado="";
		
		resultado = texto.substring(0,texto.indexOf(".do")+3);
		return resultado;
		
	}
	
	
}
