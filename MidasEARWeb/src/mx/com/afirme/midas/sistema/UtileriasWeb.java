package mx.com.afirme.midas.sistema;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.intermoduloerrorlog.InterModuloErrorLogDN;
import mx.com.afirme.midas.sistema.intermoduloerrorlog.InterModuloErrorLogDTO;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.struts.Globals;
import org.apache.struts.util.MessageResources;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

/**
 * 
 * @author Christian Ceballos
 * 
 */
public final class UtileriasWeb {
	

	public static final String SEPARADOR = "_";
	
	/**
	 * Convierte una cadena a su representacion en BigDecimal
	 * 
	 * @author Christian Ceballos
	 * @param parametro
	 * @return
	 * @throws SystemException
	 */
	public static BigDecimal regresaBigDecimal(String parametro)
			throws SystemException {
		BigDecimal bigDecimal = new BigDecimal(parametro);
		return bigDecimal;
	}

	/**
	 * Valida si una cadena de caracteres se encuetra vacia
	 * 
	 * @author Christian Ceballos
	 * @param cadena
	 * @return
	 */
	public static boolean esCadenaVacia(String cadena) {
		boolean esVacia = true;
		if (cadena != null) {
			esVacia = (cadena.trim().length() == 0);
		}
		return esVacia;
	}

	/**
	 * Convierte una cadena a su representacion en Short
	 * 
	 * @author Christian Ceballos
	 * @param parametro
	 * @return
	 */
	public static Short regresaShort(String parametro) {
		return Short.valueOf(parametro);
	}

	/**
	 * Convierte una cadena a su representacion en Double
	 * 
	 * @author Jos� Luis Arellano
	 * @param parametro
	 * @return
	 */
	public static Double regresaDouble(String parametro) {
		return Double.valueOf(parametro);
	}

	public static Integer regresaInteger(String parametro){
		return Integer.valueOf(parametro);
	}
	
	/**
	 * Valida si una secuencia de caracteres(subcadena) esta contenida dentro de
	 * otra
	 * 
	 * @author Christian Ceballos
	 * @param cadena
	 * @param subCadena
	 * @return
	 */
	public static final boolean cadenaContieneSubCadena(String cadena,
			String subCadena) {
		return Pattern.compile(cadena).matcher(subCadena).find();
	}
	
	/**
	 * Valida si una secuencia de caracteres(subcadena) esta contenida dentro de
	 * otra
	 * 
	 * @author https://stackoverflow.com/questions/86780/how-to-check-if-a-string-contains-another-string-in-a-case-insensitive-manner-in/25379180#25379180
	 * @param cadena
	 * @param subCadena
	 * @return
	 */	
	public static boolean containsIgnoreCase(String src, String what) {
	    final int length = what.length();
	    if (length == 0)
	        return true; // Empty string is contained

	    final char firstLo = Character.toLowerCase(what.charAt(0));
	    final char firstUp = Character.toUpperCase(what.charAt(0));

	    for (int i = src.length() - length; i >= 0; i--) {
	        // Quick check before calling the more expensive regionMatches() method:
	        final char ch = src.charAt(i);
	        if (ch != firstLo && ch != firstUp)
	            continue;

	        if (src.regionMatches(true, i, what, 0, length))
	            return true;
	    }

	    return false;
	}	

	/**
	 * Verifica que una excepcion haya sido registrada y envia un mensaje al
	 * usuario conteniendo el mensaje respectivo para la excepcion tomandolo del
	 * archivo de recursos
	 * 
	 * @author Christian Ceballos
	 * @param mensajeExcepcion
	 * @param request
	 * @return
	 */
	public static String mandaMensajeExcepcionRegistrado(
			String mensajeExcepcion, HttpServletRequest request) {
		if (mensajeExcepcion == null) {
			return mensajeExcepcion;
		}
		if (mensajeExcepcion.equals(Sistema.EXCEPCION_ROLLBACK)) {
			guardaValorSessionScope(request, Sistema.MENSAJE_USUARIO,
					Sistema.MENSAJE_ERROR);
			guardaValorSessionScope(request, Sistema.CLAVE_ERROR,
					getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
							Sistema.MENSAJE_EXCEPCION_ROLLBACK));
		} else if (mensajeExcepcion.equals(Sistema.NO_DISPONIBLE)) {
			guardaValorSessionScope(request, Sistema.MENSAJE_USUARIO,
					Sistema.MENSAJE_ERROR);
			guardaValorSessionScope(request, Sistema.CLAVE_ERROR,
					getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
							Sistema.MENSAJE_EXCEPCION_SISTEMA_NO_DISPONIBLE));
		} else if (mensajeExcepcion.equals(Sistema.EXCEPCION_OBTENER_DTO)) {
			guardaValorSessionScope(request, Sistema.MENSAJE_USUARIO,
					Sistema.MENSAJE_ERROR);
			guardaValorSessionScope(request, Sistema.CLAVE_ERROR,
					getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
							Sistema.MENSAJE_EXCEPCION_OBTENER_DTO));
		}
		return null;
	}

	/**
	 * Obtiene el SessionScope
	 * 
	 * @author Christian Ceballos
	 * @param request
	 * @return
	 */
	private static HttpSession getSesion(HttpServletRequest request) {
		return request.getSession();
	}

	/**
	 * Guarda un valor en el SessionScope identificado por una clave
	 * 
	 * @param request
	 * @param clave
	 * @param valor
	 * @return
	 */
	public static String guardaValorSessionScope(HttpServletRequest request,
			String clave, Object valor) {
		HttpSession sesion = getSesion(request);
		sesion.setAttribute(clave, valor);
		return null;
	}

	/**
	 * Obtiene un determinado valor del SessionScope identificado por una clave
	 * 
	 * @author Christian Ceballos
	 * @param request
	 * @param clave
	 * @return
	 */
	public static Object obtenValorSessionScope(HttpServletRequest request,
			String clave) {
		HttpSession sesion = getSesion(request);
		return sesion.getAttribute(clave);
	}

	/**
	 * Valida si existe un valor almacenado en el SessionScope
	 * 
	 * @author Christian Ceballos
	 * @param request
	 * @param clave
	 * @return
	 */
	public static boolean existeValorSessionScope(HttpServletRequest request,
			String clave) {
		return (obtenValorSessionScope(request, clave) != null);
	}

	/**
	 * Elimina un valor del SessionScope identificado por una clave
	 * 
	 * @author Christian Ceballos en caso de que este exista
	 * @param request
	 * @param clave
	 * @return
	 */
	public static String eliminaValorSessionScope(HttpServletRequest request,
			String clave) {
		HttpSession sesion;
		if (existeValorSessionScope(request, clave)) {
			sesion = getSesion(request);
			sesion.removeAttribute(clave);
		}
		return null;
	}

	/**
	 * Obtiene un mensaje identificado por una clave de un archivo de recursos
	 * 
	 * @param recurso
	 * @param clave
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static synchronized String getMensajeRecurso(String recurso,
			String clave) {
		ResourceBundle objlResourceBundle = getRecurso(recurso);
		Enumeration objlEnum = null;
		boolean blExist = false;
		String slMessage = null;
		for (objlEnum = objlResourceBundle.getKeys(); !esObjetoNulo(objlEnum)
				&& objlEnum.hasMoreElements() && !blExist;) {
			if (clave.equalsIgnoreCase((String) objlEnum.nextElement())) {
				blExist = true;
			}
		}

		if (blExist) {
			slMessage = objlResourceBundle.getString(clave);
		}
		return slMessage;
	}

	/**
	 * Obtiene un archivo de recursos
	 * 
	 * @param recurso
	 * @return
	 */
	private static synchronized ResourceBundle getRecurso(String recurso) {
		ResourceBundle objlResource = null;
		objlResource = ResourceBundle.getBundle(recurso);
		return objlResource;
	}

	/**
	 * Valida si un Objeto es nulo
	 * 
	 * @author Christian Ceballos
	 * @param objeto
	 * @return
	 */
	public static boolean esObjetoNulo(Object objeto) {
		if (objeto == null) {
			return true;
		}
		return esCadenaVacia(objeto.toString());
	}

	public static String formatoMoneda(Double cantidad) {
		NumberFormat formateador = new DecimalFormat("$#,##0.00");
		return cantidad != null ? formateador.format(cantidad) : null;
	}
	
	public static String formatoMoneda(BigDecimal cantidad) {
		if (cantidad != null) {
			return formatoMoneda(cantidad.doubleValue());
		} else {
			return null;
		}
	}
	
	public static Double parseaformatoMoneda(String cadenaMoneda) {
		NumberFormat formateador = new DecimalFormat("$#,##0.00");
		Double cantidad = null;
		try{
			cantidad = new Double(formateador.parse(cadenaMoneda).doubleValue());
		}catch(Exception e){
			System.out.println("Error en UtileriasWeb.parseaformatoMoneda para la cadena ->" + cadenaMoneda + "<- ==> " + e.getMessage());
		}
		return cantidad;
	}

	public static Double eliminaFormatoMoneda(String cadenaMoneda) {
		String pattern = "[^\\s0-9]";
		Double cantidad = null;
		if (!cadenaMoneda.equals("")) {
			cantidad = new Double(Double.parseDouble(cadenaMoneda.replaceAll(
					pattern, "")) / 100);
		}
		return cantidad;
	}

	/**
	 * Method getFechaString
	 * 
	 * @param date
	 *            : la fecha como objeto java.util.Date
	 * @return dateString: la fecha transformada a String con formato
	 *         "dd/MM/yyyy"
	 */
	public static String getFechaString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}
	
	/**
	 * Method getFechaStr
	 * 
	 * @param date
	 *            : la fecha como objeto java.util.Date
	 * @return dateString: la fecha transformada a String con formato
	 *         "yyyyMMdd"
	 */
	public static String getFechaStr(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}
	
	/**
	 * Method getFechaStringConNombreMes
	 * 
	 * @param date
	 *            : la fecha como objeto java.util.Date
	 * @return dateString: la fecha transformada a String con formato
	 *         "dd 'de' MMMMM 'de' yyyy"
	 */
	public static String getFechaStringConNombreMes(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMMM 'de' yyyy");
		return sdf.format(date);
	}

	/**
	 * Method getHoraConSegundosString
	 * 
	 * @param date
	 *            : la fecha como objeto java.util.Date
	 * @return dateString: la fecha transformada a String con formato "HH:mm:ss"
	 */
	public static String getHoraConSegundosString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(date);
	}
	
	/**
	 * Method getHoraString
	 * 
	 * @param date
	 *            : la fecha como objeto java.util.Date
	 * @return dateString: la fecha transformada a String con formato "HH:mm"
	 */
	public static String getHoraString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(date);
	}

	/**
	 * Method getFechaHoraString
	 * 
	 * @param date
	 *            : la fecha como objeto java.util.Date
	 * @return dateString: la fecha transformada a String con formato
	 *         "dd/MM/yyyy HH:mm"
	 */
	public static String getFechaHoraString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return sdf.format(date);
	}

	/**
	 * Method getFechaHoraFromString
	 * 
	 * @param fechaHora
	 *            : la fecha como String con formato "dd/MM/yyyy HH:mm"
	 * @return date: la fecha transformada a un objeto java.util.Date
	 */
	public static Date getFechaHoraFromString(String fechaHora)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return sdf.parse(fechaHora);
	}

	/**
	 * Method getFechaFromString
	 * 
	 * @param fecha
	 *            : la fecha como String con formato "dd/MM/yyyy"
	 * @return date: la fecha transformada a un objeto java.util.Date
	 */
	public static Date getFechaFromString(String fecha) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.parse(fecha);
	}
	
	public static Date getFechaFromStringHS(String fecha) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.parse(fecha);
	}
	
	/**
	 * Method sonFechasIguales
	 * Trunca las horas de las fechas y solo compara
	 * en base al formato de fecha dd/MM/yyyy, se excluyen las
	 * horas, minutos y milisegundos
	 * 
	 * @param fecha1
	 *            : la fecha1 a comparar
	 * @param fecha2
	 *            : la fecha2 a comparar
	 * @return boolean: si las fechas son iguales regresa true
	 */	
	public static boolean sonFechasIguales(Date fecha1, Date fecha2){
		boolean sonIguales = false;
		String fechaString1 = UtileriasWeb.getFechaString(fecha1);
		String fechaString2 = UtileriasWeb.getFechaString(fecha2);
		try{
			
			Date fechaDate1 = UtileriasWeb.getFechaFromString(fechaString1);
			Date fechaDate2 = UtileriasWeb.getFechaFromString(fechaString2);	
			if (fechaDate1.compareTo(fechaDate2) == 0) {
				sonIguales = true;
			}
		}catch (Exception e) {
			return sonIguales;
		}
		return sonIguales;
	}
	/**
	 * Method llenarIzquierda
	 * 
	 * @param original
	 *            : String original
	 * @param caracter
	 *            : caracter (o caracteres) con los que se requiere llenar el
	 *            String original por la izquierda
	 * @param cantidadTotal
	 *            : cantidad total de caracteres del String resultante
	 * @return String: String modificado
	 */
	public static String llenarIzquierda(String original, String caracter,
			int cantidadTotal) {
		StringBuilder modificado = new StringBuilder("");
		for (int i = 0; i < cantidadTotal - original.length(); i++) {
			modificado.append(caracter);
		}
		modificado.append(original);
		return modificado.toString();
	}

	/**
	 * Metodo que devuelve un 1 o 0 si el valor del check en la JSP es "on" o
	 * null respectivamente
	 * 
	 * @param String
	 *            checkValue El valor recibido del check.
	 * @return String valorEntero El valor 1 o cero correspondiente al valor del
	 *         checkbox
	 */
	public static String calculaValorEnteroDelCheck(String checkValue) {
		String valorEntero = "0";
		if (!StringUtil.isEmpty(checkValue)) {
			if (checkValue.equals("on")) {
				valorEntero = "1";
			}
		}
		return valorEntero;
	}
	
	/**
	 * Metodo que devuelve true o falsesi el valor del check en la JSP es "on" o
	 * null respectivamente
	 * 
	 * @param String
	 *            checkValue El valor recibido del check.
	 * @return Boolean
	 */
	public static Boolean calculaValorBooleanDelCheck(String checkValue) {
		Boolean result = Boolean.FALSE;
		if (!StringUtil.isEmpty(checkValue)) {
			result = checkValue.equals("on");
		}
		return result;
	}

	/**
	 * Metodo que devuelve un 1 o 0 si el valor del check en la JSP es "on" o
	 * null respectivamente
	 * 
	 * @param String
	 *            checkValue El valor recibido del check.
	 * @return Integer valorEntero El valor 1 o cero correspondiente al valor
	 *         del checkbox
	 */
	public static Integer calculaValorEnteroDelCheckInteger(String checkValue) {
		Integer valorEntero = 0;
		if (!StringUtil.isEmpty(checkValue)) {
			if (checkValue.equals("on")) {
				valorEntero = 1;
			}
		}
		return valorEntero;
	}

	/**
	 * Metodo que devuelve "on" o null para mostrar en un check si se recibe un
	 * 1 o 0 respectivamente
	 * 
	 * @param String
	 *            fieldValue El valor del campo en el objeto DTO.
	 * @return String result El valor "on" o null correspondiente al valor del
	 *         String recibido.
	 */
	public static String calculaValorStringDelCheck(String fieldValue) {
		String result = null;
		if (!StringUtil.isEmpty(fieldValue)) {
			if (fieldValue.equals("1")) {
				result = "on";
			}
		}
		return result;
	}
	
	/**
	 * Metodo que devuelve "on" o null para mostrar en un check si se recibe un
	 * 1 o 0 respectivamente
	 * 
	 * @param Boolean
	 *            fieldValue El valor del campo en el objeto DTO.
	 * @return String result El valor "on" o null correspondiente al valor del
	 *         String recibido.
	 */
	public static String calculaValorStringDelCheck(Boolean fieldValue) {
		String result = null;
		if (fieldValue != null && fieldValue.booleanValue()) {
			result = "on";
		}
		return result;
	}

	/**
	 * Metodo que devuelve "on" o null para mostrar en un check si se recibe un
	 * 1 o 0 respectivamente
	 * 
	 * @param int fieldValue El valor del campo en el objeto DTO.
	 * @return String result El valor "on" o null correspondiente al valor del
	 *         String recibido.
	 */
	public static String calculaValorStringDelCheck(int fieldValue) {
		String result = null;
		if (fieldValue == 1) {
			result = "on";
		}
		return result;
	}

	/**
	 * Busca un registro de CatalogoValorFijo usando sus id�s y devuelve la
	 * descripci�n del cat�logo
	 * 
	 * @param int idGrupoValores, el grupo de valores del catalogo de valores
	 *        fijos.
	 * @param int idDato, el ID del registro con el grupo de valores indicado.
	 * @return String result La descripci�n del registro CatalogoValorFijo, si
	 *         no encuentra el registro devuelve una cadena vac�a.
	 */
	public static String getDescripcionCatalogoValorFijo(int idGrupoValores,
			int idDato) {
		String result = "";
		CatalogoValorFijoId id = new CatalogoValorFijoId(idGrupoValores, idDato);
		try {
			CatalogoValorFijoFacadeRemote beanRemoto = ServiceLocator
					.getInstance().getEJB(CatalogoValorFijoFacadeRemote.class);
			CatalogoValorFijoDTO catalogoValorFijoDTO = (CatalogoValorFijoDTO) beanRemoto.findById(id);
			result = catalogoValorFijoDTO.getDescripcion();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Busca un registro de CatalogoValorFijo usando sus id�s y devuelve la
	 * descripci�n del cat�logo
	 * 
	 * @param int idGrupoValores, el grupo de valores del catalogo de valores
	 *        fijos.
	 * @param String
	 *            idDato, el ID del registro con el grupo de valores indicado.
	 * @return String result La descripci�n del registro CatalogoValorFijo, si
	 *         no encuentra el registro devuelve una cadena vac�a.
	 */
	public static String getDescripcionCatalogoValorFijo(int idGrupoValores,
			String idDato) {
		String result = "";
		try {
			result = getDescripcionCatalogoValorFijo(idGrupoValores, Integer
					.valueOf(idDato));
		} catch (Exception e) {
		}
		return result;
	}

	/**
	 * gernerarReporte. M�todo que genera un reporte PDF o XLS y lo devuelve al
	 * objeto HttpServletResponse que recibe. En caso de que la plantilla
	 * indicada est� mal estructurada, se lanza la excepci�n JRException.
	 * 
	 * @param String
	 *            nombrePlantilla. El nombre de la plantilla con la cual se
	 *            generar� el reporte, esta cadena debe contener el nombre
	 *            completo del archivo, incluyendo su extensi�n.
	 * @param String
	 *            rutaPlantilla. El directorio en el cual se encuentra la
	 *            plantilla. Este directorio debe indicarse de la misma forma
	 *            que est�n estructurados los archivos fuente en los paquetes
	 *            del proyecto, ejemplo:
	 *            "/mx/com/afirme/midas/danios/reportes/cotizacion/unicaubicacion/"
	 * @param String
	 *            extension. String con el cual se indica qu� tipo de reporte se
	 *            desea generar, los valores permitidos son "xls" o "pdf".
	 * @param Map
	 *            <String,String> parametros. Colecci�n de par�metros que
	 *            recibir� la plantilla, los cuales son definidos en la misma
	 *            plantilla.
	 * @param Collection
	 *            listaObjetos. Colecci�n de objetos con los cuales se poblar�
	 *            el reporte.
	 * @param HttpServletResponse
	 *            response. objeto response en el cual se escribir� el reporte
	 *            generado.
	 * @throws JRException.
	 * @author Jose Luis Arellano
	 */
	@SuppressWarnings( { "unchecked" })
	public static void generarReporte(String nombrePlantilla,
			String rutaPlantilla, String extension,
			Map<String, Object> parametros, Collection listaObjetos,
			HttpServletResponse response) throws JRException {
		try {
			JRBeanCollectionDataSource dataSource;
			JasperReport jasperReport = null;
			JasperPrint jasperPrint;

			jasperReport = JasperCompileManager
					.compileReport(UtileriasWeb.class
							.getResourceAsStream(rutaPlantilla
									+ nombrePlantilla));
			// jasperReport =
			// JasperCompileManager.compileReport(rutaPlantilla+nombrePlantilla);
			dataSource = new JRBeanCollectionDataSource(listaObjetos);
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parametros, dataSource);

			byte byteArray[];
			String fileName;
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			if (extension.toLowerCase().indexOf("xls") != -1) {
				fileName = nombrePlantilla.substring(0, nombrePlantilla
						.lastIndexOf("."))
						+ (Math.random() * 1000) + ".xls";
				JRXlsExporter xlsExporter = new JRXlsExporter();
				xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
						outputStream);
				xlsExporter.setParameter(
						JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
						Boolean.FALSE);
				xlsExporter.setParameter(
						JRXlsExporterParameter.IS_DETECT_CELL_TYPE,
						Boolean.TRUE);
				xlsExporter.setParameter(
						JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED,
						Boolean.TRUE);
				xlsExporter.exportReport();
				byteArray = outputStream.toByteArray();
			} else {
				fileName = nombrePlantilla + (Math.random() * 1000) + ".pdf";
				JasperExportManager.exportReportToPdfStream(jasperPrint,
						outputStream);
				byteArray = outputStream.toByteArray();
			}
			OutputStream responseOutputStream = response.getOutputStream();
			response.setHeader("Content-Disposition", "attachment; filename="
					+ fileName.substring(fileName.lastIndexOf("\\") + 1,
							fileName.length()));
			response.setContentType("application/unknown");
			response.setContentLength(byteArray.length);
			response.setBufferSize(1024 * 15);
			responseOutputStream.write(byteArray);
			responseOutputStream.flush();
			responseOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * M�todo que genera un archivo .Jasper a partir de una plantilla JRXML.
	 * 
	 * @param directorioOrigen
	 *            . Directorio en el cual se encuentra ubicada la plantilla
	 *            jrxml.
	 * @param nombrePlantilla
	 *            . Nombre de la plantilla JRXML que se procesar�.
	 * @param directorioDestino
	 *            . Directorio en el cual se guardar� el archivo .jasper
	 *            generado.
	 * @param nombreJasper
	 *            . Nombre que se le dar� al archivo .jasper generado.
	 * @param sobreescribirArchivo
	 *            . Sirve para especificar si se debe sobreescribir el archivo
	 *            .jasper en caso de que ya exista en el directorio recibido
	 * @throws FileNotFoundException
	 * @throws JRException
	 * @autor Jos� Luis Arellano
	 */
	public static void generarPlantillaJasper(String directorioOrigen,
			String nombrePlantilla, String directorioDestino,
			String nombreJasper, boolean sobreescribirArchivo)
			throws FileNotFoundException, JRException {
		if ((directorioOrigen == null) || (nombrePlantilla == null)) {
			throw new FileNotFoundException();
		}

		if (esCadenaVacia(directorioDestino)) {
			directorioDestino = directorioOrigen;
		}
		if (esCadenaVacia(nombreJasper)) {
			nombreJasper = nombrePlantilla.substring(0, nombrePlantilla
					.lastIndexOf("."))
					+ ".jasper";
		} else if (!nombreJasper.endsWith(".jasper")
				|| !nombreJasper.endsWith(".JASPER")) {
			nombreJasper += ".jasper";
		}

		java.io.File file = new java.io.File(directorioDestino + nombreJasper);
		if (file.exists()) {
			if (sobreescribirArchivo) {
				JasperCompileManager.compileReportToFile(directorioOrigen
						+ nombrePlantilla, directorioDestino + nombreJasper);
			}
		} else {
			JasperCompileManager.compileReportToFile(directorioOrigen
					+ nombrePlantilla, directorioDestino + nombreJasper);
		}

		// } catch (IOException e) {
		// e.printStackTrace();
		// }

	}

	/**
	 * generarLineaImagenDataGrid. Genera un String formateado para insertar una
	 * imagen en un DataGrid.
	 * 
	 * @param urlImg
	 *            . La direcci�n de la imagen que se desea mostrar. Si recibe
	 *            null, desplegar� una imagen en blanco.
	 * @param toolTip
	 *            . El texto que aparecer� al posicionar el puntero sobre la
	 *            imagen. despliega una cadena vac�a si recibe null.
	 * @param funcion
	 *            . La funci�n Javascript que se ejecutar� al hacer clic en la
	 *            imagen. Debe terminar con ';'. ejecutar� void(0); si recibe
	 *            null.
	 * @param destino
	 *            . El frame destino de la acci�n. Por lo general se usa la
	 *            cadena "_self", la cual se utiliza por default si se recibe
	 *            null.
	 * @return La cadena formada para insertar una imagen en una celda del grid.
	 * @autor Jos� Luis Arellano.
	 */
	public static String generarLineaImagenDataGrid(String urlImg,
			String toolTip, String funcion, String destino) {
		String result = "";
		if (!UtileriasWeb.esCadenaVacia(urlImg)) {
			result += urlImg;
		} else {
			result += "/MidasWeb/img/blank.gif";
		}
		if (!UtileriasWeb.esCadenaVacia(toolTip)) {
			result += "^" + toolTip;
		} else {
			result += "^";
		}
		if (!UtileriasWeb.esCadenaVacia(funcion)) {
			result += "^javascript:" + funcion;
		} else {
			result += "^javascript:void(0)";
		}
		if (!UtileriasWeb.esCadenaVacia(destino)) {
			result += "^" + destino;
		} else {
			result += "^_self";
		}
		result = result.replaceAll("'", "&#39;");
		return result;
	}

	/**
	 * Regresa la cadena de texto referida por el key en el archivo de recursos
	 * 
	 * @param recurso
	 *            nombre del archivo de recursos
	 * @param key
	 *            Clave del texto en el archivo de recursos
	 * @param params
	 *            Argumentos del texto si los hay
	 * @return Texto definido en el archivo de recursos
	 */
	public static String getMensajeRecurso(String recurso, String key,
			Object... params) {
		ResourceBundle resourceBundle = getRecurso(recurso);
		String strTextL = "";
		try {
			strTextL = resourceBundle.getString(key);
			if (params != null && params.length > 0) {
				MessageFormat messageFormat = new MessageFormat(strTextL);
				strTextL = messageFormat.format(params);
			}
		} catch (MissingResourceException e) {
			e.printStackTrace();
		}
		return strTextL;
	}
	
	/**
	 * Regresa la cadena de texto referida por la key en el archivo
	 * {@link Sistema}.ARCHIVO_RECURSOS
	 * 
	 * @param key
	 * @param params
	 * @return
	 */
	public static String getMensajeRecurso(String key,
			Object... params) { 
		return getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, key, params);
	}
	
	public static boolean contieneRol(List<Rol> roles, Object... params) {
		for (Object valorRol : params) {
			for (Rol rol : roles) {
				if (rol.getDescripcion().equals(valorRol)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean esTipoNegocio(BigDecimal valor, int... params) {
		if (valor != null) {
			for (int tipoNegocio : params) {
				if (valor.intValue() == tipoNegocio) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @author Rodrigo M�rquez Castillo
	 * @see Funci�n "procesarRespuestaXml" de JavaScript (en
	 *      ajaxScriptWindow.js) con la cual es compatible �ste m�todo
	 * @param String
	 *            id, puede ser 10 (Ventana de operaci�n exitosa), 20 (Ventana
	 *            informativa) o 30 (Ventana de error) dependiendo del tipo de
	 *            mensaje que se est� enviando <id>tipo de mensaje</id>
	 * @param String
	 *            mensaje, mensaje a imprimir en el XML
	 *            <mensaje>mensaje</mensaje>
	 * @return void
	 */
	public static void imprimeMensajeXML(String id, String mensaje,
			HttpServletResponse response) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<id>");
		buffer.append(id);
		buffer.append("</id>");
		buffer.append("<description><![CDATA[");
		buffer.append(mensaje);
		buffer.append("]]></description>");
		buffer.append("</item>");
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Alfredo Osorio
	 * @see Funci�n "procesarRespuestaXml" de JavaScript (en
	 *      ajaxScriptWindow.js) con la cual es compatible �ste m�todo.
	 *      Metodo de conveniencia para que se pueda obtener el mensaje a traves de un resource bundle
	 *      asociado al request y el mensaje sea buscado con la llave indicada.
	 * @param request
	 * @param response
	 * @param id  puede ser 10 (Ventana de operaci�n exitosa), 20 (Ventana
	 *            informativa) o 30 (Ventana de error) dependiendo del tipo de
	 *            mensaje que se est� enviando <id>tipo de mensaje</id>
	 * @param llave esta es la llave con la que se buscara en el resource bundle de struts.
	 * @param parametros son los parametros posicionales que se utilizan para generar el mensaje.
	 * @return void
	 */
	public static void imprimeMensajeXML(HttpServletRequest request, HttpServletResponse response,
			String id, String llave, Object... parametros) {
		MessageResources messageResources = (MessageResources) request.getAttribute(
				Globals.MESSAGES_KEY);
		String mensaje = null;
		if (parametros == null) {
			mensaje = messageResources.getMessage(llave);			
		} else {
			mensaje = messageResources.getMessage(llave, parametros);
		}
		imprimeMensajeXML(id, mensaje, response);
	}

	/**
	 * @author Rodrigo M�rquez Castillo
	 * @param items
	 *            , ArrayList del objeto que se desea iterar (generalmente para
	 *            DTOs)
	 * @param nombreMetodoId
	 *            , nombre del m�todo (en el objeto) que devuelve el Id a
	 *            imprimir
	 * @param nombreMetodoDescripcion
	 *            , nombre del m�todo (en el objeto) que devuelve la descripcion
	 *            a imprimir
	 * @return void
	 */
	@SuppressWarnings("unchecked")
	public static void imprimeMensajeXML(Object itemsObject,
			String nombreMetodoId, String nombreMetodoDescripcion,
			HttpServletResponse response) {
		String nombreMetodoIdFormateado;
		String nombreMetodoDescripcionFormateado;
		Class params[] = {};
		Object paramsObj[] = {};
		if (nombreMetodoId != null && !nombreMetodoId.equals("")
				&& nombreMetodoDescripcion != null
				&& !nombreMetodoDescripcion.equals("")) {
			nombreMetodoIdFormateado = "get"
					+ String.valueOf(nombreMetodoId.charAt(0)).toUpperCase()
					+ nombreMetodoId.substring(1, nombreMetodoId.length());
			nombreMetodoDescripcionFormateado = "get"
					+ String.valueOf(nombreMetodoDescripcion.charAt(0))
							.toUpperCase()
					+ nombreMetodoDescripcion.substring(1,
							nombreMetodoDescripcion.length());
			List<Object> items = (List<Object>) itemsObject;
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			try {
				for (Object object : items) {
					Method metodo1 = object.getClass().getDeclaredMethod(
							nombreMetodoIdFormateado, params);
					Method metodo2 = object.getClass().getDeclaredMethod(
							nombreMetodoDescripcionFormateado, params);
					buffer.append("<item>");
					buffer.append("<id>");
					buffer.append(metodo1.invoke(object, paramsObj).toString());
					buffer.append("</id>");
					buffer.append("<description><![CDATA[");
					buffer.append(metodo2.invoke(object, paramsObj).toString());
					buffer.append("]]></description>");
					buffer.append("</item>");
				}
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Obtiene el nombre del usuario de Midas registrado
	 * 
	 * @param request
	 *            Request del servlet
	 * @return El nombre del usuario de Midas registrado
	 */
	@Deprecated
	public static String obtieneNombreUsuario(HttpServletRequest request) {

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);

		if (usuario != null) {
			return usuario.getNombreUsuario();
		}

		return "anonimo";
	}
	
	/**
	 * Obtiene el email del usuario de Midas registrado
	 * 
	 * @param request
	 *            Request del servlet
	 * @return El email del usuario de Midas registrado
	 */
	public static String obtieneEmailUsuario(HttpServletRequest request){
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		if (usuario != null) {
			return usuario.getEmail();
		}
		return "";
	}

	public static String getNumeroPoliza(PolizaDTO polizaDTO) {
		String numeroPoliza = "";

		numeroPoliza += polizaDTO.getCodigoProducto();
		numeroPoliza += polizaDTO.getCodigoTipoPoliza();
		numeroPoliza += "-";
		numeroPoliza += llenarIzquierda(polizaDTO.getNumeroPoliza().toString(),
				"0", 6);
		numeroPoliza += "-";
		numeroPoliza += llenarIzquierda(polizaDTO.getNumeroRenovacion()
				.toString(), "0", 2);

		return numeroPoliza;
	}
	
	public static Map<String, String> generarMensajes(Set<String> errores, 
			MessageResources messageResources) {
		Map<String, String> mensajes = new HashMap<String,String>();
		for (String errorKey: errores) {
			mensajes.put(errorKey, messageResources.getMessage(errorKey));
		}
		return mensajes;
	}
	
	/**
	 * registraLogInteraccionReaseguro. Registra un objeto InterModuloErrorLogDTO usando los datos que se reciben como par�metros.
	 * @param String metodoCapturaError. El m�todo desde el cual se registra el error.
	 * @param Short moduloOrigenError. 1: Da�os, 2: Reaseguro, 3: Siniestros. Corresponde al m�dulo que est� capturando el error.
	 * @param String objetoOrigenError. Nombre del objeto/clase en que se origina el error.
	 * @param String metodoOrigenError. Nombre del m�todo donde se origina el error.
	 * @param String excepcion. Nombre y/o descripci�n de la excepci�n.
	 * @param String descripcionError. Descripci�n t�cnica del error.
	 * @param String comentariosAdicionales.
	 * @param String objetoCapturaError.Nombre del objeto/clase que registra el error.
	 * @param Short moduloCapturaError. 1: Da�os, 2: Reaseguro, 3: Siniestros. Corresponde al m�dulo que est� capturando el error.
	 */
	public static void registraLogInteraccionReaseguro(String metodoCapturaError,Short moduloOrigenError,String objetoOrigenError,String metodoOrigenError,
			String nombreUsuario,String paramMetodoOrigenError,String retornoMetodoorigenError,Exception excepcion,
			String descripcionError,String comentariosAdicionales,String objetoCapturaError,Short moduloCapturaError){
		InterModuloErrorLogDTO log = new InterModuloErrorLogDTO();
		log.setFechaHora(new Timestamp(System.currentTimeMillis()));
		log.setNombreUsuario(validarLongitudCampo(nombreUsuario, 49));
		log.setModuloCapturaError(moduloCapturaError);
		log.setObjetoCapturaError(validarLongitudCampo(objetoCapturaError,199));
		log.setMetodoCapturaError(validarLongitudCampo(metodoCapturaError,99));
		log.setModuloOrigenError(moduloOrigenError);
		log.setObjetoOrigenError(validarLongitudCampo(objetoOrigenError,199));
		log.setMetodoOrigenError(validarLongitudCampo(metodoOrigenError,99));
		log.setParamMetodoOrigenError(validarLongitudCampo(paramMetodoOrigenError,1999));
		log.setRetornoMetodoorigenError(validarLongitudCampo(retornoMetodoorigenError,199));
		if(excepcion != null) {
			String descripcionExcepcion = "";
			descripcionExcepcion += excepcion.getClass();
			descripcionExcepcion += ". "+(excepcion.getMessage() != null ? "Mensaje: "+excepcion.getMessage() : "");
			descripcionExcepcion += ". "+(excepcion.getCause() != null ? "Causa: "+excepcion.getCause() : "");
			descripcionExcepcion += (excepcion.getStackTrace() != null ? "\nStackTrace: "+excepcion.getStackTrace() : "");
			log.setExcepcion(validarLongitudCampo(descripcionExcepcion,499));
		} else {
			log.setExcepcion("null");
		}
		log.setDescripcionError(validarLongitudCampo(descripcionError,499));
		log.setComentariosAdicionales(validarLongitudCampo(comentariosAdicionales,499));
		try {
			InterModuloErrorLogDN.getInstancia().agregar(log);
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {
		}
	}
	
	private static String validarLongitudCampo(String cadenaOriginal,int longitudMaxima){
		String cadenaNueva = "";
		if (cadenaOriginal != null){
			if(cadenaOriginal.length() > 499){
				cadenaNueva = cadenaOriginal.substring(0, 499);
			} else {
				cadenaNueva = cadenaOriginal;
			}
		}
		return cadenaNueva;
	}
	
	/**
	 * Envia un correo notificando una excepcion (Se utilizara para notificar excepciones en la interfaz con Seycos)
	 * @param conceptoCorreo Concepto que se enviara en el mail. Ej: si falla una emision de recibos de poliza, mandar
	 * 'Emision de recibos de la poliza 124'
	 */
	public static void enviaCorreoExcepcion(String conceptoCorreo) {
		enviaCorreoExcepcion(conceptoCorreo, null);
	}
	
	/**
	 * Envia un correo notificando una excepcion (Se utilizara para notificar excepciones en la interfaz con Seycos)
	 * @param conceptoCorreo Concepto que se enviara en el mail. Ej: si falla una emision de recibos de poliza, mandar
	 * 'Emision de recibos de la poliza 124'
	 * @param cadenaDetalleTecnico (Opcional) cadena con el detalle Tecnico de la excepcion (Nombre del SP, parametros) en el siguiente
	 * formato: NombreSP|parametro1=valor1,parametro2=valor2,...,parametroN=valorN
	 */
	public static void enviaCorreoExcepcion(String conceptoCorreo, String cadenaDetalleTecnico) {
		
		String detalleTecnicoCorreo = "";
		
		if (cadenaDetalleTecnico != null) {
			
			String parametros = "";
			String[] arrDetalleTecnico = cadenaDetalleTecnico.split("\\|");
			
			if (arrDetalleTecnico != null) {
				detalleTecnicoCorreo = "<br />Detalle Tecnico:<br />Nombre Procedimiento Almacenado: <br />" 
					+ arrDetalleTecnico[0] + "<br />";
				
				if (arrDetalleTecnico.length > 1) {
					parametros = "Parametros: <br />" + arrDetalleTecnico[1];
					parametros = parametros.replaceAll(",", "<br />");
				}
				
				detalleTecnicoCorreo = detalleTecnicoCorreo + parametros;
			}
		}
		
		String titulo = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "email.interfaz.excepcion.titulo") + 
			" " + conceptoCorreo;
		String contenido = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "email.interfaz.excepcion.contenido") + 
			" " + conceptoCorreo + detalleTecnicoCorreo;
	
		MailAction.enviaCorreoNotificacionAdmins(titulo, contenido);
	}

	/**
	 * Calcula la diferencia de dias entre 2 fechas y
	 * Obtiene el factor de esas fechas
	 * el factor es obtenido en base a un anio 365 dias
	 * @param Date fechaInicial
	 * @param Date fechaFinal
	 * @return double factor
	 */	
	public static BigDecimal getFactorVigencia(Date fechaInicial, Date fechaFinal) {
		BigDecimal factorVigencia = BigDecimal.ZERO;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        try{
            fechaInicial = sdf.parse(sdf.format(fechaInicial));
            fechaFinal = sdf.parse(sdf.format(fechaFinal));
        }catch(Exception e){}

        long diferencia = fechaFinal.getTime()- fechaInicial.getTime();
        
        double diferenciaEnDias = (double)diferencia / (1000 * 60 * 60 * 24);

		factorVigencia = BigDecimal.valueOf(diferenciaEnDias /365);
	  	
		return factorVigencia;
	}	
	/**
	 * Agrega meses a una fecha
	 * @param Date fecha
	 * @param int meses
	 * @return Date fechaIncrementada
	 */	
	public static Date sumaMeses(Date fecha, int meses) {
		
		Calendar fechaC = Calendar.getInstance();
		fechaC.setTime(fecha);
		
		fechaC.add(Calendar.MONTH, meses);

		return fechaC.getTime();
	}		
	//Se  sobrecarga el metodo para que el factor se obtenga dependiendo del atributo claveAjusteVigencia 	
	public static double getFactorVigencia(CotizacionDTO cotizacionDTO) {
	    	double factorVigencia =1D;
	    	Date fechaInicial = null;
	    	Date fechaFinal = null;
	    	
	    	if(cotizacionDTO!=null && cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
			cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().compareTo((short)0)!=0){

	    		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	            
	            try{
	                fechaInicial = sdf.parse(sdf.format(cotizacionDTO.getFechaInicioVigencia()));
	                fechaFinal = sdf.parse(sdf.format(cotizacionDTO.getFechaFinVigencia()));
	            }catch(Exception e){}	    		
        
        		long diferencia = fechaFinal.getTime()- fechaInicial.getTime();
        
        		double diasPorDevengar = (double)diferencia / (1000 * 60 * 60 * 24);
        		
        		factorVigencia = diasPorDevengar / 365D ;
	    
	    	}
	    		
		return factorVigencia;
	}
	
	
	
	public static double obtenerDiasEntreFechas(Date fechaInicial, Date fechaFinal){

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        try{
            fechaInicial = sdf.parse(sdf.format(fechaInicial));
            fechaFinal = sdf.parse(sdf.format(fechaFinal));
        }catch(Exception e){}

		long diferencia = fechaFinal.getTime()- fechaInicial.getTime();
		
		double dias = (double)diferencia / (1000 * 60 * 60 * 24);
		
		return dias;
		
	}
	
	/**
	 * Codifica una cadena
	 * @param cadenaOriginal cadena original
	 * @return cadena codificada
	 */
	public static String codificaCadena(String cadenaOriginal) {
		ServicioContrasenia ps=new ServicioContrasenia();
		
        try {
			return ps.encrypt(cadenaOriginal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static String obtenerNombreEIpDelServidor()
	{
		String nombreEIp="";
		String ipServidor="";
	
		//Fijar la Ip del Servidor
		try {

	         InetAddress addr = InetAddress.getLocalHost () ;
	        // Tomar la  IP 
	         byte [] ip = addr.getAddress () ;
	         ipServidor=obtenerIpServidor(ip);
	       
	        // Tomar nombre del Servidor
	         String nombreServidor = addr.getHostName () ;	        
	         LogDeMidasWeb.log(">>>>>>>>>>>>> Nombre del Servidor : " + nombreServidor , Level.ALL, null);
	         LogDeMidasWeb.log(">>>>>>>>>>>>> IP del Servidor : "  + ipServidor , Level.ALL, null);
	         nombreEIp= nombreServidor + " : " + ipServidor;
	         
	         
	     } catch ( UnknownHostException e ) {
	    	 LogDeMidasWeb.log("No es posible fijar la IP del Servidor" , Level.SEVERE, null); 
	    	 nombreEIp="";
	     }
		return nombreEIp;
	}
	
	public static String obtenerIpServidor(byte[] ip) {
		StringBuilder  ipServidor = new StringBuilder("");
		for (int x=0; x < ip.length; x++) {
	           if (x > 0) {
	        	   ipServidor.append(".");
	           }
	           ipServidor.append((ip[x]<0)?Integer.toString(ip[x]+256):Integer.toString(ip[x]));
	         }
		return ipServidor.toString();
	}

	/**
	 * (Timers) Obtiene la cantidad de milisegundos que faltan para llegar a la hora marcada como horaInicio
	 * @param horaInicio Hora a la que se desea realizar el calculo. Formato requerido : HH:mm  a 24 hrs. 
	 * @return Cantidad de milisegundos que faltan para llegar a la hora marcada como horaInicio
	 */
	public static long ajustarHoraInicio(String horaInicio) {
		
		int horaDeseada = Integer.parseInt(horaInicio.trim().substring(0, 2));
		int minutosDeseados = Integer.parseInt(horaInicio.trim().substring(3, 5));
				
		GregorianCalendar gcActual = new GregorianCalendar();
		GregorianCalendar gcDeseado = new GregorianCalendar();
		
		System.out.println(gcActual.getTime());
		
		gcDeseado.set(GregorianCalendar.HOUR_OF_DAY, horaDeseada);
		gcDeseado.set(GregorianCalendar.MINUTE, minutosDeseados);
		
		if ((gcActual.get(GregorianCalendar.HOUR_OF_DAY) > gcDeseado.get(GregorianCalendar.HOUR_OF_DAY))) {
				
			//Se le especifica que se trata del dia siguiente
			gcDeseado.add(GregorianCalendar.DATE, 1);
			
			
		} else if ((gcActual.get(GregorianCalendar.HOUR_OF_DAY) == gcDeseado.get(GregorianCalendar.HOUR_OF_DAY)) 
				&& (gcActual.get(GregorianCalendar.MINUTE) >  gcDeseado.get(GregorianCalendar.MINUTE))) {
			//Se le especifica que se trata del dia siguiente
			gcDeseado.add(GregorianCalendar.DATE, 1);
		}
		
		System.out.println(gcDeseado.getTime());
		
		
		return gcDeseado.getTimeInMillis() - gcActual.getTimeInMillis();
		
		
	}
	
	 public static boolean validarRfcPersonaFisica(String rfc){
		    rfc=rfc.toUpperCase().trim();
		    return rfc.toUpperCase().matches("[A-Z]{4}[0-9]{6}[A-Z0-9]{3}");
	}//
		 
	public static boolean validarRfcPersonaMoral(String rfc){
			    rfc=rfc.toUpperCase().trim();
			    return rfc.toUpperCase().matches("[A-Z]{3}[0-9]{6}[A-Z0-9]{3}");
	}//
	
	public static String obtenerRutaDocumentosAnexos(){
		String OSName = System.getProperty("os.name");
		String uploadFolder = null;
		if (OSName.toLowerCase().indexOf("windows")!=-1) {
			uploadFolder = Sistema.UPLOAD_FOLDER;
		} else {
			uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
		}
		return uploadFolder;
	}
	
	public static String obtenerRutaDocumentosXMLReportes(){
		String OSName = System.getProperty("os.name");
		String uploadFolder = null;
		if (OSName.toLowerCase().indexOf("windows")!=-1) {
			uploadFolder = Sistema.FOLDER_ARCHIVOS_XML_REPORTES_WINDOWS;
		} else {
			uploadFolder = Sistema.FOLDER_ARCHIVOS_XML_REPORTES_LINUX;
		}
		return uploadFolder;
	}
	
	public static String obtenerRutaDocumentosXLSReportes(){
		String OSName = System.getProperty("os.name");
		String uploadFolder = null;
		if (OSName.toLowerCase().indexOf("windows")!=-1) {
			uploadFolder = Sistema.FOLDER_ARCHIVOS_XLS_REPORTES_WINDOWS;
		} else {
			uploadFolder = Sistema.FOLDER_ARCHIVOS_XLS_REPORTES_LINUX;
		}
		return uploadFolder;
	}
	
	/**
	 * fechaIncluidaEnIntervalo 
	 * Permite evaluar validar si una fecha se encuentra dentro de un  intervalo de fechas
	 * @param fechaIni Determina la fecha de inicio del intervalo de fechas
	 * @param fechaFin Determina la fecha final del intervalo de fechas
	 * @param  fecha Determina la fecha por evaluar
	 * @return regresa un booleano - Verdadero para el caso en que la fecha este contenida en el intervalo de fechas,las fechas limite no son consideradas en el intervalo - Falso el caso contrario
	 */
	public static boolean validaFechaIncluidaEnIntervalo(Date fechaIni, Date fechaFin, Date fecha){
    	boolean respuesta = false;
    	SimpleDateFormat formatear = new SimpleDateFormat("dd/MM/yyyy");
    	LogDeMidasWeb.log("Fecha inicio de intervalo : " + formatear.format(fechaIni) , Level.INFO, null);
    	LogDeMidasWeb.log("Fecha  : " + formatear.format(fecha) , Level.INFO, null);
    	LogDeMidasWeb.log("Fecha Fin de intervalo : " + formatear.format(fechaFin) , Level.INFO, null);
	 	if (fecha.after(fechaIni) && fecha.before(fechaFin) ) {
			respuesta = true;
		}
	 	
    	return respuesta;
    }
	
	/**
	 * Valida si un conjunto de objetos BigDecimal suman 100 % usando una escala espec�fica (cantidad de decimales).
	 */
	public static BigDecimal obtenerDiferenciaPorcentual(BigDecimal[] sumandos,int escala){
		BigDecimal diferencia = new BigDecimal(100d).setScale(escala);
		if(sumandos != null){
			for(int i=0;i<sumandos.length;i++){
				if(sumandos[i] != null) {
					diferencia = diferencia.subtract(sumandos[i].setScale(escala)).setScale(escala);
				}
			}
		}
		return diferencia;
	}
	
	/**
	 * fechaIncluidaEnIntervalo 
	 * Permite evaluar validar si una fecha se encuentra dentro de un  intervalo de fechas
	 * @param fechaIni Determina la fecha de inicio del intervalo de fechas
	 * @param fechaFin Determina la fecha final del intervalo de fechas
	 * @param  fecha Determina la fecha por evaluar
	 * @return regresa un booleano - Verdadero para el caso en que la fecha este contenida en el intervalo de fechas,
	 * las fechas limite si son consideradas en el intervalo - Falso el caso contrario
	 */
	public static boolean validaFechaIncluidaEnIntervaloLimite(Date fechaIni, Date fechaFin, Date fecha){
    	boolean respuesta = false;
    	SimpleDateFormat formatear = new SimpleDateFormat("dd/MM/yyyy");
    	LogDeMidasWeb.log("Fecha inicio de intervalo : " + formatear.format(fechaIni) , Level.INFO, null);
    	LogDeMidasWeb.log("Fecha  : " + formatear.format(fecha) , Level.INFO, null);
    	LogDeMidasWeb.log("Fecha Fin de intervalo : " + formatear.format(fechaFin) , Level.INFO, null);
		if ((fecha.after(fechaIni) && fecha.before(fechaFin))
				|| (fecha.compareTo(fechaIni) == 0 || fecha.compareTo(fechaFin) == 0)) {
			respuesta = true;
		}

	 	return respuesta;
    }	
	 public static Date  fijaFecha(String fecha){
		 Date fecharegreso=null;
		 GregorianCalendar c=null;
		 String[] resultado=null;
		 
		 try {
    	 resultado = fecha.split("-");
   		 c = new GregorianCalendar(new Integer(resultado[0]),new Integer(resultado[1])-1, new Integer(resultado[2]));
   	   	 fecharegreso= c. getTime();
   
		 }catch (Exception e)
		 {
			 LogDeMidasWeb.log("Excepcion en  : " + UtileriasWeb.class.getName() + "El formato de fecha debe ser : aaaa-mm-dd . Se fijara la fecha Actual" , Level.ALL, null);
			 fecharegreso= new Date();    
		 }
	    return fecharegreso;
	}
	 
	 /**
	  * Codifica un String a formato "ISO-8859-1" para mantener acentos y caracteres especiales.
	  * @param String cadenaISO.
	  * @return Cadena codificada.
	 * @throws UnsupportedEncodingException 
	  */
	 public static String codificarCadenaISO_8859_1(String cadenaISO) throws UnsupportedEncodingException{
		 String resultado = null;
		 if(cadenaISO != null) {
			resultado = new String(cadenaISO.getBytes("ISO-8859-1"));
		}
		 return resultado;
	 }
	 
	 public static Integer calculaTipoEndoso(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		 Integer tipoEndoso = new Integer(2);
		 if(cotizacionDTO != null && cotizacionDTO.getSolicitudDTO() != null){
			 if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
				 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_CANCELACION);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){
				 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_REHABILITACION);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
				 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_DECLARACION){
				 tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_AUMENTO);
			 }else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION){
					MovimientoCotizacionEndosoDTO dto = new MovimientoCotizacionEndosoDTO();
					dto.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					BigDecimal primaNetaCotizacion = BigDecimal.ZERO;
					List<MovimientoCotizacionEndosoDTO> movimientos = MovimientoCotizacionEndosoDN
							.getInstancia(cotizacionDTO.getCodigoUsuarioModificacion()).listarFiltrado(dto);
					for (MovimientoCotizacionEndosoDTO movimiento : movimientos) {
						if (movimiento.getIdToCobertura() != null && movimiento.getIdToCobertura().intValue() >0) {
							primaNetaCotizacion = primaNetaCotizacion.add(movimiento.getValorDiferenciaPrimaNeta());
						}
					}
					if(primaNetaCotizacion.doubleValue() == 0){
						tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_CAMBIO_DATOS);
					}else if(primaNetaCotizacion.doubleValue() > 0){
						tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_AUMENTO);
					}else if(primaNetaCotizacion.doubleValue() < 0){
						tipoEndoso = Integer.valueOf(Sistema.TIPO_ENDOSO_DISMINUCION);
					}
			 }
		 }else if(cotizacionDTO != null && cotizacionDTO.getIdToCotizacion() != null){
			 cotizacionDTO = CotizacionDN.getInstancia("SISTEMA").getPorId(cotizacionDTO.getIdToCotizacion());
			 tipoEndoso = calculaTipoEndoso(cotizacionDTO);
		 }
		 
		 return tipoEndoso;
	 }
	 
	 public static String obtenerDescripcionMoneda(int idMoneda){
		 String moneda = null;
		 if(idMoneda == Sistema.MONEDA_DOLARES) {
			moneda = "DOLARES";
		} else if((idMoneda == Sistema.MONEDA_PESOS)) {
			moneda = "PESOS";
		} else {
			moneda = "";
		}
		 
		 return moneda;
	 }

	/**
	 * Agrega dias a una fecha
	 * 
	 * @param Date
	 *            fecha
	 * @param int dias
	 * @return Date fechaIncrementada
	 */
	public static Date sumaDias(Date fecha, int dias) {

		Calendar fechaC = Calendar.getInstance();
		fechaC.setTime(fecha);
		fechaC.set(GregorianCalendar.HOUR_OF_DAY, 0);
		fechaC.set(GregorianCalendar.MINUTE, 0);
		fechaC.set(GregorianCalendar.SECOND, 0);
		fechaC.set(GregorianCalendar.MILLISECOND, 0);		

		fechaC.add(Calendar.DATE, dias);

		return fechaC.getTime();
	}
	 
	 /**
	  * Regresa la descripcion de las constantes de estatus usadas en la clase Sistema: 
	  * Sistema.AUTORIZACION_REQUERIDA, Sistema.AUTORIZACION_NO_REQUERIDA, Sistema.AUTORIZADA y idEstatus == Sistema.RECHAZADA 
	  * @param idEstatus
	  * @return
	  */
	 public static String obtenerDescripcionEstatus(short idEstatus){
		 String estatus = "";
		 if(idEstatus == Sistema.AUTORIZACION_REQUERIDA) {
			estatus = "Autorizacion requerida";
		} else if((idEstatus == Sistema.AUTORIZACION_NO_REQUERIDA)) {
			estatus = "Autorizacion no requerida";
		} else if (idEstatus == Sistema.AUTORIZADA) {
			estatus = "Autorizado";
		} else if(idEstatus == Sistema.RECHAZADA) {
			estatus = "Rechazado";
		}
		 
		 return estatus;
	 }
	 /**
	  * Indica si es un arreglo vacio
	  * @param arr
	  * @return
	  */
	 public static  boolean isEmptyArray(Object[] arr){
		 return (arr==null || (arr.length==0));
	 }
	 /**
	  * Indica si una lista esta vacia
	  * @param list
	  * @return
	  */
	 public static boolean isEmptyList(List list){
		 if(list!=null && !list.isEmpty()){
			 for(Object element:list){
				 if(element!=null){
					 return false;
				 }
			 }
		 }
		 return true;
	 }
	 /**
	  * Convierte una lista en un arreglo
	  * @param list
	  * @param array
	  * @return
	  */
	 public static Object[] parseToArray(List list){
		 Object[] array=new Object[0];
		 if(list!=null && !list.isEmpty()){
			 array=list.toArray(array);
		 }
		 return array;
		 
	 }
	 /**
	  * Convierte un arreglo en una lista
	  * @param array
	  * @return
	  */
	 public static List parseToList(Object[] array){
		 List list=new ArrayList();
		 if(array!=null && array.length>0){
			 for(Object element:array){
				 list.add(element);
			 }
		 }
		 return list;
	 }
	 /**
	  * Metodo para enviar al response un objeto JSON
	  * @param response
	  * @param object
	  * @throws JSONException
	  * @throws IOException
	  * @throws SystemException
	  */
	 public static void sendJSON(HttpServletResponse response,Object object) throws JSONException, IOException, SystemException{
		 String json="";
		 if(response!=null && object!=null){
			 json=JSONUtil.serialize(object);
			 PrintWriter writer=response.getWriter();
			 response.setContentType("text/json");
			 writer.write(json);
			 writer.flush();
			 writer.close();
		 }else{
			 throw new SystemException("Error al serealizar objeto a JSON, el objeto es nulo");
		 }
	 }
	 
	 /*
	  * Metodo que codifica acentos-ñ's y los caracteres de latin basico de unicode para IE y lo ignora en Firefox
	  * @param descripcion cadena a codificar
	  * @return cadena codificada
	  */
	 public static String parseEncodingISO(String descripcion){
		 if(descripcion != null){
			//String patron2 = "^[\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00D1\u00F1\u00BF\u00A1\u0020-\u007D]+$";
			//boolean valido2 = descripcion.matches(patron2);
			String pattern1 = "^[\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00D1\u00F1\u00BF\u00A1]+$";
			Pattern p = Pattern.compile(pattern1, Pattern.MULTILINE);
			boolean valido2 = p.matcher(descripcion).find();
			
			if(!valido2){
				try {
					descripcion = new String(descripcion.getBytes("ISO-8859-1"), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		 }
		return descripcion;
	 }
	 
	/**
	 * Metodo para retornar valor parametrizado en caso de que el objeto sea null
	 * @param parametro
	 * @param retorno
	 * @return
	 */
	public static Object regresaObjectEsNuloParametro(Object parametro, Object retorno){
		if(parametro==null)
			return retorno;
		else
			return parametro;
	}
	
	public static String getExtension(String fileName){
		return (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
	}
}