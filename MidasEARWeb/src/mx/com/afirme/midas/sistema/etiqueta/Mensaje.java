/**
 * 
 */
package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.bean.MessageTag;


public class Mensaje extends MessageTag {
	
	/**
	 * @author Christian Ceballos
	 * @since 22 de Junio de 2009
	 * @category Jsp tags
	 *
	 */
	private static final long serialVersionUID = -8173667388419857661L;
	
	private String clave;

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
		setKey(clave);
	}
	
}
