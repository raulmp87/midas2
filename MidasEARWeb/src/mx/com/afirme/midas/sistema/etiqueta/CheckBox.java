package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.CheckboxTag;


public class CheckBox extends CheckboxTag {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3087460211985693221L;

	private String propiedadFormulario;
	
	private String valorEstablecido;
	
	private boolean deshabilitado;
	private String onClick;
	private String id;

	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}

	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		setProperty(propiedadFormulario);
	}

	public String getValorEstablecido() {
		return valorEstablecido;
	}

	public void setValorEstablecido(String valorEstablecido) {
		this.valorEstablecido = valorEstablecido;
		setValue(valorEstablecido);
	}

	public void setDeshabilitado(boolean deshabilitado) {
		this.deshabilitado = deshabilitado;
		super.setDisabled(deshabilitado);
	}

	public boolean isDeshabilitado() {
		return deshabilitado;
	}
	
	public String getOnClick() {
		return onClick;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
		super.setOnclick(onClick);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		super.setStyleId(id);
	}
}
