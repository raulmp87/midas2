package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.bean.WriteTag;

public class Escribe extends WriteTag{

	/**
	 * @author Christian Ceballos
	 * @since 8 de Julio de 2009
	 */
	private static final long serialVersionUID = -6087881255952348305L;
	
	
	private String propiedad;
	private String nombre;
	private String formato;
	private String local;


	public String getLocal() {
		return local;
	}


	public void setLocal(String local) {
		this.local = local;
		setLocale(local);
	}


	public String getFormato() {
		return formato;
	}


	public void setFormato(String formato) {
		this.formato = formato;
		setFormat(formato);
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
		setName(nombre);
	}


	public String getPropiedad() {
		return propiedad;
	}


	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
		setProperty(propiedad);
	}
}
