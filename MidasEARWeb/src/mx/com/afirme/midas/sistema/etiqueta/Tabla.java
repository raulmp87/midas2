package mx.com.afirme.midas.sistema.etiqueta;

import mx.com.afirme.midas.sistema.Sistema;

import org.displaytag.exception.InvalidTagAttributeValueException;
import org.displaytag.tags.TableTag;

public class Tabla extends TableTag {

	/**
	 * @author Christian Ceballos
	 * @since 22 de Junio de 2009
	 * @category Jsp tags
	 * Despliega un conjunto de datos utilizando
	 * DisplayTag
	 * 
	 */
	private static final long serialVersionUID = -3143291870476235357L;

	private String idTabla;

	private String nombreLista;
	
	private String claseDecoradora;
	
	private String claseCss;
	
	private String urlAccion;
	
	private String exportar;
	
	private String totalRegistros;
		
	
	public String getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(String totalRegistros) {
		if (totalRegistros != null) {
			this.totalRegistros = totalRegistros;
			setPartialList(true);
			setSize(Integer.parseInt(this.totalRegistros));
		}
	}

	public String getExportar() {
		return exportar;
	}

	public void setExportar(String exportar) {
		this.exportar = exportar;
		if (exportar != null)
			setExport(Boolean.valueOf(exportar).booleanValue());
		else
			setExport(false);
	}

	public String getUrlAccion() {
		return urlAccion;
	}

	public void setUrlAccion(String urlAccion) throws InvalidTagAttributeValueException {
		if (urlAccion != null){
			this.urlAccion = urlAccion;
			setRequestURI(urlAccion);
			setExcludedParams("*");
			setSort("page");
			setExport(true);
			setDefaultsort(1);
			setDefaultorder("ascending");
			setCellpadding("0");
			setPagesize(Integer.parseInt(Sistema.REGISTROS_PAGINADOS));
			setCellspacing("0");
			//cellpadding="0" cellspacing="0"
			
			
		}
	}

	public String getClaseCss() {
		return claseCss;
	}

	public void setClaseCss(String claseCss) {
		this.claseCss = claseCss;
		setClass(claseCss);
	}

	public String getIdTabla() {
		return this.idTabla;
	}

	public void setIdTabla(String idTabla) {
		this.idTabla = idTabla;
		setId(idTabla);
	}

	public String getNombreLista() {
		return this.nombreLista;
	}

	public void setNombreLista(String nombreLista) {
		this.nombreLista = nombreLista;
		setName(nombreLista);
	}

	public void setClaseDecoradora(String claseDecoradora) {
		this.claseDecoradora = claseDecoradora;
		setDecorator(claseDecoradora);
		
	}

	public String getClaseDecoradora() {
		return claseDecoradora;
	}	

}
