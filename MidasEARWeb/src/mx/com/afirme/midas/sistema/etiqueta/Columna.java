package mx.com.afirme.midas.sistema.etiqueta;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.jsp.JspException;

import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDTO;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaId;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;
import org.displaytag.tags.ColumnTag;

public class Columna extends ColumnTag {

	/**
	 * @author Christian Ceballos
	 * @since 22 de Junio de 2009
	 * @category Jsp tags
	 * Despliega un conjunto de datos utilizando
	 * DisplayTag
	 */
	private static final long serialVersionUID = 1264010766522744930L;
	
	private String propiedad;
	
	private String titulo;
	
	private String sorteable;

	private String estilo;
	
	private String formato;
	private String maxCaracteres;
	
	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
		this.setClass(estilo);
	}	
	public String getSorteable() {
		return sorteable;
	}

	public void setSorteable(String sorteable) {
		this.sorteable = sorteable;
		if (sorteable != null)
			super.setSortable(Boolean.valueOf(sorteable).booleanValue());
		else
			super.setSortable(false);		
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
		setProperty(propiedad);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
		if (titulo.startsWith("tarifa.valor")){
			TagUtils tagUtils = TagUtils.getInstance();
			try {
				Object bean = tagUtils.lookup(this.pageContext, Constants.BEAN_KEY, null);
				if (bean != null) {
					try {
						String idRiesgo = BeanUtils.getProperty(bean, "idToRiesgo");
						String idConcepto = BeanUtils.getProperty(bean, "idConcepto");
						
						ConfiguracionTarifaDTO configuracion = null;
						ConfiguracionTarifaId id = new ConfiguracionTarifaId();
						String idBase = titulo.substring(titulo.lastIndexOf("r")+1);
						
						id.setIdDato(Short.valueOf(idBase));
						id.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(idRiesgo));
						id.setIdConcepto(UtileriasWeb.regresaBigDecimal(idConcepto));
						
						ConfiguracionTarifaFacadeRemote beanRemoto;
						ServiceLocator serviceLocator = ServiceLocator.getInstance();
						beanRemoto = serviceLocator.getEJB(ConfiguracionTarifaFacadeRemote.class);
						configuracion = beanRemoto.findById(id);
						if (configuracion != null)
							if (configuracion.getEtiqueta()!=null)	titulo=configuracion.getEtiqueta().trim();
							else	titulo="";
					} catch (SystemException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					}
				} // End of if
			} catch (JspException e) {
				e.printStackTrace();
			}
		}
		setTitle(titulo);
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
		setFormat(formato);
	}

	public String getMaxCaracteres() {
		return maxCaracteres;
	}

	public void setMaxCaracteres(String maxCaracteres) {
		super.setMaxLength(Integer.valueOf(maxCaracteres).intValue());
		this.maxCaracteres = maxCaracteres;
	}
}
