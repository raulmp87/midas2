package mx.com.afirme.midas.sistema.etiqueta;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.taglib.TagUtils;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;


public class MensajeUsuario extends TagSupport {

	/**
	 * @author Christian Ceballos
	 * @since 30/07/09
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int doStartTag() throws JspException {
		super.doStartTag();
		HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
		String path = request.getContextPath();
		if (!UtileriasWeb.existeValorSessionScope(request, Sistema.MENSAJE_USUARIO))
			return SKIP_BODY;
		String tipoMensaje = UtileriasWeb.obtenValorSessionScope(request, Sistema.MENSAJE_USUARIO).toString();
		String mensajeUsuario=""; 
		if (tipoMensaje.equals(Sistema.MENSAJE_ERROR)){
			mensajeUsuario = UtileriasWeb.obtenValorSessionScope(request, Sistema.CLAVE_ERROR).toString();
			StringBuffer buffer = new StringBuffer();
			/**
			 * 
			
			buffer.append("<div class=\"mensaje_encabezado\"></div>");
			buffer.append("<div class=\"mensaje_contenido\">");
			buffer.append("<div class=\"mensaje_img\"></div>");
			buffer.append("<div id=\"mensajeGlobal\" class=\"mensaje_texto\">");
			 */
			
			buffer.append("<img src=\"".concat(path).concat("/img/errorIcon.gif\"><br>"));
			buffer.append("<pre>");
			buffer.append(mensajeUsuario);
			buffer.append("</pre>");
			/**
			buffer.append("</div><br>");
			buffer.append("<div class=\"mensaje_botones\">");
			buffer.append("<div class=\"b_aceptar\">");
			buffer.append("<a href=\"javascript:void(0);\" onclick=\"mesajeGlobal('');\">Aceptar</a>");
			buffer.append("</div>");
			buffer.append("</div>");
			buffer.append("</div>");
			buffer.append("<div class=\"mensaje_pie\"></div>");
			**/
			TagUtils tagUtils = TagUtils.getInstance();
			tagUtils.write(this.pageContext, buffer.toString());
			UtileriasWeb.eliminaValorSessionScope(request, Sistema.CLAVE_ERROR);
		}
		UtileriasWeb.eliminaValorSessionScope(request, Sistema.MENSAJE_USUARIO);
		
		return SKIP_BODY;
	}
	
	

}
