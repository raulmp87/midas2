/**
 * Clase de soporte para Botones con seguridad de MIDAS
 */
package mx.com.afirme.midas.sistema.etiqueta;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.taglib.TagUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author andres.avalos
 *
 */
public class Boton extends TagSupport {

	
	private static final long serialVersionUID = 1L;
	
	private static final String BOTON_BUSCAR = "buscar";
	private static final String BOTON_GUARDAR = "guardar";
	private static final String BOTON_REGRESAR = "regresar";
	private static final String BOTON_AGREGAR = "agregar";
	private static final String BOTON_BORRAR = "borrar";
	private static final String BOTON_MODIFICAR = "modificar";
	private static final String BOTON_NUEVA_VERSION = "nuevaVersion";
	private static final String BOTON_CONTINUAR = "continuar";
	private static final String BOTON_SELECCIONAR = "seleccionar";
	private static final String BOTON_ASIGNAR = "asignar";
	private static final String BOTON_CONSULTAR = "consultar";
	private static final String BOTON_ACEPTAR = "aceptar";

	private String textoDefault = "";
	private String texto;
	private String tipo;
	private String onclick;
	private String style;
	private String ondblclick;
	private String key;
	
	
	@Override
	public int doStartTag() throws JspException {
		
		super.doStartTag();
		
		HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
		String claseBoton = "";
		StringBuffer buffer = new StringBuffer();
		SistemaContext sistemaContext = getSistemaContext(request);
		
		boolean tipoValido = false;
		
		String[] tipos = {
				BOTON_BUSCAR,
				BOTON_GUARDAR,
				BOTON_REGRESAR,
				BOTON_AGREGAR,
				BOTON_BORRAR,
				BOTON_MODIFICAR,
				BOTON_NUEVA_VERSION,
				BOTON_CONTINUAR,
				BOTON_SELECCIONAR,
				BOTON_ASIGNAR,
				BOTON_CONSULTAR,
				BOTON_ACEPTAR
		};
		
		for (String tipoBoton : tipos) {
			if(tipoBoton.equals(this.tipo)) {
				tipoValido = true;
				break;
			}
		}
		
		if (!tipoValido) {
			throw new JspException();
		}
		
		if (!UtileriasWeb.existeValorSessionScope(request, sistemaContext.getUsuarioAccesoMidas())) {
			return SKIP_BODY;	
		}
		
		claseBoton = "b_" + this.tipo;
		
		buffer.append("<div id=\"");
		buffer.append(claseBoton);
		buffer.append("\"");
		
		if (this.style != null) {
			buffer.append(" style=\"" + this.style + "\"");
			
		}
		buffer.append(">");
		
		
		buffer.append("<a onclick=\"");
		buffer.append(this.onclick);
		buffer.append("\" href=\"javascript: void(0);\" ");
		if(this.ondblclick != null) {
			buffer.append("ondblclick=\"" + this.ondblclick + "\" ");
		} else {
			buffer.append("ondblclick=\"" + this.onclick + "\" ");
		}
		buffer.append(">");
		if (this.key != null) {
			buffer.append(UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursosFront(), this.key));
		} else if(this.texto != null) {
			buffer.append(this.texto);
		} else {
			buffer.append(obtieneTextoDefault(sistemaContext));
		}
		buffer.append("</a>");
		buffer.append("</div>");
		
		TagUtils tagUtils = TagUtils.getInstance();
		tagUtils.write(this.pageContext, buffer.toString());
		
		return SKIP_BODY;
		
	}
	
	
	private String obtieneTextoDefault(SistemaContext sistemaContext) {
		String result = StringUtils.EMPTY;
		if (BOTON_AGREGAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.agregar");
		} else if (BOTON_ASIGNAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.asignar");
		} else if (BOTON_BORRAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.borrar");
		} else if (BOTON_BUSCAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.filtrar");
		} else if (BOTON_CONSULTAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.consultar");
		} else if (BOTON_CONTINUAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.continuar");
		} else if (BOTON_GUARDAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.guardar");
		} else if (BOTON_MODIFICAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.modificar");
		} else if (BOTON_NUEVA_VERSION.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.nuevaversion");
		} else if (BOTON_REGRESAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.regresar");
		} else if (BOTON_SELECCIONAR.equals(tipo)) {
			result = UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursos(), "midas.accion.seleccionar");
		} 
		return result;
	}


	/**
	 * @return the texto
	 */
	public String getTexto() {
		return texto;
	}




	/**
	 * @param texto the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = texto.trim();
	}




	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}




	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo.toLowerCase().trim();
	}




	/**
	 * @return the onclick
	 */
	public String getOnclick() {
		return onclick;
	}




	/**
	 * @param onclick the onclick to set
	 */
	public void setOnclick(String onclick) {
		this.onclick = onclick.trim();
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	public void setOndblclick(String ondblclick) {
		this.ondblclick = ondblclick;
	}

	public String getOndblclick() {
		return ondblclick;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	private SistemaContext getSistemaContext(HttpServletRequest request) {
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());		
		return webApplicationContext.getBean(SistemaContext.class);
	}
}
