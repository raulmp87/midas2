package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.RadioTag;

public class Radio extends RadioTag{
	
	/**
	 * @author Christian Ceballos
	 * @since 29 Junio de 2009
	 */
	private static final long serialVersionUID = -2951258858750569090L;
	private String propiedadFormulario;
	private String valorEstablecido;
	private String onClick;
	private String deshabilitado;

	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}

	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		setProperty(propiedadFormulario);
	}

	public String getValorEstablecido() {
		return valorEstablecido;
	}

	public void setValorEstablecido(String valorEstablecido) {
		this.valorEstablecido = valorEstablecido;
		setValue(valorEstablecido);
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
		super.setOnclick(onClick);
	}

	public String getOnClick() {
		return onClick;
	}
	
	public String getDeshabilitado() {
		return deshabilitado;
	}

	public void setDeshabilitado(String deshabilitado) {
		this.deshabilitado = deshabilitado;
		setDisabled(Boolean.valueOf(deshabilitado).booleanValue());
	}
}
