package mx.com.afirme.midas.sistema.etiqueta;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.combos.etiqueta.SoporteBaseEtiqueta;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDTO;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaId;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

public class Tarifa extends SoporteBaseEtiqueta {

	/**
	 * @author Jorge Cano
	 * @since 14/08/2009
	 */
	private static final long serialVersionUID = -6792001625010461620L;

	private ConfiguracionTarifaFacadeRemote beanRemoto;

	private String nombre = Constants.BEAN_KEY;

	private String idToRiesgo = null;

	private String idToConcepto = null;

	private String idBase = null;

	public Tarifa() throws SystemException {
		LogDeMidasWeb.log("Entrando en la etiqueta Tarifa  - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(ConfiguracionTarifaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public int doStartTag() throws JspException {

		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);

			if (bean != null) {
				String idRiesgo = BeanUtils.getProperty(bean, "idToRiesgo");
				String idConcepto = BeanUtils.getProperty(bean, "idConcepto");
				
				ConfiguracionTarifaDTO configuracion = null;
				ConfiguracionTarifaId id = new ConfiguracionTarifaId();
				id.setIdDato(Short.valueOf(this.idBase));
				id.setIdToRiesgo(UtileriasWeb
						.regresaBigDecimal(idRiesgo));
				id.setIdConcepto(UtileriasWeb
						.regresaBigDecimal(idConcepto));
				configuracion = beanRemoto.findById(id);

				if (configuracion != null) {
					try {
						if(configuracion.getEtiqueta() != null)	writer.write(configuracion.getEtiqueta().trim()+"*:");
						else	writer.write("");
					} catch (IOException ioException) {
						throw new SystemException(ioException);
					}
				}
			} // End of if

		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing tarifa catalog.",
					Level.FINEST, sException);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		return SKIP_BODY;
	}

	public void release() {
		super.release();
		this.nombre = Constants.BEAN_KEY;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdToRiesgo() {
		return idToRiesgo;
	}

	public void setIdToRiesgo(String idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	public String getIdToConcepto() {
		return idToConcepto;
	}

	public void setIdToConcepto(String idToConcepto) {
		this.idToConcepto = idToConcepto;
	}

	public String getIdBase() {
		return idBase;
	}

	public void setIdBase(String idBase) {
		this.idBase = idBase;
	}

}
