package mx.com.afirme.midas.sistema.etiqueta;

import mx.com.afirme.midas.sistema.Sistema;

import org.apache.struts.taglib.html.FormTag;

public class Formulario extends FormTag{

	/**
	 * @author Christian Ceballos
	 * @since 29 Junio de 2009
	 */
	private static final long serialVersionUID = -4364693503384338050L;
	
	private String accion;

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
		setAction(accion);
		setMethod(Sistema.METODO_ENVIO_FORMULARIOS);
	}
	
	
}
