package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.SelectTag;

public class ComboSimple extends SelectTag{

	/**
	 * @author Christian Ceballos
	 * @since 13 Julio de 2009
	 */
	private static final long serialVersionUID = -1243229320888752689L;

	private String id;
	
	private String nombreFormulario;
	
	private String propiedad;
	
	private String deshabilitado;
	
	private String onblur;
	
	private String onchange;
	
	private String styleClass = null;

	public String getDeshabilitado() {
		return deshabilitado;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		super.setStyleId(id);
	}
	
	public void setDeshabilitado(String deshabilitado) {
		this.deshabilitado = deshabilitado;
		if (deshabilitado == null)
			setDisabled(false);
		else
			setDisabled(Boolean.valueOf(deshabilitado).booleanValue());
	}

	public String getNombreFormulario() {
		return nombreFormulario;
	}

	public void setNombreFormulario(String nombreFormulario) {
		this.nombreFormulario = nombreFormulario;
		setName(nombreFormulario);
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
		setProperty(propiedad);
	}
	
	public String getOnblur() {
		return onblur;
	}

	public void setOnblur(String onblur) {
		this.onblur = onblur;
		super.setOnblur(onblur);
	}
	
	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
		super.setOnchange(onchange);
	}
	
	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
}
