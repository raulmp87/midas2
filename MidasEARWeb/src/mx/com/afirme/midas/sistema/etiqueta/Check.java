package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.CheckboxTag;

public class Check extends CheckboxTag {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombreFormulario;
	private String deshabilitado;
	private String id;
	private String onClick;
	private String propiedadFormulario;	
	private String valorEstablecido;

	public String getNombreFormulario() {
		return nombreFormulario;
	}

	public void setNombreFormulario(String nombreFormulario) {
		this.nombreFormulario = nombreFormulario;
		super.setName(nombreFormulario);
	}

	public String getDeshabilitado() {
		return deshabilitado;
	}

	public void setDeshabilitado(String deshabilitado) {
		this.deshabilitado = deshabilitado;
		super.setDisabled(Boolean.valueOf(deshabilitado));
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		super.setId(id);
	}

	public String getOnClick() {
		return onClick;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
		super.setOnclick(onClick);
	}

	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}

	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		super.setProperty(propiedadFormulario);
	}

	public String getValorEstablecido() {
		return valorEstablecido;
	}

	public void setValorEstablecido(String valorEstablecido) {
		this.valorEstablecido = valorEstablecido;
		super.setValue(valorEstablecido);
	}

}
