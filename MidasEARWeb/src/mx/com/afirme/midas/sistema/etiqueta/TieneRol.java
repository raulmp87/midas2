package mx.com.afirme.midas.sistema.etiqueta;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.util.WebUtils;

import mx.com.afirme.midas2.service.seguridad.UsuarioService;

/**
 * Etiqueta que evalua el cuerpo de la etiqueta siempre y cuando el usuario actual tiene un rol dado.
 * @author amosomar
 */
public class TieneRol extends TagSupport {
	
	private static final long serialVersionUID = -4957386991019036492L;

	private static final String DELIMITADOR = ",";
	
	private String nombre;

	@SuppressWarnings("unused")
	private UsuarioService usuarioService;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public int doStartTag() throws JspException {
		//FIXME:Esto se tuvo que hacer en lugar de inyeccion de dependencias debido a que usuarioService no se esta inicializando al publicar
		//la aplicacion en RAD. Cuando se pueda hacer funcionar solo cambiar a que se utilice usuarioService y quitar 
		//esta llamada para obtene usuarioService2.
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());
		UsuarioService usuarioService2 = webApplicationContext.getBean(UsuarioService.class);
		
		String[] nombres = nombre.split(DELIMITADOR); 
		
		if (usuarioService2.tieneRolUsuarioActual(nombres)) {
			return EVAL_BODY_INCLUDE;
		}
		return SKIP_BODY;
	}

}
