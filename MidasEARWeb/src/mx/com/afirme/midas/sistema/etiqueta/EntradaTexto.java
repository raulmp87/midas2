package mx.com.afirme.midas.sistema.etiqueta;

import mx.com.afirme.midas.sistema.Sistema;

import org.apache.struts.taglib.html.TextTag;

public class EntradaTexto extends TextTag {

	/**
	 * @author Christian Ceballos
	 * @since 29 Junio de 2009
	 * @category Input tags Crea una caja de texto para la introduccion de datos
	 */
	private static final long serialVersionUID = -3598755702712874195L;

	private String propiedadFormulario;

	private String caracteres;

	private String nombreFormulario;

	private String deshabilitado;
	
	private String soloLectura;

	private String longitud;

	private String onchange;
	
	private String id;
	
	private String onkeypress;
	
	private String onblur;

	private String onfocus;
	
	private String onkeydown;
	
	
	public String getOnfocus() {
		return onfocus;
	}

	public void setOnfocus(String onfocus) {
		this.onfocus = onfocus;
		super.setOnfocus(onfocus);
	}

	public String getOnkeydown() {
		return onkeydown;
	}

	public void setOnkeydown(String onkeydown) {
		this.onkeydown = onkeydown;
		super.setOnkeydown(onkeydown);
		
	}
	
	public String getOnblur() {
		return onblur;
	}

	public void setOnblur(String onblur) {
		this.onblur = onblur;
		super.setOnblur(onblur);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		super.setStyleId(id);
	}

	public String getOnkeypress() {
		return onkeypress;
	}

	public void setOnkeypress(String onkeypress) {
		this.onkeypress = onkeypress;
		super.setOnkeypress(onkeypress);
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
		super.setOnchange(onchange);
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
		setSize(longitud);
	}

	public String getDeshabilitado() {
		return deshabilitado;
	}

	public void setDeshabilitado(String deshabilitado) {
		this.deshabilitado = deshabilitado;
		setDisabled(Boolean.valueOf(deshabilitado).booleanValue());
	}

	public String getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(String soloLectura) {
		this.soloLectura = soloLectura;
		setReadonly(Boolean.valueOf(soloLectura).booleanValue());
	}

	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}

	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		setProperty(propiedadFormulario);
		setStyleClass(Sistema.CLASE_CSS_CAJA_TEXTO);
		setStyle("text-transform: none;");
	}

	public String getCaracteres() {
		return caracteres;
	}

	public void setCaracteres(String caracteres) {
		this.caracteres = caracteres;
		setMaxlength(caracteres);
	}

	public String getNombreFormulario() {
		return nombreFormulario;
	}

	public void setNombreFormulario(String nombreFormulario) {
		this.nombreFormulario = nombreFormulario;
		setName(nombreFormulario);
	}

}
