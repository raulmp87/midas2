package mx.com.afirme.midas.sistema.etiqueta;

import mx.com.afirme.midas.sistema.Sistema;

import org.apache.struts.taglib.html.TextareaTag;

public class AreaTexto extends TextareaTag{
	private static final long serialVersionUID = 1L;
	private String nombreFormulario;
	private String columnas;
	private String deshabilitado;
	private String id;
	private String caracteres;
	private String onBlur;
	private String onChange;
	private String onClick;
	private String onFocus;
	private String onKeyDown;
	private String propiedadFormulario;
	private String renglones;
	private String onkeypress;
	
	
	/**
	 * @return the nombreFormulario
	 */
	public String getNombreFormulario() {
		return nombreFormulario;
	}


	/**
	 * @param nombreFormulario the nombreFormulario to set
	 */
	public void setNombreFormulario(String nombreFormulario) {
		this.nombreFormulario = nombreFormulario;
		super.setName(nombreFormulario);
	}


	/**
	 * @return the columnas
	 */
	public String getColumnas() {
		return columnas;
	}


	/**
	 * @param columnas the columnas to set
	 */
	public void setColumnas(String columnas) {
		this.columnas = columnas;
		super.setCols(columnas);
	}


	/**
	 * @return the deshabilitado
	 */
	public String getDeshabilitado() {
		return deshabilitado;
	}


	/**
	 * @param deshabilitado the deshabilitado to set
	 */
	public void setDeshabilitado(String deshabilitado) {
		this.deshabilitado = deshabilitado;
		super.setDisabled(Boolean.valueOf(deshabilitado));
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
		super.setStyleId(id);
	}


	/**
	 * @return the caracteres
	 */
	public String getCaracteres() {
		return caracteres;
	}


	/**
	 * @param caracteres the caracteres to set
	 */
	public void setCaracteres(String caracteres) {
		this.caracteres = caracteres;
		super.setMaxlength(caracteres);
	}


	/**
	 * @return the onBlur
	 */
	public String getOnBlur() {
		return onBlur;
	}


	/**
	 * @param onBlur the onBlur to set
	 */
	public void setOnBlur(String onBlur) {
		this.onBlur = onBlur;
		super.setOnblur(onBlur);
	}


	/**
	 * @return the onChange
	 */
	public String getOnChange() {
		return onChange;
	}


	/**
	 * @param onChange the onChange to set
	 */
	public void setOnChange(String onChange) {
		this.onChange = onChange;
		super.setOnchange(onChange);
	}


	/**
	 * @return the onClick
	 */
	public String getOnClick() {
		return onClick;
	}


	/**
	 * @param onClick the onClick to set
	 */
	public void setOnClick(String onClick) {
		this.onClick = onClick;
		super.setOnclick(onClick);
	}


	/**
	 * @return the onFocus
	 */
	public String getOnFocus() {
		return onFocus;
	}


	/**
	 * @param onFocus the onFocus to set
	 */
	public void setOnFocus(String onFocus) {
		this.onFocus = onFocus;
		super.setOnfocus(onFocus);
	}


	/**
	 * @return the onKeyDown
	 */
	public String getOnKeyDown() {
		return onKeyDown;
	}


	/**
	 * @param onKeyDown the onKeyDown to set
	 */
	public void setOnKeyDown(String onKeyDown) {		
		this.onKeyDown = onKeyDown;
		super.setOnkeydown(onKeyDown);
	}


	/**
	 * @return the propiedadFormulario
	 */
	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}


	/**
	 * @param propiedadFormulario the propiedadFormulario to set
	 */
	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		super.setProperty(propiedadFormulario);
		super.setStyleClass(Sistema.CLASE_CSS_CAJA_TEXTO);
		super.setStyle("text-transform: none;");
	}


	/**
	 * @return the renglones
	 */
	public String getRenglones() {
		return renglones;
	}


	/**
	 * @param renglones the renglones to set
	 */
	public void setRenglones(String renglones) {
		this.renglones = renglones;
		super.setRows(renglones);
	}
	
	
	public String getOnkeypress() {
		return onkeypress;
	}

	public void setOnkeypress(String onkeypress) {
		this.onkeypress = onkeypress;
		super.setOnkeypress(onkeypress);
	}
}
