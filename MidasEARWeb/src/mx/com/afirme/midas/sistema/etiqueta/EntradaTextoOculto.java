package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.HiddenTag;

public class EntradaTextoOculto extends HiddenTag{

	/**
	 * @author Christian Ceballos
	 * @since 29 Junio de 2009
	 * @category Input tags
	 * Crea una caja de texto oculta para almacenar datos
	 */
	private static final long serialVersionUID = -6210893827614006533L;
	
	private String propiedadFormulario;
	
	private String nombreFormulario;

	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}

	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		setProperty(propiedadFormulario);
	}

	public String getNombreFormulario() {
		return nombreFormulario;
	}

	public void setNombreFormulario(String nombreFormulario) {
		this.nombreFormulario = nombreFormulario;
		setName(nombreFormulario);
	}
	
}