package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.OptionTag;

public class OpcionComboSimple extends OptionTag{

	/**
	 * @author Christian Ceballos
	 * @since 13 de Julio de 2009
	 */
	private static final long serialVersionUID = 3333663930724440067L;
	
	private String valor;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
		setValue(valor);
	}
	
	

}
