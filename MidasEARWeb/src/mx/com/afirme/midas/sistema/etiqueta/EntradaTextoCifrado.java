package mx.com.afirme.midas.sistema.etiqueta;

import org.apache.struts.taglib.html.PasswordTag;

public class EntradaTextoCifrado extends PasswordTag{

	/**
	 * @author Christian Ceballos
	 * @since 29 Junio de 2009
	 * @category Input tags
	 * Crea una caja de texto para la introduccion de datos
	 * mostrando un * al usuario por cada caracter introducido
	 */
	private static final long serialVersionUID = 2798285794140855516L;
	
	private String propiedadFormulario;
	
	private String caracteres;
	
	private String nombreFormulario;
	
	private String deshabilitado;
	
	private String longitud;
	
	private String estilo;
	
	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
		//setProperty(estilo);
		setStyleClass(estilo);
		setStyle("text-transform: none;");
	}

	public String getlongitud() {
		return longitud;
	}

	public void setlongitud(String longitud) {
		this.longitud = longitud;
		setSize(longitud);
	}

	public String getDeshabilitado() {
		return deshabilitado;
	}

	public void setDeshabilitado(String deshabilitado) {
		this.deshabilitado = deshabilitado;
		setDisabled(Boolean.valueOf(deshabilitado).booleanValue());
	}
	
	public String getPropiedadFormulario() {
		return propiedadFormulario;
	}

	public void setPropiedadFormulario(String propiedadFormulario) {
		this.propiedadFormulario = propiedadFormulario;
		setProperty(propiedadFormulario);
	}

	public String getcaracteres() {
		return caracteres;
	}

	public void setcaracteres(String caracteres) {
		this.caracteres = caracteres;
		setMaxlength(caracteres);
	}

	public String getNombreFormulario() {
		return nombreFormulario;
	}

	public void setNombreFormulario(String nombreFormulario) {
		this.nombreFormulario = nombreFormulario;
		setName(nombreFormulario);
	}
	
}
