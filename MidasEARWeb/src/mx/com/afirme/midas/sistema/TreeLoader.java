/**
 * 
 */
package mx.com.afirme.midas.sistema;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoAction;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoAction;
import mx.com.afirme.midas.cotizacion.CotizacionAction;
import mx.com.afirme.midas.producto.ProductoAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author andres.avalos
 *
 */
public class TreeLoader {

	private String configuracionProducto = "configuracionProducto";
	private String configuracionFacultativa = "configuracionFacultativa";
	private String estructuraCotizacion = "estructuraCotizacion";
	private String estructuraODT = "estructuraODT";
	
	
	private ActionMapping mapping = null; 
	private ActionForm form = null;
	private HttpServletRequest request = null; 
	private HttpServletResponse response = null;
	
	private mx.com.afirme.midas.producto.ProductoAction productoAction;
	private mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaAction tipoPolizaAction;
	private mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionAction seccionAction;
	private mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaAction coberturaAction;
	private mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaAction riesgoCoberturaAction;
	private mx.com.afirme.midas.contratofacultativo.ContratoFacultativoAction contratoFacultativoAction;
	private mx.com.afirme.midas.cotizacion.CotizacionAction cotizacionAction;
	
	
	
	private int numNiveles = 0;
	private int selectEnabled = 0;
	private String root = null;
	private String descripcionRoot = null;
	private String idPadre = "0";
	private String nivelPadre = null;
	private String nivelHijo = null;
	private String nivel1 = null;
	private String nivel2 = null;
	private String nivel3 = null;
	private String nivel4 = null;
	private String nivel5 = null;
	private String imagenNoHijos = null;
	private String imagenNodoAbierto = null;
	private String imagenNodoCerrado = null;
	//El valor de esta variable se usara como prefijo para encontrar las imagenes
	//el prefijo se aplicara de la siguiente manera: prefijo-imagen
	private String iconColor = null;	
	private String separatorIconColor = new String("-");
	private boolean clickNodoRaizAlCargar = false;
	
	private List<String> niveles = new ArrayList<String>();
	
	/**
	 * Constructor de la clase TreeLoader
	 * @param menuContexto contexto para saber cual arbol debe cargar
	 * @param mapping Objeto mapping de la accion que instancio al arbol
	 * @param form Objeto form de la accion que instancio al arbol
	 * @param request Objeto request de la accion que instancio al arbol
	 * @param response Objeto response de la accion que instancio al arbol 
	 */
	public TreeLoader(String menuContexto, ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		this.mapping = mapping;
		this.form = form;
		this.request = request;
		this.response = response;

		if (menuContexto.equals(configuracionProducto)) {
			this.numNiveles = 6;
			this.selectEnabled = 0;
			this.root = "root";
			this.descripcionRoot = "Productos Afirme";
			this.nivel1 = "producto";
			this.nivel2 = "tipopoliza";
			this.nivel3 = "seccion";
			this.nivel4 = "cobertura";
			this.nivel5 = "riesgo";
			
			this.imagenNoHijos = "iconText.gif";
			this.imagenNodoAbierto ="folderOpen.gif";
			this.imagenNodoCerrado = "folderClosed.gif";
			this.clickNodoRaizAlCargar = true;
			
			this.niveles.add(root);
			this.niveles.add(nivel1);
			this.niveles.add(nivel2);
			this.niveles.add(nivel3);
			this.niveles.add(nivel4);
			this.niveles.add(nivel5);
		}
		else if (menuContexto.equals(configuracionFacultativa)) {//   Para otros contextos
			this.numNiveles = 5;
			this.selectEnabled = 0;
			this.root = "root";
			this.descripcionRoot = "Cotizaciones";
			this.nivel1 = "cotizacion";
			this.nivel2 = "slip";
			this.nivel3 = "cotizafacultativa";
			this.nivel4 = "cobertura";
 			
			this.imagenNoHijos = "iconText.gif";
			this.imagenNodoAbierto ="folderOpen.gif";
			this.imagenNodoCerrado = "folderClosed.gif";
			this.clickNodoRaizAlCargar = true;
						
			this.niveles.add(root);
			this.niveles.add(nivel1);
			this.niveles.add(nivel2);
			this.niveles.add(nivel3);
			this.niveles.add(nivel4);
		}
		else if (menuContexto.equals(estructuraCotizacion)) {//   Para cotizacion
			this.numNiveles = 5;
			this.selectEnabled = 0;
			this.root = "root";
			this.descripcionRoot = "Cotizacion";
			this.nivel1 = "inciso";
			this.nivel2 = "seccion";
			this.nivel3 = "cobertura";
			this.nivel4 = "riesgo";
 			
			this.imagenNoHijos = "iconText.gif";
			this.imagenNodoAbierto ="folderOpen.gif";
			this.imagenNodoCerrado = "folderClosed.gif";
			this.clickNodoRaizAlCargar = false;
						
			this.niveles.add(root);
			this.niveles.add(nivel1);
			this.niveles.add(nivel2);
			this.niveles.add(nivel3);
			this.niveles.add(nivel4);
		}
		else if (menuContexto.equals(estructuraODT)) {//   Para Orden de Trabajo
			this.numNiveles = 4;
			this.selectEnabled = 0;
			this.root = "root";
			this.descripcionRoot = "Cotizacion";
			this.nivel1 = "inciso";
			this.nivel2 = "seccion";
			this.nivel3 = "cobertura";
			
			this.imagenNoHijos = "iconText.gif";
			this.imagenNodoAbierto ="folderOpen.gif";
			this.imagenNodoCerrado = "folderClosed.gif";
			this.clickNodoRaizAlCargar = false;
						
			this.niveles.add(root);
			this.niveles.add(nivel1);
			this.niveles.add(nivel2);
			this.niveles.add(nivel3);
		}
		
		
		
//		else if () {   Para otros contextos
//			
//			
//		}
			
	}
	
private void poblarRama(String contextoMenu) {
		
		if (!this.idPadre.equals("0")) {
			this.request.setAttribute("id", this.idPadre);
		}
		
		this.request.setAttribute("nivel", this.nivelHijo);
		
		
		ejecutaAction(this.nivelHijo,contextoMenu);
	
	}
	
	
private void ejecutaAction(String nivel,String contextoMenu) {
	//Aqui deben de ir todas las posibles acciones, aun si no son del mismo contexto
	try {
	
	if (contextoMenu.equals(configuracionProducto)){
		if (nivel.equals("producto")) {
			productoAction = new ProductoAction();
			productoAction.listarPorPradre(mapping, form, request, response);
		} else if (nivel.equals("tipopoliza")) {
			tipoPolizaAction = new TipoPolizaAction();
			tipoPolizaAction.listarPorPradre(mapping, form, request, response);
		} else if (nivel.equals("seccion")) {
			seccionAction = new SeccionAction();
			seccionAction.listarPorPradre(mapping, form, request, response);
		} else if (nivel.equals("cobertura")){
			coberturaAction = new CoberturaAction();
			coberturaAction.listarPorPradre(mapping, form, request, response);
		} else if (nivel.equals("riesgo")) {
			riesgoCoberturaAction = new RiesgoCoberturaAction();
			riesgoCoberturaAction.listarPorPradre(mapping, form, request, response);
		}
	 } else if(contextoMenu.equals(configuracionFacultativa)){	
		 	contratoFacultativoAction = new ContratoFacultativoAction();
		 if(nivel.equals("cotizacion")){
		 	contratoFacultativoAction.listarSlipCotizacion(mapping, form, request, response);
		 }else if (nivel.equals("slip")){
		   contratoFacultativoAction.listarDetalleCotizacion(mapping, form, request, response);
		 }else if (nivel.equals("cotizafacultativa")){
		   contratoFacultativoAction.listarCoberturasCotizacion(mapping, form, request, response);
	 	 }else if (nivel.equals("cobertura")){
			 DetalleContratoFacultativoAction detalleContratoFacultativoAction = new DetalleContratoFacultativoAction();
			 detalleContratoFacultativoAction.configurarCobertura(mapping, form, request, response);
	 	 }
	} else if(contextoMenu.equals(estructuraCotizacion) || contextoMenu.equals(estructuraODT)){
   		cotizacionAction = new CotizacionAction();
   		cotizacionAction.cargarArbolCotizacionPorNivel(mapping, form, request, response);
   	}
	
	
	}catch (SystemException e) {
		e.printStackTrace();
	}
	
}


	
	public StringBuffer escribirItem (StringBuffer buffer, String nombre, String idEntidad, String idEntidadPadre, Integer numHijos) {
		
		String entidadNivel;
		
		if (this.request.getAttribute("nivel") != null) {
			entidadNivel = (String) this.request.getAttribute("nivel");
		}
		else {
			entidadNivel = this.nivel1; //default
		}
				
		buffer.append("<item ");
		
		buffer.append("text=\"");
		buffer.append(nombre.replaceAll("\"", "&#34;"));
		buffer.append("\" ");
		
		buffer.append("id= \"");
		buffer.append(entidadNivel + "_"); //Se va a usar _ para separar la entidad del id de la entidad
		buffer.append(idEntidad);
		//Se va a usar () para indicar el id de la entidad padre, ya que se puede dar que la entidad tenga varios padres
		buffer.append("(");        
		buffer.append(idEntidadPadre);
		buffer.append(")");
		buffer.append("\" ");
		
		buffer.append("im0= \"" + this.imagenNoHijos + "\" ");
		buffer.append("im1= \"" + this.imagenNodoAbierto + "\" ");
		buffer.append("im2= \"" + this.imagenNodoCerrado + "\" ");
		
		if(numHijos != null && numHijos.compareTo(new Integer(0)) > 0) {
			buffer.append("child=\"1\" ");
		}
		else {
			buffer.append("child=\"0\" "); 
		}
						
		buffer.append(" />");
		
		return buffer;
		
	}
	
	public StringBuffer escribirItemCotizacion (StringBuffer buffer, String nombre, String idEntidad, String idEntidadPadre, Integer numHijos) {
				
		buffer.append("<item ");
		
		buffer.append("text=\"");
		buffer.append(nombre.replaceAll("\"", "&#34;"));
		buffer.append("\" ");
		
		buffer.append("id= \"");
		buffer.append(idEntidadPadre);
		buffer.append("_");
		buffer.append(idEntidad);
		buffer.append("\" ");
		
		buffer.append("im0= \"" + this.imagenNoHijos + "\" ");
		buffer.append("im1= \"" + this.imagenNodoAbierto + "\" ");
		buffer.append("im2= \"" + this.imagenNodoCerrado + "\" ");
		
		if(numHijos != null && numHijos.compareTo(new Integer(0)) > 0) {
			buffer.append("child=\"1\" ");
		}
		else {
			buffer.append("child=\"0\" "); 
		}
						
		buffer.append(" />");
		
		return buffer;
		
	}
	
	public StringBuffer escribirItemRojo (StringBuffer buffer, String nombre, String idEntidad, String idEntidadPadre, Integer numHijos) {
		this.iconColor = "red";
		buffer = escribirItemColor (buffer,nombre,idEntidad,idEntidadPadre,numHijos);
		return buffer;		
	}
	
	private StringBuffer escribirItemColor (StringBuffer buffer, String nombre, String idEntidad, String idEntidadPadre, Integer numHijos) {
		String backUpImagenNoHijos = this.imagenNoHijos.toString();
		String backUpImagenNodoAbierto =this.imagenNodoAbierto.toString();
		String backUpImagenNodoCerrado = this.imagenNodoCerrado.toString();
		this.imagenNoHijos = this.iconColor + separatorIconColor + this.imagenNoHijos;
		this.imagenNodoAbierto = this.iconColor + separatorIconColor + this.imagenNodoAbierto;
		this.imagenNodoCerrado = this.iconColor + separatorIconColor + this.imagenNodoCerrado;
		
		buffer = escribirItem (buffer,nombre,idEntidad,idEntidadPadre,numHijos);
		
		this.imagenNoHijos = backUpImagenNoHijos.toString();
		this.imagenNodoAbierto = backUpImagenNodoAbierto.toString();
		this.imagenNodoCerrado = backUpImagenNodoCerrado.toString();
		return buffer;		
	}
	
	private StringBuffer generarPrimerNivelTree(StringBuffer bufferItems) {
		
		StringBuffer bufferInicio = new StringBuffer();
		StringBuffer bufferFin = new StringBuffer();
		
		bufferInicio.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		bufferInicio.append("<tree id=\"0\">");
		bufferInicio.append("<item ");
		
		bufferInicio.append("text=\"");
		bufferInicio.append(this.descripcionRoot);
		bufferInicio.append("\" ");
		
		bufferInicio.append("id= \"");
		bufferInicio.append(this.root); 
		bufferInicio.append("\" ");
		
		bufferInicio.append("open=\"1\" ");
				
		bufferInicio.append("im0= \"" + this.imagenNoHijos + "\" ");
		bufferInicio.append("im1= \"" + this.imagenNodoAbierto + "\" ");
		bufferInicio.append("im2= \"" + this.imagenNodoCerrado + "\" ");
		
		if (this.clickNodoRaizAlCargar) {
			bufferInicio.append("call=\"1\" ");
		}
		
		bufferInicio.append("select=\"" + this.selectEnabled + "\"");
						
		bufferInicio.append(">");
		
		
		bufferFin.append("</item>");
		bufferFin.append("</tree>");
		
		bufferInicio.append(bufferItems.toString());
		bufferInicio.append(bufferFin.toString());
		
		return bufferInicio;
		
	}
	
	private StringBuffer generarNivelTree(StringBuffer bufferItems, String itemId) {
		
		StringBuffer bufferInicio = new StringBuffer();
		StringBuffer bufferFin = new StringBuffer();
		
		bufferInicio.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		bufferInicio.append("<tree id=\"" + itemId + "\">");
		
		bufferFin.append("</tree>");
		
		bufferInicio.append(bufferItems.toString());
		bufferInicio.append(bufferFin.toString());
		
		return bufferInicio;
		
	}
	
	public void cargaSiguienteRama(String itemId,String contextoMenu) {
		
				
		this.idPadre = "0";
		this.nivelPadre = root;
		this.nivelHijo = nivel1;
				
		if (!itemId.equals(root) && !itemId.equals("")) {
			
			nivelPadre = itemId.substring(0,itemId.lastIndexOf("_"));
			idPadre = itemId.substring(itemId.lastIndexOf("_") + 1, itemId.lastIndexOf("("));
			
		}
			
		for (int n = 0; n < this.niveles.size() - 1; n++) {
			if (!this.niveles.get(n).equals(root)) {
				if (this.niveles.get(n).equals(nivelPadre)) {
					nivelHijo = this.niveles.get(n + 1);
					break;
				}
			}
			else {
				if (this.niveles.get(n).equals(nivelPadre)) {
					this.nivelHijo = this.niveles.get(n + 1);
					break;
				}
			}
		}
		
		poblarRama(contextoMenu);

	}
	
	public void cargaSiguienteRamaCotizacion(String itemId,String contextoMenu, String idCotizacion) {
		
		
		this.idPadre = "0";
		this.nivelPadre = root;
		this.nivelHijo = nivel1;
		
		if (idCotizacion != null) {
			idPadre = idCotizacion;
		}
		
		if (!itemId.equals(root) && !itemId.equals("")) {
			
			nivelPadre = this.niveles.get(itemId.split("_").length - 1);
			idPadre = itemId;
			
		}
			
		for (int n = 0; n < this.niveles.size() - 1; n++) {
			if (!this.niveles.get(n).equals(root)) {
				if (this.niveles.get(n).equals(nivelPadre)) {
					nivelHijo = this.niveles.get(n + 1);
					break;
				}
			}
			else {
				if (this.niveles.get(n).equals(nivelPadre)) {
					this.nivelHijo = this.niveles.get(n + 1);
					break;
				}
			}
		}
		
		poblarRama(contextoMenu);

	}
		
	/**
	 * Escribe el encabezado
	 * @param buffer
	 * @param itemId
	 * @return
	 */
	public StringBuffer xmlHeader (StringBuffer buffer, String itemId) {
		String entidadNivel;
		
		if (this.request.getAttribute("nivel") != null) {
			entidadNivel = (String) this.request.getAttribute("nivel");
		}
		else {
			entidadNivel = this.nivel1; //default
		}
			
		if (entidadNivel.equals(this.nivel1)) {
			return this.generarPrimerNivelTree(buffer);
		}
		else {
			return this.generarNivelTree(buffer, itemId);
		}
	
	}
		
	

	public int getNumNiveles() {
		return numNiveles;
	}


	public void setNumNiveles(int numNiveles) {
		this.numNiveles = numNiveles;
	}


	public int getSelectEnabled() {
		return selectEnabled;
	}


	public void setSelectEnabled(int selectEnabled) {
		this.selectEnabled = selectEnabled;
	}


	public String getRoot() {
		return root;
	}


	public void setRoot(String root) {
		this.root = root;
	}


	public String getDescripcionRoot() {
		return descripcionRoot;
	}


	public void setDescripcionRoot(String descripcionRoot) {
		this.descripcionRoot = descripcionRoot;
	}


	public String getIdPadre() {
		return idPadre;
	}


	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}


	public String getNivelPadre() {
		return nivelPadre;
	}


	public void setNivelPadre(String nivelPadre) {
		this.nivelPadre = nivelPadre;
	}


	public String getNivelHijo() {
		return nivelHijo;
	}


	public void setNivelHijo(String nivelHijo) {
		this.nivelHijo = nivelHijo;
	}


	public String getNivel1() {
		return nivel1;
	}


	public void setNivel1(String nivel1) {
		this.nivel1 = nivel1;
	}


	public String getNivel2() {
		return nivel2;
	}


	public void setNivel2(String nivel2) {
		this.nivel2 = nivel2;
	}


	public String getNivel3() {
		return nivel3;
	}


	public void setNivel3(String nivel3) {
		this.nivel3 = nivel3;
	}


	public String getImagenNoHijos() {
		return imagenNoHijos;
	}


	public void setImagenNoHijos(String imagenNoHijos) {
		this.imagenNoHijos = imagenNoHijos;
	}


	public String getImagenNodoAbierto() {
		return imagenNodoAbierto;
	}


	public void setImagenNodoAbierto(String imagenNodoAbierto) {
		this.imagenNodoAbierto = imagenNodoAbierto;
	}


	public String getImagenNodoCerrado() {
		return imagenNodoCerrado;
	}


	public void setImagenNodoCerrado(String imagenNodoCerrado) {
		this.imagenNodoCerrado = imagenNodoCerrado;
	}

	
	public List<String> getNiveles() {
		return niveles;
	}


	public void setNiveles(List<String> niveles) {
		this.niveles = niveles;
	}
}
