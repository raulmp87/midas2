package mx.com.afirme.midas.sistema;

import javax.ejb.ApplicationException;

/**
 * Exception used when an application's service fails.
 * 
 * @author Marsil Benavides
 * @version 1.00, September 25, 2007
 */
@ApplicationException
public class SystemException
    extends Exception {

  private static final long serialVersionUID = 1L;
  
  private final int errorCode;

  /**
   * Constructs a new <code>SystemException</code> instance with the
   * specified detail <code>message</code>, <code>cause</code> and
   * <code>errorCode</code>.
   * 
   * @param message the detail message. The detail message is saved
   *          for later retrieval by the
   *          <code>Throwable.getMessage()</code> method.
   * @param cause the cause (which is saved for later retrieval by the
   *          <code>Throwable.getCause()</code> method). (A
   *          <code>null</code> value is permitted, and indicates
   *          that the <code>cause</code> is nonexistent or unknown
   * @param errorCode a numeric identifier of the cause.
   */
  public SystemException(String message, Throwable cause, int errorCode) {
    super(message, cause);
    this.errorCode = errorCode;
  }

  /**
   * Constructs a new <code>SystemException</code> instance with the
   * specified detail <code>message</code> and <code>errorCode</code>.
   * 
   * @param message the detail message. The detail message is saved
   *          for later retrieval by the
   *          <code>Throwable.getMessage()</code> method.
   * @param errorCode a numeric identifier of the cause.
   */
  public SystemException(String message, int errorCode) {
    this(message, null, errorCode);
  }

  /**
   * Constructs a new <code>SystemException</code> instance with the
   * specified detail <code>cause</code> and <code>errorCode</code>.
   * 
   * @param cause the cause (which is saved for later retrieval by the
   *          <code>Throwable.getCause()</code> method). (A
   *          <code>null</code> value is permitted, and indicates
   *          that the <code>cause</code> is nonexistent or unknown
   * @param errorCode a numeric identifier of the cause.
   */
  public SystemException(Throwable cause, int errorCode) {
    this(null, cause, errorCode);
  }
  
  /**
   * Constructs a new <code>SystemException</code> instance with the
   * specified detail <code>message</code>, <code>cause</code>.
   * 
   * @param message the detail message. The detail message is saved
   *          for later retrieval by the
   *          <code>Throwable.getMessage()</code> method.
   * @param cause the cause (which is saved for later retrieval by the
   *          <code>Throwable.getCause()</code> method). (A
   *          <code>null</code> value is permitted, and indicates
   *          that the <code>cause</code> is nonexistent or unknown.
   */
  public SystemException(String message, Throwable cause) {
    this(message, cause, 0);
  }
  
  /**
   * Constructs a new <code>SystemException</code> instance with the
   * specified detail <code>message</code>.
   * 
   * @param message the detail message. The detail message is saved
   *          for later retrieval by the
   *          <code>Throwable.getMessage()</code> method.
   */
  public SystemException(String message) {
    this(message, 0);
  }
  
  /**
   * Constructs a new <code>SystemException</code> instance with the
   * specified detail <code>cause</code>.
   * 
   * @param cause the cause (which is saved for later retrieval by the
   *          <code>Throwable.getCause()</code> method). (A
   *          <code>null</code> value is permitted, and indicates
   *          that the <code>cause</code> is nonexistent or unknown
   */
  public SystemException(Throwable cause) {
    this(cause, 0);
  }
  
  /**
   * Retrieves the error code which triggered this exception.
   */
  public int getErrorCode() {
    return this.errorCode;
  }

}
