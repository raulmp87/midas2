package mx.com.afirme.midas.sistema;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonDTO;

import org.apache.commons.beanutils.BeanUtils;

public class TagUtil {


	private static void renderAttribute(Writer writer, String name, String value)
			throws IOException {
		writer.write(" ");
		writer.write(name);
		writer.write("=\"");
		writer.write(value);
		writer.write("\"");
	}

	@SuppressWarnings("unchecked")
	public static void renderSelectTag(Writer writer, Map attributes,
			List optionList, String code, String description, List selected , boolean isReadOnly)
			throws SystemException {
		Iterator attributeKeyIterator = attributes.keySet().iterator();
		Iterator optionIterator = optionList.iterator();
		try {
			if (isReadOnly)
				writer.write("<select disabled=\"disabled\"");
			else
				writer.write("<select");
			while (attributeKeyIterator.hasNext()) {
				String name = (String) attributeKeyIterator.next();
				String value = (String) attributes.get(name);
				if (value != null) {
					TagUtil.renderAttribute(writer, name, value);
				} // End of if
			} // End of while()
			writer.write(">\n");
			if (optionList.isEmpty()){
				writer.write("<option value=\"\"");
			}
			else
				writer.write("<option value=\"\"");
			
			if (TagUtil.noneSelected(optionList, selected)) {
				writer.write(" selected=\"selected\"");
			} // End of if/else
			writer.write("> Seleccione ... </option>");

			while (optionIterator.hasNext()) {
				Object instance = optionIterator.next();
				String codeValue = BeanUtils.getProperty(instance, code);
				String descriptionValue = BeanUtils.getProperty(instance,
						description);
				writer.write("<option value=\"");
				writer.write(codeValue);
				writer.write("\"");
				if (selected != null && selected.contains(instance)) {
					writer.write(" selected=\"selected\"");
				} // End of if()
				writer.write(">");
				writer.write(descriptionValue);
				writer.write("</option>");
			} // End of while()
			writer.write("</select>");
		} catch (IOException ioException) {
			throw new SystemException(ioException);
		} catch (IllegalAccessException iaException) {
			throw new SystemException(iaException);
		} catch (InvocationTargetException itException) {
			throw new SystemException(itException);
		} catch (NoSuchMethodException nsmException) {
			throw new SystemException(nsmException);
		} // End of try/catch
	}
	
	@SuppressWarnings("unchecked")
	public static void renderSelectTagEscalon(Writer writer, Map attributes, List optionList, String code, String description, List selected , boolean isReadOnly, short claveTipovalidacion)throws SystemException {
		Iterator attributeKeyIterator = attributes.keySet().iterator();
		Iterator optionIterator = optionList.iterator();
		try {
			if (isReadOnly)		writer.write("<select disabled=\"disabled\"");
			else		writer.write("<select");
			while (attributeKeyIterator.hasNext()) {
				String name = (String) attributeKeyIterator.next();
				String value = (String) attributes.get(name);
				if (value != null) {
					TagUtil.renderAttribute(writer, name, value);
				} // End of if
			} // End of while()
			writer.write(">\n");
			if (optionList.isEmpty()){
				writer.write("<option value=\"hola\"");
			}
			else
				writer.write("<option value=\"\"");
			
			if (TagUtil.noneSelected(optionList, selected)) {
				writer.write(" selected=\"selected\"");
			} // End of if/else
			writer.write("> Seleccione ... </option>");
			while (optionIterator.hasNext()) {
				EscalonDTO escalon = (EscalonDTO)optionIterator.next();
				String descriptionValue = "" + escalon.getValorRangoInferior()+" - "+ escalon.getValorRangoSuperior();
				if (claveTipovalidacion == 1 || claveTipovalidacion == 4)
					descriptionValue = String.format("%.4f - %.4f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
				else if (claveTipovalidacion == 2)
					descriptionValue = String.format("%.4f", escalon.getValorRangoInferior())+" % - "+String.format("%.4f",escalon.getValorRangoSuperior())+" % ";
				else if (claveTipovalidacion == 3)
					descriptionValue = String.format("$ %.2f - $ %.2f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
				else if (claveTipovalidacion == 5)
					descriptionValue = String.format("%.0f - %.0f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
				writer.write("<option value=\""+escalon.getIdToEscalon()+"\">");
				writer.write(descriptionValue);
				writer.write("</option>");
			} // End of while()
			writer.write("</select>");
		} catch (IOException ioException) {
			throw new SystemException(ioException);
		}
	}

	@SuppressWarnings("unchecked")
	public static boolean noneSelected(List optionList, List selected) {
		boolean noneSelected = true;
		Iterator optionIterator = optionList.iterator();
		while (optionIterator.hasNext()) {
			Object instance = optionIterator.next();
			if (selected != null && selected.contains(instance)) {
				noneSelected = false;
				break;
			} // End of if
		} // End of while
		return noneSelected;
	}

	@SuppressWarnings("unchecked")
	public static void renderOptionTag(Writer out, List optionList,
			String code, String description, List selected)
			throws SystemException {
		Iterator optionIterator = optionList.iterator();
		BufferedWriter writer = new BufferedWriter(out);
		try {
			writer
					.write("<option value=\"\" selected =\"selected\"> Seleccione ... </option>");
			while (optionIterator.hasNext()) {
				Object instance = optionIterator.next();
				String codeValue = BeanUtils.getProperty(instance, code);
				String descriptionValue = BeanUtils.getProperty(instance,
						description);
				writer.write("<option value=\"");
				writer.write(codeValue);
				writer.write("\"");
				if (selected != null && selected.contains(instance)) {
					writer.write(" selected=\"selected\"");
				} // End of if()
				writer.write(">");
				writer.write(descriptionValue);
				writer.write("</option>");
			} // End of while()
			writer.flush();
		} catch (IOException ioException) {
			throw new SystemException(ioException);
		} catch (IllegalAccessException iaException) {
			throw new SystemException(iaException);
		} catch (InvocationTargetException itException) {
			throw new SystemException(itException);
		} catch (NoSuchMethodException nsmException) {
			throw new SystemException(nsmException);
		} // End of try/catch
	}
	
	@SuppressWarnings("unchecked")
	public static void renderTextBox(Writer out, Map attributes)throws SystemException {
		BufferedWriter writer = new BufferedWriter(out);
		Iterator attributeKeyIterator = attributes.keySet().iterator();
		try {
			writer.write("<input type=\"text\" ");
			while (attributeKeyIterator.hasNext()) {
				String eventName = (String) attributeKeyIterator.next();
				String value = (String) attributes.get(eventName);
				if (value != null) {
					TagUtil.renderAttribute(writer, eventName, value);
				} // End of if
			} // End of while()
			writer.write(">");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void renderHidden(Writer out, String name, String value)throws SystemException {
		BufferedWriter writer = new BufferedWriter(out);
		try {
			writer.write("<input type=\"hidden\" name=\""+name+"\" value=\""+value+"\" >");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void renderTextBox(Writer out, String name) throws SystemException {
		BufferedWriter writer = new BufferedWriter(out);
		try {
			writer.write("<input type=\"text\" name=\""+name+"\">");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getParameter(HttpServletRequest request,
			String catalogParameter) {
		String parameter = catalogParameter;
		String requestedParameter = request.getParameter(catalogParameter);
		if (!StringUtil.isEmpty(requestedParameter)) {
			parameter = requestedParameter;
		}
		return parameter;
	}
}