package mx.com.afirme.midas.sistema.cargacombo;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ComboDN {

	private static final ComboDN INSTANCIA = new ComboDN();

	public static ComboDN getInstancia() {
		return ComboDN.INSTANCIA;
	}

	public String construyeCadenaCombo(String refId2,String refDesc2,String refObject2) throws SystemException,ExcepcionDeAccesoADatos {
		ComboSN comboSN = new ComboSN();
		return comboSN.construyeCadenaCombo(refId2,refDesc2, refObject2);
	}

}