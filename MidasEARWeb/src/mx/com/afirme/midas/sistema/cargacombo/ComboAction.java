package mx.com.afirme.midas.sistema.cargacombo;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * MyEclipse Struts Creation date: 06-25-2009
 * 
 * XDoclet definition:
 * 
 * @struts.action path="/ajustador" name="ajustadorForm"
 *                input="/WEB-INF/jsp/ajustador.jsp" parameter="metodo"
 *                scope="request" validate="true"
 * @struts.action-forward name="noDisponible" path="/WEB-INF/jsp/error.jsp"
 *                        contextRelative="true"
 */
public class ComboAction extends MidasMappingDispatchAction{
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		try {
			this.construyeCadenaCombo(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method listarTodos
	 * 
	 * @param request
	 */
	private void construyeCadenaCombo(HttpServletRequest request)
			throws SystemException, ExcepcionDeAccesoADatos {
		ComboDN comboDN = ComboDN.getInstancia();
		String id =request.getParameter("");
		String desc=request.getParameter("");
		String object=request.getParameter("");
		
		String resultado = comboDN.construyeCadenaCombo(id,desc,object);
		request.setAttribute("cargaCombo",resultado);
	}

}