package mx.com.afirme.midas.sistema.cargacombo;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


/**
 * @author jmartinez
 * 
 */
public class ComboSN {
	private ComboFacadeRemote comboRemoto;


	public ComboSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			comboRemoto = serviceLocator.getEJB(ComboFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public String construyeCadenaCombo(String refId,String refDesc,String refObject) throws ExcepcionDeAccesoADatos {
		try {
			return comboRemoto.buildIdDescription(refId,refDesc,refObject);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}