package mx.com.afirme.midas.sistema.menu;

import java.io.CharArrayWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Menu;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.LineSeparator;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class MenuAction extends MidasMappingDispatchAction {
	int contador = 0;
	/**
	 * Valida y carga el menú de MIDAS dependiendo del rol de usuario.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void cargaMenu(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				
		CharArrayWriter usuarioMenusXmlWriter = getUsuarioMenusXmlCharWriter(request);
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(usuarioMenusXmlWriter.size());
		response.getWriter().write(usuarioMenusXmlWriter.toString());
	}

	/**
	 * Obtiene el <code>CharWriter</code> que contiene los menus del usuario en representación XML.
	 * @param request
	 * @return
	 */
	private CharArrayWriter getUsuarioMenusXmlCharWriter(HttpServletRequest request) {
		Usuario usuario;
		ServletContext sc = request.getSession().getServletContext();
		SistemaContext sistemaContext = getSistemaContext(request);
		String path = sc.getRealPath("/WEB-INF/config/sistema/menu/"
				+ sistemaContext.getArchivoMenuMidas());

		try {

			// Se crea el parseador
			DOMParser parser = new DOMParser();
			// Se procesa el archivo XML
			parser.parse(new InputSource(new FileInputStream(path)));
			// Obtenemos el objeto Document
			Document doc = parser.getDocument();

			// Se crea un nuevo XML usando DocumentBuilder
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			// Creamos el documento XML
			Document docNuevo = docBuilder.newDocument();
			
			// Se obtienen los menus permitidos para el usuario
			usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request,
					sistemaContext.getUsuarioAccesoMidas());
			
			if (sistemaContext.getSeguridadActivada()) { //Si la seguridad esta activada
				procesarMenus(doc, docNuevo, usuario);
			} else {
				// Se cargan todas las opciones de men�
				
				//System.out.println("Sin Seguridad");
				
				Element etiquetaRaizACopiar = doc.getDocumentElement();
				Node nodoRaiz = docNuevo.importNode(etiquetaRaizACopiar, true);
				docNuevo.appendChild(nodoRaiz);
			}
			

			// Se convierte el arbol DOM en una cadena con XML
			// Se define el formato de salida: encoding, identaci�n, separador
			// de l�nea,...
			// Se pasa el nuevo documento como argumento para tener un formato
			// de partida
			OutputFormat format = new OutputFormat(docNuevo);
			format.setLineSeparator(LineSeparator.Unix);
			format.setOmitXMLDeclaration(false);
			format.setEncoding("ISO-8859-1");
			format.setIndenting(true);
			format.setLineWidth(0);
			format.setPreserveSpace(false);
			// Se define donde se va a escribir. Puede ser cualquier
			// OutputStream o un Writer
			CharArrayWriter salidaXML = new CharArrayWriter();
			// Se serializa el arbol DOM
			XMLSerializer serializer = new XMLSerializer((Writer) salidaXML,
					format);
			serializer.asDOMSerializer();
			serializer.serialize(docNuevo);

			return salidaXML;

		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Fue necesario agregar este metodo debido a que no fue posible que del documento clonado tambien se copiara
	 * el document type del original por lo tanto el <code>document.getElementById()</code> no funcionaba para este.
	 * @param document
	 * @param id
	 * @return
	 */
	private Element getElementById(Document document, String id) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "//*[@id='" + id +"']";
		NodeList nodes;
		try {
			nodes = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
		if (nodes.getLength() > 1) {
			
			return (Element) nodes.item(0);
			//throw new RuntimeException("Mas de un elemento con el mismo id. " + id);
		}
		if (nodes.getLength() == 1) {
			return (Element) nodes.item(0);
		}
		return null;
	}

	private void procesarMenus(Document menuUniverso, Document menuUsuario, Usuario usuario) {
		Element raiz = menuUniverso.getDocumentElement();
		Node nodoRaiz = menuUsuario.importNode(raiz, false);
		menuUsuario.appendChild(nodoRaiz);
		
		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		LogDeMidasWeb.getLogger().log(Level.INFO, "JECL Hora y Fecha Inicio : "+hourdateFormat.format(new Date()));
		menuUniverso.getDocumentElement().normalize();		
		ArrayList<String> listaMenus = new ArrayList<String>();
				
		Element rootElement = menuUniverso.getDocumentElement();
		NodeList nodeList = rootElement.getElementsByTagName("item");
		
		
		if(nodeList != null && nodeList.getLength()>0)
		{
			
			for(int i=0;i<nodeList.getLength();i++)
			{
				for (Menu menu : usuario.getMenus()) 
				  {
					  org.w3c.dom.Element element = (Element)nodeList.item(i);
					  if(element.hasAttribute("id") && element.getAttribute("id").equals(menu.getCodigo())) 
					  {
						  if(!listaMenus.contains(element.getAttribute("id")))
						  {
							  listaMenus.add(element.getAttribute("id"));
							  actualizarMenuUsuario(menuUniverso, menuUsuario, element);
							  break;
						  }
						  
					  }
				  }	    
			}		  
		}
		
		LogDeMidasWeb.getLogger().log(Level.INFO, "JECL Hora y Fecha Fin : "+hourdateFormat.format(new Date()));
	}
	
	
	private void procesarMenus(Document menuUniverso, Document menuUsuario, NodeList childNodes, Usuario usuario, ArrayList<String> listaUniversal
	  ) 
	{
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node item = childNodes.item(i);
				if (!(item instanceof Element)) {
					continue;
				}
				Element element = (Element) childNodes.item(i);
				if (isMenu(element)&& !listaUniversal.contains(element.getAttribute("id"))) {
					listaUniversal.add(element.getAttribute("id"));
					contador++;
					procesarMenus(menuUniverso, menuUsuario, element.getChildNodes(), usuario,listaUniversal);
				}
			}
		
		
	}
	
	private boolean isMenu(Element element) {
		return element.getNodeName().equals("item");
	}
	
	private boolean isUsuarioTienePermisoParaVerMenu(Element menu,Usuario usuario) {
		String menuCodigo = menu.getAttribute("id");
		return hasMenuCodigo(usuario.getMenus(), menuCodigo);
	}
	 
	
	private boolean isUsuarioTienePermisoParaVerMenu(Element menu,Menu usuario) {
		String menuCodigo = menu.getAttribute("id");
		return menuCodigo.equals(usuario.getCodigo());
	}
	
	
	private boolean hasMenuCodigo(List<Menu> menus , String codigo) {
		for (Menu m: menus) {
			
			if (m.getCodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	

	private void actualizarMenuUsuario(Document menuUniverso, Document menuUsuario, Element menu) {
		Element menuClon = clonarMenuPrerservandoHijosInmediatosExceptoMenus(menu);
		actualizarMenuUsuario(menuUniverso, menuUsuario, menu, menuClon);
	}	

	private void actualizarMenuUsuario(Document menuUniverso,
			Document menusUsuario, Element menu, Element menuClon) {	
		Element padre = (Element) menu.getParentNode();
		String padreId = padre.getAttribute("id");
		Element elementoEncontrado = getElementById(menusUsuario, padreId);
		if (elementoEncontrado == null) {
			//Se crea el padre solo con con un hijo.
			Element clonPadre = clonarMenuPrerservandoHijosInmediatosExceptoMenus(padre);
			clonPadre.appendChild(menuClon);
			actualizarMenuUsuario(menuUniverso, menusUsuario, padre, clonPadre);
		} else {
			elementoEncontrado.appendChild(menusUsuario.importNode(menuClon, true));
		}
	}
	
	/**
	 * Clona el menu enviado prerservando los hijos y excluyendo los
	 * elementos que sean menus.
	 * @param menu el menu a clonar
	 * @return
	 */
	private Element clonarMenuPrerservandoHijosInmediatosExceptoMenus(Element menu) {
		Element menuClon = (Element) menu.cloneNode(false);
		//Agregar todo excepto menus
		NodeList childNodes = menu.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node item = childNodes.item(i);
			if (item instanceof Element && isMenu((Element)item)) {
				continue;
			}
			Node cloneNode = item.cloneNode(true);
			menuClon.appendChild(cloneNode);
		}
		return menuClon;
	}

	/**
	 * Redirecciona a una p�gina espec�fica en caso de no pasar el filtro de
	 * seguridad
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void error(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		int acceso = Integer
				.parseInt(request.getParameter("acceso").toString());
		SistemaContext sistemaContext = getSistemaContext(request);

		if (acceso == 1) {
			response.sendRedirect(sistemaContext.getAccesoDenegado());
		} else {
			if (sistemaContext.getSeguridadActivada()) {
				if(!sistemaContext.getAsmActivo()){
					response.sendRedirect(sistemaContext.getUrlError());	
				}else{
					String path = sistemaContext.getAsmLogin() + "?clientApplicationId=" + sistemaContext.getMidasAppId() + "&clientUrl=" + sistemaContext.getMidasLogin();
					String scr = "<script>window.location=\""+path+"\"</script>";
					//System.out.println(scr);
					response.getWriter().write(scr);
					//response.sendRedirect(sistemaContext.getAsmLogin());	
				}
				
			} else {
				response.sendRedirect(sistemaContext.getUrlLoginSinSeguridad());
			}
		}

	}

	private SistemaContext getSistemaContext(HttpServletRequest request) {
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());		
		return webApplicationContext.getBean(SistemaContext.class);
	}

	
	
}
