package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TagUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

public class SubMarcaVehiculo extends SoporteBaseEtiqueta {

	public SubMarcaVehiculo() throws SystemException {
		LogDeMidasWeb.log("Entrando en la etiqueta SubMarca Vehiculo  - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(SubMarcaVehiculoFacadeRemote.class) ;
		marcaVehiculoFacadeRemote = serviceLocator.getEJB(MarcaVehiculoFacadeRemote.class) ;
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	private SubMarcaVehiculoFacadeRemote beanRemoto;
	
	private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;

	private static final long serialVersionUID = 1L;

	private String nombre = Constants.BEAN_KEY;

	private String propiedad = null;

	private String marcaVehiculo = null;

	private String onchange = null;

	private String size = null;

	private String styleId = null;

	private String styleClass = null;

	private boolean indexed;

	private String readonly = null;

	@SuppressWarnings("unchecked")
	public int doStartTag() throws JspException {

		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();
			HttpServletRequest request = (HttpServletRequest) this.pageContext
					.getRequest();

			Map attributes = new HashMap();
			attributes.put("size", this.size);
			attributes.put("onchange", this.onchange);
			attributes.put("id", this.styleId);
			attributes.put("class", this.styleClass);
			attributes.put("name", this.propiedad);

			if (this.readonly != null && this.readonly.equals("true")) {
				attributes.put("onchange",
						"this.selectedIndex = this.initialSelect;");
				attributes.put("onfocus",
						"this.initialSelect = this.selectedIndex;");
			}

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);

			if (bean != null) {
				String selectedId = null;
				List selectedList = null;
				List list = null;
				try {
					selectedId =  BeanUtils.getProperty(bean, this.marcaVehiculo);
					if (selectedId != null && !selectedId.equals("")) {
						MarcaVehiculoDTO marcaVehiculoDTO =  marcaVehiculoFacadeRemote.findById(new BigDecimal(selectedId));
						try {
							if(marcaVehiculoDTO != null && marcaVehiculoDTO.getIdTcMarcaVehiculo() != null)
								list =beanRemoto.findByProperty("marcaVehiculoDTO.idTcMarcaVehiculo",marcaVehiculoDTO.getIdTcMarcaVehiculo());
						} catch (NumberFormatException nfException) {
						} catch (NullPointerException npException) {
						} // End of try/catch
						if (list == null) {
							list = new ArrayList();
						} // End of if
					} else {
						selectedList = new ArrayList();
						list = new ArrayList();
					}
					selectedList = this.getSelectedList(request, this.nombre,
							this.propiedad);
					boolean isReadOnly = false;
					this.sortSubMarcaCollection(list);
					this.sortSubMarcaCollection(selectedList);
					if (getReadonly() != null)
						isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
						
					if (!this.indexed)
						TagUtil.renderSelectTag(writer, attributes, list,
								"id", "description", selectedList,isReadOnly);
					else
						TagUtil.renderOptionTag(writer, list, "id",
								"description", selectedList);

				} catch (InvocationTargetException iException) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, iException);
				} catch (IllegalAccessException e) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, e);
				} catch (NoSuchMethodException e) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, e);
				} catch (RemoteException rException) {
					throw new SystemException(rException);
				} // End of try/catch
			} // End of if
		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing vehicle catalog.",
					Level.FINEST, sException);
		} // End of try/catch

		return SKIP_BODY;
	}

	@SuppressWarnings("unchecked")
	private List getSelectedList(HttpServletRequest request, String name,
			String property) throws RemoteException, SystemException {
		TagUtils tagUtils = TagUtils.getInstance();
		List<SubMarcaVehiculoDTO> selectedList = new ArrayList();
		Object bean = null;
		try {
			bean = tagUtils.lookup(this.pageContext, name, null);
			if (bean != null) {
				String[] selectedIds = BeanUtils.getArrayProperty(bean,
						property);
				if (selectedIds != null) {
					for (int i = 0; i < selectedIds.length; i++) {
						try {
						
							SubMarcaVehiculoDTO submarcaVehiculoDTO = beanRemoto.findById(UtileriasWeb
									.regresaBigDecimal(selectedIds[i]));
							selectedList.add(submarcaVehiculoDTO);
						} catch (NumberFormatException nfException) {
						} // End of try/catch
					} // End of for
				} // End of if
			} // End of if
		} catch (InvocationTargetException iException) {
			LogDeMidasWeb.log("Unable to find matching values", Level.FINEST,
					iException);
		} catch (IllegalAccessException e) {
			LogDeMidasWeb
					.log("Unable to find matching values", Level.FINEST, e);
		} catch (NoSuchMethodException e) {
			LogDeMidasWeb
					.log("Unable to find matching values", Level.FINEST, e);
		} catch (JspException jspException) {
			LogDeMidasWeb.log("Unable to find bean.", Level.FINEST,
					jspException);
		} // End of try/catch
		return selectedList;
	}

	public void release() {
		super.release();
		this.nombre = Constants.BEAN_KEY;
		this.propiedad = null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}



	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}

	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

	@SuppressWarnings("unchecked")
	private void sortSubMarcaCollection(List list) {
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2) {
				if(o1 instanceof SubMarcaVehiculoDTO && o2 instanceof SubMarcaVehiculoDTO) {
					return ((SubMarcaVehiculoDTO)o1).getDescription().compareTo(((SubMarcaVehiculoDTO)o2).getDescription());
				}
				return 0;
			}
		});
	}
}