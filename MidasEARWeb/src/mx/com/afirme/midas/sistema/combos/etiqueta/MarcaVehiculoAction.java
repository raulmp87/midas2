package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class MarcaVehiculoAction extends BaseEtiquetaAction {
	
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");

		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		MarcaVehiculoFacadeRemote beanRemoto = serviceLocator.getEJB(MarcaVehiculoFacadeRemote.class);
		TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote = serviceLocator.getEJB(TipoVehiculoFacadeRemote.class) ;
		//String[] tipoVehiculoId = id.split("_");
		try {
			TipoVehiculoDTO tipoVehiculoDTO =  tipoVehiculoFacadeRemote.findById(new BigDecimal(id));
			List<MarcaVehiculoDTO> list = beanRemoto.findByProperty("tipoBienAutosDTO.claveTipoBien",tipoVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			LogDeMidasWeb.log("Vehicle Mark list size for class(" + id + ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<MarcaVehiculoDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				MarcaVehiculoDTO marcaVehiculoDTO= (MarcaVehiculoDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(marcaVehiculoDTO.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(marcaVehiculoDTO.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}
	public void escribeComboSubmarca(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		SubMarcaVehiculoFacadeRemote beanRemoto = serviceLocator.getEJB(SubMarcaVehiculoFacadeRemote.class);
		MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote = serviceLocator.getEJB(MarcaVehiculoFacadeRemote.class) ;
		//String[] tipoVehiculoId = id.split("_");
		try {
			MarcaVehiculoDTO marcaVehiculoDTO =  marcaVehiculoFacadeRemote.findById(new BigDecimal(id));
			List<SubMarcaVehiculoDTO> list = beanRemoto.findByProperty("marcaVehiculoDTO.idTcMarcaVehiculo",marcaVehiculoDTO.getIdTcMarcaVehiculo());
			LogDeMidasWeb.log("Vehicle Mark list size for class(" + id + ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<SubMarcaVehiculoDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				SubMarcaVehiculoDTO subMarcaVehiculoDTO= (SubMarcaVehiculoDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(subMarcaVehiculoDTO.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(subMarcaVehiculoDTO.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}

}
