package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CodigoPostalAction extends BaseEtiquetaAction {
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");
		String cityId = "";
		String stateId = "";
		
		try {
		    	ServiceLocator serviceLocator = ServiceLocator.getInstance();
			ColoniaFacadeRemote beanRemoto = serviceLocator.getEJB(ColoniaFacadeRemote.class);
			List<ColoniaDTO> list = beanRemoto.findByProperty("zipCode",id.trim());
			LogDeMidasWeb.log("Colony list size for zipCode(" + id + ")= "
					+ list.size(), Level.INFO, null);
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			if(list!=null && !list.isEmpty()){
                		this.sortColonyCollection(list);
                		Iterator<ColoniaDTO> iteratorList = list.iterator();
                			while (iteratorList.hasNext()) {
                				ColoniaDTO coloniaDTO = (ColoniaDTO) iteratorList.next();
                				cityId = coloniaDTO.getCityId();
                				buffer.append("<colonies>");
                				buffer.append("<id>");
                				buffer.append(coloniaDTO.getId());
                				buffer.append("</id>");
                				buffer.append("<description><![CDATA[");
                				buffer.append(coloniaDTO.getDescription());
                				buffer.append("]]></description>");
                				buffer.append("</colonies>");
                			}
        			
        			CiudadFacadeRemote ciudadRemoto = serviceLocator.getEJB(CiudadFacadeRemote.class);
        			List<CiudadDTO> cityList = ciudadRemoto.findByProperty("cityId", cityId.trim());
        			
        			if (cityList != null && !cityList.isEmpty()){
        			    	this.sortCityCollection(cityList);
        			    	Iterator<CiudadDTO> cityIterator = cityList.iterator(); 
        				CiudadDTO cityDTO = (CiudadDTO) cityIterator.next();
        				stateId = cityDTO.getStateId();
        	  			buffer.append("<city>");
        	  			buffer.append("<id>");
        	  			buffer.append(cityDTO.getCityId());
        	  			buffer.append("</id>");
        	  			buffer.append("<description><![CDATA[");
        	  			buffer.append(cityDTO.getCityName());
        	  			buffer.append("]]></description>");
        	  			buffer.append("</city>");
        				
        	  			EstadoFacadeRemote estadoRemoto = serviceLocator.getEJB(EstadoFacadeRemote.class);
        				List<EstadoDTO> stateList = estadoRemoto.findByProperty("stateId", stateId.trim());				
        	  			
        				if (stateList != null && !stateList.isEmpty()){
                				Iterator<EstadoDTO> stateIterator = stateList.iterator();
                				EstadoDTO stateDTO = (EstadoDTO) stateIterator.next();
                	    			buffer.append("<state>");
                	    			buffer.append("<id>");
                	    			buffer.append(stateDTO.getStateId());
                	    			buffer.append("</id>");
                	    			buffer.append("<description><![CDATA[");
                	    			buffer.append(stateDTO.getStateName());
                	    			buffer.append("]]></description>");
                	    			buffer.append("</state>");
        				}
        
        			}
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}

	@SuppressWarnings("unchecked")
	private void sortColonyCollection(List list) {
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2) {
				if(o1 instanceof ColoniaDTO && o2 instanceof ColoniaDTO) {
					return ((ColoniaDTO)o1).getDescription().compareTo(((ColoniaDTO)o2).getDescription());
				}
				return 0;
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void sortCityCollection(List list) {
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2) {
				if(o1 instanceof CiudadDTO && o2 instanceof CiudadDTO) {
					return ((CiudadDTO)o1).getDescription().compareTo(((CiudadDTO)o2).getDescription());
				}
				return 0;
			}
		});
	}
}
