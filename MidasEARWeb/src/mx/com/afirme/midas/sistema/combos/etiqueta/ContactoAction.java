package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;
import mx.com.afirme.midas.catalogos.contacto.ContactoDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ContactoAction extends BaseEtiquetaAction {
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");				
		
		ContactoDN contactoDN = ContactoDN.getInstancia();

		try {
			ContactoDTO contactoDTO = new ContactoDTO();			
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(id));
			contactoDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
			List<ContactoDTO> list = contactoDN.listarFiltrado(contactoDTO);
			LogDeMidasWeb.log("Contacto list size for idTcReaseguradorCorredor(" + id + ") = " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<ContactoDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				ContactoDTO dto = (ContactoDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(dto.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(dto.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} catch (ExcepcionDeAccesoADatos edaad) {
			throw new SystemException("DB Error - Unable to render select tag",
					edaad);
		}// End of try/catch
	}
}
