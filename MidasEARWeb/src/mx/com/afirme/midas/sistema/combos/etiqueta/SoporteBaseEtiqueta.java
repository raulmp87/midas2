package mx.com.afirme.midas.sistema.combos.etiqueta;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

public class SoporteBaseEtiqueta extends TagSupport {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(SoporteBaseEtiqueta.class);

	protected String getMensaje(String mensajeKey) {
		HttpServletRequest request = (HttpServletRequest) super.pageContext
				.getRequest();
		LOG.debug("Retrieving message from request \"" + request
				+ "\" with message key \"" + mensajeKey + "\".");
		return "{unimplemented by mbc (" + mensajeKey + ")}";
	}

}
