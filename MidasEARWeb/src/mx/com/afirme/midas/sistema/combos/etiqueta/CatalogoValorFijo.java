package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TagUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

/**
 * 
 * @author Jos� Luis Arellano
 * @since 11 de septiembre de 2009
 */
public class CatalogoValorFijo extends SoporteBaseEtiqueta{

	private static final long serialVersionUID = -4753335105706805227L;
	private CatalogoValorFijoFacadeRemote beanRemoto;
	
	public CatalogoValorFijo() throws SystemException{
		LogDeMidasWeb.log("Entrando en la etiqueta CatalogoValorFijo - Constructor",Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(CatalogoValorFijoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	private String nombre = Constants.BEAN_KEY;
	private String propiedad = null;
	private String onchange = null;
	private String size = null;
	private String styleId = null;
	private String styleClass = null;
	private boolean indexed;
	private String readonly = null;
	private String onblur = null;
	private String grupoValores = null;
	private String valoresExcluidos = null;
	
	
	public int doStartTag() throws JspException {
		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();
			HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();

			Map<String,String> attributes = new HashMap<String,String>();
			attributes.put("size", this.size);
			attributes.put("id", this.styleId);
			attributes.put("class", this.styleClass);
			attributes.put("name", this.propiedad);
			attributes.put("onblur", this.onblur);
			attributes.put("onchange", this.onchange);

			if (this.readonly != null && this.readonly.equals("true")) {
				attributes.put("onchange",	"this.selectedIndex = this.initialSelect;");
				attributes.put("onfocus", "this.initialSelect = this.selectedIndex;");
			}

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);
			if (bean != null) {
				List<CatalogoValorFijoDTO> selectedList = null;
				boolean isReadOnly = false;
				try {
					List<CatalogoValorFijoDTO> listaCatalogosFijos = beanRemoto.findByProperty("id.idGrupoValores", Integer.valueOf(grupoValores));
					filtrarCatalogo(listaCatalogosFijos);
					if (this.readonly != null)
						isReadOnly = Boolean.valueOf(readonly).booleanValue();
					selectedList = getSelectedList(request,listaCatalogosFijos);
					TagUtil.renderSelectTag(writer, attributes, listaCatalogosFijos, "id.idDato", "descripcion", selectedList,isReadOnly);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			} // End of if
		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing tarifa catalog.",Level.FINEST, sException);
		} // End of try/catch
		return SKIP_BODY;
	}
	
	private void filtrarCatalogo(List<CatalogoValorFijoDTO> listaCompleta){
		if(this.valoresExcluidos!= null){
			String[] valoresSplit = null;
			if(this.valoresExcluidos.contains(",")){
				valoresSplit = this.valoresExcluidos.split(",");
			}else{
				valoresSplit = new String[1];
				valoresSplit[0] = this.valoresExcluidos;				
			}
			for(String valorExcluido: valoresSplit){
				int valorInt = -999;
				try{
					valorInt = Integer.valueOf(valorExcluido);
				}catch (NumberFormatException ne){
					valorInt = -999;
				}
				if(valorInt != -999){
					int valorIndice = -999;
					int i=0;
					for(CatalogoValorFijoDTO valorCatalogo: listaCompleta){
						if(valorCatalogo.getId().getIdDato() == valorInt)
							valorIndice = i;
						if(valorIndice != -999)
							break;
						i++;
					}
					if(valorIndice != -999)
						listaCompleta.remove(valorIndice);
				}
			}
		}
	}
	private List<CatalogoValorFijoDTO> getSelectedList(HttpServletRequest request,List<CatalogoValorFijoDTO> listaCompleta) throws RemoteException, SystemException {
		TagUtils tagUtils = TagUtils.getInstance();
		List<CatalogoValorFijoDTO> selectedList = new ArrayList<CatalogoValorFijoDTO>();
		Object bean = null;
		try {
			bean = tagUtils.lookup(this.pageContext, this.nombre, null);
			if (bean != null) {
				String idGrupoStr = BeanUtils.getProperty(bean, this.propiedad);
				if (!UtileriasWeb.esCadenaVacia(idGrupoStr)) {
					int idGrupo = Integer.valueOf(idGrupoStr);
					for (CatalogoValorFijoDTO actual : listaCompleta){
						if (actual.getId().getIdDato() == idGrupo){
							selectedList.add(actual);
							break;
						}
					}
				} // End of if
			} // End of if
		} catch (InvocationTargetException iException) {
			LogDeMidasWeb.log("Unable to find matching values", Level.FINEST,
					iException);
		} catch (IllegalAccessException e) {
			LogDeMidasWeb.log("Unable to find matching values", Level.FINEST, e);
		} catch (NoSuchMethodException e) {
			LogDeMidasWeb.log("Unable to find matching values", Level.FINEST, e);
		} catch (JspException jspException) {
			LogDeMidasWeb.log("Unable to find bean.", Level.FINEST,jspException);
		} // End of try/catch
		return selectedList;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

	public String getOnblur() {
		return onblur;
	}

	public void setOnblur(String onblur) {
		this.onblur = onblur;
	}

	public String getGrupoValores() {
		return grupoValores;
	}

	public void setGrupoValores(String grupoValores) {
		this.grupoValores = grupoValores;
	}

	public String getValoresExcluidos() {
		return valoresExcluidos;
	}

	public void setValoresExcluidos(String valoresExcluidos) {
		this.valoresExcluidos = valoresExcluidos;
	}
}
