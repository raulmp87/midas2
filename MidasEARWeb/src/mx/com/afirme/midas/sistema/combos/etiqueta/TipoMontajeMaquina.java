package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import mx.com.afirme.midas.catalogos.tipomontajemaquina.TipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.TipoMontajeMaquinaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TagUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

public class TipoMontajeMaquina extends SoporteBaseEtiqueta{
	
private static final long serialVersionUID = -6792001625010461620L;
	
	private TipoMontajeMaquinaFacadeRemote beanRemoto;

	private String nombre = Constants.BEAN_KEY;

	private String propiedad = null;

	private String onchange = null;

	private String size = null;

	private String styleId = null;

	private String styleClass = null;

	private boolean indexed;

	private String readonly = null;

	public TipoMontajeMaquina() throws SystemException {
		LogDeMidasWeb.log("Entrando en la etiqueta Tipo de Montaje de maquina  - Constructor",Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(TipoMontajeMaquinaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	@SuppressWarnings("unchecked")
	public int doStartTag() throws JspException {

		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();
			HttpServletRequest request = (HttpServletRequest) this.pageContext
			.getRequest();			
			Map attributes = new HashMap();
			attributes.put("size", this.size);
			attributes.put("onchange", this.onchange);
			attributes.put("id", this.styleId);
			attributes.put("class", this.styleClass);
			attributes.put("name", this.propiedad);

			if (this.readonly != null && this.readonly.equals("true")) {
				attributes.put("onchange",
						"this.selectedIndex = this.initialSelect;");
				attributes.put("onfocus",
						"this.initialSelect = this.selectedIndex;");
			}

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);

			if (bean != null) {
				String selectedId = null;
				List selectedList = null;
				List list = null;
				try {
					selectedId = BeanUtils.getProperty(bean, this.propiedad);
					if (!StringUtil.isEmpty(selectedId)) {
						try {
							list = beanRemoto.findAll();
						} catch (NumberFormatException nfException) {
						} catch (NullPointerException npException) {
						} // End of try/catch
						if (list == null) {
							list = new ArrayList();
						} // End of if
					} else {
						selectedList = new ArrayList();
						list = beanRemoto.findAll();
					}
					selectedList = this.getSelectedList(request, this.nombre,
							this.propiedad);
					boolean isReadOnly = false;
					if (getReadonly() != null)
						isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
					if (!this.indexed)
						TagUtil.renderSelectTag(writer, attributes, list,
								"idtctipomontajemaq", "descripciontipomontajemaq", selectedList,isReadOnly);
					else
						TagUtil.renderOptionTag(writer, list, "idtctipomontajemaq",
								"descripciontipomontajemaq", selectedList);

				} catch (InvocationTargetException iException) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, iException);
				} catch (IllegalAccessException e) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, e);
				} catch (NoSuchMethodException e) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, e);
				} catch (RemoteException rException) {
					throw new SystemException(rException);
				} // End of try/catch
			} // End of if

		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing TipoMontajeMaquina catalog.",
					Level.FINEST, sException);
		} // End of try/catch

		return SKIP_BODY;
	}

	@SuppressWarnings("unchecked")
	private List getSelectedList(HttpServletRequest request, String name,
			String property) throws RemoteException, SystemException {
		TagUtils tagUtils = TagUtils.getInstance();
		List<TipoMontajeMaquinaDTO> selectedList = new ArrayList();
		Object bean = null;
		try {
			bean = tagUtils.lookup(this.pageContext, name, null);
			if (bean != null) {
				String[] selectedIds = BeanUtils.getArrayProperty(bean,property);
				if (selectedIds != null) {
					for (int i = 0; i < selectedIds.length; i++) {
						try {
							TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = beanRemoto.findById(UtileriasWeb.regresaBigDecimal(selectedIds[i]));
							selectedList.add(tipoMontajeMaquinaDTO);
						} catch (NumberFormatException nfException) {
						} // End of try/catch
					} // End of for
				} // End of if
			} // End of if
		} catch (InvocationTargetException iException) {
			LogDeMidasWeb.log("Unable to find matching values", Level.FINEST,
					iException);
		} catch (IllegalAccessException e) {
			LogDeMidasWeb
					.log("Unable to find matching values", Level.FINEST, e);
		} catch (NoSuchMethodException e) {
			LogDeMidasWeb
					.log("Unable to find matching values", Level.FINEST, e);
		} catch (JspException jspException) {
			LogDeMidasWeb.log("Unable to find bean.", Level.FINEST,
					jspException);
		} // End of try/catch
		return selectedList;
	}


	public void release() {
		super.release();
		this.nombre = Constants.BEAN_KEY;
		this.propiedad = null;
	}

	

	public TipoMontajeMaquinaFacadeRemote getBeanRemoto() {
		return beanRemoto;
	}

	public void setBeanRemoto(TipoMontajeMaquinaFacadeRemote beanRemoto) {
		this.beanRemoto = beanRemoto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

}
