package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReaseguradorCorredorAction extends BaseEtiquetaAction {
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String tipo = request.getParameter("id");
		
		
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();

		try {
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setTipo(tipo);
			List<ReaseguradorCorredorDTO> list = reaseguradorCorredorDN.listarFiltrado(reaseguradorCorredorDTO);
			LogDeMidasWeb.log("ReaseguradorCorredor list size for tipo(" + tipo + ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<ReaseguradorCorredorDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				ReaseguradorCorredorDTO dto = (ReaseguradorCorredorDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(dto.getIdtcreaseguradorcorredor());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(dto.getNombre());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} catch (ExcepcionDeAccesoADatos edaad) {
			throw new SystemException("DB Error - Unable to render select tag",
					edaad);
		}// End of try/catch
	}
	
	public void escribeComboOrdenado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String tipo = request.getParameter("id");
		
		
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();

		try {
			List<ReaseguradorCorredorDTO> list;
			if(Integer.parseInt(tipo) == 2){
				 list = reaseguradorCorredorDN.listarTodos();
			}else{
				ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
				reaseguradorCorredorDTO.setTipo(tipo);
				list = reaseguradorCorredorDN.listarFiltrado(reaseguradorCorredorDTO);
			}
			for(int i = 0; i < list.size()-1; i++){
				for(int j = i+1; j < list.size(); j++){
					if(list.get(i).getNombre().compareTo(list.get(j).getNombre()) > 0){
						ReaseguradorCorredorDTO aux = list.get(i);
						list.set(i, list.get(j));
						list.set(j, aux);
					}				
				}
			}
			LogDeMidasWeb.log("ReaseguradorCorredor list size for tipo(" + tipo + ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<ReaseguradorCorredorDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				ReaseguradorCorredorDTO dto = (ReaseguradorCorredorDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(dto.getIdtcreaseguradorcorredor());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(dto.getNombre());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} catch (ExcepcionDeAccesoADatos edaad) {
			throw new SystemException("DB Error - Unable to render select tag",
					edaad);
		}// End of try/catch
	}
	
}
