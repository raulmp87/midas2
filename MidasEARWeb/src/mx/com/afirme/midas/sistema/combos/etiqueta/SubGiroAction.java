package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SubGiroAction extends BaseEtiquetaAction {
	
	public void escribeCombo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		String id = request.getParameter("id");
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		SubGiroFacadeRemote beanRemoto = serviceLocator.getEJB(SubGiroFacadeRemote.class);

		try {
			List<SubGiroDTO> list = beanRemoto.findByProperty("giroDTO.idTcGiro", UtileriasWeb.regresaBigDecimal(id));
			LogDeMidasWeb.log("SubGiro list size for idTcGiro(" + id + ")= " + list.size(), Level.INFO, null);
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<SubGiroDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				SubGiroDTO subGiroDTO = (SubGiroDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(subGiroDTO.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(subGiroDTO.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",ioException);
		} // End of try/catch
	}
}
