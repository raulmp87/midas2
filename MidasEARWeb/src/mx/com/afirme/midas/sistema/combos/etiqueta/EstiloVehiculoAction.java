package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class EstiloVehiculoAction extends BaseEtiquetaAction {
	
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String tipoVehiculo = null;
		String idTipoVehiculo = null;
		String idClaveTipoBien = null;
		String idTcMarcaVehiculo = null;
		String idVersionCarga = null;
		
		idTipoVehiculo = request.getParameter("idTipoVehiculo");
		idTcMarcaVehiculo = request.getParameter("idMarcaVehiculo");
		idVersionCarga = request.getParameter("idVersionCarga");
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		
		EstiloVehiculoFacadeRemote beanRemoto = serviceLocator.getEJB(EstiloVehiculoFacadeRemote.class);
		TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote = serviceLocator.getEJB(TipoVehiculoFacadeRemote.class) ;
		
		TipoVehiculoDTO tipoVehiculoDTO =  tipoVehiculoFacadeRemote.findById(new BigDecimal(idTipoVehiculo));
		try {
			EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
			estiloVehiculoDTO.getId().setIdVersionCarga(UtileriasWeb
					.regresaBigDecimal(idVersionCarga));
			estiloVehiculoDTO.setIdTcTipoVehiculo(UtileriasWeb
					.regresaBigDecimal(idTipoVehiculo));
			estiloVehiculoDTO.getTipoBienAutosDTO().setClaveTipoBien(tipoVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien());
			estiloVehiculoDTO.getMarcaVehiculoDTO().setIdTcMarcaVehiculo(UtileriasWeb
					.regresaBigDecimal(idTcMarcaVehiculo));
			List<EstiloVehiculoDTO> list = beanRemoto.listarFiltrado(estiloVehiculoDTO);
			LogDeMidasWeb.log("Vehicle Style list size for class(" + idTipoVehiculo + ", " + tipoVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien()  + ", " + idVersionCarga  + ", " + idTcMarcaVehiculo  +  ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<EstiloVehiculoDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				EstiloVehiculoDTO estiloVehiculoDTOItem= (EstiloVehiculoDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(estiloVehiculoDTOItem.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(estiloVehiculoDTOItem.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}

}