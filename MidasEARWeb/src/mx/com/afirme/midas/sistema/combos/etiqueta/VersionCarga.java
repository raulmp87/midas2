package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TagUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.dto.wrapper.VersionCargaComboDTO;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

public class VersionCarga extends SoporteBaseEtiqueta {

	public VersionCarga() throws SystemException {
		LogDeMidasWeb.log("Entrando en la etiqueta VersionCarga  - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(EstiloVehiculoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	private EstiloVehiculoFacadeRemote beanRemoto;

	private static final long serialVersionUID = 1L;

	private String nombre = Constants.BEAN_KEY;

	private String propiedad = null;

	private String tipoBien = null;
	
	private String tipoVehiculo = null;
	
	private String marcaVehiculo = null;

	private String onchange = null;

	private String size = null;

	private String styleId = null;

	private String styleClass = null;

	private boolean indexed;

	private String readonly = null;

	
	public int doStartTag() throws JspException {

		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();
			HttpServletRequest request = (HttpServletRequest) this.pageContext
					.getRequest();

			Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("size", this.size);
			attributes.put("onchange", this.onchange);
			attributes.put("id", this.styleId);
			attributes.put("class", this.styleClass);
			attributes.put("name", this.propiedad);

			if (this.readonly != null && this.readonly.equals("true")) {
				attributes.put("onchange",
						"this.selectedIndex = this.initialSelect;");
				attributes.put("onfocus",
						"this.initialSelect = this.selectedIndex;");
			}

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);

			if (bean != null) {
				List<VersionCargaComboDTO> selectedList = null;
				List<VersionCargaComboDTO> list = null;
				
				String tipovehiculo = null;
				String marcaVehiculo = null;
				String tipoBien = null;
				
				MarcaVehiculoDTO marca = new MarcaVehiculoDTO();
				EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
				EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
				
				try {					
					tipoBien =  BeanUtils.getProperty(bean, this.tipoBien);
					tipovehiculo = BeanUtils.getProperty(bean, this.tipoVehiculo);
					marcaVehiculo = BeanUtils.getProperty(bean, this.marcaVehiculo);
					
					if ((tipovehiculo != null && !tipovehiculo.equals(""))
								&& (marcaVehiculo != null && !marcaVehiculo.equals(""))
								&& (tipoBien != null && !tipoBien.equals(""))) {
						marca.setIdTcMarcaVehiculo(UtileriasWeb.regresaBigDecimal(marcaVehiculo));
						
						estiloVehiculoId.setClaveTipoBien(tipoBien);
						
						estiloVehiculoDTO.setIdTcTipoVehiculo(UtileriasWeb.regresaBigDecimal(tipovehiculo));		
						estiloVehiculoDTO.setMarcaVehiculoDTO(marca);
						estiloVehiculoDTO.setId(estiloVehiculoId);
						try {
							list = beanRemoto.listarVersionCarga(estiloVehiculoDTO);
						} catch (NumberFormatException nfException) {
						} catch (NullPointerException npException) {
						} // End of try/catch
						if (list == null) {
							list = new ArrayList<VersionCargaComboDTO>();
						} // End of if
					} else {
						selectedList = new ArrayList<VersionCargaComboDTO>();
						list = new ArrayList<VersionCargaComboDTO>();
					}
					selectedList = this.getSelectedList(request, this.nombre,
							this.propiedad);
					boolean isReadOnly = false;
					this.sortVehicleCollection(list);
					this.sortVehicleCollection(selectedList);
					if (getReadonly() != null)
						isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
						
					if (!this.indexed)
						TagUtil.renderSelectTag(writer, attributes, list,
								"id", "description", selectedList,isReadOnly);
					else
						TagUtil.renderOptionTag(writer, list, "id",
								"description", selectedList);

				} catch (InvocationTargetException iException) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, iException);
				} catch (IllegalAccessException e) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, e);
				} catch (NoSuchMethodException e) {
					LogDeMidasWeb.log("Unable to find matching values",
							Level.FINEST, e);
				} catch (RemoteException rException) {
					throw new SystemException(rException);
				} // End of try/catch
			} // End of if
		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing version load catalog.",
					Level.FINEST, sException);
		} // End of try/catch

		return SKIP_BODY;
	}

	
	private List<VersionCargaComboDTO> getSelectedList(HttpServletRequest request, String name,
			String property) throws RemoteException, SystemException {
		TagUtils tagUtils = TagUtils.getInstance();
		List<VersionCargaComboDTO> selectedList = new ArrayList<VersionCargaComboDTO>();
		Object bean = null;		
		
		String tipovehiculo = null;
		String marcaVehiculo = null;
		String tipoBien = null;
		
		try {
			bean = tagUtils.lookup(this.pageContext, name, null);
			if (bean != null) {
				String[] selectedIds = BeanUtils.getArrayProperty(bean,
						property);
				tipoBien =  BeanUtils.getProperty(bean, this.tipoBien);
				tipovehiculo = BeanUtils.getProperty(bean, this.tipoVehiculo);
				marcaVehiculo = BeanUtils.getProperty(bean, this.marcaVehiculo);
				
				if (selectedIds != null) {
					if ((tipovehiculo != null && !tipovehiculo.equals(""))
							&& (marcaVehiculo != null && !marcaVehiculo.equals(""))
							&& (tipoBien != null && !tipoBien.equals(""))) {
						for (int i = 0; i < selectedIds.length; i++) {
							try {	
								MarcaVehiculoDTO marca = new MarcaVehiculoDTO();
								EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
								EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
								
								String id = selectedIds[i];							
								marca.setIdTcMarcaVehiculo(UtileriasWeb.regresaBigDecimal(marcaVehiculo));							
								estiloVehiculoId.setClaveTipoBien(tipoBien);	
								estiloVehiculoId.setIdVersionCarga(UtileriasWeb.regresaBigDecimal(id));
								estiloVehiculoDTO.setIdTcTipoVehiculo(UtileriasWeb.regresaBigDecimal(tipovehiculo));		
								estiloVehiculoDTO.setMarcaVehiculoDTO(marca);
								estiloVehiculoDTO.setId(estiloVehiculoId);
								
								VersionCargaComboDTO versionCargaComboDTO = beanRemoto.getVersionCarga(estiloVehiculoDTO);
								selectedList.add(versionCargaComboDTO);
								
							} catch (NumberFormatException nfException) {
							} // End of try/catch
						} // End of for
					} // End of if
				}// End of if
			} // End of if
		} catch (InvocationTargetException iException) {
			LogDeMidasWeb.log("Unable to find matching values", Level.FINEST,
					iException);
		} catch (IllegalAccessException e) {
			LogDeMidasWeb
					.log("Unable to find matching values", Level.FINEST, e);
		} catch (NoSuchMethodException e) {
			LogDeMidasWeb
					.log("Unable to find matching values", Level.FINEST, e);
		} catch (JspException jspException) {
			LogDeMidasWeb.log("Unable to find bean.", Level.FINEST,
					jspException);
		} // End of try/catch
		return selectedList;
	}

	public void release() {
		super.release();
		this.nombre = Constants.BEAN_KEY;
		this.propiedad = null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	public String getTipoBien() {
		return tipoBien;
	}

	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}

	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}
	
	
	private void sortVehicleCollection(List<VersionCargaComboDTO> list) {
		Collections.sort(list, new Comparator<Object>(){
			public int compare(Object o1, Object o2) {
				if(o1 instanceof EstiloVehiculoDTO && o2 instanceof EstiloVehiculoDTO) {
					return ((EstiloVehiculoDTO)o1).getDescription().compareTo(((EstiloVehiculoDTO)o2).getDescription());
				}
				return 0;
			}
		});
	}

}