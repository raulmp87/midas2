package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CuentaBancoAction extends BaseEtiquetaAction {
	public void escribeComboPesos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");				
		
		CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();

		try {
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();
			cuentaBancoDTO.setIdTcMoneda(new BigDecimal(Sistema.MONEDA_PESOS));
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(id));
			cuentaBancoDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
			List<CuentaBancoDTO> list = cuentaBancoDN.listarFiltrado(cuentaBancoDTO);
			LogDeMidasWeb.log("CuentaBanco list size for idTcReaseguradorCorredor(" + id + ") AND idTcMoneda("+Sistema.MONEDA_PESOS+") = " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<CuentaBancoDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				CuentaBancoDTO dto = (CuentaBancoDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(dto.getIdtccuentabanco());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(dto.getNumerocuenta());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} catch (ExcepcionDeAccesoADatos edaad) {
			throw new SystemException("DB Error - Unable to render select tag",
					edaad);
		}// End of try/catch
	}
	
	public void escribeComboDolares(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");				
		
		CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();

		try {
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();
			cuentaBancoDTO.setIdTcMoneda(new BigDecimal(Sistema.MONEDA_DOLARES));
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(id));
			cuentaBancoDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
			List<CuentaBancoDTO> list = cuentaBancoDN.listarFiltrado(cuentaBancoDTO);
			LogDeMidasWeb.log("CuentaBanco list size for idTcReaseguradorCorredor(" + id + ") AND idTcMoneda("+Sistema.MONEDA_DOLARES+") = " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<CuentaBancoDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				CuentaBancoDTO dto = (CuentaBancoDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(dto.getIdtccuentabanco());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(dto.getNumerocuenta());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} catch (ExcepcionDeAccesoADatos edaad) {
			throw new SystemException("DB Error - Unable to render select tag",
					edaad);
		}// End of try/catch
	}
}
