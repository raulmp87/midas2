package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDN;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.catalogos.ajustador.AjustadorDN;
import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.catalogos.clasificacionembarcacion.ClasificacionEmbarcacionDN;
import mx.com.afirme.midas.catalogos.clasificacionembarcacion.ClasificacionEmbarcacionDTO;
import mx.com.afirme.midas.catalogos.eventocatastrofico.EventoCatastroficoDN;
import mx.com.afirme.midas.catalogos.giro.GiroDN;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.girorc.GiroRCDN;
import mx.com.afirme.midas.catalogos.girorc.GiroRCDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.tipobandera.TipoBanderaDN;
import mx.com.afirme.midas.catalogos.tipobandera.TipoBanderaDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDN;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.catalogos.tipodestinotransporte.TipoDestinoTransporteDN;
import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDN;
import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDTO;
import mx.com.afirme.midas.catalogos.tipoembarcacion.TipoEmbarcacionDN;
import mx.com.afirme.midas.catalogos.tipoembarcacion.TipoEmbarcacionDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.TipoEquipoContratistaDN;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.TipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tiponavegacion.TipoNavegacionDN;
import mx.com.afirme.midas.catalogos.tiponavegacion.TipoNavegacionDTO;
import mx.com.afirme.midas.catalogos.tiporecipientepresion.TipoRecipientePresionDN;
import mx.com.afirme.midas.catalogos.tiporecipientepresion.TipoRecipientePresionDTO;
import mx.com.afirme.midas.catalogos.zonahidro.ZonaHidroDN;
import mx.com.afirme.midas.catalogos.zonahidro.ZonaHidroDTO;
import mx.com.afirme.midas.catalogos.zonasismo.ZonaSismoDN;
import mx.com.afirme.midas.catalogos.zonasismo.ZonaSismoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.consultas.tipoaeronave.TipoAeronaveDN;
import mx.com.afirme.midas.consultas.tipoaeronave.TipoAeronaveDTO;
import mx.com.afirme.midas.consultas.tipolicenciapiloto.TipoLicenciaPilotoDN;
import mx.com.afirme.midas.consultas.tipolicenciapiloto.TipoLicenciaPilotoDTO;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.ConceptoGastoDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.ConceptoIngresoDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

public class ComboDN {

	private static final ComboDN INSTANCIA = new ComboDN();

	private static final int 
			ZONA_HIDRO = 1, 
			REASEGURADOR_CORREDOR = 2,
			PARTICIPACION = 3, 
			TIPO_RECIPIENTE_PRESION = 4,
			TIPO_EQUIPO_CONTRATISTA = 5, 
			MONEDA = 6, 
			ZONA_SISMO = 7, 
			GIRO = 8,
			REASEGURADOR = 9,
			TIPO_BANDERA = 10,
			TIPO_NAVEGACION = 11,
			TIPO_EMBARCACION = 12,
			CLASIFICACION_EMBARCACION = 13,
			TIPO_AERONAVE = 14,
			TIPO_LICENCIA_PILOTO = 15,
			FORMA_PAGO = 16,
			MEDIO_PAGO = 17,
			GIRO_RC = 18,
			TIPO_DESTINO_TRANSPORTE = 19,
			EVENTO_CATASTROFICO = 20,
			COORDINADORES = 21,
			AJUSTADORES = 22,
			TIPO_DOCUMENTO_SINIESTRO = 23,
			CONCEPTOS_GASTOS = 24,
			PRESTADOR_SERVICIOS = 25,
			CONCEPTOS_INGRESOS = 26,
			SUSCRIPTORES = 27,
			TIPO_BIEN_AUTO = 28,
			CALIFICACIONES_REAS = 29;

	public ComboDN() {
	}

	public static ComboDN getInstance() {
		return INSTANCIA;
	}

	@SuppressWarnings("unchecked")
	public List listar(String nombreCatalogo, Usuario usuario) throws SystemException,
			ExcepcionDeAccesoADatos {

		int claveCatalogo = this.getClaveCatalogo(nombreCatalogo);
		switch (claveCatalogo) {
		case ZONA_HIDRO:
			ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
			return zonaHidroDN.listarTodos();
		case REASEGURADOR_CORREDOR:
			ReaseguradorCorredorDN rDN = ReaseguradorCorredorDN.getInstancia();
			return rDN.listarTodos();
		case PARTICIPACION:
			return null;
		case TIPO_RECIPIENTE_PRESION:
			TipoRecipientePresionDN tipoRecPresionDN = TipoRecipientePresionDN
					.getInstancia();
			return tipoRecPresionDN.listarTodos();
		case TIPO_EQUIPO_CONTRATISTA:
			TipoEquipoContratistaDN tipoEquipoContratistaDN = TipoEquipoContratistaDN
					.getInstancia();
			return tipoEquipoContratistaDN.listarTodos();
		case MONEDA:
			MonedaDN monedaDN = MonedaDN.getInstancia();
			return monedaDN.listarTodos();
		case ZONA_SISMO:
			ZonaSismoDN zonaSismoDN = ZonaSismoDN.getInstancia();
			return zonaSismoDN.listarTodos();
		case GIRO:
			GiroDN giroDN = GiroDN.getInstancia();
			return giroDN.listarTodos();
		case TIPO_AERONAVE:
			TipoAeronaveDN tipoAeronaveDN = TipoAeronaveDN.getInstancia();
			return tipoAeronaveDN.listarTodos();
		case TIPO_LICENCIA_PILOTO:
			TipoLicenciaPilotoDN tipoLicenciaPilotoDN = TipoLicenciaPilotoDN.getInstancia();
			return tipoLicenciaPilotoDN.listarTodos();
		case REASEGURADOR:
			ReaseguradorCorredorDN reaseguradorDN = ReaseguradorCorredorDN.getInstancia();
			ReaseguradorCorredorDTO reaseguradorDTO = new ReaseguradorCorredorDTO();
			reaseguradorDTO.setTipo(Sistema.PARTICIPANTE_REASEGURADOR+"");
			return reaseguradorDN.listarFiltrado(reaseguradorDTO);
		case TIPO_BANDERA:
			TipoBanderaDN tipoBanderaDN = TipoBanderaDN.getInstancia();
			return tipoBanderaDN.listarTodos();
		case TIPO_NAVEGACION:
			TipoNavegacionDN tipoNavegacionDN = TipoNavegacionDN.getInstancia();
			return tipoNavegacionDN.listarTodos();
		case TIPO_EMBARCACION:
			TipoEmbarcacionDN tipoEmbarcacionDN = TipoEmbarcacionDN.getInstancia();
			return tipoEmbarcacionDN.listarTodos();
		case CLASIFICACION_EMBARCACION:
			ClasificacionEmbarcacionDN clasificacionEmbarcacionDN = ClasificacionEmbarcacionDN.getInstancia();
			return clasificacionEmbarcacionDN.listarTodos();
		case FORMA_PAGO:
			FormaPagoDN formaPagoDN = FormaPagoDN.getInstancia();
			return formaPagoDN.listarTodos();
		case MEDIO_PAGO:
			MedioPagoDN medioPagoDN = MedioPagoDN.getInstancia();
			return medioPagoDN.listarTodos();
		case GIRO_RC:
			GiroRCDN giroRCDN = GiroRCDN.getInstancia();
			return giroRCDN.listarTodos();
		case TIPO_DESTINO_TRANSPORTE:
			TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
			return tipoDestinoTransporteDN.listarTodos();
		case EVENTO_CATASTROFICO:
			EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
			return eventoCatastroficoDN.listarTodos();
		case COORDINADORES:
			return UsuarioWSDN.getInstancia().obtieneUsuariosSinRolesPorNombreRol(Sistema.ROL_COORDINADOR_SINIESTROS, usuario.getNombreUsuario(), usuario.getIdSesionUsuario());
		case AJUSTADORES:
			AjustadorDN ajustadorDN = AjustadorDN.getInstancia();
			return ajustadorDN.ajustadoresPorEstatus(AjustadorDTO.ACTIVO);
		case TIPO_DOCUMENTO_SINIESTRO:
			TipoDocumentoSiniestroDN tipoDocumentoSiniestroDN = TipoDocumentoSiniestroDN.getInstancia();
			return tipoDocumentoSiniestroDN.listarTodos();
		case CONCEPTOS_GASTOS:
			ConceptoGastoDN  conceptoGastoDN = ConceptoGastoDN.getInstancia();
			return conceptoGastoDN.findAll();
		case PRESTADOR_SERVICIOS:
			PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN.getInstancia();
			return prestadorServiciosDN.listarPrestadores(null, usuario.getNombreUsuario());
		case CONCEPTOS_INGRESOS:
			ConceptoIngresoDN conceptoIngresoDN = ConceptoIngresoDN.getInstancia();
			return conceptoIngresoDN.findAll();
		case SUSCRIPTORES:
			return UsuarioWSDN.getInstancia()
					.obtieneUsuariosSinRolesPorNombreRol(
							usuario.getNombreUsuario(),
							usuario.getIdSesionUsuario(),
							Sistema.ROL_SUSCRIPTOR_COT,
							Sistema.ROL_AGENTE_EXTERNO,
							Sistema.ROL_SUSCRIPTOR_EXT);	
		case TIPO_BIEN_AUTO:
			TipoBienAutosDN tipoBienAutosDN = TipoBienAutosDN.getInstancia();
			return tipoBienAutosDN.listarTodos();
		case CALIFICACIONES_REAS:
			AgenciaCalificadoraDN aDN = AgenciaCalificadoraDN.getInstancia();
			return aDN.listarTodos();
			
		default:
			throw new RuntimeException("Falta definir el manejador para la clave de catalogo '" + claveCatalogo + "'.");
		}
	}

	public Object getPorId(String nombreCatalogo, BigDecimal id)
			throws SystemException, ExcepcionDeAccesoADatos {

		switch (this.getClaveCatalogo(nombreCatalogo)) {
		case ZONA_HIDRO:
			ZonaHidroDN zonaHidroDN = ZonaHidroDN.getInstancia();
			ZonaHidroDTO zonaHidroDTO = new ZonaHidroDTO();
			zonaHidroDTO.setIdTcZonaHidro(id);
			return zonaHidroDN.getPorId(zonaHidroDTO);
		case REASEGURADOR_CORREDOR:
			ReaseguradorCorredorDN rDN = ReaseguradorCorredorDN.getInstancia();
			ReaseguradorCorredorDTO raDTO = new ReaseguradorCorredorDTO();
			raDTO.setIdtcreaseguradorcorredor(id);
			return rDN.obtenerReaseguradorPorId(raDTO);
		case PARTICIPACION:
			return null;
		case TIPO_RECIPIENTE_PRESION:
			TipoRecipientePresionDN tipoRecPresionDN = TipoRecipientePresionDN
					.getInstancia();
			TipoRecipientePresionDTO tipoRecPresion = new TipoRecipientePresionDTO();
			tipoRecPresion.setIdTipoRecipientePresion(id);
			return tipoRecPresionDN.getTipoRecPresionPorId(tipoRecPresion);
		case TIPO_EQUIPO_CONTRATISTA:
			TipoEquipoContratistaDN tipoEquipoContratistaDN = TipoEquipoContratistaDN
					.getInstancia();
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO = new TipoEquipoContratistaDTO();
			tipoEquipoContratistaDTO.setIdTipoEquipoContratista(id);
			return tipoEquipoContratistaDN
					.getTipoEqContrPorId(tipoEquipoContratistaDTO);
		case MONEDA:
			MonedaDN monedaDN = MonedaDN.getInstancia();
			return monedaDN.getPorId(id.shortValue());
		case ZONA_SISMO:
			ZonaSismoDN zonaSismoDN = ZonaSismoDN.getInstancia();
			ZonaSismoDTO zonaSismoDTO = new ZonaSismoDTO();
			zonaSismoDTO.setIdZonaSismo(id);
			return zonaSismoDN.getZonaSismoPorId(zonaSismoDTO);
		case GIRO:
			GiroDN giroDN = GiroDN.getInstancia();
			GiroDTO giroDTO = new GiroDTO();
			giroDTO.setIdTcGiro(id);
			return giroDN.getGiroPorId(giroDTO);
		case TIPO_AERONAVE:
			TipoAeronaveDN  tipoAeronaveDN = TipoAeronaveDN.getInstancia();
			TipoAeronaveDTO tipoAeronaveDTO = new TipoAeronaveDTO();
			tipoAeronaveDTO.setIdTipoAeronave(id);
			return tipoAeronaveDN.getPorId(tipoAeronaveDTO);
		case TIPO_LICENCIA_PILOTO:
			TipoLicenciaPilotoDN  tipoLicenciaPilotoDN  = TipoLicenciaPilotoDN.getInstancia();
			TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO = new TipoLicenciaPilotoDTO();
			tipoLicenciaPilotoDTO.setIdTipoLicenciaPiloto(id);
			return tipoLicenciaPilotoDN.getPorId(tipoLicenciaPilotoDTO);
					case REASEGURADOR:
			ReaseguradorCorredorDN reaseguradorDN = ReaseguradorCorredorDN.getInstancia();
			ReaseguradorCorredorDTO reaseguradorDTO = new ReaseguradorCorredorDTO();
			reaseguradorDTO.setIdtcreaseguradorcorredor(id);
			return reaseguradorDN.obtenerReaseguradorPorId(reaseguradorDTO);
		case TIPO_BANDERA:
			TipoBanderaDN tipoBanderaDN = TipoBanderaDN.getInstancia();
			TipoBanderaDTO tipoBanderaDTO = new TipoBanderaDTO();
			tipoBanderaDTO.setIdTcTipoBandera(id);
			return tipoBanderaDN.getPorId(tipoBanderaDTO);
		case TIPO_NAVEGACION:
			TipoNavegacionDN tipoNavegacionDN = TipoNavegacionDN.getInstancia();
			TipoNavegacionDTO tipoNavegacionDTO = new TipoNavegacionDTO();
			tipoNavegacionDTO.setIdTcTipoNavegacion(id);
			return tipoNavegacionDN.getPorId(tipoNavegacionDTO);			
		case TIPO_EMBARCACION:
			TipoEmbarcacionDN tipoEmbarcacionDN = TipoEmbarcacionDN.getInstancia();
			TipoEmbarcacionDTO tipoEmbarcacionDTO = new TipoEmbarcacionDTO();
			tipoEmbarcacionDTO.setIdTcTipoEmbarcacion(id);
			return tipoEmbarcacionDN.getPorId(tipoEmbarcacionDTO);			
		case CLASIFICACION_EMBARCACION:
			ClasificacionEmbarcacionDN clasificacionEmbarcacionDN = ClasificacionEmbarcacionDN.getInstancia();
			ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO = new ClasificacionEmbarcacionDTO();
			clasificacionEmbarcacionDTO.setIdTcClasificacionEmbarcacion(id);
			return clasificacionEmbarcacionDN.getPorId(clasificacionEmbarcacionDTO);
		case FORMA_PAGO:
			FormaPagoDN formaPagoDN   = FormaPagoDN.getInstancia();
			FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
			formaPagoDTO.setIdFormaPago(id.intValue());
			return formaPagoDN.getPorId(formaPagoDTO);
		case MEDIO_PAGO:
			MedioPagoDN medioPagoDN   = MedioPagoDN.getInstancia();
			MedioPagoDTO medioPagoDTO = new MedioPagoDTO();
			medioPagoDTO.setIdMedioPago(id.intValue());
			return medioPagoDN.getPorId(medioPagoDTO);
		case GIRO_RC:
			GiroRCDN giroRCDN = GiroRCDN.getInstancia();
			GiroRCDTO giroRCDTO = new GiroRCDTO();
			giroRCDTO.setIdTcGiroRC(id);
			return giroRCDN.getGiroRCPorId(giroRCDTO);
		case TIPO_DESTINO_TRANSPORTE:
			TipoDestinoTransporteDN tipoDestinoTransporteDN = TipoDestinoTransporteDN.getInstancia();
			TipoDestinoTransporteDTO tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
			tipoDestinoTransporteDTO.setIdTipoDestinoTransporte(id);
			return tipoDestinoTransporteDN.getTipoDestinoTransportePorId(tipoDestinoTransporteDTO);
		case EVENTO_CATASTROFICO:
			EventoCatastroficoDN eventoCatastroficoDN = EventoCatastroficoDN.getInstancia();
			return eventoCatastroficoDN.getPorId(id);
		case COORDINADORES:
			return UsuarioWSDN.getInstancia().obtieneUsuario(id.intValue());
		case AJUSTADORES:
			AjustadorDN ajustadorDN = AjustadorDN.getInstancia();
			AjustadorDTO ajustadorDTO = new AjustadorDTO();
			ajustadorDTO.setIdTcAjustador(id);
			return ajustadorDN.getPorId(ajustadorDTO);
		case TIPO_DOCUMENTO_SINIESTRO:
			TipoDocumentoSiniestroDN tipoDocumentoSiniestroDN = TipoDocumentoSiniestroDN.getInstancia();
			TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO = new TipoDocumentoSiniestroDTO();
			tipoDocumentoSiniestroDTO.setIdTcTipoDocumentoSiniestro(id);
			return tipoDocumentoSiniestroDN.getTipoDocumentoSiniestroPorId(tipoDocumentoSiniestroDTO);
		case CONCEPTOS_GASTOS:
			ConceptoGastoDN  conceptoGastoDN = ConceptoGastoDN.getInstancia();
			return conceptoGastoDN.findById(id);
		case PRESTADOR_SERVICIOS:
			PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN.getInstancia();			
			return prestadorServiciosDN.detallePrestador(id, "");
		case CONCEPTOS_INGRESOS:
			ConceptoIngresoDN conceptoIngresoDN = ConceptoIngresoDN.getInstancia();
			return conceptoIngresoDN.findById(id);
		case CALIFICACIONES_REAS:
			AgenciaCalificadoraDN aDN = AgenciaCalificadoraDN.getInstancia();
			AgenciaCalificadoraDTO agenciaCalificadoraDTO =  new AgenciaCalificadoraDTO();
			agenciaCalificadoraDTO.setIdagencia(id);
			return aDN.obtenerAgenciaPorId(agenciaCalificadoraDTO); 
		default:
			throw new RuntimeException("La clave del catalogo '" + nombreCatalogo + "' no es correcta.");
		}
	}
	
	
	public Object getPorId(String nombreCatalogo, String id)
	throws SystemException, ExcepcionDeAccesoADatos {
		switch (this.getClaveCatalogo(nombreCatalogo)) {
		case TIPO_BIEN_AUTO:
			TipoBienAutosDN tipoBienAutosDN = new TipoBienAutosDN();
			TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
			tipoBienAutosDTO.setClaveTipoBien(id);
			return tipoBienAutosDN.getTipoBienAutosPorId(tipoBienAutosDTO);
		default:
			throw new RuntimeException("La clave del catalogo '" + nombreCatalogo + "' no es correcta.");
		}
	}

	public int getClaveCatalogo(String catalogo) {
		if (catalogo.equals("tczonahidro"))
			return ZONA_HIDRO;
		else if (catalogo.equals("treaseguradorcorredor"))
			return REASEGURADOR_CORREDOR;
		else if (catalogo.equals("contacto"))
			return PARTICIPACION;
		else if (catalogo.equals("tctiporecipientepresion"))
			return TIPO_RECIPIENTE_PRESION;
		else if (catalogo.equals("tctipoequipocontratista"))
			return TIPO_EQUIPO_CONTRATISTA;
		else if (catalogo.equals("vnmoneda"))
			return MONEDA;
		else if (catalogo.equals("tczonasismo"))
			return ZONA_SISMO;
		else if (catalogo.equals("tcgiro"))
			return GIRO;
		else if(catalogo.equals("tctipoaeronave"))
			return TIPO_AERONAVE;
		else if(catalogo.equals("tctipolicenciapiloto"))
			return TIPO_LICENCIA_PILOTO;
		else if (catalogo.equals("tcreasegurador"))
			return REASEGURADOR;
		else if (catalogo.equals("tctipobandera"))
			return TIPO_BANDERA;
		else if (catalogo.equals("tctiponavegacion"))
			return TIPO_NAVEGACION;
		else if (catalogo.equals("tctipoembarcacion"))
			return TIPO_EMBARCACION;
		else if (catalogo.equals("tcclasificacionembarcacion"))
			return CLASIFICACION_EMBARCACION;
		else if(catalogo.equals("vnformapago"))
			return FORMA_PAGO;
		else if(catalogo.equals("vnmediopago"))
			return MEDIO_PAGO;
		else if (catalogo.equals("tcgirorc"))
			return GIRO_RC;
		else if (catalogo.equals("tctipodestinotransporte"))
			return TIPO_DESTINO_TRANSPORTE;
		else if (catalogo.equals("tceventocatastrofico"))
			return EVENTO_CATASTROFICO;
		else if (catalogo.equals("coordinadorSiniestros"))
			return COORDINADORES;
		else if (catalogo.equals("tcajustador"))
			return AJUSTADORES;
		else if (catalogo.equals("tctipodocumentosiniestro"))
			return TIPO_DOCUMENTO_SINIESTRO;
		else if (catalogo.equals("tcconceptogasto"))
			return CONCEPTOS_GASTOS;
		else if (catalogo.equals("tcprestadorservicios"))
			return PRESTADOR_SERVICIOS;
		else if (catalogo.equals("tcconceptoingreso"))
			return CONCEPTOS_INGRESOS;
		else if (catalogo.equals("rol_suscriptor_cot"))
			return SUSCRIPTORES;
		else if (catalogo.equals("tctipobienautos"))
			return TIPO_BIEN_AUTO;
		else if (catalogo.equals("trAgencia"))
			return CALIFICACIONES_REAS;
		
		throw new RuntimeException("El nombre del catalogo '" + catalogo + "' no es correcto.");
	}
}
