package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dto.wrapper.VersionCargaComboDTO;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class VersionCargaAction extends BaseEtiquetaAction {
	
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		BigDecimal tipovehiculo = new BigDecimal(0);
		BigDecimal marcaVehiculo = new BigDecimal(0);
		String tipoBien = null;
		
		tipovehiculo = new BigDecimal(request.getParameter("tipoVehiculo"));
		marcaVehiculo = new BigDecimal(request.getParameter("marcaVehiculo"));
		tipoBien= request.getParameter("tipoBien");
		
		MarcaVehiculoDTO marca = new MarcaVehiculoDTO();
		marca.setIdTcMarcaVehiculo(marcaVehiculo);
		
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.setClaveTipoBien(tipoBien);
		
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		estiloVehiculoDTO.setIdTcTipoVehiculo(tipovehiculo);		
		estiloVehiculoDTO.setMarcaVehiculoDTO(marca);
		estiloVehiculoDTO.setId(estiloVehiculoId);
		
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		EstiloVehiculoFacadeRemote beanRemoto = serviceLocator.getEJB(EstiloVehiculoFacadeRemote.class);

		try {
			List<VersionCargaComboDTO> list = beanRemoto.listarVersionCarga(estiloVehiculoDTO);
			LogDeMidasWeb.log("Version Load list size for (IdTipoVehiculo "+ tipovehiculo + " IdMarcaVehiculo " + marcaVehiculo + " ClaveTipoBien " + tipoBien + ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<VersionCargaComboDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				VersionCargaComboDTO versionCargaComboTO = (VersionCargaComboDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(versionCargaComboTO.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(versionCargaComboTO.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}

}
