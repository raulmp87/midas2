package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CiudadAction extends BaseEtiquetaAction {
	public void escribeCombo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		String id = request.getParameter("id");

		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		CiudadFacadeRemote beanRemoto = serviceLocator.getEJB(CiudadFacadeRemote.class);

		try {
				
			List<CiudadDTO> list = beanRemoto.findByProperty("stateId", id);
			this.sortCityCollection(list);
			LogDeMidasWeb.log("City list size for stateId(" + id + ")= " + list.size(),Level.INFO, null);			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<CiudadDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				CiudadDTO ciudadDTO = (CiudadDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(ciudadDTO.getId());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(ciudadDTO.getDescription());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",
					ioException);
		} // End of try/catch
	}

	@SuppressWarnings("unchecked")
	private void sortCityCollection(List list) {
		Collections.sort(list, new Comparator(){
			public int compare(Object o1, Object o2) {
				if(o1 instanceof CiudadDTO && o2 instanceof CiudadDTO) {
					return ((CiudadDTO)o1).getDescription().compareTo(((CiudadDTO)o2).getDescription());
				}
				return 0;
			}
		});
	}
}
