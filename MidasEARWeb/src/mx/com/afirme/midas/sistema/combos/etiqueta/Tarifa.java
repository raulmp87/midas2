package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TagUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDTO;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaId;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonDTO;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonFacadeRemote;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

public class Tarifa extends SoporteBaseEtiqueta {

	/**
	 * @author Jorge Cano
	 * @since 14/08/2009
	 */
	private static final long serialVersionUID = -6792001625010461620L;

	private ConfiguracionTarifaFacadeRemote beanRemoto;

	private String nombre = Constants.BEAN_KEY;

	private String propiedad = null;

	private String idToRiesgo = null;

	private String idToConcepto = null;

	private String idBase = null;

	private String onchange = null;

	private String size = null;

	private String styleId = null;

	private String styleClass = null;

	private boolean indexed;

	private String readonly = null;

	private String onblur = null;

	public Tarifa() throws SystemException {
		LogDeMidasWeb.log("Entrando en la etiqueta Tarifa  - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(ConfiguracionTarifaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	@SuppressWarnings("unchecked")
	public int doStartTag() throws JspException {

		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();

			Map attributes = new HashMap();
			attributes.put("size", this.size);
			attributes.put("id", this.styleId);
			attributes.put("class", this.styleClass);
			attributes.put("name", this.propiedad);
			attributes.put("onblur", this.onblur);

			if (this.readonly != null && this.readonly.equals("true")) {
				attributes.put("onchange",
						"this.selectedIndex = this.initialSelect;");
				attributes.put("onfocus",
						"this.initialSelect = this.selectedIndex;");
			}

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);

			if (bean != null) {
				List selectedList = null;
				List<CacheableDTO> list = new ArrayList();
				boolean isReadOnly = false;
				try {
					String idRiesgo = BeanUtils.getProperty(bean, "idToRiesgo");
					String idConcepto = BeanUtils.getProperty(bean, "idConcepto");
					
					ConfiguracionTarifaDTO configuracion = null;
					ConfiguracionTarifaId id = new ConfiguracionTarifaId();
					id.setIdDato(Short.valueOf(this.idBase));
					id.setIdToRiesgo(UtileriasWeb.regresaBigDecimal(idRiesgo));
					id.setIdConcepto(UtileriasWeb.regresaBigDecimal(idConcepto));
					configuracion = beanRemoto.findById(id);
					
					String claseRemota;
					ServiceLocator serviceLocator;
					MidasInterfaceBase beanBase;

					switch (configuracion.getClaveTipoControl()){
						case 0: //combo deshabilitado
							TagUtil.renderHidden(writer, this.propiedad, "0");
							break;
						case 1:	//combo de cat�logo propio
							claseRemota = configuracion.getClaseRemota().trim();
							serviceLocator = ServiceLocator.getInstance();
							beanBase = serviceLocator.getEJB(claseRemota);
							//Settear un id para las listas que son hijas, cuyos datos provienen de un cascadeo
							if (claseRemota.indexOf(".SubGiroFacade") != -1){
								attributes.remove("id");
								attributes.put("id", "idTcSubGiro");
							}
							else if (claseRemota.indexOf(".SubtipoEquipoElectronico") != -1){
								attributes.remove("id");
								attributes.put("id", "idTcSubTipoEquipoElectronico");
							}
							else if (claseRemota.indexOf(".SubtipoEquipoContratista") != -1){
								attributes.remove("id");
								attributes.put("id", "idTcSubtipoEquipoContratista");
							}
							else if (claseRemota.indexOf(".SubtipoRecipientePresion") != -1){
								attributes.remove("id");
								attributes.put("id", "idTcSubTipoRecipientePresion");
							}
							else if (claseRemota.indexOf(".SubTipoMaquinaria") != -1){
								attributes.remove("id");
								attributes.put("id", "idTcSubtipoMaquinaria");
							}
							//Para las listas que no provienen de cascadeo, obtener la lista completa de registros
							else{
								list = beanBase.findAll();
								//Para las listas que son padres para cascadear, establecer el evento onchange correspondiente
								if (claseRemota.indexOf(".GiroFacadeRemote")!=-1){
									attributes.remove("onchange");
									attributes.put("onchange","getSubGiros(this,'idTcSubGiro');");
								}
								else if (claseRemota.indexOf(".TipoRecipientePresion")!=-1){
									attributes.remove("onchange");
									attributes.put("onchange","getSubTiposRecipientePresion(this,'idTcSubTipoRecipientePresion');");
								}
								else if (claseRemota.indexOf(".EquipoElectronico")!=-1){
									attributes.remove("onchange");
									attributes.put("onchange","getSubTiposEquipoElectronico(this,'idTcSubTipoEquipoElectronico');");
								}
								else if (claseRemota.indexOf(".TipoEquipoContratista")!=-1){
									attributes.remove("onchange");
									attributes.put("onchange","getSubTiposEquipoContratista(this,'idTcSubtipoEquipoContratista');");
								}
								else if (claseRemota.indexOf(".TipoMaquinaria")!=-1){
									attributes.remove("onchange");
									attributes.put("onchange","getSubTiposMaquinaria(this,'idTcSubtipoMaquinaria');");
								}
							}
							if (getReadonly() != null)
								isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
							if (!this.indexed)
								TagUtil.renderSelectTag(writer, attributes, list, "id", "description", selectedList,isReadOnly);
							else
								TagUtil.renderOptionTag(writer, list, "id",	"description", selectedList);
							break;
						case 2:	//combo de catalogo Valores Fijos (CatalogoValorFijoDTO)
							serviceLocator = ServiceLocator.getInstance();
							CatalogoValorFijoFacadeRemote beanCatalogoValorFijo = serviceLocator.getEJB(CatalogoValorFijoFacadeRemote.class);
							List<CatalogoValorFijoDTO> listaCatalogosFijos = beanCatalogoValorFijo.findByProperty("id.idGrupoValores", configuracion.getIdGrupo());
							if (getReadonly() != null)
								isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
							if (!this.indexed)
								TagUtil.renderSelectTag(writer, attributes, listaCatalogosFijos, "id.idDato", "descripcion", selectedList,isReadOnly);
							else
								TagUtil.renderOptionTag(writer, list, "id.idDato",	"descripcion", selectedList);
							break;
						case 3:	//combo de EscalonDTO
							serviceLocator = ServiceLocator.getInstance();
							EscalonFacadeRemote beanEscalon = serviceLocator.getEJB(EscalonFacadeRemote.class);
							//List<EscalonDTO> listaEscalon = beanEscalon.findAll();
							List<EscalonDTO> listaEscalon = beanEscalon.findByProperty("claveClasificacionEscalon", configuracion.getIdGrupo());
							if (getReadonly() != null)
								isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
							if (!this.indexed)
								TagUtil.renderSelectTagEscalon(writer, attributes, listaEscalon, "idToEscalon", "description", selectedList,isReadOnly,configuracion.getClaveTipoValidacion());
							else
								TagUtil.renderOptionTag(writer, listaEscalon, "idToEscalon",	"description", selectedList);
							break;
						case 4:	//caja de texto
							attributes.remove("size");
							attributes.remove("onblur");
							String onblur="";
							switch (configuracion.getClaveTipoValidacion()){
								case 1:	//cuota al millar, validar 8 digitos con 4 decimales
									onblur="validarDecimal(this.form."+this.propiedad+", this.value, 8, 4)";
									break;
								case 2:	//porcentaje, validar 8 digitos con 2 decimales
									onblur="validarDecimal(this.form."+this.propiedad+", this.value, 8, 2)";
									break;
								case 3:	//importe. validar 16 digitos con 2 decimales 
									onblur="validarDecimal(this.form."+this.propiedad+", this.value, 16, 2)";
									break;
								case 4:	//factor, valodar 16 d�gitos con 4 decimales
									onblur="validarDecimal(this.form."+this.propiedad+", this.value, 16, 4)";
									break;
								case 5:	//DSMGVDF, validar entero de 8 d�gitos
									onblur="validarDecimal(this.form."+this.propiedad+", this.value, 8, 0)";
									break;
							}
							attributes.put("onblur", onblur);
							TagUtil.renderTextBox(writer, attributes);
							break;
					}
					
				} catch (ClassNotFoundException e) {
					throw new SystemException(e);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			} // End of if

		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing tarifa catalog.",
					Level.FINEST, sException);
		} // End of try/catch

		return SKIP_BODY;
	}

	public void release() {
		super.release();
		this.nombre = Constants.BEAN_KEY;
		this.propiedad = null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

	public String getOnblur() {
		return onblur;
	}

	public void setOnblur(String onblur) {
		this.onblur = onblur;
	}

	public String getIdToRiesgo() {
		return idToRiesgo;
	}

	public void setIdToRiesgo(String idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	public String getIdToConcepto() {
		return idToConcepto;
	}

	public void setIdToConcepto(String idToConcepto) {
		this.idToConcepto = idToConcepto;
	}

	public String getIdBase() {
		return idBase;
	}

	public void setIdBase(String idBase) {
		this.idBase = idBase;
	}

}
