package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.transaction.SystemException;

import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.TagUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;


import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.html.Constants;

public class AgenciasCalificadoras extends SoporteBaseEtiqueta {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AgenciasCalificadoras() throws SystemException {
		LogDeMidasWeb.log("Entrando en la etiqueta CalificacionesReas  - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		try {
			beanRemoto = serviceLocator.getEJB(AgenciaCalificadoraFacadeRemote.class);
		} catch (mx.com.afirme.midas.sistema.SystemException e) {
			LogDeMidasWeb.log("Error Remoto instanciado", Level.WARNING, e);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	private AgenciaCalificadoraFacadeRemote beanRemoto;

	private String nombre = Constants.BEAN_KEY;

	private String propiedad = null;

	private String onchange = null;

	private String size = null;

	private String styleId = null;

	private String styleClass = null;

	private boolean indexed;

	private String readonly = null;

	@SuppressWarnings("unchecked")
	public int doStartTag() throws JspException {

		try {
			JspWriter writer = this.pageContext.getOut();
			TagUtils tagUtils = TagUtils.getInstance();
			Map attributes = new HashMap();
			attributes.put("size", this.size);
			attributes.put("onchange", this.onchange);
			attributes.put("id", this.styleId);
			attributes.put("class", this.styleClass);
			attributes.put("name", this.propiedad);
			
			if (this.readonly != null && this.readonly.equals("true")) {
				attributes.put("onchange",
						"this.selectedIndex = this.initialSelect;");
				attributes.put("onfocus",
						"this.initialSelect = this.selectedIndex;");
			}

			Object bean = tagUtils.lookup(this.pageContext, this.nombre, null);

			if (bean != null) {
				List selectedList = null;
				List list = null;
				
					selectedList = new ArrayList();
					list = new ArrayList();
					try {
						list = this.getSelectedList();
					} catch (RemoteException e) {
						LogDeMidasWeb.log("Unknown error while writing remote bean.",
							Level.FINEST, e);
						e.printStackTrace();
					}
					boolean isReadOnly = false;
					if (getReadonly() != null)
						isReadOnly = Boolean.valueOf(getReadonly().toString()).booleanValue();
					
					String selectedId = BeanUtils.getProperty(bean, this.propiedad);
					if(!UtileriasWeb.esCadenaVacia(selectedId)){ 
						AgenciaCalificadoraDTO calificacionAgenciaDTO = beanRemoto.findById(selectedId);
						if (!UtileriasWeb.esObjetoNulo(calificacionAgenciaDTO)){
							selectedList = new ArrayList();
							selectedList.add(calificacionAgenciaDTO);
						}
					}
						
					if (!this.indexed)
						TagUtil.renderSelectTag(writer, attributes, list,
								"idagencia", "nombreAgencia", selectedList,isReadOnly);
					else
						TagUtil.renderOptionTag(writer, list, "idagencia",
								"nombreAgencia", selectedList);
			} // End of if
		} catch (SystemException sException) {
			LogDeMidasWeb.log("Unknown error while writing city catalog.",
					Level.FINEST, sException);
		} // End of try/catch
		catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (mx.com.afirme.midas.sistema.SystemException e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		}

		return SKIP_BODY;
	}

	@SuppressWarnings("unchecked")
	private List getSelectedList() throws RemoteException, SystemException {
		List<AgenciaCalificadoraDTO> selectedList = beanRemoto.findAll();
		return selectedList;
	}

	public void release() {
		super.release();
		this.nombre = Constants.BEAN_KEY;
		this.propiedad = null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}
	
	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStyleId() {
		return styleId;
	}

	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

}
