package mx.com.afirme.midas.sistema.combos.etiqueta;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoDN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
/**
 * 
 * @author Jose Luis Arellano
 * @since 3 de septiembre de 2009
 *
 */
public class MonedaAction extends BaseEtiquetaAction {

	public void escribeComboTipoPoliza(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		String id = request.getParameter("id");
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		MonedaFacadeRemote beanRemoto = serviceLocator.getEJB(MonedaFacadeRemote.class);

		try {
			List<MonedaDTO> list = beanRemoto.findRelatedTipoPoliza(UtileriasWeb.regresaBigDecimal(id));
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<MonedaDTO> iteratorList = list.iterator();
			while (iteratorList.hasNext()) {
				MonedaDTO monedaDTO = (MonedaDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(monedaDTO.getIdTcMoneda());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(monedaDTO.getDescripcion());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",ioException);
		} // End of try/catch
	}
	
	public void escribeComboFormaPagoPorMoneda(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		String id = request.getParameter("id");
		try {
			List<FormaPagoIDTO> listaFormaPago = FormaPagoDN.getInstancia(id).listarTodos(new Short(id));
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			Iterator<FormaPagoIDTO> iteratorList = listaFormaPago.iterator();
			while (iteratorList.hasNext()) {
				FormaPagoIDTO formaPagoIDTO = (FormaPagoIDTO) iteratorList.next();
				buffer.append("<item>");
				buffer.append("<id>");
				buffer.append(formaPagoIDTO.getIdFormaPago());
				buffer.append("</id>");
				buffer.append("<description><![CDATA[");
				buffer.append(formaPagoIDTO.getDescripcion());
				buffer.append("]]></description>");
				buffer.append("</item>");
			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",ioException);
		} // End of try/catch
	}
	
}
