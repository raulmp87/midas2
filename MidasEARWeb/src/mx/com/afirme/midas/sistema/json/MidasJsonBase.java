package mx.com.afirme.midas.sistema.json;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.util.CollectionUtils;

/**
 * Clase usada para representar un conjunto de filas de datos en Json.
 * 
 * @author Fernando Alonzo
 * @since 07 de Octubre de 2009
 */
public class MidasJsonBase {
	private List<MidasJsonRow> rows = new ArrayList<MidasJsonRow>();

	public void setRows(List<MidasJsonRow> rows) {
		this.rows = rows;
	}

	public List<MidasJsonRow> getRows() {
		return rows;
	}

	public boolean addRow(MidasJsonRow row) {
		return rows.add(row);
	}

	public String toString() {
		final StringBuilder json = new StringBuilder();
		json.append("{rows:[");
		if(CollectionUtils.isEmpty(rows)) {
			json.append("]}");
		} else {
			for (MidasJsonRow row : rows) {
				json.append(row).append(",");
			}
			json.deleteCharAt(json.length() - 1).append("]}");
		}
		return json.toString();
	}

	public String toXMLString() {
		StringBuilder xml = new StringBuilder("<rows>");
		if (rows != null && !rows.isEmpty()) {
			for (MidasJsonRow row : rows) {
				xml.append(row.toXMLString());
			}
		}
		xml.append("</rows>");
		return xml.toString();
	}
	
	public String toCSVString() {
		return toCSVString(",");
	}
	
	public String toCSVString(String delimitador) {
		StringBuilder csv = new StringBuilder("");
		if (rows != null && !rows.isEmpty()) {
			for (MidasJsonRow row : rows) {
				csv.append(row.toCSVString(delimitador));
				csv.append("\n");
			}
		}
		return csv.toString();
	}
}
