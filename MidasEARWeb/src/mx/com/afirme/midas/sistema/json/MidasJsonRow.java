package mx.com.afirme.midas.sistema.json;

import mx.com.afirme.midas.sistema.UtileriasWeb;

/**
 * Clase usada para representar una fila de datos en Json.
 * 
 * @author Fernando Alonzo
 * @since 07 de Octubre de 2009
 */
public class MidasJsonRow {
	private String id;
	private String[] datos;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setDatos(String... datos) {
		this.datos = datos;
	}

	public String[] getDatos() {
		return datos;
	}

	public String toString() {
		final StringBuilder json = new StringBuilder("{id:\"");
		json.append(id).append("\",data:[");
		if (datos != null && datos.length > 0) {
			for (String dato : datos) {
				if (dato != null) {
					json.append("\"").append(dato.replaceAll("\"", "&#34;").replaceAll("\n", "&#13;")).append("\",");
				}
			}
			json.deleteCharAt(json.length()-1).append("]}");
		} else {
			json.append("]}");
		}
		return json.toString();
	}

	public static String generarLineaImagenDataGrid(String urlImg,
			String toolTip, String funcion, String destino) {
		String result = "";
		if (!UtileriasWeb.esCadenaVacia(urlImg))
			result += urlImg;
		else
			result += "/MidasWeb/img/blank.gif";
		if (!UtileriasWeb.esCadenaVacia(toolTip))
			result += "^" + toolTip;
		else
			result += "^";
		if (!UtileriasWeb.esCadenaVacia(funcion))
			result += "^javascript:" + funcion;
		else
			result += "^javascript:void(0);";
		if (!UtileriasWeb.esCadenaVacia(destino))
			result += "^" + destino;
		else
			result += "^_self";
		result = result.replaceAll("'", "&#39;");
		return result;
	}

	public String toXMLString() {
		StringBuilder xml = new StringBuilder("<row id=\"");
		xml.append(id).append("\">");
		if (datos != null && datos.length > 0) {
			for (String dato : datos) {
				if (dato != null) {
					xml.append("<cell>");
					xml.append(dato.replaceAll("\"", "&#34;")
							.replaceAll("\n", "&#13;")
							.replaceAll("&", "&#38;")
							.replaceAll("'", "&#39;")
							.replaceAll("<", "&#60;")
							.replaceAll(">", "&#62;"));
					xml.append("</cell>");
				}
			}
		}
		xml.append("</row>");
		return xml.toString();
	}
	
	public String toCSVString(String delimitador) {
		StringBuilder csv = new StringBuilder();
		csv.append(id).append(delimitador);
		if (datos != null && datos.length > 0) {
			for (String dato : datos) {
				if (dato != null) {
					csv.append(dato.replaceAll("\"", "&#34;")
							.replaceAll("\n", "&#13;")
							.replaceAll("&", "&#38;")
							.replaceAll("'", "&#39;")
							.replaceAll("<", "&#60;")
							.replaceAll(">", "&#62;")).append(delimitador);
				}
			}
			return csv.substring(0, csv.length()-delimitador.length());
		}
		return csv.toString();
	}
}
