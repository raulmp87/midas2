package mx.com.afirme.midas.sistema;

import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import mx.com.afirme.midas.ambiente.Config;

/**
 * This class will retrieve requested resources.
 * 
 * @author Grupo de Desarrollo MIDAS
 * @version 1.00, March 31, 2009
 */
public final class ServiceLocator {

	private static final ServiceLocator INSTANCE = new ServiceLocator();

	/**
	 * Retrieves the <code>ServiceLocator</code> singleton instance.
	 * 
	 * @return The <code>ServiceLocator</code> singleton instance.
	 */
	public static ServiceLocator getInstance() {
		return ServiceLocator.INSTANCE;
	}

	private ServiceLocator() {
		// Prevents Instantiation.
	}

	@SuppressWarnings("unchecked")
	public <T extends Object> T getEJB(String iiop, Class<T> theInterface)
			throws SystemException {
		T service = null;
		InitialContext context = null;
		Object homeObject = null;
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY,
				"com.ibm.websphere.naming.WsnInitialContextFactory");
		env.put(javax.naming.Context.PROVIDER_URL, iiop);
		env.put(javax.naming.Context.SECURITY_PRINCIPAL,
				Sistema.ASM_SEGURIDAD_USUARIO);
		env.put(javax.naming.Context.SECURITY_CREDENTIALS,
				Sistema.ASM_SEGURIDAD_PASSWORD);
		try {
			context = new InitialContext(env);
		} catch (NamingException nException) {
			throw new SystemException("No es posible inicializar el contexto.",
					nException);
		}
		try {
			homeObject = context.lookup(theInterface.getName());
			service = (T) PortableRemoteObject.narrow(homeObject, theInterface);
		} catch (NamingException e) {
			throw new SystemException("No disponible la busqueda del objeto \""
					+ theInterface.getName() + "\".");
		} catch (Exception ex) {
			throw new SystemException("No disponible la busqueda del objeto \""
					+ theInterface.getName() + "\".");
		}
		return service;
	}

	/**
	 * Retrieves the requested Enterprise Java Bean associated to the given
	 * <code>reference</code>.
	 * 
	 * @param theInterface
	 *            The interface of the SSB.
	 * @return The stateless session bean implementation
	 * @throws SystemException
	 *             If the ejb could not be located.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> T getEJB(Class<T> theInterface)
			throws SystemException {
		T service = null;
		if (System.getProperty("java.vendor").startsWith("IBM Corporation")) {
			service = (T) NamingUtil.lookup(theInterface.getName());
		} else {
			String simpleName = theInterface.getSimpleName();
			simpleName = simpleName.replace("Remote", "Bean");
			return (T) NamingUtil.lookup(simpleName);
		} // End of if/else
		return service;
	}

	/**
	 * Retrieves the requested Enterprise Java Bean associated to the given
	 * <code>reference</code>.
	 * 
	 * @param theInterface
	 *            name of The interface of the SSB.
	 * @return The stateless session bean implementation
	 * @throws SystemException
	 *             If the ejb could not be located.
	 * @throws ClassNotFoundException
	 *             If the class could not be generated.
	 */

	@SuppressWarnings("unchecked")
	public <T extends Object> T getEJB(String theInterface)
			throws SystemException, ClassNotFoundException {

		Class remoteClass = Class.forName(theInterface);

		return (T) this.getEJB(remoteClass);
	}

	@SuppressWarnings("finally")
	public String obtenerVariableEntorno(String nombreVariable) {
		String myVariable = null;
		try {
			Config config = (Config) NamingUtil.lookup("MyConstants");
			myVariable = (String) config.getAttribute(nombreVariable);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return myVariable;
		}
	}
}
