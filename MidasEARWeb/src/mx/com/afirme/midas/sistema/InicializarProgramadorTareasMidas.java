package mx.com.afirme.midas.sistema;

import javax.servlet.ServletContextEvent;

import mx.com.afirme.midas.sistema.tareas.ProgramadorTareas;

import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;

public class InicializarProgramadorTareasMidas extends
		QuartzInitializerListener {

	
	public void contextInitialized(ServletContextEvent arg0) {
		super.contextInitialized(arg0);
		
		javax.servlet.ServletContext contexto = arg0.getServletContext();
		
		StdSchedulerFactory factory = (StdSchedulerFactory) contexto
        	.getAttribute(QuartzInitializerListener.QUARTZ_FACTORY_KEY);
		
		ProgramadorTareas programadorTareas = new ProgramadorTareas();
		programadorTareas.iniciaProgramacionTareas(factory);
		
		
	}
	
}
