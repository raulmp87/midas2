package mx.com.afirme.midas.sistema;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import sun.misc.BASE64Encoder;

public class ServicioContrasenia {
	
	private static ServicioContrasenia instance;
	
	  public ServicioContrasenia()

	  {
	
	  }
	
	  public synchronized String encrypt(String plaintext) throws Exception
	
	  {
	
	    MessageDigest md = null;
	
	    try
	
	    {
	
	      md = MessageDigest.getInstance("SHA"); //step 2
	
	    }

	    catch(NoSuchAlgorithmException e)
	
	    {

	      throw new Exception(e.getMessage());
	
	    }
	
	    try
	
	    {
	
	      md.update(plaintext.getBytes("UTF-8")); //step 3

	    }
	
	    catch(UnsupportedEncodingException e)
	 {
	
	      throw new Exception(e.getMessage());
	
	    }
	
	    byte raw[] = md.digest(); //step 4
	
	    String hash = (new BASE64Encoder()).encode(raw); //step 5
	
	    return hash; //step 6
	
	  }
	
	 
	
	  public static synchronized ServicioContrasenia getInstance() //step 1
	
	  {

	    if(instance == null)
	
	    {
	
	       instance = new ServicioContrasenia();
	
	    }
	
	    return instance;
	
	  }
	  

	  /**
	  #
	   
	  #
	      * generatePassword() method give you a random password.
	  #
	      * Using SecureRandom algoritham, generate a random number and add it with prj_name string
	  #
	      * for a temporary password.
	  #
	      *
	  #
	   
	  #
	  **/
	 
	  public String generaContrasenia() {
	
	  SecureRandom r = new SecureRandom();
	
	  int randnumb = r.nextInt();
	
	  if(randnumb<0)
	
	  randnumb=randnumb*(-1);
	
	  StringBuffer newPassword = new StringBuffer();

	  newPassword.append("prj_name");

	  newPassword.append(String.valueOf(randnumb));
	
	  return newPassword.toString();
	
	  }
	  

	}
	
	 
