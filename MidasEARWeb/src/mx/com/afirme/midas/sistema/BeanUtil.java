package mx.com.afirme.midas.sistema;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtilsBean;

import com.js.dto.AbstractDTO;

import mx.com.afirme.midas.sistema.SystemException;

/**
 * This class contains a series of functions specialized in bean handling.
 * 
 * @author Marsil Benavides
 * @version 1.00, March 27, 2009
 */
public final class BeanUtil {
  
  private static final PropertyUtilsBean BEAN_UTILS =
    new PropertyUtilsBean();

  private BeanUtil() {
    // Prevents Instantiation
  }
  
  private static boolean matches(Object object, String property,
      Object matchingValue) throws SystemException {
    boolean matches = true;
    try {
      Object value = BeanUtil.BEAN_UTILS.getProperty(object, property);
      if (matchingValue == null || value == null) {
        matches = false;
      } else {
        if (matchingValue.hashCode() != value.hashCode()) {
          matches = false;
        } else if (!matchingValue.equals(value)) {
          matches = false;
        } // End of if
      } // End of if/else
    } catch (IllegalAccessException iaException) {
      throw new SystemException("Unable to access property: " + property,
          iaException);
    } catch (InvocationTargetException itException) {
      throw new SystemException("Unable to invoke target property: "
          + property, itException);
    } catch (NoSuchMethodException nsMethodException) {
      throw new SystemException("Property " + property + " has no getter.",
          nsMethodException);
    } // End of try/catch
    return matches;
  }

  private static Method retrieveMethod(Class<?> theClass, String methodName,
      Object[] methodParameters) {
    Method method = null;
    Method[] classMethods = theClass.getMethods();
    int methodParametersSize = 0;
    if (methodParameters != null)
      methodParametersSize = methodParameters.length;
    for (int i = 0; i < classMethods.length; i++) {
      String name = classMethods[i].getName();
      int parameterSize = classMethods[i].getParameterTypes().length;
      boolean sameParameters = parameterSize == methodParametersSize;
      boolean sameName = name.equals(methodName);
      if (sameName && sameParameters) {
        method = classMethods[i];
        break;
      } // End of if()
    } // End of for()
    return method;
  }

  /**
   * Invokes the given <code>method</code> in the given <code>instance</code>
   * using the given <code>methodParameters</code>.
   * 
   * @param instance
   *         The object in which the method will be called.
   * @param methodName
   *         The name of the method to be executed.
   * @param methodParameters
   *         The parameters that the method recieves.
   * @return
   *         The method's return value. <code>null</code> if <code>void</code>
   *         or if the method returns <code>null</code>.
   * @exception IllegalAccessException
   *         if the <code>method</code> object enforces Java language access
   *         control and the underlying method is inaccessible.
   * @exception InvocationTargetException if the underlying method
   *         throws an exception.
   */
  public static Object invoke(Object instance, String methodName,
      Object[] methodParameters) throws IllegalAccessException,
      InvocationTargetException {
    Object result = null;
    Class<?> theClass = instance.getClass();
    Method method = BeanUtil.retrieveMethod(
        theClass, methodName, methodParameters);
    
    if (method != null) {
      result = method.invoke(instance, methodParameters);
    } else {
      throw new IllegalAccessException("Unknown method \"" + methodName
          + "\" for instance: [" + instance + "]");
    } // End of if/else()

    return result;
  }
  
  /**
   * Invokes the given <code>method</code> in the given <code>instance</code>
   * using the given <code>methodParameter</code>.
   * 
   * @param instance
   *         The object in which the method will be called.
   * @param methodName
   *         The name of the method to be executed.
   * @param methodParameter
   *         The parameter that the method recieves.
   * @return
   *         The method's return value. <code>null</code> if <code>void</code>
   *         or if the method returns <code>null</code>.
   * @exception IllegalAccessException
   *         if the <code>method</code> object enforces Java language access
   *         control and the underlying method is inaccessible.
   * @exception InvocationTargetException if the underlying method
   *         throws an exception.
   */
  public static Object invoke(Object instance, String methodName,
      Object methodParameter) throws IllegalAccessException,
      InvocationTargetException {
    return BeanUtil.invoke(instance, methodName, new Object[]{methodParameter});
  }

  /**
   * Invokes the given <code>method</code> in the given <code>instance</code>
   * with no associated parameters.
   * 
   * @param instance
   *         The object in which the method will be called.
   * @param methodName
   *         The name of the method to be executed.
   * @return
   *         The method's return value. <code>null</code> if <code>void</code>
   *         or if the method returns <code>null</code>.
   * @exception IllegalAccessException
   *         if the <code>method</code> object enforces Java language access
   *         control and the underlying method is inaccessible.
   * @exception InvocationTargetException if the underlying method
   *         throws an exception.
   */
  public static Object invoke(Object instance, String methodName)
      throws IllegalAccessException, InvocationTargetException {
    Object[] methodParameters = null;
    return BeanUtil.invoke(instance, methodName, methodParameters);
  }

  /**
   * Retrieves a new <code>List</code> of mathching elements within the given
   * <code>collection</code>. The <code>matchingValues</code> bi-dimensional
   * Array must be filled with an "attribute name" - "matching value" pair.
   * 
   * @param collection
   *          The <code>Collection</code> in which the matching elements will be
   *          looked-up.
   * @param matchingValues
   *          "attribute name" - "matching value" pair.
   * @return A new <code>List</code> of mathching elements.
   * @throws SystemException
   *           If an unexpected error ocurrs.
   */
  public static <T extends AbstractDTO> List<T> getMatchingList(
      Collection<T> collection, Object[][] matchingValues)
      throws SystemException {
    List<T> matchingList = new ArrayList<T>();
    for(T nextObject: collection) {
      boolean matches = true;
      String property = null;
      for (int i = 0; i < matchingValues.length; i++) {
        property = (String) matchingValues[i][0];
        Object matchingValue = matchingValues[i][1];
        if(!matches(nextObject, property, matchingValue)) {
          matches = false;
          break;
        } // End of if/else
      } // End of for
      if (matches) {
        matchingList.add(nextObject);
      } // End of while
    } // End of while
    return matchingList;
  }

  /**
   * Retrieves a new <code>List</code> of mathching elements within the given
   * <code>collection</code>. The matching elements will be the ones in which
   * each value return from each element's <code>property</code> will be the
   * same as the given <code>value</code>.
   * 
   * @param collection
   *          The <code>Collection</code> in which the matching elements will be
   *          looked-up.
   * @param property
   *          The element's property that must be matched.
   * @param value
   *          The <code>property</code>'s value that must be matched.
   * @return A new <code>List</code> of mathching elements.
   * @throws SystemException
   *           If an unexpected error ocurrs.
   */
  public static <T extends AbstractDTO> List<T> getMatchingList(
      Collection<T> collection, String property,
      Object value) throws SystemException {
    return BeanUtil.getMatchingList(
        collection, new Object[][] {{property, value}});
  }
  
  /**
   * Retrieves the first mathching element within the given
   * <code>collection</code>. The matching element will be the one
   * in which the value return from the element's <code>property</code> will
   * be the same as the given <code>value</code>.
   * 
   * @param collection
   *         The <code>Collection</code> in which the matching element will be
   *         looked-up.
   * @param matchingValues
   *         "attribute name" - "matching value" pair.
   * @return 
   *         The matching element. <code>null</code> if none meet the search
   *         criteria.
   * @throws SystemException
   *         If an unexpected error ocurrs.
   */
  public static <T extends AbstractDTO> T getMatching(Collection<T> collection,
      Object[][] matchingValues) throws SystemException {
    T matchingBean = null;
    for(T nextObject: collection) {
      boolean matches = true;
      for (int i=0; i<matchingValues.length; i++) {
        String property = (String) matchingValues[i][0];
        Object matchingValue = matchingValues[i][1];
        if(!BeanUtil.matches(nextObject, property, matchingValue)) {
          matches = false;
          break;
        } // End of if
      } // End of for
      
      if(matches) {
        matchingBean = nextObject;
        break;
      } // End of if
    } // End of while
    return matchingBean;
  }

  /**
   * Retrieves the first mathching element within the given
   * <code>collection</code>. The matching element will be the one in which
   * the value returned from the element's <code>property</code> will be the
   * same as the given <code>value</code>.
   * 
   * @param collection
   *          The <code>Collection</code> in which the matching element will be
   *          looked-up.
   * @param property
   *          The element's property that must be matched.
   * @param value
   *          The <code>property</code>'s value that must be matched.
   * @return 
   *         The matching element. <code>null</code> if none meet the search
   *         criteria.
   * @throws SystemException
   *           If an unexpected error ocurrs.
   */
  public static <T extends AbstractDTO> T getMatching(Collection<T> collection,
      String property, Object value) throws SystemException {
    return BeanUtil.getMatching(
        collection, new Object[][] {{property, value}});
  }
  
  /**
   * Creates a new instance of class <code>classname</code> recieving the
   * <code>arguments</code> as the constructor's parameters.
   * 
   * @param classname
   *         The name of the class to be instantiated. For example: 
   *         java.lang.String.
   * @param arguments
   *         The constructor's parameters.
   * @return
   *         A new instance of class <code>classname</code>.
   * @throws SystemException
   *         If an unexpected error ocurrs.
   */
  public static Object newInstance(String classname, Object[] arguments)
      throws SystemException {
    Object instance = null;
    try {
      Class<?> theclass = Class.forName(classname);
      Constructor<?>[] constructors = theclass.getConstructors();
      for (int i = 0; i < constructors.length; i++) {
        Constructor<?> constructor = constructors[i];
        try {
          instance = constructor.newInstance(arguments);
          break;
        } catch (IllegalArgumentException iaException) {
          // No problem. Continue Working.
        } catch (InstantiationException iException) {
          // No problem. Continue Working.
        } catch (IllegalAccessException iaException) {
          // No problem. Continue Working.
        } catch (InvocationTargetException itException) {
          // No problem. Continue Working.
        } // End of try/catch
      } // End of for
    } catch (ClassNotFoundException cnfException) {
      throw new SystemException(
          "Class \"" + classname + "\" was not found.", cnfException);
    } catch (SecurityException sException) {
      throw new SystemException(
          "Unable to access class: \"" + classname + "\".", sException);
    } // End of try/catch

    return instance;
  }

  /**
   * Creates a new instance of class <code>classname</code>. The class must
   * have a no argument constructor.
   * 
   * @param classname
   *         The name of the class to be instantiated. For example: 
   *         java.lang.String.
   * @return
   *         A new instance of class <code>classname</code>.
   * @throws SystemException
   *         If an unexpected error ocurrs.
   */
  public static Object newInstance(String classname) throws SystemException {
    return BeanUtil.newInstance(classname, new Object[] {});
  }

}
