package mx.com.afirme.midas.sistema;

import java.util.ArrayList;


/**
 * Common <code>String</code> utilities.
 * 
 * @author Marsil Benavides
 * @version 1.00, December 16, 2008
 */
public final class StringUtil {
  
  private StringUtil() {
    // Prevents Instantiation
  }

  /**
   * Tests if the given <code>String</code> is <code>null</code> or doesn't
   * containg any element.
   * 
   * @param string
   *          The string to be checked
   * @return <code>true</code> if the string parameter is <code>null</code>
   *          or doesn't contain any element.
   */
  public static boolean isEmpty(String string) {
    return (string == null) || (string.trim().length() == 0);
  }

  /**
   * Replaces the given <code>stringToBeReplaced</code> int the given
   * <code>string</code> using the given <code>replacement</code> at the given
   * <code>atOccurenceId</code>.
   * 
   * @param string
   *          The <code>String</code> in which the
   *          <code>stringToBeReplaced</code> will be searched for.
   * @param stringToBeReplaced
   *          The String that will be replaced.
   * @param replacement
   *          The String that will be set in place of the
   *          <code>stringToBeReplaced</code>.
   * @param atOccurenceId
   *          The ocrrurence id that will be replaced.
   * @return A new <code>String</code> with the value replaced (if any).
   */
  public static String replace(String string, String stringToBeReplaced,
      String replacement, int atOccurenceId) {
    int index = 0;
    int occurence = 0;
    StringBuilder buffer = new StringBuilder();
    while((index=string.indexOf(
        stringToBeReplaced, index+stringToBeReplaced.length()))!=-1) {
      if(occurence++!=atOccurenceId) {
        continue;
      } else {
        buffer.append(string.substring(0, index));
        buffer.append(replacement);
        buffer.append(string.substring(index+stringToBeReplaced.length()));
        break;
      } // End of if/else
    } // End of while
    return buffer.toString();
  }
  
  /**
   * Capitalizes the first word in the given <code>string</code>.
   * 
   * @param string
   *         The <code>String</code> to be capitalized.
   * @return The capitalized <code>string</code>.
   */
  public static String capitalizeFirstWord(String string) {
    if(StringUtil.isEmpty(string)) {
      return string;
    } else {
      return
          Character.toUpperCase(string.charAt(0)) +
          ((string.length() > 1) ?
              string.substring(1) :
              new String()); 
    } // End of if/else
  }

  public static String decodeUri(String s){
	  class UriChar{
		  String uri;
		  String ch;
		  public UriChar(String uri, String ch){
			  this.uri = uri;
			  this.ch = ch;
		  }
		  
	  }
	  ArrayList<UriChar> chars = new ArrayList<UriChar>();
	  chars.add(new UriChar("%C3%81", "Á")); chars.add(new UriChar("%C3%89", "É")); chars.add(new UriChar("%C3%8D", "Í")); chars.add(new UriChar("%C3%93", "Ó")); chars.add(new UriChar("%C3%9A", "Ú"));
	  chars.add(new UriChar("%C3%A1", "á")); chars.add(new UriChar("%C3%A9", "é")); chars.add(new UriChar("%C3%AD", "í")); chars.add(new UriChar("%C3%B3", "ó")); chars.add(new UriChar("%C3%BA", "ú"));
	  chars.add(new UriChar("%C3%91", "Ñ")); chars.add(new UriChar("%C3%B1", "ñ")); 
	  chars.add(new UriChar("%C3%9C", "Ü")); chars.add(new UriChar("%C3%BC", "ü"));
	  if (s != null){
		  for(UriChar element : chars){
			  s = s.replaceAll(element.uri, element.ch);
		  }
	  }
	  return s;
  }
  
  public static String decodeUriModal(String s){
	  class UriChar{
		  String uri;
		  String ch;
		  public UriChar(String uri, String ch){
			  this.uri = uri;
			  this.ch = ch;
		  }
		  
	  }
	  ArrayList<UriChar> chars = new ArrayList<UriChar>();
	  chars.add(new UriChar("%C3%81", "Á")); chars.add(new UriChar("%C3%89", "É")); chars.add(new UriChar("%C3%8D", "Í")); chars.add(new UriChar("%C3%93", "Ó")); chars.add(new UriChar("%C3%9A", "Ú"));
	  chars.add(new UriChar("%C3%A1", "á")); chars.add(new UriChar("%C3%A9", "é")); chars.add(new UriChar("%C3%AD", "í")); chars.add(new UriChar("%C3%B3", "ó")); chars.add(new UriChar("%C3%BA", "ú"));
	  chars.add(new UriChar("%C3%91", "Ñ")); chars.add(new UriChar("%C3%B1", "ñ")); 
	  chars.add(new UriChar("%C3%9C", "Ü")); chars.add(new UriChar("%C3%BC", "ü"));
	  chars.add(new UriChar("%20"," "));
	  chars.add(new UriChar("%25","%"));
	  chars.add(new UriChar("%22","\""));
	  chars.add(new UriChar("%C2%BF","¿"));
	  chars.add(new UriChar("%C2%A1","¡"));
	  chars.add(new UriChar("%0A","\n"));
	  if (s != null){
		  for(UriChar element : chars){
			  s = s.replaceAll(element.uri, element.ch);
		  }
	  }
	  return s;
  }
  
  

}
