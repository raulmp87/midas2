package mx.com.afirme.midas.sistema.excel;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
 
import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaRangosDN;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

public class CargaMasivaEsquemasExcel<T> extends MidasExcel<T> {
	
	public CargaMasivaEsquemasExcel(Class<T> classObject) {
		super(classObject);
	}

	public boolean isValid() throws  IOException {
		return true;
	}

	public List<T> getRegistrosValidos() throws SystemException {
		List<T> result = super.getRegistrosValidos();
		for (HSSFRow row : super.getValidRows()) {
			int index = 1;
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				
				if(cell.getCellType() != HSSFCell.CELL_TYPE_NUMERIC)
				{
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
				}
			}
						
		}
		return result;
	}

	public List<T> getRegistrosInvalidos() throws SystemException {
	
		return super.getRegistrosInvalidos();
	}

	public boolean cargaArchivoEsquemas(BigDecimal file, String extension)
	{
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		CargaMasivaEsquemasExcel<EsquemasDTO> excel = new CargaMasivaEsquemasExcel<EsquemasDTO>(EsquemasDTO.class);
		
		controlArchivoDTO.setNombreArchivoOriginal(file+extension);
		controlArchivoDTO.setIdToControlArchivo(file);
		controlArchivoDTO.setExtension(extension);
		
		excel.setArchivo(controlArchivoDTO);
		
		try {
			Validacion valNumerico = new Validacion(TipoValidacion.NUMERICO);
			Validacion valAlfaNumerico = new Validacion(TipoValidacion.ALFANUMERICO);
			Validacion valNoRequerido = new Validacion(TipoValidacion.NO_REQUERIDO);

			Propiedad propiedad4 = new Propiedad(0, "idContrato");
			propiedad4.setDescripcion("idContrato");
			propiedad4.addValidacion(valAlfaNumerico);
			excel.addPropiedad(propiedad4);
			
			Propiedad propiedad = new Propiedad(1, "anio");
			propiedad.setDescripcion("anio");
			propiedad.addValidacion(valNumerico);
			excel.addPropiedad(propiedad);
			
			Propiedad propiedad2 = new Propiedad(2, "cer");
			propiedad2.setDescripcion("cer");
			propiedad2.addValidacion(valNumerico);
			excel.addPropiedad(propiedad2);
			 
			
			Propiedad propiedad3 = new Propiedad(3, "tipoCobertura");
			propiedad3.setDescripcion("tipoCobertura");
			propiedad3.addValidacion(valNumerico);
			excel.addPropiedad(propiedad3);
			
			Propiedad propiedad5 = new Propiedad(4, "nivel");
			propiedad5.setDescripcion("nivel");
			propiedad5.addValidacion(valNumerico);
			excel.addPropiedad(propiedad5);
			
			
			Propiedad propiedad14 = new Propiedad(5, "ordenEntrada");
			propiedad14.setDescripcion("ordenEntrada");
			propiedad14.addValidacion(valNumerico);
			excel.addPropiedad(propiedad14);
			
			Propiedad propiedad6 = new Propiedad(6, "retencion");
			propiedad6.setDescripcion("retencion");
			propiedad6.addValidacion(valNumerico);
			excel.addPropiedad(propiedad6);
			
			Propiedad propiedad7 = new Propiedad(7, "capLim");
			propiedad7.setDescripcion("capLim");
			propiedad7.addValidacion(valNumerico);
			excel.addPropiedad(propiedad7);
			
			Propiedad propiedad9 = new Propiedad(8, "retAdic");
			propiedad9.setDescripcion("retAdic");
			propiedad9.addValidacion(valNumerico);
			excel.addPropiedad(propiedad9);
			
			Propiedad propiedad15 = new Propiedad(9, "reinstalaciones");
			propiedad15.setDescripcion("reinstalaciones");
			propiedad15.addValidacion(valNumerico);
			excel.addPropiedad(propiedad15);
			
			Propiedad propiedad16 = new Propiedad(10, "llaveD");
			propiedad16.setDescripcion("llaveD");
			propiedad16.addValidacion(valNumerico);
			excel.addPropiedad(propiedad16);
			
			Propiedad propiedad17 = new Propiedad(11, "llaveRet");
			propiedad17.setDescripcion("llaveRet");
			propiedad17.addValidacion(valNumerico);
			excel.addPropiedad(propiedad17);

			Propiedad propiedad10 = new Propiedad(12, "claveREAS");
			propiedad10.setDescripcion("claveREAS");
			propiedad10.addValidacion(valAlfaNumerico);
			excel.addPropiedad(propiedad10);
			
			Propiedad propiedad11 = new Propiedad(13, "partREAS");
			propiedad11.setDescripcion("partREAS");
			propiedad11.addValidacion(valNumerico);
			excel.addPropiedad(propiedad11);
			
			Propiedad propiedad12 = new Propiedad(14, "calificacion");
			propiedad12.setDescripcion("calificacion");
			propiedad12.addValidacion(valNumerico);
			excel.addPropiedad(propiedad12);
			
			Propiedad propiedad13 = new Propiedad(15, "combinacion");
			propiedad13.setDescripcion("combinacion");
			propiedad13.addValidacion(valAlfaNumerico);
			propiedad13.addValidacion(valNoRequerido);
			excel.addPropiedad(propiedad13);
			
			Propiedad moneda = new Propiedad(16, "tipoMoneda");
			moneda.setDescripcion("tipoMoneda");
			moneda.addValidacion(valAlfaNumerico);
			moneda.addValidacion(valNoRequerido);
			excel.addPropiedad(moneda);
			
			List<EsquemasDTO> direccionesValidas = excel.getRegistrosValidos();
			List<EsquemasDTO> direccionesInvalidas = excel.getRegistrosInvalidos();
			
			if(!direccionesInvalidas.isEmpty())
			{	
				return false;
			}
			 CargaMasivaRangosDN.getInstancia()
			.agregar( direccionesValidas
					);
			return true;
		
		} catch (SystemException e) {
			log.error(e);
			return false;
		}
	}
	
}
