package mx.com.afirme.midas.sistema.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaRangosDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class CargaMasivaCadAmisExcel<T> extends MidasExcel<T> {

	public CargaMasivaCadAmisExcel(Class<T> classObject) {
		super(classObject);
	}

	public boolean isValid() throws FileNotFoundException, IOException {
		String fileName = super.obtenerNombreArchivo(super.getArchivo());
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		String name = wb.getSheetName(0);
		return true;
	}

	public List<T> getRegistrosValidos() throws SystemException {
		List<T> result = super.getRegistrosValidos();
		for (HSSFRow row : super.getValidRows()) {
			int index = 1;
			StringBuilder sumasAseguradas = new StringBuilder("");
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double numericValue = cell.getNumericCellValue();
					
					sumasAseguradas.append(numericValue);
					break;
				default:
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
					break;
				}
				sumasAseguradas.append("-");
			}
						
		}
		return result;
	}

	public List<T> getRegistrosInvalidos() throws SystemException {
		List<T> result = super.getRegistrosInvalidos();
		return result;
	}

	public boolean cargaArchivoEsquemas(BigDecimal file, String extension)
	{
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		CargaMasivaCadAmisExcel<CaducidadesDTO> excel = new CargaMasivaCadAmisExcel<CaducidadesDTO>(CaducidadesDTO.class);
		
		controlArchivoDTO.setNombreArchivoOriginal(file+extension);
		controlArchivoDTO.setIdToControlArchivo(file);
		controlArchivoDTO.setExtension(extension);
		
		excel.setArchivo(controlArchivoDTO);
		
		try {
			Validacion ValNumerico = new Validacion(TipoValidacion.NUMERICO);
			Validacion ValAlfaNumerico = new Validacion(TipoValidacion.ALFANUMERICO);

			Propiedad propiedad = new Propiedad(0, "anio");
			propiedad.setDescripcion("anio");
			propiedad.addValidacion(ValNumerico);
			excel.addPropiedad(propiedad);
			
			Propiedad propiedad2 = new Propiedad(1, "plan");
			propiedad2.setDescripcion("plan");
			propiedad2.addValidacion(ValAlfaNumerico);
			excel.addPropiedad(propiedad2);
			 			
			Propiedad propiedad3 = new Propiedad(2, "anioVigencia");
			propiedad3.setDescripcion("anioVigencia");
			propiedad3.addValidacion(ValNumerico);
			excel.addPropiedad(propiedad3);
			
			Propiedad propiedad4 = new Propiedad(3, "tipoMoneda");
			propiedad4.setDescripcion("tipoMoneda");
			propiedad4.addValidacion(ValAlfaNumerico);
			excel.addPropiedad(propiedad4);
			
			
			Propiedad propiedad5 = new Propiedad(4, "porcentaje");
			propiedad5.setDescripcion("porcentaje");
			propiedad5.addValidacion(ValNumerico);
			excel.addPropiedad(propiedad5);
						
			List<CaducidadesDTO> direccionesValidas = excel.getRegistrosValidos();
			List<CaducidadesDTO> direccionesInvalidas = excel.getRegistrosInvalidos();
			
			if(direccionesInvalidas.size() > 0)
				return false;
			
				 CargaMasivaRangosDN.getInstancia()
				.agregarCad( direccionesValidas
						);
				 
			return true;
		
		} catch (SystemException e) {
			return false;
		}
	}
	
}
