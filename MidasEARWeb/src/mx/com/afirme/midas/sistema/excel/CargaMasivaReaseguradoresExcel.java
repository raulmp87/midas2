package mx.com.afirme.midas.sistema.excel;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
 

import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCargaCNSF;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaReaseguradoresDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

public class CargaMasivaReaseguradoresExcel<T> extends MidasExcel<T> {
	
	public CargaMasivaReaseguradoresExcel(Class<T> classObject) {
		super(classObject);
	}

	public boolean isValid() throws  IOException {
		return true;
	}

	public List<T> getRegistrosValidos() throws SystemException {
		List<T> result = super.getRegistrosValidos();
		for (HSSFRow row : super.getValidRows()) {
			int index = 1;
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				
				if(cell.getCellType() != HSSFCell.CELL_TYPE_NUMERIC)
				{
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
				}
			}
						
		}
		return result;
	}

	public List<T> getRegistrosInvalidos() throws SystemException {
	
		return super.getRegistrosInvalidos();
	}

	public boolean cargaArchivoEsquemas(BigDecimal file, String extension)
	{
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		CargaMasivaReaseguradoresExcel<ReaseguradorCargaCNSF> excel = new CargaMasivaReaseguradoresExcel<ReaseguradorCargaCNSF>(ReaseguradorCargaCNSF.class);
		
		controlArchivoDTO.setNombreArchivoOriginal(file+extension);
		controlArchivoDTO.setIdToControlArchivo(file);
		controlArchivoDTO.setExtension(extension);
		
		excel.setArchivo(controlArchivoDTO);
		
		try {
			Validacion valNumerico = new Validacion(TipoValidacion.NUMERICO);
			Validacion valAlfaNumerico = new Validacion(TipoValidacion.ALFANUMERICO);
			Validacion valNoRequerido = new Validacion(TipoValidacion.NO_REQUERIDO);

			Propiedad claveReasegurador = new Propiedad(0, "claveReasegurador");
			claveReasegurador.setDescripcion("claveReasegurador");
			claveReasegurador.addValidacion(valAlfaNumerico);
			excel.addPropiedad(claveReasegurador);
			
			Propiedad nombreReasegurador = new Propiedad(1, "nombreReasegurador");
			nombreReasegurador.setDescripcion("nombreReasegurador");
			nombreReasegurador.addValidacion(valAlfaNumerico);
			excel.addPropiedad(nombreReasegurador);
			
			Propiedad tipoRiesgo = new Propiedad(2, "tipoRiesgo");
			tipoRiesgo.setDescripcion("tipoRiesgo");
			tipoRiesgo.addValidacion(valAlfaNumerico);
			excel.addPropiedad(tipoRiesgo);
			 
			
			Propiedad fechaAlta = new Propiedad(3, "fechaAlta");
			fechaAlta.setDescripcion("fechaAlta");
			fechaAlta.addValidacion(valAlfaNumerico);
			excel.addPropiedad(fechaAlta);
			
			Propiedad fechaBaja = new Propiedad(4, "fechaBaja");
			fechaBaja.setDescripcion("fechaBaja");
			fechaBaja.addValidacion(valNumerico);
			excel.addPropiedad(fechaBaja);
			
						
			List<ReaseguradorCargaCNSF> direccionesValidas = excel.getRegistrosValidos();
			List<ReaseguradorCargaCNSF> direccionesInvalidas = excel.getRegistrosInvalidos();
			
			if(!direccionesInvalidas.isEmpty())
			{	
				return false;
			}
			 CargaMasivaReaseguradoresDN.getInstancia()
			.agregar( direccionesValidas
					);
			return true;
		
		} catch (SystemException e) {
			log.error(e);
			return false;
		}
	}
	
	/*public List<ReaseguradorCargaCNSF> validarReas(List<ReaseguradorCargaCNSF> lista)
	{
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			ReaseguradorCargaCNSF reaseguradorCargaCNSF = (ReaseguradorCargaCNSF) iterator
					.next();
			
			
			
		}
	}*/
}
