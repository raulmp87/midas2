/**
 * 
 */
package mx.com.afirme.midas.sistema.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * @author jreyes
 *
 */
public class DatosSolicitudEnTramiteExcel<T> extends MidasExcel<T>{

	public DatosSolicitudEnTramiteExcel(Class<T> classObject) {
		super(classObject);
	}
	
	private BigDecimal idToSolicitud;

	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public boolean isValid()  throws FileNotFoundException, IOException{
		String fileName = super.obtenerNombreArchivo(super.getArchivo());
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
		
		if (extension.equalsIgnoreCase(".XLS")){
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		}else if(extension.equalsIgnoreCase(".XLSX")){
			return false;
//			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(fileName));
		}else{
			return false;
		}
		
		return true;
	}

	
}