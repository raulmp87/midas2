package mx.com.afirme.midas.sistema.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * @author Fernando Alonzo
 * @since 04.03.2010
 */
public class MidasExcel<T> {
	
	public static final Logger log = Logger.getLogger(MidasExcel.class);
	private Class<T> classObject;
	private T t;
	private ControlArchivoDTO archivo;
	private List<Propiedad> propiedades = new ArrayList<Propiedad>();
	private List<T> registrosValidos;
	private List<String> registrosInvalidosMensaje;
	private List<T> registrosInvalidos;
	private HSSFRow header;
	private List<HSSFRow> validRows;
	private List<HSSFRow> invalidRows;
	private boolean excludeEmptyRow = false;

	public MidasExcel(Class<T> classObject) {
		this.classObject = classObject;
	}

	public void setArchivo(ControlArchivoDTO archivo) {
		this.archivo = archivo;
	}

	public ControlArchivoDTO getArchivo() {
		return archivo;
	}

	public void addPropiedad(Propiedad propiedad) {
		propiedades.add(propiedad);
	}

	public List<Propiedad> getPropiedades() {
		return propiedades;
	}

	public List<T> getRegistrosValidos() throws SystemException {
		if(registrosValidos == null) {
			this.procesarExcel(archivo);
		}
		return registrosValidos;
	}

	public List<String> getRegistrosInvalidosMensaje() throws SystemException {
		if(registrosInvalidosMensaje == null) {
			this.procesarExcel(archivo);
		}
		return registrosInvalidosMensaje;
	}
	public List<T> getRegistrosInvalidos() throws SystemException {
		if(registrosInvalidos == null) {
			this.procesarExcel(archivo);
		}
		return registrosInvalidos;
	}	

	public boolean validColumnHeader(ControlArchivoDTO archivo){
		boolean isValidHeader = true;
		try {
			HSSFSheet sheet = getSheet(archivo);
			Iterator rowIterator = sheet.rowIterator();
			while(rowIterator.hasNext()) {
				HSSFRow row = (HSSFRow)rowIterator.next();
				for(Propiedad propiedad : propiedades) {
					if(isValidHeader){
						HSSFCell cell = row.getCell(propiedad.getIndice());
						if(cell != null) {
							if(cell.getStringCellValue().equalsIgnoreCase(propiedad.getDescripcion())){
								isValidHeader = false;
							}
						}	
					}
				}
			}
		} catch (FileNotFoundException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		return isValidHeader;
	}
	
	private void procesarExcel(ControlArchivoDTO archivo) throws SystemException {
		try {
			this.registrosValidos = new ArrayList<T>();
			this.registrosInvalidosMensaje = new ArrayList<String>();
			this.validRows = new ArrayList<HSSFRow>();
			this.registrosInvalidos = new ArrayList<T>();
			this.invalidRows = new ArrayList<HSSFRow>();
			
			HSSFSheet sheet = getSheet(archivo);
			Iterator rowIterator = sheet.rowIterator();
			header = (HSSFRow) rowIterator.next();
			while(rowIterator.hasNext()) {
				HSSFRow row = (HSSFRow)rowIterator.next();
				t = classObject.newInstance();
				Boolean isValidRow = Boolean.TRUE;
				int cellEmpty = 0;
				for(Propiedad propiedad : propiedades) {
					HSSFCell cell = row.getCell(propiedad.getIndice());
					if(cell != null) {
						if(!isPropiedadValida(propiedad, cell)) {
							isValidRow = Boolean.FALSE;
							if(excludeEmptyRow && cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
								cellEmpty++;
							} else {
								break;
							}
						} else {
							if(excludeEmptyRow && cell.getCellType() == HSSFCell.CELL_TYPE_BLANK){
								cellEmpty++;
							}
						}
					} else {
						if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
							this.addRegistroInvalido(row.getRowNum(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
							isValidRow = Boolean.FALSE;
							if(!excludeEmptyRow){
								break;
							} else {
								cellEmpty++;
							}
						} else {
							if(excludeEmptyRow){
								cellEmpty++;
							}
						}
					}
					
				}
				if(isValidRow) {
					this.registrosValidos.add(this.t);
					this.validRows.add(row);
				} else {
					if(cellEmpty != propiedades.size()){
						this.registrosInvalidos.add(this.t);
						this.invalidRows.add(row);
					} else {
						if(excludeEmptyRow){
							Iterator<String> mensajesError = this.getRegistrosInvalidosMensaje().iterator();
							while(mensajesError.hasNext()){
								if(mensajesError.next().contains("Fila: " + (row.getRowNum() + 1) + " Columna: ")) {
									mensajesError.remove();
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new SystemException(e);
		}
	}
	
	private HSSFSheet getSheet(ControlArchivoDTO archivo) 
		throws FileNotFoundException, IOException{
		String fileName = this.obtenerNombreArchivo(archivo);
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		return wb.getSheetAt(0);
	}

	private Boolean isPropiedadValida(Propiedad propiedad, HSSFCell cell)
			throws SecurityException, NoSuchFieldException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, SystemException, ClassNotFoundException {
		for(Validacion validacion : propiedad.getValidaciones()) {
			switch(validacion.getTipoValidacion()) {
			case LONGITUD_MINIMA:
				if(!this.isLongitudMinimaValida(cell, propiedad, validacion)) {
					return Boolean.FALSE;
				}
				break;
			case LONGITUD_MAXIMA:
				if(!this.isLongitudMaximaValida(cell, propiedad, validacion)) {
					return Boolean.FALSE;
				}
				break;
			case NUMERICO:
				if(!this.isNumeroValido(cell, propiedad, validacion)) {
					return Boolean.FALSE;
				}
				break;
			case ALFANUMERICO:
				if(!this.isAlfanumericoValido(cell, propiedad, validacion)) {
					return Boolean.FALSE;
				}
				break;
			case EXISTENCIA:
				if(!this.isExistente(cell, propiedad, validacion)) {
					return Boolean.FALSE;
				}
				break;
			}
		}
		return Boolean.TRUE;
	}

	private Boolean isLongitudMinimaValida(HSSFCell cell, Propiedad propiedad,
			Validacion validacion) throws SecurityException,
			NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		switch(cell.getCellType()) {
		case HSSFCell.CELL_TYPE_NUMERIC:
			Double numericValue = cell.getNumericCellValue();
			if(propiedad.contains(TipoValidacion.NUMERICO)) {
				if(numericValue.doubleValue() < Double.parseDouble(validacion.getValor())) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "El valor minimo permitido es " + validacion.getValor());
					return Boolean.FALSE;
				} else {
					this.setProperty(propiedad.getNombre(), numericValue.toString());
					return Boolean.TRUE;
				}
			} else {
				if(numericValue.toString().length() < Integer.parseInt(validacion.getValor())) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La longitud minima permitida es " + validacion.getValor());
					return Boolean.FALSE;
				} else {
					this.setProperty(propiedad.getNombre(), new DecimalFormat("#.####").format(numericValue));
					return Boolean.TRUE;
				}
			}
		case HSSFCell.CELL_TYPE_STRING:
			HSSFRichTextString stringValue = cell.getRichStringCellValue();
			if(stringValue.length() < Integer.parseInt(validacion.getValor())) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La longitud minima permitida es " + validacion.getValor());
				return Boolean.FALSE;
			} else {
				this.setProperty(propiedad.getNombre(), stringValue.getString());
				return Boolean.TRUE;
			}
		case HSSFCell.CELL_TYPE_BLANK:
			if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
				break;
			} else {
				return Boolean.TRUE;
			}
		default:
			this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Tipo de celda incorrecto");
			break;
		}
		return Boolean.FALSE;
	}

	private Boolean isLongitudMaximaValida(HSSFCell cell, Propiedad propiedad,
			Validacion validacion) throws SecurityException,
			NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		switch(cell.getCellType()) {
		case HSSFCell.CELL_TYPE_NUMERIC:
			Double numericValue = cell.getNumericCellValue();
			if(propiedad.contains(TipoValidacion.NUMERICO)) {
				if(numericValue.doubleValue() > Double.parseDouble(validacion.getValor())) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "El valor maximo permitido es " + validacion.getValor());
					return Boolean.FALSE;
				} else {
					this.setProperty(propiedad.getNombre(), numericValue.toString());
					return Boolean.TRUE;
				}
			} else {
				if(numericValue.toString().length() > Integer.parseInt(validacion.getValor())) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La longitud maxima permitida es " + validacion.getValor());
					return Boolean.FALSE;
				} else {
					this.setProperty(propiedad.getNombre(), new DecimalFormat("#.####").format(numericValue));
					return Boolean.TRUE;
				}
			}
		case HSSFCell.CELL_TYPE_STRING:
			HSSFRichTextString stringValue = cell.getRichStringCellValue();
			if(stringValue.length() > Integer.parseInt(validacion.getValor())) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La longitud maxima permitida es " + validacion.getValor());
				return Boolean.FALSE;
			} else {
				this.setProperty(propiedad.getNombre(), stringValue.getString());
				return Boolean.TRUE;
			}
		case HSSFCell.CELL_TYPE_BLANK:
			if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
				break;
			} else {
				return Boolean.TRUE;
			}
		default:
			this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Tipo de celda incorrecto");
			break;
		}
		return Boolean.FALSE;
	}

	private Boolean isNumeroValido(HSSFCell cell, Propiedad propiedad,
			Validacion validacion) throws SecurityException,
			NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		switch(cell.getCellType()) {
		case HSSFCell.CELL_TYPE_NUMERIC:
			Double numericValue = cell.getNumericCellValue();
			this.setProperty(propiedad.getNombre(), numericValue.toString());
			return Boolean.TRUE;
		case HSSFCell.CELL_TYPE_STRING:
			HSSFRichTextString stringValue = cell.getRichStringCellValue();
			try {
				Double.parseDouble(stringValue.getString());
			} catch (NumberFormatException e) {
				return Boolean.FALSE;
			}
			this.setProperty(propiedad.getNombre(), stringValue.getString());
			return Boolean.TRUE;
		case HSSFCell.CELL_TYPE_BLANK:
			if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
				break;
			} else {
				return Boolean.TRUE;
			}
		default:
			this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Tipo de celda incorrecto");
			break;
		}
		return Boolean.FALSE;
	}

	private Boolean isAlfanumericoValido(HSSFCell cell, Propiedad propiedad,
			Validacion validacion) throws SecurityException,
			NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		int op = propiedad.getCellType() != null ? propiedad.getCellType() : cell.getCellType() ;
		switch(op) {
		case HSSFCell.CELL_TYPE_NUMERIC:
			Double numericValue = cell.getNumericCellValue();
			this.setProperty(propiedad.getNombre(), numericValue.toString());
			return Boolean.TRUE;
		case HSSFCell.CELL_TYPE_STRING:
			HSSFRichTextString stringValue = cell.getRichStringCellValue();
			this.setProperty(propiedad.getNombre(), stringValue.getString());
			
			return Boolean.TRUE;
		case HSSFCell.CELL_TYPE_BLANK:
			if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
				break;
			} else {
				return Boolean.TRUE;
			}
		case 6: //Fecha
			if(HSSFCell.CELL_TYPE_BLANK==cell.getCellType()){
				if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
					break;
				} else {
					return Boolean.TRUE;
				}
			}else{
				Date dateValue = cell.getDateCellValue();
				try {
					this.setProperty(propiedad.getNombre(), dateValue);
				} catch (ParseException e) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Error en el formato " + dateValue);
					return Boolean.FALSE;
				}
			}			
			return Boolean.TRUE;
		default:
			this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Tipo de celda incorrecto");
			break;
		}
		return Boolean.FALSE;
	}

	@SuppressWarnings("unchecked")
	private Boolean isExistente(HSSFCell cell, Propiedad propiedad,
			Validacion validacion) throws SecurityException,
			NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException, SystemException,
			ClassNotFoundException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		Object beanBase = null;
		Method method = null;
		Object result = null;
		switch(cell.getCellType()) {
		case HSSFCell.CELL_TYPE_NUMERIC:
			Double numericValue = cell.getNumericCellValue();
			for(Dependencia dependencia : validacion.getDependencias()) {
				dependencia.setParametrosTemp(dependencia.getParametros());
				for(Object object : dependencia.getParametrosFromT()) {
					Class classObject = t.getClass();
					method = classObject.getMethod(object.toString());
					dependencia.addParametroTemp(method.invoke(this.t));
				}
				dependencia.addParametroTemp(new DecimalFormat("#.####").format(numericValue));
				beanBase = serviceLocator.getEJB(dependencia.getClaseRemota());
				method = beanBase.getClass().getMethod(dependencia.getMetodo(), dependencia.getTipoParametros());
				result = method.invoke(beanBase, dependencia.getParametrosTemp());
				if(result == null) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "El registro no existe");
					return Boolean.FALSE;
				} else {
					if(result instanceof Vector && !((Vector)result).isEmpty()) {
						method = ((Vector)result).get(0).getClass().getMethod(dependencia.getMapeadoPor());
						try {
							this.setProperty(propiedad.getNombre(), method.invoke(((Vector)result).get(0)).toString());
						} catch (IllegalArgumentException e) {
							this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Error en el formato " + method.invoke(((Vector)result).get(0)).toString());
							return Boolean.FALSE;
						} 
						return Boolean.TRUE;
					} else if(result instanceof ArrayList && !((ArrayList)result).isEmpty()) {
						method = ((ArrayList)result).get(0).getClass().getMethod(dependencia.getMapeadoPor());
						try {
							this.setProperty(propiedad.getNombre(), method.invoke(((ArrayList)result).get(0)).toString());
						} catch (IllegalArgumentException e) {
							this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Error en el formato " + method.invoke(((Vector)result).get(0)).toString());
							return Boolean.FALSE;
						} 
						return Boolean.TRUE;
					} else {
						this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "El registro no existe");
						return Boolean.FALSE;
					}
				}
			}
		case HSSFCell.CELL_TYPE_STRING:
			HSSFRichTextString stringValue = cell.getRichStringCellValue();
			for(Dependencia dependencia : validacion.getDependencias()) {
				dependencia.setParametrosTemp(dependencia.getParametros());
				for(Object object : dependencia.getParametrosFromT()) {
					Class classObject = t.getClass();
					method = classObject.getMethod(object.toString());
					dependencia.addParametroTemp(method.invoke(this.t));
				}
				dependencia.addParametroTemp(stringValue.getString());
				beanBase = serviceLocator.getEJB(dependencia.getClaseRemota());
				method = beanBase.getClass().getMethod(dependencia.getMetodo(), dependencia.getTipoParametros());
				result = method.invoke(beanBase, dependencia.getParametrosTemp());
				if(result == null) {
					this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "El registro no existe");
					return Boolean.FALSE;
				} else {
					if(result instanceof Vector && !((Vector)result).isEmpty()) {
						method = ((Vector)result).get(0).getClass().getMethod(dependencia.getMapeadoPor());
						try {
							this.setProperty(propiedad.getNombre(), method.invoke(((Vector)result).get(0)).toString());
						} catch (IllegalArgumentException e) {
							this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Error en el formato " + method.invoke(((Vector)result).get(0)).toString());
							return Boolean.FALSE;
						}
						return Boolean.TRUE;
					} else if(result instanceof ArrayList && !((ArrayList)result).isEmpty()) {
						method = ((ArrayList)result).get(0).getClass().getMethod(dependencia.getMapeadoPor());
						try {
							this.setProperty(propiedad.getNombre(), method.invoke(((ArrayList)result).get(0)).toString());
						} catch (IllegalArgumentException e) {
							this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Error en el formato " + method.invoke(((ArrayList)result).get(0)).toString());
							return Boolean.FALSE;
						}
						return Boolean.TRUE;
					} else {
						try{
							if(dependencia.getClaseRemota().equals("mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote")){
								Object[] valores =dependencia.getParametrosTemp();
								this.setProperty(propiedad.getNombre(), (String)valores[2]);
							}
						}catch (Exception e){}
						this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "El registro no existe");
						return Boolean.FALSE;
					}
				}
			}
		case HSSFCell.CELL_TYPE_BLANK:
			if(!propiedad.contains(TipoValidacion.NO_REQUERIDO)) {
				this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "La celda esta vac\u00eda");
				break;
			} else {
				return Boolean.TRUE;
			}
		default:
			this.addRegistroInvalido(cell.getRowIndex(), propiedad.getDescripcion(), "Tipo de celda incorrecto");
			break;
		}
		return Boolean.FALSE;
	}

	@SuppressWarnings("unchecked")
	private void setProperty(String nombrePropiedad, String value)
			throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		PropertyUtilsBean utilsBean = new PropertyUtilsBean();
		Class classObject = t.getClass();
        Field field = classObject.getDeclaredField(nombrePropiedad);
        String simpleName = field.getType().getSimpleName();
        if(simpleName.equals(BigDecimal.class.getSimpleName())) {
        	BigDecimal fieldValue = BigDecimal.valueOf(Double.parseDouble(value));
        	utilsBean.setProperty(t, field.getName(), fieldValue);
        } else if (simpleName.equals(Short.class.getSimpleName())) {
        	Short fieldValue = Short.parseShort(value);
        	utilsBean.setProperty(t, field.getName(), fieldValue);
        } else if (simpleName.equals(Integer.class.getSimpleName())) {
        	Integer fieldValue = Integer.parseInt(value);
        	utilsBean.setProperty(t, field.getName(), fieldValue);
        } else if (simpleName.equals(Float.class.getSimpleName())) {
        	Float fieldValue = Float.parseFloat(value);
        	utilsBean.setProperty(t, field.getName(), fieldValue);
        } else if (simpleName.equals(Double.class.getSimpleName())) {
        	Double fieldValue = Double.parseDouble(value);
        	utilsBean.setProperty(t, field.getName(), fieldValue);
        } else if (simpleName.equals(Long.class.getSimpleName())) {
        	Long fieldValue = Long.parseLong(value);
        	utilsBean.setProperty(t, field.getName(), fieldValue);
        } else if (simpleName.equals(String.class.getSimpleName())) {
        	utilsBean.setProperty(t, field.getName(), value);
        }
	}
	
	@SuppressWarnings("unchecked")
	private void setProperty(String nombrePropiedad, Date value) throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException{
		PropertyUtilsBean utilsBean = new PropertyUtilsBean();
		Class classObject = t.getClass();
        Field field = classObject.getDeclaredField(nombrePropiedad);
        String simpleName = field.getType().getSimpleName();
        if (simpleName.equals(Date.class.getSimpleName())) {
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			utilsBean.setProperty(t, field.getName(), sdf.parse(sdf.format(value)));
        }
	}

	protected void addRegistroInvalido(int rowIndex, String nombrePropiedad, String descripcionError) {
		this.registrosInvalidosMensaje.add("Fila: " + (rowIndex + 1) + " Columna: " + nombrePropiedad + " Error: " + descripcionError);
	}

	protected String obtenerNombreArchivo(ControlArchivoDTO archivo) {
		String uploadFolder;
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1) {
			uploadFolder = Sistema.UPLOAD_FOLDER;
		} else {
			uploadFolder = Sistema.LINUX_UPLOAD_FOLDER;
		}
		String fileName = archivo.getNombreArchivoOriginal();
		String extension = (fileName .lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
		return uploadFolder + archivo.getIdToControlArchivo().intValue() + extension;
	}

	protected List<HSSFRow> getValidRows() {
		if(validRows == null) {
			validRows = new ArrayList<HSSFRow>();
		}
		return validRows;
	}
	
	protected List<HSSFRow> getInvalidRows() {
		if(invalidRows == null) {
			invalidRows = new ArrayList<HSSFRow>();
		}
		return invalidRows;
	}	

	protected HSSFRow getHeader() {
		return header;
	}
	
	public boolean isExcludeEmptyRow() {
		return excludeEmptyRow;
	}

	public void setExcludeEmptyRow(boolean excludeEmptyRow) {
		this.excludeEmptyRow = excludeEmptyRow;
	}
}
