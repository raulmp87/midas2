package mx.com.afirme.midas.sistema.excel;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
  
import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.RangosDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaDetalleRanDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaRangosDN;
import mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva.CargaMasivaRangosRamosDTO;


import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

public class CargaMasivaRangosExcel<T> extends MidasExcel<T> {

	public CargaMasivaRangosExcel(Class<T> classObject) {
		super(classObject);
	}

	public boolean isValid() throws IOException {
		return true;
	}

	public List<T> getRegistrosValidos() throws SystemException {
		List<T> result = super.getRegistrosValidos();
		for (HSSFRow row : super.getValidRows()) {
			int index = 1;
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				
				if(cell.getCellType() != HSSFCell.CELL_TYPE_NUMERIC)
				{
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
				}
			}
						
		}
		return result;
	}

	public List<T> getRegistrosInvalidos() throws SystemException {
		List<T> result = super.getRegistrosInvalidos();
		for (HSSFRow row : super.getInvalidRows()) {
			int index = 7;
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				
				if(cell.getCellType() != HSSFCell.CELL_TYPE_NUMERIC)
				{
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
				}
			}
			
		}
		return result;
	}
	
	public boolean cargaArchivoRangos(BigDecimal file, String extension)
	{
		boolean isValid = true; 
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		CargaMasivaRangosExcel<RangosDTO> excel = new CargaMasivaRangosExcel<RangosDTO>(RangosDTO.class);
		
		controlArchivoDTO.setNombreArchivoOriginal(file+"."+extension);
		controlArchivoDTO.setIdToControlArchivo(file);
		controlArchivoDTO.setExtension(extension);
		
		excel.setArchivo(controlArchivoDTO);
		
		try {
			Validacion valNumerico = new Validacion(TipoValidacion.NUMERICO);
			Validacion valNoRequerido = new Validacion(TipoValidacion.NO_REQUERIDO);

			Propiedad propiedad = new Propiedad(0, "tipoRenglon");
			propiedad.addValidacion(valNumerico);
			propiedad.addValidacion(valNoRequerido);
			excel.addPropiedad(propiedad);
			
			Propiedad propiedad2 = new Propiedad(1, "valor");
			propiedad2.addValidacion(valNumerico);
			propiedad2.addValidacion(valNoRequerido);
			excel.addPropiedad(propiedad2);
			 
			
			Propiedad propiedad3 = new Propiedad(2, "valor2");
			propiedad3.addValidacion(valNumerico);
			propiedad3.addValidacion(valNoRequerido);
			excel.addPropiedad(propiedad3);
			
			Propiedad propiedad4 = new Propiedad(3, "valor3");
			propiedad4.addValidacion(valNumerico);
			propiedad4.addValidacion(valNoRequerido);
			excel.addPropiedad(propiedad4);
			
			
			Propiedad propiedad5 = new Propiedad(4, "valor4");
			propiedad5.addValidacion(valNumerico);
			propiedad5.addValidacion(valNoRequerido);
			excel.addPropiedad(propiedad5);
				
			List<RangosDTO> direccionesValidas = excel.getRegistrosValidos();
			List<RangosDTO> direccionesInvalidas = excel.getRegistrosInvalidos();
			
			if(!direccionesInvalidas.isEmpty())
			{
				return false;
			}
			
			ArrayList<CargaMasivaRangosRamosDTO> arrayList = organizar(direccionesValidas);
			
			isValid = isValidRangos(arrayList);
			
			if(isValid){
			 CargaMasivaRangosDN.getInstancia()
			.agregar( arrayList,
					arrayList);
			}
			return isValid;
					
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error de Carga de Excel", Level.INFO, e);
			return false;
		}
	}
	
	public boolean cargaArchivoEsquemas(BigDecimal file)
	{
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		CargaMasivaRangosExcel<EsquemasDTO> excel = new CargaMasivaRangosExcel<EsquemasDTO>(EsquemasDTO.class);
		
		controlArchivoDTO.setNombreArchivoOriginal(file+".xls");
		controlArchivoDTO.setIdToControlArchivo(file);
		controlArchivoDTO.setExtension("xls");
		
		excel.setArchivo(controlArchivoDTO);
		
		try {
			Validacion valNumerico = new Validacion(TipoValidacion.NUMERICO);
			Validacion valAlfaNumerico = new Validacion(TipoValidacion.ALFANUMERICO);

			Propiedad propiedad = new Propiedad(0, "anio");
			propiedad.setDescripcion("Anio");
			propiedad.addValidacion(valNumerico);
			excel.addPropiedad(propiedad);
			
			Propiedad propiedad2 = new Propiedad(1, "cer");
			propiedad2.setDescripcion("Cer");
			propiedad2.addValidacion(valNumerico);
			excel.addPropiedad(propiedad2);
			 
			
			Propiedad propiedad3 = new Propiedad(2, "tipoCobertura");
			propiedad3.setDescripcion("tipoCobertura");
			propiedad3.addValidacion(valNumerico);
			excel.addPropiedad(propiedad3);
			
			Propiedad propiedad4 = new Propiedad(3, "idContrato");
			propiedad4.setDescripcion("idContrato");
			propiedad4.addValidacion(valAlfaNumerico);
			excel.addPropiedad(propiedad4);
			
			
			Propiedad propiedad5 = new Propiedad(4, "ordenEntrada");
			propiedad5.setDescripcion("ordenEntrada");
			propiedad5.addValidacion(valNumerico);
			excel.addPropiedad(propiedad5);
			
			Propiedad propiedad6 = new Propiedad(5, "porcionCed");
			propiedad6.setDescripcion("porcionCed");
			propiedad6.addValidacion(valNumerico);
			excel.addPropiedad(propiedad6);
			
			Propiedad propiedad7 = new Propiedad(6, "capInf");
			propiedad7.setDescripcion("capInf");
			propiedad7.addValidacion(valNumerico);
			excel.addPropiedad(propiedad7);
			
			Propiedad propiedad8 = new Propiedad(7, "capSup");
			propiedad8.setDescripcion("capSup");
			propiedad8.addValidacion(valNumerico);
			excel.addPropiedad(propiedad8);
			
			Propiedad propiedad9 = new Propiedad(8, "impMaxCobertura");
			propiedad9.setDescripcion("impMaxCobertura");
			propiedad9.addValidacion(valNumerico);
			excel.addPropiedad(propiedad9);

			Propiedad propiedad10 = new Propiedad(9, "claveREAS");
			propiedad10.setDescripcion("claveREAS");
			propiedad10.addValidacion(valAlfaNumerico);
			excel.addPropiedad(propiedad10);
			
			Propiedad propiedad11 = new Propiedad(10, "partREAS");
			propiedad11.setDescripcion("partREAS");
			propiedad11.addValidacion(valNumerico);
			excel.addPropiedad(propiedad11);
			
			Propiedad propiedad12 = new Propiedad(11, "calificacion");
			propiedad12.setDescripcion("calificacion");
			propiedad12.addValidacion(valNumerico);
			excel.addPropiedad(propiedad12);
			
			Propiedad propiedad13 = new Propiedad(12, "combinacion");
			propiedad13.setDescripcion("combinacion");
			propiedad13.addValidacion(valAlfaNumerico);
			excel.addPropiedad(propiedad13);
			
			List<EsquemasDTO> direccionesValidas = excel.getRegistrosValidos();
			List<EsquemasDTO> direccionesInvalidas = excel.getRegistrosInvalidos();
			
			if(!direccionesInvalidas.isEmpty())
				return false;
			 
				 CargaMasivaRangosDN.getInstancia()
				.agregar( direccionesValidas
						);

			return true;
		
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error de Carga de Excel", Level.INFO, e);
			return false;
		}
	}
	
	
	public static ArrayList<CargaMasivaRangosRamosDTO> organizar(List<RangosDTO> direccionesValidas)
	{
		ArrayList<CargaMasivaRangosRamosDTO> coberturasLits = new ArrayList<CargaMasivaRangosRamosDTO>();
		CargaMasivaRangosRamosDTO cobertura =  new CargaMasivaRangosRamosDTO();
		ArrayList<CargaMasivaDetalleRanDTO> lisCargaMasivaDetalleRanDTOs = new ArrayList<CargaMasivaDetalleRanDTO>();
		BigDecimal rangoAnterior =  new BigDecimal(-1); 
		for (RangosDTO rangosDTO : direccionesValidas) {
			
			CargaMasivaDetalleRanDTO cargaMasivaDetalleRanDTO =  new CargaMasivaDetalleRanDTO();
			
			if(rangosDTO.getTipoRenglon() == null){
				continue;
			}
			
			switch (rangosDTO.getTipoRenglon().intValue()) {
			case 1://Se crea objeto
				cobertura = new CargaMasivaRangosRamosDTO();
				cobertura.setRamo(rangosDTO.getValor()+"-"+rangosDTO.getValor2()+"-"+rangosDTO.getValor3()+"-"+rangosDTO.getValor4());
				lisCargaMasivaDetalleRanDTOs = new ArrayList<CargaMasivaDetalleRanDTO>();
				coberturasLits.add(cobertura);
				break;
			case 2://Se define tipo Cobertura
				cobertura.setCobertura(Integer.parseInt(rangosDTO.getValor())+"-"+Integer.parseInt(rangosDTO.getValor2())+"-"+Integer.parseInt(rangosDTO.getValor3())+"-"+Integer.parseInt(rangosDTO.getValor4()));
				break;
			case 3:// se asignan rangos
				cargaMasivaDetalleRanDTO.setLim_inf(rangoAnterior.add(new BigDecimal(1)));
				cargaMasivaDetalleRanDTO.setLim_sup(new BigDecimal(Double.parseDouble(rangosDTO.getValor())));
				lisCargaMasivaDetalleRanDTOs.add(cargaMasivaDetalleRanDTO);
				rangoAnterior = new BigDecimal(Double.parseDouble( rangosDTO.getValor()));
				break;
			default:
				break;
			}
			
		}
		cobertura.setCargaMasivaDetalleRanDTOs(lisCargaMasivaDetalleRanDTOs);
		
		return coberturasLits;
	}
	
	public boolean isValidRangos(ArrayList<CargaMasivaRangosRamosDTO> arrayList)
	{
		BigDecimal lim;
		for (CargaMasivaRangosRamosDTO cargaMasivaRangosRamosDTO : arrayList) {
			
			lim = new BigDecimal(0);
				List<CargaMasivaDetalleRanDTO> lista = cargaMasivaRangosRamosDTO.getCargaMasivaDetalleRanDTOs();
				
				for (int i = 0; i < lista.size(); i++) {
					CargaMasivaDetalleRanDTO cargaMasivaDetalleRanDTO = lista.get(i);
					 if(cargaMasivaDetalleRanDTO.getLim_inf().compareTo(lim)==1 || cargaMasivaDetalleRanDTO.getLim_inf().compareTo(lim)==0)
					 {
						 lim = cargaMasivaDetalleRanDTO.getLim_inf();
					 }
					 else
					 {
						 return false;
					 }
				}	
		}
		return true;
	}
}
