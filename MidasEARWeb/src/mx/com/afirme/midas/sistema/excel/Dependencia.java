package mx.com.afirme.midas.sistema.excel;

import java.util.ArrayList;
import java.util.List;

public class Dependencia {
	private String claseRemota;
	private String metodo;
	private String mapeadoPor;
	private List<Object> parametrosFromT;
	private List<Object> parametros;
	private List<Object> parametrosTemp;
	@SuppressWarnings("unchecked")
	private Class[] tipoParametros;

	public void setClaseRemota(String claseRemota) {
		this.claseRemota = claseRemota;
	}

	public String getClaseRemota() {
		return claseRemota;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMapeadoPor(String mapeadoPor) {
		this.mapeadoPor = mapeadoPor;
	}

	public String getMapeadoPor() {
		return mapeadoPor;
	}

	public void addParametro(Object parametro) {
		if(this.parametros == null) {
			this.parametros = new ArrayList<Object>();
		}
		this.parametros.add(parametro);
	}

	public List<Object> getParametros() {
		if(this.parametros == null) {
			this.parametros = new ArrayList<Object>();
		}
		return parametros;
	}

	@SuppressWarnings("unchecked")
	public void addTipoPametros(Class... tipos) {
		this.tipoParametros = tipos;
	}

	public void addParametroFromT(Object parametroFromT) {
		if(this.parametrosFromT == null) {
			this.parametrosFromT = new ArrayList<Object>();
		}
		this.parametrosFromT.add(parametroFromT);
	}

	public List<Object> getParametrosFromT() {
		if(this.parametrosFromT == null) {
			this.parametrosFromT = new ArrayList<Object>();
		}
		return parametrosFromT;
	}

	public void addParametroTemp(Object parametroTemp) {
		if(this.parametrosTemp == null) {
			this.parametrosTemp = new ArrayList<Object>();
		}
		this.parametrosTemp.add(parametroTemp);
	}

	public void setParametrosTemp(List<Object> parametrosTemp) {
		this.parametrosTemp = new ArrayList<Object>();
		this.parametrosTemp.addAll(parametrosTemp);
	}

	public Object[] getParametrosTemp() {
		if(this.parametrosTemp == null) {
			this.parametrosTemp = new ArrayList<Object>();
		}
		return parametrosTemp.toArray();
	}

	@SuppressWarnings("unchecked")
	public Class[] getTipoParametros() {
		if(this.tipoParametros == null) {
			if(parametrosTemp != null) {
				List<Class> classes = new ArrayList<Class>();
				for(Object parametro : parametrosTemp) {
					classes.add(parametro.getClass());
				}
				return classes.toArray(new Class[]{});
			}
		}
		return tipoParametros;
	}
}
