package mx.com.afirme.midas.sistema.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class RegistroGuiasProveedorExcel<T> extends MidasExcel<T> {

	public static final String NUM_GUIA = "NUM_GUIA";
	public static final String POLIZA = "POLIZA";
	
	public RegistroGuiasProveedorExcel(Class<T> classObject) {
		super(classObject);
	}
	
	public boolean isValid()  throws FileNotFoundException, IOException{
		String fileName = super.obtenerNombreArchivo(super.getArchivo());
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
		
		if (extension.equalsIgnoreCase(".XLS")){
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		}else if(extension.equalsIgnoreCase(".XLSX")){
			return false;
		}else{
			return false;
		}
		
		return true;
	}
}
