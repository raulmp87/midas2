package mx.com.afirme.midas.sistema.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;

import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class CargaMasivaIncisosExcel<T> extends MidasExcel<T> {
	private String idToCotizacion;
	private String numeroInciso;
	private List<String> sumasAseguradas;
	private List<String> sumasAseguradasInvalidas;
	private String clavesSeccionRiesgo;

	public CargaMasivaIncisosExcel(Class<T> classObject) {
		super(classObject);
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroInciso() {
		return numeroInciso;
	}

	public static HSSFWorkbook generarTemplateCargaMasiva(
			BigDecimal idToCotizacion, BigDecimal numeroInciso)
			throws SystemException, IOException {
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(idToCotizacion + "-" + numeroInciso);
		HSSFRow row = sheet.createRow(0);

		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFFont font = wb.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);
		style.setWrapText(Boolean.TRUE);
		style.setVerticalAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		style.setBottomBorderColor((short) 0);

		int index = 0;
		HSSFCell cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("Calle"));
		sheet.setColumnWidth(index - 1, cell.getRichStringCellValue()
				.getString().length() * 1024);

		cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("N�mero Exterior"));

		cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("N�mero Interior"));

		cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("Estado"));
		sheet.setColumnWidth(index - 1, cell.getRichStringCellValue()
				.getString().length() * 1024);

		cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("Municipio"));
		sheet.setColumnWidth(index - 1, cell.getRichStringCellValue()
				.getString().length() * 512);

		cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("C�digo Postal"));

		cell = row.createCell(index++);
		cell.setCellStyle(style);
		cell.setCellValue(new HSSFRichTextString("Colonia"));
		sheet.setColumnWidth(index - 1, cell.getRichStringCellValue()
				.getString().length() * 1024);

		List<CoberturaCotizacionDTO> coberturas = CoberturaCotizacionDN
				.getInstancia().listarCoberturasBasicasContratadas(
						idToCotizacion, numeroInciso);
		HSSFPatriarch drawing = sheet.createDrawingPatriarch();
		for (CoberturaCotizacionDTO cobertura : coberturas) {
			String header = cobertura.getCoberturaSeccionDTO().getSeccionDTO()
					.getNombreComercial()
					+ " - "
					+ cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getNombreComercial();
			HSSFCell cell1 = row.createCell(index++);
			cell1.setCellStyle(style);
			cell1.setCellValue(new HSSFRichTextString(header));
			sheet.setColumnWidth(index - 1, cell.getRichStringCellValue()
					.getString().length() * 768);

			HSSFAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0,
					(short) (index - 1), 2, (short) (index + 1), 4);
			HSSFComment comment = drawing.createComment(anchor);
			comment.setVisible(Boolean.FALSE);
			String code = cobertura.getCoberturaSeccionDTO().getSeccionDTO()
					.getCodigo()
					+ "-"
					+ cobertura.getCoberturaSeccionDTO().getCoberturaDTO()
							.getCodigo();
			HSSFRichTextString string = new HSSFRichTextString(code);
			comment.setString(string);
			comment.setAuthor("MIDAS");
			cell1.setCellComment(comment);
		}
		return wb;
	}

	public boolean isValid() throws FileNotFoundException, IOException {
		String fileName = super.obtenerNombreArchivo(super.getArchivo());
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		String name = wb.getSheetName(0);
		String[] split = name.split("-");
		if (split[0] != null && split[0].equals(this.idToCotizacion)) {
			if (split[0] != null) {
				this.numeroInciso = split[1];
				return true;
			}
		}
		return false;
	}

	public List<T> getRegistrosValidos() throws SystemException {
		sumasAseguradas = new ArrayList<String>();
		List<T> result = super.getRegistrosValidos();
		for (HSSFRow row : super.getValidRows()) {
			int index = 7;
			StringBuilder sumasAseguradas = new StringBuilder("");
			String sumaAsegurada = "";
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double numericValue = cell.getNumericCellValue();
					HSSFComment comment = super.getHeader().getCell(index - 1)
							.getCellComment();
					sumasAseguradas.append(comment.getString().getString()).append("-");
					sumasAseguradas.append(numericValue);
					break;
				default:
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
					break;
				}
				sumasAseguradas.append("-");
			}
			if(sumasAseguradas != null)
				sumaAsegurada = sumasAseguradas.substring(0, sumasAseguradas.lastIndexOf("-"));			
			this.sumasAseguradas.add(sumaAsegurada);
		}
		return result;
	}

	public List<T> getRegistrosInvalidos() throws SystemException {
		sumasAseguradasInvalidas = new ArrayList<String>();
		List<T> result = super.getRegistrosInvalidos();
		for (HSSFRow row : super.getInvalidRows()) {
			int index = 7;
			StringBuilder sumasAseguradas = new StringBuilder("");
			String sumaAsegurada ="";
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				Double numericValue = 0D;
				HSSFComment comment;
				switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_NUMERIC:
					numericValue = cell.getNumericCellValue();
					comment = super.getHeader().getCell(index - 1)
							.getCellComment();
					sumasAseguradas.append(comment.getString().getString()).append("-");
					sumasAseguradas.append(numericValue);
					break;
				default:
					numericValue = 0D;
					comment = super.getHeader().getCell(index - 1)
							.getCellComment();
					sumasAseguradas.append(comment.getString().getString()).append("-");
					sumasAseguradas.append(numericValue);
					super.addRegistroInvalido(row.getRowNum(),
							"Suma Asegurada", "Tipo de celda invalido.");
					break;
				}
				sumasAseguradas.append("-");
			}
			if(sumasAseguradas != null)
				sumaAsegurada = sumasAseguradas.substring(0, sumasAseguradas.lastIndexOf("-"));
			this.sumasAseguradasInvalidas.add(sumaAsegurada);
		}
		return result;
	}

	public List<String> getSumasAseguradas() {
		return sumasAseguradas;
	}

	public List<String> getSumasAseguradasInvalidas() {
		return sumasAseguradasInvalidas;
	}

	public String getClavesSeccionRiesgo() throws SystemException {
		clavesSeccionRiesgo = "";
		StringBuilder clavesSeccion = new StringBuilder(clavesSeccionRiesgo);
		List<HSSFRow> result = super.getValidRows();
		if(result == null)
			super.getInvalidRows();
			
		if (result != null) {
			HSSFRow row = result.get(0);
			int index = 7;
			while (row.getCell(index) != null) {
				HSSFCell cell = row.getCell(index++);
				if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
					HSSFComment comment = super.getHeader().getCell(index - 1)
					.getCellComment();
					clavesSeccion.append(comment.getString().getString()).append("|");
				}

			}
			clavesSeccionRiesgo = clavesSeccion.toString();
			if (clavesSeccionRiesgo != null && clavesSeccionRiesgo.length() > 0) {
				clavesSeccionRiesgo = clavesSeccionRiesgo.substring(0,
						clavesSeccionRiesgo.length() - 1);
			}
		}
		return clavesSeccionRiesgo;
	}
}
