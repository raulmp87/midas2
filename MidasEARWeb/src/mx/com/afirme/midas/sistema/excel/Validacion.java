package mx.com.afirme.midas.sistema.excel;

import java.util.ArrayList;
import java.util.List;

public class Validacion {
	public enum TipoValidacion {
		LONGITUD_MINIMA, LONGITUD_MAXIMA, NUMERICO, ALFANUMERICO, EXISTENCIA, NO_REQUERIDO
	}

	private TipoValidacion tipoValidacion;
	private String valor;
	private List<Dependencia> dependencias;

	public Validacion(TipoValidacion tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}

	public Validacion(TipoValidacion tipoValidacion, String valor) {
		this.tipoValidacion = tipoValidacion;
		this.valor = valor;
	}

	public void setTipoValidacion(TipoValidacion tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}

	public TipoValidacion getTipoValidacion() {
		return tipoValidacion;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

	public void addDependencia(Dependencia dependencia) {
		if(this.dependencias == null) {
			this.dependencias = new ArrayList<Dependencia>();
		}
		this.dependencias.add(dependencia);
	}

	public List<Dependencia> getDependencias() {
		if(this.dependencias == null) {
			this.dependencias = new ArrayList<Dependencia>();
		}
		return dependencias;
	}
}
