package mx.com.afirme.midas.sistema.excel;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;

public class Propiedad {
	private Integer indice;
	private String nombre;
	private String descripcion;
	private Integer cellType;
	private List<Validacion> validaciones = new ArrayList<Validacion>();

	public Propiedad(Integer indice, String nombre) {
		this.indice = indice;
		this.nombre = nombre;
	}
	
	public Propiedad(Integer indice, String nombre, Integer cellType) {
		this.indice = indice;
		this.nombre = nombre;
		this.cellType = cellType;
	}

	public Integer getCellType() {
		return cellType;
	}

	public void setCellType(Integer cellType) {
		this.cellType = cellType;
	}

	public void setIndice(Integer indice) {
		this.indice = indice;
	}

	public Integer getIndice() {
		return indice;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void addValidacion(Validacion validacion) {
		validaciones.add(validacion);
	}

	public List<Validacion> getValidaciones() {
		return validaciones;
	}

	public Boolean contains(TipoValidacion tipoValidacion) {
		for(Validacion validacion : validaciones) {
			if(validacion.getTipoValidacion().equals(tipoValidacion)) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
}
