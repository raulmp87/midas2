package mx.com.afirme.midas.sistema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.taglib.TagUtils;
import org.apache.struts.taglib.bean.MessageTag;


public class Etiqueta
    extends MessageTag {

  private static final long serialVersionUID = 1L;

  private static final Logger LOG = Logger.getLogger(Etiqueta.class);

  private String normalClass = null;

  private String errorClass = null;

  private String errorImage = null;
  
  private String div = null;

  public String getDiv() {
    return this.div;
  }

  public void setDiv(String div) {
    this.div = div;
  }

  public String getErrorImage() {
    return this.errorImage;
  }

  public void setErrorImage(String errorImage) {
    this.errorImage = errorImage;
  }

  public String getErrorClass() {
    return this.errorClass;
  }

  public void setErrorClass(String errorClass) {
    this.errorClass = errorClass;
  }

  public String getNormalClass() {
    return this.normalClass;
  }

  public void setNormalClass(String normalClass) {
    this.normalClass = normalClass;
  }

  private boolean hasError() {
    boolean hasError = false;
    TagUtils tagUtils = TagUtils.getInstance();
    try{
      ActionMessages errors =
          tagUtils.getActionMessages(super.pageContext, Globals.ERROR_KEY);
      if(errors.size(this.property) != 0)
        hasError = true;
    }catch (JspException jspException){
      LOG.error(
          "An unexpected error ocurred while trying to retrive the actionmessages.",
          jspException);
    } // End of try/catch
    return hasError;
  }

  @SuppressWarnings("unchecked")
public String getErrorMessage() throws JspException {
    StringBuffer errorMessage = new StringBuffer();
    TagUtils tagUtils = TagUtils.getInstance();
    ActionMessages errors =
        tagUtils.getActionMessages(super.pageContext, Globals.ERROR_KEY);
    Iterator propertyIterator = errors.properties();

    // Get Messages
    while(propertyIterator.hasNext()){
      String property = (String)propertyIterator.next();
      if(!property.equals(this.property))
        continue;
      Iterator messages = errors.get(property);
      while(messages.hasNext()){
        ActionMessage actionMessage = (ActionMessage)messages.next();
        String textMessage =
            tagUtils.message(super.pageContext, null, Globals.LOCALE_KEY,
                actionMessage.getKey(), actionMessage.getValues());
        if(errorMessage.length() != 0)
          errorMessage.append("; ");
        errorMessage.append(textMessage);
      } // End of while(messages)
      break;
    } // End of while(properties)

    return removeHtml(errorMessage.toString());
  }

  public int doStartTag() throws JspException {
    TagUtils tagUtils = TagUtils.getInstance();
    HttpServletRequest request =
      (HttpServletRequest)this.pageContext.getRequest();
    String path = request.getContextPath();
    
    String key = this.key;
    if(key == null){
      // Look up the requested property value
      Object value =
          tagUtils.lookup(this.pageContext, this.name, this.property,
              this.scope);
      if((value != null) && !(value instanceof String)){
        JspException jspException =
            new JspException(messages.getMessage("message.property", key));
        tagUtils.saveException(this.pageContext, jspException);
        throw jspException;
      }
      key = (String)value;
    }

    // Construct the optional arguments array we will be using
    Object[] args = new Object[] {
      this.arg0, this.arg1, this.arg2, this.arg3, this.arg4};

    // Retrieve the message string we are looking for
    String message =
        tagUtils.message(this.pageContext, this.bundle, this.localeKey, key,
            args);

    if(message == null){
      Locale locale = tagUtils.getUserLocale(this.pageContext, this.localeKey);
      String localeVal =
          (locale == null) ? "default locale" : locale.toString();
      JspException jspException =
          new JspException(messages.getMessage("message.message", "\"" + key
              + "\"", "\""
              + ((this.bundle == null) ? "(default bundle)" : this.bundle)
              + "\"", localeVal));
      tagUtils.saveException(this.pageContext, jspException);
      throw jspException;
    }

    boolean hasError = this.hasError();
    StringBuffer text = new StringBuffer();
    String title = this.getErrorMessage();
    text.append("<span id=\"");
    text.append(this.name);
    text.append("_");
    text.append(this.property);
    text.append("_span");
    text.append("\" title=\"");
    text.append(title);
    text.append("\">");
    if(this.div!=null) {
      text.append("<input type=\"hidden\" name=\"error_div_");
      text.append(this.name);
      text.append("_");
      text.append(this.div);
      text.append("\" value=\"");
      text.append(hasError);
      text.append("\"/>");
    } // End of if
    text.append("<table class=\"");
    text.append(hasError ? this.errorClass : this.normalClass);
    text.append("\"><tr>");
    text.append("<td nowrap=\"nowrap\" valign=\"middle\">");
    text.append(message);
    text.append("</td>");
    if(hasError){
      String imagePath = path + this.errorImage;
      text.append("<td valign=\"middle\">");
      text.append("<img src=\"" + imagePath + "\" border=\"0\"/>");
      text.append("</td>");
    } // End of if
    text.append("</tr>");
    text.append("</table>");
    text.append("</span>");

    tagUtils.write(this.pageContext, text.toString());
    return SKIP_BODY;
  }
  
  @SuppressWarnings("unchecked")
private String removeHtml(String message) {
    List tags = new ArrayList();
    tags.add("br");
    Iterator tagsIterator = tags.iterator();
    while(tagsIterator.hasNext()) {
      String tag = (String)tagsIterator.next();
      message = removeTag(message, tag);
    } // End of while
    
    message = message.replaceAll("(\\\u002A)", "");
    while(message.indexOf("  ")!=-1) {
      message = message.replaceAll("(  )", " ");
    } // End of while
    return message.trim();
  }
  
  private String removeTag(String message, String tag) {
    StringBuffer newMessage = new StringBuffer();
    int startIndex = 0;
    int endIndex = 0;
    boolean first = true;
    String startTag = "<" + tag.toLowerCase();
    String messageLower = message.toLowerCase();
    while((startIndex=messageLower.indexOf(startTag, endIndex))!=-1) {
      if(first) {
        newMessage.append(message.substring(0, startIndex));
        first = false;
      } else {
        newMessage.append(message.substring(endIndex+1, startIndex));
      } // End of if/else
      endIndex = message.indexOf(">", startIndex);
    } // End of while
    newMessage.append(message.substring(endIndex+(first?0:1)));
    return newMessage.toString();
  }
  
}
