/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;

/**
 * @author andres.avalos
 *
 */
public class TemporizadorDistribuirPrimaDN {

	private static final TemporizadorDistribuirPrimaDN INSTANCIA = new TemporizadorDistribuirPrimaDN();
	
	public static TemporizadorDistribuirPrimaDN getInstancia (){
		return TemporizadorDistribuirPrimaDN.INSTANCIA;
	}
	
	public void iniciar() {
		try {
			TemporizadorDistribuirPrimaSN temporizadorSN = new TemporizadorDistribuirPrimaSN();
			temporizadorSN.iniciar();
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo iniciar el temporizador para la Distribuciond e la Prima", Level.WARNING, null);
		}
	}
	
	public void detener() {
		try {
			TemporizadorDistribuirPrimaSN temporizadorSN = new TemporizadorDistribuirPrimaSN();
			temporizadorSN.detener();
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo detener el temporizador para la Distribuciond e la Prima", Level.WARNING, null);
			ex.printStackTrace();
		}
		
	}
	
}
