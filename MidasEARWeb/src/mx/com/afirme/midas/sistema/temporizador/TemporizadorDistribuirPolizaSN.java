/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author alfredo.osorio
 *
 */
public class TemporizadorDistribuirPolizaSN {

	private TemporizadorDistribuirPolizaFacadeRemote beanRemoto;
	ServiceLocator serviceLocator;
	
	public TemporizadorDistribuirPolizaSN() throws SystemException {
		try {
			serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TemporizadorDistribuirPolizaFacadeRemote.class);
						
		} catch (Exception e) {
			
			throw new SystemException(Sistema.NO_DISPONIBLE);
		
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
		
	public void iniciar(SoporteReaseguroDTO soporteReaseguroDTO) {
		try {
			beanRemoto.iniciarTemporizador(Sistema.TIEMPO_INICIAR_DISTRIBUIR_POLIZA, soporteReaseguroDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	public void iniciar(BigDecimal idToSoporteReaseguro) {
		try {
			beanRemoto.iniciarTemporizador(Sistema.TIEMPO_INICIAR_DISTRIBUIR_POLIZA, idToSoporteReaseguro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	public void detenerTemporizadoresDistribucionPolizas(){
		try {
			beanRemoto.detenerTemporizadoresDistribucionPolizas();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
}
