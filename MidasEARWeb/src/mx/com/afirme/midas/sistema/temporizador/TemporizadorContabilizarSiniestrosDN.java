package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class TemporizadorContabilizarSiniestrosDN {
private static final TemporizadorContabilizarSiniestrosDN INSTANCIA = new TemporizadorContabilizarSiniestrosDN();
	
	public static TemporizadorContabilizarSiniestrosDN getInstancia (){
		return TemporizadorContabilizarSiniestrosDN.INSTANCIA;
	}
	
	public void iniciar(String horaInicio) {
		try {
			
			TemporizadorContabilizarSiniestrosSN temporizadorSN = new TemporizadorContabilizarSiniestrosSN();
			temporizadorSN.iniciar(UtileriasWeb.ajustarHoraInicio(horaInicio), Sistema.TIEMPO_INTERVALO_SINIESTRO);
			
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo iniciar el temporizador de contabilizar movimientos de Siniestros de MIDAS", Level.WARNING, null);
		}
	}
	
	public void detener() {
		try {
			TemporizadorContabilizarSiniestrosSN temporizadorSN = new TemporizadorContabilizarSiniestrosSN();
			temporizadorSN.detener();
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo detener el temporizador de contabilizar movimientos de Siniestros de MIDAS", Level.WARNING, null);
			ex.printStackTrace();
		}
		
	}
	
}
