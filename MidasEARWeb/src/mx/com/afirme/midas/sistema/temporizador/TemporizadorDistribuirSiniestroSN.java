/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author alfredo.osorio
 *
 */
public class TemporizadorDistribuirSiniestroSN {

	private TemporizadorDistribuirSiniestroFacadeRemote beanRemoto;
	ServiceLocator serviceLocator;
	
	public TemporizadorDistribuirSiniestroSN() throws SystemException {
		try {
			serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TemporizadorDistribuirSiniestroFacadeRemote.class);
						
		} catch (Exception e) {
			
			throw new SystemException(Sistema.NO_DISPONIBLE);
		
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
		
	public void iniciar(
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, 
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToDistribucionMovSiniestro) {
		try {
			beanRemoto.iniciarTemporizador(Sistema.TIEMPO_INICIAR_DISTRIBUIR_POLIZA, 
					idToPoliza, numeroEndoso, idToSeccion, idToCobertura, 
					numeroInciso, numeroSubInciso, conceptoMovimiento, idToMoneda, 
					fechaMovimiento, idMovimientoSiniestro, 
					montoMovimiento, tipoMovimiento, idToReporteSiniestro, 
					idToDistribucionMovSiniestro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}	
}
