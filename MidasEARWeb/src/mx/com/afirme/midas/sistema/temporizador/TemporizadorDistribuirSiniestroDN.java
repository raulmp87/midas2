/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;

/**
 * @author andres.avalos
 *
 */
public class TemporizadorDistribuirSiniestroDN {

	private static final TemporizadorDistribuirSiniestroDN INSTANCIA = new TemporizadorDistribuirSiniestroDN();
	
	public static TemporizadorDistribuirSiniestroDN getInstancia (){
		return TemporizadorDistribuirSiniestroDN.INSTANCIA;
	}
	
	public void iniciar(BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, 
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro, 
			BigDecimal idToDistribucionMovSiniestro) {
		try {
			TemporizadorDistribuirSiniestroSN temporizadorSN = new TemporizadorDistribuirSiniestroSN();
			temporizadorSN.iniciar(idToPoliza, numeroEndoso, 
					idToSeccion, idToCobertura, numeroInciso, 
					numeroSubInciso, conceptoMovimiento, idToMoneda, 
					fechaMovimiento, idMovimientoSiniestro, montoMovimiento, 
					tipoMovimiento, idToReporteSiniestro, idToDistribucionMovSiniestro);
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo iniciar el temporizador para distribucion del siniestro", Level.WARNING, null);
		}
	}
	
}
