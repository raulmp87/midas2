package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TemporizadorContabilizarSiniestrosSN {
	
	private TemporizadorContabilizarSiniestrosFacadeRemote beanRemoto;
	ServiceLocator serviceLocator;
	
	public TemporizadorContabilizarSiniestrosSN() throws SystemException {
		try {
			serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TemporizadorContabilizarSiniestrosFacadeRemote.class);
						
		} catch (Exception e) {
			
			throw new SystemException(Sistema.NO_DISPONIBLE);
		
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
		
	public void iniciar(long tiempoIniciar, long tiempoIntervalo) {
		try {
			//Primero detiene los temporizadores que pudieran seguir residiendo en el servidor
			beanRemoto.detenerTemporizador();
			//Ahora inicia uno nuevo
			beanRemoto.iniciarTemporizador(tiempoIniciar, tiempoIntervalo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	public void detener() {
		try {
			beanRemoto.detenerTemporizador();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
}
