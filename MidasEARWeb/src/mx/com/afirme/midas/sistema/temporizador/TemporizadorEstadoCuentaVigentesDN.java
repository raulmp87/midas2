/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

/**
 * @author hector sarmiento 
 *
 */
public class TemporizadorEstadoCuentaVigentesDN {

	private static final TemporizadorEstadoCuentaVigentesDN INSTANCIA = new TemporizadorEstadoCuentaVigentesDN();
	
	public static TemporizadorEstadoCuentaVigentesDN getInstancia (){
		return TemporizadorEstadoCuentaVigentesDN.INSTANCIA;
	}
 	
	public void iniciar(String horaInicio) {
		try {
			TemporizadorEstadoCuentaVigentesSN temporizadorEstadoCuentaVigentesSN = new TemporizadorEstadoCuentaVigentesSN();
			temporizadorEstadoCuentaVigentesSN.iniciar(UtileriasWeb.ajustarHoraInicio(horaInicio), Sistema.TIEMPO_INTERVALO_EDOCUENTA_VIGENTES);
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo iniciar la creacion estado cuenta por fecha", Level.WARNING, null);
		}
	} 
 	
 	public void detener() {
		try {
			TemporizadorEstadoCuentaVigentesSN temporizadorEstadoCuentaVigentesSN = new TemporizadorEstadoCuentaVigentesSN();
			temporizadorEstadoCuentaVigentesSN.detener();
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo detener el temporizador de contabilizar movimientos de Siniestros de MIDAS", Level.WARNING, null);
			ex.printStackTrace();
		}
	}
}
