/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author alfredo.osorio
 *
 */
public class TemporizadorDistribuirPrimaSN {

	private TemporizadorDistribuirPrimaFacadeRemote beanRemoto;
	ServiceLocator serviceLocator;
	
	public TemporizadorDistribuirPrimaSN() throws SystemException {
		try {
			serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TemporizadorDistribuirPrimaFacadeRemote.class);
						
		} catch (Exception e) {
			
			throw new SystemException(Sistema.NO_DISPONIBLE);
		
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
		
	public void iniciar() {
		try {
			//Primero detiene los temporizadores que pudieran seguir residiendo en el servidor
			beanRemoto.detenerTemporizador();
			//Ahora inicia uno nuevo
			beanRemoto.iniciarTemporizador(Sistema.TIEMPO_INICIAR_DISTRIBUIR_PRIMA);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	public void detener() {
		try {
			beanRemoto.detenerTemporizador();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
}
