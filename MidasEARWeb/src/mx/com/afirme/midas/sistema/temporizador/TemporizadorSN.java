/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class TemporizadorSN {

	private TemporizadorFacadeRemote beanRemoto;
	ServiceLocator serviceLocator;
	
	public TemporizadorSN() throws SystemException {
		try {
			serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TemporizadorFacadeRemote.class);
						
		} catch (Exception e) {
			
			throw new SystemException(Sistema.NO_DISPONIBLE);
		
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
		
	public void iniciar(long tiempoIniciar, long tiempoIntervalo, String maximoRegistros) {
		try {
			//Primero detiene los temporizadores que pudieran seguir residiendo en el servidor
			beanRemoto.detenerTemporizador();
			//Ahora inicia uno nuevo
			beanRemoto.iniciarTemporizador(tiempoIniciar, tiempoIntervalo, maximoRegistros);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	public void detener() {
		try {
			beanRemoto.detenerTemporizador();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
}
