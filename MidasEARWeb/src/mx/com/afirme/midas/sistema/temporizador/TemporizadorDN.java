/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

/**
 * @author andres.avalos
 *
 */
public class TemporizadorDN {

	private static final TemporizadorDN INSTANCIA = new TemporizadorDN();
	
	public static TemporizadorDN getInstancia (){
		return TemporizadorDN.INSTANCIA;
	}
	
	public void iniciar(String horaInicio) {
		try {
			TemporizadorSN temporizadorSN = new TemporizadorSN();
			temporizadorSN.iniciar(UtileriasWeb.ajustarHoraInicio(horaInicio), Sistema.TIEMPO_INTERVALO_LOG, Sistema.MAX_REGISTROS_LOG);
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo iniciar el temporizador de MIDAS", Level.WARNING, null);
		}
	}
	
	public void detener() {
		try {
			TemporizadorSN temporizadorSN = new TemporizadorSN();
			temporizadorSN.detener();
		} catch (Exception ex) {
			LogDeMidasWeb.log("No se pudo detener el temporizador de MIDAS", Level.WARNING, null);
			ex.printStackTrace();
		}
		
	}
	
}
