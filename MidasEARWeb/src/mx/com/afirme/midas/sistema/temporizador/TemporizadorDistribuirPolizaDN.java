/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.distribucion.DistribucionReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroBase;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.intermoduloerrorlog.InterModuloErrorLogDN;
import mx.com.afirme.midas.sistema.intermoduloerrorlog.InterModuloErrorLogDTO;
import mx.com.afirme.midas.sistema.mail.MailAction;

/**
 * @author alfredo.osorio
 *
 */
public class TemporizadorDistribuirPolizaDN {

	private static final TemporizadorDistribuirPolizaDN INSTANCIA = new TemporizadorDistribuirPolizaDN();
	
	public static final String MENSAJE_ERROR_REGISTRO_DISTRIBUCION_MOV_SINIESTRO = "error crear ToDistribucionMovSiniestro";
	public static final String MENSAJE_ERROR_INICIAR_TIMER_DISTRIBUCION_SINIESTRO = "error distribuir ToDistribucionMovSiniestro";
	
	public static TemporizadorDistribuirPolizaDN getInstancia (){
		return TemporizadorDistribuirPolizaDN.INSTANCIA;
	}
	
	public boolean iniciar(SoporteReaseguroDTO soporteReaseguroDTO) {
		boolean resultado = true;
		try {
			TemporizadorDistribuirPolizaSN temporizadorSN = new TemporizadorDistribuirPolizaSN();
			temporizadorSN.iniciar(soporteReaseguroDTO);
		} catch (Exception ex) {
			generarLogErrorProgramarTimerEmision(soporteReaseguroDTO.getIdToSoporteReaseguro(), soporteReaseguroDTO.getIdToPoliza(), soporteReaseguroDTO.getNumeroEndoso(), ex);			
			resultado = false;
		}
		return resultado;
	}
	
	public void generarLogErrorProgramarTimerSiniestros(){
		
	}
	
	private void generarLogErrorProgramarTimerEmision(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza,Integer numeroEndoso,Exception e){
		String error = "No se pudo iniciar el temporizador para distribucion de la prima del soporte: idToSoporteReaseguro:"+idToSoporteReaseguro+"de poliza: idToPoliza: "+idToPoliza+", endoso: "+numeroEndoso;
		String metodo = "TemporizadorDistribuirPolizaDN.iniciar(soporteReaseguroDTO)";
		String retornoMetodoOrigenError = "error iniciar timer,"+idToSoporteReaseguro;
		LogDeMidasWeb.log(error, Level.SEVERE, e);
		UtileriasWeb.registraLogInteraccionReaseguro(metodo, (short)2, this.toString(), metodo,"SISTEMA",
				idToSoporteReaseguro.toString(), retornoMetodoOrigenError, null, error, "Pendiente Lanzar nuevamente la distribuci�n.", this.toString(), (short)2);
	}
	
	public boolean iniciar(BigDecimal idToSoporteReaseguro) {
		boolean resultado = true;
		try {
			TemporizadorDistribuirPolizaSN temporizadorSN = new TemporizadorDistribuirPolizaSN();
			temporizadorSN.iniciar(idToSoporteReaseguro);
		} catch (Exception ex) {
			generarLogErrorProgramarTimerEmision(idToSoporteReaseguro, null, null, ex);
			resultado = false;
		}
		return resultado;
	}

	public void iniciarDistribucionPolizasPendientes() {
		//B�squeda de los registros intermoderrorlog correspondientes a soportes que no han sido distribuidos.
		procesarLogSoportesNoDistribuidos();
		
		//B�squeda de los registros intermoduloerrorlog correspondientes a soportes inconsistentes.
		procesarLogSoportesInconsistentes();
		
		//B�squeda de los registros intermoderrorlog correspondientes a movimientos de siniestros que no han sido registrados.
		procesarLogMovtoSiniestroNoResgitrado();
		
		//B�squeda de los registros intermoderrorlog correspondientes a movimientos de siniestros que no han sido distribuidos.
		procesarLogMovtoSiniestroNoDistribuido();
	}
	
	public void detenerTemporizadoresDistribucionPolizas() throws SystemException{
		new TemporizadorDistribuirPolizaSN().detenerTemporizadoresDistribucionPolizas();
	}
	
	private void enviarCorreoNotificacionSoporteIncorrecto(SoporteReaseguroDTO soporteReaseguroDTO,String descripcionError){
		List<String> destinatarios = new ArrayList<String>();
		String destinatario = Sistema.EMAIL_ADMINISTRADOR_REASEGURO;
		if(!UtileriasWeb.esCadenaVacia(destinatario))
			destinatarios.add(destinatario);
		String titulo = "Midas: Error detectado en soporte de reaseguro, idToPoliza: "+soporteReaseguroDTO.getIdToPoliza()+", endoso: "+soporteReaseguroDTO.getNumeroEndoso();
		
		MailAction.enviaCorreo(destinatarios, titulo, descripcionError);
	}
	
	private void procesarLogSoportesNoDistribuidos(){
		//B�squeda de los registros intermoderrorlog correspondientes a soportes que no han sido distribuidos.
		InterModuloErrorLogDTO filtro = new InterModuloErrorLogDTO();
		String retornoMetodoorigenError = "error iniciar timer,";
		filtro.setRetornoMetodoorigenError(retornoMetodoorigenError);
		
		List<InterModuloErrorLogDTO> registrosSoportesNoDistribuidos = null;
		try{
			registrosSoportesNoDistribuidos = InterModuloErrorLogDN.getInstancia().listarFiltrado(filtro);
		}catch(Exception e){}
		
		if(registrosSoportesNoDistribuidos != null && !registrosSoportesNoDistribuidos.isEmpty()){
			LogDeMidasWeb.log("Se encontraron "+registrosSoportesNoDistribuidos.size()+" registros ToInterModuloErrorLog de Soportes de reaseguro pendientes por distribuir. Se programar�n los timers para su distribuci�n.",Level.SEVERE, null);
			for (InterModuloErrorLogDTO logErrorSoporte : registrosSoportesNoDistribuidos){
				String[] datosError = logErrorSoporte.getRetornoMetodoorigenError().split(",");
				if(datosError.length >= 2){
					BigDecimal idToSoporteReaseguro = null;
					try{
						idToSoporteReaseguro = UtileriasWeb.regresaBigDecimal(datosError[1]);
						
						if(idToSoporteReaseguro!= null){
							List<String> listaErrores = new ArrayList<String>();
							boolean soporteReaseguroValido = SoporteReaseguroDN.getInstancia().validarSoporteReaseguroDTOListoParaDistribucion(idToSoporteReaseguro, listaErrores);
							if(soporteReaseguroValido){
								//De ser consistente se determina que se puede distribuir esta emisi�n sin problema y lanza un Temporizador para ello.
								LogDeMidasWeb.log("TemporizadorDistribuirPolizaDN.iniciarDistribucionPolizasPendientes(): Se prepara a lanzar el Temporizador del proceso asincrono de Distribucion de Prima para el idToSoporteReaseguro: " + idToSoporteReaseguro,Level.SEVERE, null);
								iniciar(idToSoporteReaseguro);
								LogDeMidasWeb.log("TemporizadorDistribuirPolizaDN.iniciarDistribucionPolizasPendientes(): Se lanz� el Temporizador del proceso asincrono de Distribucion de Prima para el idToSoporteReaseguro: " + idToSoporteReaseguro,Level.SEVERE, null);
								
								logErrorSoporte.setRetornoMetodoorigenError("Se program� el timer exitosamente. "+DateFormat.getDateTimeInstance().format(new Date()));
								InterModuloErrorLogDN.getInstancia().modificar(logErrorSoporte);
							}else{
								SoporteReaseguroBase.generarLogSoporteReaseguroInvalido(idToSoporteReaseguro, listaErrores);
							}
						}
					}
					catch(Exception e){
						LogDeMidasWeb.log("Ocurri� un error al validar e iniciar el timer del idToSoporteReaseguro: "+idToSoporteReaseguro, Level.SEVERE, e);
					}
				}
			}
		}
		else{
			LogDeMidasWeb.log("No se encontraron registros ToInterModErrorLog que indiquen soportes pendientes por distribuir.", Level.SEVERE, null);
		}
	}
	
	private void procesarLogSoportesInconsistentes(){
		InterModuloErrorLogDTO filtro = new InterModuloErrorLogDTO();
		String retornoMetodoorigenError = "error iniciar timer,";
		retornoMetodoorigenError = "soporte inconsistente,";
		filtro.setRetornoMetodoorigenError(retornoMetodoorigenError);
		
		List<InterModuloErrorLogDTO> registrosSoportesInconsistentes = null;
		try{
			registrosSoportesInconsistentes = InterModuloErrorLogDN.getInstancia().listarFiltrado(filtro);
		}catch(Exception e){}
		
		if(registrosSoportesInconsistentes != null && !registrosSoportesInconsistentes.isEmpty()){
			LogDeMidasWeb.log("Se encontraron "+registrosSoportesInconsistentes.size()+" registros ToInterModuloErrorLog de Soportes de reaseguro incorrectos.",Level.SEVERE, null);
			for (InterModuloErrorLogDTO logErrorSoporte : registrosSoportesInconsistentes){
				String[] datosError = logErrorSoporte.getRetornoMetodoorigenError().split(",");
				if(datosError.length >= 2){
					try{
						BigDecimal idToSoporteReaseguro = UtileriasWeb.regresaBigDecimal(datosError[1]);
						SoporteReaseguroDTO soporteReaseguroDTO = SoporteReaseguroDN.getInstancia().buscarPorIdToSoporteReaseguro(idToSoporteReaseguro);
						if(soporteReaseguroDTO != null){
							StringBuilder error = new StringBuilder("Descripci&oacute;n del error: ");
							error.append(logErrorSoporte.getDescripcionError());
							error.append("<br>Comentarios adicionales: ").append(logErrorSoporte.getComentariosAdicionales());
							enviarCorreoNotificacionSoporteIncorrecto(soporteReaseguroDTO, error.toString());
							
							logErrorSoporte.setRetornoMetodoorigenError("correo enviado. "+DateFormat.getDateTimeInstance().format(new Date()));
							InterModuloErrorLogDN.getInstancia().modificar(logErrorSoporte);
						}
					}
					catch(Exception e){
					}
				}
			}
		}
		else{
			LogDeMidasWeb.log("No se encontraron registros ToInterModErrorLog que indiquen soportes inconsistentes, pendientes por analizar.", Level.SEVERE, null);
		}
	}
	
	private void procesarLogMovtoSiniestroNoResgitrado(){
		//B�squeda de los registros intermoderrorlog correspondientes a soportes que no han sido distribuidos.
		InterModuloErrorLogDTO filtro = new InterModuloErrorLogDTO();
		String retornoMetodoorigenError = TemporizadorDistribuirPolizaDN.MENSAJE_ERROR_REGISTRO_DISTRIBUCION_MOV_SINIESTRO;
		filtro.setRetornoMetodoorigenError(retornoMetodoorigenError);
		
		List<InterModuloErrorLogDTO> listaDistribucionMovSiniestroNoRegistrados = null;
		try{
			listaDistribucionMovSiniestroNoRegistrados = InterModuloErrorLogDN.getInstancia().listarFiltrado(filtro);
		}catch(Exception e){}
		
		if(listaDistribucionMovSiniestroNoRegistrados != null && !listaDistribucionMovSiniestroNoRegistrados.isEmpty()){
			LogDeMidasWeb.log("Se encontraron "+listaDistribucionMovSiniestroNoRegistrados.size()+" registros ToInterModuloErrorLog de objetos DistribucionMovSiniestroDTO que no pudieron ser registrados.",Level.SEVERE, null);
			for (InterModuloErrorLogDTO logErrorSoporte : listaDistribucionMovSiniestroNoRegistrados){
				//Se espera que este campo contenga los siguientes datos, como se ve, separados por comma:
//				0"idToPoliza:"+idToPoliza+1",numeroEndoso:"+numeroEndoso+"," +
//				2"idToSeccion:"+idToSeccion+3",idToCobertura:"+idToCobertura+4",numeroInciso:"+numeroInciso+5",numeroSubInciso:"+numeroSubInciso+"," +
//				6"conceptoMovimiento:"+conceptoMovimiento+7",idToMoneda:"+idToMoneda+"," +
//				8"fechaMovimiento:"+fechaMovimiento+9",idMovimientoSiniestro:"+idMovimientoSiniestro+"," +
//				10"montoMovimiento:"+montoMovimiento+11",tipoMovimiento:"+tipoMovimiento+"," +
//				12"idToReporteSiniestro:"+idToReporteSiniestro
				String[] datosSiniestro= logErrorSoporte.getParamMetodoOrigenError().split(",");
				if(datosSiniestro.length >= 13){
					try{
						DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
						BigDecimal idToPoliza=null,idToSeccion=null,idToCobertura=null,idToMoneda=null,idMovimientoSiniestro=null,montoMovimiento=null,idToReporteSiniestro=null;
						Integer numeroEndoso=null,numeroInciso=null,numeroSubInciso=null,conceptoMovimiento=null,tipoMovimiento=null;
						Date fechaMovimiento = null;
						String nombreUsuario = "SISTEMA";
						
						for(int i=0;i<datosSiniestro.length;i++){
							String datoIndividual = datosSiniestro[i];
							datoIndividual = datoIndividual.split(":")[1].trim();
							switch(i){
								case 0://"idToPoliza:"+idToPoliza
									idToPoliza = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 1://1",numeroEndoso:"+numeroEndoso
									numeroEndoso = new Integer(datoIndividual);
									break;
								case 2://2"idToSeccion:"+idToSeccion
									idToSeccion = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 3://3",idToCobertura:"+idToCobertura
									idToCobertura = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 4://4",numeroInciso:"+numeroInciso
									if(!UtileriasWeb.esCadenaVacia(datoIndividual) && !datoIndividual.equals("null")){
										numeroInciso = new Integer(datoIndividual);
									}
									else{
										numeroInciso = 0;
									}
									break;
								case 5://5",numeroSubInciso:"+numeroSubInciso
									if(!UtileriasWeb.esCadenaVacia(datoIndividual) && !datoIndividual.equals("null")){
										numeroSubInciso = new Integer(datoIndividual);
									}
									else{
										numeroSubInciso = 0;
									}
									break;
								case 6://6"conceptoMovimiento:"+conceptoMovimiento
									conceptoMovimiento = new Integer(datoIndividual);
									break;
								case 7://7",idToMoneda:"+idToMoneda
									idToMoneda = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 8://8"fechaMovimiento:"+fechaMovimiento
									fechaMovimiento = formatoFecha.parse(datoIndividual);
									break;
								case 9://9",idMovimientoSiniestro:"+idMovimientoSiniestro
									idMovimientoSiniestro = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 10://10"montoMovimiento:"+montoMovimiento
									montoMovimiento = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 11://11",tipoMovimiento:"+tipoMovimiento
									tipoMovimiento = new Integer(datoIndividual);
									break;
								case 12://12"idToReporteSiniestro:"+idToReporteSiniestro
									idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
							}
						}
						if(idToPoliza != null && idToSeccion != null && idToCobertura != null && idToMoneda != null && 
								idMovimientoSiniestro != null && montoMovimiento != null && idToReporteSiniestro != null && 
								numeroEndoso != null && numeroInciso != null && numeroSubInciso != null && conceptoMovimiento != null && 
								tipoMovimiento != null && fechaMovimiento != null){
							//intentar distribuir el siniestro nuevamente.
							try{
								DistribucionReaseguroDN.getInstancia().distribuirMontoSiniestro(
									idToPoliza, numeroEndoso, idToSeccion, idToCobertura, numeroInciso, numeroSubInciso,
									conceptoMovimiento, idToMoneda, fechaMovimiento, idMovimientoSiniestro, montoMovimiento, 
									tipoMovimiento, idToReporteSiniestro, nombreUsuario);
							}catch(Exception e){
								LogDeMidasWeb.log("Ocurri� un error al intentar distribuir el siniestro. ", Level.SEVERE, e);
							}
							
							//actualizar el registro de log, para no procesarlo nuevamente
							logErrorSoporte.setRetornoMetodoorigenError("Se proces� exitosamente el movimiento de siniestro. "+DateFormat.getDateTimeInstance().format(new Date()));
							InterModuloErrorLogDN.getInstancia().modificar(logErrorSoporte);
						}
						else{
							String error = "Se encontr� un log de error de distribuci�n de siniestro inv�lido, los par�metros son incorrectos:"+
								logErrorSoporte.getParamMetodoOrigenError();
							LogDeMidasWeb.log(error, Level.SEVERE, null);
							logErrorSoporte.setComentariosAdicionales(error);
							InterModuloErrorLogDN.getInstancia().modificar(logErrorSoporte);
						}
					}
					catch(Exception e){
						LogDeMidasWeb.log("Ocurri� un error al intentar registrar y distribuir un movimiento de reaseguro: "+logErrorSoporte.getParamMetodoOrigenError(), Level.SEVERE, e);
					}
				}
			}
		}
		else{
			LogDeMidasWeb.log("No se encontraron registros ToInterModErrorLog que indiquen movimientos de siniestros pendientes por registrar.", Level.SEVERE, null);
		}
	}
	
	private void procesarLogMovtoSiniestroNoDistribuido(){
		//B�squeda de los registros intermoderrorlog correspondientes a soportes que no han sido distribuidos.
		InterModuloErrorLogDTO filtro = new InterModuloErrorLogDTO();
		String retornoMetodoorigenError = TemporizadorDistribuirPolizaDN.MENSAJE_ERROR_INICIAR_TIMER_DISTRIBUCION_SINIESTRO;
		filtro.setRetornoMetodoorigenError(retornoMetodoorigenError);
		
		List<InterModuloErrorLogDTO> distribucionMovSiniestroNoDistribuidos = null;
		try{
			distribucionMovSiniestroNoDistribuidos = InterModuloErrorLogDN.getInstancia().listarFiltrado(filtro);
		}catch(Exception e){}
		
		if(distribucionMovSiniestroNoDistribuidos != null && !distribucionMovSiniestroNoDistribuidos.isEmpty()){
			LogDeMidasWeb.log("Se encontraron "+distribucionMovSiniestroNoDistribuidos.size()+" registros ToInterModuloErrorLog de objetos DistribucionMovSiniestroDTO que no pudieron ser registrados.",Level.SEVERE, null);
			for (InterModuloErrorLogDTO logErrorSoporte : distribucionMovSiniestroNoDistribuidos){
				//Se espera que este campo contenga los siguientes datos, como se ve, separados por comma:
				
				String[] datosSiniestro= logErrorSoporte.getParamMetodoOrigenError().split(",");
				if(datosSiniestro.length >= 13){
					try{
						DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
						BigDecimal idToPoliza=null,idToSeccion=null,idToCobertura=null,idToMoneda=null,idMovimientoSiniestro=null,
											montoMovimiento=null,idToReporteSiniestro=null,idToDistribucionMovSiniestro=null;
						Integer numeroEndoso=null,numeroInciso=null,numeroSubInciso=null,conceptoMovimiento=null,tipoMovimiento=null;
						Date fechaMovimiento = null;
						
						for(int i=0;i<datosSiniestro.length;i++){
							String datoIndividual = datosSiniestro[i];
							datoIndividual = datoIndividual.split(":")[1].trim();
							switch(i){
								case 0://"idToPoliza:"+idToPoliza
									idToPoliza = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 1://1",numeroEndoso:"+numeroEndoso
									numeroEndoso = new Integer(datoIndividual);
									break;
								case 2://2"idToSeccion:"+idToSeccion
									idToSeccion = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 3://3",idToCobertura:"+idToCobertura
									idToCobertura = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 4://4",numeroInciso:"+numeroInciso
									if(!UtileriasWeb.esCadenaVacia(datoIndividual) && !datoIndividual.equals("null")){
										numeroInciso = new Integer(datoIndividual);
									}
									else{
										numeroInciso = 0;
									}
									break;
								case 5://5",numeroSubInciso:"+numeroSubInciso
									if(!UtileriasWeb.esCadenaVacia(datoIndividual) && !datoIndividual.equals("null")){
										numeroSubInciso = new Integer(datoIndividual);
									}
									else{
										numeroSubInciso = 0;
									}
									break;
								case 6://6"conceptoMovimiento:"+conceptoMovimiento
									conceptoMovimiento = new Integer(datoIndividual);
									break;
								case 7://7",idToMoneda:"+idToMoneda
									idToMoneda = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 8://8"fechaMovimiento:"+fechaMovimiento
									fechaMovimiento = formatoFecha.parse(datoIndividual);
									break;
								case 9://9",idMovimientoSiniestro:"+idMovimientoSiniestro
									idMovimientoSiniestro = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 10://10"montoMovimiento:"+montoMovimiento
									montoMovimiento = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 11://11",tipoMovimiento:"+tipoMovimiento
									tipoMovimiento = new Integer(datoIndividual);
									break;
								case 12://12"idToReporteSiniestro:"+idToReporteSiniestro
									idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
								case 13://13 "idDistMovSin:"+idToDistribucionMovSiniestroStr
									idToDistribucionMovSiniestro = UtileriasWeb.regresaBigDecimal(datoIndividual);
									break;
							}
						}
						if(idToPoliza != null && idToSeccion != null && idToCobertura != null && idToMoneda != null && 
								idMovimientoSiniestro != null && montoMovimiento != null && idToReporteSiniestro != null && 
								numeroEndoso != null && numeroInciso != null && numeroSubInciso != null && conceptoMovimiento != null && 
								tipoMovimiento != null && fechaMovimiento != null && idToDistribucionMovSiniestro != null){
							//intentar distribuir el siniestro nuevamente.
							try{
								new TemporizadorDistribuirSiniestroSN().iniciar(
										idToPoliza, numeroEndoso, idToSeccion, idToCobertura, 
										numeroInciso, numeroSubInciso, conceptoMovimiento, 
										idToMoneda, fechaMovimiento, idMovimientoSiniestro, 
										montoMovimiento, tipoMovimiento, idToReporteSiniestro, 
										idToDistribucionMovSiniestro);
								
								//actualizar el registro de log, para no procesarlo nuevamente
								logErrorSoporte.setRetornoMetodoorigenError("Se program� exitosamente el timer para distribuci�n del movimiento de siniestro." +
										" idToDistribucionMovSiniestro: "+idToDistribucionMovSiniestro+". "+DateFormat.getDateTimeInstance().format(new Date()));
								InterModuloErrorLogDN.getInstancia().modificar(logErrorSoporte);
							}catch(Exception e){
								LogDeMidasWeb.log("Ocurri� un error al intentar programar el timer de distribuci�n del idToMovimientoSiniestro: "+idToDistribucionMovSiniestro, Level.SEVERE, e);
							}
						}
						else{
							String error = "Se encontr� un log de error de distribuci�n de siniestro inv�lido, los par�metros son incorrectos:"+
								logErrorSoporte.getParamMetodoOrigenError();
							LogDeMidasWeb.log(error, Level.SEVERE, null);
							logErrorSoporte.setComentariosAdicionales(error);
							InterModuloErrorLogDN.getInstancia().modificar(logErrorSoporte);
						}
					}
					catch(Exception e){
						LogDeMidasWeb.log("Ocurri� un error al intentar registrar y distribuir un movimiento de reaseguro: "+logErrorSoporte.getParamMetodoOrigenError(), Level.SEVERE, e);
					}
				}
			}
		}
		else{
			LogDeMidasWeb.log("No se encontraron registros ToInterModErrorLog que indiquen movimientos de siniestro pendientes por distribuir.", Level.SEVERE, null);
		}
	}
}
