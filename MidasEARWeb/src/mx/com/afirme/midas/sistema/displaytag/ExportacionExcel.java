package mx.com.afirme.midas.sistema.displaytag;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.displaytag.export.ExcelView;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.TableModel;

public class ExportacionExcel extends ExcelView{
	
	/**
     * @see org.displaytag.export.BaseExportView#setParameters(TableModel, boolean, boolean, boolean)
     */
    @SuppressWarnings("unchecked")
	public void setParameters(TableModel tableModel, boolean exportFullList, boolean includeHeader,boolean decorateValues){
        List<HeaderCell> listaTitulo = tableModel.getHeaderCellList();
        //Remover el t�tulo de "Acciones"
        HeaderCell headerCell = listaTitulo.get(listaTitulo.size()-1);
        if(headerCell != null && headerCell.getTitle() != null && headerCell.getTitle().toLowerCase().indexOf("accion") != -1)
        	listaTitulo.get(listaTitulo.size()-1).setTitle("");
        super.setParameters(tableModel, exportFullList, includeHeader, decorateValues);
    }
    
    /**
     * Escaping for excel format.
     * <ul>
     * <li>Quotes inside quoted strings are escaped with a double quote</li>
     * <li>Fields are surrounded by " (should be optional, but sometimes you get a "Sylk error" without those)</li>
     * </ul>
     * @see org.displaytag.export.BaseExportView#escapeColumnValue(java.lang.Object)
     */
    protected String escapeColumnValue(Object value){
    	String valor = null;
        if (value != null)
        {
            // quotes around fields are needed to avoid occasional "Sylk format invalid" messages from excel
            valor =  "\"" //$NON-NLS-1$
                + StringUtils.replace(StringUtils.trim(value.toString()), "\"", "\"\"") //$NON-NLS-1$ //$NON-NLS-2$ 
                + "\""; //$NON-NLS-1$
            if(valor.contains("<") && valor.contains("href"))
            	valor = "";
            else{
            	valor = valor.replaceAll("&aacute;", "a");
            	valor = valor.replaceAll("&eacute;", "e");
            	valor = valor.replaceAll("&iacute;", "i");
            	valor = valor.replaceAll("&oacute;", "o");
            	valor = valor.replaceAll("&uacute;", "u");
            	valor = valor.replaceAll("&Aacute;", "A");
            	valor = valor.replaceAll("&Eacute;", "E");
            	valor = valor.replaceAll("&Iacute;", "I");
            	valor = valor.replaceAll("&Oacute;", "O");
            	valor = valor.replaceAll("&Uacute;", "U");
            }
        }
        
        return valor;
    }
}