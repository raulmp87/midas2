package mx.com.afirme.midas.sistema.displaytag;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.displaytag.export.CsvView;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.TableModel;

public class ExportacionCSV extends CsvView{
	/**
     * @see org.displaytag.export.BaseExportView#setParameters(TableModel, boolean, boolean, boolean)
     */
    @SuppressWarnings("unchecked")
	public void setParameters(TableModel tableModel, boolean exportFullList, boolean includeHeader,boolean decorateValues){
        List<HeaderCell> listaTitulo = tableModel.getHeaderCellList();
        //Remover el t�tulo de "Acciones"
        HeaderCell headerCell = listaTitulo.get(listaTitulo.size()-1);
        if(headerCell != null && headerCell.getTitle() != null && headerCell.getTitle().toLowerCase().indexOf("accion") != -1)
        	listaTitulo.get(listaTitulo.size()-1).setTitle("");
        super.setParameters(tableModel, exportFullList, includeHeader, decorateValues);
    }
    
    /**
     * Escaping for csv format.
     * <ul>
     * <li>Quotes inside quoted strings are escaped with a /</li>
     * <li>Fields containings newlines or , are surrounded by "</li>
     * </ul>
     * Note this is the standard CVS format and it's not handled well by excel.
     * @see org.displaytag.export.BaseExportView#escapeColumnValue(java.lang.Object)
     */
    protected String escapeColumnValue(Object value){
    	String valor = null;
        if (value != null){
        	valor = StringUtils.trim(value.toString());
        	if (!StringUtils.containsNone(valor, new char[]{'\n', ','})){
                valor =  "\"" + //$NON-NLS-1$
                    StringUtils.replace(value.toString(), "\"", "\\\"") + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }
            if(valor != null && valor.contains("<") && valor.contains("href"))
            	valor = "";
        }
        
        return valor;
    }
    
}
