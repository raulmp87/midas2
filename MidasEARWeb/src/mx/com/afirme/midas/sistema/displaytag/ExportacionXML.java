package mx.com.afirme.midas.sistema.displaytag;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.displaytag.export.XmlView;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.TableModel;

public class ExportacionXML extends XmlView {

	/**
     * @see org.displaytag.export.BaseExportView#setParameters(TableModel, boolean, boolean, boolean)
     */
    @SuppressWarnings("unchecked")
	public void setParameters(TableModel tableModel, boolean exportFullList, boolean includeHeader,boolean decorateValues){
        List<HeaderCell> listaTitulo = tableModel.getHeaderCellList();
        //Remover el t�tulo de "Acciones"
        HeaderCell headerCell = listaTitulo.get(listaTitulo.size()-1);
        if(headerCell != null && headerCell.getTitle() != null && headerCell.getTitle().toLowerCase().indexOf("accion") != -1)
        	listaTitulo.get(listaTitulo.size()-1).setTitle("");
        super.setParameters(tableModel, exportFullList, includeHeader, decorateValues);
    }
    
    /**
     * Escaping for excel format.
     * <ul>
     * <li>Quotes inside quoted strings are escaped with a double quote</li>
     * <li>Fields are surrounded by " (should be optional, but sometimes you get a "Sylk error" without those)</li>
     * </ul>
     * @see org.displaytag.export.BaseExportView#escapeColumnValue(java.lang.Object)
     */
    protected String escapeColumnValue(Object value){
    	String valor = null;
        if (value != null)
        {
            // quotes around fields are needed to avoid occasional "Sylk format invalid" messages from excel
            valor =  StringEscapeUtils.escapeXml(value.toString());
            if(valor.contains("&lt;") && valor.contains("href"))
            	valor = "";
        }
        return valor;
    }
	
}
