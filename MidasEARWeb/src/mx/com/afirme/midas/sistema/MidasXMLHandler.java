package mx.com.afirme.midas.sistema;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * @author Jos� Luis Arellano
 */
public class MidasXMLHandler extends DefaultHandler{
	private List<Object> listaObjetos;
	private String nombreNodoObjeto;
	private String nombreClase;
	private List<String> nombreNodosAtributos;
	private Object registro;
//	private String valor;
//	private String valorTMP;
//	private boolean concatenarTMP = false;
	private StringBuffer valorBuffer = new StringBuffer();
	private Method []metodos;
	
	public MidasXMLHandler(String nombreNodoObjeto,List<Object> listaObjetos,List<String> nombreNodosAtributos,String nombreClase){
		this.listaObjetos = listaObjetos;
		this.nombreNodoObjeto = nombreNodoObjeto;
		this.nombreNodosAtributos = nombreNodosAtributos;
		this.nombreClase = nombreClase;
	}
	
	public MidasXMLHandler(String nombreNodoObjeto,List<Object> listaObjetos,String[] nombreNodosAtributos,String nombreClase){
		this.listaObjetos = listaObjetos;
		this.nombreNodoObjeto = nombreNodoObjeto;
		this.nombreNodosAtributos = new ArrayList<String>();
		this.nombreClase = nombreClase;
		for(int i=0;i<nombreNodosAtributos.length;i++){
			this.nombreNodosAtributos.add(nombreNodosAtributos[i]);
		}
	}
	
	@Override
	public void startDocument(){
		try {
			metodos = Class.forName(nombreClase).getMethods();
		} catch (SecurityException e) {
			LogDeMidasWeb.log("Ocurri� un error al cargar la clase:"+nombreClase+" en MidasXMLHandler", Level.SEVERE, e);
		} catch (ClassNotFoundException e) {
			LogDeMidasWeb.log("Ocurri� un error al cargar la clase:"+nombreClase+" en MidasXMLHandler", Level.SEVERE, e);
		}
	}
	
	/**
	 * @param localName: contiene el nombre de la etiqueta.
	 * @param att: de la clase "org.xml.sax.Attributes", es una tabla que contiene los atributos contenidos en la etiqueta.
	 * @param namespaceURI. 
	*/
	@SuppressWarnings("unchecked")
	@Override
	public void startElement( String namespaceURI, String localName, String qName, Attributes attr ) throws SAXException{
	    if (localName.equals(nombreNodoObjeto)){
	    	Class clase;
			try {
				clase = Class.forName(nombreClase);
				registro = clase.newInstance();
			} catch (ClassNotFoundException e) {
				LogDeMidasWeb.log("Ocurri� un error al instanciar un objeto de la clase: "+nombreClase+" en MidasXMLHandler.startElement", Level.SEVERE, e);
				registro = null;
			} catch (InstantiationException e) {
				LogDeMidasWeb.log("Ocurri� un error al instanciar un objeto de la clase: "+nombreClase+" en MidasXMLHandler.startElement", Level.SEVERE, e);
				registro = null;
			} catch (IllegalAccessException e) {
				LogDeMidasWeb.log("Ocurri� un error al instanciar un objeto de la clase: "+nombreClase+" en MidasXMLHandler.startElement", Level.SEVERE, e);
				registro = null;
			}
			if(registro != null)
				listaObjetos.add(registro);
	    }
	}

	/**
	 * 
	 */
	@Override
	public void endElement (String namespaceURI, String localName, String rawName) throws SAXException{
		if(registro != null){
		    for(String nombreNodo : nombreNodosAtributos){
		    	if(localName.equals(nombreNodo)){
		    		try {
						try {
							setPropertyToBean(nombreNodo, new String(valorBuffer.toString().trim().getBytes("ISO-8859-1")));
						} catch (UnsupportedEncodingException e) {
							setPropertyToBean(nombreNodo, valorBuffer.toString().trim());
						}
					} catch (IllegalAccessException e) {
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					} catch (InvocationTargetException e) {
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					} catch(IllegalArgumentException e){
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					} catch (SecurityException e) {
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					} catch (ClassNotFoundException e) {
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					} catch (InstantiationException e) {
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					} catch (ParseException e) {
						LogDeMidasWeb.log("Ocurri� un error al settear la propiedad: "+nombreNodo+", con valor: "+valorBuffer.toString()+" al objeto: "+registro, Level.SEVERE, e);
					}
					break;
		    	}
		    }
		    valorBuffer = new StringBuffer();
		}
	}
	
//	@Override
//	public  void ignorableWhitespace(char[] ch,int start,int length) throws SAXException{
//		LogDeMidasWeb.log("Espacios ignorados: "+new String (ch, start, length), Level.INFO, null);
//	}
	
	@Override
    public void characters (char[] ch, int start, int end) throws SAXException{
		valorBuffer.append(ch,start,end);
    }
	
	private void setPropertyToBean(String nombrePropiedad,String valor) throws SecurityException, ClassNotFoundException, InstantiationException, IllegalAccessException, ParseException, IllegalArgumentException, InvocationTargetException{
		String nombreMetodoSetter = "set"+nombrePropiedad.substring(0, 1).toUpperCase()+nombrePropiedad.substring(1);
		Method metodoSetter = null;
		for(Method metodo : metodos){
			if(metodo.getName().equals(nombreMetodoSetter)){
				if (metodo.getParameterTypes().length == 1){
					metodoSetter = metodo;
					break;
				}
			}
		}
		if(metodoSetter != null){
			Class<?>[] clasesParametros = metodoSetter.getParameterTypes();
			Object objetoPorSettear = null;
			if (valor == null || valor.equals(""))
				return;
			if(clasesParametros[0].getName().equals("java.lang.String")){
				objetoPorSettear = valor;
			}
			else if(clasesParametros[0].getName().equals("java.math.BigDecimal")){
				try{
					objetoPorSettear = new BigDecimal(valor);
				}catch (NumberFormatException e){
					objetoPorSettear = new BigDecimal("0");
					LogDeMidasWeb.log("Ocurri� un error al settear la propiedad de tipo " + objetoPorSettear.getClass().getName() + " en la propiedad: " + metodoSetter.getName() + " en la clase: " + metodoSetter.getClass().getName() +", con valor: "+valor, Level.SEVERE, e);
				}
			}
			else if(clasesParametros[0].getName().equals("java.lang.Integer")){
				try{
					objetoPorSettear = Integer.valueOf(valor);
				}catch (NumberFormatException e){
					objetoPorSettear = new Integer("0");
					LogDeMidasWeb.log("Ocurri� un error al settear la propiedad de tipo " + objetoPorSettear.getClass().getName() + " en la propiedad: " + metodoSetter.getName() + " en la clase: " + metodoSetter.getClass().getName() +", con valor: "+valor, Level.SEVERE, e);
				}
			}
			else if(clasesParametros[0].getName().equals("java.lang.Double")){
				try{
					objetoPorSettear = Double.valueOf(valor);
				}catch (NumberFormatException e){
					objetoPorSettear = new Double("0");
					LogDeMidasWeb.log("Ocurri� un error al settear la propiedad de tipo " + objetoPorSettear.getClass().getName() + " en la propiedad: " + metodoSetter.getName() + " en la clase: " + metodoSetter.getClass().getName() +", con valor: "+valor, Level.SEVERE, e);
				}
			}
			else if(clasesParametros[0].getName().equals("java.lang.Short")){
				try{
					objetoPorSettear = Short.valueOf(valor);
				}catch (NumberFormatException e){
					objetoPorSettear = new Short("0");
					LogDeMidasWeb.log("Ocurri� un error al settear la propiedad de tipo " + objetoPorSettear.getClass().getName() + " en la propiedad: " + metodoSetter.getName() + " en la clase: " + metodoSetter.getClass().getName() +", con valor: "+valor, Level.SEVERE, e);
				}
			}
			else if(clasesParametros[0].getName().equals("java.lang.Long")){
				try{
					objetoPorSettear = Long.valueOf(valor);
				}catch (NumberFormatException e){
					objetoPorSettear = new Long("0");
					LogDeMidasWeb.log("Ocurri� un error al settear la propiedad de tipo " + objetoPorSettear.getClass().getName() + " en la propiedad: " + metodoSetter.getName() + " en la clase: " + metodoSetter.getClass().getName() +", con valor: "+valor, Level.SEVERE, e);
				}
			}
			else if( clasesParametros[0].getName().equals("java.util.Date")){
				if(valor.matches("[0-9]{2}[0-9]{2}[0-9]{4}")){
					SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
					objetoPorSettear = sdf.parse(valor);
				}
				else
					LogDeMidasWeb.log("La fecha siguiente no se formate� correctamente: "+valor, Level.INFO, null);
			}
			metodoSetter.invoke(registro, objetoPorSettear);
			metodoSetter = null;
		}
		else{
			LogDeMidasWeb.log("MidasWMLHandler no encontr� el m�todo: "+nombreMetodoSetter+" en el objeto: "+registro, Level.WARNING, null);
		}
		nombreMetodoSetter = null;
	}
}
