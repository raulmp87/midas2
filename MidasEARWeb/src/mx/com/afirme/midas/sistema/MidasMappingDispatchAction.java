/**
 * 
 */
package mx.com.afirme.midas.sistema;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.actions.MappingDispatchAction;

/**
 * @author Jesus Enrique Aldana Sanchez
 * 
 */
public abstract class MidasMappingDispatchAction extends MappingDispatchAction {

	/**
	 * Retrieves the session's <code>MidasUserDTO</code> instance.
	 * <code>null</code> if none.
	 * 
	 * @param request
	 *            The <code>HttpServletRequest</code> instance.
	 * @return The session's <code>MidasUserDTO</code> instance.
	 */

	private HttpServletRequest request = null;

	public Usuario getUsuarioMidas(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (Usuario) session.getAttribute(Sistema.USUARIO_MIDAS);
	}

	public void mensajeExitoAgregar(MidasBaseForm form) {
		mensajeExito(form, Sistema.ACCION_GUARDAR, null);
	}

	public void mensajeExitoAgregar(MidasBaseForm form, String mensaje) {
		mensajeExito(form, Sistema.ACCION_GUARDAR, mensaje);
	}

	public void mensajeExitoAgregar(MidasBaseForm form,
			HttpServletRequest request) {
		this.request = request;
		mensajeExito(form, Sistema.ACCION_GUARDAR, null);
	}

	public void mensajeExitoAgregar(MidasBaseForm form, String mensaje,
			HttpServletRequest request) {
		this.request = request;
		mensajeExito(form, Sistema.ACCION_GUARDAR, mensaje);
	}

	public void mensajeExitoBorrar(MidasBaseForm form) {
		mensajeExito(form, Sistema.ACCION_BORRAR, null);
	}

	public void mensajeExitoBorrar(MidasBaseForm form, String mensaje) {
		mensajeExito(form, Sistema.ACCION_BORRAR, mensaje);
	}

	public void mensajeExitoBorrar(MidasBaseForm form,
			HttpServletRequest request) {
		this.request = request;
		mensajeExito(form, Sistema.ACCION_BORRAR, null);
	}

	public void mensajeExitoBorrar(MidasBaseForm form, String mensaje,
			HttpServletRequest request) {
		this.request = request;
		mensajeExito(form, Sistema.ACCION_BORRAR, mensaje);
	}

	public void mensajeExitoModificar(MidasBaseForm form) {
		mensajeExito(form, Sistema.ACCION_MODIFICAR, null);
	}

	public void mensajeExitoModificar(MidasBaseForm form, String mensaje) {
		mensajeExito(form, Sistema.ACCION_MODIFICAR, mensaje);
	}

	public void mensajeExitoModificar(MidasBaseForm form,
			HttpServletRequest request) {
		this.request = request;
		mensajeExito(form, Sistema.ACCION_MODIFICAR, null);
	}

	public void mensajeExitoModificar(MidasBaseForm form, String mensaje,
			HttpServletRequest request) {
		this.request = request;
		mensajeExito(form, Sistema.ACCION_MODIFICAR, mensaje);
	}

	private void mensajeExito(MidasBaseForm form, String tipoAccion,
			String mensaje) {

		form.setTipoMensaje(Sistema.EXITO);
		if (mensaje != null) {
			form.setMensaje(mensaje);
		} else if (tipoAccion.equals(Sistema.ACCION_GUARDAR)) {
			form.setMensaje(UtileriasWeb.getMensajeRecurso(
					Sistema.ARCHIVO_RECURSOS,
					"mensaje.informacion.guardar.exito"));
		} else if (tipoAccion.equals(Sistema.ACCION_BORRAR)) {
			form.setMensaje(UtileriasWeb.getMensajeRecurso(
					Sistema.ARCHIVO_RECURSOS,
					"mensaje.informacion.borrar.exito"));
		} else if (tipoAccion.equals(Sistema.ACCION_MODIFICAR)) {
			form.setMensaje(UtileriasWeb.getMensajeRecurso(
					Sistema.ARCHIVO_RECURSOS,
					"mensaje.informacion.modificar.exito"));
		}

		if (this.request != null) {
			request.getSession().setAttribute(Sistema.TIPO_MENSAJE,
					form.getTipoMensaje());
			request.getSession().setAttribute(Sistema.MENSAJE,
					form.getMensaje());
		}

	}

	public void mensajeExcepcion(MidasBaseForm form, Throwable ex) {
		nuevoMensajeExcepcion(form, ex);
	}

	public void mensajeExcepcion(MidasBaseForm form,
			HttpServletRequest request, Throwable ex) {
		this.request = request;
		nuevoMensajeExcepcion(form, ex);
	}

	public void nuevoMensajeExcepcion(MidasBaseForm form, Throwable ex) {

		if (ex instanceof SystemException) {
			form.setTipoMensaje(Sistema.INFORMACION);
			form.setMensaje(UtileriasWeb.getMensajeRecurso(
					Sistema.ARCHIVO_RECURSOS,
					"mensaje.excepcion.sistema.nodisponible"));
		} else if (ex instanceof ExcepcionDeAccesoADatos) {
			form.setTipoMensaje(Sistema.ERROR);
			if (((ExcepcionDeAccesoADatos) ex).getDescripcion() != null) {
				form
						.setMensaje(((ExcepcionDeAccesoADatos) ex)
								.getDescripcion());
			} else {
				form
						.setMensaje(UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"mensaje.excepcion.rollback"));
			}
		} else if (ex instanceof ExcepcionDeLogicaNegocio) {
			form.setTipoMensaje(Sistema.ERROR);
			if (((ExcepcionDeLogicaNegocio) ex).getDescripcion() != null) {
				form.setMensaje(((ExcepcionDeLogicaNegocio) ex)
						.getDescripcion());
			} else {
				form
						.setMensaje(UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"mensaje.excepcion.rollback"));
			}
		} else if (ex instanceof Exception) {
			form.setTipoMensaje(Sistema.ERROR);
			form.setMensaje(ex.getCause().toString());
		}

		if (this.request != null) {
			request.getSession().setAttribute(Sistema.TIPO_MENSAJE,
					form.getTipoMensaje());
			request.getSession().setAttribute(Sistema.MENSAJE,
					form.getMensaje());
		}

	}

	public void agregarMensajes(Map<String, String> mensajes,
			String tipoMensaje, MidasBaseForm form, String mensajeInicial) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(mensajeInicial);
		buffer.append("<br/>");
		for (String key : mensajes.keySet()) {
			buffer.append(mensajes.get(key));
			buffer.append("<br/>");
		}
		form.setMensaje(buffer.toString());
		form.setTipoMensaje(tipoMensaje);
	}
	
	public void agregarMensajes(Set<String> mensajes,
			String tipoMensaje, MidasBaseForm form, String mensajeInicial) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(mensajeInicial);
		buffer.append("<br/>");
		for (String mensaje : mensajes) {
			buffer.append("<li>"+mensaje);
			buffer.append("<br/>");
		}
		form.setMensaje(buffer.toString());
		form.setTipoMensaje(tipoMensaje);
	}	
	public void agregarMensaje(String mensaje, String tipoMensaje, 
			MidasBaseForm form) {
		form.setMensaje(mensaje);
		form.setTipoMensaje(tipoMensaje);
	}

	protected void writeBytes(HttpServletResponse response, byte[] byteArray,
			String contentType, String filename,String sufijo, String reponseType)
			throws IOException {
		if(sufijo != null){
			filename += sufijo;
		}
		else{
			if (contentType.equals(Sistema.TIPO_PDF)) {
				filename += ".pdf";
			} else if (contentType.equals(Sistema.TIPO_XLS)) {
				filename += ".xls";
			} else if (contentType.equals(Sistema.TIPO_DOC)) {
				filename += ".doc";
			}
		}
		
		// End of if/else
		OutputStream outputStream = response.getOutputStream();
		response.setContentType(contentType);
		response.setContentLength(byteArray.length);
		response.addHeader("Content-Disposition", reponseType + ";filename="
				+ filename);
		response.setBufferSize(1024 * 15);
		outputStream.write(byteArray);
		outputStream.flush();
		IOUtils.closeQuietly(outputStream);
	}

	protected void writeBytes(HttpServletResponse response, byte[] byteArray,
			String contentType, String filename,String sufijo) throws IOException {
		writeBytes(response, byteArray, contentType, filename,sufijo, "attachment");
	}
	
	protected void writeBytes(HttpServletResponse response, byte[] byteArray,
			String contentType, String filename) throws IOException {
		writeBytes(response, byteArray, contentType, filename,null, "attachment");
	}
	
	protected void writeBytes(HttpServletResponse response,
			InputStream genericInputStream, String contentType, String fileName, String sufix) throws IOException {
		writeBytes(response, IOUtils.toByteArray(genericInputStream), contentType, fileName, sufix, "attachment");
	}
	
	protected boolean listaEnCache(ActionForm form, HttpServletRequest request) {
		
		try {
			//d-49653-p=2
			int numeroPaginaSolicitado = 1;
			
			MidasBaseForm baseForm = (MidasBaseForm) form;
			
			String qs = request.getQueryString();
			
			//Valida que tenga el parametro de la pagina
			if (qs != null && qs.indexOf("d-") != -1 && qs.indexOf("-p=") != -1) {
				String nombreParametroPagina = qs.substring(qs.indexOf("d-"), qs.indexOf("-p=") + 2);
				numeroPaginaSolicitado = Integer.parseInt(request.getParameter(nombreParametroPagina));
			}
			
			int numeroPaginaActual = Integer.parseInt(baseForm.getNumeroPaginaActual());
			int registrosPagina = Integer.parseInt(Sistema.REGISTROS_PAGINADOS);
			int paginasCache = Sistema.PAGINAS_CACHE; //Maximo numero de paginas que regresara el query
			
			int paginaInferiorCache = Integer.parseInt(baseForm.getPaginaInferiorCache());
			int paginaSuperiorCache = Integer.parseInt(baseForm.getPaginaSuperiorCache());
			int totalRegistros = Integer.parseInt(baseForm.getTotalRegistros());
					
			int totalPaginas = totalRegistros / registrosPagina;
			
			baseForm.setNumeroPaginaActual(numeroPaginaSolicitado + "");
			
			if ((numeroPaginaSolicitado < paginaInferiorCache || numeroPaginaSolicitado > paginaSuperiorCache) 
					|| request.getSession().getAttribute(Sistema.LISTA_PAGINADA) == null) {
				
				if (totalRegistros % registrosPagina > 0) {
					totalPaginas = totalPaginas + 1;
				}
				
				if (totalPaginas > paginasCache) {
					
					int primeraMitad = paginasCache / 2;
					
					if (paginasCache % 2 == 0) {
						if(numeroPaginaSolicitado > numeroPaginaActual) {
							primeraMitad = primeraMitad - 1;
						}
					}
					
					paginaInferiorCache = numeroPaginaSolicitado - primeraMitad;
					
					if (paginaInferiorCache < 1) {
						paginaInferiorCache = 1;
					} 
					
					paginaSuperiorCache = paginaInferiorCache + (paginasCache - 1);
					
					if (paginaSuperiorCache > totalPaginas) {
						paginaSuperiorCache = totalPaginas;
						paginaInferiorCache = totalPaginas - (paginasCache - 1);
					} 
				} else {
					paginaInferiorCache = 1;
					paginaSuperiorCache = totalPaginas;
					paginasCache = totalPaginas;
					
				}
				
				baseForm.setPaginaInferiorCache(paginaInferiorCache + "");
				baseForm.setPaginaSuperiorCache(paginaSuperiorCache + "");
								
				Integer primerRegistroACargar =(paginaInferiorCache - 1) * registrosPagina;
				Integer numeroMaximoRegistrosACargar =  paginasCache * registrosPagina;
				
				baseForm.setPrimerRegistroACargar(primerRegistroACargar);
				baseForm.setNumeroMaximoRegistrosACargar(numeroMaximoRegistrosACargar);
				
				return false;
				
			} 
			
			return true;

		} catch (Exception e) {
			LogDeMidasWeb.log("Error durante el paginado...", Level.WARNING, e);
			return false;
		} 
	}
	
	
	@SuppressWarnings("unchecked")
	protected List obtieneListaAMostrar(ActionForm form, HttpServletRequest request) {
		try {
			
			MidasBaseForm baseForm = (MidasBaseForm) form;
			//Realmente es numeroPaginaSolicitado, se acaba de asignar ese valor
			int numeroPaginaSolicitado = Integer.parseInt(baseForm.getNumeroPaginaActual()); 
			int paginaInferiorCache = Integer.parseInt(baseForm.getPaginaInferiorCache());
			int registrosPagina = Integer.parseInt(Sistema.REGISTROS_PAGINADOS);
			
			List listaPaginada = baseForm.getListaPaginada(request);
			
			List listaAMostrar = new ArrayList();
			Integer primerRegistroAMostrar =(numeroPaginaSolicitado - paginaInferiorCache) * registrosPagina;
			Integer ultimoRegistroAMostrar = primerRegistroAMostrar + registrosPagina;
			
			if (ultimoRegistroAMostrar >= listaPaginada.size()) {
				ultimoRegistroAMostrar = listaPaginada.size();
			}
			
			listaAMostrar = listaPaginada.subList(primerRegistroAMostrar, ultimoRegistroAMostrar);
			
			return listaAMostrar;
		
		
		} catch (Exception e) {
			LogDeMidasWeb.log("Error en obtieneListaAMostrar", Level.WARNING, e);
			return null;
		}
	}
	
	
	
}
