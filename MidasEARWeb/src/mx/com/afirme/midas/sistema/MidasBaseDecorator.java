package mx.com.afirme.midas.sistema;

import org.displaytag.decorator.TableDecorator;
/**
 * 
 * @author Jose Luis Arellano
 * @since 29 de octubre de 2009
 */
public class MidasBaseDecorator extends TableDecorator{
		
	protected String obtenerHTMLImagenBlanco(){
		return "<a href=\"javascript: void(0);\" <img border='0px' src='/MidasWeb/img/blank.gif'/></a>&nbsp;";
	}
	
}
