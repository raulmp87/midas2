/**
 * Clase que llena Paginas y Permisos para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.opreasegurofacultativo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoOpReaseguroFacultativo {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	private final int AG = 0;
	private final int AC = 1;
	private final int BR = 2;
	private final int RE = 3;
	private final int EX = 4;
	private final int AD = 5;
	private final int VD = 6;
	private final int AS = 7;
	private final int BU = 8;
	private final int CO = 9;
	private final int CT = 10;
	private final int GU = 11;
	private final int NV = 12;
	private final int SE = 14;
	
	public PaginaPermisoOpReaseguroFacultativo(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/estado.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/ciudad.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoDolares.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoPesos.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contacto.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
/* Cat�logos Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listar.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);


		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/contacto/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/contacto/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/contacto/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		/* Fin Cat�logos Reaseguro */
		
		/* Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPrincipalEstadosCuenta.jsp","/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/llenaComboSuscripcion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaContratosFacultativos.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		//Permiso para ver la JSP que contiene el tab bar para filtrado de estados de cuenta facultativos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaContratosFacultativos.jsp","/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuentaFacultativo.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para ver la JSP de filtrado de estado de cuenta facultativo por poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaFacultativosPoliza.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaFacultativoPorPoliza.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para obtener cadena json de detalle de estados de cuenta facultativos por poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarEstadosCuentaPorPoliza.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para consultar estado de cuenta facultativo combinado por p�liza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorPoliza/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para consultar listado de siniestros para consulta de su estado de cuenta
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarEstadoCuentaPorSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para consultar listado de subramos afectdos por un siniestro para consulta de su estado de cuenta
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarSubRamosAfectadosPorSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para consultar estado de cuenta de un siniestro
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorSiniestro/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para consultar estado de cuenta por reasegurador
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaFacultativoPorReasegurador/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);
		
		//Permiso para imprimir el reporte de soporte de estado de cuenta facultativo (Cuenta por pagar)
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoCuentaFacultativo/obtenerReporteMovimientosEstadoCuentaCxP.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaContratoFacultativo.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePagos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/rptEstadoCuentaFacultativoMasivo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWebreaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/guardarObservaciones.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/buscarPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);

		/* Fin Reaseguro */
		
		/* Contratos Facultativos */
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/listarDocumentos.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/borrarDocumento.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipGeneral.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipGeneral.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipBarco.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipBarco.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipEquipoContratista.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipEquipoContratista.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipTransportesDeCarga.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipTransportesDeCarga.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/validarTipoSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlipIncisoGrid.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/listarCotizacionDanos.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/registrarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/cotizafacultativa/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/cobertura/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/agregarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/eliminarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/cobertura/guardarDatosDeLaCobertura.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/modificarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/agregarDetalleCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacionCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacion.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativa/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativa/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativa/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/borrarParticipacionCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/autorizarCotizacion.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/autorizarContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/cancelarContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/validarDetalleParticipacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/configuracion/listar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/poblarTreeView.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/mostrarGridFiltroCotizacionesFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/guardarIdCotizacionEnSesion.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		/* Fin Contratos Facultativo */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/registrarMovimiento.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		/* Reportes */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuentaFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		/* Fin Reportes */
		
		/* Slips */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipGeneral.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipBarco.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipEquipoContratista.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipTransportesDeCarga.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/mostrarModificarPorcentajesContratos.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/modificarPorcentajesContratos.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/borrarDocumento.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/listarDocumentos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlipIncisoGrid.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
	    
		/* Fin Slips */
	    
	    //Permisos para mostrar grid de plan de pagos de coberturas
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/mostrarPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/regenerarPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/obtenerEstatusAutorizacionPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);
		
		return this.listaPaginaPermiso;
	}
	
}
