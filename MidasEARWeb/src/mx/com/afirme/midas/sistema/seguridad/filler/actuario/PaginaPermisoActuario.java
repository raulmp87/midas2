/**
 * Clase que llena Paginas y Permisos para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.actuario;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoActuario {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	private final int AG = 0;
	private final int AC = 1;
	private final int BR = 2;
	private final int RE = 3;
	private final int EX = 4;
	private final int AD = 5;
	private final int VD = 6;
	private final int AS = 7;
	private final int BU = 8;
	private final int CO = 9;
	private final int CT = 10;
	private final int GU = 11;
	private final int NV = 12;
	private final int SE = 14;
	
	public PaginaPermisoActuario(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subRamo.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/rptLineasContratos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/reporteMovimiento.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/rptMovtosPorReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		/* Fin Configuracion Linea Negociacion */
		
		/* Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/rptOrdenPagoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerReporteSoporteOrdenPago.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPrincipalEstadosCuenta.jsp","/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaTipoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaTipoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarEstadosCuentaTipoReaseguroGrid.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/llenaComboSuscripcion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaContratosFacultativos.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaContratoFacultativo.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePagos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWebreaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuentaFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/rptEstadoCuentaMasivo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("administrarIngresos.jsp","/MidasWeb/reaseguro/ingresos/administrarIngresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarIngreso.jsp","/MidasWeb/reaseguro/ingresos/mostrarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/listarEstadosCuentaRelacionarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarEstadosCuentaAdministrarIngresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarIngresosPendientes.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracionMovimientos.jsp","/MidasWeb/reaseguro/configuracionmovimientos/listar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/buscarPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);

		/* Fin Reaseguro */
		
		/* Reportes */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptCumulos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptCumulos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptReservasRiesgo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptReservasRiesgo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		/* Fin Reportes */
		
		return this.listaPaginaPermiso;
	}
	
}
