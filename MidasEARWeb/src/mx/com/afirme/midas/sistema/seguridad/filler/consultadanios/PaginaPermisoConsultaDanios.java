package mx.com.afirme.midas.sistema.seguridad.filler.consultadanios;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

public class PaginaPermisoConsultaDanios {
    private List<Permiso> listaPermiso = new ArrayList<Permiso>();
    private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();

    public PaginaPermisoConsultaDanios(List<Permiso> listaPermiso) {
	this.listaPermiso = listaPermiso;
    }

    private Pagina nuevaPagina(String nombrePaginaJSP, String nombreAccionDo) {
	return new Pagina(new Integer("1"), nombrePaginaJSP.trim(),
		nombreAccionDo.trim(), "Descripcion pagina");
    }

    public List<PaginaPermiso> obtienePaginaPermisos() {

	// Permisos 0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 =
	// AS, 8 = BU
	// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
	PaginaPermiso pp;
	// ************* COTIZACION SOLO LECTURA
	// *****************************************/
	// Tab de Recibos
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listarRecibosPoliza.jsp",
		"/MidasWeb/cotizacionsololectura/listarRecibos.do"));
	listaPaginaPermiso.add(pp);

	// Tab Comisiones
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("autorizarComisiones.jsp","/MidasWeb/cotizacionsololectura/comision/mostrar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("modificarComisiones.jsp","/MidasWeb/cotizacionsololectura/comision/mostrar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("mostrarSubInciso.jsp"," /MidasWeb/cotizacionsololectura/subinciso/mostrarDetalle.do"));
	listaPaginaPermiso.add(pp);
	// ****
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("void.jsp","/MidasWeb/cotizacionsololectura/void.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("cotizacionArea.jsp","/MidasWeb/cotizacionsololectura/cotizacion/mostrar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("verArbol.jsp","/MidasWeb/cotizacionsololectura/cargarArbolCotizacion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("datosGenerales.jsp","/MidasWeb/cotizacionsololectura/mostrarDatosGenerales.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("datosGenerales.jsp","/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("mostrarResumenCotizacion.jsp","/MidasWeb/cotizacionsololectura/resumen/mostrarResumenCotizacion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/resumen/guardarBonificacion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("docAnexos.jsp","/MidasWeb/cotizacionsololectura/docAnexoCot/mostrarDocAnexo.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/docAnexoCot/mostrarListarAnexos.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/docAnexoCot/modificarDocAnexo.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/docAnexoCot/guardarOrden.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("documentosDigitalesComplementarios.jsp","/MidasWeb/cotizacionsololectura/listarDocumentosDigitalesComplementarios.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("mostrarPersona.jsp","/MidasWeb/cotizacionsololectura/mostrarPersona.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("coberturas.jsp","/MidasWeb/cotizacionsololectura/cobertura/listarCoberturas.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("secciones.jsp","/MidasWeb/cotizacionsololectura/mostrarModificarSecciones.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("cotizacion.jsp","/MidasWeb/cotizacionsololectura/mostrarDetalle.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("modificarComisiones.jsp","/MidasWeb/cotizacionsololectura/mostrarComisiones.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("autorizarComisiones.jsp","/MidasWeb/cotizacionsololectura/mostrarComisiones.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/mostrarComisionesCotizacion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listarIncisos.jsp","/MidasWeb/cotizacionsololectura/listarIncisos.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listarIncisos.jsp","/MidasWeb/cotizacionsololectura/listarIncisos.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("error.jsp",
		"/MidasWeb/cotizacionsololectura/listarIncisos.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("desplegarInciso.jsp",	"/MidasWeb/cotizacionsololectura/mostrarDetalleInciso.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("secciones.jsp","/MidasWeb/cotizacionsololectura/mostrarModificarSecciones.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/cargarSeccionesPorInciso.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/mostrarDatosInciso.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/mostrarDatosSubInciso.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listarSubIncisos.jsp","/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("registroUsuario.jsp",	"/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("modificarSubInciso.jsp","/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("registroUsuario.jsp",	"/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("textosAdicionales.jsp","/MidasWeb/cotizacionsololectura/mostrarTexAdicional.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/mostrarListarTextosAdicionales.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/mostrarCoberturas.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("coberturas.jsp","/MidasWeb/cotizacionsololectura/listarCoberturas.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("autorizacionCoberturas.jsp",	"/MidasWeb/cotizacionsololectura/listarCoberturas.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("ARDs.jsp","/MidasWeb/cotizacionsololectura/mostrarARD.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("riesgos.jsp",	"/MidasWeb/cotizacionsololectura/mostrarRiesgosPorCobertura.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("autorizacionRiesgos.jsp","/MidasWeb/cotizacionsololectura/mostrarRiesgosPorCobertura.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacionsololectura/listarRiesgos.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("verArbol.jsp","/MidasWeb/cotizacion/poblarTreeView.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("verArbol.jsp", "/MidasWeb/cotizacion/cargarArbolCotizacion.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/documento/docAnexoCot/mostrarListarAnexos.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/comision/mostrarComisionesCotizacion.do"));
	listaPaginaPermiso.add(pp);
	
	//************
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/getValoresCoaseguroDeducible.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/riesgo/listarRiesgos.do"));
	listaPaginaPermiso.add(pp);


	// ************ LISTAR POLIZAS	// **********************************************/

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/poliza/listar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("error.jsp", "/MidasWeb/poliza/listar.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/poliza/listarPolizaFiltrado.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/poliza/listarPolizaFiltrado.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/poliza/listarFiltradoPaginado.do"));
	listaPaginaPermiso.add(pp);

	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/poliza/listarFiltradoPaginado.do"));
	listaPaginaPermiso.add(pp);
	
	
	
	//Imprimir Polizas
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("","/MidasWeb/danios/reportes/poliza/imprimirPoliza.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("errorImpresion.jsp","/MidasWeb/danios/reportes/poliza/imprimirPoliza.do"));
	listaPaginaPermiso.add(pp);
	
	pp = new PaginaPermiso();
	pp.setPagina(nuevaPagina("seleccionarEndosoPorImprimir.jsp","/MidasWeb/poliza/mostrarImprimirEndoso.do"));
	listaPaginaPermiso.add(pp);
	
	// Tab de endosos. JEAS
	
        pp = new PaginaPermiso();
    	pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/endoso/consultarDatosEndoso.do"));
    	listaPaginaPermiso.add(pp);
    	pp = new PaginaPermiso();
    	pp.setPagina(nuevaPagina("datosEndoso.jsp","/MidasWeb/cotizacion/endoso/consultarDatosEndoso.do"));
    	listaPaginaPermiso.add(pp);
    	pp = new PaginaPermiso();
    	pp.setPagina(nuevaPagina("datosEndosoModificacion.jsp","/MidasWeb/cotizacion/endoso/consultarDatosEndoso.do"));
    	listaPaginaPermiso.add(pp);
    	pp = new PaginaPermiso();
    	pp.setPagina(nuevaPagina("cotizacion.jsp","/MidasWeb/cotizacion/endoso/consultarDatosEndoso.do"));
    	listaPaginaPermiso.add(pp);
	
	
	
	//***** ACERCA DE **********//
	pp = new PaginaPermiso(); 
	pp.setPagina(nuevaPagina("acerca.jsp","/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));  
	listaPaginaPermiso.add(pp); 
	
	pp = new PaginaPermiso();
    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
    listaPaginaPermiso.add(pp);
    
    //Tab Datos de licitación
    pp = new PaginaPermiso();
    pp.setPagina(nuevaPagina("datosLicitacion.jsp","/MidasWeb/cotizacionsololectura/mostrarDatosLicitacion.do"));
    listaPaginaPermiso.add(pp);

	return this.listaPaginaPermiso;
  }
}