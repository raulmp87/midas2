package mx.com.afirme.midas.sistema.seguridad.filler;

import java.util.HashMap;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

/**
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.seguridad.UsuarioService}
 */
@Deprecated
public class UsuariosRol {
	
	protected HashMap<String, String> mapaRolesUsuario = new HashMap<String, String>();
	protected HashMap<String, Usuario> usuariosRegistrados = new HashMap<String, Usuario>();
	protected int i = 1;
	
	
	public void llenaUsuarios() {
		llenaUsuariosSistema();
		llenaUsuariosDanios();
		llenaUsuariosSiniestros();
		llenaUsuariosReaseguro();
		llenaUsuariosContabilidad();
		llenaUsuariosAuditoria();
	}
		
	private void llenaUsuariosSistema() {
		Usuario usuario = null;
		String nombreCompleto = null;
		String nombreUsuario = null;
		String roles = null;
		
		nombreUsuario = "KMGONVAZ";
		nombreCompleto = "KM Gonzalez Vazquez";
		roles = Sistema.ROL_AGENTE;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
				
		nombreUsuario = "SISTEMA";
		nombreCompleto = "Procesos Sistema Midas";
		roles = Sistema.ROL_AGENTE;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		nombreUsuario = "ADMIN";
		nombreCompleto = "Administrador MIDAS";
		roles = Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_CABINERO + "+" +
				Sistema.ROL_AJUSTADOR + "+" +
				Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_SINIESTROS + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_REPORTES_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
				Sistema.ROL_ACTUARIO + "+" +
				Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
				Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		//Usuarios para desarrollo
		
		//Da�os
		nombreUsuario = "DDAGENTE";
		nombreCompleto = "Usuario Agente";
		roles = Sistema.ROL_AGENTE;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDMESCTL";
		nombreCompleto = "Usuario Mesa Control";
		roles = Sistema.ROL_MESA_CONTROL;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDCOOEMI";
		nombreCompleto = "Usuario coordinador Emisor";
		roles = Sistema.ROL_COORDINADOR_EMI;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDDIRTEC";
		nombreCompleto = "Usuario Director Tecnico";
		roles = Sistema.ROL_DIRECTOR_TECNICO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDADMPRO";
		nombreCompleto = "Usuario Admin. Productos";
		roles = Sistema.ROL_ADMIN_PRODUCTOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDEMISOR";
		nombreCompleto = "Usuario Emisor";
		roles = Sistema.ROL_EMISOR;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDASIGOT";
		nombreCompleto = "Usuario Asignador OT";
		roles = Sistema.ROL_ASIGNADOR_OT;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDSUSCOT";
		nombreCompleto = "Usuario Suscriptor OT";
		roles = Sistema.ROL_SUSCRIPTOR_OT;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDSUSCCO";
		nombreCompleto = "Usuario Suscriptor COT";
		roles = Sistema.ROL_SUSCRIPTOR_COT;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDASISOL";
		nombreCompleto = "Usuario Asignador Solicitud";
		roles = Sistema.ROL_ASIGNADOR_SOL;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDSUPSUS";
		nombreCompleto = "Usuario Supervisor Suscriptor";
		roles = Sistema.ROL_SUPERVISOR_SUSCRIPTOR;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DDREPDAN";
		nombreCompleto = "Usuario Reportes Da�os";
		roles = Sistema.ROL_REPORTES_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//Siniestros
		nombreUsuario = "DSCABINE";
		nombreCompleto = "Usuario Cabinero";
		roles = Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSAJUSTA";
		nombreCompleto = "Usuario Ajustador";
		roles = Sistema.ROL_AJUSTADOR;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSDIROPE";
		nombreCompleto = "Usuario Director de Operaciones";
		roles = Sistema.ROL_DIRECTOR_DE_OPERACIONES;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSANAADM";
		nombreCompleto = "Usuario Analista Administrativo";
		roles = Sistema.ROL_ANALISTA_ADMINISTRATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSANAADF";
		nombreCompleto = "Usuario Analista Administrativo Facultativo";
		roles = Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSCOOSIN";
		nombreCompleto = "Usuario Coordinador Siniestros";
		roles = Sistema.ROL_COORDINADOR_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSGERSIN";
		nombreCompleto = "Usuario Gerente Siniestros";
		roles = Sistema.ROL_GERENTE_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSCOSIFA";
		nombreCompleto = "Usuario Coordinador Siniestro Facultativo";
		roles = Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSGERSIF";
		nombreCompleto = "Usuario Gerente Siniestros Facultativo";
		roles = Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DSREPSIN";
		nombreCompleto = "Usuario Reportes Siniestros";
		roles = Sistema.ROL_REPORTES_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//Reaseguro
		nombreUsuario = "DRGERREA";
		nombreCompleto = "Usuario Gerente Reaseguro";
		roles = Sistema.ROL_GERENTE_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DROPREAA";
		nombreCompleto = "Usuario Operador Reaseguro Aut.";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DROPASUR";
		nombreCompleto = "Usuario Oper. Asist. Subdirector Reas.";
		roles = Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DRSUBREA";
		nombreCompleto = "Usuario Subdirector Reaseguro";
		roles = Sistema.ROL_SUBDIRECTOR_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DRDIRREA";
		nombreCompleto = "Usuario Director Reaseguro";
		roles = Sistema.ROL_DIRECTOR_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DROPREAF";
		nombreCompleto = "Usuario Operador Reaseguro Facultativo";
		roles = Sistema.ROL_OP_REASEGURO_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DRACTUAR";
		nombreCompleto = "Usuario Actuario";
		roles = Sistema.ROL_ACTUARIO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DROPPCRE";
		nombreCompleto = "Usuario Oper. Pagos Cobros Reaseguradores";
		roles = Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES ;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DRCOOADR";
		nombreCompleto = "Usuario Coordinador Admin. Reaseguro";
		roles = Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DRREPREA";
		nombreCompleto = "Usuario Reportes Reaseguro";
		roles = Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "0asalinasr@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		//Funciones especiales
		nombreUsuario = "DEADMCOL";
		nombreCompleto = "Usuario Especial Admin. Colonias";
		roles = Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//Desarrollo Consulta Siniestros
		nombreUsuario = "DSCONSUL";
		nombreCompleto = "Usuario Desarrollo Consulta Siniestros";
		roles =Sistema.ROL_CONSULTA_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//Desarrollo Consulta Da�os
		nombreUsuario = "DDCONSUL";
		nombreCompleto = "Usuario Desarrollo Consulta Da�os";
		roles = Sistema.ROL_CONSULTA_DANIOS ;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "CONSULTA";
		nombreCompleto = "usuario Midas de Consulta";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO+ "+" +
		Sistema.ROL_CONSULTA_DANIOS + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);

		nombreUsuario = "MESA";
		nombreCompleto = "Mesa De Control Autos";
		roles = Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_CABINERO + "+" +
				Sistema.ROL_AJUSTADOR + "+" +
				Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_SINIESTROS + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_REPORTES_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
				Sistema.ROL_ACTUARIO + "+" +
				Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
				Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				"Rol_M2_Mesa_Control+" +
				Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);		
		
		nombreUsuario = "COORAUT";
		nombreCompleto = "Coordinador Suscripcion Autos";
		roles = Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_CABINERO + "+" +
				Sistema.ROL_AJUSTADOR + "+" +
				Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_SINIESTROS + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_REPORTES_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
				Sistema.ROL_ACTUARIO + "+" +
				Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
				Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				"Rol_M2_Coordinador_Suscripcion+" +
				Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);			

		nombreUsuario = "SUSAUT";
		nombreCompleto = "Suscriptor Autos";
		roles = Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_CABINERO + "+" +
				Sistema.ROL_AJUSTADOR + "+" +
				Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_SINIESTROS + "+" +
				Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
				Sistema.ROL_REPORTES_SINIESTROS + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
				Sistema.ROL_ACTUARIO + "+" +
				Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
				Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				Sistema.ROL_REPORTES_REASEGURO + "+" +
				"Rol_M2_Suscriptor+" +
				Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);				
	}
	
	private void llenaUsuariosDanios() {
		Usuario usuario = null;
		String nombreCompleto = null;
		String nombreUsuario = null;
		String roles = null;
		
		//Usuarios Da�os
		
		nombreUsuario = "LPRODRIG";
		nombreCompleto = "Lesli Pamela Rodr�guez";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "MALVAREZ";
		nombreCompleto = "Myriam �lvarez Contreras";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_EMISOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
				
		nombreUsuario = "AMARTINE";
		nombreCompleto = "Alfredo Martin Martinez Vazquez";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" + 
				Sistema.ROL_EMISOR  + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +				
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "FAMOYAOR";
		nombreCompleto = "Francisco Americo Moya Ordo�ez ";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" + 
				Sistema.ROL_EMISOR  + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +				
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
			
		// JEAS Actualizacion 27 Enero 2010
		nombreUsuario = "MARBAEZR";
		nombreCompleto = "Mar�a Magdalena Baez";
		roles = Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_SUSCRIPTOR_OT  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;	
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "ELERMASO";
		nombreCompleto = "Ericka Lerma Solis";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		nombreUsuario = "AGONZALE";
		nombreCompleto = "Ana Paola Gonz�lez Guzm�n";
		roles = Sistema.ROL_MESA_CONTROL + "+" +
			    Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_COORDINADOR_EMI  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "RGONZALE";
		nombreCompleto = "Rolando Alberto Gonz�lez P�rez";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_EMISOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
/*		nombreUsuario = "VVALENCI";
		nombreCompleto = "V�ctor M. Valencia Cortes";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
	
		nombreUsuario = "VVALENCI";
		nombreCompleto = "V�ctor M. Valencia Cortes";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		
/*		nombreUsuario = "AGALLEGO";
		nombreCompleto = "�ngel Gallegos Araujo";
		roles = Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
		
		// JEAS Actualizacion 27 Enero 2010
		nombreUsuario = "AGALLEGO";
		nombreCompleto = "�ngel Gallegos Araujo";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
	
		nombreUsuario = "GLOPRUIZ";
		nombreCompleto = "Guillermina Lopez Ruiz";
		roles = Sistema.ROL_MESA_CONTROL + "+" + 
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_EMISOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "LBADILLO";
		nombreCompleto = "Lesli Badillo Rangel";
		roles = Sistema.ROL_MESA_CONTROL + "+" + 
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_EMISOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		// JEAS Ajuste Solicitado por Maribel Serrano. MCA 3 Junio 2010
/*		nombreUsuario = "SANDRACR";
		nombreCompleto = "Sandra Cruz Dom�nguez";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_EMISOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
		nombreUsuario = "SANDRACR";
		nombreCompleto = "Sandra Cruz Dom�nguez";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
/*		nombreUsuario = "HHURTADO";
		nombreCompleto = "Hugo Hurtado Salazar";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
		
		// JEAS ACTUALIZACION 26 Enero 2010
		nombreUsuario = "HHURTADO";
		nombreCompleto = "Hugo Hurtado Salazar";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		// JEAS Ajuste Solicitado por Maribel Serrano. MCA 3 Junio 2010
/*		nombreUsuario = "BRODRIGU";
		nombreCompleto = "Benito Isais Rodr�guez";
		roles = Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_EMISOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
		
		nombreUsuario = "BRODRIGU";
		nombreCompleto = "Benito Isais Rodr�guez";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
/*		nombreUsuario = "AHERNAND";
		nombreCompleto = "Antonio Hern�ndez Guti�rrez";
		roles = Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
		
		// Actualizacion 26 Enero2010
		nombreUsuario = "AHERNAND";
		nombreCompleto = "Antonio Hern�ndez Guti�rrez";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		// JEAS Actualizacion 27 Enero 2010
		nombreUsuario = "KRODRIGU";
		nombreCompleto = "Karina Rodr�guez Mart�nez";
				
		roles = Sistema.ROL_MESA_CONTROL + "+" +
	    Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_COORDINADOR_EMI  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
/*		nombreUsuario = "MGARCIAT";
		nombreCompleto = "Mauro Garcia Tecontero";
		roles = Sistema.ROL_DIRECTOR_TECNICO + "+" + 
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);*/
		
		// JEAS Actualizacion 26 Enero 2010
		nombreUsuario = "MGARCIAT";
		nombreCompleto = "Mauro Garcia Tecontero";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
				Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_REASEGURO + "+" +
				Sistema.ROL_GERENTE_REASEGURO + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		
		nombreUsuario = "DMARTINE";
		nombreCompleto = "Diana Martinez Villarreal";
		roles = 
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_OP_REASEGURO_FACULTATIVO  + "+" +
				Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" +
		        Sistema.ROL_CONSULTA_DANIOS + "+" +
		        Sistema.ROL_SUPERVISOR_SUSCRIPTOR;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//Roles pendientes (no existen aun)
		nombreUsuario = "JVILLARR";
		nombreCompleto = "Julieta Soledad Villarreal Cantu";
		roles = Sistema.ROL_AGENTE + "+" + 
				Sistema.ROL_SUSCRIPTOR_OT + "+" + 
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_EMISOR + "+"+
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_OP_REASEGURO_FACULTATIVO  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "RROSSELP";
		nombreCompleto = "Rosa Isela Rossel Peralta";
		roles = 
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
				Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_ASIGNADOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_OT + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_AGENTE + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_COORDINADOR_EMI + "+" +
				Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
				Sistema.ROL_DIRECTOR_TECNICO + "+" +
				Sistema.ROL_ADMIN_PRODUCTOS+ "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_REPORTES_SINIESTROS + "+" +
				Sistema.ROL_REPORTES_REASEGURO  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "CCASTILL";
		nombreCompleto = "Cesar C. Castillo Villanueva";
		roles = Sistema.ROL_DIRECTOR_TECNICO + "+" + 
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_SUPERVISOR_SUSCRIPTOR  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
		        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
		        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		// JEAS Ingreso 26 Enero 2010
		nombreUsuario = "ZEAGUIRR";
		nombreCompleto = "Zulema Edith Aguirre Lopez";
		roles = Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_COORDINADOR_EMI  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS; 
				
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);		
	}
	
	private void llenaUsuariosSiniestros() {
		Usuario usuario = null;
		String nombreCompleto = null;
		String nombreUsuario = null;
		String roles = null;
		
		//Usuarios para Siniestros
		
		nombreUsuario = "LTREVINO";
		nombreCompleto = "Luis Roberto Trevi�o Perez";
		roles = Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" + 
				Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_COORDINADOR_SINIESTROS+ "+" + 
				Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"Luis.trevino@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DCHAVEZG";
		nombreCompleto = "David Ricardo Chavez Garcia";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_COORDINADOR_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"dchavezg@afirme.com.mx");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);

		nombreUsuario = "AGONZALT";
		nombreCompleto = "Adalberto Gonzalez Trujillo";
		roles = Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" + 
				Sistema.ROL_GERENTE_SINIESTROS + "+" + 
				Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_COORDINADOR_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"Adalberto.gonzalez@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "WEHRENST";
		nombreCompleto = "Walter Carlos Ehrentein Adame";
		roles = Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" + 
				Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_COORDINADOR_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"Walter.ehrenstein@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "MGUERRAF";
		nombreCompleto = "Myriam Isabel Guerra Flores";
		roles = Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
				Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
				Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_REPORTES_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "ADREYTRE";
		nombreCompleto = "Azael David Reyna Trejo";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" +  
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "FJCARMAR";
		nombreCompleto = "Felipe de Jesus Cardona Martinez";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "AOGARBEN";
		nombreCompleto = "Alan Omar Garcia Benavides";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" +  
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "HFGARMEN";
		nombreCompleto = "H�ctor Fernando Garcia Mendoza";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "IMCHAPER";
		nombreCompleto = "Isela Margarita Chavez Perez";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "FTAMLOPE";
		nombreCompleto = "Fernando Tamez Lopez";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "APUENTES";
		nombreCompleto = "Adolfo Puentes Rodriguez";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO + "+" +
				Sistema.ROL_REPORTES_SINIESTROS; 
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "SECONTRE";
		nombreCompleto = "Sergio Erasmo Gonzalez Contreras";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		nombreUsuario = "JHORCASI";
		nombreCompleto = "Jaime Horcasitas";
		roles = Sistema.ROL_GERENTE_SINIESTROS + "+" +
				Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "AALFAROH";
		nombreCompleto = "Alan Alfaro Hern�ndez";
		roles = Sistema.ROL_CONSULTA_DANIOS + "+" + 
				Sistema.ROL_CABINERO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		// agregado el 20101012 
		nombreUsuario = "ANLISIGU";
		nombreCompleto = "Angelica Liliana Silva Gutierrez";
		roles = Sistema.ROL_EMISOR + "+" +
				Sistema.ROL_SUSCRIPTOR_COT + "+" +
				Sistema.ROL_ASIGNADOR_SOL + "+" +
				Sistema.ROL_MESA_CONTROL + "+" +
				Sistema.ROL_REPORTES_DANIOS + "+" +
				Sistema.ROL_COORDINADOR_EMI  + "+" +
		        Sistema.ROL_CONSULTA_DANIOS; 
				
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);	
		
		nombreUsuario = "JOOMPEDU";
		nombreCompleto = "Jos� Omar P�rez Dupotex";
		roles = Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS  + "+" +		
        Sistema.ROL_REPORTES_SINIESTROS + "+" +		        
        Sistema.ROL_REPORTES_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
	}
	
	//Usuarios para Reaseguro
	private void llenaUsuariosReaseguro() {
		Usuario usuario = null;
		String nombreCompleto = null;
		String nombreUsuario = null;
		String roles = null;
		
		//**ROL OP REASEGURO AUTOMATICO**//
		//** ROL ROL_OP_PAGOS_COBROS_REASEGURADORES
		nombreUsuario = "GVANGUIA";
		nombreCompleto = "Gabriela Virginia Anguiano Carrillo";
		roles = Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" +
			Sistema.ROL_OP_REASEGURO_AUTOMATICO  + "+" +
	        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//**ROL OP REASEGURO FACULTATIVO**//
		nombreUsuario = "MMARTINE";
		nombreCompleto = "Maria Angelica Martinez Olvera";
		roles = Sistema.ROL_OP_REASEGURO_FACULTATIVO  + "+" + 
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" +
		Sistema.ROL_MAIL_REASEGURO + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"maria.martinez@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "AJIMENEZ";
		nombreCompleto = "Alejandro Jimenez Orc�";
		roles = Sistema.ROL_OP_REASEGURO_FACULTATIVO  + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" +
		Sistema.ROL_MAIL_REASEGURO + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"Alejandro.jimenez@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		//** ROL ROL_OP_PAGOS_COBROS_REASEGURADORES
		//**ROL OP REASEGURO AUTOMATICO**//
		nombreUsuario = "ACANTUCC";
		nombreCompleto = "Adriana Concepcion Cantu Cantu";
		roles =  Sistema.ROL_OP_REASEGURO_AUTOMATICO+ "+" +
			     Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES  + "+" +
			     Sistema.ROL_MAIL_REASEGURO + "+" +
 		         Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "acantuc@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
						
		//**ROL ROL_DIRECTOR_REASEGURO**//
		nombreUsuario = "ERAMIREZ";
		nombreCompleto = "Emmanuel Ramirez Lango";
		roles = Sistema.ROL_DIRECTOR_REASEGURO  + "+" +
		Sistema.ROL_MAIL_REASEGURO + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"emmanuel.ramirez@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);

		//**ROL ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO**//
		nombreUsuario = "MTREVINO";
		nombreCompleto = "Myriam Trevi�o Carranza";
		roles = Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO+ "+" +
		Sistema.ROL_CONSULTA_DANIOS;		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
	
		nombreUsuario = "EMORALES";
		nombreCompleto = "Elva America Morales Gonzalez";
		roles = Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO+ "+" +
		Sistema.ROL_MAIL_REASEGURO + "+" +
		Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "emoralesg@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		// ---- nuevo rol 
		nombreUsuario = "ZCARRILLO";
		nombreCompleto = "Elva America Morales Gonzalez";
		roles = Sistema.ROL_MAIL_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"zcarrilloc@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		nombreUsuario = "CMALDONADO";
		nombreCompleto = "Elva America Morales Gonzalez";
		roles = Sistema.ROL_MAIL_REASEGURO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"cmaldonadoe@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);	
		
	}
	
	
	private void llenaUsuariosContabilidad() {
		Usuario usuario = null;
		String nombreCompleto = null;
		String nombreUsuario = null;
		String roles = null;
		
		nombreUsuario = "CLIZSARE";
		nombreCompleto = "Claudia Lizeth Salinas Elizondo";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "csalinase@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "MAPAOLAC";
		nombreCompleto = "Marcela Patricia Olachea";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +	
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario, "molacheag@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "HOVILSOL";
		nombreCompleto = "Homero Villanueva Solis";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "DAISESAR";
		nombreCompleto = "David Israel Espinoza Arroyo";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"despinozaa@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "ROHIMAVI";
		nombreCompleto = "Rosa Hilda Martinez Vidales";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "MAGUDIZO";
		nombreCompleto = "Maria Guillermina Diaz Zorola";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "NANMARVA";
		nombreCompleto = "Nancy Martinez Vargas";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO+ "+" +
		Sistema.ROL_CONSULTA_DANIOS + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "SASUESRE";
		nombreCompleto = "Sandra Susana Espinosa Regalado";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" + 
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		nombreUsuario = "RALVARAD";
		nombreCompleto = "Ricardo Alvarado Garza";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario,"ralvaradog@afirme.com");
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		
		nombreUsuario = "JEANVIMO";
		nombreCompleto = "Jesus Angel Villarreal Montemayor";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO  + "+" +
        Sistema.ROL_CONSULTA_DANIOS;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "SEARCOAG";
		nombreCompleto = "Selene Arely Contreras Aguilar";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_REPORTES_REASEGURO  + "+" +
        Sistema.ROL_CONSULTA_DANIOS + "+" +
        Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
        Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO;
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "USUPRUEB";
		nombreCompleto = "Usuario de Pruebas";
		
		roles = Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);		
		
		nombreUsuario = "JAYANMED";
		nombreCompleto = "JUAN ANTONIO YAÑEZ MEDINA";
		
		roles = Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);		
		
		nombreUsuario = "JEVIGGON";
		nombreCompleto = "JOSE EDUARDO VIGUERAS GONZALEZ";
		
		roles = Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);		
		
		nombreUsuario = "GQUIESP";
		nombreCompleto = "GABRIELA QUINTANAR ESPINOSA";
		
		roles = Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);	
		
		nombreUsuario = "IMAYIBA";
		nombreCompleto = "ISAAC MAYORGA IBARRA";
		
		roles = Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);	
		
		nombreUsuario = "JPCRUBAR";
		nombreCompleto = "JUAN PABLO CRUZ BARRETO";
		
		roles = Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);	
		
		nombreUsuario = "VMCASPAL";
		nombreCompleto = "Victor Manuel Castillero Palencia";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "IESPCAR";
		nombreCompleto = "Ignacio Espinoza Cardona";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "MICAMALV";
		nombreCompleto = "Maria Imelda Campa Alvarez";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "MYCASARR";
		nombreCompleto = "Monica Yajaira Castillo Arredondo";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "GRAMIBA";
		nombreCompleto = "Gabriela Ramos Ibarra";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "EGCOTMAR";
		nombreCompleto = "Elda Gellely Cortes Martinez";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "SAGARMUR";
		nombreCompleto = "Sandra Aide Garza Muro";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "YAIGLPER";
		nombreCompleto = "Yahaira Alejandra Iglesias Perez";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "AARUIZAM";
		nombreCompleto = "Anel Adriana Ruiz Zamora";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "MLMONPEC";
		nombreCompleto = "Maria de la Luz Montoya Pecina";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "SCABHER";
		nombreCompleto = "Susana Cabral Hernandez";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "HEGONMOR";
		nombreCompleto = "Hilda  Edith Gonzalez Moreno";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "RATRUVEN";
		nombreCompleto = "Raul Alfredo Trujillo Venzor";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "SCAMCOR";
		nombreCompleto = "Silvia Camacho Corral";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "MESPSAN";
		nombreCompleto = "Mary Espinoza Sandoval";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "SLMACGAR";
		nombreCompleto = "Selene Lorena Macias Garcia";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "AFLUNROM";
		nombreCompleto = "Araceli Fabiola Luna Romero";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "BGARJUA";
		nombreCompleto = "Beatriz García Juárez";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

		nombreUsuario = "SEVEGLOP";
		nombreCompleto = "Sandra Eugenia Vega Lopez";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());
		
		nombreUsuario = "SHERDIA";
		nombreCompleto = "Salvador Hernandez Diaz";
		
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, getRolAdministrador());

	}
	
	private String getRolAdministrador() {
		return Sistema.ROL_AGENTE + "+" +
		Sistema.ROL_MESA_CONTROL + "+" +
		Sistema.ROL_COORDINADOR_EMI + "+" +
		Sistema.ROL_DIRECTOR_TECNICO + "+" +
		Sistema.ROL_ADMIN_PRODUCTOS + "+" +
		Sistema.ROL_EMISOR + "+" +
		Sistema.ROL_ASIGNADOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_OT + "+" +
		Sistema.ROL_SUSCRIPTOR_COT + "+" +
		Sistema.ROL_ASIGNADOR_SOL + "+" +
		Sistema.ROL_SUPERVISOR_SUSCRIPTOR + "+" +
		Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CABINERO + "+" +
		Sistema.ROL_AJUSTADOR + "+" +
		Sistema.ROL_DIRECTOR_DE_OPERACIONES + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO + "+" +
		Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_SINIESTROS + "+" +
		Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_GERENTE_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_AUTOMATICO + "+" +
		Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_SUBDIRECTOR_REASEGURO + "+" +
		Sistema.ROL_DIRECTOR_REASEGURO + "+" +
		Sistema.ROL_OP_REASEGURO_FACULTATIVO + "+" +
		Sistema.ROL_ACTUARIO + "+" +
		Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES + "+" + 
		Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS;
	}
	
	private void llenaUsuariosAuditoria(){
		Usuario usuario = null;
		String nombreCompleto = null;
		String nombreUsuario = null;
		String roles = null;
		
		nombreUsuario = "JOMAPIMU";
		nombreCompleto = "Joan Manuel Pichardo Murillo";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CONSULTA_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_CONSULTA_REASEGURO;		        
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
		
		nombreUsuario = "MARCANLU";
		nombreCompleto = "Marlene Cant� Lubin";
		roles = Sistema.ROL_REPORTES_DANIOS + "+" +
		Sistema.ROL_CONSULTA_DANIOS + "+" +
		Sistema.ROL_REPORTES_SINIESTROS + "+" +
		Sistema.ROL_CONSULTA_SINIESTROS  + "+" +
		Sistema.ROL_REPORTES_REASEGURO + "+" +
		Sistema.ROL_CONSULTA_REASEGURO;		        
		usuario = new Usuario(i++, nombreCompleto, nombreUsuario);
		usuariosRegistrados.put(nombreUsuario, usuario);
		mapaRolesUsuario.put(nombreUsuario, roles);
	}
	
}
