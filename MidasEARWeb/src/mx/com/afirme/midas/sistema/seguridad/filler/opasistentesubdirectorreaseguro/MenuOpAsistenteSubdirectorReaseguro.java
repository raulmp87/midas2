/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.opasistentesubdirectorreaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuOpAsistenteSubdirectorReaseguro {

	private List<Menu> listaMenu = null;
		
	public MenuOpAsistenteSubdirectorReaseguro() {
		listaMenu = new ArrayList<Menu>();
	}
	
public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m3_1","Reaseguro", "Catalogos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m3_1_1","Reaseguro", "Reasegurador-Corredor", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m3_1_2","Reaseguro", "Contacto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m3_1_3","Reaseguro", "Cuenta Banco", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m3_2","Reaseguro", "Contratos Proporcionales", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m3_2_1","Reaseguro", "Administración de Líneas y Contratos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m3_2_1_1","Reaseguro", "Negociación", "/MidasWeb/contratos/linea/listarLineaNegociacion.do|contenido|cargarComponentesLineaNegociacion()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m3_2_1_2","Reaseguro", "Contrato", "/MidasWeb/contratos/linea/listarLineaContrato.do|contenido|cargarComponentesLineaContrato()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"m3_2_1_3","Reaseguro", "Vigencia", "/MidasWeb/contratos/linea/listarLineaVigencia.do|contenido|cargarComponentesLineaVigencia()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m3_2_2","Reaseguro", "Administración de Facultativo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m3_2_2_1","Reaseguro", "Cotización Negociación", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa0','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m3_2_2_2","Reaseguro", "Cotización Autoriza", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa1','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m3_2_2_3","Reaseguro", "Contratos Autorizados", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa2','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m3_2_2_4","Reaseguro", "Solicitud Cancelados", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa3','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m3_2_2_5","Reaseguro", "Cancelados", "/MidasWeb/sistema/configuracion/listar.do|contenido|cargandoTree('configuracionFacultativa4','treeboxbox_tree')','treeboxbox_tree')", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("32"),"m3_5","Reaseguro", "Administracion Movimientos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("33"),"m3_5_1","Reaseguro", "Registrar Ingresos", "/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do|contenido|registrarIngresosGrid()", true);
		listaMenu.add(menu);

		menu = new Menu(new Integer("34"),"m3_5_2","Reaseguro", "Administrar Ingresos", "/MidasWeb/reaseguro/ingresos/administrarIngresos.do|contenido|inicializarComponentesAdministrarIngresos()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("35"),"m3_5_3","Reaseguro", "Administrar Egresos", "/MidasWeb/reaseguro/egresos/administrarEgresos.do|contenido|inicializarComponentesAdministrarEgresos()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("36"),"m3_6","Reaseguro", "Estados de Cuenta", "/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do|contenido|dhx_init_tabbars()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m5","Reaseguro", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m5_3","Reaseguro", "Reportes Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("39"),"m5_3_1","Reaseguro", "Administrativos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("40"),"m5_3_1_1","Reaseguro", "Perfil Cartera", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("41"),"m5_3_1_2","Reaseguro", "Trimestral de Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("44"),"m5_3_2","Reaseguro", "Movimientos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("45"),"m5_3_2_1","Reaseguro", "Reporte de Movimientos por Contrato", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("46"),"m5_3_2_2","Reaseguro", "Reporte de Movimientos de Primas por Reasegurador (REAS)", "/MidasWeb/reaseguro/reportes/buscarPoliza.do|contenido|cargarComponentesRptMovtosPorReasegurador()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("47"),"m5_3_2_3","Reaseguro", "Reporte de Soporte de Contratos de una Póliza", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("48"),"m5_3_2_4","Reaseguro", "Reporte de Distribución", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("49"),"m5_3_2_5","Reaseguro", "Reporte de Emisión", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("50"),"m5_3_3","Reaseguro", "Siniestros", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("51"),"m5_3_3_1","Reaseguro", "Reporte de Siniestros por Reasegurador (Borderaux)", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("52"),"m5_3_3_2","Reaseguro", "Reporte de Siniestros con Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("53"),"m5_3_3_3","Reaseguro", "Reporte de Siniestros de Reservas Pendientes", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("54"),"m5_3_3_4","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("55"),"m5_3_4","Reaseguro", "Contratos No Proporcionales", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("56"),"m5_3_4_1","Reaseguro", "Reporte de Siniestros por Evento Catastrófico", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("57"),"m5_3_4_2","Reaseguro", "Reporte de Siniestros Tent Plan", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("58"),"m5_3_4_3","Reaseguro", "Reporte de Siniestros Working Cover", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("59"),"m5_3_5","Reaseguro", "Ingresos y Egresos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("60"),"m5_3_5_1","Reaseguro", "Reporte de Ingresos de Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("61"),"m5_3_5_2","Reaseguro", "Reporte Estadística Fiscal Facultativos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("62"),"m5_3_5_3","Reaseguro", "Reporte Estadística Fiscal Automáticos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("66"),"m5_3_3_4_1","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("67"),"m5_3_3_4_2","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado Detallado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("68"),"m5_3_2_7","Reaseguro", "Reportes de Saldos por Reasegurador", "", true);
		listaMenu.add(menu);
		
		return this.listaMenu;
		
	}
	
}
