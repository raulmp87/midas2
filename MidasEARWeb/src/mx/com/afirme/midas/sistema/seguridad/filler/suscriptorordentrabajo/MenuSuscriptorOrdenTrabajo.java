package mx.com.afirme.midas.sistema.seguridad.filler.suscriptorordentrabajo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuSuscriptorOrdenTrabajo {
private List<Menu> listaMenu = null;
	
	public MenuSuscriptorOrdenTrabajo() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);				
		
		menu = new Menu(new Integer("2"),"m1_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m1_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
						
		menu = new Menu(new Integer("4"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3_2","Ordenes de Trabajo", "Submenu Ordenes", null, true);
		listaMenu.add(menu);				
		
		menu = new Menu(new Integer("11"),"m6","Pendientes", "Submenu Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m6_1","Lista de Pendientes", "Submenu Lista de Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m1","Autos", "Emision", null, true);
		listaMenu.add(menu);		
		
		menu = new Menu(new Integer("17"),"m1_1","Autos", "Submenu Autos", null, true);
		listaMenu.add(menu);	

		menu = new Menu(new Integer("18"),"m1_1_1","Autos", "Solicitudes Autos", null, true);
		listaMenu.add(menu);			
		return this.listaMenu;
	}
}
