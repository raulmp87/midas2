/**
 * Clase que llena las opciones de Menu para el rol de Ajustador
 */
package mx.com.afirme.midas.sistema.seguridad.filler.suscriptorcotizacion;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuSuscriptorCotizacion {

	private List<Menu> listaMenu = null;
		
	public MenuSuscriptorCotizacion() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;			
		
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m1_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m1_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3_3","Cotizaciones", "Submenu Cotizaciones", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m1_3_3_1","Cotizaciones de P�liza", "Submenu Cotizaciones de P�liza", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m1_3_3_2","Cotizaciones de Endoso", "Submenu Cotizaciones de Endoso", null, true);
		listaMenu.add(menu);				
		
		menu = new Menu(new Integer("13"),"m6","Pendientes", "Submenu Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m6_1","Lista de Pendientes", "Submenu Lista de Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("50"),"m1_3_6","Renovaciones", "Renovaciones", null, true);
		listaMenu.add(menu);			

		menu = new Menu(new Integer("51"),"m1_3_6_1","Renovaci�n Autom�tica", "Sub Menu de Renovaci�n Autom�tica", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("51"),"m1_3_6_2","Seguimiento Renovaciones", "Sub Menu de Seguimiento Renovaciones", null, true);
		listaMenu.add(menu);			
		
		menu = new Menu(new Integer("52"),"m5_1_5","Reporte de Renovaciones de P�lizas", "Reporte de Renovaciones de P�lizas", null, true);
		listaMenu.add(menu);		

		menu = new Menu(new Integer("53"),"m5_1_6","Reporte de Renovaciones de P�lizas", "Reporte de Renovaciones de P�lizas", null, true);
		listaMenu.add(menu);				
		return this.listaMenu;
	}
	
}
