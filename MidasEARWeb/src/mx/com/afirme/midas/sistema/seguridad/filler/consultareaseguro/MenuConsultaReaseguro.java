package mx.com.afirme.midas.sistema.seguridad.filler.consultareaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuConsultaReaseguro {
 	private List<Menu> listaMenu = null;
	
	public MenuConsultaReaseguro() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);		
				
		return this.listaMenu;
		
	}

}
