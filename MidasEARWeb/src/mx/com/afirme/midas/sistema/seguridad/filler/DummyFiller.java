/**
 * Clase de pruebas. Llenado de un objeto dummy simulando el objeto de seguridad para la aplicacion MIDAS.
 */
package mx.com.afirme.midas.sistema.seguridad.filler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.seguridad.Menu;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.seguridad.filler.actuario.MenuActuario;
import mx.com.afirme.midas.sistema.seguridad.filler.actuario.PaginaPermisoActuario;
import mx.com.afirme.midas.sistema.seguridad.filler.administradorcolonias.MenuAdministradorColonias;
import mx.com.afirme.midas.sistema.seguridad.filler.administradorcolonias.PaginaPermisoAdministradorColonias;
import mx.com.afirme.midas.sistema.seguridad.filler.administradorproductos.MenuAdministradorProductos;
import mx.com.afirme.midas.sistema.seguridad.filler.administradorproductos.PaginaPermisoAdministradorProductos;
import mx.com.afirme.midas.sistema.seguridad.filler.agente.MenuAgente;
import mx.com.afirme.midas.sistema.seguridad.filler.agente.PaginaPermisoAgente;
import mx.com.afirme.midas.sistema.seguridad.filler.ajustador.MenuAjustador;
import mx.com.afirme.midas.sistema.seguridad.filler.ajustador.PaginaPermisoAjustador;
import mx.com.afirme.midas.sistema.seguridad.filler.analistaadministrativo.MenuAnalistaAdministrativo;
import mx.com.afirme.midas.sistema.seguridad.filler.analistaadministrativo.PaginaPermisoAnalistaAdministrativo;
import mx.com.afirme.midas.sistema.seguridad.filler.analistaadministrativofacultativo.MenuAnalistaAdministrativoFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.analistaadministrativofacultativo.PaginaPermisoAnalistaAdministrativoFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.asignadorordentrabajo.MenuAsignadorOrdenTrabajo;
import mx.com.afirme.midas.sistema.seguridad.filler.asignadorordentrabajo.PaginaPermisoAsignadorOrdenTrabajo;
import mx.com.afirme.midas.sistema.seguridad.filler.asignadorsolicitud.MenuAsignadorSolicitud;
import mx.com.afirme.midas.sistema.seguridad.filler.asignadorsolicitud.PaginaPermisoAsignadorSolicitud;
import mx.com.afirme.midas.sistema.seguridad.filler.cabinero.MenuCabinero;
import mx.com.afirme.midas.sistema.seguridad.filler.cabinero.PaginaPermisoCabinero;
import mx.com.afirme.midas.sistema.seguridad.filler.consultadanios.MenuConsultaDanios;
import mx.com.afirme.midas.sistema.seguridad.filler.consultadanios.PaginaPermisoConsultaDanios;
import mx.com.afirme.midas.sistema.seguridad.filler.consultareaseguro.MenuConsultaReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.consultareaseguro.PaginaPermisoConsultaReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.consultasiniestros.MenuConsultaSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.consultasiniestros.PaginaPermisoConsultaSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadoradministrativoreaseguro.MenuCoordinadorAdministrativoReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadoradministrativoreaseguro.PaginaCoordinadorAdministrativoReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadoremision.MenuCoordinadorEmision;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadoremision.PaginaPermisoCoordinadorEmision;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadorsiniestrofacultativo.MenuCoordinadorSiniestrosFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadorsiniestrofacultativo.PaginaPermisoCoordinadorSiniestrosFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadorsiniestros.MenuCoordinadorSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.coordinadorsiniestros.PaginaPermisoCoordinadorSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.directorreaseguro.MenuDirectorReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.directorreaseguro.PaginaPermisoDirectorReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.directortecnico.MenuDirectorTecnico;
import mx.com.afirme.midas.sistema.seguridad.filler.directortecnico.PaginaPermisoDirectorTecnico;
import mx.com.afirme.midas.sistema.seguridad.filler.emisor.MenuEmisor;
import mx.com.afirme.midas.sistema.seguridad.filler.emisor.PaginaPermisoEmisor;
import mx.com.afirme.midas.sistema.seguridad.filler.gerentesiniestrofacultativo.MenuGerenteSiniestrosFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.gerentesiniestrofacultativo.PaginaPermisoGerenteSiniestrosFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.gerentesiniestros.MenuGerenteSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.gerentesiniestros.PaginaPermisoGerenteSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.mesacontrol.MenuMesaControl;
import mx.com.afirme.midas.sistema.seguridad.filler.mesacontrol.PaginaPermisoMesaControl;
import mx.com.afirme.midas.sistema.seguridad.filler.opasistentesubdirectorreaseguro.MenuOpAsistenteSubdirectorReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.opasistentesubdirectorreaseguro.PaginaPermisoOpAsistenteSubdirectorReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.oppagoscobrosreaseguradores.MenuOpPagosCobrosReaseguradores;
import mx.com.afirme.midas.sistema.seguridad.filler.oppagoscobrosreaseguradores.PaginaPermisoOpPagosCobrosReaseguradores;
import mx.com.afirme.midas.sistema.seguridad.filler.opreaseguroautomatico.MenuOpReaseguroAutomatico;
import mx.com.afirme.midas.sistema.seguridad.filler.opreaseguroautomatico.PaginaPermisoOpReaseguroAutomatico;
import mx.com.afirme.midas.sistema.seguridad.filler.opreasegurofacultativo.MenuOpReaseguroFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.opreasegurofacultativo.PaginaPermisoOpReaseguroFacultativo;
import mx.com.afirme.midas.sistema.seguridad.filler.reportesdanios.MenuReportesDanios;
import mx.com.afirme.midas.sistema.seguridad.filler.reportesdanios.PaginaPermisoReportesDanios;
import mx.com.afirme.midas.sistema.seguridad.filler.reportesreaseguro.MenuReportesReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.reportesreaseguro.PaginaPermisoReportesReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.reportessiniestros.MenuReportesSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.reportessiniestros.PaginaPermisoReportesSiniestros;
import mx.com.afirme.midas.sistema.seguridad.filler.sistema.MenuSistema;
import mx.com.afirme.midas.sistema.seguridad.filler.sistema.PaginaPermisoSistema;
import mx.com.afirme.midas.sistema.seguridad.filler.subdirectorreaseguro.MenuSubdirectorReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.subdirectorreaseguro.PaginaPermisoSubdirectorReaseguro;
import mx.com.afirme.midas.sistema.seguridad.filler.supervisorsuscriptor.MenuSupervisorSuscriptor;
import mx.com.afirme.midas.sistema.seguridad.filler.supervisorsuscriptor.PaginaPermisoSupervisorSuscriptor;
import mx.com.afirme.midas.sistema.seguridad.filler.suscriptorcotizacion.MenuSuscriptorCotizacion;
import mx.com.afirme.midas.sistema.seguridad.filler.suscriptorcotizacion.PaginaPermisoSuscriptorCotizacion;
import mx.com.afirme.midas.sistema.seguridad.filler.suscriptorordentrabajo.MenuSuscriptorOrdenTrabajo;
import mx.com.afirme.midas.sistema.seguridad.filler.suscriptorordentrabajo.PaginaPermisoSuscriptorOrdenTrabajo;

/**
 * @author andres.avalos
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.seguridad.UsuarioService}
 */
@Deprecated
public class DummyFiller extends UsuariosRol {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
		
	
	public DummyFiller() {
		llenaUsuarios();
		llenaPermisos();
	}
	
	private void llenaPermisos() {
		
		Permiso permiso;
		
		permiso = new Permiso(new Integer("1"),"AG","Agregar","Agregar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("2"),"AC","Actualizar","Actualizar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("3"),"BR","Borrar","Borrar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("4"),"RE","Reporte","Reporte");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("5"),"EX","Exportar","Exportar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("6"),"AD","Adjuntar","Adjuntar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("7"),"VD","VerDetalle","Ver Detalle");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("8"),"AS","Asignar","Asignar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("9"),"BU","Buscar","Buscar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("10"),"CO","Consultar","Consultar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("11"),"CT","Continuar","Continuar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("12"),"GU","Guardar","Guardar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("13"),"NV","NuevaVersion","Nueva Version");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("14"),"RE","Regresar","Regresar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("15"),"SE","Seleccionar","Seleccionar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("16"),"TE","Terminar","Terminar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("17"),"RC","rechazarCancelar","Rechazar / Cancelar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("18"),"EM","enviarEmision","Enviar a emisi�n");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("19"),"IM","imprimir","Imprimir");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("20"),"ET","emitir","Emitir");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("21"),"LI","liberar","Liberar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("22"),"AA","autoasignar","AutoAsignar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("23"),"AU","autorizar","Autorizar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("24"),"RC","rechazar","Rechazar");
		listaPermiso.add(permiso);
		
		permiso = new Permiso(new Integer("25"),"RS","resumen","Resumen");
		listaPermiso.add(permiso);		
	}
	
		
	private void llenaMenuSistema(Usuario usuario) {
		List<Menu> listaMenuSistema = new ArrayList<Menu>();
		
		MenuSistema mSistema = new MenuSistema();
		listaMenuSistema = mSistema.obtieneMenuItems();
		
		for (Menu menu : listaMenuSistema) {
			usuario.getMenus().add(menu);
		}
		
		
	}
	
	
	private void llenaMenuPorRol(Usuario usuario, Rol rol) {
		
		List<Menu> listaMenu = new ArrayList<Menu>();
				
		if (rol.getDescripcion().equals(Sistema.ROL_CABINERO)) {
			MenuCabinero mCabinero = new MenuCabinero();
			listaMenu = mCabinero.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_AJUSTADOR)) {
			MenuAjustador mAjustador = new MenuAjustador();
			listaMenu = mAjustador.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_OP_REASEGURO_AUTOMATICO)){
			MenuOpReaseguroAutomatico menuOperadorReaseguroAutomatico = new MenuOpReaseguroAutomatico();
			listaMenu = menuOperadorReaseguroAutomatico.obtieneMenuItems();
		}else if (rol.getDescripcion().equals(Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO)){
			MenuOpAsistenteSubdirectorReaseguro menuOpAsistenteSubdirectorReaseguro = new MenuOpAsistenteSubdirectorReaseguro();
			listaMenu = menuOpAsistenteSubdirectorReaseguro.obtieneMenuItems();
		}else if (rol.getDescripcion().equals(Sistema.ROL_SUBDIRECTOR_REASEGURO)){
			MenuSubdirectorReaseguro menuSubdirectorReaseguro = new MenuSubdirectorReaseguro();
			listaMenu = menuSubdirectorReaseguro.obtieneMenuItems();
		}else if (rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_REASEGURO)){
			MenuDirectorReaseguro menuDirectorReaseguro = new MenuDirectorReaseguro();
			listaMenu = menuDirectorReaseguro.obtieneMenuItems();
		}else if (rol.getDescripcion().equals(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
			MenuSupervisorSuscriptor menuSupervisorSuscriptor = new MenuSupervisorSuscriptor();
			listaMenu = menuSupervisorSuscriptor.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_SINIESTROS)){
			MenuCoordinadorSiniestros menuCoordinador = new MenuCoordinadorSiniestros();
			listaMenu = menuCoordinador.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			MenuCoordinadorSiniestrosFacultativo menuCoordinador = new MenuCoordinadorSiniestrosFacultativo();
			listaMenu = menuCoordinador.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_GERENTE_SINIESTROS)){
			MenuGerenteSiniestros menuGerente = new MenuGerenteSiniestros();
			listaMenu = menuGerente.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO)){
			MenuGerenteSiniestrosFacultativo menuGerente = new MenuGerenteSiniestrosFacultativo();
			listaMenu = menuGerente.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_REPORTES_SINIESTROS)){
			listaMenu = new MenuReportesSiniestros().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_EMISOR)){
			MenuEmisor menuEmisor = new MenuEmisor();
			listaMenu = menuEmisor.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ASIGNADOR_OT)){
			MenuAsignadorOrdenTrabajo menu = new MenuAsignadorOrdenTrabajo();
			listaMenu = menu.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_OT)){
			MenuSuscriptorOrdenTrabajo menu = new MenuSuscriptorOrdenTrabajo();
			listaMenu = menu.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ASIGNADOR_SOL)){
			listaMenu = new MenuAsignadorSolicitud().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_AGENTE)){
			listaMenu = new MenuAgente().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_COT)){
			MenuSuscriptorCotizacion menuSuscriptor = new MenuSuscriptorCotizacion();
			listaMenu = menuSuscriptor.obtieneMenuItems();
		}else if (rol.getDescripcion().equals(Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			MenuAnalistaAdministrativo mAnalistaAdministrativo = new MenuAnalistaAdministrativo();
			listaMenu = mAnalistaAdministrativo.obtieneMenuItems();
		}else if (rol.getDescripcion().equals(Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			MenuAnalistaAdministrativoFacultativo mAnalistaAdministrativoFacultativo = new MenuAnalistaAdministrativoFacultativo();
			listaMenu = mAnalistaAdministrativoFacultativo.obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS)){
			listaMenu = new MenuAdministradorColonias().obtieneMenuItems();
		}
		
		else if (rol.getDescripcion().equals(Sistema.ROL_MESA_CONTROL)){ //Nuevos Roles Da�os
			listaMenu = new MenuMesaControl().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_EMI)){
			listaMenu = new MenuCoordinadorEmision().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_TECNICO)){
			listaMenu = new MenuDirectorTecnico().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ADMIN_PRODUCTOS)){
			listaMenu = new MenuAdministradorProductos().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_REPORTES_DANIOS)){
			listaMenu = new MenuReportesDanios().obtieneMenuItems();
		}	
		
		else if (rol.getDescripcion().equals(Sistema.ROL_OP_REASEGURO_FACULTATIVO)){ //Nuevos roles reaseguro
			listaMenu = new MenuOpReaseguroFacultativo().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ACTUARIO)){
			listaMenu = new MenuActuario().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES)){
			listaMenu = new MenuOpPagosCobrosReaseguradores().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO)){
			listaMenu = new MenuCoordinadorAdministrativoReaseguro().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_REPORTES_REASEGURO)){
			listaMenu = new MenuReportesReaseguro().obtieneMenuItems();
		}
		else if (rol.getDescripcion().equals(Sistema.ROL_CONSULTA_DANIOS)){ //Roles de Consulta
			listaMenu = new MenuConsultaDanios().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_CONSULTA_SINIESTROS)){ 
			listaMenu = new MenuConsultaSiniestros().obtieneMenuItems();
		} else if (rol.getDescripcion().equals(Sistema.ROL_CONSULTA_REASEGURO)){
			listaMenu = new MenuConsultaReaseguro().obtieneMenuItems();
		}
		
		
		
		
				
		for (Menu menu : listaMenu) {
			usuario.getMenus().add(menu);
		}
		
	}
	
	private void llenaPaginaPermisoSistema(Usuario usuario) {
		List<PaginaPermiso> listaPaginaPermisoSistema = new ArrayList<PaginaPermiso>();
		
		PaginaPermisoSistema ppSistema = new PaginaPermisoSistema(this.listaPermiso);
		listaPaginaPermisoSistema = ppSistema.obtienePaginaPermisos();
		
		for (PaginaPermiso pp : listaPaginaPermisoSistema) {
			usuario.getPages().add(pp.getPagina());
		}
		
	}
	
	
	private void llenaPaginaPermisoPorRol(Usuario usuario, Rol rol) {
		
		List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
		
		if (rol.getDescripcion().equals(Sistema.ROL_CABINERO)) {
			PaginaPermisoCabinero ppCabinero = new PaginaPermisoCabinero(this.listaPermiso);
			listaPaginaPermiso = ppCabinero.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_AJUSTADOR)) {
			PaginaPermisoAjustador ppAjustador = new PaginaPermisoAjustador(this.listaPermiso);
			listaPaginaPermiso = ppAjustador.obtienePaginaPermisos();
		}else if (rol.getDescripcion().equals(Sistema.ROL_OP_REASEGURO_AUTOMATICO)){
			PaginaPermisoOpReaseguroAutomatico opReaseguroAutomatico = new PaginaPermisoOpReaseguroAutomatico(this.listaPermiso);
			listaPaginaPermiso = opReaseguroAutomatico.obtienePaginaPermisos();
		}else if(rol.getDescripcion().equals(Sistema.ROL_OP_ASISTENTE_SUBDIRECTOR_REASEGURO)){
			PaginaPermisoOpAsistenteSubdirectorReaseguro opAsistenteSubdirectorReaseguro = new PaginaPermisoOpAsistenteSubdirectorReaseguro(this.listaPermiso);
			listaPaginaPermiso = opAsistenteSubdirectorReaseguro.obtienePaginaPermisos();
		}else if (rol.getDescripcion().equals(Sistema.ROL_SUBDIRECTOR_REASEGURO)){
			PaginaPermisoSubdirectorReaseguro permisoSubdirectorReaseguro = new PaginaPermisoSubdirectorReaseguro(this.listaPermiso);
			listaPaginaPermiso = permisoSubdirectorReaseguro.obtienePaginaPermisos();
		}else if (rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_REASEGURO)){
			PaginaPermisoDirectorReaseguro paginaPermisoDirectorReaseguro = new PaginaPermisoDirectorReaseguro(this.listaPermiso);
			listaPaginaPermiso = paginaPermisoDirectorReaseguro.obtienePaginaPermisos();
		}else if (rol.getDescripcion().equals(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
			PaginaPermisoSupervisorSuscriptor paginaPermisoSupervisorSuscriptor = new PaginaPermisoSupervisorSuscriptor(this.listaPermiso);
			listaPaginaPermiso = paginaPermisoSupervisorSuscriptor.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_SINIESTROS)){
			PaginaPermisoCoordinadorSiniestros ppCoordinadorSiniestros = new PaginaPermisoCoordinadorSiniestros(this.listaPermiso);
			listaPaginaPermiso = ppCoordinadorSiniestros.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			PaginaPermisoCoordinadorSiniestrosFacultativo ppCoordinadorSiniestrosFacultativo = new PaginaPermisoCoordinadorSiniestrosFacultativo(this.listaPermiso);
			listaPaginaPermiso = ppCoordinadorSiniestrosFacultativo.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_GERENTE_SINIESTROS)){
			PaginaPermisoGerenteSiniestros ppGerenteSiniestros = new PaginaPermisoGerenteSiniestros(this.listaPermiso);
			listaPaginaPermiso = ppGerenteSiniestros.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO)){
			PaginaPermisoGerenteSiniestrosFacultativo ppGerenteSiniestrosFacultativo = new PaginaPermisoGerenteSiniestrosFacultativo(this.listaPermiso);
			listaPaginaPermiso = ppGerenteSiniestrosFacultativo.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_REPORTES_SINIESTROS)){
			listaPaginaPermiso = new PaginaPermisoReportesSiniestros(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_EMISOR)){
			listaPaginaPermiso = new PaginaPermisoEmisor(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ASIGNADOR_OT)){
			listaPaginaPermiso = new PaginaPermisoAsignadorOrdenTrabajo(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_OT)){
			listaPaginaPermiso = new PaginaPermisoSuscriptorOrdenTrabajo(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ASIGNADOR_SOL)){
			listaPaginaPermiso = new PaginaPermisoAsignadorSolicitud(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_AGENTE)){
			listaPaginaPermiso = new PaginaPermisoAgente(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_COT)){
			listaPaginaPermiso = new PaginaPermisoSuscriptorCotizacion(this.listaPermiso).obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ANALISTA_ADMINISTRATIVO)){
			PaginaPermisoAnalistaAdministrativo ppAnalista = new PaginaPermisoAnalistaAdministrativo(this.listaPermiso);
			listaPaginaPermiso = ppAnalista.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO)){
			PaginaPermisoAnalistaAdministrativoFacultativo ppAnalistaFacultativo = new PaginaPermisoAnalistaAdministrativoFacultativo(this.listaPermiso);
			listaPaginaPermiso = ppAnalistaFacultativo.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ESP_ADMINISTRADOR_COLONIAS)){
			listaPaginaPermiso = new PaginaPermisoAdministradorColonias(this.listaPermiso).obtienePaginaPermisos();
		}
		
		else if (rol.getDescripcion().equals(Sistema.ROL_MESA_CONTROL)){ //Nuevos Roles Da�os
			PaginaPermisoMesaControl ppDanos = new PaginaPermisoMesaControl(this.listaPermiso);
			listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_EMI)){
			PaginaPermisoCoordinadorEmision ppDanos = new PaginaPermisoCoordinadorEmision(this.listaPermiso);
			listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_TECNICO)){
			PaginaPermisoDirectorTecnico ppDanos = new PaginaPermisoDirectorTecnico(this.listaPermiso);
			listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ADMIN_PRODUCTOS)){
			PaginaPermisoAdministradorProductos ppDanos = new PaginaPermisoAdministradorProductos(this.listaPermiso);
			listaPaginaPermiso = ppDanos.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_REPORTES_DANIOS)){
			listaPaginaPermiso = new PaginaPermisoReportesDanios(this.listaPermiso).obtienePaginaPermisos();
		}	
		
		else if (rol.getDescripcion().equals(Sistema.ROL_OP_REASEGURO_FACULTATIVO)){ //Nuevos roles reaseguro
			PaginaPermisoOpReaseguroFacultativo ppReaseguro = new PaginaPermisoOpReaseguroFacultativo(this.listaPermiso);
			listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_ACTUARIO)){
			PaginaPermisoActuario ppReaseguro = new PaginaPermisoActuario(this.listaPermiso);
			listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_OP_PAGOS_COBROS_REASEGURADORES)){
			PaginaPermisoOpPagosCobrosReaseguradores ppReaseguro = new PaginaPermisoOpPagosCobrosReaseguradores(this.listaPermiso);
			listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_ADMINISTRATIVO_REASEGURO)){
			PaginaCoordinadorAdministrativoReaseguro ppReaseguro = new PaginaCoordinadorAdministrativoReaseguro(this.listaPermiso);
			listaPaginaPermiso = ppReaseguro.obtienePaginaPermisos();
		} else if (rol.getDescripcion().equals(Sistema.ROL_REPORTES_REASEGURO)){
			listaPaginaPermiso = new PaginaPermisoReportesReaseguro(this.listaPermiso).obtienePaginaPermisos();
		}
        	else if (rol.getDescripcion().equals(Sistema.ROL_CONSULTA_DANIOS)){ //Roles de Consulta
        	    listaPaginaPermiso = new PaginaPermisoConsultaDanios(this.listaPermiso).obtienePaginaPermisos();
        	} else if (rol.getDescripcion().equals(Sistema.ROL_CONSULTA_SINIESTROS)){ 
        	    listaPaginaPermiso = new PaginaPermisoConsultaSiniestros(this.listaPermiso).obtienePaginaPermisos();
        	} else if (rol.getDescripcion().equals(Sistema.ROL_CONSULTA_REASEGURO)){
        	    listaPaginaPermiso = new PaginaPermisoConsultaReaseguro(this.listaPermiso).obtienePaginaPermisos();
        	}
				
		for (PaginaPermiso pp : listaPaginaPermiso) {
			usuario.getPages().add(pp.getPagina());
		}
		
	}
	
	/**
	 * Obtiene un usuario del sistema MIDAS con el/los rol(es) especificado(s)
	 * @param descripcionesRol Cadena con la(s) descripcion(es) de rol(es) separadas por '+'. Ej: rol1+rol2+rol3
	 * @return Objeto de usuario MIDAS
	 */
	public Usuario obtieneUsuarioMidas(String descripcionesRol) {
		
		Usuario usuario;
		Rol rol;
		int i = 1;
		
		String[] roles = descripcionesRol.split("\\+");
		
		usuario = new Usuario(new Integer("1"), "Usuario default", "default");
		
		for (String descripcionRol : roles) {
			rol = new Rol(new Integer(i), descripcionRol);
			llenaMenuPorRol(usuario, rol);
			llenaPaginaPermisoPorRol(usuario, rol);
			usuario.getRoles().add(rol);
			//TEMPORALMENTE. El nombre de usuario para el rol de agente es usado para recuperar la lista de agentes, 
			//necesaria para registar una solicitud. Este nombre vendr� poblado de SEYCOS.
			if (rol.getDescripcion().equals(Sistema.ROL_AGENTE)) {
				usuario.setNombreUsuario("KMGONVAZ");
			} else if (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_OT)) {
				usuario.setNombreUsuario("ARMARTOR");//Arturo Martinez
			} else if (rol.getDescripcion().equals(Sistema.ROL_SUSCRIPTOR_COT)) {
				usuario.setNombreUsuario("CLGARROM");//Clara Garc�a
			}if (rol.getDescripcion().equals(Sistema.ROL_COORDINADOR_SINIESTROS)) {
				usuario.setId(new Integer(54));
			}
			
			usuario.setNombre(descripcionesRol);
			i++;
		}
			
		return usuario;
		
	}
	
	public Usuario obtieneUsuarioPorId(Integer id){
		Usuario usr = obtieneUsuarioMidas(Sistema.ROL_CABINERO,1,"","","");
		
		return usr;
		//return null;
	}
		
	
	private Usuario obtieneUsuarioMidas(String descripcionesRol, Integer id, String name, String userName, String email) {
		Usuario usuario;
		Rol rol;
		int i = 1;
		
		String[] roles = descripcionesRol.split("\\+");
		
		usuario = new Usuario(id, name, userName, email);
		
		for (String descripcionRol : roles) {
			rol = new Rol(new Integer(i), descripcionRol);
			llenaMenuSistema(usuario);
			llenaPaginaPermisoSistema(usuario);
			llenaMenuPorRol(usuario, rol);
			llenaPaginaPermisoPorRol(usuario, rol);
			usuario.getRoles().add(rol);
			i++;
		}
		return usuario;
	}
	
	public Usuario obtieneUsuarioPorid(Integer idUsuario,String rol){
		Usuario user = new Usuario();
		List<Usuario> listaUsuarios = obtieneUsuariosPorRol(rol);
		for(Usuario userLista: listaUsuarios){
			if(userLista.getId().compareTo(idUsuario) == 0){
				user = userLista;
			}
		}
		return user;
	}
	
	public Usuario obtieneUsuarioPorNombreUsuario(String nombreUsuario){
		
		nombreUsuario = nombreUsuario.toUpperCase().trim();
		
		Usuario usuarioRegistrado =  usuariosRegistrados.get(nombreUsuario);
		
		if (usuarioRegistrado != null) {
		
			Usuario usr = obtieneUsuarioMidas(mapaRolesUsuario.get(nombreUsuario),
					usuarioRegistrado.getId(), usuarioRegistrado.getNombre(), usuarioRegistrado.getNombreUsuario(), usuarioRegistrado.getEmail());
			
			return usr;
		}
		
		return null;
	}
	
	public List<Usuario> obtieneUsuariosPorRol(String rol){
		List<Usuario> usuarios = new ArrayList<Usuario>();
		
		Collection<Usuario> coleccionUsuarios =  usuariosRegistrados.values();
		
		Iterator<Usuario> itr = coleccionUsuarios.iterator();
		
		while(itr.hasNext()) {
		
		    Usuario usuario = itr.next(); 
		    String descripcionesRol = mapaRolesUsuario.get(usuario.getNombreUsuario());
		    String[] roles = descripcionesRol.split("\\+");
		    
		    for (String descripcionRol : roles) {
		    	if (descripcionRol.equals(rol)) {
		    		
		    		Usuario usr = obtieneUsuarioMidas(descripcionesRol,
		    				usuario.getId(), usuario.getNombre(), usuario.getNombreUsuario(), usuario.getEmail());
		    		
		    		usuarios.add(usr);
		    		break;
		    	}
		    }
		} 
		
		return usuarios;
	}
	
	public List<Usuario> getCoordinadoresSiniestro(){
		List<Usuario> coordinadoresSiniestro = null;
		List<Usuario> coordinadoresFacultativos = null;
		
		coordinadoresSiniestro = this.obtieneUsuariosPorRol(Sistema.ROL_COORDINADOR_SINIESTROS);
		coordinadoresFacultativos = this.obtieneUsuariosPorRol(Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO);
		
		for(Usuario coordinador : coordinadoresFacultativos){
			if(! coordinadoresSiniestro.contains(coordinador)){
				coordinadoresSiniestro.add(coordinador);
			}
		}		
		
		return coordinadoresSiniestro;
	}
	
	public Usuario obtenerUsuarioPorId(Integer idUsuario){
		Usuario usuario = new Usuario();
				
		Object[] usuarios = usuariosRegistrados.values().toArray();		
				
		for(Object userItem : usuarios){
			Usuario user = (Usuario)userItem;
			if(user.getId().compareTo(idUsuario) == 0){
				usuario = user;
				break;
			}
		}
		
		return usuario;		
	}
}
