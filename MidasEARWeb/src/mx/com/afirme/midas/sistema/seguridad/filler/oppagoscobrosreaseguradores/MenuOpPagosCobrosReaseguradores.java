/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.oppagoscobrosreaseguradores;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuOpPagosCobrosReaseguradores {

	private List<Menu> listaMenu = null;
		
	public MenuOpPagosCobrosReaseguradores() {
		listaMenu = new ArrayList<Menu>();
	}
	
public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m3_1","Reaseguro", "Catalogos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m3_1_1","Reaseguro", "Reasegurador-Corredor", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m3_1_2","Reaseguro", "Contacto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m3_1_3","Reaseguro", "Cuenta Banco", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("32"),"m3_5","Reaseguro", "Administracion Movimientos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("33"),"m3_5_1","Reaseguro", "Registrar Ingresos", "/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do|contenido|registrarIngresosGrid()", true);
		listaMenu.add(menu);

		menu = new Menu(new Integer("34"),"m3_5_2","Reaseguro", "Administrar Ingresos", "/MidasWeb/reaseguro/ingresos/administrarIngresos.do|contenido|inicializarComponentesAdministrarIngresos()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("35"),"m3_5_3","Reaseguro", "Administrar Egresos", "/MidasWeb/reaseguro/egresos/administrarEgresos.do|contenido|inicializarComponentesAdministrarEgresos()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		return this.listaMenu;
		
	}
	
}
