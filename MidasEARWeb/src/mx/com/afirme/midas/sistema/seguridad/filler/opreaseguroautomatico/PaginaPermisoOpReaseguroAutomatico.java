/**
 * Clase que llena Paginas y Permisos para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.opreaseguroautomatico;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoOpReaseguroAutomatico {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	private final int AG = 0;
	private final int AC = 1;
	private final int BR = 2;
	private final int RE = 3;
	private final int EX = 4;
	private final int AD = 5;
	private final int VD = 6;
	private final int AS = 7;
	private final int BU = 8;
	private final int CO = 9;
	private final int CT = 10;
	private final int GU = 11;
	private final int NV = 12;
	private final int SE = 14;
	
	public PaginaPermisoOpReaseguroAutomatico(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subRamo.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		/* Configuracion Linea Negociacion */
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/linea/listarLineaNegociacion.do"));  
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/mostrarAgregarInicial.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/linea/mostrarAsignarCP.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/contratocuotaparte/seleccionar.do"));
		pp.getPermisos().add(listaPermiso.get(14)); //SE 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/contratocuotaparte/mostrarInicio.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/contratocuotaparte/crearContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/participacion/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/contratocuotaparte/guardarContratoFinal.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/participacion/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoDolares.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoPesos.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contacto.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/linea/cargarParticipacionesCP.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarModificar.jsp", "/MidasWeb/contratos/participacion/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacion/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/contratos/participacion/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacion/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/cargarReaseguradores.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //C=
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AC 
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/contratocuotaparte/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/linea/mostrarAsignarPE.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/seleccionar.do"));
		pp.getPermisos().add(listaPermiso.get(14)); //SE
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoPrimerExcedente.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarInicio.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC		
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoPrimerExcedente.jsp", "/MidasWeb/contratos/contratoprimerexcedente/crearContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/contratoprimerexcedente/listarFiltradoEnGrid.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/linea/cargarParticipacionesPE.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/guardarContratoFinal.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/contratos/linea/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaNegociacion.jsp", "/MidasWeb/contratos/linea/listarLineaNegociacion.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaContrato.jsp", "/MidasWeb/contratos/linea/listarLineaContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaVigencia.jsp", "/MidasWeb/contratos/linea/listarLineaVigencia.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("autorizarLineas.jsp", "/MidasWeb/contratos/linea/autorizarLineas.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/linea/borrarLineaById.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaNegociacion.jsp", "/MidasWeb/contratos/linea/listarLineaNegociacionFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desautorizarLinea.jsp", "/MidasWeb/contratos/linea/desautorizarLinea.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("Renovar", "/MidasWeb/contratos/linea/renovarLinea.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarExtenderLinea.jsp", "/MidasWeb/contratos/linea/mostrarExtenderVigenciaLinea.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		pp.getPermisos().add(listaPermiso.get(8)); //BU 
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaVigencia.jsp", "/MidasWeb/contratos/linea/listarLineaVigenciaFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarLinea.jsp", "/MidasWeb/contratos/linea/listarLineaContratoFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		pp.getPermisos().add(listaPermiso.get(8)); //BU 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarExtenderLinea.jsp", "/MidasWeb/contratos/linea/extenderVigenciaLinea.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarContratosPorLinea", "/MidasWeb/contratos/linea/mostrarContratosPorLinea.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarLinea.jsp", "/MidasWeb/contratos/linea/mostrarLinea.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/contratos/contratocuotaparte/mostrarDetalleContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarDetalleContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarDetalleContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/autorizarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/cancelarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/regresarRegistrarContratoCuotaParte.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/contratocuotaparte/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratoprimerexcedente/autorizarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratoprimerexcedente/guardarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/guardarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/mostrarListar.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/contratoprimerexcedente/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/contratoprimerexcedente/mostrarListar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarRegistrarIngresos.jsp","/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/registrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/listarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/agregarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/borrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/modificarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/linea/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/linea/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarExtenderLinea.jsp","/MidasWeb/contratos/linea/mostrarExtenderVigenciaLinea.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/contratos/linea/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/guardaSesion.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/desasociarContratoPELinea.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/desasociarContratoCPLinea.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/rptLineasContratos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/guardarParticipacionesCP.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/guardarParticipacionesPE.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/cargarParticipacionesCPEnCrearContrato.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/reporteMovimiento.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		/* Fin Configuracion Linea Negociacion */
		
		/* Catálogos Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listar.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);


		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/contacto/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/contacto/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/contacto/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		/* Fin Catálogos Reaseguro */
		
		/* Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPrincipalEstadosCuenta.jsp","/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPrincipalEstadosCuenta.jsp","/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaTipoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarEstadosCuentaTipoReaseguroGrid.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/llenaComboSuscripcion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaContratosFacultativos.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaContratoFacultativo.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePagos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWebreaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/guardarObservaciones.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/guardarObservaciones.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuentaFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/rptEstadoCuentaMasivo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/rptEstadoCuentaFacultativoMasivo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);

		/* Fin Reaseguro */
		
		/* Reportes */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/movimientosPorContrato.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/buscarPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/reaseguroPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/reporteDistribucion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/reporteEmision.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/siniestrosReaseguradorBorderaux.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/movimientoSiniestroReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptMovtosPorContrato.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/rptMovtosPorReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSiniestrosPorReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredorOrdenado.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/rptSoporteContratoPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptDistribucion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptEmision.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
		
		/* Fin de Reportes */
		
		return this.listaPaginaPermiso;
	}
	
}
