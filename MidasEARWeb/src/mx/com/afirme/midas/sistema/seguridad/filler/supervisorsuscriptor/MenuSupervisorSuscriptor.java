/**
 * Clase que llena las opciones de Menu para el rol de Ajustador
 */
package mx.com.afirme.midas.sistema.seguridad.filler.supervisorsuscriptor;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuSupervisorSuscriptor {

	private List<Menu> listaMenu = null;
		
	public MenuSupervisorSuscriptor() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"m1","Emision", "Menu ppal Emision", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m1_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m1_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m1_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m1_3_1","Solicitudes", "Submenu Solicitudes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m1_3_2","Ordenes de Trabajo", "Submenu Ordenes", null, true);
		listaMenu.add(menu);
				
		menu = new Menu(new Integer("7"),"m1_3_3","Cotizaciones", "Submenu Cotizaciones", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("8"),"m1_3_3_1","Cotizaciones de P�liza", "Submenu Cotizaciones de P�liza", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m1_3_3_2","Cotizaciones de Endoso", "Submenu Cotizaciones de Endoso", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m1_3_4","Emisiones", "Submenu Emisiones", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"m1_3_5","Cat�logos", "Submenu Cat�logos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m1_3_5_1","Proveedores de inspecci�n", "Submenu Proveedores de inspecci�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m5","Reportes", "Submenu Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m5_1","Da�os", "Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m5_1_1","Reporte de Bases de Emisi�n", "Submenu Reporte de Bases de Emisi�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m5_1_2","Reporte PML - Terremoto y Erupci�n Volc�nica", "Submenu Reporte PML - Terremoto y Erupci�n Volc�nica", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m5_1_3","Reporte PML - Hidrometereol�gico", "Submenu Reporte PML - Hidrometereol�gico", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m5_1_6","Reporte de endosos cancelables", "", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("18"),"m6","Pendientes", "Submenu Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("19"),"m6_1","Lista de Pendientes", "Submenu Lista de Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("20"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("21"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("22"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		return this.listaMenu;
	}
	
}
