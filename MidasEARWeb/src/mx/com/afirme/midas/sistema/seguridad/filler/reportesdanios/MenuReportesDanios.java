/**
 * Clase que llena las opciones de Menu para el rol de reportesDanios
 */
package mx.com.afirme.midas.sistema.seguridad.filler.reportesdanios;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuReportesDanios {

	private List<Menu> listaMenu = null;
		
	public MenuReportesDanios() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m5","Da�os", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m5_1","Da�os", "Reportes Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m5_1_1","Da�os", "Reporte de Bases de Emisi�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m5_1_2","Reporte PML - Terremoto y Erupci�n Volc�nica", "Submenu Reporte PML - Terremoto y Erupci�n Volc�nica", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m5_1_3","Reporte PML - Hidrometereol�gico", "Submenu Reporte PML - Hidrometereol�gico", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m5_1_4","Reporte Monitoreo de Solicitudes", "Submenu Reporte Monitoreo Solicitudes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m5_1_6","Reporte de endosos cancelables", "", null, true);
		listaMenu.add(menu);
				
		return this.listaMenu;
		
	}
	
}
