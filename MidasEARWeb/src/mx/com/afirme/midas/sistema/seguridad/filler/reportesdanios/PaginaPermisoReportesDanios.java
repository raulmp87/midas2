package mx.com.afirme.midas.sistema.seguridad.filler.reportesdanios;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoReportesDanios {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	private final int AG = 0;
	private final int AC = 1;
	private final int BR = 2;
	private final int RE = 3;
	private final int EX = 4;
	private final int AD = 5;
	private final int VD = 6;
	private final int AS = 7;
	private final int BU = 8;
	private final int CO = 9;
	private final int CT = 10;
	private final int GU = 11;
	private final int NV = 12;
	private final int SE = 14;
	
	public PaginaPermisoReportesDanios(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;				

		/* Reporte de Bases Emision */
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reporteBasesEmision.jsp","/MidasWeb/danios/reportes/reporteBasesEmision/mostrarReporteMovimientoEmision.do"));
	    pp.getPermisos().add(listaPermiso.get(VD));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reporteBasesEmision.jsp","/MidasWeb/danios/reportes/reporteBasesEmision/generarReporteMovimientoEmision.do"));
	    listaPaginaPermiso.add(pp);
	    
	    /* Reporte PML Terremoto e hidro */
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reportePML.jsp","/MidasWeb/danios/reportes/reportePML/mostrarReportePML.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reportePML.jsp","/MidasWeb/danios/reportes/reportePML/validarDatosReportePML.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reportePML.jsp","/MidasWeb/danios/reportes/reportePML/generarReportePML.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reportePML.jsp","/MidasWeb/danios/reportes/reportePML/obtenerReportePML.do"));
	    listaPaginaPermiso.add(pp);
		
	    //Monitoreo de Solicitudes
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reporteMonitoreoSolicitudes.jsp","/MidasWeb/danios/reportes/mostrarReporteMonitoreoSolicitudes.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reporteMonitoreoSolicitudes.jsp","/MidasWeb/danios/reportes/generarReporteMonitoreoSolicitudes.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("reporteMonitoreoSolicitudes.jsp","/MidasWeb/danios/reportes/validarDatosReporteMonitoreoSolicitudes.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/danios/reportes/cargarAgentesPorEjecutivo.do"));
	    listaPaginaPermiso.add(pp);
	    
	   
		/* Fin de Reportes */
	    return this.listaPaginaPermiso;
	}
	
}
