package mx.com.afirme.midas.sistema.seguridad.filler;

public abstract class PaginaPermisoBase {
	protected final int AG = 0;
	protected final int AC = 1;
	protected final int BR = 2;
	protected final int RE = 3;
	protected final int EX = 4;
	protected final int AD = 5;
	protected final int VD = 6;
	protected final int AS = 7;
	protected final int BU = 8;
	protected final int CO = 9;
	protected final int CT = 10;
	protected final int GU = 11;
	protected final int NV = 12;
	protected final int SE = 14;
}
