package mx.com.afirme.midas.sistema.seguridad.filler.administradorproductos;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuAdministradorProductos {
	private List<Menu> listaMenu = null;
	
	public MenuAdministradorProductos() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
				
		menu = new Menu(new Integer("1"),"m4","Producto", "Submenu Producto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m4_1","Productos", "Submenu Productos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m4_1","Productos", "Submenu Productos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("4"),"m4_2","Tarifas", "Submenu Tarifas", null, true);
		listaMenu.add(menu);
	
		menu = new Menu(new Integer("5"),"m4_3","Cat�logos", "Submenu Cat�logos", null, true);
		listaMenu.add(menu);
			
		menu = new Menu(new Integer("6"),"m4_3_1","Autos", "Submenu Autos", null, false);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("7"),"m4_3_2","Vida", "Submenu Vida", null, false);
		listaMenu.add(menu);
				
		menu = new Menu(new Integer("8"),"m4_3_3","Da�os", "Submenu Da�os", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m4_3_3_1","Ramo", "Submenu Ramo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("10"),"m4_3_3_2","Subramo", "Submenu Subramo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("11"),"m4_3_3_3","Tipo recipiente a presi�n", "Submenu Tipo recipiente a presi�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m4_3_3_4","Subtipo recipiente a presi�n", "Submenu Subtipo recipiente a presi�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m4_3_3_5","Tipo montaje m�quina", "Submenu Tipo montaje m�quina", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("14"),"m4_3_3_6","Subtipo montaje m�quina", "Submenu Subtipo montaje m�quina", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m4_3_3_7","Zona Sismo", "Submenu Zona Sismo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("16"),"m4_3_3_8","C�digo Postal Zona Sismo", "Submenu C�digo Postal Zona Sismo", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("17"),"m4_3_3_9","Zona Hidro", "Submenu Zona Hidro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("18"),"m4_3_3_10","C�digo Postal Zona Hidro", "Submenu C�digo Postal Zona Hidro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("19"),"m4_3_3_11","Colonias", "Submenu Colonias", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("20"),"m4_3_3_12","Tipo Muro", "Submenu Tipo Muro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("21"),"m4_3_3_13","Tipo Medio de Transporte", "Submenu Tipo Medio de Transporte", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("22"),"m4_3_3_14","Tipo Empaque", "Submenu Tipo Empaque", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("23"),"m4_3_3_15","Tipo Obra Civil", "Submenu Tipo Obra Civil", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("24"),"m4_3_3_16","Material Combustible", "Submenu Material Combustible", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("25"),"m4_3_3_17","Productos", "Submenu Productos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("26"),"m4_3_3_18","Coberturas", "Submenu Coberturas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("27"),"m4_3_3_19","Riesgos", "Submenu Riesgos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("28"),"m4_3_3_20","Giro", "Submenu Giro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("29"),"m4_3_3_21","SubGiro", "Submenu SubGiro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("30"),"m4_3_3_22","GiroRC", "Submenu GiroRC", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("31"),"m4_3_3_23","SubGiroRC", "Submenu SubGiroRC", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("32"),"m4_3_3_24","Tipo equipo contratista", "Submenu Tipo equipo contratista", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("33"),"m4_3_3_25","SubTipo equipo contratista", "Submenu SubTipo equipo contratista", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("34"),"m4_3_3_26","Giro transporte", "Submenu Giro transporte", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("35"),"m4_3_3_27","Tipos de transporte", "Submenu Tipos de transporte", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("36"),"m4_3_3_28","Tipos de servicio de transporte", "Submenu Tipos de servicio de transporte", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m4_3_3_29","Tipos de propietarios de camiones", "Submenu Tipos de propietarios de camiones", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m4_3_3_30","Tipos de aeronave", "Submenu Tipos de aeronave", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("39"),"m4_3_3_31","Tipos de licencia de piloto", "Submenu Tipos de licencia de piloto", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("40"),"m4_3_3_32","Tipos de destino de transporte", "Submenu Tipos de destino de transporte", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("41"),"m4_3_3_33","Paises por tipo de destino transporte", "Submenu Paises por tipo de destino transporte", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("42"),"m4_3_3_34","Tipos de embarcaci�n", "Submenu Tipos de embarcaci�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("43"),"m4_3_3_35","Clasificaci�n de embarcaci�n", "Submenu Clasificaci�n de embarcaci�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("44"),"m4_3_3_36","Tipo de navegaci�n", "Submenu Tipo de navegaci�n", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("45"),"m4_3_3_37","Tipo de bandera", "Submenu Tipo de bandera", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("46"),"m4_3_3_38","Descuento Vario", "Submenu Descuento Vario", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("47"),"m4_3_3_39","Recargo Vario", "Submenu Recargo Vario", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("48"),"m4_3_3_40","Aumento Vario", "Submenu Aumento Vario", null, true);
		listaMenu.add(menu);	
		
		menu = new Menu(new Integer("49"),"m4_3_3_41","Datos de Riesgo", "Catalogo de Datos de Riesgo", "/MidasWeb/catalogos/datosriesgo/listarFiltrado.do|contenido|null", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("54"),"m6","Pendientes", "Submenu Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("55"),"m6_1","Lista de Pendientes", "Submenu Lista de Pendientes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("56"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("57"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("58"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
				
		return this.listaMenu;
	}
}
