/**
 * Clase que llena Paginas y Permisos para el rol de Ajustador
 */
package mx.com.afirme.midas.sistema.seguridad.filler.autorizador;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoAutorizador {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoAutorizador(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
		
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
		
		PaginaPermiso pp;
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		//
		
		//Catalogo Ajustador
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/ajustador/listar.do")); //listado ajustador
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp", "/MidasWeb/catalogos/ajustador/listar.do"));  //Pagina 2
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(2)); //BR
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  //Pagina 3
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp", "/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  //Pagina 4
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		listaPaginaPermiso.add(pp);
		//
		
		//Configuracion de Productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracion.jsp", "/MidasWeb/sistema/configuracion/listar.do"));  //inicial Productos
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		//
		
		//Desplegar Arbol de Productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracion.jsp", "/MidasWeb/configuracion/producto/poblarTreeView.do"));  //Tree Productos
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		//
		//Desplegar Arbol de Productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracion.jsp", "/MidasWeb/configuracion/producto/listarPorPradre.do"));  //listarPorPradre Productos
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		//		
		
		//Desplegar Destalle de Productos del Arbol
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/configuracion/producto/mostrarDetalle.do"));  //Tree Productos
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);		
		
		//Productos asociarDescuento
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarDescuentoAProducto.jsp", "/MidasWeb/configuracion/producto/asociarDescuento.do"));  //Productos asociarDescuento
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);			
		
		//Productos mostrarDescuentosAsociados
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarDescuentoAProducto.jsp", "/MidasWeb/configuracion/producto/mostrarDescuentosAsociados.do"));  //Productos mostrarDescuentosAsociados
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);	

		//Productos guardarDescuentoAsociado
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarDescuentoAProducto.jsp", "/MidasWeb/configuracion/producto/guardarDescuentoAsociado.do"));  //Productos guardarDescuentoAsociado
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);	
		
		//Configuracion Tarifas
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/tarifa/configuracion/listar.do"));  //Listar Tarifa
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/tarifa/configuracion/mostrarAgregar.do"));  //Mostrar pagina Agregar Tarifa
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/tarifa/configuracion/agregar.do"));  //Agregar Tarifa
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		//
				
		/****** inicio de permisos para catálogos de producto ******/
		//Pagina para visualizar la lista de productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/producto/listar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar la lista filtrada de productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/producto/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar formulario de agregar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/producto/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para agregar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/producto/agregar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de borrar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/producto/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //Borrar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de modificar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/producto/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de detalle producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarcatalogo.jsp", "/MidasWeb/catalogos/producto/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para borrar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/producto/borrar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para modificar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/producto/modificar.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar Asociar moneda a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarMonedaAProducto.jsp", "/MidasWeb/configuracion/producto/asociarMoneda.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar monedaS asociadas a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/producto/mostrarMonedasAsociadas.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar monedas no asociadas a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/producto/mostrarMonedasPorAsociar.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar guardar asosiacion de moneda a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/producto/guardarMonedaAsociada.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar Asociar ramo a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRamoAProducto.jsp", "/MidasWeb/configuracion/producto/mostrarAsociarRamo.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar ramos asociadas a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/producto/mostrarRamosAsociados.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar ramos no asociadas a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/producto/mostrarRamosPorAsociar.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar guardar asosiacion de ramo a producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/producto/guardarRamoAsociado.do"));
		listaPaginaPermiso.add(pp);
		/****** Fin de permisos para catálogos de producto ******/
		
		/****** Inicio de permisos para catálogos de TipoPoliza******/
		//Pagina para visualizar la lista de polizas
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar formulario de agregar poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para agregar poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/agregar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de borrar poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //Borrar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de modificar poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de detalle poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para borrar poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/borrar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para modificar poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/modificar.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar formulario de detalle poliza desde la vista de producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegardetalle.jsp", "/MidasWeb/configuracion/tipopoliza/mostrar.do"));
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar Asociar moneda a Tipo de Poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarMonedaATipoPoliza.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarAsociarMoneda.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar monedas asociadas a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/mostrarMonedasAsociadas.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar guardar asociación de moneda a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/guardarMonedaAsociada.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar monedas no asociadas a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/mostrarMonedasPorAsociar.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar Asociar ramo a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRamoATipoPoliza.jsp", "/MidasWeb/configuracion/tipopoliza/mostrarAsociarRamo.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar ramos asociadas a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/mostrarRamosAsociados.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar ramos no asociadas a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/mostrarRamosPorAsociar.do"));
		listaPaginaPermiso.add(pp);
		//Pagina para mostrar guardar asosiacion de ramo a poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/tipopoliza/guardarRamoAsociado.do"));
		listaPaginaPermiso.add(pp);
		/****** Fin de permisos para catálogos de TipoPoliza******/
		
		/****** Inicio de permisos para catálogos de Seccion ******/
		//Pagina para visualizar la lista de secciones
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/configuracion/seccion/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar formulario de agregar seccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/configuracion/seccion/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para agregar seccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/seccion/agregar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de borrar seccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/configuracion/seccion/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //Borrar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de modificar seccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/configuracion/seccion/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para borrar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/configuracion/seccion/borrar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para modificar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/configuracion/seccion/modificar.do"));
		listaPaginaPermiso.add(pp);
		/****** Fin de permisos para catálogos de Seccion ******/
		
		/****** inicio de permisos para catálogos de cobertura ******/
		//Pagina para visualizar la lista de coberturas
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/cobertura/listar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar la lista filtrada de coberturas
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/cobertura/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar formulario de agregar cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/cobertura/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para obtener lista de subramos pertenecientes a un ramo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/subRamo.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para agregar cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/cobertura/agregar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de borrar cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/cobertura/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //Borrar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de modificar cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/cobertura/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de detalle cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarcatalogo.jsp", "/MidasWeb/catalogos/cobertura/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para borrar cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/cobertura/borrar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para modificar cobertura
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/cobertura/modificar.do"));
		listaPaginaPermiso.add(pp);
		/****** Fin de permisos para catálogos de cobertura******/
		
		/****** inicio de permisos para catálogos de riesgo ******/
		//Pagina para visualizar la lista de productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/riesgo/listar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar la lista filtrada de productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/riesgo/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //Ver detalle
		pp.getPermisos().add(listaPermiso.get(0)); //Agregar
		pp.getPermisos().add(listaPermiso.get(8)); //Buscar
		listaPaginaPermiso.add(pp);
		
		//Pagina para visualizar formulario de agregar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/riesgo/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para agregar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/riesgo/agregar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de borrar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/riesgo/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //Borrar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de modificar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/riesgo/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para mostrar formulario de detalle producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarcatalogo.jsp", "/MidasWeb/catalogos/riesgo/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Pagina para borrar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/riesgo/borrar.do"));
		listaPaginaPermiso.add(pp);
		
		//Pagina para modificar producto
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/riesgo/modificar.do"));
		listaPaginaPermiso.add(pp);
		/****** Fin de permisos para catálogos de riesgo******/
		
		/****** Inicio de permisos para comision en las cotizaciones ******/
		//Pagina para visualizar el grid de comisiones
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificarComisiones.jsp", "/MidasWeb/cotizacion/comision/mostrar.do"));
		pp.getPermisos().add(listaPermiso.get(11));//guardar
		listaPaginaPermiso.add(pp);
		
		//Action para mostrar las comisiones en el data Grid
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificarComisiones.jsp", "/MidasWeb/cotizacion/comision/mostrarComisionesCotizacion.do"));
		//permiso para guardar el porcentaje de la comision
		pp.getPermisos().add(listaPermiso.get(11));//GU - guardar
		listaPaginaPermiso.add(pp);
		
		//Action para guardar el porcentaje en las comisiones en el data Grid
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificarComisiones.jsp", "/MidasWeb/cotizacion/comision/guardarComisionCotizacion.do"));
		listaPaginaPermiso.add(pp);
		/****** Fin de permisos para comision en las cotizaciones ******/
		
		/****** inicio de permisos para Orden de trabajo ******/
		//Permiso para visualizar las ordenes de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp", "/MidasWeb/cotizacion/listarOrdenesTrabajo.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		//Permiso para visualizar el arbol de cada Orden de Trabajo, incluyendo los tabs
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp", "/MidasWeb/cotizacion/mostrarODT.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		//Permiso para visualizar lista filtrada de ordenes de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp", "/MidasWeb/cotizacion/listarOrdenesTrabajoFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		//Permiso para visualizar lista paginada filtrada de ordenes de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp", "/MidasWeb/cotizacion/ordenTrabajo/listarFiltradoPaginado.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		//Permiso para visualizar el formulario de cancelar/rechazar orden de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cancelarOrdenTrabajo.jsp", "/MidasWeb/cotizacion/mostrarCancelarOrdenTrabajo.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11));//GU - guardar
		listaPaginaPermiso.add(pp);
		
		//Permiso para cancelar/rechazar orden de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp", "/MidasWeb/cotizacion/cancelarOrdenTrabajo.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		//pp.getPermisos().add(listaPermiso.get(6)); //VD
		//pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		//Permiso para obtener filtrado de monedas por tipo poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarOrdenTrabajo.jsp", "/MidasWeb/cotizacion/editarOrdenTrabajo.do"));
		//pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11)); //Guardar
		pp.getPermisos().add(listaPermiso.get(13)); //Regresar
		listaPaginaPermiso.add(pp);
		
		//Permiso para obtener filtrado de monedas por tipo poliza
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarOrdenTrabajo.jsp", "/MidasWeb/monedaTipoPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		//Permiso para consultar persona y direccion de una OT
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPersona.jsp", "/MidasWeb/cotizacion/mostrarPersona.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11));//Guardar
		listaPaginaPermiso.add(pp);
		
		//Permiso para mostrar pagina de agregar/editar Direccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarDireccion.jsp", "/MidasWeb/direccion/mostrarAgregarDireccion.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11));//GU
		listaPaginaPermiso.add(pp);
		
		//Permiso para confirmar agregar/editar Direccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarOrdenTrabajo.jsp", "/MidasWeb/direccion/agregarDireccion.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11));//GU
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/ajustador/mostrarAgregar.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarDireccion.jsp", "/MidasWeb/ciudad.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarDireccion.jsp", "/MidasWeb/colonia.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarDireccion.jsp", "/MidasWeb/codigoPostal.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		//Permiso para mostrar pagina de agregar/editar Persona
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarPersona.jsp", "/MidasWeb/cotizacion/agregarPersona.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11));//GU
		listaPaginaPermiso.add(pp);
		
		//Permiso para confirmar agregar/editar Direccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarPersona.jsp", "/MidasWeb/cotizacion/mostrarAgregarPersona.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11));//GU
		listaPaginaPermiso.add(pp);
		
		//Permiso para guardar los cambios a una OT
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarOrdenTrabajo.jsp", "/MidasWeb/cotizacion/guardarOrdenTrabajo.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		listaPaginaPermiso.add(pp);
		
		
		//Permiso para mostrar formulario de asignar orden de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asignarOrdenTrabajo.jsp", "/MidasWeb/cotizacion/mostrarAsignarOrdenTrabajo.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		listaPaginaPermiso.add(pp);
		
		//Permiso para asignar orden de trabajo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp", "/MidasWeb/cotizacion/asignarOrdenTrabajo.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		
		//Permiso para mostrar formulario de asignar de solicitud de endoso
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asignar.jsp", "/MidasWeb/endoso/solicitud/mostrarAsignar.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		listaPaginaPermiso.add(pp);
				
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/endoso/solicitud/asignar.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		

		//Permiso para asignar desplegar
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp", "/MidasWeb/cotizacion/mostrar.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp", "/MidasWeb/cotizacion/mostrarODT.do" ));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		listaPaginaPermiso.add(pp);	
		/****** Fin de permisos para Orden de trabajo ******/
		

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/solicitud/listar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/solicitud/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/listarFiltradoPaginado.do"));  
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/solicitud/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(10));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/solicitud/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/solicitud/agregarApartirPoliza.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/solicitud/validarDatosSolicitud.do"));
		pp.getPermisos().add(listaPermiso.get(10));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("acciones.jsp", "/MidasWeb/solicitud/mostrarAcciones.do"));
		pp.getPermisos().add(listaPermiso.get(10));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		
		
		/************Solicitud Endoso ****************/
		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/endoso/solicitud/listar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/endoso/solicitud/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/endoso/solicitud/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(10));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/endoso/solicitud/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/endoso/solicitud/validarDatosSolicitud.do"));
		pp.getPermisos().add(listaPermiso.get(10));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("acciones.jsp", "/MidasWeb/endoso/solicitud/mostrarAcciones.do"));
		pp.getPermisos().add(listaPermiso.get(10));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		
		
		/*********************************/
		
		
		
		
		
		
		
		
		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/sistema/vault/uploadHandler.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/sistema/vault/getInfoHandler.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/sistema/vault/getIdHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp", "/MidasWeb/solicitud/listarDocumentos.do"));
		pp.getPermisos().add(listaPermiso.get(2));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp", "/MidasWeb/solicitud/borrarDocumento.do"));
		listaPaginaPermiso.add(pp);
		
		/**********
		 *Solicitud Endoso
		 */
		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp", "/MidasWeb/endoso/solicitud/listarDocumentos.do"));
		pp.getPermisos().add(listaPermiso.get(2));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp", "/MidasWeb/endoso/solicitud/borrarDocumento.do"));
		listaPaginaPermiso.add(pp);
		
				
		/*******************************/

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/sistema/download/descargarArchivo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/solicitud/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/solicitud/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(2));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/solicitud/cerrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/solicitud/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		pp.getPermisos().add(listaPermiso.get(11));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/solicitud/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		pp.getPermisos().add(listaPermiso.get(11));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/solicitud/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		
		/* permisos arbol orden de trabajo */
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("verArbol.jsp", "/MidasWeb/cotizacion/cargarArbolOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/cotizacion/inciso/listar.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ubicacion.jsp", "/MidasWeb/cotizacion/inciso/mostrarUbicacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/cotizacion/inciso/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(11));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cotizacion/inciso/mostrarDatosInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/cotizacion/inciso/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(1));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/cotizacion/inciso/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/cotizacion/inciso/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(13));
		listaPaginaPermiso.add(pp);
		/********Catalogo de Productos**********************/
		//Listado
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/producto/listar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		//Mostrar Agregar
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/producto/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		//Agregar
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/producto/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/realizarContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		/// detalle facultativo contratofacultativo/slip/mostrarDetalle
	
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarDetalle.jsp", "/MidasWeb/contratofacultativo/slip/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		listaPaginaPermiso.add(pp);
	 
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("realizarCotizacionFacultativa.jsp", "/MidasWeb/contratofacultativo/slip/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		listaPaginaPermiso.add(pp);
	 
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("realizarCotizacionFacultativa.jsp", "/MidasWeb/contratofacultativo/cotizafacultativa/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		listaPaginaPermiso.add(pp);
	 
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratofacultativo/agregarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		listaPaginaPermiso.add(pp);
		
			
		/********Fin de Catalogo de Productos***************/
		
		/********Inicio de ContratoFacultativo -> Slip ***************/
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarSlip.jsp", "/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		listaPaginaPermiso.add(pp);
		/********Fin de ContratoFacultativo -> Slip ***************/
		
		/********Inicio de Catálogo de proveedores de inspeccion ***************/
		//permiso para el listado de proveedores de inspeccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarProveedores.jsp", "/MidasWeb/cotizacion/proveedorInspeccion/listar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		listaPaginaPermiso.add(pp);
		
		//permiso para ver el detalle de proveedor de inspeccion
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC - Modificar
		pp.getPermisos().add(listaPermiso.get(2)); //BR - Borrar
		listaPaginaPermiso.add(pp);
		
		/********Fin de Catálogo de proveedores de inspeccion ***************/
		
		/********Permisos para primer riesgo y luc ***************/
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("primerRiesgo.jsp", "/cotizacion/primerRiesgoLUC/mostrarPrimerRiesgo.do"));
		pp.getPermisos().add(listaPermiso.get(3)); //RE
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC - Modificar
		pp.getPermisos().add(listaPermiso.get(2)); //BR - Borrar
		listaPaginaPermiso.add(pp);		

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("igualacionPrimas.jsp", "/MidasWeb/cotizacion/igualacion/mostrarIgualacionPrimas.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("igualacionPrimas.jsp", "/MidasWeb/cotizacion/igualacion/igualarPrimaNeta.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("igualacionPrimas.jsp","/MidasWeb/cotizacion/igualacion/eliminaIgualacionPrima.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("igualacionCuotas.jsp", "/MidasWeb/cotizacion/igualacion/mostrarIgualacionCuotas.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("igualacionCuotas.jsp", "/MidasWeb/cotizacion/igualacion/igualarCuota.do"));
		pp.getPermisos().add(listaPermiso.get(0));
		listaPaginaPermiso.add(pp);

		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarCotizaciones.jsp", "/MidasWeb/cotizacion/cotizacion/listarCotizacionesFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //Consultar
		pp.getPermisos().add(listaPermiso.get(7)); //AS - Asignar
		pp.getPermisos().add(listaPermiso.get(9)); //Consultar
		pp.getPermisos().add(listaPermiso.get(19)); //Emitir
		pp.getPermisos().add(listaPermiso.get(17)); //Emitir
		pp.getPermisos().add(listaPermiso.get(18)); //Imprimir
		pp.getPermisos().add(listaPermiso.get(20)); //Liberar
		pp.getPermisos().add(listaPermiso.get(1)); //Modificar
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarCotizaciones.jsp","/MidasWeb/cotizacion/cotizacion/listarFiltradoPaginado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //Consultar
		pp.getPermisos().add(listaPermiso.get(7)); //AS - Asignar
		pp.getPermisos().add(listaPermiso.get(9)); //Consultar
		pp.getPermisos().add(listaPermiso.get(19)); //Emitir
		pp.getPermisos().add(listaPermiso.get(17)); //Emitir
		pp.getPermisos().add(listaPermiso.get(18)); //Imprimir
		pp.getPermisos().add(listaPermiso.get(20)); //Liberar
		pp.getPermisos().add(listaPermiso.get(1)); //Modificar
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarCotizaciones.jsp", "/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //Consultar
		pp.getPermisos().add(listaPermiso.get(18)); //Imprimir
		pp.getPermisos().add(listaPermiso.get(1)); //Modificar
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
		
		return this.listaPaginaPermiso;
	}
	
}
