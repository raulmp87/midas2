/**
 * Clase de Filtro de seguridad en la aplicaci�n MIDAS
 */
package mx.com.afirme.midas.sistema.seguridad;

/**
 * @author andres.avalos
 *
 */

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.sistema.MidasRequest;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Filtro implements Filter{
	
	private static final Logger LOG = Logger.getLogger(Filtro.class);
	 private String requestSessionId = null;
	 private String requestUserName = null;
	 
	 private String onErrorUrl = null;
	  
	 private SistemaContext sistemaContext;
	 private UsuarioService usuarioService;
	
	@Autowired
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	
	public void init(FilterConfig filterConfig)
	      throws ServletException {
		if(sistemaContext.getAsmActivo()){
			onErrorUrl = sistemaContext.getAsmLogin();
		}else{
			onErrorUrl = filterConfig.getInitParameter("onError");
		}		
	}
	  
	public void doFilter(ServletRequest request,  
	                     ServletResponse response,
	                     FilterChain chain)
	                 throws IOException, ServletException {
	    
		MidasRequest req = null;
	    req = new MidasRequest((HttpServletRequest) request);
		
		HttpServletResponse res = (HttpServletResponse) response;
		boolean restClient = request.getParameter("restClient") != null && request.getParameter("restClient").equals("true") ? true : false;
		boolean crearSesion = !restClient;
	    HttpSession session =null;
	    if(crearSesion){
	    	session=req.getSession(crearSesion);
	    }	    
	    Usuario usuario = null;
	    String errorURL = null;
	    boolean esServicioPublico=false;
	    //0= no tiene sesion valida, 1= no tiene permiso de acceso, 2=autorizado
	    int acceso = 0;
	    
	    //Maneja la variable de sesion para la paginacion
	    handlePaginationCache(req);
	    
	    String paginaDestinoSinContext = (req.getServletPath() != null ? req.getServletPath() : "") +  (req.getPathInfo() != null ? req.getPathInfo() : "");
	    String paginaDestino = req.getContextPath() + paginaDestinoSinContext;
	    
	    System.out.println("paginaDestino == >"+ paginaDestino);
	    // se valida si trae token
	    String token = null;
	    if(sistemaContext.getAsmActivo()){
	    	token = req.getParameter("token");
	    }
	    
	    //NV Si se trata de que se solicita validar a un usuario le permite el acceso
	    //FIXME: Debemos cargar al iniciar la aplicación una lista con las paginas publicas que tiene ASM en lugar de tenerlas hardcodeadas.
	    List<String> excepciones = Arrays.asList("/usuario/validarUsuario.do", 
	    		"/usuario/cambiarPassword.do", 
	    		"/sistema/logout.do", 
	    		"/siniestro/reporte-movil/guardar.action", 
	    		"/siniestro/reporte-movil/validarNumeroPoliza.action",
	    		"/siniestro/reporte-movil/obtenerDatosAjustador.action",
	    		"/siniestro/reporte-movil/obtenerUbicacionAjustador.action",
	    		"/siniestro/reporte-movil/confirmarArribo.action",
	    		"/siniestro/reporte-movil/confirmarPrimerArribo.action",
	    		"/portal/login.action",
	    		"/portal/doLogin.action",
	    		"/portal/createUser.action",
	    		"/portal/resetPassword.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/getTypePoliza.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/guardar.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/getSiniestrosByPadre.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/getSiniestrosByTipoProducto.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/validateGuardarPolizaMigrada.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/addPolizasMigradasToUser.action",
	    		"/rest/cliente/polizas/clientePolizas-movil/buscarPolizasByUsuario.action",
	    		"/rest/MidasWeb/rest/cliente/polizas/clientePolizas-movil/validateGuardar.action",
	    		"/rest/cliente/cotizador-movil/validarClavePromo.action",
	    		"/rest/portal/cotizador/cotizadorPortal/listarVehiculos.action",
	    		"/rest/portal/cotizador/cotizadorPortal/listarEstados.action",
	    		"/rest/portal/cotizador/cotizadorPortal/listarPaquetes.action",
	    		"/rest/portal/cotizador/cotizadorPortal/mostrarCotizacion.action",
	    		"/rest/portal/cotizador/cotizadorPortal/contratarCotizacion.action",
	    		"/rest/portal/cotizador/cotizadorPortal/mostrarDetalleCotizacion.action",
	    		"/rest/portal/cotizador/cotizadorPortal/enviarCorreoProspecto.action",
	    		"/rest/portal/cotizador/cotizadorPortal/imprimirCotizacion.action",
	    		"/rest/portal/cotizador/cotizadorPortal/listarFormasPago.action",
			    "/rest/portal/cotizador/vida/cotizadorPortal/mostrarDetalleCotizacion.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/cotizar.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/contratarCotizacion.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/listarSumasAseguradas.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/listarOcupaciones.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/listarPaquetes.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/listarClavesSexo.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/imprimirCotizacion.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/enviarCorreoProspecto.action",
	    		"/rest/portal/cotizador/vida/cotizadorPortal/listarFormasPago.action",
	    		"/rest/cliente/cotizador-movil/mostrarCotizacion.action",
	    		"/rest/cliente/cotizador-movil/listarMarcas.action",
	    		"/rest/cliente/cotizador-movil/listarModelos.action",
	    		"/rest/cliente/cotizador-movil/listarTipos.action",
	    		"/rest/cliente/cotizador-movil/listarEstados.action",
	    		"/rest/cliente/cotizador-movil/listarDescripcionVehiculo.action",
	    		"/rest/cliente/vida/cotizador-movil/cargarSumaAsegurada.action",
	    		"/rest/cliente/vida/cotizador-movil/cargarOcupacion.action",
	    		"/rest/cliente/vida/cotizador-movil/cargarOcupacionPorId.action",
	    		"/rest/cliente/vida/cotizador-movil/cotizarVida.action",
	    		"/rest/cliente/cotizador-movil/validarEstatusUsuarioActual.action",
	    		"/rest/cliente/siniestro/cuestionario/obtenerPreguntas.action",
	    		"/rest/cliente/siniestro/cuestionario/responder.action",
	    		"/rest/auth/login.action",
	    		"/rest/auth/logout.action",
	    		"/rest/auth/createPortalUser.action",
	    		"/rest/auth/confirmEmail.action",
	    		"/rest/auth/resendConfirmationEmail.action",
	    		"/rest/auth/resetPassword.action",
	    		"/rest/auth/changePasswordInput.action",
	    		"/rest/auth/changePassword2.action",
	    		"/rest/auth/changePasswordSuccess.action",
	    		"/cometd/*",
	    		"/rest/movil/cotizador/cotizacionMovilSeguroObligatorio/imprimirPdf.action",
	    		"/rest/movil/cotizador/cotizacionMovilEnlace/imprimirPdf.action",
	    		"/rest/movil/cotizador/cotizacionMovilEnlace/saveCotizacion.action",
	    		"/persona/obtenerFoto.action",
	    		"/persona/guardarFoto.action",
	    		"/rest/portal/seguros/clientes/comentarios/sendMail.action",
	    		"/rest/portal/seguros/clientes/comentarios/getDepartments.action",
	    		"/rest/portal/seguros/clientes/comentarios/getOffices.action",
	    		"/rest/perdidaTotalCFDI/enviarCorreoIndemnizacionCFDI.action",
	    		"/rest/reportes/emisiones.action",
	    		"/notification/push.action"
	    		
		);
	    
	    boolean excepcionEncontrada = false;
	    for (String excepcion : excepciones) {
	    	if (excepcion.matches(paginaDestinoSinContext)) { 
	    		excepcionEncontrada = true;
	    		esServicioPublico=true;
	    		break;
	    	}
	    }

	    if(session != null){
	    	boolean keyValue = session.getAttribute(sistemaContext.getUsuarioAccesoPortal()) != null ? ((Boolean)session.getAttribute(sistemaContext.getUsuarioAccesoPortal())) : false;
	    	if(paginaDestinoSinContext.equals("/portal/doLogin.action") || keyValue) {
	    		session.setAttribute(sistemaContext.getUsuarioAccesoPortal(), true);			
	    		if (paginaDestinoSinContext.equals("/sistema/inicio.do") && !keyValue){
	    			session.setAttribute(sistemaContext.getUsuarioAccesoPortal(), true);
	    		}					
	    	}else{
	    		session.setAttribute(sistemaContext.getUsuarioAccesoPortal(), false);
	    	}
	    }
		
	    if (excepcionEncontrada&&token==null) { 
	    	acceso = 2;
	    	chain.doFilter(request, response);	
	    }else {
		    //Se revisa si se cuenta con sesion local
	    	if (session != null) {
	    		usuario = (Usuario)session.getAttribute(sistemaContext.getUsuarioAccesoMidas());
	    	}
	    	
		    //Si no tiene sesion local
		    if (usuario == null) {
		    	if(sistemaContext.getAsmActivo()){
		    		requestSessionId = req.getParameter("userAccessId");
		    		requestUserName =  req.getParameter("username");		
		    	}else{
			    	requestSessionId = req.getParameter("idSesion");
			    	requestUserName =  req.getParameter("userName");		    		
		    	}

		    	//Se verifica si se obtuvo mediante el request el id de sesion del usuario y su clave de usuario (username)
		    	if ((requestSessionId != null && requestUserName != null) || token != null) {

					try {
						// El Sistema envia la clave de usuario y el id de
						// sesi�n al Servicio Seguridad Externo
						// El sistema recibe el objeto de usuario si el
						// usuario existe y si este tiene una sesi�n
						// iniciada,
						// recibe error en caso contrario
						if (token != null) {
							usuario = usuarioService.buscarUsuarioRegistrado(token);
						} else {
							usuario = usuarioService.buscarUsuarioRegistrado(
									requestUserName.trim(),
									requestSessionId.trim());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					//Si se recibio un objeto usuario
		    		 if (usuario != null) {
		    			 //Se le agrega el id de sesion del usuario (id de acceso de usuario) esto es para poderle dar logout
		    			 //mas tarde
		    			 if (requestSessionId != null) {
			    			 usuario.setIdSesionUsuario(requestSessionId.trim());
		    			 }
		    			 if (session != null) {
		    				 session.setAttribute(sistemaContext.getUsuarioAccesoMidas(), usuario);
		    			 } else {
		    				 req.setAttribute(sistemaContext.getUsuarioAccesoMidas(), usuario);
		    			 }
		    		 }
		    		 
		    	}
		    }
		    
		    if (usuario != null) {
		    	
		    	acceso = 1;
		    	

		    	if (req.getPathInfo() != null && req.getPathInfo().equals("/")) {
		    		//Se entiende que la pagina a visitar es la de bienvenida
		    		paginaDestino = req.getContextPath() + "/sistema/inicio.do";
		    	}
	
		    	
		    	if (sistemaContext.getSeguridadActivada()) { //Si la seguridad esta activada
		    		//Se busca el URL dentro del listado de p�ginas del objeto usuario para dejarlo pasar
		    		if (isUsuarioTienePermisoPagina(usuario, paginaDestino)) {
		    			acceso = 2;
		    		}
		    	} else {
		    		//Lo deja pasar
		    		acceso = 2;
		    	}
		    	
		    }
		      
		    if ((acceso == 2)||(esServicioPublico)) {
		    	chain.doFilter(request, response);
		    } else {
		    	 if (usuario != null && usuario.getNombreUsuario() != null) {
		    		 System.out.println("El usuario " + usuario.getNombreUsuario() + " no tiene permisos para el URL : " + paginaDestino);
		    	 }
		    	 if (!restClient) {
		    		 if(sistemaContext.getAsmActivo()){
					    	errorURL = onErrorUrl + "?clientApplicationId=" + sistemaContext.getMidasAppId() + "&clientUrl=" + sistemaContext.getMidasLogin();		    	
					    	res.sendRedirect(errorURL);	
			    	 }else{
					    	errorURL = onErrorUrl + "?acceso=" + acceso;
					    	req.getRequestDispatcher(errorURL).forward(req, res);		    		 
			    	 }
		    	 } else {
		    		 if (acceso == 0) {
		    			 res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		    		 } else if (acceso == 1) {
		    			 res.sendError(HttpServletResponse.SC_FORBIDDEN);
		    		 }
		    	 }
		    }
	    
	    }
	    
//	    req = null;
	}
	  
	public void destroy() {
	}
	
	private void handlePaginationCache(HttpServletRequest req) {
		
		String servletPath = req.getServletPath();
		String accionDestino = servletPath.substring(servletPath.lastIndexOf("/") + 1).trim();
		
		//Si no se trata de una accion para el paginado de una lista
		if (!accionDestino.equals(sistemaContext.getAccionPaginado())) {
			HttpSession session = req.getSession(false);
			//Si ya existia la variable de sesion de la lista paginada, la elimina
			if (session != null && session.getAttribute(sistemaContext.getListaPaginada()) != null) {
				session.removeAttribute(sistemaContext.getListaPaginada());
			}
		}
	}
	
	private boolean isUsuarioTienePermisoPagina(Usuario usuario, String pagina) {
		for (Pagina page : usuario.getPages()) {
			if (page.getNombreAccionDo().equals(pagina)){
				return true;
			}
		}
		
		return false;
	}
	
	
}
