/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.actuario;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuActuario {

	private List<Menu> listaMenu = null;
		
	public MenuActuario() {
		listaMenu = new ArrayList<Menu>();
	}
	
public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m5","Reaseguro", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m5_3","Reaseguro", "Reportes Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("39"),"m5_3_1","Reaseguro", "Administrativos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("44"),"m5_3_2","Reaseguro", "Movimientos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m5_3_2_6","Reaseguro", "Reporte de C�mulos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("50"),"m5_3_3","Reaseguro", "Siniestros", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("68"),"m5_3_3_5","Reaseguro", "Reportes de Siniestros de Reservas de Riesgo", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Acerca de", "Acerca de", "", true);
		listaMenu.add(menu);
		
		return this.listaMenu;
		
	}
	
}
