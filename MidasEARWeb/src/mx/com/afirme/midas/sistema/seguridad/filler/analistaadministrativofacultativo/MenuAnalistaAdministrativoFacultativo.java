/**
 * Clase que llena las opciones de Menu para el rol de Analista Administrativo Facultativo
 */
package mx.com.afirme.midas.sistema.seguridad.filler.analistaadministrativofacultativo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author Christian Antonio Gomez Flores
 *
 */
public class MenuAnalistaAdministrativoFacultativo {

	private List<Menu> listaMenu = null;
		
	public MenuAnalistaAdministrativoFacultativo() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
		menu = new Menu(new Integer("1"),"m2","Siniestros", "Menu Principal Siniestros", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("2"),"m2_1","Autos", "Submenu 1 Siniestros", null, false);
		listaMenu.add(menu);
		menu = new Menu(new Integer("3"),"m2_2","Vida", "Submenu 2 Siniestros", null, false);
		listaMenu.add(menu);
		menu = new Menu(new Integer("4"),"m2_3","Da�os", "Submenu 3 Siniestros", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("5"),"m2_3_1","Cabina", "Cabina Siniestros", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("6"),"m2_3_1_1","Iniciar Reporte", "Iniciar Reporte Siniestros", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("7"),"m2_3_2","Listar Reporte de Siniestro", "Listar Reportes Siniestros", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("8"),"m2_3_3","Analisis Interno", "Analisis Interno", null, true);
		listaMenu.add(menu);
		//J@matitla
		menu = new Menu(new Integer("9"),"m2_3_5","Aplicar Ingreso", "Aplicar Ingreso", null, true);
		listaMenu.add(menu);				
		menu = new Menu(new Integer("10"),"m6","Pendientes", "Menu Principal Pendientes", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("11"),"m6_1","Lista de Pendientes", "Listar Pendientes", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("12"),"m7","Ayuda", "Menu Principal Ayuda", null, true);
		listaMenu.add(menu);
		menu = new Menu(new Integer("13"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);		
		menu = new Menu(new Integer("14"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("15"),"m2_3_4_3","Evento Catastrofico", "Evento Catastrofico", null, true);
		listaMenu.add(menu);
		return this.listaMenu;
	}
	
}
