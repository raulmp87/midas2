package mx.com.afirme.midas.sistema.seguridad.filler.sistema;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuSistema {
private List<Menu> listaMenu = null;
	
	public MenuSistema() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		Menu menu;
		
		//TODO: Aqui se deben de agregar las opciones de menu comunes para todos los roles
		menu = new Menu(new Integer("11"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("9"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("12"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("13"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		return this.listaMenu;
	}
}
