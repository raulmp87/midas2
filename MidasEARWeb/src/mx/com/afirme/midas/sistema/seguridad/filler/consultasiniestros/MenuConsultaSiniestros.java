package mx.com.afirme.midas.sistema.seguridad.filler.consultasiniestros;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

public class MenuConsultaSiniestros {
 	private List<Menu> listaMenu = null;
	
	public MenuConsultaSiniestros() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"m7","Ayuda", "Submenu Ayuda", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("3"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);		
		
		menu = new Menu(new Integer("4"),"m2","Siniestros", "Menu Principal Siniestros", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("5"),"m2_3","Da�os", "Submenu 3 Siniestros", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("6"),"m2_3_2","Listar Reporte de Siniestro", "Listar Reportes Siniestros", null, true);
		listaMenu.add(menu);
		
		
		return this.listaMenu;
		
	}

}
